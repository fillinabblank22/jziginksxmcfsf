﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_UDT_Resources_Misc_GettingStarted" %>
<orion:resourceWrapper  runat="server" ID="Wrapper" class="GettingStartedDIV">
    <Content>
        <style type="text/css">
            
             
            .GettingStartedInnerDIV
            {
                 padding: 13px 10px 13px 10px;
                 background-color: #FFFFFF!Important;
                 border: 10px solid #204a63;
                 height:165px;
            }
            
            .GettingStartedRow1
            {
                background-repeat: no-repeat; 
	            background-position: right top; 
                padding-top: 5px;
                padding-right:156px;
                font-family: Arial, Verdana, Helvetica, sans-serif;
                font-size: 8pt;
                float: right;
                overflow:visible;
                text-align:right; 
                height:28px;
                width: auto;
                vertical-align: baseline;
            }
            
            
            .GettingStartedRemove
            {
                    float: right;
                    margin-top: 25px;
                    margin-bottom: 15px;
                    text-transform: uppercase;
                
            }
          .GettingStartedRow2
	    {
            padding-top: 5px;
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 8pt;
		    width: auto;
		    text-align:right; 
		    height:28px;
		    overflow:visible;
            vertical-align: baseline;
	     }
           
	  .sw-btn-primary{text-align:center;} 
	  .sw-btn:hover 
    	  {
	    color: #204a63 !important;
    	  }
        </style>
       
        <div class="GettingStartedInnerDIV">
                 <a class="sw-btn-primary sw-btn" href="<%= CentralDiscoveryPortsLink %>" style="float: right; width: 140px;"><span class="sw-btn-c" ><span class="sw-btn-t" style="display: inline-block;min-width: 100px; text-align: center;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_12 %></span></span> </a>      
            <a href="<%=CentralDiscoveryPortsLink %>" title="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_190 %>" class="GettingStartedRow1" style="float: left; padding-right: 0px; width: 60%; ">
            <%= Resources.UDTWebContent.UDTWEBDATA_AK1_191 %> </a>
<div style="clear:both;"></div> 
            
            <a href="<%=PortManagementLink %>" title="<%= Resources.UDTWebContent.UDTWEBDATA_MM_2 %>" class="GettingStartedRow2" style="float: left; padding-right: 0px; width: 60%; ">
            <%= Resources.UDTWebContent.UDTWEBDATA_MM_3 %> </a>
<a class="sw-btn-primary sw-btn" href="<%= PortManagementLink %>" style="float: right; width: 140px;"><span class="sw-btn-c" ><span class="sw-btn-t" style="display: inline-block;min-width: 100px; text-align: center;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_13 %></span></span> </a>      

<div style="clear:both;"></div> 
                
            <a href="<%=ManageWatchListLink %>" title="<%= Resources.UDTWebContent.UDTWEBDATA_AK1_192 %>" class="GettingStartedRow2" style="float: left; padding-right: 0px; width: 60%; ">
            <%= Resources.UDTWebContent.UDTWEBDATA_AK1_193 %> </a>
	<a class="sw-btn-primary sw-btn" href="<%= ManageWatchListLink %>" style="float: right; width: 140px;"><span class="sw-btn-c" ><span class="sw-btn-t" style="display: inline-block;min-width: 100px; text-align: center;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_14 %></span></span> </a>      
<div style="clear:both;"></div> 
                    
            <a href="<%=DiscoveryDCLink %>" title="<%= Resources.UDTWebContent.UDTWEBDATA_DF1_9 %>" class="GettingStartedRow2" style="float: left; padding-right: 0px; width: 60%; ">
            <%= Resources.UDTWebContent.UDTWEBDATA_DF1_10%> </a>
	    <a class="sw-btn-primary sw-btn" href="<%= DiscoveryDCLink %>" style="float: right; width: 140px;"><span class="sw-btn-c" ><span class="sw-btn-t" style="display: inline-block;min-width: 100px; text-align: center;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_15 %></span></span> </a>      
<div style="clear:both;"></div> 
            
            <a href="<%=SearchLink %>" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_132 %>" class="GettingStartedRow2" style="float: left; padding-right: 0px; width: 60%; ">
            <%= Resources.UDTWebContent.UDTWEBDATA_AK1_194 %> </a>
	    <a class="sw-btn-primary sw-btn" href="<%= SearchLink %>" style="float: right; width: 140px;"><span class="sw-btn-c" ><span class="sw-btn-t" style="display: inline-block;min-width: 100px; text-align: center;"><%= Resources.UDTWebContent.UDTWEBDATA_GK_16 %></span></span> </a>      
<div style="clear:both;"></div> 
        </div>
        
        <orion:LocalizableButton runat="server" ID="btnRemoveGettingStarted" CssClass="GettingStartedRemove sw-btn-secondary sw-btn sw-btn-c sw-btn-t" 
                                 LocalizedText="CustomText" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_195 %>" 
                                 DisplayType="Secondary" />
    </Content>
</orion:resourceWrapper>

<script type="text/javascript">



    $(document).ready(function () {
        //$("[id$='<%=WrapperHeaderID %>']").css("background", "transparent")
        //$("[id$='<%=WrapperHeaderID %>']").parent().css("background-image", "url('/Orion/UDT/images/GettingStarted/Background_GettingStarted.gif')")
       // $("[id$='<%=WrapperHeaderID %>']").parent().css("background-repeat", "no-repeat")
       // $("[id$='<%=WrapperHeaderID %>']").parent().css("background-color","#FDE0AE")
        
        // remove Help button in FF. For IE ToLowerCase() should be
       // $("[id$='<%=WrapperHeaderID %>']").children().each(function () { if ($(this).html().toLowerCase().indexOf("<img ") > -1) { $(this).remove(); } });
    });
</script>

