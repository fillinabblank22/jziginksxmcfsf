﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WatchList.ascx.cs" Inherits="Orion_UDT_Resources_Misc_WatchList" %>

<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <asp:UpdatePanel ID="ResourceUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
            <div style="overflow: auto">
                <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" />            
                        </td>                        
                    </tr>
                </table>
                <table runat="server" id="DeviceWatchList" border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" colspan="1">&nbsp;</td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_198 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_199 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_200 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_201 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_202 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_203 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_204 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_205 %></td>
                        <%--VRF Column Id is referred in RenderResource method of the CS file
                        Do not remove the Id and its value --%>
                        <td class="ReportHeader" colspan="1" id="Vrf"><%= Resources.UDTWebContent.UDTWEBDATA_GM1_232 %></td>
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" />
             </div>
            </ContentTemplate>
        </asp:UpdatePanel> 
	</Content>
</orion:resourceWrapper>