﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionResourceEdit.master" AutoEventWireup="true" CodeFile="EditWatchList.aspx.cs" Inherits="Orion_UDT_Resources_Misc_EditWatchList" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.UDT.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 style="padding: 20px 0px 20px 15px !important;"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_206,  CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(Resource.Title, Request["NetObject"].UrlEncode()))) %></h1>
       
    <div style="padding-left: 16px;">
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" ShowSubTitleHintMessage="false" />
        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_188 %>
        <br /><br />
        
        <b><%= Resources.UDTWebContent.UDTWEBDATA_AK1_189 %></b><br />
        <asp:TextBox runat="server" ID="maxRowsCount" Columns="5"/>
        <br /><br/>

        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
        </div>
        <br /><br />

    </div>
</asp:Content>

