﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Helpers;
using SolarWinds.UDT.Web.NetObjects;
using SolarWinds.UDT.Web.Providers;
using SolarWinds.UDT.Web.Resources;
using SolarWinds.Orion.Web.Charting;

public partial class Orion_UDT_Resources_RogueDevices : UDTBaseResourceControl
{
    private static readonly Log _log = new Log("RogueDevices");
    protected override Log GetLogger() { return _log; }

    private static readonly string[] ResourceSearchableColumns = new[] { "NetobjectID", "DisplayText" };
    protected string TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;
    protected int ResourceType;
    private DataTable allVendorMacs = null;
    private const string NetPerfMonImagePath = "/NetPerfMon/images/Vendors/";

    protected override String DefaultTitle
    {
        get { return Resources.UDTWebContent.UDTWEBCODE_MR1_4; }
    }

    public override String SubTitle { get { return Periods.GetLocalizedPeriod(TimePeriod); } }
    public override string HelpLinkFragment
    {
        get
        {
            string helpLink = Resource.Properties["HelpLink"];

            if (String.IsNullOrEmpty(helpLink))
            {
                helpLink = "OrionUDTPHRogueDevices";
                Resource.Properties["HelpLink"] = helpLink;
            }
            return helpLink;
        }
    }

    public override string EditURL
    {
        get { return string.Format("/Orion/UDT/Resources/Misc/EditRogueDevices.aspx?ResourceID={0}&ViewID={1}&NetObjectID={2}", this.Resource.ID, this.Resource.View.ViewID, HttpUtility.UrlDecode(Request.QueryString["NetObject"])); }
    }

    private bool ShowWelcomeScreen()
    {
        UDTRuleBase[] rules;
        using (var bl = BusinessLayerFactory.Create())
        {
            rules = bl.RulesGetAll();
        }
        return rules.Length == 7 && !rules.Any(r => r.RuleID > 7 || r.LastUpdate != r.FirstSeen);
    }

    private bool IsRogueFound()
    {

        using (var bl = BusinessLayerFactory.Create())
        {
            return bl.IsRogueFound();
        }
    }
    protected override Control GetResourceWrapperControl()
    {
        return Wrapper;
    }
    
    protected override void OnInitResource(EventArgs e)
    {
        Wrapper.ManageButtonImage = "~/Orion/UDT/Images/Resources/button_manage_list.gif";
        Wrapper.ManageButtonTarget = "~/Orion/UDT/Admin/WhiteList/ManageWhiteList.aspx";
        Wrapper.ShowManageButton = Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        SearchableColumns = ResourceSearchableColumns;
        try
        {
            if (Resource.Properties.ContainsKey("Period"))
                TimePeriod = Resource.Properties["Period"];

        }
        catch (Exception)
        {
            TimePeriod = Resources.UDTWebContent.UDTWCODE_VB1_51;
        }

        //Adding JS handler to add tooltips after async refresh (triggers livequery to check DOM changes)
        string scriptKey = "resourceLoadedHandler";
        Type type = this.GetType();

        if (!Page.ClientScript.IsStartupScriptRegistered(type, scriptKey))
        {
            Page.ClientScript.RegisterStartupScript(type, scriptKey, 
                @"
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(triggerLivequeryCheck);
                function triggerLivequeryCheck() { $("".notexisting"").addClass(""new""); }", 
                true);
        }
    }



    protected override DataTable LoadResourceData(string searchText)
    {

        var periodBegin = new DateTime();
        var periodEnd = new DateTime();
        Periods.Parse(ref TimePeriod, ref periodBegin, ref periodEnd);
        SearchInCachedData = false;
        var data = UDTDeviceDAL.GetRogueDevicesPopulated(periodBegin.ToUniversalTime(), periodEnd.ToUniversalTime(), searchText);
        return data;
    }

    protected override void RenderResource()
    {
        if (!IsPostBack) return;
        bool isroguefound = IsRogueFound();
        if ((!isroguefound) && ShowWelcomeScreen() && (Data == null || Data.Rows.Count == 0))
        {
            RogueDevices.Style.Add("display", "none");
            ResourcePager.Visible = false;
            ResourceSearcher.Visible = false;
            RogueDevicesGetStarted.Style.Add("display", "block");
            Resource.Properties["HelpLink"] = "OrionUDTPHCreateWL";
            return;
        }

        if ((!isroguefound) && (Data == null || Data.Rows.Count == 0)) //Display Safe Network box 
        {
            RogueDevices.Style.Add("display", "none");
            ResourcePager.Visible = false;
            ResourceSearcher.Visible = false;
            safeNetworkBox.Style.Add("display", "block");
            Resource.Properties["HelpLink"] = "OrionUDTPHWhiteListSecure";
            return;
        }
        var data = ApplyFiltering(Data);
        Resource.Properties["HelpLink"] = "OrionUDTPHRogueDevices";
        data = ApplyPaging(data);
        //build the table
        allVendorMacs = GetVendorMacPrefix();
       
        foreach (DataRow row in data.Rows)
        {
            var htmlRow = new HtmlTableRow();
            var htmlCell = new HtmlTableCell();
            var netObjectType = DALHelper.GetValueOrDefault(row["NetobjectType"], string.Empty);
            var deviceName = DALHelper.GetValueOrDefault(row["NetobjectID"], string.Empty);
            var lastSeen = DALHelper.GetValueOrDefault(row["LastSeen"], DALHelper.SQLMinDate);
            var watchID = DALHelper.GetValueOrDefault(row["WatchID"], 0);
            var displayText = DALHelper.GetValueOrDefault(row["DisplayText"], string.Empty);

            if (lastSeen != DALHelper.SQLMinDate)
                lastSeen = lastSeen.ToLocalTime();


            //Icon
            htmlCell.Attributes.Add("Class", "Property");
            htmlCell.InnerHtml = @"<img src='/Orion/UDT/Images/Rogue-Device.png'/>";
            htmlRow.Cells.Add(htmlCell);


            //Device Name
            htmlCell = new HtmlTableCell();
            htmlCell.Attributes.Add("Class", "Property");
            htmlCell.InnerHtml = string.IsNullOrEmpty(deviceName)
                                       ? UDTWebContent.UDTWEBDATA_MR1_20
                                       : String.Format(
                                           "&nbsp;<a href='/Orion/UDT/EndpointDetails.aspx?NetObject={0}:VAL={1}'>{2}</a>",
                                           netObjectType,
                                           deviceName, displayText);
            
            htmlRow.Cells.Add(htmlCell);

            // Adding Icon column
            htmlCell = new HtmlTableCell();
            htmlCell.Attributes.Add("Class", "Property");
            htmlCell.Align = "Left";
            if (netObjectType == UDTNetObjectType.EndpointMac)
            {
                string iconDetails = GetVendorIcon(displayText);
                htmlCell.InnerHtml = iconDetails;
            }
        else
            {
                htmlCell.InnerHtml = "";
            }
            htmlRow.Cells.Add(htmlCell);

            //Connection Duration
            htmlCell = new HtmlTableCell();
            htmlCell.Attributes.Add("Class", "Property");
            htmlCell.InnerHtml = String.Format("&nbsp;{0}", lastSeen);
            htmlRow.Cells.Add(htmlCell);

            string addDeviceLink =
                Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer
                    ? String.Format(
                        "»&nbsp;<a class='sw-link' href='/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&endpoint={0}&endpointType={1}&type={2}'>{3}</a>",
                        deviceName,
                        netObjectType,
                       Convert.ToInt16(UDTRuleType.Whitelist),
                        UDTWebContent.UDTWEBDATA_MR1_19)
                    : "";

            string watchDeviceLink = watchID == 0
                                     ? String.Format(
                                         "»&nbsp;<a class='sw-link' href='/Orion/UDT/ManageWatchList.aspx'>{0}</a>",
                                         UDTWebContent.UDTWEBDATA_MR1_18)
                                     : UDTWebContent.UDTWEBDATA_MR1_21;

            //add this device
            htmlCell = new HtmlTableCell();
            htmlCell.Attributes.Add("Class", "Property");
            htmlCell.InnerHtml = watchDeviceLink + "<br />" + addDeviceLink;
            htmlRow.Cells.Add(htmlCell);
            RogueDevices.Rows.Add(htmlRow);
        }
    }
    private DataTable GetVendorMacPrefix()
    {
        string cacheKey = "VendorMacPrefix";
       object cacheItem = Cache[cacheKey] as DataTable;
       if(cacheItem == null)
       {
          cacheItem = UDTDeviceDAL.GetAllVendorMacPrefix();
          Cache.Insert(cacheKey, cacheItem, null);
       }
       return (DataTable)cacheItem;
    }

    private string GetVendorIcon(string mac)
    {
        string macPrefix = "";
        string icon = "";
        string vendor = "";
        if (mac.Length > 6)
        {
            macPrefix = mac.Replace(":", "").Substring(0, 6);
        }
        if (allVendorMacs != null)
        {
            DataRow[] rows = allVendorMacs.Select(string.Format("MacPrefix ='{0}'", macPrefix));
            if (rows.Length > 0)
            {
                icon = Convert.ToString(rows[0]["Icon"]);
                vendor = Convert.ToString(rows[0]["Vendor"]);
                if (string.IsNullOrEmpty(icon))
                    return string.Format("<img src='{0}{1}' border='0' alt='{2}' style='vertical-align:middle' />", NetPerfMonImagePath, "Unknown.gif", vendor); 
                else
                    return HTMLVendorImage(icon, vendor); //Icon only 
            }
            else
            {
                return string.Format("<img src='{0}{1}' border='0' alt='{2}' style='vertical-align:middle' />", NetPerfMonImagePath, "Unknown.gif", vendor);
            }

        }
        return string.Format("<img src='{0}{1}' border='0' alt='{2}' style='vertical-align:middle' />", NetPerfMonImagePath, "Unknown.gif", vendor); 
    }
    // create html image tag
    
    private string HTMLVendorImage(string imgFile, string vendor)
    {
        string imagePath = Server.MapPath(NetPerfMonImagePath) + imgFile;
        if (File.Exists(imagePath))
            return string.Format("<img src='{0}{1}' border='0' alt='{2}' style='vertical-align:middle' />", NetPerfMonImagePath, imgFile, vendor);
        else
        {
            return string.Format("<img src='{0}{1}' border='0' alt='{2}' style='vertical-align:middle' />", NetPerfMonImagePath, "Unknown.gif", vendor);
        }
    }
    
    protected override void HandleDefaultDataLoad(bool suppressDataReload)
    {
        ReloadData = true;
        base.HandleDefaultDataLoad(suppressDataReload);
    }

}



