﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RogueDevices.ascx.cs" Inherits="Orion_UDT_Resources_RogueDevices" %>

<%@ Register TagPrefix="udt" TagName="ResourceSearcher" Src="~/Orion/UDT/Controls/ResourceSearcher.ascx" %>
<%@ Register TagPrefix="udt" TagName="ResourcePager" Src="~/Orion/UDT/Controls/ResourcePager.ascx" %>
<style type="text/css">
.CORE_GettingStartedBox
{
    background-color: #e4f1f8;
    padding: 1em;
    margin-top: 1em;    
    font:inherit;
    font-size:9pt; 
}

.CORE_GettingStartedBox div
{
    padding-top: 1em;
    clear: both;
}

.CORE_GettingStartedBox div.CORE_First
{
    padding-top: 0em;
}

.CORE_GettingStartedBox div h3
{
    font-weight: normal;
    font-size: 11pt;
    margin: 12px 0px 0px 40px;
}

.CORE_GettingStartedBox div p
{
    margin: 0px 0px 0px 25px;
    line-height: 1.8em;
}

.CORE_GettingStartedBox div img
{
    float: left;
    margin-top: 2px;
}

.CORE_Buttons
{
    text-align: right;
    padding-left:10px;
}
.sw-btn:hover 
{
   color: #204a63 !important;
}
</style>
<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>    
        <asp:UpdatePanel ID="ResourceUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table style="width: 100%; border-collapse: collapse">
                    <tr>
                        <td>&nbsp;</td>
                        <td align = "right" width="200px">
                            <udt:ResourceSearcher runat="server" ID="ResourceSearcher" Visible="true" Hidden="False"/>            
                        </td>                        
                    </tr>
                </table>
                
                <table runat="server" id="RogueDevices" border="0" cellpadding="2" cellspacing="0" width="100%" class="NeedsZebraStripes">
                    <tr>
                        <td class="ReportHeader" colspan="1">&nbsp;</td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_MR1_5 %></td>  
                        <td class="ReportHeader" colspan="1">&nbsp;</td>                      
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_MR1_7 %></td>
                        <td class="ReportHeader" colspan="1"><%= Resources.UDTWebContent.UDTWEBDATA_MR1_8 %></td>
                    </tr>
                </table>
                <udt:ResourcePager runat="server" ID="ResourcePager" Visible="True" Hidden="False" />             

          <div id="RogueDevicesGetStarted" style="display: none" runat="server">
           <div class="CORE_GettingStartedBox">
           <div class="CORE_First">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%= Resources.UDTWebContent.UDTWEBDATA_TM0_1%></b> <%= Resources.UDTWebContent.UDTWEBDATA_MR1_12 %></h3>
              </div>
              <div>
              <%= Resources.UDTWebContent.UDTWEBDATA_MR1_10 %><br/>
              <%= Resources.UDTWebContent.UDTWEBDATA_MR1_11 %>
              </div>
              
                <div class="CORE_Buttons">
                  <orion:LocalizableButton runat="server" ID="createNewWhiteList" PostBackUrl="~/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources:UDTWebContent,UDTWEBDATA_MR1_12 %>" />
              </div>
           </div>             
           </div>

           <div id="safeNetworkBox" style="display: none" runat="server">
            <div class="CORE_First">            
            <table>
            <td>
            <img src="/Orion/UDT/images/check_32x32.png" alt="Safe Device" />
            </td>
            <td>
            <%= Resources.UDTWebContent.UDTWEBDATA_MR1_14 %><br/>
             <%= Resources.UDTWebContent.UDTWEBDATA_MR1_15 %>
            </td>
            </table>
              </div>
           </div>   
           
                <orion:ExceptionWarning ID="ExceptionWarning" Visible="False" Text="" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
	</Content>
</orion:resourceWrapper>