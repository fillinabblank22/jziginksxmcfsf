﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ADDCPollingDetails.ascx.cs" Inherits="Orion_UDT_Resources_Misc" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
        <asp:UpdatePanel ID="MyPanel" runat="server">
            <ContentTemplate> 

        <link rel="stylesheet" type="text/css" href="/Orion/UDT/UDT.css" />
        <orion:Include runat="server" File="OrionCore.js" />
        <script type="text/javascript">
            var id = <%=NodeID %>;

            ORION.callWebService("/Orion/UDT/Services/ManageDomainControllers.asmx", "CheckLastScanResult", { nodeId: id }, function (result) {
				result = JSON.parse(result);
                if (!result.UserDetailPollingStatus) { //error
                    $("#<%=dcPollingStatusIcon.ClientID %>").attr("src", "/Orion/UDT/Images/Admin/Discovery/error_16x16.gif");
                    $("#<%=dcPollingStatus.ClientID %>").html(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_8) %>", "<a class=\"udtCredentialsLink\" href=\"/Orion/UDT/ManageDomainControllers.aspx\">", "</a>")); 
                }
                else {
                    $("#<%=dcPollingStatusIcon.ClientID %>").attr("src", "/Orion/UDT/Images/Admin/Discovery/OK_good.gif");
                    $("#<%=dcPollingStatus.ClientID %>").text("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_9) %>");
                }
            });
        
        </script>

        <%--AD Domain Controller Poling details table--%>
        <table runat="server" id="ADDCPollingDetails" border="0" cellpadding="2" cellspacing="0" width="100%" visible="true" class="NeedsZebraStripes">
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader" width="120"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_184 %></td>
              <td class="Property" width="10" ><asp:Image runat="server" CssClass="Property" ID="dcPollingStatusIcon"/>&nbsp;</td>
	          <td class="Property" >
                <asp:Label runat="server" ID="dcPollingStatus" />&nbsp;
              </td>
	        </tr>
	        <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_185 %></td>
              <td class="Property">&nbsp;</td>
              <td class="Property"><asp:Label runat="server" ID="dcPollingInterval" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_186 %></td>
	          <td class="Property">&nbsp;</td>
              <td class="Property"><asp:Label runat="server" ID="dcNextPoll" />&nbsp;</td>
	        </tr>
            <tr>
	          <td class="Property" width="10">&nbsp;</td>
	          <td class="PropertyHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_187 %></td>
	          <td class="Property">&nbsp;</td>
              <td class="Property"><asp:Label runat="server" ID="dcLastSuccessfulPoll" />&nbsp;</td>
	        </tr>
        </table>
<%--                <td><asp:LinkButton ID="showEmailAddressesLink" runat="server" Text="more..." onclick="showEmailAddressesClick" visible="false" /><asp:LinkButton ID="hideEmailAddressesLink" runat="server" Text="less..." onclick="hideEmailAddressesClick" visible="false" /></td>--%>

            </ContentTemplate>
        </asp:UpdatePanel> 
	</Content>
</orion:resourceWrapper>
