﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllAlerts.ascx.cs" Inherits="Orion_UDT_Resources_Alerts_AllAlerts" %>
<%@ Register TagPrefix="orion" TagName="AlertsTable" Src="~/Orion/UDT/Controls/AlertsOnThisEntity.ascx" %>

<orion:resourceWrapper runat="server" ID="wrapper" ShowEditButton="true">
    <Content>
        <orion:AlertsTable runat="server" ID="AlertsTable" />
    </Content>
</orion:resourceWrapper>