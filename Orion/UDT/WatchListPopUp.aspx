﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WatchListPopUp.aspx.cs" Inherits="Orion_UDT_WatchListPopUp" %>
<%@ Import Namespace="System.Globalization" %>

<h3 class="<%=StatusCssClass%>"><%=SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTWatchList.Name)%></h3>

<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
		<tr><td colspan="2"><%=ActivelyConnected%></td></tr>
        <tr><th><%= Resources.UDTWebContent.UDTWEBDATA_VB1_26 %></th><td colspan="1"><%=UDTWatchList.IPAddress%></td></tr>
		<tr><th><%= Resources.UDTWebContent.UDTWEBDATA_VB1_27 %></th><td colspan="1"><%=UDTWatchList.Hostname%></td></tr>
        <tr><th><%= Resources.UDTWebContent.UDTWEBDATA_VB1_25 %></th><td colspan="1"><%=UDTWatchList.MACAddress%></td> </tr>
        <tr><th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_223 %></th><td colspan="1"><%=UDTWatchList.UserName%></td> </tr>
		<tr <%=WiredVisible%>>
		    <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_224 %></th><td colspan="1"><%=UDTWatchList.NodeName%></td>
		</tr>
		<tr <%=WiredVisible%>>
		    <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_225 %></th><td colspan="1"><%=UDTWatchList.PortName%></td>
		</tr>
        <tr <%=WiredVisible%>>
            <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_226 %></th><td colspan="1"><%=UDTWatchList.PortDescription%></td>
        </tr>
        <tr <%=WiredVisible%>>
            <th><%= Resources.UDTWebContent.UDTWEBDATA_VB1_88 %></th><td colspan="1"><%=UDTWatchList.VlanID <= 0 ? "" : UDTWatchList.VlanID.ToString(CultureInfo.InvariantCulture) %></td>
        </tr>
		<tr <%=WirelessVisible%>>
		    <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_227 %></th><td colspan="1"><%=UDTWatchList.APName%></td>
		</tr>
		<tr <%=WirelessVisible%>>
		    <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_228 %></th><td colspan="1"><%=UDTWatchList.SSID%></td>
		</tr>
		<tr <%=WirelessVisible%>>
		    <th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_155 %></th><td colspan="1"><%=UDTWatchList.NodeName%></td>
		</tr>
        <tr><th><%=FirstSeenMessage%></th><td colspan="1"><%=UDTWatchList.FirstSeen%></td></tr>
        <tr><th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_152 %></th><td colspan="1"><%=UDTWatchList.LastSeen%></td></tr>
		<tr><th><%= Resources.UDTWebContent.UDTWEBDATA_AK1_229 %></th><td colspan="1"><%=UDTWatchList.WatchNote%></td></tr>
        <tr><th><%= Resources.UDTWebContent.UDTWEBDATA_GM1_233%></th><td colspan="1"><%=UDTWatchList.VrfName%></td></tr>
	</table>
</div>