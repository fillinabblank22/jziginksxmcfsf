﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageWatchList.aspx.cs" Inherits="Orion_UDT_ManageWatchList"
MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_1 %>" %>

<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Src="~/Orion/UDT/Admin/Discovery/Controls/WatchList.ascx" TagPrefix="udt" TagName="WatchList" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <script src="/Orion/js/jquery.js" type="text/javascript"></script>
    <script src="/Orion/js/jquery/ui.core.js" type="text/javascript"></script>
    <script src="/Orion/js/jquery/ui.dialog.js" type="text/javascript"></script>
    <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/UDT/Admin/Discovery/js/SearchField.js"></script> 
    <script type="text/javascript" src="/Orion/UDT/Admin/Discovery/js/WatchList.js"></script>
    <link rel="stylesheet" type="text/css" href="/Orion/UDT/UDT.css" />
    <style type="text/css">
    .WatchListBody 
    {
        padding: 10px;
    }
    .add { background-image:url(/Orion/images/add_16x16.gif) !important; }
    .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
    .updateAll { background-image:url(/Orion/images/pollnow_16x16.gif) !important; }
    .export { background-image:url(/Orion/UDT/images/export_file_icon.png) !important; }
    .searchAccounts {background-image:url(/Orion/Admin/Accounts/images/search/search_button.gif) !important; }     
    .clearAccounts {background-image:url(/Orion/Admin/Accounts/images/search/clear_button.gif) !important; }     
    .del { background-image:url(/Orion/images/delete_16x16.gif) !important; }
</style>
</asp:Content>

<asp:Content id="ManageDomainControllersTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
     <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False" />
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-bottom: 7px"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_3%></h1>
</asp:Content>

<asp:Content id="SearchResultsTitle" ContentPlaceHolderID="UdtMainContentPlaceHolder" runat="Server">
    <div style="padding-left:10px;">
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_2 %>
    </div>
    <div class="WatchListBody" style="padding: 10px 10px 0px 10px; "><udt:WatchList runat="server" id="manageWatchList" />
    </div>

    <%if (_isOrionDemoMode) {%> <div id="isOrionDemoMode" style="display: none;"/> <%}%>
</asp:Content>
