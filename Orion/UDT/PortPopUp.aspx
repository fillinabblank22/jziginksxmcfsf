﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PortPopUp.aspx.cs" Inherits="Orion_UDT_PortPopUp" %>

<h3 class="<%=this.StatusCssClass%>"><th><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_158, SolarWinds.Orion.Web.Helpers.UIHelper.Escape(UDTPort.Name)) %></th></h3>


<div class="NetObjectTipBody">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th style="white-space: nowrap;"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_158, UDTPort.Name) %></th>
             <td colspan="2"><%=this.PortDescription%></td> 
		</tr>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_159 %></th>
			<td colspan="2"><%=UDTPort.OperationalStatus%></td>    
		</tr>

		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_160 %></th>
            <td colspan="2"><%=UDTPort.AdministrativeStatus%></td>  
		</tr>

        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_161 %></th>
			<td colspan="2"><%=UDTPort.PortType%></td>  
		</tr>

		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_162 %></th>
            <td colspan="2"><%=this.ActiveConnectionMessage%></td> 
		</tr>
        <asp:Panel ID="VLANPanel" runat="server">
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_88 %></th>
			<td colspan="2"><%=vlanStringTrunk%></td> 
		</tr>
        </asp:Panel>
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_163 %></th>
             <td colspan="2"><%=UDTPort.Speed%></td>             
		</tr>
		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_164 %></th>
             <td colspan="2"><%=UDTPort.Duplex%></td>             
		</tr>

        <asp:Panel ID="IpPanel" runat="server">
        <tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_165 %></th>
			<td colspan="2"><%=UDTPort.ConnectedIPsCount%></td> 
		</tr>

		<tr>
			<th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_166 %></th>
            <td colspan="2"><%=UDTPort.ConnectedMACsCount%></td> 
		</tr>
        </asp:Panel>
        <tr>
		    <th style="white-space: nowrap;"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_167 %></th>
            <td colspan="2"><%=this.IsMonitoredMessage%></td> 
		</tr>
	</table>
</div>

