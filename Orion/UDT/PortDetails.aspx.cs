using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Web.Views;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;

public partial class Orion_UDT_PortDetails : UdtViewPage
{
    public override string ViewType
    {
        get
        {
            return "UDT PortDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = getTitle();
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHPortDetailsSummary";
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    protected string getTitle()
    {
        int portId = 0;
        string viewTitle = this.ViewInfo.ViewTitle;
        try
        {
            string tmpPort = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
            string[] netObjectParts = tmpPort.Split(':');
            portId = int.Parse(netObjectParts[1]);
            if (portId>0)
            {
                string caption = UDTPortDAL.GetNodeCaption(portId);
                string portName = string.Empty;
                string nodeId = string.Empty;
                try
                {
                    DataTable portDetails = UDTPortDAL.GetPortDetails(portId);
                    portName = portDetails.Rows[0]["Name"].ToString();
                    nodeId = portDetails.Rows[0]["NodeID"].ToString();
                }
                catch (Exception)
                {
                    
                }

                if (!string.IsNullOrEmpty(caption))
                {
                    viewTitle += String.Format(" - <a href='/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}'>{1}</a> - {2}", nodeId, caption, portName);
                }
            }
        }
        catch (Exception)
        {
        }

        return viewTitle;
    }

}
