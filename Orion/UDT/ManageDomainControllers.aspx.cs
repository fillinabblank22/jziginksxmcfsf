﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.UDT.Web.DAL;


public partial class Orion_UDT_ManageDomainControllers : System.Web.UI.Page
{

    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {

        PageTitleBar.PageTitle = Resources.UDTWebContent.UDTWEBDATA_VB1_14;
        
        //hide the customize page link for this page
        PageTitleBar.HideCustomizePageLink = true;
        //todo add help link
        PageTitleBar.HelpFragment = "OrionUDTPHManageADDomainControllers";

        PageSubTitle.Text = Resources.UDTWebContent.UDTWCODE_VB1_4;
       
    }
    
    protected override void OnInit(EventArgs e)
    {
        
        //this.resContainer.DataSource = this.ViewInfo;
        //this.resContainer.DataBind();
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }

        base.OnInit(e);
    }
    
    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }
}
