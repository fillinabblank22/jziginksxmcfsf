﻿SW.UDT = SW.UDT || {};
SW.UDT.PageableResource = SW.UDT.PageableResource || {};

(function (pageableResource) {

    var reloadGrid = function (settings, useCacheIfPossible) {
        var container = settings.container;
        var pageNumber = container.data('pageNumber') || 1;
        var searchText = container.data('searchText') || '';
        var rowsPerPage = container.data('rowsPerPage') || settings.defaultRowsPerPage;
        var cacheEnabled = false || settings.cacheEnabled;

        var param = {
            pageNumber: pageNumber,
            rowsPerPage: rowsPerPage,
            searchText: searchText,
            useCacheIfPossible: useCacheIfPossible && cacheEnabled
        };
        if (typeof (settings.createWebServiceParams) === "function")
            param = settings.createWebServiceParams(param);

        setLoadingState(container, true);

        callWebService(settings.webServiceUrl, settings.webServiceMethod, param, function (result) {
            // Handle Loaded Rows
            var rows = $(result.htmlRows).find('tr');
            $('tbody.results', container).empty().append(rows);
            updateButtons(pageNumber, rowsPerPage, result.rowsPerPageActual, rows.length, result.totalRows, settings.defaultRowsPerPage, container);

            if ((typeof (result.errorMessage) === 'string') && (result.errorMessage.length > 0))
                setErrorMessage(result.errorMessage, container);

        }, function (errorMessage) {
            // Handle the error
            if (typeof (errorMessage) === 'undefined')
                errorMessage = '@{R=UDT.Strings;K=UDTWEBJS_VB1_133;E=js}';

            setErrorMessage(errorMessage, container);
            updateButtons(pageNumber, rowsPerPage, rowsPerPage, 0, 0, settings.defaultRowsPerPage, container);

        });


    };

    var setErrorMessage = function (errorText, container) {
        $('.errorMsg', container).show().find('.errorMsgText').text(errorText);
    };

    var setLoadingState = function (container, isLoading) {
        var imageUrl = isLoading ? '/Orion/js/extjs/3.4/resources/images/default/grid/grid-loading.gif' : '/Orion/js/extjs/3.4/resources/images/default/grid/refresh.gif';
        var loadingText = isLoading ? '@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}' : '';

        var img = $('.btnRefresh img', container);
        img.attr('src', imageUrl);

        $('.displayText', container).text(loadingText);
    };


    var updateButtons = function (pageNumber, rowsPerPageRequested, rowsPerPageActual, rowsShown, totalRows, defaultRowsPerPage, container) {
        var firstItem = (pageNumber - 1) * rowsPerPageActual + 1;
        var lastItem = firstItem + rowsShown - 1;

        var isFirstPage = (pageNumber === 1) || (rowsShown === 0);
        var isLastPage = (lastItem === totalRows) || (rowsShown === 0);

        setLoadingState(container, false);

        enableDisableButton($('.btnFirst', container), !isFirstPage);
        enableDisableButton($('.btnPrev', container), !isFirstPage);
        enableDisableButton($('.btnNext', container), !isLastPage);
        enableDisableButton($('.btnLast', container), !isLastPage);

        var displayText = '@{R=UDT.Strings;K=UDTWEBJS_VB1_134;E=js}';
        if (rowsShown > 0) {
            displayText = String.format('@{R=UDT.Strings;K=UDTWEBJS_VB1_135;E=js}', firstItem, lastItem, totalRows);
        }

        $('.displayText', container).text(displayText);

        var lastPageNumber = Math.ceil(totalRows / rowsPerPageActual);
        container.data('lastPageNumber', lastPageNumber);

        var showAllText = '@{R=UDT.Strings;K=UDTWEBJS_VB1_136;E=js}';
        if (rowsPerPageRequested < 0) {
            showAllText = String.format('@{R=UDT.Strings;K=UDTWEBJS_VB1_137;E=js}', defaultRowsPerPage);
        }

        $('.btnViewAll', container).html(showAllText);

    };

    var enableDisableButton = function (link, shouldEnable) {
        var img = link.find('img');
        var imgSrc = img.attr('src');

        if (shouldEnable && imgSrc.endsWith('-disabled.gif')) {
            imgSrc = imgSrc.replace('-disabled.gif', '.gif');
        }

        if (!shouldEnable && !imgSrc.endsWith('-disabled.gif')) {
            imgSrc = imgSrc.replace('.gif', '-disabled.gif');
        }

        img.attr('src', imgSrc);

        var methodName = shouldEnable ? 'removeClass' : 'addClass';
        img[methodName]('disabledButton');
    };

    var isButtonDisabled = function (link) {
        return ($(link).find('img').hasClass('disabledButton'));
    };


    // copied from /Orion/js/OrionCore.js.  In Galaga OrionCore.js will not require ext, but it is required for now so we have to use our own copy :(
    var callWebService = function (serviceName, methodName, param, onSuccess, onError) {
        var paramString = JSON.stringify(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: serviceName + "/" + methodName,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                onSuccess(msg.d);
            },
            error: function (xhr, status, errorThrown) {
                handleError(xhr, onError);
            }
        });
    };

    // copied from /Orion/js/OrionCore.js.  In Galaga OrionCore.js will not require ext, but it is required for now so we have to use our own copy :(
    var handleError = function (xhr, customErrorHandler) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
            window.location.reload();
        }
        var errorText = xhr.responseText;
        var msg;
        try {
            var error = eval("(" + errorText + ")");
            msg = error.Message;
            $("#stackTrace").text(error.StackTrace);
        } catch (err) {
            msg = err.description;
        }
        $("#test").text(msg);
        if (typeof (customErrorHandler) == 'function') {
            customErrorHandler(msg);
        }
    };


    pageableResource.initialize = function (settings) {
        var container = settings.container;

        settings.defaultRowsPerPage = settings.defaultRowsPerPage || 10;

        $('.btnFirst', container).click(function () {
            if (isButtonDisabled(this)) return false;

            container.data('pageNumber', 1);
            reloadGrid(settings, true);
            return false;

        });
        $('.btnPrev', container).click(function () {
            if (isButtonDisabled(this)) return false;

            var pageNumber = container.data('pageNumber') || 1;
            container.data('pageNumber', pageNumber - 1);
            reloadGrid(settings, true);
            return false;

        });
        $('.btnNext', container).click(function () {
            if (isButtonDisabled(this)) return false;

            var pageNumber = container.data('pageNumber') || 1;
            container.data('pageNumber', pageNumber + 1);
            reloadGrid(settings, true);
            return false;
        });
        $('.btnLast', container).click(function () {
            if (isButtonDisabled(this)) return false;

            var lastPageNumber = container.data('lastPageNumber') || 1;
            container.data('pageNumber', lastPageNumber);
            reloadGrid(settings, true);
            return false;
        });
        $('.btnRefresh', container).click(function () {
            if (isButtonDisabled(this)) return false;

            reloadGrid(settings, false);
            return false;
        });
        $('.btnViewAll', container).click(function () {
            if (isButtonDisabled(this)) return false;

            var rowsPerPage = container.data('rowsPerPage') || settings.defaultRowsPerPage;
            rowsPerPage = (rowsPerPage < 0) ? settings.defaultRowsPerPage : -1;
            container.data('rowsPerPage', rowsPerPage);
            container.data('pageNumber', 1);
            reloadGrid(settings, true);
            return false;
        });
        $('.btnClearSearch', container).click(function () {
            if (isButtonDisabled(this)) return false;
            $('.searchInput', container).val('').blur();
            $('.btnSearch', container).click();
            return false;
        });
        $('.btnSearch', container).click(function () {
            if (isButtonDisabled(this)) return false;

            var searchInput = $('.searchInput', container);
            var searchText = searchInput.val();
            if (searchText == searchInput.attr('data-placeholder'))
                searchText = '';

            container.data('searchText', searchText);
            container.data('pageNumber', 1);

            $('.btnClearSearch', container).toggle(searchText.length !== 0);
            reloadGrid(settings, true);
            return false;
        });

        $('.searchInput', container).keydown(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();
                $('.btnSearch', container).click();
            }
        }).focus(function () {
            var input = $(this);
            if (input.val() == input.attr('data-placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function () {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('data-placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('data-placeholder'));
            }
        }).blur();


        reloadGrid(settings, false);

    };

})(SW.UDT.PageableResource);

