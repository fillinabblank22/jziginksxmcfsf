﻿SW.UDT = SW.UDT || {};
SW.UDT.Charts = SW.UDT.Charts || {};
SW.UDT.Charts.PieChartLegend = SW.UDT.Charts.PieChartLegend || {};

(function (legend) {
    var MIN_WIDTH_FOR_RIGHT = 550;
    var MAX_PIE_WIDTH = 250;

    var updateLegend = function (table) {
        var containerWidth = table.parent().parent().width();

        // toggle css for right versus bottom legend
        if (containerWidth >= MIN_WIDTH_FOR_RIGHT) {
            // RIGHT LEGEND
            table.css({ "max-width": "none", "border-collapse": "inherit", "margin-bottom": "20px" });
            table.find("tr").css("border-bottom", "1px solid #dadada");
            table.parent().css({ "display": "block", "max-width": "none", "min-height": "inherit", "overflow": "hidden" });
            table.parent().parent().find(".hasChart").css({ "float": "left", "margin": "0px auto" });
            table.find("tr:first").show();
            table.find("tr:last").hide();
            table.find("tr td:last-child").css("text-align", "left");

            // fix ie bug
            if ($.browser.msie) {
                var h = table.height();
                var topOffset = (MAX_PIE_WIDTH - h) * 0.5;
                if (topOffset < 0) {
                    topOffset = 0;
                }
                topOffset = topOffset + "px";
                table.css("position", "relative");
                table.css("top", topOffset);
            }
        }
        else {
            // BOTTOM LEGEND
            table.css({ "max-width": "none", "border-collapse": "inherit", "margin-bottom": "20px" });
            table.find("tr").css("border-bottom", "1px solid #dadada");
            table.parent().css({ "display": "block", "min-height": "inherit", "overflow": "visible" });
            table.parent().parent().find(".hasChart").css({ "float": "none", "margin": "0px auto" });
            table.find("tr:first").show();
            table.find("tr:last").hide();
            table.find("tr td:last-child").css("text-align", "left");

            if ($.browser.msie) {
                table.css({ "position": "inherit", "top": "0px" });
            }
        }
    };

	var addLegendSymbol = function (container, dataPoint) {
		var symbolWidth = 16;
		var symbolHeight = 12;
		var label = $('<span class="legendSymbol" />');
		var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
		var symbolColor = dataPoint.color;
		renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                	stroke: symbolColor,
                	fill: symbolColor
                })
                .add();

		label.appendTo(container);
	};

	legend.createLegend = function (chart, dataUrlParameters, legendContainerId, maxRowsToShow) {
	    var legendOptions = chart.options.UDTLegendPieOptions || { showInReverseOrder: false, detailsURL: "" };  // default options
	    var typeLabel = chart.options.ChartTypeLabel;
	    var headerLabel = chart.options.HeaderColumnLabel;
	    var totalsLabel = chart.options.FooterTotalLabel;
		var formatter = SW.Core.Charts.dataFormatters[chart.options.Formatter];
		var table = $('#' + legendContainerId);
	    var series = chart.series[0];
	    var row = {};	      

		table.empty();
		table.css({ "min-width": "50px", "max-width": "300px" });
		table.attr("cellspacing", "0");
		table.parent().parent().find(".hasChart").css({ "max-width": "0px", "min-width": "250px", "height": "0", "min-height": "250px", "float": "left" });
		table.parent().css({ "min-height": "250px" });
		table.parent().find("#chartDataNotAvailable").css("width", "200px");
		table.find("tr:last").addClass("udt_PieTotalRow");

		$(document).bind("resizestop", function () {
		    setTimeout(function () {
		        updateLegend(table);
		    }, 100);
		});

		var transCount = 0;
		for (var i = 0; i < series.data.length; i++) {
		    var point = series.data[i];
		    transCount += point.y;
		}

		if (transCount > 0) {		   
		    for (var i = 0; i < series.data.length; i++) {
		         var point = series.data[i];

		        if (point.legendLabel != "") {
		            row = $('<tr style="height:24px;" class="pie_details_row_' + legendContainerId + '" data-id="' + point.id + '" />');

		            var colorTD = $('<td style="text-align:left;padding-left:8px;width:20px;min-width:20px;max-width:20px;">').appendTo(row);
		            addLegendSymbol(colorTD, point);
		            $('<td style="text-align:left;padding-left:4px">' + point.legendLabel + '</td>').appendTo(row);
		            $('<td style="text-align:right;padding-right:8px;">' + formatter(point.y, chart.yAxis) + '</td>').appendTo(row);

		            if (legendOptions.showInReverseOrder) {
		                row.prependTo(table);
		            }
		            else {
		                row.appendTo(table);
		            }
		        }
		    }

		    // header row
		    row = $('<tr style="background-color:#e0e0e0;"><td style="padding:6px 8px;text-transform:uppercase;" colspan="2">' + typeLabel + '</td><td style="width:50% !important;padding:6px 0px;text-transform:uppercase;">' + headerLabel + '</td></tr>');
		    row.prependTo(table);

		    if (series.data.length > maxRowsToShow) {
		        row = $('<tr style="height:30px;"><td colspan="3" style="text-align:left;padding-left:8px;"><a style="color:#336699" target="_blank" href="/Orion/Report.aspx?Report=OUI+Summary+Report">' + '@{R=UDT.Strings;K=UDTWEB_JS_CODE_GP1_1;E=js}' + ' </a></td></tr>');
		        row.appendTo(table);
		    }

            // footer row
		    row = $('<tr style="height:30px;"><td></td><td><b>' + totalsLabel + ':</b></td><td style="text-align:right;padding-right:8px;"><b>' + formatter(transCount, chart.yAxis) + '</b></td></tr>');
		    row.appendTo(table);
		}

		if (legendOptions.detailsURL !== "") {
		    $(".pie_details_row_" + legendContainerId).css("cursor", "pointer");

		    $(".pie_details_row_" + legendContainerId).hover(
                function () {
                    $(this).css({ "background-color": "#eaeaea" });
                },
                function () {
                    $(this).css({ "background-color": "transparent" });
                }
            );

		    $(".pie_details_row_" + legendContainerId).click(function () {

		        var item = $(this).attr("data-id");

		        if (item == "Other") {

		            window.open(legendOptions.detailsURL + $(this).attr("data-id") + '&isOtherLegend=Yes', "_blank");
		        }
		        else {
		            window.open(legendOptions.detailsURL + $(this).attr("data-id") + '*', "_blank");
		        }
		    });
		}

		updateLegend(table);
	};

})(SW.UDT.Charts.PieChartLegend)