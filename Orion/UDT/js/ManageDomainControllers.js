﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

var delDialog;
var isAllSelected;
var selectedItems;

var dcSetting;

SW.UDT.ManageDomainControllers = function () {

    ORION.prefix = "UDT_ManageDomainControllers_";

    // update the Ext grid size when window size is changed
    $(window).resize(gridResize);

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        var total = grid.store.getCount();

        map.EditButton.setDisabled(count === 0);
        map.AssignCredentialsButton.setDisabled(count === 0);
        map.DeleteButton.setDisabled(count === 0);

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.AssignCredentialsButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }

        // uncheck the selected all checkbox if an item is deselected
        if (count < selectedItems) {
            isAllSelected = false;
            var obj = $('.x-grid3-hd-checker');
            if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                obj.removeClass('x-grid3-hd-checker-on');
        }

        selectedItems = count;

    }


    var beforeCheckAllClickHandler = function (sm, checked) {
        sm.lockSelect = true;
    }

    var afterCheckAllClickHandler = function (sm, checked) {


        $('.grpCheckbox').checked = checked;
        $('.grpCheckbox').attr('checked', checked);
        sm.lockSelect = false;
    }


    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }


    // *** GRID COLUMN DATA RENDERING FUNCTIONS ***
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    }

    // Column rendering functions
    function renderTextColumn(value) {
        if (value == null) {
            return value;
        }
        else {
            //alert('value: ' + value);
            return '<div style="white-space:normal !important;">' + value + '</div>';
        }
    }

    function renderStatusIconWithNodeCaption(value, meta, record, rowIndex, colIndex, store) {
        // renders node status icon next to node caption
        var img;
        var statusLED = record.data["DCStatusLED"];
        var statusDescription = record.data["DCStatusDescription"];

        if (statusLED.indexOf(".gif") == -1) {
            statusLED = 'unknown.gif';
            statusDescription = '@{R=UDT.Strings;K=UDTWEBJS_VB1_52;E=js}';
        }

        if (value == null) {
            formatted = '';
        }

        else // add node caption text and node status icon
        {
            img = '<img src="/Orion/images/StatusIcons/Small-' + statusLED + '" title="' + statusDescription + '" alt="' + statusDescription + '" style="vertical-align:bottom;" />&nbsp;';

            var urlLink = '<a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + record.data["DCNodeID"].toString() + '">' + value + '</a>';
            var formatted = img + urlLink;
        }

        return formatted;

    }

    function renderCredentials(value) {
        return value;
    }


    function renderTestCredentials(value, meta, record, rowIndex, colIndex, store) {
        var nodeId = record.data["DCNodeID"];

        switch (value) {
            case 1:

                img = '<img src="/Orion/UDT/Images/Admin/Discovery/OK_good.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" style="vertical-align:bottom;" />&nbsp;';
                text = "@{R=UDT.Strings;K=UDTWEBJS_VB1_54;E=js}";

                break;
            case 2:

                img = '<img src="/Orion/UDT/Images/Admin/Discovery/error_16x16.gif" title="@{R=UDT.Strings;K=UDTWEBDATA_GK1_24;E=js}" alt="@{R=UDT.Strings;K=UDTWEBDATA_GK1_24;E=js}" style="vertical-align:bottom;" />&nbsp;';
                text = String.format('@{R=UDT.Strings;K=UDTWEBDATA_GK1_24_F;E=js}', '<a class=\"udtCredentialsLink\" href=# onclick=\"assignCredentialsLink(', nodeId, ');\" >', '</a>');

                break;
            default:
                img = '<img src="/Orion/UDT/Images/Admin/Discovery/Icon_Info.PNG" title="@{R=UDT.Strings;K=UDTWEBDATA_GK1_25;E=js}" alt="@{R=UDT.Strings;K=UDTWEBDATA_GK1_25;E=js}" style="vertical-align:bottom;" />&nbsp;';
                text = '@{R=UDT.Strings;K=UDTWEBDATA_GK1_25;E=js}';
                break;

        }

        return img + text;
    }


    // function to get the list of selected Node ids
    function getSelectedIds() {
        var ids = [];
        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["DCNodeID"]);
        });

        return ids;
    }


    // add button clicked
    addLink = function () {
        window.location = "/Orion/Nodes/Add/Default.aspx"; //?&ReturnTo=" + $('#ReturnToUrl').val();
    }


    // edit button clicked
    editLink = function () {

        var ids = getSelectedIds();

        // call the Edit node page passing the selected nodes ids
        window.location = "/Orion/Nodes/NodeProperties.aspx?Nodes=" + ids.join(",") + "&ReturnTo=" + $('#ReturnToUrl').val();
    }


    assignCredentialsLink = function (nodeId) {


        var ids = [];

        //alert("Node:" + nodeId);

        // nodeid is not null if 'assign credentials' link has been clicked, else the toolbar button was clicked.
        if (nodeId == null) {
            ids = getSelectedIds();
        }
        else {
            ids.push(nodeId);
        }

        //alert("ids: " + ids.join(","));

        var nodeIds = ids.join("@{R=UDT.Strings;K=UDTWEBJS_VB1_51;E=js}");




        //selItems.each(function (rec) {
        eval('SW.UDT.AssignCredentials.Show("' + nodeIds + '");');

        return;
        //});
    };


    showDCValidationPopup = function (nodeId, nodeName, assignedCredential) {
        SW.UDT.ValidateDomainController.Show(nodeId, '', nodeName, assignedCredential, dcSetting.IsAuditAccountLogonEnabled, dcSetting.IsUserHasEventLogReadAccess, true, -1);

        //});
    };



    // delete node
    var deleteProgressDialog;
    var progressTimer;
    var progress;
    var progressUpdateDelay = 500;

    doNodeDelete = function (udtOnly) {

        $("#deleteProgress").empty();
        progress = new Ext.ProgressBar({
            text: '0%',
            width: 294
        });
        progress.render(document.getElementById("deleteProgress"));

        if (!deleteProgressDialog) {
            deleteProgressDialog = new Ext.Window({
                applyTo: 'deleteProgressDialog',
                width: 310,
                height: 70,
                closable: false,
                resizable: false,
                modal: true,
                items: new Ext.BoxComponent({
                    applyTo: 'deleteProgressBody',
                    layout: 'fit',
                    border: false
                })
            });
        }

        deleteProgressDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_59;E=js}");

        // Set the location
        deleteProgressDialog.alignTo(document.body, "c-c");
        deleteProgressDialog.doLayout();
        deleteProgressDialog.show();

        //set check progress timer
        progressTimer = setTimeout("DeleteDomainControllers(" + udtOnly + ")", progressUpdateDelay);

    }

    DeleteDomainControllers = function (udtOnly) {

        var ids = getSelectedIds();
        var totalItems = ids.length;
        var currentItem = 0;
        var webMethodName = "";

        if (udtOnly) {
            webMethodName = "DeleteDomainControllersFromUdtOnly";
        }
        else {
            webMethodName = "DeleteDomainControllersFromAllModules";
        }

        $.each(ids, function (index, value) {
            //            alert("item: " + value);
            callWebServiceSync("/Orion/UDT/Services/ManageDomainControllers.asmx", webMethodName, { nodeId: value }, function (result) {

                currentItem++;

                if (!result) { //error
                    clearTimeout(progressTimer);
                    deleteProgressDialog.hide();
                    Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_60;E=js}', "@{R=UDT.Strings;K=UDTWEBJS_VB1_18;E=js}");
                }
                else {
                    if (currentItem == totalItems) {
                        deleteProgressDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_61;E=js}");
                        progress.updateProgress(1, '100%');
                        deleteProgressDialog.doLayout();
                        //alert("complete");
                        clearTimeout(progressTimer);
                        $('#deleteProgressDialog').fadeOut(1000, function () { deleteProgressDialog.hide(); });
                        // load the grid with data
                        //                        grid.getStore().proxy.conn.jsonData = {};
                        //                        grid.getStore().load();
                        SW.UDT.ManageDomainControllers.RefreshGridData();

                    } else {
                        //progress.updateProgress(result / 100, result + "%");
                        deleteProgressDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_62;E=js}");
                        var results = (currentItem / totalItems) * 100.0;
                        progress.updateProgress(results / 100, results + '%');
                        deleteProgressDialog.doLayout();
                        //alert("update dialog");
                        clearTimeout(progressTimer);
                        progressTimer = setTimeout("DeleteDomainControllers()", progressUpdateDelay);
                    }
                }
            });
        });
    }

    var callWebServiceSync = function (serviceName, methodName, param, onSuccess, onError) {
        var paramString = Ext.encode(param);


        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: serviceName + "/" + methodName,
            data: paramString,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                //alert("msg.d " + msg.d);
                onSuccess(msg.d);
            },
            error: function (xhr, status, errorThrown) {
                ORION.handleError(xhr, onError);
            }
        });
    }



    deleteLink = function () {
        if (!$(this).parent().hasClass("Disabled")) {

            var ids = getSelectedIds();

            //set the appropriate message
            var layout = $("#delDialogDescription");
            var txt;

            if (isAllSelected) {
                // do for all
                layout.html('@{R=UDT.Strings;K=UDTWEBJS_VB1_63;E=js}');
            }
            else {
                var count = ids;
                if (count.length > 1) {
                    layout.html(String.format('@{R=UDT.Strings;K=UDTWEBJS_VB1_64;E=js}', count.length));
                }
                else {
                    layout.html('@{R=UDT.Strings;K=UDTWEBJS_VB1_65;E=js}');
                }
            }
            //create the dialog window if it doesn't already exist
            if (!delDialog) {
                delDialog = new Ext.Window({
                    applyTo: 'delDialog',
                    layout: 'fit',
                    width: 400,
                    height: 205,
                    // SJW: SR1.0.1 change closeAction to hide. Destroy causes Show and Group by controls to remain missing if the 'x' (close) button is clicked for the pop-up - EXT bug?
                    //closeAction: 'destroy',
                    closeAction: 'hide',
                    modal: true,
                    plain: true,
                    resizable: false,
                    ids: ids,
                    items: new Ext.BoxComponent({
                        applyTo: 'delDialogBody',
                        layout: 'fit',
                        border: false
                    }),


                    buttons: [
                        {
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js} ',
                            handler: function () {

                                var delOptions = document.getElementsByName('delOptions');
                                if (delOptions[0].checked) {
                                    doNodeDelete(true);
                                }
                                else {
                                    doNodeDelete(false);
                                }
                                delDialog.hide();
                            }
                        },
                    {
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            confirmDelete = false;
                            delDialog.hide();
                        }
                    }]
                });
            }
            // Set the location
            delDialog.alignTo(document.body, "c-c");
            delDialog.show();
        }

        return false;
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;


    return {

        RefreshGridData: function () {
            grid.getStore().proxy.conn.jsonData = {};
            grid.getStore().load();

        },

        init: function () {

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        id: 'pagingStore',
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_34;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.onClick("refresh");
                    }, this);
                }
            });

            //selectorModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: true, header: '', id: 'removeDefaultCheckbox', width: 30 });
            selectorModel = new Ext.grid.CheckboxSelectionModel({ width: 25 });
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            //Override Ext.grid.CheckboxSelectionModel behavior to set isAllSelected variable for Delete dialog popup message.
            Ext.grid.CheckboxSelectionModel.override({
                // override mouse down on header event
                onHdMouseDown: function (e, t) {
                    if (t.className == "x-grid3-hd-checker") {
                        e.stopEvent();
                        var hd = Ext.fly(t.parentNode);
                        var isChecked = hd.hasClass("x-grid3-hd-checker-on");

                        if (this.fireEvent("beforecheckallclick", this, isChecked) === false) {
                            return;
                        }

                        if (isChecked) {
                            isAllSelected = false;
                            hd.removeClass("x-grid3-hd-checker-on");
                            this.clearSelections();
                        } else {
                            isAllSelected = true;
                            hd.addClass("x-grid3-hd-checker-on");
                            this.selectAll();
                        }

                        this.fireEvent("aftercheckallclick", this, !isChecked);
                    }
                }
            });


            //SJW: FOR FUTURE REFERENCE: If you add any additional columns in the search results in between existing columns, 
            //     you need to increment the 'mapping' value of ALL columns which are higher than the one inserted (or data will be out of whack!).
            dataStore = new ORION.WebServiceStore(
            "/Orion/UDT/Services/ManageDomainControllers.asmx/GetDomainControllers",
                [
                { name: 'DCName', mapping: 0 },
                { name: 'DCStatusLED', mapping: 1 },
                { name: 'DCStatusDescription', mapping: 2 },
                { name: 'DCNodeID', mapping: 3 },
                { name: 'DCCredentials', mapping: 4 },
                { name: 'DCPollingStatus', mapping: 5 }
                ],
                "DCName", "DESC");

            grid = new Ext.grid.GridPanel({

                store: dataStore,
                // ensure we prevent columns from being located to other positions
                enableColumnMove: false,

                columns: [
                    selectorModel,
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_66;E=js}', width: 350, sortable: true, dataIndex: 'DCName', renderer: renderStatusIconWithNodeCaption },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_67;E=js}', width: 250, sortable: true, dataIndex: 'DCCredentials', renderer: renderCredentials },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_68;E=js}', width: 780, sortable: true, dataIndex: 'DCPollingStatus', renderer: renderTestCredentials }
                ],



                sm: selectorModel,

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}'
                },

                stripeRows: true,

                // width: 770,
                height: 350,

                tbar: [
                    {
                        id: 'AddButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_69;E=js}',
                        iconCls: 'add',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            addLink();
                        }
                    }, '-',
                    {
                        id: 'EditButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_30;E=js}',
                        iconCls: 'edit',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            editLink();
                        }
                    }, '-',
                    {
                        id: 'AssignCredentialsButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_70;E=js}',
                        iconCls: 'assign',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            assignCredentialsLink(null);
                        }
                    }, '-',
                    {
                        id: 'DeleteButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js}',
                        iconCls: 'del',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            deleteLink();
                        }
                    }
                ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar({
                    id: 'manageDomainControllerPager',
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_VB1_33;E=js}',
                    emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');

            grid.getSelectionModel().on("beforecheckallclick", beforeCheckAllClickHandler);
            grid.getSelectionModel().on("aftercheckallclick", afterCheckAllClickHandler);

            UpdateToolbarButtons();

            grid.bottomToolbar.addPageSizer();

            // resize the grid
            gridResize();

            // initial load of grid data
            grid.store.proxy.conn.jsonData = {};

            // load the grid with data
            grid.store.load();

        }
    };

} ();


Ext.onReady(SW.UDT.ManageDomainControllers.init, SW.UDT.ManageDomainControllers);
