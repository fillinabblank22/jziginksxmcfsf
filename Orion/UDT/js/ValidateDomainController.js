﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');
SW.UDT.ValidateDomainController = function () {
    var dialog;
    var lblDcName;
    var lblConnStatus;
    var lblAuditAlert;
    var lblAuditMsg;
    var lblEventLogAlert;
    var lblEventLogMsg;
    var lblDCCredential;
    //var grid;
    var lblDialogClose;
    var lblTestProgress;

    var dialogHtml = '<style type="text/css">\
            #ValidateDCDialog .dialogBody td { padding: 1px; padding-bottom: 1px; vertical-align: middle; white-space: nowrap;}\
            #ValidateDCDialog a.helpLink { color: #336699; font-size: smaller; text-decoration: none; }\
            #ValidateDCDialog .leftColumnStyle {width:100px; }\
            #ValidateDCDialog .rightColumnStyle {width:536px; }\
            #ValidateDCDialog .dialogBody table {background-color: #EAEAEA;}\
            #ValidateDCDialog .dialogBody { background-color: #EAEAEA; }\
            #VDCconnectionError .x-grid3-scroller {background-color: #EAEAEA ;}\
            #ValidateDCDialog .x-grid3-row, #ValidateDCDialog .x-panel-body{border:none;}\
            #ValidateDCDialog .x-grid3-row-over{ background:none;border:none;}\
            #ValidateDCDialog .x-panel-body .x-panel-body-noheader {background-color: #EAEAEA !important; background-image:none;border:0px; border-top-style:none; border-bottom-style:none; border-right-style:none; border-left-style:none;}\
            #ValidateDCDialog .x-window-bc {background-color: #EAEAEA; background-image:none;border:none;}\
            #ValidateDCDialog .x-window-br {background-color: #EAEAEA; background-image:none;border:none;}\
            #ValidateDCDialog .x-window-bl {background-color: #EAEAEA; background-image:none;border:none;}\
            #ValidateDCDialog .x-window-mr {background-color: #EAEAEA; background-image:none;border:none;}\
            #ValidateDCDialog .x-window-ml {background-color: #EAEAEA; background-image:none;border:none;}\
            #ValidateDCDialog .x-window-mc {background-color: #EAEAEA; background-image:none;border:0px; border-top-style:none; border-bottom-style:none; border-right-style:none; border-left-style:none;unselectable="on";}\
            #ValidateDCDialog .x-grid3-row.x-grid3-row-selected{background-color:transparent !important; border:none;}\
            #ValidateDCDialog #vdcDialogCloseSpan .x-btn-noicon .x-btn-small .x-btn-text { width: 12px; height: 12px; padding: 0;}\
            .smallText { color: black; font-size: 11px; }\
            .x-grid3-cell-inner x-grid3-col-0 {background-color: #EAEAEA;}\
            .x-window-mr {background-color: #EAEAEA; background-image:none;border:none;}\
            .x-window-ml {background-color: #EAEAEA !important; background-image:none;border:none;}\
            .x-window-mc {background-color: #EAEAEA; background-image:none;border:none;}\
            .x-window-bc {background-color: #EAEAEA; background-image:none;border:none;}\
            .x-window-br {background-color: #EAEAEA; background-image:none;border:none;}\
            .x-window-bl {background-color: #EAEAEA; background-image:none;border:none;}\
            .x-window-bwrap {background-color: #EAEAEA; background-image:none;border:none;}\
        </style>';
    var htmlPart2 = '<tr >\
            <td colspan="2">\
            <table width="636px" >\
            <tr>\
               <td  style="white-space: nowrap;"><b><span  class="Label">@{R=UDT.Strings;K=UDTWEBDATA_GK1_16;E=js}</span></b></td>\
               <td id="vdcDialogCloseSpan" style="padding-right:20px;" align="right"></td>\
           </tr>\
           </table>\
           </td>\
         </tr>\
         <tr >\
             <td colspan="2">\
                <div style="height=8px;"></div>\
             </td>\
         </tr>\
        <tr >\
           <td class="leftColumnStyle" style="white-space: nowrap;"><span  class="Label">@{R=UDT.Strings;K=UDTWEBDATA_GK1_1;E=js}</span></td>\
           <td class="rightColumnStyle" style="background-color: #EAEAEA;">&nbsp;&nbsp;<span id="VDCnodeName" class="Label"></span></td>\
         </tr>\
         <tr>\
             <td colspan="2">\
                <div style="height=8px;"></div>\
             </td>\
         </tr>\
         <tr>\
           <td class="leftColumnStyle" style="white-space: nowrap; "><span class="Label">@{R=UDT.Strings;K=UDTWEBDATA_GK1_2;E=js}</span></td>\
           <td class="rightColumnStyle" style="background-color: #EAEAEA;">&nbsp;&nbsp;<span id="VDCassignedCredential" class="Label"></span></td>\
         </tr>\
         <tr>\
             <td colspan="2">\
                <div style="height=8px;"></div>\
             </td>\
         </tr>\
         <tr>\
           <td class="leftColumnStyle" style="text-align:left;vertical-align:top;white-space: nowrap;">\
             <span class="Label">@{R=UDT.Strings;K=UDTWEBDATA_GK1_3;E=js}</span>\
           </td>\
            <td class="rightColumnStyle" style="background-color: #EAEAEA;vertical-align: top;" ><span id="VDCconnectionError"  class="Label" style="background-color: #EAEAEA !important;" >\
                <table style ="background-color: #EAEAEA; width:600px;">\
                    <tr>\
                        <td>\
                            <span class="Label" id="PollingStatus"></span>\
                        </td>\
                    </tr>\
                     <tr >\
                        <td >\
                            <span class="Label" id="empty"><div style="height: 8px;"></div></span>\
                        </td>\
                    </tr>\
                    <tr>\
                        <td>\
                            <span class="Label" id="AuditAccountMsg"></span>\
                        </td>\
                    </tr>\
                    <tr>\
                        <td>\
                            <span class="Label" id="AuditAccountAlert"></span>\
                        </td>\
                    </tr>\
                    <tr>\
                        <td>\
                           <span class="Label" id="EventLogMsg"></span>\
                        </td>\
                    </tr>\
                    <tr>\
                        <td>\
                         <span class="Label" id="EventLogAlert"></span>\
                        </td>\
                    </tr>\
                     <tr >\
                        <td style="height:25px">\
                            <span class="Label" id="TestProgress"></span>\
                        </td>\
                    </tr>\
                </table>\
             </td>\
         </tr>\
        </table>';
    function loadConnectionStatus(value) {
        return value;
    }

    function auditAccountLogonMessage(status) {

        var msg = (status) ? '<img src="/Orion/UDT/Images/Admin/Discovery/OK_good.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" style="vertical-align:bottom;" />&nbsp;' : '<img src="/Orion/UDT/Images/Admin/Discovery/error_16x16.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" style="vertical-align:bottom;" />&nbsp;';
        msg += ' <b>@{R=UDT.Strings;K=UDTWEBDATA_GK1_4;E=js}</b>';
        return msg;
    }

    function eventLogAccessMessage(status) {

        var msg = (status) ? '<img src="/Orion/UDT/Images/Admin/Discovery/OK_good.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" style="vertical-align:bottom;" />&nbsp;' : '<img src="/Orion/UDT/Images/Admin/Discovery/error_16x16.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" style="vertical-align:bottom; background-color: #EAEAEA;" />&nbsp;';
        msg += ' <b>@{R=UDT.Strings;K=UDTWEBDATA_GK1_5;E=js}</b>';
        return msg;
    }

    function auditAccountLogonAlert(status) {
        var msg = (!status) ? '@{R=UDT.Strings;K=UDTWEBDATA_GK1_6;E=js}' + '<a class=\"udtCredentialsLink\" style="text-underline-position: below; text-decoration: underline" href="@{R=UDT.Strings;K=UDTWEBDATA_GK1_22;E=js}" target=\"_blank\" >@{R=UDT.Strings;K=UDTWEBDATA_GK1_8;E=js}</a> <br/> <br/> @{R=UDT.Strings;K=UDTWEBDATA_GK1_9;E=js} <br/> @{R=UDT.Strings;K=UDTWEBDATA_GK1_10;E=js}</p>' : '<div unselectable="on" class="x-grid3-cell-inner x-grid3-col-0"><div style="padding-left: 25px;"> <font class="smallText"></font> </div></div>';
        msg = '<div style="padding-left: 25px; background-color: #EAEAEA;;"> <font class="smallText">' + msg + '</font> </div>';
        return msg;

    }

    function eventLogAccessAlert(status) {
        var msg = (!status) ? '<p> @{R=UDT.Strings;K=UDTWEBDATA_GK1_11;E=js}' + '<a class=\"udtCredentialsLink\" style="text-underline-position: below; text-decoration: underline" href="@{R=UDT.Strings;K=UDTWEBDATA_GK1_23;E=js}" target=\"_blank\"> @{R=UDT.Strings;K=UDTWEBDATA_GK1_12;E=js}</a> <br/> <br/> @{R=UDT.Strings;K=UDTWEBDATA_GK1_13;E=js}<br/> @{R=UDT.Strings;K=UDTWEBDATA_GK1_14;E=js} </br>@{R=UDT.Strings;K=UDTWEBDATA_GK1_15;E=js}</p>' : '<div unselectable="on" class="x-grid3-cell-inner x-grid3-col-0"><div style="padding-left: 25px;"> <font class="smallText"></font> </div></div>';
        msg = '<div style="padding-left: 25px; background-color: #EAEAEA;"> <font class="smallText">' + msg + '</font> </div>';
        return msg;

    }
    return {


        Show: function (nodeID, ipAddress, nodeName, assignedCredential, auditAccountLogonStatus, hasEventLogAccess, showDialog, credentialid) {

            var tableHtml =(!auditAccountLogonStatus && !hasEventLogAccess)? '<table id="tblDialog" width="636px" height="325px;" class="dialogBody" style="padding: 5px; background-color: #EAEAEA; " cellspacing="0">' : '<table id="tblDialog" width="636px" height="290px;" class="dialogBody" style="padding: 5px; background-color: #EAEAEA; " cellspacing="0">';
            dialogHtml = dialogHtml+tableHtml+htmlPart2;

            if ($("#isDemoMode").length > 0) {
                demoAction('UDT_DNS_CredentialTest', null);
                return;
            }
            if (!lblDcName) {
                lblDcName = new Ext.form.Label({
                    text: nodeName
                    //displayValue: 'CredentialID'
                });
            }
            else {
                lblDcName.setText(nodeName);
            }

            if (!lblDCCredential) {
                lblDCCredential = new Ext.form.Label({
                    text: assignedCredential

                });
            }
            else {
                lblDCCredential.setText(assignedCredential);
            }


            //lblDialogClose
            if (!lblDialogClose) {
                lblDialogClose = new Ext.Button({
                    text: 'x',

                    handler: function () {
                        dialog.hide();
                    }
                });
            }
            else {
                lblDialogClose.setText('x');
            }
            //            if (!grid) {
            var pollingStatusMessage = '<img src="/Orion/UDT/Images/Admin/Discovery/OK_good.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" style="vertical-align:bottom; background-color: #EAEAEA;" />&nbsp;' +
                     "<b>@{R=UDT.Strings;K=UDTWEBDATA_GK1_17;E=js} </b>";
            var blankMsg = '<div unselectable="on" class="x-grid3-cell-inner x-grid3-col-0" style="background-color: #EAEAEA;"><div style="padding-left: 25px; background-color: #EAEAEA;"> <font class="smallText"></font> </div></div>';
            var auditaccountlogonMessage = auditAccountLogonMessage(auditAccountLogonStatus);
            var auditaccountlogonAlert = auditAccountLogonAlert(auditAccountLogonStatus);
            var eventlogreadAccessMessage = eventLogAccessMessage(hasEventLogAccess);
            var eventLogAlert = eventLogAccessAlert(hasEventLogAccess);


            var progressMessage = '<img src="/Orion/images/loading_gen_small.gif" style="vertical-align:bottom;" />&nbsp;';
            progressMessage += '<font class="smallText">Testing the domain controller configurations...</font>';


            //lblConnStatus
            if (!lblConnStatus) {
                lblConnStatus = new Ext.form.Label({
                    html: pollingStatusMessage
                });
            }
            else {
                lblConnStatus.setText(pollingStatusMessage, false);
            }

            if (!lblAuditAlert) {
                lblAuditAlert = new Ext.form.Label({
                    html: auditaccountlogonAlert
                });
            }
            else {
                lblAuditAlert.setText(auditaccountlogonAlert, false);
            }

            if (!lblAuditMsg) {
                lblAuditMsg = new Ext.form.Label({
                    html: auditaccountlogonMessage
                });
            }
            else {
                lblAuditMsg.setText(auditaccountlogonMessage, false);
            }

            if (!lblEventLogAlert) {
                lblEventLogAlert = new Ext.form.Label({
                    html: eventLogAlert
                });
            }
            else {
                lblEventLogAlert.setText(eventLogAlert, false);
            }

            if (!lblEventLogMsg) {
                lblEventLogMsg = new Ext.form.Label({
                    html: eventlogreadAccessMessage
                });
            }
            else {
                lblEventLogMsg.setText(eventlogreadAccessMessage, false);
            }


            var setHeight = 350;
            if (!auditAccountLogonStatus && !hasEventLogAccess) {
                setHeight = (nodeID == -1) ? 356 : 375;
            }
            else if (auditAccountLogonStatus && hasEventLogAccess) {
                setHeight = (nodeID == -1) ? 241 : 260;
            }
            else {
                setHeight = (nodeID == -1) ? 301 : 320;
            }

            if (!dialog) {
                dialog = new Ext.Window({
                    id: 'ValidateDCDialog',
                    layout: 'fit',
                    width: 650,
                    height: setHeight,
                    modal: true,
                    closeAction: 'hide',
                    buttonAlign: 'center',
                    // plain: true,
                    resizable: false,
                    closable: false,
                    movable: true,

                    title: '',


                    items: [
                                {
                                    html: dialogHtml
                                }

                            ],
                    buttons: [
                                {
                                    id: 'btnValidateDC',
                                    text: 'Test',
                                    hidden: (nodeID == -1 && credentialid == -1),
                                    handler: function () {


                                        var btn = Ext.getCmp('btnValidateDC');
                                        btn.disabled = true;
                                        //var progressbar = new Ext.data.Record({ connectionStatus: progressMessage });

                                        //lblTestProgress
                                        if (!lblTestProgress) {
                                            lblTestProgress = new Ext.form.Label({
                                                html: progressMessage

                                            });
                                            lblTestProgress.render('TestProgress');
                                        }
                                        else {
                                            lblTestProgress.setText(progressMessage, false);
                                            lblTestProgress.setVisible(true);
                                        }

                                        ORION.callWebService("/Orion/UDT/Services/ManageDomainControllers.asmx", "TestSecurityCredentials", { nodeId: nodeID, ipAddress: ipAddress, credentialsId: credentialid }, function (result) {

                                            btn.disabled = false;

                                            var dcSetting = JSON.parse(result);
                                            auditaccountlogonMessage = auditAccountLogonMessage(dcSetting.IsAuditAccountLogonEnabled);
                                            auditaccountlogonAlert = auditAccountLogonAlert(dcSetting.IsAuditAccountLogonEnabled);
                                            eventlogreadAccessMessage = eventLogAccessMessage(dcSetting.IsUserHasEventLogReadAccess);
                                            eventLogAlert = eventLogAccessAlert(dcSetting.IsUserHasEventLogReadAccess);
                                            lblTestProgress.setVisible(false);
                                            btn.disabled = false;
                                        });

                                    }

                                },
                                {
                                    text: 'Close',
                                    style: 'margin-left: 5px; ',
                                    handler: function () {
                                        dialog.hide();
                                    }
                                }]



                });

            }
            else {
                var btns = Ext.getCmp('btnValidateDC');
                btns.handler = function () {


                    var btn = Ext.getCmp('btnValidateDC');
                    btn.disabled = true;
                    //var progressbar = new Ext.data.Record({ connectionStatus: progressMessage });

                    //lblTestProgress
                    if (!lblTestProgress) {
                        lblTestProgress = new Ext.form.Label({
                            html: progressMessage

                        });
                        lblTestProgress.render('TestProgress');
                    }
                    else {
                        lblTestProgress.setText(progressMessage, false);
                        lblTestProgress.setVisible(true);
                    }

                    ORION.callWebService("/Orion/UDT/Services/ManageDomainControllers.asmx", "TestSecurityCredentials", { nodeId: nodeID, ipAddress: ipAddress, credentialsId: credentialid }, function (result) {

                        btn.disabled = false;

                        var dcSetting = JSON.parse(result);
                        auditaccountlogonMessage = auditAccountLogonMessage(dcSetting.IsAuditAccountLogonEnabled);
                        auditaccountlogonAlert = auditAccountLogonAlert(dcSetting.IsAuditAccountLogonEnabled);
                        eventlogreadAccessMessage = eventLogAccessMessage(dcSetting.IsUserHasEventLogReadAccess);
                        eventLogAlert = eventLogAccessAlert(dcSetting.IsUserHasEventLogReadAccess);
                        lblTestProgress.setVisible(false);
                        btn.disabled = false;
                    });

                };
            }

            if (showDialog) {
                dialog.show();
            }
            else {
                dialog.render('validateDCDialog');
            }
            lblDcName.render('VDCnodeName');
            lblDCCredential.render('VDCassignedCredential');
            lblDialogClose.render('vdcDialogCloseSpan');
            lblConnStatus.render('PollingStatus');
            lblAuditAlert.render('AuditAccountAlert');
            lblAuditMsg.render('AuditAccountMsg');
            lblEventLogAlert.render('EventLogAlert');
            lblEventLogMsg.render('EventLogMsg');

        }
    };
} ();


assignDCValidationLink = function (nodeId, ipAddress, nodeName, assignedCredential, isAuditAccountLogonEnabled, isUserHasEventLogReadAccess, credentialid) {
    SW.UDT.ValidateDomainController.Show(nodeId, ipAddress, nodeName, assignedCredential, isAuditAccountLogonEnabled, isUserHasEventLogReadAccess, true, credentialid);

};
