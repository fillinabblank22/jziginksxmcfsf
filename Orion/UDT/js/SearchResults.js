﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

var searchForStringRaw;
var searchInColumnList;
var searchForMacString;

// function to reformat search text for SQL query execution
function formatSearchStringForSQL(searchText, searchInColumns) {

    var arrSearchText = [];

    if (searchText != "") {

        //replace all single quote ' instances to a double single quote instance.  e.g. test's becomes test''s
        searchText = searchText.replace(/\'/g, "''");

        //append escape character if % literal found e.g. test% becomes test[%]
        searchText = searchText.replace(/\%/g, "[%]");

        //append escape character if % literal found e.g. test% becomes test[%]
        searchText = searchText.replace(/\_/g, "[_]");

        //ToDo: replace wildcard * with SQL % in order to enforce explicit usage of wildcard searching
        searchText = searchText.replace(/\*/g, "%");
        //searchText = searchText.replace(/\?/g, "_"); // single character replace?

        // push searchText (non MAC address formatted) onto the searchString array
        arrSearchText.push(searchText);

        //REFORMAT SEARCH STRING FOR MAC ADDRESS SEARCH: 
        //if searchtext looks like part of mac address then strip MAC address formatting before search
        //as our database values are free from any :, - or . formatting.

        if ((searchText.indexOf(":") != -1) || (searchText.indexOf(".") != -1) || (searchText.indexOf("-") != -1)) {

            //first, strip out any % wildcard chars
            //var tmpSearchText = searchText.replace(/\*/g, "");
            var tmpSearchText = searchText.replace(/\%/g, "");

            var ciscoDot = /^([0-9a-f]{0,4})(([.-]|$)([0-9a-f]{0,4}$|(([0-9a-f]{4}[.-]$)|(([0-9a-f]{4}[.-][0-9a-f]{0,4}$)))))/i; //this works OK!
            var unix = /^([0-9a-f]{0,2})(([:-]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[:-]$)|(([0-9a-f]{2}[:-][0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[:-]){4}[0-9a-f]{0,2}$))))))))/i;
            var windows = /^([0-9a-f]{0,2})(([--]|$)([0-9a-f]{0,2}$|(([0-9a-f]{2}[--]$)|(([0-9a-f]{2}[--][0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){2}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){3}[0-9a-f]{0,2}$)|((([0-9a-f]{2}[--]){4}[0-9a-f]{0,2}$))))))))/i;

            if (ciscoDot.test(tmpSearchText)) searchText = searchText.replace(/\./g, "");
            if (unix.test(tmpSearchText)) searchText = searchText.replace(/\:/g, "");
            if (windows.test(tmpSearchText)) searchText = searchText.replace(/\-/g, "");

            // push recoded MAC searchText onto the array
            arrSearchText.push(searchText);
        }

        else {
            // we aren't searching on MAC address column, so push an empty string
            arrSearchText.push(searchText);
        }
    }

    else {
        // no searchText string, so push Searchtext onto array twice (2nd is for the MAC search string)
        arrSearchText.push(searchText);
        arrSearchText.push(searchText);
    }
    return arrSearchText;

}

SW.UDT.SearchResultsEx = function () {

    ORION.prefix = "UDT_SearchResultsEx_";

    // update the Ext grid size when window size is changed
    $(window).resize(gridResize);
    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        map.AddToWatchList.setDisabled(count != 1);

        //        //only enable AddToWatchList button if a single record is selected (we do not support adding of multiple search result items - for v1 that is)
        //        if (count == 1 && selItems.getSelected().get("Accounts.AccountType") == "2") {
        //            // is Windows Account, so disable the Change Password button
        //            map.AddToWatchList.setDisabled(true);
        //        }
        //        else {
        //            // not a Windows account or more than one record selected, so set button accordingly
        //            map.AddToWatchList.setDisabled(count != 1);
        //        }

        // The buttons are disabled in demo mode
        //        if ($("#isOrionDemoMode").length != 0) {
        //            map.AddToWatchList.setDisabled(true);
        //        }

    };


    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }


    // *** GRID COLUMN DATA RENDERING FUNCTIONS ***
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderTextColumn(value) {
        if (value == null) {
            return value;
        }
        else {
            //alert('value: ' + value);
            return '<div style="white-space:normal !important;">' + value + '</div>';
        }
    };

    // rendering VLAN
    function renderVlanColumn(value) {
        if (value == null || value == 0) {
            value = "";
        }
        return value;
    };

    function renderIconWithMacAddress(value, meta, record, rowIndex, colIndex, store) {
        // reformat MAC address to standard display format
        var formatted = formatMacAddressForDisplay(value)
        var img = '';

        switch (record.data[13]) {
            case 1:
                img = '<img src="/Orion/UDT/Images/icon_direct.png" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_72;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_72;E=js}" style="vertical-align:bottom;" />&nbsp;';
                break;
            case 2:
                img = '<img src="/Orion/UDT/Images/icon_indirect.png" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_73;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_73;E=js}" style="vertical-align:bottom;" />&nbsp;';
                break;
            default:
                img = '<img src="/Orion/UDT/Images/icon_direct_unknown.png" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_74;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_74;E=js}" style="vertical-align:bottom;" />&nbsp;';
        }

        formatted = img + renderHighlighting(formatted, meta, record, rowIndex, colIndex, store);

        return formatted;
    }

    function formatMacAddressForDisplay(value) {
        // renders the MAC address in a standard display format
        var regex = /(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})/;
        var formatted = value.replace(regex, "$1:$2:$3:$4:$5:$6");
        return formatted;
    }


    function renderIpAddress(value, meta, record, rowIndex, colIndex, store) {

        return renderHighlighting(value, meta, record, rowIndex, colIndex, store);
    }

    function renderStatusIconWithPortNumber(value, meta, record, rowIndex, colIndex, store) {
        // renders port status icon next to port number
        var img;

        if (value == null || record.data['ViaLayer3'] == 1) {
            //            if (value == null) {
            formatted = '';
            //            }
            //            else {
            //                formatted = '<font class="smallText">(Router)</font>';
            //            }
        }

        else // add port number text and port status icon
        {

            if (record.data[15] == "2") // Port Flag is 'Missing'
            {
                img = '<img src="/Orion/UDT/Images/Status/icon_port_missing.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_75;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_75;E=js}" style="vertical-align:bottom;" />&nbsp;';
            }

            else if (record.data[10] == "1") // Port Status is Up
            {
                img = '<img src="/Orion/UDT/Images/Status/icon_port_active_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_76;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_76;E=js}" style="vertical-align:bottom;" />&nbsp;';
            }
            else //anything other than up, (can actually have any status between 2 - 7)
            {
                img = '<img src="/Orion/UDT/Images/Status/icon_port_inactive_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_77;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_77;E=js}" style="vertical-align:bottom;" />&nbsp;';
            }

            var urlLink = '<a href="/Orion/UDT/PortDetails.aspx?NetObject=UP:' + record.data[12].toString() + '">' + renderHighlighting(value, meta, record, rowIndex, colIndex, store) + '</a>';
            var formatted = img + urlLink;
        }

        return formatted;
    }

    function renderStatusIconWithNodeCaption(value, meta, record, rowIndex, colIndex, store) {
        // renders node status icon next to node caption
        var img;

        if (value == null) {
            formatted = '';
        }
        else // add node caption text and node status icon
        {
            if (record.data[9] == "1") // Node Status is Up
            {
                img = '<img src="/Orion/images/StatusIcons/Small-up.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_78;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_78;E=js}" style="vertical-align:bottom;" />&nbsp;';
            }
            else {
                if (record.data[9] == "2") // Node Status is Down
                {
                    img = '<img src="/Orion/images/StatusIcons/Small-down.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_79;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_79;E=js}" style="vertical-align:bottom;" />&nbsp;';
                }
                else //anything other than up - Unknown
                {
                    img = '<img src="/Orion/images/StatusIcons/Small-warning.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_80;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_80;E=js}" style="vertical-align:bottom;" />&nbsp;';
                }
            }

            var urlLink = '<a href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + record.data[11].toString() + '">' + renderHighlighting(value, meta, record, rowIndex, colIndex, store) + '</a>';
            var formatted = img + urlLink;
        }

        return formatted;

    }

    function renderLastSeen(value, meta, record) {
        // renders (IP) or (MAC) text appended to the LastSeen date, based on whether LastSeen is Endpoint (Layer2) or IP (Layer3) related

        var formatted = value;
        if (record.data[8] != null) {
            if (record.data['ViaLayer3'] == 1) {
                formatted = value + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_81;E=js}</font>'
            }
            else {
                formatted = value + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_82;E=js}</font>'
            }
        }
        else {
            if (record.data['ViaLayer3'] == 1) {
                formatted = record.data['RouterLastSuccessfulScanDisplay'] + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_81;E=js}</font>'
            }
            else {
                formatted = record.data['SwitchLastSuccessfulScanDisplay'] + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_82;E=js}</font>'
            }
        }

        return formatted;
    }

    function renderFirstSeen(value, meta, record) {
        // renders (IP) or (MAC) text appended to the FirstSeen date, based on whether FirstSeen is Endpoint (Layer2) or IP (Layer3) related
        var formatted = value;

        if (record.data[14] != null) {
            if (record.data['ViaLayer3'] == 1) {
                formatted = value + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_81;E=js}</font>'
            }
            else {
                formatted = value + '&nbsp;<font class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_82;E=js}</font>'
            }
        }

        return formatted;
    }

    function renderIconWithHostName(value, meta, record, rowIndex, colIndex, store) {
        // renders the Active/Inactive icon within the same column as the Hostname
        // if no hostname, then renders just the icon 
        if (record.data[8] == null) {
            var img = '<img src="/Orion/UDT/Images/icon_user_active_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_35;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_35;E=js}" style="vertical-align:bottom;" />&nbsp;';
        }
        else {
            var img = '<img src="/Orion/UDT/Images/icon_user_inactive_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_36;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_36;E=js}" style="vertical-align:bottom;" />&nbsp;';
        }

        if (value == null) {
            formatted = '';
        }
        else {
            var formatted = renderHighlighting(value, meta, record, rowIndex, colIndex, store);
        }

        return img + formatted;
    }

    // render highlighting to search text matches
    function renderHighlighting(value, meta, record, rowIndex, colIndex, store) {

        if (value == null || value == '') { return value; }

        //check if the colIndex of the current column we are looking at is included in the 'checked' column list for the SearchBox control
        var arr = checkedItemsList.join();
        var idx = arr.indexOf(colIndex);
        if (idx == undefined || idx == -1) { return value; }

        var cellValue = value;
        var patternText;

        //check if we are looking at data for the MAC address column
        if (colIndex == '3') {
            // Steve Welsh: This is the best workaround for highlighting MAC addresses as we are stripping out any actual 
            //              formatting from the searchbox searchtext if we detect it is of 'MAC Address' format type.

            // remove all SQL wildcard chars from the reformatted searchstring
            var s = searchForString;

            // if nothing in the search string, just return
            if (s == '') { return encodeHTML(value); }
            s = s.replace(/\%/g, '').toLowerCase(); //convert to lowercase for case insensitive 'indexOf' check

            //check that search string still has some characters, else just return highlighted
            if (s == '') { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>'; }

            // to perform the match test, remove any ':' chars added by the MAC address renderer
            cellValue = cellValue.replace(/\:/g, '').toLowerCase(); //convert to lowercase for case insensitive 'indexOf' check

            var macIdx;
            var arrSplit = [];
            //convert searchForString to lowercase for matching.
            searchForStringRaw = searchForStringRaw.toLowerCase();
            //strip any formatting fro mthe search box text. 
            searchForStringRaw = searchForStringRaw.replace(/\:/g, '');
            searchForStringRaw = searchForStringRaw.replace(/\./g, '');
            searchForStringRaw = searchForStringRaw.replace(/\-/g, '');

            // split on wildcard char
            arrSplit = searchForStringRaw.split('*');

            // loop through searchstring split array to check if the search text occurs in the returned value
            for (i = 0; i < arrSplit.length; i++) {
                macIdx = cellValue.indexOf(arrSplit[i].toString(), 0);
                // if search text part not found, return the original (as per MAC Address renderer) value; else loop through next part of string
                if (macIdx == undefined || macIdx == -1) {
                    return value;
                }
            }
            // all parts of the seach string have been found, so return highlighted.
            return '<span style=\"background-color: #FFE89E\">' + encodeHTML(value) + '</span>';

        }
        else {
            patternText = searchForString;
        }


        // replace any converted SQL wildcard %'s with a regex wildcard *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return with highlighting without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(cellValue) + '</span>'; }

        //check for trailing wildcard * on end of search string
        //        var x = patternText.toString().substring(patternText.length - 1);
        //        if (x == "*") { patternText = patternText.toString().substring(0, (patternText.length - 1)); }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // replace * with .*
        //patternText = patternText.replace(/\*/g, '');
        //alert('patternText: ' + patternText);

        // set regex pattern
        var x = '((^' + patternText + '$)+)\*';
        //var x = '((' + patternText + ')+)\*';
        var content = cellValue,

        pattern = new RegExp(x, "gi"),
        //replaceWith = '<span style=\"background-color: #FFE89E\">$1</span>';

        //NOTE: we need to add literal SPAN 'markers' because otherwise the actual HTML span tags would get encoded as literal
        replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace
        cellValue = content.replace(pattern, replaceWith);
        //ensure highlighted content gets encoded
        cellValue = encodeHTML(cellValue);
        // now replace the literal SPAN markers with the HTML span tags
        cellValue = cellValue.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        cellValue = cellValue.replace(/{SPAN-END-MARKER}/g, '</span>');

        return cellValue;
    };
    //*** END OF GRID COLUMN DATA RENDERING FUNCTIONS ***


    //*** WATCHLIST FUNCTIONS ***

    function StripMacFormatting(value) {
        return value.replace(/[\.\:-]/g, '');
    }

    function DecompressIPv6(ip) {

    }

    // Add selected item to watchlist function.  
    // Pops a dialog to select which item from the selected record (MAC, IP or Hostname) you want to add to watchlist.
    // Calls on webservice to perform add to watchlist.
    addToWatchList = function () {

        var mac;
        var ipaddr;
        var host;

        // get data for selected record
        grid.getSelectionModel().each(function (rec) {
            mac = formatMacAddressForDisplay(rec.data[3]);
            ipaddr = rec.data[1];
            host = rec.data[2];
        });

        //set dialog field values based on selected record
        $("#MACAddress").val(mac);

        if (ipaddr == undefined || ipaddr == null) {
            $("#IPAddress").val('@{R=UDT.Strings;K=UDTWEBJS_VB1_108;E=js}');
            $("#radIpAddress").attr('disabled', true);
        }
        else {
            $("#IPAddress").val(ipaddr);
            $("#radIpAddress").attr('disabled', false);
        }

        if (host == undefined || host == null) {
            $("#Hostname").val('@{R=UDT.Strings;K=UDTWEBJS_VB1_108;E=js}');
            $("#Hostname").attr('title', '');
            $("#radHostname").attr('disabled', true);
        }
        else {
            $("#Hostname").val(host);
            $("#Hostname").attr('title', host);
            $("#radHostname").attr('disabled', false);
        }

        // leave optional field values blank
        $("#Name").val("");
        $("#Description").val("");

        if (!addWatchDialog) {
            addWatchDialog = new Ext.Window({
                applyTo: 'addWatchDialog',
                layout: 'fit',
                width: 405,
                height: 410, //Google Chrome seems to render dialog shorter then other browsers, so increase the dialog hieght so all controls display 
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addWatchDialogBody',
                    //layout: 'fit',  //fails to fit properly in Chrome
                    border: false
                }),
                buttons:
                [
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                    handler: function (me) {
                        AddWatchServiceCall();
                    }
                },
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addWatchDialog.hide();
                    }
                }
                ]
            });
        } else {
            addWatchDialog.buttons[0].handler = AddWatchServiceCall;
        }

        addWatchDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_83;E=js}");

        // select MAC Address as default
        $("#radMacAddress").attr('checked', 'checked');
        addDialogUI();

        // Set the location
        addWatchDialog.alignTo(document.body, "c-c");

        if (($("#isNonAdmin").length != 0) && !($("#isOrionDemoMode").length != 0)) {
            alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_84;E=js}');
            return;
        }

        addWatchDialog.show();

        //return false;

    };

    function AddWatchServiceCall() {

        // restrict adding to watch list for demo server mode, unless user is an admin.
        if (($("#isOrionDemoMode").length != 0) && ($("#isNonAdmin").length != 0)) {
            alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_85;E=js}');
            addWatchDialog.hide();
        }
        else {

            var addOptions = document.getElementsByName('addOptions');
            var _watchItemType;
            var _watchItemValue;
            if (addOptions[0].checked) { _watchItemType = 1; _watchItemValue = $("#MACAddress").val(); }
            if (addOptions[1].checked) { _watchItemType = 2; _watchItemValue = $("#IPAddress").val(); }
            if (addOptions[2].checked) { _watchItemType = 3; _watchItemValue = $("#Hostname").val(); }
            var _watchName = $("#Name").val();
            var _watchNote = $("#Description").val();

            if (_watchItemType == 1) {
                _watchItemValue = StripMacFormatting(_watchItemValue);
            }
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "AddDeviceWatchList", { watchItemType: _watchItemType, watchItemValue: _watchItemValue, watchName: _watchName, watchNote: _watchNote }, function (result) {
                if (result != "") {
                    //some error
                    Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_86;E=js}', result);
                }
                grid.getStore().load();
                addWatchDialog.hide();
            });
        }
    }

    addDialogUI = function () {
        var addOptions = document.getElementsByName('addOptions');
        if (addOptions[0].checked) {
            $("#MACAddress").attr('disabled', false);
            $("#IPAddress").attr('disabled', true);
            $("#Hostname").attr('disabled', true);
        } else {
            if (addOptions[1].checked) {
                $("#MACAddress").attr('disabled', true);
                $("#IPAddress").attr('disabled', false);
                $("#Hostname").attr('disabled', true);
            } else {
                $("#MACAddress").attr('disabled', true);
                $("#IPAddress").attr('disabled', true);
                $("#Hostname").attr('disabled', false);
            }
        }
    };
    //*** END OF WATCHLIST FUNCTIONS ***

    // Export To CSV 
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    function guid() {
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    var csvExport;
    var progressTimer;
    var downloadGuid;
    var progress;
    var progressUpdateDelay = 500;

    exportAsFile = function () {

        $("#exportProgress").empty();
        progress = new Ext.ProgressBar({
            text: '0%',
            width: 294
        });
        progress.render(document.getElementById("exportProgress"));

        if (!csvExport) {
            csvExport = new Ext.Window({
                applyTo: 'csvExport',
                width: 310,
                height: 55,
                closable: false,
                resizable: false,
                modal: true,
                items: new Ext.BoxComponent({
                    applyTo: 'csvExportBody',
                    layout: 'fit',
                    border: false
                })
            });
        }

        csvExport.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_16;E=js}");

        // Set the location
        csvExport.alignTo(document.body, "c-c");
        csvExport.show();

        //Get new download id
        downloadGuid = guid();

        // start export to CSV call
        ORION.callWebService("/Orion/UDT/Services/SearchResults.asmx", "ExportToCSVEx", { downloadId: downloadGuid, activeOnly: true, searchForString: searchForString, searchForMacString: searchForMacString, searchInColumnList: ORION.Prefs.load('SearchItemListEx', ''), connectionType: getConnectionType(), layer: getLayer() }, function (result) {
            //set check progress timer
            progressTimer = setTimeout("ExportProgress()", progressUpdateDelay);
        });

    };

    launchDownload = function (downloadGuid) {
        $("#csvExport").append("<iframe src='/Orion/UDT/ExportToCSV.ashx?DownloadId=" + downloadGuid + "&DownloadName=DeviceTrackerSearchResults' style='display: none;' ></iframe>");
        csvExport.hide();
    };

    ExportProgress = function () {

        ORION.callWebService("/Orion/UDT/Services/SearchResults.asmx", "GetExportProgressEx", { downloadId: downloadGuid }, function (result) {
            if (result == "-1") { //error
                clearTimeout(progressTimer);
                csvExport.hide();
                Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_17;E=js}', "@{R=UDT.Strings;K=UDTWEBJS_VB1_18;E=js}");
            } else {
                if (result == "100") {
                    csvExport.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_19;E=js}");
                    progress.updateProgress(1, '100%');
                    csvExport.doLayout();
                    clearTimeout(progressTimer);
                    //Wait a second, then launchDownload - no particular reason, just looks better
                    setTimeout("launchDownload('" + downloadGuid + "')", 1000);
                } else {
                    progress.updateProgress(result / 100, result + "%");
                    progressTimer = setTimeout("ExportProgress()", progressUpdateDelay);
                }
            }
        });
    };




    var selectorModel;
    var dataStore;
    var grid;
    var addWatchDialog;
    var pageSizeNum;


    var connectionTypeFilter;

    connTypeIconCheckHandler = function (item, itemId) {
        if (itemId == connectionTypeFilter)
            return;

        ORION.Prefs.save('SearchConnectionTypeOptionEx', itemId);

        if (connectionTypeFilter != null) {
            // if the item came with a iconclass, remove it.
            var chk = Ext.getCmp(connectionTypeFilter);
            chk.setChecked(false);
            chk.setIconClass(chk.initialConfig.iconCls || '');
        }

        connectionTypeFilter = itemId;
        item.setIconClass('');
        item.setChecked(true);

        var ff = Ext.getCmp('ConnectionTypeFilter');
        if (ff) {
            ff.setIconClass(item.initialConfig.iconCls || '');
            ff.setText(item.text);
        }

        SW.UDT.SearchResultsEx.ApplyFilterToGrid();
    };

    var layerTypeFilter;

    layerTypeIconCheckHandler = function (item, itemId) {
        if (itemId == layerTypeFilter)
            return;

        ORION.Prefs.save('SearchLayerTypeOptionEx', itemId);

        if (layerTypeFilter != null) {
            // if the item came with a iconclass, remove it.
            var chk = Ext.getCmp(layerTypeFilter);
            chk.setChecked(false);
            chk.setIconClass(chk.initialConfig.iconCls || '');
        }

        layerTypeFilter = itemId;
        item.setIconClass('');
        item.setChecked(true);

        var ff = Ext.getCmp('LayerTypeFilter');
        if (ff) {
            ff.setIconClass(item.initialConfig.iconCls || '');
            ff.setText(item.text);
        }

        SW.UDT.SearchResultsEx.ApplyFilterToGrid();
    };

    setCheckState = function () {
        connectionTypeFilter = ORION.Prefs.load('SearchConnectionTypeOptionEx', 'DirectConnections');
        var chk = Ext.getCmp(connectionTypeFilter);
        chk.setChecked(true);
        chk.setIconClass('');

        layerTypeFilter = ORION.Prefs.load('SearchLayerTypeOptionEx', 'AllLayers');
        chk = Ext.getCmp(layerTypeFilter);
        chk.setChecked(true);
        chk.setIconClass('');
    };

    getConnectionType = function () {
        if (Ext.getCmp('AllConnectionTypes').checked) return 0;
        if (Ext.getCmp('DirectConnections').checked) return 1;
        if (Ext.getCmp('IndirectConnections').checked) return 2;
        if (Ext.getCmp('UnknownConnections').checked) return 3;
        //Default
        return getDefaultConnectionTypeInt();
    };

    getDefaultConnectionTypeInt = function () {
        var ret = 1;
        switch (ORION.Prefs.load('SearchConnectionTypeOptionEx', 'DirectConnections')) {
            case 'AllConnectionTypes': ret = 0; break;
            case 'DirectConnections': ret = 1; break;
            case 'IndirectConnections': ret = 2; break;
            case 'UnknownConnections': ret = 3; break;
            default: ret = 1;
        }
        return ret;
    };

    getDefaultConnectionTypeText = function () {
        var ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_87;E=js}';
        switch (ORION.Prefs.load('SearchConnectionTypeOptionEx', 'DirectConnections')) {
            case 'AllConnectionTypes': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_87;E=js}'; break;
            case 'DirectConnections': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_88;E=js}'; break;
            case 'IndirectConnections': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_89;E=js}'; break;
            case 'UnknownConnections': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_90;E=js}'; break;
            default: ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_87;E=js}';
        }
        return ret;
    };

    getDefaultConnectionTypeIconCss = function () {
        var ret = 'allConn';
        switch (ORION.Prefs.load('SearchConnectionTypeOptionEx', 'DirectConnections')) {
            case 'AllConnectionTypes': ret = 'allConn'; break;
            case 'DirectConnections': ret = 'direct'; break;
            case 'IndirectConnections': ret = 'indirect'; break;
            case 'UnknownConnections': ret = 'unknown'; break;
            default: ret = 'allConn';
        }
        return ret;
    };

    getLayer = function () {
        if (Ext.getCmp('AllLayers').checked) return 0;
        if (Ext.getCmp('Layer2').checked) return 1;
        if (Ext.getCmp('Layer3').checked) return 2;
        //Default
        return getDefaultLayerTypeInt();
    };

    getDefaultLayerTypeInt = function () {
        var ret = 0;
        switch (ORION.Prefs.load('SearchLayerTypeOptionEx', 'AllLayers')) {
            case 'AllLayers': ret = 0; break;
            case 'Layer2': ret = 1; break;
            case 'Layer3': ret = 2; break;
            default: ret = 0;
        }
        return ret;
    };

    getDefaultLayerTypeText = function () {
        var ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_91;E=js}';
        switch (ORION.Prefs.load('SearchLayerTypeOptionEx', 'AllLayers')) {
            case 'AllLayers': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_91;E=js}'; break;
            case 'Layer2': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_92;E=js}'; break;
            case 'Layer3': ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_93;E=js}'; break;
            default: ret = '@{R=UDT.Strings;K=UDTWEBJS_VB1_91;E=js}';
        }
        return ret;
    };

    getDefaultLayerTypeIconCss = function () {
        var ret = 'allLayers';
        switch (ORION.Prefs.load('SearchLayerTypeOptionEx', 'AllLayers')) {
            case 'AllLayers': ret = 'allLayers'; break;
            case 'Layer2': ret = 'layer2'; break;
            case 'Layer3': ret = 'layer3'; break;
            default: ret = 'allLayers';
        }
        return ret;
    };

    validateTextField = function () {
        Ext.form.VTypes.nameVal = /^([A-Z]{1})[A-Za-z\-]+ ([A-Z]{1})[A-Za-z\-]+/;
    };
    //    Ext.onReady(function () {
    //        Ext.form.VTypes.nameVal = /^([A-Z]{1})[A-Za-z\-]+ ([A-Z]{1})[A-Za-z\-]+/;
    //        Ext.form.VTypes.nameMask = /[A-Za-z\- ]/;
    //        Ext.form.VTypes.nameText = 'In-valid.';
    //        Ext.form.VTypes.name = function (v) {
    //            return Ext.form.VTypes.nameVal.test(v);
    //        };

    //        new Ext.FormPanel({
    //            url: 'your.php',
    //            renderTo: Ext.getBody(),
    //            frame: true,
    //            title: 'Title',
    //            width: 250,
    //            items: [
    //          {
    //              xtype: 'textfield',
    //              fieldLabel: 'Title',
    //              name: 'title',
    //              vtype: 'name'
    //          }
    //        ]
    //        });


    //    });
    //    



    return {

        ApplyFilterToGrid: function () {
            grid.getStore().proxy.conn.jsonData = { activeOnly: true, searchForString: searchForString, searchForMacString: searchForMacString, searchInColumnList: ORION.Prefs.load('SearchItemListEx', ''), connectionType: getConnectionType(), layer: getLayer() };
            grid.getStore().load();
        },

        init: function () {

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '50'));
            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    var minValue = 1;
                    var maxPageSize = 250;
                    var pageSizeBoxBottom = new Ext.form.NumberField({
                        id: 'pageSizeFieldBottom',
                        width: 40,
                        allowBlank: false,
                        minValue: minValue,
                        maxValue: maxPageSize,
                        value: pageSizeNum,
                        enableKeyEvents: true
                    });
                    pageSizeBoxBottom.on('keydown', function (field, e) {
                        if (e.getCharCode() == 13 || e.getCharCode() == 9) {
                            var pSize = field.getValue();
                            if (isNaN(pSize)) {
                                pSize = 50;
                                field.setValue(50);

                            }
                            else if (pSize > maxPageSize) {
                                pSize = 250;
                                field.setValue(250);
                            }
                            else if (pSize < 10) {
                                pSize = 10;
                                field.setValue(10);
                            }
                            this.pageSize = pSize;
                            this.cursor = 0;
                            ORION.Prefs.save('PageSize', this.pageSize);
                            this.doRefresh();
                        }
                        else {

                            //Allowing only 8-BackSpace,37-Left,39-Right,46-Delete
                            if (e.getCharCode() != 8 && e.getCharCode() != 0 && e.getCharCode() != 37 && e.getCharCode() != 39 && e.getCharCode() != 46 && (e.getCharCode() < 48 || e.getCharCode() > 57) && (e.getCharCode() < 96 || e.getCharCode() > 105)) {
                                e.preventDefault();
                            }
                        }
                    }, this);

                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_34;E=js}', style: 'margin-left: 5px; margin-right: 5px;' }));
                    this.addField(pageSizeBoxBottom);

                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: true, header: '', id: 'removeDefaultCheckbox', width: 30 });
            //selectorModel.on("selectionchange", DoSomeFunction);  // leaving this here for now incase we need to do something later when checkbox is changed
            selectorModel.on("selectionchange", UpdateToolbarButtons);
            //SJW: FOR FUTURE REFERENCE: If you add any additional columns in the search results in between existing columns, 
            //     you need to increment the 'mapping' value of ALL columns which are higher than the one inserted (or data will be out of whack!).
            dataStore = new ORION.WebServiceStore(
                "/Orion/UDT/Services/SearchResults.asmx/GetSearchResults",
                [
                    { name: '8', mapping: 7 },
                    { name: '1', mapping: 0 },
                    { name: '2', mapping: 1 },
                    { name: '3', mapping: 2 },
                    { name: '4', mapping: 3 }, // caption
                    {name: '5', mapping: 4 }, // port name
                    {name: '6', mapping: 5 }, // port description
                    {name: '7', mapping: 6 },
                    { name: '9', mapping: 8 },
                    { name: '10', mapping: 9 },
                    { name: '11', mapping: 10 },
                    { name: '12', mapping: 11 },
                    { name: '13', mapping: 12 },
                    { name: '14', mapping: 13 },
                    { name: '15', mapping: 14 }, //SJW: Added 'Flag' for SR1.0.1.
                    {name: 'LastSeenDisplay', mapping: 22 },
                    { name: 'FirstSeenDisplay', mapping: 23 },
                    { name: 'ViaLayer3', mapping: 15 },
                    { name: 'SwitchLastSuccessfulScanDisplay', mapping: 20 },
                    { name: 'RouterLastSuccessfulScanDisplay', mapping: 21 },
                    { name: 'VendorIcon', mapping: 16 },
                    { name: 'Vendor', mapping: 17 },
                    { name: 'VendorIconDetails', mapping: 24 }
                ],
                "8", "DESC");

            grid = new Ext.grid.GridPanel({

                store: dataStore,
                // ensure we prevent columns from being located to other positions (else the MAC searching and highlighting breaks)
                enableColumnMove: false,

                columns: [
                        selectorModel,
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_24;E=js}', width: 200, sortable: true, dataIndex: '1', renderer: renderIpAddress },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_25;E=js}', width: 150, sortable: true, dataIndex: '2', renderer: renderIconWithHostName },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_23;E=js}', width: 150, sortable: true, dataIndex: '3', renderer: renderIconWithMacAddress },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_GK_1;E=js}', width: 125, sortable: true, dataIndex: 'VendorIconDetails' },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_96;E=js}', width: 210, sortable: true, dataIndex: '4', renderer: renderStatusIconWithNodeCaption },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_97;E=js}', width: 190, sortable: true, dataIndex: '5', renderer: renderStatusIconWithPortNumber },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_98;E=js}', hidden: true, width: 198, sortable: true, dataIndex: '6', renderer: renderHighlighting },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_99;E=js}', width: 60, sortable: true, dataIndex: '7', renderer: renderVlanColumn },
                //      { header: 'S-LastSeen', width: 190, sortable: true, dataIndex: 'SwitchLastSuccessfulScanDisplay' },
                //      { header: 'R-LastSeen', width: 190, sortable: true, dataIndex: 'RouterLastSuccessfulScanDisplay' },
                //      { header: 'Raw Last Seen', width: 190, sortable: true, dataIndex: '8' },
                //      { header: 'Raw First Seen', width: 190, sortable: true, dataIndex: '14' },
                        {header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_27;E=js}', width: 190, sortable: true, dataIndex: 'LastSeenDisplay', renderer: renderLastSeen },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_100;E=js}', width: 190, sortable: true, dataIndex: 'FirstSeenDisplay', renderer: renderFirstSeen }
                //      { header: 'ViaLayer3', visible: false, hidden: false, width: 150, sortable: false, dataIndex: 'ViaLayer3' },
                //      { header: 'Flag', width: 70, sortable: false, dataIndex: '15' }

                    ],

                sm: selectorModel,

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}'
                },
                width: 700,
                height: 700,

                //autoWidth:true,

                // top toobar on grid with Add to watch list button
                tbar: [
                        {
                            id: 'AddToWatchList',
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_101;E=js}',
                            iconCls: 'watchList',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                addToWatchList();
                            }
                        },
                        '-',
                        {
                            id: 'ExportButton',
                            text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_32;E=js}',
                            iconCls: 'export',
                            handler: function () {
                                //if (IsDemoMode()) return DemoModeMessage();
                                exportAsFile();
                            }
                        }, '-', ' @{R=UDT.Strings;K=UDTWEBJS_VB1_102;E=js} ',
                        {
                            id: 'ConnectionTypeFilter',
                            text: getDefaultConnectionTypeText(),
                            iconCls: getDefaultConnectionTypeIconCss(),

                            menu: { items: [new Ext.menu.CheckItem({
                                id: 'AllConnectionTypes',
                                iconCls: 'allConn',
                                cls: 'iconText',
                                text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_87;E=js}',
                                group: 'ConnectionTypeFilter',
                                checked: false,
                                handler: function (that) {

                                    //if (IsDemoMode()) return DemoModeMessage();
                                    connTypeIconCheckHandler(that, that.id);
                                }
                            }),

                                new Ext.menu.CheckItem({
                                    id: 'DirectConnections',
                                    iconCls: 'direct',
                                    cls: 'iconText',
                                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_88;E=js}',
                                    group: 'ConnectionTypeFilter',
                                    checked: false,
                                    handler: function (that) {
                                        //if (IsDemoMode()) return DemoModeMessage();
                                        connTypeIconCheckHandler(that, that.id);
                                    }
                                }),

                                new Ext.menu.CheckItem({
                                    id: 'IndirectConnections',
                                    iconCls: 'indirect',
                                    cls: 'iconText',
                                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_89;E=js}',
                                    group: 'ConnectionTypeFilter',
                                    checked: false,
                                    handler: function (that) {
                                        //if (IsDemoMode()) return DemoModeMessage();
                                        connTypeIconCheckHandler(that, that.id);
                                    }
                                }),

                                new Ext.menu.CheckItem({
                                    id: 'UnknownConnections',
                                    iconCls: 'unknown',
                                    cls: 'iconText',
                                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_90;E=js}',
                                    group: 'ConnectionTypeFilter',
                                    checked: false,
                                    handler: function (that) {
                                        //if (IsDemoMode()) return DemoModeMessage();
                                        connTypeIconCheckHandler(that, that.id);
                                    }
                                })

                            ]
                            }
                        },
                        ' '
                ,
                        {
                            id: 'LayerTypeFilter',
                            text: getDefaultLayerTypeText(),
                            iconCls: getDefaultLayerTypeIconCss(),

                            menu: { items: [new Ext.menu.CheckItem({
                                id: 'AllLayers',
                                iconCls: 'allLayers',
                                cls: 'iconText',
                                text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_91;E=js}',
                                group: 'LayerTypeFilter',
                                checked: false,
                                handler: function (that) {

                                    //if (IsDemoMode()) return DemoModeMessage();
                                    layerTypeIconCheckHandler(that, that.id);
                                }
                            }),

                                new Ext.menu.CheckItem({
                                    id: 'Layer2',
                                    iconCls: 'layer2',
                                    cls: 'iconText',
                                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_92;E=js}',
                                    group: 'LayerTypeFilter',
                                    checked: false,
                                    handler: function (that) {
                                        //if (IsDemoMode()) return DemoModeMessage();
                                        layerTypeIconCheckHandler(that, that.id);
                                    }
                                }),

                                new Ext.menu.CheckItem({
                                    id: 'Layer3',
                                    iconCls: 'layer3',
                                    cls: 'iconText',
                                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_93;E=js}',
                                    group: 'LayerTypeFilter',
                                    checked: false,
                                    handler: function (that) {
                                        //if (IsDemoMode()) return DemoModeMessage();
                                        layerTypeIconCheckHandler(that, that.id);
                                    }
                                })

                            ]
                            }
                        }
                    ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar({
                    id: 'searchResultsPager',
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_VB1_33;E=js}',
                    emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.store.on('load', function () {
                // ToDo: possibly show a Loading... swizler icon in the grid while the data is being retrieved
            });

            grid.render('Grid');
            UpdateToolbarButtons();

            grid.bottomToolbar.addPageSizer();

            //            grid.bottomToolbar.on('afterlayout', function (tb) {
            //                tb.el.child('.x-toolbar-right').remove();
            //                var t = tb.el.child('.x-toolbar-left');
            //                t.removeClass('x-toolbar-left');
            //                t = tb.el.child('.x-toolbar-ct');
            //                t.setStyle('width', 'auto');
            //                t.wrap({ tag: 'center' });
            //            }, null, { single: true });



            // resize the grid
            gridResize();

            // retain original unformatted search text for matching MAC address highlighting
            searchForStringRaw = searchForString;
            // get the list of checked columns to search in
            searchInColumnList = ORION.Prefs.load('SearchItemListEx', '');

            // reformat search text for SQL query execution
            var searchForStringArray = formatSearchStringForSQL(searchForString, searchInColumnList);

            searchForString = searchForStringArray[0];
            searchForMacString = searchForStringArray[1];
            // initial load of grid data (radio option defaults to 'Only show active...' (i.e. true))
            grid.store.proxy.conn.jsonData = { searchForString: searchForString, searchForMacString: searchForMacString, searchInColumnList: searchInColumnList, connectionType: getConnectionType(), layer: getLayer(), activeOnly: true };

            // load the grid with data
            grid.store.load({ callback: function () {
                //set checked
                setCheckState();
            }
            });

            SW.UDT.SearchResultsEx.ApplyFilterToGrid();

        }
    };

} ();


Ext.EventManager.onDocumentReady(SW.UDT.SearchResultsEx.init, SW.UDT.SearchResultsEx, true);
