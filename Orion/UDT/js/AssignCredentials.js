﻿SW.UDT.AssignCredentials = function () {
    var dialog;
    var cbCredentials;
    var editedCredentialId;
    var combo;
    var nodeIds;
    var credentialId;

    var dialogHtml =
       '<style type="text/css">\
            .dialogBody td { padding: 1px; padding-bottom: 1px; vertical-align: middle; }\
            a.helpLink { color: #336699; font-size: smaller; text-decoration: none; }\
            .leftColumnStyle {width: 185px;}\
            .rightColumnStyle {width: 185px;}\
       </style>\
       <table class="dialogBody" style="padding: 5px;">\
         <tr>\
	          <td class="leftColumnStyle"><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_37;E=js}</span></td>\
              <td class="rightInputColumn">\
                <div id="credentialsList" />\
                <hidden ID="credID" maxlength="10"></input>\
              </td>\
         </tr>\
         <tr>\
             <td class="leftColumnStyle"><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_38;E=js}</span></td>\
             <td class="rightColumnStyle"><input ID="credDescription" maxlength="50" style="width:185px"></input></td>\
         </tr>\
         <tr>\
            <td class="leftColumnStyle"><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_39;E=js}</span></td>\
            <td class="rightColumnStyle"><input ID="credUsername" maxlength="50" style="width:185px"></input></td>\
         </tr>\
         <tr>\
             <td></td>\
             <td >\
                 <div class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_40;E=js}</div>\
                 <span class="LinkArrow">»</span>\
                 <a class="helpLink" target="_blank"\
                 href="https://www.solarwinds.com/documentation/helploader.aspx?topic=OrionUDTPHDomainControllerDiscovery.htm">\
                 @{R=UDT.Strings;K=UDTWEBJS_VB1_41;E=js} </a>\
             </td>\
         </tr>\
         <tr>\
             <td class="leftColumnStyle"><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_42;E=js}</span></td>\
             <td class="rightColumnStyle"><input ID="credPassword" maxlength="50" type="password" style="width:185px"></input></td>\
         </tr>\
         <tr>\
             <td class="leftColumnStyle"><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_43;E=js}</span></td>\
             <td class="rightColumnStyle"><input ID="credConfirm" maxlength="50" type="password" style="width:185px"></input></td>\
         </tr>\
         <tr>\
             <td colspan="2">&nbsp;</td>\
         </tr>\
         <tr>\
             <td colspan="2" style="color: red;"><span id="errorDiv"></span></td>\
         </tr>\
     </table>';

    // validate fields
    function checkCredentials(description, username, password, confirm) {
        if ($.trim(String(description)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_44;E=js}";
        }

        if ($.trim(String(username)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_45;E=js}";
        }

        if (!(password === confirm) || $.trim(String(password)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_46;E=js}";
        }

        return "";
    };

    function setDCCredentialsForNodes() {

        var countIds = nodeIds.split('@{R=UDT.Strings;K=UDTWEBJS_VB1_51;E=js}').length;

        if (!confirm(String.format("@{R=UDT.Strings;K=UDTWEBJS_VB1_47;E=js}", countIds))) {
            dialog.hide();
            return;
        }

        //alert("set creds nodeIds : " + nodeIds);

        // update security credentials for selected nodes
        ORION.callWebService("/Orion/UDT/Services/ManageDomainControllers.asmx", "SetDCCredentials",
            {
                nodeIds: nodeIds,
                credentialId: credentialId
            },

            function (result) {
                if (result > 0) {
                    {
                        SW.UDT.ManageDomainControllers.RefreshGridData();
                        dialog.hide();
                        return;
                    }
                }
                dialog.hide();
                return;
            }
        )
    };

    return {


        Show: function (ids) {

            //alert("ids string: " + ids);

            nodeIds = ids;

            var credStore = new ORION.WebServiceStore(
                    "/Orion/UDT/Services/ManageDomainControllers.asmx/GetDCCredentials",
                    [
                        { name: 'CredentialID', mapping: 0 },
                        { name: 'CredentialName', mapping: 1 },
                        { name: 'Username', mapping: 2 },
                        { name: 'Password', mapping: 3 }
                    ]);

            if (!cbCredentials) {
                cbCredentials = new Ext.form.ComboBox({
                    store: credStore,
                    //hiddenName: 'CredentialID',
                    displayField: 'CredentialName',
                    typeAhead: true,
                    mode: 'local',
                    triggerAction: 'all',
                    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_VB1_48;E=js}',
                    selectOnFocus: true,
                    width: 190,
                    editable: false//,
                    //displayValue: 'CredentialID'
                });

                cbCredentials.on("select", function (c, record) {

                    $("#credID").val(record.get("CredentialID"));

                    if (record.get("CredentialID") > 0) {
                        $("#credDescription").attr('disabled', 'disabled').val(record.get("CredentialName"));
                        $("#credUsername").attr('disabled', 'disabled').val(record.get("Username"));
                        $("#credPassword").attr('disabled', 'disabled').val(record.get("Password"));
                        $("#credConfirm").attr('disabled', 'disabled').val(record.get("Password"));
                    }
                    else {
                        $("#credDescription").attr('disabled', '').val("");
                        $("#credUsername").attr('disabled', '').val("");
                        $("#credPassword").attr('disabled', '').val("");
                        $("#credConfirm").attr('disabled', '').val("");
                    }
                }, this);

            }

            if (!dialog) {
                dialog = new Ext.Window({
                    id: 'dialog',
                    layout: 'fit',
                    width: 420,
                    height: 295,
                    modal: true,
                    closeAction: 'hide',
                    plain: true,
                    resizable: false,
                    title: '@{R=UDT.Strings;K=UDTWEBJS_VB1_49;E=js}',


                    items: [
                       {
                           html: dialogHtml
                       }

                    ],
                    buttons: [
                    {
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                        handler: function () {

                            credentialId = $("#credID").val();
                            var description = $("#credDescription").val();
                            var username = $("#credUsername").val();
                            var password = $("#credPassword").val();
                            var confirmPassword = $("#credConfirm").val();

                            var error = checkCredentials(description, username, password, confirmPassword, true);

                            if (error != '') {
                                $("#errorDiv").text(error);
                                return;
                            }
                            else {
                                $("#errorDiv").text('');
                            }

                            //alert("cred: " + credentialId);
                            if ((credentialId == 0) || (credentialId == null)) {

                                //alert("adding new credential");

                                // if we have specified a new credential, then add to db first
                                ORION.callWebService("/Orion/UDT/Services/ManageDomainControllers.asmx", "InsertDCCredentialAndReturnId",
                                    {
                                        name: description,
                                        username: username,
                                        password: password
                                    },
                                    function (result) {

                                        // credential name exists in db    
                                        if (result == 0) {
                                            $("#errorDiv").text('@{R=UDT.Strings;K=UDTWEBJS_VB1_50;E=js}');
                                            dialog.hide();
                                            return;
                                        }
                                        else {
                                            //alert("cred id result: " + result);
                                            //set the credential id variable to the returned value
                                            credentialId = result;
                                            setDCCredentialsForNodes();
                                            return;
                                        }
                                    }
                                );
                            }
                            else {
                                // existing credential selected, so set it for selected nodes
                                //alert("set existing credential for nodes");
                                setDCCredentialsForNodes();
                            }

                            //dialog.hide();
                        }
                    },
                    {
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            dialog.hide();
                        }
                    }]
                });
            }

            $("#errorDiv").text('');


            // initial load of combo data
            cbCredentials.store.proxy.conn.jsonData = {};
            // load the combo with data
            cbCredentials.store.load();

            dialog.show();
            cbCredentials.render('credentialsList');
        }
    };
} ();
