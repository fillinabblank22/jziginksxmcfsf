using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.UDT.Web.Views;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_EndpointDetails : UdtViewPage
{
    //private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    public override string ViewType
    {
        get
        {
            return "UDT EndpointDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.PageTitle = getTitle();
        pageTitleBar.ViewID = this.ViewInfo.ViewID;
        //todo add help link
        pageTitleBar.HelpFragment = "OrionUDTPHEndpointDetailsSummary";
    }

    protected override void OnInit(EventArgs e)
    {
        string tmpEndpoint = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
        if (tmpEndpoint != null)
        {
            string[] netObjectParts = tmpEndpoint.Split('=');
            string endpointTitle = netObjectParts[1];

            if (!String.IsNullOrEmpty(endpointTitle) && HtmlHelper.FindBadCharacters(endpointTitle))
            {
                Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}",
                                              HttpUtility.UrlEncode(Request.QueryString["NetObject"])));
                return;
            }
        }

        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
    
   
    protected string getTitle()
    {
        string viewTitle = this.ViewInfo.ViewTitle;
        
        try
        {
            string tmpEndpoint = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);
            
            string[] netObjectParts = tmpEndpoint.Split('=');
            string endpointTitle = netObjectParts[1];
            
            if (endpointTitle.Length > 0 )
            {
                viewTitle += " - " + endpointTitle;
            }
        }
        catch (Exception)
        {
        
        }

        return viewTitle;
    }
    

}
