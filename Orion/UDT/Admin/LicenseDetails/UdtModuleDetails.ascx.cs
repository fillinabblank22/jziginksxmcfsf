﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Web.DAL;

public partial class Orion_UDT_Admin_LicenseDetails_UdtModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("UDT");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error while displaying details for Orion User Device Tracker module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            OrionUdtDetails.Name = module.ProductDisplayName;

            values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_80, module.ProductName);
            values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_81, module.Version);
            values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_82, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.UDTWebContent.UDTWEBDATA_TM0_17 : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_83, module.LicenseInfo);

            // Add nodes/volumes limits/currnt
            try
            {
                using (WebDAL dal = new WebDAL())
                {

                    var featureManager = new FeatureManager();
                    int maxPorts = featureManager.GetMaxElementCount(SolarWinds.UDT.Common.WellKnownElementTypes.UDT.Ports);
                    int portCount = UDTPortDAL.GetPortCount();

                    values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_85, maxPorts > 1000000 ? Resources.UDTWebContent.UDTWEBCODE_VB1_84 : maxPorts.ToString());
                    values.Add(Resources.UDTWebContent.UDTWEBCODE_VB1_86, portCount.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Cannot get number of licensed elements.", ex);
            }

            OrionUdtDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }
}