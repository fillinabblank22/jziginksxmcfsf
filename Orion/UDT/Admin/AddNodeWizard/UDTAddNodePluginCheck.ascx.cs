﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Common;
using Node = SolarWinds.Orion.Core.Common.Models.Node;

public partial class Orion_UDT_Admin_AddNodeWizard_UDTAddNodePluginCheck : System.Web.UI.UserControl, IAddCoreNodePlugin, IAddNodePluginWizard
{

    private Control monitorPortsPlugin = null;
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        try
        {
            //using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            //{
            //    /* just contact business layer */
            //}

            Controls.Clear();
            monitorPortsPlugin = LoadControl("UDTMonitorPortsPlugin.ascx");
            Controls.Add(monitorPortsPlugin);
        }
        catch (Exception ex)
        {
            log.Error("Could not load Monitor Ports Plugin page in Add Node Wizard", ex);
        }
    }

    public void Cancel()
    {
        if (monitorPortsPlugin != null)
        {
            ((IAddCoreNodePlugin)monitorPortsPlugin).Cancel();
        }
        else
        {
            NodeWorkflowHelper.Reset();
            Response.Redirect("../Default.aspx", true);
        }
    }

    public void Next()
    {
        if (monitorPortsPlugin != null)
            ((IAddCoreNodePlugin)monitorPortsPlugin).Next();
    }

    public void Previous()
    {
        if (monitorPortsPlugin != null)
            ((IAddCoreNodePlugin)monitorPortsPlugin).Previous();
    }

    public void Save(Node node)
    {
        if (monitorPortsPlugin != null)
            ((IAddCoreNodePlugin)monitorPortsPlugin).Save(node);
    }

    public string Title
    {
        get { return Resources.UDTWebContent.UDTWEBDATA_AK1_112; }
    }

    public bool ValidateInput()
    {
        if (monitorPortsPlugin != null)
            return ((IAddCoreNodePlugin)monitorPortsPlugin).ValidateInput();

        return false;
    }

}