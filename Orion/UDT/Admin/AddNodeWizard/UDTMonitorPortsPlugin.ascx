﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UDTMonitorPortsPlugin.ascx.cs" Inherits="Orion_UDT_Admin_AddNodeWizard_UDTMonitorPortsPlugin" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<style type="text/css">
    .UDT_ZebraStripe
    {
	    background-color: #e4f1f8;
    }
    
    .Header
    {
        background-color: #EAEAEA !important;
        padding: 3px;
    }
</style>

<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false"/>
<orion:Include runat="server" File="OrionCore.js"/>
<script type="text/javascript">

    Ext.namespace('SW.UDT.AddNode');

    var timeoutHandle;
    var waitingImageId = '<%=WaitingImage.ClientID %>';
    var monitoredPortsId = '<%=MonitoredPorts.ClientID %>';
    var nextButtonId = '<%=this.Parent.Parent.FindControl("imgbNext").ClientID %>';
    var scanForPortsId = '<%=ScanForPorts.ClientID  %>';

    SW.UDT.AddNode.discoveryId = '';
    SW.UDT.AddNode.monitoredPorts = [];
    SW.UDT.AddNode.monitoredPortsNames = [];
    SW.UDT.AddNode.monitoredPortsMACs = [];
    SW.UDT.AddNode.allPortsNames = [];
    SW.UDT.AddNode.allPortsMACs = [];

    //callWebService -- With onError callback Core!
    var callWebService = function (serviceName, methodName, param, onSuccess, onError) {

        var paramString = Ext.encode(param);

        if (paramString.length === 0) {
            paramString = "{}";
        }

        $.ajax({
            type: "POST",
            url: serviceName + "/" + methodName,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                onSuccess(msg.d);
            },
            error: function (xhr, status, errorThrown) {
                var errorText = xhr.responseText;
                try {
                    var error = eval("(" + errorText + ")");
                    var ret = error.Message;
                    //$("#stackTrace").text(error.StackTrace);
                }
                catch (err) {
                    var ret = err.description;
                }
                onError(ret);
            }
        });
    }

    var StartPortDiscovery = function () {
        
        callWebService("/Orion/UDT/Services/Discovery.asmx", "StartPortDiscovery", { },

            function (result) {
                SW.UDT.AddNode.discoveryId = result;
                GetPortDiscoveryProgress();
            },

            function (error) {
                //check for license exception
                if (!LicenseCheck(error)) {
                    window.location = baseUrl;
                } else {
                    alert(String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_22)%>', error));
                }
            }

        );
    }

    function CancelDiscovery() {

        callWebService("/Orion/UDT/Services/Discovery.asmx", "CancelPortDiscovery", { discoveryId: SW.UDT.AddNode.discoveryId },

        function (result) {
            alert('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_23)%>');
            $('#' + waitingImageId).hide();
        },

        function (error) {
            //check for license exception
            if (!LicenseCheck(error)) {
                window.location = baseUrl;
            } else {
                var message = error;
                finalMessage = String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_24)%>", message);
                alert(finalMessage);

            }
        }

        );
    }

    var GetPortDiscoveryProgress = function () {
        callWebService("/Orion/UDT/Services/Discovery.asmx", "GetPortDiscoveryProgress", { discoveryId: SW.UDT.AddNode.discoveryId },

        function (result) {
            OnProgressReceived(result);
        },

        function (error) {
            //check for license exception
            if (!LicenseCheck(error)) {
                window.location = baseUrl;
            } else {
                var message = error;
                finalMessage = String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_24)%>", message);
                alert(finalMessage);
            }
        }

    );
    }

    var OnProgressReceived = function (result) {

        if (result.Status.Status == 1) { // in progress
            //schedule next progress checking 
            timeoutHandle = setTimeout(GetPortDiscoveryProgress, 1000);
        }
        else if (result.Status.Status != 1) { // finished
            clearTimeout(timeoutHandle);
            if (result.Status.Status == 6) {
                alert(result.Status.Description);
            }

            //            if (result.DiscoveredPorts == 0) {

            //                callWebService("/Orion/UDT/Services/Discovery.asmx", "CancelPortDiscovery", { discoveryId: SW.UDT.AddNode.discoveryId },

            //                function (result) {
            //                    alert("No ports were discovered.");
            //                    $('#' + waitingImageId).hide();
            //                },

            //                function (error) {
            //                    //check for license exception
            //                    if (!LicenseCheck(error)) {
            //                        window.location = baseUrl;
            //                    }
            //                }

            //                );

            //                return;
            //            }



            callWebService("/Orion/UDT/Services/Discovery.asmx", "ProcessPortDiscoveryResults", { discoveryId: SW.UDT.AddNode.discoveryId },

            function (result) {
                $('#' + waitingImageId).hide();

                GetPortDiscoveryErrors();

                if (result.UDTNodes[0] != undefined) {

                    resultHtml = "<table width='100%' cellpadding='2' cellspacing='0' class='NeedsZebraStripes'>";
                    resultHtml = resultHtml + "<tr class='Header'><td><input type='checkbox' Checked id='checkAllPorts' CHECKED onclick='checkAll(this)'/></td><td>&nbsp;</td><td><%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_25)%></td><td><%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_26)%></td><td><%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_27)%></td></tr>";
                    //result.UDTNodes[0].Ports.length + ' ports found.';
                    htmlRowTemplate = "<tr{0} id='{3}'><td id='{6}'><input class='portCheck' type='checkbox' onclick='checkStateUpdate(this)' id='{1}'/></td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td hidden='true'>{6}</td></tr>";

                    SW.UDT.AddNode.monitoredPortsNames = [];
                    SW.UDT.AddNode.monitoredPortsMACs = [];

                    for (var p = 0; p < result.UDTNodes[0].Ports.length; p++) {
                        var port = result.UDTNodes[0].Ports[p];
                        if (port.IsExcluded)
                            continue;
                        resultHtml = resultHtml + htmlRowTemplate.format((p % 2 == 0 ? "" : ""), port.PortIndex, renderPortStatus(port.OperationalStatus, port.AdministrativeStatus), port.Name, getVLANs(port.Vlans), port.PortDescription, port.MACAddress);
                        SW.UDT.AddNode.allPortsNames.push(port.Name);
                        SW.UDT.AddNode.monitoredPortsNames.push(port.Name);
                        SW.UDT.AddNode.allPortsMACs.push(port.MACAddress);
                        SW.UDT.AddNode.monitoredPortsMACs.push(port.MACAddress);
                    }

                    resultHtml = resultHtml + "</table>";
                    //alert(resultHtml);
                    $("#Results").append(resultHtml);

                    checkAll("selectAll");
                }
            },

            function (error) {
                //check for license exception
                if (!LicenseCheck(error)) {
                    window.location = baseUrl;
                } else {
                    alert(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_30)%>", error));
                }
            }

        );

        }
        else {
            alert("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_28)%>");
        }

    }

    function checkStateUpdate(el) {
        var parent = $(el).parent()[0];
        var portMac = parent.id;
        var portName = $(parent).parent()[0].id;
        if (el.checked) {
            if (!SW.UDT.AddNode.monitoredPorts.contains(el.id)) {
                SW.UDT.AddNode.monitoredPorts.push(el.id);
                SW.UDT.AddNode.monitoredPortsNames.push(portName);
                SW.UDT.AddNode.monitoredPortsMACs.push(portMac);
            }
        } else {
            if (SW.UDT.AddNode.monitoredPorts.contains(el.id)) {
                var idx = SW.UDT.AddNode.monitoredPorts.indexOf(el.id);
                SW.UDT.AddNode.monitoredPorts.splice(idx, 1);
                SW.UDT.AddNode.monitoredPortsNames.splice(idx, 1);
                SW.UDT.AddNode.monitoredPortsMACs.splice(idx, 1);
            }
        }
        $("#" + monitoredPortsId).val(SW.UDT.AddNode.monitoredPorts);
        CheckLicenseStatus();
    }

    var LicenseCheck = function (error) {
        //check for license exception
        var rel = /SolarWindsLicensingException/;
        if (error.search(rel) > -1) {
            var message = "<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_29)%>";
            alert(message);
            return false;
        } else {
            return true;
        }
    }

    var GetPortDiscoveryErrors = function () {

        callWebService("/Orion/UDT/Services/Discovery.asmx", "GetPortDiscoveryErrors", { discoveryId: SW.UDT.AddNode.discoveryId },

            function (result) {

                //{"d":["Device does not respond to interface table request","Device does not respond to interface table request"]}
                if (result != null) {
                    var List = "<br/><ul style='list-style-type:disc;'>";
                    for (i = 0; i < result.length; i++) {
                        List = List + "<li>" + result[i] + "</li>";
                    }
                    List = List + "</ul>";
                    if (result.length >= 1) {
                        //error
                        $("#DiscoveryErrors").html(List);
                        $("#ErrorPanel").show();
                        $("#" + scanForPortsId).prop('checked', false);
                    }
                }

                
            },

            function (error) {
                //check for license exception
                if (!LicenseCheck(error)) {
                    window.location = baseUrl;
                } else {
                    alert(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_31)%>", error));
                    window.location = baseUrl;
                }
            });

    }

    var CheckLicenseStatus = function () {
        var monitoredPortsInfo = [];
        for (var i = 0; i < SW.UDT.AddNode.monitoredPortsNames.length; i++) {
            monitoredPortsInfo.push(SW.UDT.AddNode.monitoredPortsNames[i] +
                " " +
                SW.UDT.AddNode.monitoredPortsMACs[i]);
        }

        callWebService("/Orion/UDT/Services/Discovery.asmx", "CheckLicensedPorts", { portsInfo: monitoredPortsInfo },

            function (result) {
                var layout = $("#LicenseStatus");
                var panel = $("#LicenseStatusPanel");
                if (result != null) {
                    layout.html(result);
                    panel.show();
                    $("#" + nextButtonId).prop('disabled', true);
                } else {
                    panel.hide();
                    $("#" + nextButtonId).prop('disabled', false);
                }
            },
                function (error) {
                    //check for license exception
                    if (!LicenseCheck(error)) {
                        window.location = baseUrl;
                    } else {
                        alert(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_32)%>", error));
                        window.location = baseUrl;
                    }
                });
            }

    String.prototype.format = function () {
        var s = this,
        i = arguments.length;

        while (i--) {
            s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
        }
        return s;
    };

    Array.prototype.contains = function (element) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == element) {
                return true;
            }
        }
        return false;
    }

    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
          this[from] === elt)
                return from;
        }
        return -1;
    };

    function renderPortStatus(op_value, ad_value) {

        if (ad_value == '2'){
            var at = '<img src="/Orion/UDT/Images/Status/icon_port_shutdown_dot.gif" title="<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_238)%>" />';
        }
        else if (op_value == '1') {
            var at = '<img src="/Orion/UDT/Images/Status/icon_port_active_dot.gif" title="<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_209)%>" />';
        }
        else {
            var at = '<img src="/Orion/UDT/Images/Status/icon_port_inactive_dot.gif" title="<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEBDATA_VB1_210)%>" />';
        }
        
        return at;
    };

    function getVLANs(VLANs) {
        if (VLANs.length == 0) return "";
        var ret = "";
        for (var v = 0; v < VLANs.length; v++) {
            ret = ret + VLANs[v].ID + ",";
        }
        return ret.substring(0,ret.length - 1);
    }

    function checkAll(el) {       
        if (el.checked || el == "selectAll" ) {           
            SW.UDT.AddNode.monitoredPorts = [];

            SW.UDT.AddNode.monitoredPortsNames = [];
            SW.UDT.AddNode.monitoredPortsMACs = [];

            for (var i = 0; i < SW.UDT.AddNode.allPortsNames.length; i++) {
                SW.UDT.AddNode.monitoredPortsNames.push(SW.UDT.AddNode.allPortsNames[i]);
                SW.UDT.AddNode.monitoredPortsMACs.push(SW.UDT.AddNode.allPortsMACs[i]);
            }

            $(".portCheck").each(function () {
                this.checked = true;
                SW.UDT.AddNode.monitoredPorts.push(this.id);
            });
            $("#" + monitoredPortsId).val(SW.UDT.AddNode.monitoredPorts);
        } else {                
            SW.UDT.AddNode.monitoredPorts = [];

            SW.UDT.AddNode.monitoredPortsNames = [];
            SW.UDT.AddNode.monitoredPortsMACs = [];

            $(".portCheck").prop('checked', false);
            $("#" + monitoredPortsId).val('');
        }
        CheckLicenseStatus();

    }

    var toggleErrorDetailsState = false;
    function toggleErrorDetails(el) {
        if (!toggleErrorDetailsState) {
            el.textContent = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_14)%>';
            $('#DiscoveryErrors').show();
        } else {
            el.textContent = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_15)%>';
            $('#DiscoveryErrors').hide();
        }
        toggleErrorDetailsState = !toggleErrorDetailsState;
    }

    function StartPortScan() {
        $("#" + monitoredPortsId).val('');
        $("#Results").empty();
        $("#DiscoveryErrors").html('');
        $("#ErrorPanel").hide();
        if ($("#" + scanForPortsId).prop('checked') == true) {
            $("#" + waitingImageId).show();
            StartPortDiscovery();
        } else {
            $("#" + waitingImageId).hide();
        }
    }
</script>


<div class="GroupBox" style="margin-top: -30px;">


	<div class="udt_MonitorPortsContainer" style="width:600px;">
            <input type="hidden" runat="server" id="MonitoredPorts" />
            <input type="hidden" runat="server" id="Validator" /> 

			<span class="ActionName"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_VB1_232, NodeWorkflowHelper.Node.Caption)%>
            </span>
            <br /><br />
            <input id="ScanForPorts" runat="server" type="checkbox" onclick="StartPortScan();"/>&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_234 %>
            <table id="LicenseStatusPanel" style="width:600px; height:40px; display:none; background-color:#FACECE; color:#CE0000; font-weight:bold;" cellspacing="4"><tr>
                <td style="width:24px; text-align:center; vertical-align:top;"><img alt="error" src="/Orion/UDT/Images/Admin/Discovery/error_16x16.gif" /></td>
                <td><span id="LicenseStatus"></span></td>
                </tr>
            </table>

            <div id="ErrorPanel" style="display:none; padding-bottom:8px;">
                <table width="100%" style="background-color:#FACECE; color:#CE0000;" cellspacing="4"><tr>
                    <td style="width:24px; text-align:center; vertical-align:top;"><img alt="error" src="/Orion/UDT/Images/Admin/Discovery/error_16x16.gif" /></td>
                    <td><b><%= Resources.UDTWebContent.UDTWEBDATA_VB1_235%></b> » <a style="cursor:pointer;" onclick="toggleErrorDetails(this)"><%= Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_15%></a><br />
                    <span style="display:none; color:#000000;" id="DiscoveryErrors"></span></td>
                    </tr>
                    </table>
            </div>

            <div runat="server" id="WaitingImage" style="display:none;">
                <p style="vertical-align:middle;"><img style="vertical-align:middle;" src="../../images/animated_loading_sm3_whbg.gif" /> <%= Resources.UDTWebContent.UDTWEBDATA_VB1_236%>&nbsp;<orion:LocalizableButton runat="server" style="vertical-align:middle;" DisplayType="Small" LocalizedText="Cancel" id="CancelPortDiscovery" OnClientClick="CancelDiscovery(); return false;"/></p>            
            </div>
            <br />

            <div id="Results" />


            <br />
</div>

</div>


