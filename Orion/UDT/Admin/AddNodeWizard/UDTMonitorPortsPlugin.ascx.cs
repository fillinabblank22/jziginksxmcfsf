﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;

public partial class Orion_UDT_Admin_AddNodeWizard_UDTMonitorPortsPlugin : System.Web.UI.UserControl, IAddCoreNodePlugin, IAddNodePluginWizard
{

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public void Cancel()
    {
        NodeWorkflowHelper.Reset();
        Response.Redirect("../Default.aspx", true);
    }

    public void Next()
    {
        if (!Page.IsValid) { return; }
        UpdateData();
    }

    public void Previous()
    {
        if (!Page.IsValid) { return; }
        UpdateData();
    }

    private void UpdateData()
    {
        string monitoredPortsList = MonitoredPorts.Value;
        HttpContext.Current.Session["UDT_AddNode_PortDiscoveryMonitoredPortList"] = monitoredPortsList;
    }

    public void Save(SolarWinds.Orion.Core.Common.Models.Node node)
    {
        //no need to save anything is scan option isn't selected
        if (ScanForPorts.Checked == false) return; 
        
        if (HttpContext.Current.Session["UDT_AddNode_PortDiscoveryResults"] == null) return;
        if (HttpContext.Current.Session["UDT_AddNode_PortDiscoveryMonitoredPortList"] == null) return;

        //Get the result from sesh
        UDTDiscoveryResult result = (UDTDiscoveryResult) HttpContext.Current.Session["UDT_AddNode_PortDiscoveryResults"];

        //Get the switch/device
        UDTNode udtSwitch = (UDTNode) result.UDTNodes.Where(
            n => n.ContainsCapability(UDTNodeCapability.Layer2)).FirstOrDefault();

        //Set the node ID from Orion.Node
        udtSwitch.NodeID = node.ID;

        //Set the node ID in the capability
        foreach (var capability in udtSwitch.Capabilities)
        {
            capability.NodeID = node.ID;
        }

        //Mark monitored/unmonitored depending on "MonitoredPorts" list
        // -1 = all
        string monitoredPortsList = MonitoredPorts.Value;
        List<string> monitoredPortsLookup = new List<string>(monitoredPortsList.Split(','));
        foreach (UDTPort port in udtSwitch.Ports)
        {
            port.NodeID = node.ID;
            if (monitoredPortsLookup.Contains(port.PortIndex.ToString()) || monitoredPortsLookup.Contains("-1"))
            {
                port.IsMonitored = true;
            }
            else
            {
                port.IsMonitored = false;
                port.IsExcluded = true;
                port.UDTFlags = UDTPortFlags.Excluded;
            }
        }
        

        //Add/Update switch in BL
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            //StartImport
            try
            {
                bl.DiscoveryStartImport(result);
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                log.ErrorFormat("UDT Add Node - Port Discovery StartImport failed, NodeID={0}.  Details: {1}", node.ID, ex.Message);
                throw;
            }

            //Polling jobs update
            try
            {
                bl.PollingCheckUpdateJobs4Node(node.ID);
            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                log.ErrorFormat("UDT Add Node - Port Discovery PollingCheckUpdateJobs4Node failed, NodeID={0}.  Details: {1}", node.ID, ex.Message);
                throw;
            }
        }
    }
    
    public string Title
    {
        get { return Resources.UDTWebContent.UDTWEBDATA_AK1_112; }
    }

    public bool ValidateInput()
    {
        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        
    }

}