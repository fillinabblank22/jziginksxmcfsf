﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DCCredentialsPlugin.ascx.cs" Inherits="Orion_UDT_Admin_AddNodeWizard_DCCredentialsPlugin" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Register TagPrefix="web" Namespace="SolarWinds.Orion.Web.UI" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<style type="text/css">
    .checkbox label{padding-left:5px;}
    .sw-validation-error { padding-left: 5px;}
   
</style>
 <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
 <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
 <orion:Include ID="Include3" runat="server" File="../UDT/js/ValidateDomainController.js" />
 
   <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
<div class="contentBlock">
    <div>
        <table>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:CheckBox runat="server" ID="cbMultipleSelection" AutoPostBack="True" OnCheckedChanged="cbMultipleSelection_CheckedChanged" Checked="false" />
                    <asp:Literal ID="pluginTitle" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_221 %>" />
                </td>
                <td class="rightInputColumn">
                    <asp:CheckBox ID="cbEnableDCPolling" runat="server" OnCheckedChanged="cbEnableDCPolling_CheckedChanged" CssClass="checkbox" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_222 %>" AutoPostBack="True" />
                </td>
            </tr>
        </table>
    </div>

    <div class="blueBox" id="credentialsTable" runat="server">
        <table>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblCredential" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_42 %>"/>
                </td>
	            <td class="rightInputColumn">
                    <asp:DropDownList ID="ddlDCCredentials" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDCCredentials_IndexChanged" >
                    </asp:DropDownList>
	            </td>
	            <td>&nbsp;</td>
            </tr>
</table>
<div id="credentialDetails" runat="server">
<table>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblCredName" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_43 %>"/>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbCaption" runat="server" MaxLength="50" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCaption" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_223%>"></asp:RequiredFieldValidator>
	            </td>
	            <td>&nbsp;</td>
            </tr>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_45 %>"/>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbLogin" runat="server" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_46 %>" ></asp:RequiredFieldValidator>
	            </td>
            </tr>
            <tr>
                <td></td>
                <td class="modifiedLeftLabelColumn pollingSelector">
                    <div class="smallText">
                        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_47%></div>
                    <div id="EditCredentialHelp">
                        <span class="LinkArrow">&#0187;</span>
                        <orion:HelpLink ID="UDTDCCredHelpLink" runat="server" HelpUrlFragment="OrionCoreAGEditingUserAccounts"
                            HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_48 %>" CssClass="helpLink" />
                    </div>
	            </td>	            
            </tr>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_49 %>"/>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="50" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_50 %>"></asp:RequiredFieldValidator>
	            </td>
	            <td>&nbsp;</td>
            </tr>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_51 %>"/>
                </td>
	            <td class="rightInputColumn">
                    <asp:TextBox ID="tbPasswordConfirmation" runat="server" TextMode="Password" MaxLength="50" ></asp:TextBox>
	                <asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_52 %>"/>
	                <asp:CompareValidator
	                    ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbPassword" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_53 %>"/>
	            </td>
	            <td>&nbsp;</td>
            </tr>
        </table>
        </div>

        <div runat="server" id="DCCredValidationTrue" style="padding-left: 118px; background-color: #D6F6C6;
      text-align: center" visible="false">
            <img runat="server" id="DCCredValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_55%>
        </div>
        <div runat="server" id="DCCredValidationFalse" style="padding-left: 118px; background-color: #FEEE90;
                               text-align: center; color: Red" visible="false">
            <img runat="server" id="DCCredValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
            <asp:Label runat="server" ID="DCCredValidationFailedMessage" />
        </div>
     

        <table>
            <tr>
                <td class="leftLabelColumn">&nbsp;</td>
                <td class="rightInputColumn">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="imbtnValidateDCCred" LocalizedText="Test" 
                            OnClick="imbtnValidateDC_Click" DisplayType="Small" />
                    </div>
                </td>
		        <td><asp:UpdateProgress runat="server" ID="snmpTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" style="margin-left: 10px">
                        <ProgressTemplate>
                                &nbsp;<img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                <span style="font-weight:bold;"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>                  
                        </ProgressTemplate>
                    </asp:UpdateProgress></td>
            </tr>
        </table>

        <table>
            <tr>
	            <td class="leftLabelColumn">&nbsp;</td>
	            <td class="rightInputColumn">&nbsp;</td>
	            <td>&nbsp;</td>
            </tr>
            <tr>
	            <td class="leftLabelColumn">
	                <asp:Label runat="server" ID="lblUDTSwitchPollingInterval" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_224 %>"/>
                </td>
	            <td class="rightInputColumn" nowrap="nowrap">
                    <orion:ValidatedTextBox CustomStringValue="UDTDCPollingInterval"
                        Text="30" ID="txtUDTDCPollingInterval" Columns="20"
                        Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                        MinValue="5" MaxValue="1200" ValidatorText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_237 %>"/>
	                <asp:Label runat="server" ID="lblUDTDCPollingIntervalExtraInfo" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_AK1_30 %>"/> 
	            </td>
	            <td>&nbsp;</td>
            </tr>
        </table>
    </div>

</div>

