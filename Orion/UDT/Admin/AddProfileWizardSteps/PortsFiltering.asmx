﻿<%@ WebService Language="C#" Class="PortsFiltering" %>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Services;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Common.PackageManager;
using SolarWinds.Interfaces.Common.Models.Discovery;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models.Discovery;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class PortsFiltering : WebService
{
    private static readonly Log log = new Log();

    private bool portsRetrievedFromSession = false;
    private List<InterfaceFiltering.DiscoveredInterfaceWithUniqueID> Ports
    {
        get
        {
            if (!ResultsWorkflowHelper.AutoResetItems.ContainsKey("ports"))
            {
                ResultsWorkflowHelper.AutoResetItems["ports"] = GetPortsFromSession();
                portsRetrievedFromSession = true;
            }

            return (List<InterfaceFiltering.DiscoveredInterfaceWithUniqueID>)ResultsWorkflowHelper.AutoResetItems["ports"];
        }
    }

    private bool FirstLoad
    {
        get
        {
            if (!ResultsWorkflowHelper.AutoResetItems.ContainsKey("ports_firstload"))
            {
                ResultsWorkflowHelper.AutoResetItems.Add("ports_firstload", false);
                return true;
            }
            return false;
        }
    }

    private List<InterfaceFiltering.Group> Groups
    {
        get
        {
            return ResultsWorkflowHelper.AutoResetItems.ContainsKey("ports_groups") ? (List<InterfaceFiltering.Group>)ResultsWorkflowHelper.AutoResetItems["ports_groups"] : null;
        }

        set
        {
            ResultsWorkflowHelper.AutoResetItems["ports_groups"] = value;
        }
    }

    // remove this after Core fixes possibility of duplicate nodes in results
    private static void UpdateNodeNames(List<InterfaceFiltering.DiscoveredInterfaceWithUniqueID> interfaces, HashSet<string> duplicateNames)
    {
        foreach (var i in interfaces)
            if (duplicateNames.Contains(i.NodeName))
                i.NodeName = String.Format("{0}({1})", i.NodeName, i.ProfileID.ToString(CultureInfo.InvariantCulture));
    }

    private List<InterfaceFiltering.DiscoveredInterfaceWithUniqueID> GetPortsFromSession()
    {
        var result = new List<InterfaceFiltering.DiscoveredInterfaceWithUniqueID>();

        if (ResultsWorkflowHelper.DiscoveryResults == null)
        {
            log.Error("Unable to get discovery results");
            throw new ArgumentNullException("ResultsWorkflowHelper.DiscoveryResults");
        }

        var nodeNames = new HashSet<string>();
        var duplicateNames = new HashSet<string>();

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

            if (coreResult == null)
            {
                log.ErrorFormat("Unable to get Core discovery result for Profile {0}", discoveryResult.ProfileID);
                throw new ArgumentNullException("coreResult");
            }

            var nodes = new Dictionary<int, string>();

            foreach (var node in coreResult.DiscoveredNodes.Where(n => n.IsSelected))
            {
				nodes[node.NodeID] = node.DisplayName;

				// remove this after Core fixes possibility of duplicate nodes in results
				if (nodeNames.Contains(node.DisplayName))
				{
					duplicateNames.Add(node.DisplayName);
				}
				nodeNames.Add(node.DisplayName);
            }

            var portsResult = discoveryResult.GetPluginResultOfType<UDTDiscoveryPluginResult>();

            if (portsResult == null)
            {
                log.ErrorFormat("Unable to get Ports discovery result for Profile {0}", discoveryResult.ProfileID);
                throw new ArgumentNullException("portsResult");
            }

            // add discovered interfaces as ports (selection is stored in wrapped objects)
            if (!portsResult.DiscoveredPorts.Any())
            {
                // get interfaces from Interfaces.Discovery package result
                var interfacesResult = discoveryResult.GetPluginResultOfType<InterfacesDiscoveryPluginResult>();

                if (interfacesResult == null)
                {
                    log.ErrorFormat("Unable to get Interfaces discovery result for Profile {0}", discoveryResult.ProfileID);
                    throw new ArgumentNullException("interfacesResult");
                }
				
				portsResult.WrapInterfaces(interfacesResult);
            }

            result.AddRange(portsResult.DiscoveredPorts.Where(i => nodes.ContainsKey(i.DiscoveredNodeID)).Select(i => new InterfaceFiltering.DiscoveredInterfaceWithUniqueID(i, discoveryResult.ProfileID, nodes[i.DiscoveredNodeID])));
        }

        // remove this after Core fixes possibility of duplicate nodes in results
        UpdateNodeNames(result, duplicateNames);

        return result;
    }

    private List<InterfaceFiltering.Group> GroupBy(string type, bool useCache = true)
    {
        if (useCache && Groups != null)
        {
            return Groups;
        }

        Groups = InterfaceFiltering.GroupBy(Ports, type).ToList();

        return Groups;
    }

    /// <summary>
    /// Called once after page is loaded
    /// </summary>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.Settings Initialization()
    {
        // reset cache
        ResultsWorkflowHelper.AutoResetItems.Remove("ports");
        ResultsWorkflowHelper.AutoResetItems.Remove("ports_groups");

        // reset unique ID counter
        InterfaceFiltering.DiscoveredInterfaceWithUniqueID.Init();

        var result = new InterfaceFiltering.Settings()
        {
            ShowVLANs = true,
            ShowProtocols = false
        };

        result.ShowProtocols = false;

        return result;
    }

    /// <summary>
    /// Returns list of values (groups) for particular type like Interface Type
    /// </summary>
    /// <param name="type">Interface's property</param>
    /// <param name="show">selected/unselected/all</param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public IEnumerable<InterfaceFiltering.GroupUI> GetGroups(string type, string show)
    {
        var groups = GroupBy(type, false);

        return InterfaceFiltering.GetGroups(groups, show);
    }


    /// <summary>
    /// Returns list of interfaces
    /// </summary>
    /// <param name="type">Grouping type like 'Interface Type'</param>
    /// <param name="value">Grouping value</param>
    /// <param name="show"></param>
    /// <param name="offset"></param>
    /// <param name="items"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.ChunkUI GetInterfaces(string type, string value, string show, int offset, int items)
    {
        var group = GroupBy(type).First(g => g.ID == value);

        return InterfaceFiltering.GetInterfaces(group, type, value, show, offset, items);
    }

    /// <summary>
    /// Changes IsSelected property of particular interface in session based on imput criteria
    /// </summary>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.GlobalInfo ReselectInterfaces(string statuses, string types, string ports, string protocols, List<InterfaceFiltering.Expression> expressions)
    {
        bool selectAll = !portsRetrievedFromSession || FirstLoad;

        return InterfaceFiltering.ReselectInterfaces(Ports, statuses, types, ports, "snmp", expressions, selectAll);
    }

    /// <summary>
    /// Changes IsSelected property of particular interface in session
    /// </summary>
    /// <param name="id">discoveredInterfaceID</param>
    /// <param name="select">true if we want to select interface otherwise false</param>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.InterfaceInfo SelectInterface(int id, string type, string value, bool select)
    {
        var groups = GroupBy(type);
        var group = groups.First(g => g.ID == value);

        return InterfaceFiltering.SelectInterface(group, id, type, value, select);
    }

    /// <summary>
    /// Changes IsSelected property for group interfaces in session
    /// </summary>
    /// <param name="id">group ID</param>
    /// <param name="select">true if we want to select group otherwise false</param>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.GroupUI SelectGroup(string type, string value, bool select)
    {
        var group = GroupBy(type).First(g => g.ID == value);

        return InterfaceFiltering.SelectGroup(group, type, value, select);
    }

    /// <summary>
    /// Changes IsSelected property for group of VLAN interfaces in session
    /// </summary>
    /// <param name="id">group ID</param>
    /// <param name="select">true if we want to select group otherwise false</param>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.VLANGroupUI SelectVLANGroup(string type, string value, bool select)
    {
        // select all interfaces in current group
        var group = GroupBy(type).First(g => g.ID == value);

        return InterfaceFiltering.SelectVLANGroup(group, type, value, select);
    }

    /// <summary>
    /// Changes IsSelected property for all interfaces
    /// </summary>
    /// <param name="id">group ID</param>
    /// <param name="select">true if we want to select group otherwise false</param>
    [WebMethod(EnableSession = true)]
    public InterfaceFiltering.GlobalInfo SelectAll(bool select)
    {
        return InterfaceFiltering.SelectAll(Ports, select);
    }

    /// <summary>
    /// Empty method just to refresh session timeout 
    /// </summary>
    [WebMethod(EnableSession = true)]
    public void KeepAlive()
    {
    }
}