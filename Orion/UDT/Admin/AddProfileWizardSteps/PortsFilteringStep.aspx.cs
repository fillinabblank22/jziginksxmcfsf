﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;

public partial class Orion_UDT_Admin_AddProfileWizardSteps_PortsFilteringStep : ResultsWizardBasePage, IStep
{
    protected override void Next()
    {
        if (!ValidateUserInput()) return;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            interfacesList.DataBind();
        }
    }

    #region IStep Members

    public string Step
    {
        get { return "Ports"; }
    }

    #endregion
}