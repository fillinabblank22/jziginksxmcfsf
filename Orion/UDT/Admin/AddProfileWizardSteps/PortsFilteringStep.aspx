﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true" CodeFile="PortsFilteringStep.aspx.cs" Inherits="Orion_UDT_Admin_AddProfileWizardSteps_PortsFilteringStep" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_MZ_1%>"%>
<%@ Register Src="~/Orion/InterfacesDiscovery/Controls/Discovery/InterfaceFilteringList.ascx" TagPrefix="orion" TagName="InterfacesFiltering" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    
    <orion:InterfacesFiltering ID="interfacesList" runat="server"
        FilteringServiceUrl="/Orion/UDT/Admin/AddProfileWizardSteps/PortsFiltering.asmx/"
        NoResultsKbLinkUrl="http://www.solarwinds.com/documentation/kbloader.aspx?lang={0}&kb=5746"
        NoResultsLabelText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_1)%>"
        NoResultsInfoText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_2)%>"
        HeaderText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_4)%>"
        NumOfItemsSelectedText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_5)%>"
        ReselectItemsText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_19)%>"
        ListOfItemsText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_10)%>"
        TypeLabelText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_16)%>"
        AliasLabelText="<%#(Resources.UDTWebContent.INTERFACESFILTERING_MZ0_18)%>" 
        GroupByOption ="<%#(Convert.ToString(GroupByValue.Node).ToLower())%>"/>

    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="imgbBack_Click"/>
        <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
        <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
    </div>
    
</asp:Content>
