﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using ButtonType = SolarWinds.Orion.Web.UI.ButtonType;

public partial class Orion_UDT_Admin_UDTJobInfo : Page
{
    private static readonly Log Log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsViewUDTJobStatus";
        pageTitleBar.PageTitle = Page.Title;
        pageTitleBar.HideCustomizePageLink = true;
        Render();
    }

    private void Render()
    {
        var jobInfoTable = new Table();
        jobInfoTable.Attributes.Add("width", "100%");
        jobInfoTable.CellPadding = 0;
        jobInfoTable.CellSpacing = 0;
        jobInfoTable.CssClass = "JobInfoStyles NeedsZebraStripes";

        string[][] info;
        using (var bl = BusinessLayerFactory.Create())
        {
            info = bl.PollingGetAllEnginesJobsInfoExtended();
        }

        //sort info (by NodeID (info[row][0]))
        Array.Sort(info, (a, b) => int.Parse(a[0]).CompareTo(int.Parse(b[0])));
        //Add header row
        var headerRow = new TableHeaderRow {CssClass = "JobInfoHeaderStyles", HorizontalAlign = HorizontalAlign.Center};
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_NodeID });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWebContent_Engine });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_IPAddress });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_NodeCaption });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_AK1_3 });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_NodeUnmanaged });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_PollingDisabled });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_PollingInterval });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_ScanDuration, ToolTip = string.Format(UDTWebContent.UDTWEBCODE_ScanDurationTooltip, UDTWebContent.UDTWEBCODE_Averaging_Factor)});
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_Averaging_Factor, ToolTip = string.Format(UDTWebContent.UDTWEBCODE_AveragingFactorTooltip, UDTWebContent.UDTWEBCODE_ScanDuration)});
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_AK1_4 });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_LastSuccessfulScan });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_LastResult });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_AK1_5 });
        headerRow.Cells.Add(new TableHeaderCell() { CssClass = "JobInfoHeaderStyles", Text = UDTWebContent.UDTWEBCODE_AK1_6 });

        jobInfoTable.Rows.Add(headerRow);

        Color calmYellow = Color.FromArgb(255, 255, 128);
        Color calmOrange = Color.FromArgb(255, 200, 100);

        //Add body rows
        foreach (string[] cols in info)
        {
            try
            {
                var jobCaption = $"{cols[3]} {cols[5]} ({cols[2]}) [{cols[0]}]";
                var tableRow = new TableRow {CssClass = "JobInfoStyles"};
                TableCell tableCell;
                string buttonTtoolTipText = string.Empty;
                if (Log.IsDebugEnabled && cols[16] != "1" && cols[6] == "0" && cols[7] == "1")
                    Log.Debug($"Job does not exist in UDT_Job table for N:{cols[0]}|{cols[5]}|{cols[6]}|{cols[7]}|{cols[9]}");
                for (int col = 0; col < 15; col++)
                {
                    tableCell = new TableCell {CssClass = "JobInfoCellStyles"};
                    string toolTipText = string.Empty;
                    switch (col)
                    {
                        case 0:
                        case 3:
                            HyperLink link = new HyperLink { 
                                NavigateUrl = $"/Orion/View.aspx?NetObject=N:{cols[0]}",
                                Text = cols[col],
                                Target = "_blank"
                            };
                            tableCell.Controls.Add(link);
                            break;
                        case 4:
                            continue;
                        case 6:
                            tableCell.Text = cols[col];
                            if (cols[col] != "No")
                            {
                                tableCell.BackColor = calmOrange;
                                tableCell.Font.Bold = true;
                                toolTipText += UDTWebContent.UDTWEBCODE_NodeIsUnmanged;
                                buttonTtoolTipText += UDTWebContent.UDTWEBCODE_NodeIsUnmanged;
                            }
                            break;
                        case 7:
                            tableCell.Text = cols[col];
                            if (cols[col] != "No")
                            {
                                tableCell.BackColor = calmOrange;
                                tableCell.Font.Bold = true;
                                toolTipText += UDTWebContent.UDTWEBCODE_PollingIsDisabled;
                                buttonTtoolTipText += UDTWebContent.UDTWEBCODE_PollingIsDisabled;
                            }
                            break;
                        case 8:
                        case 9:
                            int interval;
                            double duration;
                            if (int.TryParse(cols[8], out interval)
                                && double.TryParse(cols[9], NumberStyles.AllowDecimalPoint, CultureInfo.CurrentCulture, out duration)
                                && duration > interval * 60)
                            {
                                tableCell.BackColor = calmYellow;
                                tableCell.Font.Bold = true;
                                toolTipText += UDTWebContent.UDTWEBCODE_ScanDurationIsGreater;
                            }
                            tableCell.Text = string.Concat(cols[col], col == 8 ? UDTWebContent.UDTWEBCODE_Minutes :  UDTWebContent.UDTWEBCODE_Seconds);
                            break;
                        case 13:
                            tableCell.Text = cols[col];
                            if (cols[col] != ResultStatus.Success.ToString())
                            {
                                tableCell.BackColor = Color.Red;
                                tableCell.ForeColor = Color.White;
                                tableCell.Font.Bold = true;
                            }
                            break;
                        case 11:
                        case 12:
                        case 14:
                            //when column is LastRun or NextRun or LastSuccessfulScan
                            DateTime runDateTime;
                            if (DateTime.TryParse(cols[col], out runDateTime))
                            {
                                tableCell.Text = cols[col];
                                if (runDateTime.Year <= 1900)
                                    tableCell.BackColor = Color.DarkGray;
                            }
                            else
                            {
                                tableCell.Text = string.Empty;
                                tableCell.BackColor = Color.Red;
                                tableCell.ForeColor = Color.White;
                            }
                            break;
                        default:
                            tableCell.Text = cols[col];
                            break;
                    }
                    if (col != 2)
                        tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                    if (!string.IsNullOrEmpty(toolTipText))
                        tableCell.ToolTip = toolTipText;
                }
                //Add poll now button
                tableCell = new TableCell
                {
                    Width = 100,
                    CssClass = "JobInfoCellStyles"
                };

                bool pollNowEnabled = //cols[15] != Guid.Empty.ToString() && // Job Guid exists
                                      cols[6] == "No" && // Orion Node Unmanaged = 0
                                      cols[7] == "No"; // UDT Job Enabled = 1

                var button = new LocalizableButton
                {
                    DisplayType = ButtonType.Small,
                    Text = UDTWebContent.UDTWEBCODE_AK1_6,
                    Enabled = pollNowEnabled,
                    CommandName = UDTCapability.ToJobType((UDTNodeCapability) Enum.Parse(typeof(UDTNodeCapability), cols[4])).ToString(),
                    CommandArgument = cols[0]
                };
                button.Click += PollNowClick;
                button.OnClientClick = "Ext.Msg.progress('" +
                                       string.Format(UDTWebContent.UDTWEB_JS_CODE_AK1_6, jobCaption) + "', '" +
                                       UDTWebContent.UDTWEB_JS_CODE_AK1_7 + "');";
                tableCell.HorizontalAlign = HorizontalAlign.Center;
                if (!string.IsNullOrEmpty(buttonTtoolTipText))
                    tableCell.ToolTip = buttonTtoolTipText;

                tableCell.Controls.Add(button);
                tableRow.Cells.Add(tableCell);
                jobInfoTable.Rows.Add(tableRow);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                for (int col = 0; col < 15; col++)
                    sb.AppendLine(cols[col]);
                Log.Error($"Error in Render() for info:\r\n{sb}\r\n{ex}");
            }
        }

        jobInfo.Controls.Add(jobInfoTable);
    }

    void PollNowClick(Object sender, EventArgs e)
    {
        var clickedButton = (LocalizableButton)sender;
        var jobType = clickedButton.CommandName;
        var nodeId = int.Parse(clickedButton.CommandArgument);
        using (var bl = BusinessLayerFactory.Create())
        {
            bl.PollingPollNow(nodeId, (UDTJobType)Enum.Parse(typeof(UDTJobType), jobType));
        }
    }

//    void PollAll_Click(Object sender, EventArgs e)
//    {
//        using (var bl = BusinessLayerFactory.Create())
//        {
//            bl.PollingPollAllNow();
//        }
//    }
}