<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_1 %>" CodeFile="Default.aspx.cs" Inherits="Orion_UDT_Admin_Default"  %>
<%--<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>--%>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Src="~/Orion/Admin/AdminBucket.ascx" TagPrefix="orion" TagName="AdminBucket" %>

<asp:Content runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
  <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
  <style type="text/css">
    #container { background: transparent; background-image:none;}
    #adminNav { display: none; }
	#adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
	.column1of2 { padding-left:0px; padding-right:5px; vertical-align:top; text-align: left; width: 50%; }
	.column2of2 { padding-left:5px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }	

     .BucketHeader
        {
            font-weight: bold;
            font-size: 14px;
        }
        .BucketLinkContainer
        {
            padding-left: 45px;
        }
	</style>
</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False" />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= Resources.UDTWebContent.UDTWEBDATA_AK1_1 %></h1>
</asp:Content>


<asp:Content ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
<style type="text/css">        
       body { background-color:#ECEDEE !important; }
</style>
	<table  style="margin-left: 10px">
<%--		<tr>
			<td class="PageHeader HeaderTable">
				User Device Tracker Settings			
			</td>
		</tr>--%>
        <tr><td><%= Resources.UDTWebContent.UDTWEBDATA_AK1_2 %></td></tr>
	</table>

        <table id="adminContent" style="padding-top: 0; padding-left: 0;  margin-left: 10px;" width="98%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
		  <tr>
			<td class="column1of2">
				<!-- Begin Port Management Bucket -->			
<%--				<orion:AdminBucket ID="gettingStarted" runat="server" 
				    Header="Port Management" 
				    Icon="../images/Admin/Discovery/port_icon.gif">
				    
				    <Description>Add, manage, configure and delete ports.</Description>
				    
				</orion:AdminBucket><div class="ContentBucket">--%>
				<div class="ContentBucket">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/UDT/images/Admin/Settings/port_36x36.png" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_3 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_4 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
                        	<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Discovery/Default.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_6 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Ports/Default.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_5 %></a></p>
							</td>
                            <td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>	
					</table>
				</div>
				<!-- End Port Management Bucket -->

				<!-- Begin Track Users and Endpoints Bucket-->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img alt="img" class="BucketIcon" src="/Orion/UDT/Images/Admin/Settings/detective_36x36.png" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_7 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_8 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="25%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Discovery/Default.aspx"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_MM_1, "<span><br />&nbsp;&nbsp;</span>") %></a></p>
							</td>
							<td class="LinkColumn" width="25%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/ManageDomainControllers.aspx"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_9, "<span><br />&nbsp;&nbsp;</span>") %></a></p>
							</td>
							<td class="LinkColumn" width="25%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/ManageWatchList.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_10 %></a></p>
							</td>
							<td class="LinkColumn" width="25%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/WhiteList/ManageWhiteList.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_9 %></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End Track Users and Endpoints Bucket -->


				<!-- Begin UDT Settings Bucket-->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img alt="img" class="BucketIcon" src="/Orion/UDT/Images/Admin/Settings/icon_36x36_settings.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_11 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_12 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Settings/Polling.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_13 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Settings/DataRetention.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_14 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Settings/Thresholds.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_15 %></a></p>
							</td>
						</tr>
                        <tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/UDTJobInfo.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_16 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Settings/AdvancedSettings.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_17 %></a></p>
							</td>
                            <td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<!-- End UDT Settings Bucket -->

			</td>
			<!-- Begin 2nd Column -->
			<td class="column2of2">			
				<!-- Begin License Details Bucket -->			
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/UDT/Images/Admin/Settings/license_36x36.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_18 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_19 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Details/ModulesDetailsHost.aspx"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_20 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								&nbsp;
							</td>
							<td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
				<!-- End License Details Bucket -->
                <!-- Begin thwack Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/UDT/Images/Admin/Settings/Icon.Thwack.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_21 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_22 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="http://thwack.solarwinds.com/community/network-management_tht/user-device-tracker"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_23 %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								&nbsp;
							</td>
                            <td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
                <!-- End thwack Bucket -->
                <!-- Begin Credential Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/images/notification_credential.gif" />
							</td>
							<td>
								<div class="BucketHeader"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_24 %></div>
								<p><%= Resources.UDTWebContent.UDTWEBDATA_AK1_25 %></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
                         <tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/UDT/Admin/Credentials/CredentialManager.aspx"><%= String.Format(Resources.UDTWebContent.UDTWEBDATA_AK1_25, "<span><br />&nbsp;&nbsp;&nbsp;</span>") %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
                                &nbsp;
							</td>
                            <td class="LinkColumn" width="33%">
								&nbsp;
							</td>
						</tr>
					</table>
				</div>
                <!-- End thwack Bucket -->
			</td>
			<!-- End 2nd Column -->
			
			
		  </tr>
		</tbody>
	</table>
	<br/>
	
</asp:Content>


