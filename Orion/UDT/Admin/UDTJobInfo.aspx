﻿<%@ Page Language="C#"  MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_89 %>" CodeFile="UDTJobInfo.aspx.cs" Inherits="Orion_UDT_Admin_UDTJobInfo" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
  <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
  <style type="text/css">
        .JobInfoStyles { border: 1px solid #c0c0c0 !important;
                        }
        .JobInfoCellStyles { border: 1px solid #E2E1D4 !important;
                         font-size: 11px !important;
                         padding:3px !important;
                          }
        .JobInfoHeaderStyles
        {
            border: 1px solid #E2E1D4 !important;
            background-color: #E2E1D4 !important;
            font-size: 11px !important;
            font-weight: bold !important;
            color: #000000 !important;
            padding:3px !important;
            text-align: center !important;
        }
  </style>
</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= Page.Title%></h1>
</asp:Content>

<asp:Content ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
    <br/><br/>
 <div style="padding:8px;">
	
	<div id="jobInfo" runat="server" style="width:100%"></div>

</div>
	
</asp:Content>


