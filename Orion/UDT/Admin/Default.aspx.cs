﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.Helpers;

/// <summary>
/// Summary description for Default
/// </summary>
public  partial class Orion_UDT_Admin_Default  : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettings";
        pageTitleBar.HideCustomizePageLink = true;
        pageTitleBar.HideDate = true;
        pageTitleBar.HideSettingsLink = true;
        pageTitleBar.PageTitle = Resources.UDTWebContent.UDTWEBDATA_AK1_1;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);


        if (!Profile.AllowAdmin)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWEBCODE_AK1_1));
        }
    }
}