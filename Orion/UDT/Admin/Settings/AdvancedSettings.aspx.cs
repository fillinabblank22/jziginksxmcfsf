﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;
using System.IO;

public partial class Orion_UDT_Admin_Settings_AdvancedSettings : System.Web.UI.Page
{
    private List<string> currentlySelectedPortTypes = new List<string>();
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!Profile.AllowAdmin)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWEBCODE_AK1_1));
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsAdvanced";
        pageTitleBar.PageTitle = Page.Title;
        pageTitleBar.HideCustomizePageLink = true;

        if (!IsPostBack)
        {
            LoadPortTypeCheckList();
            LoadValue(UDT_Settings_UDT_AdUserUpdateIntervalHours);
            LoadValue(UDT_Settings_UDT_SnmpTimeout);
            LoadValue(UDT_Settings_UDT_DiscoveryJobTimeout);
            LoadValue(UDT_Settings_UDT_RouterDiscoveryJobTimeout);
            LoadValue(UDT_Settings_UDT_PollingJobTimeout);
            LoadValue(UDT_Settings_UDT_Layer3JobTimeout);
            LoadValue(UDT_Settings_UDT_DnsJobTimeout);
            LoadValue(UDT_Settings_UDT_DnsCachePositiveTTL);
            LoadValue(UDT_Settings_UDT_DnsCacheNegativeTTL);
            LoadValue(UDT_Settings_UDT_SnmpPacingDelay);
            LoadValue(UDT_Settings_UDT_SearchResultsRowLimit);
            LoadValue(UDT_Settings_UDT_MacNotificationProcessInterval);
        }
    }

    private void LoadPortTypeCheckList()
    {

        //Get monitored port type list from DB (UDT_Setting)
        string monitoredPortTypes = UDTSettingDAL.GetSettingOrDefault("UDT.MonitoredIfTypes");
        List<string> monitoredPortTypeList = new List<string>(monitoredPortTypes.Split(','));

        //Read full ifType (port type) list from MibDAL
        OidEnums ifTypes;
        ifTypes = MibDAL.GetInterfaceTypes();

        //Render port type list
        int i = 0;
        string portTypeImageUrl;
        StringBuilder jsEmit = new StringBuilder();
        jsEmit.AppendLine("<script type='text/javascript'>var ifType = new Array();");

        foreach (OidEnum ifType in ifTypes)
        {
            //Render check box list
            CheckBox chk = new CheckBox();
            chk.Style.Add("padding-left", "4px");
            chk.Style.Add("padding-right", "2px");
            chk.Attributes.Add("onclick","checkPortType(this)");
            chk.Attributes.Add("id", ifType.Id);
            chk.CssClass = "portType";
            if (monitoredPortTypeList.Contains(ifType.Id))
            {
                chk.Checked = true;
                currentlySelectedPortTypes.Add(String.Format(@"&nbsp;{0} ({1})", ifType.Name, ifType.Id));
            }
            //Check the Image file Exists.If not Assign 0.gif
            string imageUrl = String.Format(@"NetPerfMon\images\Interfaces\{0}.gif", ifType.Id);
            if (Request.PhysicalApplicationPath != null)
            {
                FileInfo imageFullPath = new FileInfo(Path.Combine(Request.PhysicalApplicationPath, imageUrl));
                
                if (imageFullPath.Exists)
                {
                    imageUrl = String.Format(@"/NetPerfMon/images/Interfaces/{0}.gif", ifType.Id);
                }
                else
                {
                    imageUrl = String.Format(@"/NetPerfMon/images/Interfaces/{0}.gif", 0);
                }
            }


            Image img = new Image();
            img.ImageUrl = imageUrl;
            Label lbl = new Label();
            lbl.Text = String.Format(@"&nbsp;{0} ({1})",ifType.Name, ifType.Id);
            lbl.Style.Add("color", "#000000");
            Panel pnl = new Panel();
            pnl.Controls.Add(chk);
            pnl.Controls.Add(img);
            pnl.Controls.Add(lbl);
            pnl.Style.Add("padding-top", "2px");
            pnl.Style.Add("padding-bottom", "2px");
            if (i%2==0) pnl.CssClass = "alternateRow";
            PortTypes.Controls.Add(pnl);

            //JS ifType emit
            jsEmit.AppendLine("ifType[" + ifType.Id + "] = '" + ifType.Name + "';");

            i++;
        }
        jsEmit.AppendLine("</script>");
        ClientScript.RegisterClientScriptBlock(this.GetType(), "ifTypeList", jsEmit.ToString());

        //Bind "currently selected" repeater
        selectedifTypeRepeater.DataSource = currentlySelectedPortTypes;
        selectedifTypeRepeater.DataBind();
    }

    private void SaveMonitoredPortTypes()
    {
        if (PortTypeList == null || String.IsNullOrEmpty(PortTypeList.Value))
            return;

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.SettingsSetSetting("UDT.MonitoredIfTypes", PortTypeList.Value);
            }

        }
        catch (Exception ex)
        {
            log.Error("There was a problem contacting the UDT Business Layer to update Advanced settings - Monitored Port Types.  ", ex);
            throw;
        }
 
    }

    private static void SaveValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.SettingsSetSetting(tb.CustomStringValue, tb.Text);
            }

        }
        catch (Exception ex)
        {
            log.Error(String.Format(@"There was a problem contacting the UDT Business Layer to update Advanced settings - {0}.  ", tb.CustomStringValue), ex);
            throw;
        }

    }


    private static void LoadValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        string setting = UDTSettingDAL.GetSettingOrDefault(tb.CustomStringValue);

        if (setting != null)
        {
            switch (tb.Type)
            {
                case ValidatedTextBox.TextBoxType.Integer:
                    tb.Text = setting;
                    break;

                default:
                    break;
            }
        }
    }

    protected void SubmitClick(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        SaveMonitoredPortTypes();
        SaveValue(UDT_Settings_UDT_AdUserUpdateIntervalHours);
        SaveValue(UDT_Settings_UDT_SnmpTimeout);
        SaveValue(UDT_Settings_UDT_DiscoveryJobTimeout);
        SaveValue(UDT_Settings_UDT_RouterDiscoveryJobTimeout);
        SaveValue(UDT_Settings_UDT_PollingJobTimeout);
        SaveValue(UDT_Settings_UDT_Layer3JobTimeout);
        SaveValue(UDT_Settings_UDT_DnsJobTimeout);
        SaveValue(UDT_Settings_UDT_DnsCachePositiveTTL);
        SaveValue(UDT_Settings_UDT_DnsCacheNegativeTTL);
        SaveValue(UDT_Settings_UDT_SnmpPacingDelay);
        SaveValue(UDT_Settings_UDT_SearchResultsRowLimit);
        SaveValue(UDT_Settings_UDT_MacNotificationProcessInterval);
        ReferrerRedirectorBase.Return(Request.Url.ToString());
    }

    protected void CancelClick(object source, EventArgs e)
    {
        //do nothing, just go back
        ReferrerRedirectorBase.Return("/Orion/UDT/Admin/");
    }

}
