<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_27 %>" CodeFile="Polling.aspx.cs" Inherits="Orion_UDT_Admin_Settings_Polling"  %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <style type="text/css">
        
        #adminContentTable
        {
            font-family: Arial, Verdana, Helvetica, Sans-Serif;
            font-size: 8pt;
            width: 80%;
        }
        
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        
        #inside_table td
        {
            padding: 5px 5px 5px 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
        }

        #adminContent
        {
            padding-top: 0px;
            padding-left: 13px;
        }

        #adminContent h2
        {
            margin: 0px 0px;
            font-weight: normal;
        }

        #adminContent table tr td.allowWrap
        {
            white-space: normal;
        }           
        
        .PropertyBorderless 
        {
            border-bottom: 0px !Important;
        }
	.sw-btn:hover 
    	{
		color: #204a63 !important;
    	}
    </style>


</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-top: 20px; padding-bottom: 20px"><%= Page.Title%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
        <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%" >
            <tr>
                <td>
                    <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%" class="NeedsZebraStripes">
                        <tr>
                            <td  class="PropertyHeader PropertyBorderless">
                            <%= Resources.UDTWebContent.UDTWEBDATA_DF1_15 %>
                            </td>
                            <td class="Property PropertyBorderless" align="left" valign="middle" >
                                <web:ValidatedTextBox CustomStringValue="Layer2"
                                    Text="30" ID="textL2PollingInterval" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="5" MaxValue="1200"  ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_29 %>" />
                                &nbsp;<%=MinutesText %> &nbsp;&nbsp;<%=Resources.UDTWebContent.UDTWEBDATA_DF1_18 %>
                            </td>
                            <td class="Property PropertyBorderless">
                            </td>
                        </tr>
                        <tr>
                            <td class="PropertyHeader">
                                &nbsp;
                            </td>
                            <td class="Property" ><div class="sw-btn-bar">
                                        <orion:LocalizableButton runat="server" ID="LocalizableButton1" CausesValidation="true" Text="<%$ Resources: UDTWebContent,UDTWEBDATA_DF1_7 %>"  DisplayType="Small" LocalizedText= "CustomText" OnClick="ReApplyPollingIntervalLayer2Click" /></div>
                            </td>
                            <td class="Property">
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader PropertyBorderless">
                               <%= Resources.UDTWebContent.UDTWEBDATA_DF1_16 %>
                            </td>
                            <td class="Property PropertyBorderless" align="left" valign="middle" >
                                <web:ValidatedTextBox CustomStringValue="Layer3"
                                    Text="30" ID="textL3PollingInterval" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="5" MaxValue="1200"  ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_29 %>" />
                                &nbsp;<%=MinutesText %>&nbsp;&nbsp;<%=Resources.UDTWebContent.UDTWEBDATA_DF1_19 %>
                            </td>
                            <td class="Property PropertyBorderless">
                               
                            </td>
                        </tr>
                        <tr>
                            <td class="PropertyHeader">
                            &nbsp;
                            </td>
                            <td class="Property" >
                                <div class="sw-btn-bar">
                                        <orion:LocalizableButton runat="server" ID="LocalizableButton2" CausesValidation="true" Text="<%$ Resources: UDTWebContent,UDTWEBDATA_DF1_13 %>"  DisplayType="Small" LocalizedText= "CustomText" OnClick="ReApplyPollingIntervalLayer3Click" /></div>
                            </td>
                            <td class="Property">
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader PropertyBorderless">
                                <%= Resources.UDTWebContent.UDTWEBDATA_DF1_17 %>
                            </td>
                            <td class="Property PropertyBorderless" align="left" valign="middle" >
                                <web:ValidatedTextBox CustomStringValue="DomainController"
                                    Text="30" ID="textRELPollingInterval" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="5" MaxValue="1200"  ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_29 %>" />
                                &nbsp;<%=MinutesText %>&nbsp;&nbsp;<%=Resources.UDTWebContent.UDTWEBDATA_DF1_20 %>
                            </td>
                            <td class="Property PropertyBorderless">
                                </td>
                        </tr>
                        <tr>
                            <td class="PropertyHeader PropertyBorderless">
                                &nbsp;
                            </td>
                            <td class="Property PropertyBorderless" >
                                <div class="sw-btn-bar">
                                        <orion:LocalizableButton runat="server" ID="LocalizableButton3" CausesValidation="true" Text="<%$ Resources: UDTWebContent,UDTWEBDATA_DF1_14 %>"  DisplayType="Small" LocalizedText= "CustomText" OnClick="ReApplyPollingIntervalRELClick" /></div>
                            </td>
                            <td class="Property PropertyBorderless">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <br />
    <div class="footer_buttons">
        <table style="margin-left:1em;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="imgSubmit" CausesValidation="true" LocalizedText="Save" DisplayType="Primary" OnClick="SubmitClick" />
                        <%-- &nbsp;--%>
                        <orion:LocalizableButton runat="server" ID="imgCancel" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" />
                    </div>                
                </td>
            </tr>
        </table>
    </div>

</asp:Content>





