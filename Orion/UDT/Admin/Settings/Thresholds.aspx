<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_90 %>" CodeFile="Thresholds.aspx.cs" Inherits="Orion_UDT_Admin_Settings_Thresholds"  %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <style type="text/css">
        
        #adminContentTable
        {
            font-family: Arial, Verdana, Helvetica, Sans-Serif;
            font-size: 8pt;
            width: 80%;
        }
        
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        
        #inside_table td
        {
            padding: 5px 5px 5px 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
        }

        #adminContent
        {
            padding-top: 0px;
            padding-left: 13px;
        }

        #adminContent h2
        {
            margin: 0px 0px;
            font-weight: normal;
        }

        #adminContent table tr td.allowWrap
        {
            white-space: normal;
        }
   	.sw-btn:hover 
    	{
		color: #204a63 !important;
    	}

    </style>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#inside_table tr:nth-child(even)').not('tr.header').addClass('alternateRow');            
        });
    </script>

</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-top: 20px; padding-bottom: 20px"><%= Page.Title%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
        <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="header">
                            <td colspan="3">
                                <h2>
                                    <%= Resources.UDTWebContent.UDTWEBDATA_AK1_91 %>
                                </h2>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_92 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT-PortPercentUtilization-Error"
                                    Text="30" ID="UDT_Settings_PortPercentUtilization_Error" Name="UDT_Settings_PortPercentUtilization_Error" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="100" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_93 %>" />
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_94 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_95 %>
                            </td>
                        </tr>
                        <tr>
                            <td class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_96 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT-PortPercentUtilization-Warning"
                                    Text="30" ID="UDT_Settings_PortPercentUtilization_Warning" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="100" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_97 %>" />
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_94 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_95 %>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0 0 0 0;>
                        <tr>
                            <td colspan="5" style="text-align:left">
                                <div id="divErrorToWarning" runat="server" style="padding: 0px 7px 0px 7px;background-color:#FFC1C1; font-family:Arial Verdana Sans-Serif; font-weight:bold; color:#C00000; display:none;">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <br />
    <div class="footer_buttons">
        <table style="margin-left:1em;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="imgSubmit" CausesValidation="true" LocalizedText="Save" DisplayType="Primary" OnClick="SubmitClick" />
                        <%-- &nbsp;--%>
                        <orion:LocalizableButton runat="server" ID="imgCancel" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" />
                    </div>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>





