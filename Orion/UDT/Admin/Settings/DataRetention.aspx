<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_37 %>" CodeFile="DataRetention.aspx.cs" Inherits="Orion_UDT_Admin_Settings_DataRetention"  %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />


    <style type="text/css">
        
        #adminContentTable
        {
            font-family: Arial, Verdana, Helvetica, Sans-Serif;
            font-size: 8pt;
            width: 80%;
        }
        
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        
        #inside_table td
        {
            padding: 5px 5px 5px 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
        }

        #adminContent
        {
            padding-top: 0px;
            padding-left: 13px;
        }

        #adminContent h2
        {
            margin: 0px 0px;
            font-weight: bold;
        }

        #adminContent table tr td.allowWrap
        {
            white-space: normal;
        }

    	.sw-btn:hover 
    	{
		color: #204a63 !important;
    	}
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#inside_table tr:nth-child(even)').not('tr.header').addClass('alternateRow');
        });
    </script>
</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-top: 20px; padding-bottom: 20px"><%= Page.Title%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
        <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class="header">
                            <td colspan="3">
                                <h2>
                                    <%= Resources.UDTWebContent.UDTWEBDATA_AK1_38 %>
                                </h2>
                            </td>
                        </tr>
<%--                        <tr>
                            <td  class="PropertyHeader">
                                Device Tracker Events Retention
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="Event-Retention"
                                    Text="90" ID="UDT_Events_Retention" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="5000" ValidatorText="<img src='/Orion/images/Small-Down.gif' title='Value must be a valid integer from 5 to 1200'/>" />
                                &nbsp;days
                            </td>
                            <td class="Property">
                                UDT events will be deleted from the database after the configured days.
                            </td>
                        </tr>--%>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_39 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="Data-Retention"
                                    Text="365" ID="UDT_Settings_Data_Retention" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="5000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_40 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_41 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_42 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_43 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="Data-Retention-Statistics-Detail"
                                    Text="7" ID="UDT_Settings_Data_Retention_Statistics_Detail" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="5000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_40 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_41 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_44 %>  
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_45 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="Data-Retention-Statistics-Hourly"
                                    Text="30" ID="UDT_Settings_Data_Retention_Statistics_Hourly" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="5000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_40 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_41 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_46 %>  
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_47 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="Data-Retention-Statistics-Daily"
                                    Text="365" ID="UDT_Settings_Data_Retention_Statistics_Daily" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="5000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_40 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_41 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_48 %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    
    <br />
    <div class="footer_buttons">
        <table style="margin-left:1em;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="imgSubmit" CausesValidation="true" LocalizedText="Save" DisplayType="Primary" OnClick="SubmitClick" />
                        <%-- &nbsp;--%>
                        <orion:LocalizableButton runat="server" ID="imgCancel" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" />
                    </div>
                </td>

            </tr>
        </table>
    </div>

</asp:Content>





