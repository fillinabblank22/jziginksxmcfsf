﻿<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_49 %>" CodeFile="AdvancedSettings.aspx.cs" Inherits="Orion_UDT_Admin_Settings_AdvancedSettings" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.UDT.Web.DAL" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register Assembly="OrionWeb" Namespace="SolarWinds.Orion.Web.UI" TagPrefix="web" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="UdtHeadPlaceHolder">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <link rel="stylesheet" type="text/css" href="../../../styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <style type="text/css">
        
        #adminContentTable
        {
            font-family: Arial, Verdana, Helvetica, Sans-Serif;
            font-size: 8pt;
            width: 80%;
        }
        
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        
        #inside_table td
        {
            padding: 5px 5px 5px 5px;
            vertical-align: middle;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
        }

        #adminContent
        {
            padding-top: 0px;
            padding-left: 13px;
        }

        #adminContent h2
        {
            margin: 0px 0px;
            font-weight: normal;
        }

        #adminContent table tr td.allowWrap
        {
            white-space: normal;
        }
        .udtList
        {
          width: 45em !important;  /* room for 3 columns */
          list-style-type: disc !important;
          list-style-position: inside !important;
          padding-left:12px;
        }
        .udtListItem
        {
          float: left;
          width: 15em !important;  /* accommodate the widest item */
        }
    	.sw-btn:hover 
    	{
		color: #204a63 !important;
    	}
  
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#inside_table tr:nth-child(odd)').not('tr.header').addClass('alternateRow');
            $("#"+PortTypeListClientId).val(MonitoredPortTypes);
        });

        var PortTypeListClientId = '<%=PortTypeList.ClientID %>';
        var InitialMonitoredPortTypes = [ <%=UDTSettingDAL.GetSettingOrDefault("UDT.MonitoredIfTypes") %> ];
        var MonitoredPortTypes = [ <%=UDTSettingDAL.GetSettingOrDefault("UDT.MonitoredIfTypes") %> ];

        Array.prototype.contains = function (element) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] == element) {
                    return true;
                }
            }
            return false;
        }

        function AlertIfUserHasRemovedPortType() {
            var userHasRemovedPortType = false;
            var removedPortTypeList = [];
            for (var i = 0; i < InitialMonitoredPortTypes.length; i++) {
                if (!MonitoredPortTypes.contains(InitialMonitoredPortTypes[i])) { userHasRemovedPortType = true; removedPortTypeList.push(InitialMonitoredPortTypes[i]); }
            }
            if (userHasRemovedPortType) {
                var removedPortTypes = '<ul style="list-style-type: disc; list-style-position: inside; padding-left:12px;">';
                for (var i = 0; i < removedPortTypeList.length; i++) {
                    removedPortTypes = removedPortTypes + '<li>' + ifType[removedPortTypeList[i]] + ' (' + removedPortTypeList[i] + ')</li>';
                }
                removedPortTypes = removedPortTypes + '</ul>';
                Ext.Msg.confirm("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_1) %>", "<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_2) %>" + "<br/><br/>" + removedPortTypes + "<br/><br/>" + "<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_3) %>", 
                function(btn,text) {
                    if (btn == 'yes') {
                        <%= this.ClientScript.GetPostBackEventReference(imgSubmit , "submit") %>;
                    }
                });
                return false;
            }
            return true;
        }     

        Array.prototype.indexOf = function (elt /*, from*/) {
            var len = this.length;

            var from = Number(arguments[1]) || 0;
            from = (from < 0)
             ? Math.ceil(from)
             : Math.floor(from);
            if (from < 0)
                from += len;

            for (; from < len; from++) {
                if (from in this &&
              this[from] === elt)
                    return from;
            }
            return -1;
        };

        function checkPortType(el) {
            var checkState = el.checked;
            var portTypeId = parseInt($(el).parent().attr("id"));
            if (checkState & !MonitoredPortTypes.contains(portTypeId)) MonitoredPortTypes.push(parseInt(portTypeId));
            if (!checkState & MonitoredPortTypes.contains(portTypeId)) {
                var idx = MonitoredPortTypes.indexOf(portTypeId);
                MonitoredPortTypes.splice(idx, 1);
            }
            $("#"+PortTypeListClientId).val(MonitoredPortTypes);
        }

        function reselectDefaults() {
            MonitoredPortTypes = [ 6,22,23,53,54,56,62,69,70,117,118,135,136,171 ];
            $(".portType").each( function(idx, pt) {
                if (MonitoredPortTypes.contains(parseInt($(pt).attr("id")))) {
                    $(pt).children().attr("checked", true);
                } else {
                    $(pt).children().attr("checked", false);
                }
            });
            $("#"+PortTypeListClientId).val(MonitoredPortTypes);
            Ext.Msg.minWidth = 600;
            Ext.Msg.alert("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_4) %>", String.format("<ul style='list-style-type: disc; list-style-position: inside; padding-left:12px;'><li>ethernet-csmacd (6)</li><li>propPointToPointSerial (22)</li><li>ppp (23)</li><li>propVirtual (53)</li><li>propMultiplexor (54)</li><li>fiberchannel (56)</li><li>fastEther (62)</li><li>fastEtherFX (69)</li><li>channel (70)</li><li>gigabitEthernet (117)</li><li>hdlc (118)</li><li>l2vlan (135)</li><li>l2ipvlan (136)</li><li>pos (171)</li></ul><br/><br/>{0}", "<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_AK1_5) %>"));
        }  

        
    </script>
</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1 style="padding-top: 20px; padding-bottom: 20px"><%= Page.Title%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
        <table id="adminContentTable" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_50 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.AdUserUpdateIntervalHours"
                                    Text="12" ID="UDT_Settings_UDT_AdUserUpdateIntervalHours" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="1" MaxValue="720" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_51 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_52 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_53 %>
                            </td>
                        </tr>                        
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_54 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.SnmpTimeout"
                                    Text="5" ID="UDT_Settings_UDT_SnmpTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="5" MaxValue="30" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_55 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_56 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_57 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_58 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.DiscoveryJobTimeout"
                                    Text="10" ID="UDT_Settings_UDT_DiscoveryJobTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="10" MaxValue="60" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_59 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_60 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_61 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_62 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.RouterDiscoveryJobTimeout"
                                    Text="120" ID="UDT_Settings_UDT_RouterDiscoveryJobTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="120" MaxValue="360" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_63 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_60 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_64 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_65 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.PollingJobTimeout"
                                    Text="15" ID="UDT_Settings_UDT_PollingJobTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="15" MaxValue="120" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_66 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_60 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_67 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_68 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.Layer3JobTimeout"
                                    Text="15" ID="UDT_Settings_UDT_Layer3JobTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="15" MaxValue="120" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_66 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_60 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_69 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_70 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.DnsJobTimeout"
                                    Text="30" ID="UDT_Settings_UDT_DnsJobTimeout" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="30" MaxValue="120" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_71 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_60 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_72 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_73 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.DnsCachePositiveTTL"
                                    Text="86400" ID="UDT_Settings_UDT_DnsCachePositiveTTL" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="3600" MaxValue="604800" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_74 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_56 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_75 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_76 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.DnsCacheNegativeTTL"
                                    Text="900" ID="UDT_Settings_UDT_DnsCacheNegativeTTL" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="900" MaxValue="86400" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_77 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_56 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_78 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_79 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.SnmpPacingDelay"
                                    Text="0" ID="UDT_Settings_UDT_SnmpPacingDelay" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="0" MaxValue="1000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_80 %>" />
                                &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_81 %>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_82 %>
                            </td>
                        </tr>
                        <tr>
                            <td  class="PropertyHeader" style="vertical-align:top; padding-top:8px;">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_83 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <asp:Panel runat="server" ID="PortTypes" Height="200"  BorderStyle="Solid" BorderWidth="1" ForeColor="#7F9DB9" BackColor="White" ScrollBars="Vertical"></asp:Panel>
                                <p style="width:100%; text-align:right;"><a style="text-decoration: underline; cursor: pointer;" onclick="reselectDefaults()"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_84 %></a></p>
                                <asp:HiddenField runat="server" ID="PortTypeList" />
                            </td>
                            <td class="Property" style="vertical-align:top; padding-top:8px;">
                               <%= Resources.UDTWebContent.UDTWEBDATA_AK1_85 %><br /><br /> 
                               <ul class="udtList">
                                <asp:Repeater ID="selectedifTypeRepeater" runat="server">
                                <ItemTemplate>
                                    <li class="udtListItem">
                                        <%# Container.DataItem%>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <br/>
                                </FooterTemplate>
                                </asp:Repeater>
                               </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_86 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.SearchResultsRowLimit"
                                    Text="1000" ID="UDT_Settings_UDT_SearchResultsRowLimit" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="100" MaxValue="5000" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_87 %>" />
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_AK1_88 %>
                            </td>
                        </tr>               
                        <tr>
                            <td class="PropertyHeader">
                                <%= Resources.UDTWebContent.UDTWEBDATA_MK1_1 %>
                            </td>
                            <td class="Property" align="left" valign="middle">
                                <web:ValidatedTextBox CustomStringValue="UDT.Traps.MacNotification.FetchInterval"
                                    Text="120" ID="UDT_Settings_UDT_MacNotificationProcessInterval" Columns="5"
                                    Type="Integer" runat="server" UseRequiredValidator="true" UseRangeValidator="true"
                                    MinValue="30" MaxValue="1800" ValidatorText="<%$ Resources: UDTWebContent, UDTWEBDATA_MK1_2 %>" />
                                    &nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_AK1_56%>
                            </td>
                            <td class="Property">
                                <%= Resources.UDTWebContent.UDTWEBDATA_MK1_3 %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <br />
    <div class="footer_buttons">
        <table style="margin-left:1em;" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton runat="server" ID="imgSubmit" CausesValidation="true" LocalizedText="Save" DisplayType="Primary" OnClick="SubmitClick" OnClientClick="return AlertIfUserHasRemovedPortType();"/>
                        <%-- &nbsp;--%>
                        <orion:LocalizableButton runat="server" ID="imgCancel" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelClick" />                    
                    </div> 
                </td>
            </tr>
        </table>
    </div>

</asp:Content>





