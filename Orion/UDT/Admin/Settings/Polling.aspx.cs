using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web.DAL;


public partial class Orion_UDT_Admin_Settings_Polling : System.Web.UI.Page
{
    private readonly static Log _log = new Log();

    private string _pollingIntervalL2;
    private string _pollingIntervalL3;
    private string _pollingIntervalREL;

    protected string MinutesText { get { return Resources.UDTWebContent.UDTWEBDATA_AK1_30; } }
    
    // TODO: UI: Default Polling Interval Page: Externalize text

    protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		base.OnLoad(e);
	}
	
    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsPollingInterval";
        pageTitleBar.PageTitle = Page.Title;
        pageTitleBar.HideCustomizePageLink = true;


        // load data
        try
        {
            _pollingIntervalL2 = UDTSettingDAL.GetSettingOrDefault("UDT.DefaultPollingInterval.Layer2");
            _pollingIntervalL3 = UDTSettingDAL.GetSettingOrDefault("UDT.DefaultPollingInterval.Layer3");
            _pollingIntervalREL = UDTSettingDAL.GetSettingOrDefault("UDT.DefaultPollingInterval.DomainController");

            if (!IsPostBack)
            {
                textL2PollingInterval.Text = _pollingIntervalL2;
                textL3PollingInterval.Text = _pollingIntervalL3;
                textRELPollingInterval.Text = _pollingIntervalREL;
            }
        }
        catch (Exception ex)
        {
            _log.Error(ex);
        }

    }
    protected void ReApplyPollingIntervalLayer2Click(object source, EventArgs e)
    {
        using (var bl = BusinessLayerFactory.Create())
        {
            bl.PollingSetPollingIntervalMinutes(int.Parse(textL2PollingInterval.Text), new[] { UDTNodeCapability.Layer2 });
            bl.PollingPollAllCapabilityNow(UDTNodeCapability.Layer2);
        }
    }
    protected void ReApplyPollingIntervalLayer3Click(object source, EventArgs e)
    {
        using (var bl = BusinessLayerFactory.Create())
        {
            bl.PollingSetPollingIntervalMinutes(int.Parse(textL3PollingInterval.Text), new[] { UDTNodeCapability.Layer3 });
            bl.PollingPollAllCapabilityNow(UDTNodeCapability.Layer3);
        }
    }
    protected void ReApplyPollingIntervalRELClick(object source, EventArgs e)
    {
        using (var bl = BusinessLayerFactory.Create())
        {
            bl.PollingSetPollingIntervalMinutes(int.Parse(textRELPollingInterval.Text), new[] { UDTNodeCapability.DomainController });
        }
    }

    protected void SubmitClick(object source, EventArgs e)
	{
		if (!Page.IsValid)
			return;

        // process user input
        // Layer 2
        if (!_pollingIntervalL2.Equals(textL2PollingInterval.Text))
        {
            UDTSettingDAL.SaveSetting("UDT.DefaultPollingInterval.Layer2", textL2PollingInterval.Text);
        }

        // Layer 3
        if (!_pollingIntervalL3.Equals(textL3PollingInterval.Text))
        {
            UDTSettingDAL.SaveSetting("UDT.DefaultPollingInterval.Layer3", textL3PollingInterval.Text);
        }

        // REL
        if (!_pollingIntervalREL.Equals(textRELPollingInterval.Text))
        {
            UDTSettingDAL.SaveSetting("UDT.DefaultPollingInterval.DomainController", textRELPollingInterval.Text);
        }

        // finally
		ReferrerRedirectorBase.Return(Request.Url.ToString());
	}

	protected void CancelClick(object source, EventArgs e)
	{
        //do nothing, just go back
        ReferrerRedirectorBase.Return("/Orion/UDT/Admin/");
	}

}
