using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;


public partial class Orion_UDT_Admin_Settings_DataRetention : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsDataRetention";
        pageTitleBar.PageTitle = Page.Title;
        pageTitleBar.HideCustomizePageLink = true;      
        
        if (!IsPostBack)
        {
            LoadValue(UDT_Settings_Data_Retention);
            LoadValue(UDT_Settings_Data_Retention_Statistics_Detail);
            LoadValue(UDT_Settings_Data_Retention_Statistics_Hourly);
            LoadValue(UDT_Settings_Data_Retention_Statistics_Daily);
        }
    }


    private static void SaveValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;
        UDTSettingDAL.SaveSetting(tb.CustomStringValue, tb.Text);
    }


    private static void LoadValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        string setting = UDTSettingDAL.GetSettingOrDefault(tb.CustomStringValue);

        if (setting != null)
        {
            switch (tb.Type)
            {
                case ValidatedTextBox.TextBoxType.Integer:
                    tb.Text = setting;
                    break;

                default:
                    break;
            }
        }
    }

    protected void SubmitClick(object source, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        SaveValue(UDT_Settings_Data_Retention);
        SaveValue(UDT_Settings_Data_Retention_Statistics_Detail);
        SaveValue(UDT_Settings_Data_Retention_Statistics_Hourly);
        SaveValue(UDT_Settings_Data_Retention_Statistics_Daily);
        ReferrerRedirectorBase.Return(Request.Url.ToString());
    }

    protected void CancelClick(object source, EventArgs e)
    {
        //do nothing, just go back
        ReferrerRedirectorBase.Return("/Orion/UDT/Admin/");
    }

}
