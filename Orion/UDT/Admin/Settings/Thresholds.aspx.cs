using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class Orion_UDT_Admin_Settings_Thresholds : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
	{
		ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);
		base.OnLoad(e);
	}
	
    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsPortThresholds";
        pageTitleBar.PageTitle = Page.Title;
        pageTitleBar.HideCustomizePageLink = true;  

        if (!IsPostBack)
        {
            LoadSetting(UDT_Settings_PortPercentUtilization_Error);
            LoadSetting(UDT_Settings_PortPercentUtilization_Warning);
        }
    }

  
    private static void SaveSettingValue(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;
        // save the OrionSetting
        SettingsDAL.SetValue(tb.CustomStringValue, Convert.ToDouble(tb.Text) );
        
    }


    private static void LoadSetting(ValidatedTextBox tb)
    {
        if (tb == null || String.IsNullOrEmpty(tb.CustomStringValue))
            return;

        // get the OrionSetting
        OrionSetting setting = SettingsDAL.GetSetting(tb.CustomStringValue);

        if (setting != null)
        {
            switch (tb.Type)
            {
                case ValidatedTextBox.TextBoxType.Integer:
                    // load the current value or if not avaialble, load the default
                    tb.Text = string.IsNullOrEmpty(setting.SettingValue.ToString()) ? setting.DefaultValue.ToString() : setting.SettingValue.ToString();

                    break;

                default:
                    break;
            }
        }
    }

    private static bool CompareLevelValues(ValidatedTextBox tbError, ValidatedTextBox tbWarning)
    {
        return Convert.ToDouble(tbError.Text) > Convert.ToDouble(tbWarning.Text);
    }


    protected void SubmitClick(object source, EventArgs e)
	{
		if (!Page.IsValid)
			return;
        
        if (!CompareLevelValues(UDT_Settings_PortPercentUtilization_Error, UDT_Settings_PortPercentUtilization_Warning))
        {
            // display the error div
            divErrorToWarning.InnerHtml =
            String.Format("<img style='vertical-align:middle; padding: 7px 0px 7px 0px;' src='/Orion/images/Small-Down.gif'/>&nbsp;&nbsp;{0}", Resources.UDTWebContent.UDTWEBCODE_AK1_8);
            divErrorToWarning.Style["display"] = "block";
            return;
        }

        SaveSettingValue(UDT_Settings_PortPercentUtilization_Error);
        SaveSettingValue(UDT_Settings_PortPercentUtilization_Warning);
		ReferrerRedirectorBase.Return(Request.Url.ToString());
	}

    protected void CancelClick(object source, EventArgs e)
	{
        //do nothing, just go back
        ReferrerRedirectorBase.Return("/Orion/UDT/Admin/");
	}

}
