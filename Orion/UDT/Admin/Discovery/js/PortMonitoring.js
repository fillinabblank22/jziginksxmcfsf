﻿
//Custom WebServiceStore that supports Grouping
ORION.WebServiceGroupStore = function (url, readerFields, sortOrderField, grouperField) {
    var c = {};

    ORION.WebServiceGroupStore.superclass.constructor.call(this, Ext.apply(c, {
        proxy: new Ext.data.HttpProxy({
            url: url,
            method: "POST",
            success: function (xhr, options) {
                var obj = Ext.decode(xhr.responseText);
                if (obj.d == null) {
                    SW.UDT.PortDiscoveryStatus = SW.UDT.NoDevicesFoundMessage;
                } else {
                    try {
                        SW.UDT.PortDiscoveryStatus = obj.d.MessageType;
                    }
                    catch (err) {
                        //Handle errors here
                    }
                }
            },
            failure: function (xhr, options) {
                ORION.handleError(xhr);
            }
        }),
        reader: new Ext.data.JsonReader({
            totalProperty: "d.TotalRows",
            root: "d.DataTable.Rows"
        },
            readerFields
        ),

        remoteSort: true,
        sortInfo: {
            field: sortOrderField,
            direction: "ASC"
        },
        groupField: grouperField
    }));
};
Ext.extend(ORION.WebServiceGroupStore, Ext.data.GroupingStore);

//Override Ext.grid.CheckboxSelectionModel behavior
Ext.grid.CheckboxSelectionModel.override({
    onHdMouseDown: function (e, t) {
        if (t.className == "x-grid3-hd-checker") {
            e.stopEvent();
            var hd = Ext.fly(t.parentNode);
            var isChecked = hd.hasClass("x-grid3-hd-checker-on");

            if (this.fireEvent("beforecheckallclick", this, isChecked) === false) {
                return;
            }

            if (isChecked) {
                hd.removeClass("x-grid3-hd-checker-on");
                this.clearSelections();
            } else {
                hd.addClass("x-grid3-hd-checker-on");
                this.selectAll();
            }

            this.fireEvent("aftercheckallclick", this, !isChecked);
        }
    }
});


//Override Ext.grid.GroupingView for groupclick event support
Ext.override(Ext.grid.GroupingView, {
    renderUI: function () {
        Ext.grid.GroupingView.superclass.renderUI.call(this);

        //      Intercept mouse event handling to add our extra group header mouse events
        this.grid.processEvent = this.grid.processEvent.createInterceptor(this.processEvent, this);

        this.mainBody.on('mousedown', this.interceptMouse, this);

        if (this.enableGroupingMenu && this.hmenu) {
            this.hmenu.add('-', {
                itemId: 'groupBy',
                text: this.groupByText,
                handler: this.onGroupByClick,
                scope: this,
                iconCls: 'x-group-by-icon'
            });
            if (this.enableNoGroups) {
                this.hmenu.add({
                    itemId: 'showGroups',
                    text: this.showGroupsText,
                    checked: true,
                    checkHandler: this.onShowGroupsClick,
                    scope: this
                });
            }
            this.hmenu.on('beforeshow', this.beforeMenuShow, this);
        }
    },

    //  private
    //  Filter mouse events to select those which target a group header.
    //  Extract the group field's value from the header Id.
    processEvent: function (name, e) {
        var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
        if (hd) {
            var gf = this.getGroupField();
            var idPrefix = this.grid.getGridEl().id + '-gp-' + gf + '-';
            var groupValue = hd.id.substring(idPrefix.length);
            var groupValue = Ext.util.Format.htmlDecode(groupValue.substring(0, groupValue.lastIndexOf('-')));

            if (groupValue) {
                this.grid.fireEvent('group' + name, this.grid, gf, groupValue, e);
            }
        }
    }
});

Ext.namespace('SW');
Ext.namespace('SW.UDT');

SW.UDT.PortDiscoveryStatus = '';
SW.UDT.NoDevicesFoundMessage = '@{R=UDT.Strings;K=UDTWEBJS_AK1_66;E=js}';
SW.UDT.PrevSelection = '';
SW.UDT.NextButtonClientId = '';

SW.UDT.MonitoredPortList = [];

SW.UDT.PortSelectionFirstRun = true;

SW.UDT.MonitoredPorts = function () {

    return {
        updatePortCount: function () {
            var layout = $("#MonitoredPortCount");
            layout.html(String.format("@{R=UDT.Strings;K=UDTWEBJS_VB1_118;E=js}", "<span style=\"font-weight:bold;\">", SW.UDT.MonitoredPortList.length, "</span>"));
            //CheckLicenseStatus();
        }

    };

} ();

SW.UDT.PortFilter = function () {

    var activeStatus = false;
    var inactiveStatus = false;
    var trunk = false;
    var nonTrunk = false;
    var portExcluded = false;
    var portRange = '';
    var vlan = '';
    var portDescription = '';

    return {
        Apply: function () {
            activeStatus = $('#' + activeStatusClientId).is(':checked');
            inactiveStatus = $('#' + inactiveStatusClientId).is(':checked');
            trunk = $('#' + trunkClientId).is(':checked');
            nonTrunk = $('#' + nonTrunkClientId).is(':checked');
            portExcluded = $('#' + portExcludedClientId).is(':checked');
            if ($('#' + portRangeClientId).is(':checked')) {
                portRange = $('#' + portRangeTextClientId).val();
            } else {
                portRange = '';
            }
            if ($('#' + vlanClientId).is(':checked')) {
                vlan = $('#' + vlanTextClientId).val();
            } else {
                vlan = '';
            }
            if ($('#' + portDescriptionClientId).is(':checked')) {
                portDescription = $('#' + portDescriptionTextClientId).val();
            } else {
                portDescription = '';
            }
            //Apply filter
            SW.UDT.PortDiscovery.ApplyFilterToGrid(activeStatus, inactiveStatus, trunk, nonTrunk, portExcluded, portRange, vlan, portDescription);
        },

        ActiveStatus: activeStatus,
        InactiveStatus: inactiveStatus,
        Trunk: trunk,
        NonTrunk: nonTrunk,
        PortExcluded: portExcluded,
        PortRange: portRange,
        VLAN: vlan,
        PortDescription: portDescription

    };
} ();


var grid;

SW.UDT.PortDiscovery = function () {

    ORION.prefix = "UDT_PortDiscovery_";

    // update the Ext grid size when window size is changed
    //$(window).resize(gridResize(grid));

    function gridResize() {
        grid.setWidth(1);
        grid.setWidth($('#DeviceGridCell').width());
    }

    function renderDeviceIcon(value) {
        return '<img src="/Orion/images/StatusIcons/Small-' + value + '" />';
    };

    function renderPortStatus(value, metadata) {

        if (value == 'Up') {
            var at = '<img src="/Orion/UDT/Images/Status/icon_port_active_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_AK1_82;E=js}" />';
        }
        else {
            var at = '<img src="/Orion/UDT/Images/Status/icon_port_inactive_dot.gif" title="@{R=UDT.Strings;K=UDTWEBJS_AK1_83;E=js}" />';
        }
        return at;
    };

    function rendererTrunkMode(value) {
        switch (value) {
            case 'Unknown':
                return '@{R=UDT.Strings;K=UDTWEBJS_VB1_130;E=js}';
            case 'Trunking':
                return '@{R=UDT.Strings;K=UDTWEBJS_VB1_131;E=js}';
            case 'NonTrunking':
                return '@{R=UDT.Strings;K=UDTWEBJS_VB1_132;E=js}';
            default:
                return value;
        }
    }


    var selectorModel;
    var dataStore;

    var pageSizeNum;
    var groupField = 'NodeName';

    var portSelectionState = [];

    var statusText = new Ext.Toolbar.TextItem('');
    var selectAllPages = new Ext.form.Label({ id: 'selectAllPages', text: '', style: 'font-weight: bold; margin-left: 5px; margin-right: 5px' });

    function CheckMonitored() {

        if (SW.UDT.PrevSelection != '') {
            portSelectionState = [];
            var s = SW.UDT.PrevSelection.split(',');
            for (var i = 0; i < s.length; i++) {
                portSelectionState[s[i]] = true;
            }
            //clear SW.UDT.PrevSelection
            SW.UDT.PrevSelection = '';
        } else {
            //Are we first run?
            if (SW.UDT.PortSelectionFirstRun && !backFromResultsPage) {
                SW.UDT.PortSelectionFirstRun = false;
                //select all by default
                ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "SelectAllPorts", { grouper: groupField,
                    filterActiveStatus: false,
                    filterInactiveStatus: false,
                    filterTrunk: false,
                    filterNonTrunk: false,
                    filterPortExcluded: false,
                    filterPortRange: '',
                    filterVLAN: '',
                    filterPortDescription: ''
                }, function (portArray) {
                    SW.UDT.MonitoredPortList = portArray;
                    for (var i = 0; i < portArray.length; i++) {
                        portSelectionState[portArray[i]] = true;
                    }
                    loadPortSelectionState();
                    UpdateSelectAllLink();
                    //$('.grpCheckbox').attr('checked', 'checked');
                });
            }
        }
    }


    function GridOnLoad() {
        //Check Prev settings, and load into "portSelectionState"
        CheckMonitored();
        //Load grid selection based upon "portSelectionState"
        loadPortSelectionState();
        //Save grid selection back to "MonitoredPortList"
        savePortSelectionState();
        UpdateStatus();
        SW.UDT.MonitoredPorts.updatePortCount();
        CheckLicenseStatus();
    }

    function savePortSelectionState(selectionModel) {

        grid.store.each(function (rec) {
            var key = rec.data["NodeID"] + "|" + rec.data["PortIndex"];
            portSelectionState[key] = selectorModel.isSelected(rec);
            if (selectorModel.isSelected(rec)) {
                if (!SW.UDT.MonitoredPortList.contains(key)) {
                    SW.UDT.MonitoredPortList.push(key);
                }
            } else {
                if (SW.UDT.MonitoredPortList.contains(key)) {
                    var idx = SW.UDT.MonitoredPortList.indexOf(key);
                    SW.UDT.MonitoredPortList.splice(idx, 1);
                }
            }
        });

        $('#' + monitoredPortsClientId).val(SW.UDT.MonitoredPortList);
        SW.UDT.MonitoredPorts.updatePortCount();
        UpdateSelectAllLink();
    }

    Array.prototype.contains = function (element) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == element) {
                return true;
            }
        }
        return false;
    }

    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
          this[from] === elt)
                return from;
        }
        return -1;
    };

    function loadPortSelectionState(s) {

        //Disable event processing until finished
        selectorModel.suspendEvents(false);
        var sel = [];
        grid.store.each(function (rec) {
            var key = rec.data["NodeID"] + "|" + rec.data["PortIndex"];
            if (portSelectionState[key]) {
                sel.push(rec);
            }
        });
        selectorModel.selectRecords(sel);
        //Enable event processing again
        selectorModel.resumeEvents();

        $('#' + monitoredPortsClientId).val(SW.UDT.MonitoredPortList);
        SW.UDT.MonitoredPorts.updatePortCount();
    }

    function UpdateStatus() {
        Ext.fly(statusText.getEl()).update(SW.UDT.PortDiscoveryStatus);

    }

    function HideNextButton() {
        $('#' + SW.UDT.NextButtonClientId).hide();
    }

    var beforeCheckAllClickHandler = function (sm, cheked) {
        sm.lockSelect = true;
    }

    var afterCheckAllClickHandler = function (sm, cheked) {

        $('.grpCheckbox').checked = cheked;
        $('.grpCheckbox').attr('checked', cheked);
        sm.lockSelect = false;

    }

    function UpdateSelectAllLink() {

        activeStatus = $('#' + activeStatusClientId).is(':checked');
        inactiveStatus = $('#' + inactiveStatusClientId).is(':checked');
        trunk = $('#' + trunkClientId).is(':checked');
        nonTrunk = $('#' + nonTrunkClientId).is(':checked');
        portExcluded = $('#' + portExcludedClientId).is(':checked');
        if ($('#' + portRangeClientId).is(':checked')) {
            portRange = $('#' + portRangeTextClientId).val();
        } else {
            portRange = '';
        }
        if ($('#' + vlanClientId).is(':checked')) {
            vlan = $('#' + vlanTextClientId).val();
        } else {
            vlan = '';
        }
        if ($('#' + portDescriptionClientId).is(':checked')) {
            portDescription = $('#' + portDescriptionTextClientId).val();
        } else {
            portDescription = '';
        }

        //Show "Select All" panel
        ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "GetTotalPortCount", { grouper: groupField,
            filterActiveStatus: activeStatus,
            filterInactiveStatus: inactiveStatus,
            filterTrunk: trunk,
            filterNonTrunk: nonTrunk,
            filterPortExcluded: portExcluded,
            filterPortRange: portRange,
            filterVLAN: vlan,
            filterPortDescription: portDescription
        }, function (count) {
            if (SW.UDT.MonitoredPortList.length != count) {
                if (SW.UDT.MonitoredPortList.length != grid.store.getCount()) {
                    Ext.fly(selectAllPages.getEl()).update('&nbsp;');
                } else {
                    Ext.fly(selectAllPages.getEl()).update(String.format('{0}&nbsp;&nbsp;&nbsp;<a onclick="SW.UDT.PortDiscovery.SelectAllPorts();" style="cursor:pointer; font-weight:bold; text-decoration:underline;">{1}</a>', String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_68;E=js}', SW.UDT.MonitoredPortList.length), String.format('@{R=UDT.Strings;K=UDTWEBJS_AK1_69;E=js}', count)));
                    $('.grpCheckbox').attr('checked', 'checked');
                }
            } else {
                Ext.fly(selectAllPages.getEl()).update('@{R=UDT.Strings;K=UDTWEBJS_AK1_67;E=js}');
                $('.grpCheckbox').attr('checked', 'checked');
            }
            CheckLicenseStatus();
        });

    }

    return {
        ApplyFilterToGrid: function (activeStatus, inactiveStatus, trunk, nonTrunk, portExcluded, portRange, vlan, portDescription) {

            grid.getStore().proxy.conn.jsonData = { grouper: groupField,
                filterActiveStatus: activeStatus,
                filterInactiveStatus: inactiveStatus,
                filterTrunk: trunk,
                filterNonTrunk: nonTrunk,
                filterPortExcluded: portExcluded,
                filterPortRange: portRange,
                filterVLAN: vlan,
                filterPortDescription: portDescription
            };

            //Reload
            grid.getStore().load({ callback: function () {
                SW.UDT.MonitoredPortList = [];
                savePortSelectionState();
                loadPortSelectionState();
                UpdateSelectAllLink();
            }
            });

        },

        SelectAllPorts: function () {
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "SelectAllPorts", { grouper: groupField,
                filterActiveStatus: activeStatus,
                filterInactiveStatus: inactiveStatus,
                filterTrunk: trunk,
                filterNonTrunk: nonTrunk,
                filterPortExcluded: portExcluded,
                filterPortRange: portRange,
                filterVLAN: vlan,
                filterPortDescription: portDescription
            }, function (portArray) {
                SW.UDT.MonitoredPortList = portArray;
                for (var i = 0; i < portArray.length; i++) {
                    portSelectionState[portArray[i]] = true;
                }
                loadPortSelectionState();
                UpdateSelectAllLink();
            });
        },

        GetDiscoveryErrors: function () {

            var deviceCount = 0;
            //GetSelectedDeviceCount
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "GetSelectedDeviceCount", {}, function (count) {

                deviceCount = count;
                //GetDiscoveryErrors
                ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "GetDiscoveryErrors", {}, function (result) {
                    //{"d":["Device does not respond to interface table request","Device does not respond to interface table request"]}
                    if (result != null) {
                        var List = "<br/><ul style='list-style-type:disc;'>";
                        for (i = 0; i < result.length; i++) {
                            List = List + "<li>" + result[i] + "</li>";
                        }
                        List = List + "</ul>";
                        if (deviceCount == 0) {
                            // accounts for pure DC discovery
                        } else if (deviceCount == result.length) {
                            //error
                            $("#DiscoveryErrors").html(List);
                            $("#ErrorPanel").show();
                            SW.UDT.PortDiscoveryStatus = SW.UDT.NoDevicesFoundMessage;
                            UpdateStatus();
                            HideNextButton();

                        } else if (result.length > 0) {
                            //alert
                            $("#DiscoveryAlerts").html(List);
                            $("#AlertPanel").show();
                        } else {
                            //all good
                            if (ZeroPorts) {
                                $("#OkPanel").hide();
                            } else {
                                $("#OkPanel").show();
                            }

                        }
                    } else {
                        //all good
                        if (ZeroPorts) {
                            $("#OkPanel").hide();
                        } else {
                            $("#OkPanel").show();
                        }
                    }
                });
            });

        },

        applySelectAll: function (selModel, grid) {
            selModel.selectAll();
            try {
                // This is a hack but it works
                // @see Ext.grid.CheckboxSelectionModel.onHdMouseDown()
                var hdChecker = grid.getEl().child('.x-grid3-hd-checker');
                if (hdChecker) {
                    hdChecker.addClass('x-grid3-hd-checker-on');
                }
            } catch (e) {
                //Dashboard.log(e.message);
            }
        },

        init: function (suppressInitialLoad) {

            suppressInitialLoad = typeof (suppressInitialLoad) != 'undefined' ? suppressInitialLoad : false;

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '500'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[100], [200], [300], [500], [1000]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 70,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_81;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);
                    this.addField(selectAllPages);
                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.onClick("refresh");
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();

            selectorModel.on("selectionchange", savePortSelectionState, this, { buffer: 250 });

            dataStore = new ORION.WebServiceGroupStore(
                "/Orion/UDT/Services/Discovery.asmx/GetPorts",
                [
                { name: 'NodeID', mapping: 0 },
                { name: 'NodeName', mapping: 1, sortType: Ext.data.SortTypes.asUCString },
                { name: 'NodeIcon', mapping: 2 },
                { name: 'PortID', mapping: 3 },
                { name: 'PortStatus', mapping: 4 },
                { name: 'VLAN', mapping: 5 },
                { name: 'PortIndex', mapping: 6 },
                { name: 'PortName', mapping: 7 },
                { name: 'Description', mapping: 8 },
                { name: 'TrunkMode', mapping: 9 }
                ],
                "NodeID", groupField);

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    selectorModel,
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_103;E=js}', width: 150, hidden: true, sortable: true, dataIndex: 'NodeID' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_104;E=js}', width: 160, hidden: true, sortable: true, dataIndex: 'NodeName' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_22;E=js}', width: 100, sortable: true, dataIndex: 'PortName' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_106;E=js}', width: 100, sortable: true, dataIndex: 'PortStatus', renderer: renderPortStatus },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_107;E=js}', width: 100, sortable: true, dataIndex: 'VLAN' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_108;E=js}', width: 140, hidden: true, sortable: true, dataIndex: 'PortIndex' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_109;E=js}', width: 140, hidden: true, sortable: true, dataIndex: 'PortName' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_110;E=js}', width: 200, sortable: true, dataIndex: 'Description' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_111;E=js}', width: 140, hidden: true, sortable: true, dataIndex: 'TrunkMode', renderer: rendererTrunkMode}
                ],

                sm: selectorModel,
                autoScroll: true,

                // Add grouping view, with checkbox
                view: new Ext.grid.GroupingView({
                    forceFit: true,
                    groupTextTpl: '<img src="/Orion/images/StatusIcons/Small-EmptyIcon.gif" style="width:12px; height:16px;"/><input id="selectGroupCheckbox" class="grpCheckbox" type="checkbox"> <img style="vertical-align: middle;" src="/NetPerfMon/images/Vendors/{[values.rs[0].data["NodeIcon"]]}" /> {[values.rs[0].data["NodeName"]]}</input>',
                    startCollapsed: true,
                    enableGroupingMenu: false
                }),

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: SW.UDT.NoDevicesFoundMessage
                },

                //width: 700,
                height: 300,
                stripeRows: true,

                // top toolbar
                tbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '',
                    emptyMsg: SW.UDT.NoDevicesFoundMessage
                }),

                bbar: new Ext.StatusBar({
                    id: 'PortResultsStatusBar',
                    statusAlign: 'left',
                    items: [statusText]
                })


            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            // Hook groupclick event
            grid.on('groupclick', function (grid, field, value, e) {
                var t = e.getTarget('.grpCheckbox');
                if (t) {
                    var checked = t.checked;
                    grid.getStore().each(function (rec, index) {
                        if (rec.get(field) == value) {
                            if (checked) {
                                grid.getSelectionModel().selectRow(index, true);
                            }
                            else {
                                grid.getSelectionModel().deselectRow(index);
                            }
                        }
                    });
                }
            });

            // Render grid
            grid.render('DeviceGrid');

            grid.store.on('load', GridOnLoad, grid);

            grid.getSelectionModel().on("beforecheckallclick", beforeCheckAllClickHandler);
            grid.getSelectionModel().on("aftercheckallclick", afterCheckAllClickHandler);

            // add pager
            grid.topToolbar.addPageSizer();

            // resize the grid
            gridResize();

            // load the grid with data
            grid.store.proxy.conn.jsonData = { grouper: groupField,
                filterActiveStatus: SW.UDT.PortFilter.ActiveStatus,
                filterInactiveStatus: SW.UDT.PortFilter.InactiveStatus,
                filterTrunk: SW.UDT.PortFilter.Trunk,
                filterNonTrunk: SW.UDT.PortFilter.NonTrunk,
                filterPortExcluded: SW.UDT.PortFilter.PortExcluded,
                filterPortRange: SW.UDT.PortFilter.PortRange,
                filterVLAN: SW.UDT.PortFilter.VLAN,
                filterPortDescription: SW.UDT.PortFilter.PortDescription
            };

            if (!suppressInitialLoad) grid.store.load();

            $("form").submit(function () { return false; });
        }
    };

} ();

Ext.onReady(SW.UDT.MonitoredPorts.updatePortCount, SW.UDT.MonitoredPorts);
