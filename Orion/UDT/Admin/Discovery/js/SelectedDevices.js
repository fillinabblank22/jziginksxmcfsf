﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

SW.UDT.SelectedDeviceList = [];
SW.UDT.SelectedDevices = function () {

    return {
        init: function () {
            var layout = $("#SelectedDevicesCount");
            layout.html(String.format("@{R=UDT.Strings;K=UDTWEBJS_VB1_116;E=js}", SW.UDT.SelectedDeviceList.length, "<span style=\"font-weight:bold;\">", "</span>"));
        }
    };

} ();

Ext.onReady(SW.UDT.SelectedDevices.init, SW.UDT.SelectedDevices);