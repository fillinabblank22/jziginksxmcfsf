﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

SW.UDT.DiscoverPorts = function () {

    return {
        init: function () {
            // call webservice to get selected device count
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "GetSelectedDeviceCount", {}, function (result) {
                //Do nothing just yet
            });
        }
    };

} ();

Ext.onReady(SW.UDT.DiscoverPorts.init, SW.UDT.DiscoverPorts);