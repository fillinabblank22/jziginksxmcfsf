﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

SW.UDT.WatchList = function () {

    ORION.prefix = "UDT_WatchList_";

    // update the Ext grid size when window size is changed
    $(window).resize(gridResize);

    function gridResize() {
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth($('#gridCell').width());
    }

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };


    UpdateToolbarButtons = function () {

        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;
        var total = grid.store.getCount();

        map.EditButton.setDisabled(count != 1);
        map.DeleteButton.setDisabled(count === 0);
        map.ExportButton.setDisabled(total === 0);

        // The buttons are disabled in demo mode
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
            map.ExportButton.setDisabled(true);
        }

    };

    // render the watch list type icon
    function renderTypeIcon(value) {
        if (value == null) {
            var ai = '<img src="/Orion/UDT/Images/icon_user_active_dot.gif" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_35;E=js}" />';
        }
        else {
            var ai = '<img src="/Orion/UDT/Images/icon_user_inactive_dot.gif" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_36;E=js}" />';
        }
        return ai;
    };

    // rendering applying highlighting to search text matches
    function renderHighlight(value, meta, record) {
        if (value == null) return value;

        var cellText = value;
        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(cellText) + '</span>'; }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = cellText;

        pattern = new RegExp(x, "gi");

        //NOTE: we need to add literal SPAN 'markers' because otherwise the actual HTML span tags would get encoded as literal
        replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex replace
        cellText = content.replace(pattern, replaceWith);

        //ensure highlighted content gets encoded
        cellText = encodeHTML(cellText);

        // now replace the literal SPAN markers with the HTML span tags
        cellText = cellText.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        cellText = cellText.replace(/{SPAN-END-MARKER}/g, '</span>');

        return cellText;
    };

    function renderMacAddress(value, meta, record) {

        var watchItemType = record.data.WatchItemType;

        if (value == null) return value;
        var formatted = formatMac(value);
        if (watchItemType == 1) {
            return "<B>" + renderHighlight(formatted, meta, record) + "</B>";
        } else {
            return renderHighlight(formatted, meta, record);
        }

    }

    function formatMac(value) {
        var regex = /(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})/;
        var formatted = value.replace(regex, "$1:$2:$3:$4:$5:$6");
        return formatted;
    }

    function renderIPAddress(value, meta, record) {

        var watchItemType = record.data.WatchItemType;

        if (value == null) return value;
        if (watchItemType == 2) {
            return "<B>" + renderHighlight(value, meta, record) + "</B>";
        } else {
            return renderHighlight(value, meta, record);
        }

    }

    function renderHostname(value, meta, record) {

        var watchItemType = record.data.WatchItemType;

        if (value == null) return value;
        if (watchItemType == 3) {
            return "<B>" + renderHighlight(value, meta, record) + "</B>";
        } else {
            return renderHighlight(value, meta, record);
        }

    }
    function renderUsername(value, meta, record) {

        var watchItemType = record.data.WatchItemType;

        if (value == null) return value;
        if (watchItemType == 4) {
            return "<B>" + renderHighlight(value, meta, record) + "</B>";
        } else {
            return renderHighlight(value, meta, record);
        }

    }

    function ValidateWatch(watchItemType, watchItemValue) {
        switch (watchItemType) {
            case 1:
                return ValidateMACAddress($("#MACAddress").val());
                break;
            case 2:
                return ValidateIPAddress($("#IPAddress").val());
                break;
            case 3:
                return ValidateHostname($("#Hostname").val());
                break;
            case 4:
                return ValidateUsername($("#UserName").val());
                break;
            default:
                return "";
        }
    }

    function ValidateMACAddress(value) {
        regex = /(^([0-9a-f]{4}\.[0-9a-f]{4}\.[0-9a-f]{4})$)|(^([0-9a-f]{12})$)|(^([0-9a-f]{2}\:[0-9a-f]{2}\:[0-9a-f]{2}\:[0-9a-f]{2}\:[0-9a-f]{2}\:[0-9a-f]{2})$)|(^([0-9a-f]{2}\-[0-9a-f]{2}\-[0-9a-f]{2}\-[0-9a-f]{2}\-[0-9a-f]{2}\-[0-9a-f]{2})$)/i;
        if (regex.test(value)) {
            return "";
        }
        else {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_2;E=js}";
        }
    }

    function StripMacFormatting(value) {
        return value.replace(/[\.\:-]/g, '');
    }

    function DecompressIPv6(ip) {

    }

    function ValidateIPAddress(value) {
        var IPv4Pattern = /(^((25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3})$)/;
        var IPv6Pattern = /(^((?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4})$)/;
        var IPv6Pattern_HEXCompressed = /(^(((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?))$)/;
        if (IPv4Pattern.test(value)) { return ""; }
        if (IPv6Pattern.test(value)) { return ""; }
        if (IPv6Pattern_HEXCompressed.test(value)) { return ""; }
        return "@{R=UDT.Strings;K=UDTWEBJS_VB1_3;E=js}";
    }

    function ValidateHostname(value) {
        var regexSpaces = /[\s]/;
        var regexEmpty = /^$/;
        if (regexSpaces.test(value)) {
            return "@{R=UDT.Strings;K=UDTWEBJS_MK0_AlertCheckForSpaces;E=js}";
        }
        if (regexEmpty.test(value)) {
            return "@{R=UDT.Strings;K=UDTWEBJS_MK0_AlertEmptyField;E=js}";
        }
        return "";
    }
    
    function ValidateUsername(value) {
        if (value != "") return "";
        return "@{R=UDT.Strings;K=UDTWEBJS_MR1_4;E=js}";
    }

    var _watchID;
    var addEditDialog;
    var delDialog;

    editWatch = function () {

        var watchItemType;
        var watchItem;
        var watchName;
        var watchDescription;
        var watchID;
        var addOptions = document.getElementsByName('addOptions');
        var rec = grid.getSelectionModel().getSelected();

        watchItemType = rec.data["WatchItemType"];
        watchName = rec.data["WatchName"];
        watchID = rec.data["WatchID"];
        $("#Name").val(watchName);
        watchDescription = rec.data["Note"];
        $("#Description").val(watchDescription);
        $("#MACAddress").val("");
        $("#IPAddress").val("");
        $("#Hostname").val("");
        $("#UserName").val("");
        switch (watchItemType) {
            case '1':
                watchItem = rec.data["MACAddress"];
                $("#MACAddress").val(formatMac(watchItem));
                addOptions[0].checked = true;
                break;
            case '2':
                watchItem = rec.data["IPAddress"];
                $("#IPAddress").val(watchItem);
                addOptions[1].checked = true;
                break;
            case '3':
                watchItem = rec.data["DNSName"];
                $("#Hostname").val(watchItem);
                addOptions[2].checked = true;
                break;
            case '4':
                watchItem = rec.data["UserName"];
                $("#UserName").val(watchItem);
                addOptions[3].checked = true;
                break;
        }

        addDialogUI();

        _watchID = watchID;

        if (!addEditDialog) {
            addEditDialog = new Ext.Window({
                applyTo: 'addDialog',
                layout: 'fit',
                width: 355,
                height: 380,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons:
                [
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                    handler: function (me) {

                        EditWatchServiceCall();

                    }
                },
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addEditDialog.hide();
                    }
                }
                ]
            });
        } else {
            addEditDialog.buttons[0].handler = EditWatchServiceCall;
        }


        addEditDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_7;E=js}");

        // Set the location
        addEditDialog.alignTo(document.body, "c-c");
        addEditDialog.show();
    }

    addNewWatch = function () {

        $("#MACAddress").val("");
        $("#IPAddress").val("");
        $("#Hostname").val("");
        $("#Name").val("");
        $("#Description").val("");
        $("#UserName").val("");

        if (!addEditDialog) {
            addEditDialog = new Ext.Window({
                applyTo: 'addDialog',
                layout: 'fit',
                width: 380,
                height: 410,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'addDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons:
                [
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                    handler: function (me) {
                        AddWatchServiceCall();
                    }
                },
                {
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addEditDialog.hide();
                    }
                }
                ]
            });
        } else {
            addEditDialog.buttons[0].handler = AddWatchServiceCall;
        }

        addEditDialog.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_8;E=js}");

        // Set the location
        addEditDialog.alignTo(document.body, "c-c");
        addEditDialog.show();

    };

    function AddWatchServiceCall() {

        var addOptions = document.getElementsByName('addOptions');
        var _watchItemType;
        var _watchItemValue;
        if (addOptions[0].checked) { _watchItemType = 1; _watchItemValue = $("#MACAddress").val(); }
        if (addOptions[1].checked) { _watchItemType = 2; _watchItemValue = $("#IPAddress").val(); }
        if (addOptions[2].checked) { _watchItemType = 3; _watchItemValue = $("#Hostname").val(); }
        if (addOptions[3].checked) {
            _watchItemType = 4;
            var userName = $("#UserName").val();
            var matchedString = userName.match(/^\b([A-Z0-9.-_]+)@([A-Z0-9.-_]+)$/i);
            if (matchedString  != null) {
                userName = matchedString[2] + "\\" + matchedString[1];
                _watchItemValue = userName;
            }
            else {
                _watchItemValue = $("#UserName").val();
            }
        }

        var _watchName = $("#Name").val();
        var _watchNote = $("#Description").val();

        message = ValidateWatch(_watchItemType, _watchItemValue);
        if (message != "") {
            Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_9;E=js}', message);
        } else {
            //Strip any and all formatting from MAC address (stored without any format)
            if (_watchItemType == 1) {
                _watchItemValue = StripMacFormatting(_watchItemValue);
            }
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "AddDeviceWatchList", { watchItemType: _watchItemType, watchItemValue: _watchItemValue, watchName: _watchName, watchNote: _watchNote }, function (result) {
                if (result != "") {
                    //some error
                    Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_10;E=js}', result);
                }
                grid.getStore().load();
                addEditDialog.hide();
            });
        }

    }

    function EditWatchServiceCall() {
        var addOptions = document.getElementsByName('addOptions');
        var _watchItemType;
        var _watchItemValue;
        if (addOptions[0].checked) { _watchItemType = 1; _watchItemValue = $("#MACAddress").val(); }
        if (addOptions[1].checked) { _watchItemType = 2; _watchItemValue = $("#IPAddress").val(); }
        if (addOptions[2].checked) { _watchItemType = 3; _watchItemValue = $("#Hostname").val(); }
        if (addOptions[3].checked) { _watchItemType = 4; _watchItemValue = $("#UserName").val(); }
        var _watchName = $("#Name").val();
        var _watchNote = $("#Description").val();

        message = ValidateWatch(_watchItemType, _watchItemValue);
        if (message != "") {
            Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_9;E=js}', message);
        } else {
            //Strip any and all formatting from MAC address (stored without any format)
            if (_watchItemType == 1) {
                _watchItemValue = StripMacFormatting(_watchItemValue);
            }
            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "EditDeviceWatchList", { watchId: _watchID, watchItemType: _watchItemType, watchItemValue: _watchItemValue, watchName: _watchName, watchNote: _watchNote }, function (result) {
                if (result != "") {
                    //some error
                    Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_11;E=js}', result);
                }
                grid.getStore().load();
                addEditDialog.hide();
            });
        }
    }

    function delWatch() {
        // get WatchListType, WatchListName of selected records in the grid
        var watchList = [];
        grid.getSelectionModel().each(function (rec) {
            watchList.push({ watchItemType: rec.data["WatchItemType"], watchName: rec.data["WatchName"], MAC: rec.data["MACAddress"], IP: rec.data["IPAddress"], DNS: rec.data["DNSName"], USER: rec.data["UserName"] });
        });

        var layout = $("#delDialogDescription");

        if (watchList.length > 5) {
            layout.text(String.format('@{R=UDT.Strings;K=UDTWEBJS_VB1_12;E=js}', watchList.length));
            $("#watches").empty();
        }
        else {
            layout.text('@{R=UDT.Strings;K=UDTWEBJS_VB1_13;E=js}');
            var watches = $("#watches");
            watches.empty();
            $(watchList).each(function () {
                var watchName = encodeHTML((new String(this.watchName)).substring(0, 32));
                switch (this.watchItemType) {
                    case '1':
                        var watchItem = formatMac(this.MAC);
                        break;
                    case '2':
                        var watchItem = this.IP;
                        break;
                    case '3':
                        var watchItem = this.DNS;
                        break;
                    case '4':
                        var watchItem = this.USER;
                        break;
                }
                $(String.format('<li>@{R=UDT.Strings;K=UDTWEBJS_VB1_14;E=js}</li>', watchName, watchItem)).appendTo(watches);
            });
        }

        if (!delDialog) {
            delDialog = new Ext.Window({
                applyTo: 'delDialog',
                layout: 'fit',
                width: 310,
                height: 205,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'delDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
    {
        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js}',
        handler: function () {

            var curPageRecordCount = grid.store.getCount();
            var selectedRecordCount = grid.getSelectionModel().getCount();

            var _watchListIds = [];
            grid.getSelectionModel().each(function (rec) {
                _watchListIds.push(rec.data["WatchID"]);
            });

            ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "DeleteWatchList", { watchListIds: _watchListIds }, function (result) {

                // Note: .reload() only reloads the current page.  If deleting the last records from the current page, force a load of 
                // all data using .load() to prevent paging from going out of whack.  
                if (curPageRecordCount > selectedRecordCount) {
                    grid.getStore().reload();
                }
                else {
                    grid.getStore().load();
                }
            });

            delDialog.hide();
        }
    },
    {
        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
        style: 'margin-left: 5px;',
        handler: function () {
            delDialog.hide();
        }
    }]
            });
        }

        // Set the location
        delDialog.alignTo(document.body, "c-c");
        delDialog.show();

        return false;
    };

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    function guid() {
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    }

    var csvExport;
    var progressTimer;
    var downloadGuid;
    var progress;
    var progressUpdateDelay = 500;

    exportAsFile = function () {

        $("#exportProgress").empty();
        progress = new Ext.ProgressBar({
            text: '0%',
            width: 294
        });
        progress.render(document.getElementById("exportProgress"));

        if (!csvExport) {
            csvExport = new Ext.Window({
                applyTo: 'csvExport',
                width: 310,
                height: 55,
                closable: false,
                resizable: false,
                modal: true,
                items: new Ext.BoxComponent({
                    applyTo: 'csvExportBody',
                    layout: 'fit',
                    border: false
                })
            });
        }

        csvExport.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_16;E=js}");

        // Set the location
        csvExport.alignTo(document.body, "c-c");
        csvExport.show();

        //Get new download id
        downloadGuid = guid();

        // start export to CSV call
        ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "ExportToCSV", { downloadId: downloadGuid }, function (result) {
            //set check progress timer
            progressTimer = setTimeout("ExportProgress()", progressUpdateDelay);
        });

    }

    launchDownload = function (downloadGuid) {
        $("#csvExport").append("<iframe src='/Orion/UDT/ExportToCSV.ashx?DownloadId=" + downloadGuid + "&DownloadName=DeviceTrackerWatchList' style='display: none;' ></iframe>");
        csvExport.hide();
    }

    ExportProgress = function () {

        ORION.callWebService("/Orion/UDT/Services/Discovery.asmx", "GetExportProgress", { downloadId: downloadGuid }, function (result) {
            if (result == "-1") { //error
                clearTimeout(progressTimer);
                csvExport.hide();
                Ext.Msg.alert('@{R=UDT.Strings;K=UDTWEBJS_VB1_17;E=js}', "@{R=UDT.Strings;K=UDTWEBJS_VB1_18;E=js}");
            } else {
                if (result == "100") {
                    csvExport.setTitle("@{R=UDT.Strings;K=UDTWEBJS_VB1_19;E=js}");
                    progress.updateProgress(1, '100%');
                    csvExport.doLayout();
                    clearTimeout(progressTimer);
                    //Wait a second, then launchDownload - no particular reason, just looks better
                    setTimeout("launchDownload('" + downloadGuid + "')", 1000);
                } else {
                    progress.updateProgress(result / 100, result + "%");
                    progressTimer = setTimeout("ExportProgress()", progressUpdateDelay);
                }
            }
        });
    }


    addDialogUI = function () {
        var addOptions = document.getElementsByName('addOptions');
        if (addOptions[0].checked) {
            $("#MACAddress").attr('disabled', false);
            $("#IPAddress").attr('disabled', true);
            $("#Hostname").attr('disabled', true);
            $("#UserName").attr('disabled', true);
        } else {
            if (addOptions[1].checked) {
                $("#MACAddress").attr('disabled', true);
                $("#IPAddress").attr('disabled', false);
                $("#Hostname").attr('disabled', true);
                $("#UserName").attr('disabled', true);
            } else if (addOptions[2].checked) {
                $("#MACAddress").attr('disabled', true);
                $("#IPAddress").attr('disabled', true);
                $("#Hostname").attr('disabled', false);
                $("#UserName").attr('disabled', true);
            }
            else {
                $("#MACAddress").attr('disabled', true);
                $("#IPAddress").attr('disabled', true);
                $("#Hostname").attr('disabled', true);
                $("#UserName").attr('disabled', false);
            }
        }
    }

    updateAll = function () {
        grid.store.load();
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var radioItemChecked;

    return {

        init: function () {

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        id: 'pagingStore',
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_34;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        //this.onClick("refresh");
                        this.doRefresh();
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            // UDT_WatchList.WatchItemType
            // UDT_WatchList.WatchName
            // UDT_Endpoint.MACAddress
            // UDT_IPAddress.IPAddress
            // UDT_DNSName.DNSName
            // UDT_WatchList.Note
            // UDT_PortToEndpoint.LastSeen  

            dataStore = new ORION.WebServiceStore(
                "/Orion/UDT/Services/Discovery.asmx/GetWatchList",
                    [
                    { name: 'WatchID', mapping: 0 },
                    { name: 'WatchItemType', mapping: 1 },
                    { name: 'WatchName', mapping: 2 },
                    { name: 'MACAddress', mapping: 3 },
                    { name: 'IPAddress', mapping: 4 },
                    { name: 'DNSName', mapping: 5 },
                    { name: 'Note', mapping: 6 },
                    { name: 'LastSeen', mapping: 8 },
                    { name: 'UserName', mapping: 9 }
                    ],
                    "WatchName", "ASC");

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    selectorModel,
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_20;E=js}', width: 20, sortable: true, hidden: true, dataIndex: 'WatchID' },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_21;E=js}', width: 20, sortable: true, hidden: true, dataIndex: 'WatchItemType' },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_22;E=js}', width: 150, sortable: true, dataIndex: 'WatchName', renderer: renderHighlight },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_23;E=js}', width: 150, sortable: true, dataIndex: 'MACAddress', renderer: renderMacAddress },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_24;E=js}', width: 280, sortable: true, dataIndex: 'IPAddress', renderer: renderIPAddress },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_25;E=js}', width: 150, sortable: true, dataIndex: 'DNSName', renderer: renderHostname },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MR1_1;E=js}', width: 150, sortable: true, dataIndex: 'UserName', renderer: renderUsername },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_26;E=js}', width: 300, sortable: true, dataIndex: 'Note', renderer: renderHighlight },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_27;E=js}', width: 180, sortable: true, dataIndex: 'LastSeen', renderer: renderHighlight }
                ],

                sm: selectorModel,

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}'
                },

                height: 350,
                stripeRows: true,

                // top toobar on grid with buttons and search field
                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_29;E=js}',
                    iconCls: 'add',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        addNewWatch();
                    }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_30;E=js}',
                    iconCls: 'edit',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        editWatch();
                    }
                }, '-',
                {
                    id: 'UpdateAllButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_31;E=js}',
                    iconCls: 'updateAll',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        updateAll();
                    }
                }, '-',
                {
                    id: 'ExportButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_32;E=js}',
                    iconCls: 'export',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        exportAsFile();
                    }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_15;E=js}',
                    iconCls: 'del',
                    handler: function () {
                        //if (IsDemoMode()) return DemoModeMessage();
                        delWatch();
                    }
                }, ' ',
                '->',  // removed the right align
                ' ', ' ', ' ', ' ',
                // watch list search field and trigger buttons

                new SW.UDT.SearchField({
                    store: dataStore,
                    width: 260
                }), ' '

                ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar({
                    id: 'searchResultsPager',
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_VB1_33;E=js}',
                    emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.store.on('load', function () {
                UpdateToolbarButtons();
            });

            grid.render('Grid');


            grid.bottomToolbar.addPageSizer();

            // resize the grid
            gridResize();

            grid.store.proxy.conn.jsonData = { searchText: '', searchTextFormatStripped: '' };

            // load the grid with data
            grid.store.load();

            $("form").submit(function () { return false; });
        }
    };

} ();


Ext.EventManager.onDocumentReady(SW.UDT.WatchList.init, SW.UDT.WatchList, true);
