﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');
Ext.ns('Ext.ux');

SelectDevicesOnNextClick = function() {
    if (SW.UDT.SelectedDeviceList.length == 0) { Ext.Msg.show({ title: "@{R=UDT.Strings;K=UDTWEBJS_VB1_110;E=js}", msg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_111;E=js}", icon: Ext.MessageBox.ERROR, buttons: Ext.MessageBox.OK }); }
    return SW.UDT.SelectedDeviceList.length > 0;
};

// We need to subclass the NodeUI class to hide the icon.  When the icon is set directly
// (as opposed to using a css class) the "loading" spinner icon shows up behind the icon.
// We will simply hide our icon while we wait.
SW.UDT.SWTreeNodeUI = Ext.extend(Ext.tree.TreeNodeUI, {
    beforeLoad: function () {
        var icon = $(this.getIconEl());
        this.iconSrc = icon.attr('src');
        icon.attr('src', this.emptyIcon);
        SW.UDT.SWTreeNodeUI.superclass.beforeLoad.call(this);
    },

    afterLoad: function () {
        SW.UDT.SWTreeNodeUI.superclass.afterLoad.call(this);
        $(this.getIconEl()).attr('src', this.iconSrc);
    },

    onTextChange: function (node, text, oldText) {
        if (this.rendered) {
            this.textNode.innerHTML = node.isLeaf() ? text : this.generateGroupText(node, text);
        }
    },

    refreshText: function (node) {
        this.onTextChange(node, node.text, node.text);
    },

    renderElements: function (n, a, targetNode, bulkRender) {
        SW.UDT.SWTreeNodeUI.superclass.renderElements.call(this, n, a, targetNode, bulkRender);

        if (!n.isLeaf() && n.attributes.groupTreeSettings.allowMultipleSelections) {
            //$(this.getAnchor()).after(this._selectAllSpan(n));
            $(this.getIconEl()).before(this._checkboxAllNone(n));
        }
        this.onTextChange(n, n.text, n.text);
    },

    _checkboxAllNone: function (node) {

        var checkAllNone = String.format('<input type="checkbox" onclick="{1}(\'{0}\', this.checked);" />',
                        node.id, node.attributes.groupTreeSettings.selectAllCallbackFunctionName);

        return checkAllNone;
    },

    _selectAllSpan: function (node) {
        var _clickFunction = function (nodeId, isSelectAll, linkText) {
            var s = String.format('<a onclick="{3}(\'{0}\', {1}); return false;">{2}</a>',
                        nodeId, isSelectAll, linkText,
                        node.attributes.groupTreeSettings.selectAllCallbackFunctionName);
            return s;
        };

        var selectAllNone = String.format('<br><span class="UDT_selectAll">@{R=UDT.Strings;K=UDTWEBJS_VB1_112;E=js}</span>',
                                          _clickFunction(node.id, true, '@{R=UDT.Strings;K=UDTWEBJS_VB1_113;E=js}'),
                                          _clickFunction(node.id, false, '@{R=UDT.Strings;K=UDTWEBJS_VB1_114;E=js}'));

        return selectAllNone;
    },

    generateGroupText: function (node, text) {

        
        var gTS = node.attributes.groupTreeSettings;
        if (!gTS.allowMultipleSelections) { return text; }
        
        var countInfo = gTS.getInitialNodeCount(node);
        if (node.isLoaded()) {
            countInfo.selCount = node.getOwnerTree().getChecked('text', node).length;
        }
        
        
        var childCountText = gTS.getSelectedChildCountText(countInfo.nodeCount, countInfo.selCount);

        return String.format('{0} {1}', text, childCountText, this._selectAllSpan(node));
    }

});

SW.UDT.GroupTree = function (settings) {


    settings.onCheckStateChange = settings.onCheckStateChange || Ext.emptyFn;
    settings.isDefaultSelectedItem = settings.isDefaultSelectedItem || function () { return false; };
    var groupByDropDown = $(settings.groupByDropDown);

    var tree = new Ext.tree.TreePanel({
        el: settings.renderTo,
        animate: true,
        rootVisible: false,
        lines: false,
        trackMouseOver: false,
        height: settings.height,

        dataUrl: settings.dataUrl,
            
        tbar: new Ext.ux.StatusBar({
            id: 'GroupTreeStatusBar',
            statusAlign: 'left',
            items: ['&nbsp;&nbsp;&nbsp;&nbsp;', {
                xtype: 'checkbox',
                checked: settings.selectAllOnInit,
                id: 'GroupTreeCheckAll',
                listeners: {
                    check: onCheckAll
                }
            }

                ]
        }),

        root: {
            nodeType: 'async',
            text: 'HiddenRoot',
            draggable: false,
            id: '/'
        }
    });


    function onCheckAll(e) {
        tree.getRootNode().eachChild(function (child) {
            child.getUI().getEl().getElementsByTagName("input")[0].checked = e.checked;
            onCheckAllInGroup(child, e.checked);
        });
    }


    groupByDropDown.change(function () {
        tree.getRootNode().reload();
    });

    function onCheckIncludeRouters(e) {
        var checkState = e.checked;
        $('#' + routerCheckboxID).attr('checked', checkState);
    }


    tree.getLoader().on("beforeload", function (treeLoader, node) {

        // Pass the groupby setting to the webservice when we load nodes
        treeLoader.baseParams.groupby = groupByDropDown.val();
        treeLoader.baseParams.nodeFilter = settings.nodeFilter;

        node.on("beforechildrenrendered", function (parentNode) {
            parentNode.eachChild(function (node) {
                //FB #24335 If node name contains double quotes, it can't be selected in assign application wizard and test dialog
                if (node.id.indexOf('"')) {
                    node.id = node.id.replace(/"/gi, '&#34;');
                }
                if (node.isLeaf() && settings.allowMultipleSelections) {
                    node.attributes.checked = settings.isDefaultSelectedItem(node);
                    node.on("checkchange", onLeafCheckChange);
                }
                node.attributes.groupTreeSettings = settings;
            });
            parentNode.ui.onTextChange(parentNode, parentNode.text, parentNode.text);
        });
    });

    //tree.on("click", function (node, e) { alert("!"); });

    tree.getLoader().on("load", function (treeLoader, node) {
        if (node.parentNode == null) {
            // We just loaded the root.  Add Zebra stripes
            $('.x-tree-root-node li:nth-child(even)').addClass('UDT_ZebraStripe');

            if (settings.selectAllOnInit) {
                tree.getRootNode().eachChild(function (child) {
                    child.getUI().getEl().getElementsByTagName("input")[0].checked = true;
                    onCheckAllInGroup(child, true);
                });
            }

        }
    });

    tree.getSelectionModel().on("beforeselect", function (selectionModel, newNode, oldNode) {
        if (settings.allowMultipleSelections === false) {
            // Only change the selection if the node is a leaf
            return newNode.isLeaf();
        } else {
            // Don't show the selection
            return false;
        }
    });


    if ((settings.allowMultipleSelections === false) && (settings.selectionChangeCallback != null)) {
        tree.getSelectionModel().on("selectionchange", function (selectionModel, newNode) {
            eval(settings.selectionChangeCallback)(newNode.attributes.nodeId, newNode.attributes.text, newNode.attributes.ipAddress);
        });
    }


    function onLeafCheckChange(node, isChecked) {
        settings.onCheckStateChange([node], isChecked);
        var parent = node.parentNode;
        parent.ui.refreshText(parent);
    }

    function onCheckAllInGroup(groupNode, isChecked) {
        if (!groupNode.isLoaded()) {
            // Force the nodes to load...
            groupNode.reload(function () {
                groupNode.collapse(false, false);

                // We are loaded now, now do the check all (or none)
                onCheckAllInGroup(groupNode, isChecked);
            });
            return;
        }

        groupNode.eachChild(function (node) {
            checkNodeWithNoEvent(node, isChecked);
        });

        settings.onCheckStateChange(groupNode.childNodes, isChecked);
        groupNode.ui.refreshText(groupNode);
    }


    function checkNodeWithNoEvent(node, checkState) {
        node.suspendEvents();
        node.getUI().toggleCheck(checkState);
        node.resumeEvents();
    }


    tree.render();

    return {
        selectAllInGroup: function (groupNodeId, isChecked) {
            var groupNode = tree.getNodeById(groupNodeId);
            onCheckAllInGroup(groupNode, isChecked);
        }

    };
};

SW.UDT.HashTable = function (domStore) {

    var saveState = function () {
        saveElement.val(Ext.encode(store));
    };

    var loadState = function () {
        var v = saveElement.val();
        return (v.length == 0) ? {} : Ext.decode(v);
    };

    var that = this;
    var saveElement = $(domStore);
    var store = loadState();


    this.add = function (key) {
        store[key.attributes.nodeId] = key.attributes.text;
    };

    this.addRange = function (keys) {
        Ext.each(keys, function (n) { that.add(n); });
        saveState();
    };

    this.remove = function (key) {
        delete store[key.attributes.nodeId];
    };

    this.removeRange = function (keys) {
        Ext.each(keys, function (n) { that.remove(n); });
        saveState();
    };

    this.contains = function (key) {
        return (store[key.attributes.nodeId] !== undefined);
    };

    this.getKeys = function() {
        var keys = [];
        $.each(store, function(k, v) {
            keys.push(k);
        });

        return keys;
    };
};



SW.UDT.GroupCountCache = function (url) {
    var store = null;
    var dataUrl = url;

    this.reload = function (groupby, initialSelections) {

        var data = { groupby: groupby, items: Ext.encode(initialSelections) };
        $.ajax({
            async: false,
            type: "POST",
            url: dataUrl,
            data: data,
            dataType: "json",
            success: function (result) {
                store = result;
            }
        });

        var trash = 0;
    };

    this.getCount = function (groupName) {
        if (store && store[groupName]) {
            var i = store[groupName];
            return { nodeCount: Ext.num(i[0], 0), selCount: Ext.num(i[1], 0) };
        }
        return { nodeCount: 0, selCount: 0 };
    };
};

SW.UDT.SelectNodeTree = function (groupByDropDownClientId, selectedNodesClientId, allowMultipleSelections, selectionChangeCallback, cookieInfo, nodeFilter, selectAllOnInit) {
    var dataUrl = '/Orion/UDT/Services/NodeTree.aspx';
    var checkedNodes = new SW.UDT.HashTable(selectedNodesClientId);
    if (!nodeFilter) { nodeFilter = ""; }
    var groupCountCache = new SW.UDT.GroupCountCache(dataUrl + '?GroupCounts=1&nodeFilter=' + nodeFilter);

//        if ($.cookies.get("groupBy") === null) {
//            var expiryDate = new Date;
//            expiryDate.setDate(expiryDate.getDate() + 7);
//            $.cookies.set("groupBy", "Vendor", { expiresAt: expiryDate, path: cookieInfo.path, domain: cookieInfo.domain });
//        }

    $(groupByDropDownClientId).val('Vendor'); //$.cookies.get("groupBy"));

        $(groupByDropDownClientId).change(function () {
            var groupBy = $(groupByDropDownClientId).val();
            //$.cookies.set("groupBy", groupBy);
            groupCountCache.reload(groupBy, checkedNodes.getKeys());
        }).change();

    var nodeTree = new SW.UDT.GroupTree({
        groupByDropDown: groupByDropDownClientId,
        allowMultipleSelections: allowMultipleSelections,
        selectAllCallbackFunctionName: "SW.UDT.NodeTreeSelectAllCallback",
        dataUrl: dataUrl,
        renderTo: 'UDT_selectNodeTree',
        height: $('#UDT_selectNodeTree').css('height'),
        selectionChangeCallback: (selectionChangeCallback == '') ? null : selectionChangeCallback,
        nodeFilter: nodeFilter,
        selectAllOnInit: selectAllOnInit,
        onCheckStateChange: function (nodes, isChecked) {
            if (isChecked)
                checkedNodes.addRange(nodes);
            else
                checkedNodes.removeRange(nodes);

            var nodes = checkedNodes.getKeys();
            SW.UDT.SelectedDeviceList = nodes;
            SW.UDT.SelectedDevices.init();

        },

        isDefaultSelectedItem: function (node) {
            return checkedNodes.contains(node);
        },

        getInitialNodeCount: function (node) {
            // we need to determine how many children are initially selected for 
            // this node even though this node hasn't beed loaded yet.
            return groupCountCache.getCount(((node.id.charAt(0) === '/') ? node.id.slice(1) : node.id).replace('//', '/'));
        },

        getSelectedChildCountText: function (totalCount, checkedCount) {
            if (checkedCount > 0) {
                return String.format("<span class='nodeGroupInfo'>@{R=UDT.Strings;K=UDTWEBJS_VB1_115;E=js}</span>", checkedCount, totalCount);
            }
            return "";
        }

    });

    SW.UDT.NodeTreeSelectAllCallback = function (treeNodeId, isChecked) {
        nodeTree.selectAllInGroup(treeNodeId, isChecked);
    };

};

