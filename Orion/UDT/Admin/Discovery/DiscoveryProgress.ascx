﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryProgress.ascx.cs" 
    Inherits="Orion_UDT_Admin_Discovery_DiscoveryProgress" %>

    
<link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/ProgressIndicator.css" />    

<div class="ProgressIndicator" id="progressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>

<%--FB183017--%>
<script type="text/javascript">
    var childrenElements = document.getElementById("progressIndicator").children;
    var sumWidth = 0;
    for (var i = 0; i < childrenElements.length; i++) {
        sumWidth += childrenElements[i].offsetWidth;
    }
    document.getElementById("progressIndicator").style.minWidth = String.format('{0}px', sumWidth);
</script>