﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscoveryResults.aspx.cs" Inherits="Orion_UDT_Admin_Discovery_DiscoveryResults"
    MasterPageFile="~/Orion/UDT/Admin/Discovery/DiscoveryPage.master" Title="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_37 %>"%>
<%@ Import Namespace="SolarWinds.UDT.Common.Models" %>
<%@ Import Namespace="System.Linq" %>
    
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/UDT/Admin/Discovery/DiscoveryProgress.ascx" TagPrefix="orion" TagName="DiscoveryWorkflowProgress" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">

    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false"/>
    <style type="text/css">
    .ImportResults 
    {
        padding: 4px 0px 4px 3px;
        font-weight: normal;
        font-size: 9pt;
        color: #000000;
    }
    .ImportMessage 
    {
        vertical-align: middle;
    }
    </style>

</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionUDTPHDomainControllerDiscovery" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1><%=Page.Title%></h1>
            </td>
            <td>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    //<![CDATA[

            $(function () {
                $(".ExpanderLink + div").hide();
                $(".ExpanderLink").toggle(
            function () {
                $("img:first", this).attr("src", "/Orion/images/Button.Collapse.GIF");
                $(this).next().slideDown("fast");
            },
            function () {
                $("img:first", this).attr("src", "/Orion/images/Button.Expand.GIF");
                $(this).next().slideUp("fast");
            }
        )
            });
                    
    //]]>
    </script>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="GroupBoxAccounts" id="SelectTypeDiv" style="min-width: 480px;">
            <orion:DiscoveryWorkflowProgress ID="ProgressIndicator" runat="server" />
            <div id="ImportResults" style="padding: 10px 10px 5px 10px;">
            
                 <div style="padding-bottom:8px;">
                    <table width="100%">
                        <tr valign="top">
                            <td>
                                <h2><%= Resources.UDTWebContent.UDTWEBDATA_VB1_110 %></h2>
                                <br />

                                <asp:Panel ID="DCsImported" runat="server" CssClass="ImportResults" Visible="false">
                                <a href="#" class="ExpanderLink"><img src="/Orion/images/Button.Expand.GIF" alt="Expand" style="vertical-align: middle; padding-right: 5px;"/><img src="/Orion/images/Check.Green.gif" style="vertical-align:middle; padding-right: 5px;"/><asp:Label CssClass="ImportMessage" ID="DCsImportedMessage" runat="server" /></a>
		                        <div style="background-color:#e4f1f8; padding: 2px;">
                                    <asp:Repeater ID="DCsImportedList" runat="server">
                                        <HeaderTemplate>
                                            <ul class="ImportResults">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <li>
                                                    <%# Eval("Hostname").ToString() %> [<%# Eval("IPAddress").ToString() %>]
                                                </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                </asp:Panel>

                                <br /><br />
                                <%= Resources.UDTWebContent.UDTWEBDATA_VB1_111%>
                                <br /><br />
                                 » <a style="cursor:pointer" onclick="$('#progressDialog').dialog('open');"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_112 %></a>
                            </td>
                        </tr>
                    </table>
                </div>
                


                <table width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                   
                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right; padding-right:2px; padding-bottom:8px;">
                             <div runat="server" id="WaitingImage" style="display:none;">
                                <p style="vertical-align:middle;"><img style="vertical-align:middle;" src="../../../images/animated_loading_sm3_whbg.gif" /> <%= Resources.UDTWebContent.UDTWEBDATA_VB1_113 %></p>            
                            </div>
                        </td>
                    </tr>

                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right; padding-right:8px; padding-top:5px;">
                            <asp:Panel ID="DiscoveryButtonPanel" runat="server" style="margin-left: 15px;">
                                <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="btnBack" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="btnBack_Click" OnClientClick="" CausesValidation="false" />
                                
                                 <asp:Button runat="server" ID="btnFinish" style="display:none;" OnClick="btnFinish_Click" />
                                <orion:LocalizableButton id="btnFinishDisplay" runat="server" DisplayType="Secondary" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_114 %>" OnClientClick="this.disabled = true; $(waitingImageId).show();$(btnFinishId).click();" style="margin-left: 25px;"/>
                                </div>
                            </asp:Panel>

                        </td>
                    </tr>



                </table>             
            </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
           
                    var waitingImageId = '#<%=WaitingImage.ClientID %>';
                    var btnFinishId = '#<%= btnFinish.ClientID %>';

                    var timeoutHandle;

                    var ButtonsEnabled = false;

                    var ShowDialog = function() {
                        var encodedTitle = $("<div/>").text("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_16)%>").html();
                        $("#progressDialog").show().dialog({
                            width: 600,
                            height: 440,
                            modal: true,
                            overlay: { "background-color": "black", opacity: 0.4 },
                            title: encodedTitle
                        });

                        $(".ui-dialog-titlebar-close").unbind('click');
                        $(".ui-dialog-titlebar-close").bind('click', CancelImport);
                    };

                    var CallProcessResults = function() {
                        ShowDialog();
                        PageMethods.StartImport(OnProcessProcessed, OnError);
                    };

                    var CallGetProgress = function() {
                        PageMethods.GetImportProgress(OnResultsProcessed, OnError);
                    };

                    var OnProcessProcessed = function(result) {
                        if (result == 1) { //Started
                            CallGetProgress();
                        }

                        if (result == 2) { //NothingToImport
                            ButtonsEnabled = true;
                            $("#progressHeader").hide();
                            $(".Results").append("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_17)%>");
                            $("#CancelButton").hide();
                            setTimeout('FinishImport()', 2000);
                        }

                        if (result == 3) { //OldResults
                            alert('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_18)%>');
                            PageMethods.CancelImport(function(result) { window.location = result }, OnError);
                        }
                    };

                    var CancelImport = function() {
                        $("#progressDialog").dialog("close");
                    };

                    var FinishImport = function() {
                        $("#progressDialog").dialog("close");
                    };

                    var OnResultsProcessed = function(result) {
                        $(".Results").append(result.ResultText);

                        if (result.PercentComplete > -1) {
                            $("#percentComplete").html(result.PercentComplete);
                        }

                        var div = $(".Results").get(0);
                        div.scrollTop = div.scrollHeight;

                        if (!result.ImportIsDone) {
                            //schedule next progress checking 
                            timeoutHandle = setTimeout(CallGetProgress, 500);
                        } else {
                            clearTimeout(timeoutHandle);
                            ButtonsEnabled = true;
                            $("#progressHeader").hide();
                            //Wait a few second... then close dialog
                            $("#CancelButton").hide();
                            setTimeout('FinishImport()', 2500);
                            //FinishImport();
                        }
                    };

                    var LicenseCheck = function(error) {
                        //check for license exception
                        var rel = /SolarWindsLicensingException/ ;
                        if (error.get_message().search(rel) > -1) {
                            var message = "<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_19)%>";
                            alert(message);
                            window.location = "/Orion/UDT/Admin/Discovery/DiscoverPorts.aspx";
                            return false;
                        } else {
                            return true;
                        }
                    };

                    var OnError = function(error) {
                        //check for license exception
                        if (!LicenseCheck(error)) {
                            window.location = baseUrl;
                        } else {
                            var rel = /ApplicationException/ ;
                            if (error._exceptionType.search(rel) > -1)
                                alert(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_20)%>", error.get_message()));
                            else
                                alert(String.format("<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_21)%>", error.get_message(), error.get_stackTrace()));
                        }
                        ButtonsEnabled = true;
                    };

                    $(function () {
                        CallProcessResults();
                    });
                //]]>
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="progressDialog">
            <div id="progressContent">
                <div id="statusText">
                    <%= Resources.UDTWebContent.UDTWEBDATA_VB1_115%> <b><span id="percentComplete">0</span> %</b></div>
	            <div runat="server" id="ResultsDiv" class="Results" style="font-size:8pt; padding: 5px; line-height:16px; width:540px; height:270px; white-space: nowrap; overflow: auto;">
	            </div><br />
                <div style="text-align: right">
                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="CancelImport(); return false;"/>
                </div>
            </div>
        </div>
</asp:Content>


