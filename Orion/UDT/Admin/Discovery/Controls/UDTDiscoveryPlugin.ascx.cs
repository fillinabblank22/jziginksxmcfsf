﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.UDT.Web.DAL;
using SWUI = SolarWinds.Orion.Web.UI;

public partial class Orion_UDT_Admin_Discovery_Controls_UDTDiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected int PortCount { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            PortCount = UDTPortDAL.GetPortCount();
            if (PortCount == 0)
            {
                //FB45298 and FB50513
                //adding the class to this button tells the 'go to orion home' button not to hi-light
                //discoverNetworkButton.CssClass = "pluginButton highlightButton";
                //discoverNetworkButton.ImageUrl = "~/Orion/UDT/images/Admin/Discovery/discover_my_ports_hiLight.png";
                discoverNetworkButton.DisplayType = SWUI.ButtonType.Primary;
            }
        }
        catch (Exception ex) {
            _logger.ErrorFormat("Cannot get number of managed UDT port objects. Details: {0}",ex);
        }

        DataBind();
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        LocalizableButton localizableButton = sender as LocalizableButton;
        
        DiscoveryCentralHelper.Leave();

        Response.Redirect(localizableButton.CommandArgument);
    }
}