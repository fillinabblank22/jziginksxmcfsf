﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_UDT_Admin_Discovery_Controls_SelectDeviceTree : System.Web.UI.UserControl
{

    private string treeHeight = "250px";
    private string treeWidth = "600px";
    private string treeBorder = "1px solid #c3daf9";
    private string treeClass;
    private bool allowMultipleSelections = true;
    private string afterNodeSelectionChangeScript;

    private string viewNodeFilter;

    public string TreeHeight
    {
        get { return treeHeight; }
        set { treeHeight = value; }
    }

    public string TreeWidth
    {
        get { return treeWidth; }
        set { treeWidth = value; }
    }

    public string TreeBorder
    {
        get { return treeBorder; }
        set { treeBorder = value; }
    }

    public string TreeCssClass
    {
        get { return treeClass; }
        set { treeClass = value; }
    }

    public bool AllowMultipleSelections
    {
        get { return allowMultipleSelections; }
        set { allowMultipleSelections = value; }
    }

    public string NodeToSelectByDefault
    {
        get { return selectedNodes.Value; }
        set { selectedNodes.Value = value; }
    }

    public string ViewNodeFilter
    {
        protected get
        {
            if (String.IsNullOrEmpty(viewNodeFilter))
            {
                return "null";
            }
            return new JavaScriptSerializer().Serialize(viewNodeFilter);
        }
        set { viewNodeFilter = value; }
    }

    public string AfterNodeSelectionChangeScript
    {
        get { return afterNodeSelectionChangeScript; }
        set { afterNodeSelectionChangeScript = value; }
    }

    protected string CookieInfo
    {
        get { return string.Format("{0}path:'/sgn{1}{2}/',domain:'{3}'{4}", "{", this.Page.GetType().GetHashCode(), Profile.UserName.GetHashCode(), this.Request.Url.Host, "}"); }
    }

    protected void GroupByDropDown_OnInit(object sender, EventArgs e)
    {
        IList<string> customProperties = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetPropNamesForTable("Nodes", false);
        List<string> usableCustomProperties = new List<string>(customProperties.Count);
        foreach (string prop in customProperties)
        {
            Type fieldType = SolarWinds.Orion.Core.Common.CustomPropertyMgr.GetTypeForProp("Nodes", prop);
            if (fieldType != typeof(System.Text.StringBuilder))
                usableCustomProperties.Add(prop);
        }

        GroupByDropDown.Items.Clear();
        GroupByDropDown.Items.Add(new ListItem(Resources.UDTWebContent.UDTWCODE_VB1_11, "Vendor"));
        GroupByDropDown.Items.Add(new ListItem(Resources.UDTWebContent.UDTWCODE_VB1_12, "MachineType"));
        GroupByDropDown.Items.Add(new ListItem(Resources.UDTWebContent.UDTWCODE_VB1_13, "SNMPVersion"));

        //Add CustomProperties. to retrieve custom properties using SWIS DAL
        foreach (string customProperty in usableCustomProperties)
        {
            GroupByDropDown.Items.Add(new ListItem(customProperty,
                string.Format("CustomProperties.{0}", customProperty)));
        }
    }

    public List<string> GetSelectedNodeIds()
    {
        List<KeyValuePair<int, string>> nodeInfo = GetSelectedNodeInfo();
        List<string> result = new List<string>(nodeInfo.Count);

        foreach (KeyValuePair<int, string> pair in nodeInfo)
        {
            result.Add(pair.Key.ToString());
        }

        return result;
    }

    public bool GetRouterAutoDiscover()
    {
        return AutoDiscoverRouters.Checked;
    }

    public List<KeyValuePair<int, string>> GetSelectedNodeInfo()
    {
        string selectedNodesJson = selectedNodes.Value;
        List<KeyValuePair<int, string>> result = new List<KeyValuePair<int, string>>();

        if (string.IsNullOrEmpty(selectedNodesJson))
            return result;

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        Dictionary<string, string> selectedNodesLookup = serializer.Deserialize<Dictionary<string, string>>(selectedNodesJson);

        foreach (KeyValuePair<string, string> item in selectedNodesLookup)
        {
            int id;
            if (Int32.TryParse(item.Key, NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out id))
            {
                result.Add(new KeyValuePair<int, string>(id, item.Value));
            }
        }

        return result;
    }

}