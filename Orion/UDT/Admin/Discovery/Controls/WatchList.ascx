﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WatchList.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_WatchList" %>
<input type="hidden" name="UDT_WatchList_PageSize" id="UDT_WatchList_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_WatchList_PageSize")%>' />
<style type="text/css">
    .radiobutton_title
    {
        padding-left: 5px;
    }
    .leftColumnStyle {
        width: 50%;
    }
    .rightColumnStyle {
        width: 50%;
    }
    .helpText
    {
        font-size: 10pt;
       color: #646464;
    }
    
</style>

<div id="WatchList">
        <table class="GridTable" cellpadding="0" cellspacing="0" width="100%" >
	        <tr valign="top" align="left">
		        <td id="gridCell" style="padding-right:0px;">
                        <div id="Grid" style="visibility:visible; display:block;"/>
		        </td>
	        </tr>
        </table>
</div>

<div id="addDialog" class="x-hidden">
	<div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_4 %></div>
	<div id="addDialogBody" class="x-panel-body dialogBody" style="font-family: Arial;">
	<table style="padding:8px;" width="100%">
	    <tr>
	        <td colspan="2" style="padding-bottom:4px;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_5 %></td>
	    </tr>
	    <tr>
	        <td class="leftColumnStyle"><input type="radio" name="addOptions" checked onclick="addDialogUI()" value="0" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_6 %>"/><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_6 %></span></td>
            <td class="rightColumnStyle" style="padding-bottom:2px;"><input type="text" id="MACAddress" style="width:160px"/></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle"><input type="radio" name="addOptions" onclick="addDialogUI()" value="1" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_7 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_7 %></span></td>
            <td class="rightColumnStyle" style="padding-bottom:2px;"><input type="text" disabled id="IPAddress" style="width:160px" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle"><input type="radio" name="addOptions" onclick="addDialogUI()" value="2" title="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_8 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_8%></span></td>
            <td class="rightColumnStyle" style="padding-bottom:6px;"><input type="text" disabled id="Hostname" style="width:160px" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle"><input type="radio" name="addOptions" onclick="addDialogUI()" value="3" title="<%= Resources.UDTWebContent.UDTWEBDATA_MR1_1 %>" /><span class="radiobutton_title"><%= Resources.UDTWebContent.UDTWEBDATA_MR1_1%></span></td>
            <td class="rightColumnStyle" style="padding-bottom:1px;"><input type="text" disabled id="UserName" style="width:160px" /></td>
	    </tr>
        <tr>
            <td class="leftColumnStyle"></td>
            <td class="helpText"  style="padding-bottom:6px;"><%= Resources.UDTWebContent.UDTWEBDATA_MR1_3 %></td>
	    </tr>
        <tr>
	        <td colspan="2" style="padding-bottom:4px;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_9 %></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_10 %></td>
            <td class="rightColumnStyle" style="padding-bottom:2px;"><input type="text" id="Name" style="width:160px" /></td>
	    </tr>
        <tr>
	        <td class="leftColumnStyle" style="vertical-align:top;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_11%></td>
            <td class="rightColumnStyle" style="padding-bottom:2px;"><textarea id="Description" rows="5" cols="16" style="width:158px; height:70px;"></textarea></td>
	    </tr>
    </table>
	</div>
</div>

    <div id="delDialog" class="x-hidden">
	    <div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_12%></div>
	    <div id="delDialogBody" class="x-panel-body dialogBody">
	    <table>
	        <tr>
	            <td><div id="delDialogDescription"></div></td>
	        </tr>
	        <tr>
	            <td><ul id="watches"></ul></td>
	        </tr>
        </table>
	    </div>
	</div>

    <div id="csvExport" class="x-hidden">
	    <div class="x-window-header"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_13 %></div>
	    <div id="csvExportBody" class="x-panel-body dialogBody">
	        <div id="exportProgress" ></div>
            <div id="exportDownload" ></div>
	    </div>
	</div>