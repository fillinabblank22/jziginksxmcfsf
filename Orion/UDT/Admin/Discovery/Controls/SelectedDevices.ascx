﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectedDevices.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_SelectedDevices" %>

<orion:Include runat="server" Module="UDT" File="Admin/Discovery/js/SelectedDevices.js"/>
<%-- div that holds the selected device count --%>

<table style="width:300px;height:40px;float:right; background-color:#e4f1f8; background-image: url(../../Images/Admin/sidebar_bg_blue.gif); background-position: top; background-repeat: repeat-x;">
	<tr>
		<td align="center" valign="middle" style="width:30px;"> 
			<img src="/Orion/UDT/Images/Admin/Discovery/icon_lightbulb.gif" />
		</td>
		<td style="text-align:center;font-size:12pt;">
		    <span id="SelectedDevicesCount" />
		</td>
	</tr>	
</table>



