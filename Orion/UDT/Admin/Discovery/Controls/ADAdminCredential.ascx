﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ADAdminCredential.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_ADAdminCredential" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<style type="text/css">
    .CredBox 
    {
        width: 200px;
    }
    .ValidatorText 
    {
        font-size: 8pt;
        padding-bottom: 4px;

    }
    a.helpLink2 
    {
        color: #336699;
        font-size:8pt;
    }
    a.helpLink2:hover {
        color: orange !important;
    }
</style>

    <asp:Panel ID="CredEdit" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel" style = "max-width:900;" >
        <p class="DialogHeaderText">
            <asp:Label ID="ActionHeader" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_41 %>" Font-Bold="true"
                Font-Size="Larger" />
        </p>
        <asp:Panel ID="CredEditV1Controls" runat="server" BorderWidth="0" Visible="true"                        
            Class="EditControls">                    
            <table>
                <tr>
	                <td>
                        <asp:Label runat="server" ID="lblCredential" Text="<%$ Resources:UDTWebContent, UDTWEBDATA_VB1_42  %>"></asp:Label><br />
                        <asp:DropDownList ID="ddlADCredentials" runat="server" CssClass="CredBox" AutoPostBack="True" OnSelectedIndexChanged="ddlADCredentials_IndexChanged" >
                        </asp:DropDownList>
	                </td>
                </tr>

                <tr>
	                <td>
                        <asp:Label runat="server" ID="lblCredName" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_43 %>"></asp:Label><br />
                        <asp:TextBox ID="tbCaption" runat="server" CssClass="CredBox" MaxLength="50" ></asp:TextBox>
                        <div><asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" CssClass="ValidatorText" ControlToValidate="tbCaption" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_44 %>"></asp:RequiredFieldValidator></div>
	                </td>
                </tr>
                <tr>
	                <td>
                        <asp:Label runat="server" ID="lblLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_45 %>"></asp:Label><br />
                        <asp:TextBox ID="tbLogin" runat="server" CssClass="CredBox" MaxLength="50"></asp:TextBox>
                        <div><asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_46 %>" ></asp:RequiredFieldValidator></div>
	                </td>
                </tr>
                <tr>
                    <td class="modifiedLeftLabelColumn pollingSelector">
                        <div class="smallText">
                            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_47 %></div>
                        <div id="EditCredentialHelp">
                            <span class="LinkArrow">&#0187;</span>
                            <orion:HelpLink ID="UDTDCCredHelpLink" runat="server" HelpUrlFragment="OrionCoreAGEditingUserAccounts"
                                HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_48 %>" CssClass="helpLink" />
                        </div>
	                </td>	            
                </tr>
                <tr>
	                <td>
                        <asp:Label runat="server" ID="lblPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_49 %>"></asp:Label><br />
                        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" CssClass="CredBox" MaxLength="50" ></asp:TextBox>
                        <div><asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_50 %>"></asp:RequiredFieldValidator></div>
	                </td>
                </tr>
                <tr>
	                <td>
                        <asp:Label runat="server" ID="lblPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_51 %>"></asp:Label><br />
                        <asp:TextBox ID="tbPasswordConfirmation" runat="server" CssClass="CredBox" TextMode="Password" MaxLength="50" ></asp:TextBox>
                        <div><asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_52 %>"></asp:RequiredFieldValidator></div>
                        <div><asp:CompareValidator ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" CssClass="ValidatorText" ControlToCompare="tbPassword" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_53 %>"></asp:CompareValidator></div>
                        <br /><div style="margin-top:4px;"><orion:LocalizableButton id="TestCred" runat="server" DisplayType="Small" LocalizedText="Test" OnClick="TestCredentials_OnClick" CssClass="TestCredential" />
                            <span class="LinkArrow">&#0187;</span>
                            <orion:HelpLink ID="HelpLink1" runat="server" HelpUrlFragment="OrionUDTPHAddADCreds" 
                                HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_54 %>" CssClass="helpLink2"/>
                            
                        </div>
                
	                </td>
                </tr>
            </table>
            <div runat="server" id="ADCredValidationTrue" style="background-color: #D6F6C6;
                text-align: center" visible="false">
                <img runat="server" id="ADCredValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                <%= Resources.UDTWebContent.UDTWEBDATA_VB1_55 %>
            </div>
            <div runat="server" id="ADCredValidationFalse" style="background-color: #FEEE90;
                text-align: center; color: Red" visible="false">
                <img runat="server" id="ADCredValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                <asp:Label runat="server" ID="ADCredValidationFailedMessage" />
            </div>
        </asp:Panel>

        <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons">
            <div>
                <table>
                    <tr>
                        <td style="width:100px;">
                             <asp:UpdateProgress runat="server" ID="UpdateProgress" DynamicLayout="false" DisplayAfter="1000" AssociatedUpdatePanelID="ParentUpdatePanel">
                                <ProgressTemplate>
                                     <span class="ValidatorText"><img alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_57 %>" style="vertical-align: middle;" src="/Orion/images/loading_gen_small.gif" />&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_56 %></span>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </td>
                        <td>
                            <div class="sw-btn-bar">
                            <orion:LocalizableButton id="AddCred" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_58 %>"  OnClick="AddCredentials_OnClick" OnClientClick="" />
                            <orion:LocalizableButton id="CancelAddCred" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelCredential_OnClick" CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                </table>

             </div>
        </asp:Panel>
    </asp:Panel>

    <orion:TBox ID="modalityDiv" runat="server" />

   <div class="contentBlock">
        <table width="100%">
            <tr class="ButtonHeader">                    
                <td style="padding-left: 10px;">
                    <asp:LinkButton ID="AddLinkButton" runat="server" OnClick="AddBtn_OnClick" CssClass="iconLink" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_59 %>"/>
                </td>
            </tr>
            <tr>
            <td>
                <asp:GridView Width="100%" ID="CredentialsGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                    CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_63 %>">
                            <ItemTemplate>
                            <%#Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_64 %>">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>' Width="378" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_65 %>">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_60 %>" ImageUrl="~/Orion/Discovery/images/icon_up.gif"
                                    OnCommand="ActionItemCommand" CommandName="MoveUp" CommandArgument="<%#Container.DataItemIndex %>" />
                                <asp:ImageButton ID="MoveDownBtn" runat="server" ToolTip="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_61 %>" ImageUrl="~/Orion/Discovery/images/icon_down.gif"
                                    OnCommand="ActionItemCommand" CommandName="MoveDown" CommandArgument="<%#Container.DataItemIndex %>" />
                                <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_62 %>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                                    OnCommand="ActionItemCommand" CommandName="DeleteItem" CommandArgument="<%#Container.DataItemIndex %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
