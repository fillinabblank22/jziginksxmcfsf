﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UDTDiscoveryPlugin.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_UDTDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<style type="text/css">
    .coreDiscoveryIcon
    {
        float:left;
        padding-left: 20px;
    }
    .coreDiscoveryPluginBody
    {
        padding:0px 75px 5px 75px;
    }
    a.helpLink 
    {
        color: #336699;
        font-size:smaller;
    }
    
    a.helpLink:hover
    {
        color:Orange;
    }
</style>
<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/UDT/images/Admin/Discovery/port_icon.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_216 %></h2>
    <div>
        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_217 %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionUDTPHDiscoveryCentral"
        HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_218 %>" CssClass="helpLink" />
    <br />
    <orion:ManagedNetObjectsInfo ID="portsInfo" runat="server" EntityName="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_220 %>" NumberOfElements="<%# PortCount %>" />

    <orion:LocalizableButton ID="discoverNetworkButton" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_219 %>" OnClick="Button_Click"
     CommandArgument="~/Orion/Discovery/Default.aspx" runat="server" />
    <orion:LocalizableButton ID="AddPortstoOrionNodes" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MM_4 %>" OnClick="Button_Click"
     CommandArgument="~/Orion/UDT/Ports/Default.aspx" runat="server" />
</div>
