﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectDeviceTree.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_SelectDeviceTree" %>
<%@ Register TagPrefix="udt" TagName="SelectedDevices" Src="~/Orion/UDT/Admin/Discovery/Controls/SelectedDevices.ascx" %>

<style type="text/css">
	span.nodeGroupInfo { font-style: italic; }	
	.AutoRouters { display: none; }
	.AutoRouters.label{padding-left: 5px !important;}
</style>

<orion:Include runat="server" Module="UDT" File="Admin/Discovery/js/jquery.cookies.2.2.0.js"/>
<orion:Include runat="server" Module="UDT" File="Admin/Discovery/js/DeviceTree.js"/>
<script type="text/javascript">
   var routerCheckboxID='<%=AutoDiscoverRouters.ClientID %>';
    $(document).ready(function(){
		SW.UDT.SelectNodeTree(
			'#<%=GroupByDropDown.ClientID%>', 
			'#<%=selectedNodes.ClientID%>', 
			<%=this.AllowMultipleSelections.ToString().ToLowerInvariant() %>,
			'<%=this.AfterNodeSelectionChangeScript %>',
			<%=CookieInfo%>,
			<%=this.ViewNodeFilter%>,
            false //selectAllOnInit
		);

    });
</script>

<div style="margin-top: -47px;">
<%= Resources.UDTWebContent.UDTWEBDATA_VB1_72 %>
<br />
<table style="width:<%=this.TreeWidth %>"><tr>
    <td><asp:DropDownList ID="GroupByDropDown" runat="server" AutoPostBack="false" AppendDataBoundItems="true" OnInit="GroupByDropDown_OnInit"/></td>
    <td align="right"><asp:CheckBox ID="AutoDiscoverRouters" CssClass="AutoRouters" runat="server" Checked="true" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_71 %>" /></td>
</tr></table>
<br />
</div>
<div id="UDT_selectNodeTree" class="<%=this.TreeCssClass %>" style="position: relative; overflow:auto; height:<%=this.TreeHeight %>;width:<%=this.TreeWidth %>;border:<%=this.TreeBorder%>;"></div>
<input type="text" id="selectedNodes" runat="server" style="width:800px;display:none;" value=''/>

