﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UDTDCDiscoveryPlugin.ascx.cs" Inherits="Orion_UDT_Admin_Discovery_Controls_UDTDCDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<style type="text/css">
    .coreDiscoveryIcon
    {
        float:left;
        padding-left: 20px;
    }
    .coreDiscoveryPluginBody
    {
        padding:0px 75px 5px 75px;
    }
    a.helpLink 
    {
        color: #336699;
        font-size:smaller;
    }
    
    a.helpLink:hover
    {
        color:Orange;
    }
</style>
<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/UDT/images/Admin/Settings/detective_36x36.png"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= Resources.UDTWebContent.UDTWEBDATA_GK_2%></h2>
    <div>
        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_8%></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionUDTPHDomainControllerDiscovery"
        HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_GK_3 %>" CssClass="helpLink" />
    <br />
    <orion:ManagedNetObjectsInfo ID="DCInfo" runat="server" EntityName="<%$ Resources : UDTWebContent, UDTWEBDATA_GK_4 %>" NumberOfElements="<%# DcCount %>" />

    <orion:LocalizableButton ID="discoverNetworkButton" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_GK_5 %>" OnClick="Button_Click"
     CommandArgument="~/Orion/UDT/Admin/Discovery/Default.aspx" runat="server" />
    
</div>
