﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Services;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using System.Web.UI.WebControls;

public partial class Orion_UDT_Admin_Discovery_Controls_ADAdminCredential : System.Web.UI.UserControl
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    private const int NonExistingADCredentialID = 0;
    private List<UsernamePasswordCredential> adCredentials;

    public List<UsernamePasswordCredential> Credentials
    {
        get
        {
            return (List<UsernamePasswordCredential>)this.ViewState["ADCredentials"];
        }

        set
        {
            this.ViewState["ADCredentials"] = value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        InitCredentials();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // refill password for new credentials
        if ((CredentialsId == NonExistingADCredentialID)
        && ((string.IsNullOrEmpty(this.PlainPassword) && string.IsNullOrEmpty(this.Password)) || (this.PlainPassword != this.Password))
        && GetPassBlock(this.PlainPassword) != this.Password)
        {
            EncodePassword(this.Password);
        }

        ADCredValidationTrue.Visible = false;
        ADCredValidationFalse.Visible = false;

        if (!IsPostBack)
        {



            //this.Credentials = new List<UsernamePasswordCredential>();
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {

                //GET AD CREDS FROM BL (UDT.Settings.ADCreds (CredentialID) List).
                int[] adAdminCredIDs = bl.UserValidateAndGetADAdminCredentialIDs();
                if (adAdminCredIDs == null)
                {
                    this.Credentials = new List<UsernamePasswordCredential>();
                }
                else
                {
                    List<UsernamePasswordCredential> allCredentials = bl.UserGetAllCredentials(false).ToList();
                    Credentials = new List<UsernamePasswordCredential>();
                    foreach (int id in adAdminCredIDs)
                    {
                        Credentials.AddRange(allCredentials.Where(n => n.ID == id));
                    }
                }


                //UsernamePasswordCredential[] allCredentials = bl.RELGetAllCredentials();
                //this.Credentials = allCredentials.ToList();

            }
            BindData();
        }
    }

    // shows editor dialog for adding a new set
    protected void AddBtn_OnClick(object sender, EventArgs e)
    {
        ADCredValidationTrue.Visible = false;
        ADCredValidationFalse.Visible = false;

        CredentialsId = NonExistingADCredentialID;
        SetCredentialsFromDropDown();

        CredEdit.Visible = true;
        modalityDiv.ShowBlock = true;

    }

    // binds credentials data to gridView
    private void BindData()
    {
        this.CredentialsGrid.DataSource = this.Credentials;
        this.CredentialsGrid.DataBind();

        //Remove creds already in grid (already added)
        foreach (UsernamePasswordCredential cred in Credentials)
        {
            var credID = cred.ID;
            UsernamePasswordCredential item = adCredentials.SingleOrDefault(n => n.ID == credID);
            if (item != null)
            {
                adCredentials.Remove(item);
                CredentialsId = NonExistingADCredentialID;
            }
        }

        ddlADCredentials.DataBind();
    }


    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        int itemIndex = -1;

        if (e.CommandArgument != null)
            int.TryParse(e.CommandArgument.ToString(), out itemIndex);

        switch (e.CommandName)
        {
            case "MoveUp":
                MoveItemUp(itemIndex);
                break;
            case "MoveDown":
                MoveItemDown(itemIndex);
                break;
            case "DeleteItem":
                DeleteItem(itemIndex);
                break;
        }

    }


    // delete selected credentials
    protected void DeleteItem(int selectedIndex)
    {
        if (selectedIndex >= 0)
        {
            Credentials.RemoveAt(selectedIndex);
            BindData();
        }
    }

    // move credentials up
    protected void MoveItemUp(int selectedIndex)
    {
        if (selectedIndex > 0)
        {
            Credentials.Reverse(selectedIndex - 1, 2);
            BindData();
        }
    }

    // move credentials down
    protected void MoveItemDown(int selectedIndex)
    {
        if (selectedIndex >= 0 && selectedIndex < this.CredentialsGrid.Rows.Count - 1)
        {
            Credentials.Reverse(selectedIndex, 2);
            BindData();
        }
    }

    // button for adding filled new credentials
    protected void AddCredentials_OnClick(object sender, EventArgs e)
    {
        if (CredentialsId == NonExistingADCredentialID)
        {
            int? credId = CreateNewCredentials(CredentialName, Login, PlainPassword);
            if (credId.HasValue)
            {
                InitCredentials();
                CredentialsId = credId.GetValueOrDefault();
            }
        }

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                UsernamePasswordCredential credential = bl.UserGetAllCredentials(false).SingleOrDefault(n => n.ID == CredentialsId);
                Credentials.Add(credential);
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred adding credentials.  Details {0}", ex.ToString());
        }




        CredEdit.Visible = false;

        BindData();
        modalityDiv.ShowBlock = false;

    }

    private int? CreateNewCredentials(string credentialName, string username, string password)
    {
        UsernamePasswordCredential credential = new UsernamePasswordCredential()
        {
            Description = credentialName,
            Name = credentialName,
            Username = username,
            Password = password
        };

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                return bl.UserAddCredential(credential);
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred contacting the UDT Business Layer (AddCredential).  Details: {0}", ex.ToString());
            return 0;
        }

    }

    // test new credentials
    protected void TestCredentials_OnClick(object sender, EventArgs e)
    {
        if (!CheckAllValidators())
        {
            throw new ArgumentException("Enter valid credentials");
        }

        string validationText = string.Empty;
        bool valid = false;
        bool credentialsValid = true;

        if (CredentialsId == NonExistingADCredentialID)
        {
            int tmpId;
            credentialsValid = ValidateNewCredentials(CredentialName, Login, PlainPassword, out validationText, out tmpId);

            if ((credentialsValid) && (tmpId != CredentialsId))
                CredentialsId = tmpId;
        }

        if (credentialsValid)
        {
            //BL test creds
            try
            {
                using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                {
                    UsernamePasswordCredential credential;
                    if (CredentialsId == NonExistingADCredentialID)
                    {
                        credential = new UsernamePasswordCredential()
                        {
                            Description = CredentialName,
                            Name = CredentialName,
                            Username = Login,
                            Password = PlainPassword
                        };
                    }
                    else
                    {
                        credential = bl.UserGetAllCredentials(false).SingleOrDefault(n => n.ID == CredentialsId);
                    }

                    valid = bl.UserTestCredential(String.Empty, credential, UDTCredentialType.AD);
                    if (!valid) validationText = Resources.UDTWebContent.UDTWCODE_VB1_7;

                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("An error occurred contacting the UDT Business Layer (TestADCredential).  Details: {0}", ex.ToString());
                valid = false;
                validationText = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_8, ex.Message);
            }

        }

        SetUIValidationState(valid, validationText);

    }

    private void SetUIValidationState(bool validationSucceeded, string text)
    {
        ADCredValidationTrue.Visible = validationSucceeded;
        ADCredValidationFalse.Visible = !validationSucceeded;
        ADCredValidationFailedMessage.Text = text;
    }

    private bool ValidateNewCredentials(string description, string username, string password, out string errorMessage, out int existingCredentialId)
    {
        errorMessage = string.Empty;
        existingCredentialId = NonExistingADCredentialID;

        UsernamePasswordCredential existingCredential = adCredentials.FirstOrDefault(c => c.Name.Equals(description, StringComparison.CurrentCultureIgnoreCase));
        if (existingCredential != null)
        {
            if ((existingCredential.Username == username) && (existingCredential.Password == password))
            {
                existingCredentialId = existingCredential.ID.GetValueOrDefault();
                return true;
            }
            else
            {
                errorMessage = Resources.UDTWebContent.UDTWCODE_VB1_9;
                return false;
            }
        }
        return true;
    }

    // cancel adding new credentials
    protected void CancelCredential_OnClick(object sender, EventArgs e)
    {
        this.CredEdit.Visible = false;
        this.modalityDiv.ShowBlock = false;

        // forget edited credential index
        //this.EditedCredentialsIndex = -1;
        //BindData();
    }

    protected void ddlADCredentials_IndexChanged(object sender, EventArgs e)
    {
        CredentialsId = int.Parse(ddlADCredentials.SelectedValue);
        SetCredentialsFromDropDown();
    }

    private void SetCredentialsFromDropDown()
    {

        long credentialId = CredentialsId;

        if (credentialId == NonExistingADCredentialID)
        {
            this.CredentialName = string.Empty;
            this.Login = string.Empty;
            this.Password = string.Empty;
            this.tbLogin.Enabled = true;
            this.tbPassword.Enabled = true;
            this.tbCaption.Enabled = true;
            this.tbPasswordConfirmation.Enabled = true;

            ChangeAllValidatorsState(true);

            return;
        }

        UsernamePasswordCredential selected = adCredentials.First(c => c.ID == credentialId);
        this.CredentialName = selected.Name;
        this.Login = selected.Username;
        this.Password = selected.Password;

        this.tbLogin.Enabled = false;
        this.tbPassword.Enabled = false;
        this.tbCaption.Enabled = false;
        this.tbPasswordConfirmation.Enabled = false;

        ChangeAllValidatorsState(false);

        // If any error occured during initialization (e.g. the IP address cannot be resolved) 
        // then 'propertyBag' property isn't set.
        //if (_propertyBag != null)
        //{
        //    _propertyBag["credentialsId"] = credentialId;
        //}
    }

    private int CredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(ddlADCredentials.SelectedValue, out credentialId))
                credentialId = NonExistingADCredentialID;

            return credentialId;
        }
        set
        {
            ddlADCredentials.SelectedValue = value.ToString();
            //ddlADCredentials_IndexChanged(null, null);
        }
    }

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get
        {
            return tbPassword.Text;
        }
        set
        {
            EncodePassword(value);
        }
    }

    public string ConfirmPassword
    {
        get
        {
            return tbPasswordConfirmation.Text;
        }
    }

    public string CredentialName
    {
        get
        {
            return tbCaption.Text;
        }
        set
        {
            tbCaption.Text = value;
        }
    }

    public string PlainPassword
    {
        get
        {
            return (string)Session["UDT.ADCredential.PlainPassword"];
        }
        set
        {
            Session["UDT.ADCredential.PlainPassword"] = value;
        }
    }

    private void EncodePassword(string value)
    {
        string passBlock = GetPassBlock(value);

        if (tbPassword.Text == tbPasswordConfirmation.Text)
        {
            tbPassword.Text = passBlock;
            this.tbPassword.Attributes["value"] = passBlock;
            tbPasswordConfirmation.Text = passBlock;
            this.tbPasswordConfirmation.Attributes["value"] = passBlock;

        }
        else
        {
            value =
            tbPassword.Text =
            this.tbPassword.Attributes["value"] =
            tbPasswordConfirmation.Text =
            this.tbPasswordConfirmation.Attributes["value"] = "";
        }
        this.PlainPassword = value;
    }

    private string GetPassBlock(string value)
    {
        return (!string.IsNullOrEmpty(value)) ? string.Empty.PadLeft(value.Length, '*') : "";
    }

    private IEnumerable<BaseValidator> GetAllValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredCredentialNameValidator,
            this.RequiredUserNameValidator,
            this.RequiredPasswordValidator,
            this.RequiredConfirmPasswordValidator,
            this.CompareConfirmPasswordValidator
        };
    }

    private bool CheckAllValidators()
    {
        return GetAllValidators().All(v => v.IsValid);
    }

    private void ChangeAllValidatorsState(bool enabled)
    {
        foreach (var validator in GetAllValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private void InitCredentials()
    {
        //Get all UDT credentials from BusinessLayer
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                UsernamePasswordCredential[] allCredentials = bl.UserGetAllCredentials(false);
                adCredentials = allCredentials.ToList();
                adCredentials.Insert(0,
                                     new UsernamePasswordCredential() { ID = NonExistingADCredentialID, Name = Resources.UDTWebContent.UDTWCODE_VB1_10 });

            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(
                "An error occurred contacting the UDT Business Layer (RELGetAllCredentials).  Details: {0}",
                ex.ToString());
            throw;
        }

        ddlADCredentials.DataSource = adCredentials;
        ddlADCredentials.DataTextField = "Name";
        ddlADCredentials.DataValueField = "ID";
        ddlADCredentials.DataBind();
    }
}