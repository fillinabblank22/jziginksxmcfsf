﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddADCredentials.aspx.cs" Inherits="Orion_UDT_Admin_Discovery_AddADCredentials"
    MasterPageFile="~/Orion/UDT/Admin/Discovery/DiscoveryPage.master" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_VB1_37%>"%>
    
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/UDT/Admin/Discovery/DiscoveryProgress.ascx" TagPrefix="orion" TagName="DiscoveryWorkflowProgress" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Register TagPrefix="udt" TagName="ADAdminCredential" Src="~/Orion/UDT/Admin/Discovery/Controls/ADAdminCredential.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false"/>
    <orion:Include runat="server" File="OrionCore.js"/>
    <style type="text/css">
        
    #linksContent a { color: #336699; }
	#linksContent a:hover {color:orange;}  
	
    a.iconLink
    {
        background-image: url(/Orion/Discovery/images/icon_add.gif);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
    }
    
    a.helpLink 
    {
        color: #336699;
        font-size:9pt;
    }
    
    a.helpLink:hover
    {
        color:Orange;
    }
    .sw-btn:hover 
    {
	color: #204a63 !important;
    }
    </style>

     <style type="text/css">
        .x-grid3-row 
        {
            padding-left: 26px;
        }
        
        .x-grid3-header 
        {
            padding-left: 26px;
        }
                
        .x-grid-group-hd 
        {
            border: 0px;
            padding-top: 0px;
        }
 
        .x-grid-group-hd DIV
        {
            border: 0px;
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: normal;
            color: #000000;
            padding-top: 0px;
            background-color: #fafafa;
            background: url(/Orion/images/Button.Collapse.GIF) no-repeat ;
            background-position: 2px 3px;
        }
        
        .x-grid-group-collapsed .x-grid-group-hd DIV
        {
            background: url(/Orion/images/Button.Expand.GIF) no-repeat;
            background-position: 2px 3px;
        }
        
         .redLink:active 
        {
            color: #CE0000;
            text-decoration: underline;
        }
         .redLink:visited 
        {
            color: #CE0000;
            text-decoration: underline;
        }      
        .redLink:hover 
        {
            color: #000000;
            text-decoration: underline;
        }
        .checkboxStyle{ margin-left: 5px;}
        .progressLabel{ white-space: nowrap;width: 220px !important;}
        .x-menu{width: auto !important;}
    </style>
    <script type="text/javascript">
        //<![CDATA[
        var timeoutHandle;
        var keepAlive;
        var cancelling = false;
        var finishing = false;
        var baseUrl = '<%= this.ResolveClientUrl("~/Orion/UDT/Admin/Discovery/Default.aspx") %>';
        var nextStepUrl = '<%= this.ResolveClientUrl("~/Orion/UDT/Admin/Discovery/ImportDCs.aspx") %>';

        var ShowProgressDialog = function () {
            var encodedTitle = $("<div/>").text('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_1)%>').html();
            $("#progressDialog").show().dialog({
                width: 550, height: 280, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
            });

            $(".ui-dialog-titlebar-close").unbind('click');
            $(".ui-dialog-titlebar-close").bind('click', CancelDiscovery);
        }

        var StartDiscovery = function () {
            //FB42885: Device tracker discovery - cancel port discovery using ESC key
            $(document).keypress(function (e) {
                var code = e.keyCode ? e.keyCode : e.which;
                if (code == 27) {
                    //Catch ESC key - dialog has been closed
                    ShowProgressDialog();
                    CancelDiscovery();
                    //Eat ESC key (Arumm, arummm, arummm... :)
                    return false;
                }
            });


            //Start discovery
            ShowProgressDialog();
            PageMethods.StartDiscovery(
                function (result) {
                    //Initiate KeepAlive
                    keepAlive = setTimeout(KeepAlive, 10000);
                    GetDiscoveryProgress();
                },
                function (error) {
                    //check for license exception
                    if (!LicenseCheck(error)) {
                        window.location = baseUrl;
                    } else {
                        $("#progressDialog").dialog("close");
                        alert(String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_2)%>', error.get_message()));
                        window.location = baseUrl;
                    }
                });
        }

            var ResumeDiscovery = function () {
                SW.UDT.PortDiscovery.GetDiscoveryErrors();
                SW.UDT.PortDiscovery.init();
                SW.UDT.PortFilter.Apply();
            }

            var GetDiscoveryProgress = function () {
                PageMethods.GetDiscoveryProgress(
                    OnProgressReceived,
                    function (error) {
                        //check for license exception
                        if (!LicenseCheck(error)) {
                            window.location = baseUrl;
                        } else {
                            var message = error.get_message();
                            //if it's a Discovery session not found error try and make it more user friendly.
                            var sessionNotFoundMessage = message.indexOf("not found");
                            if (sessionNotFoundMessage > 0)
                            { finalMessage = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_3)%>'; }
                            else {
                                finalMessage = String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_4)%>', message);
                            }
                            alert(finalMessage);
                            window.location = baseUrl;
                        }
                    });
            }

                var LicenseCheck = function (error) {
                    //check for license exception
                    var rel = /SolarWindsLicensingException/;
                    if (error.get_message().search(rel) > -1) {
                        var message = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_5)%>';
                    alert(message);
                    return false;
                } else {
                    return true;
                }
            }

            var KeepAlive = function () {
                PageMethods.GetDiscoveryProgress(
                    function (result) { //Do nothing, just keep discovery session alive (keepalive @ 1min intervals)
                        keepAlive = setTimeout(KeepAlive, 60000);
                    },
                    function (error) {
                        //check for license exception
                        if (!LicenseCheck(error)) {
                            window.location = baseUrl;
                        } else {

                            var message = error.get_message();
                            //if it's a Discovery session not found error try and make it more user friendly.
                            var sessionNotFoundMessage = message.indexOf("not found");
                            if (sessionNotFoundMessage > 0)
                            { finalMessage = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_3)%>'; }
                            else {
                                finalMessage = String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_4)%>', message);
                            }

                            alert(finalMessage);
                            window.location = baseUrl;
                        }
                    });
            }

                var GetLabel = function (header, text) {
                    if (header && text)
                        return String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_6)%>', header, text);
                else if (header)
                    return header;
                else
                    return "";
            }

            function nl2br_js(myString) {
                if (myString == null) return "";
                var replaceString = '<br/>';
                return myString.replace(/\r\n/gi, replaceString);
            }

            var ZeroPorts = false;

            var OnProgressReceived = function (result) {

                if (result.Status.Status == 1) { // in progress
                    SetProgressBar(1, result.PercentComplete, 100); //result.PrimaryMax);
                    document.getElementById("statusText").innerHTML = nl2br_js(result.Status.Description); //GetLabel(result.PrimaryName, result.SecondaryName));

                    $("#dcs").text(result.DiscoveredDomainControllers);
                    //schedule next progress checking 
                    timeoutHandle = setTimeout(GetDiscoveryProgress, 1000);
                }
                else if (result.Status.Status != 1) { // finished
                    finishing = true;
                    clearTimeout(timeoutHandle);
                    $("#statusText").text('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_7)%>');
                SetProgressBar(1, 1, 1);
                if (result.Status.Status == 6) {
                    alert(result.Status.Description);
                }

                //FB61754
                //disable the next button here as there is a delay between discovery finishing and the licence
                //checks being done at which point it gets disabled if the licences is exceeded

                if ((result.DiscoveredDomainControllers == 0) && (result.DiscoveredRouters == 0) && (result.DiscoveredSwitches == 0)) {
                    PageMethods.CancelDiscovery(
                    function () { // cancel initiated
                        alert('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_8)%>');
                        window.location = baseUrl;
                    },
                function (error) {
                    //check for license exception
                    if (!LicenseCheck(error)) {
                        window.location = baseUrl;
                    } else {
                        alert(String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_9)%>', error.get_message()));
                        window.location = baseUrl;
                    }
                });

            }
                //FB46740
            if (result.DiscoveredPorts == 0) {

                //Allow zero ports inported to continue beyond this step in discovery
                ZeroPorts = true;


                //$("#OkPanel").hide();
                //                PageMethods.CancelDiscovery(
                //                    function () { // cancel initiated
                //                        $("#progressDialog").dialog("close");
                //                        alert("No ports were discovered.\n\nThe discovery session has been cancelled.");
                //                        window.location = baseUrl;
                //                    },
                //                    function (error) {
                //                        //check for license exception
                //                        if (!LicenseCheck(error)) {
                //                            window.location = baseUrl;
                //                        }
                //                    });

                //                return;
                //window.location = baseUrl;
            }
            PageMethods.ProcessResult(
                function () { // processing complete
                    SetProgressBar(1, 1, 1);
                    $("#progressDialog").dialog("close");
                    window.location = nextStepUrl;


                },
                function (error) {
                    //check for license exception
                    if (!LicenseCheck(error)) {
                        window.location = baseUrl;
                    } else {
                        alert(String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_10)%>', error.get_message()));
                        window.location = baseUrl;
                    }
                });
            }
            else {
                alert('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_11)%>');
                window.location = baseUrl;
            }
        }

    var SetProgressBar = function (id, value, maxValue) {
        if (value > maxValue) value = maxValue;
        if (value == 0) {
            $("#progressBar" + id).empty();
        }
        else {
            if ($("#progressBar" + id + " img").length == 0) {
                $("#progressBar" + id).append(CreateBarImage());
            }
            $("#progressBar" + id + " img").width((300 / maxValue) * value);
        }
    }

    var CreateBarImage = function () {
        return $("<img />").attr("src", "/Orion/Discovery/images/progBar_on.gif").height(20);
    }
    var CancelDiscovery = function () {
        if (cancelling || finishing) return;
        clearTimeout(timeoutHandle);
        if (confirm('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_12)%>')) {
            cancelling = true;
            $("#statusText").text("");
            PageMethods.CancelDiscovery(
                function () { // cancel initiated
                    GetDiscoveryProgress();
                },
                function (error) {
                    //check for license exception
                    if (!LicenseCheck(error)) {
                        window.location = baseUrl;
                    } else {
                        alert(String.format('<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_9)%>', error.get_message()));
                    window.location = baseUrl;
                }
            });
        }
        else {
            GetDiscoveryProgress();
        }
    }

    var CloseDiscovery = function () {
        window.location = baseUrl;
    }



    var toggleErrorDetailsState = false;
    function toggleErrorDetails(el) {
        if (!toggleErrorDetailsState) {
            el.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_14)%>';
                        $('#DiscoveryErrors').show();
                    } else {
                        el.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_15)%>';
                        $('#DiscoveryErrors').hide();
                    }
                    toggleErrorDetailsState = !toggleErrorDetailsState;
                }

                var toggleAlertDetailsState = false;
                function toggleAlertDetails(el) {
                    if (!toggleAlertDetailsState) {
                        el.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_14)%>';
                        $('#DiscoveryAlerts').show();
                    } else {
                        el.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.UDTWebContent.UDTWEB_JS_CODE_VB1_15)%>';
                        $('#DiscoveryAlerts').hide();
                    }
                    toggleAlertDetailsState = !toggleAlertDetailsState;
                }
                //]]>
    </script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionUDTPHDomainControllerDiscovery" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1><%=Page.Title%></h1>
            </td>
            <td align="right" style="vertical-align: top; width: 180px;">
            </td>
        </tr>
    </table>

    <div class="GroupBoxAccounts" id="ADDiv" style="min-width: 480px;">
    <orion:DiscoveryWorkflowProgress ID="ProgressIndicator" runat="server" />

    <asp:UpdatePanel runat="server" ID="ParentUpdatePanel">
        <ContentTemplate>

            <div id="AddADCredentials" style="padding: 10px 10px 5px 10px;">
                <div style="padding-bottom:8px;">
                    <table width="100%">
                        <tr valign="top">
                            <td style="width: 1150px">
                                <h2><%= Resources.UDTWebContent.UDTWEBDATA_VB1_38 %></h2>
                                <%= Resources.UDTWebContent.UDTWEBDATA_VB1_39 %>  
                                <orion:HelpLink ID="UDTDiscoveryADAdminCredentialHelpLink" runat="server" HelpUrlFragment="OrionCoreAGEditingUserAccounts" HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_40 %>" CssClass="helpLink" />
                            </td>
                         </tr>
                    
                    </table>
                </div>

                <udt:ADAdminCredential ID="AdminCredsControl" runat="server" />

            <br />
                <table width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                   
                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right; padding-right:8px; padding-top:5px;">


                            <asp:Panel ID="DiscoveryButtonPanel" runat="server" style="margin-left: 15px;">
                                <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="LocalizableButton1" runat="server" DisplayType="Primary" LocalizedText="Next" OnClick="btnNext_Click" OnClientClick="" />
                                <orion:LocalizableButton id="LocalizableButton2" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                                </div>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>             
            </div>

            
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="LocalizableButton1" />
        </Triggers>
    </asp:UpdatePanel>

    </div>

    <div id="progressDialog">
            <div id="progressContent">
                <table id="progressTable" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="progressLabel">
                            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_93%>
                        </td>
                        <td colspan="2">
                            <div id="progressBar1" class="progressBar" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="progressLabel">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="progressLabel" style="vertical-align:top;">
                            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_94%>
                        </td>
                        <td colspan="2" class="progressSeparator">
                            <div id="statusText" style="font-weight: normal; height:90px; width: 300px; white-space: nowrap; overflow-x: scroll; overflow-y: hidden;"><%= Resources.UDTWebContent.UDTWEBDATA_VB1_95%></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="progressLabel">
                            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_101 %>
                        </td>
                        <td class="progressData progressAltRow" id="dcs">
                            0
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>

                </table>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClientClick="CancelDiscovery(); return false;"/>
                </div>
            </div>
        </div>
    
</asp:Content>



