﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Web;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.UDT.Common.DAL;

public partial class Orion_UDT_Admin_Discovery_ImportDCs : System.Web.UI.Page
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private List<UDTDomainController> dcList;
    private List<UsernamePasswordCredential> adCredentials;
    private const int NonExistingADCredentialID = 0;
    private string pollingOkMessage = Resources.UDTWebContent.UDTWCODE_VB1_27;
    private string pollingWarningMessage = Resources.UDTWebContent.UDTWCODE_VB1_28;
    private string pollingDCNotFoundMessage = Resources.UDTWebContent.UDTWCODE_VB1_29;
    private string pollingDCsCurrentMessage = Resources.UDTWebContent.UDTWCODE_VB1_30;

    protected void Page_Init(object sender, EventArgs e)
    {
   
        if (!Profile.AllowAdmin)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWEBCODE_AK1_1));
        }

         
        if (!IsPostBack)
        {
            if (DiscoveryWorkflowManager.Results == null)
            {
                Server.Transfer("~/Orion/UDT/Admin/Discovery/Default.aspx");
            }
            DiscoveryWorkflowManager.ReloadDomainControllerResults();
            DiscoveryWorkflowManager.CurrentStep = DiscoveryWorkflowManager.DiscoveryStep.ImportDCs;
        }

        InitCredentials();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        dcList =
            DiscoveryWorkflowManager.Results.UDTNodes.Where(
                n => n.ContainsCapability(UDTNodeCapability.DomainController)).Select(
                    n => (UDTDomainController) n).ToList();

        // refill password for new credentials
        if ((CredentialsId == NonExistingADCredentialID)
        && ((string.IsNullOrEmpty(this.PlainPassword) && string.IsNullOrEmpty(this.Password)) || (this.PlainPassword != this.Password))
        && GetPassBlock(this.PlainPassword) != this.Password)
        {
            EncodePassword(this.Password);
        }

        DCCredValidationTrue.Visible = false;
        DCCredValidationFalse.Visible = false;

        if (!IsPostBack)
        {
            BindData();
            BindCurrentlyMonitored();
            SetStatusMessage();
        }
    }

    private void BindCurrentlyMonitored()
    {

        List<UDTDomainController> current = new List<UDTDomainController>();

        try
        {
            const string sql = @"SELECT [UDT_NodeCapability].[NodeID], [Nodes].[Caption], [Nodes].[IP_Address], [Nodes].[GroupStatus] FROM [UDT_NodeCapability], [Nodes] WHERE [Capability]=@UDTNodeCapability AND [UDT_NodeCapability].[NodeID] = [Nodes].[NodeID]";
            using (var cmd = SqlHelper.GetTextCommand(sql))
            {
                cmd.Parameters.AddWithValue("@UDTNodeCapability", UDTNodeCapability.DomainController);

                using (var reader = SqlHelperUDT.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        var nodeID = DatabaseFunctions.GetInt32(reader, 0);
                        var nodeCaption = String.Format("<img style='vertical-align:middle;' src='/Orion/images/StatusIcons/Small-{1}'/>&nbsp;{0}",
                                                        DatabaseFunctions.GetString(reader, 1),
                                                        DatabaseFunctions.GetString(reader, 3));
                        var nodeIPAddress = DatabaseFunctions.GetString(reader, 2);
                        current.Add(new UDTDomainController() { NodeID = nodeID, HostName = nodeCaption, IPAddress = nodeIPAddress });
                    }
                }
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred enumerating DC capable nodes.  Details {0}", ex.ToString());
            return;
        }

        CurrentlyMonitoredDCs.DataSource = current;
        CurrentlyMonitoredDCs.DataBind();
        DCCurrentMessage.Text = String.Format(pollingDCsCurrentMessage, current.Count);
        CurrentDCs.Visible = (current.Count>0);    
    }

    protected List<UsernamePasswordCredential> Credentials
    {
        get
        {
            return (List<UsernamePasswordCredential>)this.ViewState["ADCredentials"];
        }

        set
        {
            this.ViewState["ADCredentials"] = value;
        }
    }

    private int CredentialsId
    {
        get
        {
            int credentialId;
            if (!int.TryParse(ddlADCredentials.SelectedValue, out credentialId))
                credentialId = NonExistingADCredentialID;

            return credentialId;
        }
        set
        {
            ddlADCredentials.SelectedValue = value.ToString();
        }
    }

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get
        {
            return tbPassword.Text;
        }
        set
        {
            EncodePassword(value);
        }
    }

    public string ConfirmPassword
    {
        get
        {
            return tbPasswordConfirmation.Text;
        }
    }

    public string CredentialName
    {
        get
        {
            return tbCaption.Text;
        }
        set
        {
            tbCaption.Text = value;
        }
    }

    public string PlainPassword
    {
        get
        {
            return (string)Session["UDT.ADCredential.PlainPassword"];
        }
        set
        {
            Session["UDT.ADCredential.PlainPassword"] = value;
        }
    }

    private void EncodePassword(string value)
    {
        string passBlock = GetPassBlock(value);

        if (tbPassword.Text == tbPasswordConfirmation.Text)
        {
            tbPassword.Text = passBlock;
            this.tbPassword.Attributes["value"] = passBlock;
            tbPasswordConfirmation.Text = passBlock;
            this.tbPasswordConfirmation.Attributes["value"] = passBlock;

        }
        else
        {
            value =
            tbPassword.Text =
            this.tbPassword.Attributes["value"] =
            tbPasswordConfirmation.Text =
            this.tbPasswordConfirmation.Attributes["value"] = "";
        }
        this.PlainPassword = value;
    }

    private string GetPassBlock(string value)
    {
        return (!string.IsNullOrEmpty(value)) ? string.Empty.PadLeft(value.Length, '*') : "";
    }

    private IEnumerable<BaseValidator> GetAllValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredCredentialNameValidator,
            this.RequiredUserNameValidator,
            this.RequiredPasswordValidator,
            this.RequiredConfirmPasswordValidator,
            this.CompareConfirmPasswordValidator
        };
    }

    private bool CheckAllValidators()
    {
        return GetAllValidators().All(v => v.IsValid);
    }

    private void ChangeAllValidatorsState(bool enabled)
    {
        foreach (var validator in GetAllValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private void SetCredentialsFromDropDown()
    {

        long credentialId = CredentialsId;

        if (credentialId == NonExistingADCredentialID)
        {
            this.CredentialName = string.Empty;
            this.Login = string.Empty;
            this.Password = string.Empty;
            this.tbLogin.Enabled = true;
            this.tbPassword.Enabled = true;
            this.tbCaption.Enabled = true;
            this.tbPasswordConfirmation.Enabled = true;

            ChangeAllValidatorsState(true);

            return;
        }

        UsernamePasswordCredential selected = adCredentials.First(c => c.ID == credentialId);
        this.CredentialName = selected.Name;
        this.Login = selected.Username;
        this.Password = selected.Password;

        this.tbLogin.Enabled = false;
        this.tbPassword.Enabled = false;
        this.tbCaption.Enabled = false;
        this.tbPasswordConfirmation.Enabled = false;

        ChangeAllValidatorsState(false);

    }

    private void SetStatusMessage()
    {
        int invalid = dcList.Count(dc => dc.CredentialStatus == UDTCredentialStatus.Invalid);
        if (invalid>0)
        {
            PollingWarningMessage.Text = String.Format(pollingWarningMessage, invalid, dcList.Count());
        } else
        {
            PollingOkMessage.Text = String.Format(pollingOkMessage, dcList.Count());
        }
        PollingDCNotFoundMessage.Text = pollingDCNotFoundMessage; 
        PollingOK.Visible = !(invalid > 0) && (dcList.Count>0);
        PollingWarning.Visible = (invalid > 0) && (dcList.Count>0);
        PollingDCNotFound.Visible = (dcList.Count == 0);
        DCGridView.Visible = (dcList.Count != 0);
    }

    protected void ddlADCredentials_IndexChanged(object sender, EventArgs e)
    {
        CredentialsId = int.Parse(ddlADCredentials.SelectedValue);
        SetCredentialsFromDropDown();
    }

    protected void ExpanderButton_OnClick(object sender, EventArgs e)
    {
        if (ExpanderButton.CssClass == "Expand")
        {
            ExpanderButton.CssClass = "Collapse";
            CurrentDCsDetail.Visible = true;
        } else
        {
            ExpanderButton.CssClass = "Expand";
            CurrentDCsDetail.Visible = false;
        }
        
    }

    // button for adding filled new credentials
    protected void AssignCredentials_OnClick(object sender, EventArgs e)
    {
        List<string> validCredentials = ValidateCredentials();

        if (CredentialsId == NonExistingADCredentialID)
        {
            int? credId = CreateNewCredentials(CredentialName, Login, PlainPassword);
            if (credId.HasValue)
            {
                InitCredentials();
                CredentialsId = credId.GetValueOrDefault();
            }
        }

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                UsernamePasswordCredential credential = bl.UserGetAllCredentials(false).SingleOrDefault(n => n.ID == CredentialsId);

                foreach (var dc in SelectedDCs())
                {
                    UDTDomainController selDC = dcList.Single(n => n.IPAddress == dc.IPAddress);
                    selDC.CredentialID = credential.ID.GetValueOrDefault();
                    selDC.CredentialStatus = validCredentials.Contains(dc.IPAddress) ? UDTCredentialStatus.Valid : UDTCredentialStatus.Invalid;
                }
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred adding credentials.  Details {0}", ex.ToString());
        }

        this.CredEdit.Visible = false;
        this.modalityDiv.ShowBlock = false;

        BindData();
        SetStatusMessage();
    }

    // test new credentials
    protected void TestCredentials_OnClick(object sender, EventArgs e)
    {
        ValidateCredentials();
    }

    private List<string> ValidateCredentials()
    {
        var validCredentials = new List<string>();

        if (!CheckAllValidators())
        {
            throw new ArgumentException("Enter valid credentials");
        }

        string validationText = string.Empty;
        bool valid = false;
        bool credentialsValid = true;

        if (CredentialsId == NonExistingADCredentialID)
        {
            int tmpId;
            credentialsValid = ValidateNewCredentials(CredentialName, Login, PlainPassword, out validationText, out tmpId);

            if ((credentialsValid) && (tmpId != CredentialsId))
                CredentialsId = tmpId;
        }

        if (credentialsValid)
        {
            //BL test creds
            try
            {
                using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                {
                    UsernamePasswordCredential credential;
                    if (CredentialsId == NonExistingADCredentialID)
                    {
                        credential = new UsernamePasswordCredential()
                        {
                            Description = CredentialName,
                            Name = CredentialName,
                            Username = Login,
                            Password = PlainPassword
                        };
                    }
                    else
                    {
                        credential = bl.UserGetAllCredentials(false).SingleOrDefault(n => n.ID == CredentialsId);
                    }

                    foreach (var dc in SelectedDCs())
                    {
                        string ipAddress = dc.IPAddress;
                        valid = bl.UserTestCredential(ipAddress, credential, UDTCredentialType.REL);
                        if (!valid)
                        {
                            validationText = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_31, dc.HostName);
                            break;
                        } 
                        validCredentials.Add(dc.IPAddress);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("An error occurred contacting the UDT Business Layer (TestDCCredential).  Details: {0}", ex.ToString());
                valid = false;
                validationText = String.Format(Resources.UDTWebContent.UDTWCODE_VB1_8, ex.Message);
            }

        }

        SetUIValidationState(valid, validationText);
        return validCredentials;
    }

    private void SetUIValidationState(bool validationSucceeded, string text)
    {
        DCCredValidationTrue.Visible = validationSucceeded;
        DCCredValidationFalse.Visible = !validationSucceeded;
        DCCredValidationFailedMessage.Text = text;
    }

    private bool ValidateNewCredentials(string description, string username, string password, out string errorMessage, out int existingCredentialId)
    {
        errorMessage = string.Empty;
        existingCredentialId = NonExistingADCredentialID;

        UsernamePasswordCredential existingCredential = adCredentials.FirstOrDefault(c => c.Name.Equals(description, StringComparison.CurrentCultureIgnoreCase));
        if (existingCredential != null)
        {
            if ((existingCredential.Username == username) && (existingCredential.Password == password))
            {
                existingCredentialId = existingCredential.ID.GetValueOrDefault();
                return true;
            }
            else
            {
                errorMessage = Resources.UDTWebContent.UDTWCODE_VB1_9;
                return false;
            }
        }
        return true;
    }

    // cancel adding new credentials
    protected void CancelCredential_OnClick(object sender, EventArgs e)
    {
        this.CredEdit.Visible = false;
        this.modalityDiv.ShowBlock = false;
    }


    private List<UDTDomainController> SelectedDCs()
    {
        var dcSelected = new List<UDTDomainController>();
        for (int i = 0; i < DomainControllerGrid.Rows.Count; i++)
        {
            CheckBox chkrow = (CheckBox)DomainControllerGrid.Rows[i].FindControl("Check");
            if (chkrow.Checked)
            {
                dcSelected.Add(dcList[i]);
            }
        }
        return dcSelected;
    }

    private void EnableButtons()
    {
        bool assignButtonEnabled = false;
        for (int i = 0; i < DomainControllerGrid.Rows.Count; i++)
        {
            CheckBox chkrow = (CheckBox)DomainControllerGrid.Rows[i].FindControl("Check");
            if (chkrow.Checked)
            {
                assignButtonEnabled = true;
                break;
            }
        }
        AssignCreds.Enabled = assignButtonEnabled;
        AssignCreds.CssClass = assignButtonEnabled ? "iconLinkEnabled" : "iconLinkDisabled";

        UpdateNextButtonState();
    }

    public string GetCredentialAssigned(int credentialID)
    {
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                UsernamePasswordCredential credential = bl.UserGetAllCredentials(false).SingleOrDefault(n => n.ID == credentialID);
                return string.Format("{0}", credential.Username);
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred fetching credentials; CredentialID={0}.  Details {1}", credentialID, ex.ToString());
            throw;
        }

    }

    private int? CreateNewCredentials(string credentialName, string username, string password)
    {
        UsernamePasswordCredential credential = new UsernamePasswordCredential()
        {
            Description = credentialName,
            Name = credentialName,
            Username = username,
            Password = password
        };

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                return bl.UserAddCredential(credential);
            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("An error occurred contacting the UDT Business Layer (AddCredential).  Details: {0}", ex.ToString());
            return 0;
        }

    }

    // shows editor dialog for adding a new set
    protected void AssignCreds_OnClick(object sender, EventArgs e)
    {
        
        DCCredValidationTrue.Visible = false;
        DCCredValidationFalse.Visible = false;

        CredEdit.Visible = true;
        modalityDiv.ShowBlock = true;

        if (SelectedDCs().Count()>1)
        {
            CredentialsId = NonExistingADCredentialID;
            ddlADCredentials.SelectedValue = NonExistingADCredentialID.ToString();
            SetCredentialsFromDropDown();
        } else
        {
            var selectedDC = SelectedDCs().FirstOrDefault();
            if (selectedDC != null)
            {
                CredentialsId = selectedDC.CredentialID;
                ddlADCredentials.SelectedValue = selectedDC.CredentialID.ToString();
                SetCredentialsFromDropDown();
            }            
        }

    }

    
    protected void CheckAllRows_OnCheckChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < DomainControllerGrid.Rows.Count; i++)
        {
            CheckBox chkrow = (CheckBox)DomainControllerGrid.Rows[i].FindControl("Check");
            chkrow.Checked = ((CheckBox)sender).Checked;
        }
        EnableButtons();
    }

    protected void CheckRow_OnCheckChanged(object sender, EventArgs e)
    {
        if (!((CheckBox)sender).Checked)
        {
            foreach (Control c in DomainControllerGrid.HeaderRow.Controls)
            {
                if (c is DataControlFieldHeaderCell)
                {
                    CheckBox checkAll = (CheckBox) c.FindControl("CheckAll");
                    if (checkAll != null)
                    {
                        checkAll.Checked = false;
                    }
                }
            }
        }
        EnableButtons();
    }

    protected void DomainControllerGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        

        //Wire-up the dynamically bound Checkboxes
        foreach (Control c in e.Row.Controls)
        {
            if (c is DataControlFieldHeaderCell)
            {
                CheckBox checkAll = (CheckBox)c.FindControl("CheckAll");
                if (checkAll != null)
                {
                    checkAll.CheckedChanged += CheckAllRows_OnCheckChanged;
                    checkAll.AutoPostBack = true;
                }
            }

            if (c is DataControlFieldCell)
            {
                CheckBox check = (CheckBox)c.FindControl("Check");
                if (check != null)
                {
                    check.CheckedChanged += CheckRow_OnCheckChanged;
                    check.AutoPostBack = true;
                }
            }
        }
    }

    // binds Domain Controller data to gridView
    private void BindData()
    {
        this.DomainControllerGrid.DataSource = dcList.ToArray();
        this.DomainControllerGrid.DataBind();

        UpdateNextButtonState();
    }

    private void UpdateNextButtonState()
    {
        if (GetNewNodes().Count > 0)
        {
            btnNext.Text = Resources.UDTWebContent.UDTWCODE_VB1_32;
        }
        else
        {
            btnNext.Text = Resources.UDTWebContent.UDTWEBDATA_VB1_109;
        }
    }

    // return list of "non-core" nodes
    private List<UDTDomainController> GetNewNodes()
    {
        return SelectedDCs().Where(node => (NodeDAL.GetNode(node.NodeID) == null)).ToList();
    }

    public string GetNodeGroupStatus(int nodeID)
    {

        var groupStatus = RequestCache.Get(String.Format("N:{0}", nodeID), () =>
                                                                                  {
                                                                                      Node node = NodeDAL.GetNode(nodeID);
                                                                                      return node!=null ? node.GroupStatus.Trim() : String.Empty;
                                                                                  });

        return String.IsNullOrEmpty(groupStatus) ? "~/Orion/images/StatusIcons/Small-Unknown.gif" : String.Format("~/Orion/images/StatusIcons/Small-{0}", groupStatus);
    }

    


    public string CredentialStatusMessage(UDTCredentialStatus credStatus)
    {
        if (credStatus == UDTCredentialStatus.Valid )
        {
            return String.Format("<span><img style='vertical-align: middle;' src='/Orion/images/Check.Green.gif' />&nbsp;{0}</span>", Resources.UDTWebContent.UDTWCODE_VB1_33);
        }
            return string.Format("<span><img style='vertical-align: middle;' src='/Orion/images/failed_16x16.gif' />&nbsp;{0}</span>", Resources.UDTWebContent.UDTWCODE_VB1_34);
    }

    private void InitCredentials()
    {
        //Get all UDT credentials from BusinessLayer
        try


        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                UsernamePasswordCredential[] allCredentials = bl.UserGetAllCredentials(false);
                adCredentials = allCredentials.ToList();
                adCredentials.Insert(0,
                                     new UsernamePasswordCredential() { ID = NonExistingADCredentialID, Name = Resources.UDTWebContent.UDTWCODE_VB1_10 });

            }
        }
        catch (Exception ex)
        {
            _log.ErrorFormat(
                "An error occurred contacting the UDT Business Layer (RELGetAllCredentials).  Details: {0}",
                ex.ToString());
            throw;
        }

        ddlADCredentials.DataSource = adCredentials;
        ddlADCredentials.DataTextField = "Name";
        ddlADCredentials.DataValueField = "ID";
        ddlADCredentials.DataBind();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(DiscoveryWorkflowManager.Back());
    }

    private bool CheckLicense()
    {
        int maxNodes = 0;
        List<UDTDomainController> newNodes = GetNewNodes();
        var featureManager = new RemoteFeatureManager();
        maxNodes = featureManager.GetMaxElementCount(SolarWinds.Orion.Core.Common.WellKnownElementTypes.Nodes);

        if (maxNodes < Int32.MaxValue)
        {
            int nodeCount = 0;
            nodeCount =
                Convert.ToInt32(
                    SWISHelper.Query("SELECT COUNT(NodeID) as NodeCount FROM Orion.Nodes (nolock=true)").Rows[0]["NodeCount"]);


            if (nodeCount + newNodes.Count > maxNodes)
            {
               return this.LicenseValidator.IsValid = false;
            }
        }
        return this.LicenseValidator.IsValid = true;
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (!CheckLicense()) return;
        List<UDTDomainController> newNodes = GetNewNodes();

        //Add nodes that need to be added (SWIS CRUD)
        foreach (UDTDomainController dc in newNodes)
        {

            try
            {
                using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
                {
                    Node insertedNode = bl.PollingCreateCoreNodeForDC(dc);

                    //if nodeID = -1 then there was a problem

                    //update nodeID

                    dc.NodeID = insertedNode.ID;
                    dc.Capabilities.Single(n => n.Capability == UDTNodeCapability.DomainController).NodeID =
                        insertedNode.ID;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(
                    "An error occurred creating Core node for Domain Controller '{0}' [{1}].  Details: {2}",
                    dc.HostName, dc.IPAddress, ex.ToString());
            }

        }
        
        //Update DWM with selected only

        /* Remove DC's */
        for (int i = DiscoveryWorkflowManager.Results.UDTNodes.Count() - 1; i >= 0; i--)
        {
            if (DiscoveryWorkflowManager.Results.UDTNodes[i].ContainsCapability(UDTNodeCapability.DomainController))
                DiscoveryWorkflowManager.Results.UDTNodes.RemoveAt(i);
        }

        /* Add back seelcted */
        foreach (var dc in SelectedDCs())
        {
            DiscoveryWorkflowManager.Results.UDTNodes.Add(dc);
        }

        //DiscoveryWorkflowManager.Results.DomainControllers = SelectedDCs();

        Page.Response.Redirect(DiscoveryWorkflowManager.Next(DiscoveryWorkflowManager.DiscoveryStep.DiscoveryResults));
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(DiscoveryWorkflowManager.Cancel());
    }
}
