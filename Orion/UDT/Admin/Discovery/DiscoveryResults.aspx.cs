﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models;
using System.Web.UI.WebControls;

public struct ImportInfo
{
    public bool ImportIsDone { get; set; }
    public string ResultText { get; set; }
    public int PercentComplete { get; set; }
}

public partial class Orion_UDT_Admin_Discovery_DiscoveryResults : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DiscoveryWorkflowManager.CurrentStep = DiscoveryWorkflowManager.DiscoveryStep.DiscoveryResults;
            DCsImportedList.DataSource = DiscoveryWorkflowManager.Results.UDTNodes.Where(
                n => n.ContainsCapability(UDTNodeCapability.DomainController));
            DCsImportedList.DataBind();
            
        }

        //Redirect if you got here without an existing DiscoveryWorkflowManager session
        if (!DiscoveryWorkflowManager.Exists())
        {
            log.ErrorFormat("Discovery Results page was loaded without an existing DiscoveryWorkflowManager session.  Page Redirected to {0}.", DiscoveryWorkflowManager.DefaultUrl());
            Response.Redirect(DiscoveryWorkflowManager.DefaultUrl());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var monitoredPortCount = 0;
        var monitoredDeviceCount = 0;
        UDTDiscoveryResult results = DiscoveryWorkflowManager.Results;
        foreach (UDTNode sw in results.UDTNodes.Where(n => n.ContainsCapability(UDTNodeCapability.Layer2)))
        {
            bool portMonitoredOnDevice = false;
            foreach (UDTPort port in sw.Ports)
            {

                if (port.IsMonitored == true)
                {
                    monitoredPortCount++;
                    port.IsExcluded = false;
                    portMonitoredOnDevice = true;
                }

            }
            if (portMonitoredOnDevice) monitoredDeviceCount++;
        }

       
        if (results.UDTNodes.Where(
            n => n.ContainsCapability(UDTNodeCapability.DomainController)).Count() > 0)
        {
            DCsImported.Visible = true;
            DCsImportedMessage.Text =
                string.Format(Resources.UDTWebContent.UDTWCODE_VB1_36, results.UDTNodes.Where(
                    n => n.ContainsCapability(UDTNodeCapability.DomainController)).Count());

        } else
        {
            DCsImported.Visible = false;
        }
    }

    /// <summary> CancelImport </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static string CancelImport()
    {
        return DiscoveryWorkflowManager.Cancel();
    }

    /// <summary> Takes results and imports them into db </summary>
    /// <returns>Import info</returns>
    [ScriptMethod]
    [WebMethod(true)]
    public static int StartImport() 
    {
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {
                if ((DiscoveryWorkflowManager.Results.UDTNodes.Count() == 0))
                {
                    return 2; //Nothing to import
                }else
                {
                    bl.DiscoveryStartImport(DiscoveryWorkflowManager.Results);
                    return 1; //Need to return StartImportStatus.Started (1) instead of int 
                }

            }
            catch (Exception ex)
            {
                log.Error("Cannot import Discovery Results.", ex);
                throw;
            }

        }

    }

    /// <summary> Takes results and imports them into db </summary>
    /// <returns>Import info</returns>
    [ScriptMethod]
    [WebMethod(true)]
    public static ImportInfo GetImportProgress() //int = ImportInfo
    {

        UDTDiscoveryImportProgress importProgress;
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                importProgress = bl.DiscoveryGetImportProgress(DiscoveryWorkflowManager.DiscoveryId);
            }

        }
        catch (Exception ex)
        {
            log.Error("Cannot get import results.", ex);
            throw;
        }

        StringBuilder info = new StringBuilder();


        int percentComplete = -1;
        foreach (string importLine in importProgress.ProgressMessage)
        {
            string line = importLine.Replace("  ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            info.Append(line);
            info.Append("<br/>");
        }
            
        percentComplete = importProgress.PercentComplete;

        log.DebugFormat("UDT devices and ports imported. ResultText={0}", info.ToString());

        if (percentComplete != 100)
        {
            return new ImportInfo() { ImportIsDone = false, ResultText = info.ToString(), PercentComplete = percentComplete };
        }
        return new ImportInfo() { ImportIsDone = true, ResultText = info.ToString(), PercentComplete = 100 };
        
            
        
    }

   

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(DiscoveryWorkflowManager.Back());
    }

    protected void btnFinish_Click(object sender, EventArgs e)
    {

        //kick off PollingCheckUpdateAllJobs()
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.PollingCheckUpdateAllJobs();
            }

        }
        catch (Exception ex)
        {
            log.Error("Unable to start polling jobs.", ex);
            throw new ApplicationException("Unable to start polling jobs. \n" + ex.Message, ex);
        }
        //Redirect
        Page.Response.Redirect(DiscoveryWorkflowManager.Next(DiscoveryWorkflowManager.DiscoveryStep.Completed));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(DiscoveryWorkflowManager.Cancel());
    }
}