﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Web;

public partial class Orion_UDT_Admin_Discovery_DiscoveryProgress : System.Web.UI.UserControl
{
    private const string indicatorImageFolderPath = "~/orion/images/nodemgmt_art/progress_indicator/background/";

    protected void Page_Load(object sender, EventArgs e)
    {
        Reload();
    }

    public void Reload()
    {
        RenderProgressImages();
    }

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();

        Image[] separators = new Image[] { new Image(),  new Image(), new Image() };
        Label[] texts = new Label[] {
            new Label() {Text= Resources.UDTWebContent.UDTWEBDATA_VB1_31, CssClass = "PI_off"},         
            new Label() {Text = Resources.UDTWebContent.UDTWEBDATA_VB1_34, CssClass = "PI_off"}, 
            new Label(){Text = Resources.UDTWebContent.UDTWEBDATA_VB1_35, CssClass = "PI_off"},
           };
        
        texts[0].Style.Add("padding-left", "10px;"); // to move text a bit to the right

        switch (DiscoveryWorkflowManager.CurrentStep)
        {
            case DiscoveryWorkflowManager.DiscoveryStep.AddDCCredentials:
                texts[0].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
               
                break;

          

            case DiscoveryWorkflowManager.DiscoveryStep.ImportDCs:
                texts[1].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_off_on.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
               
                break;

            case DiscoveryWorkflowManager.DiscoveryStep.DiscoveryResults:
                texts[2].CssClass = "PI_on";
                separators[0].ImageUrl = string.Format("{0}pi_sep_off_off.gif", indicatorImageFolderPath);
                separators[1].ImageUrl = string.Format("{0}pi_sep_off_on.gif", indicatorImageFolderPath);
                separators[2].ImageUrl = string.Format("{0}pi_sep_on_off.gif", indicatorImageFolderPath);
               
                break;

          
        }

        for (int i = 0; i < 3; i++)
        {
            phPluginImages.Controls.Add(texts[i]);
            phPluginImages.Controls.Add(separators[i]);
        }
    }
}
