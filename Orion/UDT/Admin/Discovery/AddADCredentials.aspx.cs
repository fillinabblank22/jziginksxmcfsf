﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Common.Services;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DAL;
using SolarWinds.UDT.Common;
using System.Web.Services;
using SolarWinds.UDT.Common.Models;

public partial class Orion_UDT_Admin_Discovery_AddADCredentials : System.Web.UI.Page
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Profile.AllowAdmin)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWCODE_VB1_1));
        }

        if (!IsPostBack)
        {
            DiscoveryWorkflowManager.Init();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        //Store

        UDTSettingDAL.SaveSetting(UDTSettingKey.AdAdminCredentials, String.Join(@",", AdminCredsControl.Credentials.Select(n => n.ID.ToString()).ToArray()));
        // Page.Response.Redirect(DiscoveryWorkflowManager.Next(DiscoveryWorkflowManager.DiscoveryStep.ImportDCs));

        if (!ClientScript.IsStartupScriptRegistered(this.GetType(), "UDTDCDiscovery"))
            ClientScript.RegisterStartupScript(this.GetType(), "UDTDCDiscovery", "StartDiscovery();", true);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(DiscoveryWorkflowManager.Cancel());
    }

    protected bool BackFromResultsPage()
    {

        if (Request.UrlReferrer != null)
        {

            return Request.UrlReferrer.ToString().Contains(Page.ResolveUrl(
                                      DiscoveryWorkflowManager.Next(
                                          DiscoveryWorkflowManager.DiscoveryStep.ImportDCs)));
        }
        return false;
    }

    /// <summary> Create Discovery Job in BusinessLayer </summary>
    [WebMethod(true)]
    public static void StartDiscovery()
    {
        try
        {
            CreateDiscoveryJob();
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Error in WebMethod StartDiscovery. Error: {0}", ex.ToString());
            throw;

        }

    }

    /// <summary> Get Mock (simulated) discovery results. </summary>
    [WebMethod(true)]
    public static UDTDiscoveryProgress GetDiscoveryProgress()
    {
        ////==Start Mock DiscoveryProgress==
        ////  This call simulates discovery progress, Comment this out to use business layer call
        //return MockDiscoveryHelper.GenerateMockProgress(HttpContext.Current.Session);
        ////==End Mock DiscoveryProgress==

        //Get Discovery Progress from BusinessLayer
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            Guid id = DiscoveryWorkflowManager.DiscoveryId;
            UDTDiscoveryProgress info = bl.DiscoveryGetProgress(id);
            return info;
        }

    }

    [WebMethod(true)]
    public static void CancelDiscovery()
    {
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            bl.DiscoveryCancel(DiscoveryWorkflowManager.DiscoveryId);
        }

    }

    private static void CreateDiscoveryJob()
    {

        ////==Start Mock DiscoveryProgress==
        ////  This call simulates creation of discovery job, Comment this out to use business layer call
        //DiscoveryWorkflowManager.DiscoveryId = Guid.NewGuid();
        //HttpContext.Current.Session["UDT_SwitchesProcessed"] = 0;
        //return;
        ////==End Mock CreateDiscoveryJob==

        //Create Discovery Job in BusinessLayer
        using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
        {
            try
            {

                //if (DiscoveryWorkflowManager.DiscoveryId==Guid.Empty)
                //{
                //    string message = "UDT Discovery {0}: discovery job is already running for this session";
                //    log.ErrorFormat(message, DiscoveryWorkflowManager.DiscoveryId.ToString());
                //    throw new ApplicationException(String.Format(message, DiscoveryWorkflowManager.DiscoveryId.ToString()));

                //} else {
                DiscoveryWorkflowManager.Devices = new List<string>();
                List<int> devices = DiscoveryWorkflowManager.Devices.ConvertAll<int>(x => int.Parse(x));
                int[] nodes = devices.ToArray();

                DiscoveryTarget targets = DiscoveryTarget.None;

                if (!String.IsNullOrEmpty(UDTSettingDAL.GetSettingOrDefault(UDTSettingKey.AdAdminCredentials))) targets = targets | DiscoveryTarget.DomainControllers;

                if (DiscoveryWorkflowManager.Devices.Count > 0) targets = targets | DiscoveryTarget.Switches;
                //always discover routers, unless no device count
                if (DiscoveryWorkflowManager.Devices.Count > 0) targets = targets | DiscoveryTarget.Routeres;

                Guid id = bl.DiscoveryStart(nodes, targets);
                DiscoveryWorkflowManager.DiscoveryId = id;


                //}

            }
            catch (Exception ex)
            {
                //UDTBusinessLayer in fault state?
                _log.Error(ex.Message);
                throw;

            }
        }
    }

    [WebMethod(true)]
    public static void ProcessResult()
    {
        //move to the results processing portion of discovery...
        DiscoveryWorkflowManager.LoadDiscoveryResults();
    }
}