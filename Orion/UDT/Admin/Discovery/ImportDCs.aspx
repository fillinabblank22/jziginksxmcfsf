﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportDCs.aspx.cs" Inherits="Orion_UDT_Admin_Discovery_ImportDCs" 
    MasterPageFile="~/Orion/UDT/Admin/Discovery/DiscoveryPage.master" Title="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_37%>"%>
<%@ Import Namespace="SolarWinds.UDT.Common.Models" %>
    
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/UDT/Admin/Discovery/DiscoveryProgress.ascx" TagPrefix="orion" TagName="DiscoveryWorkflowProgress" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
  <orion:Include runat="server" File="OrionCore.js" />

  
    <style type="text/css">
        
    #linksContent a { color: #336699; }
	#linksContent a:hover {color:orange;}  
    
    a.helpLink 
    {
        color: #336699;
        font-size:9pt;
    }
    
    a.helpLink:hover
    {
        color:Orange;
    }
    
    .Expand
    {
        background-image: url(/Orion/images/Button.Expand.GIF);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
        color: #000000;
    }

    .Collapse
    {
        background-image: url(/Orion/images/Button.Collapse.GIF);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
        color: #000000;
    }
        
    a.iconLinkDisabled
    {
        background-image: url(/Orion/images/icon_assign_cred_16x16.gif);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
        color: #c0c0c0;
    }
    
    a.iconLinkEnabled
    {
        background-image: url(/Orion/images/icon_assign_cred_16x16.gif);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
    }   
    
    .PollingOK 
    {
        background-image: url(/Orion/images/Check.Green.gif);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 4px 0px 4px 20px;
        font-weight: bold;
        font-size: 9pt;
        background-color: #DAF7CC;
        color: #00A000;
    }
    
    .PollingWarning 
    {
        background-image: url(/Orion/images/warning_16x16.gif);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 4px 0px 4px 20px;
        font-weight: normal;
        font-size: 9pt;
        background-color: #FDF6C9;
        color: #000000;
    }
 
     .PollingDCNotFound 
    {
        padding: 4px 0px 4px 3px;
        font-weight: normal;
        font-size: 9pt;
        color: #000000;
    }
       
    .CredBox 
    {
        width: 200px;
    }
    
    .ValidatorText 
    {
        font-size: 8pt;
        padding-bottom: 4px;

    }
    </style>

</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionUDTPHDeviceTrackerDiscovery" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
 <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1><%=Page.Title%></h1>
            </td>
            <td>
            </td>
        </tr>
    </table>

    <div class="GroupBoxAccounts" id="SelectTypeDiv" style="min-width: 480px; height:100%">
    <orion:DiscoveryWorkflowProgress ID="ProgressIndicator" runat="server" />

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>

            <div id="SelectDevices" style="padding: 10px 10px 5px 10px;">
                <div style="padding-bottom:8px;">
                    <table width="100%">
                        <tr valign="top">
                            <td>
                                <h2><%= Resources.UDTWebContent.UDTWEBDATA_VB1_102 %></h2>
                                <%= Resources.UDTWebContent.UDTWEBDATA_VB1_103%>
                            </td>
                         </tr>
                    </table>
                </div>
                <asp:Panel ID="PollingOK" runat="server" visible="false" CssClass="PollingOK"><asp:Literal ID="PollingOkMessage" runat="server"></asp:Literal></asp:Panel>
                <asp:Panel ID="PollingWarning" runat="server" visible="false" CssClass="PollingWarning"><asp:Literal ID="PollingWarningMessage" runat="server"></asp:Literal><orion:HelpLink ID="UDTDiscoveryADAdminCredentialHelpLink" runat="server" HelpUrlFragment="OrionUDTPHImportADDomainController" HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_40 %>" CssClass="helpLink" /></asp:Panel>
                <asp:Panel ID="PollingDCNotFound" runat="server" visible="false" CssClass="PollingDCNotFound"><asp:Literal ID="PollingDCNotFoundMessage" runat="server"></asp:Literal></asp:Panel>
                <asp:Panel ID="CredEdit" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel" style = "max-width:900;" >
                    <p class="DialogHeaderText" style="max-width:240px !important;">
                        <asp:Label ID="ActionHeader" runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_104 %>" Font-Bold="true"
                            Font-Size="Larger"/>
                    </p>
                    <asp:Panel ID="CredEditV1Controls" runat="server" BorderWidth="0" Visible="true"                        
                        Class="EditControls">                    
                      <table>
                            <tr>
	                            <td>
	                                <asp:Label runat="server" ID="lblCredential" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_42 %>"/><br />
                                    <asp:DropDownList ID="ddlADCredentials" runat="server" CssClass="CredBox" AutoPostBack="True" OnSelectedIndexChanged="ddlADCredentials_IndexChanged" >
                                    </asp:DropDownList>
	                            </td>
                            </tr>

                            <tr>
	                            <td>
	                                <asp:Label runat="server" ID="lblCredName" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_43 %>"/><br />
                                    <asp:TextBox ID="tbCaption" runat="server" CssClass="CredBox" MaxLength="50" ></asp:TextBox>
                                    <div><asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" CssClass="ValidatorText" ControlToValidate="tbCaption" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_44 %>"></asp:RequiredFieldValidator></div>
	                            </td>
                            </tr>
                            <tr>
	                            <td>
	                                <asp:Label runat="server" ID="lblLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_45 %>"/><br />
                                    <asp:TextBox ID="tbLogin" runat="server" CssClass="CredBox" MaxLength="50"></asp:TextBox>
                                    <div><asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbLogin" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_46 %>" ></asp:RequiredFieldValidator></div>
	                            </td>
                            </tr>
                            <tr>
                                <td class="modifiedLeftLabelColumn pollingSelector">
                                    <div class="smallText">
                                        <%= Resources.UDTWebContent.UDTWEBDATA_VB1_47%></div>
                                    <div id="EditCredentialHelp">
                                        <span class="LinkArrow">&#0187;</span>
                                        <orion:HelpLink ID="UDTDCCredHelpLink" runat="server" HelpUrlFragment="OrionCoreAGEditingUserAccounts"
                                            HelpDescription="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_48 %>" CssClass="helpLink" />
                                    </div>
	                            </td>	            
                            </tr>
                            <tr>
	                            <td>
	                                <asp:Label runat="server" ID="lblPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_49 %>"/><br />
                                    <asp:TextBox ID="tbPassword" runat="server" CssClass="CredBox" TextMode="Password" MaxLength="50" ></asp:TextBox>
                                    <div><asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbPassword" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_50 %>"></asp:RequiredFieldValidator></div>
	                            </td>
                            </tr>
                            <tr>
	                            <td>
	                                <asp:Label runat="server" ID="lblPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_51 %>"/><br />
                                    <asp:TextBox ID="tbPasswordConfirmation" runat="server" CssClass="CredBox" TextMode="Password" MaxLength="50" ></asp:TextBox>
	                                <div><asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_52 %>"/></div>
                                    <asp:CompareValidator
                                        ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbPassword" Display="Dynamic" CssClass="ValidatorText" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_53 %>"></asp:CompareValidator>
                                    <br /><div style="margin-top:4px;"><orion:LocalizableButton id="TestCred" runat="server" DisplayType="Small" LocalizedText="Test" OnClick="TestCredentials_OnClick" /></div>
	                            </td>
                            </tr>
                        </table>
                        <div runat="server" id="DCCredValidationTrue" style="background-color: #D6F6C6;
                            text-align: center" visible="false">
                            <img runat="server" id="DCCredValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                            <%= Resources.UDTWebContent.UDTWEBDATA_VB1_55%>
                        </div>
                        <div runat="server" id="DCCredValidationFalse" style="background-color: #FEEE90;
                            text-align: center; color: Red" visible="false">
                            <img runat="server" id="DCCredValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                            <asp:Label runat="server" ID="DCCredValidationFailedMessage" />
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons">
                      
                            <table>
                                <tr>
                                    <td style="width:100px;">
                                         <asp:UpdateProgress runat="server" ID="UpdateProgress" DynamicLayout="false" DisplayAfter="1000">
                                            <ProgressTemplate>
                                                <span class="ValidatorText"><img alt="<%= Resources.UDTWebContent.UDTWEBDATA_VB1_57 %>" style="vertical-align: middle;" src="/Orion/images/loading_gen_small.gif" />&nbsp;<%= Resources.UDTWebContent.UDTWEBDATA_VB1_56%></span>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                    <td>
                                         <div class="sw-btn-bar">
                                        <orion:LocalizableButton id="AssignCred" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_58 %>"  OnClick="AssignCredentials_OnClick" OnClientClick="" />
                                        <orion:LocalizableButton id="CancelAddCred" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelCredential_OnClick" CausesValidation="false" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                         

                    </asp:Panel>
                </asp:Panel>

                <orion:TBox ID="modalityDiv" runat="server" />


                <div ID="DCGridView" runat="server" class="contentBlock">
                    <table width="100%">
                        <tr class="ButtonHeader">                    
                            <td style="padding-left: 10px;">
                                <asp:LinkButton Enabled="false" ID="AssignCreds" runat="server" OnClick="AssignCreds_OnClick" CssClass="iconLinkDisabled" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_105 %>"/>
                            </td>
                        </tr>
                        <tr>
                        <td>
                            <asp:GridView Width="100%" ID="DomainControllerGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                                CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow" OnRowDataBound="DomainControllerGrid_OnRowDataBound">
                                 <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                         <asp:CheckBox ID="CheckAll" OnCheckedChanged="CheckAllRows_OnCheckChanged" runat="server" Width="10" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="Check" runat="server" Width="10"  OnCheckedChanged="CheckRow_OnCheckChanged"  /> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_106 %>">
                                        <ItemTemplate>
                                            <img ID="img" style='vertical-align: middle;' runat="server" src='<%#GetNodeGroupStatus((int)Eval("NodeID")) %>' />&nbsp;<asp:Label ID="Label1" runat="server" Text='<%#Eval("Hostname") %>' Width="100" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_107 %>">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# GetCredentialAssigned((int)Eval("CredentialID")) %>' Width="120" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_108 %>">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# CredentialStatusMessage((UDTCredentialStatus)Eval("CredentialStatus"))%>' Width="578" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>


 
                <asp:Panel ID="CurrentDCs" runat="server" CssClass="PollingDCNotFound" Visible="false">
                <asp:LinkButton ID="ExpanderButton" runat="server" CssClass="Expand" OnClick="ExpanderButton_OnClick"><asp:Literal ID="DCCurrentMessage" runat="server" /></asp:LinkButton><div ID="CurrentDCsDetail" runat="server" style="background-color:#e4f1f8; padding: 2px;" visible="false">
                    <asp:Repeater ID="CurrentlyMonitoredDCs" runat="server">
                        <HeaderTemplate>
                            <table >
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td class="PollingDCNotFound"><%# Eval("Hostname").ToString() %> [<%# Eval("IPAddress").ToString() %>]</td></tr></ItemTemplate><FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                </asp:Panel>
                <br />

                <table  width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                   
                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn" style="width: 60%;text-align: left">
                            &nbsp;
                            <asp:CustomValidator
                                                ID="LicenseValidator" 
                                                runat="server" 
                                                ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_DF1_1%>" 
                                                EnableClientScript="false"
                                                Display="Dynamic" 
                                                >
                                            </asp:CustomValidator>
                        </td>
                         
                        <td  style="text-align:right; padding-right:8px; padding-top:5px;width: 40%">
                            <asp:Panel ID="DiscoveryButtonPanel" runat="server" style="margin-left: 15px;">
                                <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="btnBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="btnBack_Click" OnClientClick="" />
                                <orion:LocalizableButton id="btnNext" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_VB1_109  %>"  OnClick="btnNext_Click" OnClientClick="" />
                                <orion:LocalizableButton id="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                                </div>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>             
            </div>
            
            
        </ContentTemplate>
    </asp:UpdatePanel>

    </div>
</asp:Content>



