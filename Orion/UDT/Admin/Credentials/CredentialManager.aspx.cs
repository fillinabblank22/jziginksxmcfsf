﻿using System;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;
using System.IO;
using SolarWinds.Logging;

public partial class Orion_UDT_Admin_Credentials_CredentialManager : System.Web.UI.Page, IBypassAccessLimitation
{
    private static readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
	{
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWEBCODE_AK1_9));
        }

	}
    
    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HideCustomizePageLink = true;
        pageTitleBar.HelpFragment = "OrionUDTPHManageUDTCredentials";
    }
}
