﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

SW.UDT.CredentialManager = function () {

    var selectorModel;
    var dataStore;
    var grid;
    var oldName;
    var addDialog;
    var updateDialog;
    var delDialog;
    var pageSizeNum;
    var updatePasswordChanged = false;

    ORION.prefix = "UDTCredentialManager_";

    UpdateToolbarButtons = function () {
        var map = grid.getTopToolbar().items.map;

        if ($("#isDemoMode").length) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }
        else {
            var selItems = grid.getSelectionModel();
            var count = selItems.getCount();
            var editButtonDisabled = (count != 1);
            var deleteButtonDisabled = false;

            selItems.each(function (rec) {
                if (rec.data["NodesAssigned"] != "0") {
                    deleteButtonDisabled = true;
                }
            });

            map.EditButton.setDisabled(editButtonDisabled);
            map.DeleteButton.setDisabled(count === 0 || deleteButtonDisabled);
        }
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderCredential(value, meta, record) {
        return encodeHTML(value);
    };

    // Column rendering functions
    function renderUserName(value, meta, record) {
        return encodeHTML(value);
    };

    function renderNodeCount(value, meta, record) {
        return String.format("@{R=UDT.Strings;K=UDTWEBJS_AK1_114;E=js}", value);
    };

    function editSelecetedCredential() {
        var selItems = grid.getSelectionModel();

        selItems.each(function (rec) {
            eval('SW.UDT.CredentialManager.' + rec.data["Control"] + '.Show(' + rec.data["CredentialID"] + ');');
            return;
        });
    }

    function deleteSelecetedCredential() {
        if (!confirm("@{R=UDT.Strings;K=UDTWEBJS_AK1_115;E=js}"))
            return;

        var ids = [];

        grid.getSelectionModel().each(function (rec) {
            ids.push(rec.data["CredentialID"]);
        });

        ORION.callWebService("/Orion/UDT/Services/CredentialManagerService.asmx", "DeleteCredentials", { credentialIDs: ids }, function (result) {
            grid.getStore().reload();
        });
    }

    return {
        reload: function () { grid.getStore().reload(); },
        init: function () {

            // load default page size from Orion settings
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_34;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.onClick("refresh");
                    }, this);
                }
            });


            // set selection model to checkbox
            selectorModel = new Ext.grid.CheckboxSelectionModel();

            selectorModel.on("selectionchange", UpdateToolbarButtons);

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/UDT/Services/CredentialManagerService.asmx/GetCredentials",
                [
                { name: 'CredentialID', mapping: 0 },
                { name: 'CredentialName', mapping: 1 },
                { name: 'Username', mapping: 2 },
                { name: 'NodesAssigned', mapping: 3 },
                { name: 'Control', mapping: 4 }
                ],
                "Name");


            // define grid panel
            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    selectorModel,
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_VB1_20;E=js}', width: 100, hidden: true, hideable: false, sortable: true, dataIndex: 'CredentialID' },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_116;E=js}', width: 230, sortable: true, dataIndex: 'CredentialName', renderer: renderUserName },
                    { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_2;E=js}', width: 230, sortable: true, dataIndex: 'Username', renderer: renderUserName },
			        { header: '@{R=UDT.Strings;K=UDTWEBJS_AK1_117;E=js}', width: 280, sortable: true, dataIndex: 'NodesAssigned', renderer: renderNodeCount }
                ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                RowStripes:true,
                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 710,
                height: 300,
                stripeRows: true,

                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_118;E=js}',
                    iconCls: 'add',
                    // TODO: add menu when pluggable for other credential types
                    handler: function () { eval('SW.UDT.CredentialManager.UsernamePasswordCredential.Show(null);'); }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_119;E=js}',
                    iconCls: 'edit',
                    handler: function () { editSelecetedCredential(); }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=UDT.Strings;K=UDTWEBJS_AK1_120;E=js}',
                    iconCls: 'del',
                    handler: function () { deleteSelecetedCredential(); }
                }],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_AK1_121;E=js}',
                    emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_AK1_122;E=js}"
                })

            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.render('Grid');
            UpdateToolbarButtons();
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();
        }
    };

} ();


SW.UDT.CredentialManager.UsernamePasswordCredential = function () {
    var dialog;
    var editedCredentialId;
    var oldPassword;
    var oldCredentialName;
    var dialogHtml =
       '<style type="text/css">\
            .dialogBody td { padding: 5px; padding-bottom: 5px; vertical-align: middle; }\
            a.helpLink { color: #336699; font-size: smaller; text-decoration: none; }\
       </style>\
       <table class="dialogBody">\
         <tr>\
             <td><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_38;E=js}</span></td>\
             <td><input ID="credDescription" maxlength="50"></input></td>\
         </tr>\
         <tr>\
            <td><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_39;E=js}</span></td>\
            <td><input ID="credUsername" maxlength="50"></input></td>\
         </tr>\
         <tr>\
             <td></td>\
             <td>\
                 <div class="smallText">@{R=UDT.Strings;K=UDTWEBJS_VB1_40;E=js}</div>\
                 <span class="LinkArrow">»</span>\
                 <a class="helpLink" target="_blank"\
                  href="https://www.solarwinds.com/documentation/helploader.aspx?topic=OrionCoreAGEditingUserAccounts.htm">\
                  @{R=UDT.Strings;K=UDTWEBJS_VB1_41;E=js} </a>\
             </td>\
         </tr>\
         <tr>\
             <td><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_42;E=js}</span></td>\
             <td><input ID="credPassword" maxlength="50" type="password"></input></td>\
         </tr>\
         <tr>\
             <td><span class="Label">@{R=UDT.Strings;K=UDTWEBJS_VB1_43;E=js}</span></td>\
             <td><input ID="credConfirm" maxlength="50" type="password"></input></td>\
         </tr>\
         <tr>\
             <td colspan="2" style="color: red;"><span id="errorDiv"></span></td>\
         </tr>\
     </table>';

    function checkCredentials(description, username, password, confirm) {
        if ($.trim(String(description)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_44;E=js}";
        }

        if ($.trim(String(username)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_45;E=js}";
        }

        if (!(password === confirm) || $.trim(String(password)) == '') {
            return "@{R=UDT.Strings;K=UDTWEBJS_VB1_46;E=js}";
        }

        return "";
    };

    return {
        Show: function (credentialId) {
            editedCredentialId = credentialId;

            if (!dialog) {
                dialog = new Ext.Window({
                    id: 'dialog',
                    layout: 'fit',
                    width: 440,
                    height: 300,
                    modal: true,
                    closeAction: 'hide',
                    plain: true,
                    resizable: false,
                    title: '@{R=UDT.Strings;K=UDTWEBJS_AK1_123;E=js}',

                    items: [{ html: dialogHtml}],
                    buttons: [
                    {
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_5;E=js}',
                        handler: function () {
                            var description = $("#credDescription").val();
                            var username = $("#credUsername").val();
                            var password = $("#credPassword").val();
                            var confirmPassword = $("#credConfirm").val();
                            
                            if (password == confirmPassword && password == oldPassword && username == oldCredentialName) {
                                //user pressed ok but changed nothing
                                dialog.hide();
                                return;
                            }

                            var error = checkCredentials(description, username, password, confirmPassword, true);

                            if (error != '') {
                                $("#errorDiv").text(error);
                                return;
                            }
                            else {
                                $("#errorDiv").text('');
                            }


                            ORION.callWebService("/Orion/UDT/Services/CredentialManagerService.asmx", "InsertUpdateUDTCredential",
                            {
                                credentialId: editedCredentialId,
                                name: description,
                                username: username,
                                password: password
                            },
                            function (result) {

                                // credential name exists in db    
                                if (result == false) {
                                    $("#errorDiv").text('@{R=UDT.Strings;K=UDTWEBJS_AK1_124;E=js}');
                                    return;
                                }

                                dialog.hide();
                                SW.UDT.CredentialManager.reload();
                            });



                        }
                    },
                    {
                        text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_6;E=js}',
                        style: 'margin-left: 5px;',
                        handler: function () {
                            dialog.hide();
                        }
                    }]
                });
            }

            $("#errorDiv").text('');

            if (credentialId != null) {
                ORION.callWebService("/Orion/UDT/Services/CredentialManagerService.asmx", "GetUDTCredential", { credentialId: credentialId },
                function (result) {
                    dialog.show();

                    var descriptionInput = $("#credDescription");

                    descriptionInput.val(result.Name);
                    descriptionInput.attr('disabled', 'disabled');

                    $("#credUsername").val(result.Username);
                    $("#credPassword").val(result.Password);
                    $("#credConfirm").val(result.Password);

                    // store old password even if it is a fake one (bunch of stars)
                    oldPassword = result.Password;
                    oldCredentialName = result.Username;
                });
            }
            else {
                dialog.show();

                var descriptionInput = $("#credDescription");

                descriptionInput.val('');
                descriptionInput.removeAttr('disabled');

                $("#credUsername").val('');
                $("#credPassword").val('');
                $("#credConfirm").val('');
            }
        }
    };
} ();

Ext.onReady(SW.UDT.CredentialManager.init, SW.UDT.CredentialManager);