﻿<%@ Page Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" AutoEventWireup="true" CodeFile="CredentialManager.aspx.cs" 
    Inherits="Orion_UDT_Admin_Credentials_CredentialManager" Title="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_147 %>" %>

<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false"  Version="3.4"/>
    
    <orion:Include ID="Include1" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include2" runat="server" Module="UDT" File="Admin/Credentials/CredentialManager.js" />
    
    <style type="text/css">
        span.Label { white-space:nowrap; }
        .smallText {color:#979797;font-size:9px;}
        .add { background-image:url(/Orion/images/add_16x16.gif) !important; }
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; }
        .del { background-image:url(/Orion/images/delete_16x16.gif) !important; }
        #Grid td { padding-right: 0px; padding-bottom: 0px; vertical-align: middle; }
        #Grid { padding-top: 12px; }
       
       a.helpLink { color: #336699; }
       a.helpLink:hover { color:Orange; }
	#footermark { margin-top:0px;line-height:0px;}
	html {position: relative; min-height: 100%; height: inherit;}
	body{margin-bottom: 52px;}
	#footer{    position: absolute;bottom: 0;height: 52px;width: 100%;}

    </style>
</asp:Content>

<asp:Content id="SettingsTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="pageTitleBar" PageTitle="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_148 %>" HideCustomizePageLink="true" ShowTitle="False" />
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%= Resources.UDTWebContent.UDTWEBDATA_AK1_148%></h1>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
    <div style="padding-left:10px;">
        <%= Resources.UDTWebContent.UDTWEBDATA_AK1_149 %>
        <orion:HelpLink ID="HelpLink1" runat="server" HelpUrlFragment="OrionCoreAGEditingUserAccounts" HelpDescription="<%$ Resources: UDTWebContent, UDTWEBDATA_AK1_150 %>" CssClass="helpLink" />
    </div>
    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    <div id="gridPanel" style="width:850px; padding:8px; ">	
        <div id="Grid"/>
    </div>
</asp:Content>


