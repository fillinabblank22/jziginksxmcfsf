﻿Ext.namespace('SW');
Ext.namespace('SW.UDT');

var isAllSelected;
var selectedItems;
var ruleTypeParamValue = 2;



SW.UDT.ManageWhiteList = function () {

    ORION.prefix = "UDT_ManageWhiteList_";

    //check for querystring param "type"
    //if provided, overload ruleTypeParamValue
    var typeParam = querystring("type");
    if (typeParam.length == 1) {
        //ruleTypeParamValue = typeParam[0];

    }

    SW.UDT.TestRules = function (test) {
        var input = $('#' + test).val();
        ORION.callWebService("/Orion/UDT/Services/ManageWhiteList.asmx", "TestRules", { testText: input, ruleType: ruleTypeParamValue }, function (result) {
            SW.UDT.ShowTestResults(result, input);
        });
    };

    SW.UDT.ShowTestResults = function (results, inptxt) {

        var closeHtml = "<img style='cursor:pointer; padding-left: 8px;' onclick='$(\"#result\").empty();' src='/orion/images/Button.RemoveCondition.gif' />";
        var success = (results.length > 1);
        var html;
        var inside;
        var link;
        var testText = inptxt;
        if (success) {
            if (ruleTypeParamValue == 1) {
                html = '<span class="crn crn-tl"></span><span class="crn crn-tr"></span><span class="crn crn-bl"></span><span class="crn crn-br"></span><div class="sw-pg-neutral-inner"><table><tr><td><div class="sw-pg-neutral-body">{0}</div></td><td>{1}</td></tr></table></div>';
                inside = "@{R=UDT.Strings;K=UDTWEBJS_MR1_7;E=js}";
            }
            else {
                html = '<span class="crn crn-tl"></span><span class="crn crn-tr"></span><span class="crn crn-bl"></span><span class="crn crn-br"></span><div class="sw-pg-pass-inner"><table><tr><td><div class="sw-pg-pass-body">{0}</div></td><td>{1}</td></tr></table></div>';
                inside = "@{R=UDT.Strings;K=UDTWEBJS_MK1_7;E=js}";
            }
            inside = String.format(inside, testText);
            for (var i = 0; i < results.length - 1; i++) {
                inside = inside + "<b>" + results[i].Second + "</b>";
                link = "<a href='/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=1&ruleid=" + results[i].First + "'>" + "&nbsp;&raquo; @{R=UDT.Strings;K=UDTWEBJS_MK1_17;E=js}" + "</a><br/>";
                inside = inside + link;
            }

            $("#result").html(String.format(html, inside));
        } else {
            var ipPatternTest = results[0].Second;
            var endPointType;

            var result = (ipPatternTest == "Valid");
            if (result) {
                endPointType = "UE-IP";

            }
            else {

                testText = testText.trim();
                var macPattern = new RegExp("^([0-9a-fA-F][0-9a-fA-F][-:_.]){5}([0-9a-fA-F][0-9a-fA-F])$");
                result = macPattern.test(testText);
                if (result) {
                    endPointType = "UE-MAC";

                }
                else {
                    endPointType = "UE-DNS";

                }
            }
            if (ruleTypeParamValue == 1) {
                html = '<span class="crn crn-tl"></span><span class="crn crn-tr"></span><span class="crn crn-bl"></span><span class="crn crn-br"></span><div class="sw-pg-neutral-inner"><table><tr><td><div class="sw-pg-neutral-body">{0}</div></td><td>{1}</td></tr></table></div>';
                inside = "@{R=UDT.Strings;K=UDTWEBJS_MR1_8;E=js}";
                inside = String.format(inside, testText);
                link = "<a href='/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&endpoint=" + testText + "&endpointType=" + endPointType + "&type=" + ruleTypeParamValue + "'>" + "&raquo; @{R=UDT.Strings;K=UDTWEBJS_MR1_9;E=js}" + "</a>";
            }
            else {
                html = '<span class="crn crn-tl"></span><span class="crn crn-tr"></span><span class="crn crn-bl"></span><span class="crn crn-br"></span><div class="sw-pg-fail-inner"><table><tr><td><div class="sw-pg-fail-body">{0}</div></td><td>{1}</td></tr></table></div>';
                inside = "@{R=UDT.Strings;K=UDTWEBJS_MK1_8;E=js}";
                inside = String.format(inside, testText);
                link = "<a href='/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&endpoint=" + testText + "&endpointType=" + endPointType + "&type=" + ruleTypeParamValue + "'>" + "&raquo; @{R=UDT.Strings;K=UDTWEBJS_MK1_18;E=js}" + "</a>";
            }
            inside = inside + link;
        }

        $("#result").html(String.format(html, inside, closeHtml));
    };

    function querystring(key) {
        var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
        var r = [], m;
        while ((m = re.exec(document.location.search)) != null) r.push(m[1]);
        return r;
    }

    //tab select handler
    function handleActivate(tab) {
        var newState = tab.itemId;
        if (ruleTypeParamValue != newState) {
            ruleTypeParamValue = newState;
            //reload group by values and refresh data
            SW.UDT.LoadGroupByValues();
            SW.UDT.ManageWhiteList.RefreshGridData();
            if (ruleTypeParamValue == 1) {
                $("label[for=RuleTypeLbl]").text("@{R=UDT.Strings;K=UDTWEBJS_MR1_5;E=js}");
            }
            else {
                $("label[for=RuleTypeLbl]").text("@{R=UDT.Strings;K=UDTWEBJS_MR1_6;E=js}");
            }
        }
    }

    function updateToolbarButtons() {

        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        map.EditButton.setDisabled((count === 0) || (count > 1));
        map.DeleteButton.setDisabled(count === 0);

        //enabled/disabled
        var enCount = 0;
        var disCount = 0;
        selItems.each(function (rec) {
            var status = rec.data["Status"];
            if (status == 0) disCount++;
            if (status == 1) enCount++;
        });

        map.EnableButton.setDisabled(count === enCount);
        map.DisableButton.setDisabled(count === disCount);
        // The buttons are disabled in demo mode
    
        if ($("#isOrionDemoMode").length != 0) {
            map.AddButton.setDisabled(true);
            map.EditButton.setDisabled(true);
            map.EnableButton.setDisabled(true);
            map.DisableButton.setDisabled(true);
            map.DeleteButton.setDisabled(true);
        }

        // uncheck the selected all checkbox if an item is deselected
        if (count < selectedItems) {
            isAllSelected = false;
            var obj = $('.x-grid3-hd-checker');
            if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                obj.removeClass('x-grid3-hd-checker-on');
        }

        selectedItems = count;
    };

    function renderText(value) {
        return value;
    }

    function renderStatus(value, meta, record, rowIndex, colIndex, store) {
        var img;
        switch (value) {
            case 1:
                img = '<img src="/Orion/UDT/Images/Admin/Discovery/OK_good.gif" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_53;E=js}" style="vertical-align:bottom;" />&nbsp;';
                text = "Enabled";
                break;
            default:
                img = '<img src="/Orion/UDT/Images/Admin/Discovery/disable.png" title="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" alt="@{R=UDT.Strings;K=UDTWEBJS_VB1_55;E=js}" style="vertical-align:bottom;" />&nbsp;';
                text = "Disabled";
                break;
        }

        return img + text;
    }

    function addRule() {
        document.location.href = "/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=0&type=" + ruleTypeParamValue;
    };

    function editRule() {
        var ruleId;
        grid.getSelectionModel().each(function (rec) {
            ruleId = rec.data["ID"];
        });
        document.location.href = "/Orion/UDT/Admin/WhiteList/Wizard/Default.aspx?edit=1&ruleid=" + ruleId;
    };

    function enableRule() {
        var ruleIdArray = [];
        grid.getSelectionModel().each(function (rec) {
            ruleIdArray.push(rec.data["ID"]);
        });

        ORION.callWebService("/Orion/UDT/Services/ManageWhiteList.asmx", "EnableRules", { ruleIds: ruleIdArray }, function (result) {
            SW.UDT.ManageWhiteList.RefreshGridData();
        });
    };

    function disableRule() {
        var ruleIdArray = [];
        grid.getSelectionModel().each(function (rec) {
            ruleIdArray.push(rec.data["ID"]);
        });

        ORION.callWebService("/Orion/UDT/Services/ManageWhiteList.asmx", "DisableRules", { ruleIds: ruleIdArray }, function (result) {
            SW.UDT.ManageWhiteList.RefreshGridData();
        });
    };

    function deleteRule() {
        if (confirm("@{R=UDT.Strings;K=UDTWEBJS_SL1_01;E=js}")) {
            var ruleIdArray = [];
            grid.getSelectionModel().each(function(rec) {
                ruleIdArray.push(rec.data["ID"]);
            });

            ORION.callWebService("/Orion/UDT/Services/ManageWhiteList.asmx", "DeleteRules", { ruleIds: ruleIdArray }, function(result) {
                SW.UDT.ManageWhiteList.RefreshGridData();
            });
        }
    };


    SW.UDT.LoadGroupByValues = function () {
        var nodeGroupItemsElement = $(".NodeGroupItems");

        var nodeGroupItems = nodeGroupItemsElement.text("@{R=UDT.Strings;K=UDTWEBJS_AK1_56;E=js}");

        var groupByField = $("#groupByProperty option:selected").attr("value");
        var groupItem = null;
        var firstItem = null;


        //load group by values from webservice
        ORION.callWebService("/Orion/UDT/Services/ManageWhiteList.asmx", "GetGroupValues", { groupBy: groupByField, ruleType: ruleTypeParamValue }, function (groupValues) {

            nodeGroupItems.empty();

            if (groupValues == null) {
                SW.UDT.ManageWhiteList.RefreshGridData();
                return;
            }

            for (var i = 0; i < groupValues.length; i++) {
                var value = groupValues[i].First;
                var disp = groupValues[i].Second + " (" + groupValues[i].Third + ")";
                groupItem = $("<li class='NodeGroupItem'><a href='#' value='" + value + "'>" + disp + "</a></li>");
                $("a", groupItem).click(SW.UDT.SelectGroup);
                groupItem = groupItem.appendTo(nodeGroupItemsElement);
                if (i == 0) firstItem = groupItem;
            }

            //select first item after load
            if (firstItem != null) $("a", firstItem).click();
        });

    };

    //updates css and refreshes grid
    SW.UDT.SelectGroup = function () {
        $(this).parent().addClass("SelectedNodeGroupItem").siblings().removeClass("SelectedNodeGroupItem");
        SW.UDT.ManageWhiteList.RefreshGridData();
    };

    //returns selected group field
    SW.UDT.GetGroupField = function () {
        return $("#groupByProperty option:selected").attr("value");
    };

    //returns selected group value
    SW.UDT.GetGroupValue = function () {
        return $(".SelectedNodeGroupItem a").attr('value');
    };

    // update the Ext grid size when window size is changed
    $(window).resize(gridResize);

    //check handlers
    var beforeCheckAllClickHandler = function (sm, checked) {
        sm.lockSelect = true;
    };

    var afterCheckAllClickHandler = function (sm, checked) {

        $('.grpCheckbox').checked = checked;
        $('.grpCheckbox').attr('checked', checked);
        sm.lockSelect = false;
    };

    //grid resize handler
    function gridResize() {
        var padding = 20;
        var groupSectionWidth = $('#NodeGroupSection').width();
        var w = $(window).width();
        tabs.setWidth(w - padding - 1);
        // need to set grid width to small width first as it appears setWidth only resizes upwards! (bug in Ext maybe?)
        grid.setWidth(1);
        grid.setWidth(w - groupSectionWidth - padding - 10);
    }

    var selectorModel;
    var dataStore;
    var grid;
    var pageSizeNum;
    var tabs;

    return {

        //Refresh grid, pass params
        RefreshGridData: function () {
            grid.getStore().proxy.conn.jsonData = {};
            grid.getStore().load({ params: { ruleType: ruleTypeParamValue, groupField: SW.UDT.GetGroupField(), groupValue: SW.UDT.GetGroupValue()} });

        },

        //init!
        init: function () {

            $("#groupByProperty").val(ORION.Prefs.load("UdtWhiteListGroupBy", "Type")).change(SW.UDT.LoadGroupByValues);

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        id: 'pagingStore',
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=UDT.Strings;K=UDTWEBJS_VB1_34;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.onClick("refresh");
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel({ width: 25 });
            selectorModel.on("selectionchange", updateToolbarButtons);

            //Override Ext.grid.CheckboxSelectionModel behavior to set isAllSelected variable for Delete dialog popup message.
            Ext.grid.CheckboxSelectionModel.override({
                // override mouse down on header event
                onHdMouseDown: function (e, t) {
                    if (t.className == "x-grid3-hd-checker") {
                        e.stopEvent();
                        var hd = Ext.fly(t.parentNode);
                        var isChecked = hd.hasClass("x-grid3-hd-checker-on");

                        if (this.fireEvent("beforecheckallclick", this, isChecked) === false) {
                            return;
                        }

                        if (isChecked) {
                            isAllSelected = false;
                            hd.removeClass("x-grid3-hd-checker-on");
                            this.clearSelections();
                        } else {
                            isAllSelected = true;
                            hd.addClass("x-grid3-hd-checker-on");
                            this.selectAll();
                        }

                        this.fireEvent("aftercheckallclick", this, !isChecked);
                    }
                }
            });

            //get whitelist data
            dataStore = new ORION.WebServiceStore(
            "/Orion/UDT/Services/ManageWhiteList.asmx/GetWhiteListRules",
                [
                { name: 'ID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Type', mapping: 2 },
                { name: 'Target', mapping: 3 },
                { name: 'Description', mapping: 4 },
                { name: 'Status', mapping: 5 },
                { name: 'LastEdited', mapping: 6 }
                ],
                "Name", "ASC");

            //render tab panel
            tabs = new Ext.TabPanel({
                renderTo: 'WhiteListTabs',
                activeTab: 0,
                width: 600,
                height: 0,
                plain: true,
                defaults: { autoScroll: true },
                items: [{
                    itemId: 2,
                    title: '@{R=UDT.Strings;K=UDTWEBJS_MK1_9;E=js}',
                    listeners: { activate: handleActivate }
                }, {
                    itemId: 1,
                    title: '@{R=UDT.Strings;K=UDTWEBJS_MK1_10;E=js}',
                    listeners: { activate: handleActivate }
                }]
            });

            //render grid
            grid = new Ext.grid.GridPanel({

                store: dataStore,
                // ensure we prevent columns from being located to other positions
                enableColumnMove: false,

                columns: [
                    selectorModel,
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_1;E=js}', width: 350, sortable: true, dataIndex: 'Name', renderer: renderText },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_2;E=js}', width: 100, sortable: true, dataIndex: 'Type', renderer: renderText },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_3;E=js}', width: 100, sortable: true, dataIndex: 'Target', renderer: renderText },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_4;E=js}', width: 300, sortable: true, dataIndex: 'Description', renderer: renderText },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_5;E=js}', width: 130, sortable: true, dataIndex: 'Status', renderer: renderStatus },
                        { header: '@{R=UDT.Strings;K=UDTWEBJS_MK1_6;E=js}', width: 180, sortable: true, dataIndex: 'LastEdited', renderer: renderText }
                ],

                sm: selectorModel,

                viewConfig: {

                    //set to true if you want to resize each column with the window resize.  False maintains existing column sizes.
                    forceFit: false,
                    emptyText: '@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}'
                },
                stripeRows: true,

                // width: 700,
                height: 350,

                tbar: [
                    {
                        id: 'AddButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_MK1_11;E=js}',
                        iconCls: 'add',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            addRule();
                        }
                    }, '-',
                    {
                        id: 'EditButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_MK1_12;E=js}',
                        iconCls: 'edit',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            editRule();
                        }
                    }, '-',
                    {
                        id: 'DisableButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_MK1_13;E=js}',
                        iconCls: 'disable',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            disableRule();
                        }
                    }, '-',
                    {
                        id: 'EnableButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_MK1_14;E=js}',
                        iconCls: 'enable',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            enableRule();
                        }
                    }, '-',
                    {
                        id: 'DeleteButton',
                        text: '@{R=UDT.Strings;K=UDTWEBJS_MK1_15;E=js}',
                        iconCls: 'del',
                        handler: function () {
                            //if (IsDemoMode()) return DemoModeMessage();
                            deleteRule();
                        }
                    }
                ],

                // bottom toolbar
                bbar: new Ext.PagingToolbar({
                    id: 'manageWhiteListPager',
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=UDT.Strings;K=UDTWEBJS_VB1_33;E=js}',
                    emptyMsg: "@{R=UDT.Strings;K=UDTWEBJS_VB1_28;E=js}"
                })
            });

            //update web service call params on refresh
            grid.getView().on('refresh', function () {
                grid.store.setBaseParam('ruleType', ruleTypeParamValue);
                grid.store.setBaseParam('groupField', SW.UDT.GetGroupField());
                grid.store.setBaseParam('groupValue', SW.UDT.GetGroupValue());
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('WhiteListGrid');

            grid.getSelectionModel().on("beforecheckallclick", beforeCheckAllClickHandler);
            grid.getSelectionModel().on("aftercheckallclick", afterCheckAllClickHandler);

            updateToolbarButtons();
            SW.UDT.LoadGroupByValues();
            grid.bottomToolbar.addPageSizer();

            // resize the grid
            gridResize();

            // initial load of grid data
            grid.store.proxy.conn.jsonData = {};

            // load the grid with data, pass params
            grid.store.load({ params: { ruleType: ruleTypeParamValue, groupField: SW.UDT.GetGroupField(), groupValue: SW.UDT.GetGroupValue()} });

        }
    };

} ();

//hook onready to initialize
Ext.onReady(SW.UDT.ManageWhiteList.init, SW.UDT.ManageWhiteList);