﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_UDT_Admin_WhiteList_ManageWhiteList : System.Web.UI.Page
{
    internal bool _isOrionDemoMode = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        PageTitleBar.PageTitle = Page.Title;

        //hide the customize page link for this page
        PageTitleBar.HideCustomizePageLink = true;

        PageTitleBar.HelpFragment = "OrionUDTPHManageWhiteList";

        PageSubTitle.Text = Resources.UDTWebContent.UDTWEBDATA_MK1_10;

        if (HttpContext.Current.Request.QueryString["testText"] != null)
        {
            TestText.Text = HttpContext.Current.Request.QueryString["testText"].ToString();
        }
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        
    }
    

    protected override void OnInit(EventArgs e)
    {
        if ((!Profile.AllowAdmin) && (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWCODE_VB1_1));
        }

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }

        base.OnInit(e);
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }
}