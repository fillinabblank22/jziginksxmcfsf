﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Default" 
MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_27 %>"%>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/WizardProgress.ascx" TagPrefix="orion" TagName="WhiteListWorkflowProgress" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/SingleDevice.ascx" TagPrefix="udt" TagName="DeviceList" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/IPRange.ascx" TagPrefix="udt" TagName="IPRangeList" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/MacRange.ascx" TagPrefix="udt" TagName="MacRangeList" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/Subnet.ascx" TagPrefix="udt" TagName="SubnetList" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/Custom.ascx" TagPrefix="udt" TagName="CustomList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/discovery.css" />
    <style type="text/css">
        .sw-hatchbox {
            width: 288px !important;
        }
        .selector
        {
            margin: 0px;
            border: 1px solid #707070;
            width: 100%;
            list-style-type: none;
            padding: 0px;
            
        }
        .selector li
        {
            padding: 3px;

        }
        .header {
            background-color: #707070;
            color: #ffffff;
            font-weight: bold;
        }
        .selected 
        {
            font-weight: bold;
            background-color: #ffffff;
        }
        .deselected
        {
            font-weight: normal;
            background-color: #e0e0e0;
        }
        
        .InputContainer {
            padding: 5px;
        }
        
        .InputBox {
            background-color: #ECF6FB;
            padding: 10px;
            display: block; 
        }
        
        .sw-pg-hint-inner { border: 1px solid #eaca7f; border-radius: 5px; background: #fffdcc; }
        .sw-pg-hint-body { margin: 6px 4px 10px 4px; padding-left: 22px;padding-right: 2px; background: url(/orion/images/icon.lightbulb.gif) top left no-repeat; }
        .sw-pg-align { padding-left: 110px; }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionUDTPHAddDeviceWhiteList" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
    
    <div id="UdtWhiteListContent" style="padding-left:10px; padding-right:10px; padding-bottom:10px;">

     <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1>
                    <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
                    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_27 %>
                    <% } else { %>
                    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_28 %>
                    <% } %>
                    
                </h1>
            </td>
            <td align="right" style="vertical-align: top; width: 180px;">
            </td>
        </tr>
    </table>
    <p>
        <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_29 %>
        <% } else { %>
        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_30 %>
        <% } %>
    </p>
    <div class="GroupBoxAccounts" id="IncludedItemsContainer" style="min-width: 480px;">
    <orion:WhiteListWorkflowProgress ID="ProgressIndicator" runat="server" />

    <asp:UpdatePanel runat="server" ID="ParentUpdatePanel">
        <ContentTemplate>
            <div id="IncludedItems" style="padding: 10px 10px 5px 10px;">
                <div style="padding-bottom:8px;">
                    <table width="100%">
                        <tr valign="top">
                            <td style="width: 1150px">
                                <h2 style="padding-left:0px">
                                        <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
                                        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_31 %>
                                        <% } else { %>
                                        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_32 %>
                                        <% } %>
                                </h2>
                                <%= Resources.UDTWebContent.UDTWEBDATA_MK1_33 %>
                            </td>
                         </tr>
                    
                    </table>
                </div>
                
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:170px; vertical-align: top;">
                            
                            <asp:ListView runat="server" ID="SelectionList" OnSelectedIndexChanging="SelectionList_SelectedIndexChanging">
                                
                                <LayoutTemplate>
                                    <ul class="selector"><li class="header"><asp:Literal runat="server" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_34 %>"></asp:Literal></li></ul>
                                    <ul class="selector">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li class="deselected"><asp:LinkButton runat="server" ID="SelectCategoryButton" 
                Text="<%# Container.DataItem%>" CommandName="Select" /></li>
                                </ItemTemplate>
                                <SelectedItemTemplate>
                                    <li class="selected"><%# Container.DataItem%>
                                    
                                    </li>
                                </SelectedItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <div style="min-height:300px; margin-left: 16px; margin-bottom: 5px; border: 1px solid #707070">
                            <ul class="selector"><li class="header">Input</li></ul>
                            <table><tr><td>
                            <div runat="server" id="ControlBox" class="InputContainer">
                                <udt:DeviceList runat="server" id="deviceList" />
                                <udt:IPRangeList runat="server" id="ipRangeList" />
                                <udt:MacRangeList runat="server" id="macRangeList" />
                                <udt:SubnetList runat="server" id="subnetList" />
                                <udt:CustomList runat="server" id="customList" />
                            </div>
                            </td></tr></table>
                            </div>
                        </td>
                    </tr>
                </table>
         
            </div>
            

        </ContentTemplate>
    </asp:UpdatePanel>
    

    </div>
    
                    <table width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                   
                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right; padding-right:8px; padding-top:5px;">


                            <asp:Panel ID="WhiteListButtonPanel" runat="server" style="margin-left: 15px;">
                                <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="LocalizableButton1" runat="server" DisplayType="Primary" LocalizedText="Next" OnClick="btnNext_Click" OnClientClick="" />
                                <orion:LocalizableButton id="LocalizableButton2" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                                </div>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>   

    </div>
</asp:Content>
