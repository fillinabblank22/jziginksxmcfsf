﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Common;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using System.Web.Caching;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Description : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private const string cacheKey = "UDTWhiteListWizardInProgressSafeList";
    private const int safeLimit = 5;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            WhiteListWorkflowManager.CurrentStep = WhiteListWorkflowManager.WhiteListStep.Description;
            RuleName.Text = WhiteListWorkflowManager.RuleName;
            RuleDescription.Text = WhiteListWorkflowManager.RuleDescription;
            RuleState.SelectedValue = WhiteListWorkflowManager.RuleEnabled == true ? "1" :"0";
            BindSafeList();
            
        }

        //Redirect if you got here without an existing WhiteListWorkflowManager session
        if (!WhiteListWorkflowManager.Exists())
        {
            log.ErrorFormat("White List page was loaded without an existing WhiteListWorkflowManager session.  Page Redirected to {0}.", WhiteListWorkflowManager.DefaultUrl());
            Response.Redirect(WhiteListWorkflowManager.DefaultUrl());
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if ((!Profile.AllowAdmin) || (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer))
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWCODE_VB1_1));
        }

        base.OnInit(e);
    }

    private string[] GetSafeList()
    {
        string[] safeList;
        WhiteListWorkflowManager.RuleName = RuleName.Text;
        WhiteListWorkflowManager.RuleDescription = RuleDescription.Text;
        WhiteListWorkflowManager.RuleEnabled = (RuleState.SelectedValue == "1");
        UDTRuleBase[] rule = new List<UDTRuleBase>() { WhiteListWorkflowManager.GetRule() }.ToArray();

        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                safeList = bl.RulesApplyRulesToCurrentDataForPassedRulesOnly(rule, true);
            }

        }
        catch (Exception ex)
        {
            log.Error("Unable to create Whitelist rule.", ex);
            throw new ApplicationException("Unable to create Whitelist rule. \n" + ex.Message, ex);
        }
        return safeList;
    }
    private void BindSafeList()
    {
        //get safe list
        string[] safeList = GetSafeList();

        //cache safeList
        HttpContext.Current.Cache.Remove(cacheKey);
        HttpContext.Current.Cache.Insert(cacheKey, safeList, null, DateTime.UtcNow + TimeSpan.FromSeconds(600), Cache.NoSlidingExpiration);


        UpdateSafeDevicesList();


    }

    private void UpdateSafeDevicesList()
    {

        string[] safeList = HttpContext.Current.Cache[cacheKey] as string[];
        if (safeList == null) safeList = GetSafeList();

        //filter by search text
        var searchText = SearchSafeList.Text;
        var searchRegEx = new Regex(searchText.Replace("*",".*"));
        var filtered = safeList.Where(device => searchRegEx.IsMatch(device)).ToArray();

        var message = (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) ? Resources.UDTWebContent.UDTWEBDATA_MK1_61 : Resources.UDTWebContent.UDTWEBDATA_MK1_62;
        object showAll = ViewState["ShowAll"];
        if (showAll != null && (bool)showAll)
        {
            SafeListCurrent.DataSource = filtered;
            SafeLabel.Visible = false;
            ShowAll.Visible = false;
        }
        else
        {
            SafeListCurrent.DataSource = filtered.Take(safeLimit);
            SafeLabel.Visible = true;
            ShowAll.Visible = true;
            var count = filtered.Length;
            if ((count - safeLimit) > 0)
            {
                count = count - safeLimit;
            } else
            {
                count = 0;
                ShowAll.Visible = false;
            }
            SafeLabel.Text = String.Format(Resources.UDTWebContent.UDTWEBDATA_MK1_65, count);
        }

        SafeListCurrent.DataBind();
        SafeDevicesCurrentMessage.Text = String.Format("<B>" + message + "</B>", safeList.Length);  
    }

    protected void ShowAll_Click(object sender, EventArgs e)
    {
        ViewState["ShowAll"] = true;
        UpdateSafeDevicesList();
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        ViewState["ShowAll"] = false;
        UpdateSafeDevicesList();
    }


    protected void ExpanderButton_OnClick(object sender, EventArgs e)
    {
        if (ExpanderButton.CssClass == "Expand")
        {
            ExpanderButton.CssClass = "Collapse";
            CurrentSafeDevicesDetail.Visible = true;
        }
        else
        {
            ExpanderButton.CssClass = "Expand";
            CurrentSafeDevicesDetail.Visible = false;
        }

    }

    protected void RuleName_Changed(object sender, EventArgs e)
    {
        WhiteListWorkflowManager.RuleName = RuleName.Text;
    }

    protected void RuleDescription_Changed(object sender, EventArgs e)
    {
        WhiteListWorkflowManager.RuleDescription = RuleDescription.Text;
    }

    protected void RuleState_Changed(object sender, EventArgs e)
    {
        WhiteListWorkflowManager.RuleEnabled = (RuleState.SelectedValue == "1");
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(WhiteListWorkflowManager.Next(WhiteListWorkflowManager.WhiteListStep.Default));
    }

    protected void btnFinish_Click(object sender, EventArgs e)
    {
        if (!CheckEmpty()) return;

        WhiteListWorkflowManager.RuleName = RuleName.Text;
        WhiteListWorkflowManager.RuleDescription = RuleDescription.Text;
        WhiteListWorkflowManager.RuleEnabled = (RuleState.SelectedValue == "1");

        UDTRuleBase[] rule = new List<UDTRuleBase>() {WhiteListWorkflowManager.GetRule()}.ToArray();
        
        try
        {
            using (IUDTBusinessLayer bl = BusinessLayerFactory.Create())
            {
                bl.RulesSet(rule);
            }

        }
        catch (Exception ex)
        {
            log.Error("Unable to create Whitelist rule.", ex);
            throw new ApplicationException("Unable to create Whitelist rule. \n" + ex.Message, ex);
        }
        Page.Response.Redirect(WhiteListWorkflowManager.Next(WhiteListWorkflowManager.WhiteListStep.Completed));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(WhiteListWorkflowManager.Cancel());
    }

    public bool CheckEmpty()
    {
        this.EmptyValidator.IsValid = (!String.IsNullOrEmpty(RuleName.Text));
        return this.EmptyValidator.IsValid;
    }
}