﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Common.Models;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //If we came from Manage White List grid page, then cancel any existing Workflow.
            if ((Page.Request.UrlReferrer != null) && (Page.Request.UrlReferrer.Segments[Page.Request.UrlReferrer.Segments.Length - 1] == "ManageWhiteList.aspx")) 
                WhiteListWorkflowManager.Cancel();

            WhiteListWorkflowManager.Init();
            WhiteListWorkflowManager.CurrentStep = WhiteListWorkflowManager.WhiteListStep.Default;
            if (this.Context.Request.QueryString["type"] != null)
            {
                WhiteListWorkflowManager.RuleType = (UDTRuleType)Convert.ToInt32(Context.Request.QueryString["type"]);
            }
            if (this.Context.Request.QueryString["edit"] != null)
            {
                WhiteListWorkflowManager.Mode = EditMode;
            }
            if (this.Context.Request.QueryString["ruleid"] != null)
            {
                WhiteListWorkflowManager.LoadRule(RuleId);
            }

            //Check for endpoint/endpointtype params.
            //These passed from Endpoint Details resource: "Add to Whitelist"
            if ((this.Context.Request.QueryString["endpoint"] != null) &&
                (this.Context.Request.QueryString["endpointType"] != null))
            {
                var endpoint = this.Context.Request.QueryString["endpoint"];
                var endpointType = this.Context.Request.QueryString["endpointType"];
                WhiteListWorkflowManager.SelectionMethod = WhiteListSelectionMethod.Device;
                switch (endpointType)
                {
                    case "UE-IP":
                        WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.IPAddress;
                        break;
                    case "UE-MAC":
                        WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.MACAddress;
                        break;
                    case "UE-DNS":
                        WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.DNSName;
                        break;
                }
                WhiteListWorkflowManager.DeviceList.Values = new List<string>();
                WhiteListWorkflowManager.DeviceList.Values.Add(endpoint);
                
            }

            DataBind();
            Select(WhiteListWorkflowManager.SelectionMethod);
        } 

    }

    protected override void OnInit(EventArgs e)
    {
        if ((!Profile.AllowAdmin) &&(!(SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)))
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWCODE_VB1_1));
        }

        base.OnInit(e);
    }

    protected UDTRuleType RuleType
    {
        get
        {
            int typeParam;
            int.TryParse(this.Context.Request.QueryString["type"], out typeParam);
            switch (typeParam)
            {
                case (int)UDTRuleType.Whitelist:
                    return UDTRuleType.Whitelist;
                case (int)UDTRuleType.Exclusion:
                    return UDTRuleType.Exclusion;
                default:
                    return UDTRuleType.Whitelist;
            }
        }
    }


    protected WhiteListWorkflowManager.WhiteListMode EditMode
    {
        get
        {
            int editParam;
            int.TryParse(this.Context.Request.QueryString["edit"], out editParam);
            switch (editParam)
            {
                case 0:
                    return WhiteListWorkflowManager.WhiteListMode.CreateNew;
                case 1:
                    return WhiteListWorkflowManager.WhiteListMode.Edit;
                default:
                    return WhiteListWorkflowManager.WhiteListMode.CreateNew;
            }
        }
    }

    protected int RuleId
    {
        get
        {
            int ruleId;
            int.TryParse(this.Context.Request.QueryString["ruleid"], out ruleId);
            return ruleId;
        }
    }


    protected WhiteListSelectionMethod SelectionMethod
    {
        get
        {
            return WhiteListWorkflowManager.SelectionMethod;
        }
        set
        {
            WhiteListWorkflowManager.SelectionMethod = value;
        }
    }

    private new void DataBind()
    {
        SelectionList.DataSource = new List<string> { 
             Resources.UDTWebContent.UDTWEBDATA_MK1_35,
             Resources.UDTWebContent.UDTWEBDATA_MK1_36, 
             Resources.UDTWebContent.UDTWEBDATA_MK1_37, 
             Resources.UDTWebContent.UDTWEBDATA_MK1_38,
             Resources.UDTWebContent.UDTWEBDATA_MK1_39
        };
        SelectionList.DataBind();
    }

    private void Select(WhiteListSelectionMethod method)
    {
        SelectionList.SelectItem((int)method);
    }

    protected bool ValidateUserInput()
    {
        switch (this.SelectionMethod)
        {
            case WhiteListSelectionMethod.Device:
                deviceList.DeleteEmptyRows();
                return deviceList.CheckEmpty();
            case WhiteListSelectionMethod.IPRange:
                WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.IPAddress;
                ipRangeList.DeleteEmptyRows();
                return ipRangeList.CheckEmpty();
            case WhiteListSelectionMethod.MacRange:
                WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.MACAddress;
                macRangeList.DeleteEmptyRows();
                return macRangeList.CheckEmpty();
            case WhiteListSelectionMethod.Subnet:
                WhiteListWorkflowManager.SelectedTarget = UDTRuleTarget.IPAddress;
                return subnetList.CheckEmpty();
            case WhiteListSelectionMethod.Custom:
                customList.DeleteEmptyRows();
                return customList.CheckEmpty();           
            default:
                return true;
        }
    }

    protected void ShowSelectionMethod()
    {
        WhiteListSelectionMethod method = this.SelectionMethod;
        deviceList.Visible = (method == WhiteListSelectionMethod.Device);
        ipRangeList.Visible = (method == WhiteListSelectionMethod.IPRange);
        macRangeList.Visible = (method == WhiteListSelectionMethod.MacRange);
        subnetList.Visible = (method == WhiteListSelectionMethod.Subnet);
        customList.Visible = (method == WhiteListSelectionMethod.Custom);
        
    }

    protected void SelectionList_SelectedIndexChanging(object sender, ListViewSelectEventArgs e)
    {
        SelectionList.SelectedIndex = e.NewSelectedIndex;
        SelectionMethod = (WhiteListSelectionMethod)e.NewSelectedIndex;
        ShowSelectionMethod();
        DataBind();

    }

    protected void btnNext_Click(object sender, EventArgs e)
    {

        if (ValidateUserInput() && Page.IsValid) Page.Response.Redirect(WhiteListWorkflowManager.Next(WhiteListWorkflowManager.WhiteListStep.Description));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect(WhiteListWorkflowManager.Cancel());
    }
}