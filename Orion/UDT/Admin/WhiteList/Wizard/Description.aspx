﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Description.aspx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Description" 
MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" Title="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_27 %>"%>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/UDT/Admin/WhiteList/Wizard/Controls/WizardProgress.ascx" TagPrefix="orion" TagName="WhiteListWorkflowProgress" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>


<asp:Content ID="Content2" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <style type="text/css">
      .WhiteListDescriptionTable tr td {
            vertical-align: top;
            padding-left: 0px;
            padding-right: 8px;
            padding-top: 4px;
            margin: 0px;
        }
        
    .SafeList 
    {
        padding: 4px 0px 4px 3px;
        font-weight: normal;
        font-size: 9pt;
        color: #000000;
    }
    
    .Expand
    {
        background-image: url(/Orion/images/Button.Expand.GIF);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
        color: #000000;
    }

    .Collapse
    {
        background-image: url(/Orion/images/Button.Collapse.GIF);
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0px 2px 20px;
        font-size: 9pt;
        color: #000000;
    }
    
    .Description {
        width: 380px;
        height: 120px;
    }
    
    .Name {
         width: 380px;
    }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionUDTPHConfigSettingsDiscoverPorts" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
    
    <div id="UdtWhiteListContent" style="padding-left:10px; padding-right:10px; padding-bottom:10px;">

     <table width="100%" id="breadcrumb">
        <tr>
            <td>
                <h1><% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
                    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_27 %>
                    <% } else { %>
                    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_28 %>
                    <% } %></h1>
            </td>
            <td align="right" style="vertical-align: top; width: 180px;">
            </td>
        </tr>
    </table>
    <p> <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_29 %>
        <% } else { %>
        <%= Resources.UDTWebContent.UDTWEBDATA_MK1_30 %>
        <% } %></p>
    <div class="GroupBoxAccounts" id="DescriptionContainer" style="min-width: 480px;">
    <orion:WhiteListWorkflowProgress ID="ProgressIndicator" runat="server" />

    <asp:UpdatePanel runat="server" ID="ParentUpdatePanel">
        <ContentTemplate>
            <div id="IncludedItems" style="padding: 10px 10px 5px 10px;">
                <div style="padding-bottom:8px;">
                    <table width="100%">
                        <tr valign="top">
                            <td style="width: 1150px">
                                <h2 style="padding-left:0px"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_63 %></h2>
                                <%= Resources.UDTWebContent.UDTWEBDATA_MK1_64 %>
                                <table class="WhiteListDescriptionTable">
                                    <tr>
                                        <td><%= Resources.UDTWebContent.UDTWEBDATA_MK1_56 %></td><td><asp:TextBox ID="RuleName" CssClass="Name" runat="server" OnTextChanged="RuleName_Changed"></asp:TextBox>
                                            <asp:CustomValidator
                                                ID="EmptyValidator" 
                                                runat="server" 
                                                ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_69 %>" 
                                                EnableClientScript="false"
                                                Display="Dynamic">
                                            </asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><%= Resources.UDTWebContent.UDTWEBDATA_MK1_57 %></td><td><asp:TextBox CssClass="Description" ID="RuleDescription" OnTextChanged="RuleDescription_Changed" Text="" runat="server" TextMode="MultiLine" ></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td><%= Resources.UDTWebContent.UDTWEBDATA_MK1_58 %></td><td><asp:RadioButtonList ID="RuleState" OnSelectedIndexChanged="RuleState_Changed" runat="server">
                                                              <asp:ListItem
                                                                Enabled="True"
                                                                Selected="True"
                                                                Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_22 %>"
                                                                Value="1"
                                                            />  
                                                            <asp:ListItem
                                                                Enabled="True"
                                                                Selected="False"
                                                                Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_23 %>"
                                                                Value="0"
                                                            />  
                                                            </asp:RadioButtonList></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
                                            <%= Resources.UDTWebContent.UDTWEBDATA_MK1_59 %>
                                            <% } else { %>
                                            <%= Resources.UDTWebContent.UDTWEBDATA_MK1_60 %>
                                            <% } %>
                                        </td>
                                        <td>
                                            <asp:Panel ID="CurrentSafeDevices" runat="server" CssClass="SafeList" Visible="true">
                                            <asp:LinkButton ID="ExpanderButton" runat="server" CssClass="Expand" OnClick="ExpanderButton_OnClick"><asp:Literal ID="SafeDevicesCurrentMessage" runat="server" /></asp:LinkButton>
		                                    <div ID="CurrentSafeDevicesDetail" runat="server" style="background-color:#e4f1f8; margin-top: 4px; padding: 2px;" visible="false">
                                                <asp:Repeater ID="SafeListCurrent" runat="server">
                                                    <HeaderTemplate>
                                                        <ul >
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                            <li class="SafeList"><%# Container.DataItem %></li>
                                                           
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </ul>
                                                        
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                
                                                
                                                
                                                <div style="padding:5px;">
                                                    <table style="padding:5px;background-color:#fffdcc; width:100%;">
                                                        <tr>
                                                            <td style="width:100%; font-size: 10px"> 
                                                                <asp:Literal ID="SafeLabel" runat="server"></asp:Literal>
                                                            </td>
                                                            <td style="text-align: right; white-space: nowrap; font-size: 10px">
                                                                &nbsp;<asp:LinkButton runat="server" ID="ShowAll" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_66 %>" OnClick="ShowAll_Click"></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr style="vertical-align:middle;">
                                                            <td style="vertical-align:middle;">
                                                                <asp:TextBox runat="server" ID="SearchSafeList" Width ="100%"></asp:TextBox>
                                                            </td>
                                                            <td style="vertical-align:middle;">
                                                                <orion:LocalizableButton id="SearchButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_67 %>" OnClick="SearchButton_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                
                                                </div>

                                                
                                            </div>

                                            </asp:Panel>
                                            
                                        </td>

                                    </tr>
                                </table>
                                

                            </td>
                         </tr>
                    
                    </table>
                </div>
                
                
            </div>
            

        </ContentTemplate>
    </asp:UpdatePanel>
    

    </div>
    
                    <table width="100%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                   
                    <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right; padding-right:8px; padding-top:5px;">


                            <asp:Panel ID="WhiteListButtonPanel" runat="server" style="margin-left: 15px;">
                                <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="LocalizableButton3" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="btnBack_Click" OnClientClick="" />
                                <orion:LocalizableButton id="LocalizableButton1" runat="server" DisplayType="Primary" LocalizedText="Finish" OnClick="btnFinish_Click" OnClientClick="" />
                                <orion:LocalizableButton id="LocalizableButton2" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false" />
                                </div>
                            </asp:Panel>

                        </td>
                    </tr>
                </table>   
    </div>
</asp:Content>