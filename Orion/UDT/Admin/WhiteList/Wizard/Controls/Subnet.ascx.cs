﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;


public partial class Orion_UDT_Admin_WhiteList_Wizard_Controls_Subnet : System.Web.UI.UserControl
{
	private uint lastClassID = 0;

	/// <summary>
	/// collection of entered subnets in session
	/// </summary>
    protected UDTRuleIPSubnetList Subnets
	{
		get
		{
            return WhiteListWorkflowManager.IPSubnetList;
		}
	}

	/// <summary>
	/// collection of all group with their collapse status
	/// </summary>
	protected Dictionary<uint, bool> SubnetClasses
	{
		get 
		{
			Dictionary<uint, bool> sc = this.ViewState["SubnetClasses"] as Dictionary<uint, bool>;
			if (sc == null)
			{
				sc = new Dictionary<uint, bool>();
				this.ViewState["SubnetClasses"] = sc;
			}
			return sc;
		}
	}

    protected void Page_Init(object sender, EventArgs e)
    {
        RegularExpressionValidator r = (RegularExpressionValidator)AddSubnetDialog.FindControl("SubnetAddressValidator");
        r.ErrorMessage = Resources.UDTWebContent.UDTWEBDATA_GK1_5;
    }
   

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
           this.BindSubnets();
		}
        Update();
	}

	protected void BindSubnets()
	{
        if (WhiteListWorkflowManager.Mode == WhiteListWorkflowManager.WhiteListMode.Edit)
        {
            this.SubnetGrid.EditIndex =-1;
        }
        if (Subnets !=null)
	    {
            this.SubnetGrid.DataSource = Subnets.SubnetList;
            this.SubnetGrid.DataBind();    
	    }
	}
    private bool Update()
    {
       
        for (var i = 0; i < SubnetGrid.Items.Count; i++)
        {
            if (i >= Subnets.SubnetList.Count) break;
            Control item = SubnetGrid.Items[i];
            var txtSubNetIP = (TextBox)item.FindControl("txtSubNetIP");
            var txtSubNetMask = (TextBox)item.FindControl("txtSubNetMASK");
            if (txtSubNetIP == null || txtSubNetMask == null) continue;
            var ipValidator = (CustomValidator)item.FindControl("IPv4v6Validator");
            if (ipValidator == null) continue;
            if (!ipValidator.IsValid) return false;
            Subnets.SubnetList[i].IPAddress = txtSubNetIP.Text;
            Subnets.SubnetList[i].SubnetMask = txtSubNetMask.Text;
        }
        return true;
    }

	protected void SubnetGrid_ItemDataBound(object sender, ListViewItemEventArgs e)
	{
		if(e.Item.ItemType == ListViewItemType.DataItem)
		{
			Subnet s = ((ListViewDataItem)e.Item).DataItem as Subnet;
			if(s != null)
			{
				if(s.SubnetClassID != this.lastClassID)
				{
					e.Item.FindControl("groupHeader").Visible = true;
					((Label)e.Item.FindControl("groupLabel")).Text = s.SubnetClass;
					this.lastClassID = s.SubnetClassID;
					ImageButton collapseButton = e.Item.FindControl("CollapseGroupButton") as ImageButton;
					collapseButton.CommandArgument = s.SubnetClassID.ToString();
					
					if (!this.SubnetClasses.ContainsKey(s.SubnetClassID))
					{
						this.SubnetClasses[s.SubnetClassID] = true;
					}
					if (!SubnetClasses[s.SubnetClassID])
						collapseButton.ImageUrl = "/Orion/images/Button.Expand.gif";
					else
						collapseButton.ImageUrl = "/Orion/images/Button.Collapse.gif";
				}
				e.Item.FindControl("DataRow").Visible = this.SubnetClasses[s.SubnetClassID];
                //var ipValidator = (CustomValidator)e.Item.FindControl("IPv4v6Validator");
                //ipValidator.Enabled = true;
			}
		}
	}

	protected void CollapseGroup_Click(object sender, CommandEventArgs e)
	{
			uint classID = Convert.ToUInt32(e.CommandArgument);
			if (this.SubnetClasses.ContainsKey(classID))
			{
				this.SubnetClasses[classID] = !this.SubnetClasses[classID];
				this.BindSubnets();
			}
	}
    protected void SubNetEdit_Click(object sender, CommandEventArgs e)
    {
        if (e.CommandName != "Update" || !Page.IsValid) return;
        if(!(Update()))
            return;

        SubnetGrid.EditIndex = -1;
        SubnetGrid.DataSource = Subnets.SubnetList;
        SubnetGrid.DataBind();
    }

	protected void AddSubnetBtn_OnClick(object sender, EventArgs e)
	{
		this.AddSubnetDialog.OpenDialog();
	}

	/// <summary> Called when AddSubnet dialog is closed and its subnet should be added </summary>
	protected void AddSubnetDialog_SubnetAdded(Subnet subnet)
	{
        if (subnet.SubnetIP != null && subnet.SubnetMask !=null)
        {  

            Subnets.SubnetList.Add(new UDTSubnet() { IPAddress = subnet.SubnetIP, SubnetMask = subnet.SubnetMask });    
	    }
		this.BindSubnets();
	}


    /// <summary> Checks if there are any subnets entered - only the checked ones </summary>
	/// <returns> true if there is at least one checked subnet </returns>
    public bool CheckEmpty()
    {
        SubnetGridRequiredValidator.IsValid = Subnets.SubnetList.Exists(s => !String.IsNullOrEmpty(s.IPAddress));
        if (Subnets.SubnetList.Count == 0) SubnetGridRequiredValidator.IsValid = false;
        return SubnetGridRequiredValidator.IsValid;
    }
    protected void SubnetGrid_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        if (!Page.IsValid) return;
        SubnetGrid.EditIndex = e.NewEditIndex;
        SubnetGrid.DataSource = Subnets.SubnetList;
        SubnetGrid.DataBind();
    }
    protected void SubNetIPv4v6Validate(object source, ServerValidateEventArgs args)
    {  
        var cv = (CustomValidator)source;
        var rowIndex = ((ListViewDataItem)cv.NamingContainer).DataItemIndex;
        Control item = SubnetGrid.Items[rowIndex];
        var ipTextBox = (TextBox)item.FindControl("txtSubNetIP");
        var maskTextBox = (TextBox)item.FindControl("txtSubNetMASK");
        IPAddress ip;
        if (IPAddress.TryParse(ipTextBox.Text, out ip))
        {
            ipTextBox.Text = ip.ToStringIp();
            args.IsValid = (ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6);
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void SubNetMASKValidate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((ListViewDataItem)cv.NamingContainer).DataItemIndex;
        Control item = SubnetGrid.Items[rowIndex];
        var maskTextBox = (TextBox)item.FindControl("txtSubNetMASK");
        IPAddress mask;
        if (IPAddress.TryParse(maskTextBox.Text, out mask))
        {
            maskTextBox.Text = mask.ToStringIp();
            args.IsValid = (mask.AddressFamily == AddressFamily.InterNetwork) || (mask.AddressFamily == AddressFamily.InterNetworkV6);
        }
        else
        {
            args.IsValid = false;
        }
    }
}
