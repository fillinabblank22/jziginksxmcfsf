﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Subnet.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_Subnet" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Src="~/Orion/Discovery/Controls/AddSubnetDialog.ascx" TagPrefix="orion" TagName="AddSubnetDialog" %>

<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<style type="text/css">
    .NetObjectTable TH {white-space:normal !important;}
</style>
<div style="padding:5px;">
    <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_48 %>
    <% } else { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_49 %>
    <% } %>
</div>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
<asp:UpdatePanel ID="SubnetUpdatePanel" runat="server">
    <ContentTemplate>        
        <orion:AddSubnetDialog ID="AddSubnetDialog" runat="server" OnSubnetAdded="AddSubnetDialog_SubnetAdded"/>

        <table class="NetObjectTable SubnetTable" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
            <tr class="ButtonHeader">
                <th colspan="5" style="border-bottom: solid 1px #FFFFFF;">
                    <asp:ImageButton ID="AddSubnetBtn" runat="server" ImageUrl="~/Orion/Discovery/images/icon_add.gif" 
                        OnClick="AddSubnetBtn_OnClick" CausesValidation="false" />
                    <asp:LinkButton ID="AddSubnetLinkBtn" runat="server" OnClick="AddSubnetBtn_OnClick" CausesValidation="false" >
                        <%= Resources.CoreWebContent.WEBDATA_AK0_99 %>
                    </asp:LinkButton>
                </th>
            </tr>
            <asp:ListView runat="server" ID="SubnetGrid" 
                OnItemDataBound="SubnetGrid_ItemDataBound" 
                onitemediting="SubnetGrid_ItemEditing">
                <LayoutTemplate>
                        <tr class="SubnetGridHeader">
                            <th style="width: 10px;">&nbsp;</th>
                            <th style="width: 200px; border-right: solid 1px #FFFFFF;" colspan="2">
                                <asp:Literal ID="Subnet" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_102 %>" />
                            </th>
                            <th style="width: 200px;">
                                <asp:Literal ID="SubnetMask" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_101 %>" />
                            </th>
                              <th style="width: 200px;">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: UDTWebContent, UDTWEBDATA_MR1_22 %>" /> 
                            </th>
                        </tr>
                        <asp:PlaceHolder runat="server" id="itemPlaceholder" />
                </LayoutTemplate>   
                <ItemTemplate>
                    <tr class="GroupRow" runat="server" ID="groupHeader" Visible="false">
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="CollapseGroupButton" ImageUrl="/Orion/images/Button.Collapse.gif" OnCommand="CollapseGroup_Click" />
                        </td>
                        <td colspan="3">
                            <asp:Label runat="server" ID="groupLabel" />
                        </td>
                        <td></td>
                    </tr>
                    <tr runat="server" ID="DataRow">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("IPAddress") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("SubnetMask") %>' />
                        </td>
                           <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="ImageButton1" ImageUrl="/Orion/images/edit_16x16.gif" OnCommand="SubNetEdit_Click" CommandName="Edit"/>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="GroupRow" runat="server" ID="groupHeader" Visible="false">
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="CollapseGroupButton" ImageUrl="/Orion/images/Button.Collapse.gif" OnCommand="CollapseGroup_Click" />
                        </td>
                        <td colspan="3">
                            <asp:Label runat="server" ID="groupLabel" />
                        </td>
                        <td></td>
                    </tr>
                    <tr class="NetObjectAlternatingRow" runat="server" ID="DataRow">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("IPAddress") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("SubnetMask") %>' />
                        </td>
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="ImageButton1" ImageUrl="/Orion/images/edit_16x16.gif" OnCommand="SubNetEdit_Click" CommandName="Edit"/>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                 <tr class="GroupRow" runat="server" ID="groupHeader" Visible="false">
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="CollapseGroupButton" ImageUrl="/Orion/images/Button.Collapse.gif" OnCommand="CollapseGroup_Click" />
                        </td>
                        <td colspan="3">
                            <asp:Label runat="server" ID="groupLabel" />
                        </td>
                        <td></td>
                    </tr>
                    <tr runat="server" ID="DataRow">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                             <asp:TextBox ID="txtSubNetIP" runat="server" Text='<%# Eval("IPAddress") %>' CausesValidation="true" AutoPostBack="true" />                             
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubNetMASK" runat="server" Text='<%# Eval("SubnetMask") %>' CausesValidation="true" AutoPostBack="true"/>                            
                        </td>
                         <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="ImageButton1" ImageUrl="/Orion/images/edit_16x16.gif" OnCommand="SubNetEdit_Click" CommandName="Update" CausesValidation="true" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr runat="server" ID="Tr1">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                        </td>
                        <td> 
                             <asp:RequiredFieldValidator runat="server" ControlToValidate="txtSubNetIP" ErrorMessage="<%$ Resources: CoreWebContent, WEBDATA_AK0_93 %>"></asp:RequiredFieldValidator>
                             <asp:CustomValidator runat="server" ID="IPv4v6Validator" Display="Dynamic" Enabled="true" ControlToValidate="txtSubNetIP" OnServerValidate="SubNetIPv4v6Validate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSubNetMASK" ErrorMessage="<%$ Resources: CoreWebContent, WEBDATA_AK0_93 %>"></asp:RequiredFieldValidator>
                            <asp:CustomValidator runat="server" ID="IPv4v6Validator1" Display="Dynamic" Enabled="true" ControlToValidate="txtSubNetMASK" OnServerValidate="SubNetMASKValidate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                        </td>
                         <td style="width: 10px;">
                        </td>
                    </tr>
                </EditItemTemplate>
            </asp:ListView>
    </table>
    <asp:CustomValidator
        ID="SubnetGridRequiredValidator" 
        runat="server" 
        ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_103 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
    </ContentTemplate>
</asp:UpdatePanel>
