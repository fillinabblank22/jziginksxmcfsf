﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPRange.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_IPRange" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2">
<div style="padding:5px;">
    <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_52 %>
    <% } else { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_53 %>
    <% } %>
</div>
</td></tr><tr><td>
<div class="InputBox" style="display:block;">
<asp:GridView ID="IPRangeGrid" runat="server" automation="iprangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="IPRangeGrid_RowCommand" OnDataBound="IPRangeGrid_OnDataBound" >
    <Columns>
        <asp:TemplateField  HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_98 %>">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="BeginIPTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 288px;" Text='<%# DataBinder.Eval(Container.DataItem, "Item1") %>' CausesValidation="true" AutoPostBack="true"></asp:TextBox><br/>
                <asp:CustomValidator runat="server" Display="Dynamic" ID="BeginIPv4v6Validator" ControlToValidate="BeginIPTextBox" OnServerValidate="BeginIPv4v6Validate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                
                <asp:CustomValidator
                    ID="IPCompareValidator" 
                    runat="server" 
                    ControlToValidate="BeginIPTextBox"
                    OnServerValidate="CompareIPValidation"
                    ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_97 %>" 
                    EnableClientScript="false"
                    Display="Dynamic" ValidateEmptyText="true">
                </asp:CustomValidator>
                <asp:CustomValidator
                    ID="DuplicityValidator" 
                    runat="server" 
                    ControlToValidate="BeginIPTextBox"
                    OnServerValidate="DuplicityValidation"
                    ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_96 %>" 
                    EnableClientScript="false"
                    Display="Dynamic">
                </asp:CustomValidator>
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                <orion:LocalizableButton id="AndButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_170 %>"  OnClick="AndButton_Clicked"/>
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_95 %>">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="EndIPTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 288px;" Text='<%# DataBinder.Eval(Container.DataItem, "Item2") %>' CausesValidation="true" AutoPostBack="true"></asp:TextBox><br/>
                <asp:CustomValidator runat="server" Display="Dynamic" ID="EndIPv4v6Validator" ControlToValidate="EndIPTextBox" OnServerValidate="EndIPv4v6Validate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                &nbsp;
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow"/>
    </Columns>
</asp:GridView>
<div>
    <br /><br />
    <asp:CustomValidator
        ID="EmptyValidator" 
        runat="server" 
        ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_93 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
</div>
</td><td width="100%">&nbsp;</td></tr>
</table>