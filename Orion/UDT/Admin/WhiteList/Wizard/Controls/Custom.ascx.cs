﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Common.Net;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using System.Text.RegularExpressions;
using SolarWinds.UDT.Web.Helpers;
using System.Text;
using System.Net;
using System.Net.Sockets;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Controls_Custom : System.Web.UI.UserControl
{
 
    protected UDTRulePatternList CustomList
    {
        get
        {
            return WhiteListWorkflowManager.CustomList;
        }
    }

    protected UDTRuleTarget SelectedTarget
    {
        get { return WhiteListWorkflowManager.SelectedTarget; }
        set { WhiteListWorkflowManager.SelectedTarget = value; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        if (!this.IsPostBack)
        {
            BindData();
            Target.DataSource = new List<string>() { "IP Address", "MAC Address", "Hostname"};
            Target.DataBind();

            //TODO: IPAddress -> Unknown
            //SelectedTarget = UDTRuleTarget.IPAddress;

            switch (SelectedTarget)
            {
                case UDTRuleTarget.IPAddress:
                    Target.SelectedIndex = 0;
                    break;

                case UDTRuleTarget.MACAddress:
                    Target.SelectedIndex = 1;
                    break;

                case UDTRuleTarget.DNSName:
                    Target.SelectedIndex = 2;
                    break;

                default: //Unknown
                    Target.SelectedIndex = 3;
                    break;
            }
            
        }
		Update();
	}

	protected void DeviceGrid_OnDataBound(object sender, EventArgs e)
	{
        DeviceGrid.Columns[1].Visible = (CustomList.PatternList.Count > 1);
	}

	public void BindData()
	{
        if (CustomList.PatternList.Count != 0)
        {
            
            var concPattern = SelectedTarget == UDTRuleTarget.MACAddress
                                  ? string.Join("\r\n", CustomList.PatternList.Select(s => DALHelper.MacAddressToDisplayFormat(s))).Trim()
                                  : string.Join("\r\n", CustomList.PatternList).Trim();
            CustomList.PatternList.Clear();
            CustomList.PatternList.Add(concPattern);
        }
        DeviceGrid.DataSource = CustomList.PatternList;

        if (CustomList.PatternList.Count == 0)
		{
            CustomList.PatternList.Add(String.Empty);
		}
        DeviceGrid.DataBind();

        for (var i = 0; i < DeviceGrid.Rows.Count; i++)
        {
            Control item = DeviceGrid.Rows[i];

            var macValidator = (CustomValidator)item.FindControl("BeginMACValidator");
            var dnsValidator = (CustomValidator)item.FindControl("BeginDNSValidator");
            var ipValidator = (CustomValidator)item.FindControl("IPv4v6Validator");

            if (macValidator == null || dnsValidator == null || ipValidator == null) return;

            macValidator.Enabled = (SelectedTarget == UDTRuleTarget.MACAddress);
            dnsValidator.Enabled = (SelectedTarget == UDTRuleTarget.DNSName);
            ipValidator.Enabled = (SelectedTarget == UDTRuleTarget.IPAddress);

        } 
	}

	private void Update()
	{
        for (int i = 0; i < DeviceGrid.Rows.Count; i++)
		{
            if (i >= CustomList.PatternList.Count) break;
            Control item = DeviceGrid.Rows[i];
			var customTextBox = (TextBox)item.FindControl("DeviceTextBox");
            switch (SelectedTarget)
            {
                case UDTRuleTarget.MACAddress:
                    var macAddressValues =  customTextBox.Text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();
                    CustomList.PatternList = macAddressValues.Select(a => DALHelper.MacAddressToStorageFormat(a)).ToList();
                        break;
                default:
                    CustomList.PatternList = customTextBox.Text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();
                    break;
            }   
		}
	}

    private bool ValidateDevice(int rowIndex)
    {
        if ((CustomList.PatternList.Count == 0) && (rowIndex == 0))
        {
            Control item = DeviceGrid.Rows[rowIndex];
            var customTextBox = (TextBox)item.FindControl("DeviceTextBox");
            if (!String.IsNullOrEmpty(customTextBox.Text))
            {
                switch (SelectedTarget)
                {
                    case UDTRuleTarget.MACAddress:
                        var macAddressValues =  customTextBox.Text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries).ToList();
                        CustomList.PatternList = macAddressValues.Select(a => DALHelper.MacAddressToStorageFormat(a)).ToList();
                        break;
                    default:
                        CustomList.PatternList = customTextBox.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        break;
                }
            }
        }

        var device = CustomList.PatternList[rowIndex];
        var duplicates = CustomList.PatternList.GroupBy(x => x)
                               .Where(g => g.Count() > 1)
                               .Select(g => g.Key)
                               .ToList();

        if (duplicates.Count > 1 )return false;
        
        return CustomList.PatternList.Where((t, row) => row != rowIndex).All(t => t.Trim() != device.Trim());

    }

	/// <summary> Called when Delete or Add device button is clicked </summary>
    protected void DeviceGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName == "DeleteRow")
		{
			int index = Convert.ToInt32(e.CommandArgument);
            if (index < CustomList.PatternList.Count)
			{
                CustomList.PatternList.RemoveAt(index);
			}
			BindData();
		}

	}

	/// <summary> </summary>
	protected void AndButton_Clicked(object sender, EventArgs e)
	{
        CustomList.PatternList.Add(string.Empty);
		BindData();
	}

    /// <summary> </summary>
    protected void DeviceValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDevice(rowIndex);
    }

    protected void Target_Changed(object sender, EventArgs e)
    {
        CustomList.PatternList.RemoveAll(device => true);
        CustomList.PatternList.Add(string.Empty);
        switch (Target.SelectedIndex)
        {
            case 0:
                SelectedTarget = UDTRuleTarget.IPAddress;
                break;

            case 1:
                SelectedTarget = UDTRuleTarget.MACAddress;
                break;

            case 2:
                SelectedTarget = UDTRuleTarget.DNSName;
                break;

            default:
                SelectedTarget = UDTRuleTarget.Unknown;
                break;

        }
        BindData();
    }

	/// <summary> </summary>
	public void DeleteEmptyRows()
	{
        CustomList.PatternList.RemoveAll(device => string.IsNullOrEmpty(device));
	}

	public bool CheckEmpty()
	{
        this.EmptyValidator.IsValid = CustomList.PatternList.All(device => !String.IsNullOrEmpty(device));
        if (CustomList.PatternList.Count == 0) this.EmptyValidator.IsValid = false;
		return this.EmptyValidator.IsValid;
	}
    protected void IPv4v6Validate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = DeviceGrid.Rows[rowIndex];
        var deviceTextBox = (TextBox)item.FindControl("DeviceTextBox");
        var ipaddressMulti = deviceTextBox.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        var lineNumber = 0;

        foreach (var ipaddress in ipaddressMulti)
        {
            lineNumber++;
            var ipAdress = ipaddress;
            if (ipaddress.Trim().Contains(":"))
            {
                ipAdress = ipaddress.Replace("*", "0001"); //Replace * with default value to validate the IPV6 format
            }
            else if (ipaddress.Trim().Contains("*"))
            {
                ipAdress = ipaddress.Replace("*", "1"); //Replace * with default value to validate the IPV4 format
            }

            IPAddress ip;
            if (IPAddress.TryParse(ipAdress, out ip))
            {
                args.IsValid = ((ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6));
            }
            else
            {
                args.IsValid = false;
                cv.ErrorMessage = string.Format(Resources.UDTWebContent.UDTWEBDATA_MK1_43, lineNumber);
                break;
            }
        }
    }
    protected void MacAddressValidate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = DeviceGrid.Rows[rowIndex];
        var deviceTextBox = (TextBox)item.FindControl("DeviceTextBox");
        IPAddress ip;
        var macaddressMulti = deviceTextBox.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        const string pattern = @"(^\*?([0-9a-fA-F]{0,4}\.?\*?[0-9a-fA-F]{0,4}\.?\*?[0-9a-fA-F]{0,4})$)|(^\*?([0-9a-fA-F]{0,12}\*?)$)|(^\*?([0-9a-fA-F]{0,2}\:?\*?[0-9a-fA-F]{0,2}\:?\*?[0-9a-fA-F]{0,2}\:?\*?[0-9a-fA-F]{0,2}\:?\*?[0-9a-fA-F]{0,2}\:?\*?[0-9a-fA-F]{0,2})$)|(^\*?([0-9a-fA-F]{0,2}\-?\*?[0-9a-fA-F]{0,2}\-?\*?[0-9a-fA-F]{0,2}\-?\*?[0-9a-fA-F]{0,2}\-?\*?[0-9a-fA-F]{0,2}\-?\*?[0-9a-fA-F]{0,2})$)";
        
        var lineNumber = 0;
        
        foreach (var macAddress in macaddressMulti)
        {   
            lineNumber++;
            if (!(Regex.IsMatch(macAddress, pattern, RegexOptions.IgnoreCase)))
            {
                args.IsValid = false;
                cv.ErrorMessage = string.Format(Resources.UDTWebContent.UDTWEBDATA_MK1_42, lineNumber);
                break;
            }
        }
    }
    protected void DNSValidate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = DeviceGrid.Rows[rowIndex];
        var deviceTextBox = (TextBox)item.FindControl("DeviceTextBox");
        var dnsAddressMulti = deviceTextBox.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        const string pattern = @"^(?:[-A-Za-z0-9\.\*?]+)$";
        var lineNumber = 0;
        foreach (var dnsAddress in dnsAddressMulti)
        {
            lineNumber++;
            if (!(Regex.IsMatch(dnsAddress, pattern, RegexOptions.IgnoreCase)))
            {
                args.IsValid = false;
                cv.ErrorMessage = string.Format(Resources.UDTWebContent.UDTWEBDATA_MK1_44, lineNumber); 
                break;
            }
        }
    }
}

