﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MacRange.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_MacRange" %>

<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2">
<div style="padding:5px;">
    <% if (WhiteListWorkflowManager.RuleType == UDTRuleType.Whitelist) { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_50 %>
    <% } else { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_51 %>
    <% } %>
</div>
</td></tr><tr><td>
<div class="InputBox" style="display:block;">
<asp:GridView ID="MacRangeGrid" runat="server" automation="macrangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="MacRangeGrid_RowCommand" OnDataBound="MacRangeGrid_OnDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_98 %>">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="BeginMacTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 288px;" Text='<%# DataBinder.Eval(Container.DataItem, "Item1") %>'></asp:TextBox><br/>
               
                <asp:RegularExpressionValidator runat="server" ID="BeginMacValidator" Display="Dynamic" ValidationExpression="(^([0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4})$)|(^([0-9a-fA-F]{12})$)|(^([0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2})$)|(^([0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2})$)" ControlToValidate="BeginMacTextBox" ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MR1_23 %>"></asp:RegularExpressionValidator>
                <%--<orion:MacAddressValidator runat="server" ID="BeginMacValidator" ControlToValidate="BeginMacTextBox" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>" />--%>
                <asp:CustomValidator
                    ID="IPCompareValidator" 
                    runat="server" 
                    ControlToValidate="BeginMacTextBox"
                    OnServerValidate="CompareMacValidation"
                    ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_70 %>" 
                    EnableClientScript="false"
                    Display="Dynamic" ValidateEmptyText="true">
                </asp:CustomValidator>
                <asp:CustomValidator
                    ID="DuplicityValidator" 
                    runat="server" 
                    ControlToValidate="BeginMacTextBox"
                    OnServerValidate="DuplicityValidation"
                    ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_71 %>" 
                    EnableClientScript="false"
                    Display="Dynamic">
                </asp:CustomValidator>
               
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                <orion:LocalizableButton id="AndButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_170 %>"  OnClick="AndButton_Clicked"/>
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ Resources: CoreWebContent,WEBDATA_AK0_95 %>">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="EndMacTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 288px;" Text='<%# DataBinder.Eval(Container.DataItem, "Item2") %>'></asp:TextBox><br/>
               
                <asp:RegularExpressionValidator runat="server" ID="EndMacValidator" Display="Dynamic" ValidationExpression="(^([0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4})$)|(^([0-9a-fA-F]{12})$)|(^([0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2})$)|(^([0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2})$)" ControlToValidate="EndMacTextBox" ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MR1_23 %>"></asp:RegularExpressionValidator>
                <%--<orion:MacAddressValidator runat="server" ID="EndMacValidator" ControlToValidate="EndMacTextBox" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>" />--%>
               
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                &nbsp;
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow"/>
    </Columns>
</asp:GridView>
<div>
    <br /><br />
    <asp:CustomValidator
        ID="EmptyValidator" 
        runat="server" 
        ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_47 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
</div>
</td><td width="100%">&nbsp;</td></tr>
</table>