﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.DisplayTypes;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Controls_MacRange : System.Web.UI.UserControl
{

    protected UDTRuleRangeList AddressRange
    {
        get
        {
            return WhiteListWorkflowManager.MacAddressRange;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindData();
        }
        Update();
    }

    protected void MacRangeGrid_OnDataBound(object sender, EventArgs e)
    {
        MacRangeGrid.Columns[2].Visible = (AddressRange.RangeCollection.Count > 1);
    }

    public void BindData()
    {
        MacRangeGrid.DataSource = AddressRange.RangeCollection;
        if (AddressRange.RangeCollection.Count == 0)
        {
            AddressRange.RangeCollection.Add(
                new UDTValueRange() { Item1 = "", Item2 = "" });
        }
        MacRangeGrid.DataBind();
    }

    private void Update()
    {
        for (int i = 0; i < MacRangeGrid.Rows.Count; i++)
        {
            if (i >= AddressRange.RangeCollection.Count) break;
            Control item = MacRangeGrid.Rows[i];
            TextBox beginMacTextBox = (TextBox)item.FindControl("BeginMacTextBox");
            TextBox endMacTextBox = (TextBox)item.FindControl("EndMacTextBox");

            AddressRange.RangeCollection[i].Item1 = DALHelper.MacAddressToStorageFormat(beginMacTextBox.Text);
            AddressRange.RangeCollection[i].Item2 = DALHelper.MacAddressToStorageFormat(endMacTextBox.Text);
        }
    }

    private bool ValidateDuplicity(int rowIndex)
    {
        if (AddressRange.RangeCollection.Count > rowIndex)
        {
            UDTValueRange range = AddressRange.RangeCollection[rowIndex];
            return AddressRange.RangeCollection.Where((t, row) => row != rowIndex).All(t => ((t.Item1.Trim() != range.Item1.Trim()) || (t.Item2.Trim() != range.Item2.Trim())));
        }
        return false;
    }

    /// <summary> Called when Delete or Add IP Address Range button is clicked </summary>
    protected void MacRangeGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (index < AddressRange.RangeCollection.Count)
            {
                AddressRange.RangeCollection.RemoveAt(index);
            }
            BindData();
        }

    }

    /// <summary> </summary>
    protected void AndButton_Clicked(object sender, EventArgs e)
    {
        AddressRange.RangeCollection.Add(new UDTValueRange());
        BindData();
    }

    /// <summary> </summary>
    protected void CompareMacValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        cv.ErrorMessage = String.Empty;

        if ((AddressRange.RangeCollection.Count == 0) && (rowIndex == 0))
        {
            Control item = MacRangeGrid.Rows[rowIndex];
            TextBox beginMacTextBox = (TextBox)item.FindControl("BeginMacTextBox");
            TextBox endMacTextBox = (TextBox)item.FindControl("EndMacTextBox");
            if (!String.IsNullOrEmpty(beginMacTextBox.Text) && !String.IsNullOrEmpty(endMacTextBox.Text))
            {
                AddressRange.RangeCollection.Add(new UDTValueRange()
                {
                    Item1 = DALHelper.MacAddressToStorageFormat(beginMacTextBox.Text),
                    Item2 = DALHelper.MacAddressToStorageFormat(endMacTextBox.Text)
                });
            }
        }
        if (AddressRange.RangeCollection.Count > rowIndex)
        {
            if (AddressRange.RangeCollection.Count > 0)
            {
                UDTValueRange range = AddressRange.RangeCollection[rowIndex];
                if (String.IsNullOrEmpty(range.Item1.Trim()) != String.IsNullOrEmpty(range.Item2.Trim()))
                {
                    cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_AK0_29;
                }
                else
                {
                    try
                    {
                        if (HostHelper.ConvertMacAddressToLong(range.Item1) >
                            HostHelper.ConvertMacAddressToLong(range.Item2))
                        {
                            cv.ErrorMessage = Resources.UDTWebContent.UDTWEBDATA_MK1_70;
                        }
                    }
                    catch (FormatException)
                    {
                    }
                }
            }
        }
        args.IsValid = String.IsNullOrEmpty(cv.ErrorMessage);
    }

    /// <summary> </summary>
    protected void DuplicityValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDuplicity(rowIndex);
    }


    /// <summary> </summary>
    public void DeleteEmptyRows()
    {
        AddressRange.RangeCollection.RemoveAll(range => string.IsNullOrEmpty(range.Item1) && string.IsNullOrEmpty(range.Item2));
    }

    public bool CheckEmpty()
    {
        if (Page.IsValid)
        {
            this.EmptyValidator.IsValid = AddressRange.RangeCollection.All(range => !String.IsNullOrEmpty(range.Item1) && !String.IsNullOrEmpty(range.Item2));
            if (AddressRange.RangeCollection.Count == 0) this.EmptyValidator.IsValid = false;
            return this.EmptyValidator.IsValid;
        }
        return false;
    }
}

