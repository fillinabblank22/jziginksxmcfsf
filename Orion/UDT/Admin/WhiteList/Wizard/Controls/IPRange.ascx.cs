﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using SolarWinds.UDT.Web.Helpers;
using HostHelper = SolarWinds.Common.Net.HostHelper;
using IPAddressRange=SolarWinds.Orion.Core.Models.Discovery.IPAddressRange;
using SolarWinds.Common.Net;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Controls_IPRange : System.Web.UI.UserControl
{
 
    protected UDTRuleRangeList AddressRange
    {
        get
        {
            return WhiteListWorkflowManager.IPAddressRange;
        }

    }

	protected void Page_Load(object sender, EventArgs e)
	{
        if (!this.IsPostBack)
        {
            BindData();
        }
		Update();
	}

	protected void IPRangeGrid_OnDataBound(object sender, EventArgs e)
	{
		IPRangeGrid.Columns[2].Visible = (AddressRange.RangeCollection.Count > 1);
	}

	public void BindData()
	{
        IPRangeGrid.DataSource = AddressRange.RangeCollection;
        if (AddressRange.RangeCollection.Count == 0)
		{
            AddressRange.RangeCollection.Add(
                new UDTValueRange() { Item1 = "", Item2 = "" });
		}
		IPRangeGrid.DataBind();
	}

	private void Update()
	{		
		for (int i = 0; i < IPRangeGrid.Rows.Count; i++)
		{
            if (i >= AddressRange.RangeCollection.Count) break;
			Control item = IPRangeGrid.Rows[i];
			var beginIPTextBox = (TextBox)item.FindControl("BeginIPTextBox");
			var endIPTextBox = (TextBox)item.FindControl("EndIPTextBox");

            AddressRange.RangeCollection[i].Item1 = DALHelper.IpAddressToStorageFormat(beginIPTextBox.Text);
            AddressRange.RangeCollection[i].Item2 = DALHelper.IpAddressToStorageFormat(endIPTextBox.Text);
		}
	}

    private bool ValidateDuplicity(int rowIndex)
    {
        if (AddressRange.RangeCollection.Count > rowIndex)
        {
            UDTValueRange range = AddressRange.RangeCollection[rowIndex];

            return
                AddressRange.RangeCollection.Where((t, row) => row != rowIndex).All(
                    t => ((t.Item1.Trim() != range.Item1.Trim()) || (t.Item2.Trim() != range.Item2.Trim())));
        }
        return false;
    }

    /// <summary> Called when Delete or Add IP Address Range button is clicked </summary>
	protected void IPRangeGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName == "DeleteRow")
		{
			int index = Convert.ToInt32(e.CommandArgument);
            if (index < AddressRange.RangeCollection.Count)
			{
                AddressRange.RangeCollection.RemoveAt(index);
			}
			BindData();
		}

	}

	/// <summary> </summary>
	protected void AndButton_Clicked(object sender, EventArgs e)
	{
	    if (Page.IsValid)
	    {
            AddressRange.RangeCollection.Add(new UDTValueRange());
            BindData();    
	    }
	}

	/// <summary> </summary>
	protected void CompareIPValidation(object source, ServerValidateEventArgs args)
	{
		CustomValidator cv = (CustomValidator)source;
		int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
		cv.ErrorMessage = String.Empty;

        if ((AddressRange.RangeCollection.Count == 0) && (rowIndex==0))
        {
            Control item = IPRangeGrid.Rows[rowIndex];
            TextBox beginIPTextBox = (TextBox)item.FindControl("BeginIPTextBox");
            TextBox endIPTextBox = (TextBox)item.FindControl("EndIPTextBox");
            if (!String.IsNullOrEmpty(beginIPTextBox.Text) && !String.IsNullOrEmpty(endIPTextBox.Text))
            {
                AddressRange.RangeCollection.Add(new UDTValueRange()
                                                     {
                                                         Item1 = DALHelper.IpAddressToStorageFormat(beginIPTextBox.Text),
                                                         Item2 = DALHelper.IpAddressToStorageFormat(endIPTextBox.Text)
                                                     });
            }
        }
        if (AddressRange.RangeCollection.Count > rowIndex)
        {
            if (AddressRange.RangeCollection.Count > 0)
            {
                UDTValueRange range = AddressRange.RangeCollection[rowIndex];

                if (String.IsNullOrEmpty(range.Item1) == String.IsNullOrEmpty(range.Item2))
                {
                   
                    IPAddress startIp;
                    IPAddress endIp;
                    if (IPAddress.TryParse(range.Item1, out startIp) && IPAddress.TryParse(range.Item2, out endIp))
                    {
                        if (!(IPHelper.VerifyIncreasingRange(startIp, endIp)))
                            cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_AK0_30;
                    }
                }
            }
        }
	    args.IsValid = String.IsNullOrEmpty(cv.ErrorMessage);
	}

    /// <summary> </summary>
    protected void DuplicityValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDuplicity(rowIndex);
    }


	/// <summary> </summary>
	public void DeleteEmptyRows()
	{
        AddressRange.RangeCollection.RemoveAll(range => string.IsNullOrEmpty(range.Item1) && string.IsNullOrEmpty(range.Item2));
	}

	public bool CheckEmpty()
	{
        if (Page.IsValid)
        {
            this.EmptyValidator.IsValid =
                AddressRange.RangeCollection.All(
                    range => !String.IsNullOrEmpty(range.Item1) && !String.IsNullOrEmpty(range.Item2));
            if (AddressRange.RangeCollection.Count == 0) this.EmptyValidator.IsValid = false;
            return this.EmptyValidator.IsValid;
        }
        return false;
	}

    protected void BeginIPv4v6Validate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = IPRangeGrid.Rows[rowIndex];
        var deviceTextBox = (TextBox)item.FindControl("BeginIPTextBox");
        IPAddress ip;
        var ipCompareValidator = (CustomValidator)item.FindControl("IPCompareValidator");
        var duplicityValidator = (CustomValidator)item.FindControl("DuplicityValidator");
        
        if (IPAddress.TryParse(deviceTextBox.Text, out ip))
        {
            args.IsValid = ((ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6));
            ipCompareValidator.Enabled = true;
            duplicityValidator.Enabled = true;
            EmptyValidator.Enabled = true;
        }
        else
        {
            args.IsValid = false;
            ipCompareValidator.Enabled = false;
            duplicityValidator.Enabled = false;
            EmptyValidator.Enabled = false;
        }
    }

    protected void EndIPv4v6Validate(object source, ServerValidateEventArgs args)
    {
        var cv = (CustomValidator)source;
        var rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = IPRangeGrid.Rows[rowIndex];
        var deviceTextBox = (TextBox)item.FindControl("EndIPTextBox");
        IPAddress ip;
        var ipCompareValidator = (CustomValidator)item.FindControl("IPCompareValidator");
        var duplicityValidator = (CustomValidator)item.FindControl("DuplicityValidator");

        if (IPAddress.TryParse(deviceTextBox.Text, out ip))
        {
            args.IsValid = ((ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6));
            ipCompareValidator.Enabled = true;
            duplicityValidator.Enabled = true;
            EmptyValidator.Enabled = true;
        }
        else
        {
            args.IsValid = false;
            ipCompareValidator.Enabled = false;
            duplicityValidator.Enabled = false;
            EmptyValidator.Enabled = false;
        }
    }
}

