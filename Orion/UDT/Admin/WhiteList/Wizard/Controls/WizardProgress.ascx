﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WizardProgress.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_WizardProgress" %>

<link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/ProgressIndicator.css" />    

<div class="ProgressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>