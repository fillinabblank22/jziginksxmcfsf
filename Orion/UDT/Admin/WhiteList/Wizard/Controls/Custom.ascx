﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Custom.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_Custom" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2" style="vertical-align: top;">
<div style="padding:5px;">
    <% if (WhiteListWorkflowManager.RuleType==UDTRuleType.Whitelist ) { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_40 %>
    <% } else { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_41 %>
    <% } %>
</div>
</td></tr>

<tr><td style="vertical-align: top;">
<div style="padding:5px; font-weight: bold;">
    Target:&nbsp;<asp:DropDownList ID="Target" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Target_Changed"></asp:DropDownList>     
</div>             
<div class="InputBox" style="display:block;">
<asp:GridView ID="DeviceGrid" runat="server" automation="iprangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="DeviceGrid_RowCommand" OnDataBound="DeviceGrid_OnDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="Custom">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="DeviceTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 350px;height: 300px;" Text='<%# Container.DataItem %>' TextMode="MultiLine"></asp:TextBox><br/>                                                
                <asp:CustomValidator runat="server" ID="BeginMACValidator" Display="Dynamic" Enabled="False" ControlToValidate="DeviceTextBox" OnServerValidate="MacAddressValidate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                <asp:CustomValidator runat="server" ID="IPv4v6Validator" Display="Dynamic" Enabled="False" ControlToValidate="DeviceTextBox" OnServerValidate="IPv4v6Validate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                <asp:CustomValidator runat="server" ID="BeginDNSValidator" Display="Dynamic" Enabled="False" ControlToValidate="DeviceTextBox" OnServerValidate="DNSValidate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>

                <asp:CustomValidator
                    ID="DeviceValidator" 
                    runat="server" 
                    ControlToValidate="DeviceTextBox"
                    OnServerValidate="DeviceValidation"
                    ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_45 %>" 
                    EnableClientScript="false"
                    Display="Dynamic">
                </asp:CustomValidator>
            </ItemTemplate>
            <FooterTemplate>            
            </FooterTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow"/>
    </Columns>
</asp:GridView>
<div>
    <br /><br />
    <asp:CustomValidator
        ID="EmptyValidator" 
        runat="server" 
        ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MK1_46 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
</div>
</td><td width="100%" style="padding:10px;">
        <span class="crn crn-tl"></span><span class="crn crn-tr"></span><span class="crn crn-bl"></span><span class="crn crn-br"></span><div class="sw-pg-hint-inner"><div class="sw-pg-hint-body">
        <%=Resources.UDTWebContent.UDTWEBDATA_MK1_72 %>
        </div></div>
     </td></tr>
</table>
