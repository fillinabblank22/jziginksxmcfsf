﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.UDT.Common.Models.Rules;
using SolarWinds.UDT.Web;
using System.Text.RegularExpressions;
using SolarWinds.UDT.Web.Helpers;
using HostHelper = SolarWinds.Common.Net.HostHelper;

public partial class Orion_UDT_Admin_WhiteList_Wizard_Controls_SingleDevice : System.Web.UI.UserControl
{
 
    protected UDTRuleValueList DeviceList
    {
        get
        {
            return WhiteListWorkflowManager.DeviceList;
        }
    }

    protected UDTRuleTarget SelectedTarget
    {
        get { return WhiteListWorkflowManager.SelectedTarget; }
        set { WhiteListWorkflowManager.SelectedTarget = value; }
    }

	protected void Page_Load(object sender, EventArgs e)
	{
        if (!this.IsPostBack)
        {
            BindData();
            Target.DataSource = new List<string>() { "IP Address", "MAC Address", "Hostname" };
            Target.DataBind();
            switch (SelectedTarget)
            {
                case UDTRuleTarget.IPAddress:
                    Target.SelectedIndex = 0;
                    break;

                case UDTRuleTarget.MACAddress:
                    Target.SelectedIndex = 1;
                    break;

                case UDTRuleTarget.DNSName:
                    Target.SelectedIndex = 2;
                    break;

                default:
                    Target.SelectedIndex = 0;
                    break;
            }
            
        }
		Update();
	}

	protected void DeviceGrid_OnDataBound(object sender, EventArgs e)
	{
        DeviceGrid.Columns[1].Visible = (DeviceList.Values.Count > 1);
	}

	public void BindData()
	{
        DeviceGrid.DataSource = DeviceList.Values;
        if (DeviceList.Values.Count == 0)
		{
            DeviceList.Values.Add(String.Empty);
		}
        DeviceGrid.DataBind();

        for (int i = 0; i < DeviceGrid.Rows.Count; i++)
        {
            Control item = DeviceGrid.Rows[i];

            var macValidator = (RegularExpressionValidator)item.FindControl("BeginMACValidator");
            var dnsValidator = (RegularExpressionValidator)item.FindControl("BeginDNSValidator");
            var ipValidator = (CustomValidator)item.FindControl("IPv4v6Validator");

            if (macValidator == null || dnsValidator == null || ipValidator == null) return;

            macValidator.Enabled = (SelectedTarget == UDTRuleTarget.MACAddress);
            dnsValidator.Enabled = (SelectedTarget == UDTRuleTarget.DNSName);
            ipValidator.Enabled = (SelectedTarget == UDTRuleTarget.IPAddress);

        } 
	}

	private void Update()
	{

        for (int i = 0; i < DeviceGrid.Rows.Count; i++)
		{
            if (i >= DeviceList.Values.Count) break;
            Control item = DeviceGrid.Rows[i];
			TextBox deciceTextBox = (TextBox)item.FindControl("DeviceTextBox");

            switch (SelectedTarget)
            {
                case UDTRuleTarget.MACAddress:
                    DeviceList.Values[i] = DALHelper.MacAddressToStorageFormat(deciceTextBox.Text);
                    break;
                case UDTRuleTarget.IPAddress:
                    DeviceList.Values[i] = DALHelper.IpAddressToStorageFormat(deciceTextBox.Text);
                    break;
                default:
                    DeviceList.Values[i] = deciceTextBox.Text;
                    break;
            }
		}
	}

    private bool ValidateDevice(int rowIndex)
    {

        if ((DeviceList.Values.Count == 0) && (rowIndex==0))
        {
            Control item = DeviceGrid.Rows[rowIndex];
            TextBox deviceTextBox = (TextBox)item.FindControl("DeviceTextBox");
            if (!String.IsNullOrEmpty(deviceTextBox.Text))
            {
                switch (SelectedTarget)
                {
                    case UDTRuleTarget.MACAddress:
                        DeviceList.Values.Add(DALHelper.MacAddressToStorageFormat(deviceTextBox.Text));
                        break;
                    case UDTRuleTarget.IPAddress:
                        DeviceList.Values.Add(DALHelper.IpAddressToStorageFormat(deviceTextBox.Text));
                        break;
                    default:
                        DeviceList.Values.Add(deviceTextBox.Text);
                        break;
                }
            }
        }

        if (DeviceList.Values.Count > rowIndex)
        {
            string device = DeviceList.Values[rowIndex];


            foreach (string d in DeviceList.Values)
            {
                if (!d.Equals(device))
                {
                    if (d.Trim() == device.Trim())
                    {
                        return false;
                    }
                }
            }
        }

        return true;

    }

	/// <summary> Called when Delete or Add device button is clicked </summary>
    protected void DeviceGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName == "DeleteRow")
		{
			int index = Convert.ToInt32(e.CommandArgument);
            if (index < DeviceList.Values.Count)
			{
                DeviceList.Values.RemoveAt(index);
			}
			BindData();
		}

	}

	/// <summary> </summary>
	protected void AndButton_Clicked(object sender, EventArgs e)
	{
        DeviceList.Values.Add(string.Empty);
		BindData();
	}

    /// <summary> </summary>
    protected void DeviceValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDevice(rowIndex);
    }

    protected void Target_Changed(object sender, EventArgs e)
    {
        DeviceList.Values.RemoveAll(device => true);
        DeviceList.Values.Add(string.Empty);
        switch (Target.SelectedIndex)
        {
            case 0:
                SelectedTarget = UDTRuleTarget.IPAddress;
                break;

            case 1:
                SelectedTarget = UDTRuleTarget.MACAddress;
                break;

            case 2:
                SelectedTarget = UDTRuleTarget.DNSName;
                break;

            default:
                SelectedTarget = UDTRuleTarget.IPAddress;
                break;

        }
        BindData();
    }

	/// <summary> </summary>
	public void DeleteEmptyRows()
	{
        DeviceList.Values.RemoveAll(device => string.IsNullOrEmpty(device));
	}

	public bool CheckEmpty()
	{
        this.EmptyValidator.IsValid = DeviceList.Values.All(device => !String.IsNullOrEmpty(device));
        if (DeviceList.Values.Count == 0) this.EmptyValidator.IsValid = false;
        string errMsg = string.Empty;
        switch (Target.SelectedIndex)
        {
            case 0:
                errMsg = Resources.UDTWebContent.UDTWEBDATA_SL1_01;
                break;
            case 1:
                errMsg = Resources.UDTWebContent.UDTWEBDATA_SL1_02;
                break;
            case 2:
                errMsg = Resources.UDTWebContent.UDTWEBDATA_SL1_03;
                break;
            default:
                errMsg = Resources.UDTWebContent.UDTWEBDATA_SL1_01;
                break;
        }
        this.EmptyValidator.ErrorMessage = errMsg;
		return this.EmptyValidator.IsValid;
	}

    protected void IPv4v6Validate(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        Control item = DeviceGrid.Rows[rowIndex];
        TextBox deviceTextBox = (TextBox)item.FindControl("DeviceTextBox");
        IPAddress ip;
        if (IPAddress.TryParse(deviceTextBox.Text, out ip))
        {
            args.IsValid = ((ip.AddressFamily == AddressFamily.InterNetwork) || (ip.AddressFamily == AddressFamily.InterNetworkV6));
        }
        else
        {
            args.IsValid = false;
        }
    }
}

