﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SingleDevice.ascx.cs" Inherits="Orion_UDT_Admin_WhiteList_Wizard_Controls_SingleDevice" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.UDT.Common.Models.Rules" %>
<%@ Import Namespace="SolarWinds.UDT.Web" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<table width="100%" cellpadding="0" cellspacing="0">
<tr><td colspan="2">
<div style="padding:5px;">
    <% if (WhiteListWorkflowManager.RuleType==UDTRuleType.Whitelist ) { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_54 %>
    <% } else { %>
    <%= Resources.UDTWebContent.UDTWEBDATA_MK1_55 %>
    <% } %>
</div>
</td></tr><tr><td>
<div style="padding:5px; font-weight: bold;">
    Target:&nbsp;<asp:DropDownList ID="Target" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Target_Changed"></asp:DropDownList>     
</div>             
<div class="InputBox" style="display:block;">
<asp:GridView ID="DeviceGrid" runat="server" automation="iprangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="DeviceGrid_RowCommand" OnDataBound="DeviceGrid_OnDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="Device">
            <HeaderStyle HorizontalAlign="Left" />
            <ItemTemplate>
                <asp:TextBox ID="DeviceTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 288px;" Text='<%# Container.DataItem %>'></asp:TextBox><br/>
                <asp:RegularExpressionValidator Enabled="False" runat="server" ID="BeginMACValidator" Display="Dynamic" ValidationExpression="(^([0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4})$)|(^([0-9a-fA-F]{12})$)|(^([0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2}\:[0-9a-fA-F]{2})$)|(^([0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2}\-[0-9a-fA-F]{2})$)" ControlToValidate="DeviceTextBox" ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MR1_23 %>"></asp:RegularExpressionValidator>
                <asp:CustomValidator runat="server" ID="IPv4v6Validator" Display="Dynamic" Enabled="False" ControlToValidate="DeviceTextBox" OnServerValidate="IPv4v6Validate" EnableClientScript="False" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_94 %>"></asp:CustomValidator>
                <asp:RegularExpressionValidator Enabled="False" runat="server" ID="BeginDNSValidator" Display="Dynamic" ValidationExpression="^(?:[-A-Za-z0-9\.]+)$" ControlToValidate="DeviceTextBox" ErrorMessage="<%$ Resources: UDTWebContent,UDTWEBDATA_MR1_24 %>"></asp:RegularExpressionValidator>
                
                <asp:CustomValidator
                    ID="DeviceValidator" 
                    runat="server" 
                    ControlToValidate="DeviceTextBox"
                    OnServerValidate="DeviceValidation"
                    ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_96 %>" 
                    EnableClientScript="false"
                    Display="Dynamic">
                </asp:CustomValidator>
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 228px;">
                <orion:LocalizableButton id="AndButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_AK0_170 %>"  OnClick="AndButton_Clicked"/>
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow"/>
    </Columns>
</asp:GridView>
<div>
    <br /><br />
    <asp:CustomValidator
        ID="EmptyValidator" 
        runat="server" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
</div>
</td><td width="100%">&nbsp;</td></tr>
</table>
