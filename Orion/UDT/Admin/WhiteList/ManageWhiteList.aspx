﻿<%@ Page Title="<%$ Resources : UDTWebContent, UDTWEBDATA_MK1_9 %>" Language="C#" MasterPageFile="~/Orion/UDT/UdtPageWithoutRefresher.master" AutoEventWireup="true" CodeFile="ManageWhiteList.aspx.cs" Inherits="Orion_UDT_Admin_WhiteList_ManageWhiteList" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="udt" TagName="LearnMoreLink" Src="~/Orion/UDT/Controls/LearnMoreLink.ascx" %>

<asp:Content ID="udtHeader" ContentPlaceHolderID="UdtHeadPlaceHolder" Runat="Server">

    <orion:IncludeExtJs id="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Admin.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/NodeMNG.css" />
    <link rel="stylesheet" type="text/css" href="/Orion/UDT/UDT.css" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/UDT/Admin/WhiteList/js/ManageWhiteList.js"></script>
    
    <script type="text/javascript">
        var testText = '<%=TestText.ClientID%>';
    </script>

    <style type="text/css">
        span.Label { white-space:nowrap;  }
        .smallText {color:#979797;font-size:11px;}
        .add { background-image:url(/Orion/images/add_16x16.gif) !important; width:auto !important}
        .edit { background-image:url(/Orion/images/edit_16x16.gif) !important; width:auto !important }
        .enable { background-image: url(/Orion/UDT/images/Admin/Discovery/OK_good.gif) !important; width:auto !important}
        .disable { background-image: url(/Orion/UDT/images/Admin/Discovery/disable.png) !important; width:auto !important}
        .del { background-image:url(/Orion/images/delete_16x16.gif) !important; width:auto !important }
        #delDialog .dialogBody table td { padding-right: 10px; }
        #delDialog td.x-btn-center { text-align: center !important; }
        #delDialog td { padding-right: 0; padding-bottom: 0; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        .radiobutton_title{ padding-left: 5px;}
        .testInputTable { vertical-align: top;}
        .testInputTable td, tr { padding-right: 5px;vertical-align: top;}
        .TestText { width: 188px;}
        
        .sw-pg-hint-inner { border: 1px solid #eaca7f; border-radius: 5px; background: #fffdcc; }
        .sw-pg-hint-body { margin: 6px 4px 10px 4px; padding-left: 22px;padding-right: 2px; background: url(/orion/images/icon.lightbulb.gif) top left no-repeat; }
        .sw-pg-align { padding-left: 110px; }
        
        .sw-pg-pass-inner { border: 1px solid #00A000; border-radius: 5px; background: #DAF7CC; }
        .sw-pg-pass-body { margin: 6px 4px 10px 4px; padding-left: 22px;padding-right: 2px; background: url(/orion/UDT/Images/Admin/Discovery/OK_good.gif) top left no-repeat; }
        
        .sw-pg-fail-inner { border: 1px solid #CE0000; border-radius: 5px; background: #FACECE; }
        .sw-pg-fail-body { margin: 6px 4px 10px 4px; padding-left: 22px;padding-right: 2px; background: url(/orion/UDT/Images/Admin/Discovery/disable.png) top left no-repeat; }
        
        .sw-pg-neutral-inner { border: 1px solid #848484; border-radius: 5px; background: #e4e4e4; }
        .sw-pg-neutral-body { margin: 6px 4px 10px 4px; padding-left: 22px;padding-right: 2px; background: url(/orion/UDT/Images/Admin/Discovery/Icon_Info.png) top left no-repeat; }
        .sw-btn:hover { color: #204a63 !important; }
    </style>
</asp:Content>

<asp:Content id="ManageWhiteListTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" id="PageTitleBar" />
</asp:Content>

<asp:Content id="WhiteList" ContentPlaceHolderID="UdtMainContentPlaceHolder" Runat="Server">
<input type="hidden" name="UDT_ManageWhiteList_PageSize" id="UDT_ManageWhiteList_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("UDT_ManageWatchList_PageSize")%>' />
<input type="hidden" id="ReturnToUrl" value="<%= ReturnUrl %>" />

<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isOrionDemoMode" style="display:none;"></div>
	<%}%>

     <div id="WhiteListContent" style="padding-left: 10px; padding-right: 10px;">	
        
        <table class="TitleTable">
            <tr>
                <td>
                    <div><asp:Literal runat="server" id="PageSubTitle"></asp:Literal></div>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                <table>
                    <tr>
                        <td>
                            <!-- test input -->
                            <table class="testInputTable">
                                <tr>
                                    <td>
                                         <label for="RuleTypeLbl"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_4 %></label>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="TestText" CssClass="TestText"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                            <orion:LocalizableButton id="TestRules" runat="server" DisplayType="Small" LocalizedText="Test" OnClientClick="SW.UDT.TestRules(testText)" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                            
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="smallText"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_5 %></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <!-- test result -->
                            <div id="result">
                            </div>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        
        <div id="WhiteListTabs"></div>
        
        <table id = "WhiteListTable" class="NodeManagement">
		    <tr valign="top" align="left">
                <td style="border-right: #e1e1e0 1px solid; border-left: 0; border-top: 0; border-bottom: 0; background-color: white;">
				    <div id="NodeGroupSection" class="NodeGrouping" style="margin-left: 10;">
					    <div class="GroupSection" style="margin-left: 10;">
						    <div style="width: 180px"><%= Resources.UDTWebContent.UDTWEBDATA_AK1_102 %></div>
						    <select id="groupByProperty" style="width:100%">
							    <option value=""><%= Resources.UDTWebContent.UDTWEBDATA_AK1_103 %></option>
							    <option value="1"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_6 %></option>
							    <option value="2"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_7 %></option>
							    <option value="3"><%= Resources.UDTWebContent.UDTWEBDATA_MK1_8 %></option>
						    </select>
					    </div>
					    <ul class="NodeGroupItems"></ul>
				    </div>
			    </td>
                <td id="WhiteListGridCell">
                    <div id="WhiteListGrid"></div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
