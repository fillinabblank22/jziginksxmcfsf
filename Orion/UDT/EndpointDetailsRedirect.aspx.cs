﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.UDT.Web.Helpers;

public partial class Orion_UDT_EndpointDetailsRedirect : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        string netObject = HttpUtility.UrlDecode(Request.QueryString["NetObject"]);

        if (!string.IsNullOrEmpty(netObject))
        {

            UDTEndpointNetObjectHelper noh = new UDTEndpointNetObjectHelper(netObject);
            Server.Transfer(string.Format(@"/Orion/UDT/EndpointDetails.aspx?NetObject={0}:VAL={1}", noh.EndpointType, noh.EndpointValue));
            
        }
        else
        {
            Server.Transfer(string.Format(@"/Orion/UDT/EndpointDetails.aspx?ViewID={0}", Request.QueryString["ViewID"]));
        }

     
        //if all else fails bounce back to the referrer
        Server.Transfer(Page.Request.UrlReferrer.ToString());
    }
}