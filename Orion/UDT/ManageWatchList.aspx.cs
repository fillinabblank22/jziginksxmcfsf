﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_UDT_ManageWatchList : System.Web.UI.Page
{
    internal bool _isOrionDemoMode = false;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Profile.AllowAdmin)
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Resources.UDTWebContent.UDTWCODE_VB1_1));
        }

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            _isOrionDemoMode = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pageTitleBar.HideCustomizePageLink = true;
        pageTitleBar.HelpFragment = "OrionUDTPHConfigSettingsManageWatchList";
    }
}