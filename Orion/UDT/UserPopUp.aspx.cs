﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.UDT.Web.NetObjects;

public partial class Orion_UDT_UserPopUp : System.Web.UI.Page
{
    protected UDTUserNetObject UDTUser { get; set; }
    protected string StatusCssClass
    {
        get
        {
            if (UDTUser.IsActive)
            {
                return "StatusUp";
            }
            else
            {
                return "StatusUnknown";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UDTUser = NetObjectFactory.Create(Request["NetObject"], true) as UDTUserNetObject;
    }
}