<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/UDT/UdtPage.master" AutoEventWireup="true"
    CodeFile="EndpointDetails.aspx.cs" Inherits="Orion_UDT_EndpointDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%-- TODO ???: <%@ Register TagPrefix="udt" TagName="LicenseExpirationMessage" Src="~/Orion/UDT/Controls/LicenseExpirationMessage.ascx" %>--%>
<%@ Register TagPrefix="udt" TagName="PageTitleBar" Src="~/Orion/UDT/Controls/PageTitleBar.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="udt" Namespace="SolarWinds.UDT.Web.Resources" Assembly="SolarWinds.UDT.Web" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <udt:PageTitleBar runat="server" ID="pageTitleBar" ShowTitle="False"/>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ViewPageTitle">
    <h1><%=this.getTitle()%></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="UdtMainContentPlaceHolder" runat="server">
    <udt:EndpointResourceHost ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </udt:EndpointResourceHost>
    <%--TODO ??? <udt:LicenseExpirationMessage ID="_licenseExpirationMessage" runat="server" />--%>
</asp:Content>

