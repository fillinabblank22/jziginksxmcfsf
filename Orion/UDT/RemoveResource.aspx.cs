﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_UDT_RemoveResource : System.Web.UI.Page
{
    protected string _viewPath;
    protected Dictionary<int, int> _checkBoxes = new Dictionary<int, int>();

    public string ResourceName { get; set; }

    public string ResourceNameShort { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_13, ResourceNameShort);
        pageTitleBar.PageTitle = String.Format(Resources.UDTWebContent.UDTWEBCODE_AK1_13, ResourceNameShort);
        pageTitleBar.HideCustomizePageLink = true;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (string.IsNullOrEmpty(Request["viewPath"]) == false)
        {
            _viewPath = Request["viewPath"].Trim();
        }

        if (string.IsNullOrEmpty(Request["ResourceName"]) == false)
        {
            ResourceName = Request["ResourceName"].Replace("\"", "").Trim();
        }

        if (string.IsNullOrEmpty(Request["ResourceNameShort"]) == false)
        {
            ResourceNameShort = Request["ResourceNameShort"].Replace("\"", "").Trim();
        }


        ViewInfoCollection views = ViewManager.GetAllViews();

        int countOfCheckBoxes = 0;
        ResourceInfoCollection resources;

        foreach (ViewInfo info in views)
        {
            resources = ViewManager.GetResourcesForView(info);

            foreach (ResourceInfo resInfo in resources)
            {
                if (0 == string.Compare(resInfo.Name.Trim(), ResourceName, StringComparison.InvariantCultureIgnoreCase))
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = "CheckBox" + countOfCheckBoxes.ToString();
                    cb.Checked = true;
                    LiteralControl lc = new LiteralControl("&nbsp;" + info.ViewTitle + "<br />");
                    this.CheckBoxDiv.Controls.Add(cb);
                    this.CheckBoxDiv.Controls.Add(lc);
                    _checkBoxes[countOfCheckBoxes] = resInfo.ID;
                    countOfCheckBoxes++;
                }
            }
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        CheckBox tempCheckBox;

        int position = 0;
        foreach (Control checkies in this.CheckBoxDiv.Controls)
        {
            if (checkies.GetType().Name == "CheckBox")
            {
                tempCheckBox = (CheckBox)checkies;
                if (tempCheckBox.Checked == true)
                {
                    ResourceManager.DeleteById(_checkBoxes[position]);
                }
                position++;
            }
        }

        // on detached resource this redirect should be handled
        if (IsDetached(Request.Url.Query))
        {
            Response.Redirect("/Orion/UDT/Summary.aspx");
        }

        if (string.IsNullOrEmpty(_viewPath))
        {
            _viewPath = "/Orion/UDT/Summary.aspx";
        }

        Response.Redirect(_viewPath);
    }

    private static bool IsDetached(string absoluteUri)
    {
        return absoluteUri.Contains("DetachResource.aspx");
    }
}