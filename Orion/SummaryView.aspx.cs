using System;
using System.Data;
using System.Linq;
using System.Web;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;

public partial class Orion_SummaryView : OrionView
{
    private static readonly Log _log = new Log();

    private bool IsViewCopCheckRequired
    {
        get
        {
            return Profile.AllowViewCopCheck
                   && Request.QueryString.AllKeys
                                         .Select(key => key.ToLower())
                                         .Any(key => key.Equals("viewid")
                                                     || key.Equals("viewname")
                                                     || key.Equals("viewkey"))
                   && Profile.AllowedViews.Any();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        // check if we should redirect to the discovery wizard when coming from the login page
        if ((Request.UrlReferrer != null) && Request.UrlReferrer.LocalPath.Equals("/Orion/Login.aspx", StringComparison.InvariantCultureIgnoreCase))
        {
            CheckRedirectToDiscovery();
        }

        CheckRedirectToDashboard();

        if (IsViewCopCheckRequired)
        {
            if (!Profile.AllowedViews.Contains(this.ViewInfo.ViewID))
            {
                ReportUnauthorizedRequest();
            }
        }

        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    private static bool IsAdminUser()
    {
        return (bool) HttpContext.Current.Profile.GetPropertyValue("AllowAdmin");
    }

    private static int GetSummaryDashboardID()
    {
        return (int)HttpContext.Current.Profile.GetPropertyValue("SummaryDashboardID");
    }

    private static int GetSummaryViewID()
    {
        return (int)HttpContext.Current.Profile.GetPropertyValue("SummaryViewID");
    }

    private static bool HasNodesInLicense()
    {
        return new FeatureManager().GetMaxElementCount(WellKnownElementTypes.Nodes) > 0;
    }

    private static bool HasAddedNodes()
    {
        var hasAddedNodes = true;

        try
        {
            DataTable dt;

            using (var swis = InformationServiceProxy.CreateV3())
            {
                dt = swis.Query("SELECT COUNT(NodeID) as NodeCount FROM Orion.Nodes");
            }

            if ((dt.Rows.Count < 1) || (int.Parse((dt.Rows[0]["NodeCount"]).ToString()) <= 1))
            {
                hasAddedNodes = false;
            }
        }
        catch
        {
            // if swis fails then all bets are off (don't redirect)
        }

        return hasAddedNodes;
    }

    private static bool DoNotRedirectUser()
    {
        var settingValue = WebUserSettingsDAL.Get("DoNotRedirectToDiscoveryWizard");

        if (string.IsNullOrEmpty(settingValue))
        {
            // user override is off by default
            WebUserSettingsDAL.Set("DoNotRedirectToDiscoveryWizard", "False");
            return false;
        }
        else
        {
            return (!settingValue.Equals("False"));
        }
    }

    private void CheckRedirectToDiscovery()
    {
        // redirection only applies to admin users, without added nodes but with nodes in license and who didn't opt out
        if (!IsAdminUser() || HasAddedNodes() || !HasNodesInLicense() || DoNotRedirectUser()) return;

        Response.Redirect("/Orion/Admin/DiscoveryCentral.aspx");
    }

    private void CheckRedirectToDashboard()
    {
        var dashboardId = GetSummaryDashboardID();
        if (string.IsNullOrEmpty(Request.QueryString["ViewID"]) && dashboardId > 0 && GetSummaryViewID() < 0)
        {
            // redirect to dashboard instead of view
            Response.Redirect($"/apps/platform/dashboard/{dashboardId}");
        }
    }

    private void ReportUnauthorizedRequest()
    {
        try
        {
            var indication = new AccessDeniedLoadingViewIndication(IndicationType.Orion_AccessDeniedLoadingView, Profile.UserName, ViewInfo.ViewID, ViewInfo.ViewTitle);
            IndicationPublisher.CreateV3().ReportIndication(indication);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Failed to send UserLogin notification for account '{0}'. Details:{1}", Profile.UserName, ex);
        }

        OrionErrorPageBase.TransferToErrorPage(Context,
            new ErrorInspectorDetails
            {
                Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323),
                Title = Resources.CoreWebContent.WEBDATA_VB0_567
            });
    }

    public override string ViewType
    {
        get { return "Summary"; }
    }
}
