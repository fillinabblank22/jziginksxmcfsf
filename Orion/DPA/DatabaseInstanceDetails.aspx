﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/DPA/DPAView.master" AutoEventWireup="true"
    CodeFile="DatabaseInstanceDetails.aspx.cs" Inherits="Orion_DPA_DatabaseInstanceDetails" %>
<%@ Import Namespace="SolarWinds.DPA.Common" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="DPAPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div class="DPA_BreadcrumbsWrapper">
        <orion:PluggableDropDownMapPath ID="DatabaseInstanceSiteMapPath" runat="server" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
    </div>
    <% } %>
    
    <!-- DB Instance status and name -->
	<h1><%= Resources.DPAWebContent.DbInstanceDetails_Caption %>
        <img src="<%= StatusIconsHelper.GetSmallStatusIcon(SwisEntities.DatabaseInstance, DatabaseInstance.Status) %>"/>
        <%= DatabaseInstance.Name %>
    
        <!-- Add also "on Node" if node is assigned -->
        <%if (DatabaseInstance.Data != null && DatabaseInstance.Data.NodeId.HasValue)
          { %>
            <%= Resources.DPAWebContent.DatabaseNodeConjunction %>
            <img src="<%= StatusIconsHelper.GetOrionEntityStatusIconFile(SwisEntities.Nodes, DatabaseInstance.Data.NodeId.Value, "Small") %>"/>
            <npm:NodeLink runat="server" ID="NodeLink"/>
        <% } %>
    </h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DPAMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="ResourceContainer2" />
    </orion:ResourceHostControl>
</asp:Content>

