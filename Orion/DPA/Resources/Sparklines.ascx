﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Sparklines.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_Sparklines" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Model.SparkLines" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<orion:Include runat="server" Module="DPA" File="Charts.js" Section="Middle" />

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <a id="<%= FullAnchorId %>" name="<%= AnchorId %>"></a>
	    <div class="DPA_MultiChartContainer" runat="server" ID="mainContent">
	        <asp:Panel ID="pnlLeft" runat="server" CssClass="DPA_Left DPA_ChartHeaderColumn">
	            <span><%: SparkLinesProvider.TableHeaderLabel %></span>
            </asp:Panel>
			<asp:Panel ID="pnlMiddle" runat="server" CssClass="DPA_Middle DPA_ChartHeaderColumn">
                <!-- This element is used to access ResourceWrapper element for this resource so that 
                we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
                <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
            </asp:Panel>
			<asp:Panel ID="pnlRight" runat="server" CssClass="DPA_Right DPA_ChartHeaderColumn">
                <span>
                   <%: Resources.DPAWebContent.SparklinesResource_HeaderName_ValueFromLastPoll %>
                </span>
            </asp:Panel>            
            <asp:Repeater ID="sparkLines" runat="server">
                <ItemTemplate>
                    <div class="DPA_SparkLineItem" automation="<%# ((SparkLineName) Container.DataItem).Id  %>" style="height: <%= this.CounterDivHeight %>px;">
                        <table>
                             <tr>
                                 <td class="DPA_Icon" style="width: <%= this.IconColumnWidth %>px">
                                 </td>
                                 <td class="DPA_Name" style="width: <%= this.NameColumnWidth %>px">
                                    <%# ((SparkLineName) Container.DataItem).Name  %>                                  
                                 </td>
                                 <td class="DPA_Chart"></td>
                                 <td class="DPA_LastValue" style="width: <%= this.LastValueColumnWidth %>px">
                                    <span class="DPA_LastValue"></span>
                                 </td>
                             </tr>    
                        </table>  
                        <hr />              
                    </div>                      
                </ItemTemplate>  
            </asp:Repeater>
        </div>
	</Content>
</dpa:DpaResourceWrapper>