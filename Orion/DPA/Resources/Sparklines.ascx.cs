﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Common.Utility;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Model.SparkLines;
using SolarWinds.DPA.Web.Providers.SparkLines;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_Sparklines : DpaDatabaseInstanceStandardChartResource, IResourceIsInternal
{
	private ISparkLinesProvider _sparkLinesProvider;
	private ResourceDataCategory _resourceDataCategory;
	private IList<SparkLineName> _sparklineNames;

	protected int IconColumnWidth = 29;
	protected int NameColumnWidth = 120;
	protected int LastValueColumnWidth = 85;
    protected IList<PartialResultsError> PartialResultsErrors { get; set; }

    protected int GlobalDatabaseInstanceId { get; set; }

	protected int CounterDivHeight
	{
		get
		{
			return Constants.SparklineHeight;
		}
	}

    protected string AnchorId
    {
        get
        {
            return String.Format("{0}{1}", SparkLinesProvider.AnchorId, GlobalDatabaseInstanceId);
        }
    }

    protected string FullAnchorId
    {
        get
        {
            return String.Format("resource{0}-{1}", Resource.ID, AnchorId);
        }
    }

	protected override string DefaultTitle
	{
	    get
	    {
	        return string.Empty;
	    }
	}

    protected ISparkLinesProvider SparkLinesProvider
    {
        get
        {
            if (_sparkLinesProvider == null)
            {
                var providerId = this.Resource.Properties[ResourcePropertiesKeys.ProviderId];

                _sparkLinesProvider = ServiceLocator.Current.GetInstance<ISparkLinesProvider>(providerId);

                if (_sparkLinesProvider == null) throw new NullReferenceException("_sparkLinesProvider");
            }

            return _sparkLinesProvider;
        }
    }

    public override string SubTitle
	{
		get
		{
			if (base.SubTitle.Contains(SolarWinds.DPA.Common.CommonConstants.ZoomRangePlaceholder))
			{
				DateTime endDateTime = DateTime.UtcNow;
				DateTime startDateTime = endDateTime.AddHours(-12);
				return base.SubTitle.Replace(SolarWinds.DPA.Common.CommonConstants.ZoomRangePlaceholder,
					String.Format("{0} - {1}", startDateTime.ToString("MMM d, yyyy"), endDateTime.ToString("MMM d, yyyy")));
			}
			else
			{
				return base.SubTitle;
			}
		}
	}

	public override string HelpLinkFragment
	{
		get
		{
            return SparkLinesProvider.HelpLinkFragment;
		}
	}

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
	{
		// make it empty to don't show EXPORT Button
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		// chart width and heigth
		this.Width = this.Resource.Width - IconColumnWidth - NameColumnWidth - LastValueColumnWidth - 4 + 1 - 20; // resource width - icon cell - name cell - last value cell - padding + chart left margin - resource padding
		this.Height = this.ForceDisplayOfNeedsEditPlaceholder ? 125 : 5 + 26 + 35; // chart top margin + range selector + scrollbar

		SetChartName(ChartNames.Sparklines);

		HandleInit(WrapperContents);

		if (this._sparklineNames != null)
		{
			this.Height += _sparklineNames.Count() * CounterDivHeight;
		}

		this.pnlLeft.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;
		this.pnlRight.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;
        mainContent.Style.Add("height", Height + "px");
		if (!this.ForceDisplayOfNeedsEditPlaceholder)
		{
			pnlMiddle.Style.Add("width", this.Width + "px");
		}
	}

	protected void Page_Init(object sender, EventArgs e)
	{
        using (var swisSettingsContext = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>().CreateContext(PartialResultsMode.EnablePartialResults))
        {
            GlobalDatabaseInstanceId = base.ViewHelper.GlobalDatabaseInstanceId;
            var databaseInstance = ViewHelper.DatabaseInstance;

            var partialResultsErrors = swisSettingsContext.GetPartialErrors();

            if (partialResultsErrors.Any())
            {
                DpaWrapper.ShowConnectivityIssueWarning(partialResultsErrors);
                mainContent.Visible = false;
                return;
            }

            DpaWrapper.DpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);

            // hide resource if database instance is not available
            if (GlobalDatabaseInstanceId <= 0 
                || databaseInstance == null
                // hide CPU sparklines when not defined for this database instance
                || !SparkLinesProvider.IsResourceVisible(databaseInstance))
            {
                base.Visible = false;
                return;
            }
            _sparklineNames = SparkLinesProvider.GetSparkLineNames(GlobalDatabaseInstanceId);

            if (_sparklineNames == null) throw new NullReferenceException("_sparklineNames");

            this.sparkLines.DataSource = this._sparklineNames;
            this.sparkLines.DataBind();
        }
	}

	public override ResourceLoadingMode ResourceLoadingMode
	{
		get { return ResourceLoadingMode.Ajax; }
	}

	protected override bool AllowCustomization
	{
		get { return Profile.AllowCustomize; }
	}

	public override string EditURL
	{
		get
		{
			return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
		}
	}

	public bool IsInternal
	{
		get { return true; }
	}

    protected override void SetChartInitializerScript()
    {
        var seriesTemplatesStringBuilder = new StringBuilder("seriesTemplates: {");
        if (_sparklineNames != null)
        {
            for (var i = 0; i < _sparklineNames.Count; i++)
            {
                seriesTemplatesStringBuilder.AppendFormat(
                    "'{0}':{{name: ' ',shadow: false,lineWidth: 1,color: 'black',yAxis: {1},stack: 0}},",
                    String.Format(Constants.SparklineSerieNameFormat, i),
                    i);
            }
        }

        seriesTemplatesStringBuilder.Append("}");
        var chartOptions = this.ChartOptions.Replace("seriesTemplates: {}", seriesTemplatesStringBuilder.ToString());

        const string formatScriptRefresh = @"
$(function () {{
    var refresh = function() {{ SW.DPA.Charts.SparkLinesChart.initializeStandardChart({0}, {1}); }};
    if((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{
        SW.Core.View.AddOnRefresh(refresh, '{2}');
    }}
    refresh();
}});";

        var scriptRefresh = InvariantString.Format(formatScriptRefresh, DisplayDetails, chartOptions, ChartPlaceHolder.ClientID);

        ScriptInitializer.InnerHtml = scriptRefresh;
    }
}