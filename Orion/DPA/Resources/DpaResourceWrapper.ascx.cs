﻿using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common.Models;

public partial class Orion_DPA_Resources_DpaResourceWrapper : UserControl
{
    public int? DpaServerId 
    {
        set
        {
            ResourceWrapperBase.DpaServerId = value;
        }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return DpaResourcePlaceHolder; }
    }

    public string LoadingIconClientId
    {
        get { return ResourceWrapperBase.LoadingIconClientId; }
    }

    public string ConnectivityIssueWarningClientId
    {
        get { return ResourceWrapperBase.ConnectivityIssueWarningClientId; }
    }

    public void ShowConnectivityIssueWarning(IEnumerable<PartialResultsError> partialResultsErrors = null)
    {
        ResourceWrapperBase.ShowConnectivityIssueWarning(partialResultsErrors);
    }

    public void HideConnectivityIssueWarning()
    {
        ResourceWrapperBase.HideConnectivityIssueWarning();
    }
    
    public string ErrorMessageClientId
    {
        get { return ResourceWrapperBase.ErrorMessageClientId; }
    }

    public void ShowErrorMessage()
    {
        ResourceWrapperBase.ShowErrorMessage();
    }

    public void HideErrorMessage()
    {
        ResourceWrapperBase.HideErrorMessage();
    }

    public void ShowManageButton(string text, string target)
    {
        ResourceWrapperBase.ShowManageButton(text, target);
    }

    public void HideEditButton()
    {
        ResourceWrapperBase.HideEditButton();
    }

    public PlaceHolder HeaderButtons
    {
        get { return ResourceWrapperBase.HeaderButtons; }
    }
}