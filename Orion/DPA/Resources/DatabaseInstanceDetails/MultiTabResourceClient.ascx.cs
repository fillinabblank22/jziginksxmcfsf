﻿using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_DPA_Controls_MultiTabResourceClient : DatabaseResponseTimeBaseResource, IResourceIsInternal
{
    public override IEnumerable<string> RequiredDynamicInfoKeys
    {
        get { return new[] { APMConstants.ResourceDynamicInfoKeySam }; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[0]; }
    }

    protected string CustomEditUrl { get; private set; }

    protected override void OnLoad(EventArgs e)
    {
        // Hide resource when location is unknown,
        // .. or client type resource is placed on SAM non-client view.
        if ((ViewHelper.Location == ResourceLocation.Unknown) || (ViewHelper.Location != ResourceLocation.SAMClientView))
        {
            Visible = false;
            return;
        }

        MultiTabControl.Initialize(this.Resource.ID, GetInterfaceInstance<INodeProvider>(), ViewHelper,
            RawResourceProperties(this.Resource.Properties), this.DpaWrapper.ConnectivityIssueWarningClientId,
            ParseResourceProperties, AdjustResourceProperties);


        // show CONFIGURE CLIENT APPLICATIONS button
        if (Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent
                    .ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel,
                PageUrlHelper.RelationshipManagementClientApplications);
        }

        base.OnLoad(e);

        DpaWrapper.DpaServerId =
            GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);
    }

    protected override void OnPreRender(EventArgs e)
    {
        // skip pre-render if resource is hidden
        if (!this.Visible)
            return;

        this.DpaWrapper.HideConnectivityIssueWarning();

        if (MultiTabControl.PartialResultsErrors.Any())
        {
            DpaWrapper.ShowConnectivityIssueWarning(MultiTabControl.PartialResultsErrors);
            resourceContent.Visible = false;
            return;
        }

        base.OnPreRender(e);
    }
    
    #region customize page related

    public bool IsInternal { get { return true; } }

    #endregion
    
    public override string EditURL
    {
        get
        {
            if (string.IsNullOrEmpty(this.CustomEditUrl))
            {
                return base.EditURL;
            }
            return MultiTabControl.FormatEditUrl(this.CustomEditUrl);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/DPA/Controls/EditResourceControls/DatabaseResponseTime.ascx";
        }
    }
}
