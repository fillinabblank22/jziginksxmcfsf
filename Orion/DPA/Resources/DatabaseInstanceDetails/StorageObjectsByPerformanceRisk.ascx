﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageObjectsByPerformanceRisk.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_StorageObjectsByPerformanceRisk" %>

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div class="DPA_StoragePerformanceRiskWrapper">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">           
            $(document).ready(function() {
                new SW.DPA.UI.StorageObjectsByPerformanceRiskDetailGrid().initialize('<%= CustomTable.UniqueClientID %>', 
                                        '<%= SearchControl.SearchBoxClientID %>', 
                                        '<%= SearchControl.SearchButtonClientID %>', 
                                        '<%= PageSize %>');
            });
        </script>
    </Content>
</dpa:DpaResourceWrapper>
