﻿using System;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_DatabaseInstanceDetails_Advisors : DpaDatabaseInstanceBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_Advisors_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceAdvisors";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DpaWrapper.DpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);
        SortablePageableTable.UniqueClientID = Resource.ID;
    }
}