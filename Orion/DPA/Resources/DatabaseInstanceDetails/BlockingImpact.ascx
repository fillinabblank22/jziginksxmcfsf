﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockingImpact.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_BlockingImpact" %>

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="LatestDeadlocksTable" Src="~/Orion/DPA/Controls/LatestDeadlocksTable.ascx" %>


<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div runat="server" id="ResourceContent">
            <asp:UpdatePanel runat="server" ID="Update" ChildrenAsTriggers="True">
                <ContentTemplate>
                    <div id="TabHeaders" class="DPA_tabHeaders" runat="server">
                        <asp:PlaceHolder runat="server" ID="TabHeadersPlaceholder"></asp:PlaceHolder>
                    </div>
                    <div class="tabContents DPA_BlockingImpactResource">
                        <asp:PlaceHolder runat="server" ID="TabContentsPlaceholder"></asp:PlaceHolder>
                        <dpa:LatestDeadlocksTable runat="server" ID="LatestDeadlocksTable" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="DPA_BlockingImpactResource" id="NoDataMessage" runat="server" style="overflow-y: hidden;">
            <div class="chartDataNotAvailableArea">
                <center>
                    <div id="chartDataNotAvailable">
                        <table style="height: 100%">
                            <tbody>
                                <tr>
                                    <td style="vertical-align: middle">
                                        <div class="DPA_ChartNoDataAvailable">
                                            <p><%: Resources.DPAWebContent.NoDataAvailableMessage %></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </center>
            </div>
        </div>
    </Content>
</dpa:DpaResourceWrapper>
