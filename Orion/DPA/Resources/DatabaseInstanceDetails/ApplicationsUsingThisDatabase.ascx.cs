﻿using System;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_DatabaseInstanceDetails_ApplicationsUsingThisDatabase : DpaDatabaseInstanceBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_ApplicationsUsingThisDatabase_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceApplicationsUsingThisDatabase";
        }
    }

    protected string LearnMoreAboutSamDpaIntegrationHelpLink
    {
        get
        {
            return HelpHelper.GetHelpUrl("OrionDPALearnMoreAboutSamDpaIntegration");
        }
    }

    protected int GlobalDatabaseInstanceId
    {
        get
        {
            return ViewHelper.GlobalDatabaseInstanceId;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // show CONFIGURE CONFIGURE CLIENT APPLICATIONS button only for ADMINS and when APM is installed
        if (Settings.IsApmInstalled && Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel,
                PageUrlHelper.RelationshipManagementClientApplications);
        }
        else
        {
            startDPAIntegrationButton.Visible = false;
        }

        if (Settings.IsApmInstalled)
        {
            apmInstalledContent.Visible = true;
            apmNotInstalledContent.Visible = false;
            SortablePageableTable.UniqueClientID = Resource.ID;
            startDPAIntegrationButton.PostBackUrl = PageUrlHelper.RelationshipManagementClientApplications;
        }
        else
        {
            apmInstalledContent.Visible = false;
            apmNotInstalledContent.Visible = true;
        }
        DpaWrapper.DpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);
    }
}