﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IopsPerformance.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_IopsPerformance" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Models.SRM" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper" runat="server">
	<Content>
	    <div id="srm-performance-chart-buttons-<%=Resource.ID%>" class="DPA-performance-buttons">
            <span><%= Resources.DPAWebContent.BlockIOPSPerformancePerLun_DataSet %></span>
            <input id="btnRead" class="DPA-srm-performance-button" type="button" value="<%= Resources.DPAWebContent.BlockIOPSPerformancePerLun_ButtonRead %>">
            <input id="btnWrite" class="DPA-srm-performance-button" type="button" value="<%= Resources.DPAWebContent.BlockIOPSPerformancePerLun_ButtonWrite %>">
            <input id="btnOther" class="DPA-srm-performance-button" type="button" value="<%= Resources.DPAWebContent.BlockIOPSPerformancePerLun_ButtonOther %>">
            <input id="btnTotal" class="DPA-srm-performance-button DPA-srm-performance-button-selected" type="button" value="<%= Resources.DPAWebContent.BlockIOPSPerformancePerLun_ButtonTotal %>">
        </div>
        <div id="srm-performance-chart-<%=Resource.ID%>"></div>
        <table id="srm-performance-legend-<%=Resource.ID%>" class="DPA_SrmPerformance_ChartLegendTable"></table>
        
        <script type="text/javascript">
            $(function () {
                var cssSelectorPrefix = "#srm-performance-chart-buttons-<%=Resource.ID%> ";
                var performanceChart<%=Resource.JavaScriptFriendlyID%> = new SW.DPA.Charts.Performance();
                performanceChart<%=Resource.JavaScriptFriendlyID%>.init(false, $.extend(true, <%=Data%>, { FilterField:"<%=StatisticFields.IOPSTotal %>" }));

                $(cssSelectorPrefix + "#btnRead").click(function () {
                    performanceChart<%=Resource.JavaScriptFriendlyID%>.init(false, $.extend(true, <%=Data%>, { FilterField:"<%=StatisticFields.IOPSRead %>" }));
                    setSelected("#btnRead");
                });
                $(cssSelectorPrefix + "#btnWrite").click(function () {
                    performanceChart<%=Resource.JavaScriptFriendlyID%>.init(false, $.extend(true, <%=Data%>, { FilterField:"<%=StatisticFields.IOPSWrite %>" }));
                    setSelected("#btnWrite");
                });
                $(cssSelectorPrefix + "#btnOther").click(function () {
                    performanceChart<%=Resource.JavaScriptFriendlyID%>.init(false, $.extend(true, <%=Data%>, { FilterField:"<%=StatisticFields.IOPSWrite %>" }));
                    setSelected("#btnOther");
                });
                $(cssSelectorPrefix + "#btnTotal").click(function () {
                    performanceChart<%=Resource.JavaScriptFriendlyID%>.init(false, $.extend(true, <%=Data%>, { FilterField:"<%=StatisticFields.IOPSTotal %>" }));
                    setSelected("#btnTotal");
                });

                function setSelected(buttonId) {
                    var myObjects = ["#btnRead", "#btnWrite", "#btnOther", "#btnTotal"];

                    $.each(myObjects, function(index, value) {
                        if (value == buttonId) {
                            $(cssSelectorPrefix + value).addClass("DPA-srm-performance-button-selected");
                        } else {
                            $(cssSelectorPrefix + value).removeClass("DPA-srm-performance-button-selected");
                        }
                    });
                }
            });


        </script>
    </Content>
</orion:ResourceWrapper>
