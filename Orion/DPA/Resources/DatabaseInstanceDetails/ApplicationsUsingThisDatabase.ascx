﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsUsingThisDatabase.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_ApplicationsUsingThisDatabase" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div runat="server" ID="apmInstalledContent">
	        <div id="Resource-<%= Resource.ID %>-Content" class="DPA_ApplicationsUsingThisDatabase">
	            <dpa:SortablePageableTable ID="SortablePageableTable" runat="server" />
	        </div>
	        <div id="Resource-<%= Resource.ID %>-NoContent" style="display:none">
	           <p><%: Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_NoApplicationFound %></p>
            
               <div class="DPA_GettingStartedBox">
                  <div class="DPA_First">
                     <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                     <h3><b><%: Resources.CoreWebContent.WEBDATA_PS0_1 %></b> <%: Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_GettingStartedTitle %></h3>
                  </div>

                  <div><%: Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_GettingStartedText %></div>

                  <div class="DPA_Buttons">
                      <orion:LocalizableButton runat="server" ID="startDPAIntegrationButton" PostBackUrl="<%# PageUrlHelper.ServerManagement %>" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel %>" />
                  </div>
               </div>
	        </div>
              <script type="text/javascript">
                  $(function () {
                      var localizedStrings = {
                          tableTitle: '<%: Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_TableTitle %>'
                          },
                          tableIndex = {},
                          setTableIndex = function (columns) { $.each(columns, function (i, n) { tableIndex[n] = i; }); },
                          loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>'),
                          errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');

                      SW.DPA.UI.SortablePageableTable.initialize(
                      {
                          uniqueId: '<%= this.Resource.ID %>',
                          pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                              loadingIcon.show();
                              errorMessage.hide();
                              var globalDatabaseInstanceId = '<%= GlobalDatabaseInstanceId %>';
                              SW.Core.Services.callController("/api/DpaApplicationsUsingDatabases/GetApplicationsByDatabaseInstance", {
                                  GlobalDatabaseInstanceIds: [globalDatabaseInstanceId],
                                  Limit: pageSize,
                                  PageIndex: pageIndex,
                                  OrderByClause: ""
                              }, function(result) {
                                  loadingIcon.hide();
                                  if (result.TotalRows > 0) {
                                      $('#Resource-<%= Resource.ID %>-NoContent').hide();
                                      $('#Resource-<%= Resource.ID %>-Content').show();
                                      setTableIndex(result.DataTable.Columns);
                                      onSuccess(result);
                                  } else {
                                      $('#Resource-<%= Resource.ID %>-Content').hide();
                                      $('#Resource-<%= Resource.ID %>-NoContent').show();
                                  }


                              }, function(errorResult) {
                                  errorMessage.show(errorResult);
                                  loadingIcon.hide();
                              });
                          },
                          initialPageIndex: 0,
                          rowsPerPage: 10,
                          columnSettings: {
                              'ApplicationIcon': {
                                  caption: '',
                                  isHtml: true,
                                  allowSort: false,
                                  template: _.templateExt('<a href="{{{ApplicationLink}}}"><img src="{{{ApplicationIcon}}}" /></a>'),
                                  formatter: function(cellValue, rowArray) {
                                      return this.template({
                                          ApplicationLink: rowArray[tableIndex.ApplicationLink],
                                          ApplicationIcon: rowArray[tableIndex.ApplicationIcon]
                                      });       
                                  }
                              },
                              'ApplicationName': {
                                  caption: localizedStrings.tableTitle,
                                  isHtml: true,
                                  allowSort: false,
                                  formatter: function (cellValue, rowArray) {
                                      return SW.DPA.Common.FormatDatabaseInstanceLink(
                                          rowArray[tableIndex.ApplicationLink], undefined, rowArray[tableIndex.ApplicationName],
                                            rowArray[tableIndex.NodeLink], rowArray[tableIndex.NodeIcon], rowArray[tableIndex.NodeName],
                                            <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>);
                                  }
                              }
                          }
                      });

                      var refresh = function () { SW.DPA.UI.SortablePageableTable.refresh('<%= this.Resource.ID %>'); };
                      SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                      refresh();
                  });
              </script>
            </div>
        <div runat="server" ID="apmNotInstalledContent">
            <img src="/Orion/DPA/images/Samples/client-server-db-instance-empty.png" alt="sample" />
            <div class="DPA_AdvisorSample">
                <ul>
                    <li><%: Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_WithoutSAMText %></li>
                </ul>
                <p><a href="<%: LearnMoreAboutSamDpaIntegrationHelpLink %>" target="_blank">&#187; <%: Resources.DPAWebContent.LearnMoreAboutBenefitsSAMIntegrationLink_Label %></a></p>
            </div> 
        </div>
	</Content>
</dpa:DpaResourceWrapper>