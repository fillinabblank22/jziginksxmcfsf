﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseInstanceDetails.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_DatabaseInstanceDetails" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div id="resourceContent" runat="server" Visible="False" class="DPA_DatabaseInstanceDetails">
	        <% if(PartiallyCompatibleMessage != null) { %>
	            <div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn">
	                <span class="sw-suggestion-icon"></span><%: PartiallyCompatibleMessage %>
	                <%: SolarWinds.DPA.Strings.Resources.PartiallyCompatibleWarningGoToLink %>
                    <a href="<%: PageUrlHelper.ServerManagement %>"><%: SolarWinds.DPA.Strings.Resources.PartiallyCompatibleWarningLinkLabel %></a>
	            </div>
            <% } %>
	        <table>
	            <tbody>
	                <tr>
	                    <td><%: Resources.DPAWebContent.DatabaseInstanceDetailsResource_Table_Name %>:</td>
                        <td><span automation="DPA_DatabaseInstanceName"><%: DatabaseInstanceName %></span>
                              <% if(HasNode) { %>
                                <span class="DPA_NoWrap">
                                <%: Resources.DPAWebContent.DatabaseNodeConjunction %> 
                                <a href="<%: NodeLink %>" automation="DPA_NodeLink"><img src="<%: NodeIcon %>" automation="DPA_NodeStatusIcon" /><span automation="DPA_NodeName"><%: NodeName %></span></a>
                                </span>
                            <% } %>
                        </td>
	                </tr>
                    <tr>
	                    <td><%: Resources.DPAWebContent.DatabaseInstanceDetailsResource_Table_Status %>:</td>
                        <td><img src="<%: DatabaseInstanceIcon %>" automation="DPA_DatabaseInstanceIcon" />  <%: StatusHelper.DatabaseInstanceStatusDescription(DatabaseInstanceStatusId, IsLicensed) %></td>
	                </tr>
                    <tr>
                        <td><%: Resources.DPAWebContent.DatabaseInstanceDetailsResource_Table_DpaServer %>:</td>
                        <td><img src="<%: DpaServerStatusIconUrl %>" automation="DPA_DpaServerStatusIcon" /><a href="<%: DpaServerDetailUrl %>" automation="DPA_DpaServerStatusLink"><span automation="DPA_DpaServerName"><%: DpaServerName %></span> </a></td>
                    </tr>
	            </tbody>
	        </table>
	    </div>
	    <div runat="server" ID="sampleResourceContent" class="DPA_SampleResourceContent" Visible="False">
	        <img src="/Orion/DPA/images/Samples/databaseInstanceDetails-Sample.png" alt="sample" />

            <div class="DPA_SampleBox">
                <p><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxParagraphText %></p>

                <ul>
                    <li><b><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet1Title %></b> - <%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet1Text %></li>
                    <li><b><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet2Title %></b> - <%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet2Text %></li>
                    <li><b><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet3Title %></b> - <%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet3Text %></li>
                    <li><b><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet4Title %></b> - <%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_BoxBullet4Text %></li>
                </ul>
                
                <p>
                    <a href="<%: LearnMoreAboutOrionIntegrationHelpLink %>" target="_blank">&raquo; <%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HelpLinkLabel %></a>
                </p>
            </div>
            
            <h4><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowToTitle %></h4>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">1</div>
                <div class="DPA_Content">
                    <p><%= FormatSampleHowBullet1Text %></p>
                    
                    <% if (this.Profile.AllowAdmin) { %>
                    <div>
                        <div class="sw-btn-bar-wizard">
                        <a target="_blank" href="#" onclick="return SW.DPA.LinksToDpaSelector.show('/api/DpaLinksToDpaj/RegisterDbInstance/')" class="sw-btn sw-btn-primary"><span class="sw-btn-c">
                            <span class="sw-btn-t"><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowBullet1Button1Label %></span>
                        </span></a>
                        </div>
                    </div>
                    <% } %>
                </div>
                <div class="DPA_Clear"></div>
            </div>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">2</div>
                <div class="DPA_Content">
                    <p><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowBullet2Text %></p>
                    
                    <% if(this.Profile.AllowAdmin) { %>
                    <div>
                        <div class="sw-btn-bar-wizard">
                        <a href="<%: PageUrlHelper.ServerManagement %>" class="sw-btn sw-btn-primary"><span class="sw-btn-c">
                            <span class="sw-btn-t"><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowBullet2Button1Label %></span>
                        </span></a>
                            
                        <a href="<%: PageUrlHelper.AssignDatabaseInstance(ViewHelper.ApplicationId, ViewNodeID) %>" class="sw-btn"><span class="sw-btn-c">
                            <span class="sw-btn-t"><%: Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowBullet2Button2Label %></span>
                        </span></a>
                        </div>
                    </div>
                    <% } %>
                </div>
                <div class="DPA_Clear"></div>
            </div>  
	    </div>
	    
    </Content>
</dpa:DpaResourceWrapper>