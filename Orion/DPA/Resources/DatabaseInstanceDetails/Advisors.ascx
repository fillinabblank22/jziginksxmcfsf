﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Advisors.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_Advisors" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div id="DPA_Advisors-<%= Resource.ID %>-Content" >
	        <div class="DPA_AdvisorsSelect">
	            <label for="Select-<%= this.Resource.ID %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeverity_Label %>:</label>
                <select id="Select-<%= this.Resource.ID %>">
                    <option value="-1"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_All %></option>
                    <option value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Normal %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Normal %></option>
                    <option value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Warning %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Warning %></option>
                    <option  value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Critical %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Critical %></option>
                </select>
            </div>

            <div class="DPA_AdvisorsTable">
                <dpa:SortablePageableTable ID="SortablePageableTable" runat="server" />
                <input type="hidden" id="OrderBy-<%= this.Resource.ID %>" value="[RunTime] DESC" />
            </div>
	    </div>
        <div id="DPA_Advisors-<%= Resource.ID %>-NoData" class="DPA_SampleImage" style="display:none">
            <img src="/Orion/DPA/images/Samples/advisors-sample.png" alt="sample" />
        </div>
        
        <script type="text/javascript">
    $(function () {
        var advisorIconFileKey = 0;
        var advisorIconToolTipTextKey = 1;
        var advisorDetailUrlKey = 3;

        var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>');
        var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= DpaWrapper.ConnectivityIssueWarningClientId %>');
        var errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');

        SW.DPA.UI.SortablePageableTable.initialize(
        {
            uniqueId: '<%= this.Resource.ID %>',
            pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                loadingIcon.show();
                connectivityIssueWarning.hide();
                errorMessage.hide();

                var uniqueId = '<%= this.Resource.ID %>';
                var globalDatabaseInstanceId = '<%= base.ViewHelper.GlobalDatabaseInstanceId %>';
                var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                var currentSeverity = $("#Select-" + uniqueId + " option:selected").val();
                SW.Core.Services.callController("/api/DpaAdvisors/GetAdvisorsForDatabaseInstance", {
                    GlobalDatabaseInstanceId: globalDatabaseInstanceId,
                    Severity: currentSeverity,
                    Limit: pageSize,
                    PageIndex: pageIndex,
                    OrderByClause: currentOrderBy
                }, function (result) {
                    loadingIcon.hide();

                    if (typeof result.Metadata.PartialResultErrors != 'undefined' && result.Metadata.PartialResultErrors.length > 0) {
                        connectivityIssueWarning.show(result.Metadata.PartialResultErrors);
                    }

                    if (result.Metadata.IntegrationEstablished) {
                        $('#DPA_Advisors-<%= Resource.ID %>-NoData').hide();
                        $('#DPA_Advisors-<%= Resource.ID %>-Content').show();
                    } 

                    onSuccess(result);
                }, function (errorResult) {
                    errorMessage.show(errorResult);
                    loadingIcon.hide();
                });
            },
            initialPageIndex: 0,
            rowsPerPage: 10,
            columnSettings: {
                'AdvisorIconFile': {
                    caption: '',
                    isHtml: true,
                    allowSort: false,
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" class="NoTip" target="_blank" title="{1}"><img src="{2}" /></a>',
                            rowArray[advisorDetailUrlKey],
                            rowArray[advisorIconToolTipTextKey],
                            rowArray[advisorIconFileKey]);
                    }
                },
                'AdvisorMessage': {
                    caption: "<%= Resources.DPAWebContent.DPA_AdvisorsTableHeader_AdvisorMessage %>",
                    isHtml: true,
                    allowSort: true,
                    sortingProperty: 'AlarmLevel',
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" target="_blank">{1}</a>', rowArray[advisorDetailUrlKey], cellValue);
                    }
                },
                "Date": {
                    caption: "<%= Resources.DPAWebContent.DPA_AdvisorsTableHeader_Date %>",
                    isHtml: false,
                    allowSort: true,
                    sortingProperty: 'RunTime'
                }
            }
        });

        var refresh = function () { SW.DPA.UI.SortablePageableTable.refresh('<%= this.Resource.ID %>'); };
        SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
        refresh();
        $("#Select-<%= this.Resource.ID %>").change(function() {
            SW.DPA.UI.SortablePageableTable.refresh('<%= this.Resource.ID %>');
        });
    });
</script>
	</Content>
</dpa:DpaResourceWrapper>