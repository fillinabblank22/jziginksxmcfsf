﻿using System;
using System.Globalization;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_StorageObjectsByCapacityRisk : DpaDatabaseInstanceBaseResource
{
    public ResourceSearchControl SearchControl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        IncludeFilesHelper.IncludeScriptsForResources();
        IncludeFilesHelper.IncludeStylesForResources();
        IncludeFilesHelper.IncludeScriptsForCharts();

        if (!Settings.IsSrmInstalled)
        {
            Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        SearchControl = SearchControl;

        var query = string.Format(CultureInfo.InvariantCulture, @"
SELECT DISTINCT _ObjectId, 
       Resource,
       _IconFor_Resource, 
       _LinkFor_Resource, 
       Type, 
       Array, 
       _LinkFor_Array,        
       '' AS Last7Days,
       {0} AS Warning, 
       {1} AS Critical, 
       100 AS ATCapacity, 
       _TotalCapacity, 
       CASE WHEN _UsedCapacityPercentage > 100 THEN 100 ELSE _UsedCapacityPercentage END AS _UsedCapacityPercentage,
       _CapacityRunoutOffset,
       _CapacityRunoutSlope,
       _IconFor_Array
FROM (
	    SELECT rel.Lun.LUNID AS _ObjectId,
               rel.Lun.DisplayName as Resource, 
               concat('/Orion/StatusIcon.ashx?entity=Orion.SRM.LUNs&status=', ISNULL(rel.Lun.Status, 0)) AS _IconFor_Resource,
               rel.Lun.DetailsUrl AS _LinkFor_Resource,
               '{2}' AS Type, 
               rel.Lun.StorageArray.DisplayName as Array,  
               rel.Lun.StorageArray.DetailsUrl AS _LinkFor_Array,
               concat('/Orion/StatusIcon.ashx?entity=',
                                    CASE WHEN rel.Lun.StorageArray.IsCluster = true THEN 'Orion.SRM.Clusters' ELSE 'Orion.SRM.StorageArrays' END
                                    ,'&status=', ISNULL(rel.Lun.StorageArray.Status, 0)) AS _IconFor_Array,
               rel.Lun.CapacityTotal AS _TotalCapacity, 
	           ((rel.Lun.CapacityAllocated * 1.0 / rel.Lun.CapacityTotal) * 100) AS _UsedCapacityPercentage,
               rel.Lun.CapacityRunoutSlope AS _CapacityRunoutSlope,
               rel.Lun.CapacityRunoutOffset AS _CapacityRunoutOffset
        FROM Orion.DPA.DatabaseInstanceLun (nolock=true) rel
        WHERE rel.Lun.Thin = true
          AND rel.Lun.CapacityTotal > 0
          AND rel.Lun.StorageArray.Unmanaged = FALSE
          AND rel.GlobalDatabaseInstanceID = {3}
)",
            Settings.SrmWarningThresholdCapacityRunOut,
            Settings.SrmCriticalThresholdCapacityRunOut,
            SolarWinds.DPA.Strings.Resources.StorageObjectsByCapacityRisk_Lun_Title,
            ViewHelper.GlobalDatabaseInstanceId
            );

        CustomTable.UniqueClientID = Resource.ID;
        CustomTable.SWQL = query;
        CustomTable.SearchSWQL = $"{query} WHERE {SearchSwql}";
    }

    protected override string DefaultTitle
    {
        get
        {
            return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_StorageObjectsByCapacityRisk_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageObjectsCapacityRisks";
        }
    }

    public string SearchSwql
    {
        get { return "([Resource] LIKE '%${SEARCH_STRING}%' OR [Array] LIKE '%${SEARCH_STRING}%')"; }
    }
}