﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StoragesUsedByThisDatabase.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_StoragesUsedByThisDatabase" %>

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>


<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div id="Resource-<%= Resource.ID %>-Content" class="DPA_StoragesUsedByThisDatabase">
            <dpa:SortablePageableTable ID="SortablePageableTable" runat="server" />
        </div>
        <div id="Resource-<%= Resource.ID %>-NoContent" class="DPA_SampleResourceContent" style="display: none">
            <img src="/Orion/DPA/images/Samples/StorageUsedByMyDatabasesSample.png" alt="sample" />
            
            <div class="DPA_SampleBox">
                <p><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Paragraph1 %></p>

                <ul>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li1 %></li>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li2 %></li>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li3  %></li>
                </ul>
            </div>
            
            <h4><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_HowTo %></h4>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">1</div>
                <div class="DPA_Content">
                    <p><%= BulletOneContent %></p>
                </div>
                <div class="DPA_Clear"></div>
            </div>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">2</div>
                <div class="DPA_Content">
                    <p><%= BulletTwoContent %></p>
                </div>
                <div class="DPA_Clear"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                var localizedStrings = {
                        tableTitle: '<%: Resources.DPAWebContent.StoragesUsedByThisDatabaseResource_TableTitle %>'
                    },
                    tableIndex = {},
                    setTableIndex = function(columns) { $.each(columns, function(i, n) { tableIndex[n] = i; }); },
                    loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>'),
                    errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');

                SW.DPA.UI.SortablePageableTable.initialize(
                {
                    uniqueId: '<%= this.Resource.ID %>',
                    pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                        loadingIcon.show();
                        errorMessage.hide();
                        var globalDatabaseInstanceId = '<%= GlobalDatabaseInstanceId %>';
                        SW.Core.Services.callController("/api/DpaStoragesUsedByDatabases/GetStoragesByDatabaseInstance", {
                            GlobalDatabaseInstanceIds: [globalDatabaseInstanceId],
                            Limit: pageSize,
                            PageIndex: pageIndex,
                            OrderByClause: ""
                        }, function(result) {
                            loadingIcon.hide();
                            if (result.TotalRows > 0) {
                                $('#Resource-<%= Resource.ID %>-NoContent').hide();
                                $('#Resource-<%= Resource.ID %>-Content').show();
                                setTableIndex(result.DataTable.Columns);
                                onSuccess(result);
                            } else {
                                $('#Resource-<%= Resource.ID %>-Content').hide();
                                $('#Resource-<%= Resource.ID %>-NoContent').show();
                            }


                        }, function(errorResult) {
                            errorMessage.show(errorResult);
                            loadingIcon.hide();
                        });
                    },
                    initialPageIndex: 0,
                    rowsPerPage: 10,
                    columnSettings: {
                        'StorageIcon': {
                            caption: '',
                            isHtml: true,
                            allowSort: false,
                            template: _.templateExt('<a href="{{{StorageLink}}}"><img src="{{{StorageIcon}}}" /></a>'),
                            formatter: function(cellValue, rowArray) {
                                return this.template({
                                    StorageLink: rowArray[tableIndex.StorageLink],
                                    StorageIcon: rowArray[tableIndex.StorageIcon]
                                });
                            }
                        },
                        'StorageName': {
                            caption: localizedStrings.tableTitle,
                            isHtml: true,
                            allowSort: false,
                            formatter: function(cellValue, rowArray) {
                                return SW.DPA.Common.FormatDatabaseInstanceLink(
                                    rowArray[tableIndex.StorageLink], undefined, rowArray[tableIndex.StorageName]);
                            }
                        }
                    }
                });

                var refresh = function() { SW.DPA.UI.SortablePageableTable.refresh('<%= this.Resource.ID %>'); };
                SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</dpa:DpaResourceWrapper>
