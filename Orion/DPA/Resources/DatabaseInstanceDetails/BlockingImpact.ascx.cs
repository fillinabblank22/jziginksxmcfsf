﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Extensions;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_BlockingImpact : BlockingImpactBaseResource, IResourceIsInternal
{
    private const string CssClassItem = "DPA_item";
    private const string CssClassActive = "DPA_active";
    private const string ResourcePath = "~/Orion/DPA/Controls/CustomChartControl.ascx";

    private readonly Log _log = new Log();

    private IList<PartialResultsError> _partialResultsErrors;

    private readonly Dictionary<string, object> _defaultProperties = new Dictionary<string, object>
    {
        {"ShowTitle", "0"},
        {"ChartSubTitle", "$[ZoomRange]"},
        {"ChartDateSpan", "1"},
        {"ChartInitialZoom", "2h"},
        {"SampleSize", "10"}
    };

    private string ActiveTabId
    {
        get
        {
            var defaultTab = Tabs.FirstOrDefault(t => t.TabId == DefaultTabId) ?? Tabs.FirstOrDefault();
            return ViewState.GetStrictOrDefault(ActiveTabStateKey, (defaultTab != null ? defaultTab.TabId : string.Empty));
        }
        set { ViewState[ActiveTabStateKey] = value; }
    }

    private string ActiveTabStateKey
    {
        get { return string.Format("BlockingImpactResource{0}_ActiveTabId", Resource.ID); }
    }

    private Tab ActiveTab
    {
        get { return Tabs.FirstOrDefault(t => t.TabId == ActiveTabId); }
    }

    protected override void OnLoad(EventArgs e)
    {
        IncludeFilesHelper.IncludeScriptsForCharts();

        DpaWrapper.DpaServerId =
            GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);

        using (
            var swisSettingsContext =
                ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>()
                    .CreateContext(PartialResultsMode.EnablePartialResults))
        {
            if (ViewHelper.DatabaseInstance == null || ViewHelper.DatabaseInstance.Type == DatabaseInstanceType.DB2)
            {
                Visible = false;
                return;
            }

            NoDataMessage.Visible = false;
            
            var tabs = FilterTabs(Tabs, ViewHelper.DatabaseInstance.Type).ToList();

            var dbiNetObject =
                $"{NetObjectPrefix.DatabaseInstance}:{ViewHelper.DatabaseInstance.GlobalDatabaseInstanceId}";

            _defaultProperties.Add(Constants.ContextNetObject, dbiNetObject);
            _defaultProperties.Add(Constants.DataNetObjects, new[] { dbiNetObject });
            _defaultProperties.Add(Constants.ConnectivityIssueWarningId, DpaWrapper.ConnectivityIssueWarningClientId);

            ParseResourceProperties(Resource.Properties.OfType<DictionaryEntry>());

            if (HideEditButton)
            {
                DpaWrapper.HideEditButton();
            }
            else if (tabs.All(t => t.TabId != DefaultTabId))
            {
                DefaultTabId = tabs.First().TabId;
            }

            if (DefaultTabOnly && tabs.All(t => t.TabId != DefaultTabId))
            {
                NoDataMessage.Visible = true;
            }
            else
            {
                NoDataMessage.Visible = false;

                RenderTabHeaders(tabs);
                ParseActiveTab();
                RenderTabContent();
            }

            LatestDeadlocksTable.Initialize(ViewHelper.GlobalDatabaseInstanceId, Resource.ID,
                DpaWrapper.ConnectivityIssueWarningClientId, DpaWrapper.ErrorMessageClientId,
                DpaWrapper.LoadingIconClientId);

            if (ActiveTab.TabId == Constants.DeadlocksTabId)
            {
                LatestDeadlocksTable.Show();
            }
            else
            {
                LatestDeadlocksTable.Hide();
            }

            _partialResultsErrors = swisSettingsContext.GetPartialErrors();
        }

        if (!_partialResultsErrors.Any())
        {
            RenderActiveTab();
        }

        base.OnLoad(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        DpaWrapper.HideConnectivityIssueWarning();

        if (_partialResultsErrors.Any())
        {
            DpaWrapper.ShowConnectivityIssueWarning(_partialResultsErrors);
            ResourceContent.Visible = false;
            return;
        }

        base.OnPreRender(e);
    }

    private void RenderTabHeaders(IEnumerable<Tab> tabs)
    {
        TabHeadersPlaceholder.Controls.Clear();

        if (DefaultTabOnly)
            return;

        foreach (var tab in tabs)
        {
            AddTabHeader(tab.TabId, tab.TabName);
        }
    }

    private void AddTabHeader(string tabId, string name)
    {
        var ctlTabHeader = new LinkButton
        {
            ID = tabId,
            Text = name,
            CssClass = CssClassItem
        };
        ctlTabHeader.Attributes["automation"] = tabId;
        TabHeadersPlaceholder.Controls.Add(ctlTabHeader);
    }

    private void ParseActiveTab()
    {
        if (!IsPostBack)
        {
            return;
        }
        var target = Request.Params["__EVENTTARGET"];
        var tabIndex = TabHeadersPlaceholder.Controls.Cast<Control>().IndexOf(th => th.UniqueID == target);
        if (tabIndex == -1)
        {
            return;
        }
        var tab = Tabs.GetValueOrDefault(tabIndex);
        ActiveTabId = tab == null ? ActiveTabId : tab.TabId;
    }

    private void RenderActiveTab()
    {
        var tabs = TabHeadersPlaceholder.Controls.OfType<LinkButton>();
        foreach (var tab in tabs)
        {
            tab.CssClass = CssClassItem;
            if (tab.ID == ActiveTabId)
            {
                tab.CssClass += " " + CssClassActive;
            }
        }
    }

    void RenderTabContent()
    {
        if (ActiveTab != null)
        {
            var innerControl = LoadResource(ResourcePath);
            var provider = innerControl as IPropertyProvider;
            if (provider != null)
            {
                SetProperties(provider, _defaultProperties);
                SetProperties(provider, ActiveTab.Properties);
            }
            TabContentsPlaceholder.Controls.Add(innerControl);
        }
    }

    private Control LoadResource(string resourcePath)
    {
        try
        {
            return Page.LoadControl(resourcePath);
        }
        catch (Exception ex)
        {
            var message = string.Format(CultureInfo.InvariantCulture, "failed to load resource '{0}'", resourcePath);
            _log.Error(message, ex);
            return new HtmlGenericControl("div")
            {
                InnerText = message
            };
        }
    }

    private void SetProperties(IPropertyProvider resource, Dictionary<string, object> props)
    {
        foreach (var prop in props)
        {
            resource.Properties[prop.Key.ToLowerInvariant()] = prop.Value;
        }
    }

    protected override string DefaultTitle
    {
        get { return DPAWebContent.BlockingImpactResource_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceBlockingImpact";
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/DPA/Controls/EditResourceControls/BlockingImpact.ascx";
        }
    }

    public bool IsInternal { get { return true; } }
}