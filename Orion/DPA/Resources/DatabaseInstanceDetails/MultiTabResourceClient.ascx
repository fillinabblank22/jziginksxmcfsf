﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiTabResourceClient.ascx.cs" Inherits="Orion_DPA_Controls_MultiTabResourceClient" %>
<orion:Include runat="server" Module="DPA" File="ViewsBundle.css" />
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="MultiTabControl" Src="~/Orion/DPA/Controls/MultiTabControl.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div runat="server" id="resourceContent">
            <dpa:MultiTabControl runat="server" ID="MultiTabControl" />
        </div>
    </Content>
</dpa:DpaResourceWrapper>
