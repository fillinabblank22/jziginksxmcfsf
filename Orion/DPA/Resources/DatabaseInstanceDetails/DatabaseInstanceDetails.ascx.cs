﻿using System;
using System.Data;
using System.Linq;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using ServiceLocator = SolarWinds.DPA.ServiceLocator.ServiceLocator;
using i18n = SolarWinds.DPA.Strings.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_DPA_Resources_DatabaseInstanceDetails_DatabaseInstanceDetails : DpaDatabaseInstanceBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_DatabaseInstanceDetails_Title; }
    }

    protected override bool HideOnNonLinkedView
    {
        get { return false; }
    }

    protected bool ShowEmptyResource { get; set; }

    protected string LearnMoreAboutOrionIntegrationHelpLink
    {
        get { return HelpHelper.GetHelpUrl("OrionDPALearnMoreAboutOrionIntegration"); }
    }

    protected string FormatSampleHowBullet1Text
    {
        get
        {
            var htmlEncodedText = Server.HtmlEncode(Resources.DPAWebContent.DatabaseInstanceDetails_Sample_HowBullet1Text);
            var htmlEncodedTextWithLink = htmlEncodedText
                .Replace("[START_LINK]", String.Format("<a href=\"#\" onclick=\"return SW.DPA.LinksToDpaSelector.show('/api/DpaLinksToDpaj/MainDashboard/')\" target=\"_blank\">"))
                .Replace("[END_LINK]", "</a>");

            return htmlEncodedTextWithLink;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceDatabaseInstanceDetails";
        }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected string DatabaseInstanceName { get; set; }
    protected int DatabaseInstanceStatusId { get; set; }
    protected int GlobalDatabaseInstanceId { get; set; }
    protected string DatabaseInstanceIcon { get; set; }
    protected StatusInfo DatabaseInstanceStatusInfo { get; set; }
    protected string DatabaseInstanceStatusDescription { get; set; }
    protected bool IsLicensed { get; set; }
    protected bool HasNode { get; set; }
    protected string NodeName { get; set; }
    protected int NodeId { get; set; }
    protected int NodeStatusId { get; set; }
    protected string NodeIcon { get; set; }
    protected string NodeLink { get; set; }
    protected string ViewNodeID { get; set; }

    protected string DpaServerName { get; set; }
    protected string DpaServerStatusIconUrl { get; set; }
    protected string DpaServerDetailUrl { get; set; }

    protected string PartiallyCompatibleMessage { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // set proper NPM node id from netobject
        var nodeProvider = this.GetInterfaceInstance<INodeProvider>();
        ViewNodeID = (nodeProvider != null) ? nodeProvider.Node.NodeID.ToString() : null;

		if (ViewHelper.Location == ResourceLocation.SAMClientView)
		{
			this.Visible = false;
		}
        else if (ViewHelper.Location == ResourceLocation.SAMView_NonLinked)
        {
			// show only sample content, when Database instance has no relationship with this application view
            sampleResourceContent.Visible = true;
        }
        else if (ViewHelper.GlobalDatabaseInstanceId != -1)
        {
            using (var swisSettingsContext = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>().CreateContext(PartialResultsMode.EnablePartialResults))
            {
                DpaWrapper.DpaServerId =
                    GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);
                var dpaServerDal = ServiceLocator.Current.GetInstance<IDpaServerDAL>();
                DataTable dataTable = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>()
                    .GetDatabaseInstanceDetails(ViewHelper.GlobalDatabaseInstanceId);

                if (dataTable.Rows.Count > 0)
                {
                    DataRow dataRow = dataTable.Rows[0];

                    DatabaseInstanceStatusId = dataRow.Field<int>("Status");
                    DatabaseInstanceName = dataRow.Field<string>("Name");
                    GlobalDatabaseInstanceId = dataRow.Field<int>("GlobalDatabaseInstanceID");
                    IsLicensed = dataRow.Field<bool>("IsLicensed");
                    DatabaseInstanceIcon = StatusIconsHelper.GetSmallStatusIcon(SwisEntities.DatabaseInstance,
                        DatabaseInstanceStatusId);

                    DatabaseInstanceStatusInfo = StatusInfo.GetStatus(DatabaseInstanceStatusId);

                    HasNode = dataRow["NodeID"] != DBNull.Value;
                    if (HasNode)
                    {
                        NodeName = dataRow.Field<string>("NodeCaption");
                        NodeStatusId = dataRow.Field<int>("NodeStatus");
                        NodeId = dataRow.Field<int>("NodeID");
                        NodeIcon = StatusIconsHelper.GetSmallStatusIcon(SwisEntities.Nodes, NodeStatusId, NodeId);
                        NodeLink = PageUrlHelper.NodeDetailPage(NodeId);
                    }
                    var dpaServerId = dataRow.Field<int>("DpaServerId");
                    DpaServerDetailUrl = PageUrlHelper.GetDpaServerDetailsPage(dpaServerId);
                    DpaServerName = dataRow.Field<string>("DpaServerName");
                    DpaServerStatusIconUrl =
                        StatusIconsHelper.GetDpaServerStatusIconFile(
                            (DpaServerStatus) dataRow.Field<int>("DpaServerStatus"));

                    var dpaServer = dpaServerDal.GetDpaServerById(dpaServerId);
                    if (dpaServer.IntegrationStatus == IntegrationStatus.PartiallyCompatible && dpaServer.FeaturesCatalogDiff != null)
                    {
                        if (dpaServer.FeaturesCatalogDiff.DpaVersionInstalled !=
                            dpaServer.FeaturesCatalogDiff.FullyCompatibleDpaVersion)
                        {
                            PartiallyCompatibleMessage = i18n.PartiallyCompatible_DpaObsolete_Message;
                        }
                        else
                        {
                            PartiallyCompatibleMessage = i18n.PartiallyCompatible_DpaimObsolete_Message;
                        }
                    }
                }

                resourceContent.Visible = true;

                var partialResultsErrors = swisSettingsContext.GetPartialErrors();

                if (partialResultsErrors.Any())
                {
                    DpaWrapper.ShowConnectivityIssueWarning(partialResultsErrors);
                    resourceContent.Visible = false;
                }
            }
        }
    }
}