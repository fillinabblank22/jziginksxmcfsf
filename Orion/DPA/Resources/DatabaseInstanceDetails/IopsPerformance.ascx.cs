﻿using System;
using System.Linq;
using Resources;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_IopsPerformance : SrmPerformanceDatabaseInstanceResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var globalDatabaseInstanceId = base.ViewHelper.GlobalDatabaseInstanceId;
        var databaseInstanceLunDal = ServiceLocator.Current.GetInstance<IDatabaseInstanceLunDAL>();
        if (!Settings.IsSrmInstalled || databaseInstanceLunDal.CountLunsByGlobalDatabaseInstanceIds(new int[] { globalDatabaseInstanceId }.ToList()) == 0)
        {
            Visible = false;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_IopsPerformancePerLun_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePoolIOPS";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    protected string Data
    {
        get
        {
            return 
                Serializer.Serialize(
                new
                {
                    ResourceId = Resource.ID,
                    NetObjectId = NetObjectHelper.GetNetObject(NetObjectPrefix.DatabaseInstance, ViewHelper.GlobalDatabaseInstanceId),
                    SampleSizeInMinutes = SampleSizeInMinutesProperty,
                    DaysOfDataToLoad = DaysOfDataToLoadProperty,
                    YAxisTitle = DPAWebContent.IopsPerformancePerLun_Chart_yAxis_Title,
                    ChartTitle = String.IsNullOrEmpty(this.ChartTitleProperty) ? SolarWinds.DPA.Strings.Resources.DPAResourceConfig_IopsPerformancePerLun_Title : this.ChartTitleProperty,
                    ChartSubTitle = this.ChartSubTitleProperty,
                    ChartInitialZoom = this.ChartInitialZoomProperty,
                    NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty,
                    DataUrl = "/Orion/DPA/Services/SrmCharts.asmx/GetIOPSPerformanceByField"
                });
        }
    }
}