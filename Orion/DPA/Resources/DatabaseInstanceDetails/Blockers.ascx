﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Blockers.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_Blockers" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div runat="server" ID="BlockersContent" automation="BlockersInTheLastHour">
	        <div class="DPA_Blockers_Legend">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <h4><%: Resources.DPAWebContent.ResourceBlockers_BlockingSessions %> <a href="#" class="DPA_InfoBox" automation="BlockingSessionsPopupIcon" data-title="<%: Resources.DPAWebContent.ResourceBlockers_BlockingSessions %>" 
                                    data-description="<%: Resources.DPAWebContent.ResourceBlockers_BlockingSessions_Description %>"><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" /></a></h4>
                                <div class="DPA_MainContent" automation="BlockingSessionsCount"><%: BlockingSessions %></div>
                                <div class="DPA_Link"><a href="<%: DpajLinks.CurrentBlockers(ViewHelper.GlobalDatabaseInstanceId) %>" automation="BlockingSessionsLink" target="_blank"><%: Resources.DPAWebContent.ResourceBlockers_Link_ShowBlockingSessions_Label %> &gt;</a></div>
                            </td>
                            <td>
                                <h4><%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocking %> <a href="#" class="DPA_InfoBox" automation="BlockingTimePopupIcon" data-title="<%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocking %>" 
                                    data-description="<%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocking_Description %>"><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" /></a></h4>
                                <div class="DPA_MainContent" automation="BlockingTime"><%: TotalTimeBlocking %></div>
                                <div class="DPA_Link"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h4><%: Resources.DPAWebContent.ResourceBlockers_BlockedSessions %> <a href="#" class="DPA_InfoBox" automation="BlockedSessionsPopupIcon" data-title="<%: Resources.DPAWebContent.ResourceBlockers_BlockedSessions %>" 
                                    data-description="<%: Resources.DPAWebContent.ResourceBlockers_BlockedSessions_Description %>"><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" /></a></h4>
                                <div class="DPA_MainContent" automation="BlockedSessionsCount"><%: BlockedSessions %></div>
                                <div class="DPA_Link"><a href="<%: DpajLinks.CurrentBlockers(ViewHelper.GlobalDatabaseInstanceId) %>" automation="BlockedSessionsLink" target="_blank"><%: Resources.DPAWebContent.ResourceBlockers_Link_ShowBlockedSessions_Label %> &gt;</a></div>
                            </td>
                            <td>
                                <h4><%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocked %> <a href="#" class="DPA_InfoBox" automation="BlockedTimePopupIcon" data-title="<%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocked %>" 
                                    data-description="<%: Resources.DPAWebContent.ResourceBlockers_TotalTimeBlocked_Description %>"><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" /></a></h4>
                                <div class="DPA_MainContent" automation="BlockedTime"><%: TotalTimeBlocked %></div>
                                <div class="DPA_Link"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
	        </div>
            <div class="DPA_Blockers_Chart">
                <h4><%: Resources.DPAWebContent.ResourceBlockers_ChartTitle %></h4>
                <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
            </div>
	    </div>
        <script>
            $().ready(function() {
                $('#<%=BlockersContent.ClientID %> .DPA_InfoBox').off('click.dpa').on('click.dpa', function () {
                    return DPA.UI.InfoModal.show($(this).data('title'), $(this).data('description'));
                });
            });
        </script>
    </Content>
</dpa:DpaResourceWrapper>