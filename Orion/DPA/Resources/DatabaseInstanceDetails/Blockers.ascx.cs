﻿using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.UI;
using System;
using System.Linq;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.ServiceLocator;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_Blockers : DpaDatabaseInstanceStandardChartResource
{
    private BlockingOverview blockingOverview;

    protected string TotalTimeBlocking
    {
        get { return blockingOverview != null ? FormatHelper.FormatSecondsToTwoOrderUnitFormat(blockingOverview.TotalTimeBlocking) : ""; }
    }

    protected string TotalTimeBlocked
    {
        get { return blockingOverview != null ? FormatHelper.FormatSecondsToTwoOrderUnitFormat(blockingOverview.TotalTimeBlocked) : ""; }
    }

    protected double BlockingSessions
    {
        get { return blockingOverview != null ? blockingOverview.BlockingSessions : 0; }
    }

    protected double BlockedSessions
    {
        get { return blockingOverview != null ? blockingOverview.BlockedSessions : 0; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var end = DateTimeHelper.ToMinutes(DateTime.UtcNow);
        var start = DateTimeHelper.ToMinutes(end.AddHours(-1));
        var blockingOverviewDal = ServiceLocator.Current.GetInstance<IBlockingOverviewDAL>();
        SetChartName(ChartNames.Blockers);
        HandleInit(WrapperContents);

        using (var swisSettingsContext = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>().CreateContext(PartialResultsMode.EnablePartialResults))
        {
            var databaseInstance = ViewHelper.DatabaseInstance;
            if (databaseInstance != null && databaseInstance.Type == DatabaseInstanceType.DB2)
            {
                // hide resource
                base.Visible = false;
                return;
            }
            blockingOverview = blockingOverviewDal.GetAllByGlobalDatabaseInstanceId(ViewHelper.GlobalDatabaseInstanceId, start, end, TimesliceUnit.Hour).FirstOrDefault();
            DpaWrapper.DpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(ViewHelper.GlobalDatabaseInstanceId);

            var partialResultsErrors = swisSettingsContext.GetPartialErrors();

            if (partialResultsErrors.Any())
            {
                DpaWrapper.ShowConnectivityIssueWarning(swisSettingsContext.GetPartialErrors());
                BlockersContent.Visible = false;
            }
        }
    }

    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_Blockers_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceBlockers";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public override string EditURL
    {
        get
        {
            return ExtendCustomEditUrl("/Orion/NetPerfMon/Resources/EditResource.aspx");
        }
    }

    protected override void SetChartInitializerScript()
    {
        SetChartInitializerScript(DpaWrapper.ConnectivityIssueWarningClientId);
    }
}