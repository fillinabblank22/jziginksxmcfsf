﻿using System;
using System.Globalization;
using System.Text;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Data.Helpers;
using SolarWinds.DPA.ServiceLocator;

public partial class Orion_DPA_Resources_DatabaseInstanceDetails_StorageObjectsByPerformanceRisk : DpaDatabaseInstanceBaseResource
{
    private string query;

    protected void Page_Load(object sender, EventArgs e)
    {
        var databaseInstanceLunDal = ServiceLocator.Current.GetInstance<IDatabaseInstanceLunDAL>();
        var globalDatabaseInstanceId = ViewHelper.GlobalDatabaseInstanceId;

        if (!Settings.IsSrmInstalled || databaseInstanceLunDal.CountLunsByGlobalDatabaseInstanceIds(new [] { globalDatabaseInstanceId }) == 0)
        {
            Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        DpaWrapper.HeaderButtons.Controls.Add(SearchControl);
        SearchControl = SearchControl;

        var sb = new StringBuilder();

        sb.AppendInvariantFormattedLine(@"SELECT [_NetObjectPrefix], [_ObjectId], [Resource], [_IconFor_Resource], 
	                                    [Type], [_LinkFor_Resource], [Array], [_LinkFor_Array], [_IconFor_Array], [Latency], [IOPS], [Throughput], 
		                                [_IOPSError], [_LatencyError], [_ThroughputError], [_TotalErrors]
                                        FROM (");
        sb.AppendFormat(CultureInfo.InvariantCulture,
        @"SELECT '{1}' AS [_NetObjectPrefix], 
        L.LUNID AS [_ObjectId],
        L.DisplayName as [Resource],
        concat('/Orion/StatusIcon.ashx?entity=Orion.SRM.LUNs&status=', ISNULL(L.Status, 0)) AS [_IconFor_Resource],
        '{2}' AS [Type], 
        L.DetailsUrl AS [_LinkFor_Resource],
	    [Array], [_LinkFor_Array], [_IconFor_Array], 
        L.IOLatencyTotal AS [Latency],
        L.IOPSTotal AS [IOPS],
        L.BytesPSTotal AS [Throughput],	                    
        [_IOPSError], [_LatencyError], [_ThroughputError],  
        [_IOPSError] + [_LatencyError] + [_ThroughputError] AS _TotalErrors
FROM Orion.SRM.LUNs L
    LEFT JOIN (
        SELECT LUNID as [_ObjectId], SUM([_IOPSError]) AS [_IOPSError], SUM([_LatencyError]) AS [_LatencyError], SUM([_ThroughputError]) AS [_ThroughputError]
        FROM (
            SELECT LUNID,
			     {6} AS [_IOPSError],
			     0 AS [_LatencyError],
			     0 AS [_ThroughputError]
            FROM Orion.SRM.LUNs (nolock=true)
            WHERE {9}
            UNION ALL (
                SELECT LUNID,
				    0 AS [_IOPSError],
				    {7} AS [_LatencyError],
				    0 AS [_ThroughputError]
				FROM Orion.SRM.LUNs (nolock=true)
	            WHERE {10}
            ) UNION ALL (
                SELECT LUNID,
					0 AS [_IOPSError],
					0 AS [_LatencyError],
					{8} AS [_ThroughputError]
				FROM Orion.SRM.LUNs (nolock=true)
	            WHERE {11}
			)
        ) GROUP BY [LUNID]
    ) LUNsThresholds ON L.LUNID = LUNsThresholds._ObjectId
LEFT JOIN (
SELECT StorageArrayID, 
       DisplayName as [Array],  
       concat('/Orion/View.aspx?NetObject={3}:', StorageArrayID) AS [_LinkFor_Array],
       concat('/Orion/StatusIcon.ashx?entity=',
                CASE WHEN IsCluster = true THEN '{4}' ELSE '{5}' END
                ,'&status=', ISNULL(Status, 0)) AS [_IconFor_Array]
FROM Orion.SRM.StorageArrays
WHERE Unmanaged = FALSE
) SA ON SA.StorageArrayID = L.StorageArrayId WHERE L.DatabaseInstanceReference.GlobalDatabaseInstanceId = {12})",
                    /*0*/SwisEntities.Lun,
                    /*1*/NetObjectPrefix.Lun,
                    /*2*/SolarWinds.DPA.Strings.Resources.StorageType_Lun,
                    /*3*/NetObjectPrefix.StorageArray,
                    /*4*/SwisEntities.Clusters,
                    /*5*/SwisEntities.StorageArrays,
                    /*6*/FormatThreshold("LUNs.IOPSTotalThreshold"),
                    /*7*/FormatThreshold("LUNs.IOLatencyTotalThreshold"),
                    /*8*/FormatThreshold("LUNs.BytesPSTotalThreshold"),
                    /*9*/FormatIfThresholdStateEnabled("LUNs.IOPSTotalThreshold"),
                    /*10*/FormatIfThresholdStateEnabled("LUNs.IOLatencyTotalThreshold"),
                    /*11*/FormatIfThresholdStateEnabled("LUNs.BytesPSTotalThreshold"),
                    /*12*/globalDatabaseInstanceId
                );

        query = sb.ToString();
        CustomTable.UniqueClientID = Math.Abs(Resource.ID);

        CustomTable.SWQL = new ExpressionBuilder(query).ToString();
        CustomTable.SearchSWQL = new ExpressionBuilder(query).Where(SearchSWQL).ToString();
        
        // for opening SRM resource by clicking on cells in table
        DpaWrapper.Controls.Add(LoadControl("~/Orion/SRM/Controls/Redirector.ascx"));
    }

    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.StorageObjectsByPerformanceRisk_DatabaseInstanceDetailsResource_Title; }
    }
    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourceStorageObjectsPerformanceRisks";
        }
    }

    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }

    protected int PageSize
    {
        get
        {
            var defaultPageSize = 5;
            return defaultPageSize;
        }
    }

    private string FormatThreshold(string thresholdEntity)
    {
        return String.Format(CultureInfo.InvariantCulture,
@"CASE WHEN {0}.IsLevel2State = true THEN 10 ELSE 0 END + CASE WHEN {0}.IsLevel1State = true THEN 1  ELSE 0 END",
                             thresholdEntity);
    }

    private string FormatIfThresholdStateEnabled(string thresholdEntity)
    {
        return String.Format(CultureInfo.InvariantCulture, @"{0}.IsLevel2State = true OR {0}.IsLevel1State = true", thresholdEntity);
    }
    
    /// <summary>
    /// Additional search query for search control.
    /// </summary>
    public string SearchSWQL
    {
        get { return "([Resource] LIKE '%${SEARCH_STRING}%' OR [Array] LIKE '%${SEARCH_STRING}%')"; }
    }
}