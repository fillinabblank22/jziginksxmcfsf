﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageObjectsByCapacityRisk.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstanceDetails_StorageObjectsByCapacityRisk" %>

<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<orion:Include runat="server" Module="DPA" File="DPA.Formatters.js" />

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="Content" runat="server" class="DPA_capacityrisk">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>
        <script type="text/javascript">
            $(function () {
                new SW.DPA.UI.StorageObjectsByCapacityRisk().initialize('<%= CustomTable.UniqueClientID %>', 
                    '<%= SearchControl.SearchBoxClientID %>', 
                    '<%= SearchControl.SearchButtonClientID %>',
                    '<%= Content.ClientID %>');
            });
        </script>
    </Content>
</orion:ResourceWrapper>
