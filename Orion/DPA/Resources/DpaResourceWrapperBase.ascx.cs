﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common.Models;

public partial class Orion_DPA_Resources_DpaResourceWrapperBase : UserControl
{
    public int? DpaServerId { get; set; }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return DpaResourceWrapperPlaceHolder; }
    }

    public string LoadingIconClientId
    {
        get { return LoadingIcon.ClientID; }
    }

    public string ConnectivityIssueWarningClientId
    {
        get { return ConnectivityIssueWarning.ClientID; }
    }

    public PlaceHolder HeaderButtons
    {
        get { return Wrapper.HeaderButtons; }
    }

    public void ShowConnectivityIssueWarning(IEnumerable<PartialResultsError> partialResultsErrors = null)
    {
        ConnectivityIssueWarning.Show(partialResultsErrors);
    }
    public void HideConnectivityIssueWarning()
    {
        ConnectivityIssueWarning.Hide();
    }

    public string ErrorMessageClientId
    {
        get { return ErrorMessage.ClientID; }
    }

    public void ShowErrorMessage()
    {
        ErrorMessage.Show();
    }

    public void HideErrorMessage()
    {
        ErrorMessage.Hide();
    }

    public void HideEditButton()
    {
        Wrapper.ShowEditButton = false;
    }
    
    public void ShowManageButton(string text, string target)
    {
        Wrapper.ShowManageButton = true;
        Wrapper.ManageButtonText = text;
        Wrapper.ManageButtonTarget = target;
    }

    protected override void OnInit(EventArgs e)
    {
        LoadingIcon.UniqueClientID = Wrapper.ClientID;
        ConnectivityIssueWarning.UniqueClientID = Wrapper.ClientID;
        ErrorMessage.UniqueClientID = Wrapper.ClientID;
    }
}