﻿using System;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web;
using ServiceLocator = SolarWinds.DPA.ServiceLocator.ServiceLocator;
using SolarWinds.DPA.Web.Providers.WaitTime;
using SolarWinds.DPA.Common.Models;

public partial class Orion_DPA_Resources_DpaServerDetails_WaitTimeChart : DpaServerStandardChartResource, IResourceIsInternal
{
    private string _automationAttribute;
    protected string AutomationAttribute
    {
        get { return _automationAttribute ?? (_automationAttribute = GetAutomationAttribute()); }
    }

    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    private string _helpLinkFragment;
    public override string HelpLinkFragment
    {
        get { return _helpLinkFragment; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetChartName(DpaWaitTimeChart.ChartName);
        HandleInit(WrapperContents);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        DpaWaitTimeChart.Initialize(Resource.Properties["WaitTimeCategory"], Settings.IsIntegrationEstablished);
        _helpLinkFragment = DpaWaitTimeChart.HelpLinkFragment;
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override void SetChartInitializerScript()
    {
        SetChartInitializerScript(DpaWrapper.ConnectivityIssueWarningClientId);
    }

    protected string GetAutomationAttribute()
    {
        int resourceCategoryTypeId;

        if (!int.TryParse(Resource.Properties["WaitTimeCategory"], out resourceCategoryTypeId))
        {
            throw new FormatException("Value of WaitTimeCategory resource property 'ResourceDataCategory' has invalid format, expecting integer");
        }

        WaitTimeCategory _waitTimeCategory = _waitTimeCategory = (WaitTimeCategory)resourceCategoryTypeId;
        IWaitTimeProvider _waitTimeProvider = ServiceLocator.Current.GetInstance<IWaitTimeProvider>(_waitTimeCategory.ToString());
        if (_waitTimeProvider == null) throw new NullReferenceException("_waitTimeProvider");

        return _waitTimeProvider.AutomationAttribute;
    }
}