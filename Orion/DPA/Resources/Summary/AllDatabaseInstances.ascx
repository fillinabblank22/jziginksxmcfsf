﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllDatabaseInstances.ascx.cs" Inherits="Orion_DPA_Resources_Summary_AllDatabaseInstances" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<%@ Register TagPrefix="dpa" TagName="DpaSummaryResourceWrapper" Src="~/Orion/DPA/Resources/DpaSummaryResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="DatabaseInstances" Src="~/Orion/DPA/Controls/DatabaseInstancesControl.ascx" %>

<dpa:dpasummaryresourcewrapper runat="server" id="DpaWrapper">
    <Content>
        <dpa:DatabaseInstances ID="DpaDatabaseInstances" runat="server"/>
        <!-- Following is shown when integration is not established -->
        <div id="DPA_DatabaseInstances-<%= Resource.ID %>-NoDataContent" style="display:none">
           <img src="/Orion/DPA/images/Samples/AllDatabaseInstancesEmpty.png"/>
           <div class="DPA_GettingStartedBox">
              <div class="DPA_First">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%: Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationTitle %></b></h3>
              </div>

              <div><%= IntegrationNotEstablishedText1HtmlEncoded %></div>
               
               <% if (Profile.AllowAdmin)
                  { %>
              <div class="DPA_Buttons">
                  <orion:LocalizableButtonLink runat="server" ID="discoverNetworkButton" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, DatabaseInstancesResource_GettingStartedButtonLabel %>" />
              </div>
               
               <% if (IsSummaryView)
                  { %>
               <div>
                   <h3><b><%: Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText2a %></b></h3>
               </div>
               <div>
                   <%= IntegrationNotEstablishedText2HtmlEncoded %>
               </div>
               <% }
                  } %>
           </div>
        </div>
    </Content>
</dpa:dpasummaryresourcewrapper>
