﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllDpaServers.ascx.cs" Inherits="Orion_DPA_Resources_Summary_AllDpaServers" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
        <div runat="server" ID="mainContent" class="DPA_AllDpaServersResource">
            <script>
                $(document).ready(function() {
                    var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>');
                    var errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');

                    var allDpaServers = new SW.DPA.UI.AllDpaServers({
                        wrapperElement: $('#<%= mainContent.ClientID %>'),
                        loadingIcon: loadingIcon,
                        errorMessage: errorMessage
                    });

                    var refresh = function () { allDpaServers.load(); };
                    SW.Core.View.AddOnRefresh(refresh, <%= Resource.ID %>);
                    refresh();
                });
            </script>
        </div>
        <div ID="integrationNotEstablishedContent" runat="server">
           <img src="/Orion/DPA/images/Samples/AllDPAServersEmpty.png"/>
           <div class="DPA_GettingStartedBox">
              <div class="DPA_First">
                 <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
                 <h3><b><%: Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationTitle %></b></h3>
              </div>

              <div><%= IntegrationNotEstablishedText1HtmlEncoded %></div>
               
              <% if (Profile.AllowAdmin)
                 { %>
              <div class="DPA_Buttons">
                  <orion:LocalizableButtonLink runat="server" ID="discoverNetworkButton" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, DatabaseInstancesResource_GettingStartedButtonLabel %>" />
              </div>

                <% if (IsSummaryView)
                    { %>
               <div>
                   <h3><b><%: Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText2a %></b></h3>
               </div>
               <div>
                   <%= IntegrationNotEstablishedText2HtmlEncoded %>
               </div>
               <% }
                 } %>
           </div>            
        </div>
    </Content>
</dpa:DpaResourceWrapper>