﻿using System;
using System.Collections.Generic;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Providers.WaitTime;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web;

public partial class Orion_DPA_Resources_Summary_WaitTimeChart : DpaStandardChartResource, IResourceIsInternal
{
    protected string AutomationAtribute
    {
        get { return GetAutomationAttribute(); }
    }
    protected override string DefaultTitle
    {
        get { return string.Empty; }
    }

    private string _helpLinkFragment;
    public override string HelpLinkFragment
    {
        get { return _helpLinkFragment; }
    }

    public bool IsInternal
    {
        get { return true; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        return new string[1] { null };
    }
    
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return SolarWinds.DPA.Common.NetObjectPrefix.DpaServer; }
    }

    protected override void SetChartInitializerScript()
    {
        SetChartInitializerScript(DpaWrapper.ConnectivityIssueWarningClientId);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        DpaWaitTimeChart.Initialize(Resource.Properties["WaitTimeCategory"], Settings.IsIntegrationEstablished);
        _helpLinkFragment = DpaWaitTimeChart.HelpLinkFragment;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetChartName(DpaWaitTimeChart.ChartName);
        HandleInit(WrapperContents);

        if (!Settings.IsIntegrationEstablished)
        {
            WrapperContents.Visible = false;
        }
    }
    protected string GetAutomationAttribute()
    {
        int resourceCategoryTypeId;

        if (!int.TryParse(Resource.Properties["WaitTimeCategory"], out resourceCategoryTypeId))
        {
            throw new FormatException("Value of WaitTimeCategory resource property 'ResourceDataCategory' has invalid format, expecting integer");
        }

        WaitTimeCategory waitTimeCategory = waitTimeCategory = (WaitTimeCategory)resourceCategoryTypeId;
        IWaitTimeProvider waitTimeProvider = waitTimeProvider = ServiceLocator.Current.GetInstance<IWaitTimeProvider>(waitTimeCategory.ToString());
        if (waitTimeProvider == null) throw new NullReferenceException("_waitTimeProvider");

        return waitTimeProvider.AutomationAttribute;
    }
}