﻿using System;
using Castle.Core.Internal;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web;

public partial class Orion_DPA_Resources_Summary_WhatsNew : DpaBaseResource
{
    protected string PerfstackLink => $"<a href=\"{PageUrlHelper.Perfstack}\"> Try Now </a>";

    protected void btnRemoveMe_OnClick(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);

        // delete the resource also from other views
        var resourcesDal = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IResourcesDal>();
        var resourceIds = resourcesDal.GetResourceIdsByName("What is New in DPA");
        resourceIds.ForEach(ResourceManager.DeleteById);

        var redirectUrl = Request.Url.ToString();
        // Don't redirect to detach resource page, when the resource was removed
        if (redirectUrl.Contains("DetachResource.aspx"))
        {
            redirectUrl = PageUrlHelper.DpaSummaryPage;
        }

        Page.Response.Redirect(redirectUrl);
    }

    public override sealed string DisplayTitle
    {
        get
        {
            return String.Format(SolarWinds.DPA.Strings.Resources.Resource_WhatsNew_TitleFormat, SolarWinds.DPA.Common.CommonConstants.DpaVersion);
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_WhatsNew_Title;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        imgNewFeature.Text = String.Format(Resources.DPAWebContent.WhatsNewResource_ButtonLabelFormat,
                SolarWinds.DPA.Common.CommonConstants.DpaVersion);


        if (!Settings.IsIntegrationEstablished)
        {
            Visible = false;
        }
    }
}