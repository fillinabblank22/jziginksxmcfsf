﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_AllDpaServers : DpaBaseResource
{
    protected string IntegrationNotEstablishedText1HtmlEncoded
    {
        get
        {
            var htmlEncodedText = HttpUtility.HtmlEncode(Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText1);
            return htmlEncodedText.Replace("[LINK]", string.Format("<a href=\"{0}\" target=\"_blank\">", SolarWinds.DPA.Common.CommonConstants.LinkQuickStartGuide)).Replace("[/LINK]", "</a>");
        }
    }

    protected string IntegrationNotEstablishedText2HtmlEncoded
    {
        get
        {
            var htmlEncodedText = HttpUtility.HtmlEncode(Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText2b);
            return htmlEncodedText.Replace("[LINK]", string.Format("<a href=\"{0}\">", PageUrlHelper.EditHomeTabPage)).Replace("[/LINK]", "</a>");
        }
    }

    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_AllDpaServers_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceAllDPAServers";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsSummaryView => Resource.View != null && Resource.View.ViewType == SolarWinds.DPA.Common.CommonConstants.DpaSummaryViewType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.IsIntegrationEstablished)
        {
            integrationNotEstablishedContent.Visible = false;
        }
        else
        {
            mainContent.Visible = false;
        }

        if (Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.DpaServerManagementButtonLabel,
                PageUrlHelper.ServerManagement);
        }

        discoverNetworkButton.NavigateUrl = PageUrlHelper.ServerManagement;
    }
}