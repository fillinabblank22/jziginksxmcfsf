﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_DPA_Resources_Summary_GettingStarted" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<orion:resourcewrapper runat="server" id="Wrapper" showeditbutton="False">
    <Content>
        <div class="DPA_GettingStartedBorder">
        <table class="DPA-gettingstarted-innertable" cellpadding="0" cellspacing="0">
             <tr>
                <td class="icon">
                    <img src="/Orion/DPA/images/GettingStarted/video.png" alt="icon" />
                </td>
                <td>
                    <div class="title">
                        <a href="<%: PageUrlHelper.LinkResponseTimeAnalysisOverview %>" target="_blank"><%: Resources.DPAWebContent.GettingStartedResource_WhyIntegrateTitle %></a></div>
                    <div class="DPA-text-helpful">
                        <%: Resources.DPAWebContent.GettingStartedResource_WhyIntegrateDescription %>
                    </div>
                </td>
                <td class="centered">
                    <a href="<%: PageUrlHelper.LinkResponseTimeAnalysisOverview %>" target="_blank"><img src="/Orion/DPA/images/video.png" alt="" /></a>
                </td>
            </tr>   
            <tr>
                <td class="icon">
                    <img src="/Orion/DPA/images/GettingStarted/lightBulb.png" alt="icon" />
                </td>
                <td>
                    <div class="title">
                        <a href="<%= this.LearnBasicsUrl %>" target="_blank"><%: Resources.DPAWebContent.GettingStartedResource_LearnBasicsTitle %></a></div>
                    <div class="DPA-text-helpful">
                        <%: Resources.DPAWebContent.GettingStartedResource_LearnBasicsDescription %>
                    </div>
                </td>
                <td>
                    <a class="sw-btn sw-btn-primary" href="<%= this.LearnBasicsUrl %>" target="_blank"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.GettingStartedResource_LearnBasicsButtonTitle %></span></span> </a>
                </td>
            </tr>
            
            <%if (Profile.AllowAdmin) { %>
            <tr>
                <td class="icon">
                    <img src="/Orion/DPA/images/GettingStarted/integrate_32x32.png" alt="icon" />
                </td>
                <td>
                    <div class="title">
                        <a href="<%= this.ServerManagementUrl %>"><%: Resources.DPAWebContent.GettingStartedResource_IntegrateTitle %></a></div>
                    <div class="DPA-text-helpful">
                        <%: Resources.DPAWebContent.GettingStartedResource_IntegrateDescription %>
                    </div>
                </td>
                <td>
                    <a class="sw-btn sw-btn-primary" href="<%= this.ServerManagementUrl %>"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.GettingStartedResource_IntegrateButtonTitle %></span></span></a>
                </td>
            </tr>
            <%}%>
        </table>
        </div>
        <%if (Profile.AllowAdmin) { %>
        <orion:LocalizableButton ID="btnRemoveMe" 
            CssClass="DPA_GettingStartedRemoveBtn" 
            runat="server"
            OnClick="btnRemoveMe_OnClick"
            DisplayType="Secondary"
            Text="<%$ Resources: DPAWebContent, GettingStartedWithDatabases_Button_RemoveResource %>" />
        <%}%>
    </Content>
</orion:resourcewrapper>