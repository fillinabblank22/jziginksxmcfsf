﻿using System;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_DPA_Resources_Summary_GettingStarted : DpaBaseResource
{
    protected string ServerManagementUrl
    {
        get { return PageUrlHelper.ServerManagement; }
    }

    protected string LearnBasicsUrl
    {
        get { return SolarWinds.DPA.Common.CommonConstants.LinkQuickStartGuide; }
    }

    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_GettingStarted_Title; }
    }

    protected void btnRemoveMe_OnClick(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Settings.IsIntegrationEstablished)
        {
            Visible = false;
        }
    }
}