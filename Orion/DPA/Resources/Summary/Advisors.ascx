﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Advisors.ascx.cs" Inherits="Orion_DPA_Resources_Summary_Advisors" %>

<%@ Register TagPrefix="dpa" TagName="DpaSummaryResourceWrapper" Src="~/Orion/DPA/Resources/DpaSummaryResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="AdvisorsControl" Src="~/Orion/DPA/Controls/AdvisorsControl.ascx" %>

<dpa:DpaSummaryResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <dpa:AdvisorsControl ID="DpaAdvisors" runat="server"/>
	</Content>
</dpa:DpaSummaryResourceWrapper>