﻿using System;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_ApplicationsUsingAllMyDatabases : DpaBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_ApplicationsUsingAllMyDatabases_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceApplicationsUsingAllMyDatabases";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Settings.IsApmInstalled)
        {
            // hide resource, when SAM is not installed
            Visible = false;
        }
        else if (Settings.IsIntegrationEstablished)
        {
            mainContent.Visible = true;
            integrationNotEstablishedContent.Visible = false;
            DpaApplicationsUsingMyDatabases.Initialize(Resource.ID, DpaWrapper.ErrorMessageClientId, DpaWrapper.LoadingIconClientId, null);
        }
        else
        {
            mainContent.Visible = false;
            integrationNotEstablishedContent.Visible = true;
        }
        
        // show CONFIGURE CLIENT APPLICATIONS button
        if (Profile.AllowAdmin && Settings.IsIntegrationEstablished)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel,
                PageUrlHelper.RelationshipManagementClientApplications);
        }
    }
}