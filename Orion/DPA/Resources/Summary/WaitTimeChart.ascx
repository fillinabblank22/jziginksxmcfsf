﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WaitTimeChart.ascx.cs" Inherits="Orion_DPA_Resources_Summary_WaitTimeChart" %>
<%@ Register TagPrefix="dpa" TagName="DpaSummaryResourceWrapper" Src="~/Orion/DPA/Resources/DpaSummaryResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="WaitTimeChart" Src="~/Orion/DPA/Controls/WaitTimeChartControl.ascx" %>

<dpa:DpaSummaryResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div class="DPA_WaitTimeResource" automation="<%=this.AutomationAtribute %>">
            <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
        </div>
        <dpa:WaitTimeChart ID="DpaWaitTimeChart" runat="server" />
    </Content>
</dpa:DpaSummaryResourceWrapper>
