﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_DPA_Resources_Summary_WhatsNew" %>

<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<orion:resourcewrapper runat="server" id="Wrapper" showeditbutton="False">
    <Content>
        <table class="DPA_WhatsNew">
            <tr>
                <td class="DPA_WhatsNewIcon">
                    <span></span>
                </td>
                <td class="DPA_WhatsNewContent">
                    <p>
                        <b><%: Resources.DPAWebContent.WhatsNewResource_Perfstack_Title %></b> -
                        <%: Resources.DPAWebContent.WhatsNewResource_Perfstack_Description %>
                        <%= PerfstackLink %>
                    </p> 
                    <ul>
                        <li><%: Resources.DPAWebContent.WhatsNewResource_Perfstack_Description_1 %></li>
                        <li><%: Resources.DPAWebContent.WhatsNewResource_Perfstack_Description_2 %></li>
                        <li><%: Resources.DPAWebContent.WhatsNewResource_Perfstack_Description_3 %></li>
                    </ul>

                    <p>
                        <b><%: Resources.DPAWebContent.WhatsNewResource_BlockingAndDeadlock_Title %></b> -
                        <%: Resources.DPAWebContent.WhatsNewResource_BlockingAndDeadlock_Description %>
                    </p>

                    <p>
                        <b><%: Resources.DPAWebContent.WhatsNewResource_ImprovedDatabaseStatus_Title %></b> -
                        <%: Resources.DPAWebContent.WhatsNewResource_ImprovedDatabaseStatus_Description %>
                    </p>

                    <p>
                        <b><%: Resources.DPAWebContent.WhatsNewResource_StandaloneInstaller_Title %></b> -
                        <%: Resources.DPAWebContent.WhatsNewResource_StandaloneInstaller_Description %>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-right: 0; padding-top: 10px; text-align: right; padding-bottom: 0;">
                    <orion:LocalizableButtonLink NavigateUrl="<%# PageUrlHelper.DpaoReleaseNotesLink %>" runat="server" ID="imgNewFeature" Target="_blank" 
                        DisplayType="Primary">
                    </orion:LocalizableButtonLink>&nbsp;<orion:LocalizableButton runat="server" ID="btnRemoveResource" 
                        Text="<%$ Resources: DPAWebContent, GettingStartedWithDatabases_Button_RemoveResource %>" 
                        DisplayType="Secondary" 
                        OnClick="btnRemoveMe_OnClick"/>
                </td>
            </tr>
        </table>
    </Content>
</orion:resourcewrapper>