﻿using System;
using System.Web;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_AllDatabaseInstances : DpaBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_AllDatabaseInstances_Title; }
    }

    protected string IntegrationNotEstablishedText1HtmlEncoded
    {
        get
        {
            var htmlEncodedText = HttpUtility.HtmlEncode(Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText1);
            return htmlEncodedText.Replace("[LINK]", string.Format("<a href=\"{0}\" target=\"_blank\">", SolarWinds.DPA.Common.CommonConstants.LinkQuickStartGuide)).Replace("[/LINK]", "</a>");
        }
    }

    protected string IntegrationNotEstablishedText2HtmlEncoded
    {
        get
        {
            var htmlEncodedText = HttpUtility.HtmlEncode(Resources.DPAWebContent.DatabaseInstancesResource_NoIntegrationText2b);
            return htmlEncodedText.Replace("[LINK]", string.Format("<a href=\"{0}\">", PageUrlHelper.EditHomeTabPage)).Replace("[/LINK]", "</a>");
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceAllDatabaseInstances";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool IsSummaryView => Resource.View != null && Resource.View.ViewType == SolarWinds.DPA.Common.CommonConstants.DpaSummaryViewType;

    protected void Page_Load(object sender, EventArgs e)
    {
        DpaDatabaseInstances.Initialize(Resource.ID, DpaWrapper.ConnectivityIssueWarningClientId,
            DpaWrapper.ErrorMessageClientId, DpaWrapper.LoadingIconClientId, null, Resource.Title);

        if (Profile.AllowAdmin && Settings.IsIntegrationEstablished)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.DatabaseInstancesResource_ConfigureDbInstancesButtonLabel,
                PageUrlHelper.RelationshipManagementDatabaseInstances);
        }

        discoverNetworkButton.NavigateUrl = PageUrlHelper.ServerManagement;
    }
}