﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsUsingAllMyDatabases.ascx.cs" Inherits="Orion_DPA_Resources_Summary_ApplicationsUsingAllMyDatabases" %>

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="DpaApplicationsUsingMyDatabases" Src="~/Orion/DPA/Controls/ApplicationsUsingMyDatabasesControl.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
        <div runat="server" ID="mainContent">
            <dpa:DpaApplicationsUsingMyDatabases ID="DpaApplicationsUsingMyDatabases" runat="server"/>
        </div>
        <div ID="integrationNotEstablishedContent" runat="server">
            <img src="/Orion/DPA/images/Samples/applicationsUsingMyDatabases-sample.png" alt="sample" />
            <div class="DPA_AdvisorSample">
                <ul>
                    <li><%: Resources.DPAWebContent.ApplicationsUsingMyDatabasesResource_SampleText %></li>
                    <li><%: Resources.DPAWebContent.ApplicationsUsingMyDatabasesResource_SampleText2 %></li>
                </ul>
                
                <% if (Profile.AllowAdmin)
                   { %>
                <p><a href="<%: SolarWinds.DPA.Web.Helpers.PageUrlHelper.ServerManagement %>">&#187; <%: Resources.DPAWebContent.AdvisorsResource_StartDPAIntegrationLink_Label %></a></p>
                <% } %>
            </div>
        </div>
    </Content>
</dpa:DpaResourceWrapper>