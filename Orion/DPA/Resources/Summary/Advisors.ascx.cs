﻿using System;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_Advisors : DpaBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_AdvisorsFromAllDatabasesWithHighestWaitTime_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceAdvisorsFromAllDatabasesWithHighestWaitTime";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DpaAdvisors.Initialize(Resource.ID, DpaWrapper.ConnectivityIssueWarningClientId, DpaWrapper.ErrorMessageClientId,
            DpaWrapper.LoadingIconClientId, null);
    }
}