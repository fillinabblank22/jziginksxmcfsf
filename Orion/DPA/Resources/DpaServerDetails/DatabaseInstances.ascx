﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseInstances.ascx.cs" Inherits="Orion_DPA_Resources_Summary_DatabaseInstances" %>
<%@ Register TagPrefix="dpa" TagName="DpaSummaryResourceWrapper" Src="~/Orion/DPA/Resources/DpaSummaryResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="DatabaseInstances" Src="~/Orion/DPA/Controls/DatabaseInstancesControl.ascx" %>

<orion:Include runat="server" Module="DPA" File="Resources.js" />

<dpa:DpaSummaryResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
        <dpa:DatabaseInstances ID="DpaDatabaseInstances" runat="server"/>
	</Content>
</dpa:DpaSummaryResourceWrapper>