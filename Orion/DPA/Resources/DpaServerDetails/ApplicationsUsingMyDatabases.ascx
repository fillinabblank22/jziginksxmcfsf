﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsUsingMyDatabases.ascx.cs" Inherits="Orion_DPA_Resources_Summary_ApplicationsUsingMyDatabases" %>

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>
<%@ Register TagPrefix="dpa" TagName="DpaApplicationsUsingMyDatabases" Src="~/Orion/DPA/Controls/ApplicationsUsingMyDatabasesControl.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <dpa:DpaApplicationsUsingMyDatabases ID="DpaApplicationsUsingMyDatabases" runat="server" />
    </Content>
</dpa:DpaResourceWrapper>
