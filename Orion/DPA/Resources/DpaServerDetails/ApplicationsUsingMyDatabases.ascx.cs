﻿using System;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_ApplicationsUsingMyDatabases : DpaServerBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_ApplicationsUsingMyDatabases_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceApplicationsUsingMyDatabases";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DpaWrapper.DpaServerId = DpaServer.DpaServerId;
        if (!Settings.IsApmInstalled)
        {
            // hide resource, when SAM is not installed
            Visible = false;
        }
        else
        {
            DpaApplicationsUsingMyDatabases.Initialize(Resource.ID, DpaWrapper.ErrorMessageClientId,
                DpaWrapper.LoadingIconClientId, DpaServer.DpaServerId);
        }

        // show CONFIGURE CLIENT APPLICATIONS button
        if (Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel,
                PageUrlHelper.RelationshipManagementClientApplications);
        }
    }
}