﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DpaServerDetails.ascx.cs" Inherits="Orion_DPA_Resources_DpaServerDetails_DpaServerDetails" %>
<%@ Register TagPrefix="dpa" TagName="DpaSummaryResourceWrapper" Src="~/Orion/DPA/Resources/DpaSummaryResourceWrapper.ascx" %>

<dpa:DpaSummaryResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div runat="server" ID="mainContent" class="DPA_DpaServerDetails">
	        
            <script>
            $(document).ready(function () {
                var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>');
                var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= DpaWrapper.ConnectivityIssueWarningClientId %>');
                var errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');

                var dpaServerDetails = new SW.DPA.UI.DpaServerDetails({
                    wrapperElement: $('#<%= mainContent.ClientID %>'),
                    dpaServerId: <%= base.DpaServer.DpaServerId %>,
                    loadingIcon: loadingIcon,
                    connectivityIssueWarning: connectivityIssueWarning,
                    errorMessage: errorMessage
                });

                var refresh = function () { dpaServerDetails.load(); };
                SW.Core.View.AddOnRefresh(refresh, <%= Resource.ID %>);
                refresh();
            });
            </script>
	    </div>
	</Content>
</dpa:DpaSummaryResourceWrapper>