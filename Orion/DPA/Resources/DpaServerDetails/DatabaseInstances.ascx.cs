﻿using System;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_Summary_DatabaseInstances : DpaServerBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_DatabaseInstances_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceDatabaseInstances";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DpaWrapper.DpaServerId = base.DpaServer.DpaServerId;

        DpaDatabaseInstances.Initialize(Resource.ID, DpaWrapper.ConnectivityIssueWarningClientId,
            DpaWrapper.ErrorMessageClientId, DpaWrapper.LoadingIconClientId, DpaServer.DpaServerId, Resource.Title);

        // show CONFIGURE DB INSTANCES button
        if (Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.DatabaseInstancesResource_ConfigureDbInstancesButtonLabel,
                PageUrlHelper.RelationshipManagementDatabaseInstances);
        }
    }
}