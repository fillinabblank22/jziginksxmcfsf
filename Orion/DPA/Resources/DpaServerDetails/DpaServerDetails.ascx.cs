﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_DPA_Resources_DpaServerDetails_DpaServerDetails : DpaServerBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_DpaServerDetails_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceDPAServerDetails";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Profile.AllowAdmin)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.DpaServerManagementButtonLabel,
                PageUrlHelper.ServerManagement);
        }
    }
}