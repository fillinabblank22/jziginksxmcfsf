﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DpaResourceWrapperBase.ascx.cs" Inherits="Orion_DPA_Resources_DpaResourceWrapperBase" %>
<%@ Register TagPrefix="dpa" TagName="LoadingIcon" Src="~/Orion/DPA/Controls/LoadingIcon.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ConnectivityIssueWarning" Src="~/Orion/DPA/Controls/ConnectivityIssueWarning.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ErrorMessage" Src="~/Orion/DPA/Controls/ErrorMessage.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <dpa:LoadingIcon runat="server" ID="LoadingIcon" /> 
        <dpa:ConnectivityIssueWarning runat="server" ID="ConnectivityIssueWarning" />
        <dpa:ErrorMessage runat="server" ID="ErrorMessage" />
        <asp:PlaceHolder runat="server" ID="DpaResourceWrapperPlaceHolder" />
    </Content>
</orion:resourceWrapper>