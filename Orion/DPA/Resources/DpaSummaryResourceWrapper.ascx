﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DpaSummaryResourceWrapper.ascx.cs" Inherits="Orion_DPA_Resources_DpaSummaryResourceWrapper" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapperBase" Src="~/Orion/DPA/Resources/DpaResourceWrapperBase.ascx" %>

<dpa:DpaResourceWrapperBase runat="server" ID="ResourceWrapperBase">
    <Content>
        <span class="sw-dpa-resource">
            <asp:PlaceHolder runat="server" ID="DpaResourcePlaceHolder" />
        </span>
    </Content>
</dpa:DpaResourceWrapperBase>
