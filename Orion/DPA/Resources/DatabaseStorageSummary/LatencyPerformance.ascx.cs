﻿using System;
using Resources;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_DPA_Resources_DatabaseStorageSummary_LatencyPerformance : SrmPerformanceResourceBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var databaseInstanceLunDal = ServiceLocator.Current.GetInstance<IDatabaseInstanceLunDAL>();

        if (!Settings.IsSrmInstalled || databaseInstanceLunDal.CountLuns() == 0)
        {
            Visible = false;
            return;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_LatencyPerformancePerLun_Title;
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "SRMPHResourcePoolLatency";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string EditURL
    {
        get
        {
            return this.ExtendCustomEditUrl("/Orion/SRM/EditTopXXChartResource.aspx?EditXX=True");
        }
    }

    protected string Data
    {
        get
        {
            return
                Serializer.Serialize(
                new
                {
                    ResourceId = Resource.ID,
                    NetObjectId = String.Empty,
                    SampleSizeInMinutes = SampleSizeInMinutesProperty,
                    DaysOfDataToLoad = DaysOfDataToLoadProperty,
                    YAxisTitle = DPAWebContent.LatencyPerformancePerLun_Chart_yAxis_Title,
                    ChartTitle = String.IsNullOrEmpty(this.ChartTitleProperty) ? SolarWinds.DPA.Strings.Resources.DPAResourceConfig_LatencyPerformancePerLun_Title : this.ChartTitleProperty,
                    ChartSubTitle = this.ChartSubTitleProperty,
                    ChartInitialZoom = this.ChartInitialZoomProperty,
                    NumberOfSeriesToShow = this.NumberOfSeriesToShowProperty,
                    DataUrl = "/Orion/DPA/Services/SrmCharts.asmx/GetLatencyLunPerformanceByField"
                });
        }
    }
}