﻿using System;
using System.Web;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_DPA_Resources_DatabaseStorageSummary_StoragesUsedByAllDatabases : DpaBaseResource
{
    protected override string DefaultTitle
    {
        get { return SolarWinds.DPA.Strings.Resources.DPAResourceConfig_StorageUsedByAllDatabases_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPAResourceStoragesUsedByAllDatabases";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public string BulletOneContent
    {
        get
        {
            var htmlEncoded = HttpUtility.HtmlEncode(Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_BulletOneContent);

            var htmlWithLink = htmlEncoded
                .Replace("[STARTLINK]", $"<a href=\"{HttpUtility.HtmlEncode(SolarWinds.DPA.Common.CommonConstants.SrmLink)}\" target=\"_blank\">")
                .Replace("[ENDLINK]", "</a>");

            return htmlWithLink;
        }
    }

    public string BulletTwoContent
    {
        get
        {
            var htmlEncoded = HttpUtility.HtmlEncode(Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_BulletTwoContent);

            var htmlWithLink = Profile.AllowAdmin ? htmlEncoded
                .Replace("[STARTLINK]",
                    $"<a href=\"{HttpUtility.HtmlEncode(PageUrlHelper.RelationshipManagementStorages)}\">")
                .Replace("[ENDLINK]", "</a>") : htmlEncoded.Replace("[STARTLINK]", String.Empty).Replace("[ENDLINK]", String.Empty);

            return htmlWithLink;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Settings.IsSrmInstalled)
        {
            Visible = false;
            return;
        }

        if (Profile.AllowAdmin && Settings.IsIntegrationEstablished)
        {
            DpaWrapper.ShowManageButton(
                Resources.DPAWebContent.StoragesUsedByThisDatabaseResource_ConfigureStorages,
                PageUrlHelper.RelationshipManagementStorages);
        }
    }
}