﻿using System;
using System.Text.RegularExpressions;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Resources;

public partial class Orion_DPA_Resources_Summary_StorageObjectsByPerformanceRisk : DpaBaseResource
{
    protected override string DefaultTitle => SolarWinds.DPA.Strings.Resources.Resource_StorageObjectsByPerformanceRisk_Title;

    public override string HelpLinkFragment => "OrionDPAResourceStorageObjectsPerformanceRisks";

    public ResourceSearchControl SearchControl
    {
        get;
        set;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var databaseInstanceLunDal = ServiceLocator.Current.GetInstance<IDatabaseInstanceLunDAL>();
        if (!Settings.IsSrmInstalled || databaseInstanceLunDal.CountLuns() == 0)
        {
            Visible = false;
            return;
        }

        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        DpaWrapper.HeaderButtons.Controls.Add(SearchControl);
        SearchControl = SearchControl;

        // for opening SRM resource by clicking on cells in table
        DpaWrapper.Controls.Add(LoadControl("~/Orion/SRM/Controls/Redirector.ascx"));
    }

    protected int PageSize
    {
        get
        {
            var defaultPageSize = 5;
            int pageSize = defaultPageSize;
            return pageSize;
        }
    }
}