﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StoragesUsedByAllDatabases.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseStorageSummary_StoragesUsedByAllDatabases" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>

<orion:Include runat="server" Module="DPA" File="Resources.js" />
<orion:Include runat="server" File="DPA/Js/DPA.UI.SortablePageableTable.js" />

<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
    <Content>
        <div class="DPA_AppsUsingDb DPA_StoragesUsedByDatabases" runat="server" id="Content"></div>
        <div id="Resource-<%= Resource.ID %>-NoContent" class="DPA_SampleResourceContent" style="display: none">
            <img src="/Orion/DPA/images/Samples/StorageUsedByMyDatabasesSample.png" alt="sample" />
            
            <div class="DPA_SampleBox">
                <p><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Paragraph1 %></p>

                <ul>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li1 %></li>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li2 %></li>
                    <li><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_Li3  %></li>
                </ul>
            </div>
            
            <h4><%: Resources.DPAWebContent.StorageUsedByAllDatabases_SampleContent_HowTo %></h4>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">1</div>
                <div class="DPA_Content">
                    <p><%= BulletOneContent %></p>
                </div>
                <div class="DPA_Clear"></div>
            </div>
            
            <div class="DPA_HowToSteps">
                <div class="DPA_Bullet">2</div>
                <div class="DPA_Content">
                    <p><%= BulletTwoContent %></p>
                </div>
                <div class="DPA_Clear"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= DpaWrapper.LoadingIconClientId %>');
                var errorMessage = new SW.DPA.UI.ErrorMessage('<%= DpaWrapper.ErrorMessageClientId %>');
                var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= DpaWrapper.ConnectivityIssueWarningClientId %>');

                var refresh = function() {
                    var storagesUsedByDatabases = new SW.DPA.UI.StoragesUsedByDatabases('<%= Content.ClientID %>', 'Resource-<%= Resource.ID %>-NoContent');
                    storagesUsedByDatabases.loadDatabaseInstances(loadingIcon, errorMessage, connectivityIssueWarning, <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>);
                };

                SW.Core.View.AddOnRefresh(refresh, '<%= Content.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</dpa:DpaResourceWrapper>
