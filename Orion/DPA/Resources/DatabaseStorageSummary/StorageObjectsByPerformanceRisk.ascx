﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageObjectsByPerformanceRisk.ascx.cs" Inherits="Orion_DPA_Resources_Summary_StorageObjectsByPerformanceRisk" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="DpaResourceWrapper" Src="~/Orion/DPA/Resources/DpaResourceWrapper.ascx" %>

<orion:Include runat="server" Module="DPA" File="Resources.js" />
<orion:Include runat="server" File="DPA/Js/DPA.UI.SortablePageableTable.js" />
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>

<dpa:DpaResourceWrapper runat="server" ID="DpaWrapper">
	<Content>
	    <div runat="server" ID="Content"></div>
        <div runat="server" ID="NoIntegrationContent" style="display: none"></div>
        <script>
            $(document).ready(function() {
                SW.DPA.UI.StorageObjectsByPerformanceRiskGrid(
                    {
                        contentId:'<%: Content.ClientID %>',
                        noIntegrationContentId: '<%: NoIntegrationContent.ClientID %>',
                        searchControlId: '<%: SearchControl.ClientID %>',
                        isNodeFunctionalityAllowed: <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>,
                        loadingIconId: '<%: DpaWrapper.LoadingIconClientId %>',
                        errorMessageId: '<%: DpaWrapper.ErrorMessageClientId %>',
                        pageSize: <%: PageSize %>
                    },
                    {
                        waitTimePerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.WaitTimePerformanceStatusIconTarget %>",
                        queryPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.QueryPerformanceStatusIconTarget %>",
                        cpuPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.CPUPerformanceStatusIconTarget %>",
                        memoryPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.MemoryPerformanceStatusIconTarget %>",
                        diskPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.DiskPerformanceStatusIconTarget %>",
                        sessionPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.SessionPerformanceStatusIconTarget %>"
                });
            });
        </script>
    </Content>
</dpa:DpaResourceWrapper>
