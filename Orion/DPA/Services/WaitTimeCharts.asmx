﻿<%@ WebService Language="C#" Class="WaitTimeCharts" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WaitTimeCharts : DpaServerChartDataWebService
{
    [WebMethod]
    public ChartDataResults GetInstancesWithHighestWaitTime(ChartDataRequest request)
    {
        Init(request);
        var databaseInstanceDAL = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
        var trendDataDimensionDAL = ServiceLocator.Current.GetInstance<ITrendDataDimensionDAL>();
        var dpaSwisSettingsContextFactory = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>();

        return GetWaitTimeTrendData(request,
            ids => databaseInstanceDAL.GetTopWaitDatabaseInstances(ids, 5),
            databaseInstanceIds => trendDataDimensionDAL.GetWaitTimeForInstances(databaseInstanceIds).ToList(),
            dpaSwisSettingsContextFactory
            );
    }

    [WebMethod]
    public ChartDataResults GetGreatestUpwardWaitTimeTrends(ChartDataRequest request)
    {
        Init(request);
        var databaseInstanceDAL = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
        var trendDataDimensionDAL = ServiceLocator.Current.GetInstance<ITrendDataDimensionDAL>();
        var dpaSwisSettingsContextFactory = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>();

        return GetWaitTimeTrendData(request,
            ids => databaseInstanceDAL.GetGreatestUpwardWaitTimeTrends(ids),
            databaseInstanceIds => trendDataDimensionDAL.GetGreatestUpwardWaitTimeTrendsForInstances(databaseInstanceIds).ToList(),
            dpaSwisSettingsContextFactory
            );
    }

    [WebMethod]
    public ChartDataResults GetGreatestDownwardWaitTimeTrends(ChartDataRequest request)
    {
        Init(request);
        var databaseInstanceDAL = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
        var trendDataDimensionDAL = ServiceLocator.Current.GetInstance<ITrendDataDimensionDAL>();
        var dpaSwisSettingsContextFactory = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>();

        return GetWaitTimeTrendData(request,
            ids => databaseInstanceDAL.GetGreatestDownwardWaitTimeTrends(ids),
            databaseInstanceIds => trendDataDimensionDAL.GetGreatestDownwardWaitTimeTrendsForInstances(databaseInstanceIds).ToList(),
            dpaSwisSettingsContextFactory
            );
    }

    private ChartDataResults GetWaitTimeTrendData(
        ChartDataRequest request,
        Func<IEnumerable<int>, DataTable> dbInstancesProvider,
        Func<List<int>, IList<TrendDataDimension>> trendDataProvider,
        IDpaSwisSettingsContextFactory dpaSwisSettingsContextFactory)
    {
        var dpaServerId = GetDpaServerId(request);
        var dpaSettings = ServiceLocator.Current.GetInstance<IDPASettings>();
        var dataSeriesColection = new List<DataSeries>();


        using (var dpaSwisSettingsContext = dpaSwisSettingsContextFactory.CreateContext(PartialResultsMode.EnablePartialResults))
        {
            if (dpaSettings.IsIntegrationEstablished)
            {
                var databaseInstanceDAL = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
                DataTable topDatabaseInstances;

                var allGlobalDatabaseInstanceIds = databaseInstanceDAL.GetAllGlobalDatabaseInstanceIds(dpaServerId);
                topDatabaseInstances = dbInstancesProvider(allGlobalDatabaseInstanceIds);

                var globalDatabaseInstanceIds = new List<int>();
                foreach (DataRow dataRow in topDatabaseInstances.Rows)
                {
                    if (dataRow["Id"] != DBNull.Value)
                    {
                        var id = (int)dataRow["Id"];
                        globalDatabaseInstanceIds.Add(id);
                    }
                }

                if (globalDatabaseInstanceIds.Count > 0)
                {
                    IList<TrendDataDimension> trendData = trendDataProvider(globalDatabaseInstanceIds);

                    if (trendData.Any())
                    {
                        var endDate = GetNormalizedLocalTime(trendData.Max(e => e.LocalTime));
                        var chartInterval = SolarWinds.DPA.Common.CommonConstants.NumberOfDaysOnWaitTimeChartOnSummaryView;
                        foreach (DataRow dataRow in topDatabaseInstances.Rows)
                        {
                            if (dataRow["Id"] == DBNull.Value || (int)dataRow["Id"] <= 0)
                                continue;

                            var globalDatabaseInstanceId = (int)dataRow["Id"];

                            var hasNode = dataRow["NodeID"] != DBNull.Value;
                            var databaseInstanceName = dataRow.Field<string>("Name");
                            var databaseInstanceLink = PageUrlHelper.DatabaseInstanceDetail(globalDatabaseInstanceId);
                            var databaseInstanceIcon = StatusIconsHelper.GetSmallStatusIcon(SwisEntities.DatabaseInstance, dataRow.Field<int>("DatabaseInstanceStatus"));

                            var dataSeries = new DataSeries()
                            {
                                TagName =
                                    dataSeriesColection.Count > 0
                                        ? String.Format("WaitTime{0}", dataSeriesColection.Count)
                                        : "WaitTime",
                                Label = databaseInstanceName,
                                CustomProperties = new
                                {
                                    DbInstanceLink = databaseInstanceLink,
                                    DbInstanceName = databaseInstanceName,
                                    DbInstanceIcon = databaseInstanceIcon,

                                    HasNode = hasNode,

                                    NodeLink = hasNode ? PageUrlHelper.ViewPage(NetObjectPrefix.Node, (int)dataRow["NodeID"]) : String.Empty,
                                    NodeName = hasNode ? dataRow["NodeCaption"] : String.Empty,
                                    NodeIcon = hasNode ? StatusIconsHelper.GetSmallStatusIcon(SwisEntities.Nodes, (int)dataRow["NodeStatus"], (int)dataRow["NodeID"]) : String.Empty
                                }
                            };

                            var chartDataDictionary = trendData
                                .Where(tdd => tdd.GlobalDatabaseId == globalDatabaseInstanceId)
                                .OrderBy(tdd => tdd.LocalTime).ToDictionary(key => GetNormalizedLocalTime(key.LocalTime).Date, value => value);

                            for (var date = endDate.AddDays(-chartInterval); date <= endDate; date = date.AddDays(1))
                            {
                                TrendDataDimension trendDataDimension;
                                if (chartDataDictionary.TryGetValue(date.Date, out trendDataDimension))
                                {
                                    dataSeries.Data.Add(DataPoint.CreatePoint(date.Date, trendDataDimension.Wait));
                                }
                                else
                                {
                                    // add missing point to series
                                    dataSeries.Data.Add(DataPoint.CreateNullPoint(date.Date));
                                }
                            }

                            dataSeriesColection.Add(dataSeries);
                        }
                    }
                }
            }

            var chartResult = new ChartDataResults(dataSeriesColection);
            var partialErrors = dpaSwisSettingsContext.GetPartialErrors();

            chartResult.ChartOptionsOverride = new
            {
                PartialResultErrors = partialErrors,
                EndpointNotAvailable = partialErrors.Any()
            };

            return chartResult;
        }
    }

    static DateTime GetNormalizedLocalTime(DateTime localTime)
    {
        if (localTime.Kind != DateTimeKind.Local)
            throw new ArgumentException(string.Format("Wrong argument DateTimeKind ({0}). Expected 'DateTimeKind.Local'", localTime.Kind), "localTime");

        var daylightTime = TimeZone.CurrentTimeZone.GetDaylightChanges(localTime.Year);

        // adding 1 hour moves data to next day when DPA is in the same time zone but installed in summer time (+1)
        return GetDateTimeWithoutDaylightSaving(localTime).Add(daylightTime.Delta);
    }

    static DateTime GetDateTimeWithoutDaylightSaving(DateTime localTime)
    {
        if (localTime.Kind != DateTimeKind.Local)
            throw new ArgumentException(string.Format("Wrong argument DateTimeKind value ({0}). Expected 'DateTimeKind.Local'", localTime.Kind), "localTime");

        var daylightTime = TimeZone.CurrentTimeZone.GetDaylightChanges(localTime.Year);

        return localTime > daylightTime.Start && localTime < daylightTime.End ? localTime.Add(-daylightTime.Delta) : localTime;
    }
}