﻿<%@ WebService Language="C#" Class="BlockersChart" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.InformationService.Exceptions;
using SolarWinds.DPA.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class BlockersChart : DatabaseInstanceChartDataWebService
{

    [WebMethod]
    public ChartDataResults GetBlockers(ChartDataRequest request)
    {
        var dataSeriesColection = new List<DataSeries>();
        var endpointNotAvailable = false;

        var dataSeries = new DataSeries() { TagName = "Sessions" };

        var end = ToMinutes(DateTime.UtcNow);
        var start = ToMinutes(end.AddHours(-1));
        int globalDatabaseInstanceId = GetGlobalDatabaseInstanceId(request);
        var blockingOverviewDal = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IBlockingOverviewDAL>();
        
        dataSeries.CustomProperties = new
        {
            DatabaseInstanceId = globalDatabaseInstanceId
        };
        
        try
        {
            IList<BlockingOverview> blockingOverviews = blockingOverviewDal.GetAllByGlobalDatabaseInstanceId(globalDatabaseInstanceId, start, end, TimesliceUnit.Minute);
            IDictionary<DateTime, BlockingOverview> blockingOverviewsDictionary =
                blockingOverviews.ToDictionary(e => ToMinutes(DateTimeHelper.AddUtcKind(e.Time)));

            for (var currentTime = start; currentTime <= end; currentTime = currentTime.AddMinutes(1))
            {
                BlockingOverview blockingOverview;
                if (blockingOverviewsDictionary.TryGetValue(currentTime, out blockingOverview))
                {
                    dataSeries.AddPoint(DataPoint.CreatePoint(currentTime, blockingOverview.BlockedSessions));
                }
                else
                {
                    dataSeries.AddPoint(DataPoint.CreatePoint(currentTime, 0));
                }
            }          
        }
        catch (EndpointNotAvailableException)
        {
            endpointNotAvailable = true;
        }
        
        dataSeriesColection.Add(dataSeries);

        var chartResult = new ChartDataResults(dataSeriesColection);

        chartResult.ChartOptionsOverride = new
        {
            EndpointNotAvailable = endpointNotAvailable
        };

        return chartResult;
    }

    private DateTime ToMinutes(DateTime dateTime)
    {
        return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, DateTimeKind.Utc);
    }    
}