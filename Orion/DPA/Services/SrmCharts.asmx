﻿<%@ WebService Language="C#" Class="SrmCharts" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Models.SRM;
using SolarWinds.DPA.Common.Utility;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Model.Charts;
using SolarWinds.DPA.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;

/// <summary>
/// Summary description for SrmCharts
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class SrmCharts : DatabaseInstanceChartDataWebService
{
    [WebMethod]
    public ChartDataResults GetIOPSPerformanceByField(ChartDataRequest request)
    {
        int? globalDatabaseInstanceId = GetGlobalDatabaseInstanceId(request);
        var field = (StatisticFields)Enum.Parse(typeof(StatisticFields), InvariantString.ToString(request.AllSettings["filterField"]));

        DateTime startTime = DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"]));
        DateTime endTime = DateTime.UtcNow;
        var chartRange = new ChartDateRange(DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"])), ((int)request.AllSettings["sampleSizeInMinutes"]),
            ((int)request.AllSettings["numberOfSeriesToShow"]));

        var series = new List<V2DataSeries>();
        var lunStatisticsDal = ServiceLocator.Current.GetInstance<ILunStatisticsDal>();

        DataTable lunIops = lunStatisticsDal.GetLunPerformance(globalDatabaseInstanceId, chartRange.StartTime, chartRange.EndTime, field, chartRange.SampleSizeInMinutes);

        if (lunIops.Rows.Count == 0)
        {
            return new ChartDataResults(series);
        }

        return EntityStatisticsHelper.GetEntityPerformanceByField(lunIops, SwisEntities.Lun, chartRange);
    }

    private int? GetGlobalDatabaseInstanceId(ChartDataRequest request)
    {
        int? globalDatabaseInstanceId = null;
        if (request.NetObjectIds.Any() && !String.IsNullOrWhiteSpace(request.NetObjectIds.First()))
        {
            var netObject = request.NetObjectIds.First();
            int id;
            if (!NetObjectHelper.TryParseNetObjectId(netObject, out id))
            {
                throw new FormatException(netObject);
            }
            else
            {
                globalDatabaseInstanceId = id;
            }
        }

        return globalDatabaseInstanceId;
    }

    [WebMethod]
    public ChartDataResults GetLatencyLunPerformanceByField(ChartDataRequest request)
    {
        int? globalDatabaseInstanceId = GetGlobalDatabaseInstanceId(request);

        var fieldName = (StatisticFields)Enum.Parse(typeof(StatisticFields), InvariantString.ToString(request.AllSettings["filterField"]));

        var chartRange = new ChartDateRange(DateTime.UtcNow.AddDays(-((int)request.AllSettings["daysOfDataToLoad"])), ((int)request.AllSettings["sampleSizeInMinutes"]),
            ((int)request.AllSettings["numberOfSeriesToShow"]));

        var lunStatisticsDal = ServiceLocator.Current.GetInstance<ILunStatisticsDal>();

        DataTable performanceValues = lunStatisticsDal.GetLunPerformance(globalDatabaseInstanceId, chartRange.StartTime, chartRange.EndTime, fieldName, chartRange.SampleSizeInMinutes);

        return EntityStatisticsHelper.GetEntityPerformanceByField(performanceValues,SwisEntities.Lun,chartRange);
    }
}
