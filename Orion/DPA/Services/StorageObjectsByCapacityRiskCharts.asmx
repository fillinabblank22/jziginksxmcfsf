﻿<%@ WebService Language="C#" Class="StorageObjectsByCapacityRiskCharts" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class StorageObjectsByCapacityRiskCharts : ChartDataWebService
{
    private static readonly Log Log = new Log();

    [WebMethod]
    public ChartDataResults GetCapacityRiskData(List<object[]> data)
    {
        var series = new List<DataSeries>();
        var currentContext = HttpContext.Current;

        Parallel.ForEach(data, item =>
        {
            HttpContext.Current = currentContext;

            try
            {
                var capacityRisk = ServiceLocator.Current.GetInstance<ICapacityRisk>();

                var totalCapacity = Convert.ToInt64(item[11]);
                var offset = item[13] == null ? default(double?) : Convert.ToDouble(item[13]);
                var slope = item[14] == null ? default(double?) : Convert.ToDouble(item[14]);

                var capacityRunOutWarning = capacityRisk.ComputeCapacityRunOut((int)item[8], totalCapacity, offset, slope);
                var capacityRunOutCritical = capacityRisk.ComputeCapacityRunOut((int)item[9], totalCapacity, offset, slope);
                var capacityRunOutTotal = capacityRisk.ComputeCapacityRunOut((int)item[10], totalCapacity, offset, slope);

                dynamic properties = new JsonObject();
                properties.capacityRunOutDateWarning = GetDayDifference(capacityRunOutWarning);
                properties.capacityRunOutDateCritical = GetDayDifference(capacityRunOutCritical);
                properties.capacityRunOutDateTotal = GetDayDifference(capacityRunOutTotal);

                var serie = new DataSeries
                {
                    TagName = string.Concat(item[4], "_", item[0]),
                    Label = (string)item[1],
                    CustomProperties = properties
                };
                lock (((ICollection)series).SyncRoot)
                {
                    series.Add(serie);
                }

                double dataSlope;
                var points = capacityRisk.GetData((int)item[0], out dataSlope);
                foreach (var point in points)
                {
                    serie.AddPoint(point);
                }

                properties.slope = totalCapacity == 0 ? 0 : dataSlope * 100 / Convert.ToDouble(totalCapacity);
            }
            catch (Exception ex)
            {
                Log.Error(
                    string.Format(CultureInfo.InvariantCulture,
                        "Exception occured during getting information for charts and thresholds for Capacity Risk resource. Row: {0}",
                        string.Join(", ", item)), ex);
                throw;
            }
        });

        return new ChartDataResults(series);
    }

    private int GetDayDifference(DateTime? dateValue)
    {
        if (dateValue == null)
        {
            return -36500;
        }

        return (int)Math.Ceiling((dateValue.GetValueOrDefault().ToLocalTime() - DateTime.Now).TotalDays);
    }
}
