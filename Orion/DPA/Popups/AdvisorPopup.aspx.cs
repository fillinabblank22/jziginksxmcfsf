﻿using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPA_Popups_AdvisorPopup : System.Web.UI.Page
{
    private string _queryText;
    private int _maxLengthOfQueryString = 250;

    protected ProblemSummary ProblemSummary { get; set; }
    protected ProblemSQLStatement ProblemSQLStatement { get; set; }
    protected string StatusCssClass { get; set; }
    protected string QueryText
    {
        get { return _queryText; }
        set
        {
            _queryText = FormatHelper.ShortIt(value, _maxLengthOfQueryString, "...");
        }
    }

    protected double TotalQueryExecutionTime 
    {
        get
        {
            return FormatHelper.RoundNumber(ProblemSQLStatement.TotalSqlWaitTime);
        }
    }

    protected double PercentOfTotalDatabaseWaitTime
    {
        get
        {
            return FormatHelper.RoundNumber(ProblemSQLStatement.PercentOfTotalDatabaseWaitTime);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        AdvisorNetObject advisorNetObject = NetObjectFactory.Create(Request.QueryString["NetObject"], true) as AdvisorNetObject;

        if (advisorNetObject == null) throw new NullReferenceException("advisorNetObject");
        if (advisorNetObject.Model == null) throw new NullReferenceException("advisorNetObject.Model");

        ProblemSummary = advisorNetObject.Model;

        var problemSqlStatementDal = new ProblemSQLStatementDAL();
        ProblemSQLStatement = problemSqlStatementDal.GetProblemSQLStatement(ProblemSummary.GlobalDatabaseId,
            ProblemSummary.ProblemId);

        if (ProblemSQLStatement == null)
            throw new NullReferenceException("ProblemSQLStatement");

        QueryText = ProblemSQLStatement.SqlText;
    }
}