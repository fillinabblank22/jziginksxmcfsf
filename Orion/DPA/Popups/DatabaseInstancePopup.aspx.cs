﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Extensions;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Shared;

public partial class Orion_DPA_Popups_DatabaseInstancePopup : System.Web.UI.Page
{
    private Log _log = new Log();
    private IDatabaseInstanceDAL _databaseInstanceDAL = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();

    protected PerformanceOverview PerformanceOverview { get; set; }

    protected int DatabaseInstanceStatusId { get; set; }
    protected string DatabaseInstanceName { get; set; }
    protected int DatabaseInstanceId { get; set; }
    protected string DatabaseInstanceIcon { get; set; }
    protected StatusInfo DatabaseInstanceStatusInfo { get; set; }
    protected bool IsLicensed { get; set; }
    protected string DatabaseInstanceType { get; set; }
    protected string DatabaseInstanceVersion { get; set; }
    protected string DatabaseInstanceVersionSuffix { get; set; }

    protected bool HasNode { get; set; }
    protected string NodeName { get; set; }
    protected int NodeId { get; set; }
    protected int NodeStatusId { get; set; }
    protected string NodeIcon { get; set; }
    protected StatusInfo NodeStatusInfo { get; set; }

    protected int CpuLoad { get; set; }
    protected int MemoryUsed { get; set; }

    protected string DpaServerName { get; set; }
    protected string DpaServerStatusIconUrl { get; set; }
    protected string DpaServerDetailUrl { get; set; }

    protected IList<string> CriticalPerformanceProblems { get; set; }
    protected IList<string> WarningPerformanceProblems { get; set; }
    protected IList<string> UnknownPerformanceProblems { get; set; }

    protected bool IsDpaNotAvailable { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CriticalPerformanceProblems = new List<string>();
        WarningPerformanceProblems = new List<string>();
        UnknownPerformanceProblems = new List<string>();

        var netObject = Request.QueryString["NetObject"];
        var databaseInstanceNetObject = NetObjectFactory.Create(netObject, true) as DatabaseInstanceNetObject;
        if (databaseInstanceNetObject != null)
        {
            IsDpaNotAvailable = databaseInstanceNetObject.IsDpaNotAvailable;
            if (!databaseInstanceNetObject.IsDpaNotAvailable)
            {
                var globalDatabaseInstanceId = databaseInstanceNetObject.Model.GlobalDatabaseInstanceId;
                DataTable databaseInstanceDetails =
                    _databaseInstanceDAL.GetDatabaseInstanceDetails(globalDatabaseInstanceId);
                if (databaseInstanceDetails.Rows.Count > 0)
                {
                    DataRow dataRow = databaseInstanceDetails.Rows[0];
                    DatabaseInstanceStatusId = dataRow.Field<int>("Status");
                    DatabaseInstanceName = dataRow.Field<string>("Name");
                    DatabaseInstanceId = dataRow.Field<int>("GlobalDatabaseInstanceID");
                    DatabaseInstanceIcon = StatusIconsHelper.GetSmallStatusIcon(SwisEntities.DatabaseInstance,
                        DatabaseInstanceStatusId);
                    DatabaseInstanceStatusInfo = StatusInfo.GetStatus(DatabaseInstanceStatusId);
                    IsLicensed = dataRow.Field<bool>("IsLicensed");
                    DatabaseInstanceType = dataRow.Field<string>("Type");
                    DatabaseInstanceVersion = dataRow.Field<string>("Version");
                    DatabaseInstanceVersionSuffix = dataRow.Field<string>("VersionSuffix");

                    HasNode = dataRow["NodeID"] != DBNull.Value;
                    if (HasNode)
                    {
                        NodeName = dataRow.Field<string>("NodeCaption");
                        NodeStatusId = dataRow.Field<int>("NodeStatus");
                        NodeId = dataRow.Field<int>("NodeID");
                        NodeStatusInfo = StatusInfo.GetStatus(NodeStatusId);
                        NodeIcon = StatusIconsHelper.GetSmallStatusIcon(SwisEntities.Nodes, NodeStatusId, NodeId);
                    }
                    CpuLoad = dataRow.Field<int>("CPULoad");
                    MemoryUsed = dataRow.Field<int>("PercentMemoryUsed");

                    SetupCpuLoadBar((short) CpuLoad);
                    SetupMemoryUsedBar((short) MemoryUsed);

                    PerformanceOverview = dataRow.ToEntity<PerformanceOverview>();
                    FindPerformanceProblems();

                    DpaServerName = dataRow.Field<string>("DpaServerName");
                    DpaServerStatusIconUrl =
                        StatusIconsHelper.GetDpaServerStatusIconFile(
                            (DpaServerStatus) dataRow.Field<int>("DpaServerStatus"));
                }
                else
                {
                    _log.ErrorFormat("Not found database instance for ID '{0}'", globalDatabaseInstanceId);
                }
            }
            else
            {
                DatabaseInstanceStatusId = StatusInfo.UnknownStatusId;
                var dpaServerDal = ServiceLocator.Current.GetInstance<IDpaServerDAL>();
                var dpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(databaseInstanceNetObject.Id);
                var dpaServer = dpaServerDal.GetAll(new int[] { dpaServerId }, 1).FirstOrDefault();
                if (dpaServer != null)
                {
                    DpaServerName = dpaServer.DisplayName;
                }
                else
                {
                    _log.ErrorFormat("DPA Server with ID '{0}' was not found!", dpaServerId);
                }
            }
        }
        else
        {
            _log.ErrorFormat("Cannot render Database instance popup, because cannot get Database Instance info from netObject with netObjectID '{0}'", netObject);
        }
    }

    protected string WaitTimeDescription
    {
        get
        {
            string formatedText;
            if (PerformanceOverview.WaitTimeAlarmLevel != AlarmLevel.Unknown)
            {
                DateTime waitTimeEnd = DateTimeHelper.AddUtcKind(PerformanceOverview.WaitTimeEnd).ToLocalTime();
                var endTimeTime = waitTimeEnd.ToString("H:mm tt");
                var endTimeDay = waitTimeEnd.ToString("MMM d, yyyy");
                var waitTime = FormatHelper.FormatSecondsToTwoOrderUnitFormat(PerformanceOverview.WaitTimeSecs);


                if (PerformanceOverview.WaitTimeEnd.Date == DateTime.UtcNow.Date)
                {
                    formatedText = String.Format(Resources.DPAWebContent.DatabaseInstancesToolTip_WaitTimeDescriptionTodayFormat,
                        waitTime, endTimeTime);
                }
                else if (PerformanceOverview.WaitTimeEnd.Date == DateTime.UtcNow.AddDays(-1).Date)
                {
                    formatedText = String.Format(Resources.DPAWebContent.DatabaseInstancesToolTip_WaitTimeDescriptionYesterdatFormat,
                        waitTime, endTimeTime);
                }
                else
                {
                    formatedText =
                        String.Format(Resources.DPAWebContent.DatabaseInstancesToolTip_WaitTimeDescriptionCustomDateFormat,
                            waitTime, endTimeDay, waitTime);
                }
            }
            else
            {
                formatedText = Resources.DPAWebContent.DatabaseInstancesToolTip_WaitTimeDescription_MonitorDown;
            }
            
            return formatedText;
        }
    }

    private void FindPerformanceProblems()
    {
        // critical
        if (PerformanceOverview.WaitTimeAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalWaitTimeText);
        }
        if (PerformanceOverview.QueriesAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalQueryText);
        }
        if (PerformanceOverview.CPUAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalCPUText);
        }
        if (PerformanceOverview.MemoryAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalMemoryText);
        }
        if (PerformanceOverview.DiskAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalDiskText);
        }
        if (PerformanceOverview.SessionAlarmLevel == AlarmLevel.Critical)
        {
            CriticalPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_CriticalSessionText);
        }

        // warning
        if (PerformanceOverview.WaitTimeAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningWaitTimeText);
        }
        if (PerformanceOverview.QueriesAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningQueryText);
        }
        if (PerformanceOverview.CPUAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningCPUText);
        }
        if (PerformanceOverview.MemoryAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningMemoryText);
        }
        if (PerformanceOverview.DiskAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningDiskText);
        }
        if (PerformanceOverview.SessionAlarmLevel == AlarmLevel.Warning)
        {
            WarningPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_WarningSessionText);
        }

        // unknown
        if (PerformanceOverview.WaitTimeAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownWaitTimeText);
        }
        if (PerformanceOverview.QueriesAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownQueryText);
        }
        if (PerformanceOverview.CPUAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownCPUText);
        }
        if (PerformanceOverview.MemoryAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownMemoryText);
        }
        if (PerformanceOverview.DiskAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownDiskText);
        }
        if (PerformanceOverview.SessionAlarmLevel == AlarmLevel.Unknown)
        {
            UnknownPerformanceProblems.Add(Resources.DPAWebContent.DatabaseInstancesToolTip_UnknownSessionText);
        }
    }

    private void SetupCpuLoadBar(short cpuLoadValue)
    {
        if (cpuLoadValue != -2)
        {
            CPULoadControl.Value = cpuLoadValue;
            SetupBar(CPULoadBar, CpuLoad, 100, Thresholds.CPULoadWarning.SettingValue, Thresholds.CPULoadError.SettingValue);
        }
    }

    private void SetupMemoryUsedBar(short memoryUsedValue)
    {
        if (memoryUsedValue != -2)
        {
            MemoryUsedControl.Value = memoryUsedValue;
            SetupBar(MemoryUsedBar, memoryUsedValue, 100, Thresholds.PercentMemoryWarning.SettingValue, Thresholds.PercentMemoryError.SettingValue);
        }
    }

    private static void SetupBar(InlineBar bar, float value, float max, double warning, double error)
    {
        bar.Percentage = 100.0 * value / max;
        if (value > error)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Error;
        else if (value > warning)
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Warning;
        else
            bar.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
    }
}