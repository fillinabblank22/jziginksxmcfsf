﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdvisorPopup.aspx.cs" Inherits="Orion_DPA_Popups_AdvisorPopup" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<h3 class="<%= StatusHelper.GetToolTipStatusCssClassName(ProblemSummary.AlarmLevel) %>"><%: Resources.DPAWebContent.AdvisorToolTip_Title %></h3>
<div class="NetObjectTipBody DPA_AdvisorTip">
    <p class="DPA_Description">
         <%: ProblemSummary.ItemDescription %> <%: ProblemSummary.Summary %></p>
    
    <h5><%: Resources.DPAWebContent.AdvisorToolTip_QueryTitle %>:</h5>
    <p class="DPA_QueryText"><%: QueryText %></p>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <th style="width: 60%">
                <%: Resources.DPAWebContent.AdvisorToolTip_TotalQueryExecutionTime %>:
            </th>
            <td colspan="2" style="padding-left: 20px">
                <%: FormatHelper.FormatSeconds(TotalQueryExecutionTime) %>
            </td>
        </tr>
        <tr>
            <th style="width: 60%; white-space: normal">
                <%: Resources.DPAWebContent.AdvisorToolTip_TotalInstanceExecutionTime %>:
            </th>
            <td colspan="2" style="padding-left: 20px">
                 <%: FormatHelper.FormatPercentage(PercentOfTotalDatabaseWaitTime) %>
            </td>
        </tr>
    </table>
    
    <p class="DPA_Footer"><%: Resources.DPAWebContent.AdvisorToolTip_FooterText %></p>
</div>
