﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="DpaServerPopup.aspx.cs" Inherits="Orion_DPA_Popups_DpaServerPopup" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<h3 class="<%= StatusHelper.GetToolTipStatusCssClassName((int) DpaServer.Status) %>"><%: Resources.DPAWebContent.DatabaseInstanceNetObjectToolTip_Title %></h3>
<div class="NetObjectTipBody" class="DPA_DpaServerTooltip<%: DpaServer.DpaServerId %>" automation="DpaServerPopup">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_ServerStatus_Label %>:</th>
            <td colspan="2" automation="ServerStatus"><img src="<%: StatusIconsHelper.GetDpaServerStatusIconFile(DpaServer.Status) %>" style="vertical-align: top" /> <%: DpaServer.StatusDescription %></td>
        </tr>
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_ServerName_Label %>:</th>
            <td colspan="2" automation="ServerName"><%: DpaServer.DisplayName %></td>
        </tr>
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_Address_Label %>:</th>
            <td colspan="2" automation="ServerAddress"><%: DpaServer.GetJswisUri().Host %></td>
        </tr>
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_Port_Label %>:</th>
            <td colspan="2" automation="ServerPort"><%: DpaServer.GetJswisUri().Port %></td>
        </tr>
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_DpaVersion_Label %>:</th>
            <td colspan="2" automation="ServerVersion"><%: IsDpaAvailable ? DpaServerVersion : DpaIsNotAvailableMessage %></td>
        </tr>
        <tr>
            <th><%: Resources.DPAWebContent.DpaServerPopUp_DbInstancesCount_Label %>:</th>
            <td colspan="2" automation="ServerInstanceCount"><img src="/Orion/DPA/images/database-instance.png" style="vertical-align: top" /> <%: IsDpaAvailable ? DpaServerDbInstancesCount.ToString() : DpaIsNotAvailableMessage  %><span></span></td>
        </tr>
    </table>
    <div style="border-top:solid 1px #e2e2e2;margin-top:5px">
        <table>
            <tr>
                <th style="font-weight: bold;">
                    <%: Resources.DPAWebContent.DpaServerPopUp_DatabaseIssues_Label %>:
                </th>
                <td></td>
                <td style="text-align: right;color:#b5b5b5"><%: Resources.DPAWebContent.DatabaseInstancesToolTip_PoweredByDPA %></td>
            </tr>
        </table>
        <table>
            <% if (IsDpaAvailable)
               { %>
                <% if (CriticalInstancesCount > 0 || WarningInstancesCount > 0)
                   { %>
                    <% if (CriticalInstancesCount > 0)
                       { %>
                        <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceCritical %>" /></th><td style="padding:0"><%: CriticationInstancesMessage %></td></tr>
                    <% } %>
                    <% if (WarningInstancesCount > 0)
                       { %>
                        <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceWarning %>" /></th><td style="padding:0"><%: WarningInstancesMessage %></td></tr>
                    <% } %>   
                <% }
                   else
                   { %>
                 <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceNormal %>" /></th><td style="padding:0"><%: Resources.DPAWebContent.DpaServerPopUp_NoIssuesDetected_Label %></td></tr>           
                <% } %>
            <% } else { %> 
                <tr><td colspan="3"><%: DpaIsNotAvailableMessage %><td></tr>
            <% } %>
        </table>
    </div>
</div>
