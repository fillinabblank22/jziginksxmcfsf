﻿using System;
using System.Collections.Generic;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Data.Model;
using SolarWinds.DPA.InformationService.Exceptions;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Shared;

public partial class Orion_DPA_Popups_DpaServerPopup : System.Web.UI.Page
{
    private Log _log = new Log();

    protected string DpaIsNotAvailableMessage { get; private set; }
    protected bool IsDpaAvailable { get; private set; }
    protected DpaServer DpaServer { get; private set; }
    protected string DpaServerVersion { get; private set; }
    protected int DpaServerDbInstancesCount { get; private set; }
    protected int CriticalInstancesCount { get; private set; }
    protected string CriticationInstancesMessage { get; private set; }
    protected int WarningInstancesCount { get; private set; }
    protected string WarningInstancesMessage { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var netObject = Request.QueryString["NetObject"];
        var dpaServerNetObject = NetObjectFactory.Create(netObject, true) as DpaServerNetObject;
        if (dpaServerNetObject != null)
        {
            if (dpaServerNetObject.Model != null)
            {
                DpaServer = dpaServerNetObject.Model;
                DpaIsNotAvailableMessage = Resources.DPAWebContent.CannotLoad_Msg;

                if (DpaServer.Status == DpaServerStatus.Up || DpaServer.IntegrationStatus == IntegrationStatus.ConnectionToOrionFailed)
                {
                    try
                    {
                        var productInfoDal = ServiceLocator.Current.GetInstance<IProductInfoDAL>();
                        var databaseInstancesDal = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
                        DpaServerVersion = productInfoDal.GetVersionByDpaServerId(DpaServer.DpaServerId);
                        IDictionary<int,int> statusesCount = databaseInstancesDal.GetAllStatusesCount(new DatabaseInstancesFilter {DpaServerId = DpaServer.DpaServerId });
                        DpaServerDbInstancesCount = databaseInstancesDal.Count(DpaServer.DpaServerId) ?? 0;

                        if (statusesCount.ContainsKey(StatusInfo.CriticalStatusId))
                        {
                            CriticalInstancesCount = statusesCount[StatusInfo.CriticalStatusId];
                            CriticationInstancesMessage =
                                String.Format(Resources.DPAWebContent.DpaServerPopUp_CriticalInstancesCount_LabelFormat,
                                    CriticalInstancesCount);
                        }

                        if (statusesCount.ContainsKey(StatusInfo.WarningStatusId))
                        {
                            WarningInstancesCount = statusesCount[StatusInfo.WarningStatusId];
                            WarningInstancesMessage =
                                String.Format(Resources.DPAWebContent.DpaServerPopUp_WarningInstancesCount_LabelFormat,
                                    WarningInstancesCount);
                        }

                        IsDpaAvailable = true;
                    }
                    catch (EndpointNotAvailableException)
                    {
                        _log.DebugFormat("DPA server {0} is not available!", DpaServer.JSwisAddress);
                    }
                }
            }
        }
    }
}