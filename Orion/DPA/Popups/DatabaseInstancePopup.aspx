﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DatabaseInstancePopup.aspx.cs" Inherits="Orion_DPA_Popups_DatabaseInstancePopup" %>
<%@ Register TagPrefix="npm" TagName="CPULoad" Src="~/Orion/NetPerfMon/Controls/CPULoad.ascx" %>
<%@ Register TagPrefix="npm" TagName="MemoryUsed" Src="~/Orion/NetPerfMon/Controls/MemoryUsed.ascx" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Models" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<h3 class="<%= StatusHelper.GetToolTipStatusCssClassName(DatabaseInstanceStatusId) %>"><%: Resources.DPAWebContent.DatabaseInstanceNetObjectToolTip_Title %></h3>
<div class="NetObjectTipBody">
    <% if (IsDpaNotAvailable)
       {
        %>
        <div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn">
            <span class="sw-suggestion-icon"></span>
            <div class="DPA_Body">       
                <b><%: SolarWinds.DPA.Strings.Resources.ConnectivityIssue_Message %>:</b>
            <div id="PartialErrors" class="DPA_PartialErrors" runat="server">
                    <%: String.Format(Resources.DPAWebContent.DatabaseInstancePopUp_ConnectivityIssue_MessageFormat, DpaServerName) %>
                </div>
            </div>
        </div>
    <% } else{
    %>
    <p class="StatusDescription"><%: DatabaseInstanceName %> 
        <% if (HasNode)
           { %>
            <%: Resources.DPAWebContent.DatabaseNodeConjunction %> 
            <%: NodeName %> 
        <% } %>
    </p>
    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_StatusLabel %>:
            </th>
            <td style="text-align:center">
                <img src="<%: DatabaseInstanceIcon %>" />
            </td>
            <td>
                <%: StatusHelper.DatabaseInstanceStatusDescription(DatabaseInstanceStatusId, IsLicensed) %>
            </td>
        </tr>
         <% if (HasNode)
            { %>
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_SeverStatusLabel %>:
            </th>
            <td style="text-align:center">
                <img src="<%: NodeIcon %>" />
            </td>
            <td>
                 <%: String.Format(Resources.DPAWebContent.DatabaseInstanceNetObjectToolTip_ServerStatusFormat, NodeStatusInfo.ShortDescription.ToLower()) %>
            </td>
        </tr>
        <% } %>
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_TypeLabel %>:
            </th>
            <td colspan="2">
                <span automation="DatabaseInstanceType"><%: this.DatabaseInstanceType %></span>
                <span automation="DatabaseInstanceVersion"><%: this.DatabaseInstanceVersion %></span>
                <span automation="DatabaseInstanceVersionSuffix"><%: this.DatabaseInstanceVersionSuffix %></span>
            </td>
        </tr>
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_WaitTimeLabel %>:
            </th>
            <td style="text-align:center">
                <img src="<%: StatusIconsHelper.GetPerformanceIconFile(PerformanceOverview.WaitTimeAlarmLevel) %>" />
            </td>
            <td><%: WaitTimeDescription %></td>
        </tr> 
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_DpaServerLabel %>:
            </th>
            <td style="text-align:center">
                <img src="<%: DpaServerStatusIconUrl %>"/>
            </td>
            <td>
                <%: DpaServerName %>
            </td>
        </tr>
        <% if (CpuLoad != -2)
           { %>
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_CpuLoadLabel %>:
            </th>
            <td><span style="white-space: nowrap;"><npm:CPULoad runat="server" ID="CPULoadControl" /></span></td>
			<orion:InlineBar runat="server" ID="CPULoadBar" />
        </tr> 
        <% } %>  
        <% if (MemoryUsed != -2)
           { %>             
        <tr>
            <th>
                <%: Resources.DPAWebContent.DatabaseInstancesToolTip_MemoryUsedLabel %>:
            </th>
            <td><span style="white-space: nowrap;"><npm:MemoryUsed runat="server" ID="MemoryUsedControl" /></span></td>
			<orion:InlineBar runat="server" ID="MemoryUsedBar" />
        </tr> 
        <% } %>  
    </table>
    
    <% if (PerformanceOverview.WaitTimeAlarmLevel != AlarmLevel.Unknown)
        { %>      
    <div style="border-top:solid 1px #e2e2e2;margin-top:5px">
        <table>
            <tr>
                <th>
                    <%: Resources.DPAWebContent.DatabaseInstancesToolTip_PerformanceProblems %>:
                </th>
                <td></td>
                <td style="text-align: right;color:#b5b5b5"><%: Resources.DPAWebContent.DatabaseInstancesToolTip_PoweredByDPA %></td>
            </tr>
        </table>
        
        <table>
            <% foreach (string criticalPerformanceIssue in CriticalPerformanceProblems)
               { %>             
            <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceCritical %>" /></th><td style="padding:0"><%: criticalPerformanceIssue %></td></tr>
            <% } %>
            <% foreach (string warningPerformanceIssue in WarningPerformanceProblems)
               { %>             
            <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceWarning %>" /></th><td style="padding:0"><%: warningPerformanceIssue %></td></tr>
            <% } %>
            <% foreach (string unknownPerformanceIssue in UnknownPerformanceProblems)
               { %>             
            <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceUnknown %>" /></th><td style="padding:0"><%: unknownPerformanceIssue %></td></tr>
            <% } %>
            <% if (CriticalPerformanceProblems.Count == 0 && WarningPerformanceProblems.Count == 0 && UnknownPerformanceProblems.Count == 0)
               { %>
            <tr><th style="padding:0"><img src="<%: StatusIconsHelper.PerformanceNormal %>" /></th><td style="padding:0"><%: Resources.DPAWebContent.DatabaseInstancesToolTip_NoPerformanceIssue %></td></tr>
            <% } %>
        </table>
    </div>
    <% } %>
    <% } %>
</div>