﻿using System;
using System.Linq;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Helpers;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Exceptions;
using SolarWinds.Logging;
using SolarWinds.DPA.Web.Views;
using SolarWinds.DPA.Web.Providers;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;
using i18n = SolarWinds.DPA.Strings.Resources;

public partial class Orion_DPA_DatabaseInstanceDetails : DpaViewPage, IDpaDatabaseInstanceProvider
{
    private static readonly Log _log = new Log();

    public override string ViewType
    {
        get { return "DPA Database Instance Details"; }
    }

    public override string HelpFragment
    {
        get
        {
            return "";
        }
    }

    protected string PageTitle { get; set; }

    private IDPASettings _settings;
    private IDPASettings Settings
    {
        get
        {
            return _settings ?? (_settings = ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var master = Master as Orion_DPA_DPAView;
        if (master == null)
        {
            _log.ErrorFormat("Invalid master {0}", Master);
            throw new ApplicationException("Invalid master page.");
        }
        master.AllowDpaSettings = false;

        if (DatabaseInstance.Data != null && DatabaseInstance.Data.NodeId.HasValue)
            this.NodeLink.NodeID = string.Format("{0}:{1}", NetObjectPrefix.Node, DatabaseInstance.Data.NodeId.Value);
    }

    protected override void OnInit(EventArgs e)
    {
        if (Settings.IsIntegrationEstablished)
        {
            if (DatabaseInstance != null)
            {
                if (DatabaseInstance.ServerApplication != null 
                    && !string.IsNullOrEmpty(DatabaseInstance.ServerApplication.DetailsUrl) 
                    && Request.QueryString[APMConstants.DoNotRedirectToApplicationDetailPageQueryParam] == null)
                {
                    Response.Redirect(PageUrlHelper.AddSubViewKey(DatabaseInstance.ServerApplication.DetailsUrl, APMConstants.SubViewKeyUrl));
                }

                this.ResourceContainer2.DataSource = this.ViewInfo;
                this.ResourceContainer2.DataBind();
                base.OnInit(e);
            }
            else if (DatabaseInstanceNetObject.IsDpaNotAvailable)
            {
                var dpaServerDal = ServiceLocator.Current.GetInstance<IDpaServerDAL>();
                var dpaServerId = GlobalDatabaseInstanceIdHelper.GetDpaServerId(DatabaseInstanceNetObject.Id);
                var dpaServers = dpaServerDal.GetAll(new int[] {dpaServerId}, 1);
                var dpaServer = dpaServers.FirstOrDefault();
                var endpointNotReachableException = new EndpointNotReachableException(i18n.DatabaseInstanceDetail_ConnectivityIssue_SubTitle, dpaServer);

                throw endpointNotReachableException;
            }
            else
            {
                throw new ApplicationException(String.Format("Database Instance with NetObject '{0}' does not exist!",
                    DatabaseInstanceNetObject.NetObjectID));
            }
        }
        else
        {
            Response.Redirect(PageUrlHelper.ServerManagement, true);
        }
        
    }

    #region IDpaDatabaseInstanceProvider Members

    public DatabaseInstanceNetObject DatabaseInstanceNetObject
    {
        get { return (DatabaseInstanceNetObject)this.NetObject; }
    }

    protected DatabaseInstance DatabaseInstance
    {
        get { return DatabaseInstanceNetObject.Model; }
    }

    #endregion
}
