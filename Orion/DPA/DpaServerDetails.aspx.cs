﻿using System;
using System.Linq;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.DPA.Web.Providers;
using SolarWinds.DPA.Web.Views;

public partial class Orion_DPA_DpaServerDetails : DpaViewPage, IDpaServerProvider
{
    private IDPASettings _settings;
    private IDPASettings Settings
    {
        get
        {
            return _settings ?? (_settings = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }

    protected override void OnPreInit(EventArgs e)
    {
        if (Request.Url.PathAndQuery.IndexOf("NetObject", StringComparison.OrdinalIgnoreCase) < 0)
        {
            var dpaServer = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IDpaServerDAL>().GetAll().FirstOrDefault();
            Response.Redirect(dpaServer != null
                ? PageUrlHelper.GetDpaServerDetailsPage(dpaServer.DpaServerId)
                : PageUrlHelper.ServerManagement);
        }
        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        if (Settings.IsIntegrationEstablished)
        {
            if (DpaServer != null)
            {
                if (Settings.NumberOfIntegratedDpaServers == 1)
                {
                    this.ViewInfo.ViewTitle = SolarWinds.DPA.Strings.Resources.DatabasesTab_DatabasesSummaryTitle;
                }
                
                this.ResourceContainer2.DataSource = this.ViewInfo;
                this.ResourceContainer2.DataBind();
                base.OnInit(e);
            }
            else
            {
                throw new ApplicationException(String.Format("DPA Server with NetObject '{0}' does not exist!",
                    DpaServerNetObject.NetObjectID));
            }
        }
        else
        {
            Response.Redirect(PageUrlHelper.ServerManagement, true);
        }
    }

    public override
            string ViewType
    {
        get { return "DPA Server Details"; }
    }

    public DpaServerNetObject DpaServerNetObject
    {
        get { return (DpaServerNetObject) NetObject; }
    }

    protected DpaServer DpaServer
    {
        get { return DpaServerNetObject.Model; }
    }
}