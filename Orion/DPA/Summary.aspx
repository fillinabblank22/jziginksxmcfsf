<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/DPA/DPAView.master" CodeFile="Summary.aspx.cs" Inherits="Orion_DPA_Summary" 
    Title="<%$ Resources: DpaWebContent, SummaryView_Title %>" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="dpa" TagName="SimpleWelcomeWindow" Src="~/Orion/DPA/Controls/SimpleWelcomeWindow.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="DPAPageTitle">
    <h1><%=Page.Title %></h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="DPAMainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
		<orion:ResourceContainer runat="server" ID="resContainer" />
	</orion:ResourceHostControl>
    <dpa:SimpleWelcomeWindow runat="server" />
</asp:Content>
