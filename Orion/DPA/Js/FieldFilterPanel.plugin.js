﻿(function () {

    var monitorStatusValueInfos = {
        0: { displayName: '@{R=DPA.Strings;K=MonitorStatus_0_Running;E=js}' },
        1: { displayName: '@{R=DPA.Strings;K=MonitorStatus_1_Stopped;E=js}' },
        2: { displayName: '@{R=DPA.Strings;K=MonitorStatus_2_StartPending;E=js}' },
        3: { displayName: '@{R=DPA.Strings;K=MonitorStatus_3_StopPending;E=js}' },
        4: { displayName: '@{R=DPA.Strings;K=MonitorStatus_4_Disabled;E=js}' },
        5: { displayName: '@{R=DPA.Strings;K=MonitorStatus_5_BlackedOut;E=js}' },
        6: { displayName: '@{R=DPA.Strings;K=MonitorStatus_6_StartNoLicense;E=js}' },
        7: { displayName: '@{R=DPA.Strings;K=MonitorStatus_7_StopNoLicense;E=js}' }
    };

    var alarmLevelValueInfos = {
        0: { displayName: '@{R=DPA.Strings;K=AlarmLevel_0_Freeware;E=js}' },
        1: { displayName: '@{R=DPA.Strings;K=AlarmLevel_1_None;E=js}' },
        2: { displayName: '@{R=DPA.Strings;K=AlarmLevel_2_Normal;E=js}' },
        3: { displayName: '@{R=DPA.Strings;K=AlarmLevel_3_Unknown;E=js}' },
        4: { displayName: '@{R=DPA.Strings;K=AlarmLevel_4_Warning;E=js}' },
        5: { displayName: '@{R=DPA.Strings;K=AlarmLevel_5_Critical;E=js}' }
    };

    var appTypePresenters = {
        'DPA.DatabaseInstance.MonitorStatus': { valueInfo: monitorStatusValueInfos, iconClassPrefix: 'DPA_ValueIcon DPA_MonitorStatus-' },
        'DPA.AlarmLevel': { valueInfo: alarmLevelValueInfos, iconClassPrefix: 'DPA_ValueIcon DPA_AlarmLevel-' }
    };

    var renderStatusValue = function() {
        var rowTemplate = _.template('<tr class="value-row" data-valueid="{{id}}" data-isnull="0"><td class="value-cb"><input type="checkbox" value="{{id}}" {{checked}} /></td>'
            + '<td class="value-icon"><div class="{{iconClass}}"></div></td>'
            + '<td class="value-name">{{displayName}}</td>'
            + '<td class="value-count">{{count}}</td></tr>');

        function getValueInfo(appType, value) {
            return (appTypePresenters[appType])
                ? $.extend({}, appTypePresenters[appType].valueInfo[value], { iconClass: appTypePresenters[appType].iconClassPrefix + value })
                : undefined;
        }

        return function(keyid, prop, values) {
            values = values || prop.Values;
            var out = '';

            _.each(values, function(row) {
                var valueInfo = $.extend({}, getValueInfo(prop.ApplicationType, row.Value), {
                    id: row.Value,
                    checked: row.IsInUse ? 'checked' : '',
                    count: (row.Count === undefined || row.Count === null) ? '' : '(' + row.Count + ')'
                });

                out += rowTemplate(valueInfo);
            });

            if (values.length === 0) {
                out += '<td class="value-name"><span class="sw-ffp-novalue">' + options.labels.noObjects + '</span></td>';
            }

            return { content: out };
        }
    }();

    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "/Orion/DPA/Admin/styles/MonitorStatus.css"
    }).appendTo("head");

    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "/Orion/DPA/styles/FieldFilterPanel.css"
    }).appendTo("head");

    $.each(appTypePresenters, function(key) {
        RegisterRenderer(renderStatusValue, key);
    });
})();