﻿SW.Core.namespace("DPA.UI.DatabaseInstancesGrid.ColumnSettingsFactory");

(function (ctx) {
    var i = 0;
    var localizedStrings = {
        databaseInstancesColumnHeaderSingular: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Singular;E=js}',
        databaseInstancesColumnHeaderPlural: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Plural;E=js}',
        waitTimeColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_WaitTime;E=js}',
        addDbInDPA_NoAdminLabel: '@{R=DPA.Strings;K=DatabaseInstancesResource_AddDbInDPA_NoAdminLabel;E=js}',
        queryColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Query;E=js}',
        cpuColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_CPU;E=js}',
        memoryColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Memory;E=js}',
        diskColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Disk;E=js}',
        sessionColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Session;E=js}',
        addDbInDPA_Text: '@{R=DPA.Strings;K=DatabaseInstancesResource_AddDbInstanceLinkLabel;E=js}' + " &raquo;",
        notMonitoredByDpaMessage: '@{R=DPA.Strings;K=DatabaseInstances_NotMonitoredByDpaMessage;E=js}',
        supRowDpaServerLabel: '@{R=DPA.Strings;K=DatabaseInstances_SupRowDpaServerLabel;E=js}',
        supRowType: '@{R=DPA.Strings;K=DatabaseInstances_SupRowType;E=js}'
    };
    var tableIndex = {
        GlobalDatabaseInstanceId: i++,
        DbInstanceIcon: i++,
        DbInstanceLink: i++,
        DbInstanceName: i++,
        NodeID: i++,
        NodeLink: i++,
        NodeName: i++,
        NodeIcon: i++,
        HasNode: i++,
        IsInDPA: i++,
        IsLicensed: i++,
        DpaServerId: i++,
        ApplicationId: i++,
        WaitTimePerformanceStatusIcon: i++,
        WaitTimePerformanceStatusToolTip: i++,
        QueryPerformanceStatusIcon: i++,
        QueryPerformanceStatusToolTip: i++,
        CPUPerformanceStatusIcon: i++,
        CPUPerformanceStatusToolTip: i++,
        MemoryPerformanceStatusIcon: i++,
        MemoryPerformanceStatusToolTip: i++,
        DiskPerformanceStatusIcon: i++,
        DiskPerformanceStatusToolTip: i++,
        SessionPerformanceStatusIcon: i++,
        SessionPerformanceStatusToolTip: i++,
        DatabaseInstanceType: i++,
        DpaServerName: i++
    };

    ctx.createColumns = function(config) {
        return {
            'DbInstanceIcon': {
                caption: '',
                isHtml: true,
                formatter: function (cellValue, rowArray) {
                    var content = SW.Core.String.Format('<a href="{0}"><img src="{1}" /></a>',
                        rowArray[tableIndex.DbInstanceLink],
                        rowArray[tableIndex.DbInstanceIcon]);

                    return content;
                }
            },
            'DbInstanceName': {
                caption: localizedStrings.databaseInstancesColumnHeaderSingular,
                isHtml: true,
                formatter: function (cellValue, rowArray) {
                    var content = SW.DPA.Common.FormatDatabaseInstanceLink(rowArray[tableIndex.DbInstanceLink], undefined, rowArray[tableIndex.DbInstanceName],
                        rowArray[tableIndex.NodeLink], rowArray[tableIndex.NodeIcon], rowArray[tableIndex.NodeName], config.isNodeFunctionalityAllowed);

                    if (config.supplementaryRow.visible) {
                        if (rowArray[tableIndex.GlobalDatabaseInstanceId] !== -1) {
                            content = "<div>" + content + SW.Core.String.Format(
								"</div><div class='DPA_DatabaseInstancesSuplRow' automation='DatabaseInstancesSuplRow'><div class='DPA_SupRowPart' automation='SuplRowDatabaseInstanceType'><span>{0}:</span> <span>{1}</span></div>",
                                localizedStrings.supRowType, rowArray[tableIndex.DatabaseInstanceType]);

                            var dpaServerName = rowArray[tableIndex.DpaServerName];
                            if (config.supplementaryRow.showDpaLabel && dpaServerName !== '') {
                                content += SW.Core.String.Format("<div class='DPA_SupRowPart' automation='SuplRowDpaServer'><span>{0}:</span> <span>{1}</span></div>",
                                    localizedStrings.supRowDpaServerLabel,
                                    dpaServerName);
                            }

                            content += "</div>";
                        } else {
                            content += SW.Core.String.Format("<div class='DPA_DatabaseInstancesSuplRow' automation='ApplicationSuplRow'><div class='DPA_SupRowPart'>{0}</div></div>", localizedStrings.notMonitoredByDpaMessage);
                        }
                    }

                    return content;
                },
                headerFormatter: function (totalRows) {
                    if (totalRows > 1) {
                        return SW.Core.String.Format(localizedStrings.databaseInstancesColumnHeaderPlural, totalRows);
                    } else {
                        return localizedStrings.databaseInstancesColumnHeaderSingular;
                    }
                }
            },
            "WaitTimePerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.waitTimeColumnHeader,
                isHtml: true,
                cellAttributesProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? { "colspan": "6" } : {};
                },
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] ? "DPA_AddDbInstanceLink" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (!config.hideKpis && rowArray[tableIndex.IsInDPA]) {
                        return SW.DPA.Common.FormatKpiLink(
                            rowArray[tableIndex.DbInstanceLink],
                            rowArray[tableIndex.WaitTimePerformanceStatusToolTip],
                            rowArray[tableIndex.WaitTimePerformanceStatusIcon],
                            config.kpiTargets.waitTimePerformanceStatusIconTarget,
                            rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    } else if (!rowArray[tableIndex.IsInDPA]) {
                        if (SW.Core.Info.AllowAdmin) {
                            return SW.Core.String.Format('<a href="{0}" class="DPA_AddDbInstanceLink">{1}</a>',
                                SW.DPA.Common.assignDatabaseInstanceUrl(rowArray[tableIndex.ApplicationId], rowArray[tableIndex.NodeID]),
                                localizedStrings.addDbInDPA_Text);
                        } else {
                            return SW.Core.String.Format('<span class="DPA_NoAdminLabel">{0}</span>', localizedStrings.addDbInDPA_NoAdminLabel);
                        }
                    }
                }
            },
            "QueryPerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.queryColumnHeader,
                isHtml: true,
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? "DPA_Hide" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (config.hideKpis || !rowArray[tableIndex.IsInDPA]) {
                        return "";
                    } else {
                        return SW.DPA.Common.FormatKpiLink(
                            rowArray[tableIndex.DbInstanceLink],
                            rowArray[tableIndex.QueryPerformanceStatusToolTip],
                            rowArray[tableIndex.QueryPerformanceStatusIcon],
                            config.kpiTargets.queryPerformanceStatusIconTarget,
                            rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    }
                }
            },
            "CPUPerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.cpuColumnHeader,
                isHtml: true,
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? "DPA_Hide" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (config.hideKpis || !rowArray[tableIndex.IsInDPA]) {
                        return "";
                    } else {
                        return SW.DPA.Common.FormatKpiLink(
                            rowArray[tableIndex.DbInstanceLink],
                            rowArray[tableIndex.CPUPerformanceStatusToolTip],
                            rowArray[tableIndex.CPUPerformanceStatusIcon],
                            config.kpiTargets.cpuPerformanceStatusIconTarget,
                            rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    }
                }
            },
            "MemoryPerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.memoryColumnHeader,
                isHtml: true,
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? "DPA_Hide" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (config.hideKpis || !rowArray[tableIndex.IsInDPA]) {
                        return "";
                    } else {
                        return SW.DPA.Common.FormatKpiLink(
                        rowArray[tableIndex.DbInstanceLink],
                        rowArray[tableIndex.MemoryPerformanceStatusToolTip],
                        rowArray[tableIndex.MemoryPerformanceStatusIcon],
                        config.kpiTargets.memoryPerformanceStatusIconTarget,
                        rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    }
                }
            },
            "DiskPerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.diskColumnHeader,
                isHtml: true,
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? "DPA_Hide" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (config.hideKpis || !rowArray[tableIndex.IsInDPA]) {
                        return "";
                    } else {
                        return SW.DPA.Common.FormatKpiLink(
                        rowArray[tableIndex.DbInstanceLink],
                        rowArray[tableIndex.DiskPerformanceStatusToolTip],
                        rowArray[tableIndex.DiskPerformanceStatusIcon],
                        config.kpiTargets.diskPerformanceStatusIconTarget,
                        rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    }
                }
            },
            "SessionPerformanceStatusIcon": {
                caption: config.hideKpis ? '' : localizedStrings.sessionColumnHeader,
                isHtml: true,
                cellCssClassProvider: function (cellValue, rowArray, cellInfo) {
                    return !rowArray[tableIndex.IsInDPA] || config.hideKpis ? "DPA_Hide" : "";
                },
                formatter: function (cellValue, rowArray) {
                    if (config.hideKpis || !rowArray[tableIndex.IsInDPA]) {
                        return "";
                    } else {
                        return SW.DPA.Common.FormatKpiLink(
                        rowArray[tableIndex.DbInstanceLink],
                        rowArray[tableIndex.SessionPerformanceStatusToolTip],
                        rowArray[tableIndex.SessionPerformanceStatusIcon],
                        config.kpiTargets.sessionPerformanceStatusIconTarget,
                        rowArray[tableIndex.GlobalDatabaseInstanceId]);
                    }
                }
            }
        };
    };
})(DPA.UI.DatabaseInstancesGrid.ColumnSettingsFactory);