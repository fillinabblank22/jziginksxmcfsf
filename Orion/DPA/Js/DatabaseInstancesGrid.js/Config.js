﻿SW.Core.namespace("DPA.UI.DatabaseInstancesGrid.Config");

DPA.UI.DatabaseInstancesGrid.Config = function () {
    var localizedStrings = {
        noDataAvailable: '@{R=DPA.Strings;K=SortablePageableTable_NoDataMessage_Text;E=js}'
    };

    return {
        isNodeFunctionalityAllowed: false,
        hideKpis: true,
        uniqueClientId: 1,
        supplementaryRow: {
            visible: false,
            showDpaLabel: false
        },
        controls: {},
        kpiTargets: {
            waitTimePerformanceStatusIconTarget: 'waittimeIconTarget',
            queryPerformanceStatusIconTarget: 'queryIconTarget',
            cpuPerformanceStatusIconTarget: 'cpuIconTarget',
            memoryPerformanceStatusIconTarget: 'memoryIconTarget',
            diskPerformanceStatusIconTarget: 'diskIconTarget',
            sessionPerformanceStatusIconTarget: 'sessionIconTarget'
        },
        url: '',
        getParameters: function () {
            return {};
        },
        onSuccess: function (response) {
            return true;
        },
        noDataMessage: {
            show: true,
            isHtml: true,
            formatter: function () {
                return localizedStrings.noDataAvailable;
            },
            cssClassProvider: function () {
                return "DPA_SortablePageableTableNoData";
            }
        }
    }
};
