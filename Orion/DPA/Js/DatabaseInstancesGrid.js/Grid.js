﻿SW.Core.namespace("DPA.UI.DatabaseInstancesGrid.Grid");

DPA.UI.DatabaseInstancesGrid.Grid = function ($elementToAppend, config) {
    config = _.extend(DPA.UI.DatabaseInstancesGrid.Config(), config);

    var localizedStrings = {
        loadingIconNotificationTextOfLink: '@{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
        loadingIconNotificationText: '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js}',
        connectivityIssueNotificationText: '@{R=DPA.Strings;K=ConnectivityIssue_Message;E=js}',
    };

    var template = _.templateExt([
        // loading icon
        '{# if(typeof controls.loadingIcon === "undefined") { #}',
        '<div id="LoadingIcon-{{uniqueClientId}}" style="display:none"><span class="DPA_LoadingIcon"></span>',
        '<p class="DPA_LoadingLongerText" style="display:none">{{{loadingIconNotificationText}}}',
        '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">&#187; {{{loadingIconNotificationTextOfLink}}}</a>',
        '</p></div>',
        '{# } #}',
        // error message
        '{# if(typeof controls.errorMessage === "undefined") { #}',
        '<div id="ErrorMessage-{{uniqueClientId}}" class="sw-suggestion sw-suggestion-fail DPA_ResourceNotification" style="display:none"><span class="sw-suggestion-icon"></span><span class="DPA_Message"></span></div>',
        '{# } #}',
        // connectivity issue warning
        '{# if(typeof controls.connectivityIssueWarning === "undefined") { #}',
        '<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn" id="ConnectivityIssue-{{uniqueClientId}}" style="display:none">',
            '<span class="sw-suggestion-icon"></span><span class="DPA_DismissButton" onclick="document.getElementById(\'ConnectivityIssue-{{uniqueClientId}}\').style.display = \'none\';"></span>',
            '<div class="DPA_Body">',
                '<p>{{connectivityIssueNotificationText}}:</p>',
                '<div class="DPA_PartialErrors"></div>',
                '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">{{{loadingIconNotificationTextOfLink}}}</a>',
            '</div>',
        '</div>',
        '{# } #}',
        // pageable table
        '<div id="ErrorMsg-{{uniqueClientId}}"></div>',
        '<table id="Grid-{{uniqueClientId}}" class="sw-custom-query-table DPA_NeedsZebraStripes" cellpadding="2" cellspacing="0" width="100%">',
            '<tr class="HeaderRow"></tr>',
        '</table>',
        '<div id="Pager-{{uniqueClientId}}" class="ReportFooter ResourcePagerControl hidden"></div>',
        '<input type="hidden" id="OrderBy-{{uniqueClientId}}" value="" />'
    ]);
    
    this.init = function(initializedCallback) {
        $elementToAppend.empty();
        $elementToAppend.append(template($.extend(localizedStrings, config)));
        var loadingIcon, errorMessage, connectivityIssueWarning;

        if (typeof config.controls.loadingIcon === 'undefined')
            loadingIcon = new SW.DPA.UI.LoadingIcon("LoadingIcon-" + config.uniqueClientId);
        else
            loadingIcon = config.controls.loadingIcon;

        if (typeof config.controls.errorMessage === 'undefined')
            errorMessage = new SW.DPA.UI.ErrorMessage("ErrorMessage-" + config.uniqueClientId);
        else
            errorMessage = config.controls.errorMessage;

        if (typeof config.controls.connectivityIssueWarning === 'undefined')
            connectivityIssueWarning = new SW.DPA.UI
                .ConnectivityIssueWarning("ConnectivityIssue-" + config.uniqueClientId);
        else
            connectivityIssueWarning = config.controls.connectivityIssueWarning;

        SW.Core.Services.callController("/api/DpaDatabaseInstancesSettings/ShowKpis",
            {},
            function (response) {
                config.hideKpis = !(response);
                SW.DPA.UI.SortablePageableTable.initialize({
                    uniqueId: config.uniqueClientId,
                    pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                        loadingIcon.show();
                        connectivityIssueWarning.hide();
                        errorMessage.hide();

                        SW.Core.Services.callController(config.url,
                            $.extend({
                                PageIndex: pageIndex,
                                Limit: pageSize
                            }, config.getParameters()),
                            function (result) {
                                loadingIcon.hide();
                                if (typeof result.Metadata.PartialResultErrors !== 'undefined' && result.Metadata.PartialResultErrors.length > 0) {
                                    connectivityIssueWarning.show(result.Metadata.PartialResultErrors);
                                }

                                if (config.onSuccess(result))
                                    onSuccess(result);
                            },
                            function (error) {
                                loadingIcon.hide();
                                errorMessage.show(error);
                                if (SW.DPA.Common.isFn(error))
                                    onError(error);
                            });
                    },
                    initialPageIndex: 0,
                    rowsPerPage: 10,
                    noDataMessage: config.noDataMessage,
                    columnSettings: DPA.UI.DatabaseInstancesGrid.ColumnSettingsFactory.createColumns(config)
                });

                initializedCallback();
            });
    }

    this.refresh = function() {
        SW.DPA.UI.SortablePageableTable.refresh(config.uniqueClientId);
    }
}
