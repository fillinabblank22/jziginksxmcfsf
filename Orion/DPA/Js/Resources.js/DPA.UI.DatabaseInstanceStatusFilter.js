﻿SW.Core.namespace("SW.DPA.UI.DatabaseInstanceStatusFilter");

SW.DPA.UI.DatabaseInstanceStatusFilter = function ($tableWrapper, refreshEvent) {
    var criticalBoxElement = $tableWrapper.find('.DPA_StatusFilterCritical');
    var warningBoxElement = $tableWrapper.find('.DPA_StatusFilterWarning');
    var normalBoxElement = $tableWrapper.find('.DPA_StatusFilterNormal');
    var unknownBoxElement = $tableWrapper.find('.DPA_StatusFilterUnknown');

    var handleButtonClick = function ($buttonElement) {
        if ($buttonElement.hasClass('DPA_StatusFilterItemEnabled')) {
            if ($buttonElement.hasClass('DPA_StatusFilterItemSelected')) {
                $buttonElement.removeClass('DPA_StatusFilterItemSelected');
            } else {
                $buttonElement.addClass('DPA_StatusFilterItemSelected');
            }

            refreshEvent();
        }
    }

    var isButtonSelected = function($buttonElement) {
        return $buttonElement.hasClass('DPA_StatusFilterItemSelected');
    }

    this.init = function(criticalCount, warningCount, normalCount, unknownCount) {
        criticalBoxElement.find('.DPA_StatusFilterItemContent').html(criticalCount);
        if (criticalCount > 0) {
            criticalBoxElement.addClass('DPA_StatusFilterItemEnabled');
        } else {
            criticalBoxElement.removeClass('DPA_StatusFilterItemEnabled');
        }

        warningBoxElement.find('.DPA_StatusFilterItemContent').html(warningCount);
        if (warningCount > 0) {
            warningBoxElement.addClass('DPA_StatusFilterItemEnabled');
        } else {
            warningBoxElement.removeClass('DPA_StatusFilterItemEnabled');
        }

        normalBoxElement.find('.DPA_StatusFilterItemContent').html(normalCount);
        if (normalCount > 0) {
            normalBoxElement.addClass('DPA_StatusFilterItemEnabled');
        } else {
            normalBoxElement.removeClass('DPA_StatusFilterItemEnabled');
        }

        unknownBoxElement.find('.DPA_StatusFilterItemContent').html(unknownCount);
        if (unknownCount > 0) {
            unknownBoxElement.addClass('DPA_StatusFilterItemEnabled');
        } else {
            unknownBoxElement.removeClass('DPA_StatusFilterItemEnabled');
        }
    }

    this.isCriticalSelected = function() {
        return isButtonSelected(criticalBoxElement);
    }

    this.isWarningSelected = function () {
        return isButtonSelected(warningBoxElement);
    }

    this.isNormalSelected = function () {
        return isButtonSelected(normalBoxElement);
    }

    this.isUnknownSelected = function () {
        return isButtonSelected(unknownBoxElement);
    }

    this.isAnyFilterSelected = function() {
        return this.isCriticalSelected() ||
            this.isWarningSelected() ||
            this.isNormalSelected() ||
            this.isUnknownSelected();
    }

    criticalBoxElement.click(function () {
        handleButtonClick(criticalBoxElement);
    });

    warningBoxElement.click(function () {
        handleButtonClick(warningBoxElement);
    });

    normalBoxElement.click(function () {
        handleButtonClick(normalBoxElement);
    });

    unknownBoxElement.click(function () {
        handleButtonClick(unknownBoxElement);
    });
}
