﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.SqlDetailContent = function ($elementToAppend, globalDatabaseInstanceId, sqlHash, sqlQuery) {
    var uniqueClientId = "sqlDetailsDialog";
    var localizedStrings = {
        sqlTextTitle: '@{R=DPA.Strings;K=SqlDialog_SqlTextTitle;E=js}',
        advisorsTitle: '@{R=DPA.Strings;K=SqlDialog_AdvisorsTitle;E=js}',
        loadingIconNotificationText: '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js}',
        loadingIconNotificationTextOfLink: '@{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
        connectivityIssueNotificationText: '@{R=DPA.Strings;K=ConnectivityIssue_Message;E=js}',
        totalResponseTimeTitle: '@{R=DPA.Strings;K=SqlDialog_TotalResponseTimeTitle;E=js}',
        executionsTitle: '@{R=DPA.Strings;K=SqlDialog_ExecutionsTitle;E=js}',
        noAdvisorMessage: '@{R=DPA.Strings;K=SqlDialog_NoAdvisorsMessage;E=js}',
        readMoreSuffix: '@{R=DPA.Strings;K=SqlDetailDialog_ReadMoreLabel;E=js}'
    };

    var dialogWrapperContentTemplate = _.templateExt([
        '<div class="DPA_SqlDetailsContent">',
            // error message
            '<div id="ErrorMessage-{{UniqueClientID}}" class="sw-suggestion sw-suggestion-fail DPA_ResourceNotification" style="display:none"><span class="sw-suggestion-icon"></span><span class="DPA_Message"></span></div>',
            // connectivity issue warning
            '<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn" id="ConnectivityIssue-{{UniqueClientID}}" style="display:none">',
                '<span class="sw-suggestion-icon"></span><span class="DPA_DismissButton" onclick="document.getElementById(\'ConnectivityIssue-{{UniqueClientID}}\').style.display = \'none\';"></span>',
                '<div class="DPA_Body">',
                    '<p>{{connectivityIssueNotificationText}}:</p>',
                    '<div class="DPA_PartialErrors"></div>',
                    '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">{{{loadingIconNotificationTextOfLink}}}</a>',
                '</div>',
            '</div>',
            // loading icon
            '<div id="LoadingIcon-{{UniqueClientID}}" style="display:none"><span class="DPA_LoadingIcon"></span>',
            '<p class="DPA_LoadingLongerText" style="display:none">{{{loadingIconNotificationText}}}',
            '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">&#187; {{{loadingIconNotificationTextOfLink}}}</a>',
            '</p></div>',
            '<div class="DPA_AdvisorsSection">',
            '</div>',
            '<div class="DPA_ChartsSection" style="display:none">',
                '<h3>{{totalResponseTimeTitle}}</h3>',
                '<div id="DPA_SqlDetail_TotalResponseTimeChartWrapper"></div>',
                '<h3>{{executionsTitle}}</h3>',
                '<div id="DPA_SqlDetail_ExecutionsChartWrapper"></div>',
            '</div>',
            '<div class="DPA_DetailsSection">',
            '</div>',
        '</div>'
    ]);

    var advisorsTemplate = _.templateExt([
        '<div class="DPA_SqlDetailSection">',
        '<h3>{{advisorsTitle}}</h3>',
        '<table class="sw-custom-query-table DPA_NeedsZebraStripes" cellpadding="2" cellspacing="2" width="100%">',
        '<tbody>',
        '{# if(SqlAdvisors && SqlAdvisors.length > 0) { #}',
            '{# _.each(SqlAdvisors, function(advisor) { #}',
            '<tr><td><img src="{{advisor.IconUrl}}"/></td><td><a href="{{advisor.DetailUrl}}" target="_blank">{{advisor.Description}}</a> <a href="{{advisor.DetailUrl}}" class="DPA_Link" target="_blank">{{readMoreSuffix}}</a></td></tr>',
            '{# }); #}',
        '{# } else { #}',
        '<tr>{{noAdvisorMessage}}</tr>',
        '{# } #}',
        '</tbody>',
        '</table>',
        '</div>'
    ]);

    var sqlTextSectionTemplate = _.templateExt([
        '{# if(SqlText != null) { #}',
        '<div class="DPA_SqlDetailSection">',
            '<h3>{{sqlTextTitle}}</h3>',
            '<div class="DPA_SqlTextContent sql xui-code hljs groovy">{{SqlText}}</div>',
        '</div>',
        '{# } #}']);

    $elementToAppend.empty();

    var templatedData = $.extend({ UniqueClientID: uniqueClientId }, localizedStrings);
    $elementToAppend.append(dialogWrapperContentTemplate(templatedData));
    var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('ConnectivityIssue-' + uniqueClientId);
    var errorMessage = new SW.DPA.UI.ErrorMessage('ErrorMessage-' + uniqueClientId);
    var loadingIcon = new SW.DPA.UI.LoadingIcon("LoadingIcon-" + uniqueClientId);
    var requestData = { GlobalDatabaseInstanceId: globalDatabaseInstanceId, SqlHash: sqlHash };

    loadingIcon.show();
    var loaded = 0;
    SW.Core.Services.callController('/api/DpaSqlDetail/GetDataSeries', requestData,
        function (data) {
            loaded++;
            if (loaded === 2)
                loadingIcon.hide();

            $elementToAppend.find('.DPA_ChartsSection').show();
            if (data && data.length > 0) {
                var chart = new SW.DPA.UI.Charts.SqlDetailTotalResponseTimeCharts();
                chart.createTotalResponseTimeChart('DPA_SqlDetail_TotalResponseTimeChartWrapper',
                    data[0]);

                chart.createExecutionsTimeChart('DPA_SqlDetail_ExecutionsChartWrapper',
                    data[1]);
            }
        },
        function (errorResult) {
            loadingIcon.hide();
            errorMessage.show(errorResult);
        });
    
    SW.Core.Services.callController('/api/DpaAdvisors/GetAdvisorsForSqlHash', requestData,
        function (responseData) {
            if (responseData) {
                loaded++;
                if(loaded === 2)
                    loadingIcon.hide();

                if (responseData.PartialErrors === null || responseData.PartialErrors.length === 0) {
                    var viewModel = $.extend({
                            UniqueClientID: uniqueClientId,
                            SqlText: unescape(sqlQuery).trim(),
                            SqlAdvisors: responseData.Data
                        }, localizedStrings);
                    $elementToAppend.find('.DPA_AdvisorsSection').append(advisorsTemplate(viewModel));
                    $elementToAppend.find('.DPA_DetailsSection').append(sqlTextSectionTemplate(viewModel));

                    SW.Core.Services.callController('/api/DpaSqlDetail/GetSqlQueryInfo', requestData,
                        function (data) {
                            var sqlTextContentElement = $('div.DPA_SqlTextContent');
                            if (data) {
                                sqlTextContentElement.text(data.SqlText);
                            }
                            sqlTextContentElement.each(function (i, block) {
                                hljs.highlightBlock(block);
                            });
                        });
                } else {                   
                    connectivityIssueWarning.show(responseData.PartialErrors);
                }
            }
        },
        function (errorResult) {
            loadingIcon.hide();
            errorMessage.show(errorResult);
        });
};
