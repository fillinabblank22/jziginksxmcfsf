﻿SW.Core.namespace("SW.DPA.UI.SqlDetailDialog");

(function(ctx) {
    var localizedStrings = {
        viewInDpaButtonLabel: '@{R=DPA.Strings;K=SqlDialog_ViewInDpaButtonLabel;E=js}',
        closeButtonLabel: '@{R=DPA.Strings;K=SqlDialog_CloseButtonLabel;E=js}',
        loadingLabel: '@{R=DPA.Strings;K=SqlDialog_LoadingText;E=js}',
        dialogTitle: '@{R=DPA.Strings;K=SqlDialog_DialogTitle;E=js}'
    };

    var dialogTemplate = _.templateExt([
     '<div class=DPA_SqlDetailsDialog>',
         '<div class="DPA_SqlDetailsDialogContent">',
             '{{content}}',
         '</div>',
         '<div class="DPA_Buttons"><br/></div>',
     '</div>']);

    ctx.open = function (globalDatabaseInstanceId, sqlHash, sqlQuery, sqlName) {
        var dialogContent = $(dialogTemplate({ content: localizedStrings.loadingLabel }));

        dialogContent.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedStrings.viewInDpaButtonLabel,
            { type: 'primary' })).attr('href', '/api/DpaLinksToDpaj/SqlQueryDetails?dbi=' + globalDatabaseInstanceId + '&hash=' + sqlHash).attr('target', '_blank'));

        dialogContent.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedStrings.closeButtonLabel,
            { type: 'secondary' })).click(function () { dialogContent.dialog('close'); }));

        dialogContent.dialog({
            modal: true,
            minWidth: 700,
            minHeight: 200,
            resizable: false,
            dialogClass: 'automation_SqlDialog',
            title: localizedStrings.dialogTitle + ' ' + unescape(sqlName),
            open: function () {
                $(this).dialog("option", "position", ['center', 10]);
            },
            close: function () { dialogContent.remove(); }
        });

        SW.DPA.UI.SqlDetailContent(dialogContent.find('.DPA_SqlDetailsDialogContent'), globalDatabaseInstanceId, sqlHash, sqlQuery);
    };

})(SW.DPA.UI.SqlDetailDialog);
