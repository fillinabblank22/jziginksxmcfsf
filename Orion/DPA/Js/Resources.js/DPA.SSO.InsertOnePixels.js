﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.SSO = SW.DPA.SSO || {};
SW.DPA.SSO.InsertOnePixels = function () {
    var localizedTexts = {
        errorMessageParagraph1: '@{R=DPA.Strings;K=SSOErrorMessageParagraph1;E=js}',
        errorMessageParagraph2LinkText: '@{R=DPA.Strings;K=SSOErrorMessageParagraph2LinkText;E=js}',
        errorMessageParagraph2Text: '@{R=DPA.Strings;K=SSOErrorMessageParagraph2Text;E=js}',
        errorMessageParagraph3: '@{R=DPA.Strings;K=SSOErrorMessageParagraph3;E=js}',
        errorMessageTryAgainButtonLabel: '@{R=DPA.Strings;K=SSOErrorMessageButtonLabel;E=js}',
        errorMessageTitle: '@{R=DPA.Strings;K=SSOErrorMessageTitle;E=js}',
        errorMessageIgnoreButtonLabel: '@{R=DPA.Strings;K=SSOErrorMessageIgnoreButtonLabel;E=js}'
    };
    var cookieKey = "SW_DPA_SSOWindowIgnored";
    var dpaServerHttpsEnabledCookieKey = function(dpaServerId) {
        return "SW_DPA_EnabledHttps" + dpaServerId;
    };

    var windowTemplate = _.templateExt([
         '<div id="DPA_SsoErrorWindow">',
             '<span class="DPA_ErrorImage"></span>',
             '<div class="DPA_Texts">',
                 '<p>{{{paragraph1}}}</p>',
                 '<ul>',
                 '{# _.each(dpaServers, function(dpaServer) { #}',
                 '<li><a href="/api/DpaLinksToDpaj/MainDashboard/{{dpaServer.DpaServerId}}" target="_blank">{{{dpaServer.DisplayName}}}</a></li>',
                 '{# }); #}',
                 '</ul>',
                 '<p>{{{paragraph2LinkLabel}}} {{{paragraph2Text}}}</p>',
                 '<p><a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">{{{paragraph3}}}</a></p>',
             '</div>',
             '<div class="DPA_Buttons"><br/></div>',
         '</div>']);

    var thisObject = this;
    var maxCountOfImageInsert = 3;
    var dpaSsoImagesData = [];

    var showErrorWindow = function (dpaServerIdsWithFailedSso) {
        var isIgnored = SW.DPA.Common.getCookie(cookieKey);
        if (isIgnored == "" || isIgnored != 1) {
            getDpaServers(dpaServerIdsWithFailedSso, function (response) {
                var viewModel = {
                    paragraph1: localizedTexts.errorMessageParagraph1,
                    paragraph2LinkLabel: localizedTexts.errorMessageParagraph2LinkText,
                    paragraph2Text: localizedTexts.errorMessageParagraph2Text,
                    paragraph3: localizedTexts.errorMessageParagraph3,
                    dpaServers: response
                };

                var window = $(windowTemplate(viewModel));

                window.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedTexts.errorMessageTryAgainButtonLabel, { type: 'primary' })).click(function () { location.reload(); }));
                window.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedTexts.errorMessageIgnoreButtonLabel, { type: 'secondary' })).click(function () {
                    window.dialog('close');
                    SW.DPA.Common.setCookie(cookieKey, '1', 365);
                }));

                window.dialog({ modal: true, minWidth: 400, minHeight: 100, zIndex: 100000, resizable: false, title: localizedTexts.errorMessageTitle });
            });
        }
    };

    var showErrorWindowIfLoadingFinished = function () {
        if (_.filter(dpaSsoImagesData,
        function (data) {
            return data.loadingFinished;
        }).length === dpaSsoImagesData.length) {

            var failedImages = _.filter(dpaSsoImagesData,
                function (data) {
                    return (data.httpEnabled ? !data.https.loaded && !data.http.loaded : !data.https.loaded);
                });

            var failedDpaServerIds = _.map(failedImages, function (failedImage) {
                return failedImage.dpaServerId;
            });

            if (failedDpaServerIds.length > 0) {
                showErrorWindow(failedDpaServerIds);
            }
        }
    };

    var getDpaServers = function (dpaServerIds, loadedCallback) {
        SW.Core.Services.callController('/api/DpaServers/GetAll', { DpaServerIds: dpaServerIds }, function (response) { loadedCallback(response); }, function () { });
    };

    var getDpaServerIdFromProviderName = function (providerName) {
        var match = providerName.match(/DPA\_([0-9]+)/);
        if (match.length > 1) {
            return parseInt(match[1]);
        } else {
            throw "Not supported format of DPA provider name '" + providerName + "'";
        }
    };

    var isDpaProvider = function (providerName) {
        return providerName.startsWith('DPA');
    }

    this.insertDpaImage = function (dpaSsoImageData) {
        var targetUrl = "";
        var loadingHttps = false;
        if (dpaSsoImageData.https.loadFailedCount < maxCountOfImageInsert) {
            loadingHttps = true;
            targetUrl = dpaSsoImageData.https.url;
        } else if (typeof dpaSsoImageData.http !== "undefined") {
            loadingHttps = false;
            targetUrl = dpaSsoImageData.http.url;
        }

        if (targetUrl !== "") {
            var linkToImageWithTimestamp = targetUrl + "&ts=" + (new Date().getTime().toString());
            var image = new Image();

            // create image again if it fails to load
            image.onerror = function () {
                if (loadingHttps) {
                    dpaSsoImageData.https.loadFailedCount++;
                    if (dpaSsoImageData.https.loadFailedCount >= maxCountOfImageInsert &&
                        !dpaSsoImageData.httpEnabled) {
                        dpaSsoImageData.loadingFinished = true;
  
                        showErrorWindowIfLoadingFinished();
                    } else {
                        thisObject.insertDpaImage(dpaSsoImageData);
                    }
                } else {
                    dpaSsoImageData.http.loadFailedCount++;
                    if (dpaSsoImageData.http.loadFailedCount >= maxCountOfImageInsert) {
                        dpaSsoImageData.loadingFinished = true;

                        showErrorWindowIfLoadingFinished();
                    } else {
                        thisObject.insertDpaImage(dpaSsoImageData);
                    }
                }
            };

            image.onload = function () {
                if (loadingHttps) {
                    SW.DPA.Common.setCookie(dpaServerHttpsEnabledCookieKey(dpaSsoImageData.dpaServerId), 1, 1);
                    dpaSsoImageData.https.loaded = true;
                } else {
                    SW.DPA.Common.setCookie(dpaServerHttpsEnabledCookieKey(dpaSsoImageData.dpaServerId), 0, 1);
                    dpaSsoImageData.http.loaded = true;
                }
                dpaSsoImageData.loadingFinished = true;
            };

            image.src = linkToImageWithTimestamp;
        }
    }

    this.insertImage = function(imageUrl) {
        var image = new Image();
        image.src = imageUrl;
    }
    
    $(document).ready(function() {
        SW.Core.Services.callController(
            "/sapi/DpaSSO/GetOnePixelUrls",
            {},
            function (response) {
                for (var propertyName in response) {
                    if (isDpaProvider(propertyName)) {
                        var dpaSsoImageData = {
                            dpaServerId: getDpaServerIdFromProviderName(propertyName),
                            httpEnabled: false,
                            loadingFinished: false,
                            https: {
                                url: response[propertyName][0],
                                loadFailedCount: 0,
                                loaded: false
                            }
                        };

                        if (response[propertyName].length > 1) {
                            dpaSsoImageData.http = {
                                url: response[propertyName][1],
                                loadFailedCount: 0,
                                loaded: false
                            };
                            dpaSsoImageData.httpEnabled = true;
                        }

                        dpaSsoImagesData.push(dpaSsoImageData);
                        thisObject.insertDpaImage(dpaSsoImageData);
                    } else {
                        thisObject.insertImage(response[propertyName][0]);
                    }
                }
            },
            function (error) {

            });
    });
};
SW.DPA.SSO.InsertOnePixels();