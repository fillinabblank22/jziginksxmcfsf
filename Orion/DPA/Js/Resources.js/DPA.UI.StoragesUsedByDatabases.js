﻿SW.Core.namespace("SW.DPA.UI.StoragesUsedByDatabases");
SW.DPA.UI.StoragesUsedByDatabases = function (idOfContent, idOfEmptyContent) {
    this.id = idOfContent;
    var localizedStrings = {
        storageCountSingular: "@{R=DPA.Strings;K=StorageUsedByDatabases_StorageCountSingular;E=js}",
        storageCountPlural: "@{R=DPA.Strings;K=StorageUsedByDatabases_StorageCountPlural;E=js}",
        storageColumnHeaderSingular: '@{R=DPA.Strings;K=StorageUsedByDatabases_StorageTableTitleSingular;E=js}',
        storageColumnHeaderPlural: '@{R=DPA.Strings;K=StorageUsedByDatabases_StorageTableTitlePlural;E=js}',
        loadingIconNotificationText: '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js}',
        loadingIconNotificationTextOfLink: '@{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
        connectivityIssueNotificationText: '@{R=DPA.Strings;K=ConnectivityIssue_Message;E=js}'
    };

    var contentWrapper = $('#' + idOfContent);
    var noContent = $('#' + idOfEmptyContent);
    var rowTemplate = _.templateExt([
        '<div class="DPA_AppsUsingDbRow">',
            '<table class="DPA_AppsUsingDbHead">',
                '<tbody>',
                    '<tr>',
                        '<td class="DPA_AppsUsingDbExpand"><span class="DPA_ExpandIcon DPA_Collapsed"></span></td>',
                        '<td class="DPA_AppsUsingDbIcon"><a href="{{{Link}}}"><img src="{{{Icon}}}" /></a></td>',
                        '<td class="DPA_AppsUsingDbLabel">{{FormatDatabaseInstanceOnNodeLink()}}</td>',
                        '<td class="DPA_AppsUsingDbCount">{{ LunCount }} {{{StoragesLabel}}}</td>',
                    '</tr>',
                '</tbody>',
            '</table>',
            '<div class="DPA_AppsUsingDbTable DPA_DatabaseInstancesWrapper" data-app-id="{{Id}}" style="display:none">',
                // loading icon
                '<div id="LoadingIcon-{{UniqueClientID}}" style="display:none"><span class="DPA_LoadingIcon"></span>',
                '<p class="DPA_LoadingLongerText" style="display:none">{{{loadingIconNotificationText}}}',
                '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">&#187; {{{loadingIconNotificationTextOfLink}}}</a>',
                '</p></div>',
                // error message
                '<div id="ErrorMessage-{{UniqueClientID}}" class="sw-suggestion sw-suggestion-fail DPA_ResourceNotification" style="display:none"><span class="sw-suggestion-icon"></span><span class="DPA_Message"></span></div>',
                // connectivity issue warning
                '<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn" id="ConnectivityIssue-{{UniqueClientID}}">',
                    '<span class="sw-suggestion-icon"></span><span class="DPA_DismissButton" onclick="document.getElementById(\'ConnectivityIssue-{{UniqueClientID}}\').style.display = \'none\';"></span>',
                    '<div class="DPA_Body">',
                        '<p>{{connectivityIssueNotificationText}}:</p>',
                        '<div class="DPA_PartialErrors"></div>',
                        '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">{{{loadingIconNotificationTextOfLink}}}</a>',
                    '</div>',
                '</div>',
                // pageable table
                '<table id="Grid-{{UniqueClientID}}" class="sw-custom-query-table" cellpadding="2" cellspacing="0" width="100%">',
                    '<tr class="HeaderRow"></tr>',
                '</table>',
                '<div id="Pager-{{UniqueClientID}}" class="ReportFooter ResourcePagerControl hidden"></div>',
                '<input type="hidden" id="OrderBy-{{UniqueClientID}}" value="" />',
            '</div>',
        '</div>']);

    this.loadDatabaseInstances = function (loadingIcon, errorMessage, connectivityIssueWarning, isNodeFunctionalityAllowed) {
        var that = this;
        noContent.hide();
        loadingIcon.show();
        errorMessage.hide();
        connectivityIssueWarning.hide();
        SW.Core.Services.callController(
            "/api/DpaStoragesUsedByDatabases/GetDatabaseInstances",
            {},
            function (response) {
                loadingIcon.hide();
                contentWrapper.html('');

                if (response && response.PartialErrors && response.PartialErrors.length > 0) {
                    connectivityIssueWarning.show(response.PartialErrors);
                }

                if (response.DatabaseInstances.length > 0) {
                    contentWrapper.show();

                    for (var i = 0; i < response.DatabaseInstances.length; i++) {
                        var databaseInstancesExt = $.extend(response.DatabaseInstances[i], {
                            StoragesLabel: response.DatabaseInstances[i].LunCount > 1 ? localizedStrings.storageCountPlural : localizedStrings.storageCountSingular,
                            UniqueClientID: that.id + response.DatabaseInstances[i].Id,
                            FormatDatabaseInstanceOnNodeLink: function () {
                                return SW.DPA.Common.FormatDatabaseInstanceLink(
                                    this.Link,
                                    undefined,
                                    this.Name,
                                    this.NodeLink,
                                    this.NodeIcon,
                                    this.NodeName,
                                    isNodeFunctionalityAllowed
                                );
                            }
                        });

                        databaseInstancesExt = $.extend(databaseInstancesExt, localizedStrings);
                        contentWrapper.append(rowTemplate(databaseInstancesExt));
                    }

                    // bind expand/collapse click events
                    contentWrapper.find('.DPA_ExpandIcon').click(function () {
                        var collapsed = $(this).hasClass('DPA_Collapsed');
                        var tableWrapper = $(this).parent().parent().parent().parent().parent().find('.DPA_AppsUsingDbTable'),
                            loaded = tableWrapper.data('content-loaded'),
                            isLoaded = loaded && parseInt(loaded) == 1;
                        if (collapsed) {
                            if (!isLoaded) {
                                var databaseInstanceId = parseInt(tableWrapper.data('app-id'));
                                that.loadStoragesTable(databaseInstanceId, function () { tableWrapper.data('content-loaded', 1); });
                            }

                            $(this).removeClass('DPA_Collapsed');
                            $(this).addClass('DPA_Expanded');
                            tableWrapper.show();
                        } else {
                            $(this).removeClass('DPA_Expanded');
                            $(this).addClass('DPA_Collapsed');
                            tableWrapper.hide();
                        }
                    });
                } else {
                    noContent.show();
                    contentWrapper.hide();
                }
            },
            function (error) {
                errorMessage.show(error);
                loadingIcon.hide();
            });
    }

    this.loadStoragesTable = function (globalDatabaseInstanceId, onSuccessCallback) {
        var that = this;

        var tableIndex = {}, setTableIndex = function (columns) { $.each(columns, function (i, n) { tableIndex[n] = i; }); };
        var sortablePageableTableId = this.id + globalDatabaseInstanceId;

        var loadingIcon = new SW.DPA.UI.LoadingIcon('LoadingIcon-' + sortablePageableTableId);
        var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('ConnectivityIssue-' + sortablePageableTableId);
        var errorMessage = new SW.DPA.UI.ErrorMessage('ErrorMessage-' + sortablePageableTableId);

        SW.DPA.UI.SortablePageableTable.initialize(
        {
            uniqueId: sortablePageableTableId,
            pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                loadingIcon.show();
                connectivityIssueWarning.hide();
                errorMessage.hide();
                SW.Core.Services.callController("/api/DpaStoragesUsedByDatabases/GetStoragesByDatabaseInstance", {
                    Limit: pageSize,
                    PageIndex: pageIndex,
                    GlobalDatabaseInstanceIds: [globalDatabaseInstanceId]
                }, function (result) {
                    loadingIcon.hide();
                    if (typeof result.Metadata.PartialResultErrors !== 'undefined' && result.Metadata.PartialResultErrors.length > 0) {
                        connectivityIssueWarning.show(result.Metadata.PartialResultErrors);
                    }

                    setTableIndex(result.DataTable.Columns);
                    onSuccess(result);
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }, function (errorResult) {
                    errorMessage.show(errorResult);
                    loadingIcon.hide();
                });
            },
            initialPageIndex: 0,
            rowsPerPage: 10,
            columnSettings: {
                'StorageIcon': {
                    caption: '',
                    isHtml: true,
                    formatter: function (cellValue, rowArray) {
                        var content = SW.Core.String.Format('<a href="{0}"><img src="{1}" /></a>',
                            rowArray[tableIndex.StorageLink],
                            rowArray[tableIndex.StorageIcon]);

                        return content;
                    }
                },
                'StorageName': {
                    caption: localizedStrings.storagesColumnHeader,
                    isHtml: true,
                    formatter: function (cellValue, rowArray) {
                        var content = SW.DPA.Common.FormatDatabaseInstanceLink(rowArray[tableIndex.StorageLink], undefined, rowArray[tableIndex.StorageName]);

                        return content;
                    },
                    headerFormatter: function (totalRows) {
                        if (totalRows > 1) {
                            return SW.Core.String.Format("{0} ({1})", localizedStrings.storageColumnHeaderPlural, totalRows);
                        } else {
                            return localizedStrings.storageColumnHeaderSingular;
                        }
                    }
                }
            }
        });

        SW.DPA.UI.SortablePageableTable.refresh(sortablePageableTableId);
    };
}