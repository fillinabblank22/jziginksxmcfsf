﻿SW.Core.namespace("SW.DPA.UI.StorageObjectsByCapacityRiskExpandable");
SW.DPA.UI.StorageObjectsByCapacityRiskExpandable = function() {
    var tableTemplate = _.templateExt([
        '<tr class="DPA_TableContent DPA_StorageObjectsExpandableTable_Content DPA_ExpandableBlock_{{StorageId}}" style="display:none">',
        '<td colspan="9">',
        '<div class="DPA_TableWrapper"></div>',
        '</td>',
        '</tr>'
    ]);

    var contentId;
    var contentWrapper;

    this.initialize = function (uniqueId, searchTextBoxId, searchButtonId, _contentId, isNodeFunctionalityAllowed) {
        contentId = _contentId;
        contentWrapper = $('#' + contentId).parent().parent();

        var expanderColumnSettings = {
            "ObjectId": {
                header: '',
                isHtml: true,
                formatter: function(cellValue, row) {
                    return "<div class='DPA_ExpandIcon DPA_Collapsed' data-lun-id='" + cellValue + "'></div>";
                }
            }
        };

        var storageObjectsByCapacityRisk = new SW.DPA.UI.StorageObjectsByCapacityRisk();

        storageObjectsByCapacityRisk.columnSettings = _.extend(expanderColumnSettings, storageObjectsByCapacityRisk.columnSettings);

        storageObjectsByCapacityRisk.initialize(uniqueId, searchTextBoxId, searchButtonId, contentId, function () {
            contentWrapper.find('.DPA_ExpandIcon').each(function(index) {
                var row = $(this).closest("tr");
                var storageId = $(this).data('lun-id');
                var tablePlaceholder = tableTemplate({
                    "StorageId": storageId
                });

                $(tablePlaceholder).insertAfter(row);
            });

            // bind expand icon
            contentWrapper.find('.DPA_ExpandIcon').click(function() {
                var storageId = $(this).data('lun-id');

                var expandableBlock = contentWrapper.find('.DPA_ExpandableBlock_' + storageId);
                if ($(this).hasClass('DPA_Collapsed')) {
                    expandableBlock.show();
                    $(this).removeClass('DPA_Collapsed').addClass('DPA_Expanded');

                    var loaded = expandableBlock.data('content-loaded');
                    var isLoaded = loaded && parseInt(loaded) === 1;
                    if (!isLoaded) {
                        loadDatabaseInstanceTable(storageId,
                            function() {
                                expandableBlock.data('content-loaded', 1);
                            },
                            isNodeFunctionalityAllowed);
                    }
                } else {
                    expandableBlock.hide();
                    $(this).removeClass('DPA_Expanded').addClass('DPA_Collapsed');
                }
            });
        });
    };

    var loadDatabaseInstanceTable = function (lunId, onSuccessCallback, isNodeFunctionalityAllowed) {
        var config = {
            isNodeFunctionalityAllowed: isNodeFunctionalityAllowed,
            uniqueClientId: contentId.toString() + '-' + lunId,
            url: "/api/DpaDatabaseInstances/GetDatabaseInstancesByLun",
            getParameters: function () {
                return {
                    LunId: lunId
                };
            },
            onSuccess: function (result) {
                if (onSuccessCallback) {
                    onSuccessCallback();
                }
                return true;
            }
        };

        var grid = new DPA.UI.DatabaseInstancesGrid.Grid(contentWrapper.find('.DPA_ExpandableBlock_' + lunId + ' .DPA_TableWrapper'), config);
        grid.init(function () {
            grid.refresh();
        });
    }
};