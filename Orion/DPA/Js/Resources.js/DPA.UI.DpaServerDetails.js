﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};

SW.DPA.UI.DpaServerDetails = function (customConfig) {
    var defaultConfig = {
        wrapperElement: undefined,
        dpaServerId: undefined,
        loadingIcon: undefined,
        connectivityIssueWarning: undefined,
        errorMessage: undefined
    };

    var localizedStrings = {
        serverStatus: '@{R=DPA.Strings;K=DpaServerDetailsResource_ServerStatusLabel;E=js}',
        refreshDpaServerStatus: '@{R=DPA.Strings;K=DpaServerDetailsResource_RefreshDpaServerStatusLabel;E=js}',
        dpaServerName: '@{R=DPA.Strings;K=DpaServerDetailsResource_DpaServerNameLabel;E=js}',
        ipAdress: '@{R=DPA.Strings;K=DpaServerDetailsResource_IpAddressLabel;E=js}',
        port: '@{R=DPA.Strings;K=DpaServerDetailsResource_PortLabel;E=js}',
        dpaVersion: '@{R=DPA.Strings;K=DpaServerDetailsResource_DpaVersionLabel;E=js}',
        dbInstances: '@{R=DPA.Strings;K=DpaServerDetailsResource_DbInstancesLabel;E=js}',
        cannotLoadDataMessage: '@{R=DPA.Strings;K=CannotLoad_Msg;E=js}',
        refreshing: '@{R=DPA.Strings;K=DpaServerDetailsResource_Refreshing;E=js}',
        compatibilityIssueWarningGoToLink: '@{R=DPA.Strings;K=PartiallyCompatibleWarningGoToLink;E=js}',
        compatibilityIssueLinkLabel: '@{R=DPA.Strings;K=PartiallyCompatibleWarningLinkLabel;E=js}',
        compatibilityIssueDpaObsoleteMsg: '@{R=DPA.Strings;K=PartiallyCompatible_DpaObsolete_Message;E=js}',
        compatibilityIssueDpaimObsoleteMsg: '@{R=DPA.Strings;K=PartiallyCompatible_DpaimObsolete_Message;E=js}'
    };

    var config = $.extend({}, defaultConfig, customConfig);

    var template = _.templateExt([
        '{# if(IntegrationStatus == 106) { #}',
        '<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn">',
        '<span class="sw-suggestion-icon"></span>{{compatibilityIssueWarningMsg}} ',
        '{{compatibilityIssueWarningGoToLink}} <a href="/ui/dpa/admin/serverManagement">{{compatibilityIssueLinkLabel}} &#187;</a>',
        '</div>',
        '{# } #}',
        '<table automation="table_dpaServerDetails">',
            '<tr>',
                '<td>{{serverStatus}}:</td>',
                '<td>',
                    '<div class="DPA_ServerStatus"><img automation="dpaServerStatus" src="{{StatusIcon}}" />{{StatusDescription}}<a href="#" class="DPA_RefreshDpaServerStatusButton">{{refreshDpaServerStatus}}</a></div>',
                    '<span class="DPA_Refreshing" style="display: none">{{refreshing}}</span>',
                '</td>',
            '</tr>',
            '<tr>',
                '<td>{{dpaServerName}}:</td><td automation="dpaServerName" >{{DisplayName}}</td>',
            '</tr>',
            '<tr>',
                '<td>{{ipAdress}}:</td><td automation="dpaServerAddress" >{{Address}}</td>',
            '</tr>',
            '<tr>',
                '<td>{{port}}:</td><td automation="dpaServerPort" >{{Port}}</td>',
            '</tr>',
            '<tr>',
                '<td>{{dpaVersion}}:</td><td automation="dpaServerVersion" >{{ DpaVersion != null ? DpaVersion : cannotLoadDataMessage }}</td>',
            '</tr>',
            '<tr>',
                '<td>{{dbInstances}}:</td><td automation="dpaServerInstances" ><img src="/Orion/DPA/images/database-instance.png" />{{DatabaseInstancesCount != null ? DatabaseInstancesCount : cannotLoadDataMessage}}</td>',
            '</tr>',
        '</table>']);

    var that = this;

    var refreshDpaServerStatus = function () {
        config.loadingIcon.show();
        config.connectivityIssueWarning.hide();
        config.errorMessage.hide();
        var status = config.wrapperElement.find(".DPA_ServerStatus").hide();
        var refreshing = config.wrapperElement.find(".DPA_Refreshing").show();

        SW.Core.Services.callController('/api/DpaServers/RefreshDpaServerStatus', config.dpaServerId,
        function () {
            that.load();
        }, function (errorResult) {
            config.loadingIcon.hide();
            config.errorMessage.show(errorResult);
            status.show();
            refreshing.hide();
        });
    };

    this.load = function () {
        config.loadingIcon.show();
        config.connectivityIssueWarning.hide();
        config.errorMessage.hide();

        SW.Core.Services.callController('/api/DpaServers/GetDpaServerDetails', config.dpaServerId,
        function (data) {
            config.loadingIcon.hide();
            if (typeof data.PartialResultErrors !== "undefined" && data.PartialResultErrors.length > 0) {
                config.connectivityIssueWarning.show(data.PartialResultErrors);
            }

            config.wrapperElement.html(template($.extend({ compatibilityIssueWarningMsg: data.IsDpaObsolete ? localizedStrings.compatibilityIssueDpaObsoleteMsg : localizedStrings.compatibilityIssueDpaimObsoleteMsg }, localizedStrings, data)));
            config.wrapperElement.find(".DPA_RefreshDpaServerStatusButton").on("click", function () {
                refreshDpaServerStatus();
                return false;
            });
        }, function (errorResult) {
            config.loadingIcon.hide();
            config.errorMessage.show(errorResult);
        });
    };
};