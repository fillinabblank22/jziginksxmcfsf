﻿SW.Core.namespace("SW.DPA.UI.StorageObjectsByCapacityRisk");
SW.DPA.UI.StorageObjectsByCapacityRisk = function () {
    var resourceUniqueId;
    var visibilitySet = false;

    var getContentTablePointer = function () {
        return $('#Grid-' + resourceUniqueId);
    };

    var getErrorMessagePointer = function () {
        return $('#ErrorMsg-' + resourceUniqueId);
    };

    var showCustomErrorText = function (text) {
        getContentTablePointer().hide();
        getErrorMessagePointer().text(text).show();
    };

    var hideCustomError = function () {
        getContentTablePointer().show();
        getErrorMessagePointer().hide();
    }

    var thresholdFormatter = function (cellValue, row, cellInfo, placeholderPrefix) {
        var runout = '';
        if (cellInfo.cellCssClassProvider(cellValue, row, cellInfo)) {
            runout = '@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_Now;E=js}';
        }

        var placeHolderId = placeholderPrefix + '_' + row[4] + '_' + row[0];
        return SW.DPA.Formatters.FormatPercentageColumnValue(cellValue) + '<span id="' + placeHolderId + '_in"></span><br /><span id="' + placeHolderId + '" class="DPA_capacityrisk-threshold-days-left">' + runout + '</span>';
    };

    var setCapacityRunOut = function (value, elementId, thresholdValue, currentUsage) {
        var $element = $('#' + elementId);
        var $elementIn = $('#' + elementId + '_in');

        $element.removeClass('DPA_Unknown_Cell');
        if ($element.text()) {
            return;
        }

        if (SW.DPA.Formatters.IsRunoutCollectingData(value)) {
            $element.text('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_Collecting;E=js}');
            $element.addClass('DPA_Unknown_Cell');
            $element.removeClass('DPA_capacityrisk-threshold-days-left');
            return;
        }

        if (currentUsage >= thresholdValue) {
            var text = "@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_Now;E=js}";
            $element.text(text);
            return;
        }

        $element.text(SW.DPA.Formatters.FormatDaysDiff(value));
        $elementIn.text(' @{R=DPA.Strings;K=StorageObjectsByCapacityRisk_WillExpireIn;E=js}');
    };

    this.columnSettings = {};
    
    this.initialize = function(uniqueId, searchTextBoxId, searchButtonId, contentId, onTableLoadCallback) {
        resourceUniqueId = uniqueId;
        var contentWrapper = $('#' + contentId).parent().parent();

        SW.Core.Resources.CustomQuery.initialize(
        {
            uniqueId: uniqueId,
            initialPage: 0,
            rowsPerPage: 5,
            allowSort: false,
            columnSettings: _.extend(this.columnSettings, {
                "Resource": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_ResourceColumn;E=js}'),
                    cellCssClassProvider: function(value, row, cellInfo) { return "DPA_Icon_Cell DPA_word_wrap"; }
                },
                "Type": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_TypeColumn;E=js}'),
                    formatter: function(cellValue) {
                        return cellValue;
                    }
                },
                "Array": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_ArrayColumn;E=js}'),
                    cellCssClassProvider: function(value, row, cellInfo) { return "DPA_word_wrap"; }
                },
                "Last7Days": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_LastSevenDaysColumn;E=js}'),
                    isHtml: true,
                    formatter: function(cellValue, row, cellInfo) {
                        var placeHolderId = 'chart_placeHolder_' + uniqueId + '_' + row[4] + '_' + row[0];

                        return '<div class="DPA_capacityrisk-chart" id="' + placeHolderId + '"></div>';
                    },
                    cellCssClassProvider: function(value, row, cellInfo) {
                        return row[12] >= row[10] ? 'DPA_Grid_Total_Cell' : SW.DPA.Formatters.FormatCellStyle(row[12], row[8], row[9]);
                    }
                },
                "Warning": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_WarningColumn;E=js}'),
                    isHtml: true,
                    formatter: function(cellValue, row, cellInfo) {
                        return '&gt; ' + thresholdFormatter(cellValue, row, cellInfo, 'warning_placeHolder_' + uniqueId);
                    },
                    cellCssClassProvider: function(value, row, cellInfo) {
                        return '';
                    }
                },
                "Critical": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_CriticalColumn;E=js}'),
                    isHtml: true,
                    formatter: function(cellValue, row, cellInfo) {
                        return '&gt ' + thresholdFormatter(cellValue, row, cellInfo, 'critical_placeHolder_' + uniqueId);
                    },
                    cellCssClassProvider: function(value, row, cellInfo) {
                        return '';
                    }
                },
                "ATCapacity": {
                    header: SW.DPA.Formatters.FormatColumnTitle('@{R=DPA.Strings;K=StorageObjectsByCapacityRisk_ATCapacityColumn;E=js}'),
                    isHtml: true,
                    formatter: function(cellValue, row, cellInfo) {
                        return thresholdFormatter(cellValue, row, cellInfo, 'atcapacity_placeHolder_' + uniqueId);
                    },
                    cellCssClassProvider: function(value, row, cellInfo) {
                        return '';
                    }
                }
            }),
            onLoad: function (rows) {
                hideCustomError();
                if (rows.length > 0) {
                    SW.Core.Services.callWebService("/Orion/DPA/Services/StorageObjectsByCapacityRiskCharts.asmx", "GetCapacityRiskData", { data: rows }, function (result) {
                        $.each(rows, function(index, row) {
                            var serie = $.grep(result.DataSeries, function(x) { return x.TagName.toUpperCase() === (row[4] + '_' + row[0]).toUpperCase() })[0];

                            var warningPlaceHolderId = 'warning_placeHolder_' + uniqueId + '_' + row[4] + '_' + row[0];
                            setCapacityRunOut(serie.CustomProperties.capacityRunOutDateWarning, warningPlaceHolderId, row[8], row[12]);

                            var criticalPlaceHolderId = 'critical_placeHolder_' + uniqueId + '_' + row[4] + '_' + row[0];
                            setCapacityRunOut(serie.CustomProperties.capacityRunOutDateCritical, criticalPlaceHolderId, row[9], row[12]);

                            var totalPlaceHolderId = 'atcapacity_placeHolder_' + uniqueId + '_' + row[4] + '_' + row[0];
                            setCapacityRunOut(serie.CustomProperties.capacityRunOutDateTotal, totalPlaceHolderId, row[10], row[12]);

                            var chartPlaceHolderId = 'chart_placeHolder_' + uniqueId + '_' + row[4] + '_' + row[0];
                            new SW.DPA.Charts.StorageObjectsByCapacityRiskChart().createChart({
                                placeHolderId: chartPlaceHolderId,
                                usedInPercent: row[12],
                                slopeChartData: serie.CustomProperties.slope,
                                data: serie.Data,
                                name: serie.Label
                            });
                        });
                    });
                } else {
                    if (!visibilitySet) {
                        contentWrapper.hide();
                    }
                    showCustomErrorText("@{R=DPA.Strings;K=SortablePageableTable_NoDataMessage_Text;E=xml}");
                }

                visibilitySet = true;

                if (typeof onTableLoadCallback !== 'undefined') {
                    onTableLoadCallback();
                }
            },
            searchTextBoxId: searchTextBoxId,
            searchButtonId: searchButtonId
        });

        $('#OrderBy-' + uniqueId).val('[_UsedCapacityPercentage] DESC, [Resource] ASC');
        SW.Core.Resources.CustomQuery.refresh(uniqueId);
    };
};