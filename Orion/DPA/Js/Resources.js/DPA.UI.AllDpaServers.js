﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};

SW.DPA.UI.AllDpaServers = function (customConfig) {
    var localizedStrings = {
        compatibilityIssueWarningGoToLink: '@{R=DPA.Strings;K=PartiallyCompatibleWarningGoToLink;E=js}',
        compatibilityIssueLinkLabel: '@{R=DPA.Strings;K=PartiallyCompatibleWarningLinkLabel;E=js}',
        compatibilityIssueDpaObsoleteMsg: '@{R=DPA.Strings;K=PartiallyCompatible_DpaObsolete_Message;E=js}',
        compatibilityIssueDpaimObsoleteMsg: '@{R=DPA.Strings;K=PartiallyCompatible_DpaimObsolete_Message;E=js}'
    };

    var defaultConfig = {
        wrapperElement: undefined,
        loadingIcon: undefined,
        errorMessage: undefined
    };

    var config = $.extend({}, defaultConfig, customConfig);

    var template = _.templateExt([
        '{# if(isPartiallyCompatible) { #}',
        '<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn">',
        '<span class="sw-suggestion-icon"></span>{{compatibilityIssueWarningMsg}} ',
        '{{compatibilityIssueWarningGoToLink}} <a href="/ui/dpa/admin/serverManagement">{{compatibilityIssueLinkLabel}} &#187;</a>',
        '</div>',
        '{# } #}',
        '{# _.each(dpaServers, function(dpaServer) { #}',
        '<div class="DPA_Row"><img src="{{dpaServer.StatusIconUrl}}" /><a href="{{dpaServer.DetailsViewUrl}}">{{{dpaServer.DisplayName}}}</a></div>',
        '{# }); #}']);

    this.load = function () {
        config.loadingIcon.show();
        config.errorMessage.hide();

        SW.Core.Services.callController('/api/DpaServers/GetAll', {},
        function (data) {
            config.loadingIcon.hide();
            var isPartiallyCompatible = _.some(data, function (dpaServer) { return dpaServer.IsPartiallyCompatible; });
            var isDpaObsolete = _.some(data, function (dpaServer) { return dpaServer.IsDpaObsolete; });

            config.wrapperElement.html(template($.extend({ compatibilityIssueWarningMsg: isDpaObsolete ? localizedStrings.compatibilityIssueDpaObsoleteMsg : localizedStrings.compatibilityIssueDpaimObsoleteMsg },
                { dpaServers: data, isPartiallyCompatible: isPartiallyCompatible }, localizedStrings)));
        }, function (errorResult) {
            config.loadingIcon.hide();
            config.errorMessage.show(errorResult);
        });
    };
};