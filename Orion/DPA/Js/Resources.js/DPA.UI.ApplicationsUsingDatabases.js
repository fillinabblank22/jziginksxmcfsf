﻿SW.Core.namespace("SW.DPA.UI.ApplicationsUsingDatabases");
SW.DPA.UI.ApplicationsUsingDatabases = function (dpaServerId, idOfContent, idOfEmptyContent, kpiTargets) {
    this.id = idOfContent;
    var localizedStrings = {
        databaseInstancesSingular: "@{R=DPA.Strings;K=ApplicationsUsingMyDatabases_TitleLabelSingular;E=js}",
        databaseInstancesPlural: "@{R=DPA.Strings;K=ApplicationsUsingMyDatabases_TitleLabelPlural;E=js}",
        addDbInDPA_Text: '@{R=DPA.Strings;K=DatabaseInstancesResource_AddDbInstanceLinkLabel;E=js}' + " &raquo;",
        addDbInDPA_NoAdminLabel: '@{R=DPA.Strings;K=DatabaseInstancesResource_AddDbInDPA_NoAdminLabel;E=js}',
        databaseInstancesColumnHeaderSingular: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Singular;E=js}',
        databaseInstancesColumnHeaderPlural: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Plural;E=js}',
        waitTimeColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_WaitTime;E=js}',
        queryColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Query;E=js}',
        cpuColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_CPU;E=js}',
        memoryColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Memory;E=js}',
        diskColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Disk;E=js}',
        sessionColumnHeader: '@{R=DPA.Strings;K=DatabaseInstancesResource_TableColumnHeader_Session;E=js}',
        loadingIconNotificationText: '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js}',
        loadingIconNotificationTextOfLink: '@{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
        connectivityIssueNotificationText: '@{R=DPA.Strings;K=ConnectivityIssue_Message;E=js}'
    };
    this.kpiTargets = kpiTargets;

    var contentWrapper = $('#' + idOfContent);
    var noContent = $('#' + idOfEmptyContent);
    var rowTemplate = _.templateExt([
        '<div class="DPA_AppsUsingDbRow">',
            '<table class="DPA_AppsUsingDbHead">',
                '<tbody>',
                    '<tr>',
                        '<td class="DPA_AppsUsingDbExpand"><span class="DPA_ExpandIcon DPA_Collapsed"></span></td>',
                        '<td class="DPA_AppsUsingDbIcon"><a href="{{{Link}}}"><img src="{{{Icon}}}" /></a></td>',
                        '<td class="DPA_AppsUsingDbLabel">{{FormatApplicationOnNodeLink()}}</td>',
                        '<td class="DPA_AppsUsingDbCount">{{ DatabasesInstancesCount }} {{{DatabaseInstancesLabel}}}</td>',
                    '</tr>',
                '</tbody>',
            '</table>',
            '<div class="DPA_AppsUsingDbTable DPA_DatabaseInstancesWrapper DPA_DbRow_{{Id}}" data-app-id="{{Id}}" style="display:none">',
            '</div>',
        '</div>']);

    this.loadApplications = function (loadingIcon, errorMessage, isNodeFunctionalityAllowed) {
        var that = this;
        noContent.hide();
        loadingIcon.show();
        errorMessage.hide();
        SW.Core.Services.callController(
            "/api/DpaApplicationsUsingDatabases/GetApplications",
            dpaServerId,
            function (applications) {
                loadingIcon.hide();
                contentWrapper.html('');
                if (applications.length > 0) {
                    contentWrapper.show();
                    
                    for (var i = 0; i < applications.length; i++) {
                        var applicationExt = $.extend(applications[i], {
                            DatabaseInstancesLabel: applications[i].DatabasesInstancesCount > 1 ? localizedStrings.databaseInstancesPlural : localizedStrings.databaseInstancesSingular,
                            UniqueClientID: that.id + applications[i].Id,
                            FormatApplicationOnNodeLink: function () {
                                return SW.DPA.Common.formatEntityOnNodeLink(
                                    this.Link,
                                    undefined,
                                    this.Name,
                                    this.NodeLink,
                                    this.NodeIcon,
                                    this.NodeName
                                );
                            }
                        });

                        applicationExt = $.extend(applicationExt, localizedStrings);
                        contentWrapper.append(rowTemplate(applicationExt));
                    }

                    // bind expand/collapse click events
                    contentWrapper.find('.DPA_ExpandIcon').click(function () {
                        var collapsed = $(this).hasClass('DPA_Collapsed');
                        var tableWrapper = $(this).parent().parent().parent().parent().parent().find('.DPA_AppsUsingDbTable'),
                            loaded = tableWrapper.data('content-loaded'),
                            isLoaded = loaded && parseInt(loaded) == 1;
                        if (collapsed) {
                            if (!isLoaded) {
                                var applicationId = parseInt(tableWrapper.data('app-id'));
                                that.loadDatabaseInstanceTable(applicationId, function () { tableWrapper.data('content-loaded', 1); }, isNodeFunctionalityAllowed);
                            }

                            $(this).removeClass('DPA_Collapsed');
                            $(this).addClass('DPA_Expanded');
                            tableWrapper.show();
                        } else {
                            $(this).removeClass('DPA_Expanded');
                            $(this).addClass('DPA_Collapsed');
                            tableWrapper.hide();
                        }
                    });
                } else {
                    noContent.show();
                    contentWrapper.hide();
                }
            },
            function (error) {
                errorMessage.show(error);
                loadingIcon.hide();
            });
    }

    this.loadDatabaseInstanceTable = function (applicationId, onSuccessCallback, isNodeFunctionalityAllowed) {
        var config = {
            isNodeFunctionalityAllowed: isNodeFunctionalityAllowed,
            uniqueClientId: idOfContent.toString() + applicationId,
            url: "/api/DpaDatabaseInstances/GetDatabaseInstancesByClientApplications",
            getParameters: function() {
                return {
                    DpaServerId: dpaServerId,
                    ClientApplicationIds: [applicationId]
                };
            },
            onSuccess: function (result) {
                if (onSuccessCallback) {
                    onSuccessCallback();
                }
                return true;
            }
        };

        var grid = new DPA.UI.DatabaseInstancesGrid.Grid(contentWrapper.find(".DPA_DbRow_" + applicationId), config);
        grid.init(function() {
            grid.refresh();
        });
    };
}