﻿SW.Core.namespace("SW.DPA.UI.ErrorMessage");

SW.DPA.UI.ErrorMessage = function (id)
{
    var block = $('#' + id);
    var textBlock = block.find('.DPA_Message');

    this.text = function(text) {
        textBlock.text(text);
    };

    this.show = function (text) {
        if (typeof text !== 'undefined') {
            textBlock.text(text);
        }
        block.show();
    };

    this.hide = function () {
        block.hide();
    };
};