﻿SW.Core.namespace("DPA.UI.InfoModal");

(function (ctx) {
    var localizedStrings = {
        description: '@{R=DPA.Strings;K=InfoBox_Title;E=js}',
        closeButtonLabel: '@{R=DPA.Strings;K=InfoBox_Close;E=js}'
    };

    ctx.show = function(title, description) {
        var adviceInfoTemplate = _.templateExt([
         '<div id="DPA_InfoModal" automation="DpaInfoModal">',
             '<div id="infotext">',
                 '<h3>{{title}}</h3>',
                 '<p>{{description}}</p>',
             '</div>',
             '<div id="buttons"><br/></div>',
         '</div>']);

        var viewModel = {
            title: title,
            description: description
        };

        var adviceInfo = $(adviceInfoTemplate(viewModel));

        adviceInfo.find('#buttons').append($(SW.Core.Widgets.Button(localizedStrings.closeButtonLabel,
            { type: 'secondary' })).click(function () { adviceInfo.dialog('close').remove(); }));

        adviceInfo.dialog({ modal: true, minWidth: 480, minHeight: 100, resizable: false, title: localizedStrings.description });

        return false;
    };
})(DPA.UI.InfoModal);
