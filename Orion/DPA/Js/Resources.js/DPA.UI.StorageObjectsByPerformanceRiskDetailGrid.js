﻿SW.Core.namespace("SW.DPA.UI.StorageObjectsByPerformanceRiskDetailGrid");

SW.DPA.UI.StorageObjectsByPerformanceRiskDetailGrid = function () {
    var localizedStrings = {
        columnHeaderResource: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceName;E=js}',
        columnHeaderType: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceType;E=js}',
        columnHeaderArray: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceArray;E=js}',
        columnHeaderLatency: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceLatency;E=js}',
        columnHeaderIops: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceIops;E=js}',
        columnHeaderThroughput: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceThroughput;E=js}',
        storageType: '@{R=DPA.Strings;K=StorageType_Lun;E=js}',
        relaxFiltersMessage: "@{R=DPA.Strings;K=RelaxFiltersMessage;E=js}",
        unknownLabel: "@{R=DPA.Strings;K=StoragesByPerformanceRisk_UnknownLabel;E=js}",
        notAvailableLabel: "@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_NotAvailable;E=js}"
    };
    var iopsCategory = 'IOPS';
    var latencyCategory = 'Latency';
    var throughputCategory = 'Throughput';
    var errorWeight = 10;
    var detachedResource = '/Orion/SRM/Resources/PerformanceDrillDown/PerformanceDrillDownChart.ascx';
    var performanceServiceMethod = '/Orion/SRM/Services/PerformanceDrillDownService.asmx/GetPerformance';
    var resourceWidth = 800;

    var resourceUniqueId;
    var getContentTablePointer = function () {
        return $('#Grid-' + resourceUniqueId);
    };
    var getErrorMessagePointer = function () {
        return $('#ErrorMsg-' + resourceUniqueId);
    };

    this.initialize = function (_uniqueId, _searchTextBoxId, _searchButtonId, _rowsPerPage) {
        resourceUniqueId = _uniqueId;

        SW.Core.Resources.CustomQuery.initialize(
            {
                uniqueId: _uniqueId,
                initialPage: 0,
                rowsPerPage: _rowsPerPage,
                allowSort: false,
                columnSettings: {
                    "Resource": {
                        header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderResource),
                        cellCssClassProvider: function(value, row, cellInfo) {
                            return "DPA_WordWrap";
                        }
                    },
                    "Type": {
                        header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderType),
                        formatter: function (cellValue, row) {
                            row[4] = localizedStrings.storageType;

                            return cellValue;
                        }
                    },
                    "Array": {
                        header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderArray),
                        cellCssClassProvider: function (value, row, cellInfo) {
                            return "DPA_WordWrap";
                        }
                    },
                    "IOPS":
                        {
                            header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderIops),
                            isHtml: true,
                            formatter: function (cellValue, row, cellInfo) {
                                return formatCellValue(cellValue, row, cellInfo, iopsCategory);
                            },
                            cellCssClassProvider: function (value, row, cellInfo) {
                                return setFormatCellStyle(value, row, cellInfo, iopsCategory) + " IOPS_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                            }
                        },
                    "Latency": {
                        header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderLatency),
                        isHtml: true,
                        formatter: function (cellValue, row, cellInfo) {
                            return formatCellValue(cellValue, row, cellInfo, latencyCategory);
                        },
                        cellCssClassProvider: function (value, row, cellInfo) {
                            return setFormatCellStyle(value, row, cellInfo, latencyCategory) + " Latency_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                        }
                    },
                    "Throughput": {
                        header: SW.DPA.Formatters.FormatColumnTitle(localizedStrings.columnHeaderThroughput),
                        isHtml: true,
                        formatter: function (cellValue, row, cellInfo) {
                            return formatCellValue(cellValue, row, cellInfo, throughputCategory);
                        },
                        cellCssClassProvider: function (value, row, cellInfo) {
                            return setFormatCellStyle(value, row, cellInfo, throughputCategory) + " Throughput_Drill_Down_" + getNetObject(row, "_") + "_" + _uniqueId;
                        }
                    }
                },
                onLoad: function (rows, columnsInfo) {
                    if (rows && rows.length === 0) {
                        getContentTablePointer().hide();
                        getErrorMessagePointer().text(localizedStrings.relaxFiltersMessage).show();
                    } else {
                        getContentTablePointer().show();
                        getErrorMessagePointer().hide();
                    }

                    $.each(rows, function (rowIndex, row) {
                        var cssNetObject = getNetObject(row, "_");
                        SW.SRM.Redirector.bindRedirectClick($('.IOPS_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnIOPS(getNetObject(row)); });
                        if (cssNetObject.lastIndexOf("SMSA") == -1 && cssNetObject.lastIndexOf("SMSP") == -1 && cssNetObject.lastIndexOf("SMVS") == -1) {
                            SW.SRM.Redirector.bindRedirectClick($('.Latency_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnLatency(getNetObject(row)); });
                        }
                        SW.SRM.Redirector.bindRedirectClick($('.Throughput_Drill_Down_' + cssNetObject + "_" + _uniqueId), function () { clickOnThroughput(getNetObject(row)); });
                    });
                },
                searchTextBoxId: _searchTextBoxId,
                searchButtonId: _searchButtonId
            });

        // Order by number of errors, most critical items on top
        // Note: default ordering is not supported by CustomQuery component FB350425
        $('#OrderBy-' + _uniqueId).val('[_TotalErrors] DESC, [Latency] DESC, [IOPS] DESC, [Throughput] DESC');

        SW.Core.Resources.CustomQuery.refresh(_uniqueId);
    };

    var formatCellValue = function (cellValue, row, cellInfo, category) {
        var result = '';
        var arrayOrPoolOrVServer = row[4] == 3 || row[4] == 1 || row[4] == 4;
        var arrayOrVServer = row[4] == 3 || row[4] == 4;
        var totalValue = cellValue;

        if (totalValue == null || totalValue === '' || typeof (cellValue) === "undefined") {
            //if netObject is array or pool or vserver and category is latencyCategory then return N/A value
            if (arrayOrPoolOrVServer && category == latencyCategory)
                return localizedStrings.notAvailableLabel;
            else
                return localizedStrings.unknownLabel;
        }

        if (category == iopsCategory) {
            result = totalValue.toFixed();
        } else if (category == latencyCategory) {
            if (arrayOrVServer) {
                result = localizedStrings.notAvailableLabel;
            } else {
                result = SW.DPA.Formatters.FormatLatencyNumber(totalValue) + ' ms';
            }
        } else if (category == throughputCategory) {
            var value = totalValue / SW.DPA.Formatters.BytesInMB;
            result = value.toFixed(2) + ' MB/s';
        }
        return result;
    };

    var setFormatCellStyle = function (value, row, cellInfo, category) {
        var totalValue = value;
        if (totalValue == null) {
            return cssclass;
        }
        var cssclass = "";
        var errorCount = row[getCategoryErrorIndex(category)];
        if (errorCount >= 1 && errorCount < errorWeight) {
            cssclass = "DPA_WarningCell";
        } else if (errorCount >= errorWeight) {
            cssclass = "DPA_CriticalCell";
        }
        return cssclass;
    };

    var getCategoryErrorIndex = function (category) {
        var errorIndex;
        switch (category) {
            case iopsCategory:
                errorIndex = 12;
                break;
            case latencyCategory:
                errorIndex = 13;
                break;
            case throughputCategory:
                errorIndex = 14;
                break;
            default:
                errorIndex = 0;
        }
        return errorIndex;
    };

    var getNetObject = function (row, separator) {
        //Position of _NetObjectPrefix field position in Query
        var NetObjectPosition = 0;
        var NetObjectId = 1;
        if (separator) {
            return row[NetObjectPosition] + separator + row[NetObjectId];
        }
        return row[NetObjectPosition] + ":" + row[NetObjectId];
    }

    var clickOnIOPS = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'IOPS',
                NetObject: netObject
            },
            queryString: {
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnLatency = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Latency',
                NetObject: netObject
            },
            queryString: {
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnThroughput = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Throughput',
                NetObject: netObject
            },
            queryString: {
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };
};
