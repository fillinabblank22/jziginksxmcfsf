﻿sW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.ConnectivityIssueWarning = function (id) {
    var localizedStrings = {
        goToDpaLabel: '@{R=DPA.Strings;K=ConnectivityIssue_GoToDpaLabel;E=js}'
    };
    var notificationBlock = $('#' + id);
    var dpaPartialErrors = notificationBlock.find('.DPA_PartialErrors');

    this.show = function (partialErrors) {
        if (typeof partialErrors !== 'undefined' && partialErrors != null) {
            dpaPartialErrors.html('');
            for (var i = 0; i < partialErrors.length; i++) {
                var row = $('<div/>');
                var name = $('<b/>').text(partialErrors[i].DpaServerName);
                var errorMessage = SW.Core.String.Format(' ({0}) ', partialErrors[i].ErrorMessage);
                var dpaServerLink = $('<a />')
                    .attr('href', '/api/DpaLinksToDpaj/MainDashboard/' + partialErrors[i].DpaServerId)
                    .attr('target', '_blank')
                    .text(SW.Core.String.Format("» {0}", localizedStrings.goToDpaLabel));

                name.appendTo(row);
                row.append(errorMessage);
                dpaServerLink.appendTo(row);

                row.appendTo(dpaPartialErrors);
            }
        }

        notificationBlock.show();
    };

    this.hide = function () {
        notificationBlock.hide();
    };

    this.isVisible = function() {
        return notificationBlock.is(":visible");
    };
}