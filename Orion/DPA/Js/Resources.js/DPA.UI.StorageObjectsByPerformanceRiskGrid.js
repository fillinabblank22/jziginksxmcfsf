﻿SW.Core.namespace("SW.DPA.UI.StorageObjectsByPerformanceRiskGrid");

SW.DPA.UI.StorageObjectsByPerformanceRiskGrid = function (config, kpiTargets) {
    var contentWrapper = $('#' + config.contentId);
    var noIntegrationContentWrapper = $('#' + config.noIntegrationContentId);
    var searchControl = new SW.DPA.UI.SearchControl(config.searchControlId, function () {
        loadStorages(1, getPageSizeFromPaginator());
    });
    var localizedStrings = {
        noDataMessage: '@{R=DPA.Strings;K=SortablePageableTable_NoDataMessage_Text;E=js}',
        columnHeaderResource: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceName;E=js}',
        columnHeaderType: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceType;E=js}',
        columnHeaderArray: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceArray;E=js}',
        columnHeaderLatency: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceLatency;E=js}',
        columnHeaderIops: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceIops;E=js}',
        columnHeaderThroughput: '@{R=DPA.Strings;K=StorageObjectsByPerformanceRiskTable_ColumncResourceThroughput;E=js}',
        showAllLabel: '@{R=DPA.Strings;K=Pagination_ShowAllLabel;E=js}',
        displayingObjectsLabel: '@{R=DPA.Strings;K=IntegrationWizard_PagingToolbarDisplayMsg;E=js}',
        itemsOnPageLabel: '@{R=DPA.Strings;K=Pagination_ItemsOnPageLabel;E=js}',
        pageLabel: '@{R=DPA.Strings;K=Pagination_PageLabel;E=js}',
        ofFormat: ' @{R=DPA.Strings;K=Pagination_NofMLabel;E=js} '
    };
    var detachedResource = '/Orion/SRM/Resources/PerformanceDrillDown/PerformanceDrillDownChart.ascx';
    var performanceServiceMethod = '/Orion/SRM/Services/PerformanceDrillDownService.asmx/GetPerformance';
    var resourceWidth = 800;
    this.kpiTargets = kpiTargets;
    var that = this;
    var defaultPageSize = config.pageSize;
    var tableTemplate = _.templateExt([
        '<table class="sw-custom-query-table DPA_StorageObjectsExpadabledTable">',
            '<tbody>',
                '<tr class="HeaderRow">',
                    '<th class="ReportHeader" colspan="2">{{columnHeaderResource}}</th>',
                    '<th class="ReportHeader">{{columnHeaderType}}</th>',
                    '<th class="ReportHeader">{{columnHeaderArray}}</th>',
                    '<th class="ReportHeader">{{columnHeaderLatency}}</th>',
                    '<th class="ReportHeader">{{columnHeaderIops}}</th>',
                    '<th class="ReportHeader">{{columnHeaderThroughput}}</th>',
                '</tr>',
                '{# if(rows.length == 0) { #}',
                '<tr class="DPA_SortablePageableTableNoData"><td colspan="6">{{noDataMessage}}</td></tr>',
                '{# } else { #}',
                '{# _.each(rows, function(row) { #}',
                    '<tr class="DPA_TableContent" data-netobject="{{row.StorageNetObject}}">',
                        '<td class="DPA_StorageObjectsExpadabledGridColStorage"><span data-lun-id="{{row.StorageId}}" class="DPA_ExpandIcon DPA_Collapsed"></span></td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColStorage"><span class="DPA_WordWrap"><img src="{{{row.StorageIcon}}}" /> <a href="{{{row.StorageLink}}}">{{row.StorageName}}</a><span></td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColType">{{row.StorageType}}</td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColArray"><span class="DPA_WordWrap"><img src="{{{row.ArrayIcon}}}" /> <a href="{{{row.ArrayLink}}}">{{row.ArrayName}}</a></span></td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColLatency DPA_Clickable {{ getCellThresholdCssClass(row.LatencyStatus) }}">{{row.Latency}}</td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColIops DPA_Clickable {{ getCellThresholdCssClass(row.IopsStatus) }}">{{row.Iops}}</td>',
                        '<td class="DPA_StorageObjectsExpadabledGridColThroughput DPA_Clickable {{ getCellThresholdCssClass(row.ThroughputStatus) }}">{{row.Throughput}}</td>',
                    '</tr>',
                    '<tr class="DPA_TableContent DPA_StorageObjectsExpadabledTable_Content DPA_ExpandableBlock{{row.StorageId}}" style="display:none">',
                        '<td colspan="7">',
                        '<div class="DPA_TableWrapper"></div>',
                        '</td>',
                    '</tr>',
                '{# }); #}',
                '{# } #}',
            '</tbody>',
        '</table>',
        '{# if(totalRows > 0) { #}',
        '<div class="ReportFooter ResourcePagerControl DPA_Paginator">',
            '{# if(currentPage === 1) { #}',
                '<span style="color:#646464;"><img src="/Orion/images/Arrows/button_white_paging_first_disabled.gif" style="vertical-align:middle"></span>',
                '  |  ',
                '<span style="color:#646464;"><img src="/Orion/images/Arrows/button_white_paging_previous_disabled.gif" style="vertical-align:middle"></span>',
            '{# } else { #}',
                '<a href="#" class="firstPage NoTip"><img src="/Orion/images/Arrows/button_white_paging_first.gif" style="vertical-align:middle"></a>',
                '  |  ',
                '<a href="#" class="previousPage NoTip"><img src="/Orion/images/Arrows/button_white_paging_previous.gif" style="vertical-align:middle"></a>',
            '{# } #}',
            '  |  {{pageLabel}} ',
            '<input type="text" class="pageNumber SmallInput" value="{{currentPage}}"> {{SW.Core.String.Format(ofFormat, totalPages)}}  |  ',
            '{# if(currentPage !== totalPages) { #}',
                '<a href="#" class="nextPage NoTip"><img src="/Orion/images/Arrows/button_white_paging_next.gif" style="vertical-align:middle"></a>',
                '  |  ',
                '<a href="#" class="lastPage NoTip"><img src="/Orion/images/Arrows/button_white_paging_last.gif" style="vertical-align:middle"></a>',
            '{# } else { #}',
                '<span style="color:#646464;"><img src="/Orion/images/Arrows/button_white_paging_next_disabled.gif" style="vertical-align:middle"></span>',
                '  |  ',
                '<span style="color:#646464;"><img src="/Orion/images/Arrows/button_white_paging_last_disabled.gif" style="vertical-align:middle"></span>',
            '{# } #}',
            ' |  {{itemsOnPageLabel}} ',
            '<input type="text" class="pageSize SmallInput" value="{{pageSize}}">',
            '  |  ',
            '<a href="#" class="showAll NoTip">{{showAllLabel}}</a>',
            '  |  ',
            '<div class="ResourcePagerInfo"> {{ SW.Core.String.Format(displayingObjectsLabel, (currentPage - 1) * pageSize + 1, (currentPage * pageSize) > totalRows ? totalRows : (currentPage * pageSize), totalRows) }} </div>',
        '</div>',
        '{# } #}'
    ]);
    var isNodeFunctionalityAllowed = config.isNodeFunctionalityAllowed;

    var loadingIcon = new SW.DPA.UI.LoadingIcon(config.loadingIconId);
    var errorMessage = new SW.DPA.UI.ErrorMessage(config.errorMessageId);

    var loadStorages = function (page, pageSize) {
        var searchQuery = searchControl.getSearchValue();
        loadingIcon.show();
        errorMessage.hide();
        SW.Core.Services.callController(
            "/api/DpaStorageObjectsByPerformanceRisk/GetAll",
            {
                page: page,
                pageSize: pageSize,
                query: searchQuery
            },
            function (response) {
                loadingIcon.hide();
                if (response.Metadata.IntegrationEstablished) {
                    contentWrapper.show();
                    noIntegrationContentWrapper.hide();

                    var totalPages = parseInt(response.TotalRows / pageSize + (response.TotalRows % pageSize === 0 ? 0 : 1));
                    
                    var viewModel = $.extend({
                        rows: _.map(response.Data, function (item) {
                            return $.extend(item,
                            {
                                UniqueClientID: config.contentId + item.StorageId
                            });
                        }),
                        totalPages: totalPages,
                        currentPage: page,
                        pageSize: pageSize,
                        totalRows: response.TotalRows,
                        getCellThresholdCssClass: function(status) {
                            if (status === SW.DPA.MetricStatus.critical)
                                return 'DPA_CriticalCell';
                            else if (status === SW.DPA.MetricStatus.warning)
                                return 'DPA_WarningCell';
                            else
                                return '';
                        }
                    }, localizedStrings);
                    
                    contentWrapper.html(tableTemplate(viewModel));
                    
                    // bind expand icon
                    contentWrapper.find('.DPA_ExpandIcon').click(function () {
                        var storageId = $(this).data('lun-id');
                        var expandableBlock = contentWrapper.find('.DPA_ExpandableBlock' + storageId);
                        if ($(this).hasClass('DPA_Collapsed')) {
                            expandableBlock.show();
                            $(this).removeClass('DPA_Collapsed').addClass('DPA_Expanded');

                            var loaded = expandableBlock.data('content-loaded');
                            var isLoaded = loaded && parseInt(loaded) === 1;
                            if (!isLoaded) {
                                that.loadDatabaseInstanceTable(storageId,
                                    function () {
                                        expandableBlock.data('content-loaded', 1);
                                    },
                                    isNodeFunctionalityAllowed);
                            }
                        } else {
                            expandableBlock.hide();
                            $(this).removeClass('DPA_Expanded').addClass('DPA_Collapsed');
                        }
                    });

                    // bind paginator
                    contentWrapper.find('.DPA_Paginator a.firstPage').click(function () {
                        loadStorages(1, getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator a.previousPage').click(function () {
                        loadStorages(page - 1, getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator a.nextPage').click(function () {
                        loadStorages(page + 1, getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator a.lastPage').click(function () {
                        loadStorages(totalPages, getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator .pageSize').change(function () {
                        loadStorages(page, getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator .pageSize').keydown(function (e) {
                        if (e.keyCode == 13) {
                            loadStorages(page, getPageSizeFromPaginator());
                            return false;
                        }
                        return true;
                    });

                    contentWrapper.find('.DPA_Paginator .pageNumber').change(function () {
                        loadStorages(getPageNumberFromPaginator(), getPageSizeFromPaginator());
                        return false;
                    });

                    contentWrapper.find('.DPA_Paginator .pageNumber').keydown(function (e) {
                        if (e.keyCode == 13) {
                            loadStorages(getPageNumberFromPaginator(), getPageSizeFromPaginator());
                            return false;
                        }
                        return true;
                    });

                    contentWrapper.find('.DPA_Paginator .showAll').click(function () {
                        loadStorages(1, response.TotalRows);
                        return false;
                    });

                    // bind cell clicks
                    contentWrapper.find('.DPA_StorageObjectsExpadabledGridColLatency').each(function (index) {
                        var netObject = $(this).parent().data('netobject');
                        SW.SRM.Redirector.bindRedirectClick($(this), function () { clickOnLatency(netObject); });
                    });

                    contentWrapper.find('.DPA_StorageObjectsExpadabledGridColIops').each(function (index) {
                        var netObject = $(this).parent().data('netobject');
                        SW.SRM.Redirector.bindRedirectClick($(this), function () { clickOnIops(netObject); });
                    });

                    contentWrapper.find('.DPA_StorageObjectsExpadabledGridColThroughput').each(function (index) {
                        var netObject = $(this).parent().data('netobject');
                        SW.SRM.Redirector.bindRedirectClick($(this), function () { clickOnThroughput(netObject); });
                    });

                } else {
                    noIntegrationContentWrapper.show();
                    contentWrapper.hide();
                }
            },
            function(error) {
                loadingIcon.hide();
                errorMessage.show(error);
            });
    };

    loadStorages(1, defaultPageSize);

    var getPageSizeFromPaginator = function () {
        var pageSizeElement = contentWrapper.find('.DPA_Paginator .pageSize');
        if (pageSizeElement.length > 0) {
            var pageSize = pageSizeElement.val();
            return pageSize;
        } else {
            return defaultPageSize;
        }
    };

    var getPageNumberFromPaginator = function () {
        var pageNumber = contentWrapper.find('.DPA_Paginator .pageNumber').val();
        return pageNumber;
    };
    
    var clickOnIops = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'IOPS',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnLatency = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Latency',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    var clickOnThroughput = function (netObject) {
        var args = {
            parameters: {
                ResourceFile: detachedResource,
                Width: resourceWidth,
                DataBindMethod: performanceServiceMethod,
                RequestParameter: 'Throughput',
                NetObject: netObject
            }
        };
        SW.SRM.Redirector.Redirect(args);
    };

    this.loadDatabaseInstanceTable = function (lunId, onSuccessCallback, isNodeFunctionalityAllowed) {
        var databaseInstancesConfig = {
            isNodeFunctionalityAllowed: isNodeFunctionalityAllowed,
            uniqueClientId: config.contentId.toString() + '-' + lunId,
            url: "/api/DpaDatabaseInstances/GetDatabaseInstancesByLun",
            getParameters: function () {
                return {
                    LunId: lunId
                };
            },
            onSuccess: function (result) {
                if (onSuccessCallback) {
                    onSuccessCallback();
                }
                return true;
            }
        };

        var grid = new DPA.UI.DatabaseInstancesGrid.Grid(contentWrapper.find(".DPA_ExpandableBlock" + lunId + ' .DPA_TableWrapper'), databaseInstancesConfig);
        grid.init(function () {
            grid.refresh();
        });
    };
};
