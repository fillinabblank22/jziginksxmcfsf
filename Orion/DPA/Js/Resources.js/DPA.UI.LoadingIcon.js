﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.LoadingIcon = function(id) {
    var loadingBlock = $('#' + id);
    var loadingLongerBlock = loadingBlock.find('.DPA_LoadingLongerText');
    var longerThanUsualTimeout = 5000;
    var timeoutId = 0;

    this.show = function () {
        loadingLongerBlock.hide();
        loadingBlock.show();

        timeoutId = setTimeout(function () {
             loadingLongerBlock.show();
        }, longerThanUsualTimeout);
    };

    this.hide = function() {
        loadingBlock.hide();
        loadingLongerBlock.hide();
        if (typeof timeoutId !== "undefined") {
            clearTimeout(timeoutId);
        }
    };
};

