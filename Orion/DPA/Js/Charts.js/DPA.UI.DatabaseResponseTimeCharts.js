﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.DatabaseResponseTimeCharts = SW.DPA.UI.DatabaseResponseTimeCharts || {};

(function (charts) {

    charts.configStrings = {
        yAxisTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_yAxisTitle;E=js}',
        topQueriesChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_QueryTab_ChartTitle;E=js}',
        topWaitsChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_WaitsTab_ChartTitle;E=js}',
        topWaitInstrumentsChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_WaitInstrumentsTab_ChartTitle;E=js}',
        topOperationsChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_OperationsTab_ChartTitle;E=js}',
        topAppsChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_ApplicationsTab_ChartTitle;E=js}',
        topDatabasesChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_DatabasesTab_ChartTitle;E=js}',
        topMachinesChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_MachinesTab_ChartTitle;E=js}',
        topDBUsersChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_DatabaseResponseTime_DBUsersTab_ChartTitle;E=js}',
        loadingLongerText: [
            '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js} ',
                '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">',
                    '&#187; @{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
                '</a>'
        ].join('')
    };

    charts.tooltipStrings = {
        query: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Query;E=js}',
        sqlhash: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_SQLHash;E=js}',
        waittime: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_WaitTime;E=js}',
        totalwaittime: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_TotalWaitTime;E=js}',
        average: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Average;E=js}',
        executions: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Executions;E=js}',
        wait: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Wait;E=js}',
        waitinstrument: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_WaitInstrument;E=js}',
        operation: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Operation;E=js}',
        program: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Program;E=js}',
        database: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Database;E=js}',
        machine: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_Machine;E=js}',
        dbuser: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_DBUser;E=js}',
        totalpercentage: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_TotalPercentage;E=js}',
        notavailable: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_NotAvailable;E=js}',
        showmore: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_More;E=js}'
    };

    charts.listeners = {
        query: {
            click: function (e) {
                var series = e && e.point && e.point.series,
                    props = series && series.options && series.options.customProperties,
                    url = props && props.DetailUrl;
                url && window.open(url, '_blank');
            }
        },
        waits: { click: function (e) { return; } },
        waitinstruments: { click: function (e) { return; } },
        operations: { click: function (e) { return; } },
        apps: { click: function (e) { return; } },
        db: { click: function (e) { return; } },
        nodes: { click: function (e) { return; } },
        dbu: { click: function (e) { return; } },
        // remaining, other, ... series
        special: {
            click: function (e) {
                return;
            }
        },
    };

    charts.tooltipTemplates = {};
    charts.tooltipTemplates.query = _.template([
        '<div id="DPA_HighchartTooltip">',
            '<table>',
                // build the header
                '<tr><td colspan="2" class="hint">{{ headerDateTime }}</td></tr>',

                // build the tooltip table
                '<tr>',
                    '<td class="label">{{ labels.query }}</td>',
                    '<td class="ellipsis{# if(isRemaining){ #} remaining{# } #}" style="color:{{ color }};">',
                        '{{ options.customProperties.SqlLabel }}',
                    '</td>',
                '</tr>',
                '<tr {# if(isRemaining){ #}class="disabled"{# } #}>',
                    '<td class="label">{{ labels.sqlhash }}</td>',
                    '<td class="sql-hash">{{ options.customProperties.SqlHash }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.waittime }}</td>',
                    '<td>{{ TooltipTimeFormatter(value) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.executions }}</td>',
                    '<td>{{ point.executions }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label"><span>{{ labels.totalwaittime }}</span></td>',
                    '<td>{{ TooltipTimeFormatter(options.customProperties.TotalWaitTime) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.average }}</td>',
                    '<td>{{ TooltipTimeFormatter(point.average) }}</td>',
                '</tr>',
                // footer
                '<tr><td colspan="2" class="hint">{{ labels.showmore }}</td></tr>',
            '</table>',
        '</div>'].join('')
    );

    var baseTooltipTpl = function (labelIndex, dataIndex) {
        return _.template([
            '<div id="DPA_HighchartTooltip">',
                '<table>',
                    // build the header
                    '<tr><td colspan="2" class="hint">{{ headerDateTime }}</td></tr>',

                    // build the tooltip table
                    '<tr>',
                        '<td class="label">{{ labels.' + labelIndex + ' }}</td>',
                        '<td class="ellipsis{# if(isRemaining){ #} remaining{# } #}" style="color:{{ color }};">',
                            '{{ ' + dataIndex + ' }}',
                        '</td>',
                    '</tr>',
                    '<tr>',
                        '<td class="label">{{ labels.waittime }}</td>',
                        '<td>{{ TooltipTimeFormatter(value) }}</td>',
                    '</tr>',
                    '<tr>',
                        '<td class="label">{{ labels.totalwaittime }}</td>',
                        '<td>{{ TooltipTimeFormatter(options.customProperties.TotalWaitTime) }}</td>',
                    '</tr>',
                    '<tr>',
                        '<td class="label">{{ labels.totalpercentage }}</td>',
                        '<td>{{ point.totalpercentage }} %</td>',
                    '</tr>',
                '</table>',
            '</div>'].join('')
        );
    }

    charts.tooltipTemplates.waits = baseTooltipTpl('wait', 'options.customProperties.WaitName');
    charts.tooltipTemplates.waitinstruments = baseTooltipTpl('waitinstrument', 'options.customProperties.WaitInstrument');
    charts.tooltipTemplates.operations = baseTooltipTpl('operation', 'options.customProperties.Operation');
    charts.tooltipTemplates.apps = baseTooltipTpl('program', 'options.customProperties.Program');
    charts.tooltipTemplates.db = baseTooltipTpl('database', 'options.customProperties.Database');
    charts.tooltipTemplates.nodes = baseTooltipTpl('machine', 'options.customProperties.Machine');
    charts.tooltipTemplates.dbu = baseTooltipTpl('dbuser', 'options.customProperties.DBUser');

    var _getTooltipSeries = function () {
        var items = this.points || $.isArray(this) ? this : [this], item = items[0];
        if (!item) return null;

        var series = $.extend({}, item.series), time = item.key;
        // time and value axis
        series.time = item.x;
        series.value = item.y;
        series.point = item.point;

        // common values
        series.labels = $.extend({}, charts.tooltipStrings);
        series.headerDateTime = series.tooltipHeaderFormatter(time);
        series.total = item.total || 0;
        series.percentage = (item.percentage || 0).toFixed(2);
        series.isRemaining = !!series.options.customProperties.IsRemaining;

        // calculations
        series.point.totalpercentage = (series.value / series.options.customProperties.TotalWaitTime * 100).toFixed(2);

        // supporting finctions
        series.TooltipTimeFormatter = SW.Core.Charts.dataFormatters.waitTimeMsTwoUnit;
        return series;
    };

    charts.tooltipFormatter = function (templateId) {
        return function () {
            var series = _getTooltipSeries.apply(this),
                template = charts.tooltipTemplates[templateId];
            return series && template ? template(series) : '';
        };
    };

    charts.tooltipPositioner = function (boxWidth, boxHeight, point) {
        // follow mouse pointer
        var chart = this.chart, distance = this.options.distance || 12,
            x = point.plotX + chart.plotLeft + (chart.inverted ? distance : -boxWidth - distance),
            y = point.plotY - boxHeight + chart.plotTop + 16;
        if (x < 16) { x = chart.plotLeft + point.plotX + distance; }
        return { x: x, y: y };
    };

    var _defaultChartSettings =
    {
        chart: { type: "column" },
        scrollbar: { enabled: true },
        rangeSelector: { enabled: true },
        navigator: {
            enabled: true, series: {
                type: "area",
                index: -1,                                  // to be excluded from mapping
                step: true
            }
        },
        tooltip: {
            enabled: true,
            shared: false,
            crosshairs: false,
            formatter: undefined,                           // customize
            positioner: charts.tooltipPositioner,
            headerFormat: "{point.key}"
        },
        title: {
            text: undefined,                                // customize
            style: {
                whiteSpace: 'normal',
                left: '0px',
                top: '0px',
                position: 'absolute'
            },
            useHTML: true,
        },
        yAxis: [{
            title: {
                margin: 10,
                text: charts.configStrings.yAxisTitle
            },
            unit: "sec",
            unitFormatter: "sec",
            labels: { align: "right", x: -8 },
            startOnTick: false,
            endOnTick: true
        }],
        seriesTemplates: {
            WaitTime: {
                type: "column",
                name: "",
                events: undefined,                          // customize
                index: 1000, zIndex: 1000
            },
            RemainingTime: {
                type: "column",
                name: "",
                color: "#D0D0D0",
                events: charts.listeners['special'],
                events: undefined,                          // customize
                index: 0, zIndex: 0,
                visible: false
            },
            WaitTimeZoom: {
                type: "column",
                visible: false,
                showInLegend: false,
                index: 1000, zIndex: 1000,
                pointRange: 0.5 * 3600000
            },
            RemainingTimeZoom: {
                type: "column",
                color: "#D0D0D0",
                events: charts.listeners['special'],
                visible: false,
                showInLegend: false,
                index: 0, zIndex: 0,
                pointRange: 0.5 * 3600000
            }
        },
        xAxis: {
            minRange: 3600000 // one hour
        },
        plotOptions: {
            column: {
                stacking: "normal",
                dataGrouping: { enabled: false }
            }
        },
        loading: {
            loadingLongerText: charts.configStrings.loadingLongerText,
            loadingLongerStyle: 'DPA_LoadingLongerText',
            loadingLongerThanUsualTimeout: 5000
        },
        noDataText: charts.configStrings.noDataInChartsText
    };

    charts.settings = {
        query: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('query') },
            title: { text: charts.configStrings.topQueriesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['query'] },
                WaitTimeZoom: { events: charts.listeners['query'] },
                RemainingTime: { events: charts.listeners['query'] },
                RemainingTimeZoom: { events: charts.listeners['query'] }
            }
        }),
        waits: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('waits') },
            title: { text: charts.configStrings.topWaitsChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['waits'] },
                WaitTimeZoom: { events: charts.listeners['waits'] }
            }
        }),
        waitinstruments: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('waitinstruments') },
            title: { text: charts.configStrings.topWaitInstrumentsChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['waitinstruments'] },
                WaitTimeZoom: { events: charts.listeners['waitinstruments'] }
            }
        }),
        operations: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('operations') },
            title: { text: charts.configStrings.topOperationsChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['operations'] },
                WaitTimeZoom: { events: charts.listeners['operations'] }
            }
        }),
        apps: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('apps') },
            title: { text: charts.configStrings.topAppsChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['apps'] },
                WaitTimeZoom: { events: charts.listeners['apps'] }
            }
        }),
        db: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('db') },
            title: { text: charts.configStrings.topDatabasesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['db'] },
                WaitTimeZoom: { events: charts.listeners['db'] }
            }
        }),
        nodes: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('nodes') },
            title: { text: charts.configStrings.topMachinesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['nodes'] },
                WaitTimeZoom: { events: charts.listeners['nodes'] }
            }
        }),
        dbu: $.extend(true, {}, _defaultChartSettings, {
            tooltip: { formatter: charts.tooltipFormatter('dbu') },
            title: { text: charts.configStrings.topDBUsersChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners['dbu'] },
                WaitTimeZoom: { events: charts.listeners['dbu'] }
            }
        })
    };

    charts.zoomLevel = function (chart, zoom) {
        if (chart && zoom) chart.options.lastZoomLevel = zoom;
        return chart && chart.options.lastZoomLevel || undefined;
    };

    charts.mapping = function (serie, uniqueId) {
        var uid = uniqueId || serie.options.uniqueId,
            r = /[|](.*?)[|](.+?)$/g.exec(uid) || [];

        return r.length ? { uid: uid, serie: serie, id: r[1], zoom: r[2] } : null;
    };

    charts.mapSeries = function (series, zoomset) {
        var map = {};
        _.each(zoomset || [], function (zoom) { map[zoom.id] = {}; });
        _.each(series || [], function (s) {
            if (s.options.index >= 0) { // exclude navigator, etc.
                var m = charts.mapping(s);
                if(m) map[m.zoom][m.id] = m.serie;
            }
        });

        return map;
    };

})(SW.DPA.UI.DatabaseResponseTimeCharts);

SW.Core.Charts.xAxisSetExtremesCallbacks = SW.Core.Charts.xAxisSetExtremesCallbacks || [];
SW.Core.Charts.xAxisSetExtremesCallbacks.push(function (xAxis, event) {
    var dpa = SW.DPA.UI.DatabaseResponseTimeCharts,
        zoomset = xAxis && xAxis.options && xAxis.options.ZoomSeriesSet || [],
        series = xAxis && xAxis.series || [];
    // check inputs
    if (!event || !zoomset.length || !series.length) return;

    var zoomLevel = dpa.zoomLevel(xAxis.chart),
        range = Math.abs(event.max - event.min).toFixed(0),
        zoom = _.find(zoomset, function (set) {
            return (set.min == null || set.min < range) && (set.max == null || set.max >= range);
        });

    // check whether zoom level was changed
    if (zoomLevel == zoom.id) return;

    // map series into structure and find visible series according last zoom series visibility
    var mapped = dpa.mapSeries(series, zoomset),
        lastZoom = mapped[zoomLevel || zoomset[0].id],
        visible = _.filter(mapped[zoom.id], function (s, id) { return lastZoom[id].visible; });
    dpa.zoomLevel(xAxis.chart, zoom.id);

    // hide all series (to clear chart)
    $.each(series, function () {
        this.setVisible(false, false);
    });
    // show series with new zoom level
    $.each(visible, function (i, s) {
        s.setVisible(true, false);
    });
});

(function (ns) {
    // when need use exactly the same palete as core, get palete from standard theme like this:
    // var basePalette = ns['standard'].colors[1];
    var basePalette = ["#006ca9", "#49D2F2", "#A87000", "#D8C679", "#515151"];

    var paleteGenerator = function (count, repeat) {
        var i, palette = [];
        for (i = 0; i < repeat; i++)
            palette = palette.concat(basePalette.slice(0, count));
        return palette;
    };

    var zoomColorsGenerator = function (from, to, zooms) {
        var i, colors = [];
        for (i = from; i <= to; i++)
            colors.push(paleteGenerator(i, zooms));
        return colors;
    };

    ns['ZoomColorTheme'] = $.extend(false, {}, ns['standard'], {
        name: 'Blue Steel (Support 3 lvl of zooming)',
        colors: zoomColorsGenerator(1, 5, 3) // 3x color palette from 1 to 5 series
    });
})(SW.Core.Charts.ColorThemes);