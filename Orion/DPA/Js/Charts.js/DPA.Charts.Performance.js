﻿SW.Core.namespace("SW.DPA.Charts");

SW.DPA.Charts.TooltipHighlighter = function (config) {
    var chartOptions = config;
    var chartHilite = {};
    chartOptions = SW.DPA.Charts.EnsureObject(chartOptions, 'chart.events');
    // init series tooltip hilite state
    chartOptions.chart.events.addSeries = function () {
        this.swState = '';
    };
    SW.DPA.Charts.EnsureObject(chartOptions, 'plotOptions.series.events');
    // series tooltip hilite state update
    chartOptions.plotOptions.series.events.mouseOver = function () {
        var series = this, update = false;
        if (chartHilite.lastActiveSeries) {
            if (chartHilite.lastActiveSeries !== series) {
                chartHilite.lastActiveSeries.swState = '';
                update = true;
            }
        } else {
            update = true;
        }
        if (update) {
            chartHilite.lastActiveSeries = series;
            series.swState = 'hover';
            series.chart.redraw();
        }
    };

    return chartOptions;
};

SW.DPA.Charts.EnsureObject = function (source, path) {
    source = source || {};
    var target = source, modules = path.split('.');
    $.each(modules, function () {
        if (!target.hasOwnProperty(this)) {
            target[this] = {};
        }
        target = target[this];
    });
    return source;
};

SW.DPA.Charts.ChartSelectCurrentZoomButton = function (chart) {
    // must be at least 2 (navigator + at least 1 serie)
    if (!chart.series || chart.series.length < 2) {
        return; // data is not ready yet
    }
    if (chart.chartSettings == null || chart.chartSettings == undefined) {
        return;
    }

    $.each(chart.options.rangeSelector.buttons, function (index, item) {
        if (item.tag === chart.chartSettings.initialZoom) {
            chart.rangeSelector.clickButton(index, item, true);
            return false;
        }
    });
};

SW.DPA.Charts.Performance = function () {
    var netObjectId, resId, daysOfDataToLoad, sampleSizeInMinutes, yAxisTitle, chartTitle, chartSubTitle, dataUrl, chartInitialZoom, numberOfSeriesToShow;
    var chartId, poolID, filterField, isLatency;
    var showParent = true;
    var hasParent = false;
    var legendContainerId;
    var that = this;
    this.legendTemplate = _.templateExt([
        '<tr data-id="{{id}}">',
            '<td class="DPA_Toggle"><input type="checkbox" checked="checked" /></td>',
            '<td class="DPA_Symbol"><div style="background-color: {{color}};"></div></td>',
            '<td>',
                '<a href="javascript:void(0);" class="DPA_LegendLink">',
                    '<img src="/Orion/StatusIcon.ashx?size=small&entity={{entity}}&id={{entityID}}&status={{status}}" />',
                '&nbsp;{{label}}</a>',
            '</td>',
        '</tr>']);

    this.templateModelProvider = function (serie) {
        return {
            color: serie.color,
            entity: serie.options.customProperties.SwisEntity,
            entityID: serie.options.customProperties.Id,
            id: serie.options.customProperties.Id,
            status: serie.options.customProperties.Status,
            label: serie.name
        };
    }

    this.onLegendSerieRender = function () { }

    var chartConfig = function () {
        return {
            chart: {
                type: 'line',
                events: {
                    load: function () {
                        SW.DPA.Charts.ChartSelectCurrentZoomButton(this);
                    }
                }
            },
            title: {
                useHTML: true
            },
            yAxis: [{
                title: {
                    margin: 10,
                    text: yAxisTitle,
                    style: "text-transform:none;"
                },
                labels: { align: "right", x: -8 },
                min: 0,
                max: 0.1,
                startOnTick: false,
                endOnTick: true,
                minTickInterval: 1
            }],
            plotOptions: {
                line: {
                    pointPadding: 0,
                    borderColor: "#000000",
                    borderWidth: 1,
                    groupPadding: 0
                }
            },
            rangeSelector: {
                buttons: [{ tag: '1h' }, { tag: '12h' }, { tag: '24h' }] // can't use localized value, so add tags
            },
            xAxis: {
                minRange: 3600 * 1000 //one hour
            },
            tooltip: {
                formatter: chartTooltipFormatter,
            },
            seriesTemplates: {
                Data_Navigator: {
                    name: 'Navigator',
                    id: 'navigator',
                    showInLegend: false,
                    visible: false
                }
            }
        };
    };

    var chartTooltipFormatter = function () {
        var s = '<table><tr><td><b>' + SW.DPA.Formatters.FormatDate(new Date(this.x)) + '</b></td></tr>';
        $.each(this.points, function (i, point) {
            if (point.series.options.customProperties != null && point.series.options.customProperties.CustomProperty == 'Pool' && !showParent)
                return;
            s += '<tr class="' + point.series.swState + '"><td style = "color: ' + point.series.color + '" >' + point.series.name + ': </td><td><b>' + SW.DPA.Formatters.FormatYChartValue(point.y, isLatency === true ? '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency;E=js}' : point.series.name) + '</b></td></tr>';
        });
        s += '</table>';
        return s;
    };

    function setSerieVisibility(checkBox, serie) {
        if (serie.visible) {
            checkBox.removeAttr('checked');
            serie.hide();
        } else {
            checkBox.attr('checked', 'checked');
            serie.show();
        }
    };

    function setLegendAttr(serie) {
        if (serie.options.customProperties != null) {
            var legend = that.legendTemplate(that.templateModelProvider(serie));

            $(legend).appendTo('#' + legendContainerId);
            var rowPrefix = 'tr[data-id=' + serie.options.customProperties.Id + '] ';
            $('#' + legendContainerId)
                .find(rowPrefix + 'td.DPA_Toggle input, ' + rowPrefix + ' td.DPA_Symbol, ' + rowPrefix + ' td a.DPA_LegendLink')
                .click(function () {
                    setSerieVisibility($('#' + legendContainerId + ' ' + rowPrefix + 'td.DPA_Toggle input'), serie);
                });

            that.onLegendSerieRender(legendContainerId, rowPrefix);
        }
    };

    var createChart = function () {
        var refresh = function () {
            var params = {
                'renderTo': chartId,
                'dataUrl': dataUrl,
                'netObjectIds': [netObjectId],
                'daysOfDataToLoad': daysOfDataToLoad,
                'sampleSizeInMinutes': sampleSizeInMinutes,
                'title': chartTitle == null || typeof chartTitle === 'undefined' ? '' : chartTitle,
                'showTitles': true,
                'subtitle': chartSubTitle != "" ? chartSubTitle : "$[ZoomRange]",
                'poolID': poolID,
                'showParent': showParent,
                'hasParent': hasParent,
                'filterField': filterField != undefined ? filterField : "",
                'initialZoom': chartInitialZoom,
                'numberOfSeriesToShow': numberOfSeriesToShow
            };
            SW.Core.Charts.initializeStandardChart(params, SW.DPA.Charts.TooltipHighlighter(chartConfig()), function () {
                var chart = $('#' + chartId).data();

                if (chart.options.hideResource === true) {
                    $("div.ResourceWrapper[resourceid='" + resId + "']").hide();
                }

                var performanceBtns = $('#srm-performance-chart-buttons-' + resId);
                if (performanceBtns.length) {
                    performanceBtns.css('margin-left', chart.chartWidth - 225 + 'px');
                }
                $('#' + legendContainerId).empty();
                $.each(chart.series, function (i, serie) {
                    if (serie.name == 'Navigator') {
                        return;
                    }
                    setLegendAttr(serie);
                });
                var wrapperHeight = $('#srm-performance-chart-' + resId).closest('.ResourceWrapper').height();
                $('#srm-performance-chart-' + resId).closest('.ResourceWrapper').css('min-height', wrapperHeight);
            });
        };
        if (typeof (SW.Core.View) != 'undefined' && typeof (SW.Core.View.AddOnRefresh) != 'undefined') {
            SW.Core.View.AddOnRefresh(refresh, chartId);
        }
        refresh();
    };

    this.init = function (latency, info) {
        isLatency = latency;
        netObjectId = info.NetObjectId;
        resId = info.ResourceId;
        sampleSizeInMinutes = info.SampleSizeInMinutes;
        numberOfSeriesToShow = info.NumberOfSeriesToShow;
        daysOfDataToLoad = info.DaysOfDataToLoad;
        yAxisTitle = info.YAxisTitle;
        chartTitle = info.ChartTitle;
        chartSubTitle = info.ChartSubTitle ? info.ChartSubTitle : "";
        dataUrl = info.DataUrl;
        poolID = info.PoolID;
        chartId = 'srm-performance-chart-' + resId;
        legendContainerId = 'srm-performance-legend-' + resId;
        filterField = info.FilterField;
        hasParent = info.HasParent;
        showParent = info.ShowParent != undefined ? info.ShowParent : true;
        chartInitialZoom = info.ChartInitialZoom;
        createChart();
    };
};
