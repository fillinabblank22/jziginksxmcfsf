﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.ChartLegends = SW.DPA.UI.ChartLegends || {};
SW.DPA.UI.ChartLegends.DatabaseResponseTime = SW.DPA.UI.ChartLegends.DatabaseResponseTime || {};

(function (legend) {
    var localizedStrings = {
        expertAdviceInfoCloseButtonLabel: '@{R=DPA.Strings;K=ExpertAdviceInfo_Close;E=js}',
        expertAdviceInfoTitle: '@{R=DPA.Strings;K=ExpertAdviceInfo_Title;E=js}'
    };
    legend.legendSeriesTemplates = {};
    legend.legendSeriesTemplates.query = SW.DPA.UI.ChartLegends.QueryBase.QueryLegendTemplate;

    legend.legendSeriesTemplates.waits = _.template([
      '{# _.each(legendSeries, function(s){ #}',
        '{# if (s.show) { #}',
          '<tr>',
            '<td class="toggle">',
                '<input type="checkbox" data-uid="{{ s.uid }}" {# if(s.visible){ #}checked="checked"{# } #}/>',
            '</td>',
            '<td class="symbol">{{ AddLegendSymbol(s) }}</td>',
			'<td class="label" data-uid="{{ s.uid }}">',
                '{{ s.name }}',
                '{# if(!s.isRemaining) { #}',
                    '<a href="javascript: return true;" class="expertinfo" data-waitid="{{ s.name }}"><img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" alt=""/></a>',
                '{# } #}',
            '</td>',
          '</tr>',
        '{# } #}',
      '{# }); #}'].join('')
    );

    var basicLegendTpl = _.template([
      '{# _.each(legendSeries, function(s){ #}',
        '{# if (s.show) { #}',
          '<tr>',
            '<td class="toggle">',
                '<input type="checkbox" data-uid="{{ s.uid }}" {# if(s.visible){ #}checked="checked"{# } #}/>',
            '</td>',
            '<td class="symbol">{{ AddLegendSymbol(s) }}</td>',
			'<td class="label" data-uid="{{ s.uid }}">{{ s.name }}</td>',
          '</tr>',
        '{# } #}',
      '{# }); #}'].join('')
    );
    legend.legendSeriesTemplates.waitinstruments = basicLegendTpl;
    legend.legendSeriesTemplates.operations = basicLegendTpl;
    legend.legendSeriesTemplates.apps = basicLegendTpl;
    legend.legendSeriesTemplates.db = basicLegendTpl;
    legend.legendSeriesTemplates.nodes = basicLegendTpl;
    legend.legendSeriesTemplates.dbu = basicLegendTpl;
    
	legend.toggleSeries = function (series) {
	    series.visible ? series.hide() : series.show();
	};
    
	legend.createChartLegend = function (chart, url, legendContainerId, legentChartType) {
	    var table = $('#' + legendContainerId).empty();
	    var template = legend.legendSeriesTemplates[legentChartType];
        var xAxis = chart.xAxis[0] || {},
            zoomset = xAxis.options && xAxis.options.ZoomSeriesSet || [];

	    var data = {
	        chartSeries: chart.series,
	        mapped: SW.DPA.UI.DatabaseResponseTimeCharts.mapSeries(chart.series, zoomset),
	        legendSeries: _.map(_.sortBy(chart.series, function (s) {
	                return s.options.customProperties.LegendIndex || 0;
	        }), function (s) {
	            return {
	                show: s.options.showInLegend,
	                visible: s.visible && !s.options.customProperties.IsRemaining,
	                uid: s.options.uniqueId,
	                name: s.name,
	                color: s.color,
	                isRemaining: s.options.customProperties.IsRemaining,
	                url: s.options.customProperties.DetailUrl,
	                custom: s.options.customProperties
	            };
	        }),
	        AddLegendSymbol: SW.DPA.UI.ChartLegends.QueryBase.addLegendSymbol,
	        formatQuery: SW.DPA.UI.ChartLegends.QueryBase.formatQuery
	    };

	    table && template && table.append(template(data));

	    // checkboxes to toggle series
	    table.find("input:checkbox").click(function () {
            var dpa = SW.DPA.UI.DatabaseResponseTimeCharts,
                uid = $(this).data('uid'),
                id = dpa.mapping(null, uid).id,
                zoom = dpa.zoomLevel(xAxis.chart),
                series = data.mapped[zoom][id];

	        series.visible ? series.hide() : series.show();
	    });

	    var loadExpertAdviceInfo = function (el, params) {
	        SW.Core.Services.callController("/api/DpaExpertAdviceInfo/GetInfoText", params,
                function (result) {
                    var infotext = $('<div/>').html(result);
                    el.empty().append(infotext);
                },
                function (errorResult) { }
            );
	    };
	    
	    // expert info pop-up
	    var netObjectIds = url && url.request && url.request.NetObjectIds || [];
	    table.find("a.expertinfo").unbind('click').bind('click', function (e) {
	        e.preventDefault(); e.stopPropagation();

	        var expertinfo = $('<div id="DPA_ExpertInfo"/>'),
                infotext = $('<div id="infotext"/>').appendTo(expertinfo).scrollTop(0),
                buttons = $('<div id="buttons"><br/></div>').appendTo(expertinfo);

	        var loader = $('<div class="loading"/>').appendTo(infotext);

	        $(SW.Core.Widgets.Button(localizedStrings.expertAdviceInfoCloseButtonLabel, { type: 'secondary' })).appendTo(buttons)
                .click(function () { infotext.scrollTop(0); expertinfo.dialog('close').remove(); });

	        expertinfo.dialog({ modal: true, minWidth: 640, minHeight: 240, resizable: false, title: localizedStrings.expertAdviceInfoTitle });
	        loadExpertAdviceInfo(infotext, { NetObjectIds: netObjectIds, WaitId: $(this).data('waitid') });
	    });

	    // expanders
	    var expanderWrappers = table.find('td.expander');
        var expanders = table.find("td.expander span.expander-button");
	    expanders.click(function () {
	        var expandWrapper = $(this).parent();
	        var isExpanded = expandWrapper.hasClass("collapsed");
	        expanderWrappers.addClass("collapsed");
	        if (isExpanded) {
	            var scrollTo = expandWrapper.removeClass("collapsed").offset().top;

	            if (scrollTo < $(window).scrollTop()) {
	                $('html, body').animate({
	                    scrollTop: scrollTo
	                }, 500);
                }
	        }
	    });
	    expanderWrappers.find(".expander-body a").click(function (e) { e.stopPropagation(); });
	};
})(SW.DPA.UI.ChartLegends.DatabaseResponseTime);