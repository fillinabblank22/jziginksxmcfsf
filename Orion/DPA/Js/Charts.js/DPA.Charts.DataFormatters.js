﻿SW.Core.namespace("SW.DPA.Charts.DataFormatters");
(function (ctx) {
    ctx.units = {
        waitTime : {
            's': { min: null, max: 60, format: function(v) { return v + '@{R=DPA.Strings;K=WaitTimeChartUnits_Seconds;E=js}'; } },
            'min': { min: 60, max: 60 * 60, format: function (v) { return v + ' ' + '@{R=DPA.Strings;K=WaitTimeChartUnits_Minutes;E=js}'; } },
            'hour': { min: 60 * 60, max: 60 * 60 * 24, format: function(v) { return v + ' ' + (v != 1 ? '@{R=DPA.Strings;K=WaitTimeChartUnits_Hours;E=js}' : '@{R=DPA.Strings;K=WaitTimeChartUnits_Hour;E=js}'); } },
            'day': { min: 60 * 60 * 24, max: null, format: function(v) { return v + ' ' + (v != 1 ? '@{R=DPA.Strings;K=WaitTimeChartUnits_Days;E=js}' : '@{R=DPA.Strings;K=WaitTimeChartUnits_Day;E=js}'); } },
        },
        waitTimeNUnit: {
            's': { min: null, max: 60, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Second;E=js}'; } },
            'm': { min: 60, max: 60 * 60, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Minute;E=js}'; } },
            'h': { min: 60 * 60, max: 60 * 60 * 24, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Hour;E=js}'; } },
            'day': {
                min: 60 * 60 * 24, max: null, format: function (v) {
                    return v + ' ' + (v != 1 ? '@{R=DPA.Strings;K=TooltipTimeChartUnits_Days;E=js}' : '@{R=DPA.Strings;K=TooltipTimeChartUnits_Day;E=js}');
                }
            }
        },
        waitTimeMsNUnit: {
            'ms': { min: null, max: 1, format: function (v) { return (v * 1000) + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Milliseconds;E=js}'; } },
            's': { min: 1, max: 60, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Second;E=js}'; } },
            'm': { min: 60, max: 60 * 60, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Minute;E=js}'; } },
            'h': { min: 60 * 60, max: 60 * 60 * 24, format: function (v) { return v + '@{R=DPA.Strings;K=TooltipTimeChartUnits_Hour;E=js}'; } },
            'day': {
                min: 60 * 60 * 24, max: null, format: function (v) {
                    return v + ' ' + (v != 1 ? '@{R=DPA.Strings;K=TooltipTimeChartUnits_Days;E=js}' : '@{R=DPA.Strings;K=TooltipTimeChartUnits_Day;E=js}');
                }
            }
        }
    };

    ctx.getUnit = function (value, units) {
        return _.find(units, function (unit) {
            return (unit.min == null || unit.min <= value) &&
                    (unit.max == null || unit.max > value);
        });
    };

    ctx.waitTimeNUnitFormat = function (value, units, numberOfUnits) {
        var unit = ctx.getUnit(value, units),
            num = (unit.min != null ? Math.floor(value / unit.min) : value),
            result = unit.format(num);

        value -= num * unit.min;
        if ((numberOfUnits <= 1 || value < 1) || (unit.min == null)) return result;
        return result + ' ' + ctx.waitTimeNUnitFormat(value, units, (numberOfUnits - 1));
    }

    ctx.formatWaitTime = function (value, valueForUnitCount) {
        var targetUnit = SW.DPA.Charts.DataFormatters.getUnit(valueForUnitCount || value, SW.DPA.Charts.DataFormatters.units.waitTime);
        var number = (targetUnit.min != null ? (value / targetUnit.min) : value).toFixed(0);
        return targetUnit.format(number);
    }
})(SW.DPA.Charts.DataFormatters);

SW.Core.namespace("SW.Core.Charts.dataFormatters");
SW.Core.Charts.dataFormatters.waitTime = function(value) {
    //if axis does not exist or does not have set tickInterval, count target unit from current value
    var valueForCountingTargetUnit = (this.axis && this.axis.tickInterval) || value;
    return SW.DPA.Charts.DataFormatters.formatWaitTime(value, valueForCountingTargetUnit);
};

SW.Core.Charts.dataFormatters.waitTimeTwoUnit = function (value) {
    return $.isNumeric(value) ? SW.DPA.Charts.DataFormatters.waitTimeNUnitFormat(value, SW.DPA.Charts.DataFormatters.units.waitTimeNUnit, 2) : (value || "");
};

SW.Core.Charts.dataFormatters.waitTimeMsTwoUnit = function (value) {
    return $.isNumeric(value) ? SW.DPA.Charts.DataFormatters.waitTimeNUnitFormat(value, SW.DPA.Charts.DataFormatters.units.waitTimeMsNUnit, 2) : (value || "");
};