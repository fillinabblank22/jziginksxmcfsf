﻿SW.Core.namespace("SW.DPA.Charts.TooltipFormatters");
(function (ctx) {
    var strings = {
        clickForMoreDetailsFromDPA: '@{R=DPA.Strings;K=Blockers_Chart_Tooltip_ClickForMoreLabel;E=js}',
        blockersSeriesLabel: '@{R=DPA.Strings;K=Blockers_Chart_Tooltip_SeriesLabel;E=js}',
        executionsNotAvailabled: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_NotAvailable;E=js}',
        executionsUnitLabel: '@{R=DPA.Strings;K=SqlDetailExecutionChartUnitLabel;E=js}'
    };

    var sqlDetailsExecutionsTemplate = _.templateExt([
        '{{header}}', // header
        '{# _.each(points, function(point){ #}',
        '<tr style="line-height: 90%; font-weight: bold;">',
        '<td style="color: {{ point.series.color }}; font-weight:bold; padding-top: 4px; font-size: 12px; border: 0px;">',
        '{{ point.series.name }}',
        '</td>',
        '<td style="border: 0px; font-size: 12px; padding-top: 4px;padding-right:4px">',
        '<b>{{ PointValueFormatter(point.y) }}</b>',
        '</td>',
        '</tr>',
        '{# }); #}',
        '</table></div>' // footer
    ]);
    ctx.sqlDetailsExecutions = function () {
        var series = this.points[0].series;
        var data = {
            header: series.tooltipHeaderFormatter(this.points[0].key),
            points: this.points,
            PointValueFormatter: function (y) { return y == 0 ? strings.executionsNotAvailabled : y; }
        };

        // don't show tooltip for null values
        if (typeof this.points[0].y === 'undefined')
            return false;

        return sqlDetailsExecutionsTemplate(data);
    };

    var sqlDetailsTotalResponseTimeTemplate = _.templateExt([
    '{{header}}', // header
    '{# _.each(points, function(point){ #}',
    '<tr style="line-height: 90%; font-weight: bold;">',
    '<td style="color: {{ point.series.color }}; font-weight:bold; padding-top: 4px; font-size: 12px; border: 0px;">',
    '{{ point.series.name }}',
    '</td>',
    '<td style="border: 0px; font-size: 12px; padding-top: 4px;padding-right:4px">',
    '<b>{{ PointValueFormatter(point.y) }}</b>',
    '</td>',
    '</tr>',
    '{# }); #}',
    '</table></div>' // footer
    ]);
    ctx.sqlDetailsTotalResponseTime = function () {
        var series = this.points[0].series;
        var data = {
            header: series.tooltipHeaderFormatter(this.points[0].key),
            points: this.points,
            PointValueFormatter: function (y) { return SW.Core.Charts.dataFormatters.waitTimeTwoUnit(y); }
        };

        // don't show tooltip for null values
        if (typeof this.points[0].y === 'undefined')
            return false;

        return sqlDetailsTotalResponseTimeTemplate(data);
    };

    var databaseInstancesWaitTimeTemplate = _.templateExt([
        '{{header}}', // header
        '{# _.each(points, function(point){ #}',
        '<tr style="line-height: 90%; font-weight: bold;">',
        '<td style="color: {{ point.series.color }}; font-weight:bold; padding-top: 4px; font-size: 12px; border: 0px;">',
        '{{ point.series.name }}',
        '</td>',
        '<td style="border: 0px; font-size: 12px; padding-top: 4px;">',
        '<b>{{ PointValueFormatter(point.y) }}</b>',
        '</td>',
        '</tr>',
        '{# }); #}',
        '</table></div>' // footer
    ]);
    ctx.databaseInstancesWaitTime = function () {
        var series = this.points[0].series;
        series.tooltipOptions.xDateFormat = "%A, %b %e, %Y";
        var  data = {
            header: series.tooltipHeaderFormatter(this.points[0].key),
            points: this.points,
            PointValueFormatter: function (y) { return SW.Core.Charts.dataFormatters.waitTimeTwoUnit(y); }
        };

        return databaseInstancesWaitTimeTemplate(data);
    };

    var blockersTemplate = _.templateExt([
        '<div id="DPA_HighchartTooltip">',
        '<table>',
        // build the header
        '<tr><td colspan="2" class="hint">{{ headerDateTime }}</td></tr>',
        '<tr style="line-height: 90%; font-weight: bold;">',
        '<td style="color: {{ point.series.color }}; font-weight:bold; padding-top: 4px; padding-right: 10px; font-size: 12px; border: 0px;">{{{blockedSessionsLabel}}}</td>',
        '<td style="border: 0px; font-size: 12px; padding-top: 4px;">{{blockedSessionsValue}}</td>',
        '</tr>',
        '<tr><td colspan="2" class="hint" style="padding-top:3px">{{ showmore }}</td></tr>',
        '</table></div>' // footer
    ]);

    ctx.blockers = function() {   
        var point = this.points[0];
        var series = point.series;
        var showmore = '';
        if (point.y > 0) {
            showmore = strings.clickForMoreDetailsFromDPA;
        }
        var data = {
            point: point,
            headerDateTime: series.tooltipHeaderFormatter(point.key),
            blockedSessionsLabel: strings.blockersSeriesLabel,
            blockedSessionsValue: ' ' + point.y,
            showmore: showmore
        };

        return blockersTemplate(data);
    };
})(SW.DPA.Charts.TooltipFormatters);