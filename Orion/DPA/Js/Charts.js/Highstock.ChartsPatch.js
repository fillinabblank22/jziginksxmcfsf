﻿(function () {

    var showLoadingOld = Highcharts.Chart.prototype.showLoading;
    Highcharts.Chart.prototype.showLoading = function (str) {
        var timeoutId = 0,
            loading = this.options.loading || {},
            loadingLongerThanUsualTimeout = loading.loadingLongerThanUsualTimeout || 5000;

        showLoadingOld.call(this, str);

        var loadingLongerText = loading.loadingLongerText || '',
            loadingLongerStyle = loading.loadingLongerStyle || 'sw-loading-longer',
            loadingLongerBlock = $('<p>').addClass(loadingLongerStyle).hide().html(loadingLongerText);
        $(this.loadingDiv).append(loadingLongerBlock);

        timeoutId = setTimeout(function () {
            loadingLongerBlock.show();
        }, loadingLongerThanUsualTimeout);
    }

}());