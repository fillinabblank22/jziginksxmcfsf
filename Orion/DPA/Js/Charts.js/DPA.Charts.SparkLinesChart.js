﻿SW.Core.namespace("SW.DPA.Charts.SparkLinesChart");

(function (ctx) {
    var strings = {
        loadingLongerText: [
            '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js} ',
                    '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">',
                        '&#187; @{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
                    '</a>'
        ].join(''),
        noDataInChartsText: '@{R=DPA.Strings;K=NoDataInCharts_Text;E=js}'
    };

    var noDataInChartTemplate = _.templateExt([
        '<div class="DPA_ChartNoDataAvailable">',
        '<p>{{{text}}}</p>',
        '</div>'
    ]);
    
    ctx.initializeStandardChart = function (chartSettings, chartOptions) {
        var chartHilite = {};
        chartOptions = chartOptions || {};

        chartOptions = $.extend(true, {}, {
            loading: {
                loadingLongerText: strings.loadingLongerText,
                loadingLongerStyle: 'DPA_LoadingLongerText',
                loadingLongerThanUsualTimeout: 5000
            }
        }, chartOptions);

        SW.DPA.Common.ensureObject(chartOptions, 'chart.events');
        // init series tooltip hilite state
        chartOptions.chart.events.addSeries = function tooltipHiliteAddSeries() {
            this.swState = '';
        };

        chartOptions.chart.events.load = function () {
            // display last values and status icons   
            var lastValues = [];
            var statuses = [];
            var iconUrls = [];
            var context = this;
            if (context.options !== undefined && context.options.LastPolledValues !== undefined) {
                for (var i = 0; i < context.series.length; i++) {
                    var lastVal = context.options.LastPolledValues[i].value;
                    try {
                        var yAxisOptions = context.series[i].yAxis.options;
                        if (lastVal != null) {
                            if (yAxisOptions.unit) {
                                var formatter = SW.Core.Charts.dataFormatters[yAxisOptions.unit];
                                if (typeof formatter === "function") {
                                    lastValues[i] = formatter(lastVal);
                                } else {
                                    lastValues[i] = lastVal + " " + yAxisOptions.unit;
                                }
                            } else {
                                lastValues[i] = lastVal;
                            }
                        } else {
                            lastValues[i] = "";
                        }
                    } catch (err) {
                        console.log(err);
                        lastValues[i] = lastVal;
                    }
                    statuses[i] = context.options.LastPolledValues[i].status;
                    iconUrls[i] = context.options.LastPolledValues[i].iconUrl;
                }
            }
            var container = $(context.container).parents("div.DPA_MultiChartContainer");
            $.each(container.find("div.DPA_SparkLineItem"), function(index, value) {
                $(this).find(".DPA_Icon").html("");
                $(this).find(".DPA_Icon").append($("<img />").attr("src", iconUrls[index]));
                $(this).find("span.DPA_LastValue").html(lastValues[index]);

                if (statuses[index] == 1)
                    $(this).find("span.DPA_LastValue").css("color", "#E05B5B");
                else if (statuses[index] == 2)
                    $(this).find("span.DPA_LastValue").css("color", "#E05B5B").css("font-weight", "bold");
            });
        };      

        SW.DPA.Common.ensureObject(chartOptions, 'plotOptions.series.events');
        // series tooltip hilite state update
        chartOptions.plotOptions.series.events.mouseOver = function tooltipHiliteMouseOver() {
            var series = this, update = false;
            if (chartHilite.lastActiveSeries) {
                if (chartHilite.lastActiveSeries !== series) {
                    chartHilite.lastActiveSeries.swState = '';
                    update = true;
                }
            } else {
                update = true;
            }
            if (update) {
                chartHilite.lastActiveSeries = series;
                series.swState = 'hover';
                series.chart.redraw();
            }
        };

        SW.DPA.Common.ensureObject(chartOptions, 'tooltip');
        chartOptions.tooltip.pointFormat = '<tr style="line-height: 90%;" class="{series.swState}"><td style="border: 0; font-size: 12px; color: {series.color};">{series.name}: </td><td style="border: 0px; font-size: 12px"><b>{point.y}</b></td></td></tr>';

        chartOptions.tooltip.positioner = function (boxWidth, boxHeight, point) {
            var leftOffset = this.chart.container.offsetLeft;

            var boxX = point.plotX;
            var boxY = point.plotY;
            if (this.chart.plotWidth > point.plotX + boxWidth - 80
                || leftOffset + point.plotX - 10 < boxWidth) {
                boxX = boxX + 10 ;
            }
            else {
                boxX = boxX - 40 - boxWidth;
            }

            if (this.chart.plotHeight > point.plotY + boxHeight) {
                boxY = boxY + 40;
            }
            else {
                boxY = boxY + 10 - boxHeight;
            }

            return { x: boxX, y: boxY };
        }

        var onFinish = function () {
            if ($('#' + chartSettings.renderTo + ' .chartDataNotAvailableArea').length > 0) {
                $('#' + chartSettings.renderTo).parent().parent().css({ 'height': '212px', 'width': '100%' });
                $('#' + chartSettings.renderTo).parent().parent().parent().css('height', '212px');
                $('#' + chartSettings.renderTo).parent().parent().parent().find('.DPA_SparkLineItem').hide();
                $('#' + chartSettings.renderTo).parent().parent().parent().find('.DPA_Left').hide();
                $('#' + chartSettings.renderTo).parent().parent().parent().find('.DPA_Right').hide();
                var text = typeof chartOptions.noDataText !== "undefined" ? chartOptions.noDataText : strings.noDataInChartsText;
                $('#' + chartSettings.renderTo + ' .chartDataNotAvailableArea table tbody tr td').html(noDataInChartTemplate({
                    text: text
                }));
            }
            
            $('#' + chartSettings.renderTo + ' .highcharts-container').css("z-index", 7 - $('#' + chartSettings.renderTo).closest(".ResourceColumn").attr("columnid"));
        };

        SW.Core.Charts.initializeStandardChart(chartSettings, chartOptions, onFinish);
    };
})(SW.DPA.Charts.SparkLinesChart);
