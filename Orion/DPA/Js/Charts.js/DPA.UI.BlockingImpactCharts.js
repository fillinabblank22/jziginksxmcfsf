﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.BlockingImpactCharts = SW.DPA.UI.BlockingImpactCharts || {};

(function (charts) {

	charts.configStrings = {
        yAxisTitle: '@{R=DPA.Strings;K=DPAResourceConfig_BlockingImpact_yAxisTitle;E=js}',
        topRootBlockersChartTitle: '@{R=DPA.Strings;K=DPAResourceConfig_BlockingImpact_TopRootBlockersTab_ChartTitle;E=js}',
        loadingLongerText: [
            '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js} ',
                '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">',
                    '&#187; @{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
                '</a>'
        ].join(''),
        deadlocksYAxisLabel: '@{R=DPA.Strings;K=BlockingImpact_yAxisLabel_Deadlocks;E=js}'
	};

    charts.tooltipStrings = {
        sqlhash: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_SQLHash;E=js}',
        showmore: '@{R=DPA.Strings;K=DPAResource_DatabaseResponseTime_TooltipLabel_More;E=js}',
        blockers: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_Blockers;E=js}',
        idleBlockers: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_IdleBlockers;E=js}',
        blockerswaittime: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_BlockersWaitTime;E=js}',
        blockerswaittimeimpact: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_BlockersWaitTimeImpact;E=js}',
        blockerstotalwaittime: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_BlockersTotalWaitTime;E=js}',
        blockerstotalpercentage: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_BlockersTotalPercentage;E=js}',
        waiters: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_Waiters;E=js}',
        waiterswaittime: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_WaitersWaitTime;E=js}',
        waiterstotalwaittime: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_WaitersTotalWaitTime;E=js}',
        waiterstotalpercentage: '@{R=DPA.Strings;K=BlockingImpact_TooltipLabel_WaitersTotalPercentage;E=js}'
    };

    charts.listeners = {
        blockingImpactChart: {
            click: function (e) {
                var url = e && e.point && e.point.detailUrl;
                url && window.open(url, '_blank');
            }
        },
        blockers: { click: function (e) { return; } },
        deadlocks: { click: function (e) { return; } },
        // remaining, other, ... series
        special: {
            click: function (e) {
                return;
            }
        },
    };
    
    var tooltipTemplates = {};
    tooltipTemplates.blockers = _.template([
        '<div id="DPA_HighchartTooltip">',
            '<table>',
                // build the header
                '<tr><td colspan="2" class="hint">{{ headerDateTime }}</td></tr>',

                // build the tooltip table
                '<tr>',
                    '<td class="label">{{ labels.blockers }}</td>',
                    '<td class="ellipsis{# if(isRemaining){ #} remaining{# } #}" style="color:{{ color }};">',
                        '{# if(isRemaining){ #}{{ labels.idleBlockers }}{# } else { #}{{ options.customProperties.SqlName }}{# } #}',
                    '</td>',
                '</tr>',
                '<tr {# if(isRemaining){ #}class="disabled"{# } #}>',
                    '<td class="label">{{ labels.sqlhash }}</td>',
                    '<td class="sql-hash">{{ options.customProperties.SqlHash }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.blockerswaittime }}</td>',
                    '<td>{{ TooltipTimeFormatter(point.additionalValue) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.blockerswaittimeimpact }}</td>',
                    '<td>{{ TooltipTimeFormatter(value) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label"><span>{{ labels.blockerstotalwaittime }}</span></td>',
                    '<td>{{ TooltipTimeFormatter(options.customProperties.TotalWaitTime) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.blockerstotalpercentage }}</td>',
                    '<td>{{ point.totalpercentage }} %</td>',
                '</tr>',
                // footer
                '<tr><td colspan="2" class="hint">{{ labels.showmore }}</td></tr>',
            '</table>',
        '</div>'].join('')
        );
    tooltipTemplates.waiters = _.template([
        '<div id="DPA_HighchartTooltip">',
            '<table>',
                // build the header
                '<tr><td colspan="2" class="hint">{{ headerDateTime }}</td></tr>',

                // build the tooltip table
                '<tr>',
                    '<td class="label">{{ labels.waiters }}</td>',
                    '<td class="ellipsis{# if(isRemaining){ #} remaining{# } #}" style="color:{{ color }};">',
                        '{{ options.customProperties.SqlName }}',
                    '</td>',
                '</tr>',
                '<tr {# if(isRemaining){ #}class="disabled"{# } #}>',
                    '<td class="label">{{ labels.sqlhash }}</td>',
                    '<td class="sql-hash">{{ options.customProperties.SqlHash }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.waiterswaittime }}</td>',
                    '<td>{{ TooltipTimeFormatter(value) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label"><span>{{ labels.waiterstotalwaittime }}</span></td>',
                    '<td>{{ TooltipTimeFormatter(options.customProperties.TotalWaitTime) }}</td>',
                '</tr>',
                '<tr>',
                    '<td class="label">{{ labels.waiterstotalpercentage }}</td>',
                    '<td>{{ point.totalpercentage }} %</td>',
                '</tr>',
                // footer
                '<tr><td colspan="2" class="hint">{{ labels.showmore }}</td></tr>',
            '</table>',
        '</div>'].join('')
        );

	var getTooltipSeries = function () {
        var items = this.points || $.isArray(this) ? this : [this], item = items[0];
        if (!item) return null;

        var series = $.extend({}, item.series), time = item.key;
        // time and value axis
        series.time = item.x;
        series.value = item.y;
        series.point = item.point;

        // common values
        series.labels = $.extend({}, charts.tooltipStrings);
        series.headerDateTime = series.tooltipHeaderFormatter(time);
        series.total = item.total || 0;
        series.percentage = (item.percentage || 0).toFixed(2);
        series.isRemaining = !!series.options.customProperties.IsRemaining;

        // calculations
        series.point.totalpercentage = (series.value / series.options.customProperties.TotalWaitTime * 100).toFixed(2);

        // supporting finctions
        series.TooltipTimeFormatter = SW.Core.Charts.dataFormatters.waitTimeMsTwoUnit;
        return series;
	};

    charts.tooltipFormatter = function(templateId) {
        return function() {
            var series = getTooltipSeries.apply(this),
                template = tooltipTemplates[templateId];
            return series && template ? template(series) : '';
        };
    };

	charts.tooltipPositioner = function (boxWidth, boxHeight, point) {
        // follow mouse pointer
        var chart = this.chart, distance = this.options.distance || 12,
            x = point.plotX + chart.plotLeft + (chart.inverted ? distance : -boxWidth - distance),
            y = point.plotY - boxHeight + chart.plotTop + 16;
        if (x < 16) { x = chart.plotLeft + point.plotX + distance; }
        return { x: x, y: y };
	};

	var defaultSettings = {
        chart: { type: "column" },
        scrollbar: { enabled: true },
        rangeSelector: { enabled: true },
        navigator: {
            enabled: true,
            series: {
                type: "area",
                index: -1, // to be excluded from mapping
                step: true
            }
        },
        tooltip: {
            enabled: true,
            shared: false,
            crosshairs: false,
            formatter: undefined, // customize
            positioner: charts.tooltipPositioner,
            headerFormat: "{point.key}"
        },
        title: {
            text: undefined, // customize
            style: {
                whiteSpace: "normal",
                left: "0px",
                top: "0px",
                position: "absolute"
            },
            useHTML: true
        },
        yAxis: [
            {
                allowDecimals: false,
                title: {
                    margin: 10,
                    text: charts.configStrings.yAxisTitle
                },
                unit: "sec",
                unitFormatter: "sec",
                labels: { align: "right", x: -8 },
                startOnTick: false,
                endOnTick: true
            }
        ],
        seriesTemplates: {
            WaitTime: {
                type: "column",
                name: "",
                events: undefined, // customize
                index: 1000,
                zIndex: 1000
            },
            RemainingTime: {
                type: "column",
                name: "",
                color: "#D0D0D0",
                events: undefined,
                index: 0,
                zIndex: 0,
                visible: false
            },
            WaitTimeZoom: {
                type: "column",
                visible: false,
                showInLegend: false,
                index: 1000,
                zIndex: 1000,
                pointRange: 0.5 * 3600000
            },
            RemainingTimeZoom: {
                type: "column",
                color: "#D0D0D0",
                events: undefined,
                visible: false,
                showInLegend: false,
                index: 0,
                zIndex: 0,
                pointRange: 0.5 * 3600000
            }
        },
        xAxis: {
            minRange: 3600000 // one hour
        },
        plotOptions: {
            column: {
                stacking: "normal",
                dataGrouping: { enabled: false }
            }
        },
        loading: {
            loadingLongerText: charts.configStrings.loadingLongerText,
            loadingLongerStyle: 'DPA_LoadingLongerText',
            loadingLongerThanUsualTimeout: 5000
        },
        noDataText: charts.configStrings.noDataInChartsText
    }

    charts.settings = {
		blockers: $.extend(true, {}, defaultSettings, {
			tooltip: { formatter: charts.tooltipFormatter("blockers") },
			title: { text: charts.configStrings.topQueriesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners["blockingImpactChart"] },
                WaitTimeZoom: { events: charts.listeners["blockingImpactChart"] },
                RemainingTime: { events: charts.listeners["blockingImpactChart"] },
                RemainingTimeZoom: { events: charts.listeners["blockingImpactChart"] }
			}
        }),
		waiters: $.extend(true, {}, defaultSettings, {
            tooltip: { formatter: charts.tooltipFormatter("waiters") },
            title: { text: charts.configStrings.topQueriesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners["blockingImpactChart"] },
                WaitTimeZoom: { events: charts.listeners["blockingImpactChart"] },
                RemainingTime: { events: charts.listeners["blockingImpactChart"] },
                RemainingTimeZoom: { events: charts.listeners["blockingImpactChart"] }
            }
        }),
        deadlocks: $.extend(true, {}, defaultSettings, {
            tooltip: { enabled: false },
            yAxis: [{
                title: {
                    text: charts.configStrings.deadlocksYAxisLabel
                },
                unit: "",
                unitFormatter: ""
            }],
            title: { text: charts.configStrings.topQueriesChartTitle },
            seriesTemplates: {
                WaitTime: { events: charts.listeners["blockingImpactChart"], showInLegend: false },
                WaitTimeZoom: { events: charts.listeners["blockingImpactChart"], showInLegend: false }
            }
        })
	};

	charts.mapSeries = function (series, zoomset) {
		var map = {};
		_.each(zoomset || [], function (zoom) { map[zoom.id] = {}; });
		_.each(series || [], function (s) {
			if (s.options.index >= 0) { // exclude navigator, etc.
				var m = charts.mapping(s);
				if (m) map[m.zoom][m.id] = m.serie;
			}
		});

		return map;
	};

    charts.mapping = function(serie, uniqueId) {
        var uid = uniqueId || serie.options.uniqueId,
            r = /[|](.*?)[|](.+?)$/g.exec(uid) || [];

        return r.length ? { uid: uid, serie: serie, id: r[1], zoom: r[2] } : null;
    };

    charts.zoomLevel = function(chart, zoom) {
        if (chart && zoom) chart.options.lastZoomLevel = zoom;
        return chart && chart.options.lastZoomLevel || undefined;
    };
})(SW.DPA.UI.BlockingImpactCharts);
