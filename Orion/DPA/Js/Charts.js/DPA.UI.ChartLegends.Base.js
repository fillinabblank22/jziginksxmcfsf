﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.ChartLegends = SW.DPA.UI.ChartLegends || {};
SW.DPA.UI.ChartLegends.QueryBase = SW.DPA.UI.ChartLegends.QueryBase || {};

(function (legend) {
    legend.QueryLegendTemplate = _.template([
      '{# _.each(legendSeries, function(s){ #}',
        '{# if (s.show) { #}',
          '<tr>',
            '<td class="toggle">',
                '<input type="checkbox" data-uid="{{ s.uid }}" {# if(s.visible){ #}checked="checked"{# } #}/>',
            '</td>',
            '<td class="symbol">{{ AddLegendSymbol(s) }}</td>',
			'<td class=" {# if(s.isRemaining){ #} remaining{# } else { #} DPA_LegendBodyItem{# } #}" data-uid="{{ s.uid }}">',
                '<span class="expander-body">',
                    '{# if(!s.isRemaining){ #}',
                        '<a href="#" onclick="SW.DPA.UI.SqlDetailDialog.open(\'{{s.custom.GlobalDatabaseInstanceId}}\', \'{{s.custom.SqlHash}}\', \'{{ escape(s.custom.SqlQuery)}}\', \'{{{ escape(s.custom.SqlName) }}}\'); return false;" style="display: inline;">{{ s.custom.SqlName }}</a>',
                    '{# } else { #}',
                        '<span class="remaining-icon"></span>',
                        '<a href="{{ s.url }}" target="_blank" style="display: inline;">{{ formatQuery(s.name) }}</a>',
                    '{# } #}',
                '</span>',
            '</td>',
          '</tr>',
        '{# } #}',
      '{# }); #}'].join('')
    );

    legend.LegendSymbolDefaults = {
        x: 2,
        y: 4,
        width: 16,
        height: 12,
        cornerRadius: 2,
    };

    legend.addLegendSymbol = function (series, color) {
        var data = $.extend({}, legend.LegendSymbolDefaults, {
            color: color || series.color,
            el: $('<span class="legendSymbol" />')
        });

        var renderer = new Highcharts.Renderer(data.el[0], data.x + data.width, data.y + data.height);
        renderer.rect(0, 0, data.width, data.height, data.cornerRadius)
            .attr({ stroke: data.color, fill: data.color, translateX: data.x, translateY: data.y })
            .add();

        return data.el.html();
    };

    legend.formatQuery = function (query) {
        return query.replace(/%%NL%%/g, '<div class="nl"/>').replace(/%%TAB%%/g, '<div class="tab"/>')
    };
})(SW.DPA.UI.ChartLegends.QueryBase);