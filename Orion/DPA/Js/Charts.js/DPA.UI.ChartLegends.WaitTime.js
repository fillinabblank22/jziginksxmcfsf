﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.UI = SW.DPA.UI || {};
SW.DPA.UI.ChartLegends = SW.DPA.UI.ChartLegends || {};
SW.DPA.UI.ChartLegends.WaitTime = SW.DPA.UI.ChartLegends.WaitTime || {};

(function (legend) {
    legend.toggleSeries = function (series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function (series, container) {

        var check = $('<input type="checkbox"/>')
                    .click(function () { legend.toggleSeries(series); });

        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.instanceIcon = function (series, container) {
        var seriesData = series.options.customProperties;

        var content = SW.DPA.Common.formatLinkIcon(seriesData.DbInstanceLink,
            seriesData.DbInstanceIcon);

        container.append(content);
    };

    legend.intanceOnIconLink = function (series, container, isNodeFunctionalityAllowed) {
        var seriesData = series.options.customProperties;

        var content = SW.DPA.Common.FormatDatabaseInstanceLink(seriesData.DbInstanceLink,
            undefined,
            seriesData.DbInstanceName,
            seriesData.NodeLink,
            seriesData.NodeIcon,
            seriesData.NodeName,
            isNodeFunctionalityAllowed);

        container.append(content);
    };

    legend.addLegendSymbol = function (series, container, colorOverride) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = colorOverride || series.color;

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                    stroke: symbolColor,
                    fill: symbolColor
                })
                .add();

        label.appendTo(container);
    };

    legend.createStandardLegend = function(chart, legendContainerId, isNodeFunctionalityAllowed) {
        var table = $('#' + legendContainerId + " table");

        // we need to clean the table and title, because of async refresh
        table.empty();

        // create table
        $.each(chart.series, function(index, series) {
            if (!series.options.showInLegend)
                return;

            var row = $('<tr/>');
            var td = $('<td style="width:15px;"/>').appendTo(row);
            legend.addCheckbox(series, td);

            td = $('<td style="width:20px;"/>').appendTo(row);
            legend.addLegendSymbol(series, td);

            td = $('<td/ style="width:30px">').appendTo(row);

            legend.instanceIcon(series, td);

            td = $('<td/>').appendTo(row);

            legend.intanceOnIconLink(series, td, isNodeFunctionalityAllowed);

            row.appendTo(table);
        });
    };
})(SW.DPA.UI.ChartLegends.WaitTime);