﻿SW.Core.namespace("SW.DPA.UI.Charts.SqlDetailTotalResponseTimeCharts");

SW.DPA.UI.Charts.SqlDetailTotalResponseTimeCharts = function () {
    var getConfig = function(hasData) {
        return {
            chart: {
                zoomType: "none",
                alignTicks: false,
                height: 140
            },
            legend: { enabled: false },
            scrollbar: { enabled: false },
            rangeSelector: { enabled: false },
            navigator: { enabled: false },
            plotOptions: {
                series: {
                    minPointLength: 2
                },
                column: {
                    stacking: "normal"
                }
            },
            xAxis: {
                minRange: 2 * 3600000 // 2 hours
            },
            yAxis: hasData ? {} : {
                min: 0,
                max:2
            }
        }
    };
    
    this.createTotalResponseTimeChart = function(idOfElementToRender, dataSeries) {
        var config = $.extend(true, {}, SW.Core.Charts.getBasicChartConfig(), getConfig(dataSeries.CustomProperties.hasData),
        {
            chart: {
                type: "column",
                renderTo: idOfElementToRender
            },
            seriesTemplates: {
                TotalResponseTime: {
                    type: "column"
                }
            },
            tooltip: {
                formatter: function () {
                    return SW.DPA.Charts.TooltipFormatters.sqlDetailsTotalResponseTime.call(this);
                }
            },
            yAxis: {
                labels: {
                    align: "right",
                    x: -12,
                    formatter: function () {
                        return SW.DPA.Charts.DataFormatters.formatWaitTime(this.value, this.axis.tickInterval);
                    }
                }
            },
            series: [{
                data: dataSeries.Data,
                name: dataSeries.Label,
                tagName: dataSeries.TagName

            }]
        });

        setTimeout(function () { SW.Core.Charts.createChart(config); }, 1);
    }

    this.createExecutionsTimeChart = function (idOfElementToRender, dataSeries) {
        var config = $.extend(true, {}, SW.Core.Charts.getBasicChartConfig(), getConfig(dataSeries.CustomProperties.hasData),
        {
            chart: {
                type: "line",
                renderTo: idOfElementToRender
            },
            seriesTemplates: {
                Executions: {
                    type: "line"
                }
            },
            tooltip: {
                formatter: function () {
                    return SW.DPA.Charts.TooltipFormatters.sqlDetailsExecutions.call(this);
                }
            },
            yAxis: {
                labels: {
                    align: "right",
                    x: -12
                },
                min:0
            },
            series: [{
                data: dataSeries.Data,
                name: dataSeries.Label,
                tagName: dataSeries.TagName

            }]
        });

        setTimeout(function () { SW.Core.Charts.createChart(config); }, 1);
    }
};
