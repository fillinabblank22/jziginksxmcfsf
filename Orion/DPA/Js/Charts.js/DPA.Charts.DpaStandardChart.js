﻿SW.Core.namespace("SW.DPA.Charts.DpaStandardChart");

(function (ctx) {
    var strings = {
        loadingLongerText: [
            '@{R=DPA.Strings;K=LoadingIcon_NotificationText;E=js} ',
                    '<a href="/api/DpaHelpLink/ToHelpPage/OrionDPAConnectivityIssue" target="_blank">',
                        '&#187; @{R=DPA.Strings;K=LoadingIcon_Notification_TextOfLink;E=js}',
                    '</a>'
        ].join(''),
        noDataInChartsText: '@{R=DPA.Strings;K=NoDataInCharts_Text;E=js}'
    };

    var noDataInChartTemplate = _.templateExt([
    '<div class="DPA_ChartNoDataAvailable">',
    '<p>{{{text}}}</p>',
    '</div>'
    ]);

    ctx.initializeStandardChart = function (chartSettings, chartOptions, onFinish) {
        chartOptions = $.extend(true, {}, {
            loading: {
                loadingLongerText: strings.loadingLongerText,
                loadingLongerStyle: 'DPA_LoadingLongerText',
                loadingLongerThanUsualTimeout: 5000
            }
        }, chartOptions);
        SW.DPA.Common.ensureObject(chartOptions, 'chart.events');

        if (typeof chartSettings.connectivityIssueWarningId !== 'undefined') {
            var connectivityIssuesWarning = new SW.DPA.UI.ConnectivityIssueWarning(chartSettings.connectivityIssueWarningId);
        }

        var dpaRedraw = function () {
            var events = chartOptions.chart.events;
            if (events._hasDpaRedraw && events._prevRedraw) {
                args = [].slice.call(arguments, 0);
                events._prevRedraw.apply(this, args);
            }

            if (typeof connectivityIssuesWarning !== "undefined" && typeof this.options !== "undefined" && typeof this.options.PartialResultErrors !== "undefined") {
                if (this.options && this.options.PartialResultErrors.length > 0) {
                    connectivityIssuesWarning.show(this.options.PartialResultErrors);
                } else {
                    connectivityIssuesWarning.hide();
                }
            }
        };

        var currentOnFinish = function () {
            if (onFinish)
                onFinish();

            if ($('#' + chartSettings.renderTo + ' .chartDataNotAvailableArea').length > 0) {
                var text = typeof chartOptions.noDataText !== "undefined" ? chartOptions.noDataText : strings.noDataInChartsText;
                // replace core 'Data is not available' with custom text
                $('#' + chartSettings.renderTo + ' .chartDataNotAvailableArea table tbody tr td').html(noDataInChartTemplate({
                    text: text
                }));
            }
        };

        var events = chartOptions.chart.events;
        if (events._hasDpaRedraw !== true)
            $.extend(events, { _hasDpaRedraw: true, _prevRedraw: events.redraw, redraw: dpaRedraw });

        // call original init function with modified options
        SW.Core.Charts.initializeStandardChart(chartSettings, chartOptions, currentOnFinish);
    };
})(SW.DPA.Charts.DpaStandardChart);