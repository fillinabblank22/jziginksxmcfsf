﻿SW.Core.namespace("SW.DPA.Charts");

SW.DPA.Charts.PerformanceExpandable = function (config) {
    var contentWrapper;
    var base = new SW.DPA.Charts.Performance();
    base.legendTemplate = _.templateExt([
        '<tr data-id="{{id}}">',
            '<td class="DPA_Toggle"><input type="checkbox" checked="checked" /></td>',
            '<td class="DPA_Symbol"><div style="background-color: {{color}};"></div></td>',
            '<td class="DPA_Expander DPA_Collapsed">',
                '<span class="DPA_ExpanderButton"></span>',
                '<div class="DPA_ExpanderContent">',
                    '<div class="DPA_ExpanderHeader">',
                        '<a href="javascript:void(0);" class="DPA_LegendLink">',
                            '<img src="/Orion/StatusIcon.ashx?size=small&entity={{entity}}&id={{entityID}}&status={{status}}" />',
                        '&nbsp;{{label}}</a>',
                    '</div>',
                    '<div class="DPA_ExpanderBody DPA_ExpandableBlock{{id}}">',
                        '<div class="DPA_TableWrapper">',
                        '</div>',
                    '</div>',
                '</div>',
            '</td>',
        '</tr>']);

    base.onLegendSerieRender = function (legendContainerId, rowPrefix) {
        contentWrapper = $('#' + legendContainerId);
        contentWrapper.find(rowPrefix + '.DPA_ExpanderButton').click(function () {
            var collapsedCssClass = 'DPA_Collapsed';

            var rowElement = $(this).parent().parent();
            var lunId = rowElement.data('id');
            var tableRendered = rowElement.hasClass('DPA_TableRendered');

            if ($(this).parent().hasClass(collapsedCssClass)) {
                $(this).parent().removeClass(collapsedCssClass);
                if (!tableRendered) {
                    loadDatabaseInstancesTable(lunId, function() {
                         rowElement.addClass('DPA_TableRendered');
                    }, config.isNodeFunctionalityAllowed);
                }
            } else {
                $(this).parent().addClass(collapsedCssClass);
            }
        });
    }

    base.templateModelProvider = function (serie) {
        return $.extend({
            color: serie.color,
            entity: serie.options.customProperties.SwisEntity,
            entityID: serie.options.customProperties.Id,
            id: serie.options.customProperties.Id,
            status: serie.options.customProperties.Status,
            label: serie.name
        });
    }

    this.init = function (latency, info) {
        base.init(latency, info);
    };

    var loadDatabaseInstancesTable = function (lunId, onSuccessCallback, isNodeFunctionalityAllowed) {
        var databaseInstancesConfig = {
            isNodeFunctionalityAllowed: isNodeFunctionalityAllowed,
            uniqueClientId: config.wrapperId.toString() + '-' + lunId,
            url: "/api/DpaDatabaseInstances/GetDatabaseInstancesByLun",
            getParameters: function () {
                return {
                    LunId: lunId
                };
            },
            onSuccess: function (result) {
                if (onSuccessCallback) {
                    onSuccessCallback();
                }
                return true;
            }
        };

        var grid = new DPA.UI.DatabaseInstancesGrid.Grid(contentWrapper.find(".DPA_ExpandableBlock" + lunId + ' .DPA_TableWrapper'), databaseInstancesConfig);
        grid.init(function () {
            grid.refresh();
        });
    };
};