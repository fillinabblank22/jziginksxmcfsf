﻿SW.Core.namespace("SW.DPA.UI.SearchControl");
SW.DPA.UI.SearchControl = function (searchControlIdPrefix, searchCallback) {
    var searchBox = $('#' + searchControlIdPrefix + "_searchBox");
    var searchButton = $('#' + searchControlIdPrefix + "_searchButton");
    var searchCancelIcon = '/Orion/images/Button.SearchCancel.gif';
    var searchButtonIcon = '/Orion/images/Button.SearchIcon.gif';
    var searchValue = "";

    var handleSearch = function() {
        searchValue = searchBox.val();
        if (searchValue !== '') {
            searchButton.attr('src', searchCancelIcon);
        }

        searchCallback();
    };

    searchButton.click(function () {
        if (searchButton.attr('src') == searchCancelIcon) {
            searchButton.attr('src', searchButtonIcon);
            searchBox.val('');
        }

        handleSearch();
    });

    searchBox.keydown(function (e) {
        if (e.keyCode == 13) {
            handleSearch();
            return false;
        }
        return true;
    });

    this.getSearchValue = function () {
        return searchValue;
    }
};
