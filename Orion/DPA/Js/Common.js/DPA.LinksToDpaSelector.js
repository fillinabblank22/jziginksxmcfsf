﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.LinksToDpaSelector = ({
    localizedStrings: {
        title: '@{R=DPA.Strings;K=DpaServerSelector_Title;E=js}',
        submitButtonLabel: '@{R=DPA.Strings;K=DpaServerSelector_SubmitButtonLabel;E=js}',
        cancelButtonLabel: '@{R=DPA.Strings;K=DpaServerSelector_CancelButtonLabel;E=js}',
    },
    windowTemplate: _.templateExt([
        '<div id="DPA_DpaServerSelectorWindow">',
        '<span class="DPA_ErrorImage"></span>',
        '<div class="DPA_MainContent"><form>',
        '{# _.each(dpaServers, function(dpaServer) { #}',
        '<div><input type="radio" name="serverId" value="{{dpaServer.DpaServerId}}" id="radioDpaServer{{dpaServer.DpaServerId}}" />',
        '<label for="radioDpaServer{{dpaServer.DpaServerId}}">{{{dpaServer.DisplayName}}}</label></div>',
        '{# }); #}',
        '</form></div>',
        '<div class="DPA_Buttons"><br/></div>',
        '</div>'
    ]),
    show: function (link) {
        var that = this;

        var goToLink = function(dpaServerId) {
            var fullLink = link + dpaServerId;

            var win = window.open(fullLink, '_blank');
            if (win) {
                win.focus();
            } else {
                //Browser has blocked it
                window.location.href = fullLink;
            }
        }

        var displaySelection = function(dpaServers) {
            var viewModel = {
                dpaServers: []
            };

            viewModel.dpaServers = dpaServers;

            var window = $(that.windowTemplate(viewModel));

            window.find('.DPA_Buttons')
                .append($(SW.Core.Widgets.Button(that.localizedStrings.submitButtonLabel, { type: 'primary' }))
                    .click(function() {
                        if (window.find('form input:checked').length > 0) {
                            goToLink(window.find('form input:checked').val());
                            window.dialog('close');
                        }
                    }));

            window.find('.DPA_Buttons')
                .append($(SW.Core.Widgets.Button(that.localizedStrings.cancelButtonLabel, { type: 'secondary' }))
                    .click(function() { window.dialog('close'); }));

            window.dialog({
                modal: true,
                minWidth: 400,
                minHeight: 100,
                zIndex: 100000,
                resizable: false,
                title: that.localizedStrings.title,
                close: function() {
                    $(this).dialog("destroy");
                    $(this).remove();
                }
            });
        };

        if (that.dpaServers.length == 1) {
            goToLink(that.dpaServers[0].DpaServerId);
        } else if (that.dpaServers.length > 0) {
            displaySelection(that.dpaServers);
        }

        return false;
    },
    dpaServers: [],
    ctor: function () {
        var that = this;

        SW.Core.Services.callController('/api/DpaServers/GetAll', { DpaServerIds: [] },
            function (response) {
                that.dpaServers = response;
            },
            function() {
            });

        return that;
    }
}).ctor();