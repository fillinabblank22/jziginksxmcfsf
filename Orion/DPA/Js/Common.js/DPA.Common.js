﻿SW.Core.namespace("SW.DPA.Common");

(function(ctx) {
    var localizedStrings = {
        addNodeLabel: '@{R=DPA.Strings;K=AddOrionNodeLabel;E=js}',
        addNodeToolTipText: '@{R=DPA.Strings;K=AddOrionNodeLinkPopupText;E=js}',
        nodeConjunction: '@{R=DPA.Strings;K=DatabaseNodeConjunction;E=js}'
    };
    
    _.templateExt = function (a, c, d) {
        a = $.isArray(a) ? a.join('') : a;
        return _.template.call(this, a, c, d);
    }

    ctx.ensureObject = function(obj, path) {
        obj = this.isObject(obj) ? obj : {};
        this.linqEach(path.split('.'), function processName(name) {
            // not using hasProp due to IE host object limitations
            if (!ctx.isObject(obj[name])) {
                obj[name] = {};
            }
            obj = obj[name];
        });
        return obj;
    }

    ctx.isObject = function (o) {
        // fastest hack, basically identifies objects usable as hashmaps
        return (Object(o) === o);
    }

    ctx.linqEach = function(value, callback) {
        var key, res;
        if (!this.isFn(callback)) {
            return;
        }
        if (this.isLikeArr(value)) {
            for (key = 0; key < value.length; key++) {
                res = callback(value[key], key);
                if (res === false) {
                    break;
                }
            }
        } else if (this.isObject(value)) {
            for (key in value) {
                if (Object.prototype.hasOwnProperty.call(value, key)) {
                    res = callback(value[key], key);
                    if (res === false) {
                        break;
                    }
                }
            }
        }
    }

    ctx.isLikeArr = function(v) {
        return this.isArr(v) || (this.isObject(v) && this.isNum(v.length));
    }

    ctx.isArr = function(v) {
        return v instanceof Array;
    }

    ctx.isFn = function(v) {
        return typeof v === 'function';
    }

    ctx.isNum = function(v) {
        return typeof v === 'number';
    }

    ctx.parseNetObjectFromUrl = function(url) {
        if (typeof url !== 'undefined' && url != null) {
            var querySplit = url.split('?');
            if (querySplit.length > 1) {

                var netObjectQueryPart = _.find(querySplit[1].split('&'), function (item) {
                    var splited = item.split('=');
                    return splited.length > 1 && splited[0] == 'NetObject';
                });

                if (typeof netObjectQueryPart !== 'undefined' && netObjectQueryPart != null) {
                    return netObjectQueryPart.split('=')[1];
                }
            }            
        }

        return "";
    };

    ctx.parseIdFromNetObject = function(netObject) {
        var res = netObject.split(":");
        if (res.length > 1) {
            return res[1];
        } else {
            return 0;
        }
    };

    ctx.linkToDBPerfSubView = function (link) {
        if (typeof link !== "undefined" && link != "")
            link += "&SubViewKey=DBPerf*";
        return link;
    }

    ctx.setCookie = function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
    }

    ctx.getCookie = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    ctx.isIPv4 = function(value) {
        var validIPv4AddressRegex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
        return validIPv4AddressRegex.test(value);
    }

    ctx.isIPv6 = function(value) {
        var validIPv6AddressRegex = /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/;
        return validIPv6AddressRegex.test(value);
    }

    ctx.isHostname = function(value) {
        var validHostnameRegex = /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/;
        return validHostnameRegex.test(value);
    }

    ctx.trimHostname = function(value) {
        // trim whitespaces
        var returnValue = value.trim();
        // trim http()
        var withoutHttpPattern = /^(http(s)?:\/\/)?([^\/]*)(\/)?$/;
        var match = returnValue.match(withoutHttpPattern);
        if (match != null && match.length == 5) {
            returnValue = match[3];
        }

        return returnValue;
    }

    ctx.FormatKpiLinkTemplate = _.templateExt([
        '<a href="{{{dbInstanceLink}}}#{{{target}}}{{{databaseId}}}" title="{{{perfStatusTooltip}}}" class="NoTip"',
        '{# if(typeof moveTo !== "undefined") { #}',
            'data-moveto="{{{target}}}{{{databaseId}}}"',
        '{# } #}',
        '><img src="{{{perfStatusIcon}}}" /></a>'
    ]);

    ctx.FormatKpiLink = function (dbInstanceLink, perfStatusTooltip, perfStatusIcon, target, databaseId, moveTo) {
        return ctx.FormatKpiLinkTemplate({
            dbInstanceLink: ctx.linkToDBPerfSubView(dbInstanceLink),
            target: target,
            databaseId: databaseId,
            perfStatusTooltip: perfStatusTooltip,
            perfStatusIcon: perfStatusIcon,
            moveTo: moveTo
        });
    }

    ctx.FormatDatabaseInstanceLink = function (databaseInstanceLink, databaseInstanceIcon, databaseInstanceName, nodeLink, nodeIcon, nodeName, isNodeFunctionalityAllowed) {
        var addNodeIcon = "/Orion/DPA/images/assign_node.gif";
        var unknownNodeIcon = '/Orion/DPA/images/unknownNode.gif';

        if (typeof ctx.FormatDatabaseInstanceLinkTemplate === 'undefined') {
            ctx.FormatDatabaseInstanceLinkTemplate = _.templateExt([
              '<span class="DPA_Ellipsis">',
                '{# if(typeof databaseInstanceLink !== "undefined" && databaseInstanceLink != "") { #}',
                '<a href="{{{databaseInstanceLink}}}">',
                '{# } #}',
                '{# if(typeof databaseInstanceIcon !== "undefined" && databaseInstanceIcon != "") { #}',
                '<img src="{{{databaseInstanceIcon}}}" />',
                '{# } #}',
                '{# if(typeof databaseInstanceName !== "undefined" && databaseInstanceName != "") { #}',
                '{{{databaseInstanceName}}}',
                '{# } #}',
                '{# if(typeof databaseInstanceLink !== "undefined" && databaseInstanceLink != "") { #}',
                '</a>',
                '{# } #}',
                '{# if(isNodeFunctionalityAllowed) { #}',
                ' <span class="DPA_NoWrap"> {{{nodeConjunction}}} ',
                '{# if (typeof nodeLink !== "undefined" && nodeName != "" && typeof nodeIcon !== "undefined" && nodeIcon != "" && typeof nodeName !== "undefined" && nodeName != null) { #}',
                '<a href="{{{nodeLink}}}"><img src="{{{nodeIcon}}}" />{{{nodeName}}}</a>',
                '{# } else { #}',
                '{# if(SW.Core.Info.AllowAdmin) { #}',
                '<a href="{{{ SW.DPA.Common.assignNodeUrl(databaseInstanceId) }}}" class="DPA_AddNodeLink" title="{{{addNodeToolTipText}}}"><img src="{{{addNodeIcon}}}" />{{{addNodeLabel}}}</a>',
                '{# } else { #}',
                '<img src="{{{unknownNodeIcon}}}" alt="" /> {{{addNodeLabel}}}',
                '{# } #}',
                '{# } #}',
                '</span>',
                '{# } #}',
              '</span>'
            ]);
        }
        
        return ctx.FormatDatabaseInstanceLinkTemplate({
            databaseInstanceLink: ctx.linkToDBPerfSubView(databaseInstanceLink),
            databaseInstanceIcon: databaseInstanceIcon,
            databaseInstanceName: databaseInstanceName,
            databaseInstanceId: ctx.parseIdFromNetObject(ctx.parseNetObjectFromUrl(databaseInstanceLink)),
            nodeConjunction: localizedStrings.nodeConjunction,
            nodeLink: nodeLink,
            nodeName: nodeName,
            nodeIcon: nodeIcon,
            unknownNodeIcon: unknownNodeIcon,
            addNodeIcon: addNodeIcon,
            addNodeToolTipText: localizedStrings.addNodeToolTipText,
            addNodeLabel: localizedStrings.addNodeLabel,
            isNodeFunctionalityAllowed: isNodeFunctionalityAllowed
        });
    };

    ctx.formatEntityOnNodeLink = function (entityLink, entityIcon, entityName, nodeLink, nodeIcon, nodeName) {
        if (typeof ctx.formatEntityOnNodeLinkTemplate === 'undefined') {
            ctx.formatEntityOnNodeLinkTemplate = _.templateExt([
                '{# if(typeof enityLink !== "undefined" && enityLink != "") { #}',
                '<a href="{{{enityLink}}}">',
                '{# } #}',
                '{# if(typeof entityIcon !== "undefined" && entityIcon != "") { #}',
                '<img src="{{{entityIcon}}}" />',
                '{# } #}',
                '{# if(typeof entityName !== "undefined" && entityName != "") { #}',
                '{{{entityName}}}',
                '{# } #}',
                '{# if(typeof enityLink !== "undefined" && enityLink != "") { #}',
                '</a>',
                '{# } #}',
                '{# if(typeof nodeLink !== "undefined" && nodeLink != "" && typeof nodeIcon !== "undefined" && nodeIcon != "" && typeof nodeName !== "undefined" && nodeName != "") { #}',
                ' <span class="DPA_NoWrap"> {{{nodeConjunction}}} ',
                '<a href="{{{nodeLink}}}"><img src="{{{nodeIcon}}}" />{{{nodeName}}}</a>',
                '</span>',
                '{# } #}'
            ]);
        }

        return ctx.formatEntityOnNodeLinkTemplate({
            enityLink: entityLink,
            entityIcon: entityIcon,
            entityName: entityName,
            nodeConjunction: localizedStrings.nodeConjunction,
            nodeLink: nodeLink,
            nodeName: nodeName,
            nodeIcon: nodeIcon,
        });
    };

    ctx.formatLinkIcon = function(entityLink, entityIcon) {
        if (typeof ctx.formatEntityLinkIconTemplate === 'undefined') {
            ctx.formatEntityLinkIconTemplate = _.templateExt([
                '<a href="{{{enityLink}}}">',
                '<img src="{{{entityIcon}}}" />',
                '</a>'
            ]);
        }

        return ctx.formatEntityLinkIconTemplate({
            enityLink: ctx.linkToDBPerfSubView(entityLink),
            entityIcon: entityIcon,
        });
    };

    var _formatStatusIconImgTemplate = _.template('/Orion/StatusIcon.ashx?entity={{ entity }}&id={{ entityId }}&status={{ entityStatus }}&size=small&timestamp={{ $.now() }}');

    ctx.formatStatusIconUrl = function (entity, entityStatus, entityId) {
        var data = {
            entity: entity,
            entityStatus: entityStatus,
            entityId: entityId
        };

        return _formatStatusIconImgTemplate(data);
    };

    ctx.formatEntityWithStatusIconLink = function (entity, entityName, entityStatus, entityDetailsUrl, entityId, linkClass) {
        if (typeof ctx.formatEntityWithStatusIconLinkTemplate === 'undefined') {
            ctx.formatEntityWithStatusIconLinkTemplate = _.templateExt([
                '{# if(typeof entityDetailsUrl === "undefined" || entityDetailsUrl == "") { #}',
                '<img src="{{{ iconUrl }}}"/><span>{{ entityName }}</span>',
                '{# } else { #}',
                '<a class="{{ linkClass }}" href="{{ entityDetailsUrl }}" onClick="return false;"><img src="{{{ iconUrl }}}"/><span>{{ entityName }}</span></a>',
                '{# } #}'
            ]);
        }

        return ctx.formatEntityWithStatusIconLinkTemplate({
            iconUrl: ctx.formatStatusIconUrl(entity, entityStatus, entityId),
            entityName: entityName,
            entityDetailsUrl: entityDetailsUrl,
            linkClass: linkClass
        });
    };

    ctx.assignDatabaseInstanceUrl = function(applicationId, nodeId) {
        return SW.Core.String.Format("/Orion/DPA/Admin/AssignDatabaseInstance.aspx?applicationId={0}&nodeId={1}", applicationId, nodeId);
    };

    ctx.assignNodeUrl = function (databaseInstanceId) {
        return SW.Core.String.Format("/Orion/DPA/Admin/AssignNode.aspx?databaseInstanceId={0}", databaseInstanceId);
    };
})(SW.DPA.Common);