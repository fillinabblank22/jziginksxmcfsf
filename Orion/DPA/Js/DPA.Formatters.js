﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.Formatters = SW.DPA.Formatters || {};

SW.DPA.Formatters.BytesInKB = 1024;
SW.DPA.Formatters.BytesInMB = 1048576;
SW.DPA.Formatters.DaysIn100Years = 36500;

SW.DPA.Formatters.FormatColumnTitle = function (value) {
    return value.replace(/\|/g, "<br/>");
};

SW.DPA.Formatters.FormatCellStyle = function (value, warning, critical) {
    var cssclass = "";
    if (warning && value > warning)
        cssclass = "DPA_Grid_Warning_Cell DPA_NoWrap";
    if (critical && value > critical)
        cssclass = "DPA_Grid_Critical_Cell DPA_NoWrap";
    return cssclass;
};

SW.DPA.Formatters.FormatPercentageColumnValue = function (value, fixed) {
    if (value == "@{R=DPA.Strings;K=SrmResources_ValueUnknown;E=js}" || isNaN(value) || value === '') {
        return "@{R=DPA.Strings;K=SrmResources_ValueUnknown;E=js}";
    }
    if (value > 0 && value < 1) {
        return "< 1" + "@{R=DPA.Strings;K=SrmResources_ValueUnits_Percentage;E=js}";
    }
    if (value > 99 && value < 100) {
        return "> 99" + "@{R=DPA.Strings;K=SrmResources_ValueUnits_Percentage;E=js}";
    }
    return value.toFixed(fixed) + "@{R=DPA.Strings;K=SrmResources_ValueUnits_Percentage;E=js}";
};

var daysIn100Years = 36500;

SW.DPA.Formatters.IsRunoutCollectingData = function (value) {
    return value === "" || value == -SW.DPA.Formatters.DaysIn100Years;
};

SW.DPA.Formatters.IsRunoutAlreadyPassed = function (value) {
    return value < 1;

};

SW.DPA.Formatters.DateDiffInDays = function (d1, d2) {
    var msecInDay = 24 * 3600 * 1000;
    var t2 = d2.getTime();
    var t1 = d1.getTime();
    return Math.round((t2 - t1) / msecInDay);
};

SW.DPA.Formatters.FormatDaysDiff = function (value, warningThreshold, criticalThreshold) {
    var formatValue = function (value) {
        return Math.floor(value);
    };

    var result;

    if (SW.DPA.Formatters.IsRunoutCollectingData(value)) {
        return "@{R=DPA.Strings;K=SrmResources_CollectingData;E=js}";
    }
    if (SW.DPA.Formatters.IsRunoutAlreadyPassed(value)) {
        result = "@{R=DPA.Strings;K=SrmResources_Passed;E=js}";
    } else {
        if (value < 2) {
            result = '1 ' + '@{R=DPA.Strings;K=SrmResources_Day;E=js}';
        } else {
            if (value < 14) {
                result = value + ' ' + '@{R=DPA.Strings;K=SrmResources_Days;E=js}';
            } else {
                if (value < 60) {
                    result = formatValue(value / 7) + ' ' + '@{R=DPA.Strings;K=SrmResources_Weeks;E=js}';
                } else {
                    if (value < 5*366) {
                        result = formatValue(value / 30) + ' ' + '@{R=DPA.Strings;K=SrmResources_Months;E=js}';
                    } else {
                        result = '> 5 ' + '@{R=DPA.Strings;K=SrmResources_Years;E=js}';
                    }
                }
            }
        }
    }

    if (criticalThreshold && value <= criticalThreshold) {
        var span = $("<span />").addClass('DPA_CriticalText').text(result);
        return $("<p />").append(span).html();
    }
    if (warningThreshold && value <= warningThreshold) {
        var span = $("<span />").addClass('DPA_WarningText').text(result);
        return $("<p />").append(span).html();
    }
    return result;
};

SW.DPA.Formatters.FormatLatencyNumber = function (value) {
    return (value > 0 && value < 10) ? value.toFixed(2) : Math.round(value);
};

SW.DPA.Formatters.FormatDate = function (date) {
    var lang = Highcharts.getOptions().lang;
    return Highcharts.dateFormat(lang.tooltipDateFormat, date);
};

SW.DPA.Formatters.FormatTinyNumber = function (value, isCapacity) {
    var formatted;
    if (isCapacity) {
        formatted = (value > 0 && value < 0.01) ? String.format('@{R=DPA.Strings;K=SRM_NumberFormat_IsLessThan;E=js}', 0.01) : value.toFixed(2);
    } else {
        formatted = (value > 0 && value < 1) ? String.format('@{R=DPA.Strings;K=SRM_NumberFormat_IsLessThan;E=js}', 1) : Math.round(value);
    }
    return formatted;
};

SW.DPA.Formatters.SeriesThroughput = new Array('@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput_Read;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput_Write;E=js}');

SW.DPA.Formatters.SeriesIOSize = new Array('@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize_Read;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize_Write;E=js}');

SW.DPA.Formatters.SeriesLatency = new Array('@{R=DPA.Strings;K=Lun_Performance_Summary_Latency;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Read;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Write;E=js}',
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Other;E=js}');

SW.DPA.Formatters.SeriesNameToUnits = {
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOPS;E=js}': "@{R=DPA.Strings;K=Iops_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOPS_Read;E=js}': "@{R=DPA.Strings;K=Iops_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOPS_Write;E=js}': "@{R=DPA.Strings;K=Iops_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOPS_Other;E=js}': "@{R=DPA.Strings;K=Iops_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_CacheHitRatio;E=js}': "@{R=DPA.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Ratio_Read;E=js}': "@{R=DPA.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Ratio_Write;E=js}': "@{R=DPA.Strings;K=CacheHitRatioy_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency;E=js}': "@{R=DPA.Strings;K=Latency_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Read;E=js}': "@{R=DPA.Strings;K=Latency_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Write;E=js}': "@{R=DPA.Strings;K=Latency_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Latency_Other;E=js}': "@{R=DPA.Strings;K=Latency_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOPSRatio;E=js}': "@{R=DPA.Strings;K=IOPSRatio_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_QueueLength;E=js}': "@{R=DPA.Strings;K=QueueLength_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_QueueLength_Read;E=js}': "@{R=DPA.Strings;K=QueueLength_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_QueueLength_Write;E=js}': "@{R=DPA.Strings;K=QueueLength_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_DiskBusy;E=js}': "@{R=DPA.Strings;K=DiskBusy_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize;E=js}': "@{R=DPA.Strings;K=IOSize_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize_Read;E=js}': "@{R=DPA.Strings;K=IOSize_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_IOSize_Write;E=js}': "@{R=DPA.Strings;K=IOSize_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput;E=js}': "@{R=DPA.Strings;K=Throughput_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput_Read;E=js}': "@{R=DPA.Strings;K=Throughput_Unit;E=js}",
    '@{R=DPA.Strings;K=Lun_Performance_Summary_Throughput_Write;E=js}': "@{R=DPA.Strings;K=Throughput_Unit;E=js}"
};

SW.DPA.Formatters.FormatYChartValue = function (value, seriesName) {
    var unit = SW.DPA.Formatters.SeriesNameToUnits[seriesName] ? SW.DPA.Formatters.SeriesNameToUnits[seriesName] : "";
    if (value) {
        if ($.inArray(seriesName, SW.DPA.Formatters.SeriesThroughput) > -1) {
            return SW.DPA.Formatters.FormatTinyNumber(value / SW.DPA.Formatters.BytesInMB, true) + " " + unit;
        }
        if ($.inArray(seriesName, SW.DPA.Formatters.SeriesIOSize) > -1) {
            return SW.DPA.Formatters.FormatTinyNumber(value / SW.DPA.Formatters.BytesInKB, true) + " " + unit;
        }
        if ($.inArray(seriesName, SW.DPA.Formatters.SeriesLatency) > -1) {
            return SW.DPA.Formatters.FormatLatencyNumber(value) + " " + unit;
        }
        return SW.DPA.Formatters.FormatTinyNumber(value, false) + " " + unit;
    } else {
        return "0 " + unit;
    }
};
