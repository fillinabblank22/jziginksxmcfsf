﻿SW.Core.namespace("SW.DPA.UI.SortablePageableTable");
(function (tx) {
	var settings = {};

	var baseSettings = {
	    initialPageIndex: 0,
		rowsPerPage: 5,
		getRowClass: function (row) { return null; },
		noDataMessage: {
            show: true,
		    isHtml: false,
		    formatter: function() {
		        return '@{R=DPA.Strings;K=SortablePageableTable_NoDataMessage_Text;E=js}';
		    },
            cssClassProvider: function() {
                return "DPA_SortablePageableTableNoData";
            }
		}
	};

	var baseColumnSettings = {
		caption: null,
		formatter: null,
		cellCssClassProvider: null,
	    cellAttributesProvider: null,
		isHtml: false,
		allowSort: false,
		sortingProperty: null
	};

	var PageManager = function (pageIndex, pageSize) {
		this.currentPageIndex = pageIndex;
		this.rowsPerPage = pageSize;
		this.totalRowsCount = 0;

		this.startItem = function () {
			return (this.rowsPerPage * this.currentPageIndex) + 1;
		};

		this.lastItem = function () {
			// Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
			return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
		};

		/**
        * @return total number of pages into which is possible to split whole number of rows
        */
		this.numberOfPages = function () {
			return Math.ceil(this.totalRowsCount / this.rowsPerPage);
		};

		/**
        * @return true in the case of last page
        */
		this.isLastPage = function (rowCount) {
			// We asked for rowsPerPage + 1 rows.  If more than rowsPerPage rows 
			// got returned, we know this isn't the last page.
			//return rowCount <= this.rowsPerPage;
			return rowCount >= this.totalRowsCount;
		};
	};

	/**
    * @desc Initialize resource
    * @param {Object} initialSettings 
    * initialSettings.uniqueid {Number} - uniqueidentification of resource
    * initialSettings.initialPageIndex {Number} - number of initial page index which will be displayed for the first time of resource load
    * initialSettings.rowsPerPage {Number} - number of rows displayed on single page.
    */
    tx.initialize = function (initialSettings) {
		var mergedSettings = $.extend({}, baseSettings, initialSettings);
		settings[initialSettings.uniqueId] = mergedSettings;
	},

	/**
    * @desc Called everytime resource is needed to refresh
    * @param uniqueId {Number} - uniqueidentifier of resource
    */
    tx.refresh = function (uniqueId) {
        createTableFromQuery(uniqueId, settings[uniqueId].initialPageIndex, settings[uniqueId].rowsPerPage, $(SW.Core.String.Format("#OrderBy-{0}", uniqueId)).val());
    };

	/**
    * @param {Number} uniqueId - Uniqueidentifier of resource
    * @param {Number} pageIndex - index of page which currently should be displayed
    * @param {Number} pageSize - Maximum number of rows per page
    * @param {String] orderColumn - Name of column according is table sorted
    */
	var createTableFromQuery = function (uniqueId, pageIndex, pageSize, orderColumn) {
		var errorMsg = $('#ErrorMsg-' + uniqueId);
		var grid = $('#Grid-' + uniqueId);
	    if (grid.length === 0)
	        console.error("Cannot find element with selector '" + '#Grid-' + uniqueId + "'");

		errorMsg.hide();

		// 0 means to keep whatever page size is set in page size text box
		if (pageSize == 0) {
			pageSize = getCurrentPageSize(uniqueId);
		}

		var pageManager = new PageManager(pageIndex, pageSize);

		if ($.isFunction(settings[uniqueId].pageableDataTableProvider)) {
			settings[uniqueId].pageableDataTableProvider(pageIndex, pageSize, function (result) {

				grid.find('tr:not(.HeaderRow)').remove();

				pageManager.totalRowsCount = result.TotalRows;
				var columnInfo = generateColumnInfo(uniqueId, result.DataTable.Columns);
				renderHeaderColumns(uniqueId, pageIndex, pageSize, grid, columnInfo, orderColumn, result.TotalRows);

			    // render rows
				if (result.DataTable.Rows.length > 0) {
				    $.each(result.DataTable.Rows, function (rowIndex, row) {
				        var tr = $('<tr />');

				        var rowClass = settings[uniqueId].getRowClass(row);
				        if (rowClass != null)
				            tr.addClass(rowClass);

				        // walk through individual column in each row
				        $.each(row, function (cellIndex, cell) {
				            var info = columnInfo[cellIndex];
				            if (!info.isHidden) {
				                var cellInfo = $.extend({}, info, {
				                    cellIndex: cellIndex,
				                    rowIndex: rowIndex
				                });

				                renderCell(uniqueId, cell, row, cellInfo).appendTo(tr);
				            }
				        });

				        grid.append(tr);
				    });
				} else {
				    var noDataMessageSettings = settings[uniqueId].noDataMessage;
				    if (noDataMessageSettings.show) {
				        var visibleColumnsCount = 0;
				        $.each(columnInfo, function (colIndex, column) {
				            if (!column.isHidden) { visibleColumnsCount++; }
				        });

				        var tr = $('<tr />');
				        if (noDataMessageSettings.cssClassProvider) {
				            tr = tr.addClass(noDataMessageSettings.cssClassProvider());
				        }
				        
				        var td = $('<td />').attr('colspan', visibleColumnsCount);
				        if (noDataMessageSettings.formatter) {
				            if (noDataMessageSettings.isHtml) {
				                td = td.html(noDataMessageSettings.formatter());
				            } else {
				                td = td.text(noDataMessageSettings.formatter());
				            }
				        }
                        
				        td.appendTo(tr);
				        grid.append(tr);
				    }
				}


				updatePagerControls(uniqueId, pageManager, result.DataTable.Rows.length + (pageIndex * pageSize), orderColumn);

			}, function (error) {
				errorMsg.text(error);
				errorMsg.show();
			});
		} else {
			errorMsg.text("Function pageableDataTableProvider wasn't defined");
			errorMsg.show();
		}
	};

	/**
    * @desc Generates informations about individual columns used for rendering
    * @param {Number} uniqueId - uniqueidentifier of resource
    * @param {String Array} columns - individual columns returned from controller
    * @return {Object Array} with properties name, caption, formatter 
    */
	var generateColumnInfo = function (uniqueId, columns) {
		var columnInfo = [];
		$.each(columns, function (columnIndex, column) {
			var columnCaption = column;
			var columnSettings = $.extend({}, baseColumnSettings, settings[uniqueId].columnSettings[column]);

			if (typeof settings[uniqueId].columnSettings[column] != "undefined") {
				columnCaption = settings[uniqueId].columnSettings[column].caption;
			}
			else {
				columnSettings = $.extend({}, columnSettings, { isHidden: true });
			}
			columnInfo.push($.extend({}, columnSettings, { name: column, caption: columnCaption }));
		});

		return columnInfo;
	};

	/**
    * @desc Renders header row of table with names of column
    * @param uniqueId {Number} - uniqueidentifier of resource
    * @param pageIndex {Number} - current displayed page
    * @param pageSize {Number} - maximum number of rows possible to display on one page
    * @param grid {jQuery} - wrapped into jQuery object reference to html <table> element into which is grid rendered
    * @param cellInfo {Object} - information about rendered colum
    * @param orderColumn {String} - information according which column should be data ordered
    */
	var renderHeaderColumns = function (uniqueId, pageIndex, pageSize, grid, cellInfo, orderColumn, totalRows) {
		var headers = $('tr.HeaderRow', grid);
		headers.empty();

		$.each(cellInfo, function (colIndex, column) {
			if (!column.isHidden) {
				var headerHtml = $('<div>').text(!!column.headerFormatter ? column.headerFormatter(totalRows) : column.caption).text();

				var sortArrow = '';

				var orderBy = '[' + (column.sortingProperty != null ? column.sortingProperty : column.name) + ']';
				if (orderBy == orderColumn) {
					// reverse order on next click
					var descIndex = orderBy.indexOf(" DESC");
					if (descIndex === -1) {
						orderBy += " DESC";
					} else {
						orderBy = orderBy.substring(0, descIndex);
					}

					sortArrow += '&nbsp;<img class="SortArrow" src="/Orion/images/Arrows/Arrow_Ascending.png" />';
				} else if (orderColumn == orderBy + ' DESC') {
					sortArrow += '&nbsp;<img class="SortArrow" src="/Orion/images/Arrows/Arrow_Descending.png" />';
				}

				var cell = $('<td/>')
                    .addClass('ReportHeader ' + column.name)
                    .css('text-transform', 'uppercase')
                    .appendTo(headers);

				if (column.allowSort) {
					cell.addClass("Sortable");
					cell.click(function () {
						$('#OrderBy-' + uniqueId).val(orderBy);
						createTableFromQuery(uniqueId, pageIndex, pageSize, orderBy);
					});

					headerHtml += sortArrow;
				}

				cell.html(headerHtml);
			}
		});
	};

	/**
    * @desc Renders individual data cell
    * @param uniqueId {Number} - uniqueidentifier of resource
    * @param cellValue {String} - value of data cell
    * @param rowArray {String Array} - all row values
    * @param cellInfo {Object} - information about rendered column
    * @return {jQuery} - wrapped in {jQuery} rendered cell
    */
	var renderCell = function (uniqueId, cellValue, rowArray, cellInfo) {
		var cell = $('<td/>');
		cell.addClass('column' + cellInfo.cellIndex + ' ' + cellInfo.name);
		if (cellValue == null) {
			cellValue = "";
		} else if (Date.isInstanceOfType(cellValue)) {
			cellValue = cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
		}

	    if (cellInfo.cellAttributesProvider != null) {
	        var attributes = cellInfo.cellAttributesProvider(cellValue, rowArray, cellInfo);

	        $.each(attributes, function (key, value) {
	            cell.attr(key, value);
	        });	            
	    }

	    if (cellInfo.cellCssClassProvider != null) {
	        cell.addClass(cellInfo.cellCssClassProvider(cellValue, rowArray, cellInfo));
	    }

	    if (cellInfo.formatter != null) {
			cellValue = cellInfo.formatter(cellValue, rowArray, cellInfo);
		}

		var element = $('<span />');
		if (cellInfo.isHtml) {
			element.html(cellValue);
		} else {
			element.text(cellValue);
		}

		element.appendTo(cell);

		return cell;
	}

	/**
    * @desc Return element wrapped in jQuery object which contains pager controls
    * @return {jQuery} - object which contains page controls
    */
	var getPager = function (uniqueId) {
		return $('#Pager-' + uniqueId);
	};

	/**
    * @desc Get current maximum number of rows per page
    * @param uniqueId {Number} - uniqueidentifier of resource
    */
	var getCurrentPageSize = function (uniqueId) {
		var pager = getPager(uniqueId);
		var pageSize = pager.find('.pageSize').val();
		if (typeof (pageSize) === "undefined" || pageSize == '' || pageSize <= 0) {
			pageSize = 1;
		}
		return pageSize;
	};

	/**
    * dec Creates and updates grid pager control elements
    * @param uniqueId {Number} - uniqueidentifier of resource
    * @param pagerManager {Object} - 
    * @param rowCount - Total number of rows in result set which we will split into individual pages
    */
	var updatePagerControls = function (uniqueId, pageManager, rowCount, orderColumn) {
		var pager = getPager(uniqueId);
		var currentPageIndex = pageManager.currentPageIndex;
		var html = [];

		var displayingObjectsText = '@{R=DPA.Strings;K=SortablePageableTable_DisplayingItems;E=js}'; //Displaying items {0} - {1} of {2}
		var pageXofYText = '@{R=DPA.Strings;K=SortablePageableTable_Page;E=js}'; //Page {0} of {1}
		var itemsOnPageText = '@{R=DPA.Strings;K=SortablePageableTable_ItemsOnPage;E=js}'; // Items on page

		var style = 'style="vertical-align:middle"';
		var firstImgRoot = '/Orion/images/Arrows/button_white_paging_first';
		var previousImgRoot = '/Orion/images/Arrows/button_white_paging_previous';
		var nextImgRoot = '/Orion/images/Arrows/button_white_paging_next';
		var lastImgRoot = '/Orion/images/Arrows/button_white_paging_last';

		var haveLinks = false;

		var startHtml;
		var endHtml;
		var contents;

		if (currentPageIndex > 0) { // we are not currently on the first page
			// renders first page control element in enabled state
			startHtml = '<a href="#" class="firstPage NoTip">';
			contents = String.format('<img src="{0}.gif" {1}/>', firstImgRoot, style);
			endHtml = '</a>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			// renders previous page control element in enabled state
			startHtml = '<a href="#" class="previousPage NoTip">';
			contents = String.format('<img src="{0}.gif" {1}/>', previousImgRoot, style);
			endHtml = '</a>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			haveLinks = true;
		} else { // we are currently on the first page
			// render first page control element in disabled state
			startHtml = '<span style="color:#646464;">';
			contents = String.format('<img src="{0}_disabled.gif" {1}/>', firstImgRoot, style);
			endHtml = '</span>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			// render previous page control element in disabled state
			startHtml = '<span style="color:#646464;">';
			contents = String.format('<img src="{0}_disabled.gif" {1}/>', previousImgRoot, style);
			endHtml = '</span>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');
		}

		// render text box for entering number of page which user wants to display
		startHtml = String.format(pageXofYText, '<input type="text" class="pageNumber SmallInput" value="' + (pageManager.currentPageIndex + 1) + '" />', pageManager.numberOfPages());

		html.push(startHtml);
		html.push(' | ');

		if (!pageManager.isLastPage(rowCount)) { // we are not currently on the last available page
			// render next page control in enabled state
			startHtml = '<a href="#" class="nextPage NoTip">';
			contents = String.format('<img src="{0}.gif" {1}/>', nextImgRoot, style);
			endHtml = '</a>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			// render last page control in enabled state
			startHtml = '<a href="#" class="lastPage NoTip">';
			contents = String.format('<img src="{0}.gif" {1}/>', lastImgRoot, style);
			endHtml = '</a>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			haveLinks = true;
		} else { // we are currently on the last available page
			// render next page control in disabled state
			startHtml = '<span style="color:#646464;">';
			contents = String.format('<img src="{0}_disabled.gif" {1}/>', nextImgRoot, style);
			endHtml = '</span>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');

			// render last page control in disabled state
			startHtml = '<span style="color:#646464;">';
			contents = String.format('<img src="{0}_disabled.gif" {1}/>', lastImgRoot, style);
			endHtml = '</span>';

			html.push(startHtml + contents + endHtml);
			html.push(' | ');
		}

		// render control for change number of maximum rows per single page
		contents = itemsOnPageText;
		endHtml = '<input type="text" class="pageSize SmallInput" value="' + pageManager.rowsPerPage + '" />';

		html.push(contents + endHtml);

	    // Show how many rows are we displaying
	    html.push('<span class="itemsCount">');
		startHtml = String.format(displayingObjectsText, pageManager.startItem(), Math.min(pageManager.lastItem() - 1, pageManager.totalRowsCount), pageManager.totalRowsCount);
		html.push(startHtml);
		html.push('</span>');

		pager.empty().append(html.join(' '));
		var method = haveLinks ? 'show' : 'hide';
		pager[method]();

		// attach event handlers to individual pager controls
		pager.find('.firstPage').click(function () {
		    createTableFromQuery(uniqueId, 0, pageManager.rowsPerPage, orderColumn);
			return false;
		});

		pager.find('.previousPage').click(function () {
		    createTableFromQuery(uniqueId, currentPageIndex - 1, pageManager.rowsPerPage, orderColumn);
			return false;
		});

		pager.find('.nextPage').click(function () {
		    createTableFromQuery(uniqueId, currentPageIndex + 1, pageManager.rowsPerPage, orderColumn);
			return false;
		});

		pager.find('.lastPage').click(function () {
		    createTableFromQuery(uniqueId, pageManager.numberOfPages() - 1, pageManager.rowsPerPage, orderColumn);
			return false;
		});

		pager.find('.showAll').click(function () {
			// We don't have a good way to show all.  We'll show 1 million and 
			// accept that there's an issue if there are more than that :)
		    createTableFromQuery(uniqueId, 0, 1000000, orderColumn);
			return false;
		});

		var changePageSize = function () {
		    createTableFromQuery(uniqueId, 0, getCurrentPageSize(uniqueId), orderColumn);
		};

		pager.find('.pageSize').change(function () {
			changePageSize();
		});

		pager.find('.pageSize').keydown(function (e) {
			if (e.keyCode == 13) {
				changePageSize();
				return false;
			}
			return true;
		});

		var changePageNumber = function () {
			var pageNumber = pager.find('.pageNumber').val();
			if (pageNumber <= 0) {
				pageNumber = 1;
			} else if (pageNumber > pageManager.numberOfPages()) {
				pageNumber = pageManager.numberOfPages();
			}

			createTableFromQuery(uniqueId, pageNumber - 1, pageManager.rowsPerPage, orderColumn);
		};

		pager.find('.pageNumber').change(function () {
			changePageNumber();
		});

		pager.find('.pageNumber').keyup(function (e) {
			if (e.keyCode == 13) {
				changePageNumber();
				return false;
			}

			return true;
		});
	}
})(SW.DPA.UI.SortablePageableTable);