﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsWithoutRelationshipModal.ascx.cs" Inherits="Orion_DPA_Controls_ApplicationWithoutRelationshipModal" %>

<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />

<orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css"/>
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="DPA_ApplicationsWithoutRelationshipModal" class="DPA_IntegrationWizardSortableTable" style="display:none">
    
    <p><%: Resources.DPAWebContent.IntegrationWizard_ApplicationsWithoutRelationship_Text %></p>
    
    <dpa:SortablePageableTable ID="ApplicationsWithoutRelationshipTable" runat="server" UniqueClientID="1124" />

    <input type="hidden" id="OrderBy-<%: ApplicationsWithoutRelationshipTable.UniqueClientID %>" value="[ApplicationName]" />
    <input type="hidden" id="appsWithoutRelUniqueId" value="<%: ApplicationsWithoutRelationshipTable.UniqueClientID %>" />
    
    <div class="bottom">
        <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
            <a class="sw-btn DPA_CloseButton"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.IntegrationWizard_ApplicationsWithoutRelationship_CloseButton_Text %></span></span></a>
        </div>
    </div>
</div>