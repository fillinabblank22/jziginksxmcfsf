﻿using System.Web.UI;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Controls_ApplicationsUsingMyDatabasesControl : UserControl
{
    public int? DpaServerId { get; private set; }

    public int ResourceId { get; private set; }
    
    public string ErrorMessageClientId { get; private set; }

    public string LoadingIconClientId { get; private set; }

    public void Initialize(int resourceId, string errorMessageId, string loadingIconId, int? dpaServerId)
    {
        DpaServerId = dpaServerId;

        ResourceId = resourceId;
        ErrorMessageClientId = errorMessageId;
        LoadingIconClientId = loadingIconId;

        if (Profile.AllowAdmin)
        {
            startDPAIntegrationButton.PostBackUrl = PageUrlHelper.RelationshipManagementClientApplications;
        }
    }
}