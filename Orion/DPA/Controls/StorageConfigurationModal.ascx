﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageConfigurationModal.ascx.cs" Inherits="Orion_DPA_Controls_StorageConfigurationModal" %>

<%@ Register Src="~/Orion/DPA/Controls/DatabaseInstanceSelector.ascx" TagPrefix="dpa" TagName="DatabaseInstanceSelector" %>
<%@ Register Src="~/Orion/DPA/Controls/LunSelector.ascx" TagPrefix="dpa" TagName="LunSelector" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="DPA_StorageConfigurationModal" style="display:none">
    <div class="DPA_CollapsingBlock DPA_Collapsed" id="DPA_SelectDatabaseInstanceStep">
        <div class="DPA_CollapsingBlockHeader DPA_Clickable"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.RelationshipManagement_Storages_SelectDatabaseInstance_Title %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            <dpa:DatabaseInstanceSelector ID="DatabaseInstanceSelector" runat="server" />
        </div>        
    </div>
    
    <div class="DPA_CollapsingBlock DPA_Collapsed" id="DPA_SelectStorageStep">
        <div class="DPA_CollapsingBlockHeader DPA_Clickable"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.RelationshipManagement_Storages_SelectStorage_Title %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            <dpa:LunSelector ID="LunSelectorSelector" runat="server" />
        </div>
    </div>
    
    <div class="bottom">
        <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
            <a href="javascript:void(0);" id="DPA_NextButton" class="sw-btn sw-btn-primary"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.NextButton_Text %></span></span></a>
            <a href="javascript:void(0);" id="DPA_SaveApplicationButton" class="sw-btn sw-btn-primary"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.StorageRelationshipModal_SaveButton_Label %></span></span></a>
            <a href="javascript:void(0);" id="DPA_CancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.CancelButton_Text %></span></span></a>
        </div>
    </div>
</div>
