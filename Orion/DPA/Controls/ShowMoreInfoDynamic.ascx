﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShowMoreInfoDynamic.ascx.cs" Inherits="Orion_DPA_Controls_ShowMoreInfoDynamic" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<div id="LinkControl" class="DPA_ShowMoreInfoLink" runat="server">
    <a href="javascript: return true;" onclick="(function(){ $('#<%=this.LinkControl.ClientID %>').hide(); $('#<%=this.DetailControl.ClientID %>').show(); return false;})();">
        <%= Resources.DPAWebContent.ShowMoreInfoDynamicControl_ShortText %>
    </a>
</div>
<div id="DetailControl" class="DPA_ShowMoreDetailBubble" style="display:none;" runat="server">
  <div class="sw-suggestion">
    <span class="sw-suggestion-icon"></span>
    <table>
        <tr>
            <td><a href="<%: PageUrlHelper.LinkResponseTimeAnalysisOverview %>" target="_blank"><img src="/Orion/DPA/images/video.png" alt="" /></a></td>
            <td class="helplink">
                <a href="<%: PageUrlHelper.LinkResponseTimeAnalysisOverview %>" target="_blank"><%: Resources.DPAWebContent.ShowMoreInfoDynamicControl_DetailText %></a>
                <div class="videolength">0:55</div>
            </td>
            <td>
                <a onclick="(function(){$('#<%=this.LinkControl.ClientID %>').show(); $('#<%=this.DetailControl.ClientID %>').hide(); return false;})();"
                   href="javascript: return true;" ><div class="closeicon"></div></a>
            </td>
        </tr>
    </table>
    <%= Resources.DPAWebContent.ShowMoreInfoDynamicControl_ListText %>
  </div>
</div>