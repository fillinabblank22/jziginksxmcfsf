﻿using System;
using System.Linq;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.Orion.Web.Helpers;
using ServiceLocator = SolarWinds.DPA.ServiceLocator.ServiceLocator;

public partial class Orion_DPA_Controls_DiscoveryCentralPlugin : System.Web.UI.UserControl
{
    private IDPASettings _settings;
    protected IDPASettings Settings
    {
        get
        {
            return _settings ?? (_settings = ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }

    private int? _dpaDatabaseInstances = null;
    protected int DpaDatabaseInstances
    {
        get
        {
            if (_dpaDatabaseInstances == null)
            {
                if (!Settings.IsIntegrationEstablished)
                    _dpaDatabaseInstances = 0;
                else
                {
                    var dal = ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>();
                    using (var dpaSwisSettingsContext = ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>().CreateContext(PartialResultsMode.EnablePartialResults))
                    {
                        _dpaDatabaseInstances = dal.Count();

                        var partialErrors = dpaSwisSettingsContext.GetPartialErrors();
                        if (partialErrors != null && partialErrors.Any())
                        {
                            ConnectivityIssueWarning.Show(partialErrors);
                        }
                    }
                }
            }
            return _dpaDatabaseInstances.Value;
        }
    }

    protected string DpaDatabaseInstancesText { get; set; }

    protected string ReturnUrl
    {
        get
        {
            var currentUrl = Request.Url.AbsoluteUri;
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(currentUrl);
            return Convert.ToBase64String(plainTextBytes);
        }
    }

    protected string IntegrationWizardUrl
    {
        get { return PageUrlHelper.ServerManagement; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (DpaDatabaseInstances == 0)
        {
            okImage.Visible = false;
            bulbImage.Visible = true;
            monitoredObjectsInfo.Attributes["class"] = "noObjectsStyle";
        }

        string txt = Resources.DPAWebContent.DiscoveryCentralPlugin_MappedDatabaseInstances.Replace("{0}", "<span style=\"font-weight:bold\">{0}</span>");
        DpaDatabaseInstancesText = string.Format(txt, DpaDatabaseInstances);

        DataBind();
    }

    protected void StartDpaIntegrationWizard_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect(PageUrlHelper.ServerManagement);
    }

    protected void ManageRelationships_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect(PageUrlHelper.RelationshipManagementDatabaseInstances);
    }
}