﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_DPA_Controls_LoadingIcon : System.Web.UI.UserControl
{
    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public string UniqueClientID { get; set; }

    public override string ClientID
    {
        get { return "DPA_LoadingIcon-" + UniqueClientID; }
    }

    protected string HelpLink
    {
        get { return HelpHelper.GetHelpUrl("OrionDPAConnectivityIssue"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}