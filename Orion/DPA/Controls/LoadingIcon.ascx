﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoadingIcon.ascx.cs" Inherits="Orion_DPA_Controls_LoadingIcon" %>
<orion:Include runat="server" Module="DPA" File="Resources.js" />

<div id="<%= ClientID %>" style="display:none" class="DPA_LoadingIconWrapper">
	<span class="DPA_LoadingIcon"></span>
    <p class="DPA_LoadingLongerText" style="display:none"><%: Resources.DPAWebContent.LoadingIcon_NotificationText %> 
        <a href="<%: HelpLink %>" target="_blank">&#187; <%: Resources.DPAWebContent.LoadingIcon_Notification_TextOfLink %></a>
    </p>
</div>  