﻿using System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Controls_SimpleWelcomeWindow : System.Web.UI.UserControl
{
    protected bool ShowWelcomeWindow { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var settings = ServiceLocator.Current.GetInstance<IDPASettings>();

        ShowWelcomeWindow = !settings.IsSimpleWelcomeWindowHidden && !settings.IsIntegrationEstablished;
        ConnectDpaButton.NavigateUrl = PageUrlHelper.ServerManagement;
    }
}