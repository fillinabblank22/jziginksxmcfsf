﻿using System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.ServiceLocator;

public partial class Orion_DPA_Controls_StorageConfigurationModal : System.Web.UI.UserControl
{
    protected override void OnLoad(EventArgs e)
    {
        // do not render this control when SRM is not installed
        if (!ServiceLocator.Current.GetInstance<IDPASettings>().IsSrmInstalled)
        {
            this.Visible = false;
        }

        base.OnLoad(e);
    }
}