﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WaitTimeChartControl.ascx.cs" Inherits="Orion_DPA_Controls_WaitTimeChartControl" %>

<div runat="server" id="IntegrationNotEstablishedBlock">
    <img src="/Orion/DPA/images/Samples/InstancesWithTheHighestWaitTimeEmpty.png" />
    <div class="<%= this.NoIntegrationSampleClass %>">
        <div>
            <ul>
                <%= this.NoIntegrationSampleText %>
            </ul>
        </div>
    </div>
</div>
