﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SortablePageableTable.ascx.cs" Inherits="Orion_DPA_Controls_SortablePageableTable" %>
<orion:Include runat="server" File="DPA/Js/DPA.UI.SortablePageableTable.js" />

<div id="ErrorMsg-<%= UniqueClientID %>"></div>

<table id="Grid-<%= UniqueClientID %>" class="sw-custom-query-table DPA_NeedsZebraStripes" cellpadding="2" cellspacing="0" width="100%">
    <tr class="HeaderRow" automation="Row_Header"></tr>
</table>

<div id="Pager-<%= UniqueClientID %>" class="ReportFooter ResourcePagerControl hidden"></div>