﻿using System;

using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;

public partial class Orion_DPA_Controls_RelationshipConfigurationModal : System.Web.UI.UserControl
{
    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            ServerApplicationSelector.GridColumnDefinitions =
                ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaServerApplicationSelectorController.UniqueId);
        }

        base.OnLoad(e);
    }
}