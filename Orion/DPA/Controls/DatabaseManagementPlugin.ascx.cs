﻿using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.InformationService.Contexts;

public partial class Orion_DPA_Controls_DatabaseManagementPlugin : UserControl, IManagementTasksPlugin
{
    private const string ShowResourceScript = @"
<script type='text/javascript'>
    $('.dpaManagementPlugin').closest('.ResourceWrapperPanel').show();
</script>
";
    private bool _databaseInstanceIdNotFound;

    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        DatabaseInstance databaseInstance;
        if (!TryGetDatabaseInstanceId(netObject, out databaseInstance))
        {
            _databaseInstanceIdNotFound = true;

            // return not empty collection to allow hiding whole resource via javascript
            if (IsDbPerfSubView())
            {
                yield return new ManagementTaskItem()
                {
                    Section = String.Empty,
                    LinkInnerHtml = String.Empty,
                    LinkUrl = string.Empty
                };
            }
            
            yield break;
        }

        yield return new ManagementTaskItem
        {
            Section = Resources.DPAWebContent.ManagementResource_Section,
            LinkInnerHtml = ShowResourceScript + "<img src='/Orion/DPA/images/view-trends.png' alt='' />&nbsp;" + Resources.DPAWebContent.ManagementResource_ViewTrends,
            LinkUrl = PageUrlHelper.DatabaseInstanceTrendsDetailPage(databaseInstance.GlobalDatabaseInstanceId),
            NewWindow = true
        };
        yield return new ManagementTaskItem
        {
            Section = Resources.DPAWebContent.ManagementResource_Section,
            LinkInnerHtml = "<img src='/Orion/DPA/images/storage-i-o.png' alt='' />&nbsp;" + Resources.DPAWebContent.ManagementResource_DatabaseIO,
            LinkUrl = PageUrlHelper.StorageIoDetailPage(databaseInstance.GlobalDatabaseInstanceId),
            NewWindow = true
        };
        yield return new ManagementTaskItem
        {
            Section = Resources.DPAWebContent.ManagementResource_Section,
            LinkInnerHtml = "<img src='/Orion/DPA/images/current-activity.png' alt='' />&nbsp;" + Resources.DPAWebContent.ManagementResource_CurrentActivity,
            LinkUrl = PageUrlHelper.CurrentDbDetailPage(databaseInstance.GlobalDatabaseInstanceId),
            NewWindow = true
        };
        yield return new ManagementTaskItem
        {
            Section = Resources.DPAWebContent.ManagementResource_Section,
            LinkInnerHtml =
                ShowResourceScript +
                "<img src='/Orion/PerfStack/images/icons/perfstack_launch.svg' height='16' width='16' alt='' />&nbsp;" +
                Resources.DPAWebContent.ManagementResource_Perfstack,
            LinkUrl =
                PageUrlHelper.DatabaseInstancePerfstackLink(databaseInstance.Type,
                    databaseInstance.GlobalDatabaseInstanceId,
                    databaseInstance.DpaServerId),
            NewWindow = true
        };
    }

    private bool TryGetDatabaseInstanceId(NetObject netObject, out DatabaseInstance databaseInstance)
    {
        var databaseInstanceNetobject = netObject as DatabaseInstanceNetObject;
        if (databaseInstanceNetobject != null)
        {
            databaseInstance = databaseInstanceNetobject.Model;
            return true;
        }

        if (ServiceLocator.IsInitialized && ServiceLocator.Current.GetInstance<IDPASettings>().IsApmInstalled)
        {
            object applicationId;
            var apmApplicationType = Type.GetType(APMConstants.ApplicationNetObjectType);
            if (apmApplicationType != null && apmApplicationType.IsAssignableFrom(netObject.GetType()) && netObject.ObjectProperties.TryGetValue("ApplicationID", out applicationId))
            {
                var dpaSwisSettingsContextFactory = ServiceLocator.Current
                    .GetInstance<IDpaSwisSettingsContextFactory>();

                using (dpaSwisSettingsContextFactory.CreateContext(PartialResultsMode.EnablePartialResults))
                {
                    databaseInstance = ServiceLocator.Current.GetInstance<IDatabaseInstanceApplicationDAL>().GetDatabaseInstanceByApplicationId((int)applicationId);
                    if (databaseInstance != null)
                    {
                        return true;
                    }
                }
            }
        }
        databaseInstance = default(DatabaseInstance);
        return false;
    }

    protected bool IsResourceHidden()
    {
        //hide management resource when it's on the app details' db perf subview
        return _databaseInstanceIdNotFound && IsDbPerfSubView();
    }

    private bool IsDbPerfSubView()
    {
        var viewInfo = Context.Items[typeof(ViewInfo).Name] as ViewInfo;
        return viewInfo != null && viewInfo.ViewKey != null && viewInfo.ViewKey.StartsWith(APMConstants.SubViewKeyPrefix);
    }
}