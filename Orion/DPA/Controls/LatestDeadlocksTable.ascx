﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestDeadlocksTable.ascx.cs" Inherits="Orion_DPA_Controls_LatestDeadlocksTable" %>

<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>
<%@ Register TagPrefix="dpa" TagName="PoweredByIcon" Src="~/Orion/DPA/Controls/PoweredByIcon.ascx" %>


<div runat="server" id="LatestDeadlocks" class="DPA_LatestDeadlocksTable">
    <dpa:SortablePageableTable ID="SortablePageableTable" runat="server"/>
    <a class="DPA_ShowMoreInfoLink" target="_blank" href="<%: SolarWinds.DPA.Web.Helpers.PageUrlHelper.DeadlockList(DatabaseInstanceId) %>">
        &raquo; <%: Resources.DPAWebContent.BlockingImpactResource_DeadlocksTab_ShowMoreInfoLink %>
    </a>
    <dpa:PoweredByIcon runat="server"/>
</div>

