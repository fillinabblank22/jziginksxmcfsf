﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.DPA.Common;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_DPA_Controls_ModuleDetails : System.Web.UI.UserControl
{
    private static Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                // TODO: Use constant
                GetModuleAndLicenseInfo("DPA");
            }
            catch (Exception ex)
            {
                _log.Error("Error while displaying details for WPM module.", ex);
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            DPADetails.Name = module.ProductDisplayName;

            values.Add("Product Name", module.ProductName);
            values.Add("Version", module.Version);
            if (!string.IsNullOrWhiteSpace(Request.QueryString["BuildInfo"]))
            {
                values.Add("Build", typeof(IDPABusinessLayer).Assembly.GetName().Version.ToString());
            }
            values.Add("Service Pack", String.IsNullOrEmpty(module.HotfixVersion) ? "None" : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add("License", module.LicenseInfo);

            AddDPAInfo(values);

            DPADetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }

    private void AddDPAInfo(Dictionary<string, string> values)
    {
		// There is no DPAO license yet.
    }
}