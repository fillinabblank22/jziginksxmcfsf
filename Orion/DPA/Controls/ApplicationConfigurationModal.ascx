﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationConfigurationModal.ascx.cs" Inherits="Orion_DPA_Controls_ApplicationConfigurationModal" %>

<%@ Register Src="~/Orion/DPA/Controls/ApplicationSelector.ascx" TagPrefix="dpa" TagName="ApplicationSelector" %>
<%@ Register Src="~/Orion/DPA/Controls/DatabaseInstanceSelector.ascx" TagPrefix="dpa" TagName="DatabaseInstanceSelector" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="DPA_ApplicationConfigurationModal" style="display:none">
    
    <div class="DPA_CollapsingBlock DPA_Collapsed DPA_Clickable" id="DPA_SelectApplicationStep">
        <div class="DPA_CollapsingBlockHeader"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_SelectApplicationTitle %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            
            <dpa:ApplicationSelector ID="ClientApplicationSelector" runat="server" />

        </div>        
    </div>

    <div class="DPA_CollapsingBlock DPA_Collapsed DPA_Clickable" id="DPA_SelectInstanceStep">
        <div class="DPA_CollapsingBlockHeader"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_SelectInstanceTitle %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            
            <dpa:DatabaseInstanceSelector ID="DatabaseInstanceSelector" runat="server" />
            
        </div>        
    </div>
    
    <div class="bottom">
        <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
            <a href="javascript:void(0);" id="DPA_NextButton" class="sw-btn sw-btn-primary"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.NextButton_Text %></span></span></a>
            <a href="javascript:void(0);" id="DPA_SaveApplicationButton" class="sw-btn sw-btn-primary"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.SaveApplicationButton_Text %></span></span></a>
            <a href="javascript:void(0);" id="DPA_CancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.CancelButton_Text %></span></span></a>
        </div>
    </div>
</div>