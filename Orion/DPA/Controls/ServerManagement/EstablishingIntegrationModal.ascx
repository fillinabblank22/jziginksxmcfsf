﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EstablishingIntegrationModal.ascx.cs" Inherits="Orion_DPA_Controls_IntegrationSetup_EstablishingIntegrationModal" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css" />

<div id="DPA_EstablishingIntegrationModal" style="display: none">
    <div class="DPA_UnrecoverableError" style="display: none">
        <div class="sw-suggestion sw-suggestion-fail">
            <span class="sw-suggestion-icon"></span>
            <div class="DPA_UnexpectedError" style="display: none">
                <span automation="Text_UnexpectedError"><%: Resources.DPAWebContent.EstablishingIntegration_UnexpectedError_Message %></span>
                <span class="DPA_ErrorHint"><%: Resources.DPAWebContent.EstablishingIntegration_UnexpectedError_Hint %></span>
            </div>
            <div class="DPA_Error" automation="Text_UnrecoverableError" style="display: none"></div>
        </div>
        <div id="DPA_DesperateRobot">
            <img src="/Orion/DPA/images/desperate_robot.svg" />
        </div>
    </div>
    <div class="DPA_SuccessMessage" automation="Text_Success" style="display: none"></div>
    <div class="DPA_Progress">
        <div class="DPA_ProgressTitle DPA_InProgress"></div>
        <div class="DPA_ProgressBar" automation="ProgressBar">
            <div class="DPA_ProgressBarDone"></div>
        </div>
        <div class="DPA_ProgressMessage"></div>
    </div>
    <div class="DPA_BottomBar">
        <orion:LocalizableButton
            ID="DPA_CancelIntegrationButton"
            CssClass="DPA_CancelIntegrationButton"
            Enabled="False"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, EstablishingIntegration_CancelButtonLabel %>"
            DisplayType="Secondary"
            runat="server" />
        <orion:LocalizableButtonLink
            ID="DPA_GoToDatabasesSummaryButton"
            Style="display: none"
            CssClass="DPA_EstablishingIntegrationSuccessButtons"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, EstablishingIntegration_GoToDatabasesSummaryButtonLabel %>"
            DisplayType="Primary"
            runat="server" />
        <orion:LocalizableButtonLink
            ID="DPA_ReviewRelationshipsButton"
            Style="display: none"
            CssClass="DPA_EstablishingIntegrationSuccessButtons sw-btn-tertiary"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, EstablishingIntegration_ReviewRelationshipsButtonLabel %>"
            DisplayType="Secondary"
            runat="server" />
        <orion:LocalizableButton
            ID="DPA_CloseDialogButton"
            Style="display: none"
            CssClass="DPA_CloseDialogButton"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, EstablishingIntegration_CloseButtonLabel %>"
            DisplayType="Secondary"
            runat="server" />
    </div>
</div>
