﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IntegrationSetupModal.ascx.cs" Inherits="Orion_DPA_Controls_ServerManagement_IntegrationSetupModal" %>

<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" Module="DPA" File="ServerManagement.js" />
<orion:Include runat="server" Module="DPA" File="Common.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css" />

<div id="DPA_IntegrationSetupModal" style="display: none" automation="Wrapper_IntegrationSetupModal">
    <div class="DPA_IntegrationSetupForm" id="IntegrationSetupForm">
        <asp:Panel runat="server" DefaultButton="SubmitButton">
            <div id="DPA_IntegrationErrorMessageWrapper" style="display: none">
                <div class="sw-suggestion sw-suggestion-fail">
                    <span class="sw-suggestion-icon"></span>
                    <div id="DPA_IntegrationErrorMessage" automation="Error_Integration"></div>
                    <a id="DPA_HowCanIFixThisLink" style="display: none" href="<%: HelpHelper.GetHelpUrl("OrionDPAConnectivityIssue") %>" target="_blank"><%: Resources.DPAWebContent.IntegrationWizard_HowCanIFixThis %></a>
                    <a id="DPA_CheckVersionCompatibilityLink" style="display: none" class="sw-link" href="<%: HelpHelper.GetHelpUrl("OrionDPAUnsupportedVersion") %>" target="_blank">&#187; <%: Resources.DPAWebContent.IntegrationSetup_ConnectionTestResult_UnsupportedVersion_Text_Link %></a>
                </div>
            </div>
            <div class="DPA_InputRow">
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_ServerAddress_Label %>
                </div>
                <div class="DPA_Input DPA_ServerAddress">
                    <asp:TextBox ID="ServerAddress" runat="server" automation="Field_ServerAddress" placeholder="<%$ Resources:DPAWebContent, IntegrationSetup_ServerAddress_Hint %>" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator3"
                        ControlToValidate="ServerAddress"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_ServerAddressNotSet %>"
                        automation="Error_Validation_AddressRequired" />
                    <asp:CustomValidator runat="server"
                        ID="hostnameCustomValidator"
                        ClientValidationFunction="trimAndValidateIpOrHostnameForServerAddress"
                        ControlToValidate="ServerAddress"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_ServerAddressWrongFormat %>"
                        automation="Error_Validation_AddressNotValid" />
                </div>
            </div>
            <div class="DPA_InputRow">
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_Port_Label %>
                </div>
                <div class="DPA_Input DPA_Port">
                    <asp:HiddenField ID="DefaultPortNumber" runat="server" />
                    <asp:TextBox ID="ServerPort" runat="server" automation="Field_ServerPort" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator4"
                        ControlToValidate="ServerPort"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_PortNotSet %>"
                        automation="Error_Validation_PortRequired" />
                    <asp:RangeValidator runat="server"
                        ID="RangeValidator2"
                        ControlToValidate="ServerPort"
                        Display="Dynamic"
                        MinimumValue="1"
                        MaximumValue="65535"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_PortIsNotAllowedNumber %>"
                        Type="Integer"
                        automation="Error_Validation_PortNotValid" />
                </div>
            </div>
            <div class="DPA_InputRow">
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_DisplayName_Label %>
                </div>
                <div class="DPA_Input DPA_DisplayName">
                    <asp:TextBox ID="DisplayName" runat="server" automation="Field_DisplayName" placeholder="<%$ Resources: DPAWebContent, IntegrationSetup_DisplayName_Hint %>" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator1"
                        ControlToValidate="DisplayName"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_DisplayNameNotSet %>"
                        automation="Error_Validation_DisplayNameRequired" />
                </div>
            </div>
            <div id="DPA_OrionHostnameWrapper" class="DPA_InputRow">
                <asp:CheckBox runat="server" id="DPA_OrionHostnameCheckbox" CssClass="DPA_Checkbox" text="<%$ Resources: DPAWebContent, IntegrationSetup_OrionAddress_UseDefault %>" Checked="True"/>
                <div id="DPA_OrionHostnameErrorWrapper" style="display: none">
                    <div class="sw-suggestion sw-suggestion-fail" automation="Error_OrionHostname">
                        <span class="sw-suggestion-icon"></span>
                        <%= Resources.DPAWebContent.IntegrationSetup_ConnectionTestResult_UnableToPingBack %>
                    </div>
                </div>
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_OrionAddress_Label %>
                </div>
                <div class="DPA_Input DPA_OrionHostname">
                    <asp:HiddenField ID="SwisHostname" runat="server" />
                    <asp:TextBox ID="OrionHostname" automation="Field_OrionHostname" runat="server" Enabled="False" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator2"
                        ControlToValidate="OrionHostname"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_ServerAddressNotSet %>"
                        automation="Error_Validation_OrionHostnameRequired" />
                    <asp:CustomValidator runat="server"
                        ID="CustomValidator1"
                        ControlToValidate="OrionHostname"
                        ClientValidationFunction="trimAndValidateIpOrHostnameForOrionServerAddress"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_ServerAddressWrongFormat %>"
                        automation="Error_Validation_OrionHostnameNotValid" />
                </div>
                <div class="DPA_HelpBlock">
                    <span class="DPA_QstnIcon"></span>
                </div>
            </div>
            <div class="DPA_InputRow DPA_CredentialsHeader">
                <span><%= Resources.DPAWebContent.IntegrationSetup_Credentials_Title %></span>
            </div>
            <div class="DPA_InputRow">
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_Username_Label %>
                </div>
                <div class="DPA_Input DPA_UserName">
                    <asp:HiddenField ID="DefaultUserName" runat="server" />
                    <asp:TextBox runat="server" ID="UserName" automation="Field_UserName" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator5"
                        ControlToValidate="UserName"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_UsernameNotSet %>"
                        automation="Error_Validation_UserNameRequired" />
                </div>
            </div>
            <div class="DPA_InputRow">
                <div class="DPA_InputLabel">
                    <%= Resources.DPAWebContent.IntegrationSetup_Password_Label %>
                </div>
                <div class="DPA_Input DPA_Password">
                    <asp:TextBox runat="server" ID="Password" TextMode="Password" automation="Field_Password" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="RequiredFieldValidator6"
                        ControlToValidate="Password"
                        Display="Dynamic"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_Validation_PasswordIsNotSet %>"
                        automation="Error_Validation_PasswordRequired" />
                </div>
            </div>
            <div class="DPA_TestConnectionDiv">
                <div class="DPA_TestConnectionStatus" style="display: none">
                    <div id="DPA_TestConnectionErrorMessageWrapper" style="display: none">
                        <div class="sw-suggestion sw-suggestion-fail">
                            <span class="sw-suggestion-icon"></span>
                            <div id="DPA_TestConnectionErrorMessage" automation="Error_TestConnection"></div>
                            <a id="DPA_TestConnectionCompatibilityLink" style="display: none" class="sw-link" href="<%: HelpHelper.GetHelpUrl("OrionDPAUnsupportedVersion") %>" target="_blank">&#187; <%: Resources.DPAWebContent.IntegrationSetup_ConnectionTestResult_UnsupportedVersion_Text_Link %></a>
                        </div>
                    </div>
                    <div class="sw-suggestion sw-suggestion-pass DPA_TestConnectionSuccess">
                        <span class="sw-suggestion-icon"></span>
                        <span class="DPA_TestConnectionSuccessMessage" automation="Success_TestConnection"></span>
                    </div>
                    <span class="DPA_Spinner" style="display: none" automation="Icon_TestConnection_Spinner"><%= Resources.DPAWebContent.TestConnection_Validating %></span>
                    <orion:LocalizableButton runat="server"
                        ID="CancelTestConnection"
                        CausesValidation="False"
                        DisplayType="Secondary"
                        Text="<%$ Resources: DPAWebContent, IntegrationSetup_ConnectionTestCancel_Button %>"
                        CssClass="DPA_CancelTestConnectionButton" />
                </div>
                <orion:LocalizableButton runat="server"
                    ID="TestConnectionButton"
                    CausesValidation="True"
                    DisplayType="Secondary"
                    CssClass="DPA_TestConnectionButton"
                    Text="<%$ Resources: DPAWebContent, IntegrationSetup_ConnectionTest_Button %>" />
            </div>
        </asp:Panel>
    </div>
    <div class="DPA_SideBar" automation="Wrapper_HelpPanel">
        <div class="sw-suggestion sw-suggestion-info" id="helpPanel">
            <span class="sw-suggestion-icon"></span>
            <h4 class="DPA_HintTitle"><%= Resources.DPAWebContent.IntegrationSetup_LinkYourDpa_Title %></h4>
            <p class="DPA_HintDescription"><%= Resources.DPAWebContent.IntegrationSetup_LinkYourDpa_Description %></p>
            <br />
            <h4 class="DPA_HintTitle"><%= Resources.DPAWebContent.IntegrationWizard_AdminCredentialsTitle %></h4>
            <p class="DPA_HintDescription"><%= Resources.DPAWebContent.IntegrationWizard_AdminCredentialsHint %></p>
            <span class="DPA_HintLink">
                <a href="<%: PageUrlHelper.DpaCrossProductIntegration %>" target="_blank" class="sw-link" automation="Link_LearnMoreAboutIntegration">&raquo; <%= Resources.DPAWebContent.IntegrationWizard_AdminCredentialsLearnMore %></a>
            </span>
        </div>
    </div>
    <div class="DPA_BottomBar">
        <orion:LocalizableButton
            ID="SubmitButton"
            CssClass="DPA_SubmitButton"
            CausesValidation="True"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, IntegrationSetup_SubmitButton_Label %>"
            DisplayType="Primary"
            runat="server" />
        <orion:LocalizableButton
            ID="CloseButton"
            CssClass="DPA_CloseButton"
            CausesValidation="False"
            LocalizedText="CustomText"
            Text="<%$ Resources: DPAWebContent, EstablishingIntegration_CancelButtonLabel %>"
            DisplayType="Secondary"
            runat="server" />
    </div>
</div>
<script type="text/javascript">
    var trimAndValidateIpOrHostnameForServerAddress = function (source, args) {
        args.Value = SW.DPA.Common.trimHostname(args.Value);
        $('#<%= ServerAddress.ClientID %>').val(args.Value);

        args.IsValid = SW.DPA.Admin.ServerManagement.Validators.validateIpHostname(args.Value);
    }

    var trimAndValidateIpOrHostnameForOrionServerAddress = function (source, args) {
        args.Value = SW.DPA.Common.trimHostname(args.Value);
        $('#<%= OrionHostname.ClientID %>').val(args.Value);

        args.IsValid = SW.DPA.Admin.ServerManagement.Validators.validateIpHostname(args.Value);
    }
</script>
