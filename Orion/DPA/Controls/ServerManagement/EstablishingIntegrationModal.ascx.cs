﻿using System;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Controls_IntegrationSetup_EstablishingIntegrationModal : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DPA_GoToDatabasesSummaryButton.NavigateUrl = PageUrlHelper.DpaSummaryPage;
        DPA_ReviewRelationshipsButton.NavigateUrl = PageUrlHelper.RelationshipManagementDatabaseInstances;
    }
}