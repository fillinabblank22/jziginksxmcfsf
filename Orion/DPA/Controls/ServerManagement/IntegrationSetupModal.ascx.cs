﻿using System;
using System.Globalization;
using SolarWinds.DPA.Data.Helpers;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.Orion.Web;

public partial class Orion_DPA_Controls_ServerManagement_IntegrationSetupModal : System.Web.UI.UserControl
{
    protected string MainSwisHostname
    {
        get
        {
            return RequestCache.Get("DPA:MainSwisHostname",
                () => ServiceLocator.Current.GetInstance<ISwisHostnameHelper>().GetMainSwisHostname());
        }
    }

    protected string DefaultPort
    {
        get
        {
            return SolarWinds.DPA.Common.CommonConstants.DefaultJSwisPortNumber.ToString(CultureInfo.InvariantCulture);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DefaultPortNumber.Value = DefaultPort;
        DefaultUserName.Value = SolarWinds.DPA.Common.CommonConstants.DefaultDpaAdminAccount;

        OrionHostname.Text = MainSwisHostname;
        SwisHostname.Value = MainSwisHostname;
    }
}