﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientApplicationCandidatesSelector.ascx.cs" 
    Inherits="Orion_DPA_Controls_ClientApplicationCandidatesSelector" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Model.RelationshipManagement" %>
<%@ Import Namespace="SolarWinds.DPA.Web.UI.RelationshipsManagement" %>

<orion:Include runat="server" Module="DPA" File="ExtJsGroupByGrid.js" />
<orion:Include runat="server" Module="DPA" File="Selectors.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="clientApplicationCandidateSelector">
    <div id="clientApplicationCandidateSelectorGridPlaceholder" class="sw-relationshipsGrid" style="height:400px"></div>
    <div>
        <input type="hidden" id="settingsPrefix" value='<%= GridColumnDefinitions.IdentificationPrefix %>' />
        <input type="hidden" id="controllerName" value='<%= GridColumnDefinitions.ControllerName %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.SortOrderKey%>" value='<%= GridColumnDefinitions.GetSortOrder() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.PageSizeKey%>" value='<%= GridColumnDefinitions.GetPageSize() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.GroupingValueKey%>" value='<%= GridColumnDefinitions.GetGroupingValue() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.SelectedColumnsKey%>" value='<%= GridColumnDefinitions.GetSelectedColumns() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.ColumnsKey%>" value='<%= GridColumnDefinitions.GetColumns() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.ColumnsModelConfigKey%>" value='<%= GridColumnDefinitions.GetColumnsModelConfig() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.IdentificationPrefix%>ModelIdentifierName" value='<%= RelationshipManagementPageBase<RelationshipManagementModel>.SessionIdentifier %>' />
    </div>
</div>

