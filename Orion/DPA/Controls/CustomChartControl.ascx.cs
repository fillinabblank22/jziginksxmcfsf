﻿using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

public partial class Orion_DPA_Resources_DatabaseResponseTime_CustomChartControl : UserControl, IPropertyProvider
{
    private readonly Log log = new Log();

    public static class PropName
    {
        public const string ContextNetObject = Constants.ContextNetObject;
        public const string DataNetObjects = Constants.DataNetObjects;
        public const string NetObjectPrefix = "NetObjectPrefix";
        public const string ShowTitle = "ShowTitle";
        public const string ChartTitle = "ChartTitle";
        public const string ChartSubtitle = "ChartSubTitle";
        public const string ChartDateSpan = "ChartDateSpan";
        public const string ChartInitialZoom = "ChartInitialZoom";
        public const string ChartSampleSize = "SampleSize";
        public const string LicensingWarningId = Constants.LicensingWarningId;
        public const string ConnectivityIssueWarningId = Constants.ConnectivityIssueWarningId;
        public const string HidePoweredByIcon = Constants.HidePoweredByIcon;
    }

    #region properties

    private Dictionary<string, string> ChartSettings { get; set; }

    private readonly Dictionary<string, object> dataProperties = new Dictionary<string, object>();

    public IDictionary<string, object> Properties
    {
        get { return this.dataProperties; }
    }

    public string ContextNetObject
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ContextNetObject.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ContextNetObject.ToLowerInvariant()] = value; }
    }

    public string[] DataNetObjects
    {
        get { return (string[])this.dataProperties.GetValueOrDefault(PropName.DataNetObjects.ToLowerInvariant(), new string[] { }); }
        set { this.dataProperties[PropName.DataNetObjects.ToLowerInvariant()] = value; }
    }

    public string NetObjectPrefix
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.NetObjectPrefix.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.NetObjectPrefix.ToLowerInvariant()] = value; }
    }

    public bool ShowTitle
    {
        get { return Convert.ToBoolean(this.dataProperties.GetValueOrDefault(PropName.ShowTitle.ToLowerInvariant(), true)); }
        set { this.dataProperties[PropName.ShowTitle.ToLowerInvariant()] = value; }
    }

    public string ChartTitle
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartTitle.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ChartTitle.ToLowerInvariant()] = value; }
    }

    public string ChartSubtitle
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartSubtitle.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ChartSubtitle.ToLowerInvariant()] = value; }
    }

    public string ChartDateSpan
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartDateSpan.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ChartDateSpan.ToLowerInvariant()] = value; }
    }

    public string ChartInitialZoom
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartInitialZoom.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ChartInitialZoom.ToLowerInvariant()] = value; }
    }

    public string ChartSampleSize
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ChartSampleSize.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ChartSampleSize.ToLowerInvariant()] = value; }
    }

    public string LicensingWarningId
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.LicensingWarningId.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.LicensingWarningId.ToLowerInvariant()] = value; }
    }

    public string ConnectivityIssueWarningId
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.ConnectivityIssueWarningId.ToLowerInvariant(), string.Empty)); }
        set { this.dataProperties[PropName.ConnectivityIssueWarningId.ToLowerInvariant()] = value; }
    }

    public string HidePoweredByIcon
    {
        get { return Convert.ToString(this.dataProperties.GetValueOrDefault(PropName.HidePoweredByIcon.ToLowerInvariant(), false)); }
        set { this.dataProperties[PropName.HidePoweredByIcon.ToLowerInvariant()] = value; }
    }

    #endregion // properties

    protected override void OnInit(EventArgs e)
    {
        IncludeFilesHelper.IncludeScriptsForCharts();
    
        this.InitChartAndLegend();
        base.OnInit(e);
    }

    private void InitChartAndLegend()
    {
        this.ChartSettings = new Dictionary<string, string>();
        SolarWinds.DPA.Data.DAL.ChartSettingsDAL.GetChartSettings(this.NetObjectPrefix, (key, value) => this.ChartSettings[key] = value);

        this.ccStack.CustomChartOptions = this.ChartSettings["ChartOptions"];
        this.ccStack.CustomChartSettings = this.GetCustomChartSettings();
        this.ccStack.LegendUserControl = this.CreateLegendUserControl();
    }

    public static int TryParseInt(string text, int defaultValue)
    {
        int value;
        return int.TryParse(text, out value) ? value : defaultValue;
    }

    private IChartLegendControl CreateLegendUserControl()
    {
        string legendUserControlPath;

        if (this.ChartSettings.TryGetValue("LegendUserControl", out legendUserControlPath) &&
            !string.IsNullOrEmpty(legendUserControlPath))
        {
            var control = LoadControl(legendUserControlPath);
            return (IChartLegendControl)control;
        }

        return null;
    }

    private Dictionary<string, object> GetCustomChartSettings()
    {
        var contextNetObject = NetObjectFactory.Create(this.ContextNetObject);
        var dataNetObjects = this.DataNetObjects.Select(netObject => NetObjectFactory.Create(netObject).NetObjectID).ToArray();

        // Parse macros in the title/subtitle (if they are there)
        var title = this.ChartTitle ?? string.Empty;
        var subtitle = this.ChartSubtitle ?? string.Empty;
        if (title.Contains("${") || subtitle.Contains("${"))
        {
            var context = GetContextForMacros(contextNetObject); // netObject == null handled there

            title = Macros.ParseDataMacros(title, context, true, false, true);
            subtitle = Macros.ParseDataMacros(subtitle, context, true, false, true);
        }

        var details = new Dictionary<string, object>();

        details["dataUrl"] =  this.ChartSettings["DataUrl"];
        details["netObjectIds"] = dataNetObjects;
        details["showTitle"] = true;
        details["title"] = title;
        details["subtitle"] = subtitle;
        details["DaysOfDataToLoad"] = TryParseInt(this.ChartDateSpan, 5);
        details["initialZoom"] = this.ChartInitialZoom;
        details["sampleSizeInMinutes"] = TryParseInt(this.ChartSampleSize, 5);
        details["colorTheme"] = "ZoomColorTheme";

        details[Constants.LicensingWarningId] = this.LicensingWarningId;
        details[Constants.ConnectivityIssueWarningId] = this.ConnectivityIssueWarningId;
        details[Constants.HidePoweredByIcon] = HidePoweredByIcon;
        return details;
    }

    protected virtual Dictionary<string, object> GetContextForMacros(NetObject netObject)
    {
        // shall never return null
        return (netObject != null && netObject.ObjectProperties != null) ? netObject.ObjectProperties : new Dictionary<string, object>();
    }
}