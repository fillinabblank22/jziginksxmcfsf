﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseInstancesControl.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseInstancesBase" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>

<div class="DPA_DatabaseInstancesWrapper" id="DPA_DatabaseInstances-<%= ResourceId %>-Content" style="display: none">
    <div class="DPA_StatusFilterWrapper">
        <div class="DPA_StatusFilterItem DPA_StatusFilterCritical">
            <img src="/Orion/DPA/images/StatusIcons/performance-critical.png"/>
            <span class="DPA_StatusFilterItemContent"></span>
        </div><div class="DPA_StatusFilterItem DPA_StatusFilterWarning">
            <img src="/Orion/DPA/images/StatusIcons/performance-warning.png"/>
            <span class="DPA_StatusFilterItemContent"></span>
        </div><div class="DPA_StatusFilterItem DPA_StatusFilterNormal">
            <img src="/Orion/DPA/images/StatusIcons/performance-normal.png"/>
            <span class="DPA_StatusFilterItemContent"></span>
        </div><div class="DPA_StatusFilterItem DPA_StatusFilterUnknown">
            <img src="/Orion/DPA/images/StatusIcons/performance-unknown.png"/>
            <span class="DPA_StatusFilterItemContent"></span>
        </div>
    </div>
    
    <div class="DPA_SortingSearching">
        <div class="DPA_Sorting DPA_SortByOptions">
            <label><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxLabel %>:</label>
            <select>
                <option value=""><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxStatus %></option>
                <option value="DatabaseInstanceName"><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxDbInstanceName %></option>
                <option value="NodeCaption"><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxNodeName %></option>
                <option value="Type"><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxType %></option>
            </select>
        </div>
        <div class="DPA_Sorting DPA_FilterByOptions">
            <label><%: Resources.DPAWebContent.DatabaseInstancesResource_ComboBoxFilterByTypeLabel %>:</label>
            <select>
                <option value=""><%: Resources.DPAWebContent.DatabaseInstancesResource_FilterByDefaultValue %></option>
                <% foreach (var dbType in SolarWinds.DPA.Common.Models.DatabaseInstanceType.DatabaseInstanceTypes.Where(type => type != SolarWinds.DPA.Common.Models.DatabaseInstanceType.Unknown).OrderBy(e => e)) { %>
                    <option value="<%: dbType %>"><%: dbType %></option>
                <% } %>
            </select>
        </div>
        <div class="DPA_Searching">
            <input type="text" placeholder="<%: Resources.DPAWebContent.DatabaseInstancesResource_SearchPlaceholder %>" />
            <img src="/Orion/DPA/images/cmd-clearsearch.png" alt="clear" class="DPA_ClearButton" />
            <img src="/Orion/DPA/images/cmd-search.png" alt="search" class="DPA_SearchButton" />
        </div>
    </div>
    <div runat="server" ID="DatabaseInstanceGrid"></div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var uniqueId = "<%= ResourceId %>";
        var resourceTitle = "<%= Title %>";
        var dpaServerId = '<%= DpaServerId %>';
        var statusFilter = new SW.DPA.UI.DatabaseInstanceStatusFilter($('#DPA_DatabaseInstances-' + uniqueId + '-Content'), function () { grid.refresh(); });

        var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= LoadingIconClientId %>');
        var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= ConnectivityIssueWarningClientId %>');
        var errorMessage = new SW.DPA.UI.ErrorMessage('<%= ErrorMessageClientId %>');

        var noDataImage = "<svg xmlns='http://www.w3.org/2000/svg' width='196.37' height='122.57' viewBox='0 0 196.37 122.57'><defs><style>.a,.d{opacity:0.2;}.b,.d{fill:#bebebe;}.c{fill:#fff;stroke:#e4e4e4;stroke-linejoin:round;}</style></defs><title>Search_empty</title><g class='a'><rect class='b' x='17.5' y='9.5' width='80' height='90'/><rect class='b' x='17.5' y='70.5' width='80' height='13'/><rect class='b' x='17.5' y='4.5' width='80' height='5'/><polygon class='b' points='4.5 80.5 4.5 19.5 17.5 9.5 17.5 70.5 4.5 80.5'/><polygon class='b' points='110.5 80.5 110.5 19.5 97.5 9.5 97.5 70.5 110.5 80.5'/><polygon class='b' points='18 10 32 30 32 70.5 18 70.5 18 10'/><line class='b' x1='17.5' y1='30.5' x2='97.5' y2='30.5'/></g><rect class='c' x='13.5' y='5.5' width='80' height='90'/><rect class='c' x='13.5' y='66.5' width='80' height='13'/><rect class='c' x='13.5' y='0.5' width='80' height='5'/><polygon class='c' points='0.5 76.5 0.5 15.5 13.5 5.5 13.5 66.5 0.5 76.5'/><polygon class='c' points='106.5 76.5 106.5 15.5 93.5 5.5 93.5 66.5 106.5 76.5'/><polygon class='d' points='14 6 28 26 28 66.5 14 66.5 14 6'/><line class='c' x1='13.5' y1='26.5' x2='93.5' y2='26.5'/><g class='a'><path class='b' d='M59.05,61.79A40.08,40.08,0,1,0,111.37,40,40.08,40.08,0,0,0,59.05,61.79Z'/><path class='b' d='M195.87,118.14h0c-1.67,4-7,5.62-11.79,3.46l-45.17-20.42,4.58-11.12,46.45,17.31C194.84,109.2,197.54,114.09,195.87,118.14Z'/><rect class='b' x='134.57' y='90.04' width='4.01' height='7.35' transform='translate(-2.09 184.32) rotate(-67.62)'/><rect class='b' x='133.6' y='93.57' width='13.36' height='3.34' rx='1' ry='1' transform='translate(-1.2 188.69) rotate(-67.62)'/><circle class='b' cx='96.11' cy='77.06' r='35.07' transform='translate(-22.24 42.89) rotate(-22.62)'/><path class='b' d='M63.73,90.55a35.07,35.07,0,0,1,47.87-45A35.07,35.07,0,0,0,85,110.31,35,35,0,0,1,63.73,90.55Z'/></g><path class='c' d='M55.05,57.79A40.08,40.08,0,1,0,107.37,36,40.08,40.08,0,0,0,55.05,57.79Z'/><path class='c' d='M191.87,114.14h0c-1.67,4-7,5.62-11.79,3.46L134.92,97.18l4.58-11.12,46.45,17.31C190.84,105.2,193.54,110.09,191.87,114.14Z'/><rect class='c' x='130.57' y='86.04' width='4.01' height='7.35' transform='translate(-0.87 178.14) rotate(-67.62)'/><rect class='c' x='129.6' y='89.57' width='13.36' height='3.34' rx='1' ry='1' transform='translate(0.02 182.51) rotate(-67.62)'/><circle class='c' cx='92.11' cy='73.06' r='35.07' transform='translate(-21.01 41.04) rotate(-22.62)'/><path class='d' d='M59.73,86.55a35.07,35.07,0,0,1,47.87-45A35.07,35.07,0,0,0,81,106.31,35,35,0,0,1,59.73,86.55Z'/></svg>";
        var noDataTemplate = _.templateExt(['<div class="DPA_NoResultsWrapper"><div>{{noDataImage}}</div><h3>{{{title}}}</h3><p>{{{description}}}</p></div>']);


        var config = {
            isNodeFunctionalityAllowed:  <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>,
            uniqueClientId: uniqueId,
            supplementaryRow: {
                visible: true,
                showDpaLabel: <%= DpaServerId.HasValue ? "1===0" : "1===1" %>,
            },
            controls: {
                loadingIcon: loadingIcon,
                errorMessage: errorMessage,
                connectivityIssueWarning: connectivityIssueWarning
            },
            url: "/api/DpaDatabaseInstances/GetDatabaseInstances",
            getParameters: function () {
                return {
                    OrderByClause: $("#DPA_DatabaseInstances-<%= ResourceId %>-Content .DPA_SortByOptions select option:selected").val(),
                    Query: $("#DPA_DatabaseInstances-<%= ResourceId %>-Content .DPA_Searching input[type=text]").val(),
                    FilterStatuses: (function () {
                        var filters = [];
                        if (statusFilter.isCriticalSelected()) {
                            filters.push(<%= (int) SolarWinds.Shared.StatusInfo.CriticalStatusId %>);
                        }
                        if (statusFilter.isWarningSelected()) {
                            filters.push(<%= (int) SolarWinds.Shared.StatusInfo.WarningStatusId %>);
                        }
                        if (statusFilter.isNormalSelected()) {
                            filters.push(<%= (int) SolarWinds.Shared.StatusInfo.UpStatusId %>);
                        }
                        if (statusFilter.isUnknownSelected()) {
                            filters.push(<%= (int) SolarWinds.Shared.StatusInfo.UnknownStatusId %>);
                        }

                        return filters;
                    })(),
                    FilterDbType: (function() {
                        return $("#DPA_DatabaseInstances-<%= ResourceId %>-Content .DPA_FilterByOptions select option:selected")
                            .val();
                    })(),
                    DpaServerId: dpaServerId
                };
            },
            onSuccess: function (result) {
                var titleElement = $('div.ResourceWrapper[resourceid=<%= ResourceId %>] h1 a');
                var titleText = titleElement.text().trim();
                if (titleText === resourceTitle) {
                    titleElement.append(' (' + result.TotalRows +')');
                }
                
                if (typeof result.Metadata.IntegrationEstablished === 'undefined' &&
                    !result.Metadata.IntegrationEstablished) {
                }

                if (typeof result.Metadata.IntegrationEstablished !== 'undefined' && !result.Metadata.IntegrationEstablished) {
                    $("#DPA_DatabaseInstances-<%= ResourceId %>-Content").hide();
                    $("#DPA_DatabaseInstances-<%= ResourceId %>-NoDataContent").show();
                    return false;
                } else {
                    if (result.TotalRows !== 0 || !(typeof result.Metadata.PartialResultErrors !== 'undefined' && result.Metadata.PartialResultErrors.length > 0)) {
                        statusFilter.init(result.Metadata.CriticalCount,
                            result.Metadata.WarningCount,
                            result.Metadata.NormalCount,
                            result.Metadata.UnknownCount);
                    }

                    var contentWrapper = $("#DPA_DatabaseInstances-<%= ResourceId %>-Content");
                    if (contentWrapper.length === 0)
                        console.error("#DPA_DatabaseInstances-<%= ResourceId %>-Content");
                    $("#DPA_DatabaseInstances-<%= ResourceId %>-Content").show();
                    $("#DPA_DatabaseInstances-<%= ResourceId %>-NoDataContent").hide();
                    return true;
                }
            },
            noDataMessage: {
                show: true,
                isHtml: true,
                formatter: function () {
                    var query = $("#DPA_DatabaseInstances-<%= ResourceId %>-Content .DPA_Searching input[type=text]")
                        .val();
                    var dbType = $("#DPA_DatabaseInstances-<%= ResourceId %>-Content .DPA_FilterByOptions select option:selected")
                        .val();

                    var title = "";
                    var description = "";
                    if (query !== '') {
                        title = '<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsForQueryTitle %> "' + query + '"';
                        description = "<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsText %>";
                    } else if (statusFilter.isAnyFilterSelected()) {
                        title = '<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsForFiltersTitle %>';
                        description = "<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsForFiltersDescription %>";
                    } else if (dbType !== '') {
                        title = '<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsForQueryTitle %> ' + dbType;
                        description = "<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsForDbTypeDescription %>";
                    } else {
                        title = '<%: SolarWinds.DPA.Strings.Resources.DatabaseInstancesNoResultsFoundTitle %>';
                    }

                    return noDataTemplate({
                        noDataImage: noDataImage,
                        title: title,
                        description: description
                    });
                },
                cssClassProvider: function () {
                    return "DPA_DatabaseInstancesNoDataRow";
                }
            }
        };

        var grid = new DPA.UI.DatabaseInstancesGrid.Grid($("#<%= DatabaseInstanceGrid.ClientID %>"), config);
        grid.init(function () {
            var refresh = function () {
                grid.refresh();
            };

            SW.Core.View.AddOnRefresh(refresh, '<%= ResourceId %>');

            var content = $('#DPA_DatabaseInstances-<%= ResourceId %>-Content'),
                searching = content.find('.DPA_Searching'),
                searchField = searching.find('input[type=text]');

            content.find('.DPA_Sorting select').change(refresh);                                        // sorting drop drown selection change
            searching.find('.DPA_SearchButton').click(refresh);                                         // search button click
            searching.find('.DPA_ClearButton').click(function () { searchField.val(''); refresh(); });  // clean button click
            searchField.on("keydown", function (e) { if (e.which === 13) refresh(); });                  // press enter on input field

            grid.refresh();
        });
    });
</script>
