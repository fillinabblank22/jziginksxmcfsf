﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.InformationService.Contexts;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Extensions;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_DPA_Controls_MultiTabControl : UserControl
{
    public int ResourceId { get; private set; }
    public bool HideEditButton { get; private set; }
    public IList<PartialResultsError> PartialResultsErrors { get; private set; }

    protected class QueryParamName
    {
        public const string NetObject = "NetObject";
        public const string ViewId = "ViewID";
        public const string ReturnTo = "ReturnTo";
        public const string ResourceId = "ResourceID";
    }

    protected class CssClasses
    {
        public const string Item = "DPA_item";
        public const string Active = "DPA_active";
    }

    private readonly Log _log = new Log();

    protected DatabaseResponseTimeBaseResource.TabSettings[] Tabs { get; private set; }

    protected string DefaultTabId { get; private set; }
    protected bool DefaultTabOnly { get; private set; }
    protected bool IsSamClient { get; private set; }
    protected string CustomEditUrl { get; private set; }
    protected string ConnectivityIssueWarningClientId { get; private set; }

    private INodeProvider _nodeProvider;
    private Func<Dictionary<string, object>, Dictionary<string, object>, IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>> _parseResourceProperties;
    private Func<IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>, NetObject, IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>> _adjustResourceProperties; 

    private string ActiveTabId
    {
        get
        {
            var defaultTab = Tabs.FirstOrDefault(t => t.TabId == DefaultTabId) ?? Tabs.FirstOrDefault();
            return ViewState.GetStrictOrDefault(ActiveTabStateKey, (defaultTab != null ? defaultTab.TabId : string.Empty));
        }
        set { ViewState[ActiveTabStateKey] = value; }
    }

    private DatabaseResponseTimeBaseResource.TabSettings ActiveTab
    {
        get { return Tabs.FirstOrDefault(t => t.TabId == ActiveTabId); }
    }

    private string ActiveTabStateKey
    {
        get { return string.Format("MultiTabResource{0}_ActiveTabId", ResourceId); }
    }

    public void Initialize(int resourceId, INodeProvider nodeProvider, IViewHelper viewHelper,
        Dictionary<string, object> rawProperties, string connectivityIssueWarningClientId,
        Func
            <Dictionary<string, object>, Dictionary<string, object>,
                IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>> parseResourceProperties,
        Func
            <IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>, NetObject,
                IEnumerable<DatabaseResponseTimeBaseResource.TabSettings>> adjustResourceProperties)
    {
        ResourceId = resourceId;
        ConnectivityIssueWarningClientId = connectivityIssueWarningClientId;
        _nodeProvider = nodeProvider;
        _adjustResourceProperties = adjustResourceProperties;
        _parseResourceProperties = parseResourceProperties;

        using (
            var swisSettingsContext =
                ServiceLocator.Current.GetInstance<IDpaSwisSettingsContextFactory>()
                    .CreateContext(PartialResultsMode.EnablePartialResults))
        {
            IsSamClient = ("1" ==
                                rawProperties.GetValueOrDefault(
                                    DatabaseResponseTimeBaseResource.PropName.IsSAMClient.ToLowerInvariant(), "0")
                                    .ToString());
            
            var databaseInstancesIds = (viewHelper.Location == ResourceLocation.SAMClientView)
                ? viewHelper.ClientGlobalDatabaseInstanceIds.ToArray()
                : new[] {viewHelper.GlobalDatabaseInstanceId};

            var perViewUniqueId = viewHelper.Location == ResourceLocation.SAMClientView
                ? viewHelper.ApplicationId
                : viewHelper.GlobalDatabaseInstanceId;

            databaseInstancesTable.Initialize(
                databaseInstancesIds,
                perViewUniqueId,
                ResourceId,
                ConnectivityIssueWarningClientId,
                update.UniqueID
                );

            ParseTabSettings(rawProperties);

            if (DefaultTabOnly && Tabs.All(t => t.TabId != DefaultTabId))
            {
                WrongTabNoData.Visible = true;
            }
            else
            {
                WrongTabNoData.Visible = false;

                RenderTabHeaders();
                ParseActiveTab();
                RenderTabContent();
            }

            PartialResultsErrors = swisSettingsContext.GetPartialErrors();
        }

        // skip pre-render if resource is hidden
        if (!Visible)
            return;

        if (PartialResultsErrors.Any())
        {
            return;
        }

        RenderActiveTab();

        var script = string.Format(CultureInfo.InvariantCulture,
            @"$(document).ready(function() {{ {0}($('#{1}'), '{2}'); }});",
            databaseInstancesTable.AsyncLoadDataJsCallback,
            staticContent.ClientID,
            ActiveTab.TabName
            );

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), UniqueID + "1", script, true);
    }

    private Control LoadResource(string resourcePath)
    {
        try
        {
            return Page.LoadControl(resourcePath);
        }
        catch (Exception ex)
        {
            var msg = string.Format(CultureInfo.InvariantCulture, "failed to load resource '{0}'", resourcePath);
            _log.Error(msg, ex);
            return new HtmlGenericControl("div")
            {
                InnerText = msg
            };
        }
    }

    #region tab headers rendering

    private void ParseTabSettings(Dictionary<string, object> properties)
    {
        // get context net object (and check whether factory is able to create the BBI netobject)
        var dbiNetObject = string.Format("{0}:{1}", NetObjectPrefix.DatabaseInstance, databaseInstancesTable.SelectedDatabaseInstance);
        var contextNetObject = NetObjectFactory.Create(dbiNetObject);

        // create 'common' global properties
        var globalProperties = new Dictionary<string, object>
        {
            {Constants.ContextNetObject, contextNetObject.NetObjectID},
            {Constants.DataNetObjects, GetDataNetObjectsCollection(dbiNetObject)},
            {Constants.ConnectivityIssueWarningId, ConnectivityIssueWarningClientId}
        };

        var tabSettings = _parseResourceProperties(properties, globalProperties);
        Tabs = _adjustResourceProperties(tabSettings, contextNetObject).ToArray();

        // init internal properties
        var firstTabId = Tabs.Count() > 0 ? Tabs[0].TabId : string.Empty;
        DefaultTabId = properties.GetValueOrDefault(DatabaseResponseTimeBaseResource.PropName.DefaultTab.ToLowerInvariant(), firstTabId).ToString();
        DefaultTabOnly = ("1" == properties.GetValueOrDefault(DatabaseResponseTimeBaseResource.PropName.DefaultTabOnly.ToLowerInvariant(), "0").ToString());
        CustomEditUrl = properties.GetValueOrDefault(DatabaseResponseTimeBaseResource.PropName.EditUrl.ToLowerInvariant(), string.Empty).ToString();

        HideEditButton = ("1" ==
                          properties.GetValueOrDefault(
                              DatabaseResponseTimeBaseResource.PropName.HideEditButton.ToLowerInvariant(), "0")
                              .ToString());
    }

    private string[] GetDataNetObjectsCollection(string dbiNetObject)
    {
        if (IsSamClient && _nodeProvider != null)
        {
            queriesFromNode.Node = _nodeProvider.Node;
            queriesFromNode.Visible = true;
            return new[] { dbiNetObject, _nodeProvider.Node.NetObjectID };
        }
        return new[] { dbiNetObject };
    }

    private void AddTabHeader(string tabId, string name)
    {
        var ctlTabHeader = new LinkButton
        {
            ID = tabId,
            Text = name,
            CssClass = CssClasses.Item
        };
        ctlTabHeader.Attributes["automation"] = tabId;
        phTabHeaders.Controls.Add(ctlTabHeader);
    }

    private void RenderTabHeaders()
    {
        phTabHeaders.Controls.Clear();

        // do not render tabs at all when only default should be visible
        if (DefaultTabOnly || IsSamClient)
            return;

        foreach (var tab in Tabs)
        {
            if (tab.Disabled != "1")
                AddTabHeader(tab.TabId, tab.TabName);
        }
    }

    private void ParseActiveTab()
    {
        if (!IsPostBack)
        {
            return;
        }
        var target = Request.Params["__EVENTTARGET"];
        var tabIndex = phTabHeaders.Controls.Cast<Control>().IndexOf(th => th.UniqueID == target);
        if (tabIndex == -1)
        {
            return;
        }
        var tab = Tabs.GetValueOrDefault(tabIndex);
        ActiveTabId = (tab == null) ? ActiveTabId : tab.TabId;
    }

    private void RenderActiveTab()
    {
        var tabs = phTabHeaders.Controls.OfType<LinkButton>();
        foreach (var tab in tabs)
        {
            tab.CssClass = CssClasses.Item;
            if (tab.ID == ActiveTabId)
            {
                tab.CssClass += " " + CssClasses.Active;
            }
        }
    }

    #endregion // tab headers rendering

    #region tab content rendering

    void SetProperties(IPropertyProvider resource, Dictionary<string, object> props)
    {
        foreach (var prop in props)
        {
            resource.Properties[prop.Key.ToLowerInvariant()] = prop.Value;
        }
    }

    void AddTabContent(DatabaseResponseTimeBaseResource.TabSettings tab)
    {
        var ctlInner = LoadResource(tab.ResourcePath);
        var provider = ctlInner as IPropertyProvider;
        if (provider != null)
        {
            SetProperties(provider, tab.Properties);
        }
        phTabContents.Controls.Add(ctlInner);
    }

    void RenderTabContent()
    {
        if (ActiveTab != null)
        {
            AddTabContent(ActiveTab);
        }
    }

    #endregion // tab content rendering

    public string FormatEditUrl(string path)
    {
        var ub = new UriBuilder(Request.Url);
        ub.Path = path;
        var query = HttpUtility.ParseQueryString(ub.Query);
        query[QueryParamName.ResourceId] = ResourceId.ToInvariantString();
        // add standard orion parameters, when available
        var netobj = Request.QueryString[QueryParamName.NetObject];
        if (!string.IsNullOrEmpty(netobj))
        {
            query[QueryParamName.NetObject] = netobj;
        }
        var oview = Page as OrionView;
        if (oview != null)
        {
            query[QueryParamName.ViewId] = oview.ViewInfo.ViewID.ToString();
        }
        query[QueryParamName.ReturnTo] = ReferrerRedirectorBase.GetReturnUrl();
        ub.Query = query.ToString();
        return ub.ToString();
    }
}