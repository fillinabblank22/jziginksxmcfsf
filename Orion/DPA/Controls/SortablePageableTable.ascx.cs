﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPA_Controls_SortablePageableTable : System.Web.UI.UserControl
{
    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public int UniqueClientID { get; set; }

    public override string ClientID
    {
        get { return "Grid-" + UniqueClientID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}