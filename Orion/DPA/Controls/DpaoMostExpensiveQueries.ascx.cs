﻿using System;
using System.Collections.Generic;
using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.Orion.Web;

public partial class Orion_DPA_Controls_DpaoMostExpensiveQueries : System.Web.UI.UserControl, IPropertyProvider
{
    private const string IntegrationCallbackFnKey = "IntegrationCallbackFn";

    public IDictionary<string, object> Properties
    {
        get
        {
            if (properties == null)
            {
                properties = new Dictionary<string, object>()
                {
                    {IntegrationCallbackFnKey, "SW_DPA_MostExpensiveQueries_" + Guid.NewGuid().ToString().Replace("-", "_")}
                };
            }
            return properties;
        }
    }

    protected string IntegrationCallbackFn
    {
        get { return this.Properties[IntegrationCallbackFnKey].ToString(); }
    }

    public string NetObjectId { get; set; }
    public int GlobalDatabaseInstanceId { get; set; }

    private IDPASettings settings;
    private IDPASettings Settings
    {
        get
        {
            return settings ?? (settings = ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }

    private IDictionary<string, object> properties;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Settings.IsIntegrationEstablished)
        {
            Visible = false;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var appId = Properties.GetValueOrDefault("ApplicationId", -1);
        if ((int)appId == -1)
        {
            Visible = false;
            return;
        }

        // SAM Server View
        var dpaInstance =
            ServiceLocator.Current
                .GetInstance<IDatabaseInstanceApplicationDAL>()
                .GetGlobalDatabaseInstanceIdByApplicationId((int)appId);
        if (dpaInstance.HasValue)
        {
            GlobalDatabaseInstanceId = dpaInstance.Value;
        }
        else
        {
            //Not a server application
            Visible = false;
        }
    }



}