﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseInstancesTable.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseResponseTime_DatabaseInstancesTable" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>

<!-- Template for underscore table -->
<script id="tpl-table-<%= this.ResourceID %>" type="text/template">
    <tbody>
        <tr class="HeaderRow" automation="Row_Header">
            {# if (rows && _.size(rows) > 1) { #}<td class="col-Selection ReportHeader" style="width: 13px;">&nbsp;</td>{# } #}
            <td class="col-0 ReportHeader" style="width: 30px;">&nbsp;</td>
            <td class="col-1 ReportHeader DbInstanceName" id="name">
                {{ headerFormat(_.size(rows)) }}
            </td>
            <% if (ShowKpis)
               { %>
            <td class="col-2 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_WaitTime %></td>
            <td class="col-3 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_Query %></td>
            <td class="col-4 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_CPU %></td>
            <td class="col-5 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_Memory %></td>
            <td class="col-6 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_Disk %></td>
            <td class="col-7 ReportHeader" id="name"><%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_Session %></td>
            <% } %>
        </tr>
        {# _.each(rows, function(row){ #}<tr>
            {# if (rows && _.size(rows) > 1) { #}
                <td class="col-Selection">
                    <img src="/Orion/DPA/images/Controls/s.gif" data-dbi="{{ row.GlobalDatabaseInstanceId }}" 
                            class="DPA_RadioControl{# if(row.GlobalDatabaseInstanceId == selection) { #} checked{# } #}"/> 
                </td>
            {# } #}
            <td class="col-0 DbInstanceIcon">
                <a href="{{ formatLink(row.DbInstanceLink) }}"><img src="{{ row.DbInstanceIcon }}" /></a>
            </td>
            <td class="col-1 DbInstanceName">
                {{ SW.DPA.Common.FormatDatabaseInstanceLink(row.DbInstanceLink, undefined, row.DbInstanceName, row.NodeLink, row.NodeIcon, row.NodeName,
                    <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>) }}
            </td>
            <% if (ShowKpis)
               { %>
{# if(row.IsInDPA) { #}
            <td class="col-2 icon">
                <span>                               
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.WaitTimePerformanceStatusToolTip, 
                        row.WaitTimePerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.WaitTimePerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
            <td class="col-3 icon">
                <span>                               
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.QueryPerformanceStatusToolTip, 
                        row.QueryPerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.QueryPerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
            <td class="col-4 icon">
                <span>                               
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.CPUPerformanceStatusToolTip, 
                        row.CPUPerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.CPUPerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
            <td class="col-5 icon">
                <span>                               
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.MemoryPerformanceStatusToolTip, 
                        row.MemoryPerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.MemoryPerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
            <td class="col-6 icon">
                <span>                               
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.DiskPerformanceStatusToolTip, 
                        row.DiskPerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.DiskPerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
            <td class="col-7 icon">
                <span>
                    {{ SW.DPA.Common.FormatKpiLink(row.DbInstanceLink, 
                        row.SessionPerformanceStatusToolTip, 
                        row.SessionPerformanceStatusIcon, 
                        "<%= SolarWinds.DPA.Common.CommonConstants.SessionPerformanceStatusIconTarget %>", 
                        row.GlobalDatabaseInstanceId,
                        true)
                    }}
                </span>
            </td>
{# } else { #}
            <td class="col-2-7 DPA_AddDbInstanceLink" colspan="6">
                <span>
                    <a class="DPA_AddDbInstanceLink" href="{{ SW.DPA.Common.assignDatabaseInstanceUrl(row.ApplicationId, row.NodeID) }}">
                        <%= Resources.DPAWebContent.DatabaseInstancesResource_AddDbInstanceLinkLabel %> &raquo;
                    </a>
                </span>
            </td>
{# } #}
            <% } %>
        </tr>{# }); #}
    </tbody>
</script>

<script type="text/javascript">
(function () {
    window.SW = window.SW || {};
    window.SW.DPA = window.SW.DPA || {};

    var selectedDatabaseInstance = '<%= SelectedDatabaseInstance %>';
    var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= ConnectivityIssueWarningClientId %>');

    // database instance request
    var dbInstancesProxy = "/api/DpaDatabaseInstances/GetDatabaseInstances",
        dbInstancesParams = {
            FilterStatuses: [],
            PageIndex: 0,
            Limit: 256, // FB395670
            OrderByClause: "",
            Query: "<%= this.QueryFilter %>",
            SelectIds: [<%= this.DatabaseInstancesFilter %>]
        };

    var dbInstancesDataReader = function (dt) {
        var rows = [];

        $.each(dt.Rows, function (ri, r) {
            var i = rows.push({}) - 1;
            $.each(dt.Columns, function (ci, c) {
                rows[i][c] = r[ci];
            });
        });

        return { 
            "rows": rows,
            "selection": selectedDatabaseInstance,
            formatLink: function(link) {
                return SW.DPA.Common.linkToDBPerfSubView(link);
            }
        };
    };

    // templates
    var getTemplate = function (elId) {
            var el = $('#' + elId), html = el ? el.html() : undefined;
            return _.template(html || '');
        },
        tableTpl = getTemplate('tpl-table-<%= this.ResourceID %>'),
        emptyTpl = getTemplate('tpl-empty-<%= this.ResourceID %>');

    var onTableIconClick = function (e) {
        e.preventDefault();
        e.stopPropagation();

        var moveto = $(this).data('moveto'),
            el = $("[id$='-" + moveto + "']"),
            target = el && el[0] && $(el[0]);

        // follow anchor if there is no target on page
        if (!target) {
            var url = $(this).attr('href') + '#' + moveto;
            window.location = $(this).attr('href');
            return;
        }
        // slowly scroll to taget if exists
        var scroolTo = target.offset().top - 70;
         $('html, body').animate({ scrollTop: scroolTo }, 'slow');
    };
    
    var onRadioButtonClick = function (e) {
        e.preventDefault();
        e.stopPropagation();
        
        // set selection
        selectedDatabaseInstance = $(this).data('dbi') || '';
        // synt radio button checked state
        $.each($('#dbi-table-<%= this.ResourceID %>').find('img.DPA_RadioControl'), function () {
            ($(this).removeClass('checked').data('dbi') == selectedDatabaseInstance) && $(this).addClass('checked');
        });
        // update pannel with chart
        var evArg = 'Resource$<%= this.ResourceID %>$DBI$' + selectedDatabaseInstance;
        __doPostBack('<%= SelectionHandlerUniqueId %>', evArg);
    };

    <%=AsyncLoadDataJsCallback%> = function (target, label) {
        var el = $('#dbi-table-<%= this.ResourceID %>');
        
        var postProcessTable = function(){
            // clone table
            var clone = el.clone();
            clone.attr('id', 'clone-table-<%= this.ResourceID %>');
            clone.appendTo(target).show();

            // bind icons
            $('#clone-table-<%= this.ResourceID %>')
                .find('.icon a')
                .click(onTableIconClick);

            // bind RadioButtons
            $('#clone-table-<%= this.ResourceID %>')
                .find('img.DPA_RadioControl')
                .click(onRadioButtonClick);
        };

        // do not load data if table is already loaded
        if (el.attr("ready")) {  return postProcessTable(); }

        SW.Core.Services.callController(dbInstancesProxy, dbInstancesParams,
            function (result) {

                if (typeof result.Metadata.EndpointNotAvailable !== 'undefined' && result.Metadata.EndpointNotAvailable) 
                    connectivityIssueWarning.show();

                var data = dbInstancesDataReader(result.DataTable);

                $.extend(data, {
                    headerFormat: function(totalRows) {
                        if (totalRows > 1) {
                            return SW.Core.String.Format('<%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Plural %>', totalRows);
                        } else {
                            return '<%= Resources.DPAWebContent.DatabaseInstancesResource_TableColumnHeader_DatabaseInstances_Singular %>';
                        }
                    }
                });

                // render table content
                var tpl = data.rows.length > 0 ? tableTpl : emptyTpl;
                if (el && tpl) el.html(tpl(data)).attr('ready', true);

                return postProcessTable();
            },
            function (errorResult) { }
        );
    }
})();
</script>

<div>
    <table id="dbi-table-<%= this.ResourceID %>" class="sw-custom-query-table" cellpadding="2" cellspacing="0" width="100%" style="display:none;"></table>
</div>