﻿using System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;

public partial class Orion_DPA_Controls_ApplicationConfigurationModal : System.Web.UI.UserControl
{
    protected override void OnLoad(EventArgs e)
    {
        // do not render this control when SAM is not installed
        if (!ServiceLocator.Current.GetInstance<IDPASettings>().IsApmInstalled)
        {
            this.Visible = false;
        }
        else if (!IsPostBack)
        {
            ClientApplicationSelector.GridColumnDefinitions =
                ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaClientApplicationSelectorController.UniqueId);
        }

        base.OnLoad(e);
    }
}

