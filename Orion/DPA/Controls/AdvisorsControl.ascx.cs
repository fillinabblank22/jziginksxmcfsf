﻿using System.Web.UI;

public partial class Orion_DPA_Controls_AdvisorsControl : UserControl
{
    public int? DpaServerId { get; private set; }

    public int ResourceId { get; private set; }

    public string ConnectivityIssueWarningClientId { get; private set; }

    public string ErrorMessageClientId { get; private set; }

    public string LoadingIconClientId { get; private set; }

    public void Initialize(int resourceId, string connectivityIssueId, string errorMessageId, string loadingIconId, int? dpaServerId)
    {
        DpaServerId = dpaServerId;

        ResourceId = resourceId;
        ConnectivityIssueWarningClientId = connectivityIssueId;
        ErrorMessageClientId = errorMessageId;
        LoadingIconClientId = loadingIconId;

        SortablePageableTable.UniqueClientID = resourceId;
    }
}