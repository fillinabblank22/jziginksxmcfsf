﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Common;

public partial class Orion_DPA_Controls_QueriesFromNode : System.Web.UI.UserControl
{
    public SolarWinds.Orion.NPM.Web.Node Node { get; set; }

    public string TextHtml
    {
        get
        {
            var textFormat = HttpUtility.HtmlEncode(Resources.DPAWebContent.QueriesFromText);
            var nodeIconWithName = String.Format("<img src=\"{0}\" /> {1}", 
                StatusIconsHelper.GetSmallStatusIcon(SwisEntities.Nodes, Node.Status.ParentStatus, Node.NodeID), 
                HttpUtility.HtmlEncode(Node.Name));

            return String.Format(textFormat, nodeIconWithName);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}