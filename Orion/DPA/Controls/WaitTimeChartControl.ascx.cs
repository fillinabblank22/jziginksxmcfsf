﻿using System;
using System.Linq;
using System.Web.UI;
using SolarWinds.DPA.Common.Models;
using SolarWinds.DPA.Web.Providers.WaitTime;

public partial class Orion_DPA_Controls_WaitTimeChartControl : UserControl
{
    private IWaitTimeProvider _waitTimeProvider;
    private WaitTimeCategory _waitTimeCategory;

    public string HelpLinkFragment { get; private set; }

    public string ChartName { get; private set; }

    protected string NoIntegrationSampleClass { get; private set; }

    protected string NoIntegrationSampleText { get; private set; }

    public void Initialize(string property, bool isIntegrationEstablished)
    {
        int resourceCategoryTypeId;

        if (!int.TryParse(property, out resourceCategoryTypeId))
        {
            throw new FormatException(String.Format("Value '{0}' of resource property 'ResourceDataCategory' has invalid format, expecting integer", property));
        }

        _waitTimeCategory = (WaitTimeCategory)resourceCategoryTypeId;
        _waitTimeProvider = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IWaitTimeProvider>(_waitTimeCategory.ToString());

        if (_waitTimeProvider == null) throw new NullReferenceException("_waitTimeProvider");

        HelpLinkFragment = _waitTimeProvider.HelpLinkFragment;
        ChartName = _waitTimeProvider.ChartName;
        NoIntegrationSampleClass = _waitTimeProvider.NoIntegrationSampleClass;
        NoIntegrationSampleText = string.Format("<li>{0}</li>", _waitTimeProvider.NoIntegrationSampleTexts.Aggregate((i, j) => i + "</li><li>" + j));

        if (isIntegrationEstablished)
        {
            IntegrationNotEstablishedBlock.Visible = false;
        }
    }
}