﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryCentralPlugin.ascx.cs" Inherits="Orion_DPA_Controls_DiscoveryCentralPlugin" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Register Src="~/Orion/DPA/Controls/ConnectivityIssueWarning.ascx" TagPrefix="dpa" TagName="ConnectivityIssueWarning" %>

<orion:Include runat="server" Module="DPA" File="ConnectivityIssueWarning.css" />

<style>.DPA_InfoBoxWarning { margin: 9px 0 0; } #adminContent .DPA_InfoBoxWarning .DPA_Body p { padding: 0; }</style>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/DPA/images/DiscoveryCentral.png"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2><%= Resources.DPAWebContent.DiscoveryCentralPlugin_Title %></h2>
    <div>
        <%= Resources.DPAWebContent.DiscoveryCentralPlugin_Description %>
    </div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionDPADiscoveryCentral"
                    HelpDescription="<%$ Resources: DPAWebContent, DiscoveryCentralPlugin_LearnMoreLinkText %>" CssClass="helpLink"/>

    <dpa:ConnectivityIssueWarning runat="server" ID="ConnectivityIssueWarning"/>

    <div class="managedObjInfoWrapper">
        <span id="monitoredObjectsInfo" runat="server">
            <asp:Image ID="okImage" runat="server" ImageUrl="~/Orion/images/ok_16x16.gif" CssClass="stateImage"></asp:Image>
            <asp:Image ID="bulbImage" runat="server" ImageUrl="~/Orion/images/lightbulb_tip_16x16.gif" Visible="false" CssClass="stateImage"></asp:Image>
            <%= DpaDatabaseInstancesText %>
        </span>
    </div>

    <% if (Settings.IsIntegrationEstablished)
       { %>
        <div class="sw-btn-bar">
            <orion:LocalizableButton ID="StartDpaIntegrationWizard" runat="server" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, DiscoveryCentralPlugin_StartIntegrationButton %>"
                                     OnClick="StartDpaIntegrationWizard_Click" DisplayType="Secondary"/>
            <orion:LocalizableButton ID="ManageRelationships" runat="server" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, DiscoveryCentralPlugin_ManageRelationships %>"
                                     OnClick="ManageRelationships_Click" DisplayType="Secondary"/>
        </div>
    <% }
       else
       { %>
        <div class="sw-suggestion" style="width: 90%;">
            <span class="sw-suggestion-icon"></span>
            <p style="width: 100%; font-size: 90%">
                <%= Resources.DPAWebContent.DiscoveryCentralPlugin_NoDPAIntegratedText %>
                <span style="font-size: 111%">
                    <span class="LinkArrow">&#0187;</span>&nbsp;<a target="_blank" href="<%: HelpHelper.GetHelpUrl("DPA", "OrionDPALearnMoreAboutOrionIntegration") %>" class="helpLink">
                        <%= Resources.DPAWebContent.DiscoveryCentralPlugin_LearnMoreLinkText %>
                    </a>
                </span>
            </p>
            <orion:LocalizableButton ID="StartDpaIntegrationWizard1" runat="server" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, DiscoveryCentralPlugin_StartIntegrationButton %>"
                                     OnClick="StartDpaIntegrationWizard_Click" DisplayType="Secondary"/>
        </div>
    <% } %>
</div>
