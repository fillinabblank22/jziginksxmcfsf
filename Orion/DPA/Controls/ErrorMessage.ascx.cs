﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPA_Controls_ErrorMessage : System.Web.UI.UserControl
{
    protected bool IsVisible { get; set; }

    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public string UniqueClientID { get; set; }

    public override string ClientID
    {
        get { return "DPA_ErrorMessage-" + UniqueClientID; }
    }

    public void Show()
    {
        IsVisible = true;
    }

    public void Hide()
    {
        IsVisible = false;
    }
}