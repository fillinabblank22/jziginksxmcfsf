﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientApplicationCandidatesModal.ascx.cs" Inherits="Orion_DPA_Controls_ClientApplicationCandidatesModal" %>

<%@ Register Src="~/Orion/DPA/Controls/ClientApplicationCandidatesSelector.ascx" TagPrefix="dpa" TagName="ClientCandidatesSelector" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="DPA_ClientApplicationCandidatesModal" style="display:none">
    <%: Resources.DPAWebContent.RelationshipManagement_ClientApplications_DatabaseClientCandidatesHint %>

    <div class="DPA_ClientApplicationCandidatesGrid">
        <dpa:ClientCandidatesSelector ID="ClientCandidatesSelector" runat="server" />
    </div>

    <div class="sw-btn-bar-wizard DPA_Buttons" id="report-content-dialog-btn-ph">
    </div>
</div>