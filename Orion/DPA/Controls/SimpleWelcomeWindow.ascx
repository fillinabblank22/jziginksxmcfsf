﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SimpleWelcomeWindow.ascx.cs" Inherits="Orion_DPA_Controls_SimpleWelcomeWindow" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>

<% if (Profile.AllowAdmin)
    { %>
<orion:Include runat="server" Module="DPA" File="ViewsBundle.css" />

<div id="DPA_WelcomeWindow" style="display: none">
    <ul class="DPA_TopList">
        <li><%= Resources.DPAWebContent.WelcomeWindow_TopList_Line1 %></li>
        <li><%= Resources.DPAWebContent.WelcomeWindow_TopList_Line2 %></li>
        <li><%= Resources.DPAWebContent.WelcomeWindow_TopList_Line3 %></li>
    </ul>
    <div class="DPA_DpaSteps">
        <img class="DPA_SampleImage" src="/Orion/DPA/images/Samples/welcome-window-chart.png" />
        <span class="DPA_StepsTitle"><%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Text %></span>
        <div class="DPA_Step">
            <p class="DPA_StepTitle"><%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Step1_Title %></p>
            <span>
                <%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Step1_Text %>
                <a class="DPA_Link" target="_blank" href="<%: PageUrlHelper.DpaDownload %>">
                    »&nbsp;<%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Step1_LinkLabel %>
                </a>
            </span>
        </div>
        <div class="DPA_Step">
            <p class="DPA_StepTitle"><%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Step2_Title %></p>
            <span><%= Resources.DPAWebContent.WelcomeWindow_DpaSteps_Step2_Text %></span>
        </div>
    </div>
    <div class="DPA_BottomBar">
        <div id="ErrorMessage" class="sw-suggestion sw-suggestion-fail DPA_ErrorMessage" style="display: none">
            <span class="sw-suggestion-icon"></span>
            <span><%= Resources.DPAWebContent.WelcomeWindow_SavingErrorMessage %></span>
        </div>
        <span class="DPA_Checkbox">
            <label>
                <input type="checkbox" id="DontShowAgainCheckBox" /><%= Resources.DPAWebContent.WelcomeWindow_DontShowAgainCheckbox_Label %></label>
        </span>
        <span class="DPA_Buttons">
            <orion:LocalizableButtonLink
                ID="ConnectDpaButton"
                runat="server"
                LocalizedText="CustomText"
                Text="<%$ Resources: DPAWebContent, WelcomeWindow_ConnectDpaButton_Label %>"
                DisplayType="Primary" />
            <orion:LocalizableButton
                ID="CloseButton"
                runat="server"
                LocalizedText="CustomText"
                Text="<%$ Resources: DPAWebContent, WelcomeWindow_CloseButton_Label %>"
                DisplayType="Secondary" />
        </span>
    </div>
</div>

<script>
    $(document).ready(function () {
        <% if (ShowWelcomeWindow)
    { %>
        var dialog = $("#DPA_WelcomeWindow");
        dialog.dialog({
            modal: true,
            minWidth: 800,
            minHeight: 100,
            zIndex: 100000,
            resizable: false,
            title: "<%: Resources.DPAWebContent.WelcomeWindow_Title %>",
            open: function () {
                $("#<%: CloseButton.ClientID %>").click(function () {
                    dialog.dialog('close');
                    return false;
                });
                var header = dialog.prev().addClass("DPA_DialogHeader");
                header.find(".ui-icon-closethick").addClass("DPA_CrossBar");
                header.find(".ui-dialog-titlebar-close").addClass("DPA_CrossBarWrapper");

                dialog.find("#DontShowAgainCheckBox").click(function () {
                    var errorMessage = dialog.find("#ErrorMessage");
                    errorMessage.hide();
                    var checkbox = this;
                    checkbox.disabled = true;

                    // save settings
                    SW.Core.Services.callController(
                        "/api/DpaWelcomeWindow/SimpleWindowSaveSettings",
                        { IsHidden: checkbox.checked },
                        function (response) {
                            if (response.SavingError) {
                                errorMessage.show();
                            }
                            checkbox.disabled = false;
                        },
                        function () {
                            errorMessage.show();
                            checkbox.disabled = false;
                        });
                });
            }
        });
        <% } %>
    });
</script>
<% } %>