﻿using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.Orion.Web.Charting.v2;
using System.Web.Script.Serialization;
using System.Globalization;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Resources_DatabaseResponseTime_CustomChart : UserControl
{
    #region Properties

    public bool LoadOnDemand { get; set; }

    public string Height { get; set; }

    public string Width { get; set; }

    public string DataUrl { get; set; }

    private Dictionary<string, object> customChartSettings;
    public Dictionary<string, object> CustomChartSettings
    {
        get { return this.customChartSettings ?? (this.customChartSettings = new Dictionary<string, object>()); }
        set { this.customChartSettings = value; }
    }

    public string CustomChartOptions { get; set; }

    public IChartLegendControl LegendUserControl { get; set; }

    protected string LegendInitializer
    {
        get
        {
            return (LegendUserControl != null) ?
                LegendUserControl.LegendInitializer :
                string.Format(CultureInfo.InvariantCulture, "core_standardLegendInitializer__{0}", this.ClientID);
        }
    }

    #endregion

    #region Protected methods

    protected override void OnInit(EventArgs e)
    {
        IncludeFilesHelper.IncludeScriptsForCharts();

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(this.Height))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("height", this.Height.Trim());
            }

            if (!string.IsNullOrEmpty(this.Width))
            {
                this.pnlCustomChart.Attributes.CssStyle.Add("width", this.Width.Trim());
            }
        }

        PoweredByIcon.Visible = !Convert.ToBoolean(CustomChartSettings.GetValueOrDefault(Constants.HidePoweredByIcon, false));
    }

    protected override void OnPreRender(EventArgs e)
    {
        Control legendControl = (this.LegendUserControl != null) ?
            (Control)this.LegendUserControl :
            new Table() { ID = "tblLegend", CssClass = "chartLegend" };
        legendUserControlPlaceholder.Controls.Add(legendControl);

        //By registering the script in this manner we ensure it runs when it is loaded via update panel. Standard scripts are not run.
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID + "1", this.GetChartInitializerScript(), true);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID + "2", this.GetChartLegendScript(legendControl), true);

        base.OnPreRender(e);
    }

    #endregion

    #region Private methods

    private string GetChartOptions()
    {
        return string.IsNullOrWhiteSpace(this.CustomChartOptions) ? "{}" : this.CustomChartOptions;
    }

    private string GetChartSettings()
    {
        var details = this.CustomChartSettings;

        details["renderTo"] = this.divChart.ClientID;
        details["legendInitializer"] = this.LegendInitializer;

        details.SetIfNoKey("showTitle", false);
        details.SetIfNoKey("title", string.Empty);
        details.SetIfNoKey("subtitle", string.Empty);
        details.SetIfNoKey("loadingMode", LoadingModeEnum.StandardLoading);
        details.SetIfNoKey("sampleSizeInMinutes", 10);
        details.SetIfNoKey("netObjectIds", new string[0]);
        details.SetIfNoKey("ResourceProperties", new Dictionary<string, object>());

        var text = new JavaScriptSerializer().Serialize(details);
        return string.IsNullOrWhiteSpace(text) ? "{}" : text;
    }

    private string GetChartInitializerScript()
    {
        const string Template = @"
(function () {{
    'use strict';
    function getChartSettings(parameters) {{ return {1}; }}
    function getChartOptions(parameters) {{ return {2}; }}

    function load(parameters) {{
        function refresh() {{
            SW.DPA.Charts.DpaStandardChart.initializeStandardChart(getChartSettings(parameters), getChartOptions(parameters));
        }}

        if ((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{
            SW.Core.View.AddOnRefresh(refresh, '{3}');
        }}

        refresh();
    }}
    {4}
}}());
";

        var js = string.Format(
            Template,
            this.ID,
            this.GetChartSettings(),
            this.GetChartOptions(),
            this.divChart.ClientID,
            this.LoadOnDemand ? string.Empty : "load();"
            );

        return js;
    }

    private string GetChartLegendScript(Control legendTable)
    {
        if (this.LegendUserControl != null)
            return string.Empty;

        const string scriptTemplate = @"
(function () {{
    SW.Core.Charts.Legend['{0}'] = function (chart, dataUrlParameters) {{
        $('#{1}').empty();
        SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '{1}', true);
    }};
}}());
";
        return string.Format(CultureInfo.InstalledUICulture, scriptTemplate, this.LegendInitializer, legendTable.ClientID);
    }

    #endregion
}