﻿using System;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;

public partial class Orion_DPA_Controls_LunSelector : System.Web.UI.UserControl
{
    public IGridColumnDefinitions GridColumnDefinitions { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            GridColumnDefinitions =
                ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaLunSelectorController.UniqueId);
        }

        base.OnLoad(e);
    }
}