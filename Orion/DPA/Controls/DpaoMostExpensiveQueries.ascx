﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DpaoMostExpensiveQueries.ascx.cs" Inherits="Orion_DPA_Controls_DpaoMostExpensiveQueries" %>
    
<orion:Include runat="server" Module="DPA" File="ViewsBundle.css" />

<script type="text/javascript">
    
    var <%= this.IntegrationCallbackFn %>_createDpaLinksRow = function(id, expander, sql_handle, isExpanded) {
        var linksToDpaRow = $('<tr id="row_' + id + '" class="integrationRow"/>');
        var dpaLinksCell = $('<td colspan="11"/>').appendTo(linksToDpaRow);
        var linkDiv = $('<div class="links"/>').appendTo(dpaLinksCell);

        linkDiv.data('sql_handle', SW.DPA.IntegrationPlugin.MostExpensiveQueries.toHexString(sql_handle));
        
        // bind click on expander
        $(expander).on("click", function () {
            var handle = $(linkDiv).data('sql_handle');
            var content = $(".tQuery_" + id);
            
            if (content.hasClass("topQueries_Expanded")) {
                SW.DPA.IntegrationPlugin.MostExpensiveQueries.showLinksToDpa(id, handle);
            } 
            else {
                SW.DPA.IntegrationPlugin.MostExpensiveQueries.hideLink(id);
            }
        });

        // render content
        if (isExpanded) {
            var msg = SW.DPA.IntegrationPlugin.MostExpensiveQueries.initDpaLinks(id);
            if (msg != "") {
                linkDiv.addClass("showLinks");
                $(linkDiv).html(msg);
            }
        }

        return linksToDpaRow;
    }


    SW.Core.namespace("SW.DPA.IntegrationPlugin");
    SW.DPA.IntegrationPlugin.MostExpensiveQueries = SW.DPA.IntegrationPlugin.MostExpensiveQueries || {

        //database instance id - from codebehind based on application id
        globalDatabaseInstanceId: '<%= GlobalDatabaseInstanceId %>',

        //template for rendered message if hash was found
        msgTemplate: _.template([
            '<b>{{header}}</b>'
            ,'<ul class="linksToDpa">'
            ,   '<li>'
            ,       '<a class="sw-link" href="{{location_advices}}" target="_blank">'
            ,           '{{text_advices}} &#187;'
            ,       '</a>'
            ,   '</li>'
            ,   '<li>'
            ,       '<a class="sw-link" href="{{location_historical}}" target="_blank">'
            ,           '{{text_historical}} &#187;'
            ,       '</a>'
            ,   '</li>'
            ,'</ul>'
        ].join('')),


        //used for saving hashes on resource refresh (change of fliter)
        hashes: [],

        //used for saving row's Ids for which hash was not found
        unknownHashesRowsIds: [],

        hideLink: function (id) {
            $("#row_" + id).find("div.links").hide();
            SW.DPA.IntegrationPlugin.MostExpensiveQueries.hideLoadingIcon(id);
        },

        hideLoadingIcon: function (id) {
            $("#row_" + id).find("div.loading").hide();
        },

        showLinksToDpa: function (id, sql_handle) {
            var linksToDpa = $("#row_" + id).find("div.links");
            
            if (linksToDpa.text() == "") {
                SW.DPA.IntegrationPlugin.MostExpensiveQueries.getHash(id, sql_handle, function(hash) {
                    linksToDpa.addClass("showLinks");
                    var msg = SW.DPA.IntegrationPlugin.MostExpensiveQueries.initDpaLinks(id);
                    linksToDpa.html(msg);
                });
            }
            else {
                linksToDpa.show();
            }
        },

        getHash: function (id, sql_handle, callback) {
            var hash;
            if (SW.DPA.IntegrationPlugin.MostExpensiveQueries.unknownHashesRowsIds.indexOf(id) == -1) {
                SW.Core.Services.callController('/api/DpaSqlQueryHash/GetHash', { SqlHandle: sql_handle, GlobalDatabaseInstanceId: SW.DPA.IntegrationPlugin.MostExpensiveQueries.globalDatabaseInstanceId },
                    function(result) {
                        hash = result;
                        SW.DPA.IntegrationPlugin.MostExpensiveQueries.hideLoadingIcon(id);
                        if (hash == null) {
                            //hash was not found
                            SW.DPA.IntegrationPlugin.MostExpensiveQueries.unknownHashesRowsIds.push(id);
                        } else {
                            //hash was found
                            SW.DPA.IntegrationPlugin.MostExpensiveQueries.hashes.push({ "id": id, "hash": hash });
                            callback(hash);
                        }
                    },
                    function(error) {
                    }
                );

                //show loading icon
                SW.DPA.IntegrationPlugin.MostExpensiveQueries.showLoadingIcon(id);
            }
        },

        showLoadingIcon: function (id) {
            var linksToDpaCell = $("#row_" + id).find("td");

            var loadingEl = linksToDpaCell.find("div.loading");
            if (loadingEl.length > 0) {
                loadingEl.show();
                return;
            }

            loadingEl = $('<div class="loading"/>').appendTo(linksToDpaCell);

            var loadingIcon = $('<img src="/Orion/images/animated_loading_sm3_whbg.gif"/>').appendTo(loadingEl);

            var displayText = $("<span />").appendTo(loadingEl);
            displayText.css("font-weight", "bold");

            displayText.text("<%= Resources.DPAWebContent.MostExpensiveQueries_LoadingText %>");
        },


        initDpaLinks: function (id) {
            var row = _.find(SW.DPA.IntegrationPlugin.MostExpensiveQueries.hashes, function (record) { return record.id == id; });

            if (typeof row == 'undefined') {
                return "";
            }

            var linkToControllerHistoricalData = SW.Core.String.Format("/api/DpaLinksToDpaj/SqlQueryDetails?dbi={0}&hash={1}",
                 SW.DPA.IntegrationPlugin.MostExpensiveQueries.globalDatabaseInstanceId, row.hash);

            var linkToControllerAdvices = SW.Core.String.Format("/api/DpaLinksToDpaj/PerformanceDetailWithSqlHash?dbi={0}&hash={1}",
                 SW.DPA.IntegrationPlugin.MostExpensiveQueries.globalDatabaseInstanceId, row.hash);

            return SW.DPA.IntegrationPlugin.MostExpensiveQueries.msgTemplate(
            {
                header: SW.Core.String.Format('<%= Resources.DPAWebContent.MostExpensiveQueries_DpaKnowsThisQuery %>', row.hash),
                location_advices: linkToControllerAdvices,
                text_advices: '<%= Resources.DPAWebContent.MostExpensiveQueries_LinkToAdvices %>',
                        location_historical: linkToControllerHistoricalData,
                        text_historical: '<%= Resources.DPAWebContent.MostExpensiveQueries_LinkToHistoricalCharts %>'
            });
        },

        toHexString: function(arr) {
            return arr.map(function(b) { return (b < 16 ? "0":"") + (b & 0xFF).toString(16); }).join('');
        }
    }
</script>

<style type="text/css">
    table.NeedsZebraStripes.mostExpensiveQueries > thead ~ tbody > tr:nth-child(4n+1), table.NeedsZebraStripes.mostExpensiveQueries > thead ~ tbody > tr:nth-child(4n+2) {
        background-color: #ffffff;
    }

    table.NeedsZebraStripes.mostExpensiveQueries > thead ~ tbody > tr:nth-child(4n+3), table.NeedsZebraStripes.mostExpensiveQueries > thead ~ tbody > tr:nth-child(4n) {
        background-color: #f6f6f6;
    }
</style>