﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsUsingMyDatabasesControl.ascx.cs" Inherits="Orion_DPA_Controls_ApplicationsUsingMyDatabasesControl" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>

<orion:Include runat="server" Module="DPA" File="Resources.js" />
<orion:Include runat="server" File="DPA/Js/DPA.UI.SortablePageableTable.js" />

<div class="DPA_AppsUsingDb" runat="server" id="content"></div>
<div id="Resource-<%= ResourceId %>-NoContent" style="display: none">
    <p><%: Resources.DPAWebContent.ApplicationsUsingMyDatabasesResource_NoApplicationFound %></p>

    <div class="DPA_GettingStartedBox">
        <div class="DPA_First">
            <img src="/Orion/images/getting_started_36x36.gif" alt="Getting started" />
            <h3><%: Resources.DPAWebContent.ApplicationsUsingMyDatabaseResource_GettingStartedTitle %></h3>
        </div>

        <div><%: Resources.DPAWebContent.ApplicationsUsingMyDatabaseResource_GettingStartedText %></div>

        <% if (Profile.AllowAdmin)
            { %>
        <div class="DPA_Buttons">
            <orion:LocalizableButton runat="server" ID="startDPAIntegrationButton" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPAWebContent, ApplicationsUsingThisDatabaseResource_ConfigureClientApplicationsButtonLabel %>" />
        </div>
        <% } %>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= LoadingIconClientId %>');
        var errorMessage = new SW.DPA.UI.ErrorMessage('<%= ErrorMessageClientId %>');
        var refresh = function () {
            var applicationsUsingDatabases = new SW.DPA.UI.ApplicationsUsingDatabases('<%= DpaServerId %>', '<%= content.ClientID %>', 'Resource-<%= ResourceId %>-NoContent', {
                waitTimePerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.WaitTimePerformanceStatusIconTarget %>",
                queryPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.QueryPerformanceStatusIconTarget %>",
                cpuPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.CPUPerformanceStatusIconTarget %>",
                memoryPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.MemoryPerformanceStatusIconTarget %>",
                diskPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.DiskPerformanceStatusIconTarget %>",
                sessionPerformanceStatusIconTarget: "<%= SolarWinds.DPA.Common.CommonConstants.SessionPerformanceStatusIconTarget %>"
            });
            applicationsUsingDatabases.loadApplications(loadingIcon, errorMessage, <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>);
        };

        SW.Core.View.AddOnRefresh(refresh, '<%= content.ClientID %>');
        refresh();
    });
</script>
