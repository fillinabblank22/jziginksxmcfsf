﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErrorMessage.ascx.cs" Inherits="Orion_DPA_Controls_ErrorMessage" %>
<orion:Include runat="server" Module="DPA" File="Resources.js" />
<div id="<%= ClientID %>" class="sw-suggestion sw-suggestion-fail DPA_ResourceNotification" <% if (!IsVisible)                                                                                                        { %>style="display:none"<% } %>>
    <span class="sw-suggestion-icon"></span>
    <span class="DPA_Message"></span>
</div>