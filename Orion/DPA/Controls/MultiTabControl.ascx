﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiTabControl.ascx.cs" Inherits="Orion_DPA_Controls_MultiTabControl" %>

<%@ Register TagPrefix="dpa" TagName="DatabaseInstancesTable" Src="~/Orion/DPA/Controls/DatabaseInstancesTable.ascx" %>
<%@ Register TagPrefix="dpa" TagName="QueriesFromNode" Src="~/Orion/DPA/Controls/QueriesFromNode.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ShowMoreInfoDynamic" Src="~/Orion/DPA/Controls/ShowMoreInfoDynamic.ascx" %>

<div class="staticContent">
    <dpa:DatabaseInstancesTable ID="databaseInstancesTable" runat="server"></dpa:DatabaseInstancesTable>
    <dpa:QueriesFromNode runat="server" Visible="False" ID="queriesFromNode" />
</div>
<asp:UpdatePanel runat="server" ID="update" ChildrenAsTriggers="True">
    <contenttemplate>
        <div id="tabHeaders" class="DPA_tabHeaders" runat="server">
            <asp:PlaceHolder runat="server" ID="phTabHeaders"></asp:PlaceHolder>
        </div>
        <div id="staticContent" class="DPA_DatabaseInstancesWrapper" runat="server"></div>
        <dpa:ShowMoreInfoDynamic runat="server" />
        <div class="tabContents DPA_WaitTimeResource DPA_MultiTabResource">
            <asp:PlaceHolder runat="server" ID="phTabContents"></asp:PlaceHolder>
        </div>
    </contenttemplate>
</asp:UpdatePanel>
<div class="DPA_WaitTimeResource" id="WrongTabNoData" runat="server" style="overflow-y: hidden;">
    <div class="chartDataNotAvailableArea">
        <center>
            <div id="chartDataNotAvailable">
                <table style="height: 100%">
                    <tbody>
                        <tr>
                            <td style="vertical-align: middle">
                                <div class="DPA_ChartNoDataAvailable">
                                    <p><%: Resources.DPAWebContent.NoDataAvailableMessage %></p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </center>
    </div>
</div>
