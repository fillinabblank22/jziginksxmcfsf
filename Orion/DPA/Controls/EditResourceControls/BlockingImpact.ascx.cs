﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.DPA.Common.Extensions.System;
using SolarWinds.DPA.Web.Charting;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.Orion.Web.UI;

public partial class Orion_DPA_Controls_EditResourceControls_BlockingImpact : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var properties = new Dictionary<string, object>();

        properties.AppendItems(Resource.Properties.OfType<DictionaryEntry>(),
                item => item.Key.ToString().ToLowerInvariant(),
                item => item.Value.ToString());

        var defaultTabId =
            properties.GetValueOrDefault(DatabaseResponseTimeBaseResource.PropName.DefaultTab.ToLowerInvariant(),
                Constants.WaitersTabId).ToString();
        var defaultTabOnly =
            properties.GetValueOrDefault(
                DatabaseResponseTimeBaseResource.PropName.DefaultTabOnly.ToLowerInvariant(), "0").ToString() == "1";
        
        showOnlyDefaultTab.Checked = defaultTabOnly;

        defaultTab.Items.FindByValue(defaultTabId).Selected = true;
    }

    public override Dictionary<string, object> Properties => new Dictionary<string, object>
    {
        { "DefaultTab", defaultTab.SelectedItem.Value },
        { "DefaultTabOnly", Convert.ToInt32(showOnlyDefaultTab.Checked) }
    };
}
