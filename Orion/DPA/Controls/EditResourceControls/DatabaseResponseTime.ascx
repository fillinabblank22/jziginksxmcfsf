﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseResponseTime.ascx.cs" Inherits="Orion_DPA_Controls_EditResourceControls_DatabaseResponseTime" %>
<orion:Include runat="server" Module="DPA" File="DPA.css" />

<div runat="server" ID="chartSpecificSettings" class="DPA_chartSpecificSettings">
<table border="0">
    <tr>
        <td>
            <%: Resources.DPAWebContent.ResponseTimeEditResource_SelectDefault_Label%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButtonList runat="server" ID="defaultTab" />
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-suggestion" style="white-space: nowrap; width: 90%;">
                <span class="sw-suggestion-icon"></span>
                <b><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Label%></b>
                <ul>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Databases%></li>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Nodes%></li>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Waits%></li>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Wait_MySql%></li>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_WaitInstruments%></li>
                    <li><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Operations%></li>
                </ul>
            </div>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox id="showOnlyDefaultTab" runat="server" Text="<%$ Resources:DPAWebContent, ResponseTimeEditResource_OnlyDefaultTab %>"/>
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-text-helpful">
                <%: Resources.DPAWebContent.ResponseTimeEditResource_OnlyDefaultTab_Info %>                
            </div>
        </td>
    </tr>
   
</table>
</div>