﻿using System.Linq;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Web.Resources;
using SolarWinds.DPA.Common.Extensions.System;
using PropName = SolarWinds.DPA.Web.Resources.DatabaseResponseTimeBaseResource.PropName;

public partial class Orion_DPA_Controls_EditResourceControls_DatabaseResponseTime : BaseResourceEditControl
{
    private static readonly Regex regexTabProperty = new Regex(@"^tab\((?'id'.+)\)(\.(?'type'properties))?\.(?'key'.+)$");

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        // parse all tab related resource settings and group them by tab id
        var properties = DatabaseResponseTimeBaseResource.RawResourceProperties(this.Resource.Properties);

        // check, whether resource is configured for SAM client view location
        if ("1" == properties.GetValueOrDefault(PropName.IsSAMClient.ToLowerInvariant(), "0").ToString())
        {
            this.Visible = false;
            return;
        }

        var tabs = DatabaseResponseTimeBaseResource.ParseResourceProperties(properties, null).ToArray();

        if (tabs.Count() <= 0)
            return;

        var defaultTabId = properties.GetValueOrDefault(PropName.DefaultTab.ToLowerInvariant(), tabs[0].TabId).ToString();
        var defaultTabOnly = properties.GetValueOrDefault(PropName.DefaultTabOnly.ToLowerInvariant(), "0").ToString();

        // load if the checkbox should be checked
        int defTabOnly;
        int.TryParse(defaultTabOnly, out defTabOnly);
        showOnlyDefaultTab.Checked = Convert.ToBoolean(defTabOnly);

        //go through each tab in resource
        foreach (var tab in tabs)
        {
            var isEnabled = string.IsNullOrEmpty(tab.Disabled) || int.Parse(tab.Disabled) == 0;
            if (isEnabled && !string.IsNullOrEmpty(tab.TabOptionName))
            {
                var isSelected = tab.TabId.Equals(defaultTabId);
                defaultTab.Items.Add(new ListItem() { Value = tab.TabId, Text = tab.TabOptionName, Selected = isSelected });
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            // set result properties only if resource is not configured for SAM client view.
            if ("1" != this.Resource.Properties.GetValueOrDefault(PropName.IsSAMClient.ToLowerInvariant(), "0").ToString())
            {
                properties.Add("DefaultTab", defaultTab.SelectedItem.Value);
                properties.Add("DefaultTabOnly", Convert.ToInt32(showOnlyDefaultTab.Checked));
            }
            return properties;
        }
    }
}