﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockingImpact.ascx.cs" Inherits="Orion_DPA_Controls_EditResourceControls_BlockingImpact" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Charting" %>
<orion:Include runat="server" Module="DPA" File="DPA.css" />

<div runat="server" ID="chartSpecificSettings" class="DPA_chartSpecificSettings">
<table border="0">
    <tr>
        <td>
            <%: Resources.DPAWebContent.ResponseTimeEditResource_SelectDefault_Label%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButtonList runat="server" ID="defaultTab">
                <asp:ListItem Value="<%$ Code: Constants.WaitersTabId %>" Text="<%$ Code: SolarWinds.DPA.Strings.Resources.BlockingImpactResource_TopWaitersTab %>" />
                <asp:ListItem Value="<%$ Code: Constants.RootBlockersTabId %>" Text="<%$ Code: SolarWinds.DPA.Strings.Resources.BlockingImpactResource_TopRootBlockersTab %>" />
                <asp:ListItem Value="<%$ Code: Constants.DeadlocksTabId %>" Text="<%$ Code: SolarWinds.DPA.Strings.Resources.BlockingImpactResource_DeadlocksTab %>" />
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-suggestion" style="white-space: nowrap; width: 90%;">
                <span class="sw-suggestion-icon"></span>
                <b><%: Resources.DPAWebContent.ResponseTimeEditResource_TabsAvailability_Label%></b>
                <ul>
                    <li><%: Resources.DPAWebContent.BlockingImpactEditResource_TabsAvailability_Deadlocks %></li>
                </ul>
            </div>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:CheckBox id="showOnlyDefaultTab" runat="server" Text="<%$ Resources:DPAWebContent, ResponseTimeEditResource_OnlyDefaultTab %>"/>
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-text-helpful">
                <%: Resources.DPAWebContent.ResponseTimeEditResource_OnlyDefaultTab_Info %>                
            </div>
        </td>
    </tr>
   
</table>
</div>