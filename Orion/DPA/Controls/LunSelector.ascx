﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LunSelector.ascx.cs" Inherits="Orion_DPA_Controls_LunSelector" %>

<orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" Section="Top" />

<orion:Include runat="server" File="../js/extjs/3.4/ux/gridfilters/css/ux-gridfilters.css" />
<orion:Include runat="server" File="extjs/3.4/ux-gridfilters.js" />

<orion:Include runat="server" Module="DPA" File="ExtJsGroupByGrid.js" />
<orion:Include runat="server" File="OrionCore.js" />

<orion:Include runat="server" Module="DPA" File="Selectors.js" />

<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<style>
    #aspnetForm > h1 {
        font-size: large;
        padding-bottom: 10px;
        padding-left: 10px;
        padding-top: 15px;
    }

    div#lunSelector {
        height: 400px;
    }
</style>

<div id="lunSelector">
    <div id="lunSelectorGridPlaceholder" class="sw-relationshipsGrid"></div>
    <div>
        <input type="hidden" id="settingsPrefix" value='<%= GridColumnDefinitions.IdentificationPrefix %>' />
        <input type="hidden" id="controllerName" value='<%= GridColumnDefinitions.ControllerName %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.SortOrderKey%>" value='<%= GridColumnDefinitions.GetSortOrder() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.PageSizeKey%>" value='<%= GridColumnDefinitions.GetPageSize() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.GroupingValueKey%>" value='<%= GridColumnDefinitions.GetGroupingValue() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.SelectedColumnsKey%>" value='<%= GridColumnDefinitions.GetSelectedColumns() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.ColumnsKey%>" value='<%= GridColumnDefinitions.GetColumns() %>' />
        <input type="hidden" id="<%=GridColumnDefinitions.ColumnsModelConfigKey%>" value='<%= GridColumnDefinitions.GetColumnsModelConfig() %>' />
    </div>
</div>