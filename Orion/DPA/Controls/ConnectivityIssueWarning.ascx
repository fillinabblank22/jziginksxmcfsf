﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConnectivityIssueWarning.ascx.cs" Inherits="Orion_DPA_Controls_ConnectivityIssueWarning" %>
<orion:Include runat="server" Module="DPA" File="Resources.js" />

<div class="DPA_InfoBoxWarning sw-suggestion sw-suggestion-warn" id="<%= ClientID %>" <% if (!this.IsVisible)
    { %>style="display:none"
    <% } %>>
    <span class="sw-suggestion-icon"></span>
    <span class="DPA_DismissButton" onclick="document.getElementById('<%= ClientID %>').style.display = 'none';"></span>
    <div class="DPA_Body">
        <p>
            <b><%: SolarWinds.DPA.Strings.Resources.ConnectivityIssue_Message %>:</b>
        </p>
        <div id="PartialErrors" class="DPA_PartialErrors" runat="server">
            <% if (PartialResultsErrors != null && PartialResultsErrors.Any())
                {
                    foreach (var error in PartialResultsErrors)
                    { %>
                        <div>
                            <b><%: error.DpaServerName %></b> (<%: error.ErrorMessage %>) 
                            <a href="/api/DpaLinksToDpaj/MainDashboard/<%: error.DpaServerId %>" target="_blank">&#187; <%: SolarWinds.DPA.Strings.Resources.ConnectivityIssue_GoToDpaLabel %></a>
                        </div>
                 <% }
                } %>
        </div>
        <a href="<%: HelpLink %>" target="_blank">&#187; <%: Resources.DPAWebContent.LoadingIcon_Notification_TextOfLink %></a>
    </div>
</div>
