﻿using System.Web.UI;

public partial class Orion_DPA_Resources_DatabaseInstancesBase : UserControl
{
    public int? DpaServerId { get; private set; }

    public int ResourceId { get; private set; }

    public string ConnectivityIssueWarningClientId { get; private set; }
    
    public string ErrorMessageClientId { get; private set; }

    public string LoadingIconClientId { get; private set; }
    public string Title { get; private set; }

    public void Initialize(int resourceId, string connectivityIssueId, string errorMessageId, string loadingIconId, int? dpaServerId, string title)
    {
        DpaServerId = dpaServerId;
        Title = title;
        ResourceId = resourceId;
        ConnectivityIssueWarningClientId = connectivityIssueId;
        ErrorMessageClientId = errorMessageId;
        LoadingIconClientId = loadingIconId;
    }
}