﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelationshipConfigurationModal.ascx.cs" Inherits="Orion_DPA_Controls_RelationshipConfigurationModal" %>

<%@ Register Src="~/Orion/DPA/Controls/NodeSelector.ascx" TagPrefix="dpa" TagName="NodeSelector" %>
<%@ Register Src="~/Orion/DPA/Controls/ApplicationSelector.ascx" TagPrefix="dpa" TagName="ApplicationSelector" %>

<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css"/>

<div id="DPA_RelationshipConfigurationModal" style="display:none">
    
    <div id="DPA_SelectedDatabaseInstance">
        <span class="DPA_SelectedObjectLabel"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_ConfigDialogSelectInstanceLabel %></span><span class="DPA_SelectedObjectName"></span>
    </div>
    
    <p><%: Resources.DPAWebContent.IntegrationWizard_Relationships_ConfigDialogHint %></p>
    
    <div id="DPA_Relationships_SelectedNode">
        <span class="DPA_SelectedObjectLabel"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_ConfigDialogSelectedNodeLabel %></span><span><span class="DPA_SelectedObjectName"></span></span>
    </div>

    <div class="DPA_CollapsingBlock DPA_Collapsed DPA_Clickable DPA_CollapsingBlockNext DPA_SimpleView" id="DPA_SelectNodeInOrionStep">
        <div class="DPA_CollapsingBlockHeader"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_SelectNodeTitle %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            <div style="display: none;" class="sw-suggestion DPA_IsAmbiguous">
                <span class="sw-suggestion-icon"></span>
                <span><%: Resources.DPAWebContent.IntegrationWizard_Relationships_SelectNodeFilterMessage %>
                    <a href="#" class="clear-node-filter"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_SelectNodeFilterLink %></a>
                </span>
            </div>

            <dpa:NodeSelector ID="NodeSelector" runat="server" />

        </div>
    </div>
    
    <div class="DPA_CollapsingBlock DPA_Collapsed DPA_Clickable DPA_CollapsingBlockLast" id="DPA_SelectApplicationStep">
        <div class="DPA_CollapsingBlockHeader"><span class="DPA_Triangle"></span><span class="DPA_SectionTitle"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_SelectApplicationTitle %></span><span class="DPA_SelectedObject"></span></div>
        <div class="DPA_CollapsingBlockContent">
            <div class="sw-suggestion">
                <span class="sw-suggestion-icon"></span>
                <span id="DPA_ApplicationFilterMessage"></span>
            </div>

            <dpa:ApplicationSelector ID="ServerApplicationSelector" runat="server" />

        </div>        
    </div>

    <div class="bottom">
        <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
            <a href="javascript:void(0);" id="DPA_UpdateRelationshipButton" class="sw-btn sw-btn-primary"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.IntegrationWizard_UpdateRelationshipButton_Text %></span></span></a>
            <a href="javascript:void(0);" id="DPA_CancelButton" class="sw-btn"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.CancelButton_Text %></span></span></a>
        </div>
    </div>
</div>