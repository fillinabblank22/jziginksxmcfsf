﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WaitTimeLegend.ascx.cs" Inherits="Orion_DPA_Controls_Charts_WaitTimeLegend" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>
<orion:Include runat="server" Module="DPA" File="Charts.js" Section="Middle" />
<script type="text/javascript">
    SW.Core.Charts.Legend.dpa_WaitTimeLegendInitializer__<%= legendDiv.ClientID %> = function (chart) {
        SW.DPA.UI.ChartLegends.WaitTime.createStandardLegend(chart, '<%= legendDiv.ClientID %>', <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>);
    };
</script>
<div id="DPAHealthOverviewLegend" runat="server">
    <div id="legendDiv" runat="server">
        <table runat="server" class="chartLegend DPA_WaitTimeLegendTable"></table>
    </div>
</div>