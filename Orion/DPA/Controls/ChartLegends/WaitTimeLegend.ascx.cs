﻿using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPA_Controls_Charts_WaitTimeLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendInitializer
    {
        get
        {
            return "dpa_WaitTimeLegendInitializer__" + legendDiv.ClientID;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}