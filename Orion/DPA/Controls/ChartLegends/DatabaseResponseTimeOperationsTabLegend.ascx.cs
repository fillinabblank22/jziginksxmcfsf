﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using System;

public partial class Orion_DPA_Controls_ChartLegends_DatabaseResponseTimeOperationsTabLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return dpaLegend.LegendInitializer; } }
}