﻿using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_DPA_Controls_ChartLegends_BlockingImpactBlockersTabLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return DpaLegend.LegendInitializer; } }
}