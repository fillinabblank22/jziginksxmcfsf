﻿using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_DPA_Controls_ChartLegends_DatabaseResponseTimeDBUsersTabLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return dpaLegend.LegendInitializer; } }
}