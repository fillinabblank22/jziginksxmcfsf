﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockingImpactWaitersTabLegend.ascx.cs" Inherits="Orion_DPA_Controls_ChartLegends_BlockingImpactWaitersTabLegend" %>

<%@ Register TagPrefix="dpa" TagName="DPAChartLegend" Src="~/Orion/DPA/Controls/ChartLegends/BlockingImpactChartLegend.ascx" %>

<dpa:DPAChartLegend runat="server" id="DpaLegend" LegendChartType="waiters" />
