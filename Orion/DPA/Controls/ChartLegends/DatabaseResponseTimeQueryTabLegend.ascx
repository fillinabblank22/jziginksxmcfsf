﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseResponseTimeQueryTabLegend.ascx.cs" Inherits="Orion_DPA_Controls_ChartLegends_DatabaseResponseTimeQueryTabLegend" %>
<%@ Register TagPrefix="dpa" TagName="DPAChartLegend" Src="~/Orion/DPA/Controls/ChartLegends/DatabaseResponseTimeChartLegend.ascx" %>
<dpa:DPAChartLegend runat="server" id="dpaLegend" LegendChartType="query" />