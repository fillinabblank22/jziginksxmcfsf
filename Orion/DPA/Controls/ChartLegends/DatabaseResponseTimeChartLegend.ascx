﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatabaseResponseTimeChartLegend.ascx.cs" Inherits="Orion_DPA_Controls_ChartLegends_DatabaseResponseTimeChartLegend" %>
<orion:Include runat="server" Module="DPA" File="Charts.js" Section="Middle" />
<table id="legendTable" class="chartLegend DPA_DRT-LegendTable" runat="server"></table>