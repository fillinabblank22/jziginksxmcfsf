﻿using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_DPA_Controls_ChartLegends_BlockingImpactWaitersTabLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return DpaLegend.LegendInitializer; } }
}