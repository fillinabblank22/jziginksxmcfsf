﻿using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Globalization;
using System.Web.UI;

public partial class Orion_DPA_Controls_ChartLegends_DatabaseResponseTimeChartLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendChartType
    {
        get;
        set;
    }

    public string LegendInitializer
    {
        get
        {
            return "dpa_InstancesWithHighestWaitTimeLegendInitializer__" + legendTable.ClientID;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        const string scriptTemplate = @"
(function () {{
    SW.Core.Charts.Legend['{0}'] = function (chart, url) {{
        SW.DPA.UI.ChartLegends.DatabaseResponseTime.createChartLegend(chart, url, '{1}', '{2}');
    }};
}}());
";
        var legendInitializerScript = string.Format(CultureInfo.InstalledUICulture, scriptTemplate, 
            this.LegendInitializer, legendTable.ClientID, this.LegendChartType);

        //By registering the script in this manner we ensure it runs when it is loaded via update panel. Standard scripts are not run.
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.UniqueID, legendInitializerScript, true);

        base.OnPreRender(e);
    }
}