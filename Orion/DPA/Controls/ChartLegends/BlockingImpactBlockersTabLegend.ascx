﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BlockingImpactBlockersTabLegend.ascx.cs" Inherits="Orion_DPA_Controls_ChartLegends_BlockingImpactBlockersTabLegend" %>

<%@ Register TagPrefix="dpa" TagName="DPAChartLegend" Src="~/Orion/DPA/Controls/ChartLegends/BlockingImpactChartLegend.ascx" %>

<dpa:DPAChartLegend runat="server" id="DpaLegend" LegendChartType="blockers" />