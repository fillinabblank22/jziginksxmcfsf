﻿using System;
using System.Web;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using SolarWinds.DPA.Common.Settings.CentralizedSettings;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Extensions;

public partial class Orion_DPA_Resources_DatabaseResponseTime_DatabaseInstancesTable : UserControl
{
    private bool? _showKpis;

    public string QueryFilter
    {
        get
        {
            return string.Empty;
        }
    }

    public bool ShowKpis => _showKpis ?? (_showKpis = ServiceLocator.Current.GetInstance<IDatabaseInstancesSettings>().ShowKpis).Value;

    public int ResourceID { get; private set; }

    public string ConnectivityIssueWarningClientId { get; private set; }
    public string SelectionHandlerUniqueId { get; private set; }

    protected string DatabaseInstancesFilter { get; set; }
    
    private string SelectedDbiStateKey
    {
        get { return string.Format("DatabaseInstancesTable{0}_SelectedDbi", this.ResourceID); }
    }

    public int SelectedDatabaseInstance
    {
        get { return this.ViewState.GetStrictOrDefault(SelectedDbiStateKey, -1); }
        set { this.ViewState[SelectedDbiStateKey] = value; }
    }

    public void Initialize(IEnumerable<int> databaseInstancesIds, int uniqueId, int resourceID,
        string connectivityIssueId, string selectionHandlerUniqueId)
    {
        this.ResourceID = resourceID;
        this.ConnectivityIssueWarningClientId = connectivityIssueId;

        this.DatabaseInstancesFilter = string.Join(",", databaseInstancesIds);

        // radio button after postback will set proper selection
        this.SelectionHandlerUniqueId = selectionHandlerUniqueId;
        this.SelectedDatabaseInstance = GetSelectedDatabaseInstance(databaseInstancesIds, uniqueId);
    }

    public string AsyncLoadDataJsCallback
    {
        get
        {
            return string.Format(CultureInfo.InvariantCulture, "window.SW.DPA.AsyncLoadData{0}", this.ResourceID >= 0 ? this.ResourceID : this.ResourceID * -1);
        }
    }

    private int GetSelectedDatabaseInstance(IEnumerable<int> databaseInstancesIds, int uniqueId)
    {
        string sessionId = String.Format("DPADatabaseResponseTime_SelectedId_{0}_{1}", this.ResourceID, uniqueId);
        int selectedDatabaseInstance;

        // try to parse instance id from postback event
        string evArg = Request.Params.Get("__EVENTARGUMENT");

        if (this.Page.IsPostBack && !string.IsNullOrEmpty(evArg))
        {
            var parts = evArg.Split('$');
            if (parts.Length == 4 &&
                parts[1] == this.ResourceID.ToString() &&
                int.TryParse(parts[3], out selectedDatabaseInstance))
            {
                HttpContext.Current.Session[sessionId] =
                    selectedDatabaseInstance;

                return selectedDatabaseInstance;
            }
        }

        // look if there is somethig set from previous postback
        selectedDatabaseInstance = this.SelectedDatabaseInstance;
        if (selectedDatabaseInstance >= 0)
            return selectedDatabaseInstance;

        // look if selected database id is stored in session
        var sessionSelection = HttpContext.Current.Session[sessionId];
        if (sessionSelection != null)
        {
            return int.Parse(sessionSelection.ToString());
        }

        // if not successfull then get first instance id from collection
        return databaseInstancesIds.First();
    }
}