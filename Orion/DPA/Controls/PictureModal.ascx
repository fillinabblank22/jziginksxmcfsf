﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PictureModal.ascx.cs" Inherits="Orion_DPA_Controls_PictureModal" %>
<orion:Include runat="server" Module="DPA" File="ModalWindows.js" />
<orion:Include runat="server" File="DPA/Admin/styles/AdminAndViewsBundle.css" />

<div id="DPA_PictureModal" style="display: none">
    <img />
</div>
