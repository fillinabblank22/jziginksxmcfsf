﻿using System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;

public partial class Orion_DPA_Controls_ApplicationSelector : System.Web.UI.UserControl
{
    public IGridColumnDefinitions GridColumnDefinitions { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        // do not render this control when SAM is not installed
        if (!ServiceLocator.Current.GetInstance<IDPASettings>().IsApmInstalled)
            this.Visible = false;
    }
}