﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvisorsControl.ascx.cs" Inherits="Orion_DPA_Controls_AdvisorsControl" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>

<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>

<div id="DPA_Advisors-<%= ResourceId %>-Content">
    <div class="DPA_AdvisorsSelect">
        <label for="Select-<%= ResourceId %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeverity_Label %>:</label>
        <select id="Select-<%= ResourceId %>">
            <option value="-1"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_All %></option>
            <option value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Normal %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Normal %></option>
            <option value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Warning %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Warning %></option>
            <option value="<%= (int) SolarWinds.DPA.Common.Models.AlarmLevel.Critical %>"><%: Resources.DPAWebContent.DPA_AdvisorsSeveritySelectBox_Critical %></option>
        </select>
    </div>
    <div class="DPA_AdvisorsTable">
        <dpa:sortablepageabletable id="SortablePageableTable" runat="server" />
        <input type="hidden" id="OrderBy-<%= ResourceId %>" value="[RunTime] DESC" />
    </div>
</div>
<div id="DPA_Advisors-<%= ResourceId %>-NoData" class="DPA_SampleImage" style="display: none">
    <img src="/Orion/DPA/images/Samples/advisors-sample.png" alt="sample" />

    <div class="DPA_AdvisorSample">
        <ul>
            <li><%: Resources.DPAWebContent.AdvisorsResource_SampleText %></li>
        </ul>
        <% if (Profile.AllowAdmin)
           { %>
        <p><a href="<%: SolarWinds.DPA.Web.Helpers.PageUrlHelper.ServerManagement %>">&#187; <%: Resources.DPAWebContent.AdvisorsResource_StartDPAIntegrationLink_Label %></a></p>
        <% } %>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var advisorIconFileKey = 0;
        var advisorIconToolTipTextKey = 1;
        var advisorMessageKey = 2;
        var advisorDetailUrlKey = 3;
        var databaseIconFileKey = 4;
        var databaseNameKey = 5;
        var databaseDetailUrlKey = 6;

        var nodeLinkKey = 7;
        var nodeNameKey = 8;
        var nodeIconKey = 9;
        var hasNodeKey = 10;

        var dateKey = 11;

        var loadingIcon = new SW.DPA.UI.LoadingIcon('<%= LoadingIconClientId %>');
        var connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning('<%= ConnectivityIssueWarningClientId %>');
        var errorMessage = new SW.DPA.UI.ErrorMessage('<%= ErrorMessageClientId %>');

        SW.DPA.UI.SortablePageableTable.initialize(
        {
            uniqueId: '<%= ResourceId %>',
            pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                loadingIcon.show();
                connectivityIssueWarning.hide();
                errorMessage.hide();
                var uniqueId = '<%= ResourceId %>';
                var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                var currentSeverity = $("#Select-" + uniqueId + " option:selected").val();
                var dpaServerId = '<%= DpaServerId %>';
                SW.Core.Services.callController("/api/DpaAdvisors/GetAdvisorsForTopWaitDatabases", {
                    Severity: currentSeverity,
                    Limit: pageSize,
                    PageIndex: pageIndex,
                    OrderByClause: currentOrderBy,
                    DpaServerId: dpaServerId
                }, function (result) {
                    loadingIcon.hide();

                    if (typeof result.Metadata.PartialResultErrors !== 'undefined' && result.Metadata.PartialResultErrors.length > 0) {
                        connectivityIssueWarning.show(result.Metadata.PartialResultErrors);
                    }

                    if (typeof result.Metadata.IntegrationEstablished !== 'undefined' && !result.Metadata.IntegrationEstablished) {
                        $('#DPA_Advisors-<%= ResourceId %>-Content').hide();
                        $('#DPA_Advisors-<%= ResourceId %>-NoData').show();
                    } else {
                        $('#DPA_Advisors-<%= ResourceId %>-NoData').hide();
                        $('#DPA_Advisors-<%= ResourceId %>-Content').show();
                    }

                    onSuccess(result);
                }, function (errorResult) {
                    errorMessage.show(errorResult);
                    loadingIcon.hide();
                });
            },
            initialPageIndex: 0,
            rowsPerPage: 10,
            noDataMessage: {
                show: true,
                isHtml: true,
                formatter: function () {
                    return "<%: Resources.DPAWebContent.NoDataAvailableMessage %>";
                },
                cssClassProvider: function () {
                    return "DPA_SortablePageableTableNoData";
                }
            },
            columnSettings: {
                'AdvisorIconFile': {
                    caption: '',
                    isHtml: true,
                    allowSort: false,
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" class="NoTip" target="_blank" title="{1}"><img src="{2}" /></a>',
                            rowArray[advisorDetailUrlKey],
                            rowArray[advisorIconToolTipTextKey],
                            rowArray[advisorIconFileKey]);
                    }
                },
                'AdvisorMessage': {
                    caption: "<%= Resources.DPAWebContent.DPA_AdvisorsTableHeader_AdvisorMessage %>",
                    isHtml: true,
                    allowSort: true,
                    sortingProperty: 'AlarmLevel',
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}" target="_blank">{1}</a>', rowArray[advisorDetailUrlKey], cellValue);
                    }
                },
                "DatabaseIconFile": {
                    caption: '',
                    isHtml: true,
                    allowSort: false,
                    formatter: function (cellValue, rowArray) {
                        return SW.Core.String.Format('<a href="{0}"><img src="{1}" /></a>', rowArray[databaseDetailUrlKey], cellValue);
                    }
                },
                "DatabaseName": {
                    caption: "<%= Resources.DPAWebContent.DPA_AdvisorsTableHeader_Database %>",
                    isHtml: true,
                    allowSort: false,
                    formatter: function (cellValue, rowArray) {
                        var dbName = SW.DPA.Common.FormatDatabaseInstanceLink(rowArray[databaseDetailUrlKey],
                            undefined,
                            cellValue,
                            rowArray[nodeLinkKey],
                            rowArray[nodeIconKey],
                            rowArray[nodeNameKey],
                            <%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>);

                        return dbName;
                    }
                },
                "Date": {
                    caption: "<%= Resources.DPAWebContent.DPA_AdvisorsTableHeader_Date %>",
                    isHtml: false,
                    allowSort: true,
                    sortingProperty: 'RunTime'
                }
            }
        });

        var refresh = function () { SW.DPA.UI.SortablePageableTable.refresh('<%= ResourceId %>'); };
        SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
        refresh();
        $("#Select-<%= ResourceId %>").change(function() {
            SW.DPA.UI.SortablePageableTable.refresh('<%= ResourceId %>');
        });
    });
</script>
