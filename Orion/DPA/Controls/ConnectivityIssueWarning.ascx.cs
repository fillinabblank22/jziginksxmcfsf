﻿using System.Collections.Generic;
using SolarWinds.DPA.Common.Models;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_DPA_Controls_ConnectivityIssueWarning : System.Web.UI.UserControl
{
    /// <summary>
    /// Contains uniqueidentifier of resource (Resource.ID value).
    /// </summary>
    public string UniqueClientID { get; set; }

    protected string HelpLink
    {
        get { return HelpHelper.GetHelpUrl("OrionDPAConnectivityIssue"); }
    }

    protected bool IsVisible { get; set; }

    public override string ClientID
    {
        get { return "DPA_ConnectivityIssue-" + UniqueClientID; }
    }

    protected IEnumerable<PartialResultsError> PartialResultsErrors { get; set; }  

    public void Show(IEnumerable<PartialResultsError> partialResultsErrors = null)
    {
        PartialResultsErrors = partialResultsErrors;
        IsVisible = true;
    }

    public void Hide()
    {
        IsVisible = false;
    }
}