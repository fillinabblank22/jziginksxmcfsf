﻿using System.Web.UI;

public partial class Orion_DPA_Controls_LatestDeadlocksTable : System.Web.UI.UserControl
{
    public int DatabaseInstanceId { get; private set; }
    public int ResourceId { get; private set; }
    public string ConnectivityIssueWarningClientId { get; private set; }
    public string ErrorMessageClientId { get; private set; }
    public string LoadingIconClientId { get; private set; }
    
    public void Initialize(int databaseInstanceId, int resourceId, string connectivityIssueId, string errorMessageId, string loadingIconId)
    {
        DatabaseInstanceId = databaseInstanceId;

        ResourceId = resourceId;
        ConnectivityIssueWarningClientId = connectivityIssueId;
        ErrorMessageClientId = errorMessageId;
        LoadingIconClientId = loadingIconId;

        SortablePageableTable.UniqueClientID = resourceId;
        
        RegisterLatestDeadlocksScript();
    }

    private void RegisterLatestDeadlocksScript()
    {

        var script = $@"
            $(document).ready(function () {{
                if (!$('#{LatestDeadlocks.ClientID}').length)
                    return;

                var deadlockDetailUrlKey = 3;

                var loadingIcon = new SW.DPA.UI.LoadingIcon('{LoadingIconClientId}');
                var connectivityIssueWarning = new SW.DPA.UI
                    .ConnectivityIssueWarning('{ConnectivityIssueWarningClientId}');
                var errorMessage = new SW.DPA.UI.ErrorMessage('{ErrorMessageClientId}');

                SW.DPA.UI.SortablePageableTable.initialize(
                    {{
                        uniqueId: '{ResourceId}',
                        pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {{
                            loadingIcon.show();
                            connectivityIssueWarning.hide();
                            errorMessage.hide();
                            var databaseInstanceId = '{DatabaseInstanceId}';
                            SW.Core.Services.callController('/api/DpaDeadlocks/GetLatestDeadlocks', {{
                                DatabaseInstanceId: databaseInstanceId,
                                Limit: pageSize,
                                PageIndex: pageIndex
                            }},
                            function (result) {{
                                loadingIcon.hide();

                                if (typeof result.Metadata.PartialResultErrors !== 'undefined' &&
                                    result.Metadata.PartialResultErrors.length > 0) {{
                                    connectivityIssueWarning.show(result.Metadata.PartialResultErrors);
                                }}

                                if (typeof result.Metadata.IntegrationEstablished !== 'undefined' &&
                                    !result.Metadata.IntegrationEstablished) {{
                                    $('#{LatestDeadlocks.ClientID}').hide();
                                }} else {{
                                    $('#{LatestDeadlocks.ClientID}').show();
                                }}

                                onSuccess(result);
                            }},
                            function (errorResult) {{
                                errorMessage.show(errorResult);
                                loadingIcon.hide();
                            }});
                    }},
                    initialPageIndex: 0,
                    rowsPerPage: 5,
                    columnSettings: {{
                        'Time': {{
                            caption: '{Resources.DPAWebContent.BlockingImpactResource_DeadlocksTab_LatestDeadlocks}',
                            isHtml: true,
                            allowSort: false,
                            formatter: function (cellValue, rowArray) {{
                                return SW.Core.String.Format('<a href=""{{0}}"" class=""NoTip"" target=""_blank"">{{1}}</a>',
                                    rowArray[deadlockDetailUrlKey],
                                    cellValue);
                            }}
                        }},
                        'WaitTimeImpact': {{
                            caption: '{Resources.DPAWebContent.BlockingImpactResource_DeadlocksTab_WaitTimeImpact}',
                            isHtml: true,
                            allowSort: false,
                            formatter: function (cellValue, rowArray) {{
                                return SW.Core.String.Format('{{0}} {{1}}',
                                    cellValue,
                                    '{Resources.DPAWebContent.BlockingImpactResource_DeadlocksTab_WaitTimeImpactUnit}');
                            }}
                        }},
                        'NumberOfSessions': {{
                            caption: '{Resources.DPAWebContent.BlockingImpactResource_DeadlocksTab_NumberOfSessions}',
                            isHtml: true,
                            allowSort: false
                        }}
                    }}
                    }});

                var refresh = function () {{ SW.DPA.UI.SortablePageableTable.refresh('{ResourceId}'); }};
                SW.Core.View.AddOnRefresh(refresh, '{SortablePageableTable.ClientID}');
                refresh();
            }});";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "loadLatestDeadlocksTable", script, true);
    }

    public void Show()
    {
        LatestDeadlocks.Visible = true;
    }

    public void Hide()
    {
        LatestDeadlocks.Visible = false;
    }
}