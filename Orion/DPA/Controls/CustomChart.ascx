﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomChart.ascx.cs" Inherits="Orion_DPA_Resources_DatabaseResponseTime_CustomChart" %>
<%@ Register TagPrefix="dpa" TagName="PoweredByIcon" Src="~/Orion/DPA/Controls/PoweredByIcon.ascx" %>

<asp:Panel ID="pnlCustomChart" runat="server">
    <div id="divChart" runat="server"></div>
    <asp:PlaceHolder ID="legendUserControlPlaceholder" runat="server"></asp:PlaceHolder>
    <dpa:PoweredByIcon runat="server" id="PoweredByIcon" />
</asp:Panel>
