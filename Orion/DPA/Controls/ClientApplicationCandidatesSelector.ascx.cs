﻿using System;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;

public partial class Orion_DPA_Controls_ClientApplicationCandidatesSelector : System.Web.UI.UserControl
{
    public IGridColumnDefinitions GridColumnDefinitions { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            GridColumnDefinitions =
                ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaClientApplicationCandidatesController.UniqueId);
        }

        base.OnLoad(e);
    }
}