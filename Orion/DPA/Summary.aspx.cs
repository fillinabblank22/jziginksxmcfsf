using System;
using SolarWinds.DPA.Web.Helpers;
using SolarWinds.DPA.Web.Views;

public partial class Orion_DPA_Summary : DpaViewPage
{
    public override string ViewType
    {
        get { return "DPA Summary"; }
    }

    public override string HelpFragment
    {
        get
        {
            return "OrionDPADatabasesSummary";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (DpaSettings.NumberOfIntegratedDpaServers == 1)
        {
            Response.Redirect("/Orion/DPA/DpaServerDetails.aspx");
        }

        this.Title = this.ViewInfo.ViewTitle;
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
