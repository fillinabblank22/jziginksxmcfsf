﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/DPA/DPAView.master" CodeFile="DatabaseStorageSummary.aspx.cs" Inherits="Orion_DPA_DatabaseStorageSummary"
    Title="<%$ Resources: DpaWebContent, DatabaseStorageSummaryView_Title %>" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="DPAPageTitle">
    <h1><%=Page.Title %></h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="DPAMainContentPlaceHolder">
	<orion:ResourceHostControl ID="ResourceHostControl" runat="server">
		<orion:ResourceContainer runat="server" ID="ResourceContainer" />
	</orion:ResourceHostControl>
</asp:Content>
