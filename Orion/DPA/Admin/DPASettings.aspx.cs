﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Admin_DPASettings : System.Web.UI.Page
{
    private IDPASettings _settings;
    private IModuleInfoHelper _moduleInfoHelper;

    public IDPASettings Settings
    {
        get
        {
            return _settings ?? (_settings = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }

    public IModuleInfoHelper ModuleInfoHelper => _moduleInfoHelper ?? (_moduleInfoHelper = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IModuleInfoHelper>());

    protected void Page_Load(object sender, EventArgs e)
    {
        IntegrationEstablished.Visible = Settings.IsIntegrationEstablished;
        IntegrationNotEstablished.Visible = !Settings.IsIntegrationEstablished;
    }
}
