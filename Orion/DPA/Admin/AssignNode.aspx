﻿<%@ Page Title="<%$ Resources: DPAWebContent, AssignNode_Title%>" Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/DPA/Admin/DpaAdminPage.master" CodeFile="AssignNode.aspx.cs" Inherits="Orion_DPA_Admin_AssignNode" %>
<%@ Register Src="~/Orion/DPA/Controls/NodeSelector.ascx" TagPrefix="dpa" TagName="NodeSelector" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css" />
    <orion:Include runat="server" Module="DPA" File="Common.js" />
    <orion:Include runat="server" Module="DPA" File="DPA.Admin.AssignNode.js" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div class="DPA_AssignEntity">
        <h1><%: Resources.DPAWebContent.AssignNode_Title %></h1>
        <p><%: Resources.DPAWebContent.AssignNode_Description %></p>
        <div class="DPA_IconsLine"><img src="<%: DatabaseInstanceIconUrl %>" /> <%: DatabaseInstanceName %></div>
    
        <div class="DPA_AssignGrid">
            <dpa:NodeSelector ID="NodeSelector" runat="server" />  
        </div>
    
        <div class="DPA_BottomButtonsBar">
            <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
                <a class="sw-btn sw-btn-primary DPA_SelectObject NoTip" href="<%: ReturnUrl %>"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.NodeSelectorModal_SubmitButtonLabel %></span></span></a>
                <a class="sw-btn DPA_CancelButton NoTip" href="<%: ReturnUrl %>"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.NodeSelectorModal_CancelButtonLabel %></span></span></a>
            </div>
        </div>
    </div>
    <script>
        $().ready(function() {
            SW.DPA.Admin.AssignNode(<%= DatabaseInstanceId %>);
        });
    </script>    
</asp:Content>    

