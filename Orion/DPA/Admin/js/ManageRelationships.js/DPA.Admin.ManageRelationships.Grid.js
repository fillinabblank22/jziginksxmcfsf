﻿Ext.namespace('SW.DPA.Admin.ManageRelationships');

SW.DPA.Admin.ManageRelationships.Grid = Ext.extend(SW.DPA.Admin.GroupByGrid, {

    constructor: function (config) {

        this.renderRelationshipStatus = (function() {
                var grid = this;

                return function(value, meta, record) {
                    meta.css = "DPA_RelationshipStatusCell";
                    var msg = "";
                    var statusDescription = Ext.util.Format.htmlEncode(record.data.RelationshipStatusDescription);
                    msg = SW.Core.String.Format("<div><img src='{0}' ext:qtitle='{1}' ext:qtip=' ' /></div>{2}",
                        Ext.util.Format.htmlEncode(record.data.RelationshipStatusIcon),
                        statusDescription,
                        grid.localizedTexts.noRelationshipText !== record.data.RelationshipStatusDescription
                        ? statusDescription
                        : '');

                    return msg;
                };
            }).apply(this);

        SW.DPA.Admin.ManageRelationships.Grid.superclass.constructor.call(this, config);
    },

    createDataGridStore: function (url, readerFields, sortOrderField, connectivityIssueWarning, callbackFn) {
        return new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: url,
                method: "POST",
                timeout: 900000,
                success: function (response, options) {
                    connectivityIssueWarning.hide();
                    if (typeof response.responseText !== 'undefined' && response.status == 200) {
                        try {
                            var responseJson = JSON.parse(response.responseText);
                            var metadata = responseJson.Metadata || false;
                            if (metadata) {
                                if (typeof metadata.PartialResultErrors !== 'undefined' && metadata.PartialResultErrors !== null && metadata.PartialResultErrors.length > 0) {
                                    //some endpoints are not available
                                    connectivityIssueWarning.show(metadata.PartialResultErrors);
                                }

                                callbackFn && callbackFn(responseJson);
                            }
                        } catch (err) {
                            $('#ConnectionIssue').show();
                        }
                    }
                },
                failure: function (xhr, options) {
                    SW.Core.Services.handleError(xhr);
                }
            }),
            reader: new Ext.data.JsonReader({
                totalProperty: "TotalRows",
                root: "DataTable.Rows"
                },
                readerFields
            ),
            remoteSort: true,
            sortInfo: {
                field: sortOrderField,
                direction: "ASC"
            }
        });
    },

    displayShowOnlyCombo: false,
    displayPagingToolbar: true,
    displayPageSizeControl: true,
    init: function() {
        var me = this;
        SW.DPA.Admin.ManageRelationships.Grid.superclass.init.call(me);
    }
});