﻿Ext.namespace('SW.DPA.Admin.ManageRelationships');

SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid = Ext.extend(SW.DPA.Admin.ManageRelationships.Grid, {
    localizedTexts: {
        instancesSelectedMessage: "@{R=DPA.Strings;K=IntegrationWizard_ClientApplicationsSelected;E=js}",
        findNewRelationshipsModalTitle: "@{R=DPA.Strings;K=IntegrationWizard_FindNewClientApplicationsModal_Title;E=js}",
        findNewRelationshipsModalMessage: "@{R=DPA.Strings;K=IntegrationWizard_FindNewClientApplicationsModal_Message;E=js}",
        clientCommunicatesWithInstancesValue: "@{R=DPA.Strings;K=IntegrationWizard_Grid_ClientCommunicatesWithInstancesValue;E=js}",
        clientCommunicatesWithInstancesTipHeader: "@{R=DPA.Strings;K=IntegrationWizard_Grid_ClientCommunicatesWithInstancesTipHeader;E=js}",
        editRelationshipTooltipText: "@{R=DPA.Strings;K=IntegrationWizard_Grid_ActionsTooltip_EditRelationship;E=js}",
        addApplicationButtonCaption: '@{R=DPA.Strings; K=IntegrationWizard_Grid_AddApplication; E=js}',
        saveClientApplicationFailedMessage: '@{R=DPA.Strings; K=IntegrationWizard_Applications_SaveClientApplicationFailedMessage; E=js}',
        pagingToolbarDisplayMsg: '@{R=DPA.Strings;K=IntegrationWizard_PagingToolbarDisplayMsg;E=js}',
        pagingToolbarRefreshTooltip: '@{R=DPA.Strings;K=IntegrationWizard_ClientApplications_PagingToolbarRefreshTooltip;E=js}',
        removeRelationshipTooltipText: '@{R=DPA.Strings; K=IntegrationWizard_Grid_ActionsTooltip_RemoveRelationship; E=js}',
        removeRelationshipModalTitle: '@{R=DPA.Strings; K=IntegrationWizard_Applications_RemoveRelationshipConfirmWindow_Title; E=js}',
        removeRelationshipModalMessage: '@{R=DPA.Strings; K=IntegrationWizard_Applications_RemoveRelationshipConfirmWindow_Message; E=js}',
        noRelationshipText: "@{R=DPA.Strings;K=IntegrationWizard_TypesOfRelationships_Unknown;E=js}",
        updatingRelationshipsTitle: "@{R=DPA.Strings;K=IntegrationWizard_UpdateRelationshipsProgressDialogTitle;E=js}",
        updatingRelationshipsMessage: "@{R=DPA.Strings;K=IntegrationWizard_UpdateRelationshipsProgressDialogMessage;E=js}",
        removeAllRelationships: '@{R=DPA.Strings;K=ClientRelationshipsManagement_RemoveAllButtonLabel;E=js}',
        removeAllRelationshipsMessage: '@{R=DPA.Strings;K=ClientRelationshipsManagement_RemoveAllConfirmationMessage;E=js}',
        reviewCandidatesButtonLabel: '@{R=DPA.Strings;K=ClientRelationshipsManagement_ReviewCandidatesButtonLabel;E=js}'
    },

    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },
    existMunuallyCreatedRelationships: false,
    clearCacheAndRefreshGrid: function () {
        var me = this;
        SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationsGrid", "ClearDatabaseClients", me.getBaseParams(), function () { me.internalGridRefresh(); }, function () { });       
    },
    constructor: function (connectivityIssueWarningClientId) {
        var me = this;

        var config = {
            onPagingToolbarRefresh: function () {
                me.clearCacheAndRefreshGrid();
                return true;
            }
        };

        this.applicationConfigurationModal = null;
        var simpleProgressModal = new SW.DPA.Admin.SimpleProgressModal();
        this.connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning(connectivityIssueWarningClientId);

        this.renderActions = function (value, meta, record) {
            var msg;

            msg = SW.Core.String.Format("<div class='cellActions'><a href='#' data-client-application-id='{0}' class='DPA_ApplicationDialog'><img ext:qtitle='{1}' ext:qtip=' ' src='/Orion/DPA/images/edit_16x16.png' /></a>"
                + "<a href=\"#\" data-client-application-id='{0}' class='DPA_RemoveRelationship'><img class='DPA_ActionButton' ext:qtitle='{2}' ext:qtip=' ' src='/Orion/DPA/images/relationship-remove.png' /></a></div>",
				record.data.ApplicationID,
				Ext.util.Format.htmlEncode(me.localizedTexts.editRelationshipTooltipText),
				Ext.util.Format.htmlEncode(me.localizedTexts.removeRelationshipTooltipText));

            return msg;
        }

        this.renderNode = function (value, meta, record) {
            return me.renderEntityNameWithIcon(record.data.NodeInstanceType, record.data.NodeName, record.data.NodeStatus, record.data.NodeDetailsUrl, record.data.NodeID);
        }

        this.renderApplicationName = function (value, meta, record) {
            return me.renderEntityNameWithIcon(record.data.ApplicationInstanceType, record.data.ApplicationName, record.data.ApplicationStatus, record.data.ApplicationDetailsUrl);
        }

        this.renderDatabaseInstances = function(value, meta, record) {
            var text = SW.Core.String.Format(me.localizedTexts.clientCommunicatesWithInstancesValue, record.data.ApplicationDatabasesCount);
            var toolTipHeader = SW.Core.String.Format(me.localizedTexts.clientCommunicatesWithInstancesTipHeader, record.data.NodeName, record.data.ApplicationDatabasesCount);
            
            var instances = JSON.parse(record.data.InstanceName);
            
            var list = $('<ul/>');
            $.each(instances, function(index, item) {
                list.append($('<li/>').text(item));
            });
            var toolTipBody = SW.Core.String.Format("<ul>{0}</ul>", list.html());

            var cellValue = SW.Core.String.Format("<span ext:qtitle='{1}' ext:qtip='{2}' class='DPA_Dotted_Underline'>{0}</span>", text, toolTipHeader, Ext.util.Format.htmlEncode(toolTipBody));

            return cellValue;
        }

        this.saveApplications = function (dataToSave) {
            me.configurationModalErrorOccured = false;
            simpleProgressModal.show(me.localizedTexts.updatingRelationshipsTitle, me.localizedTexts.updatingRelationshipsMessage);
            SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationsGrid", "SaveClientRelationship", $.extend({}, dataToSave, me.getBaseParams()),
                function (onsuccess) {
                    me.applicationConfigurationModal.close();
                    simpleProgressModal.close();
                    me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
                },
                function (onerror) {
                    me.applicationConfigurationModal.close();
                    simpleProgressModal.close();
                    me.displayErrorMessage(me.localizedTexts.saveClientApplicationFailedMessage);
                    me.configurationModalErrorOccured = true;
                });
        }

        this.removeRelationship = function(clientApplicationId) {
            Ext.Msg.show({
                title: me.localizedTexts.removeRelationshipModalTitle,
                msg: me.localizedTexts.removeRelationshipModalMessage,
                buttons: Ext.Msg.YESNO,
                fn: function(res) {
                    if (res == 'yes') {
                        simpleProgressModal.show(me.localizedTexts.updatingRelationshipsTitle, me.localizedTexts.updatingRelationshipsMessage);
                        var params = { applicationId: clientApplicationId, modelId: me.getBaseParams().ModelID };
                        SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationsGrid", "RemoveRelationship", params,
                            function (onsuccess) {
                                simpleProgressModal.close();
                                me.internalGridRefresh();
                            },
                            function (onerror) {
                                simpleProgressModal.close();
                                me.displayErrorMessage(me.localizedTexts.removeRelationshipFailedMessage);
                                me.internalGridRefresh();
                            });
                    }
                }
            });
        };

        this.closeModal = function () {
            if (!me.configurationModalErrorOccured) {
                me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
            }
        }

        SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid.superclass.constructor.call(this, config);
    },

    init: function () {
        var me = this;
        SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid.superclass.init.call(me);

        this.applicationConfigurationModal = new SW.DPA.Admin.ApplicationConfigurationModal('DPA_ApplicationConfigurationModal');
    },

    onMainGridStoreLoaded: function () {
        var me = this;
        SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid.superclass.onMainGridStoreLoaded.call(me);

        // bind click events to button elements when grid is rendered
        $('.DPA_ApplicationDialog').click(function () {
            var id = $(this).data('client-application-id');
            var data = me.getDataById(id);
            me.applicationConfigurationModal.show({ selectedData: [data] }, me.saveApplications, me.closeModal);
            return false;
        });

        $('.DPA_RemoveRelationship').click(function () {
            var id = $(this).data('client-application-id');
            me.removeRelationship(id);
            return false;
        });

        SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationsGrid", "ExistManuallyCreatedRelationships", {},
                                    function (response) { me.existMunuallyCreatedRelationships = response; }, function () { });
    },

    createMainGriDataStore: function () {
        return SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid.superclass.createDataGridStore("/sapi/DpaClientApplicationsGrid/GetDatabaseClientsTable",
	        this.dataStoreMapping, "ApplicationName", this.connectivityIssueWarning);
    },

    createShowOnlyDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaClientApplicationsGrid/GetShowOnlyGroupingValues",
                [
                    { name: 'Caption', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                    { name: 'Value', mapping: 3 }
                ]);
    },

    createGroupingDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaClientApplicationsGrid/GetClientApplicationsGroupingValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);
    },

    createGroupingGridDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaClientApplicationsGrid/GetClientApplicationsGroupValues",
                [
                    { name: 'Id', mapping: 0 },
                    { name: 'Value', mapping: 1 },
                    { name: 'Cnt', mapping: 2 },
                    { name: 'SelectedCnt', mapping: 3 }
                ]);
    },

    getDataById: function (id) {
        var index = this.getMainGrid().store.findBy(function (record) {
            return (record.get('ApplicationID') == id);
        });

        return (index >= 0 ? this.getMainGrid().store.getAt(index).data : undefined);
    },

    getGridToolbarConfig: function () {
        var me = this;
        return [
            {
                id: 'AddApplicationButton',
                text: me.localizedTexts.addApplicationButtonCaption,
                icon: '/Orion/DPA/images/add_16x16.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    me.applicationConfigurationModal.show(null, me.saveApplications, me.closeModal);
                    return false;
                }
            },
            '-',
            {
                id: 'ReviewCandidates',
                text: me.localizedTexts.reviewCandidatesButtonLabel,
                icon: '/Orion/DPA/images/find-new-relationships.gif',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    DPA.Admin.ReviewClientRelationshipCandidates(function() {
                        me.clearCacheAndRefreshGrid();
                    });
                    return false;
                }
            },
            '-',
            {
                id: 'RemoveAll',
                text: me.localizedTexts.removeAllRelationships,
                icon: '/Orion/images/delete_16x16.gif',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    Ext.Msg.show({
                        width: 350,
                        title: me.localizedTexts.removeAllRelationships,
                        msg: me.localizedTexts.removeAllRelationshipsMessage +
                            (me.existMunuallyCreatedRelationships ? '<div class="DPA_ClientRelManagement_ConfirmationCheckBox"><input type="checkbox" checked id="includeManuallyCreatedRelCheckbox" /> <label for="includeManuallyCreatedRelCheckbox">Include manually created relationships</label></div>' : ''),
                        buttons: Ext.Msg.OKCANCEL,
                        fn: function (res) {
                            if (res == 'ok') {
                                var includeManuallyCreatedRelations = $('.DPA_ClientRelManagement_ConfirmationCheckBox').length > 0 ? $('.DPA_ClientRelManagement_ConfirmationCheckBox input').prop("checked") : false;
                                SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationsGrid", "RemoveAllRelationships",
                                    $.extend({}, { IncludeManuallyCreated: includeManuallyCreatedRelations }, me.getBaseParams()),
                                    function (onsuccess) { me.internalGridRefresh(); }, function (onerror) { me.internalGridRefresh(); });
                            }
                        }
                    });
                    return false;
                }
            }
        ];
    },

    getRecordId: function (data) {
        return data.ApplicationID;
    },

    getEnvelopElementId: function () {
        return "integrationGrid";
    },

    getPlaceholderElementId: function () {
        return "relationshipsPlaceholder";
    },

    getMainGridId: function () {
        return "relationshipsGrid";
    }
});