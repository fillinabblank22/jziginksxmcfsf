﻿Ext.namespace('SW.DPA.Admin.ManageRelationships');

SW.DPA.Admin.ManageRelationships.RelationshipsGrid = Ext.extend(SW.DPA.Admin.ManageRelationships.Grid, {
    localizedTexts: {
        relationshipIsOKTooltipText: "@{R=DPA.Strings;K=IntegrationWizard_Grid_StatusTooltip_RelationshipOK;E=js}",
        relationshipDoesNotExistTooltipText: "@{R=DPA.Strings;K=IntegrationWizard_Grid_StatusTooltip_RelationshipNotExist;E=js}",
		findNewRelationshipsModalTitle: "@{R=DPA.Strings;K=IntegrationWizard_FindNewRelationshipsModal_Title;E=js}",
		findNewRelationshipsModalMessage: "@{R=DPA.Strings;K=IntegrationWizard_FindNewRelationshipsModal_Message;E=js}",
		removeRelationshipModalTitle: "@{R=DPA.Strings;K=IntegrationWizard_RemoveRelationshipModal_Title;E=js}",
		removeRelationshipModalMessage: "@{R=DPA.Strings;K=IntegrationWizard_RemoveRelationshipModal_Message;E=js}",
		noRelationshipFound: "@{R=DPA.Strings;K=IntegrationWizard_Grid_NoRelationshipFound;E=js}",
		instancesSelectedMessage: "@{R=DPA.Strings;K=IntegrationWizard_DatabaseInstancesSelected;E=js}",
		saveRelationshipFailedMessage: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_SaveRelationshipFailedMessage;E=js}",
		removeRelationshipFailedMessage: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_RemoveRelationshipFailedMessage;E=js}",
		addNewRelationshipButtonCaption: '@{R=DPA.Strings; K=IntegrationWizard_RegisterNewInstanceButtonText; E=js}',
		findNewRelationshipsButtonLabel: '@{R=DPA.Strings;K=IntegrationWizard_FindNewRelationshipsButtonLabel;E=js}',
		pagingToolbarDisplayMsg: '@{R=DPA.Strings;K=IntegrationWizard_PagingToolbarDisplayMsg;E=js}',
		pagingToolbarRefreshTooltip: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_PagingToolbarRefreshTooltip;E=js}',
		defineRelationshipTooltipText: "@{R=DPA.Strings;K=IntegrationWizard_Grid_ActionsTooltip_DefineRelationship;E=js}",
		unlinkRelationshipTooltipText: '@{R=DPA.Strings; K=IntegrationWizard_UnlinkRelationship; E=js}',
		editRelationshipTooltipText: "@{R=DPA.Strings;K=IntegrationWizard_Grid_ActionsTooltip_EditRelationship;E=js}",
		noRelationshipText: "@{R=DPA.Strings;K=IntegrationWizard_TypesOfRelationships_Unknown;E=js}",
		updatingRelationshipsTitle: "@{R=DPA.Strings;K=IntegrationWizard_UpdateRelationshipsProgressDialogTitle;E=js}",
		updatingRelationshipsMessage: "@{R=DPA.Strings;K=IntegrationWizard_UpdateRelationshipsProgressDialogMessage;E=js}"
    },
    
    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },

    showFindNewRelationships: function () {
        var me = this;
        Ext.Msg.show({
            title: me.localizedTexts.findNewRelationshipsModalTitle,
            msg: me.localizedTexts.findNewRelationshipsModalMessage,
            buttons: Ext.Msg.OKCANCEL,
            fn: function (res) {
                if (res == 'ok') {
                    SW.Core.Services.callControllerActionSync("/sapi/DpaRelationshipsGrid", "ClearRelationships", me.getBaseParams(), function (onsuccess) { }, function (onerror) { });
                    me.internalGridRefresh();
                }
            }
        });
    },

    constructor: function (isNodeFunctionalityAllowed, connectivityIssueWarningClientId) {
	    var me = this;

	    var config = {
	        showNoficationBar: true,
	        onPagingToolbarRefresh: function () {
	            me.showFindNewRelationships();
                // return true to stop execution of doRefresh
	            return true;
	        }
	    };

	    this.relationshipConfigurationModal = null;
        this.isNodeFunctionalityAllowed = isNodeFunctionalityAllowed;
        this.connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning(connectivityIssueWarningClientId);

	    this.renderActions = function(value, meta, record) {
	        var msg;
	        if (record.data.NodeID != null) {
	            msg = SW.Core.String.Format("<div class='cellActions'><a href='#' data-monitored-database-id='{0}' class='DPA_RelationshipDialog'><img ext:qtitle='{1}' ext:qtip=' ' src='/Orion/DPA/images/edit_16x16.png' /></a>"
	                + "<a href=\"#\" data-monitored-database-id='{0}' class='DPA_RemoveRelationship'><img class='DPA_ActionButton' ext:qtitle='{2}' ext:qtip=' ' src='/Orion/DPA/images/relationship-unlink.png' /></a></div>",
	                record.data.GlobalDatabaseInstanceId,
	                Ext.util.Format.htmlEncode(me.localizedTexts.editRelationshipTooltipText),
	                Ext.util.Format.htmlEncode(me.localizedTexts.unlinkRelationshipTooltipText));
	        } else {
	            msg = SW.Core.String.Format("<div class='cellActions'><a href='#' data-monitored-database-id='{0}' class='DPA_RelationshipDialog'><img ext:qtitle='{1}' ext:qtip=' ' src='/Orion/DPA/images/relationship-add.png' /></a></div>",
	                record.data.GlobalDatabaseInstanceId,
	                Ext.util.Format.htmlEncode(me.localizedTexts.defineRelationshipTooltipText));
	        }

	        return msg;
	    };

	    this.renderNode = function(value, meta, record) {
	        if (record.data.NodeID == null) {
	            meta.css = "DPA_NodeCellWithNoRelationship";
	            return record.data.NodeRelationshipLabel;
	        }

	        return me.renderEntityNameWithIcon(record.data.NodeInstanceType, record.data.NodeName, record.data.NodeStatus, record.data.NodeDetailsUrl, record.data.NodeID);
	    };

	    this.renderApplication = function (value, meta, record) {
	        if (record.data.ApplicationId == null) {
	            meta.css = "DPA_NodeCellWithNoRelationship";
	            return record.data.ApplicationRelationshipLabel;
	        }

	        return me.renderEntityNameWithIcon(record.data.ApplicationInstanceType, record.data.ApplicationName, record.data.ApplicationStatus, record.data.ApplicationDetailsUrl, record.data.ApplicationId);
	    }

	    this.renderOrionObject = function (value, meta, record) {
	        if (record.data.NodeID == null) {
	            meta.css = "DPA_NodeCellWithNoRelationship";
	            return record.data.NodeRelationshipLabel;
	        }

	        if (record.data.ApplicationId != null) {
	            return me.renderEntityNameWithIcon(record.data.ApplicationInstanceType, record.data.ApplicationName, record.data.ApplicationStatus, record.data.ApplicationDetailsUrl, record.data.ApplicationId);
	        } else {
	            return me.renderEntityNameWithIcon(record.data.NodeInstanceType, record.data.NodeName, record.data.NodeStatus, record.data.NodeDetailsUrl, record.data.NodeID);
	        }
	    }

	    this.renderDpaServerName = function (value, meta, record) {
	        return me.renderEntityNameWithIcon(record.data.DpaServerInstanceType, record.data.DpaServerName, record.data.DpaServerStatus, record.data.DpaServerDetailsUrl, record.data.DpaServerId);
        };

        this.renderInstanceName = function (value, meta, record) {
            var msg = _.template("<span ext:qtitle='{{{MonitorStatusText}}}' ext:qtip=' ' class='DPA_MonitorStatus DPA_MonitorStatus-{{MonitorStatus}}'>{{InstanceName}}</span>", record.data);
	        return msg;
	    };

        this.removeRelationship = function (instanceId) {
	        Ext.Msg.show({
	            title: me.localizedTexts.removeRelationshipModalTitle,
	            msg: me.localizedTexts.removeRelationshipModalMessage,
	            buttons: Ext.Msg.YESNO,
	            fn: function (res) {
	                if (res == 'yes') {
	                    var params = { instanceId: instanceId, modelId: me.getBaseParams().ModelID };
	                    me.simpleProgressModal.show(me.localizedTexts.updatingRelationshipsTitle, me.localizedTexts.updatingRelationshipsMessage);
	                    SW.Core.Services.callControllerAction("/sapi/DpaRelationshipsGrid", "RemoveRelationship", params,
                            function (onsuccess) {
                                me.simpleProgressModal.close();
                                me.internalGridRefresh();
                            },
                            function (onerror) {
                                me.simpleProgressModal.close();
                                me.displayErrorMessage(me.localizedTexts.removeRelationshipFailedMessage);
                                me.internalGridRefresh();
                            });
	                }
	            }
	        });
	    },

        this.saveRelationship = function (dataToSave) {
            me.simpleProgressModal.show(me.localizedTexts.updatingRelationshipsTitle, me.localizedTexts.updatingRelationshipsMessage);
            SW.Core.Services.callControllerAction("/sapi/DpaRelationshipsGrid", "SaveRelationship", $.extend({}, me.getBaseParams(), dataToSave),
                function (onsuccess) {
                    me.relationshipConfigurationModal.close();
                    me.simpleProgressModal.close();
                    me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
                },
                function (onerror) {
                    me.relationshipConfigurationModal.close();
                    me.simpleProgressModal.close();
                    me.displayErrorMessage(me.localizedTexts.saveRelationshipFailedMessage);
                });

        },

        this.closeModal = function() {
            me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
        },

        this.displayErrorMessage = function(message) {
            me.getMainGrid().getView().emptyText = message;// TODO: should be displayed in error message frame introduced in FB390434
            me.getMainGrid().store.removeAll();
        },

	    SW.DPA.Admin.ManageRelationships.RelationshipsGrid.superclass.constructor.call(this, config);
	},

	init: function () {
	    var me = this;
	    SW.DPA.Admin.ManageRelationships.RelationshipsGrid.superclass.init.call(me);

	    this.relationshipConfigurationModal = new SW.DPA.Admin.RelationshipConfigurationModal('DPA_RelationshipConfigurationModal');
	    this.applicationsWithoutRelationshipModal = new SW.DPA.Admin.ApplicationsWithoutRelationshipModal('DPA_ApplicationsWithoutRelationshipModal');
	    this.simpleProgressModal = new SW.DPA.Admin.SimpleProgressModal();
	},

	onMainGridStoreLoaded: function () {
	    var me = this;
	    SW.DPA.Admin.ManageRelationships.RelationshipsGrid.superclass.onMainGridStoreLoaded.call(me);

	    // bind click events to button elements when grid is rendered
	    $('.DPA_RelationshipDialog').click(function () {
	        var scope = $(this),
                id = scope.data('monitored-database-id'),
                data = me.getDataById(id),
                nodeValues = data && data["AmbiguousNodes"] || (scope.data('node-ids') || '').split(','),
                appValues = data && data["AmbiguousApps"] || (scope.data('apps-ids') || '').split(','),
                nodes = $.grep(nodeValues, function (n) { return !!n; }),
                apps = $.grep(appValues, function (n) { return !!n; });

            
	        var filterNodes = { field: "NodeID", data: { type: "System.Int32", value: nodes.join(','), comparison: 'in' } };
	        var filterApps = { field: "ApplicationID", data: { type: "System.Int32", value: apps.join(','), comparison: 'in' } };

	        me.relationshipConfigurationModal.show({
	            selectedData: $.extend({}, data, { InstanceID: data.GlobalDatabaseInstanceId, ApplicationID: data.ApplicationId ? data.ApplicationId : data.PersistedApplicationId }),
	            nodeConfig: { filter: nodes.length > 0 ? filterNodes : null },
	            applicationConfig: { filter: apps.length > 0 ? filterApps : null },
	            showApplicationSelector: (apps.length > 0)
	        }, me.saveRelationship, me.closeModal);
	        if (!me.isNodeFunctionalityAllowed) {
	            me.relationshipConfigurationModal.hideAddNodeButton();
	        }
	        return false;
	    });
	    
	    $('.DPA_RemoveRelationship').click(function () {
	        var id = $(this).data('monitored-database-id');
	        me.removeRelationship(id);
	        return false;
	    });

	    $('.DPA_BbarAppsWithoutRelLink').off('click.dpa').on('click.dpa', function () {
	        me.applicationsWithoutRelationshipModal.show({ modelId: me.getBaseParams().ModelID });
	        return false;
	    });
	},

	createMainGriDataStore: function () {
	    function callbackFn() {
	        var grid = this;
	        return function(json) {
	            grid.metadata = json.Metadata;
	            var hideableNotificationBar = Ext.getCmp('mainGridNotificationBottomBar');
	            if (grid.metadata.CountOfApplicationsWithoutRelationship > 0) {
	                hideableNotificationBar.showMessage(grid.metadata.CountOfApplicationsWithoutRelationship);
	                hideableNotificationBar.show();
	            } else {
	                hideableNotificationBar.hide();
	            }

	            grid.getMainGrid().doLayout();
	        };
	    }

	    return SW.DPA.Admin.ManageRelationships.RelationshipsGrid.superclass.createDataGridStore(
            "/sapi/DpaRelationshipsGrid/GetRelationshipsTable",
	        this.dataStoreMapping,
            "InstanceName",
            this.connectivityIssueWarning,
	        callbackFn.apply(this));
	},

    createShowOnlyDataStore: function() {
        return new ORION.WebApiStore(
                "/sapi/DpaRelationshipsGrid/GetShowOnlyGroupingValues",
                [
                    { name: 'Caption', mapping: 0},
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                    { name: 'Value', mapping: 3 }
                ]);
    },

    createGroupingDataStore: function() {
        return new ORION.WebApiStore(
                "/sapi/DpaRelationshipsGrid/GetRelationshipGroupingValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);
    },

    createGroupingGridDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaRelationshipsGrid/GetRelationshipGroupValues",
                [
                    { name: 'Id', mapping: 0 },
                    { name: 'Value', mapping: 1 },
                    { name: 'Cnt', mapping: 2 },
                    { name: 'SelectedCnt', mapping: 3 }
                ]);
    },

    getDataById: function(id) {
        var index = this.getMainGrid().store.findBy(function(record) {
            return (record.get('GlobalDatabaseInstanceId') == id);
        });

        return (index >= 0 ? this.getMainGrid().store.getAt(index).data : undefined);
    },

    getGridToolbarConfig: function () {
        var me = this;
        return [
            {
                id: 'RegisterNewInstanceButton2',
                text: this.localizedTexts.addNewRelationshipButtonCaption,
                icon: '/Orion/DPA/images/add_16x16.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    return SW.DPA.LinksToDpaSelector.show('/api/DpaLinksToDpaj/RegisterDbInstance/');
                }
            },
            '-',
            {
                id: 'FindNewRelationships',
                text: me.localizedTexts.findNewRelationshipsButtonLabel,
                icon: '/Orion/DPA/images/find-new-relationships.gif',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    me.showFindNewRelationships();
                    return false;
                }
            }
        ];
    },

    onSelectionChanged: function (grid, webStore) {
        var me = this;
        SW.DPA.Admin.ManageRelationships.RelationshipsGrid.superclass.onSelectionChanged.call(me, grid, webStore);
    },

    getRecordId: function (data) {
        return data.GlobalDatabaseInstanceId;
    },

    getEnvelopElementId: function () {
        return "integrationGrid";
    },

    getPlaceholderElementId: function () {
        return "relationshipsPlaceholder";
    },

    getMainGridId: function () {
        return "relationshipsGrid";
    }
});