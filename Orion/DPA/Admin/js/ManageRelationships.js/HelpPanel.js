﻿SW.Core.namespace("SW.DPA.Admin.ManageRelationships");
SW.DPA.Admin.ManageRelationships.HelpPanel = function(isVisible) {
    var helpBar = $('.DPA_ManageRelationshipsContent .DPA_MainContent .DPA_Sidebar');
    var grid = $('.DPA_ManageRelationshipsContent .DPA_MainContent .DPA_Grid');

    var closeHintBlock = helpBar.find('.DPA_CloseHelpPanelButton');

    var helpLinkTemplate = _.templateExt(['<span class="DPA_HintHelpLinkWrapper"><span class="sw-suggestion sw-suggestion-info DPA_HintHelpLink" style="display: none">',
                    '<span class="sw-suggestion-icon"></span><a href="#">{{{linkLabel}}}</a>',
                    '</span></span>']);

    var hintHelpLink = $('.DPA_ManageRelationshipsContent .DPA_MainContent .DPA_HintHelpLink');
    if (hintHelpLink.length == 0) {
        var linkLabel = helpBar.find('#DPA_HintTitleSummary').text();
        $('#DPA_Description').append(helpLinkTemplate({ linkLabel: linkLabel }));
        hintHelpLink = $('.DPA_ManageRelationshipsContent .DPA_MainContent .DPA_HintHelpLink');
    }

    var hideSidebar = function () {
        helpBar.hide();
        grid.css('width', '100%');
        hintHelpLink.show();
        $(window).trigger('resize');
    };

    var showSidebar = function() {
        grid.css('width', '75%');
        helpBar.show();
        hintHelpLink.hide();
        $(window).trigger('resize');
    };
    
    var saveVisibilitySettings = function(visible) {
        SW.Core.Services.callController('/api/DpaIntegrationHelpPanelVisibility/SaveSettings', visible,
            function () { },
            function () { });
    }

    closeHintBlock.click(function () {
        saveVisibilitySettings(false);
        hideSidebar();
    });

    hintHelpLink.click(function () {
        saveVisibilitySettings(true);
        showSidebar();
    });

    if (!isVisible) {
        hideSidebar();
    }
};
