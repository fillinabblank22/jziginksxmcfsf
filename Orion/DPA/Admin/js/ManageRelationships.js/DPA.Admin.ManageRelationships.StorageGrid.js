﻿Ext.namespace('SW.DPA.Admin.ManageRelationships');

SW.DPA.Admin.ManageRelationships.StorageGrid = Ext.extend(SW.DPA.Admin.ManageRelationships.Grid, {
    localizedTexts: {
        storagesCountFormat: "@{R=DPA.Strings;K=ManageRelationships_StorageCountLabel;E=js}",
        storageUsedByTooltipHeard: '@{R=DPA.Strings;K=ManageRelationships_StorageCountTooltipTitle;E=js}',
        customRelationshipLabel: "@{R=DPA.Strings;K=ManageRelationships_CustomRelationshipLabel;E=js}",
        defineNewRelationshipButtonLabel: '@{R=DPA.Strings;K=ManageRelationships_StorageGrid_DefineNewRelationshipButton_Label;E=js}',
        removeRelationshipTooltipText: '@{R=DPA.Strings;K=StorageRelationshipManagement_RemoveButtonTooltipText;E=js}',
        removeRelationshipModalTitle: '@{R=DPA.Strings;K=StorageRelationshipManagement_RemoveRelationshipModalWindowTitle;E=js}',
        removeRelationshipModalMessage: '@{R=DPA.Strings;K=StorageRelationshipManagement_RemoveRelationshipModalWindowMessage;E=js}',
        editRelationshipTooltipText: 'Edit this relationship'
    },
    globalDatabaseInstanceIdsToLun: {},
    constructor: function (connectivityIssueWarningClientId) {
        var me = this;

        var config = {
        };

        me.storageConfigurationModal = null;
        this.connectivityIssueWarning = new SW.DPA.UI.ConnectivityIssueWarning(connectivityIssueWarningClientId);

        this.renderActions = function (value, meta, record) {
            var content = SW.Core.String.Format("<div class='cellActions'><a href='#' data-globaldatabaseinstanceid='{0}' class='DPA_EditRelationship'><img ext:qtitle='{1}' ext:qtip=' ' src='/Orion/DPA/images/edit_16x16.png' /></a>" +
                "<a href=\"#\" data-globaldatabaseinstanceid='{0}' class='DPA_RemoveRelationship'><img class='DPA_ActionButton' ext:qtitle='{2}' ext:qtip=' ' src='/Orion/DPA/images/relationship-remove.png' /></a></div>",
                record.data.GlobalDatabaseInstanceId,
                Ext.util.Format.htmlEncode(me.localizedTexts.editRelationshipTooltipText),
                Ext.util.Format.htmlEncode(me.localizedTexts.removeRelationshipTooltipText));

            return content;
        }

        this.renderRelationshipType = function (value, meta, record) {
            meta.css = "DPA_RelationshipStatusCell";
            return SW.Core.String.Format("<div><img src='/Orion/DPA/images/relationship-custom.png' ext:qtitle='{0}' ext:qtip=' ' /></div>{0}",
                me.localizedTexts.customRelationshipLabel);
        }

        this.renderDatabaseInstanceName = function (value, meta, record) {
            return me.renderEntityNameWithIcon(record.data.DatabaseInstanceInstanceType, record.data.DatabaseInstanceName, record.data.DatabaseInstanceStatus, record.data.DatabaseInstanceDetailsUrl);
        }

        this.renderStoragesCount = function (value, meta, record) {
            var text = SW.Core.String.Format(me.localizedTexts.storagesCountFormat, record.data.StoragesCount);
            var toolTipHeader = SW.Core.String.Format(me.localizedTexts.storageUsedByTooltipHeard, record.data.DatabaseInstanceName, record.data.StoragesCount);

            var toolTipBody = "";
            if (me.globalDatabaseInstanceIdsToLun[record.data.GlobalDatabaseInstanceId]) {
                var instances = me.globalDatabaseInstanceIdsToLun[record.data.GlobalDatabaseInstanceId];

                var list = $('<ul/>');
                $.each(instances, function (index, item) {
                    list.append($('<li/>').text(item.Name));
                });
                toolTipBody = SW.Core.String.Format("<ul>{0}</ul>", list.html());
            }

            var cellValue = SW.Core.String.Format("<span ext:qtitle='{1}' ext:qtip='{2}' class='DPA_Dotted_Underline'>{0}</span>", text, toolTipHeader, Ext.util.Format.htmlEncode(toolTipBody));

            return cellValue;
        }

        this.saveRelationships = function(dataToSave) {
            SW.Core.Services.callControllerAction("/sapi/DpaStorageGrid",
                "SaveStorageRelationship",
                dataToSave,
                function (response) {
                    me.storageConfigurationModal.close();
                    me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
                },
                function (response) {
                    me.storageConfigurationModal.close();
                    me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);
                });
        }

        this.removeRelationship = function(globalDatabaseInstanceId) {
            Ext.Msg.show({
                title: me.localizedTexts.removeRelationshipModalTitle,
                msg: me.localizedTexts.removeRelationshipModalMessage,
                buttons: Ext.Msg.YESNO,
                fn: function (res) {
                    if (res === 'yes') {
                        var params = { GlobalDatabaseInstanceId: globalDatabaseInstanceId };
                        SW.Core.Services.callControllerAction("/sapi/DpaStorageGrid", "RemoveRelationship", params,
                            function (onsuccess) {
                                me.internalGridRefresh();
                            },
                            function (onerror) {
                                me.internalGridRefresh();
                            });
                    }
                }
            });
        };

        SW.DPA.Admin.ManageRelationships.StorageGrid.superclass.constructor.call(this, config);
    },

    init: function () {
        var me = this;
        SW.DPA.Admin.ManageRelationships.StorageGrid.superclass.init.call(me);

        me.storageConfigurationModal = new SW.DPA.Admin.StorageConfigurationModal("DPA_StorageConfigurationModal");
    },

    onMainGridStoreLoaded: function () {
        var me = this;
        SW.DPA.Admin.ManageRelationships.StorageGrid.superclass.onMainGridStoreLoaded.call(me);
        
        $('.DPA_RemoveRelationship').click(function () {
            var id = $(this).data('globaldatabaseinstanceid');
            me.removeRelationship(id);
            return false;
        });

        $('.DPA_EditRelationship').click(function() {
            var globalDatabaseInstanceId = $(this).data('globaldatabaseinstanceid');
            var selectedLuns = [];
            if (me.globalDatabaseInstanceIdsToLun[globalDatabaseInstanceId]) {
                $.each(me.globalDatabaseInstanceIdsToLun[globalDatabaseInstanceId], function (index, item) {
                    selectedLuns.push({ LunId: item.LunId, LunName: item.Name });
                });
            }

            var selectedDatabaseInstance = _.find(me.getMainGrid().store.data.items,
                    function(item) {
                        return item.data.GlobalDatabaseInstanceId == globalDatabaseInstanceId;
                    }).data;
            var config = {
                selectedDatabaseInstances: [{
                    InstanceID: selectedDatabaseInstance.GlobalDatabaseInstanceId,
                    InstanceName: selectedDatabaseInstance.DatabaseInstanceName
                }],
                selectedLuns: selectedLuns
            };

            me.storageConfigurationModal.show(config, me.saveRelationships);
            return false;
        });
    },
    createMainGriDataStore: function () {
        var me = this;
        return SW.DPA.Admin.ManageRelationships.StorageGrid.superclass.createDataGridStore("/sapi/DpaStorageGrid/GetStorageTable",
	        this.dataStoreMapping, "DatabaseInstanceName", this.connectivityIssueWarning,
            function(response) {
                me.globalDatabaseInstanceIdsToLun = response.Metadata.GlobalDatabaseInstanceIdsToLun;
            });
    },

    createGroupingDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaStorageGrid/GetStorageGroupingValues",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Type', mapping: 2 }
                ]);
    },

    createGroupingGridDataStore: function () {
        return new ORION.WebApiStore(
                "/sapi/DpaStorageGrid/GetStorageGroupValues",
                [
                    { name: 'Id', mapping: 0 },
                    { name: 'Value', mapping: 1 },
                    { name: 'Cnt', mapping: 2 },
                    { name: 'SelectedCnt', mapping: 3 }
                ]);
    },

    getGridToolbarConfig: function () {
        var me = this;
        return [
            {
                id: 'DefineNewRelationshipButton',
                text: me.localizedTexts.defineNewRelationshipButtonLabel,
                icon: '/Orion/DPA/images/add_16x16.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    me.storageConfigurationModal.show({}, me.saveRelationships);
                    return false;
                }
            }
        ];
    },

    getRecordId: function (data) {
        return data.ApplicationID;
    },

    getEnvelopElementId: function () {
        return "integrationGrid";
    },

    getPlaceholderElementId: function () {
        return "relationshipsPlaceholder";
    },

    getMainGridId: function () {
        return "relationshipsGrid";
    }
});