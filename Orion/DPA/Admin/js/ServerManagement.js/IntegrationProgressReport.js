﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.IntegrationProgressReport = function () {
    var localizedStrings = {
        revertingChanges: '@{R=DPA.Strings;K=EstablishingIntegration_RevertingChanges;E=js}',
        initialProgress: "@{R=DPA.Strings; K=EstablishingIntegration_Progress_Initial; E=js}"
    };

    var dialog = $("#DPA_EstablishingIntegrationModal");
    var progress = dialog.find(".DPA_Progress");
    var progressTitle = dialog.find(".DPA_ProgressTitle");
    var progressBarDone = dialog.find(".DPA_ProgressBarDone");
    var progressMessage = dialog.find(".DPA_ProgressMessage");

    progressTitle.text(localizedStrings.initialProgress);

    var currentCursor = 0;

    this.reportProgress = function(cursor, message, animationFinishedCallback) {
        if (currentCursor > cursor)
            progressTitle.text(localizedStrings.revertingChanges);
        currentCursor = cursor;
        progressMessage.text(message);

        progressBarDone.animate({ width: cursor + '%' }, 500, function() {
            if (SW.DPA.Common.isFn(animationFinishedCallback))
                    animationFinishedCallback();
            });
    };

    this.start = function() {
        currentCursor = 0;
        progressBarDone.width("0%");
        progressTitle.text(localizedStrings.initialProgress);
        progressMessage.text("").show();
        progress.show();
    };

    this.hide = function() {
        progress.hide();
    }
}