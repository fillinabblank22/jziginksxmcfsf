﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.IntegrationSetupModalUiManager = function () {
    var errorDiv = $("#DPA_IntegrationErrorMessageWrapper");
    var errorMessage = $("#DPA_IntegrationErrorMessage");
    var successDiv = $("#dpaIntegrationSuccessMessage");
    var helpLink = $("#DPA_HowCanIFixThisLink");
    var compatibilityLink = $("#DPA_CheckVersionCompatibilityLink");
    var mainSwisHostname = $("#DPA_OrionHostnameWrapper .DPA_OrionHostname :hidden").val();
    var useDefaultOrionHostname = $("#DPA_OrionHostnameWrapper .DPA_Checkbox :checkbox");
    var orionHostnameField = $("#DPA_OrionHostnameWrapper :text");
    var orionHostnameErrorMessage = $("#DPA_OrionHostnameErrorWrapper");
    var addressField = $(".DPA_ServerAddress :text");
    var defaultPort = $(".DPA_Port :hidden").val();
    var portField = $(".DPA_Port :text");
    var nameField = $(".DPA_DisplayName :text");
    var defaultUserName = $(".DPA_UserName :hidden").val();
    var userNameField = $(".DPA_UserName :text");
    var passwordField = $(".DPA_Password :password");
    var testStatus = $(".DPA_TestConnectionStatus");
    var testErrorDiv = $("#DPA_TestConnectionErrorMessageWrapper");
    var testCompatibilityLink = $("#DPA_TestConnectionCompatibilityLink");

    this.highlightedFieldClassName = "DPA_HighlightedField";

    this.resetUi = function () {
        testStatus.hide();
        testErrorDiv.hide();
        testCompatibilityLink.hide();
        errorDiv.hide();
        successDiv.hide();
        helpLink.hide();
        compatibilityLink.hide();
        $(".sw-validation-error").hide();
        $("." + this.highlightedFieldClassName).removeClass(this.highlightedFieldClassName);
    };

    this.clearFields = function () {
        addressField.val("");
        portField.val(defaultPort);
        nameField.val("");
        userNameField.val(defaultUserName);
        passwordField.val("");
        orionHostnameErrorMessage.hide();
        useDefaultOrionHostname.prop("checked", true).trigger("change");
    };
    
    useDefaultOrionHostname.off("change").change(function () {
        orionHostnameField.prop("disabled", $(this).is(":checked")).text();

        if ($(this).is(":checked")) {
            orionHostnameField.val(mainSwisHostname);
        }
    });

    this.showError = function (error) {
        errorDiv.show();
        errorMessage.text(error);
    };

    this.showSuccess = function () {
        successDiv.show();
    };

    this.showCompatibilityLink = function () {
        compatibilityLink.show();
    };

    this.showHelpLink = function () {
        helpLink.show();
    };

    this.enableOrionHostnameField = function () {
        useDefaultOrionHostname.prop("checked", false).trigger("change");
    };

    this.showOrionHostnameErrorMessage = function () {
        orionHostnameErrorMessage.show();
    };

    this.disableOrionHostnameField = function () {
        useDefaultOrionHostname.prop("checked", true).trigger("change");
    };

    this.hideOrionHostnameErrorMessage = function () {
        orionHostnameErrorMessage.hide();
    };
}