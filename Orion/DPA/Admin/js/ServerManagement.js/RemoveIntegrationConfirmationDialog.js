﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.RemoveIntegrationConfirmationDialog = function () {
    var localizedStrings = {
        title: '@{R=DPA.Strings;K=IntegrationSetup_DisconnectIntegrationConfirmDialogTitle;E=js}',
        message: '@{R=DPA.Strings;K=IntegrationSetup_DisconnectIntegrationConfirmDialogMessage;E=js}',
        progressMessageFormat: '@{R=DPA.Strings;K=RemoveIntegrationDialog_ProgressMessageFormat;E=js}',
        btnYes: '@{R=DPA.Strings;K=RemoveIntegrationDialog_Yes;E=js}',
        btnNo: '@{R=DPA.Strings;K=RemoveIntegrationDialog_No;E=js}'
    };

    var dialogTemplate = _.templateExt([
         '<div id="DPA_RemoveIntegrationDialog" automation=Wrapper_RemoveIntegrationDialog>',
            '<div class="DPA_Content">',
                 '<div class="DPA_StandardMessage">',
                     '<span class="DPA_Icon"></span>',
                     '<div class="DPA_Texts">',
                         '<p>{{message}}</p>',
                     '</div>',
                 '</div>',
                 '<p class="DPA_ProgressMessage" automation="Wrapper_RemoveIntegrationProgressBar" style="display:none"><img src="/Orion/images/loading_gen_small.gif" /> {{{progressMessage}}}</p>',
             '</div>',
             '<div class="DPA_BottomBar"></div>',
         '</div>']);

    this.show = function (dpaServerId, dpaServerName, removeSuccessCallback) {
        var dialogOptions = {
            width: '500',
            height: 'auto',
            padding: 0,
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedStrings.title,
            resizable: false,
            closeOnEscape: false,
            open: function () {
                $(".ui-dialog[aria-labelledby='ui-dialog-title-DPA_RemoveIntegrationDialog']  .ui-dialog-titlebar-close").attr("automation", "Btn_CloseRemoveIntegrationDialog");
            }
        };

        var viewModel = $.extend({}, localizedStrings, {
            progressMessage: SW.Core.String.Format(localizedStrings.progressMessageFormat, dpaServerName)
        });

        var window = $(dialogTemplate(viewModel));

        window.find('.DPA_BottomBar').append($(SW.Core.Widgets.Button(localizedStrings.btnYes, { type: 'primary' })).click(function () {
            $('.ui-dialog-titlebar-close').hide();
            window.find('.DPA_BottomBar').hide();
            window.find('.DPA_StandardMessage').hide();
            window.find('.DPA_ProgressMessage').show();

            var removeIntegration = new SW.DPA.Admin.ServerManagement.RemoveIntegration();
            removeIntegration.removeIntegration(dpaServerId, function () {
                removeSuccessCallback();
                window.dialog('close');
            }, function() {
                window.dialog('close');
            });
        }));

        window.find('.DPA_BottomBar').append($(SW.Core.Widgets.Button(localizedStrings.btnNo, { type: 'secondary' })).click(function () {
            window.dialog('close');
        }));

        window.dialog(dialogOptions);
        
        return false;
    };
};