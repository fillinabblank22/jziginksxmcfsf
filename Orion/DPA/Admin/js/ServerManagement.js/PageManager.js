﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.PageManager = function () {
    var errorElement = $(".DPA_ErrorWrapper");
    var errorMessageElement = errorElement.find('.DPA_ErrorMessage');

    this.showError = function (errorMessage) {
        errorElement.show();
        errorMessageElement.text(errorMessage);
    };

    this.hideError = function() {
        errorElement.hide();
        errorMessageElement.text('');
    };
}; 