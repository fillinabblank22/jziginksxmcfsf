﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.HelpDialog = function (title, htmlContent) {
    var localizedStrings = {
        closeButtonLabel: '@{R=DPA.Strings;K=InfoBox_Close;E=js}'
    };

    var adviceInfoTemplate = _.templateExt([
     '<div id="DPA_InfoModal">',
         '<div id="infotext">',
             '<div>{{htmlContent}}</div>',
         '</div>',
         '<div id="buttons"><br/></div>',
     '</div>']);

    var viewModel = {
        title: title,
        htmlContent: htmlContent
    };

    var dialog = $(adviceInfoTemplate(viewModel));


    dialog.find('#buttons').append($(SW.Core.Widgets.Button(localizedStrings.closeButtonLabel,
        { type: 'secondary' })).click(function () { dialog.dialog('close').remove(); }));

    dialog.dialog({ modal: true, minWidth: 600, minHeight: 100, resizable: false, title: title });
};