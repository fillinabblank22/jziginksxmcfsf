﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement.Validators");

(function(ctx) {
    ctx.validateIpHostname = function(value) {
        var isValid = SW.DPA.Common.isIPv4(value) || SW.DPA.Common.isIPv6(value) || SW.DPA.Common.isHostname(value);
        return isValid;
    };
})(SW.DPA.Admin.ServerManagement.Validators);