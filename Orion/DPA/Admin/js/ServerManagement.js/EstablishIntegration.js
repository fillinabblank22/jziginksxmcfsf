﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.EstablishIntegration = function (config) {
    var uiManager = new SW.DPA.Admin.ServerManagement.IntegrationSetupModalUiManager();

    var taskId;

    var establishingIntegrationModal = new SW.DPA.Admin.EstablishingIntegrationModal("DPA_EstablishingIntegrationModal");
    var integrationProgressReport = new SW.DPA.Admin.ServerManagement.IntegrationProgressReport();

    $("#DPA_EstablishingIntegrationModal .DPA_CloseDialogButton").click(function () {
        establishingIntegrationModal.close();
        config.integrationSetupModal.show();
	    return false;
    });

    var showUnrecoverableError = function (error) {
        integrationProgressReport.hide();
        establishingIntegrationModal.hideCancelButton();
        establishingIntegrationModal.showUnrecoverableError(error);
    }

    var startTime;
    var processResult = function (result) {
        if (result.Result === SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.InProgress) {
            integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName);
            setTimeout(isConfigurationFinished, 500 - (Date.now() - startTime));
            return;
        }

        establishingIntegrationModal.disableCancelButton();

        if (result.Result === SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.Success) {
            integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
                uiManager.showSuccess();
                integrationProgressReport.hide();
                establishingIntegrationModal.hideCancelButton();
                establishingIntegrationModal.success(result.ResultMessage);
            });
            return;
        }

        if (result.Result === SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.CancelledByRequest) {
            integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
                establishingIntegrationModal.close();
                config.integrationSetupModal.show();
            });
            return;
        }

        if (!result.ResultMessage) {
            integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
                showUnrecoverableError();
            });
            return;
        }

        switch (result.Result) {
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.GeneralError:
                integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
                    showUnrecoverableError();
                });
                return;
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.SwisFailure:
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.EnableFederationFailed:
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.ErrorSavingSettings:
                integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
                    showUnrecoverableError(result.ResultMessage);
                });
                return;
        }

        integrationProgressReport.reportProgress(result.ProgressCursor, result.PhaseName, function () {
            establishingIntegrationModal.close();
            config.integrationSetupModal.show();
        });

        var unableToPingBack = false;
        var showHowCanIFixThisHelpLink = false;
        var showCheckVersionCompatibilityHelpLink = false;


        unableToPingBack = result.Result === SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.UnableToPingBack;

        if (!unableToPingBack)
            uiManager.showError(result.ResultMessage);

        switch (result.Result) {
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.NotResponding:
                config.serverAddress.addClass(uiManager.highlightedFieldClassName);
                config.port.addClass(uiManager.highlightedFieldClassName);
                break;
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.InvalidCredentials:
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.InsufficientPrivileges:
                config.username.addClass(uiManager.highlightedFieldClassName);
                config.password.addClass(uiManager.highlightedFieldClassName);
                break;
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.UnableToPingBack:
                config.orionServerAddress.addClass(uiManager.highlightedFieldClassName);
                break;
            case SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.UnsupportedVersion:
                showCheckVersionCompatibilityHelpLink = true;
                break;
        }

        if (result.Result >= SW.DPA.Admin.ServerManagement.ConfigureIntegrationResult.DPAServerAlreadyIntegrated) {
            showHowCanIFixThisHelpLink = true;
            uiManager.showError(result.ResultMessage);
        }

        if (showCheckVersionCompatibilityHelpLink)
            uiManager.showCompatibilityLink();
        if (showHowCanIFixThisHelpLink)
            uiManager.showHelpLink();

        if (unableToPingBack) {
            uiManager.enableOrionHostnameField();
            uiManager.showOrionHostnameErrorMessage();
        } else {
            uiManager.hideOrionHostnameErrorMessage();
        }
    };

    var isConfigurationFinished = function () {
        startTime = Date.now();
        SW.Core.Services.callController("/api/DpaIntegrationSetup/GetIntegrationStatus",
            taskId,
            processResult,
            function () {
                //Uncaught exception - should not happen
                showUnrecoverableError();
            });
    };

    this.establishIntegration = function () {
        window.Page_ClientValidate();
        if (window.Page_IsValid) {
            uiManager.resetUi();
            integrationProgressReport.start();
            establishingIntegrationModal.show();
            config.integrationSetupModal.close();
            var requestData = {
                ServerAddress: config.serverAddress.val(),
                Port: config.port.val(),
                UserName: config.username.val(),
                Password: config.password.val(),
                DisplayName: config.displayName.val(),
                OrionServerAddress: config.orionServerAddress.val()
            }
            SW.Core.Services.callController(
                "/api/DpaIntegrationSetup/BeginIntegration",
                requestData,
                function (result) {
                    if (result.ValidationErrors) {
                        establishingIntegrationModal.close();
                        config.integrationSetupModal.show();
                        uiManager.showError(result.ValidationErrors);
                    } else {
                        taskId = result.TaskId;
                        establishingIntegrationModal.enableCancelButton();
                        isConfigurationFinished();
                    }
                },
                function () {
                    //Uncaught exception - should not happen
                    showUnrecoverableError();
                }
            );
        }
    };

    this.cancelIntegration = function () {
        establishingIntegrationModal.disableCancelButton();
        SW.Core.Services.callController("/api/DpaIntegrationSetup/CancelIntegrationInProgress",
            taskId,
            function (integrationCancelled) {
                if (!integrationCancelled)
                    establishingIntegrationModal.enableCancelButton();
            },
            function () {
                //Uncaught exception - should not happen, isConfigurationFinished will take care of it
                establishingIntegrationModal.enableCancelButton();
            });
    };
}