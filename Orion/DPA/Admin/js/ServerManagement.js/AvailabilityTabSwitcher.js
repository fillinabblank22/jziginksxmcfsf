﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.AvailabilityTabSwitcher = function (divWrapperElement) {
    var localizedStrings = {
        showDpaTabText: '@{R=DPA.Strings;K=ServerManagement_ShowDpaTab;E=js}'
    };
    divWrapperElement.html('<div class="dpaTabAvailability" style="display:none"><a class="toggleOn" automation="Btn_ToggleOn"></a><a class="toggleOff" automation="Btn_ToggleOff"></a><div class="DPA_spinner"></div><span></span></div>');

    var tabAvailabilityElement = $('.dpaTabAvailability');
    var dpaSpinner = $('.DPA_spinner');

    dpaSpinner.hide();
    tabAvailabilityElement.find('span').text(localizedStrings.showDpaTabText);

    var toggleOn = tabAvailabilityElement.find('a.toggleOn');
    var toggleOff = tabAvailabilityElement.find('a.toggleOff');

    toggleOn.click(function() {
        toggleOn.hide();
        toggleOff.show();
        saveSettings(false);
    });

    toggleOff.click(function() {
        toggleOff.hide();
        toggleOn.show();
        saveSettings(true);
    });

    this.reload = function() {
        getSettings(function(isVisible, isEnabled) {
            if (isVisible) {
                tabAvailabilityElement.show();
            } else {
                tabAvailabilityElement.hide();
            }

            if (isEnabled) {
                toggleOff.hide();
                toggleOn.show();
            } else {

                toggleOn.hide();
                toggleOff.show();
            }
        });
    };

    var saveSettings = function(enableDpaTab) {
        dpaSpinner.show();

        SW.Core.Services.callController(
            '/api/DpaServerManagement/SetDpaTabVisibility',
            { Visible: enableDpaTab },
            function() {
                dpaSpinner.hide();
                // Needed for Databases tab refresh
                location.reload();
            },
            function() {
                dpaSpinner.hide();
            });
    };

    var getSettings = function(processSettingCallback) {
        SW.Core.Services.callController(
            '/api/DpaServerManagement/GetDpaTabVisibility',
            {},
            function (result) {
                processSettingCallback(result.Visible, result.Enabled);
            },
            function() {
            });
    };

    this.reload();
};