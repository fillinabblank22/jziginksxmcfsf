﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.HelpPanel = function (isVisible) {
    var content = $(".DPA_ServerManagementContent");
    var showHelpLink = content.find(".DPA_ShowHelpLink");
    var hideHelpLink = content.find(".DPA_HideHelpLink");
    var helpPanel = $(".DPA_ServerManagement .DPA_SideBar");
    var closeButton = helpPanel.find('.DPA_CloseHelpPanelButton');

    var showHelpPanel = function() {
        content.width("75%");
        hideHelpLink.show();
        showHelpLink.hide();
        helpPanel.show();
    };

    var hideHelpPanel = function() {
        content.width("100%");
        showHelpLink.show();
        hideHelpLink.hide();
        helpPanel.hide();
    };

    if (isVisible) {
        showHelpPanel();
    } else {
        hideHelpPanel();
    }

    var saveHelpPanelVisibilitySettings = function(visible) {
        SW.Core.Services.callController('/api/DpaServerManagement/SaveHelpPanelVisibilitySettings', visible,
            function() {},
            function() {});
    };

    showHelpLink.on("click", function() {
        showHelpPanel();
        saveHelpPanelVisibilitySettings(true);
        return false;
    });

    hideHelpLink.on("click", function() {
        hideHelpPanel();
        saveHelpPanelVisibilitySettings(false);
        return false;
    });

    closeButton.on("click", function () {
        hideHelpPanel();
        saveHelpPanelVisibilitySettings(false);
        return false;
    });
};