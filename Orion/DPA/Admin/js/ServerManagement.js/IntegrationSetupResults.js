﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");

(function(context) {

    var baseConnectionResult = {
        Success: 0,
        InProgress: 1,
        CancelledByRequest: 2,
        GeneralError: 3
    };

    context.ConnectionTestResult = $.extend({}, baseConnectionResult, {
        InvalidCredentials: 4,
        InsufficientPrivileges: 5,
        NotResponding: 6,
        UnsupportedVersion: 7,
        SwisFailure: 8,
        UnableToPingBack: 9,
        AlreadyIntegrated: 10
    });

    context.TestIntegrationResult = $.extend({}, baseConnectionResult, {
        InvalidCredentials: 4,
        NotResponding: 5,
        DPANotIntegrated: 6,
        InsufficientPrivileges: 7,
        FederationFailure: 8
    });

    context.ConfigureIntegrationResult = $.extend({}, context.ConnectionTestResult, {
        DPAServerAlreadyIntegrated: 10,
        ErrorSavingSettings: 11,
        EnableFederationFailed: 12,
        SubscriptionAlreadyExists: 13,
        ConnectionToOrionFailed: 14,
        DiscoverNodesFailed: 15,
        DiscoverServerApplicationsFailed: 16,
        DiscoverClientApplicationsFailed: 17,
        SaveMappingsFailed: 18
    });

    context.DisableIntegrationResult = {
        Success: 0,
        GeneralError: 1,
        JSwisFailure: 2,
        ErrorSavingSettings: 3,
        IsNotIntegratedError: 4,
        InvalidCredentials: 5,
        NotResponding: 6,
        InsufficientPrivileges: 7,
        ClearMappingsFailed: 8
    };

})(SW.DPA.Admin.ServerManagement);