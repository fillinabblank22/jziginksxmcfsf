﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.RemoveIntegration = function () {

    var pageManager = new SW.DPA.Admin.ServerManagement.PageManager();

    this.removeIntegration = function (dpaServerId, successCallback, errorCallback) {
        SW.Core.Services.callController(
                "/api/DpaIntegrationSetup/RemoveIntegration",
                dpaServerId,
                function (result) {
                    if (result.DisableIntegrationResult === SW.DPA.Admin.ServerManagement.DisableIntegrationResult.Success || result.DisableIntegrationResult === SW.DPA.Admin.ServerManagement.DisableIntegrationResult.IsNotIntegratedError) {
                        pageManager.hideError();
                        successCallback();
                    } else {
                        pageManager.showError(result.ResultMessage);
                    }
                    
                },
                function (error) {
                    //Uncaught exception - should not happen
                    pageManager.showError(error);
                    errorCallback();
                }
            );
    };
}