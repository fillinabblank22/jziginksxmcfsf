﻿SW.Core.namespace("SW.DPA.Admin.ServerManagement");
SW.DPA.Admin.ServerManagement.TestConnection = function (config) {
    var localizedStrings = {
        testConnectionUnexpectedError: '@{R=DPA.Strings;K=TestConnection_UnexpectedError;E=js}',
        testConnectionSuccess: '@{R=DPA.Strings;K=TestConnection_Success;E=js}',
        testConnectionInvalidCredentials: '@{R=DPA.Strings;K=TestConnection_InvalidCredentials;E=js}',
        testConnectionInsufficientPrivileges: '@{R=DPA.Strings;K=TestConnection_InsufficientPrivileges;E=js}',
        testConnectionNotResponding: '@{R=DPA.Strings;K=TestConnection_NotResponding;E=js}',
        testConnectionUnsupportedVersion: '@{R=DPA.Strings;K=TestConnection_UnsupportedVersion;E=js}',
        testConnectionSwisFailure: '@{R=DPA.Strings;K=TestConnection_SwisFailure;E=js}',
        testConnectionUnableToPingBack: '@{R=DPA.Strings;K=TestConnection_UnableToPingBack;E=js}',
        testConnectionAlreadyIntegrated: '@{R=DPA.Strings;K=TestConnection_AlreadyIntegrated;E=js}'
    };

    var testConnectionResultMessages = {};
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.Success] = localizedStrings.testConnectionSuccess;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.InvalidCredentials] = localizedStrings.testConnectionInvalidCredentials;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.InsufficientPrivileges] = localizedStrings.testConnectionInsufficientPrivileges;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.NotResponding] = localizedStrings.testConnectionNotResponding;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.UnsupportedVersion] = localizedStrings.testConnectionUnsupportedVersion;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.SwisFailure] = localizedStrings.testConnectionSwisFailure;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.UnableToPingBack] = localizedStrings.testConnectionUnableToPingBack;
    testConnectionResultMessages[SW.DPA.Admin.ServerManagement.ConnectionTestResult.AlreadyIntegrated] = localizedStrings.testConnectionAlreadyIntegrated;

    var uiManager = new SW.DPA.Admin.ServerManagement.IntegrationSetupModalUiManager();
    
    var compabilityHelpLink = $('#DPA_TestConnectionCompatibilityLink');
    var statusWrapper = $('.DPA_TestConnectionStatus');
    var parentDiv = statusWrapper.closest('.DPA_TestConnectionDiv');
    var statusDiv = statusWrapper.find('.DPA_TestConnectionSuccess');
    var statusMessage = statusWrapper.find('.DPA_TestConnectionSuccessMessage');
    var errorWrapper = $('#DPA_TestConnectionErrorMessageWrapper');
    var errorMessage = $('#DPA_TestConnectionErrorMessage');
    var spinner = statusWrapper.find('.DPA_Spinner');
    var testConnectionButton = parentDiv.find('.DPA_TestConnectionButton');
    var cancelTestConnectionButton = statusWrapper.find('.DPA_CancelTestConnectionButton');

    var taskId;
    var timerId;
    var testConnectionFinishedPollingInterval = 1000; // ms
    
    var showUnexpectedError = function () {
        spinner.hide();
        errorMessage.text(localizedStrings.testConnectionUnexpectedError);
        errorWrapper.show();
        statusWrapper.hide();
        testConnectionButton.show();
    }

    var clearAll = function () {
        uiManager.resetUi();
        parentDiv.show();
        statusDiv.hide();
        statusWrapper.show();
        spinner.show();
        cancelTestConnectionButton.show();
        testConnectionButton.hide();
        errorWrapper.hide();
        errorMessage.text('');
        compabilityHelpLink.hide();
        // hide error message + remove highlighting of the field - Orion hostname or IP address:
        uiManager.hideOrionHostnameErrorMessage();
        $('#DPA_OrionHostnameWrapper').find('input').removeClass(uiManager.highlightedFieldClassName);
        $('#DPA_OrionHostnameWrapper').find('input').removeClass('sw-dpa-validation-error');
    }

    var isTestingFinished = function() {
        SW.Core.Services.callController('/sapi/DpaTestConnection/IsStillTestingConnection', taskId,
            function (result) {
                if (result != SW.DPA.Admin.ServerManagement.ConnectionTestResult.InProgress) {
                    //Testing finished, show the response
                    spinner.hide();

                    if (result == SW.DPA.Admin.ServerManagement.ConnectionTestResult.Success) {
                        statusMessage.text(testConnectionResultMessages[result]);
                        statusDiv.show();
                    } else if (result == SW.DPA.Admin.ServerManagement.ConnectionTestResult.UnableToPingBack) { // unable to pingback
                        // show error message + field with Orion hostname + highlight the field
                        uiManager.showOrionHostnameErrorMessage();
                        $('#DPA_OrionHostnameErrorWrapper').find('.sw-suggestion').show();
                        uiManager.enableOrionHostnameField();
                        $('#DPA_OrionHostnameWrapper').find('input').addClass(uiManager.highlightedFieldClassName);
                    } else {
                        errorMessage.text(testConnectionResultMessages[result]);
                        errorWrapper.show();
                    }

                    if (result == SW.DPA.Admin.ServerManagement.ConnectionTestResult.UnsupportedVersion) {
                        compabilityHelpLink.show();
                    }

                    cancelTestConnectionButton.hide();
                    testConnectionButton.show();
                    clearInterval(timerId);
                }
            },
            function () {
                showUnexpectedError();
            });
        
    }

    var testConnection = function () {
        window.Page_ClientValidate();
        if (window.Page_IsValid) {
            // hide error messages
            if ($('.sw-dpa-integration .sw-suggestion-fail').length > 0) {
                $('.sw-dpa-integration .sw-suggestion-fail').hide();
            }
            // remove highlight style for input boxes
            if ($('input.' + uiManager.highlightedFieldClassName).length > 0) {
                $('input.' + uiManager.highlightedFieldClassName).removeClass(uiManager.highlightedFieldClassName);
            }
            
            var requestData = {
                ServerAddress: config.serverAddress.val(),
                Port: config.port.val(),
                UserName: config.username.val(),
                Password: config.password.val(), 
                OrionServerAddress: config.orionServerAddress.val()
            }

            SW.Core.Services.callController(
                '/sapi/DpaTestConnection/BeginTestConnection',
                requestData,
                function(result) {
                    timerId = setInterval(isTestingFinished, testConnectionFinishedPollingInterval);
                    taskId = result;
                    //clear all previous texts in status show validating status
                    clearAll();
                },
                function () {
                    //Uncaught exception - should not happen
                    showUnexpectedError();
                }
            );
        }
    };

    var cancelTestConnectionClick = function() {
        SW.Core.Services.callController('/sapi/DpaTestConnection/CancelTestConnection', taskId,
            function() {
                statusWrapper.hide();
                cancelTestConnectionButton.hide();
                testConnectionButton.show();
                clearInterval(timerId);
            },
            function() {
                showUnexpectedError();
            });
    };

    testConnectionButton.click(function () {
        testConnection();
        return false;
    });

    cancelTestConnectionButton.click(function () {
        cancelTestConnectionClick();
        return false;
    });
}