﻿Ext.namespace("SW.DPA.Admin");
SW.DPA.Admin.IntegrationSetupModal = function (dialogId) {
	var localizedStrings = {
	    dialogTitle: "@{R=DPA.Strings; K=IntegrationSetup_Title; E=js}",
	    orionHostnameHelpTitle: "@{R=DPA.Strings; K=OrionHostnameHelpTitle; E=js}",
	    orionHostnameHelpTextP1: "@{R=DPA.Strings; K=OrionHostnameHelpTextParagraph1; E=js}",
	    orionHostnameHelpTextP2: "@{R=DPA.Strings; K=OrionHostnameHelpTextParagraph2; E=js}"
	};

    var that = this;
	var dialogElement = $("#" + dialogId);
	var closeButton = dialogElement.find(".DPA_CloseButton");
	var submitButton = dialogElement.find(".DPA_SubmitButton");
    var cancelButton = $(".DPA_CancelIntegrationButton");
	var displayName = dialogElement.find(".DPA_DisplayName :text");
	var serverAddress = dialogElement.find(".DPA_ServerAddress :text");
    var port = dialogElement.find(".DPA_Port :text");
    var orionhostnameHelpButton = dialogElement.find('.DPA_QstnIcon');

	var connectionInputElements = {
	    serverAddress: serverAddress,
	    port: port,
	    username: dialogElement.find(".DPA_UserName :text"),
	    password: dialogElement.find(".DPA_Password :password"),
	    displayName: displayName,
	    orionServerAddress: dialogElement.find(".DPA_OrionHostname :text"),
	    mainSwisHostname: dialogElement.find(".DPA_OrionHostname :hidden"),
	    integrationSetupModal: this
	};

	var establishIntegration = new SW.DPA.Admin.ServerManagement.EstablishIntegration(connectionInputElements);
    SW.DPA.Admin.ServerManagement.TestConnection(connectionInputElements);
	var uiManager = new SW.DPA.Admin.ServerManagement.IntegrationSetupModalUiManager();

    // prevent page refresh but save input values for autocomplete
    window.aspnetForm.action = "javascript:void(0)";

	var formatName = function () {
	    displayName.val(serverAddress.val() + ':' + port.val());
	};

	displayName.on("change", function () {
	    if (displayName.val().length === 0) {
	        serverAddress.on("input keyup", function() {
	            formatName();
	        });
	        port.on("input keyup", function () {
	            formatName();
	        });
	    } else {
	        serverAddress.off("input keyup");
	        port.off("input keyup");
	    }
	});

    this.show = function(resetUi) {
        var dialogOptions = {
            width: "860px",
            height: "auto",
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedStrings.dialogTitle,
            resizable: false,
            open: function () {
                dialogElement.parent().detach().appendTo('#aspnetForm');
                $(".ui-dialog[aria-labelledby='ui-dialog-title-" + dialogId + "']  .ui-dialog-titlebar-close").attr("automation", "Icon_CloseIntegrationSetup");
                // IE won't focus without timeout
                setTimeout(function () { serverAddress.focus(); });
            }
        };

        SW.Core.Services.callControllerSync(
            "/sapi/DpaIntegrationSetup/IsSessionAlive",
            {},
            function(result) {},
            function(error) {});

        dialogElement.dialog(dialogOptions);

        if (resetUi) {
            uiManager.resetUi();
            uiManager.clearFields();

            serverAddress.off("input keyup").on("input keyup", function () {
                formatName();
            });

            port.off("input keyup").on("input keyup", function () {
                formatName();
            });
        }
    };

    this.close = function() {
        dialogElement.dialog("close");
    };
    
    submitButton.click(function () {
        establishIntegration.establishIntegration();
    });

    closeButton.on("click", function () {
        that.close();
        return false;
    });
    
    cancelButton.click(function () {
        establishIntegration.cancelIntegration();
        return false;
    });

    var orionhostnameHelpTextTemplate = _.templateExt(['<p>{{{p1}}}</p><p>{{{p2}}}</p>']);
    orionhostnameHelpButton.click(function () {
        SW.DPA.Admin.ServerManagement.HelpDialog(localizedStrings.orionHostnameHelpTitle,
            orionhostnameHelpTextTemplate({ p1: localizedStrings.orionHostnameHelpTextP1, p2: localizedStrings.orionHostnameHelpTextP2 }));
        return false;
    });
}