﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.StorageConfigurationModal = function(id) {
    var localizedTexts = {
        dialogTitle: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_Title; E=js}",
        showMoreLabelFormat: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_ShowMoreLabelFormat; E=js}",
        noDatabaseInstanceSelectedValidationTitle: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_Validation_NoDatabaseInstanceSelectedTitle; E=js}",
        noDatabaseInstanceSelectedValidationMessage: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_Validation_NoDatabaseInstanceSelectedMessage; E=js}",
        noLunSelectedValidationTitle: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_Validation_NoLunSelectedTitle; E=js}",
        noLunSelectedValidationMessage: "@{R=DPA.Strings; K=StorageRelationshipConfiguration_Validation_NoLunSelectedMessage; E=js}"
    };

    var selectDatabaseInstanceStep = $('#DPA_SelectDatabaseInstanceStep');
    var selectStorageStep = $('#DPA_SelectStorageStep');

    var modalEl = $('#' + id);
    var dialog = null;
    var that = this;
    this.selectedGlobalDatabaseInstanceIds = [];
    this.selectedLunIds = [];

    this.show = function (config, saveDataCallback) {
        // reset the window
        that.selectedGlobalDatabaseInstanceIds = [];
        that.selectedLunIds = [];
        selectDatabaseInstanceStep.find('span.DPA_SelectedObject').html("");
        selectStorageStep.find('span.DPA_SelectedObject').html("");
        
        var showConfig = config || {};
        if (showConfig.selectedDatabaseInstances && showConfig.selectedDatabaseInstances.length > 0) {
            onDatabaseInstanceSelected(showConfig.selectedDatabaseInstances);
        }

        if (showConfig.selectedLuns && showConfig.selectedLuns.length > 0) {
            onLunSelected(showConfig.selectedLuns);
        }

        that.saveDataCallback = saveDataCallback || $.noop();
        that.databaseInstanceSelectorConfig = $.extend({
            setMainGridPanelId: false,
            selectionChange: function (selection) {
                onDatabaseInstanceSelected(_.map(selection, function (item) { return item.data; }));
            },
            selectedItemsArray: _.map(showConfig.selectedDatabaseInstances, function (item) { return { data: item }; })
        }, showConfig.instanceConfig);

        that.lunSelectorConfig = $.extend({
            setMainGridPanelId: false,
            selectionChange: function (selection) {
                onLunSelected(_.map(selection, function (item) { return item.data; }));
            },
            selectedItemsArray: _.map(showConfig.selectedLuns, function (item) { return { data: item }; })
        }, showConfig.lunConfig);

        var dialogOptions = {
            width: 800,
            minHeight: 619,
            dialogClass: 'DPA_ConfigurationModal',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedTexts.dialogTitle,
            resizable: false,
            open: function () {
                if (that.selectedGlobalDatabaseInstanceIds.length > 0) {
                    expandStorageStep();
                } else {
                    expandDatabaseInstanceStep();
                }
            },
            close: function() {
                if (SW.DPA.Admin.databaseInstanceSelector) {
                    SW.DPA.Admin.databaseInstanceSelector.destroy();
                    SW.DPA.Admin.databaseInstanceSelector = undefined;
                }
                if (SW.DPA.Admin.lunSelector) {
                    SW.DPA.Admin.lunSelector.destroy();
                    SW.DPA.Admin.lunSelector = undefined;
                }
            }
        };

        // show dialog
        dialog = modalEl
            .dialog(dialogOptions).css('overflow', 'hidden');
    };

    this.close = function () {
        modalEl.dialog('close');
    };

    var expandStorageStep = function () {
        selectDatabaseInstanceStep.removeClass('DPA_Expanded').addClass('DPA_Collapsed');
        selectStorageStep.removeClass('DPA_Collapsed').addClass('DPA_Expanded');

        initLunSelector();

        that.showSaveButton();
    };

    var expandDatabaseInstanceStep = function () {
        selectStorageStep.removeClass('DPA_Expanded').addClass('DPA_Collapsed');
        selectDatabaseInstanceStep.removeClass('DPA_Collapsed').addClass('DPA_Expanded');

        initDatabaseInstanceSelector();

        that.hideSaveButton();
    };

    var initDatabaseInstanceSelector = function () {
        if (!SW.DPA.Admin.databaseInstanceSelector) {
            SW.DPA.Admin.databaseInstanceSelector = new SW.DPA.Admin.DatabaseInstanceSelector(that.databaseInstanceSelectorConfig);
        }
    };

    var initLunSelector = function () {
        if (!SW.DPA.Admin.lunSelector) {
            SW.DPA.Admin.lunSelector = new SW.DPA.Admin.LunSelector(that.lunSelectorConfig);
        }
    };

    var onDatabaseInstanceSelected = function (dataItems) {
        var maxDatabaseInstancesInHeader = 5;
        var sectionTitleValues = _.map(dataItems, function (data) { return data.InstanceName; });
        changeSectionTitle(selectDatabaseInstanceStep, sectionTitleValues, maxDatabaseInstancesInHeader);

        that.selectedGlobalDatabaseInstanceIds = _.map(dataItems, function (data) { return data.InstanceID; });
    };

    var onLunSelected = function (dataItems) {
        var maxLunsInHeader = 5;
        var sectionTitleValues = _.map(dataItems, function (data) { return data.LunName; });
        changeSectionTitle(selectStorageStep, sectionTitleValues, maxLunsInHeader);

        that.selectedLunIds = _.map(dataItems, function (data) { return data.LunId; });
    };

    var moreTooltipTemplate = _.templateExt([
      ' <span class="DPA_Dots" ext:qtip="{#for(var i=0;i<applications.length;i++) { #} {{{applications[i]}}} <br /> {# } #}"',
      ' ext:qtitle="">',
      '{{{label}}}</span>']);

    var changeSectionTitle = function (el, selectionValues, limit) {
        var selectionValuesToDisplay = _.first(selectionValues, limit);

        if (selectionValuesToDisplay && selectionValuesToDisplay.length > 0) {
            el.find('span.DPA_SectionTitle').append(function (_, txt) {
                return (txt.slice(-1) == ':') ? '' : ':';
            });
            el.find('span.DPA_SelectedObject').text(selectionValuesToDisplay.join('; '));

            if (selectionValues.length > limit) {
                var restOfApplications = _.last(selectionValues, selectionValues.length - limit);
                var htmlSuffix = moreTooltipTemplate({
                    applications: restOfApplications,
                    label: SW.Core.String.Format(localizedTexts.showMoreLabelFormat, restOfApplications.length)
                });

                el.find('span.DPA_SelectedObject').append(htmlSuffix);
            }
        } else {
            el.find('span.DPA_SectionTitle').text(function (_, txt) {
                return (txt.slice(-1) == ':') ? txt.slice(0, -1) : txt;
            });
            el.find('span.DPA_SelectedObject').empty();
        }
    };

    this.showSaveButton = function () {
        $('#DPA_NextButton').hide();
        $('#DPA_SaveApplicationButton').show();
    }

    this.hideSaveButton = function () {
        $('#DPA_SaveApplicationButton').hide();
        $('#DPA_NextButton').show();
    }

    modalEl.find('.DPA_CollapsingBlockHeader').click(function () {
        var headerEl = $(this);
        if (headerEl.hasClass('DPA_Clickable')) {
            var parentAttrId = headerEl.parent().attr('id');
            if (headerEl.parent().hasClass('DPA_Collapsed')) {
                // expand current and collapse second one
                if (parentAttrId === 'DPA_SelectDatabaseInstanceStep') {
                    expandDatabaseInstanceStep();
                }
                else if (parentAttrId === 'DPA_SelectStorageStep') {
                    expandStorageStep();
                }
            } else {
                // collapse current and expand second one
                if (parentAttrId === 'DPA_SelectDatabaseInstanceStep') {
                    expandStorageStep();
                }
                else if (parentAttrId === 'DPA_SelectStorageStep') {
                    expandDatabaseInstanceStep();
                }
            }
        }
    });

    modalEl.find('#DPA_CancelButton').click(function () {
        modalEl.dialog('close');
    });

    modalEl.find('#DPA_NextButton').click(function () {
        expandStorageStep();
    });

    modalEl.find('#DPA_SaveApplicationButton').click(function () {
        // validate selection
        if (that.selectedGlobalDatabaseInstanceIds == null || that.selectedGlobalDatabaseInstanceIds.length === 0) {
            Ext.Msg.alert(localizedTexts.noDatabaseInstanceSelectedValidationTitle, localizedTexts.noDatabaseInstanceSelectedValidationMessage);
        }
        else if (that.selectedLunIds == null || that.selectedLunIds.length === 0) {
            Ext.Msg.alert(localizedTexts.noLunSelectedValidationTitle, localizedTexts.noLunSelectedValidationMessage);
        } else {
            that.saveDataCallback({ GlobalDatabaseInstanceIds: that.selectedGlobalDatabaseInstanceIds, LunIds: that.selectedLunIds });
        }
    });
};