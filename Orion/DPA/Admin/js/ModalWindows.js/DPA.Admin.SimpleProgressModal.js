﻿SW.Core.namespace("SW.DPA.Admin");
SW.DPA.Admin.SimpleProgressModal = function () {

    var dialogTemplate = _.templateExt([
     '<div class="DPA_SimpleProgressContent">',
       '<p class="DPA_ProgressMessage"><img src="/Orion/images/loading_gen_small.gif" /> {{{progressMessage}}}</p>',
     '</div>']);

    var window;

    this.show = function(title, message) {
        var dialogOptions = {
            width: "500",
            height: "auto",
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: title,
            resizable: false,
            closeOnEscape: false,
            open: function () {
                $(".ui-dialog-titlebar-close").hide();
            }
        };

        window = $(dialogTemplate({ progressMessage: message }));

        window.dialog(dialogOptions);

        return false;
    };

    this.close = function () {
        window.dialog("close");
    };
};