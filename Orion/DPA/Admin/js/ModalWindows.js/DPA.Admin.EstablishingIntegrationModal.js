﻿Ext.namespace("SW.DPA.Admin");

SW.DPA.Admin.EstablishingIntegrationModal = function (dialogId) {
    var localizedStrings = {
        dialogTitle: "@{R=DPA.Strings; K=EstablishingIntegration_Title; E=js}",
        dialogTitleSuccess: "@{R=DPA.Strings; K=IntegrationSuccessful_Title; E=js}"
    };

    var dialogElement = $("#" + dialogId);
    var that = this;
    var buttonDisabledClass = "sw-btn-disabled";
    var cancelButton = dialogElement.find(".DPA_CancelIntegrationButton");
    var closeButton = dialogElement.find(".DPA_CloseDialogButton");
    var unrecoverableError = dialogElement.find(".DPA_UnrecoverableError");
    var error = unrecoverableError.find(".DPA_Error");
    var unexpectedError = unrecoverableError.find(".DPA_UnexpectedError");
    var successMessage = dialogElement.find(".DPA_SuccessMessage");
    var successButtons = dialogElement.find(".DPA_EstablishingIntegrationSuccessButtons");
    var dialogCloseIcon;

    this.show = function () {
        successMessage.text("").hide();
        cancelButton.show();
        this.disableCancelButton();
        unrecoverableError.hide();
        error.text("").hide();
        unexpectedError.hide();
        closeButton.hide();
        
        var dialogOptions = {
            width: "600",
            height: "auto",
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedStrings.dialogTitle,
            resizable: false,
            closeOnEscape: false,
            open: function() {
                // hide close button
                dialogCloseIcon = $(".ui-dialog[aria-labelledby='ui-dialog-title-" + dialogId + "']  .ui-dialog-titlebar-close");
                dialogCloseIcon.on("click", function () {
                    that.close();
                    window.location.reload();
                }).hide();
                dialogCloseIcon.attr("automation", "Icon_CloseProgressDialog");
            }
        };

        dialogElement.dialog(dialogOptions);
    };

	this.close = function () {
	    dialogElement.dialog("close");
	}

	this.success = function (message) {
	    dialogElement.dialog('option', 'title', localizedStrings.dialogTitleSuccess);
	    successMessage.text(message).show();
        successButtons.show();
        dialogCloseIcon.show();

	    closeButton.off("click");
        closeButton.click(function () {
            that.close();
            window.location.reload();
            return false;
        });
        closeButton.show();
	};

    this.hideCancelButton = function() {
        cancelButton.hide();
    };

    this.disableCancelButton = function() {
        cancelButton.addClass(buttonDisabledClass);
    }

    this.enableCancelButton = function() {
        cancelButton.removeClass(buttonDisabledClass);
    };

    this.showUnrecoverableError = function(errorMessage) {
        unrecoverableError.show();
        closeButton.show();

        if (errorMessage)
            error.text(errorMessage).show();
        else
            unexpectedError.show();
    };
}