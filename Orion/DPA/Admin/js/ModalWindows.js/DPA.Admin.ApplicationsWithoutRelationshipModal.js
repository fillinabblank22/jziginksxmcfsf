﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.ApplicationsWithoutRelationshipModal = function (id) {
    var localizedTexts = {
        dialogTitle: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_Title; E=js}",
        columnApplicationName: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_ColumnApplicationName; E=js}",
        columnNodeName: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_ColumnNodeNameTitle; E=js}",
        columnTemplateName: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_ColumnTemplateName; E=js}",
        columnActions: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_ColumnActions; E=js}",
        addDbInstanceTooltip: "@{R=DPA.Strings; K=IntegrationWizard_ApplicationsWithoutRelationship_AddDbInstanceTooltip; E=js}"
    };

    var modalEl = $('#' + id);
    var dialog = null;
    var that = this;
    var position = ['center', $(window).height() / 10];
    var pageableTableUniqueId;

    var getPageableTableUniqueId = function() {
        if (!pageableTableUniqueId) {
            pageableTableUniqueId = $('#appsWithoutRelUniqueId').val();
        }
        return pageableTableUniqueId;
    };

    this.show = function (config) {
        var defaultShowConfig = {
            closeCallback: $.noop,
            modelId: '???'
        };
        that.showConfig = $.extend(true, {}, defaultShowConfig, config || {});

        that.initApplicationsGrid(that.showConfig);

        var dialogOptions = {
            width: 800,
            height: 'auto',
            dialogClass: 'DPA_ConfigurationModal',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedTexts.dialogTitle,
            resizable: false,
            open: function () {
                SW.DPA.UI.SortablePageableTable.refresh(getPageableTableUniqueId());
                $(this).dialog("option", "position", position);
            },
            close: function () {
                position = $(this).dialog("option", "position");
            }
        };

        // show dialog
        dialog = modalEl
            .dialog(dialogOptions).css('overflow', 'hidden');
    };

    this.initApplicationsGrid = function (showConfig) {
        var tableIndex = {}, setTableIndex = function(columns) { $.each(columns, function(i, n) { tableIndex[n] = i; }); };

        var formatEntityNameWithIcon = function (entity, entityName, entityStatus, entityDetailsUrl, entityId) {
            return SW.DPA.Common.formatEntityWithStatusIconLink(entity, entityName, entityStatus, entityDetailsUrl, entityId, 'DPA_CellEntityNameWithIcon');;
        };

        var addDbHtml = _.template("<div class='DPA_Actions'><a href='#' onclick=\"return SW.DPA.LinksToDpaSelector.show('/api/DpaLinksToDpaj/RegisterDbInstance/')\" target='_blank'><img ext:qtitle='{{addDbInstanceTooltip}}' ext:qtip=' ' src='/Orion/DPA/images/add_16x16.png' /></a></div>", localizedTexts);

        SW.DPA.UI.SortablePageableTable.initialize({
            uniqueId: getPageableTableUniqueId(),
            pageableDataTableProvider: function(pageIndex, pageSize, onSuccess, onError) {
                var currentOrderBy = $('#OrderBy-' + getPageableTableUniqueId()).val();
                SW.Core.Services.callController("/sapi/DpaRelationshipsGrid/GetApplicationsWithoutRelationship", {
                    ModelId: showConfig.modelId,
                    Limit: pageSize,
                    PageIndex: pageIndex,
                    OrderByClause: currentOrderBy
                }, function(result) {
                    setTableIndex(result.DataTable.Columns);
                    
                    var tableEl = $('#Grid-' + getPageableTableUniqueId());
                    
                    if (tableEl.find("tr.HeaderRow").length === 0) {
                        // remove relocated header row
                        tableEl.closest("table.scrollableEnvelop").find("tr.HeaderRow").remove();
                        // include original empty header
                        tableEl.find("tbody").prepend("<tr class='HeaderRow'></tr>");
                    }

                    // render table
                    onSuccess(result);
                   
                    // envelop table by scrollable table
                    var envelopEl = tableEl.closest('table.scrollableEnvelop');
                    if (envelopEl.length === 0) {
                        tableEl = tableEl.wrap("<table class='scrollableEnvelop sw-custom-query-table' cellspacing='0'><tbody><tr class='content'><td colspan='" + tableEl.find(">tbody tr:first td").length + "'><div class='scrollableContent'></div></td></tr></tbody></table>");
                        envelopEl = tableEl.closest('table.scrollableEnvelop').prepend("<thead></thead>");
                    }

                    // get cols width from original table
                    var colWidth = tableEl.find("tr:first").children().map(function () {
                        return $(this).outerWidth();
                    });
                    
                    // relocate header row
                    envelopEl.find(">thead").append(tableEl.find("tbody tr.HeaderRow"));
                    
                    // set cols width for content rows
                    tableEl.find("tbody tr:first td").each(function (i, v) {
                        $(v).width(colWidth[i]);
                    });
                    
                    // set cols width for header rows
                    envelopEl.find(">thead tr:first td").each(function (i, v) {
                        $(v).width(colWidth[i]);
                    });

                    // adjust space bellow grid according pager visibility
                    if ($("#DPA_ApplicationsWithoutRelationshipModal .ResourcePagerControl").is(":visible")) {
                        $("#DPA_ApplicationsWithoutRelationshipModal .bottom").removeClass("pagerHidden");
                    } else {
                        $("#DPA_ApplicationsWithoutRelationshipModal .bottom").addClass("pagerHidden");
                    }

                }, function (errorResult) {
                });
            },
            initialPageIndex: 0,
            rowsPerPage: 10,
            columnSettings: {
                'ApplicationName': {
                    caption: localizedTexts.columnApplicationName,
                    isHtml: true,
                    allowSort: true,
                    sortingProperty: 'ApplicationName',
                    formatter: function (cellValue, rowArray) {
                        return formatEntityNameWithIcon(rowArray[tableIndex.ApplicationInstanceType], rowArray[tableIndex.ApplicationName], rowArray[tableIndex.ApplicationStatus], rowArray[tableIndex.ApplicationDetailsUrl]);
                    }
                },
                "NodeName": {
                    caption: localizedTexts.columnNodeName,
                    isHtml: true,
                    allowSort: true,
                    sortingProperty: 'NodeName',
                    formatter: function(cellValue, rowArray) {
                        return formatEntityNameWithIcon(rowArray[tableIndex.NodeInstanceType], rowArray[tableIndex.NodeName], rowArray[tableIndex.NodeStatus], rowArray[tableIndex.NodeDetailsUrl], rowArray[tableIndex.NodeID]);
                    }
                },
                "TemplateName": {
                    caption: localizedTexts.columnTemplateName,
                    isHtml: false,
                    allowSort: true,
                    sortingProperty: 'TemplateName'
                },
                "Actions": {
                    caption: localizedTexts.columnActions,
                    isHtml: true,
                    allowSort: false,
                    formatter: function (cellValue, rowArray) {
                        return addDbHtml;
                    }
                }
            }
        });
    };

    this.close = function () {
        that.showConfig.closeCallback();
        modalEl.dialog('close');
    };

    // bind button clicks
    modalEl.find('.DPA_CloseButton').click(function () {
        that.close();
    });
};