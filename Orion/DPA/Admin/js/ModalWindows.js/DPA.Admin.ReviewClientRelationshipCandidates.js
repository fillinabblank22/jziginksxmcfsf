﻿SW.Core.namespace("DPA.Admin");

DPA.Admin.ReviewClientRelationshipCandidates = function (successSaveRelationshipsCallback) {
    var localizedStrings = {
        dialogTitle: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_Title;E=js}',
        cancelButtonLabel: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_CancelButtonLabel;E=js}',
        saveCandidatesButtonLabel: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_SaveRelationshipsButtonLabel;E=js}',
        noRelationshipSelectedAlertTitle: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_NoSelectionAlertTitle;E=js}',
        noRelationshipSelectedAlertDescription: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_NoSelectionAlertDescription;E=js}',
        saveRelationshipFailAlertTitle: '@{R=DPA.Strings;K=ReviewClientApplicationCandidatesDialog_ErrorOccuredMessage;E=js}'
    };

    var dialog = $('#DPA_ClientApplicationCandidatesModal');
    var clientApplicationCandidatesSelector;
    var selectedRelationships = [];

    var destroyWindow = function () {        
        selectedRelationships = [];
        if (typeof clientApplicationCandidatesSelector !== 'undefined') {
            SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationCandidates", "ClearClientApplicationCandidatesCache",
                clientApplicationCandidatesSelector.getBaseParams(), function () { }, function () { });
            clientApplicationCandidatesSelector.destroy();
            clientApplicationCandidatesSelector = undefined;
        }
    };

    // set buttons
    dialog.find('.DPA_Buttons').empty();

    dialog.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedStrings.saveCandidatesButtonLabel,
        { type: 'primary' })).click(function () {
            if (selectedRelationships.length > 0) {
                SW.Core.Services.callControllerAction("/sapi/DpaClientApplicationCandidates", "SaveRelationships", selectedRelationships,
                    function() {
                        successSaveRelationshipsCallback();
                        destroyWindow();
                        dialog.dialog('close');
                    },
                    function (errorResponse) {
                        Ext.Msg.alert(localizedStrings.saveRelationshipFailAlertTitle, localizedStrings.saveRelationshipFailAlertTitle + ' ' + errorResponse);
                });
            } else {
                Ext.Msg.alert(localizedStrings.noRelationshipSelectedAlertTitle, localizedStrings.noRelationshipSelectedAlertDescription);
            }
    }));

    dialog.find('.DPA_Buttons').append($(SW.Core.Widgets.Button(localizedStrings.cancelButtonLabel,
        { type: 'secondary' })).click(function () {
            destroyWindow();
            dialog.dialog('close');
        }));

    // config
    var dialogConfig = {
        modal: true,
        width: 1000,
        minHeight: 472,
        dialogClass: 'DPA_ReviewRelationshipCandidates',
        resizable: false,
        overlay: { "background-color": "black", opacity: 0.4 },
        title: localizedStrings.dialogTitle,
        open: function() {
            clientApplicationCandidatesSelector = new SW.DPA.Admin.ClientApplicationCandidates({
                selectionChange: function (selection) {
                    if (typeof selection !== 'undefined') {
                        selectedRelationships = [];
                        for (var i = 0; i < selection.length; i++) {
                            selectedRelationships.push({
                                ApplicationId: selection[i].data.ApplicationID,
                                GlobalDatabaseInstanceId: selection[i].data.GlobalDatabaseInstanceId
                            });
                        }
                    }
                }
            });
        },
        close: function() {
            destroyWindow();
        }
    };

    // create
    dialog.dialog(dialogConfig);
};