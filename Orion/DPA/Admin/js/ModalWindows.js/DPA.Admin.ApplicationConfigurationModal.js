﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.ApplicationConfigurationModal = function(id) {
    var localizedTexts = {
        dialogTitle: "@{R=DPA.Strings; K=IntegrationWizard_ClientApplications_AddModalTitle; E=js}",
        noApplicationSelectedValidationTitle: '@{R=DPA.Strings;K=IntegrationWizard_Applications_ConfigurationDialog_ValidationTitle;E=js}',
        noApplicationSelectedValidationMessage: '@{R=DPA.Strings;K=IntegrationWizard_Applications_ConfigurationDialog_ValidationMessage;E=js}',
        noDatabaseInstanceSelectedValidationTitle: '@{R=DPA.Strings;K=IntegrationWizard_Applications_ConfigurationDialog_NoDbInstanceSelected_Title;E=js}',
        noDatabaseInstanceSelectedValidationMessage: '@{R=DPA.Strings;K=IntegrationWizard_Applications_ConfigurationDialog_NoDbInstanceSelected_Message;E=js}',
        showMoreLabelFormat: '@{R=DPA.Strings;K=ClientApplicationsModal_ShowMoreLabelFormat;E=js}',
        onNodeConjunction: '@{R=DPA.Strings;K=DatabaseNodeConjunction;E=js}'
    };

    var modalEl = $('#' + id);
    var dialog = null;
    var that = this;

    this.selectedApplicationIds = [];
    this.selectedDatabaseInstanceIds = [];

    this.show = function(config, saveDataCallback, closeModalCallback) {
        var showConfig = config || {};
        that.saveDataCallback = saveDataCallback || $.noop();
        that.closeModalCallback = closeModalCallback || $.noop();
        var preselectedInstances = getPreselectedInstances(showConfig.selectedData);

        onApplicationSelected(showConfig.selectedData);
        onDatabaseInstanceSelected(preselectedInstances);
        resetToDefault();

        var dialogOptions = {
            width: 800,
            minHeight: 619,
            dialogClass: 'DPA_ConfigurationModal',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedTexts.dialogTitle,
            resizable: false,
            open: function() {
                that.applicationConfig = $.extend({
                    selectionChange: function(selection) {
                        onApplicationSelected(_.map(selection, function(item) { return item.data; }));
                    },
                    selectedItemsArray: _.map(showConfig.selectedData, function (item) { return { data: item }; }),
                    invalidateServerFilter: function (modelId, onsuccess, onerror) {
                        SW.Core.Services.callControllerActionSync("/sapi/DpaClientApplicationSelector", "InvalidateClientApplicationCache", { modelId: modelId }, onsuccess || $.noop, onerror || $.noop);
                    },
                    setMainGridPanelId: false,
                }, showConfig.applicationConfig);

                that.instanceConfig = $.extend({
                    selectionChange: function(selection) {
                        onDatabaseInstanceSelected(_.map(selection, function(item) { return item.data; }));
                    },
                    selectedItemsArray: _.map(preselectedInstances, function (item) { return { data: item } }),
                    setMainGridPanelId: false,
                }, showConfig.instanceConfig);

                if (that.applicationConfig.selectedItemsArray.length > 0) {
                    expandDatabaseInstanceStep();
                } else {
                    expandApplicationStep();
                }
            },
            close: function() {
                if (SW.DPA.Admin.applicationSelector) {
                    SW.DPA.Admin.applicationSelector.destroy();
                    SW.DPA.Admin.applicationSelector = undefined;
                }

                if (SW.DPA.Admin.instanceSelector) {
                    SW.DPA.Admin.instanceSelector.destroy();
                    SW.DPA.Admin.instanceSelector = undefined;
                }
            }
        };

        // show dialog
        dialog = modalEl
            .dialog(dialogOptions).css('overflow', 'hidden');
    };

    var getPreselectedInstances = function(rawSelectedData) {
        var instances = _.flatten(_.map(rawSelectedData, function(item) {
            try {
                var ids = JSON.parse(item.InstanceID);
                var names = JSON.parse(item.InstanceName);

                var retval = _.map(ids, function(val, i) { return { InstanceID: val, InstanceName: names[i] }; });
                return retval;

            } catch (err) {
                return item;
            }
        }));

        return instances;
    };

    this.close = function() {
        that.closeModalCallback();
        modalEl.dialog('close');
    };

    var moreTooltipTemplate = _.templateExt([
      ' <span class="DPA_Dots" ext:qtip="{#for(var i=0;i<applications.length;i++) { #} {{{applications[i]}}} <br /> {# } #}"',
      ' ext:qtitle="">',
      '{{{label}}}</span>']);


    var changeSectionTitle = function(sectionEl, selectionValues, limit) {
        var el = $(sectionEl);
        var selectionValuesToDisplay = _.first(selectionValues, limit);

        if (selectionValuesToDisplay && selectionValuesToDisplay.length > 0) {
            el.find('span.DPA_SectionTitle').append(function(_, txt) {
                return (txt.slice(-1) == ':') ? '' : ':';
            });
            el.find('span.DPA_SelectedObject').text(selectionValuesToDisplay.join('; '));
            
            if (selectionValues.length > limit) {
                var restOfApplications = _.last(selectionValues, selectionValues.length - limit);
                var htmlSuffix = moreTooltipTemplate({
                    applications: restOfApplications,
                    label: SW.Core.String.Format(localizedTexts.showMoreLabelFormat, restOfApplications.length)
                });

                el.find('span.DPA_SelectedObject').append(htmlSuffix);
            }
        } else {
            el.find('span.DPA_SectionTitle').text(function(_, txt) {
                return (txt.slice(-1) == ':') ? txt.slice(0, -1) : txt;
            });
            el.find('span.DPA_SelectedObject').empty();
        }
    };

    var onApplicationSelected = function (dataItems) {
        var maxApplicationsInHeader = 5;
        var sectionTitleValues = _.map(dataItems, function (data) { return data.ApplicationName + " " + localizedTexts.onNodeConjunction + " " + data.NodeName; });
        changeSectionTitle('div#DPA_SelectApplicationStep', sectionTitleValues, maxApplicationsInHeader);

        that.selectedApplicationIds = _.map(dataItems, function(data) { return data.ApplicationID; });
    };

    var onDatabaseInstanceSelected = function (dataItems) {
        var maxDatabaseInstancesInHeader = 5;
        var sectionTitleValues = _.map(dataItems, function(data) { return data.InstanceName; });
        changeSectionTitle('div#DPA_SelectInstanceStep', sectionTitleValues, maxDatabaseInstancesInHeader);

        that.selectedDatabaseInstanceIds = _.map(dataItems, function(data) { return data.InstanceID; });
    };

    this.showSaveButton = function() {
        $('#DPA_NextButton').hide();
        $('#DPA_SaveApplicationButton').show();
    }

    this.hideSaveButton = function() {
        $('#DPA_SaveApplicationButton').hide();
        $('#DPA_NextButton').show();
    }

    var resetToDefault = function() {
        that.hideSaveButton();

        $('#DPA_SelectApplicationStep').addClass('DPA_Collapsed').removeClass('DPA_Expanded');
        $('#DPA_SelectApplicationStep .DPA_CollapsingBlockHeader').addClass('DPA_Clickable');

        $('#DPA_SelectInstanceStep').addClass('DPA_Collapsed').removeClass('DPA_Expanded');
        $('#DPA_SelectInstanceStep .DPA_CollapsingBlockHeader').addClass('DPA_Clickable');
    }

    var expandApplicationStep = function () {
        collapseDatabaseInstanceStep();
        $('#DPA_SelectApplicationStep').removeClass('DPA_Collapsed').addClass('DPA_Expanded');
        that.hideSaveButton();

        initApplicationSelector();
    };

    var collapseApplicationStep = function() {
        $('#DPA_SelectApplicationStep').removeClass('DPA_Expanded').addClass('DPA_Collapsed');
    };

    var expandDatabaseInstanceStep = function () {
        collapseApplicationStep();
        $('#DPA_SelectInstanceStep').removeClass('DPA_Collapsed').addClass('DPA_Expanded');
        that.showSaveButton();

        initDatabaseInstanceSelector();
    };

    var collapseDatabaseInstanceStep = function () {
        $('#DPA_SelectInstanceStep').removeClass('DPA_Expanded').addClass('DPA_Collapsed');
    };

    var initApplicationSelector = function() {
        if (!SW.DPA.Admin.applicationSelector) {
            SW.DPA.Admin.applicationSelector = new SW.DPA.Admin.ClientApplicationSelector(that.applicationConfig);
        }
    };

    var initDatabaseInstanceSelector = function() {
        if (!SW.DPA.Admin.instanceSelector) {
            SW.DPA.Admin.instanceSelector = new SW.DPA.Admin.DatabaseInstanceSelector(that.instanceConfig);
        }
    };

    modalEl.find('.DPA_CollapsingBlockHeader').click(function () {
        var headerEl = $(this);
        if (headerEl.hasClass('DPA_Clickable')) {
            var parentAttrId = headerEl.parent().attr('id');
            if (headerEl.parent().hasClass('DPA_Collapsed')) {
                // expand current and collapse second one
                if (parentAttrId == 'DPA_SelectApplicationStep') {
                    expandApplicationStep();
                }
                else if (parentAttrId == 'DPA_SelectInstanceStep') {
                    expandDatabaseInstanceStep();
                }
            } else {
                // collapse current and expand second one
                if (parentAttrId == 'DPA_SelectApplicationStep') {
                    expandDatabaseInstanceStep();
                }
                else if (parentAttrId == 'DPA_SelectInstanceStep') {
                    expandApplicationStep();
                }
            }
        }
    });

    // bind button clicks
    modalEl.find('#DPA_CancelButton').click(function() {
        that.closeModalCallback();
        modalEl.dialog('close');
    });

    modalEl.find('#DPA_SaveApplicationButton').click(function () {
        // validate selection
        if (that.selectedApplicationIds == null || that.selectedApplicationIds.length == 0) {
            Ext.Msg.alert(localizedTexts.noApplicationSelectedValidationTitle, localizedTexts.noApplicationSelectedValidationMessage);
        }
        else if (that.selectedDatabaseInstanceIds == null || that.selectedDatabaseInstanceIds.length == 0) {
            Ext.Msg.alert(localizedTexts.noDatabaseInstanceSelectedValidationTitle, localizedTexts.noDatabaseInstanceSelectedValidationMessage);
        } else {
            that.saveDataCallback({ applicationIds: that.selectedApplicationIds, instanceIds: that.selectedDatabaseInstanceIds });
        }
    });

    modalEl.find('#DPA_NextButton').click(function () {
        expandDatabaseInstanceStep();
    });
};