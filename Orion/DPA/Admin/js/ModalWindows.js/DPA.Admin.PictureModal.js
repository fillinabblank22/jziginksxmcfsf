﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.PictureModal = function (id) {
    var modalEl = $('#' + id);
    var dialog;

    this.show = function (pictureUrl) {
        var dialogOptions = {
            width: 'auto',
            height: 'auto',
            dialogClass: 'DPA_PictureModal',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            closeOnEscape: true,
            resizable: false,
            draggable: false
        };

        modalEl.find("img").attr("src", pictureUrl);

        // show dialog
        dialog = modalEl
            .dialog(dialogOptions).css('overflow', 'hidden');

        $("div.DPA_PictureModal.ui-dialog").height("+=36");
    };
};

$().ready(function () {
    var pictureModal = new SW.DPA.Admin.PictureModal('DPA_PictureModal');
    
    $('img[data-big-img-url]').wrap("<a href='#' />").each(function () {
        $(this).click(function () {
            pictureModal.show($(this).data("big-img-url"));
            return false;
        });
    });
});

