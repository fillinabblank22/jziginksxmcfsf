﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.RelationshipConfigurationModal = function(id) {
    var localizedTexts = {
        dialogTitle: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_ConfigurationDialogTitle;E=js}',
        validationTitle: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_ConfigurationDialog_ValidationTitle;E=js}',
        validationMessage: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_ConfigurationDialog_ValidationMessage;E=js}',
        applicationFilterMessage: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_SelectApplicationFilterMessage;E=js}'
    };

    var modalEl = $('#' + id);
    var dialog = null;
    var that = this;
    var position = { my: "center", at: "center", of: window };

    this.selectedDatabaseInstanceId = null;
    this.selectedNodeId = null;
    this.selectedApplicationId = null;

    this.show = function (config, saveDataCallback, closeModalCallback) {
        var showConfig = config || {};
        that.saveDataCallback = saveDataCallback || $.noop();
        that.closeModal = closeModalCallback || $.noop();
        setSelectedDatabaseInstance(showConfig.selectedData);
        setSelectedNode(showConfig.selectedData);
        onNodeSelected(showConfig.selectedData);
        resetToDefault();
        if (showConfig.showApplicationSelector) {
            showSelectApplicationStep();
        }

        var dialogOptions = {
            width: 800,
            height: 'auto',
            dialogClass: 'DPA_ConfigurationModal',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: localizedTexts.dialogTitle,
            resizable: false,
            open: function () {
                that.nodeConfig = $.extend({
                    selectionChange: function (selection) {
                        onNodeSelected(selection && selection.length > 0 ? selection[0].data : undefined);
                    },
                    createMainGridSelectionModel: function () {
                        return new Ext.grid.SingleSelectionModel();
                    },
                    selectedItemsArray: showConfig.selectedData && showConfig.selectedData.NodeID ? [{ data: { NodeID: showConfig.selectedData.NodeID } }] : [],
                    resetSelectedItemsOnRowSelect: true,
                    setMainGridPanelId: false,
                }, showConfig.nodeConfig);

                that.applicationConfig = $.extend({
                    selectionChange: function (selection) {
                        onApplicationSelected(selection && selection.length > 0 ? selection[0].data : undefined);
                    },
                    createMainGridSelectionModel: function () {
                        return new Ext.grid.SingleSelectionModel();
                    },
                    selectedItemsArray: showConfig.selectedData && showConfig.selectedData.ApplicationID ? [{ data: { ApplicationID: showConfig.selectedData.ApplicationID } }] : [],
                    resetSelectedItemsOnRowSelect: true,
                    setMainGridPanelId: false,
                    invalidateServerFilter: function (modelId, onsuccess, onerror) {
                        SW.Core.Services.callControllerActionSync("/sapi/DpaServerApplicationSelector", "InvalidateServerApplicationCache", { modelId: modelId }, onsuccess || $.noop, onerror || $.noop);
                    }
                }, showConfig.applicationConfig);
                

                // hide / show notification bubble
                var message = $('#DPA_SelectNodeInOrionStep .DPA_IsAmbiguous').hide(),
                    linq = message.find('a.clear-node-filter').unbind('click');
                if (that.nodeConfig.filter) {
                    message.show();
                    linq.bind('click', function (ev) {
                        ev.preventDefault();
                        message.hide();
                        var nodes = that.nodeSelector;
                        nodes.filter = nodes.config.filter = null;
                        nodes.loadGrid();
                    });
                }
                
                if (!showConfig.showApplicationSelector) {
                    $('#DPA_SelectNodeInOrionStep .DPA_CollapsingBlockHeader').trigger('click');
                } else {
                    $('#DPA_SelectApplicationStep .DPA_CollapsingBlockHeader').trigger('click');
                }

                $(this).dialog("option", "position", position);
            },
            close: function () {
                if (that.nodeSelector) {
                    that.nodeSelector.destroy();
                    that.nodeSelector = undefined;
                }

                if (that.applicationSelector) {
                    that.applicationSelector.destroy();
                    that.applicationSelector = undefined;
                }

                position = $(this).dialog("option", "position");
            }
        };

        // show dialog
        dialog = modalEl
            .dialog(dialogOptions).css('overflow', 'hidden');
    };

    this.close = function () {
        that.closeModal();
        modalEl.dialog('close');
    };

    var changeSectionTitle = function (sectionEl, selectionName) {
        var el = $(sectionEl);

        if (selectionName) {
            el.find('span.DPA_SectionTitle').append(function (_, txt) {
                return (txt.slice(-1) == ':') ? '' : ':';
            });
            el.find('span.DPA_SelectedObject').text(selectionName);
        } else {
            el.find('span.DPA_SectionTitle').text(function (_, txt) {
                return (txt.slice(-1) == ':') ? txt.slice(0, -1) : txt;
            });
            el.find('span.DPA_SelectedObject').empty();
        }
    };

    var setSelectedDatabaseInstance = function (data) {
        that.selectedDatabaseInstanceId = (data ? data.InstanceID : undefined);
       
        var instanceNameWithStatus = _.template("<span class='DPA_MonitorStatus DPA_MonitorStatus-{{MonitorStatus}}'>{{InstanceName}}</span>", data);
        $('div#DPA_SelectedDatabaseInstance .DPA_SelectedObjectName').html(instanceNameWithStatus);
    };

    var setSelectedNode = function(data) {
        that.selectedNodeId = (data && data.NodeID ? data.NodeID : undefined);
        if (!that.selectedNodeId) {
            $('#DPA_Relationships_SelectedNode').hide();
        } else if (data.NodeDetailsUrl) {
            $('#DPA_Relationships_SelectedNode').show();
            $('#DPA_Relationships_SelectedNode .DPA_SelectedObjectName').html(renderNode('Orion.Nodes', data.NodeName, data.NodeStatus || data.Status, data.NodeDetailsUrl, data.NodeID));
        }
    }

    var renderNode = function (entity, entityName, entityStatus, entityDetailsUrl, entityId) {
        return SW.DPA.Common.formatEntityWithStatusIconLink(entity, entityName, entityStatus, entityDetailsUrl, entityId, 'DPA_CellEntityNameWithIcon');
    }

    var onNodeSelected = function (data) {
        changeSectionTitle('div#DPA_SelectNodeInOrionStep', (data ? data.NodeName : undefined));
        var actuallySelectedNodeId = (data ? data.NodeID : undefined);

        $('#DPA_ApplicationFilterMessage').text(SW.Core.String.Format(localizedTexts.applicationFilterMessage, (data ? data.NodeName : undefined)));

        if (actuallySelectedNodeId != that.selectedNodeId) {
            that.selectedNodeId = actuallySelectedNodeId;
            deselectApplication();
        }

        setSelectedNode(data);
    };

    var onApplicationSelected = function (data) {
        changeSectionTitle('div#DPA_SelectApplicationStep', (data ? data.ApplicationName : undefined));
        that.selectedApplicationId = (data ? data.ApplicationID : undefined);
    };

    this.hideAddNodeButton = function () {
        $("#AddNodeButton").hide();
    },

    this.showSaveButton = function () {
        $('#DPA_SaveRelationshipButton').show();
    }

    var resetToDefault = function() {
        $('#DPA_SelectNodeInOrionStep').addClass('DPA_Collapsed').addClass('DPA_SimpleView').removeClass('DPA_Expanded');
        $('#DPA_SelectNodeInOrionStep .DPA_CollapsingBlockHeader').addClass('DPA_Clickable').hide();

        $('#DPA_SelectNodeInOrionStep').addClass('DPA_CollapsingBlockLast').removeClass('DPA_CollapsingBlockNext');
        $('#DPA_SelectApplicationStep').hide();
    }

    var showSelectApplicationStep = function () {
        $('#DPA_SelectNodeInOrionStep').removeClass('DPA_SimpleView');
        $('#DPA_SelectNodeInOrionStep .DPA_CollapsingBlockHeader').show();
        $('#DPA_SelectApplicationStep').show()
            .addClass('DPA_Collapsed')
            .removeClass('DPA_Expanded');
        $('#DPA_SelectApplicationStep .DPA_CollapsingBlockHeader').addClass('DPA_Clickable');
    }

    modalEl.find('#DPA_SelectNodeInOrionStep').off('expanded.dpa').on('expanded.dpa', function () {
        if (!that.nodeSelector) {
            that.nodeSelector = new SW.DPA.Admin.NodeSelector(that.nodeConfig || {});
        }
    });

    modalEl.find('#DPA_SelectApplicationStep').off('expanded.dpa').on('expanded.dpa', function () {
        var filter = (that.selectedNodeId)
            ? { field: "NodeID", data: { type: "System.Int32", value: that.selectedNodeId, comparison: 'eeq' } }
            : undefined;

        var relatedData = { GlobalDatabaseInstanceID: that.selectedDatabaseInstanceId, NodeID: that.selectedNodeId };

        if (!that.applicationSelector) {
            that.applicationSelector = new SW.DPA.Admin.ClientApplicationSelector($.extend(that.applicationConfig || {}, that.applicationConfig.filter || { filter: filter }, { relatedData: relatedData }));
        } else if (!_.isEqual(that.applicationSelector.config.filter, filter)) {
            that.applicationSelector.onPagingToolbarRefresh(); // invalidates server filter

            that.applicationSelector.getMainGrid().getSelectionModel().clearSelections();
            that.applicationSelector.config.filter = filter;
            that.applicationSelector.config.relatedData = relatedData;

            that.applicationSelector.refresh();
        }
    });

    var deselectApplication = function() {
        if (that.applicationSelector) {
            that.applicationSelector.getMainGrid().getSelectionModel().clearSelections();
        } else {
            onApplicationSelected();
        }
    }

    modalEl.find('.DPA_CollapsingBlockHeader')
        .off('click.dpa')
        .on('click.dpa', function () {
            var headerEl = $(this);
            if (headerEl.hasClass('DPA_Clickable')) {
                if (headerEl.parent().hasClass('DPA_Collapsed')) {
                    modalEl.find('.DPA_CollapsingBlock').removeClass('DPA_Expanded').addClass('DPA_Collapsed');

                    headerEl.parent().removeClass('DPA_Collapsed').addClass('DPA_Expanded').trigger('expanded');
                } else {
                    var elToExpand = headerEl.parent().prev('.DPA_Collapsed');
                    if (elToExpand.length == 0) {
                        elToExpand = headerEl.parent().next('.DPA_Collapsed');
                    }

                    elToExpand.removeClass('DPA_Collapsed').addClass('DPA_Expanded').trigger('expanded');

                    headerEl.parent().removeClass('DPA_Expanded').addClass('DPA_Collapsed');
                }

                if (headerEl.parent().hasClass('DPA_CollapsingBlockNext')) {
                    that.hideSaveButton();
                }
                else if (headerEl.parent().hasClass('DPA_CollapsingBlockLast')) {
                    that.showSaveButton();
                }
            }
        });

    // bind button clicks
    modalEl.find('#DPA_CancelButton')
        .off('click.dpa')
        .on('click.dpa', function () {
            that.closeModal();
            modalEl.dialog('close');
        });

    modalEl.find('#DPA_UpdateRelationshipButton')
        .off('click.dpa')
        .on('click.dpa', function () {
            // validate selection
            if (that.selectedDatabaseInstanceId == null) {
                Ext.Msg.alert(localizedTexts.validationTitle, localizedTexts.validationMessage);
            } else {
                that.saveDataCallback({ GlobalDatabaseInstanceId: that.selectedDatabaseInstanceId, nodeId: that.selectedNodeId, applicationId: that.selectedApplicationId });
            }
        });
};