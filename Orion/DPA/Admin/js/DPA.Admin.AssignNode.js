﻿SW.Core.namespace("SW.DPA.Admin");
SW.DPA.Admin.AssignNode = function(databaseInstanceId) {
    var localizedTexts = {
        dialogTitle: '@{R=DPA.Strings;K=NodeSelectorModal_Title;E=js}',
        validationTitle: '@{R=DPA.Strings;K=NodeSelector_ValidationTitle;E=js}',
        validationMessage: '@{R=DPA.Strings;K=NodeSelector_ValidationMessage;E=js}',
        errorBoxTitle: '@{R=DPA.Strings;K=AssignDatabaseInstanceModal_ErrorTitle;E=js}',
        errorBoxMessage: '@{R=DPA.Strings;K=AssignDatabaseInstanceModal_ErrorMessage;E=js}'
    };
    var nodeSelector = null;
    var selectedNodeId = -1;

    var instanceConfig = {
        selectionChange: function (selection) {
            selectedNodeId = selection.length > 0 ? selection[0].data.NodeID : -1;
        },
        selectedItemsArray: [],
        createMainGridSelectionModel: function () {
            return new Ext.grid.SingleSelectionModel();
        }
    };
    nodeSelector = new SW.DPA.Admin.NodeSelector(instanceConfig);

    $('.DPA_SelectObject').click(function() {
        if (selectedNodeId != -1) {
            var returnUrl = $(this).attr('href');
            SW.Core.Services.callController(
                '/api/DpaRelationship/SaveDatabaseInstanceNode',
                { NodeId: selectedNodeId, DatabaseInstanceId: databaseInstanceId },
                function () {
                    window.location.href = returnUrl;
                    return true;
                },
                function (error) {
                    Ext.Msg.alert(localizedTexts.errorBoxTitle, error);
                });
        } else {
            Ext.Msg.alert(localizedTexts.validationTitle, localizedTexts.validationMessage);
        }

        return false;
    });
};
