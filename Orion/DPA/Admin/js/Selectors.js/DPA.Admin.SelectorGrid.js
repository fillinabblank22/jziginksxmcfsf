﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.SelectorGrid = Ext.extend(SW.DPA.Admin.GroupByGrid, {

    constructor: function (config) {
        var me = this;

        SW.DPA.Admin.SelectorGrid.superclass.constructor.call(this, config);
    },

    displayShowOnlyCombo: false,
    displayPagingToolbar: true,
    displaySearchTextControl: true,

    createMainGriDataStore: function () {
        return new ORION.WebApiStore(
            SW.Core.String.Format("/sapi/{0}/GetGridValues", this.controllerName),
            this.dataStoreMapping,
            "InstanceName");
    },

    createGroupingDataStore: function () {
        return new ORION.WebApiStore(
            SW.Core.String.Format("/sapi/{0}/GetGroupingValues", this.controllerName),
            [
                { name: 'Value', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'Type', mapping: 2 },
                { name: 'GroupingProperty', mapping: 3 }
            ]);
    },

    createGroupingGridDataStore: function () {
        return new ORION.WebApiStore(
            SW.Core.String.Format("/sapi/{0}/GetGroupGridValues", this.controllerName),
            [
                { name: 'Id', mapping: 0 },
                { name: 'Value', mapping: 1 },
                { name: 'Cnt', mapping: 2 },
                { name: 'SelectedCnt', mapping: 3 }
            ]);
    },

    saveSetting: function (key, value) {
    },

    mainPanelResize: function () {
        var me = this;

        var placeHolderElement = $('#' + me.getPlaceholderElementId());

        me.mainGridPanel.setWidth(1);
        me.mainGridPanel.setHeight(1);

        me.mainGridPanel.setWidth(placeHolderElement.width());

        var height = placeHolderElement.parent().height();

        placeHolderElement.height(height);
        me.mainGridPanel.setHeight(placeHolderElement.height());
    },

    getBaseParams: function () {
        return { ModelID: null, NodeID: this.config["NodeID"] };
    },

    onMainGridStoreLoaded: function () {
        var that = this;
        if (this.selectedItemsArray && this.selectedItemsArray.length > 0) {
            var selectedRowIndexes = [];
            var selectedIds = $.map(this.selectedItemsArray, function (item) {
                return that.getRecordId(item.data);
            });
            $.each(this.getMainGrid().store.data.items, function (i) {
                if (this.data && $.inArray(that.getRecordId(this.data), selectedIds) > -1)
                    selectedRowIndexes.push(i);
            });

            var selectionModel = this.getMainGrid().getSelectionModel();
            if (selectedRowIndexes.length > 0)
                selectionModel.selectRows(selectedRowIndexes, true);
        }
    }
});
