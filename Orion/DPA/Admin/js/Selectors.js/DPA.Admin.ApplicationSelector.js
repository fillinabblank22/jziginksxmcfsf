﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.ClientApplicationSelector = Ext.extend(SW.DPA.Admin.SelectorGrid, {
    localizedTexts: {
        emptySearchText: "@{R=DPA.Strings;K=IntegrationWizard_ApplicationSelector_EmptySearchText;E=js}",
        discoverApplicationsButtonCaption: "@{R=DPA.Strings;K=IntegrationWizard_ApplicationSelector_DiscoverButtonCaption;E=js}"
    },

    constructor: function (config) {
        var that = this;

        var defaultConfig = {
            invalidateServerFilter: function (modelId, onsuccess, onerror) { },
            onPagingToolbarRefresh: function() {
                that.invalidateServerFilter(that.getBaseParams().ModelID);
            }
        };

        this.renderNode = function (value, meta, record) {
            return that.renderEntityNameWithIcon(record.data.NodeInstanceType, record.data.NodeName, record.data.NodeStatus, record.data.NodeDetailsUrl, record.data.NodeID);
        }

        this.renderApplicationName = function (value, meta, record) {
            return that.renderEntityNameWithIcon(record.data.ApplicationInstanceType, record.data.ApplicationName, record.data.ApplicationStatus, record.data.ApplicationDetailsUrl, record.data.ApplicationID);
        }

        SW.DPA.Admin.ClientApplicationSelector.superclass.constructor.call(this, $.extend(true, defaultConfig, config || {}));

        this.init();
    },

    init: function() {
        var that = this;

        that.invalidateServerFilter(that.getBaseParams().ModelID);

        SW.DPA.Admin.ClientApplicationSelector.superclass.init.call(that);
    },

    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },

    getGridToolbarConfig: function () {
        var me = this;
        return [
            {
                id: 'DiscoverApplicationButton',
                text: this.localizedTexts.discoverApplicationsButtonCaption,
                icon: '/Orion/DPA/images/find-new-relationships.gif',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    var win = window.open('/Orion/APM/Admin/AutoAssign/SelectNodes.aspx', '_blank');
                    win.focus();
                    return false;
                }
            }
        ];
    },

    getRecordId: function (data) {
        return data.ApplicationID;
    },

    getEnvelopElementId: function () {
        return "applicationSelector";
    },

    getPlaceholderElementId: function () {
        return "applicationSelectorGridPlaceholder";
    },

    getMainGridId: function () {
        return "applicationSelectorGrid";
    },

    getBaseParams: function() {
        var params = $.extend(SW.DPA.Admin.GroupByGrid.prototype.getBaseParams.call(this), this.config.relatedData);
        return params;
    }
});
