﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.LunSelector = Ext.extend(SW.DPA.Admin.SelectorGrid,
{
    constructor: function(config) {
        var me = this;
        this.renderLun = function(value, meta, record) {
            return me.renderEntityNameWithIcon(record.data.LunInstanceType,
                record.data.LunName,
                record.data.LunStatus,
                record.data.LunDetailsUrl,
                record.data.LunId);
        };

        SW.DPA.Admin.LunSelector.superclass.constructor.call(this, config);

        // call init to render
        this.init();
    },

    createMainGridSelectorModel: function() {
        return new Ext.grid.SingleSelectionModel();
    },

    storeSelectionOnServer: function() {
    },

    updateSelectionFromGridData: function() {
    },

    getGridToolbarConfig: function() {
        return [];
    },

    getRecordId: function (data) {
        return data.LunId;
    },

    getEnvelopElementId: function () {
        return "lunSelector";
    },

    getPlaceholderElementId: function () {
        return "lunSelectorGridPlaceholder";
    },

    getMainGridId: function () {
        return "lunSelectorGrid";
    }
});