﻿SW.Core.namespace("SW.DPA.Admin");

SW.DPA.Admin.ClientApplicationCandidates = Ext.extend(SW.DPA.Admin.SelectorGrid, {

    constructor: function (config) {
        var that = this;

        this.renderApplicationName = function (value, meta, record) {
            return SW.DPA.Common.formatEntityOnNodeLink(
                record.data.ApplicationDetailsUrl,
                SW.DPA.Common.formatStatusIconUrl(record.data.ApplicationInstanceType,
                    record.data.ApplicationStatus,
                    record.data.ApplicationID),
                record.data.ApplicationName,
                record.data.NodeDetailsUrl,
                SW.DPA.Common.formatStatusIconUrl(record.data.NodeInstanceType,
                    record.data.NodeStatus,
                    record.data.NodeID),
                record.data.NodeName);
        }

        this.renderDatabaseInstanceName = function (value, meta, record) {
            return that.renderEntityNameWithIcon(record.data.DatabaseInstanceType, record.data.DatabaseInstanceName, record.data.DatabaseInstanceStatus, record.data.DatabaseInstanceDetailsUrl, record.data.DpaServerId);
        }

        SW.DPA.Admin.ClientApplicationCandidates.superclass.constructor.call(this, $.extend({}, config, { displaySearchTextControl: false, setMainGridPanelId: false }));

        this.init();
    },

    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },

    getGridToolbarConfig: function () {
        return [];
    },
    getRecordId: function (data) {
        return data.ApplicationID + '_' + data.GlobalDatabaseInstanceId;
    },

    getEnvelopElementId: function () {
        return "clientApplicationCandidateSelector";
    },

    getPlaceholderElementId: function () {
        return "clientApplicationCandidateSelectorGridPlaceholder";
    },

    getMainGridId: function () {
        return "clientApplicationCandidateSelectorGrid";
    },

    getBaseParams: function () {
        var params = $.extend(SW.DPA.Admin.GroupByGrid.prototype.getBaseParams.call(this), this.config.relatedData);
        return params;
    }
});