﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.DatabaseInstanceSelector = Ext.extend(SW.DPA.Admin.SelectorGrid, {
    localizedTexts: {
        instancesSelectedMessage: "@{R=DPA.Strings;K=IntegrationWizard_DatabaseInstancesSelected;E=js}",
        emptySearchText: "@{R=DPA.Strings;K=IntegrationWizard_DatabaseInstanceSelector_EmptySearchText;E=js}",
        addNewRelationshipButtonCaption: '@{R=DPA.Strings; K=IntegrationWizard_RegisterNewInstanceButtonText; E=js}'
    },

    constructor: function (config) {
        this.renderMonitorStatus = function (value, meta, record) {
            var msg = SW.Core.String.Format("<div class='DPA_MonitorStatus DPA_MonitorStatus-{0}'>{1}</div>", record.data.MonitorStatus, Ext.util.Format.htmlEncode(record.data.MonitorStatusText));
            return msg;
        }
        var me = this;
        this.renderDpaServerName = function (value, meta, record) {
            return me.renderEntityNameWithIcon(record.data.DpaServerInstanceType, record.data.DpaServerName, record.data.DpaServerStatus, record.data.DpaServerDetailsUrl, record.data.DpaServerId);
        };
        
        SW.DPA.Admin.DatabaseInstanceSelector.superclass.constructor.call(this, config);

        // call init to render
        this.init();
    },

    createMainGridSelectorModel: function () {
        return new Ext.grid.SingleSelectionModel();
    },

    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },

    getGridToolbarConfig: function () {
        return [
            {
                id: 'RegisterNewInstanceButton',
                text: this.localizedTexts.addNewRelationshipButtonCaption,
                icon: '/Orion/DPA/images/add_16x16.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    return SW.DPA.LinksToDpaSelector.show('/api/DpaLinksToDpaj/RegisterDbInstance/');
                }
            }
        ];
    },

    getRecordId: function (data) {
        return data.InstanceID;
    },

    getEnvelopElementId: function () {
        return "instanceSelector";
    },

    getPlaceholderElementId: function () {
        return "instanceSelectorGridPlaceholder";
    },

    getMainGridId: function () {
        return "instanceSelectorGrid";
    }
});
