﻿Ext.namespace('SW.DPA.Admin');

SW.DPA.Admin.NodeSelector = Ext.extend(SW.DPA.Admin.SelectorGrid, {
    localizedTexts: {
        instancesSelectedMessage: "@{R=DPA.Strings;K=IntegrationWizard_DatabaseInstancesSelected;E=js}",
        emptySearchText: "@{R=DPA.Strings;K=IntegrationWizard_NodeSelector_EmptySearchText;E=js}",
        addNodeButtonCaption: "@{R=DPA.Strings;K=IntegrationWizard_NodeSelector_AddNodeButtonCaption;E=js}"
    },

    constructor: function (config) {
        var that = this;
        
        SW.DPA.Admin.NodeSelector.superclass.constructor.call(this, config);

        this.renderNode = function (value, meta, record) {
            return that.renderEntityNameWithIcon(record.data.InstanceType, record.data.NodeName, record.data.Status, record.data.NodeDetailsUrl, record.data.NodeID);
        }

        this.init();
    },

    storeSelectionOnServer: function () {
    },

    updateSelectionFromGridData: function () {
    },

    getGridToolbarConfig: function () {
        return [
            {
                id: 'AddNodeButton',
                text: this.localizedTexts.addNodeButtonCaption,
                icon: '/Orion/DPA/images/add_16x16.png',
                cls: 'x-btn-text-icon',
                disabled: false,
                handler: function () {
                    var win = window.open('/Orion/Nodes/Add/', '_blank');
                    win.focus();
                    return false;
                }
            }
        ];
    },

    getRecordId: function (data) {
        return data.NodeID;
    },

    getEnvelopElementId: function () {
        return "nodeSelector";
    },

    getPlaceholderElementId: function () {
        return "nodeSelectorGridPlaceholder";
    },

    getMainGridId: function () {
        return "nodeSelectorGrid";
    }
});
