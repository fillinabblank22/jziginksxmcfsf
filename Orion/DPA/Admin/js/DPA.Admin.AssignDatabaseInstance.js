﻿SW.Core.namespace("SW.DPA.Admin");
SW.DPA.Admin.AssignDatabaseInstance = function (applicationId, nodeId) {
    var localizedTexts = {
        dialogTitle: '@{R=DPA.Strings;K=AssignDatabaseInstanceModal_Title;E=js}',
        validationTitle: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_ConfigurationDialog_ValidationTitle;E=js}',
        validationMessage: '@{R=DPA.Strings;K=IntegrationWizard_Relationships_ConfigurationDialog_ValidationMessage;E=js}',
        errorBoxTitle: '@{R=DPA.Strings;K=AssignDatabaseInstanceModal_ErrorTitle;E=js}',
        errorBoxMessage: '@{R=DPA.Strings;K=AssignDatabaseInstanceModal_ErrorMessage;E=js}'
    };

    var selectedDatabaseInstanceId = -1;
    var instanceSelector = null;
    var instanceConfig = {
        NodeID: nodeId,
        selectionChange: function (selection) {
            selectedDatabaseInstanceId = selection.length > 0 ? selection[0].data.InstanceID : -1;
        },
        selectedItemsArray: [],
        createMainGridSelectionModel: function () {
            return new Ext.grid.SingleSelectionModel();
        }
    };
    instanceSelector = new SW.DPA.Admin.DatabaseInstanceSelector(instanceConfig);

    $('.DPA_SelectObject').click(function () {
        var returnUrl = $(this).attr('href');
        if (selectedDatabaseInstanceId != -1) {
            SW.Core.Services.callController(
                '/api/DpaRelationship/SaveDatabaseInstanceApplication',
                { ApplicationId: applicationId, DatabaseInstanceId: selectedDatabaseInstanceId, NodeID: nodeId },
                function (result) {
                    if (result) {
                        window.location.href = returnUrl;
                        return true;
                    } else {
                        Ext.Msg.alert(localizedTexts.errorBoxTitle, localizedTexts.errorBoxMessage);
                    }
                },
                function (error) {
                    Ext.Msg.alert(localizedTexts.errorBoxTitle, error);
                });
        } else {
            Ext.Msg.alert(localizedTexts.validationTitle, localizedTexts.validationMessage);
        }

        return false;
    });
};