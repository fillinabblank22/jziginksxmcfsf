﻿Ext.ns('Ext.ux.grid');
Ext.ux.grid.RowInfo = Ext.extend(Ext.util.Observable, {
    id: 'rowinfo',
    rowClass: undefined,
    dataHandler: function(data){ return false; },

    constructor: function (config) {
        config = config || {};
        config.id = config.id || Ext.id();
        Ext.apply(this, config);

        Ext.ux.grid.RowInfo.superclass.constructor.call(this);

        if (this.tpl) {
            if (typeof this.tpl == 'string') {
                this.tpl = new Ext.Template(this.tpl);
            }
            this.tpl.compile();
        }
    },

    getRowClass: function (record, rowIndex, p, ds) {
        p.body = this.dataHandler.call(this.grid, record.data) ? this.tpl.apply(record.data) : null;
        return this.rowClass;
    },

    init: function (grid) {
        var scope = this,
            override = {
                getRowClass: function() { this.getRowClass.apply(scope, Array.prototype.slice.call(arguments)); },
                enableRowBody: true
            };

        this.grid = grid;
        $.extend(true, grid.viewConfig, override);
        $.extend(true, grid.getView(), override);
    }
});
Ext.preg('rowinfo', Ext.ux.grid.RowInfo);
Ext.grid.RowInfo = Ext.ux.grid.RowInfo;


Ext.ux.grid.BbarInfo = Ext.extend(Ext.util.Observable, {
    id: 'bbarinfo',

    constructor: function (config) {
        config = config || { showMessage: $.noop };
        config.id = config.id || Ext.id();
        Ext.apply(this, config);

        Ext.ux.grid.BbarInfo.superclass.constructor.call(this);
    },

    init: function (grid) {
        this.grid = grid;

        if (this.showNoficationBar) {
            var newTb = new Ext.Toolbar(this);

            grid.bottomToolbar.items.items.unshift(newTb);
        }
    }
});
Ext.preg('bbarinfo', Ext.ux.grid.RowInfo);
Ext.grid.RowInfo = Ext.ux.grid.RowInfo;
