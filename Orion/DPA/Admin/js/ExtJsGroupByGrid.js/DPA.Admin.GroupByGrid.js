﻿Ext.namespace('SW.DPA.Admin');
Ext.QuickTips.init();
Ext.Tip.prototype.autoWidth = true;

SW.DPA.Admin.GroupByGrid = Ext.extend(Object, {
    settingsPrefix: null,
    selectedRowsCount:0,
    localizedTexts: {
        showOnlyComboLabel: "@{R=DPA.Strings;K=IntegrationWizard_ShowOnlyCombo_Label;E=js}",
        instancesSelectedMessage: "@{R=DPA.Strings;K=IntegrationWizard_DatabaseInstancesSelected;E=js}",
        groupValueFormatWithoutSelection: "@{R=DPA.Strings;K=IntegrationWizard_GroupByGrid_ValueWithoutSelectedItems;E=js}",
        groupValueFormatWithSelection: "@{R=DPA.Strings;K=IntegrationWizard_GroupByGrid_ValueWithSelectedItems;E=js}",
        groupByLabel: "@{R=DPA.Strings; K=IntegrationWizard_Grid_GroupBy; E=js}",
        emptySearchText: "@{R=Core.Strings; K=WEBJS_PS0_44; E=js}",
        selectNodeMessage: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_SelectNodeMessage;E=js}",
        selectNodeButton: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_SelectNodeButton;E=js}",
        selectAppMessage: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_SelectApplicationMessage;E=js}",
        selectAppButton: "@{R=DPA.Strings;K=IntegrationWizard_Relationships_SelectApplicationButton;E=js}",
        pagingToolbarDisplayMsg: '@{R=DPA.Strings;K=IntegrationWizard_PagingToolbarDisplayMsg;E=js}',
        pagingToolbarRefreshTooltip: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
        applicationsWithoutRelationshipMessage: "@{R=DPA.Strings;K=IntegrationWizard_ApplicationsWithoutRelationshipMessage;E=js}",
        applicationsWithoutRelationshipLink: "@{R=DPA.Strings;K=IntegrationWizard_ApplicationsWithoutRelationshipLink;E=js}"
    },

    defaultConfig: {
        selectionChange: $.noop,
        createMainGridSelectionModel: function () {
            return new Ext.grid.FastCheckboxSelectionModel();
        },
        resetSelectedItemsOnRowSelect: false,
        setMainGridPanelId: true,
        onPagingToolbarRefresh: $.noop
    },

    destroy: function () {
        var that = this;
        that.mainGridPanel.destroy();
        that = undefined;
    },
    
    constructor: function (config) {
        var me = $.extend(true, this, this.defaultConfig, config || {});

        var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;

        this.localizedTexts = $.extend({}, me.superclass().localizedTexts, me.localizedTexts);
        this.config = config;

        Ext.grid.GroupByGridView = Ext.extend(Ext.grid.GridView, {
            fitColumns: function (preventRefresh, onlyExpand, omitColumn) {
                var colModel = this.cm,
                totalColWidth = colModel.getTotalWidth(false),
                gridWidth = this.getGridInnerWidth(),
                extraWidth = gridWidth - totalColWidth,
                columns = [],
                extraCol = 0,
                width = 0,
                colWidth, fraction, i;

                if (gridWidth < 20 || extraWidth === 0) {
                    return false;
                }

                var visibleColCount = colModel.getColumnCount(true),
                    totalColCount = colModel.getColumnCount(false),
                    adjCount = visibleColCount - (Ext.isNumber(omitColumn) ? 1 : 0);

                if (adjCount === 0) {
                    adjCount = 1;
                    omitColumn = undefined;
                }

                var colWidthCorrection = 0;

                for (i = 0; i < totalColCount; i++) {
                    if (!colModel.isFixed(i) && i !== omitColumn) {
                        colWidth = colModel.getColumnWidth(i);
                        if (i == 0)
                            colWidth -= colWidthCorrection;

                        columns.push(i, colWidth);

                        if (!colModel.isHidden(i)) {
                            extraCol = i;
                            width += colWidth;
                        }
                    }
                }

                fraction = (gridWidth - colModel.getTotalWidth() + colWidthCorrection) / width;

                while (columns.length) {
                    colWidth = columns.pop();
                    i = columns.pop();
                    width = Math.floor(colWidth + colWidth * fraction);
                    colModel.setColumnWidth(i, width, true);
                }

                totalColWidth = colModel.getTotalWidth(false);
                totalColWidth -= colWidthCorrection;

                if (totalColWidth > gridWidth) {
                    var adjustCol = (adjCount == visibleColCount) ? extraCol : omitColumn,
                        newWidth = Math.max(1, colModel.getColumnWidth(adjustCol) - (totalColWidth - gridWidth));

                    colModel.setColumnWidth(adjustCol, newWidth, true);
                }

                if (preventRefresh !== true) {
                    this.updateAllColumnWidths();
                }

                return true;
            }
        });

        Ext.grid.SingleSelectionModel = Ext.extend(Ext.sw.grid.RadioSelectionModel, {
            id: me.getMainGridId(),
            selectRow: function (index, keepExisting, preventViewNotify, dontFireEvents) {
                if (this.locked || (index < 0 || index >= this.grid.store.getCount()) || this.isSelected(index)) return;

                Ext.grid.SingleSelectionModel.superclass.selectRow.apply(this, arguments);

                var r = this.grid.store.getAt(index);

                if (!dontFireEvents) {
                    this.fireEvent("customRowdeselect", this, index, r);
                    this.fireEvent("customSelectionchange", this);
                }
            },
            deselectRow: function (index, preventViewNotify, dontFireEvents) {
                if (this.locked || (index < 0 || index >= this.grid.store.getCount()) || !this.isSelected(index)) return;

                Ext.grid.SingleSelectionModel.superclass.deselectRow.apply(this, arguments);

                var r = this.grid.store.getAt(index);

                if (!dontFireEvents) {
                    this.fireEvent("customRowdeselect", this, index, r);
                    this.fireEvent("customSelectionchange", this);
                }
            },
            hideable: false,
            menuDisabled: true
        });

        Ext.grid.FastCheckboxSelectionModel = Ext.extend(Ext.grid.CheckboxSelectionModel, {
            selectAll: function () {
                if (this.locked) return;
                this.selections.clear();
                for (var i = 0, len = this.grid.store.getCount() ; i < len; i++) {
                    if (i == (len - 1)) {
                        this.selectRow(i, true);
                    }
                    else {
                        this.selectRow(i, true, false, true);
                    }
                }
            },

            selectRow: function (index, keepExisting, preventViewNotify, dontFireEvents) {
                if (this.locked || (index < 0 || index >= this.grid.store.getCount()) || this.isSelected(index)) return;

                var r = this.grid.store.getAt(index);
                this.selections.add(r);
                this.last = this.lastActive = index;
                if (!preventViewNotify) {
                    this.grid.getView().onRowSelect(index);
                }

                this.fireEvent("rowselect", this, index, r);
                this.fireEvent("selectionchange", this);

                if (!dontFireEvents) {
                    this.fireEvent("customRowselect", this, index, r);
                    this.fireEvent("customSelectionchange", this);
                }
            },

            clearSelections: function (fast) {
                if (this.locked)
                    return;

                if (fast !== true) {
                    var ds = this.grid.store;
                    var s = this.selections;
                    var length = s.items.length - 1;

                    s.each(function (r, index) {
                        if (index != length) {
                            this.deselectRow(ds.indexOfId(r.id), false, true);
                        } else {
                            this.deselectRow(ds.indexOfId(r.id));
                        }
                    }, this);
                    s.clear();
                } else {
                    this.selections.clear();
                }

                this.last = false;
            },

            deselectRow: function (index, preventViewNotify, dontFireEvents) {
                if (this.locked) return;
                if (this.last == index) {
                    this.last = false;
                }

                if (this.lastActive == index) {
                    this.lastActive = false;
                }

                var r = this.grid.store.getAt(index);
                if (r) {
                    this.selections.remove(r);
                    if (!preventViewNotify) {
                        this.grid.getView().onRowDeselect(index);
                    }

                    this.fireEvent("rowdeselect", this, index, r);
                    this.fireEvent("selectionchange", this);

                    if (!dontFireEvents) {
                        this.fireEvent("customRowdeselect", this, index, r);
                        this.fireEvent("customSelectionchange", this);
                    }
                }
            }
        });

        var selectorModel = null;
        this.gridPageSize = 20;
        var gridDataStore = null;

        this.showOnlyComboBox = null;
        var showOnlyDataStore = null;

        var groupingDataStore = null;
        var groupingStore = null;
        this.groupingValue = null;
        this.groupByComboBox = null;
        var groupByTopPanel = null;

        var navPanel = null;
        this.groupGrid = null;
        var groupGridSelectedRowsIndexes = [];
        var mainGrid = null;
        var mainGridFilters = null;
        var pagingToolbar = null;
        this.mainGridPanel = null;
        this.sortOrder = null;

        this.selectedItemsArray = (this.config ? this.config.selectedItemsArray : []);

        // generated from server code
        this.dataStoreMapping = [];


        // PRIVATE METHODS
        var jsonParse = function (jsonString) {
            return JSON.parse(jsonString, function (key, value) {
                if (value
                    && typeof value === "string"
                    && value.indexOf("Func:") == 0) {
                    try {
                        return eval(value.substring("Func:".length));
                    } catch (err) {
                        return eval('me.' + value.substring("Func:".length));
                    }
                }
                return value;
            });
        };

        this.updateToolbarButtons = function () {
            updateHeaderChecker();
        };

        function selectRow(grid, rowIndex, webStore) {
            if (typeof me.selectedItemsArray == 'undefined') {
                me.selectedItemsArray = [];
            }

            // add row into selected items collection
            var item = grid.store.getAt(rowIndex),
                index = getSelectedItemIndex(item);

            if (me.resetSelectedItemsOnRowSelect) {
                me.selectedItemsArray = [];
            }

            if (index == -1)
                me.selectedItemsArray.push(item);
            else
                me.selectedItemsArray[index] = item;
        }

        function getSelectedItemIndex(item) {
            var index = -1;

            if (item && item.data) {
                $.each(me.selectedItemsArray, function (i) {
                    if (this.data &&
                        me.getRecordId(item.data) == me.getRecordId(this.data)) {
                        index = i;
                        return false;
                    }

                    return true;
                });
            }

            return index;
        }

        function deselectRow(grid, rowIndex, webStore) {

            //remove row from selected collection
            var item = grid.store.getAt(rowIndex),
                index = getSelectedItemIndex(item);

            if (index != -1)
                me.selectedItemsArray.splice(index, 1);
        }

        // Escapes braces (which can be interpreted by extjs as html tags and break showing of tooltip) with whitespaces, eg. '<test>' -> '< test >'
        function escapeWithWhiteSpaces(str) {
            return str.replace(new RegExp("&lt;", 'g'), "&lt; ").replace(new RegExp("&gt;", 'g'), " &gt;");
        }

        // Encoding value function
        function encodeHTML(value) {
            return Ext.util.Format.htmlEncode(value);
        }

        function updateHeaderChecker() {
            var hd = Ext.fly(mainGrid.getView().innerHd).child('div.x-grid3-hd-checker');

            if (!hd) return;

            if (mainGrid.getStore().getCount() > 0 && mainGrid.getSelectionModel().getCount() == mainGrid.getStore().getCount()) {
                hd.addClass('x-grid3-hd-checker-on');
            }
            else {
                hd.removeClass('x-grid3-hd-checker-on');
            }
        }

        this.updateSelectionOnServer = function () {
            var selectRequest = [];
            $.each(me.selectedItemsArray, function () {
                selectRequest.push(this.data);
            });

            var deselectRequest = [];
            $.each(mainGrid.store.data.items, function () {
                if ($.inArray(this.data, selectRequest) == -1) {
                    deselectRequest.push(this.data);
                }
            });

            me.storeSelectionOnServer(selectRequest, deselectRequest, me.getBaseParams().ModelID);
        }

        this.reloadGroupByGrid = function () {
            var store = me.groupGrid.store,
                sm = me.groupGrid.getSelectionModel(),
                sel = sm.getSelected(),
                lastSelection = sel && sel.data && sel.data['Id'];

            me.groupGrid.store.load({
                callback: function () {
                    if (store.getCount() <= 0) return;

                    var rowIndex = store.find('Id', lastSelection, false, true);
                    (rowIndex >= 0) && sm.selectRow(rowIndex, false, false);
                }
            });
        }

        this.reloadElementStore = function (elem, property, type, value, groupingProperty, search, callback) {
            elem.store.removeAll();
            elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", groupingProperty: groupingProperty, search: search, baseParams: me.getBaseParams(), additionalFilters: [getFilterFromShowOnlyComboBox()] };
            currentSearchTerm = search;
            elem.store.load({ callback: callback });
        };

        this.loadGrid = function (callback) {
            me.reloadGroupByGrid();
            me.reloadMainGridStore(0, me.gridPageSize, mainGrid, "", "", "", me.filterText, callback);
        };

        this.countEntriesInMainGrid = function() {
            return mainGrid.store.totalLength;
        };

        this.reloadMainGridStore = function (start, limit, elem, property, type, value, search, callback) {
            // Following 3 rows of code solve searching functionality, when search is canceled
            me.filterPropertyParam = property;
            me.filterTypeParam = type;
            me.filterValueParam = value;
            var filters = [
                this.config && this.config.filter || null,
                getFilter(property, type, value),
                getFilterFromShowOnlyComboBox()
            ];

            reloadMainGridStoreExt(start, limit, elem, filters, getFiltersForSearchText(search), function () {
                if (callback) callback();
                processTooltips();
            });
        };

        function reloadMainGridStoreExt(start, limit, elem, additionalFilters, searchFilters, callback) {
            elem.store.removeAll();

            var filters = $.grep(additionalFilters, function (item) { return item; });

            elem.store.proxy.conn.jsonData = { additionalFilters: filters, searchFilters: searchFilters, baseParams: me.getBaseParams() };
            elem.store.load({ params: { start: start, limit: limit }, callback: callback });
        };

        function getFilter(property, type, value, comparison) {
            var filter = null;
            if (property && type) {
                filter = { field: property, data: { type: type, value: value, comparison: comparison || (value ? 'eeq' : 'isnull') } };
            }
            return filter;
        };

        function getFilterFromShowOnlyComboBox() {
            var filter = null;

            if (me.displayShowOnlyCombo) {
                var record = me.showOnlyComboBox.store.data.items[me.showOnlyComboBox.selectedIndex];
                if (record) {
                    filter = getFilter(record.data.Name, record.data.Type, record.data.Value);
                }
            }

            return filter;
        };

        function getFiltersForSearchText(searchTerm) {
            var filters = [];

            if (me.displaySearchTextControl && searchTerm) {
                var searchableVisibleColumns = me.getMainGrid().colModel.getColumnsBy(function(c) { return c.searchable && !c.hidden; });
                $.each(searchableVisibleColumns, function () {
                        filters.push(getFilter(this.dataIndex, this.type, searchTerm, 'eq'));
                });
            }

            return filters;
        };

        this.createDataStores = function () {
            // CREATE DATA STORES
            gridDataStore = me.createMainGriDataStore();

            gridDataStore.sortInfo = { field: this.sortOrder.SortColumn, direction: this.sortOrder.SortDir };

            gridDataStore.on("beforeload", function (s, params) {
                me.getMainGrid().getView().emptyText = '';
                reconfigureFilterNotifications();
            });

            showOnlyDataStore = me.createShowOnlyDataStore();

            groupingDataStore = me.createGroupingDataStore();

            groupingStore = me.createGroupingGridDataStore();
        }

        function getAllSelectedItems() {
            var allSelectedItems = new ORION.WebApiStore(
                           "/sapi/DpaRelationshipsGrid/GetRelationshipsTable",
                           dataStoreMapping,
                           "InstanceName"
                       );
            allSelectedItems.on("beforeload", function (s, requestParam) {
                //Set limit as totalLength of grid records 
                //Need for select all items in grid not only on current page
                requestParam.params.limit = mainGrid.store.totalLength;
                me.reconfigureFilterNotifications();
            });
            return allSelectedItems;
        }

        // METHODS FOR LEFT NAVIGATION PANEL
        this.createGroupByTopPanel = function () {
            groupByTopPanel = new Ext.Panel({
                split: false,
                collapsible: false,
                flex: 0,
                layout: 'anchor',
                items: [
                    getGroupByPanelItems()
                ],
                cls: 'panel-no-border panel-bg-gradient'
            });
        }

        var getGroupByPanelItems = function () {

            var items = [
                me.showOnlyComboBox,
                {
                    xtype: 'label',
                    text: "@{R=DPA.Strings; K=IntegrationWizard_Grid_GroupBy; E=js}",
                    cls: 'DPA_GridComboTitle',
                },
                me.groupByComboBox
            ];

            if (!me.displayShowOnlyCombo) {
                items = [
                    me.groupByComboBox
                ];
            }

            return items;
        }

        this.createShowOnlyComboBox = function () {
            if (!me.displayShowOnlyCombo) return;

            me.showOnlyComboBox = new Ext.form.ComboBox({
                fieldLabel: 'Caption',
                hiddenName: 'Value',
                store: showOnlyDataStore,
                displayField: 'Caption',
                triggerAction: 'all',
                value: '@{R=DPA.Strings;K=IntegrationWizard_ShowOnlyComboBox_All; E=js}',
                typeAhead: true,
                mode: 'local',
                forceSelection: true,
                selectOnFocus: false,
                anchor: '100%'
            });

            me.showOnlyComboBox.on('select', function () {
                me.internalGridRefresh();
            });
        };

        this.createGroupByComboBox = function () {
            this.groupByComboBox = new Ext.form.ComboBox({
                fieldLabel: 'Name',
                hiddenName: 'Value',
                store: groupingDataStore,
                displayField: 'Name',
                triggerAction: 'all',
                value: '@{R=DPA.Strings;K=IntegrationWizard_Grid_NoGrouping; E=js}',
                typeAhead: true,
                mode: 'local',
                forceSelection: true,
                selectOnFocus: false,
                anchor: '100%'
            });

            this.groupByComboBox.on('select', function () {
                var val = this.store.data.items[this.selectedIndex].data.Value;
                var groupingProperty = this.store.data.items[this.selectedIndex].data.GroupingProperty;
                me.reloadElementStore(me.groupGrid, val, "", "", groupingProperty, "");
                var newGroupingValue = getGroupingValue(val);
                newGroupingValue.SubGroupValue = "[All]";
                me.saveSetting('GroupingValue', JSON.stringify(newGroupingValue));
                me.reloadMainGridStore(0, me.gridPageSize, mainGrid, "", "", "", me.filterText);
            });
        }

        function getGroupingValue(val) {
            var groupName = me.groupByComboBox.selectedIndex != -1 ? me.groupByComboBox.store.data.items[me.groupByComboBox.selectedIndex].data.Name : me.groupingValue.Name;
            var groupValue = me.groupByComboBox.selectedIndex != -1 ? me.groupByComboBox.store.data.items[me.groupByComboBox.selectedIndex].data.Value : me.groupingValue.Value;
            var groupType = me.groupByComboBox.selectedIndex != -1 ? me.groupByComboBox.store.data.items[me.groupByComboBox.selectedIndex].data.Type : me.groupingValue.Type;
            var groupGroupingProperty = me.groupByComboBox.selectedIndex != -1 ? me.groupByComboBox.store.data.items[me.groupByComboBox.selectedIndex].data.GroupingProperty : me.groupingValue.GroupingProperty;
            return { Name: groupName, Value: groupValue, Type: groupType, GroupingProperty: groupGroupingProperty, SubGroupValue: val };
        }

        // Group grid which is displayed bellow Group By Combo box
        this.createGroupGrid = function () {
            this.groupGrid = new Ext.grid.GridPanel({
                border: false,
                flex: 1,
                id: 'groupingGrid',
                store: groupingStore,
                cls: 'hide-header',
                columns: [
                    {
                        width: 193,
                        editable: false,
                        sortable: false,
                        dataIndex: 'Value',
                        renderer: function(value, meta, record) {
                            return me.renderGroup.call(me, value, meta, record);
                        }
                    }
                ],
                selModel: new Ext.grid.RowSelectionModel(),
                autoScroll: 'true',
                loadMask: true,
                listeners: {
                    cellclick: function (mygrid, row, cell, e) {
                        var val = mygrid.store.data.items[row].data.Id;
                        a = reMsAjax.exec(val);
                        if (a) {
                            var b = a[1].split(/[-,.]/);
                            var dateVal = new Date(+b[0]);
                            val = dateVal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.SortableDateTimePattern);
                        }

                        var newGroupingValue = getGroupingValue(val);
                        me.saveSetting('GroupingValue', JSON.stringify(newGroupingValue));
                        me.groupingValue = newGroupingValue;

                        me.reloadMainGridStore(0, me.gridPageSize, mainGrid, newGroupingValue.Value, newGroupingValue.Type, newGroupingValue.SubGroupValue, me.filterText);
                    }
                },
                anchor: '0 0',
                viewConfig: { forceFit: true },
                split: true,
                autoExpandColumn: 'Value'
            });

            this.groupGrid.store.on('beforeload', function () {
                var json = this.proxy.conn.jsonData,
                    filter = me.config && me.config.filter || null;
                json['additionalFilters'] = filter ? [filter] : [];
                json['searchFilters'] = getFiltersForSearchText(me.filterText);
            });

            this.groupGrid.store.on('load', function (records, operation, success) {
                var selectionModel = me.groupGrid.getSelectionModel();
                for (var i = 0; i < groupGridSelectedRowsIndexes.length; i++) {
                    selectionModel.selectRow(groupGridSelectedRowsIndexes[i]);
                }

                groupGridSelectedRowsIndexes = []; // we want to clear array
                me.updateToolbarButtons();

                if (me.updateSelectedRowsMessage) {
                    me.updateSelectedRowsMessage(me.selectedRowsCount);
                }
            });
        }

        this.createNavigationPanel = function () {
            navPanel = new Ext.Panel({
                border: false,
                region: 'west',
                split: true, anchor: '0 0',
                width: 200,
                layout: { type: 'vbox', pack: 'start', align: 'stretch' },
                collapsible: true,
                title: (me.displayShowOnlyCombo ? me.localizedTexts.showOnlyComboLabel : me.localizedTexts.groupByLabel) + ":",
                viewConfig: {
                    forceFit: true
                },
                items: [
                    groupByTopPanel,
                    this.groupGrid
                ],
                cls: 'panel-no-border DPA_IW_SidePanel'
            });
        }

        // METHODS FOR FILTERING FUNCTIONALITY
        this.getGridColumHeader = function (name, displayName) {
            return SW.Core.String.Format('<img class="filter-icon" data-column-name="{1}" src="/Orion/js/extjs/resources/images/default/s.gif" alt="" style="vertical-align: middle; padding-top:1px; height: 16px; width:16px"/><span style="vertical-align: middle;" title="{0}">{0}</span>&nbsp;', (displayName == '') ? name : displayName, name);
        }

        function formatDateValue(value) {
            if (Ext.isEmpty(value))
                return '';
            var a = reMsAjax.exec(value);
            if (a) {
                var b = a[1].split(/[-,.]/);
                var val = new Date(+b[0]);
                return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
            }

            var b = new Date(value);
            if (b) {
                return b.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern);
            }
            return '';
        }

        function getMainGridFilters() {
            if (mainGridFilters == null) {
                mainGridFilters = new Ext.ux.grid.GridFilters({
                    menuFilterText: '@{R=DPA.Strings;K=IntegrationWizard_Grid_Filters; E=js}',
                    encode: false,
                    local: false
                });
            }

            return mainGridFilters;
        }

        function getFilterNotificationMessage(filter) {
            var displayName = filter.field;

            if (Ext.isEmpty(filter.data.value)) {
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_RB0_2; E=js}', displayName);// '&lt;b&gt;{0}&lt;/b&gt;: is empty'
            }
            if (filter.data.type == 'numeric') {
                var floatVal = String(filter.data.value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                if (filter.data.comparison == 'lt')
                    return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_16; E=js}', displayName, floatVal);// '&lt;b&gt;{0}&lt;/b&gt;: less than {1}'
                if (filter.data.comparison == 'gt')
                    return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_15; E=js}', displayName, floatVal);// '&lt;b&gt;{0}&lt;/b&gt;: greater than {1}'
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_17; E=js}', displayName, floatVal);// '&lt;b&gt;{0}&lt;/b&gt;: equals {1}'
            }
            if (filter.data.type == 'date') {
                var dateVal = formatDateValue(filter.data.value, false);
                if (filter.data.comparison == 'lt')
                    return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_18; E=js}', displayName, dateVal);// '&lt;b&gt;{0}&lt;/b&gt;: before &apos;{1}&apos;'
                if (filter.data.comparison == 'gt')
                    return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_19; E=js}', displayName, dateVal);// '&lt;b&gt;{0}&lt;/b&gt;: after &apos;{1}&apos;'
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_20; E=js}', displayName, dateVal);// '&lt;b&gt;{0}&lt;/b&gt;: on &apos;{1}&apos;'
            }
            if (filter.data.type == 'boolean') {
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, (filter.data.value ? '@{R=Core.Strings;K=Boolean_true; E=js}' : '@{R=Core.Strings;K=Boolean_false; E=js}'));// '&lt;b&gt;{0}&lt;/b&gt;: {1}'
            }
            if (filter.data.type.toString().startsWith('list')) {
                return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value.split('$#').join("; ")));// '&lt;b&gt;{0}&lt;/b&gt;: {1}'
            }

            return SW.Core.String.Format('@{R=Core.Strings;K=WEBJS_IB0_14; E=js}', displayName, Ext.util.Format.htmlEncode(filter.data.value));// '&lt;b&gt;{0}&lt;/b&gt;: {1}'
        }

        function showNotification(id, message) {
            var notificationTemplate = '\
        <div id="Hint" style="position: static;" runat="server"> \
            <div id="{0}" class="sw-notification">{1}</div> \
        </div>';

            var msg = SW.Core.String.Format(notificationTemplate, id, message);

            var $notificationPanel = $("#notifications");
            $notificationPanel.html(msg);
        }

        function clearNotification(identificator) {
            $('#ConnectionIssue').hide();
            if (identificator) {
                var ntf = $("#notifications #" + identificator);
                if (ntf.length > 0) {
                    $("#notifications").html('');
                }
            }
            else
                $("#notifications").html('');
        }

        function reconfigureFilterNotifications() {
            var grid = me.getMainGrid();
            var filterData = grid.filters.getFilterData();
            var notificationTemplate = '\
        <div id="Hint" style="position: static;" runat="server"> \
            <div class="sw-filter"> \
                <table cellpadding="0" cellspacing="0"> \
                    <tr> \
                        <td><span class="sw-filter-icon sw-filter-icon-hint"></span></td> \
                        <td><span style="vertical-align: middle; padding-right: 10px">{0}</span></td> \
                        <td><span><a class="DPA_RemoveFilter" href="#" data-name="{1}" data-comparison="{2}"><img id="deleteButton" src="/Orion/Nodes/images/icons/icon_delete.gif" style="cursor: pointer;" /></a></td> \
                    </tr> \
                </table> \
            </div> \
        </div>';

            $("#filterNotifications").each(function () {
                $(this).empty();

                var filtersConfigToSave = [];
                for (var i = 0; i < filterData.length; i++) {
                    var comparison = (filterData[i].data.comparison == undefined) ? "" : filterData[i].data.comparison;
                    var name = filterData[i].field;
                    var notification = $(SW.Core.String.Format(notificationTemplate, getFilterNotificationMessage(filterData[i]), name, comparison));
                    $(this).append(notification);

                    // set onclick
                    notification.find('.DPA_RemoveFilter').click(function () {
                        removeFilter($(this).data('name'), $(this).data('comparison'));
                    });

                    filtersConfigToSave.push({ ColumnName: filterData[i].field, Value: filterData[i].data.configValue ? filterData[i].data.configValue : filterData[i].data.value });
                }

                me.saveSetting('LastUsedFilters', JSON.stringify(filtersConfigToSave));
            });
        }

        function removeFilter(name, comparison) {
            var grid = me.getMainGrid();
            for (var i = 0; i < grid.filters.filters.items.length; i++) {
                if (grid.filters.filters.items[i].active && grid.filters.filters.items[i].dataIndex == name) {
                    // for one numeric column we're able to define several filters (less then, greater then etc.)
                    // so we need to clear filter with exact the same comparison
                    switch (grid.filters.filters.items[i].type) {
                        case "numeric":
                            grid.filters.filters.items[i].disableFilter(comparison);
                            break;
                        case "date":
                            grid.filters.filters.items[i].disableFilter(comparison);
                            break;
                        case "dateTime":
                            grid.filters.filters.items[i].disableFilter(comparison);
                        case "list":
                            grid.filters.filters.items[i].disableFilter();
                            break;
                        case "string":
                            grid.filters.filters.items[i].disableFilter();
                            break;
                        default:
                            grid.filters.filters.items[i].active = false;
                            break;
                    }
                }
            }
            // after remove filter go to first page
            grid.store.lastOptions.params.start = 0;
            // atrer filter changes we need to reload grid store
            grid.store.reload();
        }

        this.showFilter = function (evt, name, grid) {
            evt = (evt) ? evt : window.event;
            evt.cancelBubble = true;
            
            for (var i = 0; i < grid.filters.filters.items.length; i++) {
                if (grid.filters.filters.items[i].dataIndex == name) {
                    var menu = grid.filters.filters.items[i].menu;
                    var event = Ext.EventObject;
                    menu.showAt(event.getXY());
                }
            }
            return false;
        };

        function getFloatFilterControl(restrictedValues) {
            if (restrictedValues.length > 0) {
                var stringArray = new Array();

                for (var i = 0; i < restrictedValues.length; i++) {
                    var strVal = String(restrictedValues[i]).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator);
                    stringArray.push(strVal);
                }
                return {
                    type: 'list',
                    options: stringArray,
                    getSerialArgs: function () {
                        var args = { type: 'list$system.single', value: this.getValue().join('$#') };
                        return args;
                    }
                };
            }
            else {
                return {
                    type: 'numeric',
                    menuItemCfgs: {
                        emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',// 'Enter Filter Text...'
                        decimalSeparator: Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator,
                        allowDecimals: true,
                        allowNegative: true,
                        maxValue: 3.40282347E+38,
                        minValue: -3.40282347E+38,
                        decimalPrecision: 7
                    }
                };
            }
        };

        function getIntegerFilterControl(restrictedValues) {
            if (restrictedValues.length > 0) {
                var stringArray = new Array();
                for (var y = 0; y < restrictedValues.length; y++) {
                    stringArray.push(restrictedValues[y].toString());
                }
                return {
                    type: 'list',
                    options: stringArray,
                    getSerialArgs: function () {
                        var args = { type: 'list$system.int32', value: this.getValue().join('$#') };
                        return args;
                    }
                };
            }
            else {
                return {
                    type: 'numeric',
                    menuItemCfgs: {
                        emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',// 'Enter Filter Text...'
                        allowDecimals: false,
                        allowNegative: true,
                        maxValue: 2147483647,
                        minValue: -2147483648
                    },
                    getSerialArgs: function() {
                        var currentValue = this.getValue();
                        var args = Ext.ux.grid.filter.NumericFilter.prototype.getSerialArgs.call(this);
                        if (args && _.isArray(args) && args.length > 0) {
                            $.each(args, function() {
                                Ext.apply(this, { configValue: currentValue });
                            });
                        }
                        return args;
                    }
                };
            }
        }

        function getStringFilterControl(restrictedValues, size) {
            if (restrictedValues.length > 0) {
                var stringArray = new Array();
                for (var i = 0; i < restrictedValues.length; i++) {
                    stringArray.push(Ext.util.Format.htmlEncode(restrictedValues[i].toString()));
                }
                return {
                    type: 'list',
                    options: stringArray,
                    getSerialArgs: function () {
                        var args = { type: 'list$system.string', value: Ext.util.Format.htmlDecode(this.getValue().join('$#')) };
                        return args;
                    }
                };
            }
            else {
                return {
                    type: 'string',
                    emptyText: '@{R=Core.Strings;K=WEBJS_SO0_46; E=js}',// 'Enter Filter Text...'
                    maxLength: (size > 0) ? size : Number.MAX_VALUE
                };
            }
        }

        // METHODS FOR MAIN GRID
        function getGridSelectorModel() {
            if (selectorModel == null) {
                selectorModel = me.createMainGridSelectionModel();

                selectorModel.on("customSelectionchange", function () {
                    me.onCustomSelectionChanged();
                });

                selectorModel.on("rowselect", function (sm, rowIndex) {
                    selectRow(mainGrid, rowIndex, function () {
                        return getAllSelectedItems();
                    });
                });

                selectorModel.on("customRowselect", function (sm, rowIndex) {
                    me.onSelectionChanged(mainGrid, function () {
                        return getAllSelectedItems();
                    });
                });

                selectorModel.on("rowdeselect", function (sm, rowIndex) {
                    deselectRow(mainGrid, rowIndex, function () {
                        return getAllSelectedItems();
                    });
                });

                selectorModel.on("customRowdeselect", function (sm, rowIndex) {
                    me.onSelectionChanged(mainGrid, function () {
                        return getAllSelectedItems();
                    });
                });
            }

            return selectorModel;
        };

        function getGridColumnsModelConfig() {
            me.columnsModelConfig = me.columnsModelConfig || jsonParse.call(this, me.loadSetting("ColumnsModelConfig"));
            return me.columnsModelConfig;
        }

        this.createMainGrid = function () {
            var selectedColumns = me.loadSetting('SelectedColumns', '').split(',');

            var gridToolbarItems = me.getGridToolbarConfig();
            if (me.displaySearchTextControl) {
                gridToolbarItems.push('->');
                gridToolbarItems.push(new Ext.ux.form.SearchField({
                    initComponent: function () {
                        Ext.ux.form.SearchField.superclass.initComponent.call(this);

                        var gridId = me.getMainGridId();
                        this.triggerConfig = {
                            tag: 'span', cls: 'x-form-twin-triggers', cn: [
                            { id: "clearbutton_" + gridId, tag: "img", src: "/Orion/images/clear_button.gif", cls: "x-form-trigger " + this.trigger1Class },
                            { id: "searchbutton_" + gridId, tag: "img", src: "/Orion/images/search_button.gif", cls: "x-form-trigger " + this.trigger2Class }
                            ]
                        };

                        this.addEvents('searchStarted');

                        this.on('specialkey', function (f, e) {
                            if (e.getKey() == e.ENTER) {
                                this.onTrigger2Click();
                            }
                        }, this);

                        // set search field text styles
                        this.on('focus', function () {
                            this.el.applyStyles('font-style:normal;');
                        }, this);

                        // reset search field text style
                        this.on('blur', function () {
                            if (Ext.isEmpty(this.el.dom.value) || this.el.dom.value == this.emptyText) {
                                this.el.applyStyles('font-style:italic;');
                            }
                        }, this);
                    },

                    store: me.gridDataStore,
                    width: 200,
                    emptyText: me.localizedTexts.emptySearchText,
                    listeners: {
                        searchStarted: function() { me.selectedItemsArray = []; }
                    },
                    id: 'searchField_' + me.getMainGridId(),
                    // remove button
                    onTrigger1Click: function () {
                        if (this.hasSearch) {
                            this.el.dom.value = '';
                            me.filterText = this.getRawValue();

                            me.reloadGroupByGrid();
                            me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);

                            this.triggers[0].hide();
                            this.hasSearch = false;
                            this.fireEvent('searchStarted', me.filterText);
                        }
                    },
                    //  this is the search icon button in the search field
                    onTrigger2Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        me.filterText = this.getRawValue();

                        if (!me.filterText || me.filterText == '') {
                            this.onTrigger1Click();
                            return;
                        }

                        me.reloadGroupByGrid();
                        me.reloadMainGridStore(0, me.gridPageSize, me.getMainGrid(), me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);

                        this.hasSearch = true;
                        this.triggers[0].show();

                        this.fireEvent('searchStarted', me.filterText);
                    }
                }));
            }

            mainGrid = new Ext.grid.GridPanel({
                id: me.getMainGridId(),
                region: 'center',
                store: gridDataStore,
                split: true,
                stripeRows: true,
                forceLayout: true,
                trackMouseOver: false,
                viewConfig: {
                    forceFit: true,
                    getRowClass: function (record, index) {
                        return "sw-rowClass";
                    },
                    deferEmptyText: false,
                    emptyText: ''
                },
                columns: getGridColumnsModelConfig.call(this),
                sm: getGridSelectorModel(),
                plugins: [
                    getMainGridFilters(),
                    new Ext.ux.grid.RowInfo({
                        rowClass: "sw-rowClass",
                        dataHandler: function (data) {
                            var d = data["AmbiguousNodes"] || data["AmbiguousApps"] || [];
                            return $.isArray(d) && d.length > 0;
                        },
                        tpl: new Ext.XTemplate(
                            '<div class="sw-suggestion" style="width: 100%; padding: 5px 0 6px 0;">',
                                '<span class="sw-suggestion-icon"></span>',
                                '<span style="padding-left: 26px;">{AmbiguousNodes:this.message}</span>',
                                '<span style="float: right;">',
                                    '<a class="sw-btn DPA_RelationshipDialog" style="margin: 0px 16px;" automation="select-node" href="#" ',
                                            'data-monitored-database-id="{GlobalDatabaseInstanceId}" data-node-ids="{AmbiguousNodes}" data-apps-ids="{AmbiguousApps}">',
                                        '<span class="sw-btn-c"><span class="sw-btn-t">{AmbiguousNodes:this.button}</span></span>',
                                    '</a>',
                                '</span>',
                            '</div>', {
                                compiled: true,
                                message: function (d) { return ($.isArray(d) && d.length > 1) ? me.localizedTexts.selectNodeMessage : me.localizedTexts.selectAppMessage; },
                                button: function (d) { return ($.isArray(d) && d.length > 1) ? me.localizedTexts.selectNodeButton : me.localizedTexts.selectAppButton; }
                            })
                    }),
                    new Ext.ux.grid.BbarInfo({
                        id: 'mainGridNotificationBottomBar',
                        hidden: true,
                        showNoficationBar: this.config && !!this.config.showNoficationBar,
                        cls: 'DPA_BbarInfo',
                        items: [
                            {
                                xtype: 'tbtext',
                                html: _.templateExt(['<span class="sw-suggestion-icon"></span>', '<span class="DPA_BbarMessage">{{applicationsWithoutRelationshipMessage}}</span>', '<a href=\"#\" class="DPA_BbarAppsWithoutRelLink">{{applicationsWithoutRelationshipLink}}</a>'], me.localizedTexts),
                                cls: 'sw-suggestion'
                            }],
                        showMessage: function (numberOfApplications) {
                            $('#' + this.id + ' span.DPA_BbarMessage').text(SW.Core.String.Format(me.localizedTexts.applicationsWithoutRelationshipMessage, numberOfApplications));
                        }
                    })
                ],
                autoScroll: 'true',
                loadMask: true,
                tbar: gridToolbarItems,
                bbar: new Ext.Container({ items: pagingToolbar }),
                listeners: {
                    'bodyresize': function () {

                    },
                    filterupdate: function () {
                        me.selectedItemsArray = [];
                    },
                },
                minColumnWidth: 50
            });

            // load last used filters for user
            var filterConfig = JSON.parse(me.loadSetting("LastUsedFilters", "[]"));
            for (var i = 0; i < mainGrid.filters.filters.items.length; i++) {
                var gridFilter = mainGrid.filters.filters.items[i];
                for (var j = 0; j < filterConfig.length; j++) {
                    var savedFilter = filterConfig[j];
                    if (savedFilter.ColumnName === gridFilter.dataIndex) {
                        gridFilter.setActive(true);
                        gridFilter.setValue(savedFilter.Value && _.isString(savedFilter.Value) ? savedFilter.Value.split('$#') : savedFilter.Value);
                    }
                }
            }

            mainGrid.view = new Ext.grid.GroupByGridView(mainGrid.viewConfig);

            // making columns visible or hidden according to saved configuration in selectedColumns
            if (selectedColumns.length != 1 || selectedColumns[0] != '') {
                var colModel = mainGrid.getColumnModel();
                for (var index = 0; index < colModel.getColumnCount() ; index++) {
                    var isHidden = selectedColumns.indexOf(colModel.getDataIndex(index)) < 0;
                    colModel.setHidden(index, isHidden);
                }
            }

            mainGrid.store.on('load', function (store, records) {
                me.mainGridStoreLoaded = true;
                me.updateSelectionFromGridData(mainGrid);
                me.onMainGridStoreLoaded(arguments);
                me.onSelectionChanged(mainGrid, function () {
                    return getAllSelectedItems();
                });

                if (me.filterText && me.displaySearchTextControl) {
                    $(SW.Core.String.Format('#{0} div.x-grid3-body', me.getMainGridId())).highlight(me.filterText);
                }

                $("#" + me.getMainGridId() + " img.filter-icon").off('click.dpa').on('click.dpa', function (event) {
                    return me.showFilter(event, $(this).data('column-name'), me.getMainGrid());
                });
            });

            //var me = this;
            mainGrid.on('sortchange', function (store, option) {
                var sort = option.field;
                var dir = option.direction;
                if (sort && dir) {
                    me.sortOrder.SortColumn = sort;
                    me.sortOrder.SortDir = dir;
                    me.saveSetting('SortOrder', JSON.stringify(me.sortOrder));
                }
            });

            mainGrid.getColumnModel().on('hiddenchange', function () {
                var cols = '';
                var columnModel = mainGrid.getColumnModel();
                for (var i = 0; i < columnModel.getColumnCount() ; i++) {
                    if (!columnModel.isHidden(i)) {
                        cols += columnModel.getDataIndex(i) + ',';
                    }
                }
                me.saveSetting('SelectedColumns', cols.slice(0, -1));
            });
        }

        this.createAndInitializePagingToolbar = function () {
            pagingToolbar = new Ext.PagingToolbar(
                    {
                        hidden: !me.displayPagingToolbar,
                        store: gridDataStore,
                        pageSize: this.gridPageSize,
                        displayInfo: true,
                        displayMsg: me.localizedTexts.pagingToolbarDisplayMsg,// 'Displaying items {0} - {1} of {2}'
                        emptyMsg: "@{R=DPA.Strings;K=IntegrationWizard_Grid_NoItemsToDisplay; E=js}", // 'No items to display'
                        beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}", // 'Page'
                        afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}", // 'of {0}'
                        firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}", // 'First Page'
                        prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}", // 'Previous Page'
                        nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}", // 'Next Page'
                        lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}", // 'Last Page'
                        refreshText: me.localizedTexts.pagingToolbarRefreshTooltip, // 'Refresh'
                        items: me.displayPageSizeControl ?
                            [
                            {
                                xtype: 'tbtext',
                                text: '@{R=DPA.Strings;K=SortablePageableTable_ItemsOnPage; E=js}',// 'Items on page:'
                                style: 'margin-left: 5px; margin-right: 5px; vertical-align: middle;'
                            },
                            new Ext.form.ComboBox({
                                regex: /^\d*$/,
                                store: new Ext.data.SimpleStore({
                                    fields: ['pageSize'],
                                    data: [[10], [20], [25], [30], [50], [100], [150], [200], [250]]
                                }),
                                displayField: 'pageSize',
                                mode: 'local',
                                triggerAction: 'all',
                                selectOnFocus: true,
                                width: 50,
                                editable: false,
                                value: me.gridPageSize,
                                listeners: {
                                    select: function (c, record) {
                                        pagingToolbar.pageSize = record.get("pageSize");
                                        pagingToolbar.cursor = 0;
                                        me.saveSetting('PageSize', pagingToolbar.pageSize);
                                        me.gridPageSize = pagingToolbar.pageSize;
                                        pagingToolbar.doRefresh();
                                    }
                                }
                            })
                            ] : [],
                        doRefresh: function (toolbar, event) {
                            var clickedOnRefreshButton = typeof event !== 'undefined';
                            var onPagingToolbarRefreshResult = false;
                            if (clickedOnRefreshButton) {
                                onPagingToolbarRefreshResult = me.onPagingToolbarRefresh();
                            }

                            if (!onPagingToolbarRefreshResult) {
                                Ext.PagingToolbar.prototype.doRefresh.call(this);
                                me.reloadGroupByGrid();
                            }
                        }
                    }
                );
        }

        this.getMainGrid = function () {
            if (mainGrid == null) {
                createMainGrid();
            }

            return mainGrid;
        }

        // MAIN WINDOW
        this.createMainGridPanel = function () {
            var id;
            if (this.setMainGridPanelId) {
                id = "mainGridPanel";
            } else {
                id = "";
            }
            this.mainGridPanel =
                new Ext.Panel({
                    layout: 'fit',
                    boxMinHeight: 200,
                    height: 200,
                    width: 800,
                    id: id,
                    items: [
                         new Ext.Panel({
                             layout: 'border',
                             border: false,
                             collapsible: false,
                             height: 'auto',
                             items: [
                                 mainGrid,
                                 navPanel
                             ],
                             cls: 'no-border'
                         })
                    ]
                });

               
        }

        // Refresh GroupBy and main grid
        this.internalGridRefresh = function () {
            clearNotification();

            var tmpFilterPropertyParam = me.filterPropertyParam;
            var tmpFilterTypeParam = me.filterTypeParam;
            var tmpfilterValueParam = me.filterValueParam;
            var tmpfilterGroupingPropertyParam = me.filterGroupingPropertyParam;

            me.groupingValue = JSON.parse(me.loadSetting('GroupingValue', '[]'));

            if (me.filterPropertyParam == "" && typeof me.groupingValue != "undefined" && me.groupingValue.Value != "") {
                me.filterPropertyParam = me.groupingValue.Value;
            }

            if (me.filterTypeParam == "" && typeof me.groupingValue != "undefined" && me.groupingValue.Type != "") {
                me.filterTypeParam = me.groupingValue.Type;
            }

            if (me.filterGroupingPropertyParam == "" && typeof me.groupingValue != "undefined" && me.groupingValue.GroupingProperty != "") {
                me.filterGroupingPropertyParam = me.groupingValue.GroupingProperty;
            }

            me.reloadElementStore(me.groupGrid, me.filterPropertyParam, me.filterTypeParam, me.filterValueParam, me.filterGroupingPropertyParam, "", function () {
                var selectedVal = -1;

                var dtItems = me.groupGrid.getStore().data.items;
                for (var i = 0; i < dtItems.length; i++) {
                    if (dtItems[i].data.Id == me.groupingValue.SubGroupValue)
                        selectedVal = i;
                }

                if (selectedVal == -1) {
                    me.groupingValue.SubGroupValue = "[All]";
                    selectedVal = 0;
                }
                me.groupGrid.getSelectionModel().selectRow(selectedVal, false);
            });

            if (me.groupingValue.Name == '@{R=DPA.Strings;K=IntegrationWizard_Grid_NoGrouping; E=js}') {
                me.groupByComboBox.setValue(me.groupingValue.Name);
                me.reloadMainGridStore(0, me.gridPageSize, mainGrid, "", "", "", me.filterText);
            }
            else
                me.reloadMainGridStore(0, me.gridPageSize, mainGrid, me.groupingValue.Value, me.groupingValue.Type, me.groupingValue.SubGroupValue, me.filterText);

            me.filterPropertyParam = tmpFilterPropertyParam;
            me.filterTypeParam = tmpFilterTypeParam;
            me.filterValueParam = tmpfilterValueParam;
            me.filterGroupingPropertyParam = tmpfilterGroupingPropertyParam;
        }

        function highlightSearchString(search) {
            if (search != null && search.length > 0)
                $(SW.Core.String.Format('#{0} div.x-grid3-body', me.getMainGridId())).highlight(search);
        }

        function processTooltips() {
            $(SW.Core.String.Format("#{0} a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)", me.getEnvelopElementId())).livequery(function () {
                this.tooltip = 'processed';
                $.swtooltip(this);
            });
        }

        this.displayErrorMessage = function (message) {
            me.getMainGrid().getView().emptyText = message;
            me.getMainGrid().store.removeAll();
        };

        this.renderEntityNameWithIcon = function(entity, entityName, entityStatus, entityDetailsUrl, entityId) {
            return SW.DPA.Common.formatEntityWithStatusIconLink(entity, entityName, entityStatus, entityDetailsUrl, entityId, 'DPA_CellEntityNameWithIcon');
        };

        this.getSettingsPrefix = function () {
            if (me.settingsPrefix)
                return me.settingsPrefix;

            return me.settingsPrefix = $('#' + this.getEnvelopElementId() + ' input#settingsPrefix').val();
        };
    },

    // PUBLIC PROPS

    displayShowOnlyCombo: true,
    displayPagingToolbar: false,
    displayPageSizeControl: false,
    displaySearchTextControl: false,

    // PUBLIC METHODS
    init: function (initConfig) {
        var me = $.extend(true, this, initConfig || {});

        this.controllerName = $('#' + this.getEnvelopElementId() + ' input#controllerName').val();

        this.dataStoreMapping = JSON.parse(me.loadSetting('Columns', '[]'));

        this.gridPageSize = parseInt(me.loadSetting('PageSize'));

        this.groupingValue = JSON.parse(me.loadSetting('GroupingValue'));

        this.sortOrder = JSON.parse(me.loadSetting('SortOrder'));

        this.createDataStores();
        this.createAndInitializePagingToolbar();
        this.createShowOnlyComboBox();
        this.createGroupByComboBox();
        this.createGroupGrid();
        this.createMainGrid();
        this.createGroupByTopPanel();
        this.createNavigationPanel();
        this.createMainGridPanel();

        if (this.displayShowOnlyCombo)
            this.reloadElementStore(this.showOnlyComboBox, "", "", "", "", "");

        this.reloadElementStore(this.groupByComboBox, "", "", "", "", "");

        this.groupByComboBox.setValue(this.groupingValue.Name);

        this.mainGridPanel.render(me.getPlaceholderElementId());

        me.filterPropertyParam = this.groupingValue.Value;
        me.filterTypeParam = this.groupingValue.Type;
        me.filterValueParam = this.groupingValue.SubGroupValue;
        // me.filterText = .. already set

        this.updateToolbarButtons();
        this.mainPanelResize();
        $(window).resize(function () { me.mainPanelResize.call(me); });

        // Workaround for Chrome browser to show selected grouping value correctly
        setTimeout(this.internalGridRefresh, 0);
    },

    refresh: function () {
        this.internalGridRefresh();
    },

    onMainGridStoreLoaded: function () {
    },

    createMainGriDataStore: function () {
        throw "Implement new ORION.WebApiStore for main grid";
    },

    createShowOnlyDataStore: function () {
        if (!this.displayShowOnlyCombo)
            return null;

        throw "Implement new ORION.WebApiStore for Show Only combobox";
    },

    createGroupingDataStore: function () {
        throw "Implement new ORION.WebApiStore for grouping combobox";
    },

    createGroupingGridDataStore: function () {
        throw "Implement new ORION.WebApiStore for grouping grid";
    },

    storeSelectionOnServer: function (selectRequest, deselectRequest, modelId) {
        // implement to store selection on server
    },

    onCustomSelectionChanged: function () {
        var me = this;
        me.updateToolbarButtons();
        me.updateSelectionOnServer();
    },

    updateSelectionFromGridData: function (grid) {
        this.selectedItemsArray = []; // clear selections
        var selectedRowIndexes = [];
        $.each(grid.store.data.items, function (i) {
            if (this.data && this.data.IsSelected)
                selectedRowIndexes.push(i);
        });

        var selectionModel = grid.getSelectionModel();
        for (var rowIndex = 1; rowIndex < selectedRowIndexes.length; rowIndex++) {
            selectionModel.selectRow(selectedRowIndexes[rowIndex], true, false, true);
        }

        if (selectedRowIndexes.length > 0)
            selectionModel.selectRow(selectedRowIndexes[0], true);
    },

    getGridToolbarConfig: function () {
        // return buttons for toolbar upon a main grid
    },

    onSelectionChanged: function (grid, webStore) {
        this.selectionChange(this.selectedItemsArray);
    },

    mainPanelResize: function () {
        var me = this;

        var placeHolderElement = $('#' + me.getPlaceholderElementId());

        me.mainGridPanel.setWidth(1);

        me.mainGridPanel.setWidth(placeHolderElement.width() - 0);

        var height = $(window).height() - 480;
        if (height < 420)
            height = 420; // min height for 12 rows

        placeHolderElement.height(height);
        me.mainGridPanel.setHeight(height);
    },

    getRecordId: function (data) {
        throw "Implement to resolve unique identification for data in the grid row";
    },

    getEnvelopElementId: function () {
        throw "Define ID of div element enveloping the object";
    },

    getPlaceholderElementId: function () {
        throw "Define ID of div element where grid will be rendered";
    },

    getMainGridId: function () {
        throw "Define ID of div element containing main grid";
    },

    loadSetting: function (key, defaultValue) {
        ORION.prefix = this.getSettingsPrefix();

        return ORION.Prefs.load(key, defaultValue);
    },

    saveSetting: function (key, value) {
        ORION.prefix = this.getSettingsPrefix();

        ORION.Prefs.save(key, value);
    },

    getBaseParams: function () {
        var queryParamName = this.loadSetting('ModelIdentifierName');
        var baseParams = {
            ModelID: Ext.urlDecode(location.search.slice(1))[queryParamName]
        };
        if (this.config && this.config["NodeID"]) {
            baseParams["NodeId"] = this.config["NodeID"];
        }

        return baseParams;
    },

    renderGroup: function(value, meta, record) {
        var me = this;

        var stringFormat = (record.data.SelectedCnt > 0)
            ? me.localizedTexts.groupValueFormatWithSelection
            : me.localizedTexts.groupValueFormatWithoutSelection;

        var retval = SW.Core.String.Format(stringFormat, Ext.util.Format.htmlEncode(value), record.data.Cnt, record.data.SelectedCnt);
        return retval;
    },

    updateSelectedRowsMessage: function (numberOfSelectedRows) {
        // display number of selected rows from main grid
    }
});
