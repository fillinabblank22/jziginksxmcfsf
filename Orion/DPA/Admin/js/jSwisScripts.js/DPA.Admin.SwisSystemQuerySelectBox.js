﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.Admin = SW.DPA.Admin || {};
SW.DPA.Admin.SwisSystemQuerySelectBox = function (idOfSelectEl, idOfQueryTextareaEl, idOfErrorMessageDivEl) {
	var selectEl = $('#' + idOfSelectEl);

	this.bindSelectBox = function () {
		$('#' + idOfSelectEl).change(function () {
			// set selected swql query to textarea 
			$('#' + idOfQueryTextareaEl).val($(this).val());
		});
	};

	// load queries to select box
	this.load = function () {

		// get all system queries and write them to select box
		var jSwisQueryRepository = new SW.DPA.Admin.JSwisQueryRepository();
		jSwisQueryRepository.GetAllSystemQueries(
            function (result) {
                $('#' + idOfSelectEl).empty();
                $('#' + idOfSelectEl).append($('<option value="" selected>' + "@{R=DPA.Strings;K=DPAAdminJs_DPAAdminSwisSystemQuerySelectBox_SelectQuery;E=js}" + '</option>'));
            	
            	var data = result;
            	for (var i = 0; i < data.length; i++) {
            		$('#' + idOfSelectEl).append($('<option>' + data[i].Name + '</option>').val(data[i].Query));
            	}
            },
            function (error) {
                $('#' + idOfErrorMessageDivEl).text(error).show();
            });
	};

	this.bindSelectBox();
};