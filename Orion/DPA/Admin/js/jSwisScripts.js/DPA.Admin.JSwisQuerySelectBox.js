﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.Admin = SW.DPA.Admin || {};
SW.DPA.Admin.JSwisQuerySelectBox = function (idOfSelectEl, idOfQueryTextareaEl, idOfQueryNameInputEl, idOfErrorMessageDivEl) {
    var localizedStrings = {
        newQueryLabel: '@{R=DPA.Strings;K=DPAAdminJs_DPAAdminJSwisQuerySelectBox_NewQueryLabel;E=js}'
    };
    var queryNames = [];

	this.bindSelectBox = function () {        
	    $('#' + idOfSelectEl).change(function () {
            // set selected swql query to textarea 
	        $('#' + idOfQueryTextareaEl).val($(this).val());

            // get name of selected query
		    var queryName = $('#' + idOfSelectEl + ' option:selected').html();

		    if (queryName != localizedStrings.newQueryLabel) {
                // set name of selected query to input box
				$('#' + idOfQueryNameInputEl).val(queryName);
			}
			else {
                // set empty string when no query selected
				$('#' + idOfQueryNameInputEl).val('');
			}
		});
	};

    // load queries to select box
	this.load = function (nameOfSelectedItem) {
        // mark any query as selected
	    var isSelected = typeof (nameOfSelectedItem) !== 'undefined';

        // set name of query to input box
		if (isSelected) {
			$('#' + idOfQueryNameInputEl).val(nameOfSelectedItem);
		}
		else {
			$('#' + idOfQueryNameInputEl).val('');
		}

        // get all queries and write them to select box
		var jSwisQueryRepository = new SW.DPA.Admin.JSwisQueryRepository();
		jSwisQueryRepository.GetAll(
            function (result) {
                queryNames = [];
            	$('#' + idOfSelectEl).empty();
            	$('#' + idOfSelectEl).append($('<option value="" selected>' + localizedStrings.newQueryLabel + '</option>'));
            	
            	var data = result;
            	for (var i = 0; i < data.length; i++) {
            	    var option;

            		if (isSelected && nameOfSelectedItem == data[i].Name) {
            		    option = $('<option selected>' + data[i].Name + '</option>').val(data[i].Query);
            		}
            		else {
            		    option = $('<option>' + data[i].Name + '</option>').val(data[i].Query);
            		}

            		$('#' + idOfSelectEl).append(option);
            		queryNames.push(data[i].Name);
	            }
            },
            function (error) {
                $('#' + idOfErrorMessageDivEl).text(error).show();
            });
	};

    this.isNameAlreadyUsed = function(name) {
        return _.contains(queryNames, name);
    };

    this.bindSelectBox();
};