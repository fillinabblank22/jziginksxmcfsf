﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.Admin = SW.DPA.Admin || {};
SW.DPA.Admin.DoQuery = function (query, serverId, succeeded) {
    SW.Core.Services.callController(
        '/api/DpaJSwisInformation/Query',
        { Query: query, ServerId: serverId },
        function (result) {
            succeeded(result);
            $('#lastErrorMessage').hide();
        },
        function (error) {
            $('#lastErrorMessage').text("@{R=DPA.Strings;K=DPAAdminJs_DoQuery_ServerError;E=js} " + error).show();
        });
};