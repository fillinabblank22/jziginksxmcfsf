﻿$().ready(function () {
    var localizedStrings = {
        saveConfirmationDialogTitle: '@{R=DPA.Strings;K=DPAAdminJs_jSwis_saveConfirmationDialogTitle;E=js}',
        saveConfirmationDialogMessage: '@{R=DPA.Strings;K=DPAAdminJs_jSwis_saveConfirmationDialogMessage;E=js}'
    };

    var querySelectBox = new SW.DPA.Admin.JSwisQuerySelectBox('userQueries', 'queryEdit', 'queryNameInput', 'userQueriesErrorMessage');
    querySelectBox.load();

    var systemQueriesSelectBox = new SW.DPA.Admin.SwisSystemQuerySelectBox('systemQueries', 'queryEdit', 'systemQueriesErrorMessage');
    systemQueriesSelectBox.load();
    
    $('#deleteQueryBtn').click(function () {
        var name = $("#queryNameInput").val();
        var deleteBtn = $(this);
        if (name != '') {
            deleteBtn.attr('disabled', 'disabled');
            $('#userQueriesErrorMessage').hide();
            var jSwisQueryRepository = new SW.DPA.Admin.JSwisQueryRepository();
            jSwisQueryRepository.Delete(name,
                function (result) {
                    deleteBtn.removeAttr('disabled');
                    if (result) {
                        querySelectBox.load();
                        $('#queryEdit').val('');
                    }
                },
                function (error) {
                    $('#userQueriesErrorMessage').text(error).show();
                    deleteBtn.removeAttr('disabled');
                });
        }
    });

    var saveUserQuery = function (name, query) {
        var saveBtn = $(this);
        saveBtn.attr('disabled', 'disabled');

        var jSwisQueryRepository = new SW.DPA.Admin.JSwisQueryRepository();
        jSwisQueryRepository.Save(
            name,
            query,
            function (result) {
                if (result) {
                    querySelectBox.load(name);
                }
                saveBtn.removeAttr('disabled');
            },
            function (error) {
                $('#userQueriesErrorMessage').text(error).show();
                saveBtn.removeAttr('disabled');
            });
    };

    $('#saveQueryBtn').click(function () {
        var query = TextAreaHelper.GetSelectedText('queryEdit');
        var name = $("#queryNameInput").val();
        $('#userQueriesErrorMessage').hide();
        if (name != '') {
            if (querySelectBox.isNameAlreadyUsed(name)) {
                Ext.MessageBox.show({
                    title: localizedStrings.saveConfirmationDialogTitle,
                    msg: localizedStrings.saveConfirmationDialogMessage,
                    buttons: Ext.MessageBox.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            saveUserQuery(name, query);
                        }
                    },
                    icon: 'ext-mb-warning'
                });
            } else {
                saveUserQuery(name, query);
            }
        }
        else {
            $('#userQueriesErrorMessage').text("@{R=DPA.Strings;K=DPAAdminJs_jSwis_WarnMsgFillNameOfQuery;E=js}").show();
        }
    });

    $('#executeBtn').click(function () {

        $('#queryElapsedTime').html('');
        $('#resultset').empty();
        var swql = TextAreaHelper.GetSelectedText('queryEdit');
        var serverId = $("#dpaServers").val();

        SW.DPA.Admin.DoQuery(swql, serverId, function (result) {

            $('#queryElapsedTime').html('@{R=DPA.Strings;K=DPAAdminJs_jSwis_ExecutionTime;E=js} ' + result.ElapsedTimeInMilliseconds + " ms");

            var store = new Ext.data.SimpleStore({
                data: result.Data.Rows,
                fields: result.Data.Columns
            });

            var columns = [];
            for (var x = 0; x < result.Data.Columns.length; ++x) {
                columns.push({
                    header: result.Data.Columns[x],
                    width: 110,
                    sortable: true,
                    dataIndex: result.Data.Columns[x]
                });
            }

            var grid = new Ext.grid.GridPanel({
                store: store,
                columns: columns,
                stripeRows: true,
                autoScroll: true,
                layout: 'fit',
                height: 350,
            });
            
            var panel = new Ext.Panel({
                width: $(document).width() - 50,
                height: 360,
                autoScroll: true,
                items: [
                    grid
                ]
            });
            
            $('#resultset').empty();
            panel.setAutoScroll(true);
            panel.render('resultset');
        });
    });

    var dpaServerManager = new DpaServerManager();
    dpaServerManager.Init();
});

var EntitiesManager = function () {

    // Public methods

    this.Init = function () {

        this.LoadAllEntities();
        this.InitGeneratorBtn();
    };

    this.LoadAllEntities = function () {
        var selectAllEntitiesSwql = "SELECT FullName FROM Metadata.Entity WHERE IsAbstract = 'False' ORDER BY FullName";
        var serverId = $("#dpaServers").val();

        SW.DPA.Admin.DoQuery(selectAllEntitiesSwql, serverId, function (result) {

            var $selectWithEntities = $('#entities');
            $selectWithEntities.html('');

            for (var i = 0, end = result.Data.Rows.length; i < end; i++) {
                var entityRow = result.Data.Rows[i];

                var entityOption = $("<option />");
                entityOption.attr("value", entityRow[0]);
                entityOption.text(entityRow[0]);

                $selectWithEntities.append(entityOption);
            }
        });
    };


    this.InitGeneratorBtn = function () {
        $('#generateEntitySwqlBtn').click(function () {
            $('#queryEdit').val('');
            var selectedEntityName = $('#entities').val();
            if (selectedEntityName !== '') {
                listAllColumnsForEntity(selectedEntityName);
            }
        });
    };

    // Private methods

    var listAllColumnsForEntity = function (entityName) {
        var selectAllEntityColumnsSwql = "SELECT Name FROM Metadata.Property WHERE EntityName = '" + entityName + "' AND IsNavigable = 'False' ORDER BY Name";
        var serverId = $("#dpaServers").val();

        SW.DPA.Admin.DoQuery(selectAllEntityColumnsSwql, serverId, function (result) {
            if (result.Data.Rows.length > 0) {
                var $queryEdit = $('#queryEdit');

                var swqlForListingColumns = generateColumnsListSwql(entityName, result.Data.Rows);
                var newSwqlQuery = appendQuery($queryEdit.val(), swqlForListingColumns);

                $queryEdit.val(newSwqlQuery);
            }
        });
    };

    var generateColumnsListSwql = function (entityName, columns) {
        var swql = "SELECT ";
        swql = swql + columns.join(", ");
        swql = swql + " FROM ";
        swql = swql + entityName;

        return swql;
    };

    var appendQuery = function (currentQuery, queryToAppend) {
        var newQuery;
        currentQuery = $.trim(currentQuery);

        if (currentQuery === '') {
            newQuery = queryToAppend;
        } else {
            newQuery = currentQuery + "\n\n" + queryToAppend;
        }

        return newQuery + '\n';
    };
};

var DpaServerManager = function() {
    this.Init = function () {
        SW.Core.Services.callController(
            '/api/DpaServers/GetAll', {},
            function(result) {
                var servers = result;
                var serverList = $('#dpaServers');
                serverList.html('');

                for (var i = 0, end = servers.length; i < end; i++) {
                    var server = servers[i];

                    var serverOption = $("<option />");
                    serverOption.attr("value", server.DpaServerId);
                    serverOption.text(server.DisplayName);

                    serverList.append(serverOption);
                }

                var entitiesManager = new EntitiesManager();
                entitiesManager.Init();
            },
            function(error) {
                $('#dpaServersErrorMessage').text(error);
            });
    }
}

var TextAreaHelper = {
    GetSelectedText: function (elementId) {
        var text = $('#' + elementId).val();
        try {
            var selection = TextAreaHelper.GetSelection(elementId);

            if (typeof (selection) !== 'undefined' && selection.text !== '') {
                text = selection.text;
            }
        }
        catch (e) {
            // we dont care about selection, we will use origin text from textarea.
        }
        return text;
    },

    GetSelection: function (elementId) {
        var e = document.getElementById(elementId);

        // Mozilla and DOM 3.0
        if ('selectionStart' in e) {
            var l = e.selectionEnd - e.selectionStart;
            return { start: e.selectionStart, end: e.selectionEnd, length: l, text: e.value.substr(e.selectionStart, l) };
        }
            // IE
        else if (document.selection) {
            e.focus();
            var r = document.selection.createRange();
            var tr = e.createTextRange();
            var tr2 = tr.duplicate();
            tr2.moveToBookmark(r.getBookmark());
            tr.setEndPoint('EndToStart', tr2);
            if (r == null || tr == null) return { start: e.value.length, end: e.value.length, length: 0, text: '' };
            var textPpart = r.text.replace(/[\r\n]/g, '.'); //for some reason IE doesn't always count the \n and \r in the length
            var textWhole = e.value.replace(/[\r\n]/g, '.');
            var theStart = textWhole.indexOf(textPpart, tr.text.length);
            return { start: theStart, end: theStart + textPpart.length, length: textPpart.length, text: r.text };
        }
            // Browser not supported
        else return { start: e.value.length, end: e.value.length, length: 0, text: '' };

    }
};