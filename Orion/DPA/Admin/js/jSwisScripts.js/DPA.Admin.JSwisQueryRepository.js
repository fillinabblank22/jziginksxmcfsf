﻿SW = SW || {};
SW.DPA = SW.DPA || {};
SW.DPA.Admin = SW.DPA.Admin || {};
SW.DPA.Admin.JSwisQueryRepository = function () {
    this.Save = function (name, query, onSuccess, onError) {
        SW.Core.Services.callController(
            '/api/DpaJSwisQueries/Save',
            { Name: name, Query: query },
            onSuccess, onError);
    };

    this.GetAll = function (onSuccess, onError) {
        SW.Core.Services.callController(
            '/api/DpaJSwisQueries/GetAll',
            { Limit: 10000 },
            onSuccess, onError);
    };

    this.GetAllSystemQueries = function(onSuccess, onError) {
        SW.Core.Services.callController(
            '/api/DpaJSwisQueries/GetAll',
            { Limit: 10000, OnlySystemQueries: true },
            onSuccess, onError);
    };

    this.Delete = function (name, onSuccess, onError) {
        SW.Core.Services.callController(
            '/api/DpaJSwisQueries/Delete',
            { Name: name },
            onSuccess, onError);
    };
};