﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageRelationshipsTabs.ascx.cs" Inherits="Orion_DPA_Admin_ManageRelationships_ManageRelationshipsTabs" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<orion:NavigationTabBar ID="NavigationTabBar" runat="server" Visible="true">
    <orion:NavigationTabItem Name="<%$ Resources: DPAWebContent, ManageRelationships_Tabs_DatabaseInstances %>" Url="~/Orion/DPA/Admin/ManageRelationships/DatabaseInstances.aspx" />
    <orion:NavigationTabItem Name="<%$ Resources: DPAWebContent, ManageRelationships_Tabs_ClientApplications %>" Url="~/Orion/DPA/Admin/ManageRelationships/ClientApplications.aspx" />
    <orion:NavigationTabItem Name="<%$ Resources: DPAWebContent, ManageRelationships_Tabs_Storages %>" Url="~/Orion/DPA/Admin/ManageRelationships/Storages.aspx" />
</orion:NavigationTabBar>