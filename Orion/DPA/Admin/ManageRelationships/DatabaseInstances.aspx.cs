﻿using System;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;
using SolarWinds.DPA.Web.UI.RelationshipsManagement;

public partial class Orion_DPA_Admin_ManageRelationships_DatabaseInstances : RelationshipManagementPageBase<RelationshipManagementModel>
{
    protected readonly Lazy<IGridColumnDefinitions> LazyColumnDefinition = new Lazy<IGridColumnDefinitions>(
        () =>
            ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaRelationshipsGridController.UniqueId));
}