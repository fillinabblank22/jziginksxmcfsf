﻿using System;
using SolarWinds.DPA.Web.Controllers;
using SolarWinds.DPA.Web.Model.RelationshipManagement;
using SolarWinds.DPA.Web.Model.RelationshipManagement.Grid;
using SolarWinds.DPA.Web.UI.RelationshipsManagement;

public partial class Orion_DPA_Admin_ManageRelationships_Storages : RelationshipManagementPageBase<RelationshipManagementModel>
{
    protected readonly Lazy<IGridColumnDefinitions> LazyColumnDefinition =
        new Lazy<IGridColumnDefinitions>(() => SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IGridColumnDefinitions>(DpaStorageGridController.UniqueId));

    protected bool IsSrmInstalled => DpaSettings.IsSrmInstalled;
}