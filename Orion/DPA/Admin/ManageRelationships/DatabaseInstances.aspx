﻿<%@ Page Language="C#" MasterPageFile="~/Orion/DPA/Admin/ManageRelationships/ManageRelationships.master" AutoEventWireup="true" CodeFile="DatabaseInstances.aspx.cs" 
    Inherits="Orion_DPA_Admin_ManageRelationships_DatabaseInstances" Title="<%$ Resources: DPAWebContent, ManageRelationships_DatabaseInstances_Title %>" %>
<%@ Import Namespace="SolarWinds.DPA.Common.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="RelationshipConfigurationModal" Src="~/Orion/DPA/Controls/RelationshipConfigurationModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ApplicationsWithoutRelationshipModal" Src="~/Orion/DPA/Controls/ApplicationsWithoutRelationshipModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ConnectivityIssueWarning" Src="~/Orion/DPA/Controls/ConnectivityIssueWarning.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="grid" Runat="Server">
    <dpa:ConnectivityIssueWarning runat="server" ID="ConnectivityIssueWarning" />
    <div id="integrationGrid">
        <div id="filterNotifications" class="sw-filter-notifications"></div>
        <div id="relationshipsPlaceholder" class="sw-relationshipsGrid"></div>
        <div>
            <input type="hidden" id="settingsPrefix" value='<%= LazyColumnDefinition.Value.IdentificationPrefix %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SortOrderKey%>" value='<%= LazyColumnDefinition.Value.GetSortOrder() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.PageSizeKey%>" value='<%= LazyColumnDefinition.Value.GetPageSize() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.GroupingValueKey%>" value='<%= LazyColumnDefinition.Value.GetGroupingValue() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SelectedColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetSelectedColumns() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.LastUsedFiltersKey%>" value='<%= LazyColumnDefinition.Value.GetLastUsedFilters() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetColumns() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsModelConfigKey%>" value='<%= LazyColumnDefinition.Value.GetColumnsModelConfig() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.IdentificationPrefix%>ModelIdentifierName" value='<%= SessionIdentifier %>' />
        </div>
    </div>
    
    <dpa:RelationshipConfigurationModal runat="server" />
    <dpa:ApplicationsWithoutRelationshipModal runat="server" />
    <script>
        $(document).ready(function() {
            SW.DPA.Admin.grid1 = new SW.DPA.Admin.ManageRelationships.RelationshipsGrid(<%= FeatureManagerHelper.IsNodeFunctionalityAllowed().ToString().ToLowerInvariant() %>,
                '<%= ConnectivityIssueWarning.ClientID %>');
            Ext.onReady(SW.DPA.Admin.grid1.init, SW.DPA.Admin.grid1);
        });
    </script> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="sidebar" Runat="Server">
    <div class="DPA_RightBlockHint">
        <div class="sw-suggestion sw-suggestion-info" id="helpPanel">
            <span class="sw-suggestion-icon"></span>
            <h4 class="DPA_HintTitle" id="DPA_HintTitleSummary"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_HintTitle %></h4>
            
            <span class="DPA_CloseHelpPanelButton">
                <a href="#">
                    <img src="/Orion/DPA/images/black_close.png" alt="Close"/>
                </a>
            </span>

            <p class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_HintDescription1 %></p>
            <p class="DPA_HintImage">
                <img src="/Orion/DPA/images/IntegrationWizard/diagram1.png" data-big-img-url="/Orion/DPA/images/IntegrationWizard/diagram1.png" alt="Learn more about relationships picture" />
            </p>
            <p class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_Relationships_HintDescription2 %></p>
            <p class="DPA_HintLink"><a class="sw-link" target="_blank" href="<%: HelpHelper.GetHelpUrl("DPA", "OrionDPAManageRelationships") %>">&raquo; <%: Resources.DPAWebContent.ManageRelationships_LearnMore %></a></p>
        
            <div class="DPA_RelationshipStatusLegend">
                <h4><%: Resources.DPAWebContent.IntegrationWizard_TypesOfActionsTitle %></h4>
                <table>
                    <tbody>
                        <tr><td><img src="/Orion/DPA/images/edit_16x16.png" /></td><td><%: SolarWinds.DPA.Strings.Resources.IntegrationWizard_Grid_ActionsTooltip_EditRelationship %></td></tr>
                        <tr><td><img src="/Orion/DPA/images/relationship-unlink.png" /></td><td><%: SolarWinds.DPA.Strings.Resources.IntegrationWizard_UnlinkRelationship %></td></tr>
                        <tr><td><img src="/Orion/DPA/images/relationship-add.png" /></td><td><%: SolarWinds.DPA.Strings.Resources.IntegrationWizard_Grid_ActionsTooltip_DefineRelationship %></td></tr>
                    </tbody>
                </table>                
            </div>
        </div>
    </div> 
</asp:Content>

