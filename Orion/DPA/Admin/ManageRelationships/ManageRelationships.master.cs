﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.ServiceLocator;
using SolarWinds.DPA.Common.Settings;

public partial class Orion_DPA_Admin_ManageRelationships_ManageRelationships : System.Web.UI.MasterPage
{
    public bool IsHelpPanelVisible {
        get { return ServiceLocator.Current.GetInstance<IDPASettings>().IsHelpPanelVisible; }
    }

    public bool HelpPanelDisabled { get; set; }
}
