﻿<%@ Page Title="<%$ Resources: DPAWebContent, ManageRelationships_ClientApplications_Title %>" Language="C#" MasterPageFile="~/Orion/DPA/Admin/ManageRelationships/ManageRelationships.master" 
    AutoEventWireup="true" CodeFile="ClientApplications.aspx.cs" Inherits="Orion_DPA_Admin_ManageRelationships_ClientApplications" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="dpa" TagName="ApplicationConfigurationModal" Src="~/Orion/DPA/Controls/ApplicationConfigurationModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ClientApplicationCandidatesModal" Src="~/Orion/DPA/Controls/ClientApplicationCandidatesModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ConnectivityIssueWarning" Src="~/Orion/DPA/Controls/ConnectivityIssueWarning.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="grid" Runat="Server">    
<% if (IsApmInstalled) { %>
        <dpa:ConnectivityIssueWarning runat="server" ID="ConnectivityIssueWarning" />
        <div id="integrationGrid">
        <div id="filterNotifications" class="sw-filter-notifications"></div>
        <div id="relationshipsPlaceholder" class="sw-relationshipsGrid"></div>
        <div>
            <input type="hidden" id="settingsPrefix" value='<%= LazyColumnDefinition.Value.IdentificationPrefix %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SortOrderKey%>" value='<%= LazyColumnDefinition.Value.GetSortOrder() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.PageSizeKey%>" value='<%= LazyColumnDefinition.Value.GetPageSize() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.GroupingValueKey%>" value='<%= LazyColumnDefinition.Value.GetGroupingValue() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SelectedColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetSelectedColumns() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.LastUsedFiltersKey%>" value='<%= LazyColumnDefinition.Value.GetLastUsedFilters() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetColumns() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsModelConfigKey%>" value='<%= LazyColumnDefinition.Value.GetColumnsModelConfig() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.IdentificationPrefix%>ModelIdentifierName" value='<%= SessionIdentifier %>' />
        </div>
    </div>
    
    <dpa:ApplicationConfigurationModal ID="ApplicationConfigurationModal1" runat="server" />
    <dpa:ClientApplicationCandidatesModal  runat="server" ID="ClientApplicationCandidatesModal" />
    <script>
        $(document).ready(function() {
            SW.DPA.Admin.grid1 = new SW.DPA.Admin.ManageRelationships.ClientApplicationsGrid('<%= ConnectivityIssueWarning.ClientID %>');
            Ext.onReady(SW.DPA.Admin.grid1.init, SW.DPA.Admin.grid1);
        });
    </script>
<% } else { %>
    <div class="sw-suggestion DPA_ManagementClientApplicationsSuggestion">
        <span class="sw-suggestion-icon"></span>
        <span><%: Resources.DPAWebContent.RelationshipManagement_ClientApplications_NoApmHintMessage %></span><br/>
        <a href="http://www.solarwinds.com/server-application-monitor.aspx" target="_blank" class="sw-link" automation="learn-about-sam">»
            <%: Resources.DPAWebContent.IntegrationSetup_SAMAddress_Link %> 
        </a>
    </div>
<% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="sidebar" Runat="Server">
    <div class="DPA_RightBlockHint">
        <div class="sw-suggestion sw-suggestion-info" id="helpPanel">
            <span class="sw-suggestion-icon"></span>
            
            <span class="DPA_CloseHelpPanelButton">
                <a href="#">
                    <img src="/Orion/DPA/images/black_close.png" alt="Close"/>
                </a>
            </span>
            
            <h4 class="DPA_HintTitle" id="DPA_HintTitleSummary"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_HintTitle %></h4>

            <p class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_HintDescription1 %></p>
            <ul>
                <li class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_HintDescription2 %></li>
                <li class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_HintDescription3 %></li>
            </ul>
            <p class="DPA_HintDescription"><%: Resources.DPAWebContent.IntegrationWizard_ClientApplications_HintDescription4 %></p>
            <p class="DPA_HintImage">
                <img src="/Orion/DPA/images/IntegrationWizard/diagram3.png" data-big-img-url="/Orion/DPA/images/IntegrationWizard/diagram3.png" alt="Learn more about relationships picture" />
            </p>

            <p class="DPA_HintLink"><a class="sw-link" target="_blank" href="<%: HelpHelper.GetHelpUrl("DPA", "OrionDPAManageRelationships") %>"><%: Resources.DPAWebContent.ManageRelationships_LearnMore %> »</a></p>
        
            <div class="DPA_RelationshipStatusLegend">
                <h4><%: Resources.DPAWebContent.IntegrationWizard_TypesOfActionsTitle %></h4>
                <table>
                    <tbody>
                        <tr><td><img src="/Orion/DPA/images/edit_16x16.png" /></td><td><%: SolarWinds.DPA.Strings.Resources.IntegrationWizard_Grid_ActionsTooltip_EditRelationship %></td></tr>
                        <tr><td><img src="/Orion/DPA/images/relationship-remove.png" /></td><td><%: SolarWinds.DPA.Strings.Resources.IntegrationWizard_Grid_ActionsTooltip_RemoveRelationship %></td></tr>
                    </tbody>
                </table>                
            </div>
        </div>
    </div>   
</asp:Content>