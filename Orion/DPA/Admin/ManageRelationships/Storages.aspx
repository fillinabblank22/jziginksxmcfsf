﻿<%@ Page Title="<%$ Resources: DPAWebContent, ManageRelationships_Storages_Title %>" Language="C#" MasterPageFile="~/Orion/DPA/Admin/ManageRelationships/ManageRelationships.master" 
     AutoEventWireup="true" CodeFile="Storages.aspx.cs" Inherits="Orion_DPA_Admin_ManageRelationships_Storages" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ MasterType VirtualPath="~/Orion/DPA/Admin/ManageRelationships/ManageRelationships.master" %> 

<%@ Register TagPrefix="dpa" TagName="StorageConfigurationModal" Src="~/Orion/DPA/Controls/StorageConfigurationModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="ConnectivityIssueWarning" Src="~/Orion/DPA/Controls/ConnectivityIssueWarning.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="grid" Runat="Server">
<% if(IsSrmInstalled) { %>    
   <dpa:ConnectivityIssueWarning runat="server" ID="ConnectivityIssueWarning" />
   <div id="integrationGrid">
        <div id="filterNotifications" class="sw-filter-notifications"></div>
        <div id="relationshipsPlaceholder" class="sw-relationshipsGrid"></div>
        <div>
            <input type="hidden" id="settingsPrefix" value='<%= LazyColumnDefinition.Value.IdentificationPrefix %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SortOrderKey%>" value='<%= LazyColumnDefinition.Value.GetSortOrder() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.PageSizeKey%>" value='<%= LazyColumnDefinition.Value.GetPageSize() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.GroupingValueKey%>" value='<%= LazyColumnDefinition.Value.GetGroupingValue() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.SelectedColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetSelectedColumns() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.LastUsedFiltersKey%>" value='<%= LazyColumnDefinition.Value.GetLastUsedFilters() %>' />
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsKey%>" value='<%= LazyColumnDefinition.Value.GetColumns() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.ColumnsModelConfigKey%>" value='<%= LazyColumnDefinition.Value.GetColumnsModelConfig() %>'/>
            <input type="hidden" id="<%=LazyColumnDefinition.Value.IdentificationPrefix%>ModelIdentifierName" value='<%= SessionIdentifier %>' />
        </div>
    </div>
    
    <dpa:StorageConfigurationModal ID="StorageConfigurationModal" runat="server" />
    <script>
        $(document).ready(function() {
            SW.DPA.Admin.grid1 = new SW.DPA.Admin.ManageRelationships.StorageGrid('<%= ConnectivityIssueWarning.ClientID %>');
            Ext.onReady(SW.DPA.Admin.grid1.init, SW.DPA.Admin.grid1);
        });
    </script>
<% } else { %>
    <div class="sw-suggestion">
        <span class="sw-suggestion-icon"></span>
        <span><%: Resources.DPAWebContent.RelationshipManagement_Storages_NoSrmInstalledHintMessage %></span><br/>
        <a href="http://www.solarwinds.com/storage-resource-monitor.aspx" target="_blank" class="sw-link" automation="learn-about-srm">»
            <%: Resources.DPAWebContent.RelationshipManagement_Storages_NoSrmInstalled_LearMoreLink %> 
        </a>
    </div>
<% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="sidebar" Runat="Server">
    <div class="DPA_RightBlockHint">
        <div class="sw-suggestion sw-suggestion-info" id="helpPanel">
            <span class="sw-suggestion-icon"></span>
            
            <span class="DPA_CloseHelpPanelButton">
                <a href="#">
                    <img src="/Orion/DPA/images/black_close.png" alt="Close"/>
                </a>
            </span>
            
            <h4 class="DPA_HintTitle" id="DPA_HintTitleSummary"><%: Resources.DPAWebContent.StorageManagement_Sidebar_Title %></h4>
            
            <p><%: Resources.DPAWebContent.StorageManagement_Sidebar_SubTitle %></p>
            
            <ul>
                <li class="DPA_HintDescription"><%: Resources.DPAWebContent.StorageManagement_Sidebar_Li1 %></li>
                <li class="DPA_HintDescription"><%: Resources.DPAWebContent.StorageManagement_Sidebar_Li2 %></li>
                <li class="DPA_HintDescription"><%: Resources.DPAWebContent.StorageManagement_Sidebar_Li3 %></li>
            </ul>
            
            <p class="DPA_HintImage">
                <img src="/Orion/DPA/images/IntegrationWizard/diagram4.png" data-big-img-url="/Orion/DPA/images/IntegrationWizard/diagram4.png" alt="Learn more about relationships picture" />
            </p>

            <p class="DPA_HintLink"><a class="sw-link" target="_blank" href="<%: HelpHelper.GetHelpUrl("DPA", "OrionDPAManageRelationships") %>"><%: Resources.DPAWebContent.ManageRelationships_LearnMore %> »</a></p>

        </div>
    </div>
</asp:Content>
