<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true"  
        Title="DPA jSWIS Query" EnableViewState="false" %>
<%-- disable ViewState on this page, we really don't need to maintain anything across postbacks --%>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css"/>
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.2.0"/>
    <orion:Include runat="server" Module="DPA" File="jSwisScripts.js" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1><%=Page.Title %></h1>
    
    <div id="contents" >
        <div>
            <b>DPA Servers</b><br />
            <select id="dpaServers"><option>Loading ...</option></select>
            <div id="dpaServersErrorMessage"></div><br />
        </div>
        <div class="leftQueryOptions queryOptions">
            <div id="entityList">
                <b>System Queries:</b><br /> 
                <select id="systemQueries"><option>Loading ...</option></select>
                <div id="systemQueriesErrorMessage"></div><br />

               <b>Select entity:</b> <select id="entities"></select> 
               <input type="button" id="generateEntitySwqlBtn" value=" Generate Select Query " />
            </div>
        </div>
        <div class="rightQueryOptions queryOptions">
            <div id="queryList">
                <b>User queries:</b>
                <select id="userQueries"><option>Loading ...</option></select><br />
                
                <input type="text" id="queryNameInput" placeholder="Name of query" /><br />
                <input type="button" id="saveQueryBtn" value="Save Query" /> 
                <input type="button" id="deleteQueryBtn" value="Delete Query" /><br />
                <div id="userQueriesErrorMessage"></div>
            </div>
        </div>
        <div class="clr"></div>

        <div id="query">
            <div id="textareaDiv">resultset
                <textarea id="queryEdit" rows="5">SELECT StatusId, StatusName, ShortDescription, RollupType, Ranking, Color, IconPostfix, ChildStatusMap, DefaultIconName FROM Orion.StatusInfo
                </textarea>
            </div>
            <input type="button" id="executeBtn" value=" Execute Query "></input>


        </div>


        <div id="results">
            <div id="queryElapsedTime"></div>
            <div id="lastErrorMessage"></div>
            <div id="resultset"></div>
        </div>
    </div>
    
</asp:Content>

