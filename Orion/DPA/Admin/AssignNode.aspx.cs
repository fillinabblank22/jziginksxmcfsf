﻿using System;
using SolarWinds.DPA.Common;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Admin_AssignNode : System.Web.UI.Page
{
    protected int DatabaseInstanceId { get; set; }
    protected string DatabaseInstanceName { get; set; }
    protected string DatabaseInstanceIconUrl { get; set; }
    protected string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        int databaseInstanceId;

        if (Request.UrlReferrer != null && Request.QueryString["databaseInstanceId"] != null &&
            int.TryParse(Request.QueryString["databaseInstanceId"], out databaseInstanceId))
        {
            ReturnUrl = Request.UrlReferrer.AbsoluteUri;
            DatabaseInstanceId = databaseInstanceId;

            var databaseInstance =
                SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IDatabaseInstanceDAL>()
                    .GetDatabaseInstanceByGlobalId(databaseInstanceId);

            if (databaseInstance != null)
            {
                DatabaseInstanceName = databaseInstance.Name;
                DatabaseInstanceIconUrl =
                    StatusIconsHelper.GetSmallStatusIcon(SwisEntities.DatabaseInstance, databaseInstance.Status);
                return;
            }
        }

        Response.Redirect(PageUrlHelper.DpaSummaryPage);
    }
}