﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DPASettings.aspx.cs" Inherits="Orion_DPA_Admin_DPASettings" MasterPageFile="~/Orion/DPA/Admin/DPAAdminPage.master" Title="<%$ Resources:DPAWebContent,DPASettingsPage_Title %>" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <style type="text/css">
        body { background: #ecedee; }
        #adminNav { display: none; }
        #adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
        .column1of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
        .column2of2 { padding-left:0px; padding-right:0px; vertical-align:top; text-align: left; width: 50%; }
    </style>
    
    <script type="text/javascript">
        $(document).ready(function () {
            //if not integrated, license summary section is not visible, thus thwack section must go one row upper
            <% if (!Settings.IsIntegrationEstablished) { %>
                $(".row1").find("td.column2of2").html($(".row2").find("td.column2of2").html());
                $(".row2").find("td.column2of2").hide();
            <% } %>
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

    <h1 style="padding-bottom:2px;"><%= Page.Title %></h1>
    <div style="font-size: 11px;">
		<span><%: ModuleInfoHelper.GetModuleVersionString(Resources.DPAWebContent.DPASettings_SubtitlePlusVersion)%></span> - 
        <span><%: ModuleInfoHelper.GetBuildNumberString(Resources.DPAWebContent.DPASettings_BuildNumber)  %></span>
    </div> 

	<table id="adminContent" style="padding-top: 0; padding-left: 0;" width="100%" border="0" cellpadding="0" cellspacing="0">
	    
        <!-- Begin 1st row -->
		  <tr class="row1">
			<td class="column1of2">
				<!-- Begin Getting started Bucket -->		
			
				<div class="NewContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/DPA/images/icon_integration_36x36.gif" />
							</td>
							<td>
								<div class="NewBucketHeader"><%= Resources.DPAWebContent.DPASettings_Integration_01 %></div>
								<p><%= Resources.DPAWebContent.DPASettings_Integration_02%></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr runat="server" ID="IntegrationNotEstablished">
						    <td class="LinkColumn" width="50%">
                                <p id="ServerManagementLink1" runat="server" clientidmode="Static"><span class="LinkArrow">&#0187;</span> 
                                    <a automation="Link_ServerManagement" href="<%: PageUrlHelper.ServerManagement %>"><%: Resources.DPAWebContent.DPASettings_ServerManagement %></a>
                                </p>
						    </td>
                           
							<td class="LinkColumn" width="50%">
								<p id="LearnHowToInstallLink" runat="server" clientidmode="Static"><span class="LinkArrow">&#0187;</span>
                                    <a href="<%: SolarWinds.DPA.Common.CommonConstants.LinkQuickStartGuide %>" target="_blank"><%: Resources.DPAWebContent.DPASettings_LearnHowToInstallLink %></a>
								</p>
							</td>
						</tr>	
                        				    
						<tr runat="server" ID="IntegrationEstablished">
						    <td class="LinkColumn" width="50%">
                                <p id="ServerManagementLink2" runat="server" clientidmode="Static"><span class="LinkArrow">&#0187;</span> 
                                    <a automation="Link_ServerManagement" href="<%: PageUrlHelper.ServerManagement %>"><%: Resources.DPAWebContent.DPASettings_ServerManagement %></a>
                                </p>
						    </td>
                            
                            <td class="LinkColumn" width="50%">
								<p id="RelationshipManagementLink" runat="server" clientidmode="Static"><span class="LinkArrow">&#0187;</span> 
                                    <a href="<%: PageUrlHelper.RelationshipManagementDatabaseInstances %>"><%: Resources.DPAWebContent.DPASettings_RelationshipManagementLink %></a>
								</p>
							</td>
						</tr>
					</table>
				</div>
			
				<!-- End Getting started Bucket -->

			</td>
			
			<!-- Begin 2nd Column -->
			<td class="column2of2">
			
				<!-- Begin License Bucket -->		
			
				<div class="NewContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/DPA/images/icon_license_36x36.gif" />
							</td>
							<td>
								<div class="NewBucketHeader"><%= Resources.DPAWebContent.DPASettings_LicenseSummary_01 %></div>
								<p><%= Resources.DPAWebContent.DPASettings_LicenseSummary_02%></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a target="blank" href="#" onclick="return SW.DPA.LinksToDpaSelector.show('<%= PageUrlHelper.LicenseAllocation %>')"><%= Resources.DPAWebContent.DPASettings_LicenseAllocation %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a target="blank" href="#" onclick="return SW.DPA.LinksToDpaSelector.show('<%= PageUrlHelper.LicenseManagement %>')"><%= Resources.DPAWebContent.DPASettings_LicenseManagement %></a></p>
							</td>
                            <td class="LinkColumn" width="33%">
							    &nbsp;
							</td>
						</tr>
					</table>
				</div>
			
				<!-- End License Bucket -->

			</td>
			<!-- End 2nd Column -->
		</tr>
        <!-- End 1st row -->
        
        <!-- Begin 2nd row -->
        <tr class="row2">
			<td class="column1of2">
				<!-- Begin Getting started Bucket -->		
                
                <!-- Empty cell -->
			
				<!-- End Getting started Bucket -->

			</td>
			
			<!-- Begin 2nd Column -->
			<td class="column2of2">
			
				<!-- Begin License Bucket -->		
			
				<div class="NewContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/DPA/images/Icon.Thwack.gif" />
							</td>
							<td>
								<div class="NewBucketHeader"><%= Resources.DPAWebContent.DPASettings_Thwack_Title %></div>
								<p><%= Resources.DPAWebContent.DPASettings_Thwack_Subtitle%></p>
							</td>
						</tr>
					</table>
					<table class="NewBucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="66%">
								<p><span class="LinkArrow">&#0187;</span> <a target="_blank" href="<%= PageUrlHelper.ThwackForum() %>"><%= Resources.DPAWebContent.DPASettings_Thwack_ThwackLink %></a></p>
							</td>
							<td class="LinkColumn" width="33%">
								<p>&nbsp;</p>
							</td>
						</tr>
					</table>
				</div>
			
				<!-- End License Bucket -->

			</td>
			<!-- End 2nd Column -->
		</tr>
        <!-- End 2nd row -->

	</table>
	<br/>

</asp:Content>