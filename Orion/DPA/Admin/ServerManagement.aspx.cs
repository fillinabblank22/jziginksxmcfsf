﻿using System;
using SolarWinds.DPA.Common.Settings;
using SolarWinds.DPA.ServiceLocator;

public partial class Orion_DPA_Admin_ServerManagement : System.Web.UI.Page
{
    private IDPASettings _settings;
    private IDPASettings Settings
    {
        get
        {
            return _settings ?? (_settings = ServiceLocator.Current.GetInstance<IDPASettings>());
        }
    }

    protected int ServerGridId
    {
        get { return 1; }
    }

    public bool IsServerManagementHelpPanelVisible
    {
        get { return Settings.IsServerManagementHelpPanelVisible; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ServersTable.UniqueClientID = ServerGridId;

        NavigateToRelationshipsManagement.Visible = Settings.IsIntegrationEstablished;
    }
}