﻿<%@ Page Title="<%$ Resources: DPAWebContent, AssignDatabaseInstance_MetaTitle%>" Language="C#" MasterPageFile="~/Orion/DPA/Admin/DpaAdminPage.master" AutoEventWireup="true" CodeFile="AssignDatabaseInstance.aspx.cs" Inherits="Orion_DPA_Admin_AssignDatabaseInstance" %>
<%@ Register Src="~/Orion/DPA/Controls/DatabaseInstanceSelector.ascx" TagPrefix="dpa" TagName="DatabaseInstanceSelector" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="DPA/Admin/styles/AdminBundle.css" />
    <orion:Include runat="server" Module="DPA" File="Common.js" />
    <orion:Include runat="server" File="DPA/Admin/js/DPA.Admin.AssignDatabaseInstance.js" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <div class="DPA_AssignEntity">
        <h1><%: Resources.DPAWebContent.AssignDatabaseInstance_PageTitle %></h1> 
        <p><%: Resources.DPAWebContent.AssignDatabaseInstance_Description %></p>
        <div class="DPA_IconsLine"><img src="<%: ApplicationIconUrl %>" /> <%: ApplicationName %> <%: Resources.DPAWebContent.DatabaseNodeConjunction %> <img src="<%: NodeIconUrl %>" /><%: NodeName %></div>
    
        <div class="DPA_AssignGrid">
            <dpa:DatabaseInstanceSelector ID="DatabaseInstanceSelector" runat="server" />        
        </div>
    
        <div class="DPA_BottomButtonsBar">
            <div class="sw-btn-bar-wizard" id="report-content-dialog-btn-ph">
                <a class="sw-btn sw-btn-primary DPA_SelectObject NoTip" href="<%: ReturnUrl %>"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.AssignDatabaseInstanceModal_SubmitButtonLabel %></span></span></a>
                <a class="sw-btn DPA_CancelButton NoTip" href="<%: ReturnUrl %>"><span class="sw-btn-c"><span class="sw-btn-t"><%: Resources.DPAWebContent.AssignDatabaseInstanceModal_CancelButtonLabel %></span></span></a>
            </div>
        </div>
    </div>
    <script>
        $().ready(function() {
            SW.DPA.Admin.AssignDatabaseInstance(<%= ApplicationId %>, <%= NodeId %>);
        });
    </script>
</asp:Content>