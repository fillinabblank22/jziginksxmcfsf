﻿<%@ Page Title="<%$ Resources: DPAWebContent, ServerManagement_Title%>" Language="C#" MasterPageFile="~/Orion/DPA/Admin/DpaAdminPage.master" AutoEventWireup="true" CodeFile="ServerManagement.aspx.cs" Inherits="Orion_DPA_Admin_ServerManagement" %>

<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Import Namespace="Resources" %>

<%@ Register TagPrefix="dpa" TagName="IntegrationSetupModal" Src="~/Orion/DPA/Controls/ServerManagement/IntegrationSetupModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="EstablishingIntegrationModal" Src="~/Orion/DPA/Controls/ServerManagement/EstablishingIntegrationModal.ascx" %>
<%@ Register TagPrefix="dpa" TagName="SortablePageableTable" Src="~/Orion/DPA/Controls/SortablePageableTable.ascx" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" Module="DPA" File="AdminBundle.css" /> 
    <orion:Include runat="server" Module="DPA" File="ServerManagement.js" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="DPA_ServerManagement">
        <div style="margin-bottom: 10px;" class="sw-suggestion sw-suggestion-warn"><span class="sw-suggestion-icon"></span> 
            <b><%: DPAWebContent.ServerManagement_ObsoleteWarning_Title %></b> <%: DPAWebContent.ServerManagement_ObsoleteWarning_DescriptionFirstPart %> <a href="<%: PageUrlHelper.ServerManagement %>"><%: DPAWebContent.ServerManagement_ObsoleteWarning_DescriptionSecondPart %></a>.
        </div>

        <h1><%= Page.Title %></h1>
        <div class="DPA_ServerManagementContent">
            <p>
                <%= Resources.DPAWebContent.ServerManagement_Description %>
            </p>
            
            <div runat="server" ID="noAdmInstalledWarning" Visible="False">
                <div class="sw-suggestion sw-suggestion-warn">
                    <span class="sw-suggestion-icon"></span>
                    <%: Resources.DPAWebContent.ServerManagement_AdmNotInstalledWarning_Text %>
                </div>
            </div>

            <div class="DPA_InfoBar">
                <span id="DPA_TabSwitcher"></span>
                <span id="NavigateToRelationshipsManagement" class="sw-suggestion" runat="server">
                    <span class="sw-suggestion-icon"></span>
                    <%: Resources.DPAWebContent.ServerManagement_NavigateToRelationshipsManagement_Text %>
                    <a href="<%: PageUrlHelper.RelationshipManagementDatabaseInstances %>" class="sw-link" automation="Link_RelationshipManagement"><%= Resources.DPAWebContent.ServerManagement_NavigateToRelationshipsManagement_LinkText %></a>
                </span>
                <span class="DPA_HintHelpLinkWrapper">
                    <span class="sw-suggestion sw-suggestion-info DPA_HintHelpLink">
                        <span class="sw-suggestion-icon"></span>
                        <a href="#" class="DPA_ShowHelpLink" style="display: none" automation="Btn_ShowHelpLink"><%: Resources.DPAWebContent.ServerManagement_ShowHelpLink %></a>
                        <a href="#" class="DPA_HideHelpLink" style="display: none" automation="Btn_HideHelpLink"><%: Resources.DPAWebContent.ServerManagement_HideHelpLink %></a>
                    </span>
                </span>
            </div>
            
            <div style="display: none" class="sw-suggestion sw-suggestion-fail DPA_ErrorWrapper">
                <span class="sw-suggestion-icon"></span>
                <div class="DPA_ErrorMessage"></div>
            </div>
    
            <div class="DPA_ServerGrid">
                <a href="#" class="DPA_AddDpaServerButton" automation="Btn_AddDpaServer"><%: Resources.DPAWebContent.ServerManagement_AddDpaServerButton_Label %></a>
                <div class="DPA_ServerTable" automation="Wrapper_DpaServerTable">
                    <dpa:SortablePageableTable ID="ServersTable" runat="server" />
                </div>
            </div>
        </div>
        <div class="DPA_SideBar" style="display: none">
            <div class="sw-suggestion sw-suggestion-info" id="helpPanel" automation="Wrapper_HelpPanel">
                <span class="sw-suggestion-icon"></span>
                <span class="DPA_CloseHelpPanelButton" automation="Btn_CloseHelpPanel">
                    <a href="#">
                        <img src="/Orion/DPA/images/black_close.png" alt="Close" />
                    </a>
                </span>
                <h4 class="DPA_HintTitle"><%= Resources.DPAWebContent.ServerManagement_WhereICanGetDpa_Title %></h4>
                <p class="DPA_HintDescription"><%= Resources.DPAWebContent.ServerManagement_WhereICanGetDpa_Description %></p>
                <span class="DPA_HintLink">
                    <a href="<%: PageUrlHelper.DpaDownload %>" target="_blank" class="sw-link" automation="Link_QuickStartGuide">&raquo; <%= Resources.DPAWebContent.ServerManagement_WhereICanGetDpa_GetDpaLink %></a>
                </span>
                <br/>
                <span class="DPA_HintLink">
                    <a href="<%: PageUrlHelper.DpaCrossProductIntegration %>" target="_blank" class="sw-link" automation="Link_LearnDpaIntegration">&raquo; <%= Resources.DPAWebContent.ServerManagement_WhereICanGetDpa_LearnMoreLink %></a>
                </span>
            </div>
        </div>
    </div>
    <dpa:IntegrationSetupModal runat="server" />
    <dpa:EstablishingIntegrationModal runat="server" />
    <script type="text/javascript">
        $().ready(function () {
            var integrationSetupModal = new SW.DPA.Admin.IntegrationSetupModal("DPA_IntegrationSetupModal");

            $(".DPA_AddDpaServerButton").click(function () {
                integrationSetupModal.show(true);
                return false;
            });

            SW.DPA.Admin.ServerManagement.HelpPanel(<%: IsServerManagementHelpPanelVisible.ToString().ToLower() %>);
            var gridId = "<%= ServerGridId %>";
            SW.DPA.Admin.ServerManagement.AvailabilityTabSwitcher($('#DPA_TabSwitcher'));
            var confirmationDialog = new SW.DPA.Admin.ServerManagement.RemoveIntegrationConfirmationDialog();

            var refreshTable = function () {
                SW.DPA.UI.SortablePageableTable.refresh(gridId);
            };
            
            var tableIndex = {}, setTableIndex = function (columns) { $.each(columns, function (i, n) { tableIndex[n] = i; }); };
            var dpaServerLinkTemplate = _.template('<a href="{{{link}}}">{{{label}}}</a>');
            var dpaServerStatusTemplate = _.template('<img src="{{{imageUrl}}}" /> {{{statusDescription}}}');

            var loadVersions = function() {
                $('.DPA_VersionCell').each(function() {
                    var dpaServerId = $(this).data('dpa-server-id');
                    var currentCellElement = $(this);

                    currentCellElement.addClass('DPA_LoadingIcon');
                    SW.Core.Services.callController(
                        "/api/DpaServerManagement/GetVersionOfDpaServer/" + dpaServerId,
                        {},
                        function (result) {
                            currentCellElement.removeClass('DPA_LoadingIcon');
                            if (SW.DPA.Common.isObject(result) && result.Metadata && result.Metadata.EndpointNotAvailable) {
                                currentCellElement.text('<%: Resources.DPAWebContent.CannotLoad_Msg %>');
                            } else {
                                currentCellElement.text(result);
                            }
                        },
                        function (error) {
                            currentCellElement.removeClass('DPA_LoadingIcon');
                            currentCellElement.html('');
                            currentCellElement.text(error);
                        });
                });
            };

            var actionsCellTemplate = _.templateExt(['<a href="#" data-dpa-server-id="{{{DpaServerId}}}" data-dpa-server-name="{{{DpaServerName}}}" automation="Btn_RemoveIntegration" class="DPA_RemoveIntegration" title="{{{Tooltip}}}"><img src="/Orion/images/delete_16x16.gif" /></a>']);

            SW.DPA.UI.SortablePageableTable.initialize(
            {
                uniqueId: gridId,
                pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                    SW.Core.Services.callController(
                        "/sapi/DpaServerManagement/GetDpaServers",
                        {},
                        function (result) {                           
                            setTableIndex(result.DataTable.Columns);
                            onSuccess(result);
                            $('a.DPA_RemoveIntegration').click(function() {
                                var dpaServerId = $(this).data('dpa-server-id');
                                var dpaServerName = $(this).data('dpa-server-name');
                                confirmationDialog.show(dpaServerId, dpaServerName, function() {
                                    location.reload();
                                });
                                return false;
                            });
                            $(".DPA_AddDpaServerLink").click(function () {
                                integrationSetupModal.show(true);
                                return false;
                            });
                            loadVersions();
                        },
                        function (error) {
                        });
                },
                initialPageIndex: 0,
                rowsPerPage: 10,
                noDataMessage: {
                    show: true,
                    isHtml: true,
                    formatter: function () {
                        return "<%: Resources.DPAWebContent.ServerManagement_DpaServers_NoData %>"
                            .replace('[STARTLINK]', '<a href="#" class="DPA_AddDpaServerLink" automation="Link_AddFirstDpaServer">')
                            .replace('[ENDLINK]', '</a>');
                    },
                    cssClassProvider: function () {
                        return "DPA_SortablePageableTableNoData";
                    }
                },
                columnSettings: {
                    'Name': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Name %>",
                        allowSort: false,
                        isHtml: true,
                        formatter: function (cellValue, rowArray) {                       
                            var content = dpaServerLinkTemplate({ link: rowArray[tableIndex.DpaServerDetailsUrl], label: cellValue });

                            return content;
                        }
                    },
                    'Address': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Address %>",
                        allowSort: false
                    },
                    'Port': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Port %>",
                        allowSort: false
                    },
                    'Version': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Version %>",
                        allowSort: false,
                        isHtml: true,
                        formatter: function (cellValue, rowArray) {
                            var content = SW.Core.String.Format('<span data-dpa-server-id="{0}" class="DPA_VersionCell"></span>', rowArray[tableIndex.DpaServerId]);

                            return content;
                        }
                    },
                    'Status': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Status %>",
                        allowSort: false,
                        isHtml: true,
                        formatter: function (cellValue, rowArray) {                       
                            var content = dpaServerStatusTemplate({ imageUrl: rowArray[tableIndex.StatusIconFile], statusDescription: rowArray[tableIndex.StatusDescription] });

                            return content;
                        }
                    },
                    'Actions': {
                        caption: "<%= Resources.DPAWebContent.ServerManagement_TableColumnHeader_Actions %>",
                        allowSort: false,
                        isHtml: true,
                        formatter: function (cellValue, rowArray) {
                            var content = actionsCellTemplate({
                                DpaServerId: rowArray[tableIndex.DpaServerId],
                                DpaServerName: rowArray[tableIndex.Name],
                                Tooltip: '<%: Resources.DPAWebContent.ServerManagement_ServersGrid_RemoveIntegrationButtonTooltip %>'
                            });

                            return content;
                        }
                    }
                }
            });

            refreshTable();
        });
    </script>
</asp:Content>
