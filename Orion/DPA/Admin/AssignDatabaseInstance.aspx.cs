﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPA.Data.DAL;
using SolarWinds.DPA.Web.Helpers;

public partial class Orion_DPA_Admin_AssignDatabaseInstance : System.Web.UI.Page
{
    protected int ApplicationId { get; set; }
    protected string ApplicationName { get;set; }
    protected string ApplicationIconUrl { get; set; }

    protected int NodeId { get; set; }
    protected string NodeName { get; set; }
    protected string NodeIconUrl { get; set; }

    protected string ReturnUrl { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        int applicationId;
        int nodeId;

        if (Request.UrlReferrer != null && Request.QueryString["applicationId"] != null && Request.QueryString["nodeId"] != null
            && int.TryParse(Request.QueryString["applicationId"], out applicationId) && int.TryParse(Request.QueryString["nodeId"], out nodeId))
        {
            ReturnUrl = Request.UrlReferrer.AbsoluteUri;
            ApplicationId = applicationId;
            NodeId = nodeId;

            var application =
                SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<IApplicationDAL>().GetApplication(applicationId);

            var node = SolarWinds.DPA.ServiceLocator.ServiceLocator.Current.GetInstance<INodeDAL>().GetNodeById(nodeId);

            if (application != null && node != null)
            {
                ApplicationName = application.Name;
                ApplicationIconUrl = StatusIconsHelper.GetSmallStatusIcon(application.InstanceType, application.Status,
                    application.ApplicationId);

                NodeName = node.Caption;
                NodeIconUrl = StatusIconsHelper.GetSmallStatusIcon(node.EntityType, node.Status, node.NodeID);
                return;
            }
        }

        Response.Redirect(PageUrlHelper.DpaSummaryPage);
    }
}