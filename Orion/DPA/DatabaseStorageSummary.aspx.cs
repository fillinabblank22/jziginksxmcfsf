﻿using System;
using SolarWinds.DPA.Web.Views;

public partial class Orion_DPA_DatabaseStorageSummary : DpaViewPage
{
    public override string ViewType
    {
        get { return "DPA Database Storage Summary"; }
    }

    public override string HelpFragment
    {
        get
        {
            return "OrionDPADatabaseStorageSummary";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        Title = ViewInfo.ViewTitle;
        ResourceContainer.DataSource = ViewInfo;
        ResourceContainer.DataBind();

        base.OnInit(e);
    }
}