﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/DPA/DPAView.master" AutoEventWireup="true" 
    CodeFile="DpaServerDetails.aspx.cs" Inherits="Orion_DPA_DpaServerDetails" %>
<%@ Import Namespace="SolarWinds.DPA.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DPAPageTitle" Runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled && (base.DpaSettings.NumberOfIntegratedDpaServers != 1)) { %>
        <div class="DPA_BreadcrumbsWrapper">
            <orion:PluggableDropDownMapPath ID="DpaServerSiteMapPath" runat="server" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
        </div>
    <% } %>
    

    <h1>
        <% if(base.DpaSettings.NumberOfIntegratedDpaServers != 1) { %>
        <%: Resources.DPAWebContent.DpaServerDetailsView_Prefix %> <img src="<%= StatusIconsHelper.GetDpaServerStatusIconFile(DpaServer.Status) %>"/> <%: this.DpaServer.DisplayName %>
        <% } else { %>
            <%: SolarWinds.DPA.Strings.Resources.DatabasesTab_DatabasesSummaryTitle %>
        <% } %>
    </h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="DPAMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="ResourceContainer2" />
    </orion:ResourceHostControl>
</asp:Content>