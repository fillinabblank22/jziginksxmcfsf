﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.DPA.Web.Views;

public partial class Orion_DPA_DPAView : System.Web.UI.MasterPage
{
    private bool _allowExportPDF = true;
    private bool _allowViewTimestamp = true;
    private bool _allowDpaSettings = true;

    public string HelpFragment
    {
        get
        {
            if (!(Page is DpaViewPage))
                return String.Empty;

            return ((DpaViewPage)Page).HelpFragment;
        }
    }

    public bool AllowExportPDF
    {
        get { return _allowExportPDF; }
        set { _allowExportPDF = value; }
    }

    public bool AllowViewTimestamp
    {
        get { return _allowViewTimestamp; }
        set { _allowViewTimestamp = value; }
    }

    public bool AllowDpaSettings
    {
        get { return _allowDpaSettings; }
        set { _allowDpaSettings = value; }
    }

    public string CustomizeViewHref
    {
        get
        {
            if (!(Page is DpaViewPage))
                return String.Empty;

            return ((DpaViewPage)Page).CustomizeViewHref;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }
}
