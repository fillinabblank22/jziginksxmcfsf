<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceWrapper.ascx.cs" Inherits="Orion_ResourceWrapper" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<%
Response.Cache.SetCacheability(HttpCacheability.NoCache);
Response.Cache.SetExpires(DateTime.UtcNow);
Response.Cache.SetNoStore();
 %>
<div class="ResourceWrapper <%= DefaultSanitizer.SanitizeHtml(CssClass) %>" ResourceID="<%= ID %>"
    <%-- Adding 2 to the width here to make this line up with legacy resources --%>
    style="width: <%= DefaultSanitizer.SanitizeHtml(this.WidthString) %>; <%= DefaultSanitizer.SanitizeHtml(this.Attributes["style"]) %>" <%= DefaultSanitizer.SanitizeHtml(AutomationTag) %>
    sw-widget-container="<%= ID %>"
    sw-widget-properties="<%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(PropertiesAsJson)) %>"
    sw-widget-title="<%= DefaultSanitizer.SanitizeHtml(Title) %>"
    sw-widget-subtitle="<%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(SubTitle)) %>"
    sw-widget-defaulttitle="<%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(DefaultTitle)) %>">
    <div class="HeaderBar" id="headerBar" runat="server">
        <%if (!IsMobileView)
          {%>
<%--        <div class="xui">
            <xui-menu display-style="tertiary" menu-align="right" icon="menu" class="sw-widget__edit-button">
                <xui-menu-link url="<%# DefaultSanitizer.SanitizeHtml(this.EditLink) %>" class="sw-widget__edit-command">Edit</xui-menu-link>
                <xui-menu-action>Help</xui-menu-action>
            </xui-menu>
        </div>--%>

        <orion:HelpButton runat="server" ID="HelpButton" CssClass="sw-widget__help-command" />
        <asp:PlaceHolder ID="phEditButton" runat="server">
            <orion:LocalizableButtonLink ID="lbtnEdit" runat="server" href="<%# DefaultSanitizer.SanitizeHtml(this.EditLink) %>" DisplayType="Resource" CssClass="EditResourceButton sw-widget__edit-command" LocalizedText="Edit" />
        </asp:PlaceHolder>
        <div class="sw-widget__header__custom"></div>
        <orion:LocalizableButtonLink ID="ManageButton" runat="server" DisplayType="Resource" CssClass="EditResourceButton ManageButton sw-widget__manage-command" LocalizedText="CustomText" />
        <asp:PlaceHolder ID="HeaderButtonsPlaceHolder" runat="server"></asp:PlaceHolder> <!-- used in FB84311 -->
        <div id="customMenu" class="EditResourceButton">
            <asp:DropDownList ID="drDownMenu" runat="server" AutoPostBack="false">
                <asp:ListItem id="liViewOptions" Value="" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_186 %>" Selected="True"></asp:ListItem>
                <asp:ListItem id="li7Days" Value="/Orion/NetPerfMon/CustomChart.aspx?ChartName={0}&CustomPollerID={1}&Rows={2}&PlotStyle={3}&ResourceID={4}&NetObject={5}&Period=Last 7 Days&SampleSize=1H&NetObjectPrefix={6}"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_187 %>"></asp:ListItem>
                <asp:ListItem id="li30Days" Value="/Orion/NetPerfMon/CustomChart.aspx?ChartName={0}&CustomPollerID={1}&Rows={2}&PlotStyle={3}&ResourceID={4}&NetObject={5}&Period=Last 30 Days&SampleSize=1D&NetObjectPrefix={6}"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_188 %>"></asp:ListItem>
                <asp:ListItem id="liEditChart" Value="/Orion/NetPerfMon/CustomChart.aspx?ChartName={0}&CustomPollerID={1}&Rows={2}&PlotStyle={3}&ResourceID={4}&NetObject={5}&Period={6}&SampleSize={7}&NetObjectPrefix={8}"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_189 %>"></asp:ListItem>
                <asp:ListItem id="liViewChartData" Value="/Orion/NetPerfMon/ChartData.aspx?ChartName={0}&CustomPollerID={1}&Rows={2}&NetObject={3}&Period={4}&SampleSize={5}&PeriodBegin={6}&PeriodEnd={7}&SelectedEntities={8}&ChartEntity={9}&ShowSum={10}&DataColumnName={11}"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_190 %>"></asp:ListItem>
                <asp:ListItem id="liViewExcel" Value="/Orion/NetPerfMon/ChartData.aspx?ChartName={0}&CustomPollerID={1}&Rows={2}&NetObject={3}&Period={4}&SampleSize={5}&PeriodBegin={6}&PeriodEnd={7}&DataFormat=Excel&SelectedEntities={8}&ChartEntity={9}&ShowSum={10}&DataColumnName={11}"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_191 %>"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div id='<%= "ResourceID-" + ID %>'><%--SubNav HyperLink Target--%> </div>
        <h1 <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %> >
            <asp:Image runat="server" ID="resourceIcon" Visible="False" CssClass="ResourceIcon" AlternateText=""/>
            <a href='<%= DefaultSanitizer.SanitizeHtml((IsPresentInDB())?DetachURL:string.Empty) %>' target="_blank" class="sw-widget__title">
                <%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Title)) %></a>
        </h1>
        <h2 class="sw-widget__subtitle" <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable()) %> >
            <%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(SubTitle)) %></h2>
        <%}
          else
          {%>
        <div class="mResourceHeader">
            <img src="/Orion/images/Button.Collapse.gif" alt="" style="cursor: pointer;" onclick='if ($("#<%= this.ClientID %>_resContent")[0].style.display == "none") { this.src = "/Orion/images/Button.Collapse.gif"; $("#<%= this.ClientID %>_resContent")[0].style.display = "block"; } else { this.src = "/Orion/images/Button.Expand.gif"; $("#<%= this.ClientID %>_resContent")[0].style.display = "none"; }' />
            <a href='<%= DefaultSanitizer.SanitizeHtml((IsPresentInDB())?DetachURL:string.Empty) %>' target="_blank" class="sw-widget__title">
                <%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(Title)) %></a><br />
            <h2 class="sw-widget__subtitle">
                <%= DefaultSanitizer.SanitizeHtml(CommonWebHelper.EncodeHTMLTags(SubTitle)) %></h2>
        </div>
        <%} %>
    </div>
    <div id="<%= DefaultSanitizer.SanitizeHtml(this.ClientID) %>_resContent" class="ResourceContent" <%= DefaultSanitizer.SanitizeHtml(AngularJsHelper.RenderNgNonBindable(!this.IsAngularJsResource)) %> >
        <asp:PlaceHolder runat="server" ID="ResourcePlaceHolder" />
    </div>
    <div class="ResourceDragAndDropPh">
        <asp:Image ID="imgResourceDragAndDropIcon" runat="server" CssClass="ResourceDragAndDropIcon sw-widget-drag-element" ImageUrl="/Orion/images/DragAndDrop_Target_70x8.png" AlternateText=""/>
    </div>
    <div class="HiddenResourcePh">
        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_Hidden_Resource%>" />
    </div>
    <sw-widget-overlay></sw-widget-overlay>
</div>
