using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Hubble;
using SolarWinds.Orion.Web.UI;
using System.Text.RegularExpressions;
using SolarWinds.Newtonsoft.Json;
using SolarWinds.Newtonsoft.Json.Serialization;
using SolarWinds.Orion.Web.Helpers;
using System.Diagnostics;
using SolarWinds.Orion.Web.Instrumentation;
using SolarWinds.Orion.Core.Common.Instrumentation;
using SolarWinds.Orion.Core.Common.Instrumentation.Keys;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_ResourceContainer : System.Web.UI.UserControl
{
    private static readonly Log myLog = new Log();

    public ViewInfo DataSource { get; set; }

    private ViewInfoCollection Views { get; set; }

    public override bool Visible
    {
        get
        {
            if (this.DataSource == null || this.Views == null)
                return false;

            return base.Visible;
        }
        set 
        { 
            base.Visible = value; 
        }
    }

    public string GetCustomizationUrl()
    {
        return IsDemoMode ? "" :
            string.Format("/Orion/Admin/CustomizeView.aspx?ViewID={0}{1}&ReturnTo={2}",
                View.ViewInfo.ViewID,
                (!string.IsNullOrEmpty(Request["NetObject"]) ? string.Format("&NetObject={0}", HttpUtility.UrlEncode(Request["NetObject"])) : ""),
                View.ReturnUrl);
    }

    public OrionView View => Page as OrionView;

    public string SerializedViewInfo
    {
        get
        {
            dynamic viewInfoTransport = new ExpandoObject();
            var netobject = Request["NetObject"];
            if (!string.IsNullOrEmpty(netobject))
                viewInfoTransport.netObject = netobject;

            if (View.NetObject?.Uri != null)
            {
                var dynamicInfoDict = new Dictionary<string, string>
                {
                    {"uri", View.NetObject.Uri}
                };

                var nodeParent = View.NetObject.GetParentOfType<Node>();
                if (nodeParent?.Uri != null)
                {
                    dynamicInfoDict.Add("nodeUri", nodeParent.Uri);
                }

                viewInfoTransport.dynamicInfo = dynamicInfoDict;
            }

            //default the viewType to the view property
            viewInfoTransport.viewType = View.ViewType;
            
            var viewInfo = View.ViewInfo;
            if (viewInfo != null)
            {
                //if there is a different viewType in the DB, use that instead of the view property
                if (!string.IsNullOrEmpty(viewInfo.ViewType))
                {
                    viewInfoTransport.viewType = viewInfo.ViewType;
                }
                viewInfoTransport.viewId = viewInfo.ViewID;
                viewInfoTransport.moduleId = viewInfo.ModuleID;
                viewInfoTransport.netObjectType = viewInfo.NetObjectType?.ToString();
                viewInfoTransport.readOnly = viewInfo.IsReadOnly;
            }

            PopulateDynamicViewKeys(viewInfoTransport);

            return JsonConvert.SerializeObject(viewInfoTransport, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
        }
    }

    private void PopulateDynamicViewKeys(dynamic viewInfoTransport)
    {
        var supportedKeys = ViewManager.GetSupportedDynamicInfoKeysForViewType(View.ViewType);
        if (supportedKeys.Any())
        {
            Control controlTreeWalker = this;
            while (null != controlTreeWalker)
            {
                var dynamicInfo = controlTreeWalker as IDynamicInfoProvider;
                controlTreeWalker = controlTreeWalker.Parent;
                if (dynamicInfo != null)
                {
                    dynamic dynamicInfoTransport = new ExpandoObject();
                    var dynamicInfoDict = (IDictionary<string, object>) dynamicInfoTransport;
                    foreach (var supportedKey in supportedKeys)
                    {
                        dynamicInfoDict[supportedKey] = dynamicInfo.GetValue(supportedKey);
                    }
                    viewInfoTransport.dynamicInfo = dynamicInfoTransport;
                    break;
                }
            }
        }
    }

    protected string ViewsColumnInfo
    {
        get
        {
            var strBuilder = new StringBuilder("{");
            var comma = "";
            foreach (var view in Views)
            {
                strBuilder.AppendFormat("{0}'{1}':", comma, view.ViewID);
                strBuilder.Append("{");
                strBuilder.AppendFormat("'1': {0},", view.Column1Width);
                strBuilder.AppendFormat("'2': {0},", view.Column2Width);
                strBuilder.AppendFormat("'3': {0},", view.Column3Width);
                strBuilder.AppendFormat("'4': {0},", view.Column4Width);
                strBuilder.AppendFormat("'5': {0},", view.Column5Width);
                strBuilder.AppendFormat("'6': {0}", view.Column6Width);
                strBuilder.Append("}");
                comma = ",";
            }
            strBuilder.Append("}");
            return strBuilder.ToString();
        }
    }

    protected bool IsReadOnlyMode => Views.First().IsReadOnly;

    protected bool IsFullScreenMode => IsReadOnlyMode && Views.First().Column1Width == -1;

    protected bool IsDemoMode => SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

    protected int ToDataRow
	{
		get
		{
			int count = 0;
			if (int.TryParse(Request["ResourcesCount"], out count))
				return count;
			return 5;
		}
	}

    protected int ViewsCount => Views?.Count ?? 1;

    protected bool GetShowLeftNav()
    {
        bool hasMultipleSubviews = 1 < ViewManager.GetViewsByGroup(DataSource.ViewGroup, false, true, ((OrionView)Page).NetObject).Count;
        return (Profile.AllowCustomize || hasMultipleSubviews) &&
            OrionMinReqsMaster.IsApolloEnabled &&
            !OrionMinReqsMaster.IsNOCView &&
            !OrionMinReqsMaster.IsMobileView;
    }

    protected bool IsMobileView
	{
		get
		{
            if (ViewState["ResContainerIsMobileView"] == null)
                ViewState["ResContainerIsMobileView"] = OrionMinReqsMaster.IsMobileView;
            return (bool)ViewState["ResContainerIsMobileView"];
		}
	}

    protected bool IsNOCView
    {
        get
        {
            if (ViewState["ResContainerIsNOCView"] == null)
                ViewState["ResContainerIsNOCView"] = OrionMinReqsMaster.IsNOCView;
            return (bool)ViewState["ResContainerIsNOCView"];
        }
    }

    protected string ShowMoreResourcesUrl
	{
		get {
			var requestString = Request.Url.AbsoluteUri;
			if (!requestString.Contains("ResourcesCount="))
			{
				requestString += string.Format("{0}ResourcesCount={1}", requestString.Contains("?") ? "&" : "?", ToDataRow + 5);
			}
			else
			{
				var reqArray = Regex.Split(requestString, "ResourcesCount=", RegexOptions.IgnoreCase);
				if (reqArray.Length == 2)
				{
					requestString = string.Format("{0}ResourcesCount={1}", reqArray[0], ToDataRow + 5);
					if (reqArray[1].Contains("&"))
						requestString += reqArray[1].Substring(reqArray[1].IndexOf("&"));
				}
			}

			return requestString;
		}
	}

	public override void DataBind()
	{
        // shift the page form to the left when there is no left nav
        if (Page.Form != null && !GetShowLeftNav())
        {
            Page.Form.Attributes["class"] += " " + "sw-hidden-left-nav";
        }

		Views = (IsNOCView && DataSource.ViewGroup > 0 && !IsMobileView)
					   ? ViewManager.GetViewsByGroup(DataSource.ViewGroup, false, true, ((OrionView)Page).NetObject)
					   : new ViewInfoCollection(new List<ViewInfo> { DataSource });
		
		if (Views.Count == 0)
			Views = new ViewInfoCollection(new List<ViewInfo> { DataSource });

        rptContainers.DataSource = Views;
        rptContainers.DataBind();

        if (DataSource.NOCView && !IsMobileView && !IsNOCView)
        {
            var pageLinkMasterGutter = Page.Form.FindControl("TopRightPageLinks") as PageLinkMasterGutter;
            if (pageLinkMasterGutter != null)
            {
                try
                {
                    // try to add NOC link into TopRightPageLinks
                    pageLinkMasterGutter.Controls.AddAt(0, new LiteralControl(
                                                            string.Format("<a target=\"_blank\" href=\"{0}\" class=\"viewInNOCLink\">{1}</a>",
                                                                UrlHelper.UrlAppendUpdateParameter(HttpContext.Current.Request.Url.AbsoluteUri, "isNOCView", "true"),
                                                                Resources.CoreWebContent.WEBDATA_AB0_42)));
                }
                catch (Exception ex)
                {
                    myLog.Warn(string.Format("Unable to add NOC link into top right menu for page '{0}'", DataSource.PagePath), ex);
                }
            }
        }
	}

    protected void rptContainers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (IsMobileView)
		{
            var rptColumn1 = e.Item.FindControl("rptColumn1") as Repeater;
            var showMoreDiv = e.Item.FindControl("ShowMoreDiv") as HtmlGenericControl;
            if (rptColumn1 == null || showMoreDiv == null) return;

            var resources = ViewManager.GetResourcesForView(this.DataSource);
			rptColumn1.DataSource = resources.GetResources(0, ToDataRow);
            rptColumn1.DataBind();
			showMoreDiv.Visible = resources.Count > ToDataRow;
		}
		else
			NonMobileDateBind(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AngularJsHelper.EnableAngularJsForPageContent();

        var file = OrionInclude.CoreFile( "OrionMaster.js", OrionInclude.Section.Bottom );
        file.AddJsInit("$(function() { SW.Core.Header.SyncTo('.ResourceContainer'); });");

        OrionInclude.CoreFile("RenderControl.js", OrionInclude.Section.Bottom);
        OrionInclude.CoreFile("AsyncView.js", OrionInclude.Section.Bottom);
    }
    
    private void NonMobileDateBind(RepeaterItemEventArgs e)
	{
		using (HubbleProfiler.Step("Binding Resource Container"))
		{
		    var viewInfo = e.Item.DataItem as ViewInfo;
		    var leftSubNav = e.Item.FindControl("LeftSubNav") as ViewGroupSubNav;
		    var leftSubNav2 = e.Item.FindControl("LeftSubNav2") as ViewGroupSubNav;
            var tdStaticSubview = e.Item.FindControl("tdStaticSubview") as HtmlTableCell;
            var iframeStaticSubview = e.Item.FindControl("iframeStaticSubview") as HtmlIframe;
            var tdColumn1 = e.Item.FindControl("tdColumn1") as HtmlTableCell;
            var tdColumn2 = e.Item.FindControl("tdColumn2") as HtmlTableCell;
            var tdColumn3 = e.Item.FindControl("tdColumn3") as HtmlTableCell;
            var tdColumn4 = e.Item.FindControl("tdColumn4") as HtmlTableCell;
            var tdColumn5 = e.Item.FindControl("tdColumn5") as HtmlTableCell;
            var tdColumn6 = e.Item.FindControl("tdColumn6") as HtmlTableCell;
            var rptColumn1 = e.Item.FindControl("rptColumn1") as Repeater;
            var rptColumn2 = e.Item.FindControl("rptColumn2") as Repeater;
            var rptColumn3 = e.Item.FindControl("rptColumn3") as Repeater;
            var rptColumn4 = e.Item.FindControl("rptColumn4") as Repeater;
            var rptColumn5 = e.Item.FindControl("rptColumn5") as Repeater;
            var rptColumn6 = e.Item.FindControl("rptColumn6") as Repeater;

            if (leftSubNav == null || viewInfo==null
                || tdColumn1 == null || tdColumn2 == null || tdColumn3 == null || tdColumn4 == null || tdColumn5 == null || tdColumn6 == null 
                || rptColumn1 == null || rptColumn2 == null || rptColumn3 == null || rptColumn4 == null || rptColumn5 == null || rptColumn6 == null)
                return;

            leftSubNav.IsActive = leftSubNav2.IsActive = true;
			leftSubNav.View = leftSubNav2.View = DataSource;

            var resources = ViewManager.GetResourcesForView(viewInfo);

		    if (IsFullScreenMode)
		    {
		        tdColumn1.Style.Add("width","100%");
		    }

            if (viewInfo.IsStaticSubview)
            {
                tdColumn1.Visible = false;
                tdStaticSubview.Visible = true;
                iframeStaticSubview.Src = $"{viewInfo.PagePath}?printable=true&ViewID={viewInfo.ViewID}&NetObject={NetObjectID}";
            }

            tdColumn2.Visible = false;
            tdColumn3.Visible = false;
            tdColumn4.Visible = false;
            tdColumn5.Visible = false;
            tdColumn6.Visible = false;

		    rptColumn1.DataSource = resources.GetResourcesForColumn(1);
		    rptColumn1.DataBind();

            if (viewInfo.ColumnCount > 1)
			{
                tdColumn2.Visible = true;
				rptColumn2.DataSource = resources.GetResourcesForColumn(2);
                rptColumn2.DataBind();

				if (viewInfo.ColumnCount > 2)
				{
                    tdColumn3.Visible = true;
					rptColumn3.DataSource = resources.GetResourcesForColumn(3);
                    rptColumn3.DataBind();

					if (viewInfo.ColumnCount > 3)
					{
                        tdColumn4.Visible = true;
						rptColumn4.DataSource = resources.GetResourcesForColumn(4);
                        rptColumn4.DataBind();

						if (viewInfo.ColumnCount > 4)
						{
                            tdColumn5.Visible = true;
							rptColumn5.DataSource = resources.GetResourcesForColumn(5);
                            rptColumn5.DataBind();
							if (viewInfo.ColumnCount > 5)
							{
                                tdColumn6.Visible = true;
								rptColumn6.DataSource = resources.GetResourcesForColumn(6);
                                rptColumn6.DataBind();
							}
						}
					}
				}
			}
		}
	}

	private bool CheckAsyncPostback(Control control)
	{
		if (control != null)
		{
			Page page = control.Page;
			if (page != null)
			{
				ScriptManager scripter = ScriptManager.GetCurrent(page);
				if (scripter != null)
				{
					return scripter.IsInAsyncPostBack;
				}
			}
		}

		return false;
	}

	protected void ResourcePlaceHolder_DataBind(object sender, EventArgs e)
	{
		PlaceHolder placeholder = (PlaceHolder)sender;
		ResourceInfo rInfo = (ResourceInfo)DataBinder.Eval(placeholder.NamingContainer, "DataItem");

		if (myLog.IsDebugEnabled)
			myLog.DebugFormat("IsInAsyncPostBack = {0}", CheckAsyncPostback(placeholder));

		using (HubbleProfiler.Step(String.Format("{0} ({1})", rInfo.Title, rInfo.ID)))
		{
			if (rInfo.IsAspNetControl)
			{
				string resourcePath = rInfo.File;
				BaseResourceControl control;

                var initWatch = new Stopwatch();

				try
				{
                    initWatch.Restart();
					control = (BaseResourceControl)LoadControl(resourcePath);

                    WebsitePerformanceMonitor.StoreMeasurement(
                        ResourceId.MakeNew(rInfo.ID),
                        OperationType.Init,
                        initWatch.Elapsed);
                }
				catch (HttpException ex)
				{
					myLog.Debug("Unable to load control.  Assume path not valid.  Path: " + resourcePath, ex);
					control = (BaseResourceControl)LoadControl(@"ResourceNotFound.ascx");
				}

				control.Resource = rInfo;
                // some resources may have visible=false on view but we need to put this div to calculate resource position when drag & drop
			    var resourcePanel = new Panel {CssClass = "ResourceWrapperPanel"};

                try
                {
					var marker = new LiteralControl();
					bool inlineRendering;
					bool.TryParse(Request.QueryString["InlineRendering"], out inlineRendering);
					if (control.ResourceLoadingMode == ResourceLoadingMode.RenderControl && !inlineRendering)
					{
						// we use empty control to see name and edit button of the resource while loading it by AJAX call.
						var emptyControl = (EmptyResource)LoadControl(@"/Orion/NetPerfMon/Resources/EmptyResource.ascx");
						emptyControl.Resource = rInfo;

						var editUrl = String.Empty;
						try
						{
							editUrl = control.EditURL;
						}
						catch
						{
							// this can fail due to missing data in EditURL, let's just hide the Edit button (set empty url), it'll fix itself when RenderControl resource is loaded
						}
						emptyControl.SetEditUrl(editUrl);

                        resourcePanel.Controls.Add(emptyControl);
					}
					else
					{
                        if (OrionModuleManager.isResourceAllowed(rInfo.File))
                        {
                            resourcePanel.Controls.Add(control);
                        }
                        else
                        {
                            myLog.WarnFormat("Resource {0} present on view but can't be added due to resource host disabled", rInfo.Name);
                        }
					}
                    resourcePanel.Controls.Add(marker);

					marker.Text = string.Format("<div class='resourceReloadMarker' " +
												"style='display:none;' " +
												"data-resource-id='{0}' " +
												"data-resource-loading-mode='{1}' " +
												"data-naming-container='{2}' " +
                                                "data-current-url='{3}'" +
                                                " ></div>",
												rInfo.ID, 
                                                control.ResourceLoadingMode, 
                                                placeholder.NamingContainer.NamingContainer.UniqueID,
                                                (Page is OrionView) ? ((OrionView)Page).ReturnUrl : string.Empty);

                    placeholder.Controls.Add(resourcePanel);
					myLog.DebugFormat("Loaded resource with ResourceLoadingMode {0} from {1}", control.ResourceLoadingMode, resourcePath);
				}
				catch (Exception ex)
				{
					BaseResourceExceptionControl exceptionHandler = control.CreateExceptionHandlerControl(ex);
					exceptionHandler.Resource = rInfo;
					placeholder.Controls.Remove(control);
					placeholder.Controls.Add(exceptionHandler);
				}
			}
			else
			{
				placeholder.Controls.Add(new Label { Text = string.Format("{0} is an unsupported legacy resource.", rInfo.File) });
			}
		}
	}

	protected string NetObjectID
	{
		get
		{
			OrionView view = Page as OrionView;
			if (view == null)
				return Request.QueryString["NetObject"];
			else
				return view.NetObjectIDForASP;
		}
	}
}
