<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SwisfErrorControl.ascx.cs" Inherits="SwisfErrorControl" %>
<div class='sw-suggestion sw-suggestion-warn' style='width: 100%; padding-left: 0px !important; padding-right: 0px !important; margin-top: 5px; margin-bottom: 5px; margin-left: 0px;' id="SwisFErrorControl">
    <span class='sw-suggestion-icon'></span>
    <span style="padding-left: 25px; font-size: small;font-weight: bold;">
        <%: Resources.CoreWebContent.SwisfErrorControl_ErrorTitle %>:
    </span>
    <br />
    <div id="SwisFErrorControlDescription" style="padding-left: 25px; font-size: small; font-weight: normal; float: left;"><%= DefaultSanitizer.SanitizeHtml(ErrorDescriptHtml) %></div>
</div>
