using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_NodeByIP : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		string ip = Request["IP"];
		IPAddress ipAddr;

        if (string.IsNullOrEmpty(ip) || !IPAddress.TryParse(ip.Trim(), out ipAddr) || (ipAddr.AddressFamily != AddressFamily.InterNetwork && ipAddr.AddressFamily != AddressFamily.InterNetworkV6))
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => string.Format(Resources.CoreWebContent.WEBCODE_VB0_319, ip)) });  // [localization] {Case 59447}

		Dictionary<string, object> sqlParams = new Dictionary<string,object>();
		sqlParams["ip"] = ip.Trim();
		List<NetObject> netObjects = new List<NetObject>(Node.GetFilteredNodes("IP_Address=@ip", sqlParams));
		if (netObjects.Count == 0)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => string.Format(Resources.CoreWebContent.WEBCODE_VB0_320, ip)) });// [localization] {Case 59447}

		if (netObjects.Count > 1)
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => string.Format(Resources.CoreWebContent.WEBCODE_VB0_321, ip)) });// [localization] {Case 59447}

        Response.Redirect("/Orion/View.aspx?NetObject=" + netObjects[0].NetObjectID);
	}
}
