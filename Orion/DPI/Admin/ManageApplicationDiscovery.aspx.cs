﻿using System.Globalization;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Common.DAL.Helpers;
using SolarWinds.DPI.Common.Models;
using System;

public partial class Orion_DPI_Admin_ManageApplicationDiscovery : System.Web.UI.Page
{
    private void btnSubmit_Click(object sender, EventArgs e)
    {
        using (var p = new DPIBusinessLayerProxy(HttpContext.Current.Profile.UserName))
        {
            DpiDiscoverySettings settings = p.GetApplicationDiscoverySettings();
            settings.ApplicationsByDataVolume = DiscoverySettingsHelper.ParseApplicationsByDataVolume(tbAppNum.Text);
            settings.ApplicationBytesPercentageLimit = DiscoverySettingsHelper.ParseApplicationBytesPercentageLimit(tbDataVolume.Text);
            settings.IsApplicationDiscoveryActive = DiscoverySettingsHelper.ParseIsDiscoveryActive(ddlAppDiscoveryState.SelectedValue);
            settings.ApplicationDiscoveryUrlLevel = DiscoverySettingsHelper.ParseDiscoveryUrlLevel(ddlDiscoveryUrlLevel.SelectedValue);
            settings.ApplicationDiscoveryTraffic = DiscoverySettingsHelper.ParseDiscoveryTraffic(ddlDiscoveryTraffic.SelectedValue);
            settings.IsNodeDiscoveryActive = DiscoverySettingsHelper.ParseIsDiscoveryActive(ddlNodeDiscoveryState.SelectedValue);
            settings.NodeDiscoveryTraffic = DiscoverySettingsHelper.ParseDiscoveryTraffic(ddlNodeDiscoveryTraffic.SelectedValue);
            p.SetApplicationDiscoverySettings(settings);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        btnSubmit.Click += btnSubmit_Click;

        ddlAppDiscoveryState.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState_Inactive,
            Convert.ToString((int)ApplicationDiscoveryMode.Inactive, CultureInfo.InvariantCulture)));
        ddlAppDiscoveryState.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState_Active,
            Convert.ToString((int)ApplicationDiscoveryMode.Active, CultureInfo.InvariantCulture)));

        ddlDiscoveryUrlLevel.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryUrlLevel_None,
            Convert.ToString((int)ApplicationDiscoveryUrlLevel.None, CultureInfo.InvariantCulture)));
        ddlDiscoveryUrlLevel.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryUrlLevel_Level0,
            Convert.ToString((int)ApplicationDiscoveryUrlLevel.Level0, CultureInfo.InvariantCulture)));
        ddlDiscoveryUrlLevel.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryUrlLevel_Level1,
            Convert.ToString((int)ApplicationDiscoveryUrlLevel.Level1, CultureInfo.InvariantCulture)));

        ddlDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Server,
            Convert.ToString((int)DiscoveryTraffic.Server, CultureInfo.InvariantCulture)));
        ddlDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Client,
            Convert.ToString((int)DiscoveryTraffic.Client, CultureInfo.InvariantCulture)));
        ddlDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Both,
            Convert.ToString((int)DiscoveryTraffic.Both, CultureInfo.InvariantCulture)));

        ddlNodeDiscoveryState.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState_Inactive,
            Convert.ToString((int)ApplicationDiscoveryMode.Inactive, CultureInfo.InvariantCulture)));
        ddlNodeDiscoveryState.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState_Active,
            Convert.ToString((int)ApplicationDiscoveryMode.Active, CultureInfo.InvariantCulture)));

        ddlNodeDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Server,
            Convert.ToString((int)DiscoveryTraffic.Server, CultureInfo.InvariantCulture)));
        ddlNodeDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Client,
            Convert.ToString((int)DiscoveryTraffic.Client, CultureInfo.InvariantCulture)));
        ddlNodeDiscoveryTraffic.Items.Add(new ListItem(Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Both,
            Convert.ToString((int)DiscoveryTraffic.Both, CultureInfo.InvariantCulture)));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (var p = new DPIBusinessLayerProxy())
            {
                DpiDiscoverySettings settings = p.GetApplicationDiscoverySettings();

                tbAppNum.Text = Convert.ToString(settings.ApplicationsByDataVolume, CultureInfo.InvariantCulture);
                tbDataVolume.Text = Convert.ToString(settings.ApplicationBytesPercentageLimit, CultureInfo.InvariantCulture);
                ddlAppDiscoveryState.SelectedValue = Convert.ToString(settings.IsApplicationDiscoveryActive ? 1 : 0, CultureInfo.InvariantCulture);
                ddlDiscoveryUrlLevel.SelectedValue = Convert.ToString((int)settings.ApplicationDiscoveryUrlLevel, CultureInfo.InvariantCulture);
                ddlDiscoveryTraffic.SelectedValue = Convert.ToString((int)settings.ApplicationDiscoveryTraffic, CultureInfo.InvariantCulture);
                ddlNodeDiscoveryTraffic.SelectedValue = Convert.ToString((int)settings.NodeDiscoveryTraffic, CultureInfo.InvariantCulture);
                ddlNodeDiscoveryState.SelectedValue = Convert.ToString(settings.IsNodeDiscoveryActive ? 1 : 0, CultureInfo.InvariantCulture);
            }
        }
    }
}