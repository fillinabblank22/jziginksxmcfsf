﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectObjectsControl.ascx.cs" Inherits="Orion_DPI_Admin_Controls_SelectObjectsControl" %>

<orion:include runat="server" file="Admin/Containers/Containers.css" />
<orion:include runat="server" framework="Ext" frameworkversion="3.4" />
<orion:include runat="server" file="OrionCore.js" />
<orion:include runat="server" file="DPI/Admin/Controls/SelectObjects/SelectObjects.js" />

<div class="swis-error-wrapper" style="width: 99%;"></div>

<div id="GroupItemsSelector">
    <div class="GroupSection">
        <label for="groupBySelect"><%= Resources.CoreWebContent.WEBDATA_VB0_5 %></label>
        <select id="groupBySelect">
        </select>
    </div>
    <div class="GroupSection">
        <label for="searchBox"><%= Resources.CoreWebContent.WEBDATA_TM0_225 %></label>
        <table>
            <tr>
                <td>
                    <input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
                <td><a href="javascript:void(0);" id="searchButton">
                    <img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
    </div>
</div>

<div id="ContainerMembersTable" style="width: 99%;"></div>

<div id="AddButtonPanel" style="padding: 10px;">
    <a id="AddToGroupButton" href="javascript:void(0);" style="top: 44%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" />
    </a>
    <a id="RemoveFromGroupButton" href="javascript:void(0);" style="top: 46%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" />
    </a>
</div>

<input type="hidden" id="groupByType" />
<asp:HiddenField ID="gridItems" runat="server" ClientIDMode="Static" />

<script type="text/javascript">
    // <![CDATA[
    SW.Orion.DPI.SelectObjects.SetTitles('<%= LeftPanelTitle %>', '<%= RightPanelTitle %>');
    SW.Orion.DPI.SelectObjects.SetEntity('<%= EntityName %>');
    SW.Orion.DPI.SelectObjects.SetFilter("<%= Filter %>");
    SW.Orion.DPI.SelectObjects.SetSingleSelectionOnly(<%= SingleSelectionOnly.ToString(System.Globalization.CultureInfo.InvariantCulture).ToLowerInvariant() %>);
    SW.Orion.DPI.SelectObjects.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');
    <% if (!SuppressInitOnLoad)
    { %>
    Ext.onReady(SW.Orion.DPI.SelectObjects.init, SW.Orion.DPI.SelectObjects);
    <% } %>

    // ]]>
</script>