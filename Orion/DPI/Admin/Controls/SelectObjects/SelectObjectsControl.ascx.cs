﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Common.Models;
using System.Data;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_DPI_Admin_Controls_SelectObjectsControl : System.Web.UI.UserControl
{
    public String LeftPanelTitle { get; set; }      // title of the left panel
    public String RightPanelTitle { get; set; }     // title of the right panel
    public String Filter { get; set; }
    public bool SingleSelectionOnly { get; set; }

    // Orion.Nodes, Orion.NPM.Interfaces,...
    public String EntityName
    {
        get
        {
            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }

    // when set to true, no init is called by default, but the initialization needs to be called explicitely (SW.Orion.DPI.SelectObjects.init)
    public bool SuppressInitOnLoad { get; set; }

    public String SelectedItems
    {
        get
        {
            return gridItems.Value;
        }

        set
        {
            gridItems.Value = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        EntityName = String.Empty;
    }

}
