﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<orion:include runat="server" framework="Ext" frameworkversion="4.0" />
<orion:include runat="server" file="OrionCore.js" />
<orion:include runat="server" file="DPI/Admin/Controls/CollectorEdit/CollectorEditControl.js" />
<orion:include runat="server" file="DPI/styles/CollectorEditControl.css" />

<div id="<%= this.ClientID %>">
    <div class="dpi-collectorEdit" id="dpi-collectorEditCtrl">
        <table>
            <tr>
                <td class="bold"><%= Resources.DPIWebContent.ConfigureCollector_Device %></td>
                <td class="td-last">
                    <asp:Label runat="server" ID="lbTitle" ClientIDMode="Static" /></td>
            </tr>
            <tr>
                <td class="bold"><%= Resources.DPIWebContent.ConfigureCollector_Interface %></td>
                <td class="td-last">
                    <asp:DropDownList runat="server" ID="ddlInterfaces" ClientIDMode="Static" /></td>
            </tr>
            <tr>
                <td class="bold" colspan="2" style="vertical-align: bottom"><%= Resources.DPIWebContent.ConfigureCollector_DoNotExceed %></td>
            </tr>
            <tr>
                <td class="do-not-exceed" colspan="2">
                    <table>
                        <tr>
                            <td class="bold"><%= Resources.DPIWebContent.ConfigureCollector_Memory %></td>
                            <td class="dd-select">
                                <asp:DropDownList runat="server" ID="tbMemory" ClientIDMode="Static" /></td>
                            <td class="td-last"><span clientidmode="Static" id="recMemory"></span></td>
                        </tr>
                        <tr>
                            <td class="bold"><%= Resources.DPIWebContent.ConfigureCollector_CPU %></td>
                            <td class="dd-select">
                                <asp:DropDownList runat="server" ID="tbCPU" ClientIDMode="Static" /></td>
                            <td class="td-last"><span clientidmode="Static" id="recCPU"></span></td>
                        </tr>
                        <tr id="warning_inline_help" clientidmode="Static">
                            <td class="warn" colspan="3">
                                <div class="sw-suggestion" style="margin-top: 12px; margin-bottom: 12px;">
                                    <span class="sw-suggestion-icon"></span>
                                    <%= Resources.DPIWebContent.ConfigureCollector_WarnUseRecommended %>
                                    <!-- &nbsp; &raquo; <a href="#"><%= Resources.DPIWebContent.ConfigureCollector_LearnMore %></a> -->
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>