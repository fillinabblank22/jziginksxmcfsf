﻿SW.DPI = SW.DPI || {};
SW.DPI.CollectorEditControl = SW.DPI.CollectorEditControl || {};

(function (c) {
    var serviceUrl = "/Orion/DPI/Services/CollectorNodes.asmx";
    var probeModel;
    var dialogElement;
    var CPU_AVAIL;
    var CPU_RECOMMENDED;
    var CPU_MAX_RECOMMENDED = 97;
    var MEM_AVAIL;
    var MEM_RECOMMENDED;
    var MEM_MAX_RECOMMENDED = 10;

    function ShowDialog(dialogId) {
        dialogElement = $("#" + dialogId);
        dialogElement.show().dialog({
            minWidth: 510,
            modal: true,
            overlay: {
                "background-color": "black",
                opacity: 0.4
            },
            title: "@{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNodeTitle;E=js}"
        });
    }

    function CloseDialog() {
        dialogElement.dialog("close");
    }

    function Validate() {
        var result = true;

        var memory = $("#tbMemory").val();
        if ((memory == null || memory == "")
            && (isNaN(parseInt(memory)) || !isFinite(memory)
                || (probeModel.Capabilities.Hardware.PhysicalMemoryMb != null && (parseInt(memory) > probeModel.Capabilities.Hardware.PhysicalMemoryMb / 1024))
                || (parseInt(memory) <= 0))) {
            result = false;
        }

        var cpu = $("#tbCPU").val();
        if ((cpu == null || cpu == "")
            && (isNaN(parseInt(cpu)) || !isFinite(cpu)
                || (probeModel.Capabilities.Hardware.CpuCores != null && (parseInt(cpu) > probeModel.Capabilities.Hardware.CpuCores))
                || (parseInt(cpu) <= 0))) {
            result = false;
        }

        if (!result) {
            Ext4.Msg.minWidth = 310;
            Ext4.Msg.alert(
                '@{R=DPI.Strings;K=Web_ConfigureCollector_Validation_Title;E=js}',
                '@{R=DPI.Strings;K=Web_ConfigureCollector_Validation_Msg;E=js}');
        }

        return result;
    }

    function DisableSubmitButton() {
        $("#editDialogSave").addClass("x-item-disabled");
        $("#editDialogSave .sw-btn-c").on("click", function (ev) {
            ev.stopPropagation();
            return false;
        });
    }

    function checkLimitSelections() {
        if ((MEM_AVAIL > 0 && $("#tbMemory").val() < MEM_RECOMMENDED * 1024) || (CPU_AVAIL > 0 && $("#tbCPU").val() < CPU_RECOMMENDED)) {
            $("#warning_inline_help").show();
        } else {
            $("#warning_inline_help").hide();
        }

        updateRecommendedText();
    }

    function updateRecommendedText() {
        var styleMem = ($("#tbMemory").val() < MEM_RECOMMENDED * 1024) ? "font-weight:bold;" : "";
        var styleCpu = ($("#tbCPU").val() < CPU_RECOMMENDED) ? "font-weight:bold;" : "";

        if (MEM_AVAIL > 0) {
            $("#recMemory").html("<span style='" + styleMem + "'>@{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Recommended;E=js}: " + MEM_RECOMMENDED + " GB</span> &nbsp; &nbsp; @{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Available;E=js}: " + MEM_AVAIL + " GB");
        }
        else {
            //$("#recMemory").html("@{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Recommended;E=js}: N/A &nbsp; &nbsp; @{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Available;E=js}: N/A");
        }

        if (CPU_AVAIL > 0) {
            $("#recCPU").html("<span style='" + styleCpu + "'>@{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Recommended;E=js}: " + CPU_RECOMMENDED + "</span> &nbsp; &nbsp; @{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Available;E=js}: " + CPU_AVAIL);
        }
        else {
            //$("#recCPU").html("@{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Recommended;E=js}: N/A &nbsp; &nbsp; @{R=DPI.Strings;K=Web_ManageProbes_EditCollectorNode_Available;E=js}: N/A");
        }
    }

    function calculateRecommended(capabilities, localTrafficOnly) {
        if (capabilities == null) return;

        CPU_AVAIL = capabilities.Hardware.CpuCores;
        MEM_AVAIL = convertMegsToGigs(capabilities.Hardware.PhysicalMemoryMb);

        if (localTrafficOnly) {
            // if there are less than 2 CPUs, recommend one, otherwise two
            CPU_RECOMMENDED = CPU_AVAIL > 2 ? 2 : 1;

            // recommend at max 2 GB
            MEM_RECOMMENDED = MEM_AVAIL > 2 ? 2 : 1;
        }
        else {
            // recommend the largest prime number available
            var pL = getPrimes(CPU_AVAIL);
            CPU_RECOMMENDED = (pL.length > 0) ? pL[pL.length - 1] : 1;
            CPU_RECOMMENDED = Math.min(CPU_RECOMMENDED, CPU_MAX_RECOMMENDED);

            // recommend half the memory but stay under limit
            MEM_RECOMMENDED = Math.round(MEM_AVAIL / 2);
            MEM_RECOMMENDED = Math.min(MEM_RECOMMENDED, MEM_MAX_RECOMMENDED);
        }
    }

    function convertMegsToGigs(n) {
        var numberAsString = (n / 1024).toString();
        return numberAsString.indexOf('.') != -1 ? parseFloat(numberAsString.substring(0, numberAsString.indexOf('.') + 2)) : parseFloat(numberAsString);
    }

    function getPrimes(max) {
        var sieve = [], i, j, primes = [];
        for (i = 2; i <= max; ++i) {
            if (!sieve[i]) {
                primes.push(i);
                for (j = i << 1; j <= max; j += i) {
                    sieve[j] = true;
                }
            }
        }
        return primes;
    }

    c.OpenDialog = function (dialogElementId, probeId) {
        var loadMask = new Ext4.LoadMask("adminContent");
        loadMask.show();

        // when memory is < 1GB, offer only 1 CPU
        $("#tbMemory").on('change', function () {
            if ($(this).val() < 1024) {
                $("#tbCPU").val('1');
                $("#tbCPU").attr('disabled', 'disabled');
            } else {
                $("#tbCPU").removeAttr('disabled');
            }
        });

        ORION.callWebService(
            serviceUrl,
            "GetProbeForEdit", {
            probeId: probeId
        },
            function (model) {
                probeModel = model;

                var ddlInterfaces = $("#ddlInterfaces");
                ddlInterfaces.empty();

                if (model.Capabilities == null) {
                    alert('@{R=DPI.Strings;K=Web_ConfigureCollector_Error_NoResponse;E=js}');
                    ddlInterfaces.prop("disabled", true);
                    DisableSubmitButton();
                } else {
                    $.each(model.Capabilities.Interfaces, function (i, iface) {
                        var interfOption = $("<option />").val(iface.Identifier).text(iface.Name);
                        if (model.UserConfiguration.CapturingInterface.indexOf(iface.Identifier) != -1) {
                            interfOption.attr('selected', true);
                        }
                        ddlInterfaces.append(interfOption);
                    });
                }

                calculateRecommended(model.Capabilities, model.LocalTrafficOnly);

                $("#tbCPU").empty();
                for (var i = 1; i <= CPU_AVAIL; i++) {
                    $("#tbCPU").append('<option value="' + i + '">' + i + '</option>');
                }

                $("#tbMemory").empty();

                if (MEM_AVAIL >= 1) {
                    $("#tbMemory").append('<option value="' + 256 + '">' + 256 + ' MB</option>');
                    $("#tbMemory").append('<option value="' + 512 + '">' + 512 + ' MB</option>');
                    $("#tbMemory").append('<option value="' + 768 + '">' + 768 + ' MB</option>');
                }

                for (i = 1; i <= MEM_AVAIL; i++) {
                    $("#tbMemory").append('<option value="' + (i * 1024) + '">' + i + ' GB</option>');
                }

                if (MEM_AVAIL % 1 != 0)
                    $("#tbMemory").append('<option value="' + Math.floor(MEM_AVAIL * 1024) + '">' + MEM_AVAIL + ' GB</option>');

                $("#lbTitle").text(model.UserConfiguration.Name);

                if (model.UserConfiguration.MemoryLimit == null) {
                    $("#tbMemory").val(MEM_RECOMMENDED * 1024);
                }
                else {
                    $("#tbMemory").val(model.UserConfiguration.MemoryLimit);
                }

                if (model.UserConfiguration.CpuLimit == null) {
                    $("#tbCPU").val(CPU_RECOMMENDED);
                } else {
                    $("#tbCPU").val(model.UserConfiguration.CpuLimit);
                }

                $("#tbCPU,#tbMemory").change(function () {
                    checkLimitSelections();
                });

                $("#tbMemory").trigger('change');

                updateRecommendedText();

                checkLimitSelections();

                loadMask.hide();
                ShowDialog(dialogElementId);
            });
    }

    c.Save = function (successCallback) {
        if (!Validate()) {
            return false;
        }

        probeModel.UserConfiguration.CapturingInterface = $("#ddlInterfaces option:selected").val();
        probeModel.UserConfiguration.MemoryLimit = $("#tbMemory").val();
        probeModel.UserConfiguration.CpuLimit = $("#tbCPU").val();

        ORION.callWebService(
            serviceUrl,
            "SaveProbeForEdit", {
            model: probeModel
        },
            function (model) {
                CloseDialog();
                if (typeof successCallback == 'function') {
                    successCallback();
                }
            },
            function (err) {
                alert("Error in saving Probe: " + err);
            }
        );
    }

    c.Close = function () {
        CloseDialog();
    }
})(SW.DPI.CollectorEditControl);