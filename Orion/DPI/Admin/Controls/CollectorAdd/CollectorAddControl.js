﻿SW.DPI = SW.DPI || {};
SW.DPI.CollectorAddControl = SW.DPI.CollectorAddControl || {};

(function (c) {
    var serviceUrl = "/Orion/DPI/Services/CollectorNodes.asmx";
    var deploymentFailedHelp = "@{R=DPI.Strings;K=Web_ManageProbes_AgentNotFunctional;E=js}";
    var dialog1Id = "addCollectorDialog1";
    var dialog2Id = "addCollectorDialog2";
    var dialog1Initialized = false;
    var dialog2Initialized = false;
    var rootId = null;
    var btSaveId = "addDialog1Save";
    var allTrafficEngineId = null;

    var CleanDialog1 = function () {
        SW.Orion.DPI.DeploymentSettingsGrid.Clear();

        DisableSubmitButton();
    };

    var CleanDialog2 = function () {
        SW.Orion.DPI.SelectObjects.Clear();
    };

    function ShowDialog1() {
        if (dialog1Initialized) {
            CleanDialog1();
            $("#" + dialog1Id).dialog("show");
        }
        dialog1Initialized = true;

        $("#" + dialog1Id).show().dialog({
            minWidth: 710,
            minHeight: 500,
            modal: true,
            overlay: {
                "background-color": "black",
                opacity: 0.4
            },
            title: '@{R=DPI.Strings;K=Web_ManageProbes_AddPacketAnalysisEngine;E=js}',
            close: function () {
                // overcome the issue with jquery.dialog / extjs window integration
                SW.Orion.DPI.DeploymentSettingsGrid.CloseAssignCredentialsWindow();
                $("#" + dialog2Id).dialog("close");
            }
        });
    }

    var CloseDialog1 = function () {
        $("#" + dialog1Id).dialog("close");
    };

    var ShowDialog2 = function () {
        if (dialog2Initialized) {
            CleanDialog2();
            $("#" + dialog2Id).dialog("show");
        }
        dialog2Initialized = true;

        $("#" + dialog2Id).show().dialog({
            minWidth: 710,
            height: 635,
            modal: true,
            overlay: {
                "background-color": "black",
                opacity: 0.4
            },
            title: "@{R=DPI.Strings;K=Web_ManageProbes_DeployingAgent_Title;E=js}"
        });

        setTooltip('wmiCredetialsHelp', '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_WMICredentials_Help;E=js}');

        SW.Orion.DPI.SelectObjects.init();
    };

    var buttonDisabledByForce = false;

    function setTooltip(id, tip) {
        var tt = new Ext.ToolTip({
            target: id,
            title: tip,
            plain: true,
            showDelay: 0,
            hideDelay: 0,
            trackMouse: false
        });
    }

    function DisableSubmitButton(force) {
        if (force)
            buttonDisabledByForce = true;

        $("#" + btSaveId).addClass("x-item-disabled");
    }

    function EnableSubmitButton(force) {
        if (force)
            buttonDisabledByForce = false;
        else if (buttonDisabledByForce) // when we were disabled by force, do not enable, unless we are enabled by force again
            return;

        $("#" + btSaveId).removeClass("x-item-disabled");
    }

    function SubmitButtonDisabled() {
        return $("#" + btSaveId).hasClass("x-item-disabled");
    }

    c.OpenDialog1 = function (startType) {
        if (startType === "addNetwork") {
            $("#alltrafficEngine").prop("checked", true);
            $("#sensorGridLabel").html('@{R=DPI.Strings;K=DPINodeWizard_DeployCollector_NodesToDeployNetworkOn;E=js}:');
        }
        else if (startType === "addServer") {
            $("#localEngine").prop("checked", true);
            $("#sensorGridLabel").html('@{R=DPI.Strings;K=DPINodeWizard_DeployCollector_NodesToDeployServerOn;E=js}:');
        }

        ShowDialog1();
    };

    c.RemoveSelectedNodes = function () {
        SW.Orion.DPI.DeploymentSettingsGrid.RemoveSelected();
    };

    c.ShowDialog2 = function () {
        SW.Orion.DPI.SelectObjects.SetExcludeIds(SW.Orion.DPI.DeploymentSettingsGrid.GetIds());
        ShowDialog2();
    };

    c.CloseDialog1 = function () {
        CloseDialog1();
    };

    c.SaveDialog2 = function () {
        var items = SW.Orion.DPI.SelectObjects.GetItems();
        var newOnes = [];
        for (var i = 0; i < items.length; ++i) {
            newOnes.push({ NodeId: items[i].Id, Name: items[i].Name, NodeStatus: items[i].MemberStatus, CredentialTestResult: 0 });
        }
        SW.Orion.DPI.DeploymentSettingsGrid.Add(newOnes);
    };

    c.CloseDialog2 = function () {
        $("#" + dialog2Id).dialog("close");
    };

    c.SetElementId = function (id) {
        rootId = id;
    };

    c.Init = function () {
        $("#alltrafficEngine,#localEngine").click(function () {
            if ($(this).val() === "localEngine") {
                $("#sensorGridLabel").html('@{R=DPI.Strings;K=DPINodeWizard_DeployCollector_NodesToDeployServerOn;E=js}:');
            }
            else {
                $("#sensorGridLabel").html('@{R=DPI.Strings;K=DPINodeWizard_DeployCollector_NodesToDeployNetworkOn;E=js}:');
            }
        });

        DisableSubmitButton();

        SW.Orion.DPI.DeploymentSettingsGrid.SetCredentialsList([]);
        SW.Orion.DPI.DeploymentSettingsGrid.SetNodeList([]);

        SW.Orion.DPI.DeploymentSettingsGrid.OnUpdate(function () {
            if (SW.Orion.DPI.DeploymentSettingsGrid.HaveAnyCredential())
                EnableSubmitButton();
            else
                DisableSubmitButton();
        });
    };

    c.SetRadioIds = function (localEngineId, _allTrafficEngineId) {
        allTrafficEngineId = _allTrafficEngineId;
    };

    var doActualSubmit = function () {
        var loadMask = new Ext4.LoadMask("adminContent");
        loadMask.show();

        var nodesToDeploy = SW.Orion.DPI.DeploymentSettingsGrid.GetResult();
        var isAllTraffic = $('#' + allTrafficEngineId).is(':checked');

        var callForNodes = function (nodelist, failedNodes, onFinished) {
            if (!nodelist.length) {
                onFinished(failedNodes);
                return;
            }

            var currentNode = nodelist[0];

            nodelist.shift();

            ORION.callWebService(
                serviceUrl,
                "DeployRemoteProbe", {
                nodeId: currentNode.NodeId,
                credentialId: currentNode.CredentialId || -1,
                credentialName: currentNode.CredentialUsername || "",
                credentialPass: currentNode.CredentialPassword || "",
                allTraffic: isAllTraffic
            },
                function (probeDeploymentResult) {
                    if (probeDeploymentResult.ProbeId == -1) {
                        callForNodes(nodelist, failedNodes.concat([{ node: currentNode, message: probeDeploymentResult.DeploymentError + deploymentFailedHelp }]), onFinished);
                    }
                    else {
                        callForNodes(nodelist, failedNodes, onFinished);
                    }
                },
                function (err) {
                    callForNodes(nodelist, failedNodes.concat([{ node: currentNode, message: err }]), onFinished);
                }
            );
        };

        callForNodes(nodesToDeploy, [], function (failedNodes) {
            loadMask.hide();
            if (!failedNodes.length) {
                CleanDialog1();
                CloseDialog1();

                var loc = window.location.href,
                    index = loc.indexOf('#');

                if (index > 0) {
                    window.location = loc.substring(0, index);
                } else {
                    window.location.reload();
                }
            }
            else {
                EnableSubmitButton(true);
                var failedNodesNode = [];
                Ext.each(failedNodes, function (item) {
                    failedNodesNode.push(item.node);
                    SW.Orion.DPI.DeploymentSettingsGrid.SetErrorMessage(item.node, item.message);
                });
                SW.Orion.DPI.DeploymentSettingsGrid.RemoveAllExcept(failedNodesNode);
            }
        });
    };

    c.Proceed = function () {
        if (SubmitButtonDisabled()) {
            return;
        }

        DisableSubmitButton(true);

        if (!SW.Orion.DPI.DeploymentSettingsGrid.AreAllCredentialsValid()) {
            SW.Orion.DPI.DeploymentSettingsGrid.TestUntestedCredentials(
                doActualSubmit,
                function () {
                    EnableSubmitButton(true);
                }
            );
        }
        else {
            doActualSubmit();
        }
    };
})(SW.DPI.CollectorAddControl);