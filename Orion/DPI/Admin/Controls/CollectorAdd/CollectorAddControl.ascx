﻿<%@ Control CodeFile="CollectorAddControl.ascx.cs" Inherits="Orion_DPI_Admin_Controls_CollectorAdd_CollectorAddControl" Language="C#" AutoEventWireup="false" %>

<orion:include runat="server" framework="Ext" frameworkversion="3.4" />

<orion:include runat="server" file="OrionCore.js" />
<orion:include runat="server" file="DPI/Admin/Controls/CollectorAdd/CollectorAddControl.js" />
<orion:include runat="server" file="DPI/Admin/js/DeploymentSettingsGrid.js" />
<orion:include runat="server" file="DPI/styles/wizard/DpiNodeWizard.css" />
<orion:include runat="server" file="DPI/styles/CollectorEditControl.css" />
<orion:include runat="server" file="DPI/styles/wizard/ManageSpanCollectors.css" />

<div id="<%= this.ClientID %>">
    <div class="sw-suggestion" style="margin-bottom: 15px">
        <span class="sw-suggestion-icon"></span>
        <%= string.Format(Resources.DPIWebContent.DPINodeWizard_DeployCollector_Hint_EngineType, SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionDPIDeployingPacketAnalysisSensors")) %>
    </div>

    <div>
        <b><%= Resources.DPIWebContent.DPINodeWizard_DeployCollector_EngineType %>:</b>
        <table cellpadding="0" border="0" style="padding-top: 0.5em; padding-bottom: 0.5em; border-spacing: 0.4em">
            <tr>
                <td style="vertical-align: top">
                    <asp:RadioButton ClientIDMode="Static" runat="server" ID="localEngine" GroupName="engineType" /></td>
                <td style="position: relative; top: -2px;">
                    <label for="<%=localEngine.ClientID %>"><b><%= Resources.DPIWebContent.DPINodeWizard_DeployCollector_Engine_Server %></b></label>
                    <div style="font-size: smaller; margin-bottom: 6px;"><%=Resources.DPIWebContent.DPINodeWizard_DeployCollector_Engine_Server_Description %></div>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <asp:RadioButton ClientIDMode="Static" runat="server" ID="alltrafficEngine" GroupName="engineType" /></td>
                <td style="position: relative; top: -2px;">
                    <label for="<%=alltrafficEngine.ClientID %>"><b><%= Resources.DPIWebContent.DPINodeWizard_DeployCollector_Engine_Network %></b></label>
                    <div style="font-size: smaller; margin-bottom: 6px;"><%=Resources.DPIWebContent.DPINodeWizard_DeployCollector_Engine_Network_Description %></div>
                </td>
            </tr>
        </table>
        <span id="sensorGridLabel" clientidmode="Static" style="font-weight: bold;"><%= Resources.DPIWebContent.DPINodeWizard_DeployCollector_NodesToDeployServerOn %>:</span>
        <div id="localCollectorsGrid" class="collectors-grid" style="margin-top: 10px;"></div>
        <script type="text/javascript">
            (function () {
                function Init() {
                    SW.DPI.CollectorAddControl.Init();
                    SW.DPI.CollectorAddControl.SetRadioIds('<%=localEngine.ClientID%>', '<%=alltrafficEngine.ClientID%>');

                    SW.Orion.DPI.DeploymentSettingsGrid.SetCredentialsList(<%= CredentialsList %>);

                    SW.Orion.DPI.DeploymentSettingsGrid.Init(false,
                        [
                            {
                                id: 'addNode',
                                text: '<%= Resources.DPIWebContent.Web_ManageProbes_DeployCollector_AddNode %>',
                                iconCls: 'deployment-settings-add-node-button',
                                handler: function () {
                                    SW.DPI.CollectorAddControl.ShowDialog2();
                                }
                            },
                            '-'
                        ], [
                            '-',
                            {
                                id: 'deleteNode',
                                text: '<%= Resources.DPIWebContent.Web_ManageProbes_DeployCollector_DeleteNode %>',
                                iconCls: 'deployment-settings-delete-node-button',
                                handler: function () {
                                    SW.DPI.CollectorAddControl.RemoveSelectedNodes();
                                }
                            }
                        ],
                        true
                    );
                        }
        Ext.onReady(Init, SW.Orion.DPI.DeploymentSettingsGrid);

    })();
        </script>
    </div>
</div>