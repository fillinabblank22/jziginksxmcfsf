﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Common.Models;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_DPI_Admin_Controls_CollectorAdd_CollectorAddControl : UserControl
{
    private readonly JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();
    private readonly CredentialManager _credentialManager = new CredentialManager();

    protected readonly LicensingInfo _licensingInfo;

    public Orion_DPI_Admin_Controls_CollectorAdd_CollectorAddControl()
    {
        using (var bl = new DPIBusinessLayerProxy())
        {
            _licensingInfo = bl.GetLicenseInfo();
        }
    }

    public bool IsUnifiedITManager
    {
        get
        {
            return RegistrySettings.IsUnifiedITManager();
        }
    }

    protected string CredentialsList
    {
        get
        {
            return
                _jsSerializer.Serialize(
                    (new[] { new KeyValuePair<int, string>(-1,
                        Resources.DPIWebContent.DPINodeWizard_DeploymentSettings_CredentialNew
                    ) }).Concat(
                        _credentialManager.GetCredentialNames<UsernamePasswordCredential>(
                            CoreConstants.CoreCredentialOwner)));
        }
    }

    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);

        localEngine.Enabled = _licensingInfo.AvailableLocalTrafficProbes > 0;
        alltrafficEngine.Enabled = _licensingInfo.AvailableAllTrafficProbes > 0;

        if (!IsPostBack && localEngine.Enabled)
        {
            localEngine.Checked = true;
        }
        else if (!IsPostBack && alltrafficEngine.Enabled)
        {
            alltrafficEngine.Checked = true;
        }
    }
}