﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ValidationPropertyAttribute("NodeId")]
public partial class Orion_DPI_Admin_Controls_SingleNodeSelector_SingleNodeSelector : System.Web.UI.UserControl
{
    public string NodeName
    {
        get { return this.hdnNodeName.Value; }
        set
        {
            this.txtNodeName.Text = value;
            this.hdnNodeName.Value = value;
        }
    }

    public string NodeUri
    {
        get { return this.hdnNodeUri.Value; }
        set { this.hdnNodeUri.Value = value; }
    }

    public int? NodeId
    {
        get { return NetObjectFromUri(this.NodeUri).Value; }
    }

    /// <summary>
    /// Additional filter for SQL.
    /// </summary>
    public string Filter { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (this.IsPostBack)
        {
            this.txtNodeName.Text = this.hdnNodeName.Value;
        }
    }

    static private KeyValuePair<string, int?> NetObjectFromUri(string uri)
    {
        string entityType = SolarWinds.Orion.Web.Helpers.UriHelper.GetUriType(uri);
        var entityId = SolarWinds.Orion.Web.Helpers.UriHelper.GetId(uri);
        return string.IsNullOrWhiteSpace(entityType)
            ? new KeyValuePair<string, int?>(null, null)
            : new KeyValuePair<string, int?>(entityType, entityId);
    }
}