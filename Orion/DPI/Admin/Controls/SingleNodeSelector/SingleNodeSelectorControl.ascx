﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SingleNodeSelectorControl.ascx.cs" Inherits="Orion_DPI_Admin_Controls_SingleNodeSelector_SingleNodeSelector" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagPrefix="orion" TagName="Progress" %>
<%@ Register Src="~/Orion/Controls/NetObjectPicker.ascx" TagPrefix="orion" TagName="NetObjectPicker" %>

<orion:Include runat="server" File="DPI/Admin/Controls/SingleNodeSelector/SingleNodeSelector.js" Section="Bottom" />
<style type="text/css">
    .x-grid3-viewport .x-grid3-hd-row td, .x-grid3-viewport .x-grid3-row-table td {
        padding-bottom: 0 !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        SW.Orion.DPI.selector_<%=this.DialogWindow.ClientID %> = new SW.Orion.DPI.SingleNodeSelector({
                dialog: '<%=this.DialogWindow.ClientID %>',
                nodeText: '<%=this.txtNodeName.ClientID %>',
                nodeName: '<%=this.hdnNodeName.ClientID %>',
                nodeUri: '<%=this.hdnNodeUri.ClientID %>',
                browseButton: '<%=this.BrowseButton.ClientID %>',
                submitButton: '<%=this.SelectButton.ClientID %>',
                closeButton: '<%=this.CancelButton.ClientID %>'
            },
            '<%=this.Filter %>'
        );

    // [oh] make standard client side validators works
    // like: require field validator, etc...
    (function (thisId, hdnValId) {
        // clear ASP validators .. we will add them later
        var valEl = $('#' + hdnValId);
        if (valEl) valEl[0].Validators = undefined;

        if (typeof(Page_Validators) != "undefined") {
            $.each(Page_Validators, function() {
                if (this.controltovalidate != thisId) return true;
                // re-enable validator, set new value field and hook value change events...
                this.enabled = true;
                this.controltovalidate = hdnValId;
                ValidatorHookupControlID(this.controltovalidate, this);
                return false;
            });
        }
    })('<%=this.ClientID %>', '<%=this.hdnNodeUri.ClientID %>');

});
</script>

<asp:TextBox ID="txtNodeName" ReadOnly="true" runat="server" onclick="$('#BrowseButton').trigger('click')" />
<asp:HiddenField ID="hdnNodeName" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hdnNodeUri" runat="server" ClientIDMode="Static" />
<orion:LocalizableButton ClientIDMode="Static" id="BrowseButton" runat="server" DisplayType="Secondary" LocalizedText="CustomText"
    Text="<%$ Resources: DPIWebContent, SingleNodeSelector_BrowseButton %>" />

<div id="DialogWindow" style="display: none;" runat="server">
    <orion:NetObjectPicker ID="NetObjectPicker" runat="server" />
    <div class='sw-btn-bar-wizard' style="margin-bottom: 0; padding-bottom: 0;">
        <orion:LocalizableButton ID="SelectButton" runat="server" DisplayType="Primary" LocalizedText="CustomText"
            Enabled="false" Text="<%$ Resources: DPIWebContent, SingleNodeSelector_SelectNode %>" />
        <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" />
    </div>
</div>