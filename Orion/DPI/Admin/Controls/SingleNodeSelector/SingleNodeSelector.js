﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.namespace('SW.Orion.DPI');

SW.Orion.DPI.SingleNodeSelector = function () {
    ORION.prefix = "Orion_DPI_SingleNodeSelector_";

    var texts = {
        'toomany.title': '@{R=Core.Strings;K=WEBJS_PCC_8; E=js}',
        'toomany.text': '@{R=Core.Strings;K=WEBJS_PCC_7; E=js}',
        'dialog.title': '@{R=DPI.Strings;K=Web_SingleNodeSelector_PopupTitle;E=js}'
    };

    var _node = { name: null, uri: null, valid: false };
    var _defaults = {
        dialog: undefined,          // dialog div element ID
        nodeText: undefined,        // text box to show selected node name
        nodeName: undefined,        // hidden field to sotre node name
        nodeUri: undefined,         // hidden field to store node ID
        browseButton: undefined,
        submitButton: undefined,
        closeButton: undefined
    };

    var SetUpNetObjectPicker = function (selected, onSelect, filter) {
        SW.Orion.NetObjectPicker.SetText('toomany.title', texts['toomany.title']);
        SW.Orion.NetObjectPicker.SetText('toomany.text', texts['toomany.text']);
        if (filter && filter != '') {
            SW.Orion.NetObjectPicker.SetFilter(filter);
        }

        SW.Orion.NetObjectPicker.updateSourceNameDelegate = function (name) {
            var nobj = SW.Orion.NetObjectPicker.GetSelectedEntities() || [];
            if (!name || $.trim(name) === "" ||                  // check for net object name
                !nobj || !$.isArray(nobj) || nobj.length != 1) { // check for "selected" exactly one netobject
                onSelect($.extend({}, _node)); return;
            } else {
                onSelect({ name: name, uri: nobj[0], valid: true });
            }
        };

        // [oh] setup and select after 'selection' handler assignment
        var sel = selected && $.isArray(selected) ? selected : [];
        SW.Orion.NetObjectPicker.SetUpEntities([{ MasterEntity: 'Orion.Nodes' }], 'Orion.Nodes', sel, 'Single');
    };
    var TearDownNetObjectPicker = function (unselect) {
        // [oh] unselect before revert back 'selection' handler
        if (unselect === true)
            SW.Orion.NetObjectPicker.SetSelectedEntities([]);

        SW.Orion.NetObjectPicker.updateSourceNameDelegate = null;
    };

    var showDialog = function () {
        var dlg = this.els['dialog'], btn = this.buttons.submit,
            node = this.node, lastSel = node.uri && node.uri != '' ? [node.uri] : [];

        SetUpNetObjectPicker(
            lastSel,
            function (n) {
                node = $.extend(node, n);
                if (btn) btn[node.valid ? 'removeClass' : 'addClass']('sw-btn-disabled');
            },
            this.filter
        );

        if (dlg && dlg.dialog) dlg.dialog({
            position: 'center',
            width: 890,
            height: 'auto',
            modal: true,
            draggable: true,
            title: texts['dialog.title'],
            show: { duration: 0 },
            close: function () { }
        });
    };

    var closeDialog = function (submit) {
        var node = this.node, dlg = this.els['dialog'],
            txt = this.els['nodeText'],
            val = this.els['nodeName'],
            uri = this.els['nodeUri'];

        if (submit) {
            // prevent to submit invalid node
            if (!node.valid) return;

            var fullName = node.name.replace(/<\/?[^>]+(>|$)/g, "");
            if (txt) txt.val(fullName);
            if (val) val.val(fullName);
            if (uri) uri.val(node.uri);
        }

        if (dlg && dlg.dialog) dlg.dialog('close');
        TearDownNetObjectPicker(!submit);
    };

    return function (options, sqlFilter) {
        this.filter = sqlFilter;
        this.node = selection = $.extend({}, _node);
        this.opts = $.extend(true, {}, _defaults, options);
        var els = {}; $.each(this.opts, function (n, id) { els[n] = $('#' + id); });
        this.els = els;
        this.buttons = {
            browse: els['browseButton'],
            submit: els['submitButton'],
            close: els['closeButton']
        };

        var scope = this;
        if (this.buttons.browse)
            this.buttons.browse.click(function (event) {
                event.preventDefault();
                scope.node.uri = scope.els['nodeUri'] ? scope.els['nodeUri'].val() : '';
                showDialog.apply(scope, []);
                return false;
            });

        if (this.buttons.submit)
            this.buttons.submit.click(function (event) {
                event.preventDefault();
                closeDialog.apply(scope, [true]);
                return false;
            });

        if (this.buttons.close)
            this.buttons.close.click(function (event) {
                event.preventDefault();
                closeDialog.apply(scope, [false]);
                return false;
            });
    };
}();