﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CollectorNodesControl.ascx.cs" Inherits="Orion_DPI_Admin_Controls_CollectorNodes_CollectorNodesControl" %>

<%@ Register Src="~/Orion/DPI/Admin/Controls/CollectorEdit/CollectorEditControl.ascx" TagPrefix="orion" TagName="CollectorEditControl" %>
<%@ Register Src="~/Orion/DPI/Admin/Controls/CollectorAdd/CollectorAddControl.ascx" TagPrefix="orion" TagName="CollectorAddControl" %>
<%@ Register Src="~/Orion/DPI/Admin/Controls/SelectObjects/SelectObjectsControl.ascx" TagPrefix="orion" TagName="SelectObjectsControl" %>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="4.0" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="DPI/js/EscapeHtml.js" />
<orion:Include runat="server" File="DPI/Admin/Controls/CollectorNodes/CollectorNodes.js" />
<orion:Include runat="server" File="DPI/Admin/js/ManagePages.Renderers.js" />
<orion:Include runat="server" File="DPI/Admin/Controls/CollectorNodes/CollectorNodes.css" />
<orion:Include runat="server" File="DPI/styles/wizard/DpiNodeWizard.css" />

<asp:HiddenField runat="server" ID="hfSelectedIds" ClientIDMode="Static" />
<div id="chooseCollectorNodesExtGrid" style="margin: 0"></div>

<div id="editCollectorDialog" style="display: none;">
    <orion:CollectorEditControl runat="server" ID="CollectorEditControl" ControlIdToMask="adminContent" />
    <div class="sw-btn-bar" style="text-align: right; vertical-align: top;">
        <orion:LocalizableButtonLink ID="editDialogSave" ClientIDMode="Static" DisplayType="Primary" LocalizedText="Save" runat="server" />
        <orion:LocalizableButtonLink ID="editDialogCancel" ClientIDMode="Static" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
    </div>
</div>

<div id="addCollectorDialog1" style="display: none;">
    <orion:CollectorAddControl runat="server" ID="CollectorAddControl" ControlIdToMask="adminContent" />
    <div class="sw-btn-bar" style="text-align: right; padding-top: 25px;">
        <orion:LocalizableButtonLink ID="addDialog1Save" ClientIDMode="Static" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPIWebContent, Web_ManageProbes_DeployingAgent_DeployButton %>" runat="server" />
        <orion:LocalizableButtonLink ID="addDialog1Cancel" ClientIDMode="Static" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
    </div>
</div>

<div id="addCollectorDialog2" style="display: none;">
    <orion:SelectObjectsControl ID="selectObjectsControl" runat="server" SingleSelectionOnly="False" />
    <div class="sw-btn-bar" style="text-align: right;">
        <orion:LocalizableButtonLink ID="addDialog2Save" ClientIDMode="Static" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: DPIWebContent, Web_ManageProbes_DeployingAgent_AddNodes %>" runat="server" />
        <orion:LocalizableButtonLink ID="addDialog2Cancel" ClientIDMode="Static" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
    </div>
</div>

<div id="AssignCredentialWindow" style="display: none;">
    <table>
        <tbody>
            <tr>
                <td>
                    <div class="x-panel-body sw-deployment-settings-credential-window">
                        <p>
                            <%= Resources.DPIWebContent.DPINodeWizard_DeploymentSettings_ChooseExistingOrCreateNewOne %>
                            <img src="/Orion/DPI/images/info.png" id="wmiCredetialsHelp" />
                        </p>
                        <table>
                            <tr class="sw-deployment-settings-first-row">
                                <td><%= Resources.DPIWebContent.DPINodeWizard_DeploymentSettings_ChooseCredential %></td>
                                <td>
                                    <select id="CredentialNamesCombo">
                                        <option value="test">test</option>
                                    </select></td>
                            </tr>
                            <tr class="sw-deployment-settings-hideable">
                                <td><%= Resources.DPIWebContent.DPINodeWizard_DeploymentSettings_UserName %></td>
                                <td>
                                    <input type="text" id="CredentialUsername" value="" /></td>
                            </tr>
                            <tr class="sw-deployment-settings-hideable">
                                <td><%= Resources.DPIWebContent.DPINodeWizard_DeploymentSettings_Password %></td>
                                <td>
                                    <input type="password" id="CredentialPassword" value="" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <orion:LocalizableButton ID="imgbTest" LocalizedText="Test" DisplayType="Small" runat="server" OnClientClick="SW.Orion.DPI.DeploymentSettingsGrid.TestCredentialsInPopup();return false;" />
                                    <ul id="testResults" style="display: none;"></ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="sw-btn-bar" style="text-align: right;">
                        <orion:LocalizableButtonLink ID="assignCredentialWindowSubmit" ClientIDMode="Static" DisplayType="Primary" LocalizedText="Submit" runat="server" />
                        <orion:LocalizableButtonLink ID="assignCredentialWindowCancel" ClientIDMode="Static" DisplayType="Secondary" LocalizedText="Cancel" runat="server" />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        SW.DPI.CollectorNodes.Init(<%= this.Mode %>);
    });
</script>
