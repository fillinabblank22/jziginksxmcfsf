﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public partial class Orion_DPI_Admin_Controls_CollectorNodes_CollectorNodesControl : System.Web.UI.UserControl
{
    public List<int> SelectedIds
    {
        get
        {
            return hfSelectedIds.Value
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(t => int.Parse(t, CultureInfo.InvariantCulture))
                .ToList();
        }
        set
        {
            hfSelectedIds.Value = string.Join(",", value);
        }
    }

    /// <summary>
    /// Mode:
    ///     0 - List of Collector nodes with Add/Edit functionality, etc.
    ///     1 - SingleSelect control
    /// </summary>
    public int Mode { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        selectObjectsControl.EntityName = "Orion.Nodes"; // setting this in ascx doesn't work for unknown reasons
        var windowsFilter = "e.Vendor=\'Windows\' AND e.NodeDescription NOT LIKE \'%x86%\' AND e.NodeDescription NOT LIKE \'%Windows Version 5.%\' AND e.NodeDescription NOT LIKE \'%Windows Version 4.%\'";
        var nodesWithSensors = "SELECT n.NodeId FROM Orion.Nodes n INNER JOIN Orion.AgentManagement.Agent a on a.NodeId = n.NodeId  INNER JOIN Orion.DPI.Probes p ON p.AgentID = a.AgentId union (SELECT n.NodeId FROM Orion.Nodes n INNER JOIN Orion.AgentManagement.Agent a on a.NodeID = n.NodeID INNER JOIN Orion.DPI.Probes p ON p.AgentID = a.AgentId) union (SELECT NodeID FROM Orion.DPI.ProbeAssignments)";
        selectObjectsControl.Filter = string.Format("e.NodeID NOT IN ({0}) AND ({1})", nodesWithSensors, windowsFilter);
        selectObjectsControl.SuppressInitOnLoad = true;
        selectObjectsControl.LeftPanelTitle = Resources.DPIWebContent.Web_ManageProbes_DeployingAgent_NodeSelection_LeftPanel;
        selectObjectsControl.RightPanelTitle = Resources.DPIWebContent.Web_ManageProbes_DeployingAgent_NodeSelection_RightPanel;
    }
}