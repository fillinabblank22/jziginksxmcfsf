Ext4.namespace("SW.DPI.CollectorNodes");

(function (n) {
    var serviceUrl = "/Orion/DPI/Services/CollectorNodes.asmx";
    var itemsPerPage = 20;
    var leftGrid;
    var leftDataStore;
    var enabled = true;
    var btnAddCollectorNode;
    var btnEdit;
    var btnEnable;
    var btnDisable;
    var btnDelete;
    var controlMode = 0;   //0 - List mode, 1 - SingleSelect mode

    var statusTips =
    {
        0: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Unknown;E=js}', //Unknown,
        1: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Up;E=js}',//Up
        2: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Down;E=js}',//Down
        3: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Warning;E=js}',//Warning
        7: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_NotPresent;E=js}',//Not Present
        9: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Unmanaged;E=js}',//Unmanaged
        28: '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_NotLicensed;E=js}'//Not Licensed
    };
    // need to prevent multiple search calls that are close together when typing fast
    var typingTimer;
    var doneTypingInterval = 300;
    var canSearch = true;

    var nodeNames = {};

    var expandedProbeIds = [];

    var modelFields = [
        { name: 'ProbeID' },
        { name: 'Name' },
        { name: 'Enabled' },
        { name: 'Mode' },
        { name: 'DpiNodeCount' },
        { name: 'Utilization' },
        { name: 'AgentStatus' },
        { name: 'PluginStatus' },
        { name: 'PluginStatusNumeric' },
        { name: 'PluginStatusDescription' }
    ];

    var position = 0;
    var isFirstSelection = true;

    function onLeftSelectionChange(isInit) {
        if (!(isInit === true)) {
            if (isFirstSelection) {
                isFirstSelection = false;
                $('body').scrollTop(position);
            }
        }

        if (parseInt(controlMode) === 0) {
            var selectedModels = leftGrid.getSelectionModel().getSelection();
            var selectedCount = selectedModels.length;
            //btnDelete.setDisabled(selectedCount !== 1);
            btnDelete.setDisabled(selectedCount === 0);

            if (selectedCount == 1) {
                btnEdit.setDisabled(selectedModels[0].raw.PluginStatusNumeric == 7 /* notpresent */);
            } else {
                btnEdit.setDisabled(true);
            }

            var anyProbeDisabled = false;
            var anyProbeEnabled = false;
            for (var i = 0; i < selectedModels.length; i++) {
                anyProbeEnabled |= selectedModels[i].data.Enabled;
                anyProbeDisabled |= !selectedModels[i].data.Enabled;
            }

            //var mixedStatuses = anyProbeDisabled && anyProbeEnabled;
            btnEnable.setDisabled((selectedCount === 0) || (selectedCount === 1 && anyProbeEnabled));
            btnDisable.setDisabled((selectedCount === 0) || (selectedCount === 1 && anyProbeDisabled));
        }
        else if (parseInt(controlMode) === 1) {
            saveSingleSelect();
        }
    }

    function getLeftSelectedIds() {
        var selectedModels = leftGrid.getSelectionModel().getSelection();
        var selectedIds = [];

        for (var i = 0; i < selectedModels.length; i++) {
            selectedIds.push(selectedModels[i].internalId);
        }

        return selectedIds;
    }

    //#region LeftGrid Toolbar
    function enableSelectedProbes(enable) {
        ORION.callWebService(
            serviceUrl,
            "SetProbesEnabled", {
            probeIds: getLeftSelectedIds(),
            enabled: enable
        },
            function () { reloadLeftGrid(); });
    }

    function deleteSelectedProbes() {
        var probeMsg = "";
        if (getLeftSelectedIds().length > 1) {
            probeMsg = "@{R=DPI.Strings;K=Web_ManageProbes_DeleteProbesDialog_Msg;E=js}".replace("{0}", getLeftSelectedIds().length);
        } else {
            var probeId = getLeftSelectedIds()[0];
            var probeName = "";
            for (var i = 0; i < leftDataStore.data.items.length; i++) {
                var probeModel = leftDataStore.data.items[i].data;
                if (probeModel.ProbeID === probeId) {
                    probeName = probeModel.Name;
                    break;
                }
            }
            probeMsg = "@{R=DPI.Strings;K=Web_ManageProbes_DeleteProbeDialog_Msg;E=js}".replace("{0}", escapeHtml(probeName));
        }

        var confirmDlg = new Ext4.window.MessageBox({
            buttons: [{
                type: 'buttons',
                text: '@{R=DPI.Strings;K=Web_ManageProbes_DeleteProbeDialog_OK;E=js}',
                handler: function (btn, o) {
                    ORION.callWebService(
                        serviceUrl,
                        "DeleteProbes",
                        { probeIds: getLeftSelectedIds() },
                        function () {
                            confirmDlg.close();
                            reloadLeftGrid();
                        });
                }
            },
            {
                type: 'buttons',
                text: '@{R=DPI.Strings;K=Web_ManageProbes_DeleteProbeDialog_Cancel;E=js}',
                handler: function (btn, o) {
                    confirmDlg.close();
                }
            }]
        });

        confirmDlg.show({
            title: "@{R=DPI.Strings;K=Web_ManageProbes_DeleteProbeDialog_Title;E=js}",
            msg: probeMsg,
            icon: Ext4.MessageBox.QUESTION
        });
    }

    function editSelectedProbe() {
        var selectedProbe = leftGrid.getSelectionModel().getSelection()[0].raw;
        showEditDialog(selectedProbe.ProbeID);
    }

    function addDpiProbe() {
        SW.DPI.CollectorAddControl.OpenDialog1();
    }

    function fixAlignIssues(grid) {
        //This fixes a bug with missaligned corner after the grid is created and loaded for the 1st time.
        var counter = 20;
        var fixFunc = function () {
            var w = grid.width;
            grid.setWidth(w - 1);
            ///grid.columns[0].setWidth(w + 1);
            grid.setWidth(w);
            counter--;
            if (counter > 0)
                setTimeout(fixFunc, 100);
        };
        fixFunc();
    }

    function reloadLeftGrid() {
        leftDataStore.load();
    }

    function initSubGrid(htmlContainer, expandedProbeId, expandedProbeLicenseType) {
        leftGrid.setLoading(true);
        ORION.callWebService(
            serviceUrl,
            "GetNodesByProbeId",
            { probeId: expandedProbeId },
            function (data) {
                var isServerLic = (parseInt(expandedProbeLicenseType) === 1);

                var pRow = $("#" + htmlContainer).closest("tr").prev();

                if (pRow.hasClass("x4-grid-row-alt")) {
                    $("#" + htmlContainer).closest("td").css({ "background": "#f4f4f4" });
                }

                // custom ADD QoE NODE BUTTON
                var addQoeBtn = '<table class="qoe-add-wrap"><tr><td><a href="/Orion/DPI/Admin/DPINode/ChooseDpiNode.aspx?probeId=' + expandedProbeId + '" id="qoeAddNode" class="qoe-add-btn">@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_AddNodeToMonitor;E=js}</a></td></tr></table>';

                var table = $("<table class='subGrid x4-panel-body-default NeedsZebraStripes' />");

                //Header
                var tpart = $("<thead style='line-height: 22px;' class='x4-panel-header-default'/>");

                var th = $("<tr>");
                th.append($("<td class='nodeColumn' >@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_Node;E=js}</td>"));
                th.append($("<td class='appColumn' style='width: 99%;'>@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_Applications;E=js}</td>"));
                if (controlMode == 0) {
                    th.append($("<td class='cmdColumn' >@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_Edit;E=js}</td>"));
                    if (!isServerLic) {
                        th.append($("<td class='cmdColumn' >@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_Delete;E=js}</td>"));
                    }
                }
                tpart.append(th);
                table.append(tpart);

                tpart = $("<tbody />");
                for (var i = 0; i < data.length; i++) {
                    var tr = $("<tr />");
                    tr.append(
                        $("<td class='nodeColumn' style='min-width: 300px;'><a href='/Orion/View.aspx?NetObject=N:{0}' target='_blank' >{1}</a>"
                            .replace("{0}", data[i].NodeID)
                            .replace("{1}", escapeHtml(data[i].NodeName))));
                    var td = $("<td class='appColumn' style='width: 99%; padding-right:30px;'/>");

                    var html = "";
                    var editAppURL = '/Orion/DPI/Admin/DPINode/ChooseDpiNode.aspx?EditID=' + data[i].NodeID;
                    for (var a = 0; a < data[i].Apps.length; a++) {
                        /*
                        if (a > 1) {
                            html += "<a href='" + editAppURL + "' onmouseover='SW.DPI.CollectorNodes.NodeMouseOver(event, " + data[i].NodeID + ")'>@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_AppsListEtc;E=js}</a>";
                            break;
                        }*/
                        if (a !== 0)
                            html += "@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_AppsListSeparator;E=js}";

                        html += '<a href="/Orion/DPI/DPIAppDetails.aspx?NetObject=DPA:{0}" target="_blank" >{1}</a>'
                            .replace("{0}", data[i].Apps[a].ID)
                            .replace("{1}", data[i].Apps[a].Name);
                    }

                    if (data[i].Apps.length === 0) {
                        html = "@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_NoAppsDefined;E=js} &raquo; <a href='" + editAppURL + "'>@{R=DPI.Strings;K=Web_ManageProbes_SubGrid_DefineAppsNow;E=js}</a>";
                    }

                    td.html(html);

                    tr.append(td);

                    if (controlMode == 0) {
                        nodeNames[data[i].NodeID] = data[i].NodeName;

                        tr.append($("<td class='cmdColumn' ><a href='/Orion/DPI/Admin/DPINode/ChooseDpiNode.aspx?EditID={0}' ><img src='{1}' alt='.' /></a></td>"
                            .replace("{0}", data[i].NodeID)
                            .replace("{1}", "/Orion/images/edit_16x16.gif")));
                        if (!isServerLic) {
                            tr.append($("<td class='cmdColumn' ><a href='javascript:SW.DPI.CollectorNodes.DeleteNode({nodeId},{probeId});' ><img src='/Orion/images/nodemgmt_art/icons/icon_delete.gif' alt='.' /></td>"
                                .replace("{nodeId}", data[i].NodeID)
                                .replace("{probeId}", expandedProbeId)));
                        }
                    }
                    tpart.append(tr);
                }

                table.append(tpart);

                // fixing IE bug that refresh is called twice - remove all old tables created before
                $("#" + htmlContainer + " .qoe-add-wrap").remove();
                $("#" + htmlContainer + " .subGrid").remove();
                $("#" + htmlContainer + " .subGridHeightDivider").remove();

                $("#" + htmlContainer).append(table);

                if (!isServerLic) {
                    $("#" + htmlContainer).append(addQoeBtn);
                }

                $("#" + htmlContainer).append('<div style="height: 20px" class="subGridHeightDivider">&nbsp;</div>');

                leftGrid.setLoading(false);
            });
    }

    initLeftGrid = function (htmlElement) {
        leftDataStore = new Ext4.create('Ext.data.Store', {
            pageSize: itemsPerPage,
            fields: modelFields,
            proxy: {
                type: 'ajax',
                url: serviceUrl + "/GetProbes",
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                actionMethods: { read: 'GET' },
                reader: { type: 'json', root: 'd.Items', totalProperty: 'd.ResultCount', successProperty: 'd.Success', idProperty: 'ProbeID' },
                writer: { type: 'json' },
                extraParams: {
                    skipIds: "[]",
                    allTrafficFirst: (controlMode != 0)
                }
            },
            remoteSort: true,
            autoLoad: true,
            sorters: [{ property: 'Name', direction: 'asc' }],
            filters: [{ property: 'Name', value: '' }],
            listeners: {
                load: function (store, records, successful, operation, eOpts) {
                    if (controlMode == 1) {
                        var val = getSingleSelectValue();
                        if (val != null && val != "") {
                            var rec = store.findRecord("ProbeID", getSingleSelectValue());
                            leftGrid.getSelectionModel().select(rec);
                        }
                    }
                }
            }
        });

        function subRowRenderer(data, rowIndex, record, orig) {
            var headerCt = this.view.headerCt;
            return {
                rowBody: '<div style="display: none; margin: 0 20px 0 25px;" id="subrow_' + record.get("ProbeID") + '" ></div>',
                rowBodyColspan: headerCt.getColumnCount()
            };
        }

        Ext4.define('Ext.ux.form.SearchField', {
            extend: 'Ext.form.field.Trigger',
            alias: 'widget.searchfield',
            cls: 'dpi_grid_search_field',
            trigger1Cls: Ext4.baseCSSPrefix + 'form-clear-trigger',
            trigger2Cls: Ext4.baseCSSPrefix + 'form-search-trigger',
            emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',
            validationEvent: false,
            validateOnBlur: false,
            width: 180,
            onTrigger1Click: function () {
                this.setValue('');
            },
            onTrigger2Click: function () {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(function () {
                    canSearch = true;
                }, doneTypingInterval);

                if (canSearch || (navigator.appVersion.indexOf("MSIE") == -1)) {
                    //set the global variable for the SQL query and regex highlighting
                    filterText = this.getRawValue();

                    // convert search string to SQL format (i.e. replace * with %)
                    var tmpfilterText = "";
                    tmpfilterText = filterText.replace(/\*/g, "%");

                    if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                        tmpfilterText = "";
                    }

                    leftDataStore.remoteFilter = false;
                    leftDataStore.clearFilter();
                    leftDataStore.remoteFilter = true;
                    leftDataStore.filters.add({ property: 'Name', value: tmpfilterText });

                    var o = { start: 0, limit: parseInt(itemsPerPage) };
                    leftDataStore.load({ params: o });

                    canSearch = false;
                }
            },
            initComponent: function () {
                this.callParent(arguments);

                this.on('specialkey', function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onTrigger2Click();
                    }
                }, this);

                this.on('focus', function (f, e) {
                    if (this.getRawValue() == '') {
                        this.setValue('');
                    }
                }, this);

                this.on('change', function (f, e) {
                    this.onTrigger2Click();
                }, this);
            },
            afterRender: function () {
                this.callParent();

                // need to hide the clear trigger button for IE since they add their own to all text inputs
                if (navigator.appVersion.indexOf("MSIE") != -1) {
                    this.triggerEl.elements[0].remove();
                    this.onTrigger2Click();
                }
            }
        });

        var topToolbar = null;
        var title = null;
        if (controlMode == 0) {
            var items = [];

            items.push(btnAddCollectorNode = Ext4.create('Ext.button.Button', { id: 'btnAddCollectorNode', text: '@{R=DPI.Strings;K=Web_ManageProbes_AddPacketAnalysisEngine;E=js}', icon: '/Orion/images/add_16x16.gif', handler: addDpiProbe }));
            items.push(btnEdit = Ext4.create('Ext.button.Button', { id: 'btnEdit', text: '@{R=DPI.Strings;K=Web_ManageProbes_Edit;E=js}', icon: '/Orion/images/edit_16x16.gif', handler: editSelectedProbe }));
            items.push(btnEnable = Ext4.create('Ext.button.Button', { id: 'btnEnable', text: '@{R=DPI.Strings;K=Web_ManageProbes_EnableProbe;E=js}', icon: '/Orion/images/enable_monitoring.gif', handler: function () { enableSelectedProbes(true); } }));
            items.push(btnDisable = Ext4.create('Ext.button.Button', { id: 'btnDisable', text: '@{R=DPI.Strings;K=Web_ManageProbes_DisableProbe;E=js}', icon: '/Orion/images/disable_monitoring.gif', handler: function () { enableSelectedProbes(false); } }));
            items.push(btnDelete = Ext4.create('Ext.button.Button', { id: 'btnDelete', text: '@{R=DPI.Strings;K=Web_ManageProbes_Delete;E=js}', icon: '/Orion/images/nodemgmt_art/icons/icon_delete.gif', handler: deleteSelectedProbes }));

            topToolbar = Ext4.create('Ext.toolbar.Toolbar', {
                id: 'topToolbar',
                border: 0,
                items: [items, '->', ' ', { xtype: 'searchfield' }]
            });
        }
        else {
            title = '@{R=DPI.Strings;K=Web_ManageProbes_AvailableCollectors;E=js}';
        }

        var pagingToolbar = Ext4.create('Ext.toolbar.Paging', {
            store: leftDataStore,
            displayInfo: true,
            displayMsg: '@{R=DPI.Strings;K=Web_ManageProbes_Paging_Stat;E=js}',
            beforePageText: '@{R=DPI.Strings;K=Web_PagingBeforePageText;E=js}',
            afterPageText: '@{R=DPI.Strings;K=Web_PagingAfterPageText;E=js}',
            emptyMsg: '@{R=DPI.Strings;K=Web_ManageProbes_NoSensorsToDisplayText;E=js}'
        });

        var width = $("#" + htmlElement).width();
        // create the Grid
        leftGrid = new Ext4.create('Ext.grid.Panel', {
            cls: 'sw-dpi-grid',
            store: leftDataStore,
            renderTo: htmlElement,
            height: 500,
            width: width,
            stripeRows: true,
            autoScroll: true,
            title: title,
            disabled: !enabled,
            selModel: Ext4.create('Ext.selection.CheckboxModel', {
                mode: (controlMode == 1) ? 'SINGLE' : 'MULTI',
                listeners: { selectionchange: onLeftSelectionChange }
            }),
            viewConfig: {
                style: { overflow: 'scroll', overflowX: 'hidden' },
                forceFit: true,
                getRowClass: function (record, index) {
                    if (record.data.Enabled === false) {
                        return 'disabled-row';
                    }
                },
                listeners: {
                    refresh: function (view) {
                        for (var i = 0; i < expandedProbeIds.length; i++) {
                            var expanderHtmlId = "#expander_" + expandedProbeIds[i];
                            var expander = $(expanderHtmlId);
                            expander.click();
                        }
                    }
                }
            },
            listeners: {
                beforeselect: function (sm, record) {
                    if ((controlMode != 0) && (record.data.Mode !== 2)) return false;
                },
                selectionchange: function (sm, selected) {
                    if ((controlMode != 0) && (selected.length > 2)) {
                        Ext4.Array.each(selected, function (record) {
                            if (record.data.Mode !== 2) sm.deselect(record, true);
                        });
                    }
                }
            },
            features: [
                {
                    ftype: 'rowbody',
                    getAdditionalData: subRowRenderer
                }
            ],
            tbar: topToolbar,
            bbar: pagingToolbar,
            columns: [
                {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_Name;E=js}',
                    flex: 2,
                    dataIndex: 'Name',
                    renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                        var wrapper = $("<div/>");
                        var probeID = record.get("ProbeID") + "";
                        var probeMode = record.get("Mode") + "";
                        var nodeCount = record.get("DpiNodeCount");
                        var expanderImg = "data:image/gif;base64,R0lGODlhDgAOAIABAGRkZPLy8iH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCNDAwQ0Q5MTI5QzUxMUUzQTE3MkY5NjY4NjkyMjg4NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCNDAwQ0Q5MjI5QzUxMUUzQTE3MkY5NjY4NjkyMjg4NiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkI0MDBDRDhGMjlDNTExRTNBMTcyRjk2Njg2OTIyODg2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkI0MDBDRDkwMjlDNTExRTNBMTcyRjk2Njg2OTIyODg2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAQAsAAAAAA4ADgAAAiOMjwnLvQaSguidGDDUmV6ORQ7TdeMTVqC3VWy2lpJ3OvNdAAA7";

                        if ((probeMode === "2") && (nodeCount == 0) && ($.inArray(probeID, expandedProbeIds) < 0)) {
                            expandedProbeIds.push(probeID);
                        }

                        if ($.inArray(probeID, expandedProbeIds) >= 0)
                            expanderImg = "data:image/gif;base64,R0lGODlhDgAOAIABAGRkZPLy8iH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDNjZDNjQ5NjI5QzUxMUUzQTJCOUIwQUFCNkVCM0Q5QiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDNjZDNjQ5NzI5QzUxMUUzQTJCOUIwQUFCNkVCM0Q5QiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkM2NkM2NDk0MjlDNTExRTNBMkI5QjBBQUI2RUIzRDlCIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkM2NkM2NDk1MjlDNTExRTNBMkI5QjBBQUI2RUIzRDlCIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAQAsAAAAAA4ADgAAAh+MjwnLvQaSghLFcCumem/HeBH4XFlldqKadibZqEkBADs=";
                        var div = $("<img class='expander' src='" + expanderImg + "' onclick='SW.DPI.CollectorNodes.gridExpandClick(event, \"" + probeID + "\", \"" + probeMode + "\")' id='expander_" + probeID + "'>");

                        div.appendTo(wrapper);

                        var term = $('.dpi_grid_search_field').find('[type="text"]').val();
                        var n = escapeHtml((term) ? highlight(value, term) : value);

                        return wrapper.html() + n;
                    }
                }, {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_Enabled;E=js}',
                    dataIndex: 'Enabled',
                    width: 60,
                    renderer: SW.DPI.ManagePages.Renderers.YesNo
                }, {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_License;E=js}',
                    dataIndex: 'Mode',
                    width: 100,
                    renderer: SW.DPI.ManagePages.Renderers.ProbeLicense
                },
                {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_NumberOfNodes;E=js}',
                    dataIndex: 'DpiNodeCount',
                    width: 230,
                    renderer: SW.DPI.ManagePages.Renderers.NodeCount
                },
                {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_ProbeLoad;E=js}',
                    dataIndex: 'Utilization',
                    width: 120,
                    renderer: SW.DPI.ManagePages.Renderers.ProbeUtilization
                }, {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_AgentStatus;E=js}',
                    dataIndex: 'AgentStatus',
                    flex: 1,
                    renderer: SW.DPI.ManagePages.Renderers.AgentStatus
                }, {
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_PluginStatus;E=js}',
                    dataIndex: 'PluginStatusNumeric',
                    flex: 1,
                    renderer: function sensorStatus(statusValue, metadata, record) {
                        // add tooltip for sensor status
                        var tooltip = record.get('PluginStatusDescription');
                        if (!tooltip)
                            tooltip = statusTips[statusValue];
                        metadata.tdAttr = 'title="' + tooltip + '"';

                        // show value for plugin (sensor) status, show Unknown if status is out of range
                        var externalizedColumnValue = statusTips[statusValue];
                        if (!externalizedColumnValue)
                            externalizedColumnValue = '@{R=DPI.Strings;K=Web_ManageProbes_StatusTip_Other;E=js}' // Unknown sensor status.
                        return externalizedColumnValue;
                    }
                }
            ]
        });
        fixAlignIssues(leftGrid);
    };

    saveSingleSelect = function () {
        var leftSelectedItems = leftGrid.getSelectionModel().getSelection();
        if (leftSelectedItems.length == 0)
            return;

        var anyOverloaded = false;
        $("#hfSelectedIds").val(leftSelectedItems[0].data.ProbeID);
        if (leftSelectedItems[0].data.Utilization > 80)
            anyOverloaded = true;
    }

    getSingleSelectValue = function () {
        return $("#hfSelectedIds").val();
    }

    n.gridExpandClick = function (e, recordID, recordLicenseType) {
        if (e) {
            var subRowId = "subrow_" + recordID;
            var subRow = $("#" + subRowId);
            var expander = $(e.target || e.srcElement);

            if (!subRow.is(":visible")) {
                expander.attr("src", "data:image/gif;base64,R0lGODlhDgAOAIABAGRkZPLy8iH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDNjZDNjQ5NjI5QzUxMUUzQTJCOUIwQUFCNkVCM0Q5QiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDNjZDNjQ5NzI5QzUxMUUzQTJCOUIwQUFCNkVCM0Q5QiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkM2NkM2NDk0MjlDNTExRTNBMkI5QjBBQUI2RUIzRDlCIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkM2NkM2NDk1MjlDNTExRTNBMkI5QjBBQUI2RUIzRDlCIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAQAsAAAAAA4ADgAAAh+MjwnLvQaSghLFcCumem/HeBH4XFlldqKadibZqEkBADs=");
                subRow.empty();
                if ($.inArray(recordID, expandedProbeIds) < 0)
                    expandedProbeIds.push(recordID);

                initSubGrid(subRowId, recordID, recordLicenseType);
            } else {
                expander.attr("src", "data:image/gif;base64,R0lGODlhDgAOAIABAGRkZPLy8iH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCNDAwQ0Q5MTI5QzUxMUUzQTE3MkY5NjY4NjkyMjg4NiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCNDAwQ0Q5MjI5QzUxMUUzQTE3MkY5NjY4NjkyMjg4NiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkI0MDBDRDhGMjlDNTExRTNBMTcyRjk2Njg2OTIyODg2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkI0MDBDRDkwMjlDNTExRTNBMTcyRjk2Njg2OTIyODg2Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAQAsAAAAAA4ADgAAAiOMjwnLvQaSguidGDDUmV6ORQ7TdeMTVqC3VWy2lpJ3OvNdAAA7");
                expandedProbeIds.splice($.inArray(recordID, expandedProbeIds), 1);
            }

            subRow.toggle();
        }
    };

    n.DeleteNode = function (nodeId, probeId) {
        var nodeName = nodeNames[nodeId];
        var probeName = "";
        for (var i = 0; i < leftDataStore.data.items.length; i++) {
            var probeModel = leftDataStore.data.items[i].data;
            if (probeModel.ProbeID === probeId) {
                probeName = probeModel.Name;
                break;
            }
        }
        var confirmDlg = new Ext4.window.MessageBox({
            buttons: [
                {
                    type: 'buttons',
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_DeleteNodeFromProbeDialog_OK;E=js}',
                    handler: function (btn, o) {
                        ORION.callWebService(
                            serviceUrl,
                            "RemoveNodeFromProbe", { nodeId: nodeId, probeId: probeId },
                            function () {
                                confirmDlg.close();
                                reloadLeftGrid();
                            });
                    }
                },
                {
                    type: 'buttons',
                    text: '@{R=DPI.Strings;K=Web_ManageProbes_DeleteNodeFromProbeDialog_Cancel;E=js}',
                    handler: function (btn, o) {
                        confirmDlg.close();
                    }
                }
            ]
        });

        confirmDlg.show({
            title: "@{R=DPI.Strings;K=Web_ManageProbes_DeleteNodeFromProbeDialog_Title;E=js}",
            msg: "@{R=DPI.Strings;K=Web_ManageProbes_DeleteNodeFromProbeDialog_Msg;E=js}"
                .replace("{0}", escapeHtml(nodeName))
                .replace("{1}", escapeHtml(probeName)),
            icon: Ext4.MessageBox.QUESTION,
        });
    };

    n.NodeMouseOver = function (e, nodeId) {
        //Firefox browsers bug - http://stackoverflow.com/questions/5301643
        var target = e.target || e.srcElement;
        var elem = $(target);

        //do not show tooltip when web service is slow and mouse is already out
        var callOnMouseOver = true;

        elem.mouseleave(function () {
            callOnMouseOver = false;
            elem.mouseleave(null);
        });

        ORION.callWebService(
            "/Orion/DPI/Services/CollectorNodes.asmx",
            "GetAppsByNodeId",
            { nodeId: nodeId },
            function (d) {
                var tooltipContent = "";

                for (var i = 0; i < d.length; i++) {
                    tooltipContent +=
                        "<div><img src='/Orion/StatusIcon.ashx?size=small&entity=Orion.DPI.Applications&status={0}'>{1}</div>"
                            .replace("{0}", d[i].Status)
                            .replace("{1}", d[i].Name);
                }

                elem.each(function () {
                    $.swtooltip(this, $("<div><tooltip><h3 class=StatusUndefined>@{R=DPI.Strings;K=Web_ManageProbes_AppTooltipHeader;E=js}</h3><div class=NetObjectTipBody>" + tooltipContent + "</div></tooltip></div>"));
                    if (callOnMouseOver)
                        this.onmouseover();
                });
            });
    };

    n.Init = function (mode) {
        controlMode = mode;

        initLeftGrid('chooseCollectorNodesExtGrid');
        onLeftSelectionChange(true);

        $(window).scroll(function () {
            clearTimeout($.data(this, 'scrollTimer'));
            $.data(this, 'scrollTimer', setTimeout(function () {
                position = $('body').scrollTop();
            }, 200));
        });

        $("#addDialog1Cancel").on('click', function () {
            SW.DPI.CollectorAddControl.CloseDialog1();
        });

        $("#addDialog1Save").on('click', function () {
            SW.DPI.CollectorAddControl.Proceed();
        });

        $("#addDialog2Cancel").on('click', function () {
            SW.DPI.CollectorAddControl.CloseDialog2();
        });

        $("#addDialog2Save").on('click', function () {
            SW.DPI.CollectorAddControl.SaveDialog2();
            SW.DPI.CollectorAddControl.CloseDialog2();
        });

        $("#editDialogCancel").on('click', function () {
            SW.DPI.CollectorEditControl.Close();
        });

        $("#editDialogSave").on('click', function () {
            SW.DPI.CollectorEditControl.Save();
        });
    };

    n.SetDisabled = function (disabled) {
        enabled = !disabled;
        if (leftGrid)
            leftGrid.setDisabled(disabled);
    };

    function pregQuote(str) {
        return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
    }

    function highlight(data, search) {
        return data.replace(new RegExp("(" + pregQuote(search) + ")", 'gi'), "<span style='background-color:#ffe89e;'>$1</span>");
    }

    function showEditDialog(selectedProbeID) {
        SW.DPI.CollectorEditControl.OpenDialog("editCollectorDialog", selectedProbeID);
    }
})(SW.DPI.CollectorNodes);
