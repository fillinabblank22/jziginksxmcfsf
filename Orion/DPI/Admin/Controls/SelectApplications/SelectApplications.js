Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.namespace('SW.Orion.DPI');

SW.Orion.DPI.SelectApplications = function () {
    ORION.prefix = "Orion_DPI_SelectApplications_";

    var selectorModel;
    var dataStore;
    var grid;
    var gridPanel;
    var initialized;
    var tree;
    var treePanel;
    var mask;
    var rightPanelTitle = '';
    var leftPanelTitle = '';
    var entityName = '';
    var gridItemsFieldClientID = '';
    var nodesToRemove; // remove these nodes at the end of the last batch

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        if (query != null) {
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }
        return "";
    };

    function renderName(value, meta, record) {
        // entity - Orion.Nodes, Orion.NPM.Interfaces
        // id - entityId (for computed status)
        // status - entity status
        // size - size of icon
        return String.format('<span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small" /></span> {2} ', GetItemType(), record.data.MemberStatus, value);
    }

    GetItemType = function () {
        return entityName;
    };

    GetGroupBy = function () {
        $("#groupBySelect").val();
    };

    ShowAddMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_32; E=js}"
        });
        mask.show();
    }

    ShowRemoveMask = function () {
        mask = new Ext.LoadMask(grid.el, {
            msg: "@{R=Core.Strings;K=WEBJS_TM0_33; E=js}"
        });
        mask.show();
    }

    HideMask = function () {
        grid.store.sort('Name', 'ASC');
        mask.hide();
    };

    DoSearch = function () {
        $('#groupBySelect').val("");
        ReloadTree();
    };

    ReloadTree = function () {
        tree.loader.load(tree.root);
    };

    RemoveSelectedItems = function () {
        var items = selectorModel.getSelections();
        ShowRemoveMask();
        grid.store.remove(items);
        ReloadTree();
        HideMask();
    };

    RemoveCheckedNodesFromGrid = function () {
        if (selectorModel.getCount() > 0) {
            Ext.Msg.minWidth = 310;
            Ext.Msg.confirm(
                String.format("@{R=Core.Strings;K=WEBJS_VB1_1; E=js}", rightPanelTitle),
                "@{R=Core.Strings;K=WEBJS_VB1_2; E=js}",
                function (btn, text) {
                    if (btn == "yes") {
                        RemoveSelectedItems();
                    }
                }
            );
        }
    };

    //Remove given Id from hidden field.
    RemoveFromSelection = function (item) {
        var data = GetSelection();
        var replacedItem = item.data.Id + ",";
        data = data.replace(replacedItem, "");
        SaveSelection(data);
    };

    //Gets value of hidden field.
    GetSelection = function () {
        var data = $('#' + gridItemsFieldClientID).val();
        //'data' must end with ',' - it is needed for dupicity check
        if (!(/,$/.test(data))) {
            data += ",";
        }
        return data;
    };

    SaveSelection = function (data) {
        //Save Ids of items from grid to hidden field. Content of this field will be posted to the server in form data.
        $('#' + gridItemsFieldClientID).val(data);
    };

    AddCheckedNodesToGrid = function () {
        var items = tree.getChecked();
        var duplicity = false;
        var silentMode = true;

        for (var i = 0; i < items.length; i++) {
            if (AddNodeToGrid(items[i], silentMode)) {
                items[i].remove(true);
            } else {
                duplicity = true;
            }
        }

        //In 'Silent mode' if there is dulicity, dialog was not shown to user before.
        if (duplicity && silentMode) {
            Ext.Msg.alert("@{R=Core.Strings;K=WEBJS_TM0_40; E=js}", "@{R=Core.Strings;K=WEBJS_TM0_39; E=js}");
        }
    };

    AddNodeToGrid = function (node, silentMode) {
        if (grid.store.findExact("Id", node.id) != -1) {
            if (!silentMode) {
                Ext.Msg.alert("@{R=Core.Strings;K=WEBJS_TM0_40; E=js}", "@{R=Core.Strings;K=WEBJS_TM0_39; E=js}");
            }
            return false;
        }

        var blankRecord = Ext.data.Record.create(grid.store.fields);
        var record = new blankRecord({
            Id: node.attributes.id,
            Name: node.attributes.text,
            MemberStatus: node.attributes.type
        });

        grid.store.addSorted(record);
        return true;
    };

    SetNodeIcon = function (node) {
        if (!node.attributes || !node.attributes.type) return;

        switch (node.attributes.type) {
            case "RiskLevel":
                node.setIcon(String.format("/Orion/DPI/images/app_risk_level_{0}.png", node.id));
                break;
            case "ProductivityRating":
                node.setIcon(String.format("/Orion/DPI/images/app_productivity_{0}.png", node.id));
                break;
            case "0":
            case "Orion.DPI.ApplicationProtocols":  //For now there is no icon for 'ApplicationProtocols'. Icon for 'Applications' in 'disabled' status will be used.
                node.setIcon(String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small', GetItemType(), 0));
                break;
            case "1":
            case "Orion.DPI.Applications":
                node.setIcon(String.format('/Orion/StatusIcon.ashx?entity={0}&status={1}&size=small', GetItemType(), 1));
            default:
                break;
        }
    };

    GetTreeLoader = function () {
        return new Ext.tree.TreeLoader({
            dataUrl: '/Orion/DPI/Services/SelectApplicationsDataLoader.ashx',
            requestMethod: 'POST',
            listeners: {
                beforeload: {
                    fn: function (treeLoader, node) {
                        this.baseParams.operation = 'LoadGroupingData',
                            this.baseParams.parentType = $("#groupBySelect option:selected").val();
                        this.baseParams.parentId = node.id;
                        this.baseParams.searchString = $("#searchBox").val();
                        this.baseParams.excludedItems = $('#' + gridItemsFieldClientID).val();
                    }
                }
            }
        });
    };

    InitTree = function () {
        selectPanel = new Ext.Container({
            applyTo: 'GroupItemsSelector',
            region: 'north',
            height: 100,
            layout: 'fit'
        });

        tree = new Ext.tree.TreePanel({
            id: 'TreeExtPanel',
            useArrows: true,
            autoScroll: true,
            animate: true,
            enableDrag: true,
            height: 337, // border layout somehow doesn't work so we have to set height manually
            region: 'center',
            nodeType: 'async',
            loader: GetTreeLoader(),
            rootVisible: false,
            ddGroup: "treeDDGroup",
            listeners: {
                beforeappend: function (tree, parent, node) {
                    SetNodeIcon(node);
                }
            }
        });

        var items = [selectPanel];
        items.push(tree);

        treePanel = new Ext.Panel({
            title: (leftPanelTitle) ? Ext.util.Format.htmlEncode(leftPanelTitle) : '@{R=Core.Strings;K=WEBJS_TM0_31; E=js}',
            frame: true,
            region: 'west',
            width: 272,
            split: true,
            items: items
        });

        var root = new Ext.tree.AsyncTreeNode({
            text: 'root',
            id: 'root',
            draggable: false,
        });

        tree.setRootNode(root);
    };

    //Save set of given Ids to hidden field.
    AddToSelection = function (arr) {
        var data = GetSelection();

        for (var i = 0; i < arr.length; i++) {
            var newItem = arr[i].data.Id + ",";
            if (data.indexOf(newItem) < 0) {
                data += newItem;
            }
        }

        SaveSelection(data);
    };

    FinishDrop = function (node) {
        var items = null;
        if (!node.leaf) {
            items = node.childNodes;
        } else {
            items = tree.getChecked();
            if (!node.attributes.checked) {
                items.push(node);
            }
        }

        for (var i = items.length - 1; i >= 0; i--) {
            if (AddNodeToGrid(items[i], false)) {
                items[i].remove(true);
            }
        }
    };

    InitDragDrop = function () {
        var gridDropTargetEl = grid.getView().el.dom.childNodes[0].childNodes[1];
        var gridDropTarget = new Ext.dd.DropTarget(gridDropTargetEl, {
            ddGroup: 'treeDDGroup',
            copy: false,
            notifyDrop: function (ddSource, e, data) {
                //check if children were loaded before
                if (!data.node.leaf && !data.node.expanded) {
                    tree.loader.load(data.node, FinishDrop);    //load children
                } else {
                    FinishDrop(data.node);
                }

                return true;
            }
        });
    };

    InitGrid = function () {
        var proxy = new Ext.data.HttpProxy({
            method: 'POST',
            url: '/Orion/DPI/Services/SelectApplicationsDataLoader.ashx',
            listeners: {
                exception: function (proxy, type, action, o, response, arg) {
                    alert("Connection error: " + response.responseText);
                }
            }
        });

        var record = Ext.data.Record.create([
            { name: 'Id' },
            { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
            { name: 'MemberStatus' }
        ]);

        var jsonReader = new Ext.data.JsonReader({
            fields: [
                { name: 'Id', mapping: 'id' },
                { name: 'Name', mapping: 'text' },
                { name: 'MemberStatus', mapping: 'type' }
            ]
        });

        dataStore = new Ext.data.Store({
            reader: jsonReader,
            sortInfo: { field: 'Name', direction: 'ASC' },
            proxy: proxy,
            baseParams: {
                'operation': 'LoadSelectedApplications',
                'entityIds': GetSelection()
            },
            listeners: {
                add: function (store, records, index) {
                    AddToSelection(records);
                },
                remove: function (store, records, index) {
                    RemoveFromSelection(records);
                },
            }
        });

        dataStore.load();

        selectorModel = new Ext.grid.CheckboxSelectionModel();

        var tbar = [{
            text: '@{R=Core.Strings;K=WEBJS_VB0_36; E=js}',
            iconCls: 'selectAllButton',
            handler: function () {
                selectorModel.selectAll();
            }
        }, '-', {
            text: '@{R=Core.Strings;K=WEBJS_VB0_38; E=js}',
            iconCls: 'selectNoneButton',
            handler: function () {
                selectorModel.clearSelections();
            }
        }];

        grid = new Ext.grid.GridPanel({
            store: dataStore,
            columns: [
                selectorModel,
                { id: 'Name', header: '@{R=Core.Strings;K=WEBJS_TM0_42; E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'Name', renderer: renderName }
            ],
            autoExpandColumn: 'Name',
            sm: selectorModel,
            viewConfig: {
                forceFit: false
            },
            border: true,
            frame: true,
            hideHeaders: true,
            stripeRows: true,
            ddGroup: 'treeDDGroup',
            region: 'center',
            title: (rightPanelTitle) ? Ext.util.Format.htmlEncode(rightPanelTitle) : '@{R=Core.Strings;K=WEBJS_VB1_3; E=js}',
            loadMask: { msg: '@{R=Core.Strings;K=WEBJS_TM0_35; E=js}' },
            tbar: tbar
        });

        var addButtonPanel = new Ext.Panel({
            border: false,
            region: 'west',
            width: 54,
            contentEl: 'AddButtonPanel'
        });

        gridPanel = new Ext.Panel({
            id: 'AddButtonPanelExtPanel',
            region: 'center',
            layout: 'border',
            border: false,
            items: [addButtonPanel, grid]
        });
    };

    InitLayout = function () {
        var panel = new Ext.Container({
            id: 'MainExtPanel',
            renderTo: 'ContainerMembersTable',
            height: 500,
            layout: 'border',
            items: [treePanel, gridPanel]
        });
        $(window).bind('resize', function () {
            panel.doLayout();
        });
    };

    return {
        SetTitles: function (leftPanel, rightPanel) {
            leftPanelTitle = leftPanel;
            rightPanelTitle = rightPanel;
        },
        SetEntity: function (name) {
            entityName = name;
        },
        SetGridItemsFieldClientID: function (id) {
            gridItemsFieldClientID = id;
        },
        init: function () {
            if (initialized) return;
            initialized = true;

            InitTree();
            InitGrid();
            InitLayout();
            InitDragDrop();

            $('#AddButtonPanel').height($('#ContainerMembersTable').height());
            $('#AddToGroupButton').click(AddCheckedNodesToGrid);
            $('#RemoveFromGroupButton').click(RemoveCheckedNodesFromGrid);
            $('#searchButton').click(DoSearch);
            $('#searchBox').keydown(function (event) {
                if (event.keyCode == '13') {
                    event.preventDefault();
                    DoSearch();
                }
            });

            $('#groupBySelect').on('change', function () {
                $('#searchBox').val("");
                ReloadTree();
            });
        }
    };
}();

Ext.onReady(SW.Orion.DPI.SelectApplications.init, SW.Orion.DPI.SelectApplications);