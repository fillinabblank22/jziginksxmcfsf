﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectApplicationsControl.ascx.cs" Inherits="Orion_DPI_Admin_Controls_SelectApplicationsControl" %>
<orion:include runat="server" file="DPI/js/EscapeHtml.js" />
<orion:include id="Include1" runat="server" file="Admin/Containers/Containers.css" />
<orion:include id="Include2" runat="server" framework="Ext" frameworkversion="3.4" />
<orion:include id="Include3" runat="server" file="OrionCore.js" />
<orion:include id="Include4" runat="server" file="DPI/Admin/js/ManagePages.Renderers.js" />
<orion:include id="Include5" runat="server" file="DPI/Admin/Controls/SelectApplications/SelectApplications.js" />

<div id="GroupItemsSelector">
    <div class="GroupSection">
        <label for="groupBySelect"><%= Resources.CoreWebContent.WEBDATA_VB0_5 %></label>
        <select id="groupBySelect">
            <option value=""><%= Resources.DPIWebContent.DPIApplicationWizard_SelectApplication_LabelNoGrouping %></option>
            <option value="CategoryID" selected="selected"><%= Resources.DPIWebContent.DPIApplicationWizard_SelectApplication_LabelCategory %></option>
            <option value="RiskLevel"><%= Resources.DPIWebContent.DPIApplicationWizard_SelectApplication_LabelRisk %></option>
            <option value="ProductivityRating"><%= Resources.DPIWebContent.DPIApplicationWizard_SelectApplication_LabelProductivity %></option>
        </select>
    </div>
    <div class="GroupSection">
        <label for="searchBox"><%= Resources.CoreWebContent.WEBDATA_TM0_225 %></label>
        <table>
            <tr>
                <td>
                    <input type="text" class="searchBox" id="searchBox" name="searchBox" /></td>
                <td><a href="javascript:void(0);" id="searchButton">
                    <img src="/Orion/images/Button.SearchIcon.gif" /></a></td>
            </tr>
        </table>
    </div>
</div>

<div id="ContainerMembersTable" style="width: 99%;">
</div>

<div id="AddButtonPanel" style="padding: 10px;">
    <a id="AddToGroupButton" href="javascript:void(0);" style="top: 44%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_add_32x32.gif" />
    </a>
    <a id="RemoveFromGroupButton" href="javascript:void(0);" style="top: 46%; position: relative;">
        <img src="/Orion/images/AddRemoveObjects/arrows_remove_32x32.gif" />
    </a>
</div>

<input type="hidden" id="groupByType" />
<asp:HiddenField ID="gridItems" runat="server" ClientIDMode="Static" />

<script type="text/javascript">
    // <![CDATA[
    SW.Orion.DPI.SelectApplications.SetTitles('<%= LeftPanelTitle %>', '<%= RightPanelTitle %>');
    SW.Orion.DPI.SelectApplications.SetEntity('<%= EntityName %>');
    SW.Orion.DPI.SelectApplications.SetGridItemsFieldClientID('<%= gridItems.ClientID %>');
    // ]]>
</script>
