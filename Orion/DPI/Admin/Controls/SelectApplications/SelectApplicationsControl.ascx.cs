﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Admin_Controls_SelectApplicationsControl : System.Web.UI.UserControl
{
    public String LeftPanelTitle { get; set; }
    public String RightPanelTitle { get; set; }

    // Orion.Nodes, Orion.NPM.Interfaces,...
    public String EntityName
    {
        get
        {
            return ViewState["EntityName"].ToString();
        }

        set
        {
            ViewState["EntityName"] = value;
        }
    }

    public String SelectedItems
    {
        get
        {
            return gridItems.Value;
        }

        set
        {
            gridItems.Value = value;
        }
    }
}