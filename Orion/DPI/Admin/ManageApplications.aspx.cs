﻿using SolarWinds.Orion.Web.Controls;
using System;
using System.Web.UI;

public partial class Orion_DPI_Admin_ManageApplications : Page
{
    public bool? hidePrebuilt = false;

    protected override void OnInit(EventArgs e)
    {
        clearSessionValues();
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            checkHidePrebuiltToggle();
        }
    }

    protected void HidePrebuilt_CheckedChanged(object sender, EventArgs e)
    {
        hidePrebuilt = (HidePrebuilt.Checked);
        Session["DPI_Wizard_Current_Application_HidePrebuilt"] = hidePrebuilt;
    }

    private void clearSessionValues()
    {
        Session["DPI_Wizard_Current_Application"] = null;
        Session["DPI_Wizard_Current_Application_Mode"] = null;
        Session["DPI_Wizard_Current_Application_Protocol"] = null;
    }

    private void checkHidePrebuiltToggle()
    {
        if (Session["DPI_Wizard_Current_Application_HidePrebuilt"] != null)
        {
            hidePrebuilt = (bool?)Session["DPI_Wizard_Current_Application_HidePrebuilt"];
        }
        else
        {
            Session["DPI_Wizard_Current_Application_HidePrebuilt"] = false;
        }

        HidePrebuilt.Checked = (bool)hidePrebuilt;
    }
}