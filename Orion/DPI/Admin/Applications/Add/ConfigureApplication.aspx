﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ConfigureApplication.aspx.cs" Inherits="Orion_DPI_Admin_Applications_Add_ConfigureApplication"
    Title="<%$ Resources: DPIWebContent, DPIApplicationWizard_Title %>" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" File="DPI/styles/wizard/ManageApplications.css" />
    <orion:Include ID="Include2" runat="server" File="DPI/styles/ToggleSwitch.css" />

    <!--[if IE]>
	<style>
        p.sw-checkbox-switch > input {
            width: auto;
            position: static;
            opacity: 1.0;
        }

        p.sw-checkbox-switch > label {
            display: none;
        }
    </style>
    <![endif]-->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoEApplication" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

    <h1><%= Resources.DPIWebContent.DPIApplicationWizard_Title %></h1>

    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />

    <div id="dpi-wizard-add-application">
        <h2><%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_Title %></h2>

        <br />

        <div id="WizardViewUserDefined" runat="server" class="dpi-user-defined-box">
            <table id="dpi-wizard-table">
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelName" runat="server" AssociatedControlID="FieldName">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelName %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="FieldName" runat="server" Width="260" MaxLength="255"></asp:TextBox></td>
                                <td><span class="dpi-wizard-table-error">
                                    <asp:RequiredFieldValidator ID="ValidateName" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateName %>" runat="server" ControlToValidate="FieldName" SetFocusOnError="true" /></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelDescription" runat="server" AssociatedControlID="FieldDescription">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelDesc %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <textarea id="FieldDescription" runat="server" cols="50" rows="5" maxlength="1000"></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelEnabled" runat="server" AssociatedControlID="FieldEnabled">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelEnabled %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <p class="sw-checkbox-switch">
                            <asp:CheckBox ID="FieldEnabled" runat="server"></asp:CheckBox>
                            <asp:Label ID="CheckboxLabelElement" runat="server" AssociatedControlID="FieldEnabled"></asp:Label>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="FieldEnabled">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelDiscovery %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <p class="sw-checkbox-switch">
                            <asp:CheckBox ID="FieldDiscovery" runat="server"></asp:CheckBox>
                            <asp:Label ID="CheckboxLabelDiscoveryElement" runat="server" AssociatedControlID="FieldDiscovery"></asp:Label>
                        </p>
                    </td>
                </tr>
                <!--
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelProtocol" runat="server" AssociatedControlID="FieldProtocol"><%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelProtocol %>:</asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <asp:Label ID="FieldProtocol" runat="server" width="240">HTTP</asp:Label>
                    </td>
                </tr>
                -->
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelCategory" runat="server" AssociatedControlID="FieldCategory">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelCategory %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="FieldCategory" runat="server" Width="240"></asp:DropDownList></td>
                                <td><span class="dpi-wizard-table-error">
                                    <asp:CompareValidator ID="ValidateCategory" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateCategory %>" runat="server" ControlToValidate="FieldCategory" Operator="NotEqual" ValueToCompare="0" SetFocusOnError="true" /></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelRisk" runat="server" AssociatedControlID="FieldRisk">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelRisk %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="FieldRisk" runat="server" Width="240"></asp:DropDownList></td>
                                <td><span class="dpi-wizard-table-error">
                                    <asp:CompareValidator ID="ValidateRisk" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateRisk %>" runat="server" ControlToValidate="FieldRisk" Operator="NotEqual" ValueToCompare="0" SetFocusOnError="true" /></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelProductivity" runat="server" AssociatedControlID="FieldProductivity">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelProductivity %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="FieldProductivity" runat="server" Width="240"></asp:DropDownList></td>
                                <td><span class="dpi-wizard-table-error">
                                    <asp:CompareValidator ID="ValidateProductivity" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateProductivity %>" runat="server" ControlToValidate="FieldProductivity" Operator="NotEqual" ValueToCompare="0" SetFocusOnError="true" /></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="FilterRow" runat="server">
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelFilter" runat="server" AssociatedControlID="FieldFilter">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_LabelFilter %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-table-field">
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ClientIDMode="Static" ID="FilterSyntax" runat="server" Width="240"></asp:DropDownList></td>
                                            <td>
                                                <asp:TextBox ClientIDMode="Static" ID="FieldFilter" runat="server" Width="240" MaxLength="255"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                                <td><span class="dpi-wizard-table-field" style="line-height: 30px !important;">
                                    <asp:RequiredFieldValidator ID="ValidateFilter" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateFilter %>" runat="server" ControlToValidate="FieldFilter" SetFocusOnError="true" /></span></td>
                            </tr>
                        </table>
                        <span class="dpi-wizard-table-hint"><%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_HintFilter %> <span id="dynamic_filter_hint" clientidmode="Static"></span></span>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br />

    <div class="dpi-wizard-button-box">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="BackButton" LocalizedText="Back" OnClick="Back_Click" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="NextButton" LocalizedText="Next" OnClick="Next_Click" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="CancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </div>

    <script>
        $(document).ready(function () {
            // dynamic url filter sample
            var doneTypingInterval = 300;
            var typingTimer;

            $('#FieldFilter').keyup(function () {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(function () {
                    updateFilterExample();
                }, doneTypingInterval);
            });

            $('#FilterSyntax').change(function() {
                updateFilterExample();
            });

            updateFilterExample();
        });
        function updateFilterExample() {
            if (!$("#FilterSyntax")[0])
                return;

            var output = '';
            var fSyntax = $("#FilterSyntax")[0].selectedIndex;
            var fString = $("#FieldFilter").val();

            if (fString.length === 0) {
                fString = '...';
            }

            if (fSyntax === 0) {
                output = 'http://<span style="color:#ff6347">*' + fString + '*</span>/path/page.html';
            }
            else if (fSyntax === 1) {
                output = 'http://<span style="color:#ff6347">' + fString + '*</span>/path/page.html';
            }
            else if (fSyntax === 2) {
                output = 'http://<span style="color:#ff6347">*' + fString + '</span>/path/page.html';
            }
            else if (fSyntax === 3) {
                output = 'http://<span style="color:#ff6347">' + fString + '</span>/path/page.html';
            }
            else if (fSyntax === 4) {
                output = 'http://<span style="color:#ff6347">*' + fString + '*</span>';
            }
            else if (fSyntax === 5) {
                output = 'http://<span style="color:#ff6347">' + fString + '*</span>';
            }
            else if (fSyntax === 6) {
                output = 'http://<span style="color:#ff6347">*' + fString + '</span>';
            }
            else if (fSyntax === 7) {
                output = 'http://<span style="color:#ff6347">' + fString + '</span>';
            }

            $("#dynamic_filter_hint").html(output);
        }
    </script>
</asp:Content>