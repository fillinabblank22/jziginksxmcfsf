﻿using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web.UI.Wizards.DPIApplication;
using SolarWinds.Orion.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Orion_DPI_Admin_Applications_Add_ConfigureApplication : DPIApplicationWizard
{
    private DpiApplicationWebModel appModel;

    protected void Page_Load(object sender, EventArgs e)
    {
        appModel = base.LoadFromSession();
        if (!IsPostBack)
        {
            if (Workflow.IsEditingExistingModel)
            {
                BackButton.Visible = false;
            }
            initPageControls();
            updatePageControls();
        }
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            appModel.DpiApplication.Name = FieldName.Text;
            appModel.DpiApplication.Description = FieldDescription.Value;
            appModel.DpiApplication.AdminStatus = FieldEnabled.Checked ? OrionObjectStatus.Up : OrionObjectStatus.Unmanaged;
            appModel.DpiApplication.CategoryID = int.Parse(FieldCategory.SelectedValue);
            appModel.DpiApplication.RiskLevel = byte.Parse(FieldRisk.SelectedValue);
            appModel.DpiApplication.ProductivityRating = byte.Parse(FieldProductivity.SelectedValue);

            if (appModel.IsPreconfigured.HasValue && appModel.IsPreconfigured.Value)
            {
                appModel.DpiApplication.FilterSyntax = null;
                appModel.DpiApplication.Filter = null;
            }
            else
            {
                if (FieldFilter.Text == "*")
                // this is special case that should match *everything*
                // in that case, Filter would make no sense
                // this one is there because we create the app during installation
                {
                    appModel.DpiApplication.FilterSyntax = null;
                    appModel.DpiApplication.Filter = FieldFilter.Text;
                }
                else
                {
                    appModel.DpiApplication.FilterSyntax = FilterSyntax.SelectedValue;
                    appModel.DpiApplication.Filter = FieldFilter.Text;
                }
            }
            appModel.DpiApplication.DiscoveryMode = FieldDiscovery.Checked
                ? ApplicationDiscoveryMode.Active
                : ApplicationDiscoveryMode.Inactive;

            SaveToSession(appModel);
            return true;
        }
        catch
        {
            // error
            return false;
        }
    }

    private void initPageControls()
    {
        var optionNotSelectedString = Resources.DPIWebContent.DPIApplicationWizard_ConfigureApplication_ChooseOption;

        var categoryList = SolarWinds.DPI.Web.DALs.Swis.ApplicationCategory.GetAll();
        var catList = new Dictionary<int, string>().ToList();
        catList.Insert(0, new KeyValuePair<int, string>(0, optionNotSelectedString));
        foreach (SolarWinds.DPI.Web.DALs.Swis.ApplicationCategory c in categoryList)
        {
            catList.Add(new KeyValuePair<int, string>(c.CategoryID, c.Name));
        }
        FieldCategory.DataSource = catList.ToList();
        FieldCategory.DataTextField = "Value";
        FieldCategory.DataValueField = "Key";
        FieldCategory.DataBind();

        var riskList = Constants.RiskLevels.ToList();
        riskList.Insert(0, new KeyValuePair<int, string>(0, optionNotSelectedString));
        FieldRisk.DataSource = riskList;
        FieldRisk.DataTextField = "Value";
        FieldRisk.DataValueField = "Key";
        FieldRisk.DataBind();

        var prodList = Constants.ProductivityRatings.ToList();
        prodList.Insert(0, new KeyValuePair<int, string>(0, optionNotSelectedString));
        FieldProductivity.DataSource = prodList;
        FieldProductivity.DataTextField = "Value";
        FieldProductivity.DataValueField = "Key";
        FieldProductivity.DataBind();

        var filterSyntaxList = Constants.HTTPFilterSyntaxTypes.ToList();
        FilterSyntax.DataSource = filterSyntaxList;
        FilterSyntax.DataTextField = "Key";
        FilterSyntax.DataValueField = "Value";
        FilterSyntax.DataBind();
    }

    private void updatePageControls()
    {
        FieldName.Text = appModel.DpiApplication.Name;
        FieldDescription.Value = appModel.DpiApplication.Description;
        FieldEnabled.Checked = appModel.DpiApplication.AdminStatus != OrionObjectStatus.Unmanaged;
        FieldDiscovery.Checked = appModel.DpiApplication.DiscoveryMode != ApplicationDiscoveryMode.Inactive;
        FieldCategory.SelectedValue = appModel.DpiApplication.CategoryID.ToString();
        FieldRisk.SelectedValue = appModel.DpiApplication.RiskLevel.ToString();
        FieldProductivity.SelectedValue = appModel.DpiApplication.ProductivityRating.ToString();
        FilterSyntax.SelectedValue = appModel.DpiApplication.FilterSyntax;
        FieldFilter.Text = appModel.DpiApplication.Filter;

        if (appModel.IsPreconfigured.Value)
        {
            FilterRow.Visible = false;
            FieldName.Enabled = false;
            FieldDescription.Disabled = true;
        }
    }
}