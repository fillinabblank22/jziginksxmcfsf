using SolarWinds.DPI.Common;
using SolarWinds.DPI.Common.DAL.Helpers;
using SolarWinds.DPI.Common.Models;
using SolarWinds.DPI.Web.UI.Wizards.DPIApplication;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Data;
using System.Linq;
using System.Web;

public partial class Orion_DPI_Admin_Applications_Add_Summary : DPIApplicationWizard
{
    protected DpiApplicationWebModel appModel;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            NextButton.Enabled = false;
            NextButton.ToolTip = Resources.DPIWebContent.DemoMode_DisableSaveButton_ToolTip;
        }

        appModel = LoadFromSession();
        if (!IsPostBack)
        {
            updatePageControls();
        }
    }

    protected override bool SaveCurrentState()
    {
        var thresholdControl = ControlHelper.FindControlRecursive(CollapsePanel1, "dpiThresholdControl") as DPIThresholdControl;
        appModel.DpiApplication.Thresholds = thresholdControl.GetThresholdConfiguration();
        return true;
    }

    private void updatePageControls()
    {
        var filterSyntaxKey = Constants.HTTPFilterSyntaxTypes.FirstOrDefault(x => x.Value == appModel.DpiApplication.FilterSyntax).Key;
        FieldName.Text = UIHelper.Escape(appModel.DpiApplication.Name);
        FieldDescription.Text = UIHelper.Escape(appModel.DpiApplication.Description);
        FieldEnabled.ImageUrl = (appModel.DpiApplication.AdminStatus == OrionObjectStatus.Up) ? "/Orion/DPI/images/app_enable_on.png" : "/Orion/DPI/images/app_enable_off.png";
        FieldCategory.Text = SolarWinds.DPI.Web.DALs.Swis.ApplicationCategory.Get(appModel.DpiApplication.CategoryID).Name;
        FieldRisk.Text = Constants.RiskLevels[appModel.DpiApplication.RiskLevel];
        IconRisk.ImageUrl = String.Format("/Orion/DPI/images/app_risk_level_{0}.png", appModel.DpiApplication.RiskLevel);
        FieldProductivity.Text = Constants.ProductivityRatings[appModel.DpiApplication.ProductivityRating];
        IconProductivity.ImageUrl = String.Format("/Orion/DPI/images/app_productivity_{0}.png", appModel.DpiApplication.ProductivityRating);
        FieldFilter.Text = String.Format("{0} {1}", filterSyntaxKey, UIHelper.Escape(appModel.DpiApplication.Filter));

        if (appModel.IsPreconfigured.HasValue && appModel.IsPreconfigured.Value)
        {
            FilterRow.Visible = false;
        }

        getSelectedNodes();

        PreloadThresholds(appModel.DpiApplication);

        var thresholdControl = ControlHelper.FindControlRecursive(CollapsePanel1, "dpiThresholdControl") as DPIThresholdControl;

        thresholdControl.Thresholds = appModel.DpiApplication.Thresholds;
    }

    // if application is newly created, create default set of thresholds as well
    private void PreloadThresholds(SolarWinds.DPI.Common.Models.DpiApplication dpiApplication)
    {
        if (dpiApplication.Thresholds == null && dpiApplication.ID == 0)
        {
            using (var swis3 = InformationServiceProxy.CreateV3())
            {
                var thresholds = swis3.Query( // there is no reasonable DAL
                    "SELECT Name, DisplayName, Unit, DefaultThresholdOperator FROM Orion.ThresholdsNames WHERE EntityType='Orion.DPI.Applications' ORDER BY ThresholdOrder");

                dpiApplication.Thresholds = thresholds.Rows.Cast<DataRow>().Select(CreateThresholdFromDataRow).ToList();
            }
        }
    }

    private static DpiApplicationThreshold CreateThresholdFromDataRow(DataRow row)
    {
        var threshold = new DpiApplicationThreshold();

        threshold.Name = row["Name"].ToString();
        threshold.DisplayName = DBHelper.GetDbValueOrDefault<string>(row["DisplayName"], null);
        threshold.ThresholdType = ThresholdType.Global;
        threshold.ThresholdOperator = (ThresholdOperatorEnum)row["DefaultThresholdOperator"];
        threshold.Unit = row["Unit"].ToString();

        return threshold;
    }

    private void getSelectedNodes()
    {
        var h = String.Empty;

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"SELECT NodeID, Status, NodeName FROM Orion.Nodes WHERE NodeID IN ({0})", string.Join(", ", appModel.DpiApplication.Nodes.Select(x => x.NodeID)));
            var nodes = swis3.Query(query, true);
            foreach (DataRow nodeRow in nodes.Rows)
            {
                h += "<img src=\"" + lookupStatusIcon((int)nodeRow["NodeID"], (int)nodeRow["Status"]) + "\" />&nbsp; <span style=\"line-height:24px;position: relative;top: -4px;\">" + HttpUtility.HtmlEncode((string)nodeRow["NodeName"]) + "</span><br/>";
            }
        }

        SelectedNodes.InnerHtml = h;
    }

    private string lookupStatusIcon(int nID, int nStatus)
    {
        return SolarWinds.Orion.Web.UI.StatusIcons.NodeIconFactory.SmallIconURL(nID, nStatus);
    }
}
