using SolarWinds.DPI.Common.Models;
using SolarWinds.DPI.Web.UI.Wizards.DPIApplication;
using System;
using System.Linq;
using System.Web;

public partial class Orion_DPI_Admin_Applications_Add_ConfigureDataCollection : DPIApplicationWizard
{
    private DpiApplicationWebModel appModel;

    protected void Page_Load(object sender, EventArgs e)
    {
        appModel = base.LoadFromSession();
        if (!IsPostBack)
        {
            initPageControls();
            updatePageControls();
        }
    }

    protected override bool SaveCurrentState()
    {
        try
        {
            appModel.DpiApplication.Nodes = selectObjectsControl.SelectedItems.Split(',').Select(s => new DpiApplicationAssignment(appModel.DpiApplication.ID, int.Parse(s))).ToList();
            SaveToSession(appModel);
            return true;
        }
        catch
        {
            // error
            return false;
        }
    }

    private void initPageControls()
    {
        selectObjectsControl.EntityName = "Orion.Nodes";
        selectObjectsControl.Filter = "e.NodeID IN (SELECT DISTINCT NodeID FROM Orion.DPI.ProbeAssignments)";

        selectObjectsControl.LeftPanelTitle = Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_AvailableNodes;
        selectObjectsControl.RightPanelTitle = Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_SelectedNodes;

        if (appModel.DpiApplication.Nodes != null)
        {
            listOfSelectedItems.Value = string.Join(", ", appModel.DpiApplication.Nodes.Select(n => n.NodeID));
            selectObjectsControl.SelectedItems = string.Join(", ", appModel.DpiApplication.Nodes.Select(n => n.NodeID));
        }
    }

    private void updatePageControls()
    {
        var boldAppName = "<b>" + appModel.DpiApplication.Name + "</b>";
        LabelApplicationName.InnerHtml = String.Format(Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_Subtitle, HttpUtility.HtmlEncode(boldAppName));
        customValidator.ErrorMessage = Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_RequiredError;
    }
}