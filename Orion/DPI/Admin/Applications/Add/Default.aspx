<%@ Page Language="C#"
    MasterPageFile="~/Orion/Admin/OrionAdminPage.master"
    AutoEventWireup="true"
    CodeFile="Default.aspx.cs"
    Inherits="Orion_DPI_Admin_Applications_Add_Default"
    Title="<%$ Resources: DPIWebContent, DPIApplicationWizard_Title %>" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" File="DPI/js/EscapeHtml.js" />
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="4.0" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <orion:Include ID="Include3" runat="server" File="DPI/Admin/js/ManageApps.js" />
    <orion:Include ID="Include4" runat="server" File="DPI/Admin/js/ManagePages.Renderers.js" />
    <orion:Include ID="Include5" runat="server" File="DPI/styles/wizard/ManageApplications.css" />
    <orion:Include ID="Include6" runat="server" File="DPI/styles/DpiDataGrid.css" />
    <style>
        .hiddenToggle {
            opacity: 0.0;
            width: 0px;
            height: 0px;
            position: absolute;
            top: 0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoEApplication" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

    <h1><%= Resources.DPIWebContent.DPIApplicationWizard_Title %></h1>

    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />

    <div id="dpi-wizard-add-application">
        <h2><%= Resources.DPIWebContent.DPIApplicationWizard_DefineApplication_Title %></h2>

        <div>
            <!-- Choose a pre-configured application -->
            <asp:RadioButton ID="RadioOptionPreconfigured" Text="<%$ Resources:DPIWebContent, DPIApplicationWizard_DefineApplication_OptionChoosePreconfigured %>" runat="server" AutoPostBack="true" OnCheckedChanged="RadioOption_Change" GroupName="appType" />
            &nbsp; 
            <span class="dpi-wizard-table-error">
                <asp:RequiredFieldValidator ID="ValidateProceraProtocolSelected" ErrorMessage="<%$ Resources: DPIWebContent, DPIApplicationWizard_ConfigureApplication_ValidateName %>" runat="server" ControlToValidate="ProceraProtocolSelected" SetFocusOnError="false" /></span>

            <div id="WizardViewPreDefined" runat="server" class="dpi-pre-defined-box">
                <div id="ManageAppsGrid" />
                <script type="text/javascript">
                    Ext4.apply(Ext4, ((a = navigator.userAgent) && /Trident/.test(a) && /rv:11/.test(a)) ? { isIE: true, isIE11: true, ieVersion: 11 } : {});
                    $(document).ready(function () {
                        SW.DPI.ManageApps.CreateGrid(true);
                    });
                </script>
            </div>

            <asp:TextBox ID="ProceraProtocolSelected" Value="" runat="server" ClientIDMode="Static" AutoPostBack="true" CssClass="hiddenToggle" />
        </div>

        <div>
            <!-- Create a new HTTP application -->
            <asp:RadioButton ID="RadioOptionCustom" Text="<%$ Resources:DPIWebContent, DPIApplicationWizard_DefineApplication_OptionCreateNew %>" runat="server" AutoPostBack="true" OnCheckedChanged="RadioOption_Change" GroupName="appType" />
        </div>

        <br />
    </div>

    <div class="dpi-wizard-button-box">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="nextButton" LocalizedText="Next" OnClick="Next_Click" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </div>
</asp:Content>
