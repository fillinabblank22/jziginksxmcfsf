﻿using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web.UI.Wizards.DPIApplication;
using System;
using System.Collections.Generic;

public partial class Orion_DPI_Admin_Applications_Add_Default : DPIApplicationWizard
{
    private DpiApplicationWebModel appModel;

    protected void Page_Load(object sender, EventArgs e)
    {
        appModel = base.LoadFromSession();
        SaveToSession(appModel);
        if (!IsPostBack)
        {
            if (Workflow.IsEditingExistingModel)
            {
                base.Workflow.Steps.RemoveAt(0);
                GotoFirstStep();
            }
            updatePageControls();
        }
    }

    protected override bool SaveCurrentState()
    {
        if (!base.Workflow.IsEditingExistingModel)
        {
            appModel.DpiApplication.DiscoveryMode = ApplicationDiscoveryMode.Active;

            if (appModel.IsPreconfigured.HasValue && appModel.IsPreconfigured.Value)
            {
                // loads the default values for the selected pre-defined protocol
                var app = SolarWinds.DPI.Web.DALs.Swis.ApplicationProtocol.Get(ProceraProtocolSelected.Text);
                appModel.DpiApplication.ProtocolID = ProceraProtocolSelected.Text;
                appModel.DpiApplication.Name = app.Name;
                appModel.DpiApplication.Description = app.Description;
                appModel.DpiApplication.AdminStatus = SolarWinds.Orion.Core.Common.OrionObjectStatus.Up;
                appModel.DpiApplication.CategoryID = app.CategoryID;
                appModel.DpiApplication.RiskLevel = app.RiskLevel;
                appModel.DpiApplication.ProductivityRating = app.ProductivityRating;
            }
        }

        SaveToSession(appModel);
        return true;
    }

    private void updatePageControls()
    {
        if (appModel.IsPreconfigured.HasValue)
        {
            if (appModel.IsPreconfigured.Value)
            {
                if (!String.IsNullOrEmpty(appModel.DpiApplication.ProtocolID))
                {
                    ProceraProtocolSelected.Text = appModel.DpiApplication.ProtocolID;
                }
                else
                {
                    ProceraProtocolSelected.Text = "";
                }
                WizardViewPreDefined.Visible = true;
            }
            else
            {
                WizardViewPreDefined.Visible = false;
                ProceraProtocolSelected.Text = Constants.KnownProtocols.Http;
            }
            RadioOptionCustom.Checked = !appModel.IsPreconfigured.Value;
            RadioOptionPreconfigured.Checked = appModel.IsPreconfigured.Value;
        }
        else
            WizardViewPreDefined.Visible = false;
    }

    protected void RadioOption_Change(object sender, EventArgs e)
    {
        appModel.IsPreconfigured = RadioOptionPreconfigured.Checked;
        updatePageControls();
    }
}