﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ConfigureDataCollection.aspx.cs" Inherits="Orion_DPI_Admin_Applications_Add_ConfigureDataCollection"
    Title="<%$ Resources: DPIWebContent, DPIApplicationWizard_Title %>" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/DPI/Admin/Controls/SelectObjects/SelectObjectsControl.ascx" TagName="SelectObjectsControl" TagPrefix="orion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" File="DPI/styles/wizard/ManageApplications.css" />

    <script type="text/javascript">
        function dpiValidate(source, arguments) {
            var gi = document.getElementById("gridItems");

            if ((gi && gi.value && gi.value.length > 0)) {
                document.getElementById("listOfSelectedItems").value = gi.value;
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
    </script>
    <style>
        .sw-suggestion a {
            color: #336699 !important;
        }

            .sw-suggestion a:hover {
                color: #f99d1c !important;
            }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoEApplication" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

    <h1><%= Resources.DPIWebContent.DPIApplicationWizard_Title %></h1>

    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />

    <div id="dpi-wizard-add-application">
        <br />

        <h2><%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_Title %></h2>

        <p id="LabelApplicationName" runat="server"></p>

        <div class="sw-suggestion" style="margin-bottom: 15px">
            <span class="sw-suggestion-icon"></span>
            <%= Resources.DPIWebContent.DPIApplicationWizard_ConfigureDataCollection_InlineTip %>
        </div>

        <br />

        <orion:SelectObjectsControl ID="selectObjectsControl" runat="server" SingleSelectionOnly="false" />

        <asp:CustomValidator ID="customValidator" runat="server" ClientValidationFunction="dpiValidate" Display="Dynamic" />
        <asp:HiddenField ID="listOfSelectedItems" runat="server" ClientIDMode="Static" Value="" />
    </div>

    <br />
    <br />

    <div class="dpi-wizard-button-box">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="BackButton" LocalizedText="Back" OnClick="Back_Click" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="NextButton" LocalizedText="Next" OnClick="Next_Click" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="CancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </div>
</asp:Content>