﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_DPI_Admin_Applications_Add_Summary"
    Title="<%$ Resources: DPIWebContent, DPIApplicationWizard_Title %>" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/DPI/Admin/Applications/DPIThresholdControl.ascx" TagPrefix="orion" TagName="DPIThresholdControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include ID="Include1" runat="server" File="DPI/styles/wizard/ManageApplications.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoEApplication" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

    <h1><%= Resources.DPIWebContent.DPIApplicationWizard_Title %></h1>

    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />

    <div id="dpi-wizard-add-application">
        <h2><%= Resources.DPIWebContent.DPIApplicationWizard_Summary_Title %></h2>

        <br />

        <div id="WizardViewSummary">
            <table id="dpi-wizard-table">
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelName" runat="server" AssociatedControlID="FieldName">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelName %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <asp:Label ID="FieldName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelDescription" runat="server" AssociatedControlID="FieldDescription">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelDesc %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <asp:Label ID="FieldDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelEnabled" runat="server" AssociatedControlID="FieldEnabled">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelEnabled %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <asp:Image ID="FieldEnabled" runat="server"></asp:Image>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelCategory" runat="server" AssociatedControlID="FieldCategory">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelCategory %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <asp:Label ID="FieldCategory" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelRisk" runat="server" AssociatedControlID="FieldRisk">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelRisk %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <span style="position: relative; top: 2px;">
                            <asp:Image ID="IconRisk" runat="server" /></span>&nbsp;
                        <asp:Label ID="FieldRisk" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelProductivity" runat="server" AssociatedControlID="FieldProductivity">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelProductivity %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <span style="position: relative; top: 2px;">
                            <asp:Image ID="IconProductivity" runat="server" /></span>&nbsp;
                        <asp:Label ID="FieldProductivity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="FilterRow" runat="server">
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelFilter" runat="server" AssociatedControlID="FieldFilter">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelFilter %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <asp:Label ID="FieldFilter" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="dpi-wizard-table-label">
                        <asp:Label ID="LabelNodes" runat="server">
                            <%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelNodes %>:
                        </asp:Label>
                    </td>
                    <td class="dpi-wizard-summary-field">
                        <div id="SelectedNodes" runat="server"></div>
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td colspan="2">
                        <orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
                            <titletemplate>
                                <div class="threshold-toggle">
                                    <span><b><%= Resources.DPIWebContent.DPIApplicationWizard_Summary_LabelThresholds %>:</b></span>
                                    <span class="threshold-info"><%= Resources.DPIWebContent.DPIApplicationWizard_Summary_ThresholdInfo %></span>
                                </div>
                            </titletemplate>
                            <blocktemplate>
                                <div class="threshold-container">
                                    <orion:DPIThresholdControl runat="server" ID="dpiThresholdControl" />
                                </div>
                            </blocktemplate>
                        </orion:CollapsePanel>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="dpi-wizard-button-box">
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="BackButton" LocalizedText="Back" OnClick="Back_Click" DisplayType="Secondary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="NextButton" LocalizedText="Finish" OnClick="Finish_Click" DisplayType="Primary" />
            <orion:LocalizableButton runat="server" ID="CancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
        </div>
    </div>
</asp:Content>