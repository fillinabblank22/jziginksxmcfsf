﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.DPI.Common.Models;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.Controllers;
using SolarWinds.Orion.Web.Model.Thresholds;

public partial class DPIThresholdControl : UserControl
{
    private static readonly Log _log = new Log();
    public List<DpiApplicationThreshold> Thresholds { get; set; }

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            repThresholds.DataSource = Thresholds;
            repThresholds.DataBind();
        }
    }

    private void AddOnSubmitValidating(string thresholdIsValidHfClientId, string thresholdName)
    {
        ScriptManager.RegisterOnSubmitStatement(this, GetType(),
            "ValidateThreshold_" + thresholdName.Replace(".", "_"),
            "if ($('#" + thresholdIsValidHfClientId + "').val() == '0') return false;");
    }

    protected void repThresholds_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var control = e.Item.FindControl("thresholdControl") as ThresholdControl;
            if (control == null)
                return;

            var item = e.Item.DataItem as DpiApplicationThreshold;
            if (item == null) throw new InvalidCastException("'item' must be type of 'DpiApplicationThreshold'");

            control.ObjectIDs = item.InstanceId.ToString(CultureInfo.InvariantCulture);
            control.ThresholdName = item.Name;
            AddOnSubmitValidating(control.IsValidFieldClientId, control.ThresholdName);
            control.ThisThresholdType = item.ThresholdType;
            control.ThisThresholdOperator = item.ThresholdOperator;
            control.GlobalWarningValue = item.GlobalWarningValue.HasValue ? item.GlobalWarningValue.Value.ToString(CultureInfo.InvariantCulture) : null;
            control.GlobalCriticalValue = item.GlobalCriticalValue.HasValue ? item.GlobalCriticalValue.Value.ToString(CultureInfo.InvariantCulture) : null;

            if (item.ThresholdType == ThresholdType.Static)
            {
                control.WarningValue = item.Level1Value.HasValue ? item.Level1Value.Value.ToString(CultureInfo.InvariantCulture) : null;
                control.CriticalValue = item.Level2Value.HasValue ? item.Level2Value.Value.ToString(CultureInfo.InvariantCulture) : null;
            }
            else if (item.ThresholdType == ThresholdType.Dynamic)
            {
                control.WarningValue = item.Level1Formula;
                control.CriticalValue = item.Level2Formula;
            }
        }
    }

    public List<DpiApplicationThreshold> GetThresholdConfiguration()
    {
        var rv = new List<DpiApplicationThreshold>();
        foreach (RepeaterItem threshold in repThresholds.Items)
        {
            var control = threshold.FindControl("thresholdControl") as ThresholdControl;
            var item = new DpiApplicationThreshold();

            item.InstanceId = Convert.ToInt32(control.ObjectIDs);
            item.ThresholdType = control.ThisThresholdType;
            item.ThresholdOperator = control.ThisThresholdOperator;
            item.Name = control.ThresholdName;
            item.GlobalWarningValue = string.IsNullOrWhiteSpace(control.GlobalWarningValue) ? (double?)null : Convert.ToDouble(control.GlobalWarningValue);
            item.GlobalCriticalValue = string.IsNullOrWhiteSpace(control.GlobalCriticalValue) ? (double?)null : Convert.ToDouble(control.GlobalCriticalValue);

            double warningValue;
            double criticalValue;

            if (double.TryParse(control.WarningValue, out warningValue) && double.TryParse(control.CriticalValue, out criticalValue))
            {
                item.Level1Value = warningValue;
                item.Level2Value = criticalValue;
            }

            if (item.ThresholdType == ThresholdType.Dynamic)
            {
                item.Level1Formula = control.WarningValue;
                item.Level2Formula = control.CriticalValue;

                var request = new ComputeThresholdRequest()
                {
                    ThresholdName = item.Name,
                    InstancesId = new[] { item.InstanceId },
                    Operator = item.ThresholdOperator,
                    WarningFormula = item.Level1Formula,
                    CriticalFormula = item.Level2Formula
                };

                var response = new ThresholdsController().Compute(request);
                if (response.IsComputed)
                {
                    item.Level1Value = response.WarningThreshold;
                    item.Level2Value = response.CriticalThreshold;
                }
                if (response.ErrorMessages != null)
                {
                    foreach (var e in response.ErrorMessages)
                    {
                        _log.Error(e);
                    }
                }
            }

            rv.Add(item);
        }
        return rv;
    }
}