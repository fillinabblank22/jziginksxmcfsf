﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DPIThresholdControl.ascx.cs" Inherits="DPIThresholdControl" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="SolarWinds.DPI.Common.Models" %>
<%@ Register Src="~/Orion/Controls/ThresholdControl.ascx" TagPrefix="orion" TagName="ThresholdControl" %>
<%@ Register Src="~/Orion/Controls/BaselineDetails.ascx" TagPrefix="orion" TagName="BaselineDetails" %>

<asp:Repeater runat="server" ID="repThresholds" OnItemDataBound="repThresholds_ItemDataBound">
    <ItemTemplate>
        <orion:ThresholdControl EnableViewState="True" runat="server" ID="thresholdControl"
            DisplayName='<%#((DpiApplicationThreshold) Container.DataItem).DisplayName %>'
            ThresholdName='<%#((DpiApplicationThreshold) Container.DataItem).Name %>'
            Unit='<%#((DpiApplicationThreshold) Container.DataItem).Unit %>' />
    </ItemTemplate>
</asp:Repeater>

<orion:BaselineDetails runat="server" ID="BaselineDetails" />