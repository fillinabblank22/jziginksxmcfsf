using System;
using System.Data;
using System.Text;
using System.Web.Script.Serialization;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web.UI.Wizards.DPINode;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.InformationService;

public partial class Orion_DPI_Admin_DPINode_Summary : DPINodeWizard
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var model = base.LoadFromSession();
            rptNodeNames.DataSource = model.TempNodeNames.OrderBy(n => n);
            rptNodeNames.DataBind();

            rptQoeAppNames.DataSource = model.TempQoEAppNames.OrderBy(x => x);
            rptQoeAppNames.DataBind();

            ltrCollector.Text = HttpUtility.HtmlEncode(model.TempCollectorName);
        }
    }

    protected override bool SaveCurrentState()
    {
        return true;
    }
}
