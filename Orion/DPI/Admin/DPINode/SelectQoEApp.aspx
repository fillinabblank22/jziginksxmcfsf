﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="SelectQoEApp.aspx.cs" Inherits="Orion_DPI_Admin_DPINode_SelectQoEApp" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/DPI/Admin/Controls/SelectApplications/SelectApplicationsControl.ascx" TagName="SelectApplicationsControl" TagPrefix="orion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoENode" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1><%= Resources.DPIWebContent.DPINodeWizard_Title %></h1>
    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />
    <div class="GroupBox">
        <span id="stepHeader"><%= Resources.DPIWebContent.DPINodeWizard_SelectApp_Header %></span>
        <div id="stepSubHeader">
            <%= Resources.DPIWebContent.DPINodeWizard_SelectApp_subtitle %>
        </div>
        <div class="sw-suggestion" style="margin: 0 0 10px 0">
            <span class="sw-suggestion-icon"></span>
            <%= Resources.DPIWebContent.DPINodeWizard_SelectApp_Monitoring %>
        </div>
        <orion:SelectApplicationsControl ID="selectApplicationsControl" runat="server" />
        <div class="dpi-wizard-button-box">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="BackButton" LocalizedText="Back" OnClick="Back_Click" DisplayType="Secondary" CausesValidation="false" />
                <orion:LocalizableButton runat="server" ID="nextButton" LocalizedText="Next" OnClick="Next_Click" DisplayType="Primary" CausesValidation="true" />
                <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Content>