using SolarWinds.DPI.Web.UI.Wizards.DPINode;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

public partial class Orion_DPI_Admin_DPINode_ChooseDpiNode : DPINodeWizard
{
    protected string CurrentUrlEncoded
    {
        get
        {
            return UrlHelper.ToSafeUrlParameter(Page.Request.RawUrl);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            selectObjectsControl.EntityName = "Orion.Nodes";
            selectObjectsControl.LeftPanelTitle = Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_LeftPanelTitle;
            selectObjectsControl.RightPanelTitle = Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_RightPanelTitle;
            selectObjectsControl.Filter = "e.NodeID NOT IN (SELECT NodeID FROM Orion.DPI.ProbeAssignments)";

            var model = base.LoadFromSession();
            selectObjectsControl.SelectedItems = (model.NodeIds.Any()) ? string.Join(",", model.NodeIds) : string.Empty;

            AddNodesToSensorTitle.InnerHtml = Resources.DPIWebContent.DPINodeWizard_Title;
            StepSubHeader.InnerHtml = string.Format(Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_subtitle, HttpUtility.HtmlEncode(model.TempCollectorName));

            if (Workflow.IsEditingExistingModel)
            {
                base.Workflow.Steps.RemoveAt(0);
                SaveCurrentState();
                GotoFirstStep();
            }
        }
    }

    protected override bool SaveCurrentState()
    {
        var model = base.LoadFromSession();

        model.NodeIds = this.selectObjectsControl.SelectedItems.Split(',').Select(s => int.Parse(s)).ToList();

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var swql = string.Format("SELECT DisplayName FROM Orion.Nodes WHERE NodeID IN ({0})",
                string.Join(", ", model.NodeIds));
            var nodeData = swis.Query(swql);
            model.TempNodeNames = nodeData.Rows.Cast<DataRow>().Select(r => r.Field<string>(0)).ToList();
        }
        SaveToSession(model);
        return true;
    }
}
