﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="ChooseDpiNode.aspx.cs" Inherits="Orion_DPI_Admin_DPINode_ChooseDpiNode" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/DPI/Admin/Controls/SelectObjects/SelectObjectsControl.ascx" TagName="SelectObjectsControl" TagPrefix="orion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoENode" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <script>
        function dpiValidate(source, arguments) {
            var value = $("#gridItems").val();
            if (!value) {
                Ext.Msg.minWidth = 310;
                Ext.Msg.alert(
                    '<%= Resources.DPIWebContent.DPINodeWizard_ChooseOneDpiNode_Title %>',
                    '<%= Resources.DPIWebContent.DPINodeWizard_ChooseOneDpiNode_Msg %>');
                arguments.IsValid = false;
            }
        }
    </script>
    <style>
        .sw-suggestion a {
            color: #336699 !important;
        }

            .sw-suggestion a:hover {
                color: #f99d1c !important;
            }
    </style>
    <h1 id="AddNodesToSensorTitle" runat="server"></h1>
    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />
    <div class="GroupBox">
        <span id="stepHeader"><%= Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_Header %></span>
        <div id="StepSubHeader" runat="server"></div>
        <div class="sw-suggestion" style="margin: 15px 0">
            <span class="sw-suggestion-icon"></span>
            <%= Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_AddNodeDescription %> &nbsp; &raquo; <a href="/Orion/Nodes/Add/Default.aspx?ReturnTo=<%= CurrentUrlEncoded %>"><%= Resources.DPIWebContent.DPINodeWizard_ChooseDpiNode_AddNodeTitle %></a>
        </div>
        <orion:SelectObjectsControl ID="selectObjectsControl" runat="server" />
        <asp:CustomValidator ID="customValidator" runat="server" ClientValidationFunction="dpiValidate" />
        <div class="dpi-wizard-button-box">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="nextButton" LocalizedText="Next" OnClick="Next_Click" DisplayType="Primary" CausesValidation="true" />
                <orion:LocalizableButton runat="server" ID="cancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Content>