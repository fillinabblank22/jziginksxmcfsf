﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Orion_DPI_Admin_DPINode_Summary" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/Controls/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIAddQoENode" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

    <h1><%= Resources.DPIWebContent.DPINodeWizard_Title %></h1>
    <orion:Progress ID="ProgressIndicator" runat="server" IndicatorHeight="OneLine" RedirectEnabled="false" />
    <div class="GroupBox">
        <span id="stepHeader"><%= Resources.DPIWebContent.DPINodeWizard_Summary_Header %></span>
        <table class="summaryTable">
            <tbody>
                <tr>
                    <td class="summaryTable_Col1"><%= Resources.DPIWebContent.DPINodeWizard_Summary_DpiNode %></td>
                    <td>
                        <asp:Repeater runat="server" ID="rptNodeNames">
                            <ItemTemplate><%#  HttpUtility.HtmlEncode((string)Container.DataItem) %></ItemTemplate>
                            <SeparatorTemplate>
                                <br />
                            </SeparatorTemplate>
                        </asp:Repeater>
                </tr>
                <tr>
                    <td class="summaryTable_Col1"><%= Resources.DPIWebContent.DPINodeWizard_Summary_AoEApps %></td>
                    <td>
                        <asp:Repeater runat="server" ID="rptQoeAppNames">
                            <ItemTemplate><%# HttpUtility.HtmlEncode((string)Container.DataItem) %></ItemTemplate>
                            <SeparatorTemplate>
                                <br />
                            </SeparatorTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td class="summaryTable_Col1"><%= Resources.DPIWebContent.DPINodeWizard_Summary_CollectorNodes %></td>
                    <td>
                        <asp:Literal runat="server" ID="ltrCollector" />
                    </td>
                </tr>
            </tbody>
        </table>

        <asp:HiddenField ID="hResult" ClientIDMode="Static" runat="server"></asp:HiddenField>

        <div class="dpi-wizard-button-box">
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="BackButton" LocalizedText="Back" OnClick="Back_Click" DisplayType="Secondary" OnClientClick="return CheckGoBack();" CausesValidation="false" />
                <orion:LocalizableButton runat="server" ID="NextButton" LocalizedText="Finish" OnClick="Finish_Click" DisplayType="Primary" OnClientClick="return CheckFinish();" />
                <orion:LocalizableButton runat="server" ID="CancelButton" LocalizedText="Cancel" OnClick="Cancel_Click" DisplayType="Secondary" CausesValidation="false" />
            </div>
        </div>
    </div>
</asp:Content>
