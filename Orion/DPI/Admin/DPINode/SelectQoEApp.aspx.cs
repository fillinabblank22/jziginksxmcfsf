﻿using SolarWinds.DPI.Common.Models;
using SolarWinds.DPI.Web.UI.Helpers;
using SolarWinds.DPI.Web.UI.Wizards.DPINode;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Admin_DPINode_SelectQoEApp : DPINodeWizard
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BackButton.Visible = !Workflow.IsEditingExistingModel;

            var model = base.LoadFromSession();
            //selectApplicationsControl.NodeId = model.Node.OrionNode;
            selectApplicationsControl.EntityName = SolarWinds.DPI.Common.Constants.DpiApplications;
            selectApplicationsControl.SelectedItems = string.Join(",", GetSelectedIds(model));
            selectApplicationsControl.LeftPanelTitle = Resources.DPIWebContent.DPINodeWizard_SelectApp_LeftPanelTitle;
            selectApplicationsControl.RightPanelTitle = Resources.DPIWebContent.DPINodeWizard_SelectApp_RightPanelTitle;
        }
    }

    private IEnumerable<string> GetSelectedIds(DPINodeWebModel model)
    {
        var result = model.QoEAppIds.Select(a => String.Format("A:{0}", a)).ToList();

        if (model.QoEApplicationProtocols == null)
        {
            return result;
        }

        result.AddRange(model.QoEApplicationProtocols.Select(ap => String.Format("AP:{0}", ap)));
        return result;
    }

    protected override bool SaveCurrentState()
    {
        var model = base.LoadFromSession();
        var appIds = DpiApplicationHelper.ParseDpiApplicationIds(selectApplicationsControl.SelectedItems);
        model.QoEAppIds = appIds.ToList();
        model.QoEApplicationProtocols = DpiApplicationHelper.ParseDpiApplicationProtocolIds(selectApplicationsControl.SelectedItems).ToList();
        model.TempQoEAppNames = new List<string>();
        //Load app names into temp variable in workflow object - used only on UI
        if (model.QoEAppIds.Any())
        {
            var query = string.Format("SELECT DisplayName FROM Orion.DPI.Applications WHERE ApplicationID IN ({0})", string.Join(", ", model.QoEAppIds));
            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(query);
                model.TempQoEAppNames = data.Rows.Cast<DataRow>().Select(r => r.Field<string>(0)).ToList();
            }
        }

        if (model.QoEApplicationProtocols.Any())
        {
            var query = string.Format("SELECT Name FROM Orion.DPI.ApplicationProtocols WHERE ProtocolID IN ('{0}')", string.Join("','", model.QoEApplicationProtocols));

            using (var swis = InformationServiceProxy.CreateV3())
            {
                var data = swis.Query(query);
                model.TempQoEAppNames.AddRange(data.Rows.Cast<DataRow>().Select(r => r.Field<string>(0)).ToList());
            }
        }

        SaveToSession(model);
        return true;
    }
}