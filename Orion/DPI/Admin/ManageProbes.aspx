﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" AutoEventWireup="true" Title="<%$ Resources:DPIWebContent,ManageProbesPage_Title %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/DPI/Admin/Controls/CollectorNodes/CollectorNodesControl.ascx" TagPrefix="orion" TagName="CollectorNodesControl" %>

<asp:Content ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <style>
        .sw-suggestion a {
            color: #336699 !important;
        }

            .sw-suggestion a:hover {
                color: #f99d1c !important;
            }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIManageQoECollectors" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size: large!important;"><%= Page.Title %></h1>
    <orion:CollectorNodesControl runat="server" ID="CollectorNodesControl" Mode="0" />
    <orion:Include ID="Include1" runat="server" File="DPI/styles/DpiDataGrid.css" />
    <script>
        $(document).ready(function () {
            Ext4.apply(Ext4, ((a = navigator.userAgent) && /Trident/.test(a) && /rv:11/.test(a)) ? { isIE: true, isIE11: true, ieVersion: 11 } : {});
            var startHash = window.location.hash.substr(1);
            if (startHash === "addNetwork" || startHash === "addServer") {
                SW.DPI.CollectorAddControl.OpenDialog1(startHash);
                $(".sw-btn-bar").css("padding-top", "0");
            }
        });
    </script>
</asp:Content>