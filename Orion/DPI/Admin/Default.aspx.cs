﻿using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web.Admin;
using SolarWinds.Orion.Web;
using System;

public partial class Orion_DPI_Admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            LinkToManagePacketEngines.HRef = "#";
            LinkToManagePacketEngines.Title = Resources.DPIWebContent.SiteMap_ManageApps_LinkNotAvailInDemoMode;
            LinkToManagePacketEngines.Attributes.CssStyle.Add("color", "#336699");
        }
    }
}