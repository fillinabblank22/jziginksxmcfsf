﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" CodeFile="ManageApplications.aspx.cs" Inherits="Orion_DPI_Admin_ManageApplications" AutoEventWireup="true" Title="<%$ Resources:DPIWebContent,ManageAppsPage_Title %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="4.0" />
    <orion:Include runat="server" File="DPI/js/EscapeHtml.js" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="DPI/Admin/js/ManageApps.js" />
    <orion:Include runat="server" File="DPI/Admin/js/ManagePages.Renderers.js" />
    <orion:Include runat="server" File="DPI/styles/DpiDataGrid.css" />
    <style>
        .hiddenToggle {
            opacity: 0.0;
            filter: alpha(opacity=0);
            -moz-opacity: 0.0;
            -khtml-opacity: 0.0;
            width: 0;
            height: 0;
            position: absolute;
            top: 0;
        }

        input:-ms-clear {
            width: 0;
            height: 0;
        }

        input:-ms-reveal {
            width: 0;
            height: 0;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPIManageQoEApplications" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <h1 style="font-size: large!important; padding-bottom: 0;"><%= Page.Title %></h1>
    <div style="padding: 0 0 1em 0;">
        <%= Resources.DPIWebContent.ManageAppsPage_Description %>
        <asp:CheckBox ID="HidePrebuilt" Checked="false" Text="" runat="server" OnCheckedChanged="HidePrebuilt_CheckedChanged" AutoPostBack="true" CssClass="hiddenToggle" />
    </div>

    <div id="ManageAppsGrid" />

    <script type="text/javascript">
 	    <% if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        { %> var isDemoServer = true; <% }
        else
        { %> var isDemoServer = false; <% } %>
        $(document).ready(function () {
            Ext4.apply(Ext4, ((a = navigator.userAgent) && /Trident/.test(a) && /rv:11/.test(a)) ? { isIE: true, isIE11: true, ieVersion: 11 } : {});
            SW.DPI.ManageApps.CreateGrid(false, isDemoServer);
        });
    </script>
</asp:Content>
