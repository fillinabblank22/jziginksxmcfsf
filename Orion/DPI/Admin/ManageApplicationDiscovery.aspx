﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Admin/OrionAdminPage.master" CodeFile="ManageApplicationDiscovery.aspx.cs"
    Title="<%$ Resources:DPIWebContent,ManageAppDiscoveryPage_Title %>" Inherits="Orion_DPI_Admin_ManageApplicationDiscovery" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="server">
    <style>
        .ReadOnlyCell {
            color: gray;
            font-style: italic;
        }

        .Align {
            vertical-align: middle !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconHelpButton ID="IconHelpButton2" runat="server" HelpUrlFragment="OrionDPI-DiscoveringApplications" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table border="0" width="100%">
        <tr>
            <td>
                <h1><%=Page.Title %></h1>
            </td>
        </tr>
        <tr>
            <td>
                <table id="adminContentTable" class="NeedsZebraStripes" border="0" cellpadding="3" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <h2><%= Resources.DPIWebContent.Web_ManageAppDiscovery_QoEApps_Title %></h2>
                        </td>
                    </tr>
                    <tr class="alternateRow">
                        <td class="PropertyHeader Align" style="vertical-align: middle">
                            <%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlAppDiscoveryState" Width="80"></asp:DropDownList>
                            <span class="ReadOnlyCell">
                                <%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryState_Desc %>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader Align">
                            <%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryUrlLevel %>
                        </td>
                        <td class="Property" style="vertical-align: middle;">
                            <asp:DropDownList runat="server" ID="ddlDiscoveryUrlLevel" Width="210"></asp:DropDownList>
                            <span class="ReadOnlyCell">
                                <%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryUrlLevel_Desc %>
                            </span>
                        </td>
                    </tr>
                    <tr class="alternateRow">
                        <td class="PropertyHeader Align">
                            <%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic %>
                        </td>
                        <td class="Property" style="vertical-align: middle;">
                            <asp:DropDownList runat="server" ID="ddlDiscoveryTraffic" Width="210"></asp:DropDownList>
                            <span class="ReadOnlyCell"><%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Desc %></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader Align">
                            <span>
                                <%= Resources.DPIWebContent.Web_ManageAppDiscovery_AppLimit_Text1 %>
                            </span>
                            <asp:TextBox runat="server" ID="tbAppNum" MaxLength="4" Width="72"></asp:TextBox>
                        </td>
                        <td class="PropertyHeader">
                            <span>
                                <%= Resources.DPIWebContent.Web_ManageAppDiscovery_AppLimit_Text2 %>
                            </span>
                            <asp:TextBox runat="server" ID="tbDataVolume" MaxLength="3" Width="30"></asp:TextBox>
                            <span>
                                <%= Resources.DPIWebContent.Web_ManageAppDiscovery_AppLimit_Text3 %>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader">
                            <asp:RangeValidator ControlToValidate="tbAppNum" ID="AppNumValidator"
                                runat="server" ErrorMessage="<%$Resources:DPIWebContent, Web_ManageAppDiscovery_AppByDataVolume_ErrorMsg%>"
                                Display="Dynamic" MinimumValue="1" MaximumValue="1000" Type="Integer" />
                        </td>
                        <td class="Property">
                            <asp:RangeValidator ControlToValidate="tbDataVolume" ID="DataVolumeValidator"
                                runat="server" ErrorMessage="<%$Resources:DPIWebContent,Web_ManageAppDiscovery_AppLimit1_ErrorMsg%>"
                                Display="Dynamic" MinimumValue="0" MaximumValue="100" Type="Integer" />
                            <span class="ReadOnlyCell"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <h2><%= Resources.DPIWebContent.Web_ManageAppDiscovery_QoeNodes_Title%></h2>
                        </td>
                    </tr>
                    <tr class="alternateRow">
                        <td class="PropertyHeader Align" style="vertical-align: middle">
                            <%= Resources.DPIWebContent.Web_ManageNodeDiscovery_DiscoveryState %>
                        </td>
                        <td class="Property">
                            <asp:DropDownList runat="server" ID="ddlNodeDiscoveryState" Width="80"></asp:DropDownList>
                            <span class="ReadOnlyCell">
                                <%= Resources.DPIWebContent.Web_ManageNodeDiscovery_DiscoveryState_Desc %>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader Align">
                            <%= Resources.DPIWebContent.Web_ManageAppDiscovery_NodeDiscoveryTraffic %>
                        </td>
                        <td class="Property" style="vertical-align: middle;">
                            <asp:DropDownList runat="server" ID="ddlNodeDiscoveryTraffic" Width="210"></asp:DropDownList>
                            <span class="ReadOnlyCell"><%= Resources.DPIWebContent.Web_ManageAppDiscovery_DiscoveryTraffic_Desc %></span>
                        </td>
                    </tr>
                    <tr class="alternateRow">
                        <td>&nbsp;</td>
                        <td style="text-align: right">
                            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit"
                                DisplayType="Primary" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>