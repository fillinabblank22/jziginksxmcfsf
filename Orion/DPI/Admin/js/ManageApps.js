﻿SW.DPI = SW.DPI || {};
SW.DPI.ManageApps = SW.DPI.ManageApps || {};

(function (a) {
    var isPredefinedProtocolsMode = false;  // set to true for pre-defined protocol radio button selection

    var startItem = 0;
    var itemsPerPage = 20;
    var gridHeight = 640;
    var gridWidth = 1000;

    var serviceUrl = "/Orion/DPI/Services/ManageApps.asmx";

    var grid;
    var btnAdd;
    var btnEdit;
    var btnEnable;
    var btnDisable;
    var btnDelete;
    var btnToggle;
    var btnEnableDiscovery;
    var btnDisableDiscovery;

    var position = 0;
    var isFirstSelection = true;

    var getDataGridOptions = function () { return { start: startItem, limit: parseInt(itemsPerPage) }; };

    onSelectionChange = function (isInit) {
        // DPI-1614 : needed to work around scrolling issue that was fixed in EXTJS 4.2
        if (!(isInit === true)) {
            if (isFirstSelection) {
                isFirstSelection = false;
                $('body').scrollTop(position);
            }
        }

        var selectedModels = grid.getSelectionModel().getSelection();
        var selectedCount = selectedModels.length;

        if (selectedCount === 0) {
            btnDelete.setDisabled(true);
            btnEnable.setDisabled(true);
            btnDisable.setDisabled(true);
            btnEnableDiscovery.setDisabled(true);
            btnDisableDiscovery.setDisabled(true);

            btnEdit.setDisabled(true);
        }
        else if (selectedCount === 1) {
            $("#ProceraProtocolSelected").attr("value", selectedModels[0].data.ApplicationID);

            if (!SW.DPI.IsDemo) {
                btnDelete.setDisabled(false);

                if (isEnabledAppSelected()) {
                    btnEnable.setDisabled(true);
                    btnDisable.setDisabled(false);
                }
                else {
                    btnEnable.setDisabled(false);
                    btnDisable.setDisabled(true);
                }

                if (isEnabledDiscoveryAppSelected()) {
                    btnEnableDiscovery.setDisabled(true);
                    btnDisableDiscovery.setDisabled(false);
                }
                else {
                    btnEnableDiscovery.setDisabled(false);
                    btnDisableDiscovery.setDisabled(true);
                }
            }

            btnEdit.setDisabled(false);
        }
        else {
            if (!SW.DPI.IsDemo) {
                btnDelete.setDisabled(false);
                btnEnable.setDisabled(false);
                btnDisable.setDisabled(false);
                btnEnableDiscovery.setDisabled(false);
                btnDisableDiscovery.setDisabled(false);
            }

            btnEdit.setDisabled(true);
        }
    };

    setEnableDisableButton = function (enable) {
        if (enable) {
            btnEnable.setDisabled(true);
            btnDisable.setDisabled(false);
        } else {
            btnEnable.setDisabled(false);
            btnDisable.setDisabled(true);
        }
    };

    setEnableDisableDiscoveryButton = function (discoveryMode) {
        if (discoveryMode == 1) {
            btnEnableDiscovery.setDisabled(true);
            btnDisableDiscovery.setDisabled(false);
        } else {
            btnEnableDiscovery.setDisabled(false);
            btnDisableDiscovery.setDisabled(true);
        }
    };

    reloadGrid = function () {
        window.location = "../Admin/ManageApplications.aspx";
    };

    getSelectedIds = function () {
        var selectedModels = grid.getSelectionModel().getSelection();
        var selectedIds = [];

        for (var i = 0; i < selectedModels.length; i++) {
            selectedIds.push(selectedModels[i].internalId);
        }
        return selectedIds;
    };

    isEnabledAppSelected = function () {
        var isEnabledSelected = false;
        var selectedModels = grid.getSelectionModel().getSelection();
        var len = selectedModels.length;

        while (len--) {
            if (selectedModels[len].data.Status) {
                isEnabledSelected = true;
                break;
            }
        }

        return isEnabledSelected;
    };

    isEnabledDiscoveryAppSelected = function () {
        var isEnabledSelected = false;
        var selectedModels = grid.getSelectionModel().getSelection();
        var len = selectedModels.length;

        while (len--) {
            if (selectedModels[len].data.DiscoveryMode == 1) {
                isEnabledSelected = true;
                break;
            }
        }

        return isEnabledSelected;
    };

    enableAppByID = function (id, enable) {
        ORION.callWebService(
            serviceUrl,
            "SetApplicationsEnabled", {
            applicationIds: [id],
            enabled: enable
        },
            function () {
                var idx = grid.store.find('ApplicationID', id, 0, false, false, true);
                var dataRow = grid.store.getAt(idx);

                if (dataRow) {
                    dataRow.data.Status = enable;
                    setEnableDisableButton(enable);
                    grid.getView().refresh();
                }
            });
    };

    enableSelectedApps = function (enable) {
        var appIds = getSelectedIds();

        ORION.callWebService(
            serviceUrl,
            "SetApplicationsEnabled", {
            applicationIds: appIds,
            enabled: enable
        },
            function () {
                $.each(appIds, function (id, value) {
                    var idx = grid.store.find('ApplicationID', value, 0, false, false, true);
                    var dataRow = grid.store.getAt(idx);

                    if (dataRow) {
                        dataRow.data.Status = enable;
                    }
                });

                grid.getView().refresh();
            });
    };

    setDiscoveryModeByAppID = function (id, discoveryMode) {
        ORION.callWebService(
            serviceUrl,
            "SetApplicationDiscoveryMode", {
            applicationIds: [id],
            discoveryMode: discoveryMode
        },
            function () {
                var idx = grid.store.find('ApplicationID', id, 0, false, false, true);
                var dataRow = grid.store.getAt(idx);

                if (dataRow) {
                    dataRow.data.DiscoveryMode = discoveryMode;
                    setEnableDisableDiscoveryButton(discoveryMode);
                    grid.getView().refresh();
                }
            });
    };

    setDiscoveryModeBySelectedApps = function (discoveryMode) {
        var appIds = getSelectedIds();

        ORION.callWebService(
            serviceUrl,
            "SetApplicationDiscoveryMode", {
            applicationIds: appIds,
            discoveryMode: discoveryMode
        },
            function () {
                $.each(appIds, function (id, value) {
                    var idx = grid.store.find('ApplicationID', value, 0, false, false, true);
                    var dataRow = grid.store.getAt(idx);

                    if (dataRow) {
                        dataRow.data.DiscoveryMode = discoveryMode;
                    }
                });

                grid.getView().refresh();
            });
    };

    enableSelected = function () {
        enableSelectedApps(true);
        setEnableDisableButton(true);
    };

    disableSelected = function () {
        enableSelectedApps(false);
        setEnableDisableButton(false);
    };

    enableSelectedDiscovery = function () {
        setDiscoveryModeBySelectedApps(1);
        setEnableDisableDiscoveryButton(true);
    };

    disableSelectedDiscovery = function () {
        setDiscoveryModeBySelectedApps(0);
        setEnableDisableDiscoveryButton(false);
    };

    var confirmDlg = new Ext4.window.MessageBox({
        buttons: [{
            type: 'buttons',
            text: '@{R=DPI.Strings;K=ManageAppsDelete_ConfirmBtnDelete;E=js}',
            handler: function (btn, o) {
                ORION.callWebService(
                    serviceUrl,
                    "DeleteApps",
                    { ids: getSelectedIds() },
                    function () {
                        confirmDlg.close();
                        reloadGrid();
                    }
                );
            }
        },
        {
            type: 'buttons',
            text: '@{R=DPI.Strings;K=ManageAppsDelete_ConfirmBtnCancel;E=js}',
            handler: function (btn, o) {
                confirmDlg.close();
            }
        }]
    });

    deleteSelectedApps = function () {
        var msg = [];
        var selectedApps = grid.getSelectionModel().getSelection();

        msg.push("@{R=DPI.Strings;K=ManageAppsDelete_ConfirmTitle;E=js}");

        if (selectedApps.length === 1) {
            var appDisplayName = selectedApps[0].raw["Name"];
            if (appDisplayName.length > 38) {
                appDisplayName = appDisplayName.substring(0, 38) + "&hellip;";
                appDisplayName = '<span title="' + selectedApps[0].raw["Name"] + '">' + appDisplayName + '</span>';
            }
            msg.push("@{R=DPI.Strings;K=ManageAppsDelete_ConfirmMsg1;E=js}".replace("{0}", appDisplayName));
        }
        else {
            msg.push("@{R=DPI.Strings;K=ManageAppsDelete_ConfirmMsg2;E=js}".replace("{0}", selectedApps.length));
        }

        msg.push("@{R=DPI.Strings;K=ManageAppsDelete_ConfirmMsg3;E=js}");

        confirmDlg.show({
            title: msg[0],
            msg: '<span style="font-size:14px;font-weight:bold;">{0}</span><br/><br/>{1}<br/><br/><br/>'.replace("{0}", msg[1]).replace("{1}", msg[2]),
            icon: Ext4.MessageBox.ERROR
        });
    };

    editSelectedApp = function () {
        window.location = "../Admin/Applications/Add/Default.aspx?EditID=" + getSelectedIds()[0];
    };

    togglePredefinedApps = function () {
        $("span.hiddenToggle :checkbox").trigger('click');
    };

    addApp = function () {
        window.location = "../Admin/Applications/Add/Default.aspx";
    };

    a.CreateGrid = function (mode, demo) {
        isPredefinedProtocolsMode = mode;
        SW.DPI.IsDemo = demo;

        if (isPredefinedProtocolsMode) {
            // show fewer items per page in the wizard
            itemsPerPage = 15;
            gridHeight = 490;
        }

        var toggleLabel = ($("span.hiddenToggle :checkbox").is(':checked')) ? '@{R=DPI.Strings;K=ManageAppsAdd_Toggle_ShowAll;E=js}' : '@{R=DPI.Strings;K=ManageAppsAdd_Toggle_HidePredefined;E=js}';
        var toggleIcon = ($("span.hiddenToggle :checkbox").is(':checked')) ? '/Orion/DPI/images/toolbar_show_16.png' : '/Orion/DPI/images/toolbar_hide_16.png';

        var enabledRenderFn = function (v, o, e) {
            // SolarWinds.Orion.Core.Common.OrionObjectStatus.Unmanaged
            var enabled = ('' + v != "9");
            return SW.DPI.ManagePages.Renderers.AppEnabledToggle(enabled, o, e);
        };
        var addBrackets = function (v) { return " (" + v + ")"; };
        var addSpanPos = function (v) { return "<span style='position: relative; top: -3px; left: 8px;'> (" + v + ")</span>"; };
        var categoryRenderers = [SW.DPI.ManagePages.Renderers.AppName, addBrackets];
        var riskLevelRenderers = [SW.DPI.ManagePages.Renderers.AppRiskLevel, addSpanPos];
        var productivityRenderers = [SW.DPI.ManagePages.Renderers.AppProductivityRating, addSpanPos];
        var enabledRenderers = [enabledRenderFn, addSpanPos];

        var PredefinedProtocolsGroupByOptions = [
            ['@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', '', null],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelCategory;E=js}', 'c.Name', categoryRenderers],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelRisk;E=js}', 'a.RiskLevel', riskLevelRenderers],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelProductivity;E=js}', 'a.ProductivityRating', productivityRenderers]
        ];
        var ApplicationsModeGroupByOptions = [
            ['@{R=Core.Strings;K=WEBJS_VB0_76; E=js}', '', null],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelCategory;E=js}', 'c.Name', categoryRenderers],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelRisk;E=js}', 'a.RiskLevel', riskLevelRenderers],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelProductivity;E=js}', 'a.ProductivityRating', productivityRenderers],
            ['@{R=DPI.Strings;K=ManageAppsAdd_LabelEnabled;E=js}', 'a.Status', enabledRenderers]
        ];

        var PredefinedProtocolsColumns = [
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelApplicationName;E=js}', flex: 1, dataIndex: 'Name', renderer: SW.DPI.ManagePages.Renderers.AppName },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelCategory;E=js}', flex: 1, dataIndex: 'Category' },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelRisk;E=js}', flex: 1, dataIndex: 'RiskLevel', renderer: SW.DPI.ManagePages.Renderers.AppRiskLevel },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelProductivity;E=js}', flex: 1, dataIndex: 'ProductivityRating', renderer: SW.DPI.ManagePages.Renderers.AppProductivityRating }
        ];

        var ApplicationsModeColumns = [
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelApplicationName;E=js}', flex: 2, dataIndex: 'Name', renderer: SW.DPI.ManagePages.Renderers.AppName },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelEnabled;E=js}', flex: 1, dataIndex: 'Status', renderer: SW.DPI.ManagePages.Renderers.AppEnabledToggle },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelApplicationDiscoveryStatus;E=js}', flex: 1, dataIndex: 'DiscoveryMode', renderer: SW.DPI.ManagePages.Renderers.DiscoveryModeToggle },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelCategory;E=js}', flex: 1, dataIndex: 'Category' },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelRisk;E=js}', flex: 1, dataIndex: 'RiskLevel', renderer: SW.DPI.ManagePages.Renderers.AppRiskLevel },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelProductivity;E=js}', flex: 1, dataIndex: 'ProductivityRating', renderer: SW.DPI.ManagePages.Renderers.AppProductivityRating },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelCollectingDataOn;E=js}', flex: 1, dataIndex: 'NodeCount' },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelLastDiscoveryDate;E=js}', flex: 1, dataIndex: 'LastDiscoveryDate' },
            { text: '@{R=DPI.Strings;K=ManageAppsAdd_LabelLastDiscoveryProbe;E=js}', flex: 1, dataIndex: 'LastDiscoveryProbe' }
        ];

        var _GroupByOptions = isPredefinedProtocolsMode ? PredefinedProtocolsGroupByOptions : ApplicationsModeGroupByOptions;
        var _GroupByServiceUrl = serviceUrl + (isPredefinedProtocolsMode ? "/GetGroupByProtocols" : "/GetGroupByApp");
        var _DataTableServiceUrl = serviceUrl + (isPredefinedProtocolsMode ? "/GetPredefinedProtocols" : "/GetApps");
        var _DataTableSelectionMode = isPredefinedProtocolsMode ? 'SINGLE' : 'MULTI';
        var _DataTableColumns = isPredefinedProtocolsMode ? PredefinedProtocolsColumns : ApplicationsModeColumns;

        var findRenderers = function (grouping) {
            var renderers = null, noop = function (v) { return v; };

            $.each(_GroupByOptions, function () {
                if (this[1] != grouping) return true;
                renderers = { value: this[2][0] || noop, count: this[2][1] || noop };
                return false;
            });
            return renderers || { value: noop, count: noop };
        };

        var gridFilters;
        var groupByUnknown = '@{R=Core.Strings;K=WEBDATA_PS0_0;E=js}';
        var renderGroup = function (value, meta, record) {
            var txt = $.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value)) || groupByUnknown;
            var renderers = findRenderers(gridFilters.getGrouping());
            txt = renderers.value(Ext4.util.Format.htmlEncode(txt)) + renderers.count(record.data.Count);

            return String.format('<a href="javascript:void(0)" value="{0}">{1}</a>', value, txt);
        };

        var groupByStore = Ext4.create('Ext.data.Store', {
            fields: [
                { name: 'Value' },
                { name: 'Count' }
            ],
            proxy: {
                type: 'ajax',
                url: _GroupByServiceUrl,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                actionMethods: { read: 'GET' },
                reader: { type: 'json', root: 'd.Items', totalProperty: 'd.ResultCount', successProperty: 'd.Success', idProperty: 'index' },
                writer: { type: 'json' },
                pageParam: undefined
            },
            remoteSort: true,
            autoLoad: false,
            sorters: [{ property: 'Value', direction: 'asc' }],
            listeners: {
                load: function () {
                    var grid = Ext4.getCmp('groupingGrid'),
                        groupby = gridFilters.getGroupBy(),
                        row = this.findRecord('Value', groupby);

                    if (grid && row && groupby) {
                        var sm = grid.getSelectionModel();
                        sm.select(row, false, true);
                    }
                }
            }
        });

        var dataStore = Ext4.create('Ext4.data.Store', {
            baseParams: {
                start: 0,
                limit: parseInt(itemsPerPage)
            },
            pageSize: itemsPerPage,
            fields: [
                { name: 'ApplicationID' },
                { name: 'Name' },
                { name: 'Description' },
                { name: 'Status' },
                { name: 'CategoryID' },
                { name: 'Category' },
                { name: 'RiskLevel' },
                { name: 'ProductivityRating' },
                { name: 'NodeCount' },
                { name: 'DiscoveryMode' },
                { name: 'LastDiscoveryDate' },
                { name: 'LastDiscoveryProbe' }
            ],
            proxy: {
                type: 'ajax',
                url: _DataTableServiceUrl,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                actionMethods: { read: 'GET' },
                reader: { type: 'json', root: 'd.Items', totalProperty: 'd.ResultCount', successProperty: 'd.Success', idProperty: 'ApplicationID' },
                writer: { type: 'json' },
                pageParam: undefined
            },
            remoteSort: true,
            remoteFilter: true,
            autoLoad: true,
            sorters: [{ property: 'Name', direction: 'asc' }],
            filters: [{}]
        });

        gridFilters = (function (gbs, ds) {
            var _grouping = null, _groupBy = null, _groupByStore = gbs;
            var _search = null, _dataStore = ds;

            var validate = function (query) {
                if (query === undefined || query === null)
                    return null;

                // convert filter string to SQL format (i.e. replace * with %)
                query = query.replace(/\*/g, "%");

                // check also against localized value for 'hint' text
                return (query == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") ? null : query;
            };

            var setFilter = function (store) {
                store.remoteFilter = false;
                store.clearFilter();
                store.remoteFilter = true;

                if (_search) store.filters.add({ property: 'Search', value: _search });
                if (_groupBy) store.filters.add({ property: 'GroupBy', value: _groupBy });
                if (_grouping) store.filters.add({ property: 'Grouping', value: _grouping });

                // add at least one filter (for service)
                if (store.filters.length == 0)
                    store.filters.add({ property: '', value: '' });
            };

            return {
                getGrouping: function () { return _grouping; },
                getGroupBy: function () { return _groupBy; },
                setGrouping: function (query, o) {
                    query = validate(query);
                    var wasGrouping = (_groupBy != null);
                    if (query != _grouping) {
                        _grouping = query;
                        _groupBy = null;
                        setFilter(_groupByStore);
                        _groupByStore.load();

                        if (wasGrouping) {
                            setFilter(_dataStore);
                            _dataStore.load(o || {});
                        }
                    }
                },
                setGroupBy: function (query, o) {
                    query = validate(query);
                    if (query != _groupBy) {
                        _groupBy = query;
                        setFilter(_dataStore);
                        _dataStore.load(o || {});
                    }
                },
                setSearch: function (query, o) {
                    query = validate(query);
                    if (query != _search) {
                        _search = query;

                        // refresh grouping & data grids
                        setFilter(_groupByStore);
                        _groupByStore.load();

                        setFilter(_dataStore);
                        _dataStore.load(o || {});
                    }
                }
            };
        })(groupByStore, dataStore);

        Ext4.define('Ext.ux.form.SearchField', {
            extend: 'Ext.form.field.Trigger',
            alias: 'widget.searchfield',
            cls: 'dpi_grid_search_field',
            trigger1Cls: Ext4.baseCSSPrefix + 'form-clear-trigger',
            trigger2Cls: Ext4.baseCSSPrefix + 'form-search-trigger',
            emptyText: '@{R=Core.Strings;K=WEBJS_TM0_53;E=js}',
            validationEvent: false,
            validateOnBlur: false,
            width: 180,
            onTrigger1Click: function () {
                this.setValue('');
                this.onTrigger2Click();
            },
            onTrigger2Click: function () {
                var val = this.getRawValue();
                gridFilters.setSearch(val, getDataGridOptions());
            },
            initComponent: function () {
                this.callParent(arguments);

                this.on('specialkey', function (f, e) {
                    if (e.getKey() == e.ENTER) {
                        this.onTrigger2Click();
                    }
                }, this);

                this.on('focus', function (f, e) {
                    if (this.getRawValue() == '') {
                        this.setValue('');
                    }
                }, this);

                var timeout = null;
                this.on('change', function (f, e) {
                    var scope = this;
                    if (timeout) clearTimeout(timeout);
                    timeout = setTimeout(function () { scope.onTrigger2Click.apply(scope, []); }, 250);
                }, this);
            },
            afterRender: function () {
                this.callParent();

                // need to hide the clear trigger button for IE since they add their own to all text inputs
                if (navigator.appVersion.indexOf("MSIE") != -1) {
                    this.triggerEl.elements[0].remove();
                }
            }
        });

        btnAdd = Ext4.create('Ext.button.Button', { id: 'btnAdd', text: '@{R=DPI.Strings;K=ManageApps_AddNew;E=js}', icon: '/Orion/images/add_16x16.gif', handler: addApp });
        btnEdit = Ext4.create('Ext.button.Button', { id: 'btnEdit', text: '@{R=DPI.Strings;K=ManageApps_Edit;E=js}', icon: '/Orion/images/edit_16x16.gif', handler: editSelectedApp });
        btnEnable = Ext4.create('Ext.button.Button', { id: 'btnEnable', text: '@{R=DPI.Strings;K=ManageApps_Enable;E=js}', icon: '/Orion/images/enable_monitoring.gif', handler: enableSelected });
        btnDisable = Ext4.create('Ext.button.Button', { id: 'btnDisable', text: '@{R=DPI.Strings;K=ManageApps_Disable;E=js}', icon: '/Orion/images/disable_monitoring.gif', handler: disableSelected });
        btnDelete = Ext4.create('Ext.button.Button', { id: 'btnDelete', text: '@{R=DPI.Strings;K=ManageApps_Delete;E=js}', icon: '/Orion/images/nodemgmt_art/icons/icon_delete.gif', handler: deleteSelectedApps });
        btnToggle = Ext4.create('Ext.button.Button', { id: 'btnToggle', text: toggleLabel, icon: toggleIcon, handler: togglePredefinedApps });
        btnEnableDiscovery = Ext4.create('Ext.button.Button', { id: 'btnEnableDiscovery', text: '@{R=DPI.Strings;K=ManageApps_EnableDiscovery;E=js}', icon: '/Orion/images/enable_monitoring.gif', handler: enableSelectedDiscovery });
        btnDisableDiscovery = Ext4.create('Ext.button.Button', { id: 'btnDisableDiscovery', text: '@{R=DPI.Strings;K=ManageApps_DisableDiscovery;E=js}', icon: '/Orion/images/disable_monitoring.gif', handler: disableSelectedDiscovery });

        Ext4.tip.QuickTipManager.init();

        if (SW.DPI.IsDemo) {
            btnEnable.disabled = true;
            btnDisable.disabled = true;
            btnDelete.disabled = true;
            btnEnable.tooltip = "@{R=DPI.Strings;K=ManageApps_ActionNotAllowedTip;E=js}";
            btnDisable.tooltip = "@{R=DPI.Strings;K=ManageApps_ActionNotAllowedTip;E=js}";
            btnDelete.tooltip = "@{R=DPI.Strings;K=ManageApps_ActionNotAllowedTip;E=js}";
        }

        var topToolbarSingleSelect = Ext4.create('Ext.toolbar.Toolbar', {
            id: 'topToolbar',
            border: 0,
            items: ['->', ' ', { xtype: 'searchfield' }]
        });

        var topToolbar = Ext4.create('Ext.toolbar.Toolbar', {
            id: 'topToolbar',
            border: 0,
            items: [btnAdd, '-', btnEdit, '-', btnEnable, '-', btnDisable, '-', btnEnableDiscovery, '-', btnDisableDiscovery, '-', btnDelete, '->', btnToggle, ' ', '-', ' ', { xtype: 'searchfield' }]
        });
        var _DataTableToolBar = isPredefinedProtocolsMode ? topToolbarSingleSelect : topToolbar;

        var pagingToolbar = Ext4.create('Ext.toolbar.Paging', {
            store: dataStore,
            border: 0,
            displayInfo: true,
            displayMsg: '@{R=DPI.Strings;K=ManageApps_DisplayingItems;E=js}',
            beforePageText: '@{R=DPI.Strings;K=Web_PagingBeforePageText;E=js}',
            afterPageText: '@{R=DPI.Strings;K=Web_PagingAfterPageText;E=js}',
            emptyMsg: '@{R=DPI.Strings;K=ManageApps_NoAppsToDisplayText;E=js}'
        });

        grid = Ext4.create('Ext.grid.GridPanel', {
            cls: 'sw-dpi-grid',
            region: 'center',
            store: dataStore,
            border: 0,
            stripeRows: true,
            selModel: Ext4.create('Ext.selection.CheckboxModel', {
                showHeaderCheckbox: false,
                mode: _DataTableSelectionMode,
                listeners: {
                    selectionchange: onSelectionChange
                }
            }),
            scroll: false,
            viewConfig: {
                style: { overflow: 'auto', overflowX: 'hidden' },
                getRowClass: function (record, index) {
                    if (record.data.Status === false) {
                        return 'disabled-row';
                    }
                }
            },
            columns: _DataTableColumns,
            tbar: _DataTableToolBar,
            bbar: pagingToolbar
        });

        var mainGridPanel = Ext4.create('Ext.Panel', {
            renderTo: 'ManageAppsGrid',
            region: 'center',
            split: true,
            minWidth: gridWidth,
            height: gridHeight,
            layout: 'border',
            collapsible: false,
            items: [{
                xtype: 'panel',     // left panel
                region: 'west',
                split: true,
                anchor: '0 0',
                width: 200,
                minWidth: 170,
                maxWidth: 420,
                border: 0,
                collapsible: false,
                viewConfig: { forceFit: true },
                cls: 'dpi-group-panel',
                items: [{
                    // GroupBy panel (top combo)
                    xtype: 'panel',
                    region: 'north',
                    split: false,
                    height: ((isPredefinedProtocolsMode) ? 37 : 39),
                    border: 0,
                    margin: '5 10 5 10',
                    cls: 'dpi-group-panel',
                    collapsible: false,
                    items: [{
                        xtype: 'label',
                        text: '@{R=Core.Strings;K=WEBJS_AK0_76; E=js}'
                    }, {
                        xtype: 'combobox',
                        id: 'GroupByCombo',
                        queryMode: 'local',
                        displayField: 'Name',
                        valueField: 'Value',
                        store: Ext4.create('Ext.data.SimpleStore', {
                            fields: ['Name', 'Value'],
                            data: _GroupByOptions
                        }),
                        triggerAction: 'all',
                        value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                        autoSelect: true,
                        typeAhead: true,
                        forceSelection: true,
                        selectOnFocus: false,
                        multiSelect: false,
                        editable: false,
                        listeners: {
                            'select': function (combo, sel, opt) {
                                var val = (sel && sel.length > 0) ? sel[0].data.Value : null;
                                gridFilters.setGrouping(val, getDataGridOptions());
                                pagingToolbar.moveFirst();
                            }
                        }
                    }]
                }, {
                    // GroupBy panel (content)
                    xtype: 'gridpanel',
                    id: 'groupingGrid',
                    region: 'center',
                    store: groupByStore,
                    cls: 'hide-header',
                    border: 0,
                    columns: [{
                        flex: 1,
                        editable: false,
                        sortable: false,
                        dataIndex: 'Value',
                        renderer: renderGroup
                    }],
                    autoScroll: true,
                    loadMask: true,
                    listeners: {
                        cellclick: function (g, row, cell, e) {
                            var query = e.data.Value != groupByUnknown ? e.data.Value : null;
                            gridFilters.setGroupBy(query, getDataGridOptions());
                            pagingToolbar.moveFirst();
                        }
                    },
                    anchor: '0 0',
                    height: '100%',
                    layout: 'fit',
                    viewConfig: { forceFit: true },
                    split: false,
                    autoExpandColumn: 'Value'
                }]
            },
                grid
            ],
            cls: 'no-border'
        });

        // select first value in GroupBy combo box
        var combo = Ext4.getCmp('GroupByCombo');
        combo.setValue(combo.getStore().getAt(0));

        function initGrid() {
            var styleOverrides = "#gridCell div#Grid { width: 100%; }  #adminContent td { padding: 0px 0px 0px 0px !important; vertical-align: middle; }  .hide-header .x4-grid-header-ct { display: none; }  .dpi-group-panel { background: #eeeeee !important; }  .dpi-group-panel>:first-child { background: #eeeeee !important; }";

            if (isPredefinedProtocolsMode) {
                // use radio buttons instead of checkboxes
                var singleSelectStyles = ".x4-column-header-checkbox {display: none;}  .x4-reset .x4-grid-row-selected .x4-grid-row-checker, .x4-reset .x4-grid-row-checked .x4-grid-row-checker{background-image:url('/Orion/DPI/images/checked_radio.gif'); }  .x4-reset .x4-grid-row-checker { background-image:url('/Orion/DPI/images/unchecked_radio.gif'); }";
                styleOverrides += singleSelectStyles;
            }

            $('<style type="text/css">' + styleOverrides + '</style>').appendTo("head");

            onSelectionChange(true);

            // DPI-1614 : needed to work around scrolling issue that was fixed in EXTJS 4.2
            $(window).scroll(function () {
                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {
                    position = $('body').scrollTop();
                }, 200));
            });
        }

        $(document).ready(function () {
            initGrid();
        });
    }
})(SW.DPI.ManageApps);