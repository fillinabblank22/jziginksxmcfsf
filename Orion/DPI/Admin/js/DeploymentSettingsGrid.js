﻿Ext.namespace('SW');
Ext.namespace('SW.Orion');
Ext.namespace('SW.Orion.DPI');

SW.Orion.DPI.DeploymentSettingsGrid = function () {
    var TestResultEnum =
    {
        Success: 0,
        CredentialTestFailure: 1,
        OsCheckFailure: 2,
        GeneralFailure: 3
    };

    var errorTitle = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_ErrorTitle;E=js}';
    var assignCredentialsButtonText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_AssignCredentialsButtonText;E=js}';
    var testCredentialsButtonText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_TestCredentialsButtonText;E=js}';
    var unassignedText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_Unassigned;E=js}';
    var testingCredentialsText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_TestingCredentials;E=js}';
    var notTestedText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_NotTested;E=js}';
    var deployFailedText = '@{R=DPI.Strings;K=Web_ManageProbes_DeployingProbe_Error;E=js}';
    var testSuccessfulText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_CredentialTestSuccessful;E=js}';
    var testFailedText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_CredentialTestFailed;E=js}';
    var testFailedHelpLinkText = '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_CredentialTestFailedHelpLinkText;E=js}';
    var testFailedHelpKB = "http://www.solarwinds.com/documentation/kbloader.aspx?kb=5574&lang=@{R=Core.Strings;K=CurrentHelpLanguage;E=js}";
    var testOSFailedHelpText = "@{R=DPI.Strings;K=Web_ManageProbes_DeployingProbe_TestRequirements_Failed;E=js}";
    var credentialsNotApplicableText = "@{R=DPI.Strings;K=Web_ManageProbes_DeployingProbe_CredentialsNotApplicable;E=js}";
    var initialized;
    var panel;
    var grid;
    var selectorModel;
    var dataStore;
    var loadingMask = null;
    var assignCredentialWindow;
    var credentialsCombo;
    var credentialsList;
    var selectedCredential = -1;
    var usernameField;
    var passwordField;
    var nodeList;
    var updateCallback;

    var CheckExistingTestResult = function (itemId) {
        var testResult = $('#' + getCredentialTestResultId('popup' + itemId));
        if (testResult.length > 0) {
            return testResult.data("result");
        }
        return 0;
    };

    var AssignExistingCredentials = function (items) {
        Ext.each(items, function (item) {
            item.data.CredentialId = credentialsCombo.getValue();
            item.data.CredentialName = credentialsCombo.getRawValue();
            item.data.CredentialUsername = credentialsCombo.getRawValue();
            item.data.CredentialTestResult = CheckExistingTestResult(item.data.NodeId);
            item.commit();
        });
    };

    AssignNewCredentials = function (items, username, password) {
        Ext.each(items, function (item) {
            item.data.CredentialId = 0;
            item.data.CredentialName = username;
            item.data.CredentialUsername = username;
            item.data.CredentialPassword = password;
            item.data.CredentialTestResult = CheckExistingTestResult(item.data.NodeId);
            item.commit();
        });
    };

    var GetItemByNodeId = function (nodeId, items) {
        var resultItem = null;
        Ext.each(items, function (item) {
            if (item.data.NodeId == nodeId) {
                resultItem = item;
                return false;
            }
        });

        return resultItem;
    }

    var TestCredentials = function (items, credentialId, username, password, onFinish, onFailure) {
        var items2Test = [];
        var PluginRequirementTest = function (nodeId, engineId) {
            this.NodeId = nodeId;
            this.NodeEngineId = engineId;
            this.CredentialId = 0;
            this.UserName = null;
            this.Password = null;
        }

        Ext.each(items, function (item) {
            if ((item.data.AgentExists) || (item.data.CredentialUsername != null && item.data.CredentialUsername != '') || (item.data.CredentialId && item.data.CredentialId > 0)
                || (credentialId && credentialId > 0) || (username && password && username != '' && password != '')) {
                var item2Test = new PluginRequirementTest(item.data.NodeId, item.data.EngineId);

                if (credentialId && credentialId > 0) {
                    item2Test.CredentialId = credentialId;
                } else if (username && password && username != '' && password != '') {
                    item2Test.UserName = username;
                    item2Test.Password = password;
                } else if (item.data.CredentialId && item.data.CredentialId > 0) {
                    item2Test.CredentialId = item.data.CredentialId;
                } else {
                    item2Test.UserName = item.data.CredentialUsername;
                    item2Test.Password = item.data.CredentialPassword;
                }

                items2Test.push(item2Test);

                item.data.CredentialTestResult = 1; // test in progress
                item.commit();
            } else {
                item.data.CredentialTestResult = 3; // test failed -> no agent or no selected credential or no entered username and password
                item.data.CredentialTestMessage = '';
                item.commit();
            }
        });

        ORION.callWebService("/Orion/DPI/Services/DeploymentSettings.asmx",
            "TestPluginRequirementsOnMachine", { items2Test: items2Test },
            function (resultItems) {
                var allSucceded = true;
                Ext.each(resultItems, function (result) {
                    var item = GetItemByNodeId(result.NodeId, items);

                    if (item) {
                        if (result.PluginRequirementTestResult == TestResultEnum.Success) {
                            item.data.CredentialTestResult = 6; // validation passed
                        } else if (result.PluginRequirementTestResult == TestResultEnum.OsCheckFailure) {
                            item.data.CredentialTestResult = 7; // validation OS failed
                            allSucceded = false;
                        } else if (result.PluginRequirementTestResult == TestResultEnum.CredentialTestFailure ||
                            result.PluginRequirementTestResult == TestResultEnum.GeneralFailure) {
                            item.data.CredentialTestResult = 3; // credential check failed
                            allSucceded = false;
                        }

                        item.data.CredentialTestMessage = result.PluginRequirementTestResultMessage;
                        item.commit();
                    }
                }, function (err) {
                    item.data.CredentialTestResult = 5;
                    item.data.CredentialTestMessage = testOSFailedHelpText + err;
                    item.commit();
                    allSucceded = false;
                });

                if (allSucceded) {
                    if (typeof (onFinish) === "function") {
                        onFinish(resultItems);
                    }
                }
                else if (typeof (onFailure) === "function")
                    onFailure(resultItems);
            }, function (exceptionMessage) { // on failure
                // set fails
                Ext.each(items2Test, function (testedItem) {
                    var item = GetItemByNodeId(testedItem.NodeId, items);
                    if (item) {
                        item.data.CredentialTestResult = 5;
                        item.data.CredentialTestMessage = exceptionMessage;
                        item.commit();
                    }
                });

                if (typeof (onFailure) === "function")
                    onFailure([]);
            });
    };

    var TestUntestedCredentials = function (onSuccess, onFailure) {
        var items2Test = [];

        Ext.each(dataStore.data.items, function (item, index, array) {
            if (item.data.CredentialTestResult != 6) {
                items2Test.push(item);
            }
        });

        TestCredentials(items2Test, null, null, null, onSuccess, onFailure);
    };

    var TestCredentialsInPopupInternal = function (items) {
        if (!usernameField.isValid() || !passwordField.isValid())
            return;
        var username = usernameField.getValue();
        var password = passwordField.getValue();

        var testResults = $('#testResults');
        testResults.html('');
        testResults.show();

        Ext.each(items, function (item) {
            testResults.append(String.format('<li>{0}</li>', getCredentialTestResultMarkup('popup' + item.id, 1, '', item.data.Name)));
        });

        var onSuccessOrFailure = function (resultItems) {
            Ext.each(resultItems, function (item) {
                var nodeItem = GetItemByNodeId(item.NodeId, items);

                if (nodeItem) {
                    $('#' + getCredentialTestResultId('popup' + nodeItem.id)).remove();
                    testResults.append(getCredentialTestResultMarkup('popup' + nodeItem.id, nodeItem.data.CredentialTestResult, nodeItem.data.CredentialTestMessage, nodeItem.data.Name));
                }
            });
        }

        TestCredentials(items, selectedCredential, username, password, onSuccessOrFailure, onSuccessOrFailure);
    };

    var AssignCredentialsToSingleItem = function (itemId) {
        grid.getSelectionModel().clearSelections();

        var recordIndex = dataStore.findBy(function (record, id) {
            return record.data.NodeId == itemId;
        });

        grid.getSelectionModel().selectRow(recordIndex, true);

        assignCredentialWindow.show();
    };

    var UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var selectedItems = selItems.getSelections();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;

        map.AssignCredentials.setDisabled(true);
        map.TestCredentials.setDisabled(true);

        var anyNodeWithAgentSelected = false;
        Ext.each(selectedItems, function (item, index, array) {
            if (item.data.AgentExists) {
                anyNodeWithAgentSelected = true;
            }
        });

        if (count >= 1) {
            map.TestCredentials.setDisabled(false);
            if (!anyNodeWithAgentSelected)
                map.AssignCredentials.setDisabled(false);
        }
    };

    function renderDefault(value, meta, record) {
        return value;
    }

    function renderName(value, meta, record) {
        if (record.data.NodeId > 0) {
            return renderDefault(
                String.format('<img style="position:relative;top:4px;" src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={0}&amp;size=small" /> {1}', record.data.NodeStatus, value),
                meta, record);
        }
        return renderDefault(value, meta, record);
    }

    function renderCredentialsName(value, meta, record) {
        if (record.data.AgentExists) {
            return renderDefault(
                String.format('<div style="line-height:22px">{0}</div>', credentialsNotApplicableText),
                meta, record);
        }
        if (record.data.CredentialName == null || record.data.CredentialName == '') {
            return renderDefault(
                String.format('<div style="line-height:22px"><span class="deployment-settings-unassigned-credentials">{0}</span> ' +
                    '<span class="deployment-settings-helplink" onclick="SW.Orion.DPI.DeploymentSettingsGrid.AssignCredentials(\'{1}\');">' +
                    '{2}</span></div>',
                    unassignedText, record.data.NodeId, assignCredentialsButtonText), meta, record);
        }
        else {
            return renderDefault('<dev style="line-height:22px;">' + value + '</div>', meta, record);
        }
    }

    function renderCredentialsTestResult(value, meta, record) {
        return renderDefault(getCredentialTestResultMarkup(record.id, value, record.data.CredentialTestMessage), meta, record);
    }

    function renderProbeStatusMessage(value, meta, record) {
        if (record.data.ProbeId == -1) {
            return String.format('<div class="sw-suggestion sw-suggestion-fail" automation="fail"><span class="sw-suggestion-icon"></span><span>{0}</span></div>', record.data.ProbeStatusMessage);
        } else {
            return renderDefault(value, meta, record);
        }
    }

    function getCredentialTestResultId(itemId) {
        return "test-result-" + itemId;
    }

    function getCredentialTestResultMarkup(itemId, resultValue, resultMessage, itemName) {
        if (typeof (itemName) == 'undefined')
            itemName = '';
        else
            itemName = itemName + ' - ';

        if (resultValue == 0)
            return String.format('<div id="{0}" style="white-space: normal;" class="sw-suggestion sw-suggestion-warn" automation="warn" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}</div>', getCredentialTestResultId(itemId), itemName, notTestedText, resultValue);
        else if (resultValue == 1)
            return String.format('<div id="{0}" style="white-space: normal;" data-result="{3}"><img src="/Orion/images/loading_gen_small.gif" alt="loading..." />{1}{2}</div>', getCredentialTestResultId(itemId), itemName, testingCredentialsText, resultValue);
        else if (resultValue == 2)
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-pass" style="white-space: normal;border-color: #00A000;" automation="pass" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}</div>', getCredentialTestResultId(itemId), itemName, testSuccessfulText, resultValue);
        else if (resultValue == 5) // other failure. magic!
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" style="white-space: normal;border-color: #A00000;" automation="fail" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}<br/><span style="font-weight:normal;font-size:smaller;">{7}<br/><a href="{5}" class="AgentManagement_HelpLink">{4}</a></span></div>', getCredentialTestResultId(itemId), itemName, deployFailedText, $('<div/>').text(resultMessage).html(), testFailedHelpLinkText, testFailedHelpKB, resultValue, resultMessage);
        else if ((resultValue == 6) || (resultValue == 8)) // OS validation passed OR OS validation warning (from Agent data only)
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-pass" style="white-space: normal;border-color: #00A000;" automation="pass" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}</div>', getCredentialTestResultId(itemId), itemName, testSuccessfulText, resultValue);
        else if (resultValue == 7) // OS validation failed
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" style="white-space: normal;border-color: #A00000;" automation="fail" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}<br/><span style="font-weight:normal;font-size:smaller;">{7}<br/><a href="{5}" class="AgentManagement_HelpLink">{4}</a></span></div>', getCredentialTestResultId(itemId), itemName, deployFailedText, $('<div/>').text(resultMessage).html(), testOSFailedHelpText, testFailedHelpKB, resultValue, resultMessage);
        else
            return String.format('<div id="{0}" class="sw-suggestion sw-suggestion-fail" style="white-space: normal;border-color: #A00000;" automation="fail" data-result="{3}"><span class="sw-suggestion-icon"></span>{1}{2}<br/><span style="font-weight:normal;font-size:smaller;">{7}<br/><a href="{5}" class="AgentManagement_HelpLink">{4}</a></span></div>', getCredentialTestResultId(itemId), itemName, testFailedText, $('<div/>').text(resultMessage).html(), testFailedHelpLinkText, testFailedHelpKB, resultValue, resultMessage);
    }

    function textFieldsValidator(value) {
        if (selectedCredential != -1)
            return true;

        if (value == null || value == '')
            return '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_FieldMustNotBeEmpty;E=js}';

        return true;
    }

    var InitLayout = function (width) {
        panel = new Ext.Container({
            renderTo: 'localCollectorsGrid',
            width: width,
            height: 200,
            layout: 'fit',
            items: [grid]
        });
    };

    var InitGrid = function (showProbeResultColumn, additionalToolbarButtonsBefore, additionalToolbarButtonsAfter, compact) {
        showProbeResultColumn = showProbeResultColumn || false;
        selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on("selectionchange", function () {
            UpdateToolbarButtons();
        });

        dataStore = new Ext.data.JsonStore({
            fields: ['NodeId', 'Name', 'NodeStatus', 'CredentialId', 'CredentialName', 'CredentialUsername', 'CredentialPassword', 'CredentialTestResult', 'CredentialTestMessage', 'ProbeId', 'ProbeStatusMessage', 'AgentExists', 'EngineId'],
            data: nodeList,
            idProperty: 'NodeId',
        });

        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.addListener("update", function (store, record, operation) {
            if (typeof (updateCallback) === "function")
                updateCallback(record);
        });

        var widths = [300, 230, 360, 400];
        if (compact)
            widths = [150, 200, 290, 400];

        var config = {
            store: dataStore,
            columns: [
                selectorModel,
                { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                { header: '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_GridColumn_Node;E=js}', width: widths[0], hideable: false, sortable: true, dataIndex: 'Name', renderer: renderName },
                { header: '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_GridColumn_CredentialName;E=js}', width: widths[1], hideable: false, sortable: false, dataIndex: 'CredentialName', renderer: renderCredentialsName },
                { id: 'credential-test', header: '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_GridColumn_CredentialTest;E=js}', width: widths[2], hidden: showProbeResultColumn, hideable: false, sortable: true, dataIndex: 'CredentialTestResult', renderer: renderCredentialsTestResult },
                { id: 'probe-status', header: '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_GridColumn_ProbeStatus;E=js}', width: widths[3], hidden: !showProbeResultColumn, hideable: false, sortable: false, dataIndex: 'ProbeStatusMessage', renderer: renderProbeStatusMessage }
            ],
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            enableColumnHide: false,
            stripeRows: true,
            clicksToEdit: 1,
            autoExpandColumn: (showProbeResultColumn ? 'credential-test' : 'probe-status'),
            viewConfig: {
                deferEmptyText: false,
                emptyText: '@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_ThereAreNoAgents;E=js}'
            },
            tbar:
                additionalToolbarButtonsBefore.concat(
                    [
                        {
                            id: 'AssignCredentials',
                            text: assignCredentialsButtonText,
                            iconCls: 'deployment-settings-assign-credentials-button',
                            handler: function () {
                                selectedCredential = "-1";
                                credentialsCombo.setValue("-1");
                                assignCredentialWindow.show();
                            }
                        }, '-', {
                            id: 'TestCredentials',
                            text: testCredentialsButtonText,
                            iconCls: 'deployment-settings-test-credentials-button',
                            handler: function () {
                                TestCredentials(grid.getSelectionModel().getSelections());
                            }
                        }
                    ]).concat(additionalToolbarButtonsAfter)
        };

        grid = new Ext.grid.EditorGridPanel(config);
    };

    InitAssignCredentialWindow = function () {
        credentialsCombo = new Ext.form.ComboBox({
            store: new Ext.data.JsonStore({
                fields: ['Key', 'Value'],
                data: credentialsList
            }),
            valueField: 'Key',
            displayField: 'Value',
            triggerAction: 'all',
            mode: 'local',
            width: 230,
            editable: false,
            transform: 'CredentialNamesCombo',
            hiddenId: 'CredentialNamesCombo',
            tpl: '<tpl for="."><div class="x-combo-list-item">{Value:htmlEncode}</div></tpl>',
            listeners: {
                select: function (record, index) {
                    selectedCredential = index.data.Key;
                    usernameField.validate();
                    passwordField.validate();
                    if (selectedCredential == -1) {
                        $('#AssignCredentialWindow tr.sw-deployment-settings-hideable').show();
                    } else {
                        $('#AssignCredentialWindow tr.sw-deployment-settings-hideable').hide();
                    }

                    $('#testResults').empty();
                    assignCredentialWindow.syncShadow();
                }
            }
        });
        credentialsCombo.setValue(selectedCredential);

        usernameField = new Ext.form.TextField({
            applyTo: "CredentialUsername",
            width: 230,
            validator: textFieldsValidator
        });

        passwordField = new Ext.form.TextField({
            applyTo: "CredentialPassword",
            width: 230,
            validator: textFieldsValidator
        });

        $('#assignCredentialWindowSubmit').on('click', function () {
            if (selectedCredential != -1) { // new credential
                AssignExistingCredentials(grid.getSelectionModel().getSelections());
            } else { // existing credential
                if (!usernameField.isValid() || !passwordField.isValid())
                    return;

                var username = usernameField.getValue();
                var password = passwordField.getValue();

                AssignNewCredentials(grid.getSelectionModel().getSelections(), username, password);
            }

            assignCredentialWindow.hide();
            selectedCredential = -1;
        });

        $('#assignCredentialWindowCancel').on('click', function () {
            assignCredentialWindow.hide();
            selectedCredential = -1;
            usernameField.setValue('');
            passwordField.setValue('');
        });

        assignCredentialWindow = new Ext.Window({
            title: "@{R=DPI.Strings;K=DPINodeWizard_DeploymentSettings_AssignCredentialDialogTitle;E=js}",
            width: 400,
            autoHeight: true,
            closable: false,
            modal: true,
            resizable: false,

            items: [
                new Ext.Panel({
                    applyTo: 'AssignCredentialWindow',
                    border: false
                })
            ],
            listeners: {
                beforeshow: function () {
                    var assignWindow = $('#AssignCredentialWindow');

                    assignWindow.show();
                    assignWindow.closest('.x-window-bwrap').zIndex(assignWindow.closest('.x-window').zIndex());
                    $('#AssignCredentialWindow tr.sw-deployment-settings-hideable').show();

                    if (grid.getSelectionModel().getCount() != 1) {
                        credentialsCombo.setValue(-1);
                    } else {
                        var record = grid.getSelectionModel().getSelected();
                        if (record.data.CredentialId > 0) {
                            credentialsCombo.setValue(record.data.CredentialId);
                            $('#AssignCredentialWindow tr.sw-deployment-settings-hideable').hide();
                        } else {
                            credentialsCombo.setValue(-1);
                        }
                    }
                    usernameField.setValue('');
                    usernameField.clearInvalid();
                    passwordField.setValue('');
                    passwordField.clearInvalid();

                    var testResults = $('#testResults');
                    testResults.html('');
                    testResults.hide();
                },
                beforehide: function () {
                    var assignWindow = $('#AssignCredentialWindow');
                    assignWindow.closest('.x-window-bwrap').zIndex('auto');
                }
            }
        });
    };

    function GetViewportHeight() {
        var viewportheight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
        if (typeof window.innerWidth != 'undefined') {
            viewportheight = window.innerHeight;
        }
        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
        else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
            viewportheight = document.documentElement.clientHeight;
        }
        // older versions of IE
        else {
            viewportheight = document.getElementsByTagName('body')[0].clientHeight;
        }

        return viewportheight;
    };

    function GetManagementGridHeight() {
        var height = GetViewportHeight() - 410;
        if (height >= 200)
            return height;
        else
            return 200;
    };

    function RecalculcatePanelLayout(elementId, skipWidth, skipHeight) {
        if (!skipWidth) {
            panel.setWidth(1);
            panel.setWidth($('#' + elementId).width());
        }

        if (!skipHeight) {
            panel.setHeight(GetManagementGridHeight());
        }

        panel.doLayout();
    };

    ORION.prefix = "DPI_DeploymentSettingsGrid_";

    return {
        SetCredentialsList: function (list) {
            credentialsList = list;
        },
        SetNodeList: function (list) {
            nodeList = list;
        },
        TestCredentialsInPopup: function () {
            TestCredentialsInPopupInternal(grid.getSelectionModel().getSelections());
        },
        AssignCredentials: function (itemId) {
            AssignCredentialsToSingleItem(itemId);
        },
        HaveAnyCredential: function () {
            var data = dataStore.data.items;
            if (!data.length)
                return false;
            else
                return true;
        },
        CloseAssignCredentialsWindow: function () {
            assignCredentialWindow.hide();
            selectedCredential = -1;
            usernameField.setValue('');
            passwordField.setValue('');
        },
        AreAllCredentialsValid: function () {
            var data = dataStore.data.items;
            var valid = true;

            if (!data.length)
                return false;

            Ext.each(data, function (item, index, array) {
                if (item.data.CredentialTestResult != 6) {
                    valid = false;
                }
            });
            return valid;
        },
        OnUpdate: function (callback) {
            updateCallback = callback;
        },
        TestUntestedCredentials: TestUntestedCredentials,
        Clear: function () {
            dataStore.removeAll();

            if (typeof (updateCallback) === "function")
                updateCallback();
        },
        Add: function (row) {
            for (var i = 0; i < row.length; i++) {
                var nodeId = row[i].NodeId;
                var record = new dataStore.recordType(row[i], nodeId);
                ORION.callWebService("/Orion/DPI/Services/DeploymentSettings.asmx",
                    "GetAgentInfo", { nodeId: nodeId },
                    (function (record) {
                        return function (result) {
                            record.data.AgentExists = result.IsAgentInstalled;
                            record.data.EngineId = result.EngineId;

                            if (result.NodeWmiCredentialsId && result.NodeWmiCredentialsId != -1 &&
                                result.NodeWmiCredentialsName && result.NodeWmiCredentialsUserName) {
                                record.data.CredentialId = result.NodeWmiCredentialsId;
                                record.data.CredentialName = result.NodeWmiCredentialsName;
                                record.data.CredentialUsername = result.NodeWmiCredentialsUserName;
                            }
                            dataStore.add(record);
                        };
                    })(record));
            }

            if (typeof (updateCallback) === "function")
                updateCallback();
        },
        RemoveAllExcept: function (nodesNotToRemove) {
            var nodeIdsNotToRemove = [];
            var itemsToRemove = [];

            Ext.each(nodesNotToRemove, function (item) {
                nodeIdsNotToRemove.push(item.NodeId);
            });

            Ext.each(dataStore.data.items, function (item, index, array) {
                if ($.inArray(item.data.NodeId, nodeIdsNotToRemove) == -1)
                    itemsToRemove.push(item);
            });

            dataStore.remove(itemsToRemove);
        },
        SetErrorMessage: function (node, message) {
            var myNode = dataStore.getById(node.NodeId);
            myNode.data.CredentialTestResult = 5;
            myNode.data.CredentialTestMessage = message;
            myNode.commit();
        },
        RemoveSelected: function () {
            dataStore.remove(grid.getSelectionModel().getSelections());

            if (typeof (updateCallback) === "function")
                updateCallback();
        },
        GetIds: function () {
            var rv = [];
            dataStore.each(function (item) {
                rv.push(item.data.NodeId);
            });
            return rv;
        },
        GetResult: function () {
            var dataArray = [];
            var data = dataStore.data.items;
            Ext.each(data, function (item) {
                dataArray.push(item.data);
            });
            return dataArray;
        },
        SaveResult: function () {
            var dataArray = [];
            var data = dataStore.data.items;
            Ext.each(data, function (item) {
                dataArray.push(item.data);
            });
            $("#hResult").val(JSON.stringify(dataArray));
            return true;
        },
        Init: function (showProbeResultColumn, additionalToolbarButtonsBefore, additionalToolbarButtonsAfter, compact) {
            if (initialized)
                return;

            initialized = true;

            InitGrid(showProbeResultColumn, additionalToolbarButtonsBefore || [], additionalToolbarButtonsAfter || [], compact);
            InitLayout(compact ? 680 : 960);
            InitAssignCredentialWindow();

            UpdateToolbarButtons();
        }
    };
}();