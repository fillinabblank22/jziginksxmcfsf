SW.DPI = SW.DPI || {};
SW.DPI.ManagePages = SW.DPI.ManagePages || {};
SW.DPI.ManagePages.Renderers = SW.DPI.ManagePages.Renderers || {};

(function (r) {
    r.Percent = function (value) {
        if (value)
            return value + ' %';
        else
            return "@{R=DPI.Strings;K=Web_Manage_NotAvailable;E=js}";
    };

    r.ProbeUtilization = function (value) {
        if (value) {
            if (value >= 80) {
                var tooltip = "@{R=DPI.Strings;K=Web_ManageProbes_UtilizationWarningMessage; E=js}";
                return "<span title='" + tooltip + "' class='probeOverloaded'>" + value + "%</span>";
            }
            return value + '%';
        }
        else
            return "@{R=DPI.Strings;K=Web_Manage_NotAvailable;E=js}";
    };

    r.ProbeLicense = function (mode) {
        var probeMode = {
            LocalTraffic: 1,
            AllTraffic: 2
        };

        if (mode === probeMode.LocalTraffic)
            return "@{R=DPI.Strings;K=Web_ManageProbes_LocalTraffic;E=js}";
        else
            return "@{R=DPI.Strings;K=Web_ManageProbes_AllTraffic;E=js}";
    };

    r.NodeCount = function (count, o, e) {
        var updatedCount = count;

        if (parseInt(count) === 0) { // add special link when there are no nodes configured
            updatedCount = '@{R=DPI.Strings;K=Web_ManageProbes_NoNodesConfigured;E=js} &nbsp; <a href="/Orion/DPI/Admin/DPINode/ChooseDpiNode.aspx?probeId=' + e.raw.ProbeID + '"><span class="inline-link"> &raquo; @{R=DPI.Strings;K=Web_ManageProbes_AddNodeNow;E=js}</span></a>';
        }

        return updatedCount;
    };

    r.AgentStatus = function (status, o, e) {
        var updatedStatus = status;

        if (status === "@{R=Core.Strings;K=WEBJS_AK0_38;E=js}") { // Connected
            updatedStatus = '<img src="/Orion/images/ok_16x16.gif" />&nbsp; <span style="position: relative; top: -3px;">' + status + ' &nbsp; <a href="../../AgentManagement/Admin/EditAgent.aspx?id=' + e.raw.AgentID + '"><span class="inline-link"> &raquo; @{R=DPI.Strings;K=Web_ManageProbes_ManageAgent;E=js}</span></a></span>';
        }

        return updatedStatus;
    };

    r.ZeroNull = function (num) {
        if (num === 0)
            return '<span style="color:#808080">-</span>';
        else
            return num;
    };

    r.YesNo = function (value) {
        if (value)
            return "@{R=DPI.Strings;K=Web_Enabled_True;E=js}";
        else
            return "@{R=DPI.Strings;K=Web_Enabled_False;E=js}";
    }

    function preg_quote(str) {
        return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
    }

    function highlight(data, search) {
        return data.replace(new RegExp("(" + preg_quote(search) + ")", 'gi'), "<span style='background-color:#ffe89e;'>$1</span>");
    }

    r.AppName = function (name, o, e) {
        var term = $('.dpi_grid_search_field').find('[type="text"]').val(),
            n = (term) ? highlight(name, term) : name,
            title = e && e.data ? e.data.Description : '';
        return '<span title="' + title + '">' + escapeHtml(n) + '</span>';
    };

    r.AppEnabledToggle = function (isEnabled, o, e) {
        var ie7fix = (navigator.appVersion.indexOf("MSIE 7.") != -1) ? 'background-position: 1px 1px;' : '';
        var tip = (SW.DPI.IsDemo) ? '@{R=DPI.Strings;K=ManageApps_ActionNotAllowedTip;E=js}' : '';
        var appId = e && e.data ? parseInt(e.data.ApplicationID) : null,
            switchImg = '/Orion/DPI/images/app_enable_' + (isEnabled ? 'on' : 'off') + '.png';
        onClickFn = (appId && !SW.DPI.IsDemo) ? ' onclick="enableAppByID(' + appId + ',' + (isEnabled ? 'false' : 'true') + ');"' : '';

        return '<span title="' + tip + '" style="float: left; width: 46px; height:18px; cursor:pointer;border: 1px solid transparent;background: url(\'' +
            switchImg + '\');' + ie7fix + '" onmouseover="this.style.border=\'1px solid #cccccc\'" onmouseout="this.style.border=\'1px solid transparent\'"' +
            onClickFn + '>&nbsp;</span>';
    };

    r.DiscoveryModeToggle = function (discoveryMode, o, e) {
        var ie7fix = (navigator.appVersion.indexOf("MSIE 7.") != -1) ? 'background-position: 1px 1px;' : '';
        var tip = (SW.DPI.IsDemo) ? '@{R=DPI.Strings;K=ManageApps_ActionNotAllowedTip;E=js}' : '';
        var appId = e && e.data ? parseInt(e.data.ApplicationID) : null,
            switchImg = '/Orion/DPI/images/app_enable_' + (discoveryMode == 1 ? 'on' : 'off') + '.png';
        onClickFn = (appId && !SW.DPI.IsDemo) ? ' onclick="setDiscoveryModeByAppID(' + appId + ',' + (discoveryMode == 1 ? '0' : '1') + ');"' : '';

        return '<span title="' + tip + '" style="float: left; width: 46px; height:18px; cursor:pointer;border: 1px solid transparent;background: url(\'' +
            switchImg + '\');' + ie7fix + '" onmouseover="this.style.border=\'1px solid #cccccc\'" onmouseout="this.style.border=\'1px solid transparent\'"' +
            onClickFn + '>&nbsp;</span>';
    };

    r.AppRiskLevel = function (id) {
        var txt = '@{R=DPI.Strings;K=DPIApplicationWizard_Unknown;E=js}';

        if (id == 1) {
            txt = '@{R=DPI.Strings;K=RiskLevel_1;E=js}';
        }
        else if (id == 2) {
            txt = '@{R=DPI.Strings;K=RiskLevel_2;E=js}';
        }
        else if (id == 3) {
            txt = '@{R=DPI.Strings;K=RiskLevel_3;E=js}';
        }
        else if (id == 4) {
            txt = '@{R=DPI.Strings;K=RiskLevel_4;E=js}';
        }
        else if (id == 5) {
            txt = '@{R=DPI.Strings;K=RiskLevel_5;E=js}';
        }

        return '<span><span><img src="/Orion/DPI/images/app_risk_level_' + id + '.png"/></span><span style="position: relative; top: -3px; left: 8px;">' + txt + '</span></span>';
    };

    r.AppProductivityRating = function (id) {
        var txt = '@{R=DPI.Strings;K=DPIApplicationWizard_Unknown;E=js}';
        var ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Unknown;E=js}';

        if (id == 1) {
            txt = '@{R=DPI.Strings;K=ProductivityRating_1;E=js}';
            ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Productivity_Tip_1;E=js}';
        }
        else if (id == 2) {
            txt = '@{R=DPI.Strings;K=ProductivityRating_2;E=js}';
            ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Productivity_Tip_2;E=js}';
        }
        else if (id == 3) {
            txt = '@{R=DPI.Strings;K=ProductivityRating_3;E=js}';
            ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Productivity_Tip_3;E=js}';
        }
        else if (id == 4) {
            txt = '@{R=DPI.Strings;K=ProductivityRating_4;E=js}';
            ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Productivity_Tip_4;E=js}';
        }
        else if (id == 5) {
            txt = '@{R=DPI.Strings;K=ProductivityRating_5;E=js}';
            ttip = '@{R=DPI.Strings;K=DPIApplicationWizard_Productivity_Tip_5;E=js}';
        }

        return '<span title="' + ttip + '"><span><img src="/Orion/DPI/images/app_productivity_' + id + '.png"/></span><span style="position: relative; top: -3px; left: 8px;">' + txt + '</span></span>';
    };
})(SW.DPI.ManagePages.Renderers);