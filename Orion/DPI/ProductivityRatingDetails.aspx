﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductivityRatingDetails.aspx.cs" Inherits="Orion_DPI_ProductivityRatingDetails" MasterPageFile="~/Orion/View.master" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="dpi" Assembly="SolarWinds.DPI.Web" Namespace="SolarWinds.DPI.Web.UI" %>
<%@ Import Namespace="SolarWinds.DPI.Web.UI" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ViewPageTitle" runat="Server">
    <h1><%= string.Format(System.Globalization.CultureInfo.InvariantCulture, Resources.DPIWebContent.PageTitle_ProductivityRatingDetails, this.ProductivityNames.FirstOrDefault()) %></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <dpi:DpiDetailsResourceHost ID="ResourceHostControl1" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </dpi:DpiDetailsResourceHost>
</asp:Content>