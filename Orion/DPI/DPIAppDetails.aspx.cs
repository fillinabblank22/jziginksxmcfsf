﻿using System;
using System.Linq;
using SolarWinds.DPI.Web;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;
using SolarWinds.DPI.Common;

public partial class Orion_DPI_DPIAppDetails : OrionView, IDpiApplicationProvider
{
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        CategoryIds = new List<int>() { DpiApplication.CategoryID };
        ProductivityIds = new List<int>() { DpiApplication.ProductivityRating };
        RiskLevelIds = new List<int>() { DpiApplication.RiskLevel };

        this.Title = DpiApplication.Name;
    }

    public override string ViewType
    {
        get { return "DPIApplicationDetails"; }
    }

    public DpiApplication DpiApplication
    {
        get { return (DpiApplication)this.NetObject; }
    }

    public List<int> CategoryIds { get; private set; }
    public List<int> ProductivityIds { get; private set; }
    public List<int> RiskLevelIds { get; private set; }

    public IEnumerable<string> CategoryNames
    {
        get { return CategoryIds.Select(i => ApplicationCategoryCache.Values[i]); }
    }
    public IEnumerable<string> ProductivityNames
    {
        get { return ProductivityIds.Select(i => Constants.ProductivityRatings[i]); }
    }
    public IEnumerable<string> RiskLevelNames
    {
        get { return RiskLevelIds.Select(i => Constants.RiskLevels[i]); }
    }
}