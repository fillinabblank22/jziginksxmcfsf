﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="QoEStatsDashboardResource.ascx.cs"
    Inherits="Orion_DPI_Resources_Summary_QoEStatsDashboardResource" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="charting" Namespace="SolarWinds.Orion.Web.Charting.v2" Assembly="OrionWeb" %>
<orion:include runat="server" file="DPI/styles/TreeGridDashboardResource.css" />
<orion:include runat="server" file="DPI/js/Charts/SW.Core.Charts.dataFormatters.js" />
<orion:include runat="server" file="DPI/js/TreeGridDashboardResource.js" />
<charting:IncludeCharts runat="server" />
<orion:resourcewrapper id="ResourceWrapper" runat="server">
    <Content>
        <script type="text/javascript">
            $(function () {
                var thresholds = function(val, stat){
                    return {'1': 'warning', '2': 'critical'}[''+stat] || '';
                };
                var formatFixed = function (val, col, row, inp, out) {
                    out.cls = out.cls || {};
                    out.cls[col] = thresholds(val, inp['S'+col]);
                    return SW.Core.Charts.dataFormatters.empty(val, '', 1000); };
                var formatBytes = function (val, col, row, inp, out) {
                    out.cls = out.cls || {};
                    out.cls[col] = thresholds(val, inp['S'+col]);
                    return SW.Core.Charts.dataFormatters.bytes(val, {});
                };
                var formatTimes = function (val, col, row, inp, out) {
                    out.cls = out.cls || {};
                    out.cls[col] = thresholds(val, inp['S'+col]);
                    return SW.Core.Charts.dataFormatters.msec(val, {unit: '<%= Resources.DPIWebContent.DPIResources_milliseconds %>'}, 0);
                };
                var refresh = SW.DPI.Resources.TreeGridDashboardResource.init({
                    resource: '<%= Resource.ID %>',
                    entities: '<%= SolarWinds.DPI.Web.UI.TreeGridDashboardResourceKeys.Entity.MissionCriticalStats.Application %>|<%= SolarWinds.DPI.Web.UI.TreeGridDashboardResourceKeys.Entity.MissionCriticalStats.Node %>',
                    baseparams: {
                        <%= SolarWinds.DPI.Web.UI.TreeGridDashboardResourceKeys.ViewLimitationID %> : '<%= this.Resource.View.LimitationID %>'
                    },
                    maxdepth: 2,
                    timestamp: <%= this.TimeStamp %>,
                    timeframe: <%= this.TimeFrame %>,
                    expandLevel: <%= base.Resource.IsInReport ? "-1" : "null" %>,
                    sorting: {
                        attr: 'id',
                        ambient: 'name',
                        dir: 'ASC'
                    },
                    // data & formatters: [id, name, avgNRT, avgART, bytes, transactions]
                    columns: ['name', 'avgNRT', 'avgART', 'bytes', 'transactions'],
                    formatters: {id: null, name: null, avgNRT: formatTimes, avgART: formatTimes, bytes: formatBytes, transactions: formatFixed},
                    table: 'treegrid-<%= Resource.ID %>',
                    hdrs: '.header-row th',
                    rows: '.data-row',
                    tpl: {
                        table: 'tpl-table-<%= Resource.ID %>',
                        tree: 'tpl-tree-<%= Resource.ID %>',
                        leaf: 'tpl-leaf-<%= Resource.ID %>',
                        empty: 'tpl-empty-<%= Resource.ID %>',
                        failed: 'tpl-failed-<%= Resource.ID %>'
                    },
                    listeners: {
                        rendered: function(){
                            var el = $(this), cols = [2,3,4,5];
                            $.each(cols, function(){ el.find('td.col-'+this).find('.warning, .critical').setMaxWidth(); });
                        }
                    }
                });
                SW.Core.View.AddOnRefresh(refresh);
                refresh();
            });
        </script>
        <script id="tpl-empty-<%= Resource.ID %>" type="text/template">
            <tbody>
                <tr class="data-row"><td><div class="empty">
                    <span><img src="/orion/images/ok_16x16.gif"></span>
                    <span><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_Empty %></span>
                </div></td></tr>
            </tbody>
        </script>
        <script id="tpl-failed-<%= Resource.ID %>" type="text/template">
            <tbody>
                <tr class="data-row">
                    <td>
                    <table>
                        <tr>
                            <td><span style="margin-right: 5px"><img src="/orion/images/failed_16x16.gif"></span></td>
                            <td><span>{{ErrorMessage}}</span></td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </tbody>
        </script>
        <script id="tpl-table-<%= Resource.ID %>" type="text/template">
            <tbody>
                <tr class="header-row">
                    <th class="col-1 ReportHeader" id="name"><a href="javascript:;" onclick="return false;"><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_TreeColumn %></a></th>
                    <th class="col-2 ReportHeader" id="avgNRT"><a href="javascript:;" onclick="return false;"><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_AvgNRTColumn %></a></th>
                    <th class="col-3 ReportHeader" id="avgART"><a href="javascript:;" onclick="return false;"><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_AvgARTColumn %></a></th>
                    <th class="col-4 ReportHeader" id="bytes"><a href="javascript:;" onclick="return false;"><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_BytesColumn %></a></th>
                    <th class="col-5 ReportHeader" id="transactions"><a href="javascript:;" onclick="return false;"><%= Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_TransColumn %></a></th>
                </tr>
                {# _.each(ndata, function(row){ #}<tr class="data-row">
                    <td class="col-1"><div id="{{path}}&&{{ row.id }}" class="level-0"><span class="EllipsisText" title="{{ row.name }}">
<a class="CustomLink" href="{{ row.uri }}"><img class="dpi-app-icon"
    alt="<%= Resources.DPIWebContent.DPIResources_DPIApplication_IconAlt %>"
    src="/Orion/StatusIcon.ashx?entity=Orion.DPI.Applications&status={{ row.status }}&size=small">&nbsp; {{ row.name }}</a>
                    </span></div></td>
                    <td class="col-2"><div id="{{path}}&&{{ row.id }}" class="level-0"><span>{{ row.avgNRT }}</span></div></td>
                    <td class="col-3"><div id="{{path}}&&{{ row.id }}" class="level-0"><span>{{ row.avgART }}</span></div></td>
                    <td class="col-4"><div id="{{path}}&&{{ row.id }}" class="level-0"><span>{{ row.bytes }}</span></div></td>
                    <td class="col-5"><div id="{{path}}&&{{ row.id }}" class="level-0"><span>{{ row.transactions }}</span></div></td>
                </tr>{# }); #}
            </tbody>
        </script>
        <script id="tpl-tree-<%= Resource.ID %>" type="text/template">
            {# _.each(ndata, function(row){ #}<div id="{{path}}&&{{ row.id }}" class="level-{{level}}"><span>{{ row[column] }}</span></div>{# }); #}
        </script>
        <script id="tpl-leaf-<%= Resource.ID %>" type="text/template">
            {# _.each(ndata, function(row){ #}<div id="{{path}}&&{{ row.id }}" class="level-{{level}} leaf">
{# if(column == "name") { #}
<span><a class="CustomLink" href="{{ row.uri }}"><img class="StatusIcon"
    alt="<%= Resources.CoreWebContent.WEBDATA_VB0_14 %>"
    src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={{ row.status }}&id={{ row.id }}&cb=<%= Math.Floor(DateTime.Now.TimeOfDay.TotalSeconds) %>"
>{{ row[column] }}</a></span>
{# } else { #}
    <span class="{{ row.cls[column] }}">{{ row[column] }}</span>
{# } #}
            </div>{# }); #}
        </script>

        <div id="QoEAppsTable" runat="server">
            <table id="treegrid-<%= Resource.ID %>" class="Resource NeedsZebraStripes" cellspacing="0" cellpadding="2" border="0"></table>
        </div>

        <asp:ScriptManagerProxy ID="sm" runat="server">
            <Services>
                <asp:ServiceReference path="/Orion/Services/Information.asmx" />
            </Services>
        </asp:ScriptManagerProxy>
    </Content>
</orion:resourcewrapper>