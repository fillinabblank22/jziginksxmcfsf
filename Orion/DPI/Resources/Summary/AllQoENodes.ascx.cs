﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Resources;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, typeof(DPIWebContent), "DPIMetadataSearchTagsChartsValues_QoE")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_DPI_Resources_Summary_AllQoENodes : BaseResourceControl
{
    protected bool ExceedingThresholdOnly;
    protected bool ShowNodeColumn = true;
    protected bool ShowAppColumn = true;
    protected long TimeStamp;
    protected long TimeFrame;
    protected string DataFilter;

    private void SetDataFilter()
    {
        DataFilter = "1=1";
        var np = base.GetInterfaceInstance<INodeProvider>();
        if (np != null)
        {
            DataFilter = string.Format(CultureInfo.InvariantCulture, "aa.NodeID = {0}", np.Node.NodeID);
            ShowNodeColumn = false;
            return;
        }

        var ap = base.GetInterfaceInstance<IDpiApplicationProvider>();
        if (ap != null)
        {
            DataFilter = string.Format(CultureInfo.InvariantCulture, "aa.ApplicationID = {0}", ap.DpiApplication.ApplicationId);
            ShowAppColumn = false;
            return;
        }

        var pp = base.GetInterfaceInstance<IDpiProductivityRatingProvider>();
        var cp = base.GetInterfaceInstance<IDpiApplicationCategoryProvider>();
        var rp = base.GetInterfaceInstance<IDpiRiskLevelProvider>();

        var filters = new List<string>();
        if ((pp != null) && pp.ProductivityIds.Any())
            filters.Add(string.Format(CultureInfo.InvariantCulture, "a.ProductivityRating IN ({0})", string.Join(", ", pp.ProductivityIds)));
        if ((cp != null) && cp.CategoryIds.Any())
            filters.Add(string.Format(CultureInfo.InvariantCulture, "a.CategoryID IN ({0})", string.Join(", ", cp.CategoryIds)));
        if ((rp != null) && rp.RiskLevelIds.Any())
            filters.Add(string.Format(CultureInfo.InvariantCulture, "a.RiskLevel IN ({0})", string.Join(", ", rp.RiskLevelIds)));

        if (filters.Any())
            DataFilter = string.Join(" AND ", filters);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SortablePageableTable.UniqueClientID = Resource.ID;

        bool excdOnly;
        bool.TryParse(Resource.Properties[ResourcePropertyKeys.ShowExceedingThresholdsOnly], out excdOnly);
        ExceedingThresholdOnly = excdOnly;

        SetDataFilter();

        string periodName = Resource.Properties[ResourcePropertyKeys.TimePeriod];
        if (string.IsNullOrWhiteSpace(periodName))
            periodName = Periods.GetPeriod("LAST 24 HOURS").Name;

        DateTime periodBegin = DateTime.UtcNow;
        DateTime periodEnd = DateTime.UtcNow;
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

        // update if changed
        if (Resource.Properties[ResourcePropertyKeys.TimePeriod] != periodName)
            Resource.Properties[ResourcePropertyKeys.TimePeriod] = periodName;

        TimeStamp = periodEnd.Ticks;
        TimeFrame = (periodEnd.Ticks - periodBegin.Ticks);
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] { typeof(INodeProvider), typeof(IDpiApplicationProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.DPIWebContent.AllQoENodes_DefaultTitle; }
    }

    public override string SubTitle
    {
        get
        {
            string period = Resource.Properties[ResourcePropertyKeys.TimePeriod];
            if (!string.IsNullOrWhiteSpace(Resource.SubTitle) || (String.IsNullOrEmpty(period)))
                return Resource.SubTitle;

            return Periods.GetLocalizedPeriod(period);
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return @"/Orion/DPI/Controls/EditResourceControls/EditQoeNodes.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}