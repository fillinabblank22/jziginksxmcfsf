﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_DPI_Resources_Summary_GettingStarted" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<orion:include id="Include1" runat="server" file="DPI/styles/resources/GettingStarted.css" />

<orion:resourcewrapper runat="server" id="Wrapper" showeditbutton="False" showmanagebutton="True" cssclass="qoe-gs-bg">
    <HeaderButtons>
        <orion:LocalizableButton id="btnRemoveMe" CssClass="EditResourceButton" runat="server" OnClick="btnRemoveMe_OnClick" DisplayType="Resource" />
    </HeaderButtons>
    <Content>
        <div class="qoe-gs-content">
            <div class="qoe-gs-lightbulb">
                <span class="qoe-txt-default">
                    <%= DPIWebContent.DPIResources_GettingStarted_TextBlock1 %>
					 <a href="<%= HelpHelper.GetHelpUrl("OrionDPIIntroduction")  %>" target="_blank"><%=  DPIWebContent.DPIResources_GettingStarted_LearnMore %></a>
                </span>
            </div>

            <div class="qoe-gs-blue">
                <span clientidmode="static" id="qoe_gs_1" class="qoe-txt-header"><div class="qoe-chev-wrap"><i clientidmode="Static" id="chev1" class="qoe-chevron chev-down"></i></div>&nbsp;<%= DPIWebContent.DPIResources_GettingStarted_TextBlock2a %></span><br />
                <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock2b %></span>
            </div>
            <div clientidmode="Static" id="qoe_gs_block_1">
                <div class="qoe-gs-step">
                    <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock3 %></span>
                </div>
                <div class="qoe-gs-img"><img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("DPI","gs_image1.png") %>" /></div>
                <div class="qoe-gs-step">
                    <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock4 %></span>
                </div>
                <div class="qoe-gs-img"><img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("DPI","gs_image2.png") %>" /></div>
            </div>

            <div class="qoe-gs-blue">
                <span clientidmode="static" id="qoe_gs_2" class="qoe-txt-header"><div class="qoe-chev-wrap"><i clientidmode="Static" id="chev2" class="qoe-chevron chev-down"></i></div>&nbsp;<%= DPIWebContent.DPIResources_GettingStarted_TextBlock5a %></span><br />
                <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock5b %></span>
            </div>
            <div clientidmode="Static" id="qoe_gs_block_2">
                <div class="qoe-gs-step">
                    <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock6 %></span>
                </div>
                <div class="qoe-gs-img"><img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("DPI","gs_image3.png") %>" /></div>
                <div class="qoe-gs-step">
                    <span class="qoe-txt-default"><%= DPIWebContent.DPIResources_GettingStarted_TextBlock7 %></span>
                </div>
                <div class="qoe-gs-img"><img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("DPI","gs_image4.png") %>" /></div>
            </div>
        </div>
        <script>
            $("#qoe_gs_1").click(function () { $("#qoe_gs_block_1").slideToggle(300); $("#chev1").toggleClass("chev-down"); });
            $("#qoe_gs_2").click(function () { $("#qoe_gs_block_2").slideToggle(300); $("#chev2").toggleClass("chev-down"); });
            var qoe_gs_cc=0;$(".qoe-gs-lightbulb").click(function(){if(qoe_gs_cc++>100){$(".qoe-gs-bg").addClass("qoe-rb");$(".qoe-gs-bg .HeaderBar a").css("color", "#fff");}});
        </script>
    </Content>
</orion:resourcewrapper>