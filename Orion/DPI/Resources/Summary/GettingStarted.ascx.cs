﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_DPI_Resources_Summary_GettingStarted : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get
        {
            return Resources.DPIWebContent.DPIResources_GettingStarted_Title;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        btnRemoveMe.Text = Resources.DPIWebContent.DPIResources_GettingStarted_Remove;
    }

    public override string HelpLinkFragment
    {
        get
        {
            return null;
        }
    }

    protected void btnRemoveMe_OnClick(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }
}