﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllQoENodes.ascx.cs" Inherits="Orion_DPI_Resources_Summary_AllQoENodes" %>
<%@ Register TagPrefix="orion" TagName="SortablePageableTable" Src="~/Orion/NetPerfMon/Controls/SortablePageableTable.ascx" %>
<orion:Include runat="server" File="DPI/js/Charts/SW.Core.Charts.dataFormatters.js" />
<orion:Include runat="server" File="DPI/styles/Resources/TableResources.css" />
<orion:Include runat="server" File="DPI/js/EscapeHtml.js" />

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <content>
        <div class="allQoeNodes">
            <orion:SortablePageableTable ID="SortablePageableTable" runat="server" />

            <input type="hidden" id="OrderBy-<%= base.Resource.ID %>" value="Name" />
            <script type="text/javascript">
                $(function () {
                    var ColumnIds =
                    {
                        AppUrl: 0,
                        AppStatus: 1,
                        NodeUrl: 2,
                        NodeStatus: 3,
                        ARTStatus: 4,
                        NRTStatus: 5
                    };

                    var colorFormatter = function (value, status) {
                        if (value !== "") {
                            var cellStyle = "";
                            if (status == 1)
                                cellStyle = "DpiValueWarning";
                            else if (status == 2)
                                cellStyle = "DpiValueCritical";
                            return SW.Core.String.Format("<span class='{0}'>{1}</span>",
                                cellStyle,
                                SW.Core.Charts.dataFormatters.msec(value));
                        }
                        return "<%= Resources.DPIWebContent.AllQoENodes_NA %>";
                    }

                    SW.Core.Resources.ActiveAlerts.SortablePageableTable.initialize(
                        {
                            uniqueId: '<%= Resource.ID %>',
                            pageableDataTableProvider: function (pageIndex, pageSize, onSuccess, onError) {
                                var uniqueId = '<%= Resource.ID %>';
                                var currentOrderBy = $('#OrderBy-' + uniqueId).val();
                                SW.Core.Services.callController("/api/DPI_TableResource/GetQoENodes",
                                    {
                                        CurrentPageIndex: pageIndex,
                                        PageSize: pageSize,
                                        OrderByClause: currentOrderBy,
                                        ExceedingThresholdOnly: '<%= base.ExceedingThresholdOnly %>',
                                        DataFilter: '<%= base.DataFilter %>',
                                        TimeStamp: '<%= base.TimeStamp %>',
                                        TimeFrame: '<%= base.TimeFrame%>',
                                        ViewLimitationID: '<%= this.Resource.View.LimitationID %>'
                                    }, function (result) {
                                        onSuccess(result);
                                    }, function (errorResult) {
                                        onError(errorResult);
                                    });
                            },
                            initialPage: 0,
                            rowsPerPage: 10,
                            allowSort: true,
                            columnSettings: {
                        <% if (ShowNodeColumn)
                { %>
                                "NodeName": {
                                    caption: "<%= Resources.DPIWebContent.AllQoENodes_NodeColumn %>",
                                    isHtml: true,
                                    formatter: function (cellValue, rowArray, cellInfo) {

                                        return SW.Core.String.Format("<a href='{0}'><img src='/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={1}&size=small' alt='{2}' ><span class='DpiEllipsisText'>{2}</span></a>",
                                            rowArray[ColumnIds.NodeUrl],
                                            rowArray[ColumnIds.NodeStatus],
                                            escapeHtml(cellValue));
                                    }
                                },
                            <%}
                if (ShowAppColumn)
                { %>
                                "AppName": {
                                    caption: "<%= Resources.DPIWebContent.AllQoENodes_AppColumn %>",
                                    isHtml: true,
                                    formatter: function (cellValue, rowArray, cellInfo) {
                                        return SW.Core.String.Format("<a href='{0}'><img src='/Orion/StatusIcon.ashx?entity=Orion.DPI.Applications&status={1}&size=small' alt='{2}' ><span class='DpiEllipsisText'>{2}</span></a>",
                                            rowArray[ColumnIds.AppUrl],
                                            rowArray[ColumnIds.AppStatus],
                                            escapeHtml(cellValue));
                                    }
                                },
                             <%}%>
                                "ART": {
                                    caption: "<%= Resources.DPIWebContent.AllQoENodes_ARTColumn %>",
                                    isHtml: true,
                                    formatter: function (cellValue, rowArray, cellInfo) {
                                        return colorFormatter(cellValue, rowArray[ColumnIds.ARTStatus]);
                                    }
                                },
                                "NRT": {
                                    caption: "<%= Resources.DPIWebContent.AllQoENodes_NRTColumn %>",
                                    isHtml: true,
                                    formatter: function (cellValue, rowArray, cellInfo) {
                                        return colorFormatter(cellValue, rowArray[ColumnIds.NRTStatus]);
                                    }
                                }

                            }
                        });

                    var refresh = function () { SW.Core.Resources.ActiveAlerts.SortablePageableTable.refresh(<%= Resource.ID %>); };
                    SW.Core.View.AddOnRefresh(refresh, '<%= SortablePageableTable.ClientID %>');
                    refresh();
                });
            </script>
        </div>
    </content>
</orion:resourceWrapper>
