﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using i18nExternalized = Resources.DPIWebContent;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, typeof(i18nExternalized), "DPIMetadataSearchTagsChartsValues_QoE")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_DPI_Resources_Summary_QoEStatsDashboardResource : BaseResourceControl
{
    public string TimeStamp { get; private set; }
    public string TimeFrame { get; private set; }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_Title;
        }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

            // if subtitle is not specified subtitle is a period
            string period = Resource.Properties[ResourcePropertyKeys.TimePeriod];

            if (String.IsNullOrEmpty(period))
                return Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_Grouping;

            return string.Format("{0}, {1}", Periods.GetLocalizedPeriod(period), Resources.DPIWebContent.DPIResources_QoEStatsDashboardResource_Grouping);
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPIQoEApplicationStats";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.Ajax;
        }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/DPI/Controls/EditResourceControls/EditQoEStatsDashboardResource.ascx";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        ResourceWrapper.ManageButtonText = Resources.DPIWebContent.QoEStatsDashboardResource_ManageApplications;
        ResourceWrapper.ManageButtonTarget = "~/Orion/DPI/Admin/ManageApplications.aspx";
        ResourceWrapper.ShowManageButton = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin;

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        // read period property
        string periodName = Resource.Properties[ResourcePropertyKeys.TimePeriod];
        if (string.IsNullOrWhiteSpace(periodName))
            periodName = Periods.GetPeriod("LAST 24 HOURS").Name;

        // parse to begin-end pair
        DateTime periodBegin = DateTime.UtcNow;
        DateTime periodEnd = DateTime.UtcNow;
        Periods.Parse(ref periodName, ref periodBegin, ref periodEnd);

        // update if normalized (changed)
        if (Resource.Properties[ResourcePropertyKeys.TimePeriod] != periodName)
            Resource.Properties[ResourcePropertyKeys.TimePeriod] = periodName;

        // set internal values to render resource
        long time = DateTime.Now.Ticks;
        this.TimeStamp = periodEnd.Ticks.ToString();
        this.TimeFrame = (periodEnd.Ticks - periodBegin.Ticks).ToString();

        base.OnLoad(e);
    }

    private static bool isQoEAppConfigured()
    {
        using (var dbl = new SolarWinds.DPI.Common.DPIBusinessLayerProxy())
        {
            return dbl.GetApplicationCount() != 0;
        }
    }
}