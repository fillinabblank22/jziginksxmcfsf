﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.DPI.Web.UI.Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_DPI_Resources_ChartResource : DpiChartBase
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            return new[]
            {
                typeof(INodeProvider),
                typeof(IDpiApplicationProvider)
            };
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        var appProv = GetInterfaceInstance<IDpiApplicationProvider>();
        if (appProv != null)
            Resource.Properties[ResourcePropertyKeys.GroupBy] = ResourcePropertyKeys.GroupByNode;
        else
        {
            var nodeProv = GetInterfaceInstance<INodeProvider>();
            if (nodeProv != null)
                Resource.Properties[ResourcePropertyKeys.GroupBy] = ResourcePropertyKeys.GroupByApp;
        }
        base.OnLoad(e);
    }
}