﻿using SolarWinds.DPI.Web.UI.Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_DPI_Resources_SingleLineChartResource_Node : DpiChartBase
{
    protected override string NetObjectPrefix
    {
        //Node netobject
        get { return "N"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(INodeProvider) };
        }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            return new[]
            {
                typeof(INodeProvider),
                typeof(IMultiNodeProvider)
            };
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
}