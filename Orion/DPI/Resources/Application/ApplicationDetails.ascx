﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDetails.ascx.cs" Inherits="Orion_DPI_Resources_Application_ApplicationDetails" %>
<orion:include id="Include1" runat="server" file="DPI/styles/resources/ApplicationDetails.css" />

<orion:resourcewrapper runat="server" id="Wrapper">
    <Content>
        <table class="dpi-appdetails-table NeedsZebraStripes">
            <tr>
                <td class="dpi-appdetails-label" style="border-bottom: 1px solid #ffffff;"><%= Resources.DPIWebContent.DPILabel_Application %></td>
                <td class="dpi-appdetails-icon"><asp:Image ID="IconName" runat="server" /></td>
                <td class="dpi-appdetails-value"><asp:Label ID="AppName" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dpi-appdetails-label"><%= Resources.DPIWebContent.DPILabel_Category %></td>
                <td class="dpi-appdetails-icon">&nbsp;</td>
                <td class="dpi-appdetails-value"><asp:HyperLink ID="AppCategory" runat="server"></asp:HyperLink></td>
            </tr>
            <tr>
                <td class="dpi-appdetails-label"><%= Resources.DPIWebContent.DPILabel_RiskLevel %></td>
                <td class="dpi-appdetails-icon"><asp:Image ID="IconRisk" runat="server" /></td>
                <td class="dpi-appdetails-value"><asp:HyperLink ID="AppRisk" runat="server"></asp:HyperLink></td>
            </tr>
            <tr>
                <td class="dpi-appdetails-label"><%= Resources.DPIWebContent.DPILabel_ProductivityRating %></td>
                <td class="dpi-appdetails-icon"><span><svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="16" height="15"><rect rx="2" ry="2" fill="<%= SolarWinds.DPI.Common.Constants.ProductivityRatingsColors[app.ProductivityRating] %>" x="0" y="3" width="16" height="12"></rect></svg></span></td>
                <td class="dpi-appdetails-value"><asp:HyperLink ID="AppProductivity" runat="server"><asp:Label ID="AppProductivityLabel" runat="server"></asp:Label></asp:HyperLink></td>
            </tr>
        </table>
    </Content>
</orion:resourcewrapper>