using SolarWinds.DPI.Web;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web;
using i18nExternalized = Resources.DPIWebContent;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, typeof(i18nExternalized), "DPIMetadataSearchTagsChartsValues_QoE")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_DPI_Resources_Application_ApplicationDetails : BaseResourceControl
{
    public DpiApplication app;

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IDpiApplicationProvider) };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    public override string DisplayTitle
    {
        get { return Resources.DPIWebContent.DPIResources_ApplicationDetails_Title; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionDPIQoEApplicationDetails";
        }
    }

    protected override string DefaultTitle
    {
        get { return DisplayTitle; }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        var provider = GetInterfaceInstance<IDpiApplicationProvider>();

        if (provider != null)
        {
            app = provider.DpiApplication;
            InitValues();
        }

        Wrapper.ShowEditButton = false;
        Wrapper.ManageButtonText = Resources.DPIWebContent.DPILabel_Manage;
        Wrapper.ManageButtonTarget = "/Orion/DPI/Admin/ManageApplications.aspx";
        Wrapper.ShowManageButton = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin;
    }

    private void InitValues()
    {
        int appStatus = (int)app.Status;

        IconName.ImageUrl = String.Format("/Orion/StatusIcon.ashx?entity=Orion.DPI.Applications&status={0}&size=small", appStatus);
        AppName.Text = String.Format(
            "{0} &nbsp; - &nbsp; {1} {2}",
            HttpUtility.HtmlEncode(app.Name),
            Resources.DPIWebContent.DPIResources_ApplicationDetails_AppStatus,
            OrionObjectStatusHelper.GetLocalizedStatusText(appStatus).ToUpper()
            );

        AppCategory.Text = app.Category;
        AppCategory.NavigateUrl = String.Format("/Orion/DPI/ApplicationCategoryDetails.aspx?Category={0}", app.CategoryID);

        AppRisk.Text = SolarWinds.DPI.Common.Constants.RiskLevels[app.RiskLevel];
        AppRisk.NavigateUrl = String.Format("/Orion/DPI/RiskLevelDetails.aspx?RiskLevel={0}", app.RiskLevel);
        IconRisk.ImageUrl = String.Format("/Orion/DPI/images/app_risk_level_{0}.png", app.RiskLevel);

        AppProductivity.NavigateUrl = String.Format("/Orion/DPI/ProductivityRatingDetails.aspx?ProductivityRating={0}", app.ProductivityRating);
        AppProductivityLabel.Text = SolarWinds.DPI.Common.Constants.ProductivityRatings[app.ProductivityRating];
    }
}
