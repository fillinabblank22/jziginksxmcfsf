﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.DPI.Web.UI.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;
using System.Collections.Generic;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_DPI_Resources_SingleLineChartResource_Application : DpiChartBase
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(IDpiApplicationProvider) };
        }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            return new[]
            {
                typeof(IDpiApplicationProvider),
                typeof(IMultiApplicationProvider)
            };
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }
}