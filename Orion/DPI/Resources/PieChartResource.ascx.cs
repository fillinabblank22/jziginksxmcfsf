﻿using SolarWinds.DPI.Web.UI.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
public partial class Orion_DPI_Resources_PieChartResource : DpiChartBase
{
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    protected override void AddChartExportControl(System.Web.UI.Control resourceWrapperContents)
    {
        // There is no need for export button
    }
}