﻿using System;
using SolarWinds.DPI.Web;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.UI;
using System.Linq;

public partial class Orion_DPI_ProductivityRatingDetails : DpiDetailsView
{
    public override string ViewType
    {
        get { return "DPIProductivityRatingDetails"; }
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);

        this.Title = ProductivityNames.FirstOrDefault();
    }
}