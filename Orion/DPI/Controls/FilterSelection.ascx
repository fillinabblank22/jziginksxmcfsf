﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterSelection.ascx.cs" Inherits="Orion_DPI_Controls_FilterSelection" %>
<%@ Import Namespace="Resources" %>

<p>
    <%= DPIWebContent.FilterControl_CategoryHeader %><br />
    <asp:DropDownList runat="server" ID="ddlCategories" />
</p>
<p>
    <%= DPIWebContent.FilterControl_RiskLevelHeader %><br />
    <asp:DropDownList runat="server" ID="ddlRiskLevels" />
</p>
<p>
    <%= DPIWebContent.FilterControl_ProductivityRatingHeader %><br />
    <asp:DropDownList runat="server" ID="ddlProductivities" />
</p>