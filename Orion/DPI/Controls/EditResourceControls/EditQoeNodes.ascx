﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditQoeNodes.ascx.cs"
    Inherits="Orion_DPI_Controls_EditResourceControls_EditQoeNodes" %>

<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" Text="<%$ Resources: CoreWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="chbExceedingThresholdsOnly" runat="server" Text="<%$ Resources: DPIWebContent, AllQoENodes_Edit_ShowExcdThres %>" />
        </td>
    </tr>
</table>