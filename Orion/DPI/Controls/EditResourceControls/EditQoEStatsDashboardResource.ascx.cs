﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class EditResourceControls_EditQoEStatsDashboardResource : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (this.IsPostBack)
            return;

        InitTimePeriod();
    }

    private void InitTimePeriod()
    {
        foreach (var period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

        Period.Text = Periods.GetLocalizedPeriod(Resource.Properties[ResourcePropertyKeys.TimePeriod]);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();
            properties.Add(ResourcePropertyKeys.TimePeriod, Period.SelectedValue);
            return properties;
        }
    }
}