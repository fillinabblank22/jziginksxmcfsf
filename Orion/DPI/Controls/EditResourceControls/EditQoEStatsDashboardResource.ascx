﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditQoEStatsDashboardResource.ascx.cs"
    Inherits="EditResourceControls_EditQoEStatsDashboardResource" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/PeriodControl.ascx" %>
<table border="0">
    <tr>
        <td>
            <asp:Label runat="server" Text="<%$ Resources: CoreWebContent,WEBDATA_AK0_263 %>" Font-Bold="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList runat="server" ID="Period" />
        </td>
    </tr>
</table>