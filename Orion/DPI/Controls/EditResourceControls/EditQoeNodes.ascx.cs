﻿using System.Globalization;
using System.Web.UI.WebControls;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;

public partial class Orion_DPI_Controls_EditResourceControls_EditQoeNodes : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (this.IsPostBack)
            return;

        bool excdOnly;
        bool.TryParse(Resource.Properties[ResourcePropertyKeys.ShowExceedingThresholdsOnly], out excdOnly);

        chbExceedingThresholdsOnly.Checked = excdOnly;

        foreach (var period in Periods.GenerateSelectionList())
            Period.Items.Add(new ListItem(Periods.GetLocalizedPeriod(period), period));

        Period.Text = Periods.GetLocalizedPeriod(Resource.Properties[ResourcePropertyKeys.TimePeriod]);
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var props = new Dictionary<string, object>();
            props[ResourcePropertyKeys.ShowExceedingThresholdsOnly] =
                chbExceedingThresholdsOnly.Checked.ToString(CultureInfo.InvariantCulture);
            props[ResourcePropertyKeys.TimePeriod] = Period.SelectedValue;
            return props;
        }
    }
}