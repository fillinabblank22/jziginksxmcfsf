﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPieChartSettings.ascx.cs" Inherits="Orion_DPI_Controls_Charts_EditPieChartSettings" %>
<%@ Import Namespace="Resources" %>

<script>
    $(document).ready(function () {
        $(".sw-res-editor table tbody tr:nth-child(2) td > div:not(.chartSpecificSettings)").hide();
        $(".sw-res-editor input:text").css("width", 300);
        $(".chartSpecificSettings").css({ "margin": 0, "padding": 0, "background": "transparent" });
    });
</script>

<br />

<div class="dpiPieOptions">

    <b><%= DPIWebContent.EditPieChart_DataType %></b><br />
    <asp:DropDownList ID="PieDataField" runat="server" Width="300" CssClass="dpiDropdownSelect dpiPieOption1"></asp:DropDownList>
    <br />
    <br />

    <b><%= DPIWebContent.EditPieChart_Period %></b><br />
    <asp:DropDownList ID="PieDataRange" runat="server" Width="300" CssClass="dpiDropdownSelect dpiPieOption2" DataValueField="Key" DataTextField="Value">
    </asp:DropDownList>
    <br />
    <br />
</div>