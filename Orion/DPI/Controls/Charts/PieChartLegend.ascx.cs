﻿using SolarWinds.Orion.Web.Charting.v2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Controls_Charts_PieChartLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "dpi_chartLegendInitializer__" + legend.ClientID; } }
}