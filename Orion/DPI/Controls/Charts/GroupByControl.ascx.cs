﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.DPI.Web.UI;
using Resources;
using System.Collections.Generic;

public partial class Orion_DPI_Controls_Charts_GroupByControl : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var providers = this.GetEditPageProvidersParam().ToList();

        //Group by is not allowed on AppDetails and Node Details
        this.Visible = !providers.Contains("A") && !providers.Contains("N");

        var items = new[]
        {
            new ListItem(DPIWebContent.EditTopXXChart_GroupByNode, ResourcePropertyKeys.GroupByNode),
            new ListItem(DPIWebContent.EditTopXXChart_GroupByApplication, ResourcePropertyKeys.GroupByApp)
        };

        ddlGroupBy.Items.AddRange(items.OrderBy(l => l.Text).ToArray());
    }

    public void LoadFromResourceProperties(ResourceInfo resourceInfo)
    {
        ddlGroupBy.SelectedValue = resourceInfo.Properties[ResourcePropertyKeys.GroupBy];
    }

    public void SaveToResourceProperties(Dictionary<string, object> properties)
    {
        if (this.Visible)
            properties[ResourcePropertyKeys.GroupBy] = ddlGroupBy.SelectedValue;
    }
}