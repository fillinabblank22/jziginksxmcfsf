﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.DPI.Web.UI.Helpers;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Controls_Charts_EditPieChartSettings : BaseResourceEditControl, IChartEditorSettings
{
    protected override void OnInit(EventArgs e)
    {
        foreach (string s in SolarWinds.DPI.Common.Constants.PieDataSummaryFields)
        {
            PieDataField.Items.Add(new ListItem(SolarWinds.DPI.Common.Constants.EditFieldOptionLabels[s], s));
        }

        PieDataRange.DataSource = TimePeriodHelper.Options;
        PieDataRange.DataBind();

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Page.IsPostBack)
        {
            Page.Validate();
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return new Dictionary<string, object>();
        }
    }

    public void Initialize(ChartSettings settings, SolarWinds.Orion.Web.ResourceInfo resourceInfo, string netObjectId)
    {
        if (Resource.Properties.ContainsKey(ResourcePropertyKeys.DataField) && !string.IsNullOrEmpty(Resource.Properties[ResourcePropertyKeys.DataField]))
            PieDataField.SelectedValue = Resource.Properties[ResourcePropertyKeys.DataField];
        else
            PieDataField.SelectedIndex = 1;  // Data Volume

        if (Resource.Properties.ContainsKey(ResourcePropertyKeys.DataRange) && !string.IsNullOrEmpty(Resource.Properties[ResourcePropertyKeys.DataRange]))
            PieDataRange.SelectedValue = Resource.Properties[ResourcePropertyKeys.DataRange];
        else
            PieDataRange.SelectedIndex = 4;  // Last 24-Hours
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties[ResourcePropertyKeys.DataField] = PieDataField.SelectedValue;
        properties[ResourcePropertyKeys.DataRange] = PieDataRange.SelectedValue;
    }
}