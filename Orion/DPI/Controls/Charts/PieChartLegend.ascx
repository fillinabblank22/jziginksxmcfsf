﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PieChartLegend.ascx.cs" Inherits="Orion_DPI_Controls_Charts_PieChartLegend" %>
<orion:include id="Include1" runat="server" file="DPI/js/Charts/Charts.DPI.PieChartLegend.js" />

<script type="text/javascript">
	SW.Core.Charts.Legend.dpi_chartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
		$('#<%= legend.ClientID %>').empty();
	    SW.DPI.Charts.PieChartLegend.createLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
	};
</script>

<div id="DPIPieLegend" class="dpi_PieChartLegend" style="display: -ms-flexbox; -ms-flex-align: center; display: -moz-box; -moz-box-align: center; display: -webkit-box; -webkit-box-align: center; display: box; box-align: center;" runat="server">
    <table runat="server" id="legend" class="chartLegend"></table>
</div>