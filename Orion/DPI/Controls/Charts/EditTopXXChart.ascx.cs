﻿using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Controls_Charts_EditTopXXChart : BaseResourceEditControl, IChartEditorSettings
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (Page.IsPostBack)
        {
            Page.Validate();
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return new Dictionary<string, object>();
        }
    }

    public void Initialize(ChartSettings settings, SolarWinds.Orion.Web.ResourceInfo resourceInfo, string netObjectId)
    {
        if (Resource.Properties.ContainsKey(ResourcePropertyKeys.TopXXCount) && !string.IsNullOrEmpty(Resource.Properties[ResourcePropertyKeys.TopXXCount]))
            maxCountBox.Text = Resource.Properties[ResourcePropertyKeys.TopXXCount];
        else
            maxCountBox.Text = "10";

        FilterSelection.LoadFromResourceProperties(Resource);
        GroupByControl.LoadFromResourceProperties(Resource);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties[ResourcePropertyKeys.TopXXCount] = maxCountBox.Text;
        FilterSelection.SaveToResourceProperties(properties);
        GroupByControl.SaveToResourceProperties(properties);
    }
}