﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BottomTableLegend.ascx.cs" Inherits="Orion_DPI_Controls_Charts_BottomTableLegend" %>
<orion:include runat="server" file="DPI/js/Charts/Charts.DPI.BottomTableLegend.js" />
<orion:include runat="server" file="DPI/styles/ChartResources.css" />

<script type="text/javascript">
	SW.Core.Charts.Legend.dpi_chartLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
		$('#<%= legend.ClientID %>').empty();
		SW.DPI.Charts.BottomTableLegend.createLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
    };
</script>
<div runat="server" id="legend"></div>