﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopXXChart.ascx.cs" Inherits="Orion_DPI_Controls_Charts_EditTopXXChart" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/DPI/Controls/FilterSelection.ascx" TagPrefix="dpi" TagName="FilterSelection" %>
<%@ Register Src="~/Orion/DPI/Controls/Charts/GroupByControl.ascx" TagPrefix="dpi" TagName="GroupByControl" %>

<div>
    <b><%= DPIWebContent.TopXXChartEditor_NumberOfItems %></b><br />
    <asp:TextBox runat="server" ID="maxCountBox" Rows="1" Columns="30" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCountBox" Display="Dynamic"
        SetFocusOnError="true"><%= DPIWebContent.Validator_ValueNeeded %></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCountBox"
        Display="Dynamic" Type="Integer" MaximumValue="50" MinimumValue="1"
        SetFocusOnError="true"><%= DPIWebContent.Validator_ValueOutOfRange %></asp:RangeValidator>
</div>
<div>
    <dpi:GroupByControl runat="server" ID="GroupByControl" />
</div>
<div>
    <dpi:FilterSelection runat="server" ID="FilterSelection" />
</div>