﻿using Resources;

using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web;
using SolarWinds.DPI.Web.UI;
using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_DPI_Controls_FilterSelection : System.Web.UI.UserControl
{
    /// <summary>
    /// Currently only one ID is supported on UI
    /// </summary>
    public List<int> SelectedCategories
    {
        get
        {
            return GetSelectedIds(ddlCategories);
        }
        set
        {
            SetSelectedIds(ddlCategories, value);
        }
    }

    /// <summary>
    /// Currently only one ID is supported on UI
    /// </summary>
    public List<int> SelectedRiskLevels
    {
        get
        {
            return GetSelectedIds(ddlRiskLevels);
        }
        set
        {
            SetSelectedIds(ddlRiskLevels, value);
        }
    }

    /// <summary>
    /// Currently only one ID is supported on UI
    /// </summary>
    public List<int> SelectedProductivityRatings
    {
        get
        {
            return GetSelectedIds(ddlProductivities);
        }
        set
        {
            SetSelectedIds(ddlProductivities, value);
        }
    }

    private List<int> GetSelectedIds(DropDownList dropDownList)
    {
        int selectedValue;
        if (int.TryParse(dropDownList.SelectedValue, out selectedValue))
            return new List<int>() { selectedValue };
        else
            return new List<int>(0);
    }

    private void SetSelectedIds(DropDownList dropDownList, List<int> selectedIds)
    {
        if (selectedIds.Count > 1)
            throw new ArgumentException("Only one selected Id is supported");

        if (selectedIds.Count == 1)
            dropDownList.SelectedValue = selectedIds.Single().ToString();
        else
            dropDownList.SelectedValue = string.Empty;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.Visible = !this.GetEditPageProvidersParam().Any();
        ddlCategories.Items.Add(new ListItem(DPIWebContent.FilterControl_AllCategories, string.Empty));
        ddlRiskLevels.Items.Add(new ListItem(DPIWebContent.FilterControl_AllRiskLevels, string.Empty));
        ddlProductivities.Items.Add(new ListItem(DPIWebContent.FilterControl_AllProductivityRatings, string.Empty));
        ddlCategories.Items.AddRange(ApplicationCategoryCache.Values.Select(c => new ListItem(c.Value, c.Key.ToString())).ToArray());
        ddlRiskLevels.Items.AddRange(Constants.RiskLevels.Select(c => new ListItem(c.Value, c.Key.ToString())).ToArray());
        ddlProductivities.Items.AddRange(Constants.ProductivityRatings.Select(c => new ListItem(c.Value, c.Key.ToString())).ToArray());
    }

    public void LoadFromResourceProperties(ResourceInfo resourceInfo)
    {
        SelectedCategories = DetailViewsHelper.GetIdsFromProperties(resourceInfo, ResourcePropertyKeys.CategoryIds);
        SelectedRiskLevels = DetailViewsHelper.GetIdsFromProperties(resourceInfo, ResourcePropertyKeys.RiskLevelIds);
        SelectedProductivityRatings = DetailViewsHelper.GetIdsFromProperties(resourceInfo, ResourcePropertyKeys.ProductivityRatingIds);
    }

    public void SaveToResourceProperties(Dictionary<string, object> properties)
    {
        properties[ResourcePropertyKeys.CategoryIds] = string.Join(",", SelectedCategories);
        properties[ResourcePropertyKeys.RiskLevelIds] = string.Join(",", SelectedRiskLevels);
        properties[ResourcePropertyKeys.ProductivityRatingIds] = string.Join(",", SelectedProductivityRatings);
    }
}