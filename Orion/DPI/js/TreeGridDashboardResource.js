﻿SW.DPI = SW.DPI || {};
SW.DPI.Resources = SW.DPI.Resources || {};
SW.DPI.Resources.TreeGridDashboardResource = SW.DPI.Resources.TreeGridDashboardResource || {};
SW.DPI.Resources.GroupByFormatter = SW.DPI.Resources.GroupByFormatter || function (k, v) {
    return k == "Status" ? GetObjectStatusText('' + v) : '' + v;
};

$.fn.setMaxWidth = function () {
    var max = Math.max.apply(Math, this.map(function () { return $(this).width(); }).get());
    return this.each(function () { $(this).width(max); });
};

(function (dpi) {
    dpi.defaults = {
        url: '/Orion/DPI/Services/TreeGridDashboardData.asmx/QueryData',
        resource: undefined,
        timestamp: 0,
        timeframe: 0,
        entities: undefined,    // path of entities (prefixed with filters)
        ipath: [],              // initial path prefix (for default filters)
        opento: [],             // array of paths opento (autoexpand)
        maxdepth: 3,            // level for leafs
        isMaxDepth: function (p, o, off) {
            var offset = !isNaN(off) ? off : 0;
            var ipath = (o && o.ipath) ? o.ipath.length : 0;
            return (!o || o.maxdepth > (p || []).length + offset - ipath);
        },
        expandLevel: undefined, // [null, -1, int] no-expand; expand-all; expand to level;
        baseparams: {},
        sorting: {
            attr: 'id',
            ambient: undefined,
            column: undefined,
            dir: undefined
        },
        hdrs: '',               // filter for header cells
        rows: '',               // filter for data rows
        tpl: {
            table: undefined,   // treegrid main template
            tree: undefined,    // columns recursive template (wrap in container div element)
            leaf: undefined,    // columns last level template
            empty: undefined,   // template when there are no rows to be rendered
            failed: undefined   // template when request failed
        },
        columns: [],            // column mapping (index by 'hdrs' elements)
        formatters: undefined,  // collection of formatters for each data column
        expCls: 'expanded',     // class name for expanded nodes
        loadCls: 'loaded',      // class name for loaded nodes
        drillDownCls: 'customlink', // Add class to <a/> in cells to start working Href link.
        listeners: {
            rendering: undefined,
            rendered: undefined,
            loaded: undefined
        }
    };
    dpi.getFireEventFn = function (scope, opts) {
        return function (event, args) {
            var ev = opts.listeners[event];
            if (ev && $.isFunction(ev)) ev.apply(scope, args);
        };
    };

    dpi.formatData = function (json, formatters) {
        var format = function (val, col, row, inp, out) {
            return formatters && $.isFunction(formatters[col]) ? formatters[col](val, col, row, inp, out) : val;
        };
        // make named objects from row arrays
        var ndata = $.map(json.rows || [], function (row) {
            var d = {}; $.each(json.columns || [], function (i, col) { d[col] = row[i]; }); return d;
        });
        // formatting of values formatter(value, columnName, rowIndex, rawInputRowData, formattedResultRowData);
        return $.map(ndata, function (inp, row) {
            var out = {}; $.each(inp || {}, function (col, val) {
                out[col] = format(val, col, row, inp, out);
            }); return out;
        });
    };

    dpi.load = function (opts, path, success, error) {
        var _arresc = function (arr) {
            return $.map(arr || [], function (v) {
                return escape(v)
            });
        };
        var _params = {
            entities: _arresc(opts.entities.split('|')),
            path: _arresc(path),
            options: $.extend(true, {}, opts.baseparams, {
                timestamp: opts.timestamp,
                timeframe: opts.timeframe,
                ambientsort: opts.sorting.ambient,
                sort: opts.sorting.column,
                dir: (opts.sorting.dir === 'DESC' ? 'DESC' : 'ASC')
            })
        };
        $.ajax({
            type: "POST",
            url: opts.url,
            data: JSON.stringify(_params),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: this.getAjaxHandler(success, this, path, opts),
            error: this.getAjaxHandler(error, this, path, opts),
        });
    };

    dpi.getAjaxHandler = function (callback, scope, path, opts) {
        return function (data) {
            var d = data && data.d ? data.d : data,
                json = (typeof (d) !== "string") ? d : $.parseJSON(d);
            if (json.rows) json.ndata = scope.formatData(json, opts.formatters);
            if (callback) callback.apply(scope, [json, path, opts]);
        }
    };

    dpi.init = function (options) {
        var opts = $.extend(true, {}, dpi.defaults, options),
            ipath = opts.ipath || [];
        $.each(opts.tpl, function (n, id) {
            var el = $('#' + id), html = el ? el.html() : undefined;
            opts.tpl[n] = _.template(html || '');
        });

        var refresh = function () { dpi.load(opts, ipath, success, error); };
        var success = function (json, path, opts) {
            json.path = this.buildPathPrefix(path);
            this.getFireEventFn(this, opts)('loaded', [true, json]);

            // render table content
            var el = $('#' + opts.table),
                fireEvent = this.getFireEventFn(el, opts),
                tpl = json.rows.length > 0 ? opts.tpl.table : opts.tpl.empty;

            fireEvent('rendering', []);
            if (el && tpl) el.html(tpl(json));
            fireEvent('rendered', []);

            var scope = this,
                hdrs = el.find(opts.hdrs),
                rows = el.find(opts.rows);

            $.each(hdrs, function () {
                scope.bindSorting.apply(scope, [this, refresh, opts]);
            });

            // bind rows click toggling
            $.each(json.rows, function (i) {
                var p = $.merge($.merge([], ipath), [this[0]]);
                scope.bindOnClick(scope, p, rows[i], opts);
            });
        };
        var error = function (json, path, opts) {
            this.getFireEventFn(this, opts)('loaded', [false, json]);
            // show error text...
            var errorObj = { 'ErrorMessage': '' };

            try {
                var exceptionDetail = $.parseJSON(json.responseText);
                errorObj.ErrorMessage = exceptionDetail && exceptionDetail.Message ? exceptionDetail.Message : json.statusText;
            } catch (e) {
                errorObj.ErrorMessage = json.statusText;
            }

            var el = $('#' + opts.table),
                tpl = opts.tpl.failed;
            if (el && tpl) el.html(tpl(errorObj));
        };
        return refresh;
    };

    dpi.bindSorting = function (hEl, refresh, opts) {
        if (!hEl || !opts || !opts.sorting) return;
        var hdr = $(hEl), sort = opts.sorting,
            sortby = sort.column || sort.ambient || '',
            id = $(hEl).attr(sort.attr) || '';

        if (id == sortby) hdr.addClass('sortby').addClass(sort.dir.toLowerCase());

        hdr.bind('click', function () {
            opts.sorting.column = id;
            opts.sorting.dir = (id != sortby) || (opts.sorting.dir != 'ASC') ? 'ASC' : 'DESC';
            refresh();
        });
    };

    dpi.bindOnClick = function (scope, path, row, opts) {
        path = path || [];
        var cs = $(row).find("div[id='" + scope.buildPathPrefix(path) + "']");
        if (!cs || cs.length <= 0) return;

        var hasCustomLink = function (e) {
            var t = e.target, mb = (e.which === 2);
            if (t && t.tagName == 'A' && t.href && /customlink/gi.test(t.className)) {
                if (mb) window.open(t.href); else window.location.href = t.href;
                return true;
            }
            return false;
        };

        var onClick = opts.isMaxDepth(path, opts) ? function (e) {
            e.stopPropagation();
            if (!hasCustomLink(e))
                scope.OnRowClick.apply(scope, [path, row, this, cs, opts]);
            return false;
        } : function (e) {
            e.stopPropagation();
            hasCustomLink(e);
            return false;
        };
        cs.bind('click', onClick);

        // do auto expand of subtrees
        if (opts.expandLevel == -1 || opts.expandLevel >= path.length)
            scope.OnRowClick.apply(scope, [path, row, cs[0], cs, opts]);
    };

    dpi.OnRowClick = function (path, row, cell, cells, opts) {
        var scope = this, node = $(cells[0]),
            id = $(cell).attr('id'),
            path = scope.getPathFromId(id);

        if (!node || !id || !path)
            return;

        var doToggle = function (tgl) {
            var filter = "div[id^='" + scope.buildPathPrefix([]) + "']";
            $.each(cells, function () {
                var inner = $(this).children(filter);
                if (inner && inner.length > 0)
                    if (tgl) inner.show(); else inner.hide();
            });
        };

        var doLoad = function () {
            var success = function (json, path, opts) {
                path = path || [], offset = (opts && opts.ipath ? opts.ipath.length : 0);
                var cellTpl = opts.isMaxDepth(path, opts, 1) ? opts.tpl.tree : opts.tpl.leaf;
                // render content
                scope.renderInners(cells, cellTpl, $.extend(true, {}, opts, {
                    path: scope.buildPathPrefix(path),
                    level: path.length - offset,
                    rows: json.rows,
                    ndata: json.ndata
                }));

                // bind on-click
                $.each(json.rows, (function (scope) {
                    return function () {
                        var p = $.merge($.merge([], path), [this[0]]);
                        scope.bindOnClick(scope, p, row, opts);
                    };
                })(this));

                node.addClass(opts.loadCls);
            };
            var error = function () { };
            scope.load(opts, path, success, error);
        };

        if (node.hasClass(opts.expCls)) {
            node.removeClass(opts.expCls);
            doToggle(false);
        } else {
            node.addClass(opts.expCls);
            if (node.hasClass(opts.loadCls)) doToggle(true); else doLoad();
        }
    };

    var buildPath = function (path) {
        return $.map(path || [], function (p) { return escape(p) }).join('&&');
    };
    var parsePath = function (id, sep) {
        //var nums = (id || '').replace(/^path&&([\d-]+).*$/g, '$1');
        var path = (id || '').replace(/^path&&(.+).*$/g, '$1');

        // handle unknown 'root' categories
        if (path == 'path&&') return [''];

        return $.map(path.split('&&'), function (v) {
            var id = parseInt(v, 10), num = /^\d$/.test(v);
            return num && !isNaN(id) ? id : unescape(v);
        });
    };
    dpi.buildPathPrefix = function (path) {
        return (path || []).length ? 'path&&' + buildPath(path) : 'path';
    };

    dpi.getPathFromId = function (id) {
        return parsePath(id);
    };

    dpi.renderInners = function (cells, tpl, opt) {
        var el = $('#' + opt.table),
            fireEvent = this.getFireEventFn(el, opt);
        $.each(cells, function (i, cell) {
            var map = (opt.columns || [])[i],
                data = $.extend({ index: i, column: map }, opt),
                html = tpl(data);

            fireEvent('rendering', []);
            $(cell).append(html);
            fireEvent('rendered', []);
        });
    };
})(SW.DPI.Resources.TreeGridDashboardResource);