﻿SW.Core.Charts = SW.Core.Charts || {};
(function (charts) {
    // Let modules define their custom formatters before this script. Sometimes it may
    // be difficult to put module script after this script.
    charts.dataFormatters = charts.dataFormatters || {};

    DPI_innerFormat = function (value, orderMultiplier, orderUnits) {
        var currentOrderMultiplier = 1;
        for (var order = 0; order < orderUnits.length; order++) {
            var nextOrderMultiplier = currentOrderMultiplier * orderMultiplier;
            if (Math.abs(value) < nextOrderMultiplier) {
                return value.toFixed(2).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) + orderUnits[order];
            }

            value = value / orderMultiplier;
            currentOrderMultiplier = nextOrderMultiplier;
        }
        return value;
    };

    charts.dataFormatters.msec = function (value) {
        return DPI_innerFormat(value, 1000, [' @{R=DPI.Strings;K=Web_Charts_DataFormatters_Msec_Ms;E=js}', ' @{R=DPI.Strings;K=Web_Charts_DataFormatters_Msec_S;E=js}']);
    };
})(SW.Core.Charts);