﻿SW.DPI = SW.DPI || {};
SW.DPI.Charts = SW.DPI.Charts || {};
SW.DPI.Charts.BottomTableLegend = SW.DPI.Charts.BottomTableLegend || {};

(function (legend) {
    toggleSeries = function (series) {
        series.visible ? series.hide() : series.show();
    };

    addLegendSymbol = function (series, container, colorOverride) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = colorOverride || series.color;

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
            .attr({
                stroke: symbolColor,
                fill: symbolColor
            })
            .add();

        label.appendTo(container);
    };
    addTitle = function (series, container, nameOverride) {
        var label = $('<span class="ellipsisText"/>');
        var link = $('<a/>');
        var title = nameOverride || SW.Core.Charts.htmlDecode(series.name);
        label.attr('title', title);
        link.attr("href", series.options.customProperties.NetObjectUrl);
        link.text(title);
        link.appendTo(label);
        label.appendTo(container);
    };
    addIcon = function (series, container) {
        var img = $("<img />");
        img.attr("src", series.options.customProperties.IconUrl);
        img.attr("style", "position: relative;top: 2px;");
        img.appendTo(container);
    }
    addCheckbox = function (series, container) {
        var check = $('<input type="checkbox"/>')
            .click(function () { toggleSeries(series); });

        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.createLegend = function (chart, dataUrlParameters, legendContainerId, interactiveMode) {
        var container = $('#' + legendContainerId);
        if (typeof (interactiveMode) == 'undefined') {
            interactiveMode = true;
        }

        var table = $('<table class="chartDataGrid NeedsZebraStripes"/>');
        var thead = $('<thead/>').appendTo(table);
        var tr = $('<tr/>').appendTo(thead);

        var columnHeaders = [null];
        if (chart.options.hasOwnProperty("TableLegend")) {
            if (chart.options.TableLegend.hasOwnProperty("ColumnHeaders"))
                columnHeaders = chart.options.TableLegend.ColumnHeaders;
        }

        var td = $('<td style="width:50%"/>').appendTo(tr);
        td.text(columnHeaders[0]);

        for (var i = 1; i < columnHeaders.length; i++) {
            td = $('<td/>').appendTo(tr);
            td.text(columnHeaders[i]);
        }
        var formatterName = chart.options.yAxis[0].unit;
        var formatter0 = SW.Core.Charts.dataFormatters[formatterName];
        var formatter1 = SW.Core.Charts.dataFormatters[formatterName];

        if (formatterName === "bytes")
            formatter0 = SW.Core.Charts.dataFormatters["bps"];

        $.each(chart.series, function (index, series) {
            if (!series.options.showInLegend)
                return;

            var subTable = $('<table class="chartDataGrid"/>');
            var subRow = $('<tr/>');
            var cell = $('<td/>');
            var row = $('<tr/>');

            subRow.appendTo(subTable);

            td = $('<td style="width:6%;border-bottom:none;"/>').appendTo(subRow);
            addLegendSymbol(series, td);

            if (interactiveMode) {
                td = $('<td style="width:6%;border-bottom:none;"/>').appendTo(subRow);
                addCheckbox(series, td);
            }

            td = $('<td style="width:6%;border-bottom:none;"/>').appendTo(subRow);
            addIcon(series, td);

            if (interactiveMode)
                td = $('<td style="width:82%;border-bottom:none;"/>').appendTo(subRow);
            else
                td = $('<td style="width:88%;border-bottom:none;"/>').appendTo(subRow);

            addTitle(series, td);

            subTable.appendTo(cell);
            cell.appendTo(row);

            var noLegendData = false;
            for (var i = 0; i < series.options.customProperties.TableLegendValues.length; i++) {
                td = $('<td/>').appendTo(row);
                var span = $('<span />').appendTo(td);
                var value = series.options.customProperties.TableLegendValues[i];
                if ((value < 0) || noLegendData) {
                    noLegendData = true;
                    span.text("@{R=DPI.Strings;K=Web_ChartTableLegend_NA; E=js}");
                }
                else if (i == 0) {
                    if (series.options.customProperties.Threshold === "W")
                        span.addClass("tableLegendWarning");
                    else if (series.options.customProperties.Threshold === "C")
                        span.addClass("tableLegendCritical");

                    span.text(formatter0(value, chart.yAxis));
                }
                else
                    span.text(formatter1(value, chart.yAxis));
            }

            row.appendTo(table);
        });

        table.appendTo(container);
    };
})(SW.DPI.Charts.BottomTableLegend)