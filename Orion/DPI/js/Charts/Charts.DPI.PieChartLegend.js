﻿SW.DPI = SW.DPI || {};
SW.DPI.Charts = SW.DPI.Charts || {};
SW.DPI.Charts.PieChartLegend = SW.DPI.Charts.PieChartLegend || {};

(function (legend) {
    var MIN_WIDTH_FOR_RIGHT = 590;
    var MAX_PIE_WIDTH = 250;

    var updateLegend = function (table) {
        var containerWidth = table.parent().parent().parent().width();
        // toggle css for right versus bottom legend
        if (containerWidth >= MIN_WIDTH_FOR_RIGHT) {
            // RIGHT LEGEND
            table.css({ "max-width": "320px", "border-collapse": "collapse" });
            table.find("tr").css("border-bottom", "0px");
            table.parent().parent().css({ "height": "280px" });
            table.parent().css({ "display": "block", "float": "left", "min-height": "250px", "width": "55%", "height": "280px", "padding-top": "20px" });
            table.parent().parent().find(".hasChart").parent().css({ "float": "left", "margin": "0", "min-width": "0", "width": "45%", "height": "280px" });
            table.find("tr:first").hide();
            table.find("tr:last").show();
            table.find("tr td:last-child").css("text-align", "right");
        }
        else {
            // BOTTOM LEGEND
            table.css({ "max-width": "none", "border-collapse": "inherit", "margin-bottom": "20px", "width": "100%" });
            table.find("tr").css("border-bottom", "1px solid #dadada");
            table.parent().parent().css({ "height": "auto" });
            table.parent().css({ "display": "block", "float": "none", "min-height": "0", "width": "100%", "height": "100%", "padding-top": "0" });
            table.parent().parent().find(".hasChart").parent().css({ "float": "none", "margin": "0 auto", "width": "100%", "height": "240px" });
            table.find("tr:first").show();
            table.find("tr:last").hide();
            table.find("tr td:last-child").css("text-align", "left");
        }
    };

    var addLegendSymbol = function (container, dataPoint) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = dataPoint.color;
        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
            .attr({
                stroke: symbolColor,
                fill: symbolColor
            })
            .add();

        label.appendTo(container);
    };

    legend.createLegend = function (chart, dataUrlParameters, legendContainerId) {
        var legendOptions = chart.options.DPILegendPieOptions || { showInReverseOrder: false, detailsURL: "" };  // default options
        var typeLabel = chart.options.ChartTypeLabel;
        var headerLabel = chart.options.HeaderColumnLabel;
        var totalsLabel = chart.options.FooterTotalLabel;
        var formatter = SW.Core.Charts.dataFormatters[chart.options.Formatter];
        var table = $('#' + legendContainerId);
        var series = chart.series[0];
        var row = {};

        table.empty();
        table.css({ "min-width": "50px", "max-width": "300px" });
        table.attr("cellspacing", "0");
        table.parent().siblings(".hasChart").css({ "max-width": "0px", "min-width": "250px", "height": "0px", "min-height": "250px", "float": "left" });
        table.parent().css({ "min-height": "250px" });
        table.parent().find("#chartDataNotAvailable").css("width", "200px");
        table.find("tr:last").addClass("dpi_PieTotalRow");
        table.addClass("NeedsZebraStripes");

        $(document).bind("resizestop", function () {
            setTimeout(function () {
                updateLegend(table);
            }, 100);
        });

        var transCount = 0;
        for (var i = 0; i < series.data.length; i++) {
            var point = series.data[i];
            transCount += point.y;
        }

        if (transCount > 0) {
            for (var i = 0; i < series.data.length; i++) {
                var point = series.data[i];

                row = $('<tr style="height:24px;" class="pie_details_row_' + legendContainerId + '" data-id="' + point.id + '" />');

                var colorTD = $('<td style="text-align:left;padding-left:8px;width:20px;min-width:20px;max-width:20px;">').appendTo(row);
                addLegendSymbol(colorTD, point);
                $('<td style="text-align:left;padding-left:4px;">' + point.name + '</td>').appendTo(row);
                $('<td style="text-align:right;padding-right:8px;">' + formatter(point.y, chart.yAxis) + '</td>').appendTo(row);

                // special case for Traffic by Risk legend in reverse order
                if (legendOptions.showInReverseOrder) {
                    row.prependTo(table);
                }
                else {
                    row.appendTo(table);
                }
            }

            // header row
            row = $('<tr style="background-color:#e0e0e0;"><td style="padding:6px 8px;text-transform:uppercase;" colspan="2">' + typeLabel + '</td><td style="width:50% !important;padding:6px 0px;text-transform:uppercase;">' + headerLabel + '</td></tr>');
            row.prependTo(table);

            // footer row
            row = $('<tr style="height:30px;"><td></td><td><b>' + totalsLabel + ':</b></td><td style="text-align:right;padding-right:8px;"><b>' + formatter(transCount, chart.yAxis) + '</b></td></tr>');
            row.appendTo(table);
        }

        if (legendOptions.detailsURL !== "") {
            $(".pie_details_row_" + legendContainerId).css("cursor", "pointer");

            //Commented because of QOE-880. If there will be "zebra stripping" like on other resources we dont need this functionality.
            //
            //$(".pie_details_row_" + legendContainerId).hover(
            //    function () {
            //        $(this).css({ "background-color": "#eaeaea" });
            //    },
            //    function () {
            //        $(this).css({ "background-color": "transparent" });
            //    }
            //);

            $(".pie_details_row_" + legendContainerId).click(function () {
                window.location = legendOptions.detailsURL + $(this).data("id");
            });
        }

        updateLegend(table);
    };
})(SW.DPI.Charts.PieChartLegend)