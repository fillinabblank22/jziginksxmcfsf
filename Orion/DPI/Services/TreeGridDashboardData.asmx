﻿<%@ WebService Language="C#" Class="TreeGridDashboardData" %>
using SolarWinds.DPI.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.DPI.Web.UI;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TreeGridDashboardData : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    #region Constants

    private const string QueryFormat = "query";                 // SWIS query defition
    private const string QueryFilters = "filters";              // SWIS query filters

    private const string ParamTimestamp = "timestamp";          // time of query
    private const string ParamTimeframe = "timeframe";          // time frame for data to be queried
    private const string ParamAmbientSort = "ambientsort";      // default sorting
    private const string ParamSort = "sort";                    // sorting
    private const string ParamSortDir = "dir";                  // asc / desc
    private const string ParamLimit = "limit";                  // page size
    private const string ParamPage = "page";                    // page offset
    private const string ParamExtParam = "extparam";            // extra parameter

    #endregion // Constants

    private readonly Dictionary<string, Dictionary<string, object>> DefaultOptions = new Dictionary<string, Dictionary<string, object>>
    {
        // [Mission Critical Stats]
        // ------------------------
        { TreeGridDashboardResourceKeys.Entity.MissionCriticalStats.Application, new Dictionary<string, object> {
            {ParamTimestamp, 0L}, {ParamTimeframe, 0L}, {ParamAmbientSort, "name"}, {ParamSort, null}, {ParamSortDir, "ASC"}, {ParamLimit, int.MaxValue}, {ParamPage, 0}, {ParamExtParam, null},
            {QueryFormat, @"SELECT app.ApplicationID AS [id],app.Name AS [name],app.DetailsUrl AS [uri],app.Status AS [status]," +
                            @"CASE ISNULL(SUM(aat.RecordCount_ART), 0) WHEN 0 THEN 0 ELSE (SUM(aat.ART * aat.RecordCount_ART) / SUM(aat.RecordCount_ART)) END AS [avgART]," +
                            @"CASE ISNULL(SUM(aat.RecordCount_NRT), 0) WHEN 0 THEN 0 ELSE (SUM(aat.NRT * aat.RecordCount_NRT) / SUM(aat.RecordCount_NRT)) END AS [avgNRT]," +
                            @"SUM(aat.Ingress + aat.Egress) AS [bytes],SUM(aat.Transactions) as [transactions]" +
                          @" FROM Orion.DPI.ApplicationAssignmentsThresholds("+
                            @"StartDate=@" + TreeGridDashboardResourceKeys.Filter.TimeframeStart +
                            @",EndDate=@"  + TreeGridDashboardResourceKeys.Filter.TimeframeStop +
                            @") AS aat"+
                          @" INNER JOIN Orion.Nodes n ON n.NodeID = aat.NodeID" +
                          @" INNER JOIN Orion.DPI.Applications AS app ON app.ApplicationID = aat.ApplicationID AND app.Status <> 9" +
                          @" WHERE {0}" +                           // where condition: {0};
                          @" GROUP BY app.ApplicationID,app.Name,app.DetailsUrl,app.Status" +
                          @" ORDER BY [{1}] {2}"},                  // order by: {1}; order direction: {2};
            {QueryFilters, new Dictionary<string, string>{
                // [oh] filters here automatically add timeframe parameters (@start, @stop)
                {TreeGridDashboardResourceKeys.Filter.TimeframeStart, string.Empty},
                {TreeGridDashboardResourceKeys.Filter.TimeframeStop, string.Empty}
            }}
        }},
        { TreeGridDashboardResourceKeys.Entity.MissionCriticalStats.Node, new Dictionary<string, object> {
            {ParamTimestamp, 0L}, {ParamTimeframe, 0L}, {ParamAmbientSort, "name"}, {ParamSort, null}, {ParamSortDir, "ASC"}, {ParamLimit, int.MaxValue}, {ParamPage, 0}, {ParamExtParam, null},
            {QueryFormat, @"SELECT DISTINCT n.NodeID AS [id],n.DisplayName AS [name],n.WebUri.WebUri AS [uri], n.Status AS [status]," +
                                @" (aat.ART) AS [avgART], aat.ARTStatus AS [SavgART], " +
                                @" (aat.NRT) AS [avgNRT], aat.NRTStatus AS [SavgNRT], " +
                                @" (aat.Ingress+aat.Egress) AS [bytes], aat.DataVolumePerSecStatus AS [Sbytes]," +
                                @" (aat.Transactions) AS [transactions], aat.TransactionsPerMinStatus AS [Stransactions]" +
                          @" FROM Orion.DPI.ApplicationAssignmentsThresholds("+
                            @"StartDate=@" + TreeGridDashboardResourceKeys.Filter.TimeframeStart +
                            @",EndDate=@"  + TreeGridDashboardResourceKeys.Filter.TimeframeStop +
                            @") AS aat"+
                          @" INNER JOIN Orion.Nodes n ON n.NodeID = aat.NodeID" +
                          @" INNER JOIN Orion.DPI.Applications AS app ON app.ApplicationID = aat.ApplicationID AND app.Status <> 9" +
                          @" WHERE {0}" +                           // where condition: {0};
                          @" ORDER BY [{1}] {2}"},                  // order by: {1}; order direction: {2};
            {QueryFilters, new Dictionary<string, string>{
                //{"risk", "a.RiskLevel=@{0}"},
                {TreeGridDashboardResourceKeys.Filter.TimeframeStart, string.Empty},
                {TreeGridDashboardResourceKeys.Filter.TimeframeStop, string.Empty},
                {TreeGridDashboardResourceKeys.Entity.MissionCriticalStats.Application, "app.ApplicationID=@{0}"}
            }}
        }}
    };

    [DataContract]
    public class ResponseData
    {
        [DataMember(Name = "entity")]
        public string Entity { get; private set; }

        [DataMember(Name = "result")]
        public string Result { get; private set; }

        [DataMember(Name = "columns")]
        public string[] Columns { get; private set; }

        [DataMember(Name = "rows")]
        public List<object[]> Rows { get; private set; }

        [DataMember(Name = "count")]
        public int Count
        {
            get { return this.Rows != null ? this.Rows.Count : 0; }
            private set { }
        }

        public ResponseData(string entity, string error)
        {
            this.Entity = entity;
            Result = error;
        }

        public ResponseData(string entity, DataTable dt)
        {
            if (dt == null)
                throw new ArgumentNullException("dt", "DataTable must not be null");

            this.Entity = entity;
            this.Result = string.Empty;

            this.Columns = new string[dt.Columns.Count];
            for (int i = 0; i < this.Columns.Length; ++i)
                this.Columns[i] = dt.Columns[i].ColumnName;

            this.Rows = new List<object[]>();
            using (DataTableReader dtr = dt.CreateDataReader())
            {
                while (dtr.Read())
                    this.Rows.Add(Columns.Select(c =>
                    {
                        int ordinal = dtr.GetOrdinal(c);
                        return dtr.IsDBNull(ordinal) ? null : dtr.GetValue(ordinal);
                    }).ToArray());
            }
        }
    }

    [WebMethod(EnableSession = true), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string QueryData(string[] entities, string[] path, object options)
    {
        try
        {
            var entity = string.Empty;
            var swisQuery = string.Empty;
            var swisFilters = new List<string>();
            var swisParams = new Dictionary<string, object>();

            if (!TryProcessPath(entities, path, out entity, swisFilters, swisParams))
                throw new ArgumentException("Invalid path.");

            Dictionary<string, object> defaults;
            if (!DefaultOptions.TryGetValue(entity, out defaults))
                throw new ArgumentException("Invalid entity value.");

            using (var swis3 = InformationServiceProxy.CreateV3())
            {
                var parameters = ProcessPameters(options, defaults);

                var sort = (string)parameters[ParamSort] ?? (string)parameters[ParamAmbientSort];
                var sortdir = (string)parameters[ParamSortDir];

                // .. add time frame filter
                ProcessTimeFilter(parameters, swisFilters, swisParams);

                var query = (string)defaults[QueryFormat];
                var where = (swisFilters.Count > 0) ? string.Join(" AND ", swisFilters.ToArray()) : "1=1";

                swisQuery = string.Format(query, where, sort, sortdir);

                // .. apply extra parameters
                var extparams = new object[] { (string)parameters[ParamExtParam] };
                if (extparams != null && extparams.Length > 0)
                    swisQuery = string.Format(swisQuery, extparams);

                CheckViewLimitation(parameters);
                var dt = swis3.Query(swisQuery, swisParams, true);

                // serialize to json
                var response = new ResponseData(entity, dt);
                var serializer = new DataContractJsonSerializer(typeof(ResponseData));
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, response);
                    return System.Text.Encoding.Default.GetString(ms.ToArray());
                }
            }
        }
        catch (Exception ex)
        {
            var arrpath = path != null ? string.Format("['{0}']", string.Join("','", path)) : "null";
            var message = string.Format("Error occurred when trying to query data for: {0}", arrpath);
            log.Error(message, ex);
            throw ResourceException.CreateResourceException(ex);
        }
    }

    private bool TryProcessPath(string[] entityPath, string[] idPath,
        out string entity, List<string> swisFilters, Dictionary<string, object> swisParams)
    {
        entity = string.Empty;

        if (entityPath == null || idPath == null)
            return false;

        var entities = entityPath.Where(e => DefaultOptions.ContainsKey(e)).ToArray();
        var parts = idPath.Select(id => Server.UrlDecode(id)).ToArray();

        // [oh] The path must be one less then entities, because the last entity is leaf.
        if (entities.Length < parts.Length + 1)
        {
            var errorMsg = string.Format("Path is too long. Entities:{0}, Path:{1}.", string.Join("|", entities), string.Join("|", idPath));
            throw new ArgumentException(errorMsg);
        }

        entity = entities[parts.Length];

        var filters = ((Dictionary<string, string>)DefaultOptions[entity][QueryFilters]);
        for (int i = 0; i < parts.Length; i++)
        {
            var filterValue = Server.UrlDecode(parts[i]);

            var filterEntity = entities[i];
            if (!string.IsNullOrEmpty(filterEntity))
                swisParams.Add(filterEntity, filterValue);

            if (!filters.ContainsKey(filterEntity))
                throw new ArgumentOutOfRangeException("There is no filter for:" + filterEntity);

            var filterFormat = filters[filterEntity];
            if (!string.IsNullOrEmpty(filterFormat))
                swisFilters.Add(string.Format(filterFormat, filterEntity));
        }

        return true;
    }

    private void ProcessTimeFilter(Dictionary<string, object> parameters, List<string> swisFilters, Dictionary<string, object> swisParams)
    {
        var timestamp = (long)parameters[ParamTimestamp];
        var timeframe = (long)parameters[ParamTimeframe];
        var filters = (Dictionary<string, string>)parameters[QueryFilters];

        if (timestamp > 0 && timeframe > 0)
        {
            if (!filters.ContainsKey(TreeGridDashboardResourceKeys.Filter.TimeframeStart))
                throw new ArgumentOutOfRangeException("There is no custom timeframe filter for: " + TreeGridDashboardResourceKeys.Filter.TimeframeStart);
            if (!filters.ContainsKey(TreeGridDashboardResourceKeys.Filter.TimeframeStop))
                throw new ArgumentOutOfRangeException("There is no custom timeframe filter for: " + TreeGridDashboardResourceKeys.Filter.TimeframeStop);

            // [oh] setup time filters
            if (!string.IsNullOrWhiteSpace(filters[TreeGridDashboardResourceKeys.Filter.TimeframeStart]))
                swisFilters.Add(string.Format(filters[TreeGridDashboardResourceKeys.Filter.TimeframeStart], TreeGridDashboardResourceKeys.Filter.TimeframeStart));
            if (!string.IsNullOrWhiteSpace(filters[TreeGridDashboardResourceKeys.Filter.TimeframeStop]))
                swisFilters.Add(string.Format(filters[TreeGridDashboardResourceKeys.Filter.TimeframeStop], TreeGridDashboardResourceKeys.Filter.TimeframeStop));

            // [oh] add time parameters or update it's values into dictionary
            var startTime = new DateTime(timestamp - timeframe).ToUniversalTime();
            var stopTime = new DateTime(timestamp).ToUniversalTime();

            if (!swisParams.ContainsKey(TreeGridDashboardResourceKeys.Filter.TimeframeStart))
                swisParams.Add(TreeGridDashboardResourceKeys.Filter.TimeframeStart, startTime);
            else
                swisParams[TreeGridDashboardResourceKeys.Filter.TimeframeStart] = startTime;

            if (!swisParams.ContainsKey(TreeGridDashboardResourceKeys.Filter.TimeframeStop))
                swisParams.Add(TreeGridDashboardResourceKeys.Filter.TimeframeStop, stopTime);
            else
                swisParams[TreeGridDashboardResourceKeys.Filter.TimeframeStop] = stopTime;
        }
    }

    private Dictionary<string, object> ProcessPameters(object options, Dictionary<string, object> defaults)
    {
        if (options == null || defaults == null)
            return defaults;

        Dictionary<string, object> opts = (Dictionary<string, object>)options;
        Dictionary<string, object> parameters = new Dictionary<string, object>();

        parameters[QueryFilters] = defaults[QueryFilters];
        parameters[ParamTimestamp] = ParsePameter(ParamTimestamp, (long)defaults[ParamTimestamp], opts);
        parameters[ParamTimeframe] = ParsePameter(ParamTimeframe, (long)defaults[ParamTimeframe], opts);
        parameters[ParamAmbientSort] = ParsePameter(ParamAmbientSort, (string)defaults[ParamAmbientSort], opts);
        parameters[ParamSort] = ParsePameter(ParamSort, (string)defaults[ParamSort], opts);
        parameters[ParamSortDir] = ParsePameter(ParamSortDir, (string)defaults[ParamSortDir], opts);
        parameters[ParamLimit] = ParsePameter(ParamLimit, (int)defaults[ParamLimit], opts);
        parameters[ParamPage] = ParsePameter(ParamPage, (int)defaults[ParamPage], opts);
        parameters[ParamExtParam] = ParsePameter(ParamExtParam, (string)defaults[ParamExtParam], opts);

        // add special view limitation parameter
        parameters[TreeGridDashboardResourceKeys.ViewLimitationID] =
            ParsePameter(TreeGridDashboardResourceKeys.ViewLimitationID, (int)0, opts);

        return parameters;
    }

    private string ParsePameter(string key, string defValue, Dictionary<string, object> parameters)
    {
        object value;
        return parameters.TryGetValue(key, out value) ? (value != null ? value.ToString() : null) : defValue;
    }

    private int ParsePameter(string key, int defValue, Dictionary<string, object> parameters)
    {
        object value;
        return parameters.TryGetValue(key, out value) ? Convert.ToInt32(value) : defValue;
    }

    private long ParsePameter(string key, long defValue, Dictionary<string, object> parameters)
    {
        object value;
        return parameters.TryGetValue(key, out value) ? Convert.ToInt64(value) : defValue;
    }

    private void CheckViewLimitation(Dictionary<string, object> parameters)
    {
        // add view info with limitation id to apply limitations when request is called
        object viewLimitationId;
        if (!HttpContext.Current.Items.Contains(typeof(ViewInfo).Name) &&
            parameters.TryGetValue(TreeGridDashboardResourceKeys.ViewLimitationID, out viewLimitationId) &&
            viewLimitationId != null && (int)viewLimitationId > 0)
        {
            var viewInfo = new ViewInfo("", "", "", false, false, null) { LimitationID = (int)viewLimitationId };
            HttpContext.Current.Items[typeof(ViewInfo).Name] = viewInfo;
        }
    }
}