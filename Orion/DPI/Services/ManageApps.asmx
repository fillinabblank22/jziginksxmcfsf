﻿<%@ WebService Language="C#" Class="ManageApps" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.DPI.Common;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ManageApps : WebService
{
    public class Result
    {
        public int ResultCount;
        public bool Success = true;
        public List<object> Items;
    }

    private string CastGroupByValue(DataRow dr)
    {
        object value = !dr.IsNull("Value") ? dr["Value"] : string.Empty;

        if (value.GetType() == typeof(Byte))
            return dr.Field<Byte>("Value").ToString();

        if (value.GetType() == typeof(int))
            return dr.Field<int>("Value").ToString();

        if (value.GetType() == typeof(string))
            return dr.Field<string>("Value");

        return null;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetGroupByApp(int start, int limit, Dictionary<string, object>[] sort, Dictionary<string, object>[] filter)
    {
        bool hideProceraApps = (bool)Session["DPI_Wizard_Current_Application_HidePrebuilt"];

        object orderBy = "Value";
        object orderDirection = "ASC";
        if (sort.Length > 0)
        {
            sort[0].TryGetValue("property", out orderBy);
            sort[0].TryGetValue("direction", out orderDirection);
        }

        string grouping = getFilterFromObject("Grouping", filter);
        if (string.IsNullOrWhiteSpace(grouping))
        {
            return new Result()
            {
                ResultCount = 0
            };
        }

        if (String.Equals(grouping, "a.Status", StringComparison.OrdinalIgnoreCase))
        {
            grouping = "(CASE (a.Status) WHEN 9 THEN 9 ELSE 1 END)";
        }

        string whereClause = getAppWhereFilter(hideProceraApps, filter, false);

        using (var swis3 = InformationServiceProxy.CreateV3())
        {

            string query = string.Format(@"
SELECT {0} as [Value], COUNT({0}) AS [Count]
FROM Orion.DPI.Applications a
LEFT JOIN Orion.DPI.ApplicationCategories c ON a.CategoryID = c.CategoryID
{3}
GROUP BY {0} ORDER BY {1} {2} WITH NOLOCK",
                grouping, orderBy, orderDirection, whereClause);

            var data = swis3.Query(query, true);
            var totalRows = data.ExtendedProperties["TotalRows"];

            return new Result()
            {
                ResultCount = (totalRows == null) ? 0 : int.Parse(totalRows.ToString()),
                Items = data.Rows.Cast<DataRow>().Select(r => new
                {
                    Value = CastGroupByValue(r),
                    Count = r.Field<int>("Count")
                }).ToList<object>()
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetApp(int id)
    {
        var result = new Result();
        var app = SolarWinds.DPI.Web.DALs.Swis.Application.Get(false, id);

        if (app != null)
        {
            result.Items.Add(app);
            result.ResultCount = 1;
        }

        return result;
    }

    private string getAppWhereFilter(bool hideProcera, Dictionary<string, object>[] filters, bool withGrouping = true)
    {
        List<string> result = new List<string>();
        string groupByColumn = getFilterFromObject("Grouping", filters);
        string groupByFilter = getFilterFromObject("GroupBy", filters);
        string searchFilter = getFilterFromObject("Search", filters);

        if (hideProcera)
            result.Add("ProtocolID = 'HTTP' AND Filter IS NOT NULL");

        if (!string.IsNullOrEmpty(searchFilter))
            result.Add(getSingleSearchFilter("a.Name", searchFilter));

        if (withGrouping && !string.IsNullOrEmpty(groupByColumn) && !string.IsNullOrEmpty(groupByFilter))
        {
            // DPI-1488 : (9 = disabled, 1 = not disabled)
            if (String.Equals(groupByColumn, "a.Status", StringComparison.OrdinalIgnoreCase)
                && String.Equals((string)groupByFilter, "1", StringComparison.OrdinalIgnoreCase))
            {
                result.Add(string.Format("{0}!='9'", groupByColumn));
            }
            else
            {
                result.Add(string.Format("{0}='{1}'", groupByColumn, groupByFilter));
            }
        }

        var join = result.Count > 0 ? string.Join(") AND (", result.ToArray()) : "1=1";
        return string.Format(" WHERE ({0})", join);
    }

    private string getSingleSearchFilter(string searchField, string searchVal)
    {
        return String.Format(" {0} LIKE '%{1}%' ", searchField, searchVal);
    }

    private string getFilterFromObject(string property, Dictionary<string, object>[] filters)
    {
        var filter = filters.FirstOrDefault(f => f.ContainsKey("property") && f.ContainsKey("value") && Convert.ToString(f["property"]) == property);
        return filter != null ? Convert.ToString(filter["value"]) : string.Empty;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetApps(int start, int limit, Dictionary<string, object>[] sort, Dictionary<string, object>[] filter)
    {
        bool hideProceraApps = (bool)Session["DPI_Wizard_Current_Application_HidePrebuilt"];

        object orderBy = "ApplicationID";
        object orderDirection = "ASC";

        if (sort.Length > 0)
        {
            sort[0].TryGetValue("property", out orderBy);
            sort[0].TryGetValue("direction", out orderDirection);
        }

        string whereClause = getAppWhereFilter(hideProceraApps, filter);

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"SELECT
                a.ApplicationID as ApplicationID,
                a.Name as Name,
                a.Description as Description,
                (CASE (a.AdminStatus) WHEN 9 THEN 9 ELSE 1 END) as Status,
                c.Name as Category,
                a.CategoryID as CategoryID,
                a.RiskLevel as RiskLevel,
                a.ProductivityRating as ProductivityRating,
                a.Filter as Filter,
                a.DiscoveryMode,
                a.LastDiscoveryDate,
                ISNULL(p.Name, '') AS LastDiscoveryProbe,
                (SELECT COUNT(NodeID) as NodeCount FROM Orion.DPI.ApplicationAssignments aa WHERE aa.ApplicationID = a.ApplicationID) as NodeCount
                FROM Orion.DPI.Applications a left join Orion.DPI.ApplicationCategories c on a.CategoryID = c.CategoryID
                LEFT JOIN Orion.DPI.Probes p ON a.LastDiscoveryProbeID=p.ProbeID
                {4}
                ORDER BY {0} {1} WITH ROWS {2} TO {3} WITH TOTALROWS WITH NOLOCK",
                orderBy, orderDirection, start + 1, start + limit, whereClause);

            var data = swis3.Query(query, true);

            var result = new Result();

            if (data.ExtendedProperties["TotalRows"] == null)
                result.ResultCount = 0;
            else
                result.ResultCount = int.Parse(data.ExtendedProperties["TotalRows"].ToString());

            result.Items = data.Rows.Cast<DataRow>()
                .Select(r =>
                {
                    return new
                    {
                        ApplicationID = r.Field<int>("ApplicationID"),
                        Name = r.Field<string>("Name"),
                        Description = r.Field<string>("Description"),
                        Status = r.Field<int>("Status") != (int)SolarWinds.Orion.Core.Common.OrionObjectStatus.Unmanaged,
                        CategoryID = r.Field<int>("CategoryID"),
                        Category = r.Field<string>("Category"),
                        RiskLevel = r.Field<byte>("RiskLevel"),
                        ProductivityRating = r.Field<byte>("ProductivityRating"),
                        NodeCount = r.Field<int>("NodeCount"),
                        DiscoveryMode = r.Field<int>("DiscoveryMode"),
                        LastDiscoveryDate = r.IsNull("LastDiscoveryDate") ? string.Empty : Utils.FormatDateTime(r.Field<DateTime>("LastDiscoveryDate").ToLocalTime(), true),
                        LastDiscoveryProbe = r.Field<string>("LastDiscoveryProbe")
                    };
                })
                .ToList<object>();

            return result;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetGroupByProtocols(int start, int limit, Dictionary<string, object>[] sort, Dictionary<string, object>[] filter)
    {
        object orderBy = "Value";
        object orderDirection = "ASC";
        if (sort.Length > 0)
        {
            sort[0].TryGetValue("property", out orderBy);
            sort[0].TryGetValue("direction", out orderDirection);
        }

        string grouping = getFilterFromObject("Grouping", filter);
        if (string.IsNullOrWhiteSpace(grouping))
        {
            return new Result()
            {
                ResultCount = 0
            };
        }

        string whereClause = getProtocolsWhereFilter(filter, false);
        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"
SELECT {0} as [Value], COUNT({0}) AS [Count]
FROM Orion.DPI.ApplicationProtocols a
LEFT JOIN Orion.DPI.ApplicationCategories c ON a.CategoryID = c.CategoryID
{3}
GROUP BY {0} ORDER BY {1} {2} WITH NOLOCK",
                grouping, orderBy, orderDirection, whereClause);

            var data = swis3.Query(query, true);
            var totalRows = data.ExtendedProperties["TotalRows"];

            return new Result()
            {
                ResultCount = (totalRows == null) ? 0 : int.Parse(totalRows.ToString()),
                Items = data.Rows.Cast<DataRow>().Select(r => new
                {
                    Value = CastGroupByValue(r),
                    Count = r.Field<int>("Count")
                }).ToList<object>()
            };
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetPredefinedProtocols(int start, int limit, Dictionary<string, object>[] sort, Dictionary<string, object>[] filter)
    {
        object orderBy = "ApplicationID";
        object orderDirection = "ASC";

        if (sort.Length > 0)
        {
            sort[0].TryGetValue("property", out orderBy);
            sort[0].TryGetValue("direction", out orderDirection);
        }

        string whereClause = getProtocolsWhereFilter(filter);

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"SELECT
                a.ProtocolID as ProtocolID,
                a.Name as Name,
                a.Description as Description,
                a.CategoryID as CategoryID,
                c.Name as Category,
                a.RiskLevel as RiskLevel,
                a.ProductivityRating as ProductivityRating
                FROM Orion.DPI.ApplicationProtocols a left join Orion.DPI.ApplicationCategories c on a.CategoryID = c.CategoryID
                {4}
                ORDER BY {0} {1} WITH ROWS {2} TO {3} WITH TOTALROWS WITH NOLOCK",
                orderBy, orderDirection, start + 1, start + limit, whereClause);

            var data = swis3.Query(query, true);

            var result = new Result();

            if (data.ExtendedProperties["TotalRows"] == null)
                result.ResultCount = 0;
            else
                result.ResultCount = int.Parse(data.ExtendedProperties["TotalRows"].ToString());

            result.Items = data.Rows.Cast<DataRow>()
                .Select(r =>
                {
                    return new
                    {
                        ApplicationID = r.Field<string>("ProtocolID"),
                        Name = r.Field<string>("Name"),
                        Description = r.Field<string>("Description"),
                        Status = 0,
                        CategoryID = r.Field<int>("CategoryID"),
                        Category = r.Field<string>("Category"),
                        RiskLevel = r.Field<byte>("RiskLevel"),
                        ProductivityRating = r.Field<byte>("ProductivityRating"),
                        NodesList = String.Empty
                    };
                })
                .ToList<object>();

            return result;
        }
    }

    private string getProtocolsWhereFilter(Dictionary<string, object>[] filters, bool withGrouping = true)
    {
        List<string> result = new List<string>();
        string groupByColumn = getFilterFromObject("Grouping", filters);
        string groupByFilter = getFilterFromObject("GroupBy", filters);
        string searchFilter = getFilterFromObject("Search", filters);

        // exclude existing applications
        result.Add(@"a.ProtocolID NOT IN (SELECT ProtocolID FROM Orion.DPI.Applications WHERE Filter IS NULL)");

        // only visible protocols
        result.Add(@"IsVisible = 1");

        if (!string.IsNullOrEmpty(searchFilter))
            result.Add(getSingleSearchFilter("a.Name", searchFilter));

        if (withGrouping && !string.IsNullOrEmpty(groupByColumn) && !string.IsNullOrEmpty(groupByFilter))
            result.Add(string.Format("{0}='{1}'", groupByColumn, groupByFilter));

        var join = result.Count > 0 ? string.Join(") AND (", result.ToArray()) : "1=1";
        return string.Format(" WHERE ({0})", join);
    }

    [WebMethod]
    public void SetApplicationsEnabled(IEnumerable<int> applicationIds, bool enabled)
    {
        using (var dbl = new SolarWinds.DPI.Common.DPIBusinessLayerProxy(HttpContext.Current.Profile.UserName))
        {
            dbl.SetApplicationsEnabled(applicationIds, enabled);
        }
    }

    [WebMethod]
    public void SetApplicationDiscoveryMode(IEnumerable<int> applicationIds, ApplicationDiscoveryMode discoveryMode)
    {
        using (var dbl = new SolarWinds.DPI.Common.DPIBusinessLayerProxy(HttpContext.Current.Profile.UserName))
        {
            dbl.SetApplicationDiscoveryMode(applicationIds, discoveryMode);
        }
    }

    [WebMethod]
    public void DeleteApps(int[] ids)
    {
        using (var dbl = new SolarWinds.DPI.Common.DPIBusinessLayerProxy())
        {
            foreach (int id in ids)
            {
                dbl.RemoveApplication(id);
            }
        }
    }
}