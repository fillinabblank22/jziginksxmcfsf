﻿<%@ WebHandler Language="C#" Class="SelectApplicationsDataLoader" %>

using System;
using System.Data;
using System.Text;
using System.Web;
using SolarWinds.DPI.Web.UI.Helpers;
using SolarWinds.Orion.Web.InformationService;

public class SelectApplicationsDataLoader : IHttpHandler 
{
    public void ProcessRequest (HttpContext context) 
    {
        string operation = context.Request.Params["operation"];
        
        string result;
        if (operation == "LoadGroupingData")
        {
            string parentType = context.Request.Params["parentType"];
            string parentId = context.Request.Params["parentId"];
            string searchString = context.Request.Params["searchString"];
            string excludedItems = context.Request.Params["excludedItems"];
            
            result = LoadGroupingData(parentType, parentId, searchString, excludedItems);
        }
        else if (operation == "LoadSelectedApplications")
        {
            string entityIds = context.Request.Params["entityIds"];

            result = LoadSelectedApplications(entityIds);
        }
        else
        {
            throw new InvalidOperationException(operation);
        }

        context.Response.ContentType = "application/json";
        context.Response.Write(result);
    }
 
    public bool IsReusable 
    { 
        get {return false;}
    }

    /// <summary>
    /// Load data for tree.
    /// </summary>
    /// <param name="parentType">The parent type: it is filled from "Group by" combo.</param>
    /// <param name="parentId">The parent's Id. It can be 'root', 'A:[[ApplicationID ]]' or 'AP:[[AppplicationProtocol ID]]'</param>
    /// <param name="searchString">The search string. It is filled from "Search for" box.</param>
    /// <param name="excludedItems">List of items already selected.</param>
    private string LoadGroupingData(string parentType, string parentId, string searchString, string excludedItems)
    {
        string result;

        if (String.IsNullOrEmpty(parentType) || parentId != "root")
        {
            result = GetApplications(parentType, parentId, searchString, excludedItems);
        }
        else
        {
            result = GetGroups(parentType);
        }

        return result;
    }

    /// <summary>
    /// Load data for selected items.
    /// </summary>
    /// <param name="entityIds">List of selected entity Ids.</param>
    private string LoadSelectedApplications(string entityIds)
    {
        StringBuilder result = new StringBuilder("[");
        bool isFirst = true;
        var appIds = DpiApplicationHelper.ParseDpiApplicationIds(entityIds);
        var appProtocolsIds = DpiApplicationHelper.ParseDpiApplicationProtocolIds(entityIds);

        string appQuery = String.Format("SELECT CONCAT('A:', ApplicationID) AS EntityID, " +
                                        "Name, " +
                                        "'{0}' AS EntityType " +
                                     "FROM Orion.DPI.Applications " +
                                        "WHERE ApplicationID IN ('{1}')",
                                     SolarWinds.DPI.Common.Constants.DpiApplications,
                                     String.Join("','", appIds));

        string appProtocolsQuery = String.Format("SELECT CONCAT('AP:', ProtocolID) AS EntityID, " +
                                                 "Name, " +
                                                 "'{0}' AS EntityType " +
                                     "FROM Orion.DPI.ApplicationProtocols " +
                                        "WHERE ProtocolID IN ('{1}')",
                                        SolarWinds.DPI.Common.Constants.DpiApplicationProtocols,
                                        String.Join("','", appProtocolsIds));

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            var data = swis3.Query(appQuery, true);
            ProcessSelectedEntitiesQuery(result, data, ref isFirst);

            data = swis3.Query(appProtocolsQuery, true);
            ProcessSelectedEntitiesQuery(result, data, ref isFirst);
        }

        result.Append("]");
        
        return result.ToString();
    }

    private string GetApplications(string parentType, string parentId, string searchString, string excludedItems)
    {
        StringBuilder result = new StringBuilder("[");
        bool isFirst = true;

        StringBuilder query = new StringBuilder();
        string parentFilter = "1=1";    //dummy operation
        if (!String.IsNullOrEmpty(parentType) && !String.IsNullOrEmpty(parentId))
        {
            parentFilter = String.Format("ISNULL(b.{0}, a.{0}) = {1} ", parentType, parentId);
        }

        string filteExcluded = "1=1";    //dummy operation
        if (!String.IsNullOrEmpty(excludedItems))
        {
            var splitted = excludedItems.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            filteExcluded = String.Format("EntityID NOT IN ('{0}')", String.Join("','", splitted));
        }

        query.AppendFormat("SELECT Name, EntityID, EntityType " +
                           "FROM (" +
                           "SELECT ISNULL(b.Name, a.Name) AS Name, " +
                                "CASE WHEN b.ApplicationID IS NULL THEN CONCAT('AP:', a.ProtocolID) ELSE CONCAT('A:', b.ApplicationID) END AS EntityID, " +
                                "CASE WHEN b.ApplicationID IS NULL THEN '{0}' ELSE '{1}' END AS EntityType, " +
                                "CASE WHEN b.ApplicationID IS NOT NULL THEN 1 ELSE a.IsVisible END AS IsVisible " +
                           "FROM Orion.DPI.ApplicationProtocols a " +
                                "left join Orion.DPI.Applications b " +
                                    "on a.ProtocolID = b.ProtocolID " +
                           "WHERE {2} " +
                                "AND ISNULL(b.Name, a.Name) LIKE '%{3}%' " +
                           ") WHERE IsVisible = 1 AND {4} " +
                           "ORDER BY Name",
                                SolarWinds.DPI.Common.Constants.DpiApplicationProtocols,
                                SolarWinds.DPI.Common.Constants.DpiApplications,
                                parentFilter,
                                searchString,
                                filteExcluded
                            );

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            var data = swis3.Query(query.ToString(), true);
            ProcessSelectedEntitiesQuery(result, data, ref isFirst);
        }

        result.Append("]");
        return result.ToString();
    }

    private string GetGroups(string groupType)
    {
        StringBuilder result = new StringBuilder("[");
        bool isFirst = true;

        string query;
        switch (groupType)
        {
            case "CategoryID":
                //Category name gets from database, therefore it has different SQL query.
                query = "SELECT c.Name AS GroupName, " +
                        "ISNULL(b.CategoryID, a.CategoryID) AS GroupID, " +
                        "'Orion.DPI.ApplicationCategories' AS EntityType " +
                    "FROM Orion.DPI.ApplicationProtocols a, Orion.DPI.ApplicationCategories c " +
                        "left join Orion.DPI.Applications b " +
                            "on a.ProtocolID = b.ProtocolID " +
                    "WHERE ISNULL(b.CategoryID, a.CategoryID) = c.CategoryID " +
                    "GROUP BY ISNULL(b.CategoryID, a.CategoryID), c.Name " +
                    "ORDER BY c.Name WITH NOLOCK";
                break;
            default:
                query = String.Format("SELECT '' As GroupName, " +
                                        "ISNULL(b.{0}, a.{0}) AS GroupID, " +
                                        "'{0}' AS EntityType " +
                                      "FROM Orion.DPI.ApplicationProtocols a " +
                                        "left join Orion.DPI.Applications b " +
                                            "on a.ProtocolID = b.ProtocolID " +
                                      "GROUP BY ISNULL(b.{0}, a.{0}) WITH NOLOCK",
                                      groupType);
                break;
        }

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            var data = swis3.Query(query, true);
            foreach (DataRow row in data.Rows)
            {
                if (!isFirst) result.Append(",");

                isFirst = false;
                result.AppendFormat("{{ id: '{0}', text: '{1}', type: '{2}', leaf: false }}", row["GroupID"], GetGroupName(groupType, row), row["EntityType"]);
            }
        }

        result.Append("]");
        return result.ToString();
    }

    private string GetEntityType(DataRow row)
    {
        return GetEntityType(row["EntityType"].ToString());
    }

    private string GetEntityType(string entityType)
    {
        //EntityType is mapped to 1/3. Based on this, icon for Application or ApplicationProtocols will be set in javascript.
        if (entityType == SolarWinds.DPI.Common.Constants.DpiApplications)
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }

    private void ProcessSelectedEntitiesQuery(StringBuilder sb, DataTable dt, ref bool isFirst)
    {
        foreach (DataRow row in dt.Rows)
        {
            if (!isFirst) sb.Append(",");

            isFirst = false;
            var name = row.IsNull("Name") ? string.Empty : row["Name"].ToString().Replace("'", "\\'");
            sb.AppendFormat("{{ id: '{0}', text: '{1}', type: '{2}', leaf: true, checked: false }}", row["EntityID"],
                name, GetEntityType(row));
        }
    }

    private string GetGroupName(string entityType, DataRow row)
    {
        switch (entityType)
        {
            case SolarWinds.DPI.Common.Constants.CategoryID:
                return row["GroupName"].ToString();
            case SolarWinds.DPI.Common.Constants.RiskLevel:
                return SolarWinds.DPI.Common.Constants.RiskLevels[int.Parse(row["GroupID"].ToString())];
            case SolarWinds.DPI.Common.Constants.ProductivityRating:
                return SolarWinds.DPI.Common.Constants.ProductivityRatings[int.Parse(row["GroupID"].ToString())];
            default:
                return row["GroupName"].ToString();
        }
    }
}