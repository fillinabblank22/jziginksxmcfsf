﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.DPI.Web;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Web.Charts;
using SolarWinds.DPI.Web.UI;
using Resources;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ChartData : ChartDataWebService
{
    static Log log = new Log();

    private ChartDataResults GetSampledSeries(ChartDataRequest request, ChartDataContext chartContext, string sql, Func<IEnumerable<DataPoint>, double> sampleMethod)
    {
        var limitedSql = SolarWinds.Orion.Web.Limitation.LimitSQL(sql);
        var rawSeries = ChartDataServiceHelper.GetRawSeries(limitedSql, chartContext.SqlParameters.Select(x => new SqlParameter(x.Key, x.Value)));

        var seriesToReturn = new List<DataSeries>();

        var sampleSize = chartContext.DynamicLoader.CalculateDynamicSampleSize(chartContext.DateRange);

        foreach (var raw in rawSeries)
        {
            var sampled = raw.CreateSampledSeries(chartContext.BucketDateRange, sampleSize, sampleMethod);
            sampled.CustomProperties = raw.CustomProperties;

            if (string.IsNullOrEmpty(sampled.Label)) sampled.Label = Resources.DPIWebContent.Charts_UnknownDataSeriesLabel;
            if (string.IsNullOrEmpty(sampled.TagName)) sampled.TagName = "Default";

            seriesToReturn.Add(sampled);
        }

        if (request.CalculateSum)
            seriesToReturn.Add(DataSeries.CreateSumSeries(seriesToReturn));
        var result =
            chartContext.DynamicLoader.SetDynamicChartOptions(
                new ChartDataResults(ApplyLimitOnNumberOfSeries(request, seriesToReturn)));
        result.AppliedLimitation = request.AppliedLimitation;

        return result;
    }

    private ChartDataContext CreateChartContext(
        ChartDataRequest request,
        bool singleResult)
    {
        ChartDataServiceHelper.CheckViewLimitation(request);

        var chartContext = new ChartDataContext();

        chartContext.DynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        chartContext.DateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var originalDateRange = new DateRange() { StartDate = chartContext.DateRange.StartDate, EndDate = chartContext.DateRange.EndDate };
        const DateTimeKind dateFormatInDatabase = DateTimeKind.Utc;
        chartContext.DateRange = SetCustomDateRangeBasedOnRequest(request, chartContext.DateRange, dateFormatInDatabase);
        chartContext.BucketDateRange = chartContext.DynamicLoader.GetSamplingDateRange(chartContext.DateRange, originalDateRange);
        chartContext.SqlParameters = new Dictionary<string, object>()
        {
            {"StartTime", chartContext.DateRange.StartDate},
            {"EndTime", chartContext.DateRange.EndDate}
        };

        var netObjectIds = ChartDataServiceHelper.GetRequestedNetObjects(request);

        chartContext.GroupBy = ChartDataServiceHelper.GroupBy.Application;

        var groupByProperty = ChartDataServiceHelper.TryGetResourceProperty(request, ResourcePropertyKeys.GroupBy);
        if (groupByProperty == ResourcePropertyKeys.GroupByApp)
            chartContext.GroupBy = ChartDataServiceHelper.GroupBy.Application;
        else if (groupByProperty == ResourcePropertyKeys.GroupByNode)
            chartContext.GroupBy = ChartDataServiceHelper.GroupBy.Node;

        var providerFilter = string.Empty;
        var resourceHost = ChartDataServiceHelper.TryGetSettings<string>(request, ResourcePropertyKeys.ResourceHost);
        if (resourceHost == "N")
        {   //Chart is placed on Node resource host
            providerFilter = string.Format("s.NodeID IN ({0})", string.Join(", ", netObjectIds));
            chartContext.IdColumn = "NodeID";
        }
        else if (resourceHost == DpiApplication.Prefix)
        {   //Chart is placed on DPI Application Resource host
            providerFilter = string.Format("s.ApplicationID IN ({0})", string.Join(", ", netObjectIds));
            chartContext.IdColumn = "ApplicationID";
        }
        else if (singleResult && (resourceHost == null) && (groupByProperty == null))  //Chart is placed on Baseline Threshold details dialog
        {
            providerFilter = string.Format("s.ApplicationID IN ({0})", string.Join(", ", netObjectIds));
            chartContext.IdColumn = "ApplicationID";

        }

        var whereConditions = new List<string>();

        if (!string.IsNullOrWhiteSpace(providerFilter))     //Chart is on App/Node provider
            whereConditions.Add(providerFilter);
        else                                                //Chart is on summary page (not on supported resource host)
        {
            var prods = DetailViewsHelper.GetProductivityRatingsFromChartRequest(request);
            var risks = DetailViewsHelper.GetRiskLevelsFromChartRequest(request);
            var categs = DetailViewsHelper.GetCategoriesFromChartRequest(request);

            if (prods.Any())
                whereConditions.Add(string.Format(CultureInfo.InvariantCulture, "a.ProductivityRating IN ({0})", string.Join(", ", prods)));
            if (risks.Any())
                whereConditions.Add(string.Format(CultureInfo.InvariantCulture, "a.RiskLevel IN ({0})", string.Join(", ", risks)));
            if (categs.Any())
                whereConditions.Add(string.Format(CultureInfo.InvariantCulture, "a.CategoryID IN ({0})", string.Join(", ", categs)));
        }

        chartContext.WhereStatement = "1=1";
        if (whereConditions.Any())
            chartContext.WhereStatement = string.Join(" AND ", whereConditions);

        switch (chartContext.GroupBy)
        {
            case ChartDataServiceHelper.GroupBy.Application:
                chartContext.GroupByColumn = "ApplicationID";
                break;
            case ChartDataServiceHelper.GroupBy.Node:
                chartContext.GroupByColumn = "NodeID";
                break;
            default:
                throw new ArgumentException("groupBy not supported");
        }

        return chartContext;
    }

    private ChartDataResults GetTopXXData(ChartDataRequest request, ChartDataContext chartContext, string valueColumn, string rowCountColumn, Func<IEnumerable<DataPoint>, double> sampleMethod)
    {
        var sql =
//Add AS s and use provider filter
@"SELECT s.{0}, DateTime, {1}, {2}
FROM DPI_QoeStatistics AS s WITH (NOLOCK)
INNER JOIN DPI_Applications AS a ON a.ApplicationID = s.ApplicationID
INNER JOIN Nodes WITH (NOLOCK) ON s.NodeID = Nodes.NodeID
WHERE ${{LIMITATION}} AND DateTime >= @StartTime AND DateTime <= @EndTime AND {3} AND {4} AND {2} <> 0
ORDER BY s.{0}, DateTime";

        sql = string.Format(sql,
            chartContext.GroupByColumn,
            valueColumn,
            rowCountColumn,
            chartContext.WhereStatement,
            chartContext.LegendValues.Any() ? string.Format("s.{0} IN ({1})", chartContext.GroupByColumn, string.Join(", ", chartContext.LegendValues.Select(l => l.Id))) : "1=1");

        return GetSampledSeries(request, chartContext, sql, sampleMethod);
    }

    private ChartDataResults GetSingleLineData(
        ChartDataRequest request,
        ChartDataContext chartContext,
        string historySqlColumn,
        string recordCountSqlColumn,
        Func<IEnumerable<DataPoint>, double> sampleMethod)
    {
        var sql = string.Format(
@"SELECT s.{0}, DateTime, {1} AS Value, {2}
FROM DPI_QoeStatistics AS s WITH (NOLOCK)
INNER JOIN Nodes WITH (NOLOCK) ON s.NodeID = Nodes.NodeID
INNER JOIN DPI_Applications AS a WITH (NOLOCK) ON s.ApplicationID = a.ApplicationID
WHERE ${{LIMITATION}} AND DateTime >= @StartTime AND DateTime <= @EndTime AND {3} AND {2} <> 0
ORDER BY s.{4}, DateTime",
                chartContext.IdColumn,
                historySqlColumn,
                recordCountSqlColumn,
                chartContext.WhereStatement,
                chartContext.GroupByColumn);

        return GetSampledSeries(request, chartContext, sql, sampleMethod);
    }

    private ChartDataResults GetMultipleDataSeries(ChartDataRequest request,
        string[] legendSqlColumns,
        List<string> legendColumnTitles,
        string thresholdName,
        string orderByColumn,
        string historySqlColumn,
        string recordCountSqlColumn,
        Func<IEnumerable<DataPoint>, double> samplingMethod)
    {
        try
        {
            var sw = new System.Diagnostics.Stopwatch();

            var chartContext = CreateChartContext(request, false);
            sw.Start();
            ChartDataServiceHelper.GetTopXXRecords(request, chartContext, legendSqlColumns, orderByColumn, thresholdName);
            sw.Stop();
            log.TraceFormat("Getting list of Top10 entities took {0}ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var chartResult = GetTopXXData(request, chartContext, historySqlColumn, recordCountSqlColumn, samplingMethod);
            sw.Stop();
            log.TraceFormat("Getting historical data for Top10 took {0}ms", sw.ElapsedMilliseconds);

            sw.Restart();
            var iconUrl = ChartDataServiceHelper.GetLegendIconUrl(chartContext.GroupBy);
            var netObjectUrl = ChartDataServiceHelper.GetLegendLink(chartContext.GroupBy);

            List<DataSeries> DSresult = new List<DataSeries>();
            foreach (var lvi in chartContext.LegendValues)
            {
                var serie = chartResult.DataSeries.SingleOrDefault(ds => ds.NetObjectId == lvi.Id.ToString(CultureInfo.InvariantCulture));
                if (serie == null)
                {
                    serie = new DataSeries();
                    serie.AddPoint(DataPoint.CreateNullPoint(DateTime.UtcNow));
                }

                serie.Label = lvi.Name;

                string currentDsThreshold = string.Empty;
                if ((lvi.ColumnValues != null) && (lvi.ColumnValues.Count > 0))
                {
                    var valueForThreshold = lvi.ColumnValues[0];
                    currentDsThreshold = ChartDataServiceHelper.GetThresholdForDataSerie(lvi.Threshold, valueForThreshold);
                }

                serie.CustomProperties = new DataSeriesMetadata
                {
                    TableLegendValues = lvi.ColumnValues,
                    IconUrl = string.Format(CultureInfo.InvariantCulture, iconUrl, lvi.Status),
                    NetObjectUrl = string.Format(CultureInfo.InvariantCulture, netObjectUrl, lvi.Id),
                    Threshold = currentDsThreshold,
                };
                DSresult.Add(serie);
            }

            chartResult.DataSeries = DSresult;

            legendColumnTitles.Insert(0, (chartContext.GroupBy == ChartDataServiceHelper.GroupBy.Node) ? DPIWebContent.Charts_NodeLabel : DPIWebContent.Charts_ApplicationLabel);
            chartResult.ChartOptionsOverride = new
            {
                TableLegend = new
                {
                    ColumnHeaders = legendColumnTitles
                }
            };
            sw.Stop();
            log.TraceFormat("Formatting Top10 results took {0}ms", sw.ElapsedMilliseconds);
            return chartResult;
        }
        catch (Exception ex)
        {
            log.Error("Error occurred when trying to get chart data", ex);
            throw ResourceException.CreateResourceException(ex);
        }
    }

    private ChartDataResults GetSingleDataSerie(ChartDataRequest request, Func<IEnumerable<DataPoint>, double> samplingMethod, string historySqlColumn, string recordCountSqlColumn)
    {
        var chartContext = CreateChartContext(request, true);
        var chartResult = GetSingleLineData(request, chartContext, historySqlColumn, recordCountSqlColumn, samplingMethod);

        if (chartResult.DataSeries.Any())
        {
            string swqlQuery;
            if (chartContext.IdColumn.Contains("ApplicationID"))
                swqlQuery = @"SELECT ApplicationID, DisplayName FROM Orion.DPI.Applications WHERE ApplicationID IN ({0})";
            else
                swqlQuery = @"SELECT NodeID, DisplayName FROM Orion.Nodes WHERE NodeID IN ({0})";

            using (var swis = InformationServiceProxy.CreateV3())
            {
                swqlQuery = string.Format(CultureInfo.InvariantCulture, swqlQuery, string.Join(", ", chartResult.DataSeries.Select(ds => ds.NetObjectId)));
                var swisResult = swis.Query(swqlQuery)
                                      .Rows.Cast<DataRow>()
                                      .ToDictionary(
                                          dr => dr.Field<int>(0).ToString(CultureInfo.InvariantCulture),
                                          dr => dr.Field<string>(1));
                foreach (var ds in chartResult.DataSeries)
                {
                    string dsName;
                    if (swisResult.TryGetValue(ds.NetObjectId, out dsName))
                        ds.Label = dsName;
                }
            }
        }
        return chartResult;
    }

    [WebMethod]
    public ChartDataResults GetTopDataVolume(ChartDataRequest request)
    {
        return GetMultipleDataSeries(
           request,
           new[] { "(SUM(s.Avg_IngressPerSec + s.Avg_EgressPerSec) / SUM(s.RecordCount))*8 AS DPS", "SUM(s.Ingress + s.Egress) AS Sum_Data" },
           new List<string> { DPIWebContent.Charts_TopDataVolume_Average, DPIWebContent.Charts_TopDataVolume_Total },
           Constants.ThresholdNameDataVolumePerSec,
           "DPS",
           "Ingress + Egress", "RecordCount",
           SampleMethod.Total);
    }

    [WebMethod]
    public ChartDataResults GetTopTransactions(ChartDataRequest request)
    {
        return GetMultipleDataSeries(
            request,
            new[] { "SUM(s.Avg_TransactionsPerMin * s.RecordCount) / SUM(s.RecordCount) AS TPM", "SUM(s.Transactions) AS Sum_Trans" },
            new List<string> { DPIWebContent.Charts_TopTransactions_Average, DPIWebContent.Charts_TopTransactions_Total },
            Constants.ThresholdNameTransactionsPerMin,
            "TPM",
            "Transactions", "RecordCount",
            SampleMethod.Total);
    }

    [WebMethod]
    public ChartDataResults GetTopNRT(ChartDataRequest request)
    {
        return GetMultipleDataSeries(
            request,
            new[] { "CASE COALESCE(SUM(RecordCount_NRT), 0) WHEN 0 THEN -2 ELSE SUM(s.Avg_NRT * s.RecordCount_NRT) / SUM(s.RecordCount_NRT) END AS NRT", "MAX(s.Max_NRT) AS Max_NRT" },
            new List<string> { DPIWebContent.Charts_TopNRT_Average, DPIWebContent.Charts_TopNRT_Peak },
            Constants.ThresholdNameNRT,
            "NRT",
            "Avg_NRT", "RecordCount_NRT",
            WeightedAverage);
    }

    [WebMethod]
    public ChartDataResults GetTopART(ChartDataRequest request)
    {
        return GetMultipleDataSeries(
            request,
            new[] { "CASE COALESCE(SUM(RecordCount_ART), 0) WHEN 0 THEN -2 ELSE SUM(s.Avg_ART * s.RecordCount_ART) / SUM(s.RecordCount_ART) END AS ART", "MAX(s.Max_ART) AS Max_ART" },
            new List<string> { DPIWebContent.Charts_TopART_Average, DPIWebContent.Charts_TopART_Peak },
            Constants.ThresholdNameART,
            "ART",
            "Avg_ART", "RecordCount_ART",
            WeightedAverage);
    }

    [WebMethod]
    public ChartDataResults GetSingleDataVolume(ChartDataRequest request)
    {
        return GetSingleDataSerie(request, SampleMethod.Total, "Ingress + Egress", "RecordCount");
    }

    [WebMethod]
    public ChartDataResults GetSingleTransactions(ChartDataRequest request)
    {
        return GetSingleDataSerie(request, SampleMethod.Total, "Transactions", "RecordCount");
    }

    [WebMethod]
    public ChartDataResults GetSingleART(ChartDataRequest request)
    {
        return GetSingleDataSerie(request, WeightedAverage, "Avg_ART", "RecordCount_ART");
    }

    [WebMethod]
    public ChartDataResults GetSingleNRT(ChartDataRequest request)
    {
        return GetSingleDataSerie(request, WeightedAverage, "Avg_NRT", "RecordCount_NRT");
    }

    [WebMethod]
    public ChartDataResults GetTrafficByRiskLevel(ChartDataRequest request)
    {
        return ChartDataServiceHelper.GetPieChartData(request, "RiskLevel", Constants.RiskLevels, Constants.RiskLevelsColors, null, 5);
    }

    [WebMethod]
    public ChartDataResults GetTrafficByProductivityRating(ChartDataRequest request)
    {
        return ChartDataServiceHelper.GetPieChartData(request, "ProductivityRating", Constants.ProductivityRatings, Constants.ProductivityRatingsColors, null, 5);
    }

    [WebMethod]
    public ChartDataResults GetTrafficByCategory(ChartDataRequest request)
    {
        return ChartDataServiceHelper.GetPieChartData(request, "CategoryID", SolarWinds.DPI.Web.DALs.Swis.ApplicationCategory.GetAll().ToDictionary(c => c.CategoryID, c => c.Name), null, "DESC", 0);
    }

    private static double WeightedAverage(IEnumerable<DataPoint> values)
    {
        var denominator = values.Sum(x => x[2]);
        if (denominator == 0)
            return 0;
        else
            return values.Sum(x => x.Value * x[2]) / denominator;
    }
}