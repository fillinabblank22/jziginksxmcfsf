<%@ WebService Language="C#" Class="CollectorNodes" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Resources;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Common.Models;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class CollectorNodes : WebService
{
    private readonly CredentialManager _credentialManager = new CredentialManager();

    public class Result
    {

        public int ResultCount;
        public bool Success = true;
        public List<object> Items;
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public Result GetProbes(int start, int limit, Dictionary<string, object>[] sort, Dictionary<string, object>[] filter, int[] skipIds, bool allTrafficFirst)
    {
        object orderBy = "ProbeID";
        object orderDirection = "ASC";

        if (sort.Length > 0)
        {
            sort[0].TryGetValue("property", out orderBy);
            sort[0].TryGetValue("direction", out orderDirection);
        }

        var whereClause = (skipIds.Any()) ? string.Format(" WHERE ProbeID NOT IN ({0}) ", string.Join(", ", skipIds)) : string.Empty;
        var searchString = getSearchValueFromObject(filter);

        if (searchString.Length > 0)
        {
            var sqlFilter = getSingleSearchFilter("Name", searchString);
            whereClause += (skipIds.Any()) ? string.Format(" AND {0}", sqlFilter) : string.Format(" WHERE {0} ", sqlFilter);
        }

        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = string.Format(@"SELECT
                    ProbeID,
					AgentID,
                    Name,
                    Enabled,
                    Mode,
					(SELECT COUNT(NodeID) AS cnt FROM Orion.DPI.ProbeAssignments AS pa WHERE pa.ProbeID = p.ProbeID) AS NodeCount,
    				(SELECT TOP 1 Value AS pv1 FROM Orion.DPI.ProbeProperties AS pp WHERE pp.ProbeID = p.ProbeID AND pp.Name = @pLoad) AS Utilization,
                    Status AS ProbeStatus,
    				(SELECT TOP 1 a.ConnectionStatusMessage FROM Orion.AgentManagement.Agent a WHERE a.AgentId = p.AgentId) AS AgentStatus,
                    (SELECT TOP 1 ap.StatusMessage FROM Orion.AgentManagement.AgentPlugin ap WHERE ap.AgentId = p.AgentId AND PluginId = 'DPIProbe') AS PluginStatusDescription
	    		 FROM Orion.DPI.Probes p
                 {0}
                 ORDER BY {1} {2} {3} WITH ROWS {4} TO {5} WITH TOTALROWS WITH NOLOCK",
                    whereClause,
                    allTrafficFirst ? "Mode DESC, " : string.Empty,
                    orderBy,
                    orderDirection,
                    start + 1,
                    start + limit);
            var parameters = new Dictionary<string, object>()
            {
                {"sInterfaceName", SolarWinds.DPI.Common.ProbeSettingNames.CapturingInterface},
                {"pLoad", SolarWinds.DPI.Common.Models.HealthStatsResult.PerfCpuUsed},
            };

            var data = swis3.Query(query, parameters);

            var result = new Result();
            if (data.ExtendedProperties["TotalRows"] == null)
                result.ResultCount = 0;
            else
                result.ResultCount = int.Parse(data.ExtendedProperties["TotalRows"].ToString());

            var rnd = new System.Random(System.Environment.TickCount);

            result.Items = data.Rows.Cast<DataRow>().Select(r =>
            {
                var probeStatus = r.Field<short>("ProbeStatus");
                string pluginStatus = string.Empty;
                switch (probeStatus)
                {
                    case 28:
                        pluginStatus = DPIWebContent.Status_Unlicensed;
                        break;
                    case 29:
                        pluginStatus = DPIWebContent.Status_InstallationFailed;
                        break;
                    default:
                        pluginStatus = SolarWinds.Orion.Core.Common.OrionObjectStatusHelper.GetLocalizedStatusText(probeStatus);
                        break;
                }

                var strCpu = r.Field<string>("Utilization");
                double dblCpu;
                if (double.TryParse(strCpu, NumberStyles.Float, CultureInfo.InvariantCulture, out dblCpu))
                    strCpu = dblCpu.ToString("N1", CultureInfo.CurrentCulture);

                return new
                {
                    ProbeID = r.Field<int>("ProbeID"),
                    AgentID = r.Field<int>("AgentID"),
                    Name = r.Field<string>("Name"),
                    Enabled = r.Field<bool>("Enabled"),
                    Mode = r.Field<byte>("Mode"),
                    DpiNodeCount = r.Field<int>("NodeCount"),
                    Utilization = strCpu,
                    AgentStatus = r.IsNull("AgentStatus") ? string.Empty : r.Field<string>("AgentStatus"),
                    PluginStatus = pluginStatus,
                    PluginStatusNumeric = probeStatus,
                    PluginStatusDescription = r.IsNull("PluginStatusDescription") ? string.Empty : r.Field<string>("PluginStatusDescription")
                };
            })
                                        .ToList<object>();
            return result;
        }
    }

    [WebMethod]
    public List<object> GetNodesByProbeId(int probeId)
    {
        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT
	                            pa.NodeID,
	                            pa.Node.DisplayName AS NodeName,
                                a.ApplicationID AS AppID,
	                            a.DisplayName AS AppName
                            FROM Orion.DPI.ProbeAssignments AS pa
                            LEFT JOIN Orion.DPI.ApplicationAssignments AS aa
	                            ON aa.NodeID = pa.NodeID AND aa.ApplicationID IN
                                (SELECT iaa.ApplicationID FROM Orion.DPI.ApplicationAssignments AS iaa WHERE iaa.NodeID = pa.NodeID)
                            LEFT JOIN Orion.DPI.Applications AS a on aa.ApplicationID = a.ApplicationID
                            WHERE pa.Node.NodeID IS NOT NULL AND pa.ProbeID = @probeID
                            ORDER BY a.DisplayName";
            var parameters = new Dictionary<string, object>()
            {
                {"probeID", probeId}
            };
            var data = swis3.Query(query, parameters);

            return data.Rows.Cast<DataRow>()
                .GroupBy(r => r.Field<int>("NodeID"))
                .Select(g =>
                {
                    return new
                    {
                        NodeID = g.Key,
                        NodeName = g.First().Field<string>("NodeName"),
                        Apps = g.Where(r => !r.IsNull("AppID")).Select(r => new
                        {
                            ID = r.Field<int>("AppID"),
                            Name = r.Field<string>("Appname")
                        }),
                    };
                })
                .ToList<object>();

        }
    }

    [WebMethod]
    public List<object> GetAppsByNodeId(int nodeId)
    {
        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            string query = @"SELECT aa.Application.Status, aa.Application.Name
                             FROM Orion.DPI.ApplicationAssignments AS aa
                             WHERE aa.NodeID = @nodeID";
            var parameters = new Dictionary<string, object>()
            {
                {"nodeID", nodeId}
            };
            var data = swis3.Query(query, parameters);

            return data.Rows.Cast<DataRow>()
                .Select(a =>
                {
                    return new
                    {
                        Status = a.Field<int>("Status"),
                        Name = a.Field<string>("Name")
                    };
                })
                .ToList<object>();
        }
    }

    [WebMethod]
    public void SetProbesEnabled(IEnumerable<int> probeIds, bool enabled)
    {
        using (var bl = new DPIBusinessLayerProxy())
        {
            bl.SetProbesEnabled(probeIds, enabled);
        }
    }

    //[WebMethod]
    //public void DeleteProbe(int probeId)
    //{
    //    using (var bl = new DPIBusinessLayerProxy())
    //    {
    //        bl.DeleteProbe(probeId);
    //    }
    //}

    [WebMethod]
    public void DeleteProbes(IEnumerable<int> probeIds)
    {
        using (var bl = new DPIBusinessLayerProxy())
        {
            bl.DeleteProbes(probeIds);
        }
    }

    [WebMethod]
    public void RemoveNodeFromProbe(int nodeId, int probeId)
    {
        using (var bl = new DPIBusinessLayerProxy())
        {
            bl.DeleteProbeAssignment(probeId, nodeId);
        }
    }

    [WebMethod]
    public DPIProbeEditModel GetProbeForEdit(int probeId)
    {
        var result = new DPIProbeEditModel();

        using (var bl = new DPIBusinessLayerProxy())
        {
            result.UserConfiguration = bl.GetProbeConfiguration(probeId);
            result.Capabilities = bl.GetProbeCapabilities(probeId);
        }

        using (var swis = InformationServiceProxy.CreateV3())
        {
            var data = swis.Query("SELECT Mode FROM Orion.DPI.Probes WHERE ProbeID = @probeId",
                       new Dictionary<string, object> { { "probeId", probeId } });

            if (data.Rows.Count > 0)
                result.LocalTrafficOnly = Convert.ToInt32(data.Rows[0][0]) == 1;
        }

        return result;
    }

    [WebMethod]
    public void SaveProbeForEdit(DPIProbeEditModel model)
    {
        var probeConfiguration = new DpiProbeConfiguration();
        probeConfiguration.ProbeId = model.UserConfiguration.ProbeId;
        probeConfiguration.ProbeName = model.UserConfiguration.Name;

        var probeSettings = new DbProbeSettings();
        probeConfiguration.ProbeSettings = probeSettings;
        probeSettings.UserMemoryLimitMb = model.UserConfiguration.MemoryLimit;
        probeSettings.ProcessingThreads = model.UserConfiguration.CpuLimit;
        probeSettings.CapturingInterface = model.UserConfiguration.CapturingInterface;

        using (var bl = new DPIBusinessLayerProxy(HttpContext.Current.Profile.UserName))
        {
            bl.SetProbeConfiguration(model.UserConfiguration);
        }
    }

    [WebMethod]
    public ProbeDeploymentResult DeployRemoteProbe(int nodeId, int credentialId, string credentialName, string credentialPass, bool allTraffic)
    {
        string nodeName;
        using (var swis3 = InformationServiceProxy.CreateV3())
        {
            var data = swis3.Query(String.Format("SELECT NodeName, Status FROM Orion.Nodes WHERE NodeID = {0}", nodeId));
            nodeName = data.Rows[0].Field<string>("NodeName");
        }

        using (var bl = new DPIBusinessLayerProxy(HttpContext.Current.Profile.UserName))
        {
            if (credentialId != -1)
            {
                var credential = _credentialManager.GetCredential<UsernamePasswordCredential>(credentialId);
                credentialName = credential.Username;
                credentialPass = credential.Password;
            }

            var rv = bl.DeployRemoteProbe(nodeId, credentialName, credentialPass, allTraffic ? ProbeMode.AllTraffic : ProbeMode.LocalTraffic, string.Format(DPIWebContent.DefaultCollectorName, nodeName), String.Empty);
            if (rv.ProbeId > 0 && !allTraffic) // succeeded
            {
                using (var swis = InformationServiceProxy.CreateV3())
                {
                    // we need to assign http/* application
                    var httpApplications = swis.Query("SELECT ApplicationID FROM " +
                                                      "Orion.DPI.Applications " +
                                                      "WHERE ProtocolID='HTTP' " +
                                                      "AND Filter='*'");

                    if (httpApplications.Rows.Count > 0) // already exist
                    {
                        int appId = Convert.ToInt32(httpApplications.Rows[0][0]);

                        var node = bl.GetDpiNode(nodeId);
                        node.QoEApps = new[] { new DpiApplicationAssignment(appId, nodeId) }.ToList();
                        bl.SetDpiNode(node);
                    }
                    else // doesn't exist. create one
                    {
                        var newApp = new DpiApplication();
                        var httpAppPrototype = // given prototype MUST exist
                            swis.Query("SELECT a.ProtocolID, a.Name, " +
                                       "a.Description, a.CategoryID, " +
                                       "a.RiskLevel, a.ProductivityRating " +
                                       "FROM Orion.DPI.ApplicationProtocols " +
                                       "a WHERE ProtocolID='HTTP'").Rows[0];

                        newApp.ProtocolID = Convert.ToString(httpAppPrototype["ProtocolID"]);
                        newApp.Name = Convert.ToString(httpAppPrototype["Name"]);
                        newApp.Description = Convert.ToString(httpAppPrototype["Description"]);
                        newApp.CategoryID = Convert.ToInt32(httpAppPrototype["CategoryID"]);
                        newApp.RiskLevel = Convert.ToByte(httpAppPrototype["RiskLevel"]);
                        newApp.ProductivityRating = Convert.ToByte(httpAppPrototype["ProductivityRating"]);
                        newApp.Filter = "*";
                        newApp.ApplicationType = ApplicationType.CustomHttp;
                        newApp.Thresholds = new List<DpiApplicationThreshold>();
                        newApp.Nodes = new[] { new DpiApplicationAssignment(-1, nodeId) }.ToList();

                        bl.SetApplication(newApp);
                    }
                }
            }
            return rv;
        }
    }

    private string getSingleSearchFilter(string searchField, string searchVal)
    {
        return String.Format(" {0} LIKE '%{1}%' ", searchField, searchVal);
    }

    private string getSearchValueFromObject(Dictionary<string, object>[] filter)
    {
        object filterValue = String.Empty;

        if (filter.Length > 0)
        {
            filter[0].TryGetValue("value", out filterValue);
        }

        return Convert.ToString(filterValue);
    }

    public class DPIProbeEditModel
    {
        public ProbeUserConfiguration UserConfiguration { get; set; }

        public ProbeCapabilities Capabilities { get; set; }

        public bool LocalTrafficOnly { get; set; }
    }
}
