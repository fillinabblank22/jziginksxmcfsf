﻿<%@ WebService Language="C#" Class="DeploymentSettings" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Globalization;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Castle.Core.Internal;
using SolarWinds.Data.Query.LogicalQueryPlan;
using SolarWinds.DPI.Common;
using SolarWinds.DPI.Common.Models;
using SolarWinds.DPI.Web.UI.Wizards.DPINode;
using SolarWinds.Logging;
using SolarWinds.Net.WMI;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.SharedCredentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Web.InformationService;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DeploymentSettings : WebService
{
    private static readonly Log _log = new Log();

    [WebMethod(EnableSession = true)]
    public DeploymentSettingsRow[] TestPluginRequirementsOnMachine(PluginRequirementTest[] items2Test)
    {
        var credentialManager = new CredentialManager();

        if (items2Test.Any(i => i.CredentialId > 0))
        {
            var credentials = credentialManager.GetCredentials<UsernamePasswordCredential>(
                items2Test.Select(i => i.CredentialId).Where(i => i > 0)
                ).ToDictionary(x => x.ID);

            foreach (var item in items2Test)
            {
                UsernamePasswordCredential creds;
                if (item.CredentialId > 0 && credentials.TryGetValue(item.CredentialId, out creds))
                {
                    item.UserName = creds.Username;
                    item.Password = creds.Password;
                }
            }
        }

        using (var bl = new DPIBusinessLayerProxy())
        {
            PluginRequirementTestResult[] testResults = bl.TestProbeRequirements(items2Test.ToArray());
            return testResults.Select(i => new DeploymentSettingsRow()
            {
                NodeId = i.NodeId,
                PluginRequirementTestResult = i.Result,
                PluginRequirementTestResultMessage = i.Message,
                CredentialTestMessage = i.Message
            }).ToArray();
        }
    }

    [WebMethod]
    public AgentInfo GetAgentInfo(int nodeId)
    {
        try
        {
            using (var bl = new DPIBusinessLayerProxy())
            {
                int nodeWmiCredentialsId = -1;
                string nodeWmiCredentialsName = null, nodeWmiCredentialsUserName = null;

                var nodeInfo = bl.GetNodeInformationByNodeId(nodeId);

                if (nodeInfo.Credentials != null)
                {
                    nodeWmiCredentialsId = nodeInfo.Credentials.ID.GetValueOrDefault();
                    nodeWmiCredentialsName = nodeInfo.Credentials.Name;
                    nodeWmiCredentialsUserName = nodeInfo.Credentials.Username;
                }

                return new AgentInfo()
                {
                    AgentId = nodeInfo.AgentId.HasValue ? nodeInfo.AgentId.Value : -1,
                    EngineId = nodeInfo.EngineId,
                    IsAgentInstalled = nodeInfo.AgentId.HasValue,
                    NodeWmiCredentialsId = nodeWmiCredentialsId,
                    NodeWmiCredentialsName = nodeWmiCredentialsName,
                    NodeWmiCredentialsUserName = nodeWmiCredentialsUserName
                };
            }
        }
        catch (Exception ex)
        {
            _log.Error("Unable to GetAgentInfo", ex);
            throw;
        }
    }
}