<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DPIAppDetails.aspx.cs" Inherits="Orion_DPI_DPIAppDetails" MasterPageFile="~/Orion/View.master" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="dpi" Assembly="SolarWinds.DPI.Web" Namespace="SolarWinds.DPI.Web.UI" %>

<asp:Content ID="SummaryTitle" ContentPlaceHolderID="ViewPageTitle" runat="Server">
    <h1><%= HttpUtility.HtmlEncode(DpiApplication.Name) %></h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <dpi:DPIAppResourceHost ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </dpi:DPIAppResourceHost>
</asp:Content>