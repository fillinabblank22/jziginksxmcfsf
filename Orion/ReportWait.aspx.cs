﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_ReportWait : System.Web.UI.Page
{
    private const string QueryStringParamWaitId = "waitid";

    public string AjaxStartUrl { get; set; }
    public string AjaxDestinationUrl { get; set; }
    public string AjaxRefreshUrl { get; set; }
    public string ReportName { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        int reportid;
        string reportIdTxt = Request.QueryString["ReportID"];

        if (string.IsNullOrWhiteSpace(reportIdTxt) || int.TryParse(reportIdTxt, out reportid) == false)
            throw new ArgumentException("URL must have a ReportID query string parameter");

        string rptName;

        if (TryGetReportName(reportid, out rptName) == false)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Redirect("/Orion/Reports/");
            return;
        }

        ReportName = rptName;

        var items = HttpUtility.ParseQueryString(Request.Url.Query);

        string ajaxIdTxt = Request.QueryString[QueryStringParamWaitId];
        Guid ajaxId;

        // first time in? let's generate a waitid.

        if (string.IsNullOrWhiteSpace(ajaxIdTxt) || Guid.TryParse(ajaxIdTxt, out ajaxId) == false)
        {
            items[QueryStringParamWaitId] = Guid.NewGuid().ToString("N");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Redirect("/Orion/ReportWait.aspx?" + items);
            return;
        }

        var d = SolarWinds.Orion.Core.Reporting.TempFiles.GetDetails(ajaxId);

        // user refreshed the page... and the report is rendered?

        if (d.Exists && d.IsInProgress == false)
        {
            if (items[QueryStringParamWaitId] != null)
                items.Remove(QueryStringParamWaitId);

            items["showid"] = ajaxId.ToString("N");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Redirect("/Orion/Report.aspx?" + items);
            return;
        }

        items[QueryStringParamWaitId] = ajaxId.ToString("N");
        AjaxRefreshUrl = "/Orion/Reports/Preview.aspx?" + items;
        AjaxStartUrl = AjaxRefreshUrl;

        if (items[QueryStringParamWaitId] != null)
            items.Remove(QueryStringParamWaitId);

        items["showid"] = ajaxId.ToString("N");
        AjaxDestinationUrl = "/Orion/Report.aspx?" + items;
    }

    bool TryGetReportName(int reportId, out string name)
    {
        var queryParams = new Dictionary<string, object>();

        name = null;

        using (var swis = SolarWinds.Orion.Web.InformationService.InformationServiceProxy.CreateV3())
        {
            queryParams[@"rptid"] = reportId;

            var results = swis.Query(@"SELECT r.Name AS RptName FROM Orion.Report r WHERE r.ReportId = @rptid", queryParams);

            if (results.Rows.Count < 1)
                return false;

            var v = results.Rows[0]["RptName"];

            name = v is DBNull ? null : (string) v;
            return true;
        }
    }
}