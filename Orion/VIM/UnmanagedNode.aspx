﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true" CodeFile="UnmanagedNode.aspx.cs" Inherits="Orion_VIM_UnmanagedNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" Runat="Server" >    
    <orion:Include ID="Include2" runat="server" Module="VIM" File="Controls/ManageNodeDialog.js" />
    <script type="text/javascript">
        function ManageNode() {
            if (<%= HasMultipleIPAddress.ToString().ToLower() %>)
                ManageNodeDialog(<%= ManageNodeDialogParams %>);
            else
                window.location.href = "<%= ManageNodeUrl %>";
            return false;
        }

        function DontManageNode() {
            if (window.history.length < 1)
                window.location.href = "/Orion/VIM/Summary.aspx";
            else
                window.history.back();

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="VimMainContentPlaceHolder" Runat="Server">
    <h1><%= PageTitle %></h1>        
    <div class="sw-vim-content-wrapper">
        <p>
            <%= Resources.VIMWebContent.VIMWEBDATA_LV0_41 %>
        </p>
        <div>
            <orion:LocalizableButton ID="ManageNode" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_42 %>" DisplayType="Primary" runat="server" CausesValidation="false" OnClientClick="return ManageNode();" />&nbsp;
            <orion:LocalizableButton ID="NotManageNode" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_43 %>" DisplayType="Secondary" runat="server" CausesValidation="false" OnClientClick="return DontManageNode();" />
        </div>    
    </div> 
</asp:Content>

