﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;

public partial class VIM_Admin_RetentionSettings : System.Web.UI.Page
{
    private const string SettingIDDetailedRetention = "VIM_Setting_Detailed_Retain";
    private const string SettingIDHourlyRetention = "VIM_Setting_Hourly_Retain";
    private const string SettingIDDailyRetention = "VIM_Setting_Daily_Retain";

    private const int DefaultDetailedRetention = 7;
    private const int DefaultHourlyRetention = 30;
    private const int DefaultDailyRetention = 365;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            detailedRetention.Text = SettingsDAL.GetCurrentInt(SettingIDDetailedRetention, DefaultDetailedRetention).ToString();
            hourlyRetention.Text = SettingsDAL.GetCurrentInt(SettingIDHourlyRetention, DefaultHourlyRetention).ToString();
            dailyRetention.Text = SettingsDAL.GetCurrentInt(SettingIDDailyRetention, DefaultDailyRetention).ToString();
        }
    }

    protected void Submit_Click(object source, EventArgs e)
    {
        var detailed = (float)Convert.ToDouble(detailedRetention.Text);
        var hourly = (float)Convert.ToDouble(hourlyRetention.Text);
        var daily = (float)Convert.ToDouble(dailyRetention.Text);

        SettingsDAL.Set(SettingIDDetailedRetention, detailed);
        SettingsDAL.Set(SettingIDHourlyRetention, hourly);
        SettingsDAL.Set(SettingIDDailyRetention, daily);

        ReferrerRedirectorBase.Return("/Orion/VIM/Admin/VirtualizationSettings.aspx");
    }

    protected void Cancel_Click(object source, EventArgs e)
    {
        ReferrerRedirectorBase.Return("/Orion/VIM/Admin/VirtualizationSettings.aspx");
    }
}
