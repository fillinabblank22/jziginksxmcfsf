﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" CodeFile="VirtualizationEntityProperties.aspx.cs" Inherits="Orion_VIM_Admin_VirtualizationEntityProperties" %>
<%@ Register TagPrefix="vim" TagName="ThresholdControl" Src="~/Orion/VIM/Controls/ThresholdControl.ascx" %>
<%@ Reference Control="~/Orion/Controls/ThresholdControl.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        .vimAdminDateArea
        {
            display: none;
        }        
    </style>
    
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <h1><%= Resources.VIMWebContent.VIMWEBDATA_LV0_40 %></h1>
    <div class="sw-hdr-subtitle"><%= PageSubtitle %></div>
    
    <div class="sw-pg-selected-items">
        <asp:BulletedList runat="server" ID="blEntities">
        </asp:BulletedList>
    </div>
   
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="vimTableWrapper">
                <tr><td>
                    <div class="GroupBox">
                        <div class="contentBlock thresholdControlWrapper">
                            <asp:PlaceHolder runat="server" ID="ThresholdControlPlaceholder"></asp:PlaceHolder>    
                            <div class="sw-btn-bar-wrapper">
                                <div class="sw-btn-bar">
                                    <orion:LocalizableButton id="imbtnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit" OnClick="SubmitBtn_Click" />
                                    <orion:LocalizableButton id="imbtnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelBtn_Click" CausesValidation="false"/>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </td></tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
