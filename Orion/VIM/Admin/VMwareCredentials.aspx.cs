﻿using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

public partial class Orion_VIM_Admin_VMwareCredentials : System.Web.UI.Page, IBypassAccessLimitation
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer(String.Format("~/Orion/Error.aspx?Message={0}", HttpUtility.UrlEncode(Resources.VIMWebContent.VIMWEBCODE_VB0_49)));
        }

        ((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VMwareCredentialsLibrary";
    }
}
