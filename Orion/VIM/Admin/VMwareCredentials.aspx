<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" CodeFile="VMwareCredentials.aspx.cs" 
    Inherits="Orion_VIM_Admin_VMwareCredentials" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_LC0_68%>" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/VIM/Admin/VMwareTabs.ascx" TagPrefix="vim" TagName="VMwareTabs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="VMwareCredentials.js" Module="VIM" />
    <orion:Include runat="server" File="DemoServerHelper.js" Module="VIM" />

    <style type="text/css">
        span.Label { white-space:nowrap; }
        .smallText {color:#979797;font-size:9px;}
        .add { background-image:url(/Orion/VIM/images/add_16x16.gif) !important; }
        .edit { background-image:url(/Orion/VIM/images/edit_16x16.gif) !important; }
        .del { background-image:url(/Orion/VIM/images/delete_16x16.gif) !important; }
        .vimAdminDateArea {display:none;}
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px;}
        #delDialog .dialogBody table td { padding-right: 10px; }
        #adminContent .dialogBody ul { margin-left: 20px; list-style-type: square; }
        #addDialog td.x-btn-mc, #delDialog td.x-btn-mc, #updateDialog td.x-btn-mc { text-align: center !important; }
        #addDialog td, #delDialog td, #updateDialog td { padding-right: 0; padding-bottom: 0; }
        #addDialogBody {height: auto !important;}
        #updateDialogBody {height: auto !important;}
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align: middle; }
        #adminContent { padding-top: 0; }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <input type="hidden" name="VIM_VMwareCredentials_PageSize" id="VIM_VMwareCredentials_PageSize" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_VMwareCredentials_PageSize")%>' />

    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
        <div id="isDemoMode" style="display:none;"></div>
    <%}%>

    <h1 style="font-size:large"><%= Page.Title %></h1>
    <div style="padding: 1em 0 1em 0;"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_117%></div>
    <div style="width:950px;">    
        <vim:VMwareTabs ID="VMwareTabs1" runat="server" />

        <div id="tabPanel" class="tab-top">
            <table class="ESXCredentialsTable" cellpadding="0" cellspacing="0" width="100%" >
                <tr valign="top" align="left">
                    <td id="gridCell" style="padding-right: 0px;">
                        <div id="Grid"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <div id="delDialog" class="x-hidden">
        <div class="x-window-header"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_118%></div>
        <div id="delDialogBody" class="x-panel-body dialogBody">
        <table>
            <tr>
                <td><div id="delDialogDescription"></div></td>
            </tr>
            <tr>
                <td><ul id="credentials"></ul></td>
            </tr>
        </table>
        <br />
        </div>
    </div>

    <div id="addDialog" class="x-hidden">
        <div class="x-window-header"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_1%></div>
        <div id="addDialogBody" class="x-panel-body dialogBody">
        <table>
            <tr>
                <td colspan="2"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_119%></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_3%></span></td>
                <td><input ID="addDescription" maxlength="50"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_7%></span></td>
                <td>
                    <input ID="addUsername" maxlength="50"></input>
                    <div class="smallText" style="max-width:130px;"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_8%></div>
                </td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%></span></td>
                <td><input ID="addPassword" maxlength="50" type="password"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_12%></span></td>
                <td><input ID="addConfirm" maxlength="50" type="password"></input></td>
            </tr>
            <tr>
                <td colspan='2'><span class="Error"></span></td>
            </tr>
        </table>
        </div>
    </div>

    <div id="updateDialog" class="x-hidden">
        <div class="x-window-header"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_120%></div>
        <div id="updateDialogBody" class="x-panel-body dialogBody">
        <input ID="updateID" type="hidden" style="display:none"></input>  
        <table>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_3%></span></td>
                <td><input ID="updateDescription" maxlength="50"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_7%></span></td>
                <td>
                    <input ID="updateUsername" maxlength="50"></input>
                       <div class="smallText" style="max-width:130px;"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_121%></div>
                </td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%></span></td>
                <td><input ID="updatePassword" maxlength="50" type="password"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_12%></span></td>
                <td><input ID="updateConfirm" maxlength="50" type="password"></input></td>
            </tr>
            <tr>
                <td colspan='2'><span class="Error"></span></td>
            </tr>
        </table>
        </div>
    </div>
    
</asp:Content>


