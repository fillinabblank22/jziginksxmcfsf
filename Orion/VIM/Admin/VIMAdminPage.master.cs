﻿using System;

public partial class Orion_VIM_Admin_VIMAdminPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(HelpFragment))
        {
            btnHelp.HelpUrlFragment = this.HelpFragment;
            btnHelp.Visible = true;
        }
    }

    public string HelpFragment { get; set; }
}
