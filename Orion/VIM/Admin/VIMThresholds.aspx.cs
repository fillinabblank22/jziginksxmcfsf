using System;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Web.DAL;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.VIM.Common.Models;
using SolarWinds.Orion.Web;
using System.Web;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using SolarWinds.VIM.Web.Controls;
using SolarWinds.VIM.Web.Enums;

public partial class Orion_VIM_Admin_VIMThresholds : System.Web.UI.Page
{
    protected Dictionary<VirtualizationSetting, string> settings = new Dictionary<VirtualizationSetting, string>();
    private string descriptions;
    private string descriptions_title;

    protected override void OnLoad(EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

        var vsettings = new VirtualizationSettingsDAL().GetVirtualizationSettings();
        List<VirtualizationSetting> settings = null;
        if (vsettings.Count == 0) // prevent displaying Settings header on empty settings list
            vsettings = null;


        EnsureChildControls();

        // well, I need to create the order by hand. HACK, please, when adding another settings, think of better solution
        if (vsettings != null)
        {
            string[] ordr =
                new string[]
                {
                    "Virtualization-VRM-Forecasting-RedInterval",
                    "Virtualization-VRM-Forecasting-Interval",
                    "Virtualization-VRM-Optimalcapacity-Sampleinterval"
                };

            settings = new List<VirtualizationSetting>();
            foreach (var or in ordr)
            {
                settings.AddRange(vsettings.Where(x => x.Name == or));
            }

            settings.AddRange(vsettings.Where(v => ordr.All(x => x != v.Name)));

        }
        this.SettingsRepeater.DataSource = settings;
        

        this.SettingsRepeater.DataBind();

        base.OnLoad(e);
    }

    protected void OnSettingsRepeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        var item = e.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox tb = (TextBox)item.FindControl("valueTB") as TextBox;
            ListBox listBox = (ListBox)item.FindControl("ListBox") as ListBox;
            Panel panelText = (Panel)item.FindControl("textPanel") as Panel;
            Panel panelList = (Panel)item.FindControl("listPanel") as Panel;
            VirtualizationSetting setting = (VirtualizationSetting)item.DataItem;
            if (setting.Name == "Virtualization-VRM-Optimalcapacity-Sampleinterval")
            {
                panelList.Visible = true;
                panelText.Visible = false;
                
                listBox.SelectedValue = setting.Value.ToString();

                var optimalCapacity = _thresholdControls.Where(x => x.Key.Threshold.TypeId == 102).Select(x => x.Value).FirstOrDefault();
                if (optimalCapacity != null)
                {
                    var tr = item.FindControl("settingItemRow") as System.Web.UI.HtmlControls.HtmlTableRow; // we remove class from the tr, if it has any
                    tr.Attributes["class"] = "";
                    optimalCapacity.FindControl("warningPlaceholder").Controls.Add(item);
                }

                this.settings.Add(setting, listBox.ClientID.Replace("_", "$"));
            }
            else
            {
                panelList.Visible = false;
                panelText.Visible = true;
                
                tb.Text = setting.Value.ToString();

                if (setting.Name == "Virtualization-VRM-Forecasting-RedInterval") // ugly hack .but there is no other way how to do it 
                {
                    // description should be in 'text1;text2' format.
                    // the text is inserted so it is displayed like text1 <textbox> text2.
                    var descr =setting.Description;
                    if (!descr.Contains(';'))
                        descr = ';' + descr;

                    var desc = descr.Split(';');

                    Label lb = (Label)item.FindControl("rangeLB");
                    lb.Text = desc[1];

                    Label tbtb = (Label)item.FindControl("textBeforeTB");
                    tbtb.Text = desc[0];

                    item.FindControl("descLB").Visible = false;
                }

                this.settings.Add(setting, tb.ClientID.Replace("_", "$"));
            }
        }
    }

    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        foreach (var threshold in Thresholds)
        {
            var tc = (ThresholdSettingBase)LoadControl("~/Orion/VIM/Controls/ThresholdSetting.ascx");
            tc.Setting = threshold.Threshold;
            tc.ThresholdHeaderValue = threshold.Name;
            tc.HasWarning = threshold.HasWarningLevel;
            tc.Description = threshold.Description;

            // order After or Before settings?
            (!threshold.AfterSettings ? thresholdsHere : thresholdsHere2).Controls.Add(tc);
           
            _thresholdControls[threshold] = tc;
        }


        if (!string.IsNullOrEmpty(descriptions))
        {
            thresholds_description.Controls.Clear();
            thresholds_description.Controls.Add(new LiteralControl(descriptions));
        }

        if (!string.IsNullOrEmpty(descriptions_title))
        {
            thresholds_title.Text = descriptions_title;
        }
        else
        {
            thresholds_title.Text = Page.Title;
        }
        
    }

    private List<DisplayedThresholds> _thresholds;
    protected IEnumerable<DisplayedThresholds> Thresholds
    {
        get
        {
            if (_thresholds == null)
                _thresholds = LoadThresholdSettings().ToList();

            return _thresholds;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
         ((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VirtualizationThresholds";
    }

    private Dictionary<DisplayedThresholds, ThresholdSettingBase> _thresholdControls = new Dictionary<DisplayedThresholds, ThresholdSettingBase>();

    protected struct DisplayedThresholds
    {
        public Threshold Threshold;
        public string Name;
        public bool AfterSettings;
        public bool HasWarningLevel;
        public string Description;
    }

    private IEnumerable<DisplayedThresholds> LoadThresholdSettings()
    {
        var _modules = SolarWinds.Orion.Web.OrionModuleManager.GetModules();
        foreach (string str in Directory.GetDirectories(HttpContext.Current.Server.MapPath("/Orion")))
        {
            DirectoryInfo info = new DirectoryInfo(str);
            string path = str + @"\" + info.Name + ".config";
            if (File.Exists(path) && _modules.Any(x => x.Name == info.Name && x.Enabled) )
            {
                XDocument document = XDocument.Load(path);
                foreach (var c in LoadModuleThresholds(info.Name, document))
                    yield return c;
            }
        }
    }

    private IEnumerable<DisplayedThresholds> LoadModuleThresholds(string p, XDocument document)
    {
        if (document.Root.Element("thresholds") != null)
        {
            if (document.Root.Element("thresholds").Element("description") != null)
            {
                descriptions = string.Join("", document.Root.Element("thresholds").Element("description").DescendantNodes().Select(x => x.ToString()).ToArray());
                if (document.Root.Element("thresholds").Element("description").Attribute("title") != null)
                {
                    descriptions_title = (document.Root.Element("thresholds").Element("description").Attribute("title").Value);
                }
            }

            


            return document.Root.Element("thresholds").Elements("threshold").Select(x => 
                new DisplayedThresholds() { Threshold = VMwareThresholdCache.Thresholds[new ThresholdType(int.Parse(x.Attribute("id").Value))],
                    Name = SolarWinds.Orion.Core.Common.i18n.TokenSubstitution.Parse(x.Attribute("name").Value),
                    AfterSettings = (x.Attribute("afterSettings") != null && x.Attribute("afterSettings").Value == "true"),
                    HasWarningLevel = (x.Attribute("withoutWarning") == null || x.Attribute("withoutWarning").Value != "true"),
                    Description = (x.Attribute("description") != null ? x.Attribute("description").Value : null),
                });

        }
        else{
            return new DisplayedThresholds[] {};
        }
    }


    protected void SubmitClick(object sender, EventArgs e)
    {
        List<Threshold> thresholds = new List<Threshold>();
        foreach (var tc in _thresholdControls)
        {
            tc.Value.Setting.Warning = float.Parse(tc.Value.WarningTxt);
            tc.Value.Setting.High = float.Parse(tc.Value.HighTxt);
            thresholds.Add(tc.Value.Setting);
        }

        foreach (VirtualizationSetting s in this.settings.Keys)
        {
            s.Value = Convert.ToSingle(Request.Form.GetValues(settings[s]).First());
            s.Update();
        }

/*        List<Threshold> thresholds = new List<Threshold>();
        
        networkUtilization.Setting.Warning = float.Parse(networkUtilization.WarningTxt);
        networkUtilization.Setting.High = float.Parse(networkUtilization.HighTxt);
        thresholds.Add(networkUtilization.Setting);*/
        VMwareThresholdCache.UpdateThreshold(thresholds);
        ReferrerRedirectorBase.Return("/Orion/Admin/");
    }
}
