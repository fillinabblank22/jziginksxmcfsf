﻿using System;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.VIM.Base.Contract.Constants;

public partial class Orion_VIM_Admin_EditViews_EditVManAccessRight : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override string PropertyValue { get; set; }

    protected string HelpLink
    {
        get { return HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, "OrionVMPHLearnMoreVManRoles"); }
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var hasAccessRight = false;
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                if (!Boolean.TryParse(PropertyValue, out hasAccessRight))
                {
                    hasAccessRight = false;
                }
            }
            ctrHasVManAccessRight.ClearSelection();
            ctrHasVManAccessRight.SelectedValue = hasAccessRight.ToString();
        }

        if (Boolean.TrueString.Equals(ctrHasVManAccessRight.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
        {
            PropertyValue = Boolean.TrueString;
        }
        else
        {
            PropertyValue = Boolean.FalseString;
        }
    }
}