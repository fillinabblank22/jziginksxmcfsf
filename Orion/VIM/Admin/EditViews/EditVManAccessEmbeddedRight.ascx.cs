﻿using System;

public partial class Orion_VIM_Admin_EditViews_EditVManAccessEmbeddedRight : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
{
    public override string PropertyValue { get; set; }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var hasAccessRight = false;
            if (!String.IsNullOrEmpty(PropertyValue))
            {
                if (!Boolean.TryParse(PropertyValue, out hasAccessRight))
                {
                    hasAccessRight = false;
                }
            }
            ctrHasVManAccessRight.ClearSelection();
            ctrHasVManAccessRight.SelectedValue = hasAccessRight.ToString();
        }

        if (Boolean.TrueString.Equals(ctrHasVManAccessRight.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
        {
            PropertyValue = Boolean.TrueString;
        }
        else
        {
            PropertyValue = Boolean.FalseString;
        }
    }
}