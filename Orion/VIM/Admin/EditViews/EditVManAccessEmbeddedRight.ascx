﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVManAccessEmbeddedRight.ascx.cs" Inherits="Orion_VIM_Admin_EditViews_EditVManAccessEmbeddedRight" %>

<asp:RadioButtonList ID="ctrHasVManAccessRight" runat="server">
    <asp:ListItem Selected="True" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_73 %>" Value="True" />
    <asp:ListItem Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_74 %>" Value="False" />
</asp:RadioButtonList>
