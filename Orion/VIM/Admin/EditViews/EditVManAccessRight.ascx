﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVManAccessRight.ascx.cs" Inherits="Orion_VIM_Admin_EditViews_EditVManAccessRight" %>

<asp:RadioButtonList ID="ctrHasVManAccessRight" runat="server">
    <asp:ListItem Selected="True" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_61 %>" Value="True" />
    <asp:ListItem Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_62 %>" Value="False" />
</asp:RadioButtonList>

<p>
    <a href="<%=HelpLink%>" class="sw-link" target="_blank" rel="noopener noreferrer">&raquo; <%= Resources.VIMWebContent.VIMWEBDATA_LC0_60%></a>
</p>