﻿<%@ Control Language="C#" AutoEventWireup="true" ClassName="EditPowerManagementProperty" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl" %>
<%@ Register Src="~/Orion/VIM/Controls/AllowProperty.ascx" TagPrefix="orion" TagName="AllowProperty" %>

<script runat="server">
    public override string PropertyValue
    {
        get { return AllowProperty.PropertyValue; }
        set { AllowProperty.PropertyValue = value; }
    }
</script>

<orion:AllowProperty runat="server" ID="AllowProperty" />
