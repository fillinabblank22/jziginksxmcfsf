﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" 
    CodeFile="BaselineThresholds.aspx.cs" Inherits="Orion_VIM_Admin_BaselineThresholds" Title="<%$ Resources: VIMWebContent, VIMWEBDATA_VB1_2%>"%>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="vim" TagName="VMWareTabs" Src="~/Orion/VIM/Admin/VMWareTabs.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">    
    <orion:IncludeExtJs runat="server" Debug="False" Version="3.4" />
    <orion:Include ID="Include2" runat="server" File="OrionCore.js" />
    <orion:Include Module="VIM" runat="server" File="js/BaselineThresholds.js" />    
    <orion:Include ID="Include1" runat="server" Module="VIM" File="VMWareServers.css" />    
    
    <style type="text/css">        
        .vimAdminDateArea {display:none;}
        #adminContent { padding-top: 0; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #assignDialog td { padding-right: 0; padding-bottom: 0; }
        #testButton table td { padding: 0px !important;}
        #assignDialog td.x-btn-center { text-align: center !important; }
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align:middle; }
        #cancelAssignButton td { text-align: center; }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>

    <h1 style="font-size:large"><%= Resources.VIMWebContent.VIMWEBDATA_VB1_2%></h1>
    <div style="padding: 1em 0 1em 0;"><%= Resources.VIMWebContent.VIMWEBDATA_VB1_1%></div>
    
    <input type="hidden" id="VManIntegrationEnabled" value="<%= VManIntegrationEnabled.ToString() %>"/> <!-- Used in BaselineThresholds.js -->

    <div class="sw-vim-standard-resizable-wrapper">
        <vim:VMWareTabs runat="server" />   
        <div id="tabPanel" class="tab-top">                        
            <div id="thresholdsSearchPanel"></div>                                     
            <div id="thresholdsMainPanel" class="sw-vim-wizard-extjs-grid"></div>
        </div>
    </div>
</asp:Content>
