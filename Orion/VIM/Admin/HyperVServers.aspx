﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true"
    CodeFile="HyperVServers.aspx.cs" Inherits="Orion_HyperVServers" Title="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_68%>" %>

<%@ Import Namespace="SolarWinds.VIM.Web.DAL" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Helpers" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/VIM/Admin/VMwareTabs.ascx" TagPrefix="vim" TagName="VMwareTabs" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" Debug="false" Version="3.4" />
    <orion:Include ID="Include1" runat="server" Module="VIM" File="VMWareServers.css" />
    <style type="text/css">
        .editProperties
        {
            background-image: url(/Orion/Nodes/images/icons/icon_edit.gif) !important;
        }
        .listResources
        {
            background-image: url(/Orion/Nodes/images/icons/icon_list.gif) !important;
        }
        .enablePolling
        {
            background-image: url(/Orion/VIM/images/enable_monitoring.gif) !important;
        }
        .disablePolling
        {
            background-image: url(/Orion/VIM/images/disable_monitoring.gif) !important;
        }
        .pollThrough { background-image:url(/Orion/VIM/images/icon_pollthrough.gif) !important; }
        ul.vim-bullet { list-style-type: disc !important; margin-left: 20px }
        .ext-el-mask-msg { z-index: 9002}
        .vimAdminDateArea
        {
            display: none;
        }
        #adminContent
        {
            padding-top: 0;
        }
        #adminContent .dialogBody table td
        {
            padding-top: 5px;
            padding-left: 10px;
        }
        #Grid td
        {
            padding-right: 0;
            padding-bottom: 0;
            vertical-align: middle;
        }
        .GroupItem, .SelectedGroupItem
        {
            height: 18px;
            list-style-type: none;
            padding: 2px 2px 2px 6px;
            white-space: nowrap;
        }
        .SelectedGroupItem
        {
            background-color: #97D6FF;
            background-image: url(/Orion/Nodes/images/background/left_selection_gradient.gif);
            background-repeat: repeat-x;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="smp" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/Orion/js/OrionCore.js" />
            <asp:ScriptReference Path="~/Orion/VIM/js/ScriptServiceInvoker.js" />
            <asp:ScriptReference Path="~/Orion/VIM/js/OrionCoreScriptServiceProxy.js" />
            <asp:ScriptReference Path="~/Orion/VIM/js/HyperVServers.js" />
            <asp:ScriptReference Path="~/Orion/VIM/js/DemoServerHelper.js" />
        </Scripts>
    </asp:ScriptManagerProxy>
    <input type="hidden" name="VIM_HyperVServers_PageSize" id="VIM_HyperVServers_PageSize"
        value="<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_HyperVServers_PageSize")%>" />
    <input type="hidden" name="VIM_HyperVServers_HiddenColumns" id="VIM_HyperVServers_HiddenColumns"
        value="<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_HyperVServers_HiddenColumns")%>" />
    <input type="hidden" name="VIM_HyperVServers_ReturnToUrl" id="VIM_HyperVServers_ReturnToUrl" value="<%= ReturnUrl %>" />
    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
      {%>
    <div id="isDemoMode" style="display: none;">
    </div>
    <%}%>
    <h1 style="font-size: large"><%= Page.Title %></h1>
    <div style="padding: 1em 0 1em 0;"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_6%></div>
    <%if (VimFipsHelper.IsPollingFipsRestricted)
    {%>
    <div class="vim-hyperv-fips-restriction-warning sw-suggestion sw-suggestion-info">
        <span class="sw-suggestion-icon"></span>
        <span><%= String.Format(Resources.VIMWebContent.Web_FIPSRestrictionEnabled_HyperV_Warning, VmanPollingKbLink)%></span>
    </div>
    <%}%>
    <div style="width: 1077px;">
        <vim:VMwareTabs ID="VMwareTabs1" runat="server" />
        <div id="tabPanel" class="tab-top">
            <table class="ESXServersTable" cellpadding="0" cellspacing="0" width="100%">
                <tr valign="top" align="left">
                    <td id="gridCell" style="padding-right: 0px;">
                        <div id="Grid" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
