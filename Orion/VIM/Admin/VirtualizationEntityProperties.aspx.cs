﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Models;

public partial class Orion_VIM_Admin_VirtualizationEntityProperties : System.Web.UI.Page
{    
    private VimEntityType _entityType;
    private IList<IBaseEntity> _entities;
    private string _previousPage;

    protected void Page_Load(object sender, EventArgs e)
    {
        ReferrerRedirectorBase.Initialize(ViewState, IsPostBack);

        string netObjectId = Request.QueryString["NetObject"];
        List<string> entityIds;

        if (!String.IsNullOrEmpty(netObjectId))
        {
            EntityWorkflowHelper.ResetEntityMultiEdit();
            entityIds = new List<string>();
            var format = netObjectId.Split(':');

            if (format.Length == 2)
            {
                _entities = new List<IBaseEntity>();

                if (ParseEntityType(format[0], out _entityType))
                    entityIds.Add(format[1]);
                else                
                    throw new InvalidEnumArgumentException();
            }
        }        
        else
        {
            if (!Enum.TryParse(EntityWorkflowHelper.EntityType, out _entityType))
                throw new InvalidEnumArgumentException();

            entityIds = EntityWorkflowHelper.EntityMultiEditIDs;
        }

        switch (_entityType)
        {
            case VimEntityType.Cluster:
                InitializeClusters(entityIds);
                break;
            case VimEntityType.Host:
                InitializeHosts(entityIds);
                break;
            case VimEntityType.DataStore:
                InitializeDatastores(entityIds);
                break;
            case VimEntityType.VirtualMachine:
                InitializeVirtualMachines(entityIds);
                break;
        }

        if (_entities != null &&_entities.Count > 0)
        {
            Orion_VIM_Controls_ThresholdControl tc =
                (Orion_VIM_Controls_ThresholdControl) LoadControl("~/Orion/VIM/Controls/ThresholdControl.ascx");
            tc.SetEntities(_entities, _entityType);
            ThresholdControlPlaceholder.Controls.Add(tc);
        }

        if (!IsPostBack)
        {
            _previousPage = Request.UrlReferrer == null ? "/Orion/Admin/Default.aspx" : Request.UrlReferrer.ToString();
            InitializeUI();
        }
    }

    private bool ParseEntityType(string netObjectPrefix, out VimEntityType entityType)
    {
        switch (netObjectPrefix.ToLowerInvariant())
        {
            case "vvm":
                entityType = VimEntityType.VirtualMachine;
                return true;                
            case "vmc":
                entityType = VimEntityType.Cluster;
                return true;                
            case "vh":
                entityType = VimEntityType.Host;
                return true;                
            case "vms":
                entityType = VimEntityType.DataStore;
                return true;                
            default:
                entityType = VimEntityType.VirtualMachine;
                return false;
        }
    }

    private void InitializeVirtualMachines(IEnumerable<string> vmIds)
    {
        var dal = new VirtualMachineDAL();
        _entities = new List<IBaseEntity>();

        foreach (var vmId in vmIds)
        {
            int id;
            if (Int32.TryParse(vmId, out id))
            {
                var virtualMachine = dal.GetVMModelByVMId(id);

                if (virtualMachine != null)
                    _entities.Add(virtualMachine);
            }
        }
    }

    private void InitializeClusters(IEnumerable<string> clusterIds)
    {
        var dal = new ClusterDAL();
        _entities = new List<IBaseEntity>();

        foreach (var clusterId in clusterIds)
        {
            int id;
            if (Int32.TryParse(clusterId, out id))
            {
                var clusterModel = dal.GetClusterModel(id);

                if (clusterModel != null)
                {
                    _entities.Add(clusterModel);
                }
            }
        }
    }


    private void InitializeHosts(IEnumerable<string> hostIds)
    {
        var dal = new HostDAL();
        _entities = new List<IBaseEntity>();

        foreach (var hostId in hostIds)
        {
            int id;
            if (Int32.TryParse(hostId, out id))
            {
                var hostModel = dal.GetHostModelByHostId(id);

                if (hostModel != null)
                {
                    _entities.Add(hostModel);
                }
            }
        }
    }

    private void InitializeDatastores(IEnumerable<string> datastoreIds)
    {
        var dal = new DatastoreDAL();
        _entities = new List<IBaseEntity>();

        foreach (var hostId in datastoreIds)
        {
            int id;
            if (Int32.TryParse(hostId, out id))
            {
                var datastoreModel = dal.GetDataStoreModel(id);

                if (datastoreModel != null)
                {
                    _entities.Add(datastoreModel);
                }
            }
        }
    }


    private void InitializeUI()
    {
        foreach (var entity in _entities)
        {
            blEntities.Items.Add(new ListItem(entity.Name));
        }
    }

    protected string PageSubtitle
    {
        get
        {
            switch (_entityType)
            {
                case VimEntityType.VirtualMachine:
                    return Resources.VIMWebContent.VIMWEBDATA_ZS0_1;
                    break;
                case VimEntityType.Cluster:
                    return Resources.VIMWebContent.VIMWEBDATA_ZS0_2;
                    break;
                case VimEntityType.Host:
                    return Resources.VIMWebContent.VIMWEBDATA_ZS0_3;
                    break;
                case VimEntityType.DataStore:
                    return Resources.VIMWebContent.VIMWEBDATA_ZS0_4;
                    break;
            }
            return String.Empty;
        }
    }

    protected void CancelBtn_Click(object sender, EventArgs e)
    {        
        ReferrerRedirectorBase.Return("/Orion/Admin/Default.aspx");
    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        if (ThresholdControlPlaceholder.Controls.Count > 0)
            ((Orion_VIM_Controls_ThresholdControl) ThresholdControlPlaceholder.Controls[0]).UpdateThresholds();

        ReferrerRedirectorBase.Return("/Orion/Admin/Default.aspx");
    }

    
}
