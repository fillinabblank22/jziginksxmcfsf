﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" CodeFile="RetentionSettings.aspx.cs" 
    Inherits="VIM_Admin_RetentionSettings" Title="<%$ Resources: VIMWebContent, WEBDATA_LM0_11 %>" %>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    
    <h1 style="font-size:large"><%= Page.Title %></h1>
    <div style="padding: 1em 0 1em 0;"><%= Resources.VIMWebContent.WEBDATA_LM0_24%></div>
        
    <table border="0" width="100%">
        <tr>
            <td>
                <table id="adminContentTable" border="0" cellpadding="3" cellspacing="0" width="100%">
                    
                    <tr class="alternateRow">
                        <td class="PropertyHeader">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_12 %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="detailedRetention" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="detailedRetention" Operator="DataTypeCheck" ID="CompareValidator1" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="detailedRetention" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp; <%= Resources.VIMWebContent.WEBDATA_LM0_14 %>
                            <asp:RangeValidator ControlToValidate="detailedRetention" ID="RangeValidator1" runat="server" ErrorMessage="<%$ Resources: VIMWebContent, WEBDATA_LM0_15 %>" 
                            Display="Dynamic" MinimumValue="1" MaximumValue="180" Type="Integer" /> 
                        </td>
                        <td class="Property">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_13 %>
                        </td>
                    </tr>

                    <tr>
                        <td class="PropertyHeader">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_16 %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="hourlyRetention" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="hourlyRetention" Operator="DataTypeCheck" ID="CompareValidator2" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="hourlyRetention" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp; <%= Resources.VIMWebContent.WEBDATA_LM0_18 %>
                            <asp:RangeValidator ControlToValidate="hourlyRetention" ID="RangeValidator2" runat="server" ErrorMessage="<%$  Resources: VIMWebContent, WEBDATA_LM0_19 %>" 
                            Display="Dynamic" MinimumValue="1" MaximumValue="180" Type="Integer" /> 
                        </td>
                        <td class="Property">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_17 %>
                        </td>
                    </tr>

                    <tr class="alternateRow">
                        <td class="PropertyHeader">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_20 %>
                        </td>
                        <td class="Property">
                            <asp:TextBox runat="server" ID="dailyRetention" Columns="5"></asp:TextBox>
                            <asp:CompareValidator Type="Integer" ControlToValidate="dailyRetention" Operator="DataTypeCheck" ID="CompareValidator3" runat="server" ErrorMessage="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ControlToValidate="dailyRetention" ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                            &nbsp; <%= Resources.VIMWebContent.WEBDATA_LM0_22 %>
                            <asp:RangeValidator ControlToValidate="dailyRetention" ID="RangeValidator3" runat="server" ErrorMessage="<%$ Resources: VIMWebContent, WEBDATA_LM0_23 %>" 
                            Display="Dynamic" MinimumValue="1" MaximumValue="5000" Type="Integer" /> 
                        </td>
                        <td class="Property">
                            <%= Resources.VIMWebContent.WEBDATA_LM0_21 %>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

    <div>
        <orion:LocalizableButton runat="server" ID="bt_Submit" Localizedtext="Submit" DisplayType="Primary" OnClick="Submit_Click" />
        <span style="width: 20px">&nbsp;</span>
        <orion:LocalizableButton runat="server" ID="bt_Cancel" Localizedtext="Cancel" DisplayType="Secondary" OnClick="Cancel_Click" CausesValidation="false"/>
    </div>

</asp:Content>
