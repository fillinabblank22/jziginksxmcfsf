﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.VIM.Common.Constants;
using SolarWinds.VIM.Common.Licensing;
using SolarWinds.VIM.Web.DAL;

public partial class Orion_VIM_Admin_VimModuleDetails : System.Web.UI.UserControl
{
    private static readonly Log _logger = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("VIM");
            }
            catch (Exception ex)
            {
                _logger.Error("Error while displaying details for VIM module.", ex);
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductTag.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();
            values.Add(Resources.VIMWebContent.VIMWEBCODE_TK0_3, module.LicenseInfo);
            values.Add(Resources.VIMWebContent.VIMWEBCODE_TK0_10, module.ProductName);
            values.Add(Resources.VIMWebContent.VIMWEBCODE_TK0_13, module.Version);
            values.Add(Resources.VIMWebContent.VIMWEBCODE_TK0_4, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.VIMWebContent.VIMWEBCODE_TK0_5 : module.HotfixVersion);
            AddVimLicenseInfo(values);

            VimDetails.Name = module.ProductDisplayName;
            VimDetails.DataSource = values;

            break; //we support only one "Primary" engine
        }
    }

    private void AddVimLicenseInfo(Dictionary<string, string> values)
    {
        try
        {
            String licenseType = String.Empty;
            var manager = new FeatureManager();
            int licenseSize = manager.GetMaxElementCount(VimLicenseConstants.VMs);
            if (licenseSize > 0)
            {
                licenseType = VimLicenseConstants.VMs;
            }
            else
            {
                licenseType = VimLicenseConstants.Sockets;
                licenseSize = manager.GetMaxElementCount(VimLicenseConstants.Sockets);
            }

            var isEval = IsVimEval();

            AddElementsCount(values, licenseType, licenseSize, isEval);
            AddUsedElementsCount(values, licenseType, isEval);
        }
        catch (Exception ex)
        {
            _logger.Warn("Cannot get VIM specific information about license", ex);
        }
    }

    private void AddElementsCount(Dictionary<string, string> values, String licenseType, int licenseSize, bool isEval)
    {
        string elementDescription;
        string elementCount;
        if (isEval)
        {
            //eval license should have unlimited number of any elements
            elementCount = Resources.VIMWebContent.VIMWEBCODE_TK0_6;
            elementDescription = Resources.VIMWebContent.VIMWEBCODE_TK0_11;
        }
        else
        {
            switch (licenseType)
            {
                case VimLicenseConstants.Sockets:
                    elementCount = licenseSize.ToString();
                    elementDescription = Resources.VIMWebContent.VIMWEBCODE_TK0_14;
                    break;
                case VimLicenseConstants.VMs:
                    elementCount = licenseSize.ToString();
                    elementDescription = Resources.VIMWebContent.VIMWEBCODE_TK0_15;
                    break;
                default:
                    elementDescription = Resources.VIMWebContent.VIMWEBCODE_TK0_11;
                    elementCount = Resources.VIMWebContent.VIMWEBCODE_TK0_16;
                    break;
            }
        }
        values.Add(elementDescription, elementCount);
    }

    private void AddUsedElementsCount(Dictionary<string, string> values, String licenseType, bool isEval)
    {
        if (isEval)
        {
            values.Add(
                Resources.VIMWebContent.UsedNumberOfSockets,
                new HostDAL().GetTotalSocketCount().ToString()
            );

            values.Add(
                Resources.VIMWebContent.UsedNumberOfVms,
                new VirtualMachineDAL().GetTotalRunningVMs().ToString()
            );

            return;
        }

        string elementDescription;
        string elementCount;
        int? licensedElements = ReadLicensedElementsCount();

        switch (licenseType)
        {
            case VimLicenseConstants.Sockets:
                elementCount = licensedElements?.ToString() ?? Resources.VIMWebContent.VIMWEBCODE_TK0_16;
                elementDescription = Resources.VIMWebContent.UsedNumberOfSockets;
                break;
            case VimLicenseConstants.VMs:
                elementCount = licensedElements?.ToString() ?? Resources.VIMWebContent.VIMWEBCODE_TK0_16;
                elementDescription = Resources.VIMWebContent.UsedNumberOfVms;
                break;
            default:
                elementCount = Resources.VIMWebContent.VIMWEBCODE_TK0_16;
                elementDescription = Resources.VIMWebContent.UsedNumberOfElements;
                break;
        }
        values.Add(elementDescription, elementCount);
    }

    private bool IsVimEval()
    {
        string query = $@"SELECT IsEval FROM Orion.InstalledModule WHERE LicenseName = '{VimLicenseConstants.LicenseName}'";

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var dataTable = proxy.Query(query);

            return dataTable.Rows.Count > 0 ? Convert.ToBoolean(dataTable.Rows[0][0]) : false;
        }
    }

    private int? ReadLicensedElementsCount()
    {
        try
        {
            string query = "SELECT LicensedElements FROM Orion.VIM.LicenseInfo";

            using (var proxy = InformationServiceProxy.CreateV3())
            {
                var dataTable = proxy.Query(query);

                return dataTable.Rows.Count > 0 ? (int?) Convert.ToInt32(dataTable.Rows[0][0]) : null;
            }
        }
        catch (Exception ex)
        {
            _logger.Error("Reading licensed elements count failed.", ex);
            return null;
        }
    }
}