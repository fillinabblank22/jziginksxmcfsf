﻿using SolarWinds.VIM.Web.Common.Licensing;
using System;
public partial class Orion_VIM_Admin_BaselineThresholds : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VirtualizationThresholds";            
    }

    protected bool VManIntegrationEnabled
    {
        get { return LicenseManager.Instance.IsVimFullyLicensed(); }
    }
}