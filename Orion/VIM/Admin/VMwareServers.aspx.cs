using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

public partial class Orion_VMwareServers : System.Web.UI.Page, IBypassAccessLimitation
{
    protected override void OnPreInit(EventArgs e)
    {
        // this needs to be reverted after GW, see FB 130186
        SolarWinds.Orion.Web.UI.ModulePatchHelper.SetIECompatibilityRendering(7);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer("~/Orion/Error.aspx?Message=" + Server.HtmlEncode(Resources.VIMWebContent.VIMWEBCODE_TM0_9));
        }

        ((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VMwareSettings";
    }
}
