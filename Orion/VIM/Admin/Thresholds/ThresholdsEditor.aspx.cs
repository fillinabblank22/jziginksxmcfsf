﻿using System;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Admin_ThresholdsEditor : System.Web.UI.Page, IBypassAccessLimitation
{
    public string NetObjectID { get; private set; }
    public string GlobalAreaID { get; private set; }
    public string ReturnUrl { get; private set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.NetObjectID = Request.QueryString["NetObject"];
        this.GlobalAreaID = Request.QueryString["GlobalArea"];
        this.ReturnUrl = Request.QueryString["ReturnUrl"] ?? "/ui/clm/accounts";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Profile.AllowAdmin && !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {

            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });

        }
    }
}
