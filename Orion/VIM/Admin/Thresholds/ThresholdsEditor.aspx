﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="ThresholdsEditor.aspx.cs" Inherits="Orion_VIM_Admin_ThresholdsEditor"%>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <orion:Include Module="VIM" File="Admin/Thresholds/ThresholdsEditor.gen.js" runat="server" />
    <orion:Include Module="VIM" File="Admin/Thresholds/ThresholdsEditor.css" runat="server" />
    <script type="text/javascript">
    $(document).ready(function () {
        SW.VIM.Admin.ThresholdsEditor.initialize({
            container: $('#<%= this.tabsEditorItems.ClientID %>'),
            globalAreaId: '<%= this.GlobalAreaID %>',
            netObjectId: '<%= this.NetObjectID %>',
            returnUrl: '<%= this.ReturnUrl %>',
            isDemo: '<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer%>' === 'True'
        });
    });
    </script>

    <div class="thresholdsEditor" style="padding: 2em;" id="tabsEditorItems" runat="server"/>

</asp:Content>
