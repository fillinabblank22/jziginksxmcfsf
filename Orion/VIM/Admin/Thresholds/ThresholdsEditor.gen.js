var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var TemplateFormatter;
        (function (TemplateFormatter) {
            var thistr = 'this.';
            function getValue(data, path, defaultValue) {
                function evaluate() {
                    if (path.substr(0, thistr.length) !== thistr) {
                        path = thistr + path;
                    }
                    return eval(path);
                }
                var value;
                try {
                    value = evaluate.call(data);
                }
                catch (e) { }
                return (value === undefined || value === null)
                    ? defaultValue
                    : String(value);
            }
            function format(text, data) {
                return text.replace(/\{\{([^{}]+)\}\}/g, function () {
                    var matches = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        matches[_i] = arguments[_i];
                    }
                    return getValue(data, matches[1], matches[0]);
                });
            }
            TemplateFormatter.format = format;
            function prefixIds($root, prefix) {
                ['id', 'for', 'name'].forEach(function (attrName) {
                    $root.find("*[" + attrName + "]").andSelf().each(function () {
                        var $sub = $(this);
                        var value = $sub.attr(attrName);
                        if (value) {
                            $sub.attr(attrName, "" + prefix + value);
                        }
                    });
                });
            }
            TemplateFormatter.prefixIds = prefixIds;
            function formatDOM(text, data, prefix) {
                var html = format(text, data);
                var $result = $(html);
                if (prefix) {
                    prefixIds($result, prefix);
                }
                return $result;
            }
            TemplateFormatter.formatDOM = formatDOM;
        })(TemplateFormatter = VIM.TemplateFormatter || (VIM.TemplateFormatter = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var BaseApiController = (function () {
                    function BaseApiController() {
                    }
                    BaseApiController.prototype.parseWebApiError = function (xhr) {
                        var responseText = xhr.responseText;
                        var debug = {
                            responseText: responseText
                        };
                        var data = {
                            Message: responseText || "web api error " + xhr.statusText
                        };
                        try {
                            var responseData = JSON.parse(xhr.responseText);
                            debug.responseData = responseData;
                            var responseMessage = (responseData && (responseData.ExceptionMessage || responseData.Message));
                            if (responseMessage) {
                                data.Message = responseMessage;
                            }
                        }
                        catch (e) {
                            debug.parsingException = e;
                        }
                        data.debug = debug;
                        return data;
                    };
                    BaseApiController.prototype.convertPromise = function (promise) {
                        var _this = this;
                        var defer = $.Deferred();
                        promise.done(function (data) { return defer.resolve(data); });
                        promise.fail(function (err) { return defer.reject(_this.parseWebApiError(err)); });
                        return defer.promise();
                    };
                    BaseApiController.prototype.convertPromiseCustom = function (promise, convertor) {
                        var _this = this;
                        var defer = $.Deferred();
                        promise.done(function (data) { return defer.resolve(convertor(data)); });
                        promise.fail(function (err) { return defer.reject(_this.parseWebApiError(err)); });
                        return defer.promise();
                    };
                    BaseApiController.prototype.getActionUrl = function (action, params) {
                        var root = "/api/ThresholdsEditor/" + action;
                        if (params) {
                            var encoded = Object.keys(params)
                                .map(function (k) { return k + "=" + encodeURIComponent(params[k]); });
                            return root + "?" + encoded.join('&');
                        }
                        return root;
                    };
                    return BaseApiController;
                }());
                ThresholdsEditor.BaseApiController = BaseApiController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Messages = {
                    NothingChanged: 'no changed thresholds to submit',
                    SubmitSuccess: 'all changes submitted successfully',
                    SubmitFailed: 'error occured during submit',
                    CancelEditor: 'cancelling changes',
                    TabLoadFailed: 'failed to load tab information ({{Message}})',
                    TabUpdateFailed: '{{Name}}: {{Message}}',
                    TabUpdateSuccess: '{{Name}}: {{Message}}',
                };
                function resolveUpdateStatus(dfd, tabId, success) {
                    if (success) {
                        dfd.resolve(tabId);
                    }
                    else {
                        dfd.reject(tabId);
                    }
                }
                ThresholdsEditor.resolveUpdateStatus = resolveUpdateStatus;
                var reLogin = /login\.aspx/i;
                var BaseEditorController = (function () {
                    function BaseEditorController(editorView, thresholdView, location) {
                        this.editorView = editorView;
                        this.thresholdView = thresholdView;
                        this.location = location;
                    }
                    BaseEditorController.prototype.initialize = function (opts) {
                        var _this = this;
                        this.editorView.renderEditor();
                        if (opts && opts.isDemo) {
                            this.editorView.onSubmit(function (e) { return demoAction(); });
                        }
                        else {
                            this.editorView.onSubmit(function (e) { return _this.handleSubmit(); });
                        }
                        this.editorView.onCancel(function (e) { return _this.handleCancel(); });
                        this.returnUrl = opts && opts.returnUrl;
                    };
                    BaseEditorController.prototype.getTab = function (id) {
                        return this.tabs.filter(function (tab) { return tab.Id === id; })[0];
                    };
                    BaseEditorController.prototype.handleInitDone = function (data) {
                        this.tabs = data.TabList;
                        this.editorView.updateTabs(this.tabs);
                        this.editorView.clearMessages();
                    };
                    BaseEditorController.prototype.handleInitFail = function (err) {
                        this.tabs = [];
                        this.editorView.updateTabs(this.tabs);
                        this.editorView.clearMessages();
                        this.editorView.addError(VIM.TemplateFormatter.format(Messages.TabLoadFailed, err));
                    };
                    BaseEditorController.prototype.handleLoadDone = function (result) {
                        this.thresholdView.renderAllThresholds(result.PluginId, result.Thresholds);
                    };
                    BaseEditorController.prototype.handleLoadFail = function (tabId, error) {
                        var tab = this.getTab(tabId) || { Name: tabId };
                        var message = VIM.TemplateFormatter.format(Messages.TabLoadFailed, {
                            Name: tab.Name,
                            Message: error.Message
                        });
                        this.editorView.addError(message);
                    };
                    BaseEditorController.prototype.handleUpdateResult = function (tabId, updateResult) {
                        var _this = this;
                        this.thresholdView.clearMessages(tabId);
                        if (updateResult.Errors) {
                            updateResult.Errors.forEach(function (error) {
                                _this.thresholdView.addError(tabId, error.ThresholdName, error.Message);
                            });
                            return false;
                        }
                        if (updateResult.Message) {
                            var message = VIM.TemplateFormatter.format(Messages.TabUpdateSuccess, {
                                Name: this.getTab(tabId).Name,
                                Message: updateResult.Message
                            });
                            this.editorView.addMessage(message);
                        }
                        return true;
                    };
                    BaseEditorController.prototype.reportTabUpdateFail = function (tab, err) {
                        var message = VIM.TemplateFormatter.format(Messages.TabUpdateFailed, {
                            Name: tab.Name,
                            Message: err.Message
                        });
                        this.editorView.addMessage(message);
                    };
                    BaseEditorController.prototype.reportTabUpdateDone = function (tab, updateResult) {
                        var success = this.handleUpdateResult(tab.Id, updateResult);
                        this.thresholdView.updateAllOriginalData(tab.Id);
                        return success;
                    };
                    BaseEditorController.prototype.tryRedirect = function () {
                        var referrer = this.location.getReferrer();
                        if (referrer && !reLogin.test(referrer)) {
                            this.location.setLocation(referrer);
                        }
                        else if (this.returnUrl) {
                            this.location.setLocation(this.returnUrl);
                        }
                        else {
                            this.location.close();
                        }
                    };
                    BaseEditorController.prototype.allSubmitsDone = function (success) {
                        this.editorView.clearMessages();
                        this.editorView.addMessage(success ? Messages.SubmitSuccess : Messages.SubmitFailed);
                        if (success) {
                            this.tryRedirect();
                        }
                    };
                    BaseEditorController.prototype.handleCancel = function () {
                        this.editorView.clearMessages();
                        this.editorView.addMessage(Messages.CancelEditor);
                        this.tryRedirect();
                    };
                    BaseEditorController.prototype.handleSubmit = function () {
                        var _this = this;
                        var tabThresholds = this.tabs
                            .map(function (tab) { return ({
                            Tab: tab,
                            Thresholds: _this.thresholdView.getAllThresholdData(tab.Id)
                        }); });
                        if (!this.editorView.checkValidity(tabThresholds)) {
                            return;
                        }
                        var tabsChanges = tabThresholds.map(function (tt) {
                            return {
                                Tab: tt.Tab,
                                Thresholds: tt.Thresholds
                                    .filter(function (thr) { return thr.IsChanged; })
                                    .map(function (t) { return t.Threshold; })
                            };
                        }).filter(function (tt) { return tt.Thresholds.length > 0; });
                        var tabsRemaining = tabsChanges.map(function (tt) { return tt.Tab.Id; });
                        var tabsSuccess = [];
                        var submitDone = function (tabId, success) {
                            if (success) {
                                tabsSuccess.push(tabId);
                            }
                            var index = tabsRemaining.indexOf(tabId);
                            if (index !== -1) {
                                tabsRemaining.splice(index, 1);
                                if (tabsRemaining.length === 0) {
                                    _this.allSubmitsDone(tabsSuccess.length === tabsChanges.length);
                                }
                            }
                        };
                        if (tabsChanges.length === 0) {
                            this.editorView.addMessage(Messages.NothingChanged);
                            this.tryRedirect();
                            return;
                        }
                        tabsChanges.forEach(function (thrList) {
                            _this.updateTab(thrList.Tab, thrList.Thresholds)
                                .done(function (tabId) { return submitDone(tabId, true); })
                                .fail(function (tabId) { return submitDone(tabId, false); });
                        });
                    };
                    return BaseEditorController;
                }());
                ThresholdsEditor.BaseEditorController = BaseEditorController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Attr = {
                    TabId: 'data-tab-id',
                };
                var Class = {
                    EditorTitle: 'sw-hdr-title',
                    EditorSubTitle: 'sw-hdr-subtitle',
                    TabsFrame: 'thresholdsEditorFrame',
                    TabSelected: 'tabSelected',
                    TabBody: 'tabContent',
                    HeadersPanel: 'tabHeaders',
                    ContentPanel: 'tabContents',
                    MessagePanel: 'tabMessages',
                    TabMessage: 'tabMessage',
                    ButtonBar: 'sw-btn-bar',
                    SubmitButton: 'thresholdEditorSubmit',
                    CancelButton: 'thresholdEditorCancel',
                };
                var Selector = {
                    SubmitButton: "." + Class.SubmitButton,
                    CancelButton: "." + Class.CancelButton,
                    ButtonBar: "." + Class.ButtonBar,
                    HeadersPanel: "." + Class.HeadersPanel,
                    ContentPanel: "." + Class.ContentPanel,
                    MessagePanel: "." + Class.MessagePanel,
                };
                var Templates = {
                    skeleton: "\n<h1 class=\"" + Class.EditorTitle + "\"></h1>\n<h2 class=\"" + Class.EditorSubTitle + "\"></h2>\n<div class=\"" + Class.TabsFrame + "\">\n    <ul class=\"" + Class.HeadersPanel + "\"></ul>\n    <div class=\"" + Class.ContentPanel + "\"></div>\n</div>\n<div class=\"" + Class.MessagePanel + "\">\n</div>\n<div class=\"" + Class.ButtonBar + "\">\n    <a href=\"#\" class=\"" + Class.SubmitButton + " sw-btn-primary sw-btn\" automation=\"Submit\">\n        <span class=\"sw-btn-c\"><span class=\"sw-btn-t\">@{R=Core.Strings;K=CommonButtonType_Submit;E=js}</span></span>\n    </a>\n    <a href=\"#\" class=\"" + Class.CancelButton + " sw-btn\" automation=\"Cancel\">\n        <span class=\"sw-btn-c\"><span class=\"sw-btn-t\">@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}</span></span>\n    </a>\n</div>",
                    tabHeader: "<li><a href=\"#\">{{Name}}</a></li>",
                    tabBody: "<div class=\"" + Class.TabBody + "\"></div>"
                };
                var BaseEditorView = (function () {
                    function BaseEditorView($container) {
                        this.$container = $container;
                    }
                    BaseEditorView.prototype.getContainer = function () {
                        return this.$container;
                    };
                    BaseEditorView.prototype.updateTitle = function (text) {
                        this.$container
                            .find("." + Class.EditorTitle)
                            .html("<span class=\"entityName\">" + text + "</span>");
                    };
                    BaseEditorView.prototype.updateSubTitle = function (text) {
                        this.$container
                            .find("." + Class.EditorSubTitle)
                            .html("<span class=\"entityName\">" + text + "</span>");
                    };
                    BaseEditorView.prototype.renderEditor = function () {
                        var _this = this;
                        var $editor = VIM.TemplateFormatter.formatDOM(Templates.skeleton, null);
                        this.$container
                            .empty()
                            .append($editor);
                        this.$Headers()
                            .on('click', 'li', function (evt) { return _this.handleHeaderClick(evt); });
                        this.$ButtonBar()
                            .on('click', Selector.SubmitButton, function (evt) { return _this.handleSubmitClick(evt); });
                        this.$ButtonBar()
                            .on('click', Selector.CancelButton, function (evt) { return _this.handleCancelClick(evt); });
                        this.clearMessages();
                    };
                    BaseEditorView.prototype.getTabBody = function (tabId) {
                        return this.$Content()
                            .find("div." + Class.TabBody + "[" + Attr.TabId + "=\"" + tabId + "\"]");
                    };
                    BaseEditorView.prototype.updateTabs = function (tabs) {
                        var _this = this;
                        var $hdrs = this.$Headers().empty();
                        var $body = this.$Content().empty();
                        tabs.forEach(function (tab) {
                            $hdrs.append(_this.renderTabHeader(tab));
                            $body.append(_this.renderTabBody(tab));
                        });
                        if (tabs.length) {
                            this.makeTabActive(tabs[0].Id);
                        }
                    };
                    BaseEditorView.prototype.clearMessages = function () {
                        this.$Messages()
                            .empty();
                    };
                    BaseEditorView.prototype.addMessage = function (message, isHtml) {
                        if (isHtml === void 0) { isHtml = false; }
                        return this.addMessageBox($('<div class="sw-suggestion sw-suggestion-info">'), message, isHtml);
                    };
                    BaseEditorView.prototype.addError = function (message, isHtml) {
                        if (isHtml === void 0) { isHtml = false; }
                        return this.addMessageBox($('<div class="sw-suggestion sw-suggestion-fail">'), message, isHtml);
                    };
                    BaseEditorView.prototype.onSubmit = function (handler) {
                        this.submitHandler = handler;
                    };
                    BaseEditorView.prototype.onCancel = function (handler) {
                        this.cancelHandler = handler;
                    };
                    BaseEditorView.prototype.makeTabActive = function (tabId) {
                        var $active = $();
                        this.$Headers()
                            .find('li')
                            .each(function () {
                            var $item = $(this);
                            if ($item.attr(Attr.TabId) === tabId) {
                                $item.addClass(Class.TabSelected);
                            }
                            else {
                                $item.removeClass(Class.TabSelected);
                            }
                        });
                        this.$Content()
                            .children()
                            .each(function () {
                            var $item = $(this);
                            if ($item.attr(Attr.TabId) === tabId) {
                                $active = $item;
                                $item.show();
                            }
                            else {
                                $item.hide();
                            }
                        });
                        return $active;
                    };
                    BaseEditorView.prototype.checkValidity = function (tabThresholds) {
                        var _this = this;
                        this.clearMessages();
                        var errors = tabThresholds
                            .map(function (tabThr) { return ({
                            Tab: tabThr.Tab,
                            Thresholds: tabThr.Thresholds.filter(function (thr) { return !thr.IsValid; })
                        }); })
                            .filter(function (tabThr) { return tabThr.Thresholds.length > 0; });
                        if (errors.length === 0) {
                            return true;
                        }
                        errors.forEach(function (err) {
                            var message = "Tab '" + err.Tab.Name + "' contains " + err.Thresholds.length + " error(s).";
                            _this.addError(message)
                                .css({ cursor: 'pointer' })
                                .click(function (event) { return _this.makeTabActive(err.Tab.Id); });
                        });
                        this.makeTabActive(errors[0].Tab.Id);
                        return false;
                    };
                    BaseEditorView.prototype.setText = function ($el, message, isHtml) {
                        if (isHtml) {
                            $el.html(message);
                        }
                        else {
                            $el.text(message);
                        }
                    };
                    BaseEditorView.prototype.addMessageBox = function ($el, message, isHtml) {
                        this.setText($el, message, isHtml);
                        return $('<div>')
                            .addClass(Class.TabMessage)
                            .append($el)
                            .appendTo(this.$Messages());
                    };
                    BaseEditorView.prototype.handleSubmitClick = function (event) {
                        if (this.submitHandler) {
                            this.submitHandler();
                        }
                    };
                    BaseEditorView.prototype.handleCancelClick = function (event) {
                        if (this.cancelHandler) {
                            this.cancelHandler();
                        }
                    };
                    BaseEditorView.prototype.handleHeaderClick = function (event) {
                        var tabId = $(event.currentTarget).attr(Attr.TabId);
                        this.makeTabActive(tabId);
                    };
                    BaseEditorView.prototype.$ButtonBar = function () {
                        return this.$container
                            .children(Selector.ButtonBar);
                    };
                    BaseEditorView.prototype.$Headers = function () {
                        return this.$container
                            .find(Selector.HeadersPanel);
                    };
                    BaseEditorView.prototype.$Content = function () {
                        return this.$container
                            .find(Selector.ContentPanel);
                    };
                    BaseEditorView.prototype.$Messages = function () {
                        return this.$container
                            .find(Selector.MessagePanel);
                    };
                    BaseEditorView.prototype.renderTabHeader = function (tab) {
                        return VIM.TemplateFormatter.formatDOM(Templates.tabHeader, tab, tab.Id + "_")
                            .attr(Attr.TabId, tab.Id);
                    };
                    BaseEditorView.prototype.renderTabBody = function (tab) {
                        return VIM.TemplateFormatter.formatDOM(Templates.tabBody, tab, tab.Id + "_")
                            .attr(Attr.TabId, tab.Id)
                            .addClass(Class.TabBody);
                    };
                    return BaseEditorView;
                }());
                ThresholdsEditor.BaseEditorView = BaseEditorView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Utility;
        (function (Utility) {
            function containsElementDeep($source, element) {
                var elements = $source.find('*').andSelf().get();
                return elements.filter(function (el) { return el === element; }).length > 0;
            }
            function initGroupEvent(group, eventName) {
                var handlers = [];
                for (var _i = 2; _i < arguments.length; _i++) {
                    handlers[_i - 2] = arguments[_i];
                }
                var $group = $(group);
                var groupEventName = "group:" + eventName;
                var dataFlag = groupEventName + ".initialized";
                function checkAfter() {
                    var target = document.activeElement;
                    if (!containsElementDeep($group, target)) {
                        $group.trigger(groupEventName);
                    }
                }
                if (!$group.data(dataFlag)) {
                    if ($group.attr('tabindex') === undefined) {
                        $group
                            .attr('tabindex', '-1')
                            .css('outline', 'none');
                    }
                    $group
                        .on(eventName, function (e) {
                        setTimeout(checkAfter);
                    })
                        .data(dataFlag, true);
                }
                handlers.forEach(function (fn) { return $group.on(groupEventName, fn); });
            }
            Utility.initGroupEvent = initGroupEvent;
        })(Utility = VIM.Utility || (VIM.Utility = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Attr = Object.freeze({
                    ThresholdName: 'data-threshold-name',
                    Id: 'data-id',
                    Unit: 'data-unit',
                    Valid: 'data-valid',
                    TabId: 'data-tab-id',
                });
                var Messages = Object.freeze({
                    ValidationFailed: 'Validation failed.',
                });
                var Data = Object.freeze({
                    original: 'value.original',
                    changed: 'value.changed',
                });
                var Class = Object.freeze({
                    DisplayName: 'displayName',
                    Threshold: 'thresholdControl',
                    InWarning: 'tbWarningValue',
                    InCritical: 'tbCriticalValue',
                    InOperator: 'ddlInTable',
                    SectionError: 'sectionErrorMessage',
                    SectionWarning: 'sectionWarningMessage',
                });
                var Selector = Object.freeze({
                    Threshold: "." + Class.Threshold,
                    ErrorSection: "." + Class.SectionError,
                    WarningSection: "." + Class.SectionWarning,
                    DisplayName: "." + Class.DisplayName,
                    InWarning: "input." + Class.InWarning,
                    InCritical: "input." + Class.InCritical,
                    InOperator: "select." + Class.InOperator,
                });
                var Templates = Object.freeze({
                    ThresholdEditor: "\n<div class=\"" + Class.Threshold + "\">\n    <div class=\"" + Class.DisplayName + "\"></div>\n    <div>\n        <table class=\"thresholdCustom\"><tbody>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Warn.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}:</td>\n                <td>\n                    <select name=\"ddlOperator\" class=\"" + Class.InOperator + "\">\n                        <option selected=\"selected\" value=\"0\">@{R=Core.Strings;K=ThresholdOperatorEnum_Greater;E=js}</option>\n                        <option value=\"1\">@{R=Core.Strings;K=ThresholdOperatorEnum_GreaterOrEqual;E=js}</option>\n                        <option value=\"2\">@{R=Core.Strings;K=ThresholdOperatorEnum_Equal;E=js}</option>\n                        <option value=\"3\">@{R=Core.Strings;K=ThresholdOperatorEnum_LessOrEqual;E=js}</option>\n                        <option value=\"4\">@{R=Core.Strings;K=ThresholdOperatorEnum_Less;E=js}</option>\n                        <option value=\"5\">@{R=Core.Strings;K=ThresholdOperatorEnum_NotEqual;E=js}</option>\n                    </select>\n                </td>\n                <td><input name=\"tbWarningValue\" type=\"text\" class=\"formulaTextbox " + Class.InWarning + "\"></td>\n            </tr>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Critical.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}:</td>\n                <td><input name=\"tbCriticalValue\" type=\"text\" class=\"formulaTextbox " + Class.InCritical + "\"></td>\n            </tr>\n        </tbody></table>\n        <div class=\"sw-suggestion sw-suggestion-fail " + Class.SectionError + "\"></div>\n        <div class=\"sw-suggestion sw-suggestion-warn " + Class.SectionWarning + "\"></div>\n    </div>\n</div>"
                });
                function getNumberOrNull(value) {
                    return value ? Number(value) : null;
                }
                function formatNumber(value) {
                    return (value !== null && value !== undefined) ? String(parseFloat(value.toFixed(7))) : '';
                }
                var BaseThresholdDom = (function () {
                    function BaseThresholdDom($thr) {
                        this.$thr = $thr;
                    }
                    Object.defineProperty(BaseThresholdDom.prototype, "Messages", {
                        get: function () {
                            return Messages;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$ErrorSection", {
                        get: function () {
                            return this.$thr.find(Selector.ErrorSection);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$WarningSection", {
                        get: function () {
                            return this.$thr.find(Selector.WarningSection);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$DisplayName", {
                        get: function () {
                            return this.$thr.find(Selector.DisplayName);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$Warning", {
                        get: function () {
                            return this.$thr.find(Selector.InWarning);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$Critical", {
                        get: function () {
                            return this.$thr.find(Selector.InCritical);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "$Operator", {
                        get: function () {
                            return this.$thr.find(Selector.InOperator);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "TabId", {
                        get: function () {
                            return this.$thr.attr(Attr.TabId);
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.TabId, value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "ThresholdName", {
                        get: function () {
                            return this.$thr.attr(Attr.ThresholdName);
                        },
                        set: function (name) {
                            this.$thr.attr(Attr.ThresholdName, name);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "Id", {
                        get: function () {
                            return Number(this.$thr.attr(Attr.Id));
                        },
                        set: function (id) {
                            this.$thr.attr(Attr.Id, String(id));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "Unit", {
                        get: function () {
                            return this.$thr.attr(Attr.Unit);
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.Unit, value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "DisplayName", {
                        get: function () {
                            return this.$DisplayName.text();
                        },
                        set: function (value) {
                            this.$DisplayName.text(value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "OriginalData", {
                        get: function () {
                            return this.$thr.data(Data.original);
                        },
                        set: function (value) {
                            this.$thr.data(Data.original, value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "Critical", {
                        get: function () {
                            return getNumberOrNull(this.$Critical.val());
                        },
                        set: function (value) {
                            this.$Critical.val(formatNumber(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "Warning", {
                        get: function () {
                            return getNumberOrNull(this.$Warning.val());
                        },
                        set: function (value) {
                            this.$Warning.val(formatNumber(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "OperatorId", {
                        get: function () {
                            return this.$Operator.val() ? Number(this.$Operator.val()) : 1;
                        },
                        set: function (value) {
                            this.$Operator.val(String(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    BaseThresholdDom.prototype.updateOriginalData = function () {
                        this.OriginalData = this.ThresholdData;
                    };
                    Object.defineProperty(BaseThresholdDom.prototype, "ThresholdData", {
                        get: function () {
                            return this.getThresholdData();
                        },
                        set: function (value) {
                            this.setThresholdData(value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    BaseThresholdDom.prototype.getThresholdData = function () {
                        return {
                            Id: this.Id,
                            ThresholdName: this.ThresholdName,
                            ThresholdOperator: this.OperatorId,
                            Warning: this.Warning,
                            Critical: this.Critical,
                            Unit: this.Unit,
                        };
                    };
                    BaseThresholdDom.prototype.setThresholdData = function (thr) {
                        var _this = this;
                        this.ThresholdName = thr.ThresholdName;
                        this.Id = thr.Id;
                        this.DisplayName = thr.DisplayName || thr.ThresholdName;
                        this.Unit = thr.Unit;
                        this.Warning = thr.Warning;
                        this.$Warning.text(thr.Unit);
                        this.Critical = thr.Critical;
                        this.$Critical.text(thr.Unit);
                        this.$Operator
                            .find('option')
                            .each(function () {
                            var $opt = $(this);
                            $opt.prop('selected', $opt.val() === String(thr.ThresholdOperator));
                        });
                        setTimeout(function () { return _this.handleOperatorChange(); });
                        this.OriginalData = thr;
                    };
                    Object.defineProperty(BaseThresholdDom.prototype, "ThresholdEditorData", {
                        get: function () {
                            return this.getThresholdEditorData();
                        },
                        enumerable: true,
                        configurable: true
                    });
                    BaseThresholdDom.prototype.getThresholdEditorData = function () {
                        var data = this.ThresholdData;
                        var original = this.OriginalData;
                        var valueChanged = (data.Warning != original.Warning) || (data.Critical != original.Critical);
                        return {
                            Threshold: data,
                            IsValid: this.valid,
                            IsChanged: valueChanged,
                        };
                    };
                    Object.defineProperty(BaseThresholdDom.prototype, "valid", {
                        get: function () {
                            var value = this.$thr.attr(Attr.Valid);
                            return value === 'true';
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.Valid, String(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdDom.prototype, "changed", {
                        get: function () {
                            return this.$thr.data(Data.changed);
                        },
                        set: function (value) {
                            this.$thr.data(Data.changed, value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    BaseThresholdDom.prototype.lock = function (lock) {
                        if (lock === void 0) { lock = true; }
                        this.$thr
                            .find('input,select')
                            .prop('disabled', lock);
                    };
                    BaseThresholdDom.prototype.clearErrors = function () {
                        this.valid = true;
                        this.$ErrorSection
                            .empty()
                            .hide();
                    };
                    BaseThresholdDom.prototype.addError = function (errorMessage) {
                        var $msg = $('<div>').text(errorMessage);
                        this.$ErrorSection
                            .append($msg)
                            .show();
                        return $msg;
                    };
                    BaseThresholdDom.prototype.clearWarnings = function () {
                        this.$WarningSection
                            .empty()
                            .hide();
                    };
                    BaseThresholdDom.prototype.addWarning = function (warningMessage) {
                        var $msg = $('<div>').text(warningMessage);
                        this.$WarningSection
                            .append($msg)
                            .show();
                        return $msg;
                    };
                    BaseThresholdDom.prototype.bindEvents = function () {
                        var _this = this;
                        VIM.Utility.initGroupEvent(this.$thr, 'focusout', function (event) {
                            if (_this.changed) {
                                _this.validate();
                            }
                        });
                        this.$thr
                            .on('change', Selector.InOperator, function (event) { return _this.handleOperatorChange(); })
                            .on('change', 'input', function (event) { return _this.changed = true; });
                    };
                    BaseThresholdDom.prototype.validate = function () {
                        var _this = this;
                        this.lock();
                        this.clearErrors();
                        this.clearWarnings();
                        return this.handleValidate(this.ThresholdData)
                            .promise()
                            .always(function () { return _this.lock(false); });
                    };
                    BaseThresholdDom.prototype.handleValidate = function (data) {
                        var dfd = $.Deferred();
                        setTimeout(function () {
                            dfd.resolve();
                        });
                        return dfd;
                    };
                    BaseThresholdDom.prototype.handleOperatorChange = function () {
                        var _this = this;
                        this.validate()
                            .always(function () { return _this.$Operator.focus(); });
                    };
                    return BaseThresholdDom;
                }());
                ThresholdsEditor.BaseThresholdDom = BaseThresholdDom;
                function findOrSelf($el, selector) {
                    return $el.is(selector) ? $el : $el.find(selector);
                }
                var BaseThresholdView = (function () {
                    function BaseThresholdView(editorView) {
                        this.editorView = editorView;
                    }
                    Object.defineProperty(BaseThresholdView, "Class", {
                        get: function () {
                            return Class;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(BaseThresholdView, "Selector", {
                        get: function () {
                            return Selector;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    BaseThresholdView.prototype.getParentThreshold = function ($el) {
                        return this.getDom($el.eq(0).closest(Selector.Threshold));
                    };
                    BaseThresholdView.prototype.findThreshold = function ($el) {
                        return this.getDom(findOrSelf($el, Selector.Threshold));
                    };
                    BaseThresholdView.prototype.getThresholdEditor = function ($root, thresholdName) {
                        return this.getDom(findOrSelf($root, Selector.Threshold + "[" + Attr.ThresholdName + "=\"" + thresholdName + "\"]"));
                    };
                    BaseThresholdView.prototype.findThresholds = function ($el) {
                        return findOrSelf($el, Selector.Threshold);
                    };
                    BaseThresholdView.prototype.getByName = function (tabId, thresholdName) {
                        var $tab = this.editorView.getTabBody(tabId);
                        return thresholdName
                            ? this.getThresholdEditor($tab, thresholdName)
                            : this.findThreshold($tab);
                    };
                    BaseThresholdView.prototype.clearMessages = function (tabId, thresholdName) {
                        var dom = this.getByName(tabId, thresholdName);
                        dom.clearErrors();
                        dom.clearWarnings();
                    };
                    BaseThresholdView.prototype.addWarning = function (tabId, thresholdName, warningMessage) {
                        var dom = this.getByName(tabId, thresholdName);
                        dom.addWarning(warningMessage);
                    };
                    BaseThresholdView.prototype.addError = function (tabId, thresholdName, errorMessage) {
                        var dom = this.getByName(tabId, thresholdName);
                        dom.valid = false;
                        dom.addError(errorMessage);
                    };
                    BaseThresholdView.prototype.renderAllThresholds = function (tabId, thresholds) {
                        var _this = this;
                        var controls = thresholds
                            .map(function (threshold) {
                            var $dom = _this.renderThreshold(tabId, threshold);
                            var dom = _this.findThreshold($dom);
                            dom.ThresholdData = threshold;
                        });
                    };
                    BaseThresholdView.prototype.getAllThresholdData = function (tabId) {
                        var _this = this;
                        var elements = this.findThresholds(this.editorView.getTabBody(tabId)).get();
                        var dataList = elements.map(function (element) { return _this.getDom($(element)).ThresholdEditorData; });
                        return dataList;
                    };
                    BaseThresholdView.prototype.updateAllOriginalData = function (tabId) {
                        var _this = this;
                        this.findThresholds(this.editorView.getTabBody(tabId))
                            .get()
                            .forEach(function (element) { return _this.getDom($(element)).updateOriginalData(); });
                    };
                    BaseThresholdView.prototype.renderThresholdRaw = function (template, tabId, threshold) {
                        var thresholdPrefix = tabId + "_" + threshold.ThresholdName + "_";
                        var $result = VIM.TemplateFormatter.formatDOM(template, threshold, thresholdPrefix);
                        var $thr = this.findThresholds($result).attr(Attr.TabId, tabId);
                        var dom = this.getDom($thr);
                        dom.bindEvents();
                        this.editorView.getTabBody(tabId)
                            .append($result);
                        return $result;
                    };
                    BaseThresholdView.prototype.renderThreshold = function (tabId, threshold) {
                        return this.renderThresholdRaw(Templates.ThresholdEditor, tabId, threshold);
                    };
                    return BaseThresholdView;
                }());
                ThresholdsEditor.BaseThresholdView = BaseThresholdView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var EntityApiController = (function (_super) {
                    __extends(EntityApiController, _super);
                    function EntityApiController() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    EntityApiController.prototype.initialize = function (netObjectId) {
                        var promise = $.ajax({
                            url: this.getActionUrl('InitializeEntity', { netObjectId: netObjectId }),
                        });
                        return this.convertPromiseCustom(promise, function (response) { return ({
                            TabList: response.TabList,
                            Title: response.EntityName,
                            SubTitle: ''
                        }); });
                    };
                    EntityApiController.prototype.getThresholds = function (pluginId, netObjectId) {
                        var promise = $.ajax({
                            url: this.getActionUrl('GetEntityThresholds', { pluginId: pluginId, netObjectId: netObjectId }),
                        });
                        return this.convertPromise(promise);
                    };
                    EntityApiController.prototype.update = function (pluginId, thresholds) {
                        var promise = $.ajax({
                            url: this.getActionUrl('UpdateEntity', { pluginId: pluginId }),
                            type: 'POST',
                            data: {
                                Thresholds: thresholds
                            }
                        });
                        return this.convertPromise(promise);
                    };
                    EntityApiController.prototype.validate = function (pluginId, thresholds) {
                        var dfd = $.Deferred();
                        var thresholdsDone = [];
                        var finalResult = {
                            Errors: []
                        };
                        function checkFinish(thresholdName) {
                            thresholdsDone.push(thresholdName);
                            if (thresholdsDone.length === thresholds.length) {
                                dfd.resolve(finalResult);
                            }
                        }
                        thresholds.forEach(function (thr) {
                            var data = {
                                CriticalFormula: thr.CriticalFormula || String(thr.Critical),
                                WarningFormula: thr.WarningFormula || String(thr.Warning),
                                InstancesId: [thr.InstanceId],
                                Operator: thr.ThresholdOperator,
                                ThresholdName: thr.ThresholdName,
                            };
                            $.ajax({
                                url: '/api/Thresholds/Compute',
                                type: 'POST',
                                data: data
                            }).done(function (result) {
                                var _a;
                                var errors = result.ErrorMessages.map(function (msg) { return ({ Message: msg, ThresholdName: thr.ThresholdName }); });
                                (_a = finalResult.Errors).push.apply(_a, errors);
                                checkFinish(thr.ThresholdName);
                            }).fail(function (err) {
                                finalResult.Errors.push({
                                    Message: err,
                                    ThresholdName: thr.ThresholdName
                                });
                                checkFinish(thr.ThresholdName);
                            });
                        });
                        return dfd.promise();
                    };
                    EntityApiController.prototype.compute = function (data) {
                        var promise = $.ajax({
                            url: '/api/Thresholds/Compute',
                            type: 'POST',
                            data: data
                        });
                        return this.convertPromise(promise);
                    };
                    return EntityApiController;
                }(ThresholdsEditor.BaseApiController));
                ThresholdsEditor.EntityApiController = EntityApiController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Messages = {
                    TabLoadFailed: 'failed to load tab information ({{Message}})',
                    TabUpdateFailed: '{{Name}}: {{Message}}',
                    TabUpdateSuccess: '{{Name}}: {{Message}}',
                };
                var EntityEditorController = (function (_super) {
                    __extends(EntityEditorController, _super);
                    function EntityEditorController(api, editorView, thresholdView, location) {
                        var _this = _super.call(this, editorView, thresholdView, location) || this;
                        _this.api = api;
                        return _this;
                    }
                    EntityEditorController.prototype.initialize = function (opts) {
                        var _this = this;
                        _super.prototype.initialize.call(this, opts);
                        this.netObjectId = opts.objectId;
                        this.api.initialize(this.netObjectId)
                            .done(function (data) { return _this.handleInitDone(data); })
                            .fail(function (err) { return _this.handleInitFail(err); });
                    };
                    EntityEditorController.prototype.handleInitDone = function (data) {
                        var _this = this;
                        _super.prototype.handleInitDone.call(this, data);
                        this.editorView.updateTitle(data.Title);
                        this.tabs.forEach(function (tab) {
                            _this.api.getThresholds(tab.Id, _this.netObjectId)
                                .done(function (result) { return _this.handleLoadDone(result); })
                                .fail(function (error) { return _this.handleLoadFail(tab.Id, error); });
                        });
                    };
                    EntityEditorController.prototype.updateTab = function (tab, thresholds) {
                        var _this = this;
                        var dfd = $.Deferred();
                        this.api.update(tab.Id, thresholds)
                            .fail(function (err) {
                            _this.reportTabUpdateFail(tab, err);
                            ThresholdsEditor.resolveUpdateStatus(dfd, tab.Id, false);
                        })
                            .done(function (updateResult) {
                            var success = _this.reportTabUpdateDone(tab, updateResult);
                            ThresholdsEditor.resolveUpdateStatus(dfd, tab.Id, success);
                        });
                        return dfd;
                    };
                    return EntityEditorController;
                }(ThresholdsEditor.BaseEditorController));
                ThresholdsEditor.EntityEditorController = EntityEditorController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var EntityEditorView = (function (_super) {
                    __extends(EntityEditorView, _super);
                    function EntityEditorView($container) {
                        return _super.call(this, $container) || this;
                    }
                    return EntityEditorView;
                }(ThresholdsEditor.BaseEditorView));
                ThresholdsEditor.EntityEditorView = EntityEditorView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Attr = {
                    InstanceId: 'data-instance-id',
                };
                var Class = Object.freeze({
                    ComputedWarning: 'lblComputedWarningValue',
                    ComputedCritical: 'lblComputedCriticalValue',
                    ComputedOperator: 'lblOperatorText',
                });
                var Selector = Object.freeze({
                    ComputedWarning: "." + Class.ComputedWarning,
                    ComputedCritical: "." + Class.ComputedCritical,
                    ComputedOperator: "." + Class.ComputedOperator,
                });
                var Templates = Object.freeze({
                    ThresholdEditor: "\n<div class=\"" + ThresholdsEditor.BaseThresholdView.Class.Threshold + "\">\n    <div class=\"" + ThresholdsEditor.BaseThresholdView.Class.DisplayName + "\"></div>\n    <div>\n        <table class=\"thresholdCustom\"><tbody>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Warn.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}:</td>\n                <td>\n                    <select name=\"ddlOperator\" class=\"" + ThresholdsEditor.BaseThresholdView.Class.InOperator + "\">\n                        <option selected=\"selected\" value=\"0\">@{R=Core.Strings;K=ThresholdOperatorEnum_Greater;E=js}</option>\n                        <option value=\"1\">@{R=Core.Strings;K=ThresholdOperatorEnum_GreaterOrEqual;E=js}</option>\n                        <option value=\"2\">@{R=Core.Strings;K=ThresholdOperatorEnum_Equal;E=js}</option>\n                        <option value=\"3\">@{R=Core.Strings;K=ThresholdOperatorEnum_LessOrEqual;E=js}</option>\n                        <option value=\"4\">@{R=Core.Strings;K=ThresholdOperatorEnum_Less;E=js}</option>\n                        <option value=\"5\">@{R=Core.Strings;K=ThresholdOperatorEnum_NotEqual;E=js}</option>\n                    </select>\n                </td>\n                <td><input name=\"tbWarningValue\" type=\"text\" class=\"formulaTextbox " + ThresholdsEditor.BaseThresholdView.Class.InWarning + "\"></td>\n                <td><span class=\"" + Class.ComputedWarning + "\"></span></td>\n            </tr>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Critical.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}:</td>\n                <td><span class=\"" + Class.ComputedOperator + "\"></span></td>\n                <td><input name=\"tbCriticalValue\" type=\"text\" class=\"formulaTextbox " + ThresholdsEditor.BaseThresholdView.Class.InCritical + "\"></td>\n                <td><span class=\"" + Class.ComputedCritical + "\"></span></td>\n            </tr>\n        </tbody></table>\n        <div class=\"sw-suggestion sw-suggestion-fail " + ThresholdsEditor.BaseThresholdView.Class.SectionError + "\"></div>\n        <div class=\"sw-suggestion sw-suggestion-warn " + ThresholdsEditor.BaseThresholdView.Class.SectionWarning + "\"></div>\n    </div>\n</div>"
                });
                var EntityThresholdDom = (function (_super) {
                    __extends(EntityThresholdDom, _super);
                    function EntityThresholdDom($thr, api) {
                        var _this = _super.call(this, $thr) || this;
                        _this.api = api;
                        return _this;
                    }
                    Object.defineProperty(EntityThresholdDom.prototype, "$OperatorText", {
                        get: function () {
                            return this.$thr.find(Selector.ComputedOperator);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(EntityThresholdDom.prototype, "$ComputedWarning", {
                        get: function () {
                            return this.$thr.find(Selector.ComputedWarning);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(EntityThresholdDom.prototype, "$ComputedCritical", {
                        get: function () {
                            return this.$thr.find(Selector.ComputedCritical);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(EntityThresholdDom.prototype, "InstanceId", {
                        get: function () {
                            return Number(this.$thr.attr(Attr.InstanceId));
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.InstanceId, String(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    EntityThresholdDom.prototype.setThresholdData = function (thr) {
                        _super.prototype.setThresholdData.call(this, thr);
                        this.InstanceId = thr.InstanceId;
                        this.updateComputedValue(this.$ComputedWarning, thr.Warning, thr.Unit);
                        this.updateComputedValue(this.$ComputedCritical, thr.Critical, thr.Unit);
                    };
                    EntityThresholdDom.prototype.getThresholdData = function () {
                        var data = _super.prototype.getThresholdData.call(this);
                        data.InstanceId = this.InstanceId;
                        data.CriticalFormula = '';
                        data.WarningFormula = '';
                        return data;
                    };
                    EntityThresholdDom.prototype.updateComputedValue = function ($ctl, value, unit) {
                        $ctl.text("= " + value + " " + unit);
                    };
                    EntityThresholdDom.prototype.handleValidate = function (data) {
                        var _this = this;
                        var dfd = $.Deferred();
                        var request = {
                            ThresholdName: this.ThresholdName,
                            InstancesId: [this.InstanceId],
                            CriticalFormula: String(this.Critical),
                            WarningFormula: String(this.Warning),
                            Operator: this.OperatorId,
                        };
                        this.api.compute(request)
                            .done(function (result) {
                            if (typeof result === 'string') {
                                _this.$ErrorSection.text(String(result));
                                dfd.resolve();
                                return;
                            }
                            if (result.WarningMessage) {
                                _this.addWarning(result.WarningMessage);
                            }
                            _this.valid = result.IsValid;
                            if (!result.IsValid) {
                                if (result.ErrorMessages && result.ErrorMessages.length) {
                                    result.ErrorMessages.forEach(function (msg) { return _this.addError(msg); });
                                }
                                else {
                                    _this.addError(_this.Messages.ValidationFailed);
                                }
                                dfd.reject();
                            }
                            else {
                                if (result.IsComputed) {
                                    _this.updateComputedValue(_this.$ComputedWarning, result.WarningThreshold, _this.Unit);
                                    _this.updateComputedValue(_this.$ComputedCritical, result.CriticalThreshold, _this.Unit);
                                }
                                dfd.resolve();
                            }
                        })
                            .fail(function (err) {
                            _this.valid = false;
                            _this.addError(err.Message);
                            dfd.reject();
                        })
                            .always(function () { return _this.changed = false; });
                        return dfd;
                    };
                    EntityThresholdDom.prototype.handleOperatorChange = function () {
                        this.$OperatorText
                            .text(this.$Operator.find('option:selected').text());
                        _super.prototype.handleOperatorChange.call(this);
                    };
                    return EntityThresholdDom;
                }(ThresholdsEditor.BaseThresholdDom));
                ThresholdsEditor.EntityThresholdDom = EntityThresholdDom;
                var EntityThresholdView = (function (_super) {
                    __extends(EntityThresholdView, _super);
                    function EntityThresholdView(api, editorView) {
                        var _this = _super.call(this, editorView) || this;
                        _this.api = api;
                        return _this;
                    }
                    EntityThresholdView.prototype.getDom = function ($thr) {
                        return new EntityThresholdDom($thr, this.api);
                    };
                    EntityThresholdView.prototype.renderThreshold = function (tabId, threshold) {
                        return _super.prototype.renderThresholdRaw.call(this, Templates.ThresholdEditor, tabId, threshold);
                    };
                    return EntityThresholdView;
                }(ThresholdsEditor.BaseThresholdView));
                ThresholdsEditor.EntityThresholdView = EntityThresholdView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var GlobalApiController = (function (_super) {
                    __extends(GlobalApiController, _super);
                    function GlobalApiController() {
                        return _super !== null && _super.apply(this, arguments) || this;
                    }
                    GlobalApiController.prototype.initialize = function (areaId) {
                        var promise = $.ajax({
                            url: this.getActionUrl('InitializeGlobal', { areaId: areaId }),
                        });
                        return this.convertPromiseCustom(promise, function (response) { return ({
                            TabList: response.TabList,
                            Title: response.AreaTitle,
                            SubTitle: response.AreaSubTitle
                        }); });
                    };
                    GlobalApiController.prototype.getThresholds = function (pluginId, areaId) {
                        var promise = $.ajax({
                            url: this.getActionUrl('GetGlobalThresholds', { pluginId: pluginId, areaId: areaId }),
                        });
                        return this.convertPromise(promise);
                    };
                    GlobalApiController.prototype.update = function (pluginId, thresholds) {
                        var promise = $.ajax({
                            url: this.getActionUrl('UpdateGlobal', { pluginId: pluginId }),
                            type: 'POST',
                            data: {
                                Thresholds: thresholds
                            }
                        });
                        return this.convertPromise(promise);
                    };
                    GlobalApiController.prototype.validate = function (pluginId, thresholds) {
                        var promise = $.ajax({
                            url: this.getActionUrl('ValidateGlobalThresholds', { pluginId: pluginId }),
                            type: 'POST',
                            data: {
                                Thresholds: thresholds
                            }
                        });
                        return this.convertPromise(promise);
                    };
                    return GlobalApiController;
                }(ThresholdsEditor.BaseApiController));
                ThresholdsEditor.GlobalApiController = GlobalApiController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Messages = {
                    TabLoadFailed: 'failed to load tab information ({{Message}})',
                    TabUpdateFailed: '{{Name}}: {{Message}}',
                    TabUpdateSuccess: '{{Name}}: {{Message}}',
                };
                var GlobalEditorController = (function (_super) {
                    __extends(GlobalEditorController, _super);
                    function GlobalEditorController(api, editorView, thresholdView, location) {
                        var _this = _super.call(this, editorView, thresholdView, location) || this;
                        _this.api = api;
                        return _this;
                    }
                    GlobalEditorController.prototype.initialize = function (opts) {
                        var _this = this;
                        _super.prototype.initialize.call(this, opts);
                        this.areaId = opts.objectId;
                        this.api.initialize(this.areaId)
                            .done(function (data) { return _this.handleInitDone(data); })
                            .fail(function (err) { return _this.handleInitFail(err); });
                    };
                    GlobalEditorController.prototype.handleInitDone = function (data) {
                        var _this = this;
                        _super.prototype.handleInitDone.call(this, data);
                        this.editorView.updateTitle(data.Title);
                        this.editorView.updateSubTitle(data.SubTitle);
                        this.tabs.forEach(function (tab) {
                            _this.api.getThresholds(tab.Id, _this.areaId)
                                .done(function (result) { return _this.handleLoadDone(result); })
                                .fail(function (error) { return _this.handleLoadFail(tab.Id, error); });
                        });
                    };
                    GlobalEditorController.prototype.updateTab = function (tab, thresholds) {
                        var _this = this;
                        var dfd = $.Deferred();
                        this.api.update(tab.Id, thresholds)
                            .fail(function (err) {
                            _this.reportTabUpdateFail(tab, err);
                            ThresholdsEditor.resolveUpdateStatus(dfd, tab.Id, false);
                        })
                            .done(function (updateResult) {
                            var success = _this.reportTabUpdateDone(tab, updateResult);
                            ThresholdsEditor.resolveUpdateStatus(dfd, tab.Id, success);
                        });
                        return dfd;
                    };
                    return GlobalEditorController;
                }(ThresholdsEditor.BaseEditorController));
                ThresholdsEditor.GlobalEditorController = GlobalEditorController;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var GlobalEditorView = (function (_super) {
                    __extends(GlobalEditorView, _super);
                    function GlobalEditorView($container) {
                        return _super.call(this, $container) || this;
                    }
                    return GlobalEditorView;
                }(ThresholdsEditor.BaseEditorView));
                ThresholdsEditor.GlobalEditorView = GlobalEditorView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var Attr = {
                    AdditionalData: 'data-additional',
                    Description: 'data-description',
                    Minimum: 'data-minimum',
                    Maximum: 'data-maximum',
                };
                var Class = Object.freeze({
                    DisplayOperator: 'displayOperator',
                    DisplayUnit: 'displayUnit',
                    WarningDescription: 'warningDescription',
                    CriticalDescription: 'criticalDescription',
                });
                var Selector = Object.freeze({
                    DisplayUnit: "." + Class.DisplayUnit,
                    DisplayOperator: "." + Class.DisplayOperator,
                });
                var Templates = Object.freeze({
                    ThresholdEditor: "\n<div class=\"" + ThresholdsEditor.BaseThresholdView.Class.Threshold + "\">\n    <div class=\"" + ThresholdsEditor.BaseThresholdView.Class.DisplayName + "\"></div>\n    <div>\n        <table class=\"thresholdCustom\"><tbody>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Warn.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}:</td>\n                <td>\n                    <span class=\"" + Class.DisplayOperator + "\"></span>\n                </td>\n                <td><input name=\"tbWarningValue\" type=\"text\" class=\"formulaTextbox " + ThresholdsEditor.BaseThresholdView.Class.InWarning + "\"><span class=\"" + Class.DisplayUnit + "\"></span></td>\n                <td><span class=\"" + Class.WarningDescription + "\"></span></td>\n            </tr>\n            <tr>\n                <td><img alt=\"@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}\" src=\"/Orion/images/ThresholdControl/Small-Up-Critical.gif\"></td>\n                <td>@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}:</td>\n                <td><span class=\"" + Class.DisplayOperator + "\"></span></td>\n                <td><input name=\"tbCriticalValue\" type=\"text\" class=\"formulaTextbox " + ThresholdsEditor.BaseThresholdView.Class.InCritical + "\"><span class=\"" + Class.DisplayUnit + "\"></span></td>\n                <td><span class=\"" + Class.CriticalDescription + "\"></span></td>\n            </tr>\n        </tbody></table>\n        <div class=\"sw-suggestion sw-suggestion-fail " + ThresholdsEditor.BaseThresholdView.Class.SectionError + "\"></div>\n        <div class=\"sw-suggestion sw-suggestion-warn " + ThresholdsEditor.BaseThresholdView.Class.SectionWarning + "\"></div>\n    </div>\n</div>"
                });
                var getOperatorText = (function () {
                    var text = {};
                    text[2] = '=';
                    text[0] = '>';
                    text[1] = '>=';
                    text[4] = '<';
                    text[3] = '<=';
                    text[5] = '!=';
                    return (function (value) { return text[value] || ''; });
                }());
                var GlobalThresholdDom = (function (_super) {
                    __extends(GlobalThresholdDom, _super);
                    function GlobalThresholdDom($thr, api) {
                        var _this = _super.call(this, $thr) || this;
                        _this.api = api;
                        return _this;
                    }
                    Object.defineProperty(GlobalThresholdDom.prototype, "AdditionalData", {
                        get: function () {
                            return this.$thr.attr(Attr.AdditionalData);
                        },
                        set: function (data) {
                            this.$thr.attr(Attr.AdditionalData, data);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "Description", {
                        get: function () {
                            return this.$thr.attr(Attr.Description);
                        },
                        set: function (text) {
                            this.$thr.attr(Attr.Description, text);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "Minimum", {
                        get: function () {
                            return Number(this.$thr.attr(Attr.Minimum));
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.Minimum, String(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "Maximum", {
                        get: function () {
                            return Number(this.$thr.attr(Attr.Maximum));
                        },
                        set: function (value) {
                            this.$thr.attr(Attr.Maximum, String(value));
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "$DisplayUnit", {
                        get: function () {
                            return this.$thr.find(Selector.DisplayUnit);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "DisplayUnit", {
                        set: function (value) {
                            this.$DisplayUnit.text(value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "$DisplayOperator", {
                        get: function () {
                            return this.$thr.find(Selector.DisplayOperator);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(GlobalThresholdDom.prototype, "DisplayOperator", {
                        set: function (value) {
                            this.$DisplayOperator.text(value);
                        },
                        enumerable: true,
                        configurable: true
                    });
                    GlobalThresholdDom.prototype.setThresholdData = function (thr) {
                        _super.prototype.setThresholdData.call(this, thr);
                        this.DisplayUnit = thr.Unit;
                        this.DisplayOperator = getOperatorText(thr.ThresholdOperator);
                        this.AdditionalData = thr.AdditionalData;
                        this.Description = thr.Description;
                        this.Minimum = thr.Minimum;
                        this.Maximum = thr.Maximum;
                    };
                    GlobalThresholdDom.prototype.getThresholdData = function () {
                        var data = _super.prototype.getThresholdData.call(this);
                        data.AdditionalData = this.AdditionalData;
                        data.Description = this.Description;
                        data.Minimum = this.Minimum;
                        data.Maximum = this.Maximum;
                        return data;
                    };
                    GlobalThresholdDom.prototype.handleValidate = function (threshold) {
                        var _this = this;
                        var dfd = $.Deferred();
                        this.api.validate(this.TabId, [threshold])
                            .done(function (result) {
                            var isValid = result.Errors.length === 0;
                            _this.valid = isValid;
                            if (!isValid) {
                                if (result.Errors && result.Errors.length) {
                                    result.Errors.forEach(function (msg) { return _this.addError(msg.Message); });
                                }
                                else {
                                    _this.addError(_this.Messages.ValidationFailed);
                                }
                                dfd.reject();
                            }
                            dfd.resolve();
                        })
                            .fail(function (err) {
                            _this.valid = false;
                            _this.addError(err.Message);
                            dfd.reject();
                        })
                            .always(function () { return _this.changed = false; });
                        return dfd;
                    };
                    return GlobalThresholdDom;
                }(ThresholdsEditor.BaseThresholdDom));
                ThresholdsEditor.GlobalThresholdDom = GlobalThresholdDom;
                var GlobalThresholdView = (function (_super) {
                    __extends(GlobalThresholdView, _super);
                    function GlobalThresholdView(api, editorView) {
                        var _this = _super.call(this, editorView) || this;
                        _this.api = api;
                        return _this;
                    }
                    GlobalThresholdView.prototype.getDom = function ($thr) {
                        return new GlobalThresholdDom($thr, this.api);
                    };
                    GlobalThresholdView.prototype.renderThreshold = function (tabId, threshold) {
                        return _super.prototype.renderThresholdRaw.call(this, Templates.ThresholdEditor, tabId, threshold);
                    };
                    return GlobalThresholdView;
                }(ThresholdsEditor.BaseThresholdView));
                ThresholdsEditor.GlobalThresholdView = GlobalThresholdView;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                var LocationWrapper = (function () {
                    function LocationWrapper() {
                    }
                    LocationWrapper.prototype.getReferrer = function () {
                        return window.document.referrer;
                    };
                    LocationWrapper.prototype.setLocation = function (url) {
                        window.location.href = url;
                    };
                    LocationWrapper.prototype.close = function () {
                        window.close();
                    };
                    return LocationWrapper;
                }());
                ThresholdsEditor.LocationWrapper = LocationWrapper;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
var SW;
(function (SW) {
    var VIM;
    (function (VIM) {
        var Admin;
        (function (Admin) {
            var ThresholdsEditor;
            (function (ThresholdsEditor) {
                function initialize(opts) {
                    if (opts.netObjectId) {
                        return initializeEntity({
                            api: new ThresholdsEditor.EntityApiController(),
                            editorView: new ThresholdsEditor.EntityEditorView(opts.container),
                            objectId: opts.netObjectId,
                            returnUrl: opts.returnUrl,
                            isDemo: opts.isDemo
                        });
                    }
                    if (opts.globalAreaId) {
                        return initializeGlobal({
                            api: new ThresholdsEditor.GlobalApiController(),
                            editorView: new ThresholdsEditor.GlobalEditorView(opts.container),
                            objectId: opts.globalAreaId,
                            returnUrl: opts.returnUrl,
                            isDemo: opts.isDemo
                        });
                    }
                    console.error("neither netObjectId nor globalAreaId specified!");
                    throw new Error("options mismatch");
                }
                ThresholdsEditor.initialize = initialize;
                function initializeEntity(options) {
                    var api = options.api;
                    var editorView = options.editorView;
                    var thresholdView = options.thresholdView || new ThresholdsEditor.EntityThresholdView(api, editorView);
                    var location = options.location || new ThresholdsEditor.LocationWrapper();
                    var editor = new ThresholdsEditor.EntityEditorController(api, editorView, thresholdView, location);
                    editor.initialize(options);
                    return editor;
                }
                ThresholdsEditor.initializeEntity = initializeEntity;
                function initializeGlobal(options) {
                    var api = options.api;
                    var editorView = options.editorView;
                    var thresholdView = options.thresholdView || new ThresholdsEditor.GlobalThresholdView(api, editorView);
                    var location = options.location || new ThresholdsEditor.LocationWrapper();
                    var isDemo = options.isDemo;
                    var editor = new ThresholdsEditor.GlobalEditorController(api, editorView, thresholdView, location);
                    editor.initialize(options);
                    return editor;
                }
                ThresholdsEditor.initializeGlobal = initializeGlobal;
            })(ThresholdsEditor = Admin.ThresholdsEditor || (Admin.ThresholdsEditor = {}));
        })(Admin = VIM.Admin || (VIM.Admin = {}));
    })(VIM = SW.VIM || (SW.VIM = {}));
})(SW || (SW = {}));
//# sourceMappingURL=ThresholdsEditor.gen.js.map