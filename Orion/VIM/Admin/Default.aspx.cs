﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

/// <summary>
/// Summary description for Default
/// </summary>
public partial class Orion_VIM_Admin_Default : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        Response.Redirect("~/Orion/VIM/Admin/VMwareServers.aspx");
    }
}