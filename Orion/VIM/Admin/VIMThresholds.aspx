<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true"
    CodeFile="VIMThresholds.aspx.cs" Inherits="Orion_VIM_Admin_VIMThresholds" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_IY0_1%>" %>

<%@ Register TagPrefix="vim" TagName="ThresholdSetting" Src="~/Orion/VIM/Controls/ThresholdSetting.ascx" %>
<%@ Register Src="~/Orion/Controls/HelpButton.ascx" TagPrefix="orion" TagName="HelpButton" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="adminHeadPlaceholder">
    <style type="text/css">
        #adminContentTable td
        {
            padding: 10px 10px 10px 10px;
        }
        #inside_table td
        {
            padding: 5px;
            vertical-align: middle;
            margin: 1px 0;
        }
        #inside_table td.Property img
        {
            width: 15px;
            height: 15px;
        }
        #inside_table .header td
        {
            padding-left: 0px;
            font-weight: bold;
        }
        .footer_buttons
        {
            margin: 10px 0px 0px 0px;
            align: center;
        }
        #adminContent h2
        {
            margin: 10px 0px;
        }
        #inside_table tr
        {
            line-height: 30%;
        }
        .vimAdminDateArea
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="adminContentPlaceholder">
    <table class="PageHeader HeaderTable" cellspacing="0" cellpadding="0">
        <tr>
            <td width="60%">
                <h1>
                    <asp:Label ID="thresholds_title" runat="server" />
                </h1>
            </td>
        </tr>
    </table>
    <asp:Label Width="60%" ID="thresholds_description" runat="server">
                    <%= Resources.VIMWebContent.VIMWEBDATA_VB1_1%>
    </asp:Label>
    <br/><br/>
    <table id="adminContentTable" border="0" cellspacing="0" style="width: 60%">
        <tr>
            <td>
                <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <asp:PlaceHolder ID="thresholdsHere" runat="server" />
                </table>
                <asp:Repeater ID="SettingsRepeater" runat="server" OnItemDataBound="OnSettingsRepeater_ItemDataBound">
                    <HeaderTemplate>
                        <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%"
                            style="border-bottom-style: none">
                            <tr>
                                <td colspan="3" style="padding-left: 0px">
                                    <h2>
                                        <%= Resources.VIMWebContent.VIMWEBDATA_VB1_3%>
                                    </h2>
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="settingItemRow" runat="server" class="alternateRow">
                            <td class="PropertyHeader" style="width: 32%;">
                                <%# Eval("HeaderText") %>
                            </td>
                            <td style="width: 8%; text-align: right">
                                <asp:Label ID="textBeforeTB" runat="server"></asp:Label>
                            </td>
                            <td colspan="5" class="Property">
                                <asp:Panel ID="listPanel" runat="server" Visible="false">
                                    <asp:ListBox ID="ListBox" runat="server" Rows="1">
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_4%>" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_5%>" Value="5" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_6%>" Value="7"></asp:ListItem>
                                    </asp:ListBox>
                                    <%# Eval("Range") %>
                                </asp:Panel>
                                <asp:Panel ID="textPanel" runat="server" Visible="false">
                                    <asp:TextBox ID="valueTB" runat="server" Width="50"></asp:TextBox>
                                    &nbsp;
                                    <asp:Label ID="rangeLB" runat="server">
                                <%# Eval("Range") %>
                                    </asp:Label>
                                </asp:Panel>
                                <asp:RequiredFieldValidator runat="server" ID="valueTBRequiredFieldValidator1" Display="Dynamic"
                                    ControlToValidate="valueTB" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_7%>"><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="valueTBRangeValidator1" Display="Dynamic" runat="server"
                                    ControlToValidate="valueTB" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>"
                                    MinimumValue='<%#Eval("Min")%>' MaximumValue='<%#Eval("Max")%>' Type="Integer"><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RangeValidator>
                                <div class="vimThresholdDescription" id="descLB" runat="server">
                                    <%# Eval("Description") %></div>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr id="settingItemRow" runat="server">
                            <td class="PropertyHeader" style="width: 32%;">
                                <%# Eval("HeaderText") %>
                            </td>
                            <td style="width: 8%; text-align: right">
                                <asp:Label ID="textBeforeTB" runat="server"></asp:Label>
                            </td>
                            <td colspan="5" class="Property">
                                <asp:Panel ID="listPanel" runat="server" Visible="false">
                                    <asp:ListBox ID="ListBox" runat="server" Rows="1">
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_4%>" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_5%>" Value="5" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="<%= Resources.VIMWebContent.VIMWEBDATA_VB1_6%>" Value="7"></asp:ListItem>
                                    </asp:ListBox>
                                    <%# Eval("Range") %>
                                </asp:Panel>
                                <asp:Panel ID="textPanel" runat="server" Visible="false">
                                    <asp:TextBox ID="valueTB" runat="server" Width="50"></asp:TextBox>
                                    &nbsp;
                                    <asp:Label ID="rangeLB" runat="server">
                                <%# Eval("Range") %>
                                    </asp:Label>
                                </asp:Panel>
                                <asp:RequiredFieldValidator runat="server" ID="valueTBRequiredFieldValidator1" Display="Dynamic"
                                    ControlToValidate="valueTB" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_7%>"><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="valueTBRangeValidator1" Display="Dynamic" runat="server"
                                    ControlToValidate="valueTB" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>"
                                    MinimumValue='<%#Eval("Min")%>' MaximumValue='<%#Eval("Max")%>' Type="Integer"><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RangeValidator>
                                <div class="vimThresholdDescription" id="descLB" runat="server">
                                    <%# Eval("Description") %></div>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <table id="inside_table" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <asp:PlaceHolder ID="thresholdsHere2" runat="server" />
                </table>
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar">
        <orion:LocalizableButton ID="btnCollisionCancel" runat="server" DisplayType="Primary"
            LocalizedText="Submit" OnClick="SubmitClick" />
    </div>
</asp:Content>
