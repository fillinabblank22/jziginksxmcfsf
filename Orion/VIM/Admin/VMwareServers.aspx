﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" CodeFile="VMwareServers.aspx.cs" 
    Inherits="Orion_VMwareServers" Title="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_68%>" %>
<%@ Import Namespace="SolarWinds.VIM.Web.DAL" %>

<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/VIM/Admin/VMwareTabs.ascx" TagPrefix="vim" TagName="VMwareTabs" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false" Version="3.4"/>
    <%--<script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <script type="text/javascript" src="/Orion/VIM/js/ScriptServiceInvoker.js"></script>
    <script type="text/javascript" src="/Orion/VIM/js/OrionCoreServiceProxy.js"></script>
    <script type="text/javascript" src="/Orion/VIM/js/VMwareServers.js"></script>--%>
    
    <orion:Include runat="server" Module="VIM" File="VMWareServers.css" />

    <orion:Include runat="server" File="OrionCore.js"/>
    <orion:Include runat="server" Module="VIM" File="ScriptServiceInvoker.js"/>
    <orion:Include runat="server" Module="VIM" File="OrionCoreScriptServiceProxy.js"/>
    <orion:Include runat="server" Module="VIM" File="VMwareServers.js"/>
    <orion:Include runat="server" Module="VIM" File="DemoServerHelper.js" />

    <style type="text/css">
        .assign { background-image:url(/Orion/VIM/images/icon_assign_cred_16x16.gif) !important; }
        .enablePolling { background-image:url(/Orion/VIM/images/enable_monitoring.gif) !important; }
        .disablePolling { background-image:url(/Orion/VIM/images/disable_monitoring.gif) !important; }
        .vimAdminDateArea {display:none;}
        #adminContent { padding-top: 0; }
        #adminContent .dialogBody table td { padding-top: 5px; padding-left: 10px; }
        #assignDialog td { padding-right: 0; padding-bottom: 0; }
        #testButton table td { padding: 0px !important;}
        #assignDialog td.x-btn-center { text-align: center !important; }
        #Grid td { padding-right: 0; padding-bottom: 0; vertical-align:middle; }
        #cancelAssignButton td { text-align: center; }
        
        ul.vim-bullet { list-style-type: disc !important; margin-left: 20px }
        .ext-el-mask-msg { z-index: 9002}
        .GroupItem, .SelectedGroupItem 
        {
            height:18px;
            list-style-type:none;
            padding:2px 2px 2px 6px;
            white-space:nowrap;
        }
        .SelectedGroupItem 
        { 
            background-color:#97D6FF; 
            background-image:url(/Orion/Nodes/images/background/left_selection_gradient.gif);
            background-repeat:repeat-x;
            font-weight:bold;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <asp:ScriptManagerProxy id="smp" runat="server">
    </asp:ScriptManagerProxy>
    <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
    <%--<input type="hidden" name="VIM_VMwareServers_GroupBy" id="VIM_VMwareServers_GroupBy" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_VMwareServers_GroupBy")%>' />
    <input type="hidden" name="VIM_VMwareServers_GroupByValue" id="VIM_VMwareServers_GroupByValue" value='<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_VMwareServers_GroupByValue")%>' />
    --%>
    
    <input type="hidden" name="VIM_VMwareServers_PageSize" id="VIM_VMwareServers_PageSize" value="<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_VMwareServers_PageSize")%>" />
    <input type="hidden" name="VIM_VMwareServers_HiddenColumns" id="VIM_VMwareServers_HiddenColumns" value="<%=SolarWinds.Orion.Web.DAL.WebUserSettingsDAL.Get("VIM_VMwareServers_HiddenColumns")%>" />

    <%-- Render the DIV control only in demo mode --%>
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
        <div id="isDemoMode" style="display:none;"></div>
    <%}%>

    <h1 style="font-size:large"><%= Page.Title %></h1>
    
    <div style="padding: 1em 0 1em 0;"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_43%></div>
    
    <div style="width:1077px;">    
        <vim:VMwareTabs ID="VMwareTabs1" runat="server" />
        
        <div id="tabPanel" class="tab-top">

                <table class="ESXServersTable" cellpadding="0" cellspacing="0" width="100%" >
                    <tr valign="top" align="left">
<%--                        <td style="padding-right: 10px;width: 180px;">
                            <div class="Grouping">
                                <div class="GroupSection">
                                    <div>Group by:</div>
                                    <select id="groupByProperty" style="width:100%">
                                        <option value="">[No Grouping]</option>
                                        <option value="n.Status">Node Status</option>
                                        <option value="v.HostStatus">Polling Status</option>
                                        <%foreach (string prop in new NodesCustomPropertiesDAL().GetCustomPropertyNames(true, true))
                                        {
                                            %>
                                                <option value="n.CustomProperties.<%=prop%>"><%=prop%></option>
                                            <%
                                        }%>
                                    </select>
                                </div>
                                <ul class="GroupItems" style="width:180px;"></ul>
                            </div>
                        </td>--%>

                        <td id="gridCell" style="padding-right: 0px;">
                            <div id="Grid"/>
                        </td>
                    </tr>
                </table>
        </div>
    </div>

    <div id="assignDialog" class="x-hidden">
        <div class="x-window-header"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_33%></div>
        <div id="assignDialogBody" class="x-panel-body dialogBody">
        <table width="100%">
            <tr>
                <td colspan='2'><%= Resources.VIMWebContent.VIMWEBDATA_TM0_44%></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_4%></span></td>
                <td><select ID="assignList" style="width: 150px;"></select></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_3%></span></td>
                <td><input ID="assignDescription" maxlength="190"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_7%></span></td>
                <td><input ID="assignUsername" maxlength="190"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%></span></td>
                <td><input ID="assignPassword" type="password" maxlength="190"></input></td>
            </tr>
            <tr>
                <td><span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_12%></span></td>
                <td><input ID="assignConfirm" type="password" maxlength="190"></input></td>
            </tr>
            <tr>
                <td></td>
                <td><div id="testButton"></div></td>
            </tr>
            <tr>
                <td colspan='2'><div class="Error"></div></td>
            </tr>
        </table>
        </div>
    </div>

</asp:Content>

