<%@ Control Language="C#" ClassName="VMwareTabs" %>

<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<orion:NavigationTabBar ID="NavigationTabBar1" runat="server" Visible="true">
    <orion:NavigationTabItem Name="<%$ Resources:VIMWebContent,VIMWEBDATA_LB0_5%>" Url="~/Orion/VIM/Admin/HyperVServers.aspx" />
    <orion:NavigationTabItem Name="<%$ Resources:VIMWebContent,VIMWEBDATA_VB0_122%>" Url="~/Orion/VIM/Admin/VMwareServers.aspx" />
    <orion:NavigationTabItem Name="<%$ Resources:VIMWebContent,VirtualizationPollingSettings_Nutanix%>" Url="~/apps/vim/settings/nutanix" />
    <orion:NavigationTabItem Name="<%$ Resources:VIMWebContent,VIMWEBDATA_VB0_123%>" Url="~/Orion/VIM/Admin/VMwareCredentials.aspx" />
    <orion:NavigationTabItem Name="<%$ Resources:VIMWebContent,VIMWEBDATA_LV0_39%>" Url="/Orion/VIM/Admin/BaselineThresholds.aspx" />
</orion:NavigationTabBar>
