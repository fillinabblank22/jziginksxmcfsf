﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Common;

public partial class Orion_HyperVServers : System.Web.UI.Page, IBypassAccessLimitation
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement)
        {
            Server.Transfer("~/Orion/Error.aspx?Message=" + Server.HtmlEncode(Resources.VIMWebContent.VIMWEBCODE_TM0_9));
        }

        ((Orion_VIM_Admin_VIMAdminPage)this.Master).HelpFragment = "OrionVIMAG_VMwareSettings";
    }

    protected string ReturnUrl
    {
        get { return ReferrerRedirectorBase.GetReturnUrl(); }
    }

    protected string VmanPollingKbLink
    {
        get { return String.Format("http://www.solarwinds.com/documentation/kbloader.aspx?lang={0}&kb=R117", Resources.CoreWebContent.CurrentHelpLanguage); }
    }
}