﻿<%@ Page Title="<%$ Resources: VIMWebContent, VIMWEBDATA_LB0_4%>" Language="C#" MasterPageFile="~/Orion/VIM/Admin/VIMAdminPage.master" AutoEventWireup="true" CodeFile="VirtualizationSettings.aspx.cs" Inherits="Orion_VIM_Admin_VirtualizationSettings" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <style type="text/css">
        body { background-color: #EEEEEE; }
        #adminNav { display: none; }
        #adminContent p { border: none; width: auto; padding: 0; font-size: 12px;}
        #adminContent a { color: #336699; }
        #adminContent a:hover {color:orange;}
        .column1of2 { padding-left:0; padding-right:0; vertical-align:top; text-align: left; width: 50%; }
        .column2of2 { padding-left:0; padding-right:0; vertical-align:top; text-align: left; width: 50%; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <h1 style="padding-bottom:2px;"><%= Page.Title %></h1>
    <div style="font-size: 11px;">
        <span><%= SolarWinds.VIM.Web.Helpers.ModuleInfoHelper.GetModuleVersionString() %></span>
    </div>
    <table id="adminContent" style="padding-top: 0; padding-left: 0;" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <!-- Begin 2nd Column -->
            <td class="column2of2">
                <!-- Begin License Bucket -->        
                <div class="NewContentBucket" runat="server" id="licensingBucket">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="36" height="100%">
                                <img class="BucketIcon" src="/Orion/VIM/images/icon_license_36x36.gif" />
                            </td>
                            <td>
                                <div class="NewBucketHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_4%></div>
                                <p><%= Resources.VIMWebContent.VIMWEBDATA_LC0_5%></p>
                                <% if(!string.IsNullOrEmpty(InfoMessage)) { %><div class="sw-suggestion sw-suggestion-pass"><span class="sw-suggestion-icon"></span><%= InfoMessage %></div><br/><% } %>
                                <% if(!string.IsNullOrEmpty(ErrorMessage)) { %><div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= ErrorMessage %></div><br/><% } %>
                            </td>
                        </tr>
                    </table>
                    <table class="NewBucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="100%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Details/ModulesDetailsHost.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_4%></a></p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End License Bucket -->
            </td>
            <!-- End 2nd Column -->
        </tr>
          <tr>
            <td class="column1of2">
                <!-- Begin Virtualization settings Bucket -->        
                <div class="NewContentBucket">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="36" height="100%">
                                <img class="BucketIcon" src="/Orion/VIM/images/product_specific_settings_icon32x32.png" />
                            </td>
                            <td>
                                <div class="NewBucketHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_4%></div>
                                <p><%= Resources.VIMWebContent.VIMWEBDATA_LC0_6%></p>
                            </td>
                        </tr>
                    </table>
                    <table class="NewBucketLinkContainer">
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/VMwareServers.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_8%></a></p>
                            </td>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/HyperVServers.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_7%></a></p>
                            </td>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/apps/vim/settings/nutanix"><%= Resources.VIMWebContent.VirtualizationSettings_NutanixSettings%></a></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/VMwareCredentials.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_123%></a></p>
                            </td>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/VIMThresholds.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_IY0_1%></a></p>
                            </td>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/BaselineThresholds.aspx"><%= Resources.VIMWebContent.VIMWEBDATA_VB1_2%></a></p>
                            </td>
                        </tr>
                        <tr>
                            <td class="LinkColumn" width="33%">
                                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/VIM/Admin/RetentionSettings.aspx"><%= Resources.VIMWebContent.WEBDATA_LM0_11%></a></p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- End Getting started Bucket -->
            </td>
            <!-- Begin 2nd Column -->
            <td class="column2of2">
            </td>
            <!-- End 2nd Column -->
        </tr>
    </table>
    <br/>
</asp:Content>
