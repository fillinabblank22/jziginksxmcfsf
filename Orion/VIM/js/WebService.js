//
// Note: This is simplified copy of OrionCore.js that is used by AssignVMWareCredentialsDialog.ascx. This resource can't
//       use ExtJS 3.4 and must use ExtJS 4.
//
window.SW = window.SW || {};
window.SW.VIM = window.SW.VIM || {};

(function(VIM) {
  VIM.WebService = {
      prefix: "_not_set_",
  
      Prefs: {
          load: function (prefName, defaultValue) {
              return $("#" + SW.VIM.WebService.prefix + prefName).val() || defaultValue;
          },
          save: function (prefName, value) {
              var name = ORION.prefix + prefName;
              $("#" + name).val(value);
              SW.VIM.WebService.callWebService("/Orion/Services/NodeManagement.asmx",
                                   "SaveUserSetting", { name: name, value: value },
                                   function (result) {
                                       // do nothing.
                                   });
          }
      },
  
      clearError: function () {
          $("#originalQuery,#test,#stackTrace").text('');
      },
  
      handleError: function (xhr, customErrorHandler) {
          if (xhr.status == 401 || xhr.status == 403) {
              // login cookie expired. reload the page so we get bounced to the login page.
              alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
              window.location.reload();
          }
          var errorText = xhr.responseText;
          var msg;
          try {
              var error = eval("(" + errorText + ")");
              msg = error.Message;
              $("#stackTrace").text(error.StackTrace);
          }
          catch (err) {
              msg = err.description;
          }
          $("#test").text(msg);
          if (typeof (customErrorHandler) == 'function') {
              customErrorHandler(msg);
          }
      },
  
      callWebService: function (serviceName, methodName, param, onSuccess, onError) {
         var paramString = JSON.stringify(param);
  
          if (paramString.length === 0) {
              paramString = "{}";
          }
  
          $.ajax({
              type: "POST",
              url: serviceName + "/" + methodName,
              data: paramString,
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: function (msg) {
                  onSuccess(msg.d);
              },
              error: function (xhr, status, errorThrown) {
                  SW.VIM.WebService.handleError(xhr, onError);
              }
          });
      },
  
      objectFromDataTable: function (result) {
          var table = [];
          for (var y = 0; y < result.Rows.length; ++y) {
              var row = result.Rows[y];
              var tableRow = {};
              for (var x = 0; x < result.Columns.length; ++x) {
                  tableRow[result.Columns[x]] = row[x];
              }
              table.push(tableRow);
          }
          return { Rows: table };
      },
  
      postToTarget: function (targetPage, data) {
          var formTag = String.format('<form method="post" action="{0}"></form>', targetPage);
          var form = $(formTag);
  
          for (var prop in data) {
              var inputTag = String.format('<input type="hidden" name="{0}" id="{0}" />', prop);
             var jsonData = JSON.stringify(data[prop]);
              form.append($(inputTag).val(jsonData));
          }
  
          $(form).appendTo('body').submit();
      }
  };
})(SW.VIM);
