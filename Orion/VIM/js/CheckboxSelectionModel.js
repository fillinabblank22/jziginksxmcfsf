﻿/**
* Ext.sw.grid.CheckboxSelectionModel
*/

Ext.ns('Ext.sw.VIM.grid');

/**
* Extension to Checkbox selection model for expandable grids 
* to allow checkboxes only for specific levels of nesting.
* @class Ext.sw.vim.grid.CheckboxSelectionModel
* @extends Ext.grid.RowSelectionModel
* @constructor
* @param {Object} config The configuration options
*/
Ext.sw.VIM.grid.CheckboxSelectionModel = function (config) {
    var maxCheckboxLevel = 99;
    var isDisabledCallback = function (record) { return false; };

    if (config != null) {
        if (config.maxCheckboxLevel != null)
            maxCheckboxLevel = config.maxCheckboxLevel;

        if (config.isDisabledCallback != null)
            isDisabledCallback = config.isDisabledCallback;
    }

    // call parent
    Ext.sw.VIM.grid.CheckboxSelectionModel.superclass.constructor.call(this, config);

    // force renderer to run in this scope
    this.renderer = function (v, p, record) {
        var level = 0;
        if (record.data != null) {
            if (record.data.Level != null)
                level = record.data.Level;
        }

        if (level > maxCheckboxLevel)
            return '';

        if (this.isDisabled(record))
            return '';

        return Ext.sw.VIM.grid.CheckboxSelectionModel.superclass.renderer.apply(this, arguments);
    } .createDelegate(this);

    this.isDisabled = function (record) {
        return isDisabledCallback(record);
    };

    this.canBeSelected = function (record) {
        if (record == null || record.data == null)
            return true;

        if (record.data.Level != null && record.data.Level > maxCheckboxLevel)
            return false;

        if (this.isDisabled(record))
            return false;

        return true;
    };

};                       // end of constructor

Ext.extend(Ext.sw.VIM.grid.CheckboxSelectionModel, Ext.grid.CheckboxSelectionModel, {
    listeners: {
        beforerowselect: function (obj, rowIndex, keepExisting, record) {
            return this.canBeSelected(record);
        }
    }
});