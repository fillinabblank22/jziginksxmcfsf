ManageNodeDialog = function (engineID, ipAddress, entity, multipleAddresses, platform, cancelHandler, isCloudInstance) {

    var dialog;
    var unknownNode = typeof (engineID) == 'undefined';
    var totalDialogHeight = 135;
    var elementIdSuffix = ipAddress;
    var closeTrigger;

    if (typeof (multipleAddresses) == "undefined") {
        multipleAddresses = false;
    }

    function onClose() { if (dialog) { dialog.dialog("close"); ; dialog = null; } };

    function onManage() {

        // In demo mode return alert message
        // if (VIMIsDemoMode()) return VIMDemoModeMessage();      

        if (multipleAddresses) {
            ipAddress = dialog.find(":checked").val();
            ipAddress = decodeURIComponent(ipAddress); // the text is in URI encoded format (we need to decode it)
        }

        if (dialog) { closeTrigger = "btnManage"; dialog.dialog("close"); ; dialog = null; }

        var url;

        if (unknownNode) {
            url = "/Orion/Discovery/Config/";
        } else if (isCloudInstance === true) {
            url = "/Orion/Nodes/Add/Default.aspx";
            if (ipAddress) {
                url += "?IPAddress=" + encodeURIComponent(ipAddress);
            } 
        } else {
            url = "/Orion/Nodes/Add/Default.aspx?EngineID=" + engineID + "&IPAddress=" + encodeURIComponent(ipAddress);
        }

        // For HyperV guests we don't want to add VMwareEntity parameter
        if (entity == undefined || platform == 2)
            window.location = url;
        else
            window.location = url + "&VMwareEntity=" + entity;
    };

    var textHtml =
                '<div style="height:70px; background-color:white; display:block; padding:10px;">' +
                   '<table>' +
                        '<tr>' +
                            '<td style="width:40px;">' +
                                '<img src="/Orion/Images/Info-Notification.gif" alt="" />' +
                            '</td>' +
                            '<td>' +
                                (isCloudInstance ? '@{R=VIM.Strings.CloudMonitoring;K=CLM_ManageAsNode_Popup_Instance_Not_Managed_Text1;E=js}'
                                    : unknownNode ? '@{R=VIM.Strings;K=VIMWEBJS_LC0_1;E=js}' : '@{R=VIM.Strings;K=VIMWEBJS_VB0_1;E=js}') +
                            '</td>' +
                        '</tr>' +
                    '</table> ' +
                    (isCloudInstance ? '<p>' +
                            '@{R=VIM.Strings.CloudMonitoring;K=CLM_ManageAsNode_Popup_Instance_Not_Managed_Text2;E=js}' +
                            (multipleAddresses ? (ipAddress && ipAddress.split(";").length > 1
                                ? '@{R=VIM.Strings.CloudMonitoring;K=CLM_ManageAsNode_Popup_MultipleIP_Text;E=js}'
                                : '@{R=VIM.Strings.CloudMonitoring;K=CLM_ManageAsNode_Popup_SingleIP_Text;E=js}')
                            : '') +
                        '</p>' : '') +
                    '{0}' +
                    (isCloudInstance ? '<table class="sw-suggestion-info" style="margin-top: 20px">' +
                        '<tr>' +
                            '<td style="padding:5px; padding-bottom:85px">' +
                                '<img src="/orion/images/NotificationImages/info_message_icon_16x16.png" alt="" />' +
                            '</td>' +
                            '<td style="padding:5px" class="ManageAsNode-ICMPWarning-HelpLinkPlaceholder">' +
                                '@{R=VIM.Strings.CloudMonitoring;K=CLM_ManageAsNode_Popup_ICMP_Warning_Text;E=js}' +
                            '</td>' +
                        '</tr>' +
                    '</table>' : '') +
                    '{1}' +
                '</div>';
    
    var multipleAddressesHtml =
                '<div style="height:100%; background-color:white; display:block; padding:10px;">' +
                    (isCloudInstance ? '' : '@{R=VIM.Strings;K=VIMWEBJS_VB0_2;E=js}') +
                    '<div style="padding-left: 35px; margin-top: 10px">{0}</div>' +
                '</div>';

    var dialogButtons = '<br /><div style="float: right">';
    if (unknownNode) {
        dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_LC0_2;E=js}", { type: 'primary', id: 'btnManage' });
        dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_LC0_3;E=js}", { type: 'secondary', id: 'btnClose' });
    } else {
        dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VB0_4;E=js}", { type: 'primary', id: 'btnManage' });
        dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VB0_5;E=js}", { type: 'secondary', id: 'btnClose' });
    }
    dialogButtons += '</div>';

    var addressItems = {};
    if (multipleAddresses) {
        all = ipAddress.split(";");
        var ipAddressListHTML = "";
        for (i = 0; i < all.length; i++) {
            if (i == 0) {
                ipAddressListHTML += '<div style="padding-bottom: 5px"><input type="radio" name="ipAddress" id="ipAddress' + i + '" checked value="' + all[i] + '" /><label for="ipAddress' + i + '">' + all[i] + '</label></div>';
            } else {
                ipAddressListHTML += '<div style="padding-bottom: 5px"><input type="radio" name="ipAddress" id="ipAddress' + i + '" value="' + all[i] + '" /><label for="ipAddress' + i + '">' + all[i] + '</label></div>';
            }
        }

        multipleAddressesHtml = String.format(multipleAddressesHtml, ipAddressListHTML);
        textHtml = String.format(textHtml, multipleAddressesHtml, dialogButtons);
    }
    else {
        textHtml = String.format(textHtml, "", dialogButtons);
    }

    var $dialog = $(textHtml);
    $dialog.find("#btnManage").click(function () { onManage(); });
    $dialog.find("#btnClose").click(function () { onClose(); });
    dialog = $dialog.dialog({ width: 390,
        title: "@{R=VIM.Strings;K=VIMWEBJS_VB0_3;E=js}",
        modal: true,
        resizable: false,
        close: function () {
            if (typeof cancelHandler === 'function' && closeTrigger != "btnManage")
                cancelHandler();
        }
    });

    if (isCloudInstance) {
        var helpLinkElement = $(".clm-dialog-link-icmp");
        var alreadyAdded = $dialog.has(".clm-dialog-link-icmp");
        if (alreadyAdded.length == 0) {
            $(".ManageAsNode-ICMPWarning-HelpLinkPlaceholder").append(helpLinkElement[0]);
        }
    }
}

ManageInstanceAsNodeDialog = function (instanceId) {
    GetIpAddressesAndDnsNamesForCloudInstance(instanceId,
        function (ipAddressesAndDnsNames) {
            var publicIpAddresses = [];
            var privateIpAddresses = [];
            var publicDnsName;
            var privateDnsName;

            for (var i = 0; i < ipAddressesAndDnsNames.length; i++) {
                var tableRow = ipAddressesAndDnsNames[i];
                if (i === 0) {
                    publicDnsName = tableRow.PublicDNSName;
                    privateDnsName = tableRow.PrivateDNSName;
                }
                if (tableRow.Type == "1") {
                    publicIpAddresses.push(tableRow.IPAddress);
                } else {
                    privateIpAddresses.push(tableRow.IPAddress);
                }
            }

            var addressesParam = null;
            var hasMultipleIpAddresses = false;

            var ipAddressess = publicIpAddresses.concat(privateIpAddresses);
            if (ipAddressess.length > 0 && ipAddressess[0]) {
                addressesParam = ipAddressess.join(";");
                hasMultipleIpAddresses = true;
            } else if (publicDnsName) {
                addressesParam = publicDnsName;
            } else {
                addressesParam = privateDnsName;
            }

            ManageNodeDialog(null, addressesParam, null, hasMultipleIpAddresses, null, null, true);
        });
}

function GetIpAddressesAndDnsNamesForCloudInstance(instanceId, onSuccess, onFailed) {
    var query = "SELECT VIP.IPAddress, VIP.Type, I.PublicDNSName, I.PrivateDNSName " +
                "FROM Orion.Cloud.Instances AS I " +
                "LEFT JOIN Orion.VIM.VirtualMachineIPAddresses AS VIP " +
                "ON VIP.VirtualMachineID = I.VirtualMachineID " +
                "WHERE I.InstanceId = '" + instanceId + "' " +
                "ORDER BY VIP.Type";

    Information.QueryWithPartialErrors(query, function (result) {
        var ipAddressesAndDnsNames = [];
        for (var y = 0; y < result.Data.Rows.length; ++y) {
            var row = result.Data.Rows[y];
            var tableRow = {};
            for (var x = 0; x < result.Data.Columns.length; ++x) {
                tableRow[result.Data.Columns[x]] = row[x];
            }
            ipAddressesAndDnsNames.push(tableRow);
        }
        onSuccess(ipAddressesAndDnsNames);
    }, function (error) { onFailed(error) });
}
