/// <reference name="MicrosoftAjax.js"/>

Type.registerNamespace("SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl");

SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl = function (element) {
    SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl.initializeBase(this, [element]);
    this.WebServiceProxy = null;
    this.nodes = [];
    this.rootTreeNodeId = "";
    this.dataProviderId = "";
    this.hiddenDisabled = null;
    this.rootParameters = [];
    this.reloading = false;
}

SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl.prototype = {
    get_WebServiceProxy: function () {
        return this.WebServiceProxy;
    },
    set_WebServiceProxy: function (value) {
        this.WebServiceProxy = value;
    },

    get_Disabled: function () {
        return (this.hiddenDisabled.value == "True");
    },
    set_Disabled: function (value) {
        this.hiddenDisabled.value = value ? "True" : "False";
        this.DisableInputElements(jQuery(this.get_element()));
    },

    get_hiddenDisabled: function () {
        return this.hiddenDisabled;
    },
    set_hiddenDisabled: function (value) {
        this.hiddenDisabled = value;
    },

    get_rootParameters: function () {
        return this.rootParameters;
    },
    set_rootParameters: function (value) {
        this.rootParameters = value;
    },

    GetTreeSectionSucceeded: function (result, treeNodeId) {
        for (var i = 0; i < result.Parts.length; i++) {
            var resultPart = result.Parts[i];
            var contentDiv = null;
            if (resultPart.IsFirst) {
                contentDiv = jQuery('#' + resultPart.TreeNodeId + '-content');
                contentDiv.html(resultPart.RenderedHtml);
                contentDiv.slideDown('fast');
            }
            else {
                contentDiv = jQuery('#' + resultPart.TreeNodeId + '-nodeList');
                contentDiv.append(resultPart.RenderedHtml);
            }
            if (contentDiv.length == 0) {
                resultPart.IsLast = false;
                return;
            }
            this.DisableInputElements(contentDiv);
        }
        if (!result.IsLast) {
            var getTreeSectionSucceedDelegate = Function.createDelegate(this, this.GetTreeSectionSucceeded);
            var getTreeSectionFailedDelegate = Function.createDelegate(this, this.GetTreeSectionFailed);

            this.WebServiceProxy.GetTreeSection(
                this.get_id(),
                result.TreeNodeId,
                result.Parameters,
                result.TreeLevel,
                result.LastNodeIndex,
                result.OtherTreeNodesToExpand,
                getTreeSectionSucceedDelegate,
                getTreeSectionFailedDelegate,
                treeNodeId);
        }

    },

    GetTreeSectionFailed: function (error, treeNodeId) {
        if (this.CheckSessionTimeout(error)) {
            return;
        }
        var contentDiv = jQuery('#' + treeNodeId + '-content');
        contentDiv.html(String.format("@{R=VIM.Strings;K=VIMWEBJS_VB0_8;E=js}", error.get_message()));
    },

    DescriptionSucceeded: function (result, treeNodeId) {
        this.UpdateNodeDescription(treeNodeId, result);
    },

    DescriptionFailed: function (error, treeNodeId) {
        if (this.CheckSessionTimeout(error)) {
            return;
        }
        this.UpdateNodeDescription(treeNodeId, error.get_message());
    },

    UpdateNodeDescription: function (treeNodeId, description) {
        var descriptionSpan = jQuery('#' + treeNodeId + '-description');
        jQuery(descriptionSpan).html(description);
    },

    CollapseSucceeded: function (result, treeNodeId) {
    },

    CollapseFailed: function (error, treeNodeId) {
        this.CheckSessionTimeout(error);
    },

    CheckSessionTimeout: function (error) {
        if (this.reloading) {
            return true;
        }
        var errorCode = error.get_statusCode();
        if (errorCode == 401 || errorCode == 403) {
            alert('@{R=VIM.Strings;K=VIMWEBJS_VB0_9;E=js}');
            this.reloading = true;
            window.location.reload();
            return true;
        }
        return false;
    },

    Click: function (controlId, treeNodeId, parameters, treeLevel) {
        var getTreeSectionSucceedDelegate = Function.createDelegate(this, this.GetTreeSectionSucceeded);
        var getTreeSectionFailedDelegate = Function.createDelegate(this, this.GetTreeSectionFailed);
        var descriptionSucceededDelegate = Function.createDelegate(this, this.DescriptionSucceeded);
        var descriptionFailedDelegate = Function.createDelegate(this, this.DescriptionFailed);

        var getTreeSectionDelegate = Function.createDelegate(this, this.WebServiceProxy.GetTreeSection);
        var collapseDelegate = Function.createDelegate(this, this.WebServiceProxy.CollapseTreeSection);
        var collapseSucceededDelegate = Function.createDelegate(this, this.CollapseSucceeded);
        var collapseFailedDelegate = Function.createDelegate(this, this.CollapseFailed);
        var updateDescriptionDelegate = Function.createDelegate(this, this.WebServiceProxy.UpdateTreeNodeDescription);

        var contentDiv = jQuery(treeNodeId);

        this.HandleClick(
            treeNodeId,
            function (treeNodeIdParam) {
                getTreeSectionDelegate(controlId, treeNodeId, parameters, treeLevel, 0, null,
                    getTreeSectionSucceedDelegate,
                    getTreeSectionFailedDelegate,
                    treeNodeIdParam);
            },
            function () {
                collapseDelegate(controlId, treeNodeId, parameters, treeLevel,
                    collapseSucceededDelegate,
                    collapseFailedDelegate);
            },
            function (treeNodeIdParam, isExpanded) {
                updateDescriptionDelegate(controlId, treeNodeId, parameters, treeLevel, isExpanded,
                    descriptionSucceededDelegate,
                    descriptionFailedDelegate,
                    treeNodeIdParam);
            }
        );
        return false;
    },

    HandleClick: function (treeNodeId, getTreeSectionCallback, collapseCallback, updateDescriptionCallback) {
        var toggleImg = jQuery('#' + treeNodeId + '-toggle');
        var contentDiv = jQuery('#' + treeNodeId + '-content');

        if (!contentDiv.is(':hidden')) {
            contentDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
            updateDescriptionCallback(treeNodeId, false);
        } else {
            contentDiv.html("@{R=VIM.Strings;K=VIMWEBJS_VB0_7;E=js}");
            contentDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            getTreeSectionCallback(treeNodeId);
            updateDescriptionCallback(treeNodeId, true);
        }
    },

    GetIsNodeVisible: function (treeNodeId) {
        var nodeDiv = jQuery('#' + treeNodeId + '-node');
        return (nodeDiv != null);
    },

    GetIsNodeExpanded: function (treeNodeId) {
        if (this.GetIsNodeExpanded(treeNodeId)) {
            return false;
        }
        var nodeContentDiv = jQuery('#' + treeNodeId + '-content');
        return (!contentDiv.is(':hidden'));
    },

    DisableInputElements: function (elem) {
        if (this.get_Disabled()) {
            elem.find('input[type!=hidden]').attr('disabled', true);
        }
        else {
            elem.find('input[type!=hidden]').removeAttr('disabled');
        }
    },

    initialize: function () {
        SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl.callBaseMethod(this, 'initialize');
        // Add custom initialization here
        this.set_Disabled(this.get_Disabled());
    },
    dispose: function () {
        //Add custom dispose actions here
        SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl.callBaseMethod(this, 'dispose');
    }
}

SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl.registerClass('SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl', Sys.UI.Control);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();
