﻿/// <reference path="~/Orion/js/jquery.js" />
$(function () {
    var $customizeDiv = $('<div id="customizeDialog" style="display:none; width:600px;"> \
                                    <div id="inside"> <table width="100%"> \
                                    <tr id="customizeRow1" ><td id="customizeCaption" style="width: 70%;"> </td> <td> <select id="groupByProperty" style="width: 160px;"> </td> </tr> \
                                    <tr id="customizeRow2"><td id="customizeCaption2" style="width: 70%;"> </td> <td> <select id="groupByProperty2" style="width: 160px;"> </td> </tr> \
                                    </table> </div> \
                                    <div class="sw-btn-bar-wizard">' +
                                    SW.Core.Widgets.Button('@{R=VIM.Strings;K=CommonButtonType_Submit; E=js}', { type: 'primary', id: 'customizeSubmit' }) + 
                                    SW.Core.Widgets.Button('@{R=VIM.Strings;K=CommonButtonType_Cancel; E=js}', { type: 'secondary', id: 'customizeCancel' }) +
                                    '</div></div>');
    $('body').append($customizeDiv);
    var $cancelButton = $customizeDiv.find("#customizeCancel");
    var $submitButton = $customizeDiv.find("#customizeSubmit");
    var $dropDownProp = $customizeDiv.find("#groupByProperty");
    var $dropDownProp2 = $customizeDiv.find("#groupByProperty2");
    var $caption = $customizeDiv.find("#customizeCaption");
    var $caption2 = $customizeDiv.find("#customizeCaption2");
    var $customizeRow2 = $customizeDiv.find("#customizeRow2");
    var customizeMode = "";
    var customizeMode2 = "";

    showCustomizeDialog = function (objectName) {
        $dropDownProp.children().remove();
        $dropDownProp2.children().remove();
        var objectTitle;
        var dialogHeight;
        switch (objectName) {
            case 'DataCenters':
                customizeMode = objectName;
                customizeMode2 = null;
                $caption[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_34;E=js}</b> <br>";
                objectTitle = "@{R=VIM.Strings;K=VIMWEBJS_VB0_35;E=js}";
                break;
            case 'HostsInCluster':
                customizeMode = objectName;
                customizeMode2 = null;
                $caption[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_36;E=js}</b> <br>";
                objectTitle = "@{R=VIM.Strings;K=VIMWEBJS_VB0_37;E=js}";
                break;
            case 'VirtualMachines':
                customizeMode = objectName;
                customizeMode2 = null;
                $caption[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_38;E=js}</b> <br>";
                objectTitle = "@{R=VIM.Strings;K=VIMWEBJS_VB0_39;E=js}";
                break;
            case 'SummaryChilds':
                customizeMode = 'VCenters';
                customizeMode2 = 'StandaloneHosts';
                $caption[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_40;E=js}</b> <br>";
                $caption2[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_41;E=js}</b> <br>";
                objectTitle = "@{R=VIM.Strings;K=VIMWEBJS_VB0_42;E=js}";
                break;
            case 'DataCenterChilds':
                customizeMode = 'Clusters';
                customizeMode2 = 'HostsInDataCenter';
                $caption[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_43;E=js}</b> <br>";
                $caption2[0].innerHTML = "<b>@{R=VIM.Strings;K=VIMWEBJS_VB0_44;E=js}</b> <br>";
                objectTitle = "@{R=VIM.Strings;K=VIMWEBJS_VB0_45;E=js}";
                break;
            default:
                customizeMode = null;
                customizeMode2 = null;
                break;
        }

        if (customizeMode != null) {
            onGetListOfOptionsSuccess = function (result) {
                for (var i = 0; i < result.d.length; i++) {
                    var option = result.d[i];
                    $dropDownProp.append(String.format('<option value=\'{0}\' {1}>{2}</option>', option.Key, option.IsDefault ? 'selected=\'true\'' : '', option.Value));
                }
            };
            onGetListOfOptionsError = function (error) {
                $dropDownProp.append(String.format("<option value=''>{0}</option>", String.format("@{R=VIM.Strings;K=VIMWEBJS_VB0_46;E=js}", customizeMode)));
            };

            $.ajax({
                type: 'Post',
                url: '/Orion/Vim/Services/CustomizeVimBreadcrumb.asmx/GetListOfOptions',
                data: "{customizeMode:'" + customizeMode + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onGetListOfOptionsSuccess,
                error: onGetListOfOptionsError
            });
        }

        if (customizeMode2 != null) {
            $customizeRow2.show();
            //$dropDownProp2.show();
            dialogHeight = 185;
            $('#inside').height(80);
            onGetListOfOptionsSuccess = function (result) {
                for (var i = 0; i < result.d.length; i++) {
                    var option = result.d[i];
                    $dropDownProp2.append(String.format('<option value=\'{0}\' {1}>{2}</option>', option.Key, option.IsDefault ? 'selected=\'true\'' : '', option.Value));
                }
            };
            onGetListOfOptionsError = function (error) {
                $dropDownProp2.append(String.format("<option value=''>{0}</option>", String.format("@{R=VIM.Strings;K=VIMWEBJS_VB0_46;E=js}", customizeMode2)));
            };

            $.ajax({
                type: 'Post',
                url: '/Orion/Vim/Services/CustomizeVimBreadcrumb.asmx/GetListOfOptions',
                data: "{customizeMode:'" + customizeMode2 + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: onGetListOfOptionsSuccess,
                error: onGetListOfOptionsError
            });
        }
        else {
            dialogHeight = 135;
            $('#inside').height(30);
            $customizeRow2.hide();
            //$dropDownProp2.hide();
        }

        if (customizeMode != null || customizeMode2 != null) {
            var dialog = $customizeDiv.dialog({
                width: 630, height: dialogHeight, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: objectTitle
            });
            dialog.show();
        }

    };

    $cancelButton.click(function () {
        $customizeDiv.dialog("close");
    });

    $submitButton.click(function () {
        $.ajax({
            type: 'Post',
            url: '/Orion/Vim/Services/CustomizeVimBreadcrumb.asmx/SaveBcCustomization',
            data: "{'customizeMode': '" + customizeMode + "', 'customizationValue': '" + $dropDownProp.val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });
        if (customizeMode2 != null) {
            $.ajax({
                type: 'Post',
                url: '/Orion/Vim/Services/CustomizeVimBreadcrumb.asmx/SaveBcCustomization',
                data: "{'customizeMode': '" + customizeMode2 + "', 'customizationValue': '" + $dropDownProp2.val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
        }
        $customizeDiv.dialog("close");
        location.reload();
    });

    $("#customizeSummaryChildList").click(function () {
        showCustomizeDialog("SummaryChilds");
        setStyles();
        return false;
    });

    $("#customizeVCenterChildList").click(function () {
        showCustomizeDialog("DataCenters");
        setStyles();
        return false;
    });

    $("#customizeStandaloneHostChildList").click(function () {
        showCustomizeDialog("VirtualMachines");
        setStyles();
        return false;
    });

    $("#customizeDataCenterChildList").click(function () {
        showCustomizeDialog("DataCenterChilds");
        setStyles();
        return false;
    });

    $("#customizeClusterChildList").click(function () {
        showCustomizeDialog("HostsInCluster");
        setStyles();
        return false;
    });

    $("#customizeHostInDataCenterChildList").click(function () {
        showCustomizeDialog("VirtualMachines");
        setStyles();
        return false;
    });

    $("#customizeHostInClusterChildList").click(function () {
        showCustomizeDialog("VirtualMachines");
        setStyles();
        return false;
    });

    setStyles = function () {
        $("li.bc_item").removeClass("bc_itemselected");
        $("a").removeClass("bc_refselected");
        if ($.browser.msie && ($.browser.version == "6.0"))
            $iframe.css('display', 'none');
        $("ul.bc_submenu").hide();
        $("ul.bc_submenu").css({ overflow: 'auto' }).css("height", "auto");
        $("img.bc_itemimage").attr({ src: "/Orion/images/breadcrumb_arrow.gif" });

        isClicked = false;
    };

});

