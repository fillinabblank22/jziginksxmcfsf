﻿// The method for fixing the modal window bug in ExtJS 4.0.7
if (typeof(Ext4) != "undefined") {
    Ext4.override(Ext4.ZIndexManager, {
        _showModalMask: function (comp) {
            var zIndex = comp.el.getStyle('zIndex') - 4,
                        maskTarget = comp.floatParent ? comp.floatParent.getTargetEl() : Ext4.get(comp.getEl().dom.parentNode),
                        parentBox = maskTarget.getBox();

            if (Ext4.isSandboxed) {
                if (comp.isXType('loadmask')) {
                    zIndex = zIndex + 3;
                }

                if (maskTarget.hasCls(Ext4.baseCSSPrefix + 'reset') && maskTarget.is('div')) {
                    maskTargetParentTemp = Ext4.get(maskTarget.dom.parentNode)
                    if (maskTargetParentTemp.is('body')) {
                        maskTarget = maskTargetParentTemp;
                        parentBox = maskTarget.getBox();
                    }
                }
            }

            if (!this.mask) {
                this.mask = Ext4.getBody().createChild({
                    cls: Ext4.baseCSSPrefix + 'mask'
                });
                this.mask.setVisibilityMode(Ext4.Element.DISPLAY);
                this.mask.on('click', this._onMaskClick, this);
            }

            if (maskTarget.dom === document.body) {
                parentBox.height = document.documentElement.scrollHeight;
            }
            maskTarget.addCls(Ext4.baseCSSPrefix + 'body-masked');
            this.mask.setBox(parentBox);
            this.mask.setStyle('zIndex', zIndex);
            this.mask.show();
        }
    });
}