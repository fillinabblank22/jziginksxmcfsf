﻿SW.VIM = SW.VIM || {};
SW.VIM.Utils = SW.VIM.Utils || {};

(function (vimUtils) {

    vimUtils.GetViewportHeight = function () {
        var viewportheight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

        if (typeof window.innerWidth != 'undefined') {
            viewportheight = window.innerHeight;
        }

        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

        else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) {
            viewportheight = document.documentElement.clientHeight;
        }

        // older versions of IE

        else {
            viewportheight = document.getElementsByTagName('body')[0].clientHeight;
        }

        return viewportheight;
    };

    vimUtils.GetManagementGridHeight = function () {
        var height = vimUtils.GetViewportHeight() - 410;
        if (height >= 200)
            return height;
        else
            return 200;
    };

})(SW.VIM.Utils);
