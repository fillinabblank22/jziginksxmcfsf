﻿/// <reference path="../ClusterDetails.aspx" />
Ext.namespace('SW.VIM');

SW.VIM.SyncExceptions = function () {
    var grid;
    var dialog;

    function RenderSyncExceptionType(value, metaData, record) {
        switch (value) {
            case 1:
                return "<img src='/Orion/VIM/images/CredentialConflict_icon16x16.png' />";
            case 2:
                return "<img src='/Orion/VIM/images/BadCollectionTarget_icon16x16.png' />";
            case 3:
                return "<img src='/Orion/VIM/images/PartialEnvironment_icon16x16.png' />";
            case 4:
                return "<img src='/Orion/VIM/images/LicenseCountExceeded_icon16x16.png' />";
            default:
                return "<img src='/Orion/VIM/images/placeholder.png' />";
        }
    }

    function RenderSyncExceptionTypeName(value, metaData, record) {
        return String.format("<a href='#' onclick=\"SW.VIM.SyncExceptions.OpenDialog('{1}'); return false;\">{0}</a>", value, record.data.Id);
    }

    function RenderDescription(value, metaData, record) {
        return "<span style='white-space:normal'>" + value + "</span>";
    }

    function LoadOrionSyncExceptions() {
        grid.store.removeAll();

        ORION.callWebService("/Orion/VIM/Services/VManSynchronizationWizard.asmx", "LoadOrionSyncExceptions", {}, function (result) {
            var blankRecord = Ext.data.Record.create(grid.store.fields);
            for (var i = 0; i < result.length; i++) {
                var item = result[i];

                var rec = new blankRecord();
                rec.set("Id", item.Id);
                rec.set("SyncExceptionType", item.SyncExceptionType);
                rec.set("SyncExceptionName", item.SyncExceptionName);
                rec.set("IsResolved", item.IsResolved);
                rec.set("Node", item.Node);
                rec.set("Platform", item.Platform);
                rec.set("Description", item.Description);

                grid.store.add(rec);
            }
        });
    };

    var record = Ext.data.Record.create([
        { name: 'Id' },
        { name: 'SyncExceptionType' },
        { name: 'SyncExceptionName' },
        { name: 'Node' },
        { name: 'Platform' },
        { name: 'Description' },
        { name: 'IsResolved' }
    ]);

    var arrayReader = new Ext.data.ArrayReader(
        { idIndex: 0 },
        record
    );

    var dataStore = new Ext.data.Store({
        reader: arrayReader,
        sortInfo: { field: 'Type', direction: 'ASC' }
    });

    var colModel = new Ext.grid.ColumnModel({
        columns: [
            { dataIndex: 'SyncExceptionType', renderer: RenderSyncExceptionType, width: 30, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_43;E=js}', dataIndex: 'SyncExceptionName', renderer: RenderSyncExceptionTypeName, width: 180, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_44;E=js}', dataIndex: 'Node', width: 140, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_45;E=js}', dataIndex: 'Platform', width: 140, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_46;E=js}', id: 'Details', dataIndex: 'Description', renderer: RenderDescription }
        ],

        defaults: {
            sortable: true,
            menuDisabled: true,
            hideable: false
        }
    });

    function InitGrid() {
        grid = new Ext.grid.GridPanel({
            store: dataStore,
            colModel: colModel,
            viewConfig: {
                forceFit: true,
                getRowClass: function (record, rowIndex, rp, ds) {
                    return (record.data.IsResolved) ? "x-item-disabled" : "";
                }
            },
            stripeRows: true,
            enableColumnMove: false,
            autoExpandColumn: 'Details',
            autoExpandMax: 2000
        });
    }

    function InitLayout() {
        var container = new Ext.Container({
            renderTo: 'syncExceptionPanel',
            layout: 'fit',
            height: SW.VIM.Utils.GetManagementGridHeight(),
            items: [ grid ]
        });

        Ext.EventManager.onWindowResize(function () {
            grid.setWidth(1);
            grid.setWidth($('#syncExceptionPanel').width());

            container.setHeight(SW.VIM.Utils.GetManagementGridHeight());
            container.doLayout();
        }, container);
    }

    function LoadDialogContent(syncExceptionId) {
        $.ajax({
            type: 'POST',
            url: "/Orion/VIM/Admin/Synchronization/SyncExceptionDialogContentLoader.aspx",
            data: { syncExceptionId: syncExceptionId },
            success: function (result) {
                if (result) {
                    $("#dlgResolveBody").html(result);
                    OpenDialog();
                    OnDialogOpened();
                }
            },
        });
    }

    function OpenDialog() {
        if (!dialog) {
            dialog = new Ext.Window({
                applyTo: 'dlgResolve',
                layout: 'fit',
                width: 600,
                height: 'auto',
                modal: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                bodyBorder: false,
                contentEl: 'dlgResolveBody',
                buttons: [
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_47;E=js}',
                        id: 'dlgOk',
                        handler: function() {
                            if (typeof SyncExceptionDialogCallback === "function") {
                                SyncExceptionDialogCallback(); //It is implemented in dialog content site.
                            }
                        },
                        style: "text-align: center"
                    },
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_48;E=js}',
                        id: 'dlgCancel',
                        handler: function() {
                            dialog.hide();
                        }
                    }
                ]
            });
        }

        dialog.setTitle($("#dlgResolveBody .x-window-header").text());
        dialog.alignTo(document.body, "c-c");
        dialog.show();
    }

    function OnDialogOpened() {
        if ($("#dlgResolveBody #syncExceptionId").data("sync-exception-type") == "license-exceeded") {
            Ext.getCmp("dlgCancel").hide();
        } else {
            Ext.getCmp("dlgCancel").show();
        }
    }

    return {
        Init: function () {
            InitGrid();
            InitLayout();

            LoadOrionSyncExceptions();
        },

        OpenDialog: function (syncExceptionId) {
            LoadDialogContent(syncExceptionId);
        },
        
        CloseDialog: function(withoutReload) {
            if (!withoutReload) {
                LoadOrionSyncExceptions();
            }
            dialog.hide();
        },

        IsResolvedAll: function () {
            for (var i = 0; i < dataStore.data.items.length; i++) {
                if (!dataStore.data.items[i].data.IsResolved) {
                    return false;
                }
            }
            return true;
        }
    };
}();

Ext.onReady(function () {
    SW.VIM.SyncExceptions.Init();
});
