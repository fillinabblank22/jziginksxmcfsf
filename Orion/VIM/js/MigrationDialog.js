﻿SW = SW || {};
SW.VIM = SW.VIM || {};



SW.VIM.MigrationDialog = function (actionType, actionName, titleName, vmName, vmId, hostId, platform, cancelHandler) {

    var currentHost;
    var hosts;
    
    var pageSize = 12;

    titleName = SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_VZ0_15;E=js} ', vmName);

    function successCallback(result) {
        hosts = result;
        if (!hosts || hosts.length == 0) {
            var dialogcontent = $("#sw-vim-dialogcontent");
            dialogcontent.empty();
            dialogcontent[0].innerHTML = "@{R=VIM.Strings;K=VIMWEBJS_ZS0_15;E=js}";
            $('#sw-vim-btnYes').hide();
            $('#sw-vim-btnNo .sw-btn-t').text("@{R=VIM.Strings;K=VIMWEBJS_LV0_9;E=js}");
        } else {
            showTable(0);
        } 
    }
    
    function showTable(newPage) {
        if (newPage < 0 || newPage >= Math.ceil(hosts.length / pageSize))
            return;
        var curPage = newPage;
        var dialogcontent = $("#sw-vim-dialogcontent");
        dialogcontent.empty();

        var content = SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_VZ0_13;E=js}', vmName);
        content += SW.Core.String.Format('<p class="sw-vim-currentHostLabel">{0}</p>', SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_LV0_10;E=js}', generateHostLabel(currentHost)));
        content += '<table border="0" cellpadding="2" cellspacing="0" width="100%">';
        var end = (curPage + 1) * pageSize < hosts.length ? (curPage + 1) * pageSize : hosts.length;
        for (var i = curPage * pageSize; i < end; i++) {
            var checkedAttr = i == 0 ? 'checked="checked"' : '';
            content += SW.Core.String.Format('<tr><td class="Property vimTopXXProperty"><input type="radio" name="migrationitem" value="{0}" {1}/>{2}</td></tr>', i, checkedAttr, generateHostLabel(hosts[i]));
        }
        if (pageSize < hosts.length) {
            content += '<tr><td>';
            if (curPage > 0) {
                content += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-first.gif" onclick="SW.VIM.MigrationDialog.showTable(0)">';
                content += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-prev.gif" onclick="SW.VIM.MigrationDialog.showTable(' + (curPage - 1) + ')">';
            }
            
            content += SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_SH0_1;E=js}',
                '<input style="text-align:right; width:40px;" id="sw-vim-relocationPage" type="number" min="1" max="' + Math.ceil(hosts.length / pageSize) + '" size="1" value="' + (curPage + 1) + '" onchange="SW.VIM.MigrationDialog.showTable(($(\'#sw-vim-relocationPage\').val()-1))" >',
                Math.ceil(hosts.length / pageSize)
            );
            
            if (curPage < Math.ceil(hosts.length / pageSize) - 1) {
                content += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-next.gif" onclick="SW.VIM.MigrationDialog.showTable(' + (curPage + 1) + ')">';
                content += '<input type="image" name="submit" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-last.gif" onclick="SW.VIM.MigrationDialog.showTable(' + (Math.ceil(hosts.length / pageSize) - 1) + '); ">';
            }

            content += '</td></tr>';
        }
        content += '</table>';
        dialogcontent[0].innerHTML = content;
    }

    function generateHostLabel(host) {
        var hostImage = SW.Core.String.Format('<span class="entityIconBox"><img src="/Orion/StatusIcon.ashx?entity=Orion.VIM.Hosts.{0}&status={1}" /></span>', platform, host.Status);
        var hostLink = Label = SW.Core.String.Format('<a href="{0}">{1}</a>', host.DetailsUrl, host.Name);
        return hostLabel = SW.Core.String.Format("{0} {1}", hostImage, hostLink);
    }

    SW.VIM.MigrationDialog.showTable = function (newPage) {
        showTable(newPage);
    };


    function failCallback(message) {
        $("#sw-vim-dialogcontent").empty();
        $('#sw-vim-dialogcontent').append(message);
    }

    function currentHostLoaded(result) {
        currentHost = result;
        SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetMigrateDestinations", { vmId: vmId, platform: platform },
            successCallback, failCallback);
    }

    SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetHost", { hostId: hostId }, currentHostLoaded, failCallback);

    var dialog;

    function onClose() {
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
    }

    function onMigrate() {
        var destHost = $('input[name=migrationitem]:checked').val();
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
        SW.VIM.ManagementActionDialog(actionType, actionName, titleName, vmId, "", 2, false, VIMIsDemoMode() ? null : [hosts[destHost].ManagedObjectID, hosts[destHost].ID, "", hosts[destHost].Name], vmName);
    }    

    var textHtml =
        '<div>' +
            '<div  id = "sw-vim-dialogcontent" style="min-height: 50px;">' +
            '<img src="/Orion/images/AJAX-Loader.gif" />@{R=VIM.Strings;K=VIMWEBJS_VZ0_2;E=js}<br />' +
            '</div>' +
            '{0}' +
        '</div>';    

    var dialogButtons = '<div style="float: right; font-size: 11px; padding-top: 12px">';
    dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_17;E=js}", { type: 'primary', id: 'sw-vim-btnYes' });
    dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_7;E=js}", { type: 'secondary', id: 'sw-vim-btnNo' });
    dialogButtons += '</div>';

    textHtml = String.format(textHtml, dialogButtons);


    var $dialog = $(textHtml);
    $dialog.find("#sw-vim-btnYes").click(onMigrate);
    $dialog.find("#sw-vim-btnNo").click(onClose);
    dialog = $dialog.dialog({
        width: 'auto',
        title: titleName,
        modal: true,
        resizable: false,
        open: function () {
            $('.ui-dialog-content').addClass('sw-vim-vmmanag-ui-dialog-content');
        },
        close: function() {
            if (typeof cancelHandler === 'function' && closeTrigger != "sw-vim-btnYes") {
                cancelHandler();
            }
            $(this).dialog('destroy').remove();
        }
    });
}
