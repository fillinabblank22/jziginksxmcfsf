﻿SW = SW || {};
SW.VMan = SW.VMan || {};
SW.VMan.Charts = SW.VMan.Charts || {};
SW.VMan.Charts.StorageChartLegend = SW.VMan.Charts.StorageChartLegend || {};
(function (legend) {

    legend.toggleSeries = function(series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function(series, container) {
        
        var check = $('<input type="checkbox"/>')
                    .click(function() { legend.toggleSeries(series); });
        
        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.addLegendSymbol = function(series, container, colorOverride) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = colorOverride || series.color;

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                    stroke: symbolColor,
                    fill: symbolColor
                })
                .add();

        label.appendTo(container);
    };

    legend.createStandardLegend = function (chart, legendContainerId) {
        var table = $('#' + legendContainerId);
        var headerRow = $('<tr><td class="ReportHeader">&nbsp;</td><td class="ReportHeader">&nbsp;</td></tr>');
        var numSeries = chart.series.length;

        table.empty();
        headerRow.appendTo(table);

        var columns = chart.options.chartColumns.length;
        for (var j = 0; j < columns; j++) {
            $('<td class="ReportHeader vimReportHeader">' + chart.options.chartColumns[j] + '</td>').appendTo(headerRow);
        }

        for (var i = 0; i < numSeries; i++) {
            if (chart.series[i].options.id == 'highcharts-navigator-series')
                continue;

            var row = $('<tr />').appendTo(table);
            var td = $('<td class="Property vimStorageChartProperty vimLegendColorIconCell" />').appendTo(row);
            legend.addLegendSymbol(chart.series[i], td);
            td = $('<td class="Property vimStorageChartProperty vimLegendColorIconCell" />').appendTo(row);
            legend.addCheckbox(chart.series[i], td);

            var columnsNum = chart.options.seriesInfo[i].columns.length;
            for (var k = 0; k < columnsNum; k++) {
                var value = chart.options.seriesInfo[i].columns[k];
                td = $('<td class="Property vimStorageChartProperty" />').appendTo(row);
                td.html(value);
            }
        }

        // the table is by default hidden, because if chart has no data we would show the header even thought it would be incomplete
        table.show();
    };

} (SW.VMan.Charts.StorageChartLegend));