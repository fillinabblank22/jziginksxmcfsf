﻿SW = SW || {};
SW.VMan = SW.VMan || {};
SW.VMan.Alert = SW.VMan.Alert || {};

(function(al) {
    var alertWrapper = '<div class="sw-vman-alert"><table><tr><td style="width:40px;"><img src="{0}" alt="" /></td><td>{1}</td></tr></table>{2}</div>';

    // default options for the custom dialog
    var defaults = {
        title: '',
        content: '',
        buttons: [],
        icon: '/Orion/Images/info_32x32.gif',
        width: 390,
        resizable: false
    };

    function onClose(dialog) {
        if (dialog) {
            dialog.dialog("close");        
        }
    }    

    function showCustom(options) {
        var settings = $.extend({}, defaults, options);

        var buttons = '';
        var callbacks = [];

        // configure all used buttons
        if ($.isArray(settings.buttons)) {
            for (var i = 0; i < settings.buttons.length; i++) {
                buttons += '<div style="float: right; padding-top: 10px; padding-left: 5px;">';
                buttons += SW.Core.Widgets.Button(settings.buttons[i].text, { type: settings.buttons[i].type, id: settings.buttons[i].id });
                buttons += '</div>';

                callbacks.push({ id: settings.buttons[i].id, callback: settings.buttons[i].callback });
            }
        }

        var content = $(String.format(alertWrapper, settings.icon, settings.content, buttons));

        var dialog = content.dialog({
            width: settings.width,
            title: settings.title,
            resizable: settings.resizable
        });

        // hook callbacks to buttons
        for (var j = 0; j < callbacks.length; j++) {
            var callback = callbacks[j].callback;
            content.find("#" + callbacks[j].id).click(function() { callback(dialog); });
        }
    }

    al.ShowInfo = function(titleText, contentText, buttonText, buttonCallback) {
        var options = {
            title: titleText,
            content: contentText,
            buttons: [
                {
                    id: 'sw-vman-alert-ok',
                    text: buttonText || "@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}",
                    type: 'primary',
                    callback: buttonCallback || onClose
                }
            ]
        };

        showCustom(options);
    };

    al.ShowQuestion = function(titleText, contentText, buttonText, buttonCallback) {
        var options = {
            icon: '', // todo: needs icon
            title: titleText,
            content: contentText,
            buttons: [
                {
                    id: 'sw-vman-alert-ok',
                    text: buttonText || "@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}",
                    type: 'primary',
                    callback: buttonCallback || onClose
                }
            ]
        };

        showCustom(options);
    };

    al.ShowWarning = function(titleText, contentText, buttonText, buttonCallback) {
        var options = {
            icon: '/Orion/Images/stop_32x32.gif',
            title: titleText,
            content: contentText,
            buttons: [
                {
                    id: 'sw-vman-alert-ok',
                    text: buttonText || "@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}",
                    type: 'primary',
                    callback: buttonCallback || onClose
                }
            ]
        };

        showCustom(options);
    };

    al.ShowCustom = function(options) {
        showCustom(options);
    };

})(SW.VMan.Alert);

