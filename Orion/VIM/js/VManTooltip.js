﻿SW.VMan = SW.VMan || {};
SW.VMan.Tooltip = SW.VMan.Tooltip || {};

(function(tl) {
    tl.Init = function(selector, options) {

        $.vmanTooltip = function (control, settings) {

            var $control = $(control);

            var tooltipHtml = '';

            tooltipHtml += '<div class="' + settings.tooltipCls + '" style="display: none;">';
            tooltipHtml += '  <div class="' + settings.hoverArrowCls + '"></div>';
            tooltipHtml += '  <div class="' + settings.hoverBodyCls + ' ' + settings.hoverBodyParentCls + '">';
            tooltipHtml += '    <span class="' + settings.iconCls + '"></span>';

            if (settings.title.length != 0) {
                tooltipHtml += '<div class="'+ settings.hoverBodyCls +'">' + settings.title + '</div>';
            }

            var text = "";
            if (settings.findHtml) {
                text = $control.closest(settings.findHtml).attr('title');
            } else {
                text = settings.html;
            }

            if (!text)
                return;

            tooltipHtml += text;
            tooltipHtml += '  </div>';
            tooltipHtml += '</div>';



            var tooltip = $(tooltipHtml);

            control.onmousemove = function(e) {
                if (!e)
                    e = window.event;

                if (!$control.vmanTooltip) {
                    $("body").append(tooltip);
                    $control.vmanTooltip = tooltip;
                    $control.removeAttr("title");
                    if (settings.findHtml)
                        $control.closest(settings.findHtml).removeAttr('title');
                }

                var tooltipHeight = tooltip.height();

                $control.vmanTooltip.css('left', e.clientX + 3);
                $control.vmanTooltip.css('top', e.clientY - tooltipHeight - 3);
                $control.vmanTooltip.children('.' + settings.hoverArrowCls).css('top', tooltipHeight - 17);

                $control.vmanTooltip.fadeIn("fast");
            };

            control.onmouseout = function (e) {
                e = e || window.event;
                var el = e.toElement || e.relatedTarget; // we need to check that event is not from child element
                if (!isChild(control, el) && el != control)
                    tooltip.fadeOut("fast");
            };

            // IE 7 HACK 
            // Unregistered events cause memory leaks.  
            if ($.browser.msie && parseInt($.browser.version, 10) == 7) {
                $(window).unload(function() {
                    control.onmousemove = null;
                    control.onmouseout = null;
                });
            }
        };

        function isChild(parent, child) {
            if (child != null) {
                while (child.parentNode) {
                    if ((child = child.parentNode) == parent) {
                        return true;
                    }
                }
            }
            return false;
        }

        // do not show tooltips if the link has a class "NoTip"
        $(selector + "[tooltip!='processed']:not(.NoTip)").livequery(function() {
            this.tooltip = 'processed';

            var defaults = {
                html: $(this).attr('title'),
                findHtml: false,                            //use this field to get 'title' property from another control instead of html
                title: '', 
                hoverArrowCls: 'sw-vman-hover-help-arrow',
                hoverBodyCls: 'sw-vman-hover-help-body',
                hoverBodyParentCls: 'sw-suggestion',
                tooltipCls: 'sw-vman-hover-help-tooltip',
                iconCls: 'sw-suggestion-icon',
                titleCls: 'sw-suggestion-title'
            };

            var settings = $.extend({}, defaults, options);
            $.vmanTooltip(this, settings);
        });
    };
})(SW.VMan.Tooltip);
