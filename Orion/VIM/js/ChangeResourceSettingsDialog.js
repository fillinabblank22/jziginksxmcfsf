﻿SW = SW || {};
SW.VIM = SW.VIM || {};


SW.VIM.ChangeSettingsDialog = function (actionType, actionName, titleName, vmId, platform, cancelHandler, restartNedeed, vmName) {

    var dialog;
    var cpuLimit=4;
    var memoryMaximum=15000;
    var memoryMinimum = platform == "HyperV" ? 32 : 4;
    var currentCpuCount;
    var currentMemoryAmount;

    function validateInput() {
        var memory = ($("#dpMemoryAmount").val() == "Custom") ? $("#customMemoryInput").val() : $("#dpMemoryAmount").val();
        if (!$("#nmbrofproc").val() || !memory || memory == "None" || !(/^\d+$/.test($("#nmbrofproc").val())) || !(/^\d+$/.test(memory)))
        {
            showErrorMessage("@{R=VIM.Strings;K=VIMWEBJS_ZS0_8;E=js}");
            return false;
        }
        if ($("#nmbrofproc").val() < 1 || $("#nmbrofproc").val() > cpuLimit || $("#nmbrofproc").val() % 1 > 0)
        {
            showErrorMessage(String.format("@{R=VIM.Strings;K=VIMWEBJS_ZS0_9;E=js}", cpuLimit));
            return false;
        }

        memory = parseInt(memory);
        if (memory < memoryMinimum || memory > memoryMaximum || memory % 2 != 0)
        {
            showErrorMessage(String.format("@{R=VIM.Strings;K=VIMWEBJS_ZS0_10;E=js}", memoryMinimum, memoryMaximum));
            return false;
        }
        return true;
    }

    function showErrorMessage(errorText) {
        var errorBox = $('.resource-settings-error');
        errorBox.html(errorText);
        errorBox.show();
        dialog.animate({ height: height }, 100);
    }

    function onClose() {
         if (dialog) {
             dialog.dialog("close");
             dialog = null;
         }
    };

    function onManage() {

        if (validateInput()) {
            var parameters = [];
            var cpuCount = $("#nmbrofproc").val();
            if (cpuCount != currentCpuCount) {
                parameters.push(["Processor", cpuCount]);
            }
            var memory = ($("#dpMemoryAmount").val() == "Custom") ? $("#customMemoryInput").val() : $("#dpMemoryAmount").val();
            if (memory != currentMemoryAmount) {
                parameters.push(["Memory", memory]);
            }
            if (parameters.length == 0) {
                showErrorMessage("@{R=VIM.Strings;K=VIMWEBJS_SH0_2;E=js}");
                return;
            }
            var stopStart = $('#powerControlCheckbox').prop('checked') && (restartNedeed.toLowerCase() == "true");
            if (dialog) {
                dialog.dialog("close");
                dialog = null;
            }            
            SW.VIM.ManagementActionDialog(actionType, actionName, titleName, vmId, 2, '', stopStart, parameters, vmName);
        }               
    };

    function onMemoryChange() {
        if ($("#dpMemoryAmount").val() == "Custom") {
            $("#customMemoryInput").show();
            $("#customMemoryText").show();
        } else {
            $("#customMemoryInput").hide();
            $("#customMemoryText").hide();
        }
    };

    var memoryOptions = [
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_1;E=js}", value: "None" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_2;E=js}", value: "1024" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_3;E=js}", value: "2048" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_4;E=js}", value: "4096" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_5;E=js}", value: "8192" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_6;E=js}", value: "16384" },
        { display: "@{R=VIM.Strings;K=VIMWEBJS_JS0_7;E=js}", value: "Custom" }
    ];

    var textHtml =
            '<div>' +
                '<div id = "sw-vim-dialogcontent" style="background-color:white; display:block;">' +
                    '@{R=VIM.Strings;K=VIMWEBJS_ZS0_4;E=js}<br /><br />' +
                    '<table class="sw-vim-vmmanag-ui-dialog-table">' +
                        '<tr>' +
                            '<td style="width:150px;">' +
                                '@{R=VIM.Strings;K=VIMWEBJS_ZS0_5;E=js}' +
                            '</td>' +
                            '<td>' +
                                '<input id="nmbrofproc" name="nmbrofproc" style="width:60px; text-align:right;">' +
                            '</td>' +
                        '</tr><tr><td colspan=2; class="cpu-count-recommendation" style="display: none; color:#247390;"></td></tr>' +
                        '<tr>' +
                            '<td>' +
                                '@{R=VIM.Strings;K=VIMWEBJS_ZS0_6;E=js}' +
                            '</td>' +
                            '<td>' +
                                '<select id="dpMemoryAmount"></select>' +
                            '</td>' +
                            '<td>' +
                                '<input id="customMemoryInput" maxlength="400" type="text" style="display: none; text-align: right;">' +
                            '</td>' +
                            '<td>' +
                                '<span id="customMemoryText" style="display: none;">MB</span>' +
                            '</td>' +
                        '</tr><tr><td colspan=2; class="memory-recommendation" style="display: none; color:#247390;"></td></tr>' +
                    '</table>' +
                    '<div class="resource-settings-error sw-vim-vmmanag-ui-dialog-error-panel" style="display: none;"></div>' +
                    '<div id="powerControlContainer" style="display: none;"><input id="powerControlCheckbox" type="checkbox" checked><label>@{R=VIM.Strings;K=VIMWEBJS_ZS0_13;E=js}</label><div class="sw-text-helpful" style="margin-left: 17px">@{R=VIM.Strings;K=VIMWEBJS_ZS0_14;E=js}</div></div>' +
                    '{0}' +
                '</div>' +
            '</div>';
    
    var dialogButtons = '<br /><div style="float: right">';
    dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_ZS0_7;E=js}", { type: 'primary', id: 'btnSave' });
    dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=CommonButtonType_Cancel;E=js}", { type: 'secondary', id: 'btnCancel' });
    dialogButtons += '</div>';
    
    textHtml = String.format(textHtml, dialogButtons);

    var $dialog = $(textHtml);
    $dialog.find("#dpMemoryAmount").html("");
    $(memoryOptions).each(function (i) {
        $dialog.find("#dpMemoryAmount").append("<option value=\"" + memoryOptions[i].value + "\">" + memoryOptions[i].display + "</option>");
    });
    $dialog.find("#dpMemoryAmount").change(onMemoryChange);
    $dialog.find("#btnSave").click(onManage);
    $dialog.find("#btnCancel").click(onClose);
    dialog = $dialog.dialog({
        width: 'auto',
        title: titleName,
        modal: true,
        resizable: false,
        open: function () {
            $('.ui-dialog-content').addClass('sw-vim-vmmanag-ui-dialog-content');
        },
        close: function () {
            if (typeof cancelHandler === 'function' && closeTrigger != "btnSave") {
                cancelHandler();
            }
            $(this).dialog('destroy').remove();

        }
    });

    if (restartNedeed.toLowerCase() == "true") {
        var powerControlBox = $('#powerControlContainer');
        powerControlBox.show();
    }    
    
    SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetLimitCurrentAndRecommendedCPUandRAMsize", { virtualMachineId: vmId },
                  function (result) {
                      if (result.Succeeded == true) {
                          $("#nmbrofproc")[0].value = result.CPU;
                          var validValue = false;
                          $(memoryOptions).each(function() {
                              if (this.value == result.RAM) validValue = true;
                          });
                          if (validValue) {
                              $("#dpMemoryAmount").val(result.RAM);
                          } else {
                              $("#dpMemoryAmount").val("Custom");
                              onMemoryChange();
                              $("#customMemoryInput").val(result.RAM);
                          }
                          currentCpuCount = result.CPU;
                          currentMemoryAmount = result.RAM;
                          if (result.CpuRecommendation) {
                              var cpuRecommendation = $('.cpu-count-recommendation');
                              cpuRecommendation.html(result.CpuRecommendation);
                              cpuRecommendation.show();
                          }
                          if (result.MemoryRecommendation) {
                              var memoryRecommendation = $('.memory-recommendation');
                              memoryRecommendation.html(result.MemoryRecommendation);
                              memoryRecommendation.show();
                          }
                          if (result.CPULimit) {
                              cpuLimit=result.CPULimit;
                          }
                          if (result.MinimumMemory) {
                              memoryMinimum = result.MinimumMemory;
                          }
                          if (result.MaximumMemory) {
                              memoryMaximum = result.MaximumMemory;
                          }
                      }
                  });
    

}
