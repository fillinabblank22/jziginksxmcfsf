﻿SW = SW || {};
SW.VIM = SW.VIM || {};



SW.VIM.DeleteSnapshotDialog = function (actionType, actionName, titleName, vmName, vmId, cancelHandler) {
    
    function prepareSnapshot(snapshot) {
        var name = snapshot.IsCurrentState ? "@{R=VIM.Strings;K=VIMWEBJS_VZ0_4;E=js}" : htmlEncode(snapshot.Name);
        var res = { "data": name };
        res.children = [];
        res.attr = {};
        res.attr.id = snapshot.IsCurrentState ? "current_tree_item" : "snapshot_tree_item";
        res.attr.rel = snapshot.IsCurrentState ? "current_tree_item" : "snapshot_tree_item";
        res.attr.path = encodeURIComponent(snapshot.Path);
        if (!snapshot.IsCurrentState) {
            res.attr.title = '@{R=VIM.Strings;K=VIMWEBJS_VZ0_5;E=js} ' + new Date(parseInt(snapshot.CreationTime.substr(6))).toLocaleString();
        }
        if (snapshot.Children) {
            for (var i = 0; i < snapshot.Children.length; i++) {
                res.children[i] = prepareSnapshot(snapshot.Children[i]);
            }
        }
        return res;
    }

    
    SW.VIM.DeleteSnapshotDialog.refreshAllHighlight = function() {
        refreshHighlight();
    };
    
    
    function refreshHighlight(e, data) {
        if (data) {
            var checked = data.inst.get_checked();
            if (checked.length > 1) {
                for (var i = 0; i < checked.length; i++) {

                    if (data.rslt[0] != checked[i]) {
                        $("#sw-vim-dialogcontent").jstree("uncheck_node", checked[i]);
                    }
                }
            }
        }
        var dialogcontent = $("#sw-vim-dialogcontent");

        dialogcontent.jstree("deselect_all");
        var changed = data ? data.rslt[0] : dialogcontent.jstree("get_checked")[0];
        if (dialogcontent.jstree("is_checked", changed)) {
            if ($(".jstree-wholerow").length > 0 && parseInt($(".jstree-wholerow")[0].style.top, 10) != -$("#sw-vim-dialogcontent").children("ul").eq(0).height()) {
                //====jsTree wholerow plugin loading fix====
                //Wholerow uses element position to tederminate it index.
                //But it takes position not from original tree but from it own shadow copy.
                //This copy is being created immidiatly after tree nodes prepared.
                //But in some very rare cases (usually after browser restarted), tree can finalize it deploying a little bit later (for example image loading).
                //In this case shadow copy coordinates can be different from original tree.
                //So, Wholerow plugin can select another node to highlight.
                //The condition above determinates this difference (excpet IE7 which will not work properly).
                //And the code below set a right value.
                //Unfortunately we can't find better place for this code.
                //Because jsTree has no nodes after creating. It adds them one by one later, and we can't handle this process.
                //So, we try to fix shadow tree after first selection changed if it present: $(".jstree-wholerow").length > 0
                $(".jstree-wholerow").css("top", (-$("#sw-vim-dialogcontent").children("ul").eq(0).height()));
            }
            dialogcontent.jstree("select_node", changed);
            if ($('#sw-vim-deletechildren')[0].checked) {
                dialogcontent.jstree("select_node", $(changed).find('li'));
                dialogcontent.jstree("deselect_node", $('#current_tree_item'));
            }
        }
        $('#current_tree_item .jstree-checkbox').remove();
        $('#current_tree_item a > ins.jstree-icon').replaceWith('<img src="/Orion/VIM/images/flag.png" />');
        $("#dialogcontent a").css(" font-size", "11px");
        $("#dialogcontent a").css(" font-family", "Arial, Verdana, Helvetica, sans-serif");


    }



    function redrawTree(snapshots) {
        $.jstree._themes = "js/jsTree/themes/";

        $("#sw-vim-dialogcontent").empty();

        //Only "you are here" point
        if (snapshots.length == 1 && (!snapshots[0].Children || snapshots[0].Children.length == 0))
            return;

        for (var i = 0; i < snapshots.length; i++) {
            snapshots[i] = prepareSnapshot(snapshots[i]);
        }
        $('#sw-vim-dialogcontent').jstree({
            "core": {
                "initially_open": ["snapshot_tree_item"]
            },
            "json_data": {
                "data": snapshots
            },
            "themes": {
                "url": "/Orion/VIM/js/jsTree/themes/default/style.css",
                "theme": "default",
                "icons": false,
                "dots": false
            },
            "checkbox": {
                "two_state": true,
            },
            "ui": {
                "select_limit": 0
            },
            "types": {
                "types": {
                    "current_tree_item": {
                        "check_node": false,
                        "uncheck_node": false,
                        "hover_node": false
                    },
                    "snapshot_tree_item": {
                        "hover_node": false
                    }
                }
            },
            "sort": function (a, b) {
                if (a && a.id == "current_tree_item")
                    return -1;
                if (b && b.id == "current_tree_item")
                    return 1;
                return this.get_text(a) > this.get_text(b) ? 1 : -1;
            },

            "plugins": ["themes", "checkbox", "json_data", "ui", "types", "wholerow", "sort"]
        }).bind("change_state.jstree", refreshHighlight).bind("open_node.jstree", SW.VIM.DeleteSnapshotDialog.refreshAllHighlight);

        var settings = {
            findHtml: 'li',
            hoverArrowCls: 'sw-vim-dummy',
            hoverBodyParentCls: "sw-vim-snapshot-tooltip",
            iconCls: 'sw-vim-dummy',
            titleCls: 'sw-vim-dummy'
        };

        $('#sw-vim-dialogcontent').off("click.jstree", 'a');
        $('#sw-vim-dialogcontent').on("click.jstree", 'li > a', function (e) {
            e.stopPropagation();
            var $node = $(this).parents('li').eq(0);
            $('#sw-vim-dialogcontent').jstree('change_state', $node, 'toggle');
            return false;
        });

        SW.VMan.Tooltip.Init('#sw-vim-dialogcontent li > a', settings);


    }

    function htmlEncode(value) {
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return $('<div/>').text(value).html();
    }

    function htmlDecode(value) {
        return $('<div/>').html(value).text();
    }


    var failFinish = function(message) {
        $("#sw-vim-dialogcontent").empty();
        $('#sw-vim-dialogcontent').append(message);
    };
    titleName += ' ' + vmName;

    function successCallback(task) {
        var taskId = task;
        var gotResult = false;

        var getResult = setInterval(
            function() {
                SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetActionResultsDetails", { taskId: taskId, actionType: "GetSnapshots" },
                    function(result) {
                        gotResult = result.GotResult;
                        if (gotResult == true) {
                            clearInterval(getResult);
                            if (result.Data) {
                                redrawTree(result.Data);
                            } else {
                                failFinish(result.FailureMessage);
                            }
                        }
                    },
                    function(msg) {
                        clearInterval(getResult);
                        failFinish(msg);
                    });
            }, 3000);
    }

    function failCallback(response) {
        failFinish(response);
    }

    var parameters = [];
    SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "PerformManagementAction", { managementObjectId: vmId, actionType: "GetSnapshots", parameters: parameters, restartNedeed:false, vmName:vmName },
        successCallback, failCallback);

    var dialog;

    function onClose() {
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
    }

    function onDelete() {
        var checked = $("#sw-vim-dialogcontent").jstree("get_checked");
        if (checked.length != 1)
            return;
        SW.VIM.ManagementActionDialog(actionType, actionName, titleName, vmId, "", 2, false, VIMIsDemoMode() ? null : [decodeURIComponent(checked[0].attributes.path.value), $('#sw-vim-deletechildren')[0].checked], vmName);
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
    }


    var textHtml =
        '<div>' +
            '@{R=VIM.Strings;K=VIMWEBJS_VZ0_8;E=js}' + vmName +
            '<div class="sw-vim-vmmanag-delSnapshotArea" id = "sw-vim-dialogcontent">' +
            '<img src="/Orion/images/AJAX-Loader.gif" />@{R=VIM.Strings;K=VIMWEBJS_VZ0_2;E=js}<br />' +
            '</div>' +
            '{0}' +
            '{1}' +
            '</div>';
    var checkBox = '<div style="font-size: 11px; padding-top: 4px">';
    checkBox += '<input onclick="SW.VIM.DeleteSnapshotDialog.refreshAllHighlight();" type="checkbox" id="sw-vim-deletechildren"> @{R=VIM.Strings;K=VIMWEBJS_VZ0_3;E=js}';
    checkBox += '</div> <div id="jstree_demo_div"></div>';

    var dialogButtons = '<div style="float: right; font-size: 11px; padding-top: 12px">';
    dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_6;E=js}", { type: 'primary', id: 'sw-vim-btnYes' });
    dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_7;E=js}", { type: 'secondary', id: 'sw-vim-btnNo' });
    dialogButtons += '</div>';

    textHtml = String.format(textHtml, checkBox, dialogButtons);


    var $dialog = $(textHtml);
    $dialog.find("#sw-vim-btnYes").click(function () { onDelete(); });
    $dialog.find("#sw-vim-btnNo").click(function () { onClose(); });
    dialog = $dialog.dialog({
        width: 'auto',
        title: titleName,
        modal: true,
        resizable: false,
        open: function () {
            $('.ui-dialog-content').addClass('sw-vim-vmmanag-ui-dialog-content');
        },
        close: function() {
            if (typeof cancelHandler === 'function' && closeTrigger != "sw-vim-btnYes") {
                cancelHandler();
            }
            $(this).dialog('destroy').remove();
        }
    });
}
