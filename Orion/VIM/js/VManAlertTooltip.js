﻿SW.VMan = SW.VMan || {};
SW.VMan.AlertTooltip = SW.VMan.AlertTooltip || {};

(function (vmanAlert) {

    vmanAlert.Init = function () {

        $(document).mousemove(function (e) {
            if ($.browser.msie && e.clientX != null) {
                var html = document.documentElement;
                var body = document.body;
                e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
                e.pageX -= html.clientLeft || 0;
                e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0);
                e.pageY -= html.clientTop || 0;
            }
            cursor_position.x = e.pageX;
            cursor_position.y = e.pageY;
        });

        var $vmanTooltip = $('\
        <div class="tooltip-outer">\
            <div class="tooltip-wrapper">\
                <div class="tooltip-shadow" style="left: 1px; top: 1px;"></div>\
                <div class="tooltip-shadow" style="left: 2px; top: 2px;"></div>\
                <div class="tooltip-shadow" style="left: 3px; top: 3px;"></div>\
                <div class="tooltip-shadow" style="left: 4px; top: 4px;"></div>\
                <div class="tooltip-shadow" style="left: 5px; top: 5px;"></div>\
                <div class="tooltip-shadow" style="left: 6px; top: 6px;"></div>\
                <div class="cluetip-orion">\
                    <table class="cluetip-orion-table" cellpadding="0" cellspacing="0">\
                        <tr>\
                            <td class="cluetip-orion-inner-content" style=""></td>\
                        </tr>\
                    </table>\
                </div>\
            </div>\
            <div class="tooltip-stem"></div>\
        </div>\
    ');

        var $vmanTooltip_contentTemplate = '<h3 class="StatusUnknown">@{R=VIM.Strings;K=VIMWEBJS_LC0_4;E=js}</h3>\
                                        <div class="NetObjectTipBody sw-vman-alert-tooltip">\
                                            <table cellpadding="0" cellspacing="0">\
                                                <tr>\
                                                    <td class="sw-vman-alert-tooltip-title">{0}&nbsp;{1}</td>\
                                                </tr>\
                                                <tr>\
                                                    <td><b>@{R=VIM.Strings;K=VIMWEBJS_LC0_5;E=js} {2}</b></td>\
                                                </tr>\
                                                <tr>\
                                                    <td class="sw-vman-alert-tooltip-description">{3}</td>\
                                                </tr>\
                                            </table>\
                                        </div>';

        var $vmanTooltip_inner = $("td.cluetip-orion-inner-content", $vmanTooltip);
        var $vmanTooltip_stem = $("div.tooltip-stem", $vmanTooltip);
        var $vmanTooltip_wrapper = $("div.tooltip-wrapper", $vmanTooltip);
        var $vmanTooltip_shadow = $("div.tooltip-shadow", $vmanTooltip).css({ opacity: 0.1 });
        var $vmanTooltip_divs = $vmanTooltip.children().hide();
        var $vmanTooltip_timer;
        var $iframe = $('<iframe class="tooltip-iframe" scrolling="no" frameborder="0"></iframe>');

        var is_tooltip_available = false;
        var cursor_position = { x: 0, y: 0 };

        $.vmanAlertTooltip = function (control) {
            var $control = $(control);

            var iconHtml = $("<div />").append($('img', $control).first().clone()).html();  // get icon from the link control
            var tipText = String.format($vmanTooltip_contentTemplate, iconHtml, $control.attr('data-name'), $control.attr('data-time-raised'), $control.attr('data-description'));
            $vmanTooltip_wrapper.css('top', 0);
            
            // add iframe for put a tooltip DIV over a SELECT in IE6
            if ($.browser.msie && ($.browser.version == "6.0"))
                $("body").append($iframe);

            control.onmouseover = function () {
                $vmanTooltip_timer = setTimeout(function () {
                    if (!$control.vmanAlertTooltip)
                        $control.vmanAlertTooltip = new $vmanTooltipHelper(control, tipText);

                    $control.vmanAlertTooltip.show();
                }, 200);
            };
            control.onmouseout = function () {
                if ($vmanTooltip_timer) {
                    clearTimeout($vmanTooltip_timer);
                    $vmanTooltip_timer = null;
                }

                if ($.browser.msie && ($.browser.version == "6.0"))
                    $iframe.css('display', 'none');

                if ($control.vmanAlertTooltip)
                    $control.vmanAlertTooltip.hide();
            };

            // IE 7 HACK 
            // Unregistered events cause memory leaks.  
            if ($.browser.msie && parseInt($.browser.version, 10) == 7) {
                $(window).unload(function () {
                    control.onmouseover = null;
                    control.onmouseout = null;
                });
            }
        };

        $vmanTooltipHelper = function(control, tipText) {
            this.control = control;
            this.tipText = tipText;
            this.cache = {};

            $("img[alt]", control).removeAttr("alt");

            if (!is_tooltip_available) {
                is_tooltip_available = true;
                $("body").append($vmanTooltip);
            }
        };

        $.extend($vmanTooltipHelper.prototype, {
            static_cache: {},

            hide: function () {
                $vmanTooltip.hide();
                $vmanTooltip_divs.hide();
            },

            show: function () {
                if (this.tipText) {
                    $vmanTooltip_inner.html(this.tipText);
                    this.adjustPosition();
                }
            },

            adjustPosition: function () {
                $vmanTooltip.removeClass("stem-bottom-top stem-top-right stem-bottom-left stem-bottom-right");
                $vmanTooltip_divs.show();
                $vmanTooltip.show();
                var v = viewport();
                var p = position($vmanTooltip_wrapper);
                var r = (v.x + v.cx < p.x + $vmanTooltip_inner.width());
                var b = (v.y + v.cy < p.y + $vmanTooltip_inner.height());

                if (b && r) {
                    $vmanTooltip.addClass("stem-bottom-right");
                    $vmanTooltip.css('top', p.y - 20);
                    $vmanTooltip.css('left', p.x - $vmanTooltip_inner.width() - 40);
                    $vmanTooltip_stem.css('left', $vmanTooltip_inner.width() + 1);
                }
                else if (b) {
                    $vmanTooltip.addClass("stem-bottom-left");
                    $vmanTooltip.css('top', p.y - 20);
                    $vmanTooltip.css('left', p.x + 15);
                    $vmanTooltip_stem.css('left', 0);
                }
                else if (r) {
                    $vmanTooltip.addClass("stem-top-right");
                    $vmanTooltip.css('top', p.y);
                    $vmanTooltip.css('left', p.x - $vmanTooltip_inner.width() - 40);
                    $vmanTooltip_stem.css('left', $vmanTooltip_inner.width() + 1);
                }
                else {
                    $vmanTooltip.addClass("stem-top-left");
                    $vmanTooltip.css('top', p.y);
                    $vmanTooltip.css('left', p.x + 15);
                    $vmanTooltip_stem.css('left', 0);
                }

                if ((p.cy + p.y) < $vmanTooltip_stem.height())
                    $vmanTooltip_wrapper.css('top', '0');

                $vmanTooltip_shadow.css({
                    width: $vmanTooltip_inner.width(),
                    height: $vmanTooltip_inner.height()
                });

                if ($.browser.msie && ($.browser.version == "6.0")) {
                    $iframe.css('display', 'block');
                    $iframe.css({
                        width: $vmanTooltip_inner.width() + 4,
                        height: $vmanTooltip_inner.height() + 4,
                        top: $vmanTooltip_inner.offset().top,
                        left: $vmanTooltip_inner.offset().left
                    });
                }
            },

            width: function () {
                return this.static_cache.width || (this.static_cache.width = this.getWidth());
            },

            getWidth: function () {
                var left = 0;
                var right = 0;

                $vmanTooltip.children().each(function () {
                    var p = position($(this));

                    if (p.x < left)
                        left = p.x;

                    if (p.x + p.cx > right)
                        right = p.x + p.cx;
                });

                return right - left;
            }
        });

        function viewport() {
            var $window = $(window);

            return {
                x: $window.scrollLeft(),
                y: $window.scrollTop(),
                cx: $window.width(),
                cy: $window.height()
            };
        }

        function position(control) {
            return {
                x: cursor_position.x, //p.left,
                y: cursor_position.y, //p.top,
                cx: control.width,
                cy: control.heigth
            };
        }

        // do not show tooltips if the link has a class "NoTip"
        $("a[data-vman-alert-tooltip='tooltip'][tooltip!='processed']:not(.NoTip)").livequery(function () {
            this.tooltip = 'processed';
            $.vmanAlertTooltip(this);
        });
    };

})(SW.VMan.AlertTooltip);
