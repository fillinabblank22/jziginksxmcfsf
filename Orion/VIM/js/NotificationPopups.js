﻿SW.VIM = SW.VIM || {};
SW.VIM.NotificationPopups = SW.VIM.
    NotificationPopups || {};

(function (vimPopups) {

    var contentWrapper = '<div class="sw-vman-alert"><table><tr><td style="width:40px;"><img src="{0}" alt="" /></td><td>{1}</td></tr></table><div style="float:right">{2}</div></div>';
    var licenseNotificationWrapper = '{0}<br /><br /><span class="LinkArrow">&#0187;</span>&nbsp;<a href="http://www.solarwinds.com/documentation/helpLoader.aspx?lang=@{R=Core.Strings;K=CurrentHelpLanguage; E=js}&topic=OrionVM_InstallationLicensing.htm" class="linkLabel" target="_blank">@{R=Vim.Strings;K=VIMWEBJS_LV0_8; E=js}</a><br/><br/>';
    
    function onClose(dialog) {
        if (dialog) {
            dialog.dialog("close");
        }
    }
    
    vimPopups.ShowInvalidMasterLicenseWarningPopup = function (daysRemaining) {
        var message = String.format('@{R=Vim.Strings;K=VIMWEBJS_LV0_6; E=js}', daysRemaining);        
        var body = String.format(licenseNotificationWrapper, message);
        var button = SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}", { type: "primary", id: "okBtn" });
        var content = $(String.format(contentWrapper, '/Orion/images/NotificationImages/notification_warning.gif', body, button));

        var dialog = content.dialog({
            width: 600,
            modal: true,
            title: '@{R=Vim.Strings;K=VIMWEBJS_LV0_5; E=js}',
            resizable: false
        });

        dialog.find("#okBtn").click(function () {
            onClose(dialog);
        });
    }

    vimPopups.ShowInvalidMasterLicenseErrorPopup = function () {        
        var message = String.format('@{R=Vim.Strings;K=VIMWEBJS_LV0_7; E=js}', 7);
        var body = String.format(licenseNotificationWrapper, message);
        var button = SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}", { type: "primary", id: "okBtn" });
        var content = $(String.format(contentWrapper, '/Orion/Images/stop_32x32.gif', body, button));

        var dialog = content.dialog({
            width: 600,
            modal: true,
            title: '@{R=Vim.Strings;K=VIMWEBJS_LV0_5; E=js}',
            resizable: false
        });

        dialog.find("#okBtn").click(function () {
            onClose(dialog);
        });
    }    
})(SW.VIM.NotificationPopups);