﻿Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.SelectObjects = function () {
    ORION.prefix = "VIM_SelectObjects_";

    var batchSize = 100;
    var batchDelay = 10;
    var exportTask = 0;
    var importTask = 1;

    var sourceDataStore;
    var targetDataStore;
    var targetGridPanel;
    var initialized;
    var itemsToAdd;
    var sourceMask;
    var targetMask;
    var task;

    var rightPanelTitle = '';
    var leftPanelTitle = '';
    var entityName = '';
    var sourceGrid;
    var targetGrid;
    var sourceGridPanel;
    var targetGridItemsFieldClientID = '';

    InitDragDrop = function () {

        // adding to source grid
        var sourceGridDropTargetEl = sourceGrid.getView().scroller.dom;
        var sourceGridDropTarget = new Ext.dd.DropTarget(sourceGridDropTargetEl, {
            ddGroup: 'sourceGridDDGroup',
            notifyDrop: function (ddSource, e, data) {
                RemoveCheckedSourcesFromGrid();
                return true;
            }
        });

        // adding to target grid
        var targetGridDropTargetEl = targetGrid.getView().scroller.dom;
        var targetGridDropTarget = new Ext.dd.DropTarget(targetGridDropTargetEl, {
            ddGroup: 'targetGridDDGroup',
            notifyDrop: function (ddSource, e, data) {                
                AddCheckedSourcesToGrid(sourceGrid, targetGrid);
                return true;
            }
        });
    };

    RefreshItemsData = function () {
        var data = '';
        var first = true;

        targetGrid.store.each(function (record) {
            if (!first) {
                data += ",";
            }
            data += record.data.Id;
            first = false;
        });

        $('#' + targetGridItemsFieldClientID).val(data);
    };

    AddBatch = function (source, target, callback) {
        if (itemsToAdd.length > 0) {
            var blankRecord = Ext.data.Record.create(target.store.fields);

            if (itemsToAdd.length > 0) {
                for (var i = itemsToAdd.length - 1, j = 0; i >= 0, j < batchSize; i--, j++) {
                    if (i < 0)
                        break;

                    var item = itemsToAdd[i];
                    var record = new blankRecord({
                        Id: item.data.Id,
                        Name: item.data.Name,
                        Type: item.data.Type,
                        Platform: item.data.Platform,
                        Synced: false
                    });

                    var index;

                    if (target == sourceGrid) {
                        target.store.clearFilter();
                        index = target.store.findExact("Id", record.data.Id);
                        if (index != -1) {
                            source.store.remove(item);
                            target.store.getAt(index).data.Synced = false;
                        } else {
                            target.store.add(record);
                            source.store.remove(item);
                        }
                        target.store.filterBy(FilterDataSources);
                    } else {
                        index = target.store.findExact("Id", record.data.Id);
                        if (index != -1) {
                            source.store.remove(item);
                        } else {
                            target.store.add(record);
                            source.store.remove(item);
                        }
                    }

                    itemsToAdd.remove(item);
                }
            }
        }

        if (itemsToAdd.length > 0) {
            setTimeout(function () { AddBatch(source, target, callback) }, batchDelay);
        } else {
            if (callback) {
                callback();
            }
        }
    };

    ShowMask = function (grid, message) {
        var container = grid == sourceGrid ? sourceGridPanel : targetGrid;
        var mask = new Ext.LoadMask(container.el, {
            msg: message
        });

        if (container == sourceGridPanel) {
            sourceMask = mask;
        } else {
            targetMask = mask;
        }

        mask.show();
        grid.store.suspendEvents(false);
    };

    HideMask = function (grid) {
        if (grid == sourceGrid)
            grid.store.filterBy(FilterDataSources);

        grid.store.sort('Name', 'ASC');
        grid.store.resumeEvents();
        grid.store.fireEvent('datachanged');

        var mask = GetMaskForGrid(grid);
        if (mask != undefined)
            mask.hide();
    };

    GetMaskForGrid = function (grid) {
        return grid == sourceGrid ? sourceMask : targetMask;
    };

    RemoveCheckedSourcesFromGrid = function () {
        if (targetGrid.selModel.getSelections().length > 0) {

            var title = '';
            var prompt = '';

            if (task == exportTask) {
                title = "@{R=VIM.Strings;K=VIMWEBJS_JH0_27;E=js}";
                prompt = "@{R=VIM.Strings;K=VIMWEBJS_JH0_28;E=js}";
            } else {
                title = "@{R=VIM.Strings;K=VIMWEBJS_JH0_29;E=js}";
                prompt = "@{R=VIM.Strings;K=VIMWEBJS_JH0_30;E=js}";
            }

            Ext.Msg.confirm(title, prompt, function (btn, text) {
                if (btn == "yes") {
                    AddCheckedSourcesToGrid(targetGrid, sourceGrid);
                }
            });
        }
    };

    AddCheckedSourcesToGrid = function (source, target) {
        itemsToAdd = [];
        itemsToAdd = source.selModel.getSelections();

        if (itemsToAdd.length == 0) {
            HideMask(target);
            HideMask(source);
        }
        else {
            ShowMask(source, '@{R=VIM.Strings;K=VIMWEBJS_JH0_31;E=js}');
            ShowMask(target, '@{R=VIM.Strings;K=VIMWEBJS_JH0_32;E=js}');
            var callback = function () {
                HideMask(target);
                HideMask(source);
            };
            AddBatch(source, target, callback);
        }
    };

    LoadVManDataSourcesForImport = function () {
        ShowMask(sourceGrid, '@{R=VIM.Strings;K=VIMWEBJS_JH0_33;E=js}');
        ORION.callWebService("/Orion/VIM/Services/VManSynchronizationWizard.asmx", "LoadVManDataSourcesForImport", {}, function (result) {
            var blankRecord = Ext.data.Record.create(sourceGrid.store.fields);

            if (result != null) {
                for (var i = 0; i < result.Rows.length; i++) {
                    var item = result.Rows[i];
                    var record = new blankRecord({
                        Id: item[0],
                        Name: item[1],
                        Platform: item[2],
                        Type: item[3],
                        Synced: item[4]
                    });

                    if (sourceGrid.store.findExact("Id", record.data.Id) == -1) {
                        sourceGrid.store.add(record);
                    }
                }
            }

            HideMask(sourceGrid);
            sourceGrid.store.filterBy(FilterDataSources);
            sourceGrid.store.sort('Name', 'ASC');

            LoadVIMDataSourcesForImport();
        },
        function () {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_34;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_35;E=js}');
        });

    };

    FilterDSCheckBox_clickHandler = function () {
        if (sourceGrid != undefined && sourceGrid.store != undefined)
            sourceGrid.store.filterBy(FilterDataSources);
    };

    FilterDataSources = function (record, id) {
        if (IsInTargetGrid(record)) {
            return false;
        }

        return $("#FilterDSCheckBox").is(":checked") ? !record.data.Synced : true;
    };

    IsInTargetGrid = function(record) {
        return (targetGrid.store.find("Id", record.data.Id) != -1);
    };

    LoadVIMDataSourcesForImport = function () {
        ShowMask(targetGrid, 'Loading VIM Data Sources');
        ORION.callWebService("/Orion/VIM/Services/VManSynchronizationWizard.asmx", "LoadVIMDataSourcesForImport", {}, function (result) {
            var blankRecord = Ext.data.Record.create(sourceGrid.store.fields);

            if (result != null) {
                for (var i = 0; i < result.Rows.length; i++) {
                    var item = result.Rows[i];

                    var record = new blankRecord({
                        Id: item[0],
                        Name: item[1],
                        Type: item[2],
                        Platform: item[3],
                        Synced: item[4]
                    });

                    if (targetGrid.store.findExact("Id", record.data.Id) == -1) {
                        targetGrid.store.add(record);
                    }
                }
            }

            HideMask(targetGrid);
        });
    };


    InitSourceGrid = function () {
        var record = Ext.data.Record.create([
            { name: 'Id' },
            { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
            { name: 'Type' },
            { name: 'Platform' },
            { name: 'Synced' }
        ]);
        var arrayReader = new Ext.data.ArrayReader({
            idIndex: 0
        },
            record);

        sourceDataStore = new Ext.data.Store({
            reader: arrayReader,
            sortInfo: { field: 'Name', direction: 'ASC' }
        });

        //Necessary workaround to handle DnD and multiselection on GridPanel               
        Ext.override(Ext.grid.GridDragZone, {
            getDragData: function (e) {
                var target = Ext.lib.Event.getTarget(e);
                var rowIndex = this.view.findRowIndex(target);
                
                if (rowIndex !== false) {
                    var sm = this.grid.selModel;
                    
                    if (sm instanceof (Ext.grid.CheckboxSelectionModel)) {                        
                        sm.onMouseDown(e, target);
                    }

                    if (target.className != 'x-grid3-row-checker' && (!sm.isSelected(rowIndex) || e.hasModifier())) {
                        sm.handleMouseDown(this.grid, rowIndex, e);
                    }
                    
                    return { grid: this.grid, ddel: this.ddel, rowIndex: rowIndex, selections: sm.getSelections() };
                }
                return false;
            }
        });                

        var selectorModel = new Ext.grid.CheckboxSelectionModel();

        sourceGrid = new Ext.grid.GridPanel({
            store: sourceDataStore,

            columns: [
                selectorModel,
                { id: 'Name', header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_37;E=js}', sortable: true, hideable: false, dataIndex: 'Name' }
            ],
            autoExpandColumn: 'Name',

            sm: selectorModel,
            viewConfig: {
                forceFit: false
            },

            border: true,
            hideHeaders: true,
            height: 433,
            stripeRows: true,
            enableDragDrop: true,
            ddGroup: 'targetGridDDGroup',
            region: 'center',
            loadMask: { msg: '@{R=VIM.Strings;K=VIMWEBJS_JH0_38;E=js}' },

            tbar: [{
                text: "@{R=VIM.Strings;K=VIMWEBJS_JH0_39;E=js}",
                iconCls: 'selectAllButton',
                handler: function () {
                    selectorModel.selectAll();
                }
            }, '-', {
                text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_40;E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    selectorModel.clearSelections();
                }
            }]
        });
    };


    InitTargetGrid = function () {        
        var record = Ext.data.Record.create([
                { name: 'Id' },
                { name: 'Name', sortType: Ext.data.SortTypes.asUCString },
                { name: 'Type' },
                { name: 'Platform' },
                { name: 'Synced' }
            ]);
        var arrayReader = new Ext.data.ArrayReader({
            idIndex: 0
        },
                record);

        targetDataStore = new Ext.data.Store({
            reader: arrayReader,
            sortInfo: { field: 'Name', direction: 'ASC' }
        });



        targetDataStore.on("add", RefreshItemsData);
        targetDataStore.on("remove", RefreshItemsData);
        targetDataStore.on('datachanged', RefreshItemsData);

        var selectorModel = new Ext.grid.CheckboxSelectionModel();

        targetGrid = new Ext.grid.GridPanel({
            store: targetDataStore,

            columns: [
                    selectorModel,
                    { id: 'Name', header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_42;E=js}', width: 300, sortable: true, hideable: false, dataIndex: 'Name' }
                ],
            autoExpandColumn: 'Name',

            sm: selectorModel,

            viewConfig: {
                forceFit: true
            },

            border: true,
            frame: true,
            hideHeaders: true,
            stripeRows: true,
            ddGroup: 'sourceGridDDGroup',
            enableDragDrop: true,
            region: 'center',
            title: (rightPanelTitle) ? Ext.util.Format.htmlEncode(rightPanelTitle) : '@{R=VIM.Strings;K=VIMWEBJS_JH0_41;E=js}',
            loadMask: { msg: '@{R=VIM.Strings;K=VIMWEBJS_JH0_38;E=js}' },

            tbar: [{
                text: "@{R=VIM.Strings;K=VIMWEBJS_JH0_39;E=js}",
                iconCls: 'selectAllButton',
                handler: function () {
                    selectorModel.selectAll();
                }
            }, '-', {
                text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_40;E=js}',
                iconCls: 'selectNoneButton',
                handler: function () {
                    selectorModel.clearSelections();
                }
            }]
        });

        var addButtonPanel = new Ext.Panel({
            border: false,
            region: 'west',
            width: 54,
            contentEl: 'AddButtonPanel'
        });

        targetGridPanel = new Ext.Panel({
            id: 'AddButtonPanelExtPanel',
            region: 'center',
            layout: 'border',
            border: false,
            items: [addButtonPanel, targetGrid]
        });
    };

    InitLayout = function () {

        var container = new Ext.Container({
            applyTo: 'SourceDataOptionBox',
            region: 'north',
            layout: 'fit',
            width: 320,
            height: 30
        });

        sourceGridPanel = new Ext.Panel({
            title: (leftPanelTitle) ? Ext.util.Format.htmlEncode(leftPanelTitle) : '@{R=VIM.Strings;K=VIMWEBJS_JH0_41;E=js}',
            frame: true,
            split: true,
            region: 'west',
            width: 320,
            border: true,
            items: [
                container,
                sourceGrid
            ]
        });

        var panel = new Ext.Container({
            id: 'MainExtPanel',
            renderTo: 'ContainerMembersTable',
            height: 500,
            layout: 'border',
            items: [sourceGridPanel, targetGridPanel]
        });

        $(window).bind('resize', function () {
            panel.doLayout();
        });
    };

    return {
        SetTitles: function (leftPanel, rightPanel) {
            leftPanelTitle = leftPanel;
            rightPanelTitle = rightPanel;
        },
        SetEntity: function (name) {
            entityName = name;
        },

        SetTask: function (taskName) {
            task = taskName;
        },
        SetTargetGridItemsFieldClientID: function (id) {
            targetGridItemsFieldClientID = id;
        },
        IMPORT: importTask,
        EXPORT: exportTask,
        init: function () {
            if (initialized)
                return;

            initialized = true;

            InitSourceGrid();
            InitTargetGrid();
            InitLayout();

            InitDragDrop();

            if (task == exportTask) {
                //@todo
                //LoadVIMDataSourcesForExport();                
            }
            else {
                LoadVManDataSourcesForImport();
            }

            $('#AddButtonPanel').height($('#ContainerMembersTable').height());
            $('#AddToGridButton').click(function () { AddCheckedSourcesToGrid(sourceGrid, targetGrid); });
            $('#RemoveFromGridButton').click(function () { RemoveCheckedSourcesFromGrid(); });
            $('#FilterDSCheckBox').click(function () { FilterDSCheckBox_clickHandler(); });
        }
    };
} ();

Ext.onReady(SW.VIM.SelectObjects.init, SW.VIM.SelectObjects);
