﻿/* global SW */

(function (charts) {
    // Let modules define their custom formatters before this script. Sometimes it may
    // be difficult to put module script after this script.
    charts.dataFormatters = charts.dataFormatters || {};

    function isDefined(value) {
        return value !== undefined && value !== null;
    }

    function formatValue(value, multiplier, numberFormat, unit) {
        var multipliedValue = value / multiplier;
        var formattedValue = multipliedValue.toFixed(numberFormat.decimalPlaces);
        if (isDefined(numberFormat.customPrecision)) {
            if (numberFormat.customPrecision > numberFormat.decimalPlaces) {
                var preciseFormattedValue = (multipliedValue.toFixed(numberFormat.customPrecision) * 1).toString();
                if (preciseFormattedValue.length > formattedValue.length) {
                    formattedValue = preciseFormattedValue;
                }
            }
        }

        // dealing with -0.00
        if (formattedValue == 0) {
            formattedValue = (0).toFixed(numberFormat.decimalPlaces);
        }

        // Localization of decimal mark
        return formattedValue + ((unit.length > 0) ? " " + unit : "");
    }

    function getNumberFormat(decimalPlaces, axisOptions) {
        var retval = { decimalPlaces: 2 }; // default precision
        if (isDefined(decimalPlaces)) {
            retval.decimalPlaces = decimalPlaces;
        }
        if (isDefined(axisOptions) && isDefined(axisOptions.customPrecision)) {
            retval.customPrecision = axisOptions.customPrecision;
        }
        return retval;
    }

    function innerFormat(value, orderMultiplier, numberFormat, orderUnits, targetUnit) {
        if (isDefined(targetUnit)) {
            return formatValue(value, Math.pow(orderMultiplier, orderUnits.indexOf(targetUnit)), numberFormat, targetUnit);
        }
        var currentOrderMultiplier = 1;
        for (var order = 0; order < orderUnits.length; order++) {
            var nextOrderMultiplier = currentOrderMultiplier * orderMultiplier;
            if (Math.abs(value) < nextOrderMultiplier || order == orderUnits.length - 1) {
                return formatValue(value, currentOrderMultiplier, numberFormat, orderUnits[order]);
            }
            currentOrderMultiplier = nextOrderMultiplier;
        }
        return value;
    }

    function innerFormatTime(value, noFormatLimit) {
        //Don´t format when value is small
        var limit = isDefined(noFormatLimit) ? noFormatLimit : 300;
        if (value <= limit) {
            return value.toFixed(2) + " s";
        }

        var tmpValue = Math.round(value);

        //sec
        var result = tmpValue % 60 + "s";
        if (tmpValue >= 60) {
            tmpValue = Math.floor(tmpValue / 60);

            //min
            result = tmpValue % 60 + "m " + result;
            if (tmpValue >= 60) {
                result = Math.floor(tmpValue / 60) + "h " + result;
            }
        }

        return result;
    }

    charts.dataFormatters.statistic = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1000, getNumberFormat(decimalPlaces, axis), ["", "K", "M", "G", "T", "P", "E", "Z", "Y"], targetUnit);
    };

    charts.dataFormatters.byte = function (value, axis, decimalPlaces, targetUnit, startingUnit) {
        var units = ["bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"].slice(startingUnit);
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), units, targetUnit);
    };

    charts.dataFormatters.kbyte = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], targetUnit);
    };

    charts.dataFormatters.percent = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1e+10, getNumberFormat(decimalPlaces, axis), ["%"], targetUnit);
    };

    charts.dataFormatters.msec = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1000, getNumberFormat(decimalPlaces, axis), ["ms", "s"], targetUnit);
    };

    charts.dataFormatters.persecond = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1000, getNumberFormat(decimalPlaces, axis), ["/s"], targetUnit);
    };

    charts.dataFormatters.kbytepersecond = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ["kB/s", "MB/s", "GB/s", "TB/s"], targetUnit);
    };

    charts.dataFormatters.bytepersecond = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ["B/s", "kB/s", "MB/s", "GB/s", "TB/s"], targetUnit);
    };

    charts.dataFormatters.bitpersecond = function (value, axis, decimalPlaces, targetUnit) {
        return innerFormat(value, 1024, getNumberFormat(decimalPlaces, axis), ["bps", "kbps", "Mbps", "Gbps", "Tbps"], targetUnit);
    };

    charts.dataFormatters.second = function (value) {
        return innerFormatTime(value, 300);
    };

    charts.dataFormatters.secondNoLimit = function (value) {
        return innerFormatTime(value, 0);
    };

    function formatByUnit(unit, value, axis, decimalPlaces, targetUnit) {
        var formatter = charts.dataFormatters[unit];
        return formatter && formatter(value, axis, decimalPlaces, targetUnit);
    }

    function formatAny(value, axis, decimalPlaces, targetUnit) {
        if (!isDefined(axis)) {
            return value;
        }
        return (axis.unit === "")
            ? value.toFixed(2).toLocaleString()
            : formatByUnit(axis.unit, value, axis, decimalPlaces, targetUnit);
    }

    charts.dataFormatters.any = formatAny;

    charts.getUnitFromValue = function (formattedValue) {
        var index = formattedValue.indexOf(" ");
        return (index >= 0)
            ? formattedValue.substr(index + 1)
            : "";
    };

    charts.getValidDecimalPlaces = function (formattedValue, requiredPrecision) {
        var retval = 0;

        if (!isDefined(requiredPrecision)) {
            requiredPrecision = 2;  // default precision
        }

        var matchResults = formattedValue.match(/\.\d+/g); // take only decimal point and following digits
        if (matchResults.length > 0) {

            var precisedValue = parseFloat(matchResults[0]).toPrecision(requiredPrecision);

            // get num of decimal places
            var decimalPlaces = 0;
            var matchNumber = parseFloat(precisedValue).toExponential().match(/\d+(\.\d+)?e-(\d+)/);
            if (matchNumber != null && matchNumber.length > 2) {
                if (isDefined(matchNumber[1])) {
                    decimalPlaces += matchNumber[1].length - 1; // do not count decimal point
                }
                if (isDefined(matchNumber[2])) {
                    decimalPlaces += parseInt(matchNumber[2], 10);
                }
            }

            retval = Math.max(requiredPrecision, decimalPlaces);
        }
        return retval;
    };

    charts.formatValue = function (rawValue, formatOptions, edgeValues) {
        function format(value, decimalPlaces) {
            return formatAny(value, formatOptions, decimalPlaces, currentUnit);
        }

        var formattedValue = formatAny(rawValue, formatOptions);

        if (edgeValues && edgeValues.length
            && isDefined(formatOptions)
            && isDefined(formatOptions.unit)
            && formatOptions.unit.indexOf("second") != 0) {

            var currentUnit = SW.Core.Charts.getUnitFromValue(formattedValue);
            var customPrecisions = [];
            $.each(edgeValues, function (index, value) {
                if (format(value, null) == formattedValue) {
                    var formattedDiff = format(Math.abs(rawValue - value), 10);
                    customPrecisions.push(SW.Core.Charts.getValidDecimalPlaces(formattedDiff));
                }
            });

            if (customPrecisions.length > 0) {
                formatOptions.customPrecision = Math.max.apply(Math, customPrecisions);
                formattedValue = format(rawValue, formatOptions.customPrecision);
            }
        }

        return formattedValue;
    };

    charts.dataFormatters.getFriendlyDateString = function (date) {
        if (date === null || isNaN(date)) {
            return "";
        }

        var dateToCompare = new Date(date.valueOf());
        var now = new Date();
        var dateStrPart;

        if (dateToCompare.setHours(0, 0, 0, 0) == now.setHours(0, 0, 0, 0)) {
            dateStrPart = "@{R=VIM.Strings;K=VIMWEBJS_LC0_6;E=js}";
        } else {
            var dif = now.setHours(0, 0, 0, 0) - dateToCompare.setHours(0, 0, 0, 0);
            if (dif > 0 & dif <= 86400000) {
                dateStrPart = "@{R=VIM.Strings;K=VIMWEBJS_LC0_7;E=js}";
            } else if (dif < 0 & dif >= -86400000) {
                dateStrPart = "@{R=VIM.Strings;K=VIMWEBJS_LC0_8;E=js}";
            }
        }

        return String.format(
            "@{R=VIM.Strings;K=VIMWEBJS_LC0_9;E=js}",
            dateStrPart == null
                ? date.toLocaleDateString()
                : dateStrPart,
            date.toLocaleTimeString()
        );
    };

}(SW.Core.namespace("SW.Core.Charts")));
