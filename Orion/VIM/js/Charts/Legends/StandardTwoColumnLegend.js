﻿
(function (legend) {

    var coreCharting = SW.Core.namespace("SW.Core.Charts.Legend");

    legend.calculateVisibleSeriesCount = function (dataSeries) {
        var count = 0;

        $.each(dataSeries, function (index, series) {
            if (series.options.showInLegend) {
                count++;
            }
        });
        return count;
    }

    legend.calculateSingleColumnItemLimit = function (visibleSeriesCount) {
        return Math.ceil(visibleSeriesCount / 2);
    }

    legend.buildLegendItem = function (series, row, interactiveMode) {
        var td;
        if (interactiveMode) {
            td = $("<td style=\"width:15px;\"/>").appendTo(row);
            coreCharting.addCheckbox(series, td);
        }

        td = $("<td style=\"width:20px;\"/>").appendTo(row);
        coreCharting.addLegendSymbol(series, td);

        td = $("<td/>").appendTo(row);
        coreCharting.addTitle(series, td);

        $("<td/>").appendTo(row);
    }

    legend.createStandardLegend = function (chart, dataUrlParameters, legendContainerId, interactiveMode) {
        var table = $("#" + legendContainerId);

        if (typeof (interactiveMode) == "undefined") {
            interactiveMode = true;
        }

        var visibleSeriesCount = legend.calculateVisibleSeriesCount(chart.series);
        var columnItemLimit = legend.calculateSingleColumnItemLimit(visibleSeriesCount);

        $.each(chart.series, function (index, series) {
            if (!series.options.showInLegend) {
                return;
            }

            var tableBody = table.children();
            var row;

            if (index < columnItemLimit) {
                row = $("<tr/>");

                legend.buildLegendItem(series, row, interactiveMode);
                row.appendTo(table);

            } else {
                var rowIndex = index - columnItemLimit + 1;
                var childRowSelector = "tr:nth-child(" + rowIndex + ")";

                row = tableBody.children(childRowSelector);

                legend.buildLegendItem(series, row, interactiveMode);
            }
        });

    };

}(SW.Core.namespace("SW.VMan.Charts.StandardTwoColumnLegend")));