﻿/*jslint browser: true*/
/*global SW: true, $: false*/
SW = SW || {};
SW.VIM = SW.VIM || {};
SW.VIM.Charts = SW.VIM.Charts || {};
SW.VIM.Charts.TooltipHilite = SW.VIM.Charts.TooltipHilite || {};
(function tooltipHiliteModuleInit(module) {
    'use strict';
    function ensureObject(source, path) {
        source = source || {};
        var target = source, modules = path.split('.');
        $.each(modules, function () {
            if (!target.hasOwnProperty(this)) {
                target[this] = {};
            }
            target = target[this];
        });
        return source;
    }
    module.initializeStandardChart = function tooltipHiliteInitializeStandardChart(chartSettings, chartOptions) {
        var chartHilite = {};
        chartOptions = ensureObject(chartOptions, 'chart.events');
        // init series tooltip hilite state
        chartOptions.chart.events.addSeries = function tooltipHiliteAddSeries() {
            this.swState = '';
        };
        ensureObject(chartOptions, 'plotOptions.series.events');
        // series tooltip hilite state update
        chartOptions.plotOptions.series.events.mouseOver = function tooltipHiliteMouseOver() {
            var series = this, update = false;
            if (chartHilite.lastActiveSeries) {
                if (chartHilite.lastActiveSeries !== series) {
                    chartHilite.lastActiveSeries.swState = '';
                    update = true;
                }
            } else {
                update = true;
            }
            if (update) {
                chartHilite.lastActiveSeries = series;
                series.swState = 'hover';
                series.chart.redraw();
            }
        };
        ensureObject(chartOptions, 'tooltip');
        chartOptions.tooltip.pointFormat = '<tr style="line-height: 90%;" class="{series.swState}"><td style="border: 0; font-size: 12px; color: {series.color};">{series.name}: </td><td style="border: 0px; font-size: 12px"><b>{point.y}</b></td></td></tr>';
        // call original init function with modified options
        SW.Core.Charts.initializeStandardChart(chartSettings, chartOptions);
    };
}(SW.VIM.Charts.TooltipHilite));
