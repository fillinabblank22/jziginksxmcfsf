SW = SW || {};
SW.VIM = SW.VIM || {};

SW.VIM.AssetSummary = {

    ExpandCollapse: function (element) {
        expanded = element.src.indexOf("Collapse") >= 0;

        element.src = expanded ? "/Orion/images/Button.Expand.gif" : "/Orion/images/Button.Collapse.gif";

        // get all parents of the element
        parents = $(element).parents();

        // find the first TABLE parent element
        i = 0;
        while (parents.get(i).tagName != "TABLE" && i < parents.length) {
            i++;
        }

        if (i < parents.length) {
            // find the all child elements with 'vimCollapsableTableRow' CSS class and set display property
            $(parents.get(i)).find(".vimCollapsableTableRow").css("display", expanded ? "none" : "table-row");
        }

        return false;
    }
};
