﻿Ext.namespace('SW.VIM');

SW.VIM.SyncMissingNodesInVMan = function () {
    var grid;
    var dialog;

    function OpenDialog() {
        if (!dialog) {
            dialog = new Ext.Window({
                applyTo: 'dlg',
                layout: 'fit',
                width: 790,
                height: 'auto',
                modal: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,
                bodyBorder: false,
                contentEl: 'dlgBody',
                buttons: [
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_65;E=js}',
                        handler: function () {
                            dialog.hide();
                        }
                    }
                ]
            });
        }

        dialog.setTitle('@{R=VIM.Strings;K=VIMWEBJS_JH0_62;E=js}');
        dialog.alignTo(document.body, "t-t", [0, 110]);
        dialog.show();
    }

    function LoadData() {
        VManSynchronizationWizardService.LoadMissingNodesInVMan(LoadDataSuccessCallback, LoadDataErrorCallback);
    };

    function LoadDataSuccessCallback(result) {
        if (result && result.Rows) {
            store.loadData(result, true);
        }
    }

    function LoadDataErrorCallback(result) {
        if (result && result._message) {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js} ' + result._message);
        }
        else {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js}');
        }
    }

    var jsonReader = new Ext.data.JsonReader({
        root: 'Rows',
        fields: [
            { name: 'Name', mapping: 'Name' },
            { name: 'Type', mapping: 'Type' },
            { name: 'Credentials', mapping: 'Credentials' }
        ]
    });

    var store = new Ext.data.Store({
        reader: jsonReader,
        fields: [
            { name: 'Name', type: 'string' },
            { name: 'Type', type: 'string' },
            { name: 'Credentials', type: 'string' }
        ]
    });

    var colModel = new Ext.grid.ColumnModel({
        columns: [
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_63;E=js}', dataIndex: 'Name', width: 360, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_67;E=js}', dataIndex: 'Type', width: 140, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_64;E=js}', dataIndex: 'Credentials', width: 240, fixed: true }
        ],

        defaults: {
            sortable: false,
            menuDisabled: true,
            hideable: false
        }
    });

    function InitGrid() {
        if (!grid) {
            grid = new Ext.grid.GridPanel({
                renderTo: "sw-vim-wizard-oriondatasources-grid",
                store: store,
                layout: {
                    type: 'fit',
                    align: 'stretch',
                    pack: 'start'
                },
                height: SW.VIM.Utils.GetManagementGridHeight(),
                colModel: colModel,
                viewConfig: { forceFit: true },
                stripeRows: true,
            });

            Ext.EventManager.onWindowResize(function () {
                grid.setWidth(1);
                grid.setWidth($('#sw-vim-wizard-oriondatasources-grid').width());

                grid.setHeight(SW.VIM.Utils.GetManagementGridHeight());
                grid.doLayout();
            }, container);

            LoadData();
        }
    }

    return {
        OpenDialog: function () {
            OpenDialog();
            InitGrid();
        }
    };
}();