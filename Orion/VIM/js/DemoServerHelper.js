function VIMIsDemoMode() {
    return ($("#isDemoMode").length != 0);
}

function VIMDemoModeMessage() {
    alert('@{R=VIM.Strings;K=VIMWEBJS_VB0_10;E=js}');
    return false;
}