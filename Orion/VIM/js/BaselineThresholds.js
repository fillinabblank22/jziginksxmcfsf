﻿Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.BaselineThresholds = function () {

    var swisVCenterEntity = 'Orion.VIM.VCenters';
    var swisClusterEntity = 'Orion.VIM.Clusters';
    var swisHostEntity = 'Orion.VIM.Hosts';
    var swisDatastoreEntity = 'Orion.VIM.Datastores';
    var swisDatacenterEntity = 'Orion.VIM.DataCenters';
    var swisVMEntity = "Orion.VIM.VirtualMachines";
    var pollingSourceVIM = 0;
    var pollingSourceVMan = 1;    
        
    // grid
    var grid;
    var expandedItems = [];
    var dataStore;
    var dsColumnMapping = [
                { name: 'ID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'IPAddress', mapping: 2 },
                { name: 'Status', mapping: 3 },
                { name: 'InstanceType', mapping: 4 },
                { name: 'PlatformID', mapping: 5 },
                { name: 'ChildsCount', mapping: 6},
                { name: 'Level', mapping: 7},
                { name: 'NodeID', mapping: 8},
                { name: 'PollingSource', mapping: 9}
            ];
    
    // grid paging    
    var pagingToolbar;
    var pageSizeBox;
    var pageSize = 40;
    var pagingToolbarFormat = "@{R=VIM.Strings;K=VIMWEBJS_JH0_2;E=js}";
    var noDataPagingToolbarText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_3;E=js}";

    var initialized;
    var loadingMask;
    
    // search
    var searchField;
    var searchPanel;
    var entityTypeCombo;

    var loadingText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_1;E=js}";
        
    // grouping
    var groupByCombo;
    var groupByStore;
    var groupByList;
    var useGrouping = false;

    RenderEntityName = function(value, meta, record) {
        var level = record.data.Level;
        var result = '';
        
         // each level means extra padding
        for (var i = 1; i < level; i++) {
            result += '<span class="tree-grid-indent-block" />';
        }        

        if (id != -1 && record.data.ChildsCount > 0) {
            result += String.format('<span><img src="/Orion/images/Pixel.gif" class="tree-grid-expander-expand" onclick="SW.VIM.BaselineThresholds.toggleRow(this, \'{0}\');"></span>', record.id);
        } else {
            result += '<span class="tree-grid-expander-dummy" />';
        }
              
        return String.format('{0}<span>{1}</span>', result, GetEntityLabel(record) );        
    };

    OpenManageNodeDialog = function(entityType, entityID, ipAddress) {
        if (entityType == swisVMEntity) {
            ORION.callWebService("/Orion/VIM/Services/BaselineThresholds.asmx", "GetVMParentEngineID", { vmID: entityID }, function(result) {
                ManageNodeDialog(result, ipAddress, 'G' + entityID, false);
            });
        } else if (entityType == swisHostEntity) {
            ORION.callWebService("/Orion/VIM/Services/BaselineThresholds.asmx", "GetHostManageNodeProperties", { hostID: entityID }, function(result) {                
                if (result.Rows != null && result.Rows.length > 0) {                    
                    ManageNodeDialog(result.Rows[0][0], ipAddress, 'H' + entityID, result.Rows[0][1].indexOf(";") != -1);    
                }                
            });
        }
    };

    GetEntityLabel = function(record) {
        var netObject = '';

        var link = '/Orion/View.aspx';
        var ipAddress = '';

        switch (record.data.InstanceType) {
            case swisVCenterEntity:
                netObject = 'N:' + record.data.NodeID;
                break;
            case swisDatacenterEntity:
                netObject = 'VMD:' + record.data.ID;
                break;
            case swisClusterEntity:
                netObject = 'VMC:' + record.data.ID;
                break;
            case swisDatastoreEntity:
                netObject = 'VMS:' + record.data.ID;
                break;
            case swisVMEntity:
                if (record.data.NodeID == null && record.data.PollingSource == pollingSourceVMan) {
                    netObject = 'VVM:' + record.data.ID;
                    link = '/Orion/VIM/VMDetails.aspx';    
                }                
                else if (record.data.NodeID != null) {
                    netObject = 'N:' + record.data.NodeID;
                } 
                else {
                    ipAddress = record.data.IPAddress == null ? record.data.Name : record.data.IPAddress;
                    return String.format('<span class="sw-vim-assetTree-unmanagedEntityLabel">' +
                        '<a href="javascript:void(0);" OnClick="SW.VIM.BaselineThresholds.openManageNodeDialog(\'{0}\',{1},\'{2}\')">{3} {4}</a></span>', swisVMEntity, record.data.ID, ipAddress, GetStatusIcon(record), record.data.Name);
                }                
                break;
            case swisHostEntity:
                if (record.data.NodeID == null && record.data.PollingSource == pollingSourceVMan) {
                    netObject = 'VH:' + record.data.ID;
                    link = '/Orion/VIM/HostDetails.aspx';    
                }                
                else if (record.data.NodeID != null) {
                    netObject = 'N:' + record.data.NodeID;
                } 
                else {                    
                    ipAddress = record.data.IPAddress == null ? record.data.Name : record.data.IPAddress;
                    return String.format('<span class="sw-vim-assetTree-unmanagedEntityLabel">' +
                        '<a href="javascript:void(0);" OnClick="SW.VIM.BaselineThresholds.openManageNodeDialog(\'{0}\',{1},\'{2}\')">{3} {4}</a></span>', swisHostEntity, record.data.ID, ipAddress, GetStatusIcon(record), record.data.Name);                    
                }                
                break;
        }        

        return String.format('<a href="{0}?NetObject={1}">{2} {3}</a>', link, netObject, GetStatusIcon(record), record.data.Name);
    };
    
    // marks all expander icons with the right class and fills expandedItems array with expanded records ids
    UpdateExpandStates = function() {
        var clollapsedExpanders = $("img:visible[class=tree-grid-expander-collapse]").removeClass('tree-grid-expander-collapse').addClass('tree-grid-expander-expand');
        var previousRecord = null;
        var expanders = $("img:visible[class=tree-grid-expander-expand]");
        var expanderIndex = -1;

        grid.store.data.each(function(record, index, length) {
            if (record != null && record.data.ChildsCount > 0) {
                delete expandedItems[GetRecordFullID(record)];
            }

            if (previousRecord != null && previousRecord.data.Level < record.data.Level) {
                expandedItems[GetRecordFullID(previousRecord)] = true;
                expanders[expanderIndex].className = 'tree-grid-expander-collapse';
            }

            previousRecord = record;

            if (record.data.ChildsCount > 0) {
                expanderIndex++;
            }
        });
    };

    GetStatusIcon = function(record) {
        var entityName;

        switch (record.data.InstanceType) {            
            case 'Orion.VIM.VirtualMachines':
            case 'Orion.VIM.Hosts':
                entityName = String.format('{0}.{1}', record.data.InstanceType, ToPlatformName(record.data.PlatformID));
                break;
            default:
                entityName = record.data.InstanceType;
                break;                
        }

        return String.format('<img src="/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}" />', entityName, record.data.Status);        
    };

    ToPlatformName = function(platformId) {
        switch (platformId) {
            case 1:
                return 'Vmware';
            case 2:
                return 'HyperV';
            default:
                return '';
        }
    };

    UpdateToolbarButtons = function() {        
        var selItems = grid.getSelectionModel().getSelections();
        var map = grid.getTopToolbar().items.map;
        var instanceType;
        var editEnabled = false;

        if (selItems.length > 0)
            instanceType = selItems[0].data.InstanceType;

        if (instanceType != swisDatacenterEntity && instanceType != swisVCenterEntity) {
            for (var i = 0; i < selItems.length; i++) {
                if (instanceType != selItems[i].data.InstanceType) {
                    editEnabled = false;
                    break;
                } else {
                    editEnabled = true;
                }
            }
        }
            
        map.EditThresholdsBtn.setDisabled(!editEnabled);
    };

    ToVimEntityType = function(swisInstanceType) {
        switch (swisInstanceType) {
            case swisVCenterEntity:
                return "VCenter";
            case swisClusterEntity:
                return "Cluster";
            case swisHostEntity:
                return "Host";
            case swisDatastoreEntity:
                return "DataStore";
            case swisVMEntity:
                return "VirtualMachine";
            default:
                return null;
        }
    };

    EditSelectedEntities = function() {        
        var selItems = grid.getSelectionModel().getSelections();
        var entityIds = [];
        
        for (var i = 0; i < selItems.length; i++) {
            entityIds.push(selItems[i].data.ID);
        }
        
        if (entityIds.length < 1)
            return;

        ORION.callWebService(
            "/Orion/VIM/Services/BaselineThresholds.asmx",
            "SetEntityIdsToEdit",
            { entityIds: entityIds, entityType: ToVimEntityType(selItems[0].data.InstanceType) },
            function(result) {
                RedirectToEditPropertiesPage();
            },
            function(fault) {
                ShowError("@{R=VIM.Strings;K=VIMWEBJS_JH0_14;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_LV0_3;E=js}");
            }
        );        
    };

    RedirectToEditPropertiesPage = function () {
        window.location.href = "/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?&ReturnTo=QmFzZWxpbmVUaHJlc2hvbGRzLmFzcHg=";
    };
   
    InitSearchPanel = function() {
        var entities = [
            [swisVCenterEntity, "@{R=VIM.Strings;K=VIMXMLDATA_VB1_22;E=js}"],
            [swisClusterEntity, "@{R=VIM.Strings;K=VIMXMLDATA_VB1_6;E=js}"], 
            [swisHostEntity, "@{R=VIM.Strings;K=VIMXMLDATA_VB1_11;E=js}"],
            [swisVMEntity, "@{R=VIM.Strings;K=VIMXMLDATA_VB1_25;E=js}"]
        ];

        if ($("#VManIntegrationEnabled").val().toLowerCase() == 'true') {
            entities.push([swisDatastoreEntity, "@{R=VIM.Strings;K=VIMXMLDATA_JD0_3;E=js}"]);
        }

        entityTypeCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: entities
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            region: 'center',
            editable: false,
            width: 150,
            listeners: {
                select: function(record, index) {
                    RefreshGroupByList(groupByCombo.getValue());
                    RefreshGrid();
                }
            }
        });
        
        entityTypeCombo.setValue(swisHostEntity);

        var label = new Ext.form.Label({
            text: "@{R=VIM.Strings;K=Web_Thresholds_ShowEntityTypeLabel;E=js}"
        });

        var button = new Ext.Button({
            text: "@{R=VIM.Strings;K=CommonButtonType_Search;E=js}"
        });

        button.on("click", function() {
            useGrouping = false;
            RefreshGrid();
        });

        searchField = new Ext.form.TextField({
            width: 300
        });

        searchField.on("specialkey", function (el,e) {
            if (e.keyCode == e.ENTER) {
                useGrouping = false;
                RefreshGrid();
            }                
        });

        searchPanel = new Ext.Container({
            applyTo: 'thresholdsSearchPanel',                      
            items: [ label, entityTypeCombo, searchField, button ],
            layout: 'hbox',
            layoutConfig: {
                padding: 10,
                pack: 'center',
                align: 'middle'
            },
            defaults: {
                margins:'0 5 0 0'
            }
        });        
    };
    
    RefreshGroupByList = function(groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory, entityType: entityTypeCombo.getValue() };
        if (handler) {
            var internalHandler = function() {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    };
    
    InitGroupingPanel = function () {
        var label = new Ext.form.Label({
            text: "@{R=VIM.Strings;K=VIMWEBJS_TK0_6;E=js}"
        });

        groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: [
                    ['none', "@{R=VIM.Strings;K=VIMWEBJS_JH0_6;E=js}"],
                    ['vendor', "@{R=VIM.Strings;K=VIMWEBJS_JH0_7;E=js}"]
                ]
            }),
            valueField: 'itemValue',            
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            width: 150,
            margins: '2 0 0 0',
            listeners: {
                select: function(record, index) {
                    RefreshGroupByList(record.value);

                    if (record.value == 'none') {
                        useGrouping = false;
                        RefreshGrid();
                    }                    
                }
            }
        });

        groupByCombo.setValue('none');        
                
        var selectPanel = new Ext.Container({            
            region: 'north',
            height: 51,    
            layout: 'vbox',
            cls: 'sw-vim-form-groupByPanel',
            items: [label, groupByCombo]
        });

        groupByStore = new ORION.WebServiceStore("/Orion/VIM/Services/BaselineThresholds.asmx/GetEntitiesFacets",
            [
                { name: "Value", mapping: 0 },                
                { name: "ValueCount", mapping: 1 }                
            ], "Value");
        
        var template = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="sw-vim-form-groupByItem">',
                '<tpl if="IsVendorGrouping() == true">',
                    '<tpl if="Value == 0"><img src="/Orion/VIM/images/Vendors/unknown.gif" /></tpl>',
                    '<tpl if="Value == 1"><img src="/Orion/VIM/images/Vendors/vmware.gif" /></tpl>',
                    '<tpl if="Value == 2"><img src="/Orion/VIM/images/Vendors/hyperv.gif" /></tpl>',                    
                    ' {[RenderVendorFacetLabel(values.Value, values.ValueCount)]}',
                '</tpl>',                
                '</div>',
            '</tpl>',
            '<div class="x-clear"></div>',
            {
                RenderVendorFacetLabel: function() {
                    return 'test';
                }
            }
        );

        groupByList = new Ext.DataView({
            border: true,
            store: groupByStore,
            singleSelect: true,
            itemSelector: "div.sw-vim-form-groupByItem",
            tpl: template,                                                                       
            autoHeight: false,
            autoScroll: true,            
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",            
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0) {
                        useGrouping = true;
                        RefreshGrid();
                    }
                }
            }
        });
        
        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 200,
            split: true,
            items: [
                selectPanel,
                groupByList
            ]
        });                              
        
        groupingPanel.on('bodyresize', function () {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    IsVendorGrouping = function() {
        return groupByCombo.getValue() == 'vendor';
    };

    RenderVendorFacetLabel = function(platform, count) {        
        var name;

        switch (platform) {
            case 1:
                name = "VMware";
                break;
            case 2:
                name = "Hyper-V";
                break;
            default:
                name = "Unknown";
                break;
        }

        return String.format('{0} ({1})', name, count);
    };

    ShowLoadingMask = function() {
        if (!grid.el.isMasked()) {
            loadingMask.show();
        }
    };    

    RefreshGrid = function() {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingText
            });
        }                

        grid.store.on('beforeload', function() {
            ShowLoadingMask();
        });
        
        var records = groupByList.getSelectedRecords();        
        var groupByItem = '';

        if (records.length > 0)
            groupByItem = records[0].data.Value;        
        
        grid.store.proxy.conn.jsonData = {
            entityType: entityTypeCombo.getValue(),
            groupByCategory: useGrouping ? groupByCombo.getValue() : '',
            groupByValue: useGrouping ? groupByItem : ''
        };
        
        grid.store.baseParams = { start: 0 * pageSize, limit: pageSize, search: searchField.getValue() };

        grid.store.on('load', function() {
            loadingMask.hide();
        });

        grid.store.on('exception', function() {
            ShowError("@{R=VIM.Strings;K=VIMWEBJS_LV0_2;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_LV0_1;E=js}");
            loadingMask.hide();
        });

        grid.store.load();
    };

    InitGrid = function () {
        var selectorModel = new Ext.grid.CheckboxSelectionModel({
            renderer: function(v, p, record) {
                if (record.data.InstanceType == swisDatacenterEntity || record.data.InstanceType == swisVCenterEntity)
                    return '';
                else
                    return '<div class="x-grid3-row-checker">&nbsp;</div>';
            },
            
            selectAll: function() {
                var store = this.grid.getStore();

                var recordsToSelect = new Array();


                store.each(function(record) {
                    if (record.data.InstanceType != swisDatacenterEntity && record.data.InstanceType != swisVCenterEntity) {
                        recordsToSelect.push(record);
                    }
                });
                this.selectRecords(recordsToSelect);
            }
        });

        selectorModel.on("selectionchange", UpdateToolbarButtons);

        dataStore = new ORION.WebServiceStore(
            "/Orion/VIM/Services/BaselineThresholds.asmx/GetEntities",
            dsColumnMapping,
            "Name");

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: pageSize
        });

        pageSizeBox.on('change', function (f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                ShowError("@{R=VIM.Strings;K=VIMWEBJS_JH0_14;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_15;E=js}");
                return;
            }

            if (pagingToolbar.pageSize != pSize) {
                pagingToolbar.pageSize = pSize;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }
        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: pagingToolbarFormat,
            emptyMsg: noDataPagingToolbarText,
            items: [
                '-',
                '@{R=VIM.Strings;K=VIMWEBJS_JH0_16;E=js}',
                pageSizeBox
            ]
        });

        grid = new Ext.grid.GridPanel({
            store: dataStore,
            columns: [
                selectorModel,
                { header: "@{R=VIM.Strings;K=VIMWEBJS_RB0_1;E=js}", width: 40, hidden: true, hideable: false, sortable: false, dataIndex: 'Id' },
                { header: "@{R=VIM.Strings;K=VIMWEBJS_TK0_4;E=js}", width: 450, hideable: false, sortable: true, dataIndex: 'Name', renderer: RenderEntityName },
                { header: "@{R=VIM.Strings;K=VIMWEBJS_TK0_5;E=js}", width: 300, hideable: true, sortable: true, dataIndex: 'IPAddress' },
                { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
            ],
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            tbar: [
                {
                    id: 'EditThresholdsBtn',
                    text: "@{R=VIM.Strings;K=VIMWEBJS_LV0_4;E=js}",
                    iconCls: 'sw-vim-baselineThresholds-editPropertiesButton',
                    handler: function () {
                        EditSelectedEntities();
                    }
                }],
            autoExpandColumn: "filler",
            bbar: pagingToolbar,
            listeners: {
                rowmousedown: function(grid, index, e) {
                    if (e.target.className == "tree-grid-expander-expand"
                        || e.target.className == "tree-grid-expander-collapse") {
                        selectorModel.lock();
                    }
                }
            }
        });
    };

    ToggleRow = function(expander, recordId) {
        grid.getSelectionModel().unlock();
        var item = dataStore.getById(recordId);
        var itemID = GetRecordFullID(item);

        if (expandedItems[itemID]) {
            CollapseRow(expander, recordId);
        } else {
            ShowLoadingMask();
            ExpandRow(expander, recordId);
        }
    };

    GetRecordFullID = function(record) {
        return String.format("{0}-{1}", record.data.ID, record.data.InstanceType);
    };
    
     // removes subtree
    CollapseRow = function (expander, recordId) {
        var rowRecord = dataStore.getById(recordId);
        var index = dataStore.indexOfId(recordId);
        var limit = dataStore.getCount();
        var toRemove = new Array();

        // remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = dataStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        dataStore.remove(toRemove);

        delete expandedItems[GetRecordFullID(rowRecord)];
        expander.className = 'tree-grid-expander-expand';
    };
    
    GetLoadingRecord = function (level) {
        var blankRecord = Ext.data.Record.create(dataStore.fields);
        var loadingRecord = new blankRecord({
            ID: -1,            
            Name: 'Loading...'
        });
        return loadingRecord;
    }
    
    // loads subtree and inserts under expanded row
    ExpandRow = function (expander, recordId, callback, items) {
        var rowRecord = dataStore.getById(recordId);
        var insertIndex = dataStore.indexOfId(recordId) + 1;

        dataStore.insert(insertIndex, GetLoadingRecord(parseInt(rowRecord.data.Level)));

        var dalMethod;
        var args;

        switch (rowRecord.data.InstanceType) {
            case swisVCenterEntity:
                dalMethod = "GetVCenterChildNodes";
                args = { vCenterID: rowRecord.data.ID, parentLevel: 1 };
                break;
            case swisDatacenterEntity:
                dalMethod = "GetDatacenterChildNodes";
                args = { dataCenterID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            case swisClusterEntity:
                dalMethod = "GetClusterChildNodes";
                args = { clusterID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            case swisHostEntity:
                dalMethod = "GetHostChildNodes";
                args = { hostID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            default:
                loadingMask.hide();
                return; // unknown type of entity, don't know what dal method use to get childs
        }

        var store = new ORION.WebServiceStore(String.format("/Orion/VIM/Services/BaselineThresholds.asmx/{0}", dalMethod), dsColumnMapping, "Name");
        
        store.proxy.conn.jsonData = args;
        store.addListener("load", function (store) {
            
            expandedItems[recordId] = true;
            expander.className = 'tree-grid-expander-collapse';

            var toAdd = new Array();

            store.each(function (record) { toAdd.push(record); });

            dataStore.removeAt(insertIndex); // remove "loading" record

            if (toAdd.length > 0) { dataStore.insert(insertIndex, toAdd); }
            UpdateExpandStates();
            loadingMask.hide();
        });

        store.load();
    };      
    
    ShowError = function(title, message) {
        Ext.Msg.alert(title, message).setIcon(Ext.Msg.ERROR);        
    };

    InitLayout = function () {
         var wrapper = new Ext.Container({
            renderTo: 'thresholdsMainPanel',
            layout: 'border',
            items: [groupingPanel, grid],
            height: 500
        });                
        
        Ext.EventManager.onWindowResize(function () {            
            wrapper.doLayout();
        }, wrapper);        
    };    

    return {
        init: function () {
            if (initialized)
                return;

            initialized = true;

            InitSearchPanel();
            InitGroupingPanel();
            InitGrid();
            InitLayout();
            RefreshGrid();
            UpdateToolbarButtons();
        },
        toggleRow: ToggleRow,
        openManageNodeDialog: OpenManageNodeDialog
    };
} ();

Ext.onReady(SW.VIM.BaselineThresholds.init, SW.VIM.BaselineThresholds);
