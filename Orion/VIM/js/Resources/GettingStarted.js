﻿SW = SW || {};
SW.VIM = SW.VIM || {};

SW.VIM.GettingStarted = function() {
    var _steps;
    var _workflowModeMigration = 2;
    this.init = function (steps) {
        _steps = steps;
        steps.forEach(setVisited);
    };

    this.OnStepButtonClicked = function(stepId) {
        RedirectToPage(stepId);
        return false;
    };

    function setVisited(element) {
        var elementId = "#" + element + "_RowData";
        $(elementId).addClass("vim-rowdata-visited");
    }

    function CheckStep(stepId, url) {
        SW.Core.Services.callController(String.format("/api/GettingStarted/CheckStep?stepId={0}", stepId), _steps, function (success) {
            window.location.href = url;
        }, function (error) { });
    }

    function RedirectToPage(stepId) {
        if (stepId === "ManageRecommendations") {
             CheckStep(stepId, "/ui/recommendations/current");
        } else if (stepId === "ManageStrategies") {
            CheckStep(stepId, "/ui/recommendations/settings/strategies");
        } else if (stepId === "PollingSettings") {
            window.location.href = "/Orion/VIM/Admin/VMwareServers.aspx";
        } else if (stepId === "LicenseManager") {
            window.location.href = "/ui/licensing/license-manager";
        }
        else if (stepId === "CapacityPlanning_CreateReport") {
            window.location.href = "/ui/vim/capacity-planning/report-wizard/";
        } else if (stepId === "CapacityPlanning_ViewReports") {
            CheckStep(stepId, "/ui/vim/capacity-planning/");
        }
    }

    SW.VIM.GettingStarted.onExpanderChanged = function (element) {
        var rowId = ($(element).attr("data-rowId") || $(element).attr("id"))

        var drawer = $("#Drawer_" + rowId);
        var expanderImg = $(element).find("img:first")
        var expanderLabel = $(element).find(".vim-gettingstarted-label").first();

        var collapsed = !drawer.is(":visible");
        if (collapsed) {
            drawer.slideDown();
            expanderImg.attr("src", "/Orion/VIM/images/GettingStarted/arrow-expanded.png");
            expanderImg.attr("alt", "[-]");
            expanderLabel.addClass("vim-gettingstarted-label-expanded");
        } else {
            drawer.slideUp();
            expanderImg.attr("src", "/Orion/VIM/images/GettingStarted/arrow-collapsed.png");
            expanderImg.attr("alt", "[+]");
            expanderLabel.removeClass("vim-gettingstarted-label-expanded");
        }
    };
};