﻿SW.Core.namespace("SW.VIM.Summary.Recommendations").SummaryRecommendationsController = function(config) {
    "use strict";
    
    this.formatRecommendations = function (value, row, cellInfo) {
        var label = value === 1 ? "@{R=VIM.Strings;K=VIMWEBJS_TK0_8;E=js}" : "@{R=VIM.Strings;K=VIMWEBJS_TK0_9;E=js}";
        return String.format("{0} {1}", value, label);        
    }

   this.formatName = function (value, row, cellInfo) {
        return value;
    }

    this.formatRisksCount = function (value, row, cellInfo) {
        var parts = value.split(":");
        var retval = "";
        if (parts[0] != "0") {
            retval = retval + '<img src="/Orion/images/StatusIcons/Small-Critical.gif" style="position: inherit" /> <span style="color:red;position:relative;bottom:3px"><b>' + parts[0] + '</b></span>&nbsp;&nbsp;&nbsp;';
        }
        if (parts[1] != "0") {
            retval = retval + '<img src="/Orion/images/StatusIcons/Small-Warning.gif" style="position: inherit" /> <span style="position:relative;bottom:3px"><b>' + parts[1] + '</b></span>&nbsp;&nbsp;&nbsp;';
        }
        if (parts[2] != "0") {
            retval = retval + '<img src="/Orion/images/Info_Icon_MoreDecent_16x16.png" style="position: inherit" /> <span style="position:relative;bottom:3px"><b>' + parts[2] + '</b></span>&nbsp;&nbsp;&nbsp;';
        }
        return retval;
    }

    this.onLoad = function (rowsToOutput, columnInfo) {
        var overallCount = 0;
        
        if (rowsToOutput.length > 0)
            overallCount = rowsToOutput[0][12];

        var $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=" + config.resourceId + "] h1:first");
        if ($resourceWrapperTitle.find("span").length === 0) {
            $resourceWrapperTitle.append(SW.Core.String.Format("<span>" + config.countFormat + "</span>", overallCount));
        } else {
            $resourceWrapperTitle.find("span").text(SW.Core.String.Format(config.countFormat, overallCount));
        }
    }
}