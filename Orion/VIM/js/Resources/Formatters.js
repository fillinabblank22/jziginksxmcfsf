﻿SW = SW || {};
SW.VIM = SW.VIM || {};

SW.VIM.Formatters = function () { }

SW.VIM.Formatters.bytesFormatter = function (bytesValue) {
    if (bytesValue >= 1024 * 1024 * 1024) {
        return (bytesValue / (1024 * 1024 * 1024)).toFixed(2) + " @{R=VIM.Strings;K=WebResourceFormatter_GB_Units;E=js}";
    } else if (bytesValue >= 1024 * 1024) {
        return (bytesValue / (1024 * 1024)).toFixed(2) + " @{R=VIM.Strings;K=WebResourceFormatter_MB_Units;E=js}";
    } else {
        return (bytesValue / 1024).toFixed(2) + " @{R=VIM.Strings;K=WebResourceFormatter_KB_Units;E=js}";
    }
}

SW.VIM.Formatters.minutesFormatter = function (minutesDiff) {
    var days = Math.floor(minutesDiff / (60 * 24));
    minutesDiff -= days * (60 * 24);

    var hours = Math.floor(minutesDiff / 60);
    minutesDiff -= hours * 60;

    var minutes = Math.floor(minutesDiff);

    var result = "";
    if (days > 0) {
        result += days + "@{R=VIM.Strings;K=WebResourceFormatter_Day_Units;E=js} ";
    }
    if (result.length > 0) {
        result += hours + "@{R=VIM.Strings;K=WebResourceFormatter_Hour_Units;E=js} ";
    } else {
        if (hours > 0) {
            result += hours + "@{R=VIM.Strings;K=WebResourceFormatter_Hour_Units;E=js} ";
        }
    }
    if (minutes > 0) {
        result += minutes + "@{R=VIM.Strings;K=WebResourceFormatter_Minute_Units;E=js}";
    }
    return result;
}
