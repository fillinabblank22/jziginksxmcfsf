Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.VMwareCredentials = function () {

    var updatePasswordChanged = false;

    ORION.prefix = "VIM_VMwareCredentials_";

    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

        var editButtonDisabled = (count != 1);
        if (!editButtonDisabled) {
            selItems.each(function (rec) {
                if (rec.data["Username"] == "<unknown>")
                    editButtonDisabled = true;
            });
        };

        map.EditButton.setDisabled(editButtonDisabled);
        map.DeleteButton.setDisabled(count === 0);
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // Column rendering functions
    function renderCredential(value, meta, record) {
        return encodeHTML(value);
    };

    // Column rendering functions
    function renderUserName(value, meta, record) {
        var userName = encodeHTML(value);
        if (value == "<unknown>")
            userName = "<i>" + userName + "</i>";
        return userName;
    };

    function checkCredentials(description, username, password, confirm, checkPassword) {
        if ($.trim(String(description)) == '') {
            return "@{R=VIM.Strings;K=VIMWEBJS_AK0_1;E=js}";
        }

        if ($.trim(String(username)) == '') {
            return "@{R=VIM.Strings;K=VIMWEBJS_AK0_2;E=js}";
        }

        if (checkPassword) {
            if (!(password === confirm) || $.trim(String(password)) == '') {
                return "@{R=VIM.Strings;K=VIMWEBJS_VB0_11;E=js}";
            }
        }

        return "";
    };

    function setErrorMessage(message, updateDialog) {
        var icon = 'bad_credentials.gif';
        var color = '#ce0000';
        var background = '#facece';
        var text = '@{R=VIM.Strings;K=VIMWEBJS_AK0_4;E=js}';

        var img = $(String.format('<img src="/Orion/VIM/images/VMwarePollingStatus/{0}" />', icon)).css('padding', '2px 5px 2px 2px').css('vertical-align', 'middle');
        var table = $('<table>').append($('<tr>').append($('<td>').css('padding', '0px').css('vertical-align', 'middle').append(img).append(message)));
        var div = $('<div>').css('background-color', background).css('color', color).css('font-weight', 'bold').append(table);

        if (updateDialog) {
            $("#updateDialogBody .Error").empty();
            $("#updateDialogBody .Error").append(div);
        } else {
            $("#addDialogBody .Error").empty();
            $("#addDialogBody .Error").append(div);
        }
    };

    function enableButtons(dialog) {
        dialog.buttons[0].enable();   // ok button
        dialog.buttons[1].enable();   // cancel button
    };

    function disableButtons(dialog) {
        dialog.buttons[0].disable();  // ok button
        dialog.buttons[1].disable();  // cancel button  
    };

    function addCredential() {
        $("#addDialogBody input").val('');
        $("#addDialogBody .Error").text('');

        if (!addDialog) {
            addDialog = new Ext.Window({
                applyTo: 'addDialog',
                layout: 'fit',
                width: 370,
                height: 'auto',
                modal: true,
                closeAction: 'hide',
                plain: true,
                resizable: false,

                items: new Ext.BoxComponent({
                    applyTo: 'addDialogBody'
                }),
                buttons: [
                {
                    text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}',
                    handler: function () {
                        var description = $("#addDescription").val();
                        var username = $("#addUsername").val();
                        var password = $("#addPassword").val();
                        var confirm = $("#addConfirm").val();

                        var error = checkCredentials(description, username, password, confirm, true);
                        if (error != '') {
                            $("#addDialogBody .Error").text(error);
                            return;
                        }

                        disableButtons(addDialog);
                        ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "CheckCredentialName", { credentialName: description }, function (result) {
                            enableButtons(addDialog);

                            // credential name exists in db    
                            if (result == false) {
                                setErrorMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', false);
                                return;
                            }

                            ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "AddVMwareCredential", { description: description, username: username, password: password }, function (result) { grid.getStore().reload(); });
                            addDialog.hide();
                        });
                    }
                },
                {
                    text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_13;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        addDialog.hide();
                    }
                }]
            });
        }

        // Set the location
        addDialog.alignTo(document.body, "c-c");
        addDialog.show();

        return false;
    };

    function updateCredential() {
        var rec;
        grid.getSelectionModel().each(function (record) {
            rec = record;
        });

        $("#updateID").val(rec.data["CredentialID"]);
        $("#updateDescription").val(rec.data["Description"]);
        $("#updateUsername").val(rec.data["Username"]);
        $("#updatePassword").val('------');
        $("#updateConfirm").val('------');
        $("#updateDialogBody .Error").text('');
        $("#updatePassword").focus(
                            function () {
                                $(this).val('');
                            });
        $("#updatePassword").blur(
                            function () {
                                if (!updatePasswordChanged) {
                                    $(this).val('------');
                                }
                            });
        $("#updatePassword").change(
                            function () {
                                updatePasswordChanged = true;
                                $("#updateConfirm").val('');
                            });

        updatePasswordChanged = false;

        if (!updateDialog) {
            updateDialog = new Ext.Window({
                applyTo: 'updateDialog',
                layout: 'fit',
                width: 370,
                height: 'auto',
                closeAction: 'hide',
                plain: true,
                modal: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'updateDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                {
                    text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_12;E=js}',
                    handler: function () {
                        var id = $("#updateID").val();
                        var description = $("#updateDescription").val();
                        var username = $("#updateUsername").val();
                        var password = $("#updatePassword").val();
                        var confirmed = $("#updateConfirm").val();

                        var error = checkCredentials(description, username, password, confirmed, updatePasswordChanged);
                        if (error != '') {
                            $("#updateDialogBody .Error").text(error);
                            return;
                        }

                        disableButtons(updateDialog);

                        // detect if there are any profiles using edited credentials
                        ORION.callWebService("/Orion/Services/CredentialManagerService.asmx", "GetNumberOfDiscoveryProfilesUsingCredentials",
                        {
                            credentialId: id
                        },
                        function (result)
                        {
                            if (result > 0)
                            {
                                if (!confirm(String.format("@{R=VIM.Strings;K=VIMWEBJS_VB0_13;E=js}",result))) 
                                {
                                    enableButtons(updateDialog);                                                                                                    
                                    return;
                                }
                            }

                            ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "CheckCredentialName", { credentialName: description }, function (result) {
                                enableButtons(updateDialog);

                                // credential name exists in db    
                                if (result == false && description != oldName) {
                                    setErrorMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', true);
                                    return;
                                }

                                ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "UpdateVMwareCredential", { credentialID: id, description: description, username: username, password: password, passwordChanged: updatePasswordChanged }, function (result) { grid.getStore().reload(); });
                                updateDialog.hide();
                            });
                        });
                    }
                },
                {
                    text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_13;E=js}',
                    style: 'margin-left: 5px;',
                    handler: function () {
                        updateDialog.hide();
                    }
                }]
            });
            //            updateDialog.on('show', function () {
            //            });
        }


        // Set the location
        updateDialog.alignTo(document.body, "c-c");
        updateDialog.show();

        oldName = $("#updateDescription").val();

        return false;
    };


    function delCredential() {
        var names = [];
        grid.getSelectionModel().each(function (rec) {
            names.push(rec.data["Description"]);
        });

        var layout = $("#delDialogDescription");

        if (names.length > 5) {
            layout.text(String.format('@{R=VIM.Strings;K=VIMWEBJS_VB0_14;E=js}', names.length));
            $("#credentials").empty();
        }
        else {
            layout.text('@{R=VIM.Strings;K=VIMWEBJS_VB0_15;E=js}');
            var credentials = $("#credentials");
            credentials.empty();
            $(names).each(function () {
                var name = encodeHTML((new String(this)).substring(0, 32));
                $(String.format('<li>{0}</li>', name)).appendTo(credentials);
            });
        }

        if (!delDialog) {
            delDialog = new Ext.Window({
                applyTo: 'delDialog',
                layout: 'fit',
                width: 310,
                height: 'auto',
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'delDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
        {
            text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_16;E=js}',
            handler: function () {
                var ids = [];
                grid.getSelectionModel().each(function (rec) {
                    ids.push(rec.data["CredentialID"]);
                });

                ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "DeleteVMwareCredentials", { credentialIDs: ids }, function (result) {
                    grid.getStore().reload();
                });
                delDialog.hide();
            }
        },
        {
            text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_13;E=js}',
            style: 'margin-left: 5px;',
            handler: function () {
                delDialog.hide();
            }
        }]
            });
        }

        // Set the location
        delDialog.alignTo(document.body, "c-c");
        delDialog.show();

        return false;
    };

    var selectorModel;
    var dataStore;
    var grid;
    var oldName;
    var addDialog;
    var updateDialog;
    var delDialog;
    var pageSizeNum;

    return {
        init: function () {
            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[10], [20], [30], [40], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_17;E=js}', style: 'margin-left: 5px; margin-right: 5px' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", UpdateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                "/Orion/VIM/Services/VMwareServers.asmx/GetVMwareCredentialsCorruptedIncluded",
                [
                { name: 'CredentialID', mapping: 0 },
                { name: 'Description', mapping: 1 },
                { name: 'Username', mapping: 2 }
                ],
                "Description");


            grid = new Ext.grid.GridPanel({

                store: dataStore,

                columns: [
                    selectorModel,
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_VB0_19;E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'CredentialID' },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_VB0_20;E=js}', width: 250, sortable: true, dataIndex: 'Description', renderer: renderCredential },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_VB0_21;E=js}', width: 250, sortable: true, dataIndex: 'Username', renderer: renderUserName }
                ],

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 610,
                height: 300,
                stripeRows: true,

                tbar: [
                {
                    id: 'AddButton',
                    text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_22;E=js}',
                    iconCls: 'add',
                    handler: function () {
                        if (VIMIsDemoMode()) return VIMDemoModeMessage();
                        addCredential();
                    }
                }, '-',
                {
                    id: 'EditButton',
                    text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_23;E=js}',
                    iconCls: 'edit',
                    handler: function () {
                        if (VIMIsDemoMode()) return VIMDemoModeMessage();
                        updateCredential();
                    }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: '@{R=VIM.Strings;K=VIMWEBJS_VB0_24;E=js}',
                    iconCls: 'del',
                    handler: function () {
                        if (VIMIsDemoMode()) return VIMDemoModeMessage();
                        delCredential();
                    }
                }],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=VIM.Strings;K=VIMWEBJS_VB0_25;E=js}',
                    emptyMsg: "@{R=VIM.Strings;K=VIMWEBJS_VB0_26;E=js}",
                    beforePageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_27;E=js}",
                    afterPageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_28;E=js}",
                    firstText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_29;E=js}",
                    prevText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_30;E=js}",
                    nextText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_31;E=js}",
                    lastText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_32;E=js}",
                    refreshText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_33;E=js}"
                })

            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            grid.render('Grid');
            UpdateToolbarButtons();
            grid.bottomToolbar.addPageSizer();
            grid.setWidth($('#gridCell').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();


            $("form").submit(function () { return false; });
        }
    };

} ();


    Ext.onReady(SW.VIM.VMwareCredentials.init, SW.VIM.VMwareCredentials);




