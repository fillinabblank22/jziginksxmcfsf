﻿/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/js/OrionCore.js" />
Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.HyperVServers = function () {

    HostIconEntity = "Orion.VIM.Hosts.HyperV";

    loadingMask = null;
    expandedItems = [];

    columnMapping = [
                            { name: 'ID', mapping: 0 },
                            { name: 'NodeID', mapping: 1 },
                            { name: 'IPAddress', mapping: 2 },
                            { name: 'Name', mapping: 3 },
                            { name: 'Status', mapping: 4 },
                            { name: 'HostStatus', mapping: 5 },
                            { name: 'ProductName', mapping: 6 },
                            { name: 'ProductVersion', mapping: 7 },
                            { name: 'CredentialName', mapping: 8 },
                            { name: 'EngineID', mapping: 9 }, 
                            { name: 'PollingSource', mapping: 10 },
                            { name: 'ClusterName', mapping: 11 }
                     ];

    ORION.prefix = "VIM_HyperVServers_";

    ORION.handleError = function (xhr) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=VIM.Strings;K=VIMWEBJS_VB0_9;E=js}');
            window.location.reload();
        }

        var errorText = xhr.responseText;
        try {
            var error = eval("(" + errorText + ")");

            if (grid != null) {
                nodeInProgressIds = [];
                grid.getView().refresh();
            }
            if (loadingMask != null) {
                loadingMask.hide();
            }
            Ext.Msg.show({
                title: "@{R=VIM.Strings;K=VIMWEBJS_TM0_1;E=js}",
                msg: error.Message,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                minWidth: 596
            });
            $("#test").text(error.Message);
            $("#stackTrace").text(error.StackTrace);
        }
        catch (err) {
            $("#test").text(err.description);
        }
    };

    PollingSourceChangeCallback = function (result) {
        var isLicenseProblem = result.licenseBased.length > 0;
        var isClusterProblem = result.clusterBased.length > 0;
        var warningMessages = [];

        if (isLicenseProblem || isClusterProblem) {
            if (isLicenseProblem) {
                var warningMessageLicenseProblem = ["@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_SwitchingErrorLicenseBased;E=js}"];
                warningMessageLicenseProblem += BuildList(result.licenseBased);
                warningMessages.push(warningMessageLicenseProblem);
            }

            if (isClusterProblem) {
                var warningMessageClusterProblem = ["@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_SwitchingErrorClusterBased;E=js}"];
                warningMessageClusterProblem += BuildList(result.clusterBased);
                warningMessages.push(warningMessageClusterProblem);
            }

            Ext.Msg.show({
                title: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_WarningTitle;E=js}",
                msg: warningMessages.join("<br>"),
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK,
                minWidth: 596
            });
        }

        grid.getStore().reload();
    };

    function BuildList(result) {
        var list;
        list = "<ul class=\"vim-bullet\">";
        for (var i = 0; i < result.length; i++) {
            var name = encodeHTML(result[i]);
            list += String.format("<li>{0}</li>", name);
        }

        list += "</ul>";
        return list;
    }

    RefreshObjects = function () {
        // create loading mask
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=VIM.Strings;K=VIMWEBJS_TM0_2;E=js}"
            });
        }

        // hide loading mask when load is complete
        grid.store.on('load', function () {
            loadingMask.hide();
            UpdateToolbarButtons();
        });
        grid.getView().on('refresh', function () { UpdateToolbarButtons(); });

        loadingMask.show();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load();

    };


    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var map = grid.getTopToolbar().items.map;

        var count = selItems.getCount();
        var anyEnabled = false;
        var anyDisabled = false;
        var isPollingSourceBasicButtonDisabled = true;
        var isPollingSourceVmanOrionButtonDisabled = true;
        var vimFullyLicensed;
        isVimFullyLicensed(function (result) { vimFullyLicensed = result; });

        if (count > 0) {
            grid.getSelectionModel().each(function (rec) {
                if (rec.data.HostStatus != 3) anyEnabled = true;
                if (rec.data.HostStatus == 3) anyDisabled = true;
                isPollingSourceBasicButtonDisabled = isPollingSourceBasicButtonDisabled && rec.data.PollingSource == 0;
                isPollingSourceVmanOrionButtonDisabled = isPollingSourceVmanOrionButtonDisabled && (rec.data.PollingSource == 2 || !vimFullyLicensed);
            });
        }

        map.EnableButton.setDisabled(anyEnabled || count == 0);
        map.DisableButton.setDisabled(anyDisabled || count == 0);
        map.EditPropertiesButton.setDisabled(count == 0);
        map.ListResourcesButton.setDisabled(count != 1);

        map.PollingSourceButton.setDisabled(count == 0 || (isPollingSourceBasicButtonDisabled && isPollingSourceVmanOrionButtonDisabled));
        map.PollingSourceButton.menu.items.map.PollingSourceBasicButton.setDisabled(isPollingSourceBasicButtonDisabled);
        map.PollingSourceButton.menu.items.map.PollingSourceVmanOrionButton.setDisabled(isPollingSourceVmanOrionButtonDisabled);
    };


    //////////////////////////////////////////////////////////////////////////////////////////////
    // Column rendering functions 
    //////////////////////////////////////////////////////////////////////////////////////////////
    function renderServer(value, meta, record) {
        return String.format('<span class="tree-grid-caption"><a href="/Orion/View.aspx?NetObject={0}" >{1} {2}</a></span>', "N:" + record.data.NodeID, getStatusIcon(record), value);
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    function renderText(value, meta, record) {
        return value == null ? '' : encodeHTML(value);
    };

    function renderStatus(value, meta, record) {
        // don't want to render polling status icon for loading record or record without host status
        if (record.data.ID == -1 || record.data.HostStatus == -1) {
            return '';
        }

        var iconPath = '/Orion/VIM/images/VMwarePollingStatus/'
        var status = (record.data.PollingMethod == 0 && value != 3 && value != 5)
            ? record.data.HostStatus
            : value;
        switch (status) {
            case 1: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'needs_credentials.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_3;E=js}');
            case 2: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'polling.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_5;E=js}');
            case 3: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'disabled.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_8;E=js}');
            case 4: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'bad_credentials.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_6;E=js}');
            case 5: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'disabled.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_7;E=js}');
            default: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'unknown.gif', '@{R=VIM.Strings;K=VIMLIBCODE_VB0_7;E=js}');
        }
    };

    function renderPollingSource(value, meta, record) {
        switch (record.data.PollingSource) {
        case 0: return '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}';
        case 1: return '@{R=VIM.Strings;K=PollingSource_VMan;E=js}';
        case 2:
            var vimFullyLicensed;
            isVimFullyLicensed(function (result) { vimFullyLicensed = result; });
            if (vimFullyLicensed) {
                return '@{R=VIM.Strings;K=PollingSource_LicenseBased;E=js}';
            } else {
                return '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}';
            }
        default: return '';
        }
    };

    function getStatusIcon(record) {
        if (record.data.ID == -1) {
            return '<img src="/Orion/images/AJAX-Loader.gif" />';
        }
        else {
            return String.format('<img src="/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}" />', HostIconEntity, record.data.Status);
        }
    };

    function updateVMwarePollingStatus(enable, onSuccess) {
        loadingMask.show();
        var nodeIds = getSelectedNodeIds(grid, false);
        ORION.callWebService("/Orion/VIM/Services/HyperVServers.asmx", "UpdateHyperVPollingStatus", { nodeIds: nodeIds, enable: enable }, onSuccess);
    };

    function updateNodePollingSource(pollingSource, onSuccess) {
        loadingMask.show();
        var nodeIds = getSelectedNodeIds(grid, false);
        var vimFullyLicensed;
        isVimFullyLicensed(function (result) { vimFullyLicensed = result; });
        if (!isBasicPollingOnly(grid, false) && pollingSource == 0 && vimFullyLicensed) {
            Ext.MessageBox.show({
                title: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_ConfirmSwitchDialogTitle;E=js}",
                msg: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_ConfirmSwitchDialog;E=js}",
                icon: Ext.Msg.QUESTION,
                buttons: Ext.MessageBox.YESNO,
                minWidth: 596,
                fn: function (btn) {
                    if (btn == "yes") {
                        ORION.callWebService("/Orion/VIM/Services/HyperVServers.asmx",
                            "UpdateNodePollingSource",
                            { nodeIds: nodeIds, pollingSource: pollingSource },
                            onSuccess);
                    } else {
                        grid.getStore().reload();
                    }
                }
            });
        } else {
            ORION.callWebService("/Orion/VIM/Services/HyperVServers.asmx",
                "UpdateNodePollingSource",
                { nodeIds: nodeIds, pollingSource: pollingSource },
                onSuccess);
        }
    };

    function isVimFullyLicensed(onSuccess) {
        SW.Core.Services.callWebServiceSync("/Orion/VIM/Services/HyperVServers.asmx", "IsVimFullyLicensed", {}, onSuccess, {});
    }

    function editProperties() {
        var nodeIds = getSelectedNodeIdsWithEngine(grid);
        var formId = "selectedNodes";
        $('body').append(preparePostFormForEditProperties(nodeIds, $('#VIM_HyperVServers_ReturnToUrl').val(), formId));
        $('#' + formId).submit();
    };

    function listResources() {
        window.location = getListResourcesUrl(getSelectedNodeIds(grid, false)[0], $('#VIM_HyperVServers_ReturnToUrl').val());
    };

    isBasicPollingOnly = function (grid, includeNulls) {
        var toReturn = true;
        grid.getSelectionModel().each(function (rec) {
            if (includeNulls || rec.data.PollingSource != null) {
                if (rec.data.PollingSource != 0) {
                    toReturn = false;
                }
            }
        });

        return toReturn;
    };

    getSelectedNodeIds = function (grid, includeNulls) {
        var nodeIds = [];
        grid.getSelectionModel().each(function (rec) {
            if (includeNulls || rec.data.NodeID != null) {
                nodeIds.push(rec.data.NodeID);
            }
        });
        return nodeIds;
    },

    getSelectedNodeIdsWithEngine = function (grid) {
        var nodeIds = [];
        grid.getSelectionModel().each(function (rec) {
            nodeIds.push("N:" + rec.data.NodeID + ":" + rec.data.EngineID);
        });
        return nodeIds;
    },

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Inspiration in /Orion/Nodes/js/NodeManagementPaging.js
    //////////////////////////////////////////////////////////////////////////////////////////////
    preparePostFormForEditProperties = function (ids, returnUrl, formId) {
        var editPropertiesUrl = String.format("/Orion/Nodes/NodeProperties.aspx?ReturnTo={0}", returnUrl);
        var stringForm = "<form id='{0}' action='{1}' method='POST'> \
                              <input type='hidden' name='Nodes' value='{2}'/> \
                              <input type='hidden' name='ReturnTo' value='{3}'/> \
                              <input type='hidden' name='GuidID' value='{4}'/> \
                          </form>";
        return String.format(stringForm, formId, editPropertiesUrl, ids.join(","), returnUrl, guidGenerator());
    },

    getListResourcesUrl = function (nodeId, returnUrl) {
        return String.format("/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}", nodeId, returnUrl);
    }

    function guidGenerator() {
        var guidPiece = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (guidPiece() + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + guidPiece() + guidPiece());
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Grid
    //////////////////////////////////////////////////////////////////////////////////////////////
    var selectorModel;
    var dataStore;
    var grid;
    var select;
    var pageSizeNum;

    var hiddenColumns;

    saveGridConfig = function () {
        hiddenColumns = [];
        for (var i = 0; i < grid.colModel.config.length; i++) {
            var column = grid.colModel.columns[i];
            if (column.hidden) {
                hiddenColumns.push(column.dataIndex);
            }
        }
        var hiddenColumnsString = (hiddenColumns.length > 0 ? ('[\'' + hiddenColumns.join('\',\'') + '\']') : '[]');
        ORION.Prefs.save('HiddenColumns', hiddenColumnsString);
    };

    isHiddenColumn = function (columnName) {
        for (var i = 0; i < hiddenColumns.length; i++) {
            if (hiddenColumns[i] == columnName) return true;
        }
        return false;
    };

    return {
        init: function () {
            hiddenColumns = eval(ORION.Prefs.load('HiddenColumns', '[\'PollingMethod\']'));

            Ext.override(Ext.PagingToolbar, {
                updateInfo: function () {
                    if (this.displayItem) {
                        var count = this.store.data.length;
                        var msg = count == 0 ?
                            this.emptyMsg :
                            String.format(
                                this.displayMsg,
                                this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                            );
                        this.displayItem.setText(msg);
                    }
                }
            });

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));

            selectorModel = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    selectionchange: UpdateToolbarButtons
                }
            });

            // create datastore for the grid
            // top level can contain either vCenter or standalone ESX
            dataStore = new ORION.WebServiceStore("/Orion/VIM/Services/HyperVServers.asmx/GetServers", columnMapping, "Name");

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                colModel: new Ext.grid.ColumnModel({
                    columns: [
                        selectorModel,
                        { id: 'ID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_LB0_5;E=js}', hideable: false, width: 250, sortable: false, dataIndex: 'Name', renderer: renderServer },
                        { header: '@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_ClusterName;E=js}', hidden: isHiddenColumn('ClusterName'), width: 250, sortable: false, dataIndex: 'ClusterName', renderer: renderText },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_LB0_6;E=js}', hidden: isHiddenColumn('CredentialName'), width: 180, sortable: false, dataIndex: 'CredentialName', renderer: renderText },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_LB0_7;E=js}', hidden: isHiddenColumn('HostStatus'), width: 145, sortable: false, dataIndex: 'HostStatus', renderer: renderStatus },
                        { header: '@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_PollingSource;E=js}', hidden: isHiddenColumn('PollingSource'), width: 140, sortable: false, dataIndex: 'PollingSource', renderer: renderPollingSource },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_LB0_8;E=js}', hidden: isHiddenColumn('ProductName'), width: 180, sortable: false, dataIndex: 'ProductName', renderer: renderText },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_LB0_9;E=js}', hidden: isHiddenColumn('ProductVersion'), width: 130, sortable: false, dataIndex: 'ProductVersion', renderer: renderText }
                    ],
                    listeners: {
                        hiddenchange: function (columnModel, columnIndex, hidden) {
                            saveGridConfig();
                        }
                    }
                }),

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 680,
                height: 360,
                stripeRows: true,

                listeners: {
                    rowmousedown: function (grid, index, e) {
                        if (e.target.className == "tree-grid-expander-expand"
                            || e.target.className == "tree-grid-expander-collapse") {
                            selectorModel.lock();
                        }
                    }
                },

                tbar:
                    [
                        {
                            id: 'EditPropertiesButton',
                            text: '@{R=VIM.Strings;K=VIMWEBJS_LB0_1;E=js}',
                            iconCls: 'editProperties',
                            handler: function () {
                                if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                editProperties();
                            }
                        }, '-',
                        {
                            id: 'ListResourcesButton',
                            text: '@{R=VIM.Strings;K=VIMWEBJS_LB0_2;E=js}',
                            iconCls: 'listResources',
                            handler: function () {
                                if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                listResources();
                            }
                        }, '-',
                        {
                            id: 'EnableButton',
                            text: '@{R=VIM.Strings;K=VIMWEBJS_LB0_3;E=js}',
                            iconCls: 'enablePolling',
                            handler: function () {
                                if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                updateVMwarePollingStatus(true, function (result) {
                                    var nodeIds = getSelectedNodeIds(grid, false);
                                    grid.getStore().reload();
                                    grid.getView().refresh();
                                });
                            }
                        }, '-',
                        {
                            id: 'DisableButton',
                            text: '@{R=VIM.Strings;K=VIMWEBJS_LB0_4;E=js}',
                            iconCls: 'disablePolling',
                            handler: function () {
                                if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                updateVMwarePollingStatus(false, function (result) {
                                    grid.getStore().reload();
                                });
                            }
                        }, '-',
                        {
                            id: 'PollingSourceButton',
                            text: '@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_PollingSource;E=js}',
                            iconCls: 'pollThrough',
                            menu: {
                                items: [
                                    {
                                        id: 'PollingSourceBasicButton',
                                        text:
                                            '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}',
                                        iconCls: '', //todo
                                        handler: function() {
                                            if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                            updateNodePollingSource(0, PollingSourceChangeCallback);
                                        }
                                    }, {
                                        id: 'PollingSourceVmanOrionButton',
                                        text:
                                            '@{R=VIM.Strings;K=PollingSource_LicenseBased;E=js}',
                                        iconCls: '', //todo
                                        handler: function() {
                                            if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                            updateNodePollingSource(2, PollingSourceChangeCallback);
                                        }
                                    }
                                ]
                            }
                        }
                    ],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=VIM.Strings;K=VIMWEBJS_TM0_27;E=js}',
                    emptyMsg: "@{R=VIM.Strings;K=VIMWEBJS_TM0_28;E=js}",
                    beforePageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_27;E=js}",
                    afterPageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_28;E=js}",
                    firstText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_29;E=js}",
                    prevText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_30;E=js}",
                    nextText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_31;E=js}",
                    lastText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_32;E=js}",
                    refreshText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_33;E=js}",
                    items: [
                        new Ext.form.Label({ text: '@{R=VIM.Strings;K=VIMWEBJS_TM0_30;E=js}', style: 'margin-left: 5px; margin-right: 5px' }),
                        new Ext.form.ComboBox({
                            regex: /^\d*$/,
                            store: new Ext.data.SimpleStore({
                                fields: ['pageSize'],
                                data: [[10], [20], [30], [40], [50]]
                            }),
                            displayField: 'pageSize',
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus: true,
                            width: 50,
                            editable: false,
                            value: pageSizeNum,
                            listeners: {
                                'select': function (c, record) {
                                    grid.bottomToolbar.pageSize = record.get("pageSize");
                                    grid.bottomToolbar.cursor = 0;
                                    ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                                    grid.bottomToolbar.doRefresh();
                                }
                            }
                        })
                    ]
                })

            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.render('Grid');

            var fudgeFactor = 12;
            var groupItemsHeight = $("#Grid").height() - $(".GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            // Set the width of the grid
            grid.setWidth($('#gridCell').width());

            RefreshObjects();
            UpdateToolbarButtons();


            $("form").submit(function () { return false; });
        }
    };

} ();



Ext.onReady(SW.VIM.HyperVServers.init, SW.VIM.HyperVServers);
