﻿Ext.namespace('SW.VIM');

SW.VIM.Progress = function () {
    var grid;
    var timeoutHandler = null;
    var miliseconds = 2000;
    var syncActionsCount = 0;

    function RenderStatus(value, metaData, record) {
        switch (value) {
            case 1:
                return "<img src='/Orion/images/ok_16x16.gif' />";
            case 2:
                return "<img src='/Orion/images/warning_16x16.gif' />";
            case 3:
                return "<img src='/Orion/images/Icon.Info.gif' />";
            default:
                return "<img src='/Orion/VIM/images/placeholder.png' />";
        }
    }

    function RenderNode(value, metaData, record) {
        return record.data.Node;
    }
    
    function RenderType(value, metaData, record) {
        return value;
    }
    
    function RenderMessage(value, metaData, record) {
        return "<span style='white-space:normal'>" + value + "</span>";
    }

    function LoadData() {
        VManSynchronizationWizardService.GetProgress(LoadDataSuccessCallback, LoadDataErrorCallback);
    };

    function LoadDataSuccessCallback(result) {
        if (result && result.Rows) {
            store.loadData(result, true);
        }

        if (result && result.Status == "Finished") {
            StopGetProgress();
            UpdateProgressStatus(result.Status);
        }
        else {
            UpdateProgressStatus(null);
            timeoutHandler = setTimeout(function () { LoadData(); }, miliseconds);
        }
    }

    function LoadDataErrorCallback(result) {
        StopGetProgress();

        Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js}' + result);
    }

    var jsonReader = new Ext.data.JsonReader({
        root: 'Rows',
        fields: [
            { name: 'Status', mapping: 'Status' },
            { name: 'Node', mapping: 'Node' },
            { name: 'Type', mapping: 'Type' },
            { name: 'Message', mapping: 'Description' }
        ]
    });

    var store = new Ext.data.Store({
        reader: jsonReader,
        fields: [
            { name: 'Status', type: 'string' },
            { name: 'Node', type: 'string' },
            { name: 'Type', type: 'string' },
            { name: 'Message', type: 'string' }
        ]
    });

    var colModel = new Ext.grid.ColumnModel({
        columns: [
            { header: '', dataIndex: 'Status', renderer: RenderStatus, width: 30, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_24;E=js}', dataIndex: 'Node', renderer: RenderNode, width: 150, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_25;E=js}', dataIndex: 'Type', renderer: RenderType, width: 110, fixed: true },
            { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_26;E=js}', dataIndex: 'Message', renderer: RenderMessage }
        ],

        defaults: {
            sortable: false,
            menuDisabled: true,
            hideable: false
        }
    });

    function InitGrid() {
        grid = new Ext.grid.GridPanel({
            renderTo: "progressGrid",
            store: store,
            layout:{
                type:'fit',
                align:'stretch',
                pack:'start'
            },
            height: SW.VIM.Utils.GetManagementGridHeight(),
            colModel: colModel,
            viewConfig: { forceFit: true },
            stripeRows: true,
        });

        Ext.EventManager.onWindowResize(function () {
            grid.setWidth(1);
            grid.setWidth($('#progressGrid').width());

            grid.setHeight(SW.VIM.Utils.GetManagementGridHeight());
            grid.doLayout();
        }, container);
    }

    function StopGetProgress() {
        window.clearTimeout(timeoutHandler);

        $(".sw-vim-wizard-btn-done").prop("disabled", false).removeClass("x-item-disabled");
        $(".sw-vim-wizard-btn-done").off("click");
    }

    function UpdateProgressStatus(status) {
        if (status != "Finished" && syncActionsCount > 0) {
            var count = Math.floor((store.getCount() * 100) / syncActionsCount);    //count progress in percent
            $("#sw-vim-wizard-progress-status-text").text("@{R=VIM.Strings;K=VIMWEBJS_JH0_61;E=js} (" + count + "%)");
        }
        else {
            var imgSrc = "/Orion/images/ok_16x16.gif";
            var text = "@{R=VIM.Strings;K=VIMWEBJS_JH0_59;E=js}";

            //Check if all operations ended without errors. 
            for (key in store.data.items) {
                if (store.data.items[key].data && store.data.items[key].data.Status == 2) {
                    imgSrc = "/Orion/images/warning_16x16.gif";
                    text = "@{R=VIM.Strings;K=VIMWEBJS_JH0_60;E=js}";
                }
            }

            $("#sw-vim-wizard-progress-status-img").attr("src", imgSrc);
            $("#sw-vim-wizard-progress-status-text").text(text);

            CheckMissingNodesInVMan();
        }
    }

    function CheckMissingNodesInVMan() {
        VManSynchronizationWizardService.CheckMissingNodesInVMan(CheckMissingNodesInVManSuccessCallback, CheckMissingNodesInVManErrorCallback);
    }

    function CheckMissingNodesInVManSuccessCallback(result) {
        if (result) {
            $("div.sw-vim-wizard-oriondatasources").removeClass("hidden");
        }
    }

    function CheckMissingNodesInVManErrorCallback(result) {
        Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js}' + result);
    }

    return {
        Init: function (processedSyncActionsCount) {
            syncActionsCount = processedSyncActionsCount;
            UpdateProgressStatus(null);

            $(".sw-vim-wizard-btn-done").prop("disabled", true).addClass("x-item-disabled");
            $(".sw-vim-wizard-btn-done").on("click", function () { return false; });

            InitGrid();
            LoadData();
        },
    };
}();