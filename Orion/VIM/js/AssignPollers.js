﻿Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.AssignPollers = function () {
    ORION.prefix = "VIM_AssignObjects_";
        
    var grid;
    var dataStore;
    var dialog;
    var selectPollerCombo;
    var pollersDataStore;
    var loadingMask;    
    var loadingText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_1;E=js}";
    
    // paging
    var pagingToolbar;
    var pageSizeBox;
    var pageSize = 40;
    var pagingToolbarFormat = "@{R=VIM.Strings;K=VIMWEBJS_JH0_2;E=js}";
    var noDataSourcesPagingToolbarText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_3;E=js}";
        
    function renderDescription(value, meta, record) {                
        var type = record.data.Type == 1 ? "VCenter" : "Host";        
        return String.format('{0} {1}', record.data.PlatformName, type);
    }        

    RefreshGrid = function() {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingText
            });
        }
        
        grid.store.on('beforeload', function() {
            ShowLoadingMask();
        });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = { start: 0 * pageSize, limit: pageSize, search: "" };        
        
        grid.store.on('load', function() {
            loadingMask.hide();
        });

        grid.store.on('exception', function() {
            ShowError("@{R=VIM.Strings;K=VIMWEBJS_JH0_4;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_5;E=js}");
            loadingMask.hide();
        });
        
        grid.store.load();
    };
    

 ShowLoadingMask = function() {
        if (!grid.el.isMasked()) {
            loadingMask.show();
        }
    };

    AssignPollerToNodes = function(nodesRecords) {        
        if (nodesRecords == undefined || nodesRecords.length == 0)
            return;

        var selectedPoller = selectPollerCombo.getValue();
        
        if (selectedPoller.toString().length == 0)
            return;

        var nodes = [];       
        
        for (var i = 0; i < nodesRecords.length; i++) {
            nodes.push(nodesRecords[i].data.ID);
        }

        ShowLoadingMask();
        
        ORION.callWebService(
            "/Orion/VIM/Services/VManSynchronizationWizard.asmx",
            "AssignPollerToNodes",
            { nodes: nodes, engineId: selectPollerCombo.getValue() },
            function(result) {            
                RefreshGrid();
            },
            function(fault) {
                ShowError("@{R=VIM.Strings;K=VIMWEBJS_JH0_8;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_9;E=js}");
                loadingMask.hide();
            }
        );
        
    };

    InitDialogWindow = function() {
        if (!dialog) {
            InitDialogContent();
            
            dialog = new Ext.Window({
                applyTo: 'assignPollersDialog',
                layout: 'fit',
                width: 500,
                height: 'auto',
                modal: true,
                closeAction: 'hide',
                plain: false,
                resizable: false,
                title: '@{R=VIM.Strings;K=VIMWEBJS_JH0_10;E=js}',
                bodyBorder: false,
                contentEl: 'assingPollersDialogBody',
                buttons: [
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_11;E=js}',
                        id: 'dlgOk',
                        handler: function() {
                            AssignPollerToNodes(grid.getSelectionModel().getSelections());
                            dialog.hide();
                        },
                        style: "text-align: center"
                    },
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_JH0_12;E=js}',
                        handler: function() {
                            dialog.hide();
                        }
                    }
                ]                
            });
        }
    };

    UpdatePollersDialogLabel = function() {
        $("#pollersDialogLabel").text(String.format("@{R=VIM.Strings;K=VIMWEBJS_JH0_13;E=js}", grid.getSelectionModel().getCount()));
    };

    InitDialogContent = function() {        
        pollersDataStore = new ORION.WebServiceStore(
                            "/Orion/VIM/Services/VManSynchronizationWizard.asmx/GetPollers",                             
                            [
                                { name: 'EngineID', mapping: 0 },
                                { name: 'ServerInfo', mapping: 1 }
                            ],
                            "ServerInfo");

        pollersDataStore.on("load", function(store) {
            if (store.getCount() > 0) {                
                var selectedItems = grid.getSelectionModel().getSelections();                
                var preselectEngineId = selectedItems.length > 0 ? selectedItems[0].data.PollerID : store.getAt(0).get("EngineID");
                selectPollerCombo.setValue(preselectEngineId);
            }            
        });        

        selectPollerCombo = new Ext.form.ComboBox({
            store: pollersDataStore,
            width: 250,
            autoSelect: true,
            loadingText: loadingText,
            transform: 'pollersDialogCombo',
            valueField: 'EngineID',            
            displayField: 'ServerInfo',
            triggerAction: 'all',
            mode: 'remote',            
            region: 'center',
            editable: false
        });

        pollersDataStore.proxy.conn.jsonData = {};                
    };

    UpdateToolbarButtons = function () {        
        var selItems = grid.getSelectionModel();
        var count = selItems.getCount();
        var map = grid.getTopToolbar().items.map;
        
        map.AssignPollerBtn.setDisabled(count < 1);
    };


    InitGrid = function() {        
        var selectorModel = new Ext.grid.CheckboxSelectionModel();

        selectorModel.on("selectionchange", UpdateToolbarButtons);        
        
        dataStore = new ORION.WebServiceStore(
                            "/Orion/VIM/Services/VManSynchronizationWizard.asmx/GetDataSourcesToAssignPollers",                             
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'PlatformName', mapping: 2 },
                                { name: 'Type', mapping: 3 },                                
                                { name: 'PollerID', mapping: 4 },
                                { name: 'PollerName', mapping: 5 }                                
                            ],
                            "Name");
        
        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: pageSize
        });        
        
        pageSizeBox.on('change', function (f, numbox, o) {            
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                ShowError("@{R=VIM.Strings;K=VIMWEBJS_JH0_14;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_15;E=js}");
                return;
            }

            if (pagingToolbar.pageSize != pSize) {
                pagingToolbar.pageSize = pSize;                
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }
        });
        
        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: pagingToolbarFormat,
            emptyMsg: noDataSourcesPagingToolbarText,
            items: [
                    '-',
                    '@{R=VIM.Strings;K=VIMWEBJS_JH0_16;E=js}',
                    pageSizeBox
                ]
        });

        grid = new Ext.grid.GridPanel({
            store: dataStore,            
            columns: [
                    selectorModel,
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_17;E=js}', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'Id' },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_18;E=js}', width: 300, hideable: false, sortable: true, dataIndex: 'Name' },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_19;E=js}', width: 300, hideable: true, sortable: true, dataIndex: 'Type', renderer: renderDescription },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_20;E=js}', width: 300, hideable: false, sortable: true, dataIndex: 'PollerName' },
                    { id: 'filler', hidden: false, hideable: false, width: 1, sortable: false, menuDisabled: true, dataIndex: -1 }
                ],
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,            
            tbar: [
            {
                id: 'AssignPollerBtn',
                text: "@{R=VIM.Strings;K=VIMWEBJS_JH0_21;E=js}",
                iconCls: 'sw-vim-wizard-assignPollerButton',
                handler: function() {
                    OpenAssignPollerDialog();
                }
            }],
            autoExpandColumn: "filler",
            bbar: pagingToolbar                        
        });

    };

    OpenAssignPollerDialog = function() {
        InitDialogWindow();          
        UpdatePollersDialogLabel();
        selectPollerCombo.store.load();                
        dialog.show();
    };

    ShowError = function(title, message) {
        Ext.Msg.alert(title, message).setIcon(Ext.Msg.ERROR);        
    };

    InitLayout = function () {
        var wrapper = new Ext.Container({
            renderTo: 'pollersMainPanel',
            layout: 'border',
            items: [grid],
            height: 500,            
        });                
        
        Ext.EventManager.onWindowResize(function () {            
            wrapper.doLayout();
        }, wrapper);        
    };

    return {                
        init: function () {            
            InitGrid();
            InitLayout();
            RefreshGrid();
            UpdateToolbarButtons();                        
        }
    };
} ();

Ext.onReady(SW.VIM.AssignPollers.init, SW.VIM.AssignPollers);
