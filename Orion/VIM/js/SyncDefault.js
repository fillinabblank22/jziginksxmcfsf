﻿Ext.namespace('SW.VIM');

SW.VIM.SyncDefault = function () {
    var seconds = 2;
    var identityMappingTimeout = 600;
    var iteration = 0;

    function CheckStatus() {
        if ((iteration * seconds) > identityMappingTimeout) {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_66;E=js}');
        }
        else {
            VManSynchronizationWizardService.HasIdentityMappingFinished(SuccessCallback, ErrorCallback);
        }
    };

    function SuccessCallback(result) {
        if (result && !result.Result) {
            setTimeout(function () { CheckStatus(); }, (seconds * 1000));
            iteration++;
        }
        else {
            $("#sw-vim-wizard-default-progress").hide();
            $(".sw-vim-wizard-btn-next").prop("disabled", false).removeClass("x-item-disabled");
            $(".sw-vim-wizard-btn-next").off("click");
        }

        $("#sw-vim-wizard-default-progress").removeClass("hidden");
    };

    function ErrorCallback(result) {
        if (result && result._message) {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js} ' + result._message);
        }
        else {
            Ext.Msg.alert('@{R=VIM.Strings;K=VIMWEBJS_JH0_22;E=js}', '@{R=VIM.Strings;K=VIMWEBJS_JH0_23;E=js}');
        }
    };

    return {
        Init: function (VManIdentityMappingTimeout) {
            if ($("#sw-vim-wizard-default-progress").hasClass("hidden")) {
                return;
            }

            identityMappingTimeout = VManIdentityMappingTimeout;
            $(".sw-vim-wizard-btn-next").prop("disabled", true).addClass("x-item-disabled");
            $(".sw-vim-wizard-btn-next").on("click", function () { return false; });
            CheckStatus();
        }
    };
}();