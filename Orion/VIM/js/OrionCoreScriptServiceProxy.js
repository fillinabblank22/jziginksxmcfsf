﻿/// <reference name="MicrosoftAjax.js"/>
/// <reference name="/Orion/VIM/js/ScriptServiceInvoker.js"/>
/// <reference name="/Orion/js/OrionCore.js"/>

if (!SolarWinds) var SolarWinds = {};
if (!SolarWinds.VIM) SolarWinds.VIM = {};
if (!SolarWinds.VIM.js) SolarWinds.VIM.js = {};

SolarWinds.VIM.js.OrionCoreScriptServiceProxy = function () {
    SolarWinds.VIM.js.OrionCoreScriptServiceProxy.initializeBase(this);
    this.invoker = $create(SolarWinds.VIM.js.ScriptServiceInvoker);
    this.onSuccessOuter = null;
    this.onSuccessNotLastOuter = null;
    this.onCallSuccess = null;
    this.context = null;
};

SolarWinds.VIM.js.OrionCoreScriptServiceProxy.prototype = {

    dispose: function () {
        SolarWinds.VIM.js.OrionCoreScriptServiceProxy.callBaseMethod(this, 'dispose');
    },

    callWebService: function (serviceName, methodName, param, onSuccess, onSuccessNotLast) {
        this.onSuccessOuter = onSuccess;
        if (onSuccessNotLast != undefined) {
            this.onSuccessNotLastOuter = onSuccessNotLast;
        }
        this.invoker.callService(
            Function.createDelegate(this, this.methodToCall),
            [serviceName, methodName, param],
            null,
            1000,
            this,
            Function.createDelegate(this, this.onInvokerSuccessNotLast),
            Function.createDelegate(this, this.onInvokerSuccessLast),
            Function.createDelegate(this, this.onInvokerError)
        );
    },
    
    callWebServiceWithProgress: function (serviceName, methodName, param, onSuccess) {
        this.callWebService(serviceName, methodName, param, onSuccess, onSuccess);
    },

    //simulates scriptservice method by the same signature
    methodToCall: function (serviceName, methodName, param, onCallSuccess, onCallError, context) {
        //store invoker onSuccess delegate to call when orion core callback succeeds
        this.onCallSuccess = Function.createDelegate(this, onCallSuccess);
        this.context = context;

        //onError isn't supported by core, how ti finih on error?
        var onOrionCoreSuccessDelegate = Function.createDelegate(this, this.onOrionCoreSuccess);

        // We had to move "callWebService" method to separate JS file in VIM. But this "OrionCoreScriptServiceProxy" class
        // can be used by pages that can include either OrionCore.js or /VIM/js/WebService.js. When Core fixes the OrionCore.js
        // to not call "Ext" methods.
        if (typeof(SW.VIM.WebService) != "undefined")
            SW.VIM.WebService.callWebService(serviceName, methodName, param, onOrionCoreSuccessDelegate);
        else
            ORION.callWebService(serviceName, methodName, param, onOrionCoreSuccessDelegate);
    },

    //simulates scriptservice method success request by calling of invoker's success callback
    onOrionCoreSuccess: function (responseData) {
        this.onCallSuccess.apply(this, [responseData, this.context]);
    },

    cancel: function () {
        this.invoker.cancel();
    },

    onInvokerSuccessNotLast: function (resultContainer, context) {
        if (this.onSuccessNotLastOuter != null) {
            this.onSuccessNotLastOuter(resultContainer.result);
        }
    },

    //last success => correct end by calling of outer success callback
    onInvokerSuccessLast: function (resultContainer, context) {
        this.onSuccessOuter(resultContainer.result);
    },

    onInvokerError: function (error, context) {
        //we will not handle error since orion core method doesn't support error handling 
        //=> this method shouldn't be called
    }
};

// JSON object that describes all properties, events, and methods of this component that should
// be addressable through the Sys.TypeDescriptor methods, and addressable via xml-script.
SolarWinds.VIM.js.OrionCoreScriptServiceProxy.descriptor = {
    properties: [],
    events: []
};

SolarWinds.VIM.js.OrionCoreScriptServiceProxy.registerClass('SolarWinds.VIM.js.OrionCoreScriptServiceProxy', Sys.Component);

// Since this script is not loaded by System.Web.Handlers.ScriptResourceHandler
// invoke Sys.Application.notifyScriptLoaded to notify ScriptManager 
// that this is the end of the script.
if (typeof (Sys) !== 'undefined') { Sys.Application.notifyScriptLoaded(); }
