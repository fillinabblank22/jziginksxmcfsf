/// <reference path="~/Orion/js/jquery.js" />
/// <reference path="~/Orion/js/extjs/debug/Ext.js" />
/// <reference path="~/Orion/js/extjs/debug/jquery-bridge.js" />
/// <reference path="~/Orion/js/extjs/debug/ext-all-debug.js" />
/// <reference path="~/Orion/js/OrionCore.js" />
Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.VMwareServers = function () {
    // swis entity names
    VCenterEntity = "Orion.VIM.VCenters";
    DataCenterEntity = "Orion.VIM.DataCenters";
    ClusterEntity = "Orion.VIM.Clusters";
    HostEntity = "Orion.VIM.Hosts";

    // HostEntity extended with platform (only for VimIconProvider purpose)
    VMwareHostEntityName = "Orion.VIM.Hosts.Vmware";

    loadingMask = null;
    expandedItems = [];

    columnMapping = [
        { name: 'InstanceType', mapping: 0 },
        { name: 'ID', mapping: 1 },
        { name: 'NodeID', mapping: 2 },
        { name: 'IPAddress', mapping: 3 },
        { name: 'Name', mapping: 4 },
        { name: 'ChildsCount', mapping: 5 },
        { name: 'Status', mapping: 6 },
        { name: 'HostStatus', mapping: 7 },
        { name: 'CredentialID', mapping: 8 },
        { name: 'CredentialName', mapping: 9 },
        { name: 'VMwareProductName', mapping: 10 },
        { name: 'VMwareProductVersion', mapping: 11 },
        { name: 'PollingMethod', mapping: 12 },
        { name: 'Level', mapping: 13 },
        { name: 'VCenterName', mapping: 14 },
        { name: 'VCenterPollingSource', mapping: 15 },
        { name: 'VCenterNodeID', mapping: 16 },
        { name: 'EngineID', mapping: 17 }
    ];

    ORION.prefix = "VIM_VMwareServers_";

    ORION.handleError = function (xhr) {
        if (xhr.status == 401 || xhr.status == 403) {
            // login cookie expired. reload the page so we get bounced to the login page.
            alert('@{R=VIM.Strings;K=VIMWEBJS_VB0_9;E=js}');
            window.location.reload();
        }

        var errorText = xhr.responseText;
        try {
            var error = eval("(" + errorText + ")");

            if (assignDialog && !assignDialog.hidden)
                assignDialog.hide();
            if (grid != null) {
                nodeInProgressIds = [];
                grid.getView().refresh();
            }
            if (loadingMask != null) {
                loadingMask.hide();
            }
            Ext.Msg.show({
                title: "@{R=VIM.Strings;K=VIMWEBJS_TM0_1;E=js}",
                msg: error.Message,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                minWidth: 596
            });
            $("#test").text(error.Message);
            $("#stackTrace").text(error.StackTrace);
        }
        catch (err) {
            $("#test").text(err.description);
        }
    };

    PollingSourceChangeCallback = function(result) {
        if (result.length > 0) {
            var message = "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_SwitchingErrorLicenseBased;E=js}";
            message += "<br> <ul class=\"vim-bullet\">";

            for (var i = 0; i < result.length; i++) {
                var name = encodeHTML(result[i]);
                message += String.format("<li>{0}</li>", name);
            }
            message += "</ul>";
            Ext.Msg.show({
                title: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_WarningTitle;E=js}",
                msg: message,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK,
                minWidth: 596
            });

        }
        grid.getStore().reload();
    };

    RefreshObjects = function () {
        // create loading mask
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=VIM.Strings;K=VIMWEBJS_TM0_2;E=js}"
            });
        }

        // hide loading mask when load is complete
        grid.store.on('load', function () {
            loadingMask.hide();
            UpdateToolbarButtons();
            UpdateExpandStates();
        });
        grid.getView().on('refresh', function () { UpdateToolbarButtons(); UpdateExpandStates(); });

        loadingMask.show();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load();

    };


    UpdateToolbarButtons = function () {
        var selItems = grid.getSelectionModel();
        var map = grid.getTopToolbar().items.map;

        var anyEnabled = false,
            anyDisabled = false,
            anyNonESXnodes = false,
            allNodesPolledDirectly = true,
            allNodesPolledThroughVCenter = true,
            anyNodesIsNotVCentersOrEsx = false,
            disableAll = selItems.getCount() == 0,
            anyWithoutNodeId = false;
        var anyWithoutVCenterNodeID = false;
        var anyWithVCenterNodeID = false;
        var anyWithSynchronizedVCenter = false;
        var isPollingSourceBasicButtonDisabled = true;
        var isPollingSourceVmanOrionButtonDisabled = true;
        var vimFullyLicensed;
        isVimFullyLicensed(function (result) { vimFullyLicensed = result; });

        if (!disableAll) {
            grid.getSelectionModel().each(function (rec) {
                if (rec.data.HostStatus != 3) anyEnabled = true;
                if (rec.data.HostStatus == 3) anyDisabled = true;
                if (rec.data.InstanceType != HostEntity) anyNonESXnodes = true;
                if (rec.data.PollingMethod == 0) allNodesPolledDirectly = false;
                if (rec.data.PollingMethod == 1) allNodesPolledThroughVCenter = false;
                if (rec.data.InstanceType != HostEntity && rec.data.InstanceType != VCenterEntity)
                    anyNodesIsNotVCentersOrEsx = true;
                if (rec.data.NodeID == null || rec.data.NodeID == -1) anyWithoutNodeId = true;
                if (rec.data.VCenterNodeID == null || rec.data.VCenterNodeID == -1)
                    anyWithoutVCenterNodeID = true;
                else
                    anyWithVCenterNodeID = true;
                if (rec.data.VCenterPollingSource == 1) anyWithSynchronizedVCenter = true;
                isPollingSourceBasicButtonDisabled = isPollingSourceBasicButtonDisabled && rec.data.VCenterPollingSource == 0;
                isPollingSourceVmanOrionButtonDisabled = isPollingSourceVmanOrionButtonDisabled && (rec.data.VCenterPollingSource == 2 || !vimFullyLicensed);
            });
        }

        map.EnableButton.setDisabled(anyEnabled || disableAll || anyWithoutNodeId);
        map.DisableButton.setDisabled(anyDisabled || disableAll || anyWithoutNodeId);

        map.PollingSourceButton.setDisabled(anyWithoutNodeId || disableAll || anyWithVCenterNodeID || (isPollingSourceBasicButtonDisabled && isPollingSourceVmanOrionButtonDisabled));
        map.PollingSourceButton.menu.items.map.PollingSourceBasicButton.setDisabled(isPollingSourceBasicButtonDisabled);
        map.PollingSourceButton.menu.items.map.PollingSourceVmanOrionButton.setDisabled(isPollingSourceVmanOrionButtonDisabled);

        map.AssignButton.setDisabled(anyNodesIsNotVCentersOrEsx || anyWithoutNodeId || disableAll);
    };


    //////////////////////////////////////////////////////////////////////////////////////////////
    // Column rendering functions 
    //////////////////////////////////////////////////////////////////////////////////////////////
    function renderServer(value, meta, record) {
        var id = record.data.ID;
        var level = record.data.Level;
        var engineId = record.data.EngineID;
        var ipAddress = record.data.IPAddress;
        var result = '';
        var caption;

        if (record.data.NodeID == null) {
            caption = '<span style="color: rgb(100, 100, 100); font-style: italic;">' + value + '</span>';
        }
        else {
            caption = value;
        }

        // each level means extra padding
        for (var i = 1; i < level; i++) {
            result += '<span class="tree-grid-indent-block" />';
        }

        if (id != -1 && record.data.ChildsCount > 0) {
            result += String.format('<span><img src="/Orion/images/Pixel.gif" class="tree-grid-expander-expand" onclick="SW.VIM.VMwareServers.toggleRow(this, \'{0}\');"></span>', record.id);
        } else {
            result += '<span class="tree-grid-expander-dummy" />';
        }

        if (record.data.NodeID == null && record.data.InstanceType == HostEntity) {
            return String.format('{0}<span class="tree-grid-caption"><a href="javascript:void(0);" OnClick="ManageNodeDialog(\'{2}\', \'{3}\',\'H{1}\', {6});">{4} {5}</a></span>',
                result,
                id,
                engineId,
                ipAddress == null ? '' : ipAddress,
                getStatusIcon(record),
                caption,
                ipAddress != null && ipAddress.indexOf(";") != -1);
        }
        else {
            var netObject;

            switch (record.data.InstanceType) {
                case VCenterEntity: netObject = "N:" + record.data.NodeID; break;
                case DataCenterEntity: netObject = "VMD:" + record.data.ID; break;
                case ClusterEntity: netObject = "VMC:" + record.data.ID; break;
                case HostEntity: netObject = "N:" + record.data.NodeID; break;
                default: return String.format('{0} {1} {2}', result, getStatusIcon(record), value); //unknown entity -> no link
            }

            return String.format('{0}<span class="tree-grid-caption"><a href="/Orion/View.aspx?NetObject={1}" >{2} {3}</a></span>', result, netObject, getStatusIcon(record), value);
        }
    };

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    function renderText(value, meta, record) {
        return value == null ? '' : encodeHTML(value);
    };

    function renderStatus(value, meta, record) {
        // don't want to render polling status icon for loading record or record without host status
        if (record.data.ID == -1 || record.data.HostStatus == -1) {
            return '';
        }

        if (isNodeInProgress(record.data['NodeID'])) {
            return String.format('<img src="/Orion/VIM/images/AJAX-Loader.gif" /> {1}', '@{R=VIM.Strings;K=VIMWEBJS_TM0_4;E=js}');
        }

        var iconPath = '/Orion/VIM/images/VMwarePollingStatus/'
        var status = (record.data.PollingMethod == 0 && value != 3 && value != 5)
            ? record.data.HostStatus
            : value;
        switch (status) {
            case 1: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'needs_credentials.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_3;E=js}');
            case 2: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'polling.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_5;E=js}');
            case 3: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'disabled.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_8;E=js}');
            case 4: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'bad_credentials.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_6;E=js}');
            case 5: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'disabled.gif', '@{R=VIM.Strings;K=VIMWEBJS_TM0_7;E=js}');
            default: return String.format('<img src="{0}{1}" /> {2}', iconPath, 'unknown.gif', '@{R=VIM.Strings;K=VIMLIBCODE_VB0_7;E=js}');
        }
    };

    function renderPollingMethod(value, meta, record) {
        // don't want to render polling method icon for loading record
        if (record.data.ID == -1) {
            return '';
        }

        var iconPath = '/Orion/VIM/images/StatusIcons/'
        var pollingMethod = record.data.PollingMethod;
        if (pollingMethod == 1) {
            return '@{R=VIM.Strings;K=VIMWEBJS_TM0_9;E=js}';
        }
        else if (pollingMethod == 0) {
            return String.format(String.format('@{R=VIM.Strings;K=VIMWEBJS_TM0_10;E=js}', '<a href="/Orion/View.aspx?NetObject=N:{0}" class="tree-grid-link">{1}</a>'), record.data.VCenterNodeID, record.data.VCenterName);
        }
        return '';
    };

    function renderPollingSource(value, meta, record) {
        switch (record.data.VCenterPollingSource) {
            case 0: return '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}';
            case 1: return '@{R=VIM.Strings;K=PollingSource_VMan;E=js}';
            case 2:
                var vimFullyLicensed;
                isVimFullyLicensed(function (result) { vimFullyLicensed = result; });
                if (vimFullyLicensed) {
                    return '@{R=VIM.Strings;K=PollingSource_LicenseBased;E=js}';
                } else {
                    return '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}'; 
                }
            default: return '';
        }
    };

    function getStatusIcon(record) {
        if (record.data.ID == -1) {
            return '<img src="/Orion/images/AJAX-Loader.gif" />';
        }
        else {
            var iconEntityName = record.data.InstanceType == HostEntity ? VMwareHostEntityName : record.data.InstanceType;
            return String.format('<img src="/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}" />', iconEntityName, record.data.Status);
        }
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Credentials dialog 
    //////////////////////////////////////////////////////////////////////////////////////////////
    function checkCredentials(description, username, password, confirm) {
        if ($.trim(String(description)) == '') {
            return "@{R=VIM.Strings;K=VIMWEBJS_AK0_1;E=js}";
        }

        if ($.trim(String(username)) == '') {
            return "@{R=VIM.Strings;K=VIMWEBJS_AK0_2;E=js}";
        }

        if (!(password === confirm) || $.trim(String(password)) == '') {
            return "@{R=VIM.Strings;K=VIMWEBJS_AK0_3;E=js}";
        }

        return "";
    };

    function updateVMwarePollingStatus(enable, onSuccess) {
        loadingMask.show();
        var nodeIds = getSelectedNodeIds(grid, false);
        ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "UpdateVMwarePollingStatus", { nodeIds: nodeIds, enable: enable }, onSuccess);
    };

    function updateNodePollingSource(pollingSource, onSuccess) {
        loadingMask.show();
        var nodeIds = getSelectedNodeIds(grid, false);
        var vimFullyLicensed;
        isVimFullyLicensed(function (result) { vimFullyLicensed = result; });
        if (!isBasicPollingOnly(grid, false) && pollingSource == 0 && vimFullyLicensed) {
            Ext.MessageBox.show({
                title: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_ConfirmSwitchDialogTitle;E=js}",
                msg: "@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_ConfirmSwitchDialog;E=js}",
                icon: Ext.Msg.QUESTION,
                buttons: Ext.MessageBox.YESNO,
                minWidth: 596,
                fn: function (btn) {
                    if (btn == "yes") {
                        ORION.callWebService("/Orion/VIM/Services/VMWareServers.asmx",
                            "UpdateNodePollingSource",
                            { nodeIds: nodeIds, pollingSource: pollingSource },
                            onSuccess);
                    } else {
                        grid.getStore().reload();
                    }
                }
            });
        } else {
            ORION.callWebService("/Orion/VIM/Services/VMWareServers.asmx",
                "UpdateNodePollingSource",
                { nodeIds: nodeIds, pollingSource: pollingSource },
                onSuccess);
        }
    };

    function isVimFullyLicensed(onSuccess) {
        SW.Core.Services.callWebServiceSync("/Orion/VIM/Services/VMWareServers.asmx", "IsVimFullyLicensed", {}, onSuccess, {});
    }

    function loadCredentials() {
        select.empty();

        // On following line is needed to replace characters < and > because in resx files thanks Visual Studio are replaced on the start and end entities &amp; by &
        $(String.format("<option value='new'>{0}</option>", "@{R=VIM.Strings;K=VIMWEBJS_TM0_11;E=js}".replace("<", "&lt;").replace(">", "&gt;"))).appendTo(select);
        ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "GetAllVMwareCredentials", {}, function (result) {
            credentialList = result;
            for (var i = 0; i < result.length; i++) {
                var name = encodeHTML((new String(result[i].Description)).substring(0, 64));
                $(String.format("<option value='{1}'>{0}</option>", name, i)).appendTo(select);
            }
        });
    };

    function setStatusMessage(message, succeeded) {
        var div = (succeeded) ? $('<div class="sw-suggestion sw-suggestion-pass">') : $('<div class="sw-suggestion sw-suggestion-fail">');
        div.append($('<span class="sw-suggestion-icon">')).css('width', '85%').append(message);
        var table = $('<table>').css('width', '100%').css('height', '50px').append($('<tr>').append($('<td>').css('padding', '0px').css('vertical-align', 'middle').append(div)));

        $("#assignDialogBody .Error").empty();
        $("#assignDialogBody .Error").append(table);
    };

    function enableButtons() {
        testButton.enable();
        $('#assignList').removeAttr('disabled');
        assignDialog.buttons[0].enable();   // assign button
        assignDialog.buttons[1].enable();   // cancel button
    };

    function disableButtons() {
        testButton.disable();
        $('#assignList').attr('disabled', 'disabled');
        assignDialog.buttons[0].disable();  // assign button
        assignDialog.buttons[1].disable();  // cancel button  
    };

    function assignCredential() {
        $("#assignDialogBody input").val('');
        setStatusMessage('', true);

        if (!testButton) {
            testButton = new Ext.Button({
                renderTo: 'testButton',
                text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_7;E=js}',

                handler: function () {
                    $("#assignDialogBody .Error").empty();
                    var description = $("#assignDescription").val();
                    var select = $('#assignList');
                    var credentialId = credentialList[select.val()] ? credentialList[select.val()].Id : 0;

                    var username = $("#assignUsername").val();
                    var password = $("#assignPassword").val();
                    var confirm = $("#assignConfirm").val();

                    var nodeIds = getSelectedNodeIds(grid, false);

                    var error = checkCredentials(description, username, password, confirm);
                    if (error != '') {
                        setStatusMessage(error, false);
                        return;
                    }

                    // check for existing credential name
                    if (credentialId == 0) {
                        for (var i = 0; i < credentialList.length; i++) {
                            if (credentialList[i].Description == description) {
                                setStatusMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', false);
                                enableButtons();
                                return;
                            }
                        }
                    }

                    // polling...
                    var img = $('<img src="/Orion/VIM/images/AJAX-Loader.gif" />').css('padding', '2px 5px 2px 2px').css('vertical-align', 'middle');
                    var table = $('<table>').append($('<tr>').append($('<td>').css('padding', '0px').css('vertical-align', 'middle').append(img).append('@{R=VIM.Strings;K=VIMWEBJS_TM0_12;E=js}')));
                    var div = $('<div>').css('color', '#000000').css('font-weight', 'normal').append(table);
                    $("#assignDialogBody .Error").append(div);

                    disableButtons();

                    ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "BeginTestCredentials", { nodeIds: nodeIds, credentialId: credentialId, username: username, password: password, updateStatus: false }, function (result) {
                        var taskId = result;
                        var proxy = $create(SolarWinds.VIM.js.OrionCoreScriptServiceProxy);
                        proxy.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "GetTestCredentialResultsSummary", { taskId: taskId }, function (result) {
                            enableButtons();
                            var succeeded = result.Succeeded;
                            setStatusMessage((succeeded) ? '@{R=VIM.Strings;K=VIMWEBJS_AK0_5;E=js}' : '@{R=VIM.Strings;K=VIMWEBJS_AK0_4;E=js}', succeeded);
                        });
                    });
                }
            });
        }

        if (!assignDialog) {
            assignDialog = new Ext.Window({
                applyTo: 'assignDialog',
                layout: 'fit',
                width: 400,
                closable: false,
                closeAction: 'hide',
                modal: true,
                plain: true,
                resizable: false,
                items: new Ext.BoxComponent({
                    applyTo: 'assignDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_10;E=js}',
                        cls: 'assignButton',
                        handler: function () {

                            var description = $("#assignDescription").val();
                            var username = $("#assignUsername").val();
                            var password = $("#assignPassword").val();
                            var confirm = $("#assignConfirm").val();

                            var nodeIds = getSelectedNodeIds(grid, false);

                            var val = select.val();
                            if (val === 'new') {
                                // assign new credential
                                var error = checkCredentials(description, username, password, confirm);
                                if (error != '') {
                                    setStatusMessage(error, false);
                                    return;
                                }

                                // check for existing credential name
                                for (var i = 0; i < credentialList.length; i++) {
                                    if (credentialList[i].Description == description) {
                                        setStatusMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', false);
                                        return;
                                    }
                                }

                                ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "CreateAndAssignVMwareCredential", { nodeIds: nodeIds, description: description, username: username, password: password },
                                    function (result) {
                                        onCredentialAssignSuccess(nodeIds, result, username, password);
                                    });
                            }
                            else {
                                // assign existing credential
                                var credentialId = credentialList[select.val()].Id;
                                ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "AssignVMwareCredential", { nodeIds: nodeIds, credentialID: credentialId },
                                    function () {
                                        onCredentialAssignSuccess(nodeIds, credentialId, username, password);
                                    });
                            }

                            assignDialog.hide();
                        }
                    },
                    {
                        text: '@{R=VIM.Strings;K=VIMWEBJS_AK0_13;E=js}',
                        style: 'margin-left: 5px; width: auto;',
                        id: 'cancelAssignButton',
                        handler: function () {
                            assignDialog.hide();
                        }
                    }]
            });
        }
        //load credentials every time when assign dialog is opening
        loadCredentials();
        enableButtons();
        var select = $("#assignList");
        select.val('new');
        select.change();

        // Set the location
        assignDialog.alignTo(document.body, "c-c");
        assignDialog.show();

        return false;
    };

    isBasicPollingOnly = function (grid, includeNulls) {
        var toReturn = true;
        grid.getSelectionModel().each(function(rec) {
            if (includeNulls || rec.data.VCenterPollingSource != null) {
                if (rec.data.VCenterPollingSource != 0) {
                    toReturn = false;
                }
            }
        });

        return toReturn;
    };

    getSelectedNodeIds = function (grid, includeNulls) {
        var nodeIds = [];
        grid.getSelectionModel().each(function (rec) {
            if (includeNulls || rec.data.NodeID != null) {
                nodeIds.push(rec.data.NodeID);
            }
        });
        return nodeIds;
    },

    onCredentialAssignSuccess = function (nodeIds, credentialId, username, password) {
        loadingMask.show();
        grid.getStore().reload();
        UpdateExpandStates();
        //exclude nodes polled through vcenter from credential test
        var nodeIdsToTest = [];
        for (var i = 0; i < nodeIds.length; i++) {
            var nodeId = nodeIds[i];
            var recordIndex = grid.store.find('NodeID', nodeId);
            if (recordIndex >= 0) {
                var record = grid.store.getAt(recordIndex);
                if (record.get('PollingMethod') == 1 || record.get('PollingMethod') == -1) { //directly or vCenter
                    nodeIdsToTest.push(nodeId);
                }
            }
        }
        addNodesInProgress(nodeIdsToTest);
        grid.getView().refresh();
        testCredentials(nodeIdsToTest, credentialId, username, password, true);
    };

    testCredentials = function (nodeIds, credentialId, username, password, updateStatus) {
        ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "BeginTestCredentials", { nodeIds: nodeIds, credentialId: credentialId, username: username, password: password, updateStatus: updateStatus }, function (result) {
            var taskId = result;
            var proxy = $create(SolarWinds.VIM.js.OrionCoreScriptServiceProxy);
            proxy.callWebServiceWithProgress("/Orion/VIM/Services/VMwareServers.asmx", "GetTestCredentialResultsDetails", { taskId: taskId },
                function (result) {
                    var x = 0;
                    for (var i = 0; i < result.Results.length; i++) {
                        var nodeId = result.Results[i].Key;
                        var status = result.Results[i].Value;
                        var recordIndex = grid.store.find('NodeID', nodeId);
                        if (recordIndex >= 0) {
                            var record = grid.store.getAt(recordIndex);
                            removeFromNodesInProgress(nodeId);
                            record.set('HostStatus', status);
                            if (record.modified) {
                                record.modified['HostStatus'] = undefined;
                            }
                        }
                    }
                    if (result.IsLastResult) {
                        grid.getStore().reload();
                    }
                    else {
                        grid.getView().refresh();
                    }
                });
        });
    };

    reTestCredentials = function (nodeIds, includePollingThroughVCenter) {
        var credentialIds = [];
        var nodeIdsToTest = [];
        for (var i = 0; i < nodeIds.length; i++) {
            var nodeId = nodeIds[i];
            var recordIndex = grid.store.find('NodeID', nodeId);
            if (recordIndex >= 0) {
                var record = grid.store.getAt(recordIndex);
                if (!includePollingThroughVCenter && record.data['PollingMethod'] == 0) {
                    continue;
                }
                var credentialId = record.data['CredentialID'];
                if (credentialId == null) {
                    continue;
                }
                nodeIdsToTest.push(nodeId);
                if (credentialIds.indexOf(credentialId) == -1) {
                    credentialIds.push(credentialId);
                }
            }
        }
        addNodesInProgress(nodeIdsToTest);
        for (var j = 0; j < credentialIds.length; j++) {
            var credentialId = credentialIds[j];
            var nodeIdsForCredential = [];
            for (var i = 0; i < nodeIdsToTest.length; i++) {
                var nodeId = nodeIdsToTest[i];
                var recordIndex = grid.store.find('NodeID', nodeId);
                if (recordIndex >= 0) {
                    var record = grid.store.getAt(recordIndex);
                    if (record.data['CredentialID'] == credentialId) {
                        nodeIdsForCredential.push(nodeId);
                    }
                }
            }
            if (nodeIdsForCredential.length > 0) {
                testCredentials(nodeIdsForCredential, credentialId, '', '', true);
            }
        }
    };

    addNodesInProgress = function (nodeIds) {
        nodeInProgressIds = nodeInProgressIds.concat(nodeIds);
    };


    removeFromNodesInProgress = function (nodeId) {
        var index = -1;
        for (var i = 0; i < nodeInProgressIds.length; i++) {
            if (nodeInProgressIds[i] === nodeId) {
                index = i;
            }
        }
        if (index >= 0) {
            nodeInProgressIds.splice(index, 1);
        }
    };

    isNodeInProgress = function (nodeId) {
        for (var i = 0; i < nodeInProgressIds.length; i++) {
            if (nodeInProgressIds[i] === nodeId) {
                return true;
            }
        }
        return false;
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Manipulation with rows
    //////////////////////////////////////////////////////////////////////////////////////////////

    GetRecordFullID = function (record) {
        return String.format("{0}-{1}", record.data.ID, record.data.InstanceType);
    }

    // marks all expander icons with the right class and fills expandedItems array with expanded records ids
    UpdateExpandStates = function () {
        var clollapsedExpanders = $("img:visible[class=tree-grid-expander-collapse]").removeClass('tree-grid-expander-collapse').addClass('tree-grid-expander-expand');
        var previousRecord = null;
        var expanders = $("img:visible[class=tree-grid-expander-expand]");
        var expanderIndex = -1;

        grid.store.data.each(function (record, index, length) {
            if (record != null && record.data.ChildsCount > 0) {
                delete expandedItems[GetRecordFullID(record)];
            }

            if (previousRecord != null && previousRecord.data.Level < record.data.Level) {
                expandedItems[GetRecordFullID(previousRecord)] = true;
                expanders[expanderIndex].className = 'tree-grid-expander-collapse';
                changed = true;
            }

            previousRecord = record;

            if (record.data.ChildsCount > 0) {
                expanderIndex++;
            }
        });
    }

    GetLoadingRecord = function (level) {
        var blankRecord = Ext.data.Record.create(dataStore.fields);
        var loadingRecord = new blankRecord({
            ID: -1,
            Level: level + 1,
            Name: "@{R=VIM.Strings;K=VIMWEBJS_TM0_13;E=js}"
        });
        return loadingRecord;
    }

    // handler for expanding or collapsing of rows
    ToggleRow = function (expander, recordId) {
        selectorModel.unlock();
        var item = dataStore.getById(recordId);
        var itemID = GetRecordFullID(item);

        if (expandedItems[itemID]) {
            CollapseRow(expander, recordId);
            ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "ChangeTree", { item: itemID, expand: false }, function (results) { });
        } else {
            loadingMask.show();
            ExpandRow(expander, recordId);
            ORION.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "ChangeTree", { item: itemID, expand: true }, function (results) { });
        }
    }

    // loads subtree and inserts under expanded row
    ExpandRow = function (expander, recordId, callback, items) {
        var rowRecord = dataStore.getById(recordId);
        var insertIndex = dataStore.indexOfId(recordId) + 1;

        dataStore.insert(insertIndex, GetLoadingRecord(parseInt(rowRecord.data.Level)));

        var dalMethod;
        var args;

        switch (rowRecord.data.InstanceType) {
            case VCenterEntity:
                dalMethod = "GetVCenterChildNodes";
                args = { vCenterID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            case DataCenterEntity:
                dalMethod = "GetDatacenterChildNodes";
                args = { datacenterID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            case ClusterEntity:
                dalMethod = "GetClusterChildNodes";
                args = { clusterID: rowRecord.data.ID, parentLevel: rowRecord.data.Level };
                break;
            default:
                loadingMask.hide();
                return; // unknown type of entity, don't know what dal method use to get childs
        }

        var store = new ORION.WebServiceStore(String.format("/Orion/VIM/Services/VMwareServers.asmx/{0}", dalMethod), columnMapping, "Name");

        store.proxy.conn.jsonData = args;
        store.addListener("load", function (store) {
            expandedItems[recordId] = true;
            expander.className = 'tree-grid-expander-collapse';

            var toAdd = new Array();

            store.each(function (record) { toAdd.unshift(record); });

            dataStore.removeAt(insertIndex); // remove "loading" record

            if (toAdd.length > 0) { dataStore.insert(insertIndex, toAdd); }

            UpdateExpandStates();
            loadingMask.hide();
        });

        store.load();
    };

    // removes subtree
    CollapseRow = function (expander, recordId) {
        var rowRecord = dataStore.getById(recordId);
        var index = dataStore.indexOfId(recordId);
        var limit = dataStore.getCount();
        var toRemove = new Array();

        // remove all records in a branch under current record, use Level to check this
        for (var i = index + 1; i < limit; i++) {
            var record = dataStore.getAt(i);
            if (record.data.Level > rowRecord.data.Level) {
                toRemove.push(record);
            } else {
                break;
            }
        }

        dataStore.remove(toRemove);

        delete expandedItems[GetRecordFullID(rowRecord)];
        expander.className = 'tree-grid-expander-expand';
    };


    //////////////////////////////////////////////////////////////////////////////////////////////
    // Grid
    //////////////////////////////////////////////////////////////////////////////////////////////
    var selectorModel;
    var dataStore;
    var grid;
    var assignDialog;
    var testButton;
    var credentialList;
    var select;
    var pageSizeNum;

    var nodeInProgressIds = [];
    var hiddenColumns;

    saveGridConfig = function () {
        hiddenColumns = [];
        for (var i = 0; i < grid.colModel.config.length; i++) {
            var column = grid.colModel.columns[i];
            if (column.hidden) {
                hiddenColumns.push(column.dataIndex);
            }
        }
        var hiddenColumnsString = (hiddenColumns.length > 0 ? ('[\'' + hiddenColumns.join('\',\'') + '\']') : '[]');
        ORION.Prefs.save('HiddenColumns', hiddenColumnsString);
    };

    isHiddenColumn = function (columnName) {
        for (var i = 0; i < hiddenColumns.length; i++) {
            if (hiddenColumns[i] == columnName) return true;
        }
        return false;
    };


    return {
        // handler for row toggle - onclick method on expander image
        toggleRow: ToggleRow,

        init: function () {
            hiddenColumns = eval(ORION.Prefs.load('HiddenColumns', '[\'PollingMethod\']'));

            Ext.override(Ext.PagingToolbar, {
                updateInfo: function () {
                    if (this.displayItem) {
                        var count = !this.store.data.length
                            ? 0
                            : this.store.query('Level', '1').length;
                        var msg = count == 0 ?
                            this.emptyMsg :
                            String.format(
                                this.displayMsg,
                                this.cursor + 1, this.cursor + count, this.store.getTotalCount()
                            );
                        this.displayItem.setText(msg);
                    }
                }
            });

            pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));
            select = $("#assignList");

            //loadCredentials();

            select.change(function () {
                $("#assignDialogBody .Error").text('');
                var val = select.val();
                if (val === 'new') {
                    $("#assignDialogBody input").val('').removeAttr("disabled");
                }
                else {
                    $("#assignDialogBody input").attr("disabled", "disabled");
                    $("#assignDescription").val(credentialList[select.val()].Description);
                    $("#assignUsername").val(credentialList[select.val()].Username);
                    $("#assignPassword").val('------');
                    $("#assignConfirm").val('------');
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel({
                listeners: {
                    selectionchange: UpdateToolbarButtons
                }
            });

            // create datastore for the grid
            // top level can contain either vCenter or standalone ESX
            dataStore = new ORION.WebServiceStore("/Orion/VIM/Services/VMwareServers.asmx/GetTopLevelNodes", columnMapping, "Name");

            grid = new Ext.grid.GridPanel({

                store: dataStore,

                colModel: new Ext.grid.ColumnModel({
                    columns: [
                        selectorModel,
                        { id: 'ID', width: 80, hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_14;E=js}', hideable: false, width: 250, sortable: false, dataIndex: 'Name', renderer: renderServer },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_15;E=js}', hidden: isHiddenColumn('CredentialName'), width: 180, sortable: false, dataIndex: 'CredentialName', renderer: renderText },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_16;E=js}', hidden: isHiddenColumn('HostStatus'), width: 160, sortable: false, dataIndex: 'HostStatus', renderer: renderStatus },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_17;E=js}', hidden: isHiddenColumn('PollingMethod'), width: 140, sortable: false, dataIndex: 'PollingMethod', renderer: renderPollingMethod },
                        { header: '@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_PollingSource;E=js}', hidden: isHiddenColumn('VCenterPollingSource'), width: 140, sortable: false, dataIndex: 'VCenterPollingSource', renderer: renderPollingSource },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_18;E=js}', hidden: isHiddenColumn('VMwareProductName'), width: 180, sortable: false, dataIndex: 'VMwareProductName', renderer: renderText },
                        { header: '@{R=VIM.Strings;K=VIMWEBJS_TM0_19;E=js}', hidden: isHiddenColumn('VMwareProductVersion'), width: 130, sortable: false, dataIndex: 'VMwareProductVersion', renderer: renderText }
                    ],
                    listeners: {
                        hiddenchange: function (columnModel, columnIndex, hidden) {
                            saveGridConfig();
                        }
                    }
                }),

                sm: selectorModel,

                viewConfig: {
                    forceFit: false
                },

                //width has to be smaller here than div width on page
                //grid will be resized when it's rendered
                width: 680,
                height: 360,
                stripeRows: true,

                listeners: {
                    rowmousedown: function (grid, index, e) {
                        if (e.target.className == "tree-grid-expander-expand"
                            || e.target.className == "tree-grid-expander-collapse") {
                            selectorModel.lock();
                        }
                    }
                },

                tbar:
                [
                    {
                        id: 'AssignButton',
                        text: '@{R=VIM.Strings;K=VIMWEBJS_TM0_20;E=js}',
                        tooltip: '@{R=VIM.Strings;K=VIMWEBJS_TM0_21;E=js}',
                        iconCls: 'assign',
                        handler: function () {
                            if (VIMIsDemoMode()) return VIMDemoModeMessage();
                            assignCredential();
                        }
                    }, '-',
                    {
                        id: 'EnableButton',
                        text: '@{R=VIM.Strings;K=VIMWEBJS_TM0_22;E=js}',
                        iconCls: 'enablePolling',
                        handler: function () {
                            if (VIMIsDemoMode()) return VIMDemoModeMessage();
                            updateVMwarePollingStatus(true, function (result) {
                                var nodeIds = getSelectedNodeIds(grid, false);
                                grid.getStore().reload();
                                reTestCredentials(nodeIds, false);
                                grid.getView().refresh();
                            });
                        }
                    }, '-',
                    {
                        id: 'DisableButton',
                        text: '@{R=VIM.Strings;K=VIMWEBJS_TM0_23;E=js}',
                        iconCls: 'disablePolling',
                        handler: function () {
                            if (VIMIsDemoMode()) return VIMDemoModeMessage();
                            updateVMwarePollingStatus(false, function (result) {
                                grid.getStore().reload();
                            });
                        }
                    }, '-',
                    {
                        id: 'PollingSourceButton',
                        text: '@{R=VIM.Strings;K=VirtualizationPollingSettingsPage_PollingSource;E=js}',
                        iconCls: 'pollThrough',
                        menu: {
                            items: [
                                {
                                    id: 'PollingSourceBasicButton',
                                    text: '@{R=VIM.Strings;K=PollingSource_Vanilla;E=js}',
                                    iconCls: '', //todo
                                    handler: function () {
                                        if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                        updateNodePollingSource(0, PollingSourceChangeCallback);
                                    }
                                }, {
                                    id: 'PollingSourceVmanOrionButton',
                                    text: '@{R=VIM.Strings;K=PollingSource_LicenseBased;E=js}',
                                    iconCls: '', //todo
                                    handler: function () {
                                        if (VIMIsDemoMode()) return VIMDemoModeMessage();
                                        updateNodePollingSource(2, PollingSourceChangeCallback);
                                    }
                                }]
                        }
                    }
                ],

                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=VIM.Strings;K=VIMWEBJS_TM0_27;E=js}',
                    emptyMsg: "@{R=VIM.Strings;K=VIMWEBJS_TM0_28;E=js}",
                    beforePageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_27;E=js}",
                    afterPageText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_28;E=js}",
                    firstText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_29;E=js}",
                    prevText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_30;E=js}",
                    nextText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_31;E=js}",
                    lastText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_32;E=js}",
                    refreshText: "@{R=VIM.Strings;K=VIMWEBJS_VB0_33;E=js}",
                    items: [
                        new Ext.form.Label({ text: '@{R=VIM.Strings;K=VIMWEBJS_TM0_30;E=js}', style: 'margin-left: 5px; margin-right: 5px' }),
                        new Ext.form.ComboBox({
                            regex: /^\d*$/,
                            store: new Ext.data.SimpleStore({
                                fields: ['pageSize'],
                                data: [[10], [20], [30], [40], [50]]
                            }),
                            displayField: 'pageSize',
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus: true,
                            width: 50,
                            editable: false,
                            value: pageSizeNum,
                            listeners: {
                                'select': function (c, record) {
                                    grid.bottomToolbar.pageSize = record.get("pageSize");
                                    grid.bottomToolbar.cursor = 0;
                                    ORION.Prefs.save('PageSize', grid.bottomToolbar.pageSize);
                                    grid.bottomToolbar.doRefresh();
                                }
                            }
                        })
                    ]
                })

            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });


            grid.render('Grid');

            var fudgeFactor = 12;
            var groupItemsHeight = $("#Grid").height() - $(".GroupSection").height() - fudgeFactor;
            $(".GroupItems").height(groupItemsHeight);

            // Set the width of the grid
            grid.setWidth($('#gridCell').width());

            RefreshObjects();
            UpdateToolbarButtons();

            //FB114398 'VMware Settings' page : can't change 'Number of items per page'
            //work around IE9 issue - get the method body from EXT 3.4
            if ((typeof Range !== 'undefined') && !Range.prototype.createContextualFragment) {
                Range.prototype.createContextualFragment = function (html) {
                    var div = document.createElement("div"),
                        fragment = document.createDocumentFragment(),
                        i = 0,
                        length, childNodes;

                    div.innerHTML = html;
                    childNodes = div.childNodes;
                    length = childNodes.length;

                    for (; i < length; i++) {
                        fragment.appendChild(childNodes[i].cloneNode(true));
                    }

                    return fragment;
                };
            }

            $("form").submit(function () { return false; });
        }
    };

}();



Ext.onReady(SW.VIM.VMwareServers.init, SW.VIM.VMwareServers);
