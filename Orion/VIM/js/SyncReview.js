﻿Ext.namespace('SW');
Ext.namespace('SW.VIM');

SW.VIM.SyncReview = function () {
    var selectorModel;
    var dataStore;
    var grid;
    var initialized;
    var selectedElementId;
    var availableLicenseCount = 0;
    var showAvailableLicenseCount = true;

    var loadingMask = null;

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: "@{R=VIM.Strings;K=VIMWEBJS_JH0_49;E=js}"
            });
        }

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {};
        grid.store.baseParams = {};
        grid.store.on('load', function (store, records, options) {
            loadingMask.hide();

            var selected = new Array();
            for (var i = 0; i < store.data.length; i++) {
                if (!records[i].data.IsDisabled) {
                    selected.push(records[i]);
                }
            }

            grid.getSelectionModel().selectRecords(selected);
        });

        grid.store.load();
    };

    function renderName(value, meta, record, rowIndex) {
        if (!record.data.IsRoot)
            value = '<span style="padding-left: 20px;">' + value + '</span>';

        return renderDefault(value, meta, record);
    }

    function renderDefault(value, meta, record) {
        if (typeof (value) == 'undefined')
            return '';

        return value;
    }

    function renderTask(value, meta, record) {
        if (typeof (value) == 'undefined')
            return '';

        return "<span style='white-space:normal'>" + value + "</span>";
    }


    function fillSelection() {
        var result = new Array();

        for (var i = 0; i < dataStore.data.items.length; i++) {
            if (!dataStore.data.items[i].data.IsDisabled) {
                result.push(dataStore.data.items[i].data.Id);
            }
        }
    }

    function recalculateLicenseCount() {
        var licenseCount = 0;

        if (dataStore && dataStore.data && dataStore.data.items) {
            for (var i = 0; i < dataStore.data.items.length; i++) {
                var record = dataStore.data.items[i];

                if (!record.data.IsDisabled && record.data.IsNewNode) {
                    licenseCount++;
                }
            }
        }

        $(".sw-vim-sync-review-licenses-count").text(licenseCount);
        checkLicenseCountExceeded(licenseCount);
    }

    function checkLicenseCountExceeded(licenseCount) {
        var btnNext = $(".sw-vim-wizard-btn-next");

        if (availableLicenseCount >= licenseCount) {
            if (btnNext.hasClass("x-item-disabled")) {
                $(".sw-vim-sync-review-licenses").removeClass("warn");
                btnNext.prop("disabled", false).removeClass("x-item-disabled");
                btnNext.off("click");
            }
        }
        else {
            if (!btnNext.hasClass("x-item-disabled")) {

                $(".sw-vim-sync-review-licenses").addClass("warn");
                btnNext.prop("disabled", true).addClass("x-item-disabled");
                btnNext.on("click", function () {
                    Ext.Msg.alert("@{R=VIM.Strings;K=VIMWEBJS_JH0_50;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_51;E=js}");
                    return false;
                });
            }
        }
    }

    ORION.prefix = "VIM_SyncReviewGrid_";

    return {
        init: function () {

            if (initialized)
                return;

            initialized = true;

            selectorModel = new Ext.sw.VIM.grid.CheckboxSelectionModel(
            {
                isDisabledCallback: function (record) {
                    return (record.data.IsRoot != true) || (record.data.Id == '00000000-0000-0000-0000-000000000000');
                }
            });

            selectorModel.addListener('rowselect', function (selModel, rowIndex, record) {
                record.data.IsDisabled = false;

                var selected = $("#" + selectedElementId).val();
                if (selected.indexOf(record.data.Id) < 0) {
                    $("#" + selectedElementId).val(selected + record.data.Id + ",");
                }

                recalculateLicenseCount();
            });

            selectorModel.addListener('rowdeselect', function (selModel, rowIndex, record) {
                record.data.IsDisabled = true;

                var selected = $("#" + selectedElementId).val();
                $("#" + selectedElementId).val(selected.replace(record.data.Id + ",", ""));

                recalculateLicenseCount();
            });

            dataStore = new ORION.WebServiceStore(
                            "/Orion/VIM/Services/VManSynchronizationWizard.asmx/GetReviewDataTable",
                            [
                                { name: 'Id', mapping: 0 },
                                { name: 'EntityName', mapping: 1 },
                                { name: 'Type', mapping: 2 },
                                { name: 'Task', mapping: 3 },
                                { name: 'IsRoot', mapping: 4 },
                                { name: 'IsDisabled', mapping: 5 },
                                { name: 'IsNewNode', mapping: 6 }
                            ],
                            "Name");

            dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: "@{R=VIM.Strings;K=VIMWEBJS_JH0_68;E=js}",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK,
                    minWidth: 596
                });

                loadingMask.hide();
            });

            dataStore.addListener("load", function (store, records, options) {
                grid.getView().updateHeaders();

                fillSelection();
                recalculateLicenseCount();
            });

            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                    selectorModel,
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_52;E=js}', width: 40, hidden: true, hideable: false, sortable: false, dataIndex: 'ID', menuDisabled: true },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_53;E=js}', width: 300, hideable: false, sortable: false, dataIndex: 'EntityName', renderer: renderName, menuDisabled: true },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_54;E=js}', width: 200, hideable: false, sortable: false, dataIndex: 'Type', renderer: renderDefault, menuDisabled: true },
                    { header: '@{R=VIM.Strings;K=VIMWEBJS_JH0_55;E=js}', id: 'Task', width: 400, sortable: false, hideable: false, dataIndex: 'Task', renderer: renderTask, menuDisabled: true }
                ],
                sm: selectorModel,
                stripeRows: true,
                autoExpandColumn: 'Task',
                autoExpandMax: 2000,
                enableColumnMove: false,  // turn off column reorder drag drop
                enableRowHeightSync: true, // turn ON sync of locked and non locked row heights
                deferRowRender: false,
                viewConfig: {
                    emptyText: '@{R=VIM.Strings;K=VIMWEBJS_JH0_56;E=js}',
                    markDirty: false
                }
            });

            var panel = new Ext.Container({
                renderTo: 'sw-vim-sync-review-grid',
                layout: 'fit',
                height: SW.VIM.Utils.GetManagementGridHeight(),
                items: [grid]
            });

            Ext.EventManager.onWindowResize(function () {
                panel.setWidth(1);
                panel.setWidth($('#sw-vim-sync-review-grid').width());

                panel.setHeight(SW.VIM.Utils.GetManagementGridHeight());
                panel.doLayout();
            }, panel);

            RefreshObjects();
        },

        Configure: function (options) {
            if (!options) return;

            if (options.selectedElementId != null) {
                selectedElementId = options.selectedElementId;
            }
            else {
                throw "SelectedElementId must be set.";
            }
            if (options.availableLicenseCount != null) {
                availableLicenseCount = options.availableLicenseCount;
            }
            if (options.showAvailableLicenseCount != null) {
                showAvailableLicenseCount = options.showAvailableLicenseCount;
            }

            recalculateLicenseCount();

            if (showAvailableLicenseCount) {
                if (availableLicenseCount > 2000000) {
                    //trial version can have unlimited license count
                    $(".sw-vim-sync-review-available-licenses").html("@{R=VIM.Strings;K=VIMWEBJS_JH0_57;E=js} <span style='font-size:16px;position:relative;top:2px;'>&infin;</span><br />@{R=VIM.Strings;K=VIMWEBJS_JH0_58;E=js}");
                } else {
                    $(".sw-vim-sync-review-available-licenses").html("@{R=VIM.Strings;K=VIMWEBJS_JH0_57;E=js} " + Ext.util.Format.number(availableLicenseCount, "0,000") + "<br />@{R=VIM.Strings;K=VIMWEBJS_JH0_58;E=js}");
                }
            }
            
        }
    };
} ();

Ext.onReady(SW.VIM.SyncReview.init, SW.VIM.SyncReview);
