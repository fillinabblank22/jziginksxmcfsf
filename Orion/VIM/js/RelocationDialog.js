﻿SW = SW || {};
SW.VIM = SW.VIM || {};



SW.VIM.RelocationDialog = function (actionType, actionName, titleName, vmName, vmId, hostId, platform, cancelHandler, status, defaultHyperVRelocationPath) {

    var datastores; 

    var pageSize = 12;
    
    titleName = SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_VZ0_16;E=js} ', vmName);

    function successCallback(result) {
        datastores = result;
        if (!datastores || datastores.length == 0) {
            var dialogcontent = $("#sw-vim-dialogcontent");
            dialogcontent.empty();
            dialogcontent[0].innerHTML = "@{R=VIM.Strings;K=VIMWEBJS_VZ0_12;E=js}";
        } else {
            showTable(0);
        }
    }
    
    function getReadableSizeString(sizeInBytes) {

        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            sizeInBytes = sizeInBytes / 1024;
            i++;
        } while (sizeInBytes > 1024);

        return Math.max(sizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };
    
    function showTable(newPage) {
        if (newPage < 0 || newPage >= Math.ceil(datastores.length / pageSize))
            return;
        var curPage = newPage;
        var dialogcontent = $("#sw-vim-dialogcontent");
        dialogcontent.empty();
        var vmIcon = '<img src="/Orion/StatusIcon.ashx?size=small&entity=' + 'Orion.VIM.VirtualMachines.' + platform + '&status=' + status + '">';
        var tableString = SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_JS0_8;E=js}', vmIcon, '<b> ' + vmName + '</b>') + '<br/><br/><table border="0" cellpadding="2" cellspacing="0" width="100%">';
        var end = (curPage + 1) * pageSize < datastores.length ? (curPage + 1) * pageSize : datastores.length;
        var begin = curPage * pageSize;
        var selected = false;
        for (var i = begin; i < end; i++) {

        var freeSpace = getReadableSizeString(datastores[i].DataStoreModel.FreeSpace);

        var percent = 0;
        if (datastores[i].DataStoreModel.Capacity > 0) {
            percent = (datastores[i].DataStoreModel.Capacity - datastores[i].DataStoreModel.FreeSpace) * 100 / datastores[i].DataStoreModel.Capacity;
        }


        var freeSpaceMessage =
            '<span style="float:right; margin-top:3px;">' +
                '<span class="sw-vim-action-storageMotion-capacityValue">' +
                    '&nbsp;' + SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_LM0_3;E=js}', freeSpace) + '&nbsp;' +
                '</span> ' +
                '<span class="BarBackground sw-vim-action-storageMotion-capacityBar">' +
                    '<div class="sw-vim-action-storageMotion-capacityBar_fill" style="width:' + percent + 'px;">&nbsp;</div>' +
                '</span>' +
            '</span>';

        var selectedStr = "";
        if (datastores[i].IsSelected && !selected) {
            selected = true;
            selectedStr = " checked='checked'";
        }

        tableString += "<tr><td class='Property vimTopXXProperty vimStorageMigration'><input type='radio' style='float:left;' onclick='SW.VIM.RelocationDialog.setEditorValue()' name='migrationitem' value='" + i + "'" + selectedStr + ">" +
            "<span class='entityIconBox' style='float:left;'><img src='/Orion/StatusIcon.ashx?entity=Orion.VIM.Datastores&status=" + datastores[i].DataStoreModel.OrionStatus + "' /></span><span style='float:left; margin-top:3px;'><a href='" + datastores[i].DataStoreModel.DetailsUrl + "' target='_blank'>" + datastores[i].DataStoreModel.Name + " " + (datastores[i].IsSelected ? "<span class='sw-vim-action-storageMotion-current'>@{R=VIM.Strings;K=VIMWEBJS_TK0_10;E=js}</span> " : "") + "</a></span>" + freeSpaceMessage + "</td></tr>";
        }

        if (pageSize < datastores.length) {
            tableString += '<tr><td>';
            if (curPage > 0) {
                tableString += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-first.gif" onclick="SW.VIM.RelocationDialog.showTable(0)">';
                tableString += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-prev.gif" onclick="SW.VIM.RelocationDialog.showTable(' + (curPage - 1) + ')">';
            }
            
            tableString += SW.Core.String.Format('@{R=VIM.Strings;K=VIMWEBJS_SH0_1;E=js}', 
                '<input style="text-align:right; width:40px;" id="sw-vim-relocationPage" type="number" min="1" max="' + Math.ceil(datastores.length / pageSize) + '" size="1" value="' + (curPage + 1) + '" onchange="SW.VIM.RelocationDialog.showTable(($(\'#sw-vim-relocationPage\').val()-1))" >',
                Math.ceil(datastores.length / pageSize)
            );
            
            if (curPage < Math.ceil(datastores.length / pageSize)-1) {
                tableString += '<input type="image" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-next.gif" onclick="SW.VIM.RelocationDialog.showTable(' + (curPage + 1) + ')">';
                tableString += '<input type="image" name="submit" style="vertical-align:middle;" src="/Orion/VIM/images/Arrows/page-last.gif" onclick="SW.VIM.RelocationDialog.showTable(' + (Math.ceil(datastores.length / pageSize) - 1) + '); ">';
            }

            tableString += '</td></tr>';
        }
        tableString += '</table>';
        dialogcontent[0].innerHTML = tableString;
        setEditorValue();
    }
    
    SW.VIM.RelocationDialog.showTable = function (newPage) {
        showTable(newPage);
    };


    function failCallback(message) {
        $("#sw-vim-dialogcontent").empty();
        $('#sw-vim-dialogcontent').append(message);
    }

    SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetRelocateDestinations", { vmId: vmId },
        successCallback, failCallback);

    var dialog;

    function onClose() {
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
    }

    function onRelocate() {
        var index = $('input[name=migrationitem]:checked').val();
        var dest;
        var isValid = true;
        if (platform == 'HyperV') {
            // For HyperV we can choose custom location
            if ($('#sw-vim-customlocation')[0].checked && $('#sw-vim-customlocationpath').val().length > 0) {
                isValid = validateInput();
                dest = $("#sw-vim-customlocationconst:first").html() + $('#sw-vim-customlocationpath:first').val() + "\\" + vmName;
            } else {
                dest = datastores[index].DataStoreModel.ManagedObjectID + "\\" + defaultHyperVRelocationPath + "\\" + vmName;
            }
        } else {
            dest = datastores[index].DataStoreModel.ManagedObjectID;
        }
        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
        if (isValid) {
            SW.VIM.ManagementActionDialog(actionType, actionName, titleName, vmId, "", 2, false, [dest, datastores[index].DataStoreModel.ID, datastores[index].DataStoreModel.Name], vmName);
        }
    }
    
    function validateInput() {
        var specificPathInput = $("#sw-vim-customlocationpath").val();
        var pattern = /^([A-Za-z_\-\s0-9\.\\\^\&\'\@\{\}\[\]\,\$\=\!\#\(\)\%\+\~]+)+$/;
        var isValidPath = pattern.test(specificPathInput);
        if (!isValidPath) {
            showErrorMessage("@{R=VIM.Strings;K=VIMWEBJS_RB0_2;E=js}");
        }
        
        return isValidPath;
    }

    function showErrorMessage(errorText) {
        var errorBox = $('.storage-motion-error');
        errorBox.outerWidth($('.sw-vim-vmmanag-ui-dialog-content').width());
        errorBox.html(errorText);
        errorBox.show();
        dialog.animate({ height: height }, 100);
    }
    
    SW.VIM.RelocationDialog.setEditorValue = function () {
        setEditorValue();
    };

    function refreshEditor() {
        //$("#sw-vim-customlocationpath")[0].style.display = $('#sw-vim-customlocation')[0].checked ? "block" : "none";
        //$("#sw-vim-customlocationconst")[0].style.display = $('#sw-vim-customlocation')[0].checked ? "block" : "none";
        $("#sw-vim-customlocationspan")[0].style.display = $('#sw-vim-customlocation')[0].checked ? "block" : "none";
    }

    function setEditorValue() {
        if ($('#sw-vim-customlocation').length == 0)
            return;
        var index = $('input[name=migrationitem]:checked').val();
        $("#sw-vim-customlocationconst")[0].innerHTML = datastores[index].DataStoreModel.ManagedObjectID + "\\";
    }

    var textHtml =
        '<div>' +
            '<div id = "sw-vim-dialogcontent">' +
            '<img src="/Orion/images/AJAX-Loader.gif" />@{R=VIM.Strings;K=VIMWEBJS_VZ0_2;E=js}<br />' +
            '</div>' +
            '{1}' +
            '{0}' +
        '</div>';
    
    var enhancement = '';
    if (platform == 'HyperV') {
        enhancement = 
            '<input id="sw-vim-customlocation" type="checkbox"><label for="sw-vim-customlocation">@{R=VIM.Strings;K=VIMWEBJS_ZS0_12;E=js}</label><br>' +
            '<span id="sw-vim-customlocationspan" style="white-space:nowrap;display:none;">' +
                '<label for="sw-vim-customlocationpath" id="sw-vim-customlocationconst" style="float:left; line-height:22px;"></label>' +
                '<div style="overflow:hidden; padding: 0 4px 0 1px;"><input id="sw-vim-customlocationpath" type="text" value="' + defaultHyperVRelocationPath + '&#92" name="txt" style="width:95%;"></div>' +
            '</span >' +
            '<div class="storage-motion-error sw-vim-vmmanag-ui-dialog-error-panel" style="display: none; margin-top:5px;"></div>';
    }

    var dialogButtons = '<div style="float: right; font-size: 11px; padding-top: 12px">';
    dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_18;E=js}", { type: 'primary', id: 'sw-vim-btnYes' });
    dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_VZ0_7;E=js}", { type: 'secondary', id: 'sw-vim-btnNo' });
    dialogButtons += '</div>';

    textHtml = String.format(textHtml, dialogButtons, enhancement);


    var $dialog = $(textHtml);
    $dialog.find("#sw-vim-btnYes").click(onRelocate);
    $dialog.find("#sw-vim-btnNo").click(onClose);
    if (platform == 'HyperV') {
        $dialog.find("#sw-vim-customlocation").click(refreshEditor);
    }
    dialog = $dialog.dialog({
        width: 'auto',
        title: titleName,
        modal: true,
        resizable: false,
        open: function () {
            $('.ui-dialog-content').addClass('sw-vim-vmmanag-ui-dialog-content');
        },
        close: function () {
            if (typeof cancelHandler === 'function' && closeTrigger != "sw-vim-btnYes") {
                cancelHandler();
            }
            $(this).dialog('destroy').remove();
        }
    });
}
