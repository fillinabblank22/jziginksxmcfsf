﻿using System;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.StatusIcons;

public partial class Orion_VIM_UnmanagedNode : System.Web.UI.Page
{
    private string netObjectType;
    private int netObjectId;
    private string netObject;
    private bool _hasMultipleIPAddress;
    private string _manageNodeDialogParams;
    private string _manageNodeUrl;

    protected void Page_Load(object sender, EventArgs e)
    {
        var master = (Orion_VIM_VimView) Master;
        master.AllowViewTimestamp = false;

        netObject = Request.QueryString["NetObject"];

        if (netObject != null)
        {
            var arr = netObject.Split(':');

            if (arr.Length == 2 && Int32.TryParse(arr[1], out netObjectId))
            {
                netObjectType = arr[0];
            }

            InitManageNodeDialog();
        }
        else
        {
            ReportError();           
        }
    }

    protected string PageTitle
    {
        get
        {
            const string format = "{0} - <img src=\"/Orion/StatusIcon.ashx?entity={1}&amp;size=Small&id={2}&amp;status={3}\" /> {4}";

            switch (netObjectType)
            {
                case VimHost.Prefix:
                    var host = new HostDAL().GetHostModelByHostId(netObjectId);
                    return String.Format(format, Resources.VIMWebContent.VIMWEBDATA_LV0_44, VimIconProvider.CombineWithPlatform(VimIconProvider.HostEntityName, host.Platform), netObjectId, host.Status, host.Name);
                case VimVirtualMachine.Prefix:
                    var vm = new VirtualMachineDAL().GetVMModelByVMId(netObjectId);
                    return String.Format(format, Resources.VIMWebContent.VIMWEBDATA_LV0_45, VimIconProvider.CombineWithPlatform(VimIconProvider.VirtualMachineEntityName, vm.HostPlatform), netObjectId, vm.Status, vm.Name);
                default:
                    ReportError();
                    break;
            }
            return null;
        }
    }

    private void InitManageNodeDialog()
    {        
        var prefix = String.Empty;
        var address = String.Empty;
        int? engineId = -1;
        var hostPlatform = VirtualizationPlatform.Unknown;
        
        _hasMultipleIPAddress = false;

        switch (netObjectType)
        {
            case VimVirtualMachine.Prefix:
                prefix = "G";
                var vmDal = new VirtualMachineDAL();
                var vm = vmDal.GetVMModelByVMId(netObjectId);
                
                if (vm == null)
                    ReportError();

                address = string.IsNullOrEmpty(vm.IPAddress) ? HttpUtility.UrlEncode(vm.Name) : vm.IPAddress;
                engineId = vmDal.GetParentEngineId(netObjectId);
                hostPlatform = vm.HostPlatform;                      
                break;
            case VimHost.Prefix:
                prefix = "H";
                var hostDal = new HostDAL();
                var host = hostDal.GetHostModelByHostId(netObjectId);
                if (host == null)
                    ReportError();
                address = string.IsNullOrEmpty(host.IPAddress) ? host.Name : host.IPAddress;
                var list = new VMHostDetailsDAL().GetHostIpAddresses(netObjectId);
                
                if (list != null && list.Count() > 1)
                {
                    address = String.Join(";", list.ToArray());
                    _hasMultipleIPAddress = true;
                }

                engineId = hostDal.GetParentEngineId(netObjectId);
                hostPlatform = host.Platform;                                      
                
                break;
        }

        _manageNodeDialogParams = string.Format("'{0}', '{1}','{2}{3}', {4}, '{5}'", engineId ?? -1,
                                                address, prefix, netObjectId,
                                                _hasMultipleIPAddress.ToString().ToLower(), hostPlatform);

        _manageNodeUrl = String.Format("/Orion/Nodes/Add/Default.aspx?EngineID={0}&IPAddress={1}", engineId, HttpUtility.UrlPathEncode(address));

        if (hostPlatform == VirtualizationPlatform.Vmware)
            _manageNodeUrl = string.Concat(_manageNodeUrl, string.Format("&VMwareEntity={0}{1}", prefix, netObjectId));
    }

    protected bool HasMultipleIPAddress
    {
        get { return _hasMultipleIPAddress; }
    }

    protected string ManageNodeDialogParams
    {
        get { return _manageNodeDialogParams; }
    }

    protected string ManageNodeUrl
    {
        get { return _manageNodeUrl; }
    }

    private void ReportError()
    {
        throw new ErrorInspectorDetailsCarrier(String.Format(Resources.VIMWebContent.VIMWEBDATA_LV0_10, netObject), new ErrorInspectorDetails());
    }
 }