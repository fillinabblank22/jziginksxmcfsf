using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;

public partial class Orion_VIM_VIMVMwareSummary : VimViewPage
{
    public override string ViewType
    {
        get 
        {
            return "VIM VMware Summary"; 
        }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMAG_Views_VMwareSummary";
        }
    }
    protected override void OnPreInit(EventArgs e)
    {
        // Set the header "X-UA-Compatible" value to "IE=8"
        ModulePatchHelper.SetIECompatibilityRendering(8);

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
