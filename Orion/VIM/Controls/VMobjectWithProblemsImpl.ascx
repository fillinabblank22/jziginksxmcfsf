﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMobjectWithProblemsImpl.ascx.cs" Inherits="Orion_VIM_Resources_VMobjectWithProblemsImpl" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_AK0_35%>
                    </td>
                </tr>
            </table>
   </asp:Panel>
       <asp:Repeater runat="server" ID="resourceTable">
         <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader" style="padding-left: 20px !important;" colspan="2"><%= GetDisplayedVMObjectType(Resource.DisplayedVMObjectType).ToUpper()%></td>
<%if (this.Resource.ShowPlatform) {%>
                    <td class="ReportHeader" width="20%" ><%= Resources.VIMWebContent.VIMWEBDATA_LB0_3%></td>
                    <td class="ReportHeader" width="45%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_36%></td>
<% } else { %>
                    <td class="ReportHeader" width="65%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_36%></td>
<% } %>
                </tr>
         </HeaderTemplate>
         <ItemTemplate>
            <tr>
                <td width="30px"><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("HostStatus")%>' />
                </td>
                <td>
                      <a href="<%# Eval("detailsUrl") %>">
                         <%# SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(HttpContext.Current.Server.HtmlEncode(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown)))%>
                      </a>
                </td>
<%if (this.Resource.ShowPlatform) {%>
                <td class="Property vimProblemProperty">
                    <%# SolarWinds.VIM.Web.Helpers.PlatformHelper.GetVirtualizationTypeName(Eval("PlatformID"))%>
                </td>
<% } %>
                <td class="Property vimProblemProperty"><%# string.IsNullOrEmpty(Eval("Description").ToString())?"&nbsp;":HttpUtility.HtmlEncode(Eval("Description")) %></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
      </asp:Repeater>