﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSanDedupControl.ascx.cs" Inherits="Orion_VIM_Controls_VSanDedupControl" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Helpers" %>
<%@ Import Namespace="Resources" %>

<tr runat="server" id="DedupEnabledHeaderRow">
    <td class="PropertyHeader"><%= Resources.VIMWebContent.VsanInfo_DeduplicationAndCompression %></td>
    <td class="Property" runat="server" id="DedupEnabledValue"></td>
</tr>
<% if (this.HasCompressionAndDeduplicationEnabled()) { %>
<tr class="sw-vim-resource-datastore-usage-vsan">
    <td class="PropertyHeader PaddingLeft"><%= Resources.VIMWebContent.VsanInfo_DedupUsedBefore %></td>
    <td class="Property">
        <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_11, 
                            FormatHelper.FormatBytes(VSanInfo.DedupUsedSpaceBefore ?? 0, ByteDisplayValueFormat.Short),
                            FormatHelper.FormatBytes(VSanInfo.VSanCapacity, ByteDisplayValueFormat.Short)) %>
        <%= SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(VSanInfo.DedupBeforePercentage ?? 0, ThresholdType.SpaceUsage)%>
    </td>
</tr>
<tr class="sw-vim-resource-datastore-usage-vsan">
    <td class="PaddingLeft PropertyHeader"><%= Resources.VIMWebContent.VsanInfo_DedupUsedAfter %></td>
    <td class="Property">
        <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_11,
                FormatHelper.FormatBytes(VSanInfo.DedupUsedSpaceAfter ?? 0, ByteDisplayValueFormat.Short),
                FormatHelper.FormatBytes(VSanInfo.VSanCapacity, ByteDisplayValueFormat.Short)) %>
        <%= SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(VSanInfo.DedupAfterPercentage ?? 0, ThresholdType.SpaceUsage) %>
    </td>
</tr>
<tr class="sw-vim-resource-datastore-usage-vsan">
    <td class="PropertyHeader PaddingLeft"><%= Resources.VIMWebContent.VsanInfo_DedupRatio %></td>
    <td class="Property"><%= FormatHelper.FormatRatio(VSanInfo.DedupRatio ?? 0) %></td>
</tr>
<tr class="sw-vim-resource-datastore-usage-vsan">
    <td class="PropertyHeader PaddingLeft"><%= Resources.VIMWebContent.VsanInfo_DedupSavings %></td>
    <td class="Property"><%= FormatHelper.FormatBytes(VSanInfo.DedupSavings ?? 0, ByteDisplayValueFormat.Short) %></td>
</tr>
<% } %>
