﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollingWarning.ascx.cs" Inherits="Orion_VIM_Controls_PollingWarning" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<% if (DisplayAgentPollingWarning){ %>
<div class="sw-pg-hint-yellow">
   <div class="sw-pg-hint-body-info"><%= Resources.VIMWebContent.General_Polling_NodePolledByAgentWarningMessage %></div>
</div>
<% } %>