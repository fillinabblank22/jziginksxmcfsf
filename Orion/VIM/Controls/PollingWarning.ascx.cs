﻿using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.Resources;
using System;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.Common.Enums;

public partial class Orion_VIM_Controls_PollingWarning : System.Web.UI.UserControl
{
    private static readonly SolarWinds.Logging.Log Log = new SolarWinds.Logging.Log();

    protected bool IsDoublePolled { get; private set; }
    public string LocalizedPollingSource { get; private set; }
    public ViewType ViewType { private get; set; }
    public int EntityId { private get; set; }
    public PollingSource PollingSource { private get; set; }
    protected string DoublePollingMessage { get; private set; }
    protected bool DisplayAgentPollingWarning { get; private set; }
    public int? NodeId { private get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = false;
        LocalizedPollingSource = VimBaseResource.LocalizePollingSource(PollingSource);

        CheckAgentPolling();
    }

    private void CheckAgentPolling()
    {
        DisplayAgentPollingWarning = false;

        if (PollingSource != PollingSource.LicenseBased || !NodeId.HasValue)
        {
            return;
        }

        try
        {
            if ((ViewType == ViewType.vCenter || ViewType == ViewType.Host) &&
                new LicenseManager().IsVimFullyLicensed() && new NodesDAL().IsNodeAgent(NodeId.Value))
            {
                DisplayAgentPollingWarning = true;
                Visible = true;
            }
        }
        catch (Exception ex)
        {
            Log.ErrorFormat("Exception occured when trying to check entity for agent polling: {0}", ex);
        }
    }
}