﻿var assignDialog;
var testButton;
var credentialList;

function enableButtons() {
    $('#assignList').removeAttr('disabled');
    $('#assignList').removeClass('sw-btn-disabled'); // test button
    $('#assignDialogB1').removeClass('sw-btn-disabled'); // assign button
    $('#assignDialogB2').removeClass('sw-btn-disabled'); // cancel button
};

function disableButtons() {
    $('#assignList').attr('disabled', 'disabled');
    $('#assignList').attr('sw-btn-disabled'); // test button

    $('#assignDialogB1').addClass('sw-btn-disabled'); // assign button
    $('#assignDialogB2').addClass('sw-btn-disabled'); // cancel button 
};

function checkCredentials(description, username, password, confirm) {
    if ($.trim(String(description)) == '') {
        return "@{R=VIM.Strings;K=VIMWEBJS_AK0_1;E=js}";
    }

    if ($.trim(String(username)) == '') {
        return "@{R=VIM.Strings;K=VIMWEBJS_AK0_2;E=js}";
    }

    if (!(password === confirm) || $.trim(String(password)) == '') {
        return "@{R=VIM.Strings;K=VIMWEBJS_AK0_3;E=js}";
    }

    return "";
};

function encodeHTML(htmlText) {
    return $('<div/>').text(htmlText).html();
};

function setStatusMessage(message, succeeded) {
    var icon = (succeeded) ? 'polling.gif' : 'bad_credentials.gif';
    var color = (succeeded) ? '#00a000' : '#ce0000';
    var background = (succeeded) ? '#daf7cc' : '#facece';
    var text = (succeeded) ? '@{R=VIM.Strings;K=VIMWEBJS_AK0_5;E=js}' : '@{R=VIM.Strings;K=VIMWEBJS_AK0_4;E=js}';

    var img = $(String.format('<img src="/Orion/VIM/images/VMwarePollingStatus/{0}" />', icon)).css('padding', '2px 5px 2px 2px').css('vertical-align', 'middle');
    var table = $('<table>').append($('<tr>').append($('<td>').css('padding', '0px').css('vertical-align', 'middle').append(img).append(message)));
    var div = $('<div>').css('background-color', background).css('color', color).css('font-weight', 'bold').append(table);

    $("#assignDialogBody .Error").empty();
    $("#assignDialogBody .Error").append(div);
};

function loadCredentials() {
    var select = $('#assignList');
    select.empty();
    $("<option value='new'>&lt;@{R=VIM.Strings;K=VIMWEBJS_AK0_6;E=js}&gt;</option>").appendTo(select);
    SW.VIM.WebService.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "GetAllVMwareCredentials", {}, function (result) {
        credentialList = result;
        for (var i = 0; i < result.length; i++) {
            var name = encodeHTML((new String(result[i].Description)).substring(0, 64));
            $(String.format("<option value='{1}'>{0}</option>", name, i)).appendTo(select);
        }
    });
};

function updateVMwarePollingStatus(nodeID, enable, onSuccess) {
    SW.VIM.WebService.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "UpdateVMwarePollingStatus", { nodeIds: [nodeID], enable: enable }, onSuccess);
};

function showWaitMessage(text) {
    $("#assignDialogBody .Error").empty();
    var img = $('<img src="/Orion/VIM/images/AJAX-Loader.gif" />').css('padding', '2px 5px 2px 2px').css('vertical-align', 'middle');
    var table = $('<table>').append($('<tr>').append($('<td>').css('padding', '0px').css('vertical-align', 'middle').append(img).append(text)));
    var div = $('<div>').css('color', '#000000').css('font-weight', 'normal').append(table);
    $("#assignDialogBody .Error").append(div);
};

function TestCredential(nodeID, testButton) {
    if (testButton.hasClass('sw-btn-disabled')) {
        return;
    }

    $("#assignDialogBody .Error").empty();
    var description = $("#assignDescription").val();
    var select = $('#assignList');
    var credentialId = credentialList[select.val()] ? credentialList[select.val()].Id : 0;

    var username = $("#assignUsername").val();
    var password = $("#assignPassword").val();
    var confirm = $("#assignConfirm").val();

    var error = checkCredentials(description, username, password, confirm);
    if (error != '') {
        setStatusMessage(error, false);
        return;
    }

    // check for existing credential name
    if (credentialId == 0) {
        for (var i = 0; i < credentialList.length; i++) {
            if (credentialList[i].Description == description) {
                setStatusMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', false);
                enableButtons();
                return;
            }
        }
    }

    // polling...
    showWaitMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_9;E=js}');

    disableButtons();
    testButton.attr('disabled', 'disabled');
    testButton.addClass('sw-btn-disabled');

    SW.VIM.WebService.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "BeginTestCredentials", { nodeIds: [nodeID], credentialId: credentialId, username: username, password: password, updateStatus: false }, function (result) {
        var taskId = result;
        var proxy = $create(SolarWinds.VIM.js.OrionCoreScriptServiceProxy);
        proxy.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "GetTestCredentialResultsSummary", { taskId: taskId }, function (result) {
            enableButtons();
            testButton.removeAttr('disabled');
            testButton.removeClass('sw-btn-disabled');
            var succeeded = result.Succeeded;
            setStatusMessage((succeeded) ? '@{R=VIM.Strings;K=VIMWEBJS_AK0_5;E=js}' : '@{R=VIM.Strings;K=VIMWEBJS_AK0_4;E=js}', succeeded);
        });
    },
    function (message) {
        enableButtons();
        setStatusMessage(message, false);
    });
}

function AssignCredential(nodeID) {
    if ($('#assignDialogB1').hasClass('sw-btn-disabled')) {
        return;
    }

    var description = $("#assignDescription").val();
    var username = $("#assignUsername").val();
    var password = $("#assignPassword").val();
    var confirm = $("#assignConfirm").val();
    var select = $("#assignList");

    //TODO: select proper node here
    var nodeId = 1;

    var val = select.val();
    if (val === 'new') {
        // assign new credential
        var error = checkCredentials(description, username, password, confirm);
        if (error != '') {
            setStatusMessage(error, false);
            return;
        }

        // check for existing credential name
        for (var i = 0; i < credentialList.length; i++) {
            if (credentialList[i].Description == description) {
                setStatusMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_8;E=js}', false);
                return;
            }
        }

        showWaitMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_11;E=js}');

        SW.VIM.WebService.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "CreateAndAssignVMwareCredential", { nodeIds: [nodeID], description: description, username: username, password: password },
                                function (result) {
                                    updateVMwarePollingStatus(nodeID, true, function () {
                                        alert('@{R=VIM.Strings;K=VIMWEBJS_AK0_12;E=js}');
                                        window.location.reload(false);
                                        assignDialog.dialog("close"); ;
                                    });
                                },
                                function (message) {
                                    enableButtons();
                                    setStatusMessage(message, false);
                                });
    }
    else {

        showWaitMessage('@{R=VIM.Strings;K=VIMWEBJS_AK0_11;E=js}');
        // assign existing credential
        var credentialId = credentialList[select.val()].Id;
        SW.VIM.WebService.callWebService("/Orion/VIM/Services/VMwareServers.asmx", "AssignVMwareCredential", { nodeIds: [nodeID], credentialID: credentialId },
                                function () {
                                    updateVMwarePollingStatus(nodeID, true, function () {
                                        alert('@{R=VIM.Strings;K=VIMWEBJS_AK0_12;E=js}');
                                        window.location.reload(false);
                                        assignDialog.dialog("close"); ;
                                    });
                                },
                                function (message) {
                                    enableButtons();
                                    setStatusMessage(message, false);
                                });
    }

}

function InitializeTextboxesByCredSelection(select) {
    $("#assignDialogBody .Error").text('');
    var val = select.val();
    if (val === 'new') {
        $("#assignDialogBody input").val('').removeAttr("disabled");
    }
    else {
        $("#assignDialogBody input").attr("disabled", "disabled");
        $("#assignDescription").val(credentialList[select.val()].Description);
        $("#assignUsername").val(credentialList[select.val()].Username);
        $("#assignPassword").val('------');
        $("#assignConfirm").val('------');
    }
}

function ShowAssignCredentialDialog(nodeID) {
    $("#assignDialogBody input").val('');
    $("#assignDialogBody .Error").empty();
        
    if (!assignDialog) {
        var assignDialogBody = $("#assignDialogBody");

        var dialogButtons = '<br /><div style="float: right">';
        dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_AK0_10;E=js}", { type: 'primary', cls: 'assignButton', id: 'assignDialogB1' });
        dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_AK0_13;E=js}", { type: 'secondary', id: 'assignDialogB2' });
        dialogButtons += '</div>';

        $(dialogButtons).appendTo(assignDialogBody);

        assignDialog = assignDialogBody.dialog({ width: 410,
            modal: true,
            dialogClass: 'no-close',
            closeOnEscape: false,
            modal: true,
            resizable: false,
            title: $('#assignDialog div.x4-window-header').text().trim()
        });

        assignDialog.find("#assignDialogB1").click(function () {
            if (VIMIsDemoMode()) return VIMDemoModeMessage();
            AssignCredential(nodeID);
        });

        assignDialog.find("#assignDialogB2").click(function () {
            if (!$('#assignDialogB1').hasClass('sw-btn-disabled')) {
                assignDialog.dialog("close");
            }
        });

        var testButtonPlaceholder = assignDialogBody.find("#testButton");
        testButton = $(SW.Core.Widgets.Button('@{R=VIM.Strings;K=VIMWEBJS_AK0_7;E=js}', { type: 'secondary', id: 'btnTestButton' }));
        testButton.appendTo(testButtonPlaceholder).click(function() {
            if (VIMIsDemoMode()) return VIMDemoModeMessage();
            TestCredential(nodeID, testButton);
        });

        loadCredentials();
        var select = $("#assignList");
        select.val('new');

        select.change(function () {
            InitializeTextboxesByCredSelection(select);
        });

        select.change();
    }
    else {
        assignDialog.dialog("open");
        var select = $("#assignList");
        InitializeTextboxesByCredSelection(select);
    }
        
    return false;
};