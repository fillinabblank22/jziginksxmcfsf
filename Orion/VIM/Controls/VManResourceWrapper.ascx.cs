﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.Common.Enums;

public partial class Orion_VIM_Controls_VManResourceWrapper : System.Web.UI.UserControl, IResourceWrapper
{
    private ContentType _wrapperContentType = ContentType.Default;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return WrapperResourcePlaceHolder; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public virtual PlaceHolder HeaderButtons
    {
        get { return Wrapper.HeaderButtons; }
    }
    [PersistenceMode(PersistenceMode.Attribute)]
    [Obsolete("Use ManageButtonText instead of image.")]
    public string ManageButtonImage
    {
        get { return Wrapper.ManageButtonImage; }
        set { Wrapper.ManageButtonImage = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonTarget
    {
        get { return Wrapper.ManageButtonTarget; }
        set { Wrapper.ManageButtonTarget = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonId
    {
        get { return Wrapper.ManageButtonId; }
        set { Wrapper.ManageButtonId = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonText
    {
        get { return Wrapper.ManageButtonText; }
        set { Wrapper.ManageButtonText = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowEditButton
    {
        get { return Wrapper.ShowEditButton; }
        set { Wrapper.ShowEditButton = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowHeaderBar
    {
        get { return Wrapper.ShowHeaderBar; }
        set { Wrapper.ShowHeaderBar = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowManageButton
    {
        get { return Wrapper.ShowManageButton; }
        set { Wrapper.ShowManageButton = value; }
    }

    /// <summary>
    /// Wrapper content type which should be shown
    /// </summary>
    public ContentType WrapperContentType
    {
        get { return _wrapperContentType; }
        set { _wrapperContentType = value; }
    }

    public bool CanShowContent
    {
        get { return WrapperContentType == ContentType.Content; }
    }

    protected string Title
    {
        get
        {
            BaseResourceControl parent = GetParentResource();

            if (parent != null)
                return BaseResourceWrapper.ParseMacros(parent, parent.DisplayTitle);
            else
                return Resources.CoreWebContent.WEBCODE_AK0_54;
        }
    }

    protected string Heading { get; set; }
    protected string IconUrl { get; set; }
    protected string Description { get; set; }

    private ContentType DetermineContentType()
    {
        if (!LicenseManager.Instance.IsVimFullyLicensed())
        {
            return ContentType.LicenseVim;
        }

        var parent = GetParentResource();
        if (parent == null)
        {
            return ContentType.Content;
        }

        // check if there are any data in VMan for the requested view type and entity
        var viewDetectionHelper = new ViewDetectionHelper(parent);
        PollingSource? pollingSource = new NetObjectDAL().GetPollingSource(viewDetectionHelper.DetectedViewType, viewDetectionHelper.NodeId);
        bool isLicensedOnSumary = new LicenseManager().IsVimFullyLicensed() && viewDetectionHelper.DetectedViewType == ViewType.Summary;

        if (pollingSource == PollingSource.Vanilla)
        {
            if (new LicenseManager().IsVimFullyLicensed())
            {
                return ContentType.PolledByVim;
            }
        }
        return ContentType.Content;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (WrapperContentType == ContentType.Default)
        {
            WrapperContentType = DetermineContentType();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IconUrl = String.Empty;
        Description = String.Empty;

        switch (WrapperContentType)
        {
            case ContentType.Default:
                ShowContent();
                break;
            case ContentType.Content:
                ShowContent();
                break;
            case ContentType.NoData:
                ShowNoData();
                break;
            case ContentType.RestrictedAccess:
                ShowRestrictedAccess();
                break;
            case ContentType.PolledByVim:
                ShowPolledByVim();
                break;
            case ContentType.UnauthorizedRequest:
                ShowUnauthorizedRequest();
                break;
            case ContentType.ConnectionFailure:
                ShowConnectionFailure();
                break;
            case ContentType.LicenseVim:
                ShowLicenseVim();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected BaseResourceControl GetParentResource()
    {
        return GetParentResource(this);
    }

    protected BaseResourceControl GetParentResource(Control control)
    {
        if (control == null)
            return null;
        else if (control is BaseResourceControl && ((BaseResourceControl)control).Resource != null)
            return (BaseResourceControl)control;
        else
            return GetParentResource(control.Parent);
    }

    private void ShowContent()
    {
        NoContentPanel.Visible = false;
        WrapperResourcePlaceHolder.Visible = true;
    }

    private void ShowNoData()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_LC0_4;
        IconUrl = "/Orion/images/getting_started_36x36.gif";
        Description = Resources.VIMWebContent.VIMWEBCODE_LC0_5;

        NoContentPanel.Visible = true;
    }

    private void ShowRestrictedAccess()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_LC0_8;
        IconUrl = "/Orion/images/stop_32x32.gif";
        //IconUrl = "/Orion/VIM/images/not_available_icon40x40.png"; //It does not exist
        Description = Resources.VIMWebContent.VIMWEBCODE_LC0_9;

        NoContentPanel.Visible = true;
    }

    private void ShowPolledByVim()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_JH0_45;
        IconUrl = "/Orion/images/getting_started_36x36.gif";
        Description = Resources.VIMWebContent.VIMWEBCODE_JH0_46;

        NoContentPanel.Visible = true;
    }

    private void ShowUnauthorizedRequest()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_LC0_8;
        IconUrl = "/Orion/images/stop_32x32.gif"; //maybe there should be more suitable image ?
        Description = Resources.VIMWebContent.VIMWEBCODE_TK0_1;

        NoContentPanel.Visible = true;
    }

    private void ShowConnectionFailure()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_LC0_8;
        IconUrl = "/Orion/images/stop_32x32.gif"; //maybe there should be more suitable image ?
        Description = Resources.VIMWebContent.VIMWEBCODE_TK0_2;

        NoContentPanel.Visible = true;
    }

    private void ShowLicenseVim()
    {
        WrapperResourcePlaceHolder.Visible = false;

        Heading = Resources.VIMWebContent.VIMWEBCODE_JD0_0;
        IconUrl = "/Orion/images/getting_started_36x36.gif  ";
        if (new LicenseManager().IsVimFullyLicensed())
        {
            Description = Resources.VIMWebContent.VIMWEBCODE_JD0_4;
        }
        else
        {
            Description = Resources.VIMWebContent.VIMWEBCODE_JD0_1;
        }


        applyVirtualizationLicense.Visible = true;

        NoContentPanel.Visible = true;
    }

    public enum ContentType
    {
        /// <summary>
        /// Show default content decided by the wrapper
        /// </summary>
        Default,
        /// <summary>
        /// Show the content of the resource wrapper
        /// </summary>
        Content,
        /// <summary>
        /// Show no data content
        /// </summary>
        NoData,
        /// <summary>
        /// Show information that current user has restricted access
        /// </summary>
        RestrictedAccess,
        /// <summary>
        /// Show information that this entity is polled by VIM and user cannot see data from VMan.
        /// </summary>
        PolledByVim,
        /// <summary>
        /// Show information that integration with VMan is enabled, but credential sent by VIM are not valid (e.g. 2 VIM are integrated with the same one VMan)
        /// </summary>
        UnauthorizedRequest,
        /// <summary>
        /// Show warning that there is connection issue while trying to connect to Vman
        /// </summary>
        ConnectionFailure,
        /// <summary>
        /// Show information that by licensing VIM user will get more data.
        /// </summary>
        LicenseVim
    }
}