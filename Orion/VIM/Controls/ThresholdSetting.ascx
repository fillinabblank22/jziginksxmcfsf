<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdSetting.ascx.cs" Inherits="Orion_VIM_Controls_ThresholdSetting" %>
<tr class="header"><td colspan="6"><h2><asp:PlaceHolder runat="server" ID="ThresholdHeader" /></h2></td></tr>
<tr class="alternateRow">
    <td class="PropertyHeader" style="width: 32%;"><asp:PlaceHolder runat="server" ID="High" /></td>
    <td style="width: 8%"></td>
    <td class="Property" align="left" colspan="8">
        <asp:TextBox runat="server" ID="HighTextValue" Columns="5" />
        <asp:PlaceHolder runat="server" ID="HighMinMax" />
        <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator2" Display="Dynamic" controltovalidate="HighTextValue" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_9%>" ><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator1" Display="Dynamic" runat="server" ControlToValidate="HighTextValue" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>" MinimumValue="1" Type="Integer" ><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RangeValidator>
        <div class="vimThresholdDescription"><asp:PlaceHolder runat="server" ID="HighDesc" /></div>
    </td>
</tr>
<asp:PlaceHolder ID="warningPlaceholder" runat="server">
<tr>
    <td class="PropertyHeader"><asp:PlaceHolder runat="server" ID="Warning" /></td>
    <td ></td>
    <td class="Property" align="left" colspan="4">
        <asp:TextBox runat="server" ID="WarningTxtValue"  Columns="5" />
        <asp:PlaceHolder runat="server" ID="WarnMinMax" />
        <asp:RequiredFieldValidator runat="server" id="RequiredFieldValidator1" Display="Dynamic" controltovalidate="WarningTxtValue" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_10%>" ><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RequiredFieldValidator>
        <asp:CompareValidator runat="server" id="cmpNumbers" Display="Dynamic" controltovalidate="WarningTxtValue" controltocompare="HighTextValue" operator="LessThan" type="Integer" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_11%>" ><img src='/Orion/images/Small-Down.gif' alt=""/></asp:CompareValidator>
        <asp:RangeValidator ID="RangeValidator2" Display="Dynamic" runat="server" ControlToValidate="WarningTxtValue" Title="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>" MinimumValue="1" Type="Integer" ><img src='/Orion/images/Small-Down.gif' alt=""/></asp:RangeValidator>
    </td>
    <td colspan="3" width="230" ><asp:PlaceHolder runat="server" ID="WarnDesc" /></td>
</tr>
</asp:PlaceHolder>
