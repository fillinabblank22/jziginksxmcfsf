﻿using System;

namespace Orion.VIM.Controls
{
    public partial class AllowProperty : SolarWinds.Orion.Web.UI.ProfilePropEditUserControl
    {
        public override string PropertyValue { get; set; }

        protected void Page_Load(Object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var propValue = false;
                if (!String.IsNullOrEmpty(PropertyValue))
                {
                    if (!Boolean.TryParse(PropertyValue, out propValue))
                    {
                        propValue = false;
                    }
                }
                ctrAllowProperty.ClearSelection();
                ctrAllowProperty.SelectedValue = propValue.ToString();
            }

            if (Boolean.TrueString.Equals(ctrAllowProperty.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
            {
                PropertyValue = Boolean.TrueString;
            }
            else
            {
                PropertyValue = Boolean.FalseString;
            }
        }
    }
}