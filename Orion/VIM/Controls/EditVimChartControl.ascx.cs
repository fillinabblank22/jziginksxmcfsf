﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting;
using System.Globalization;

public partial class Orion_VIM_Controls_EditResourceControls_EditVMVareChart : BaseResourceEditControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!string.IsNullOrEmpty(Resource.Properties["SubTitle2"]))
            chartSubTitle2.Text = Resource.Properties["SubTitle2"];

        if (!string.IsNullOrEmpty(Resource.Properties["Period"]))
        {
            if (Resource.Properties["Period"].Contains("~"))
            {
                timePeriodControl.TimePeriodText = "Custom";
                string[] periods = Resource.Properties["Period"].Split('~');
                timePeriodControl.CustomPeriodBegin = DateTime.FromBinary(long.Parse(periods[0])).ToString(CultureInfo.CurrentCulture);
                timePeriodControl.CustomPeriodEnd = DateTime.FromBinary(long.Parse(periods[1])).ToString(CultureInfo.CurrentCulture);
            }
            else
            {
                timePeriodControl.TimePeriodText = Resource.Properties["Period"];
            }
        }
        else
        {
            timePeriodControl.TimePeriodText = Periods.DEFAULT_PERIOD_VALUE;
        }

        if (!string.IsNullOrEmpty(Resource.Properties["SampleSize"]))
        {
            sampleSizeControl.SampleSizeValue = Resource.Properties["SampleSize"];
        }
        else
        {
            sampleSizeControl.SampleSizeValue = "1H";
        }

        timePeriodControl.AttachSampleSizeControl(sampleSizeControl.SampleSizeListControl);
    }

    public override Dictionary<string, object> Properties
    {
        get {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (!Page.IsValid) return properties;

            if (timePeriodControl.TimePeriodText != "Custom")
            {
                properties.Add("Period", timePeriodControl.TimePeriodText);
            }
            else
            {
                try
                {
                    properties.Add("Period", Convert.ToDateTime(timePeriodControl.CustomPeriodBegin, CultureInfo.CurrentCulture).Ticks.ToString() + "~" + Convert.ToDateTime(timePeriodControl.CustomPeriodEnd, CultureInfo.CurrentCulture).Ticks.ToString());
                }
                catch
                {
                    // this happends when fileds are blank and default values have to be filled automaticlaly 
                    properties.Add("Period", DateTime.Today.Ticks.ToString() + "~" + (DateTime.Today.AddDays(1)).AddSeconds(-1).Ticks.ToString());
                }
            }

            properties.Add("SampleSize", sampleSizeControl.SampleSizeValue);
            properties.Add("SubTitle2", chartSubTitle2.Text);

            return properties;
        }
    }
}
