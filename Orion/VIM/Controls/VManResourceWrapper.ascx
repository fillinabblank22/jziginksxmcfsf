﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManResourceWrapper.ascx.cs" Inherits="Orion_VIM_Controls_VManResourceWrapper" %>
<%@ Register TagPrefix="vim" TagName="ManageNodeDialog" Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" %>
<orion:Include runat="server" Module="VIM" File="DemoServerHelper.js"/>
<orion:Include runat="server" Module="VIM" File="VManTooltip.js"/>
<orion:Include runat="server" Module="VIM" File="VIM.css"/>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <asp:Panel runat="server" ID="NoContentPanel">
            <script type="text/javascript">

                function ApplyVirtualizationLicense() {
                    window.location.href = '/ui/licensing/license-manager';
                    return false;
                }

            </script>
            <div class="sw-vman-gettingstarted">
                <div class="sw-vman-first">
                    <img src="<%= IconUrl %>" alt="<%= Heading %>" />
                    <h3><b><%= Heading %></b></h3>
                </div>

                <div>
                    <p>
                        <%= Description %>
                    </p>
                </div>
                <div class="sw-vman-buttons">
                    <orion:LocalizableButton ID="applyVirtualizationLicense" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_53 %>" DisplayType="Primary" runat="server" CausesValidation="false" OnClientClick="return ApplyVirtualizationLicense();" Visible="false" />
                </div>
            </div>
        </asp:Panel>
        <vim:ManageNodeDialog runat="server"/>
        <script type="text/javascript">
            $().ready(function () {
                SW.VMan.Tooltip.Init(".sw-vman-tooltip", {});
                <% if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                   { %>
                    if ($("#isDemoMode").length == 0) {
                        $('body').prepend('<div id="isDemoMode"></div>');
                    }
                <% } %>
            });
        </script>
        <asp:PlaceHolder runat="server" ID="WrapperResourcePlaceHolder" />
    </Content>
</orion:resourceWrapper>