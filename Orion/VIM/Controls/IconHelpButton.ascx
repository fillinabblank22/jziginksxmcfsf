<%@ Control Language="C#" ClassName="VimIconHelpButton" %>

<script runat="server">
    private const string _helpTemplate = "{0}/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm";

    public string HelpUrlFragment { get; set; }

    protected string HelpURL
    {
        get
        {
            return string.Format(_helpTemplate, SolarWinds.Orion.Web.DAL.WebSettingsDAL.HelpServer, HelpUrlFragment);
        }
    }
    
   
</script>

<a href="<%=HelpURL%>" class="vimHelpLink" target="_blank"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_132%></a>
