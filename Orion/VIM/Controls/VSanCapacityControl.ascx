﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VSanCapacityControl.ascx.cs" Inherits="Orion_VIM_Controls_VSanCapacityControl" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Models" %>

<asp:Repeater ID="SpaceSummariesRepeater" runat="server" ItemType="SolarWinds.VIM.Web.Models.VSanObjectSpaceSummary" >
    <HeaderTemplate>
        <tr>
            <td class="PropertyHeader" colspan="2"><%= Resources.VIMWebContent.VsanBreakdown_UsedCapacityBreakdown %></td>
        </tr>
    </HeaderTemplate>

    <ItemTemplate>
        <tr>
            <td class="PropertyHeader  PaddingLeft"><%# ToDisplayName(Item.ObjectType) %></td>
            <td class="Property"><%# FormatValue(Item.UsedCapacity, TotalUsed) %></td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
