﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_Controls_RecommendationsResourceWrapper : UserControl, IResourceWrapper
{
    private ContentType _wrapperContentType = ContentType.Default;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return WrapperResourcePlaceHolder; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public virtual PlaceHolder HeaderButtons {
        get { return Wrapper.HeaderButtons; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    [Obsolete("Use ManageButtonText instead of image.")]
    public string ManageButtonImage
    {
        get { return Wrapper.ManageButtonImage; }
        set { Wrapper.ManageButtonImage = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonTarget
    {
        get { return Wrapper.ManageButtonTarget; }
        set { Wrapper.ManageButtonTarget = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonId
    {
        get { return Wrapper.ManageButtonId; }
        set { Wrapper.ManageButtonId = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonText
    {
        get { return Wrapper.ManageButtonText; }
        set { Wrapper.ManageButtonText = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowEditButton
    {
        get { return Wrapper.ShowEditButton; }
        set { Wrapper.ShowEditButton = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowHeaderBar {
        get { return Wrapper.ShowHeaderBar; }
        set { Wrapper.ShowHeaderBar = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowManageButton {
        get { return Wrapper.ShowManageButton; }
        set { Wrapper.ShowManageButton = value; }
    }

    /// <summary>
    /// Wrapper content type which should be shown
    /// </summary>
    public ContentType WrapperContentType
    {
        get { return _wrapperContentType; }
        set { _wrapperContentType = value; }
    }

    public bool CanShowContent
    {
        get { return WrapperContentType == ContentType.Content; }
    }

    public bool IsComputationEnabled => SettingsDAL.GetSetting("Recommendations_IsComputationEnabled").SettingValue == 1;

    protected string Title
    {
        get
        {
            BaseResourceControl parent = GetParentResource();

            if (parent != null)
                return BaseResourceWrapper.ParseMacros(parent, parent.DisplayTitle);
            else
                return Resources.CoreWebContent.WEBCODE_AK0_54;
        }
    }

    protected string ImageTitle { get; set; }
    protected string ImageUrl { get; set; }
    protected string Description { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (WrapperContentType == ContentType.Default)
        {
            WrapperContentType = IsComputationEnabled ? ContentType.Content : ContentType.RecommendationsDisabled;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ImageTitle = String.Empty;
        ImageUrl = String.Empty;
        Description = String.Empty;

        switch (WrapperContentType)
        {
            case ContentType.Default:
            case ContentType.Content:
                ShowContent();
                break;
            case ContentType.RecommendationsDisabled:
                ShowNoData();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected BaseResourceControl GetParentResource()
    {
        return GetParentResource(this);
    }

    protected BaseResourceControl GetParentResource(Control control)
    {
        if (control == null)
            return null;
        else if (control is BaseResourceControl && ((BaseResourceControl)control).Resource != null)
            return (BaseResourceControl)control;
        else
            return GetParentResource(control.Parent);
    }

    private void ShowContent()
    {
        NoContentPanel.Visible = false;
        WrapperResourcePlaceHolder.Visible = true;
    }

    private void ShowNoData()
    {
        NoContentPanel.Visible = true;
        WrapperResourcePlaceHolder.Visible = false;

        ImageTitle = Resources.VIMWebContent.RecommendationsResourceWrapper_ImageTitle;
        ImageUrl = "/Orion/VIM/images/resource_recommendations_disabled.png";
        Description = String.Format(Resources.VIMWebContent.RecommendationsResourceWrapper_Description, "<a href='/ui/recommendations/settings/strategies'>", "</a>");
    }

    public enum ContentType
    {
        /// <summary>
        /// Show default content decided by the wrapper
        /// </summary>
        Default,
        /// <summary>
        /// Show the content of the resource wrapper
        /// </summary>
        Content,
        /// <summary>
        /// Show recommendations disabled content
        /// </summary>
        RecommendationsDisabled
    }
}