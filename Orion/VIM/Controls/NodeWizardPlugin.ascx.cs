﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Common;
using System.Web.Configuration;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.Threading;
using EO.Internal;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.ApiProxyFactory;
using SolarWinds.VIM.Base.Contract.Models;

public partial class Orion_VIM_Controls_NodeWizardPlugin : UserControl, INodePropertyPlugin
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private const long NonExistingVMCredentialID = 0;
    private const string PluginName = "VIMNodeWizardPlugin";
    private const string PlainPasswordSessionKey = "VIM.NodeManagement.PlainPassword";

    private List<CredentialInfo> vmCredentials;
    private IList<Node> nodes;
    private NodePropertyPluginExecutionMode mode;
    private Dictionary<string, object> propertyBag;
    private NodePropertyPluginWrapper plugin;

    #region Form values
    private long CredentialsId
    {
        get
        {
            long credentialId;
            if (!long.TryParse(ddlVMwareCredentials.SelectedValue, out credentialId))
                credentialId = NonExistingVMCredentialID;

            return credentialId;
        }
        set
        {
            ddlVMwareCredentials.SelectedValue = value.ToString();
            ddlVMwareCredentials_IndexChanged(null, null);
        }
    }

    public string Login
    {
        get { return tbLogin.Text; }
        set { tbLogin.Text = value; }
    }

    public string Password
    {
        get
        {
            return tbPassword.Text;
        }
        set
        {
            EncodePassword(value);
        }
    }

    public string ConfirmPassword
    {
        get
        {
            return tbPasswordConfirmation.Text;
        }
    }

    public string CredentialName
    {
        get
        {
            return tbCaption.Text;
        }
        set
        {
            tbCaption.Text = value;
        }
    }

    public string PlainPassword
    {
        get
        {
            return (string)Session[PlainPasswordSessionKey];
        }
        set
        {
            Session[PlainPasswordSessionKey] = value;
        }
    }

    public bool ChangeForMultipleNodes
    {
        get
        {
            return cbMultipleSelection.Checked;
        }
        set
        {
            cbMultipleSelection.Checked = value;
        }
    }

    public bool MultipleSelectionVisible
    {
        get
        {
            return cbMultipleSelection.Visible;
        }
        set
        {
            cbMultipleSelection.Visible = value;
        }
    }

    public bool VMwarePollingEnabled
    {
        get
        {
            return cbEnableVMwarePolling.Checked;
        }
        set
        {
            cbEnableVMwarePolling.Checked = value;
        }
    }

    public bool VMwarePollingEnabledCheckboxVisible
    {
        get
        {
            return cbEnableVMwarePolling.Visible;
        }
        set
        {
            cbEnableVMwarePolling.Visible = value;
        }
    }

    public bool EditControlsVisible
    {
        get
        {
            return credentialsTable.Visible;
        }
        set
        {
            credentialsTable.Visible = value;
        }
    }
    #endregion

    #region Controls events handlers
    protected void ddlVMwareCredentials_IndexChanged(object sender, EventArgs e)
    {
        SetCredentialsFromDropDown();
    }

    protected void imbtnValidateVMware_Click(object sender, EventArgs e)
    {
        Validate(false);
    }

    protected void cbEnableVMwarePolling_CheckedChanged(object sender, EventArgs e)
    {
        // Show either credentials or message for VMware entity polled through vCenter/ESX
        EditControlsVisible = VMwarePollingEnabled && !ExistingVMwareNode;
        SetExistingVMwareNodeMessage(VMwarePollingEnabled && ExistingVMwareNode);
    }

    protected void cbMultipleSelection_CheckedChanged(object sender, EventArgs e)
    {
        RefreshControlsEnabledState();
    }
    #endregion

    protected override void OnUnload(EventArgs e)
    {
        PlainPassword = string.Empty;

        base.OnUnload(e);
    }

    private IEnumerable<BaseValidator> GetAllValidators()
    {
        return new BaseValidator[]
        {
            this.RequiredCredentialNameValidator,
            this.RequiredUserNameValidator,
            this.RequiredPasswordValidator,
            this.RequiredConfirmPasswordValidator,
            this.CompareConfirmPasswordValidator
        };
    }

    private bool CheckAllValidators()
    {
        return GetAllValidators().All(v => v.IsValid);
    }

    private void ChangeAllValidatorsState(bool enabled)
    {
        foreach(var validator in GetAllValidators())
        {
            validator.Enabled = enabled;
        }
    }

    private bool IsMultipleNodes
    {
        get
        {
            return (nodes.Count > 1);
        }
    }

    /// <summary>
    /// The bool value if we are adding existing VMware host or virtual machine.
    /// </summary>
    private bool ExistingVMwareNode
    {
        get
        {
            if (propertyBag.ContainsKey("VIMExistingVMwareNode"))
                return Convert.ToBoolean(propertyBag["VIMExistingVMwareNode"]);
            else
                return false;
        }
        set
        {
            propertyBag["VIMExistingVMwareNode"] = value;
        }
    }

    /// <summary>
    /// The type of added VMware entity (Host or Virtual Machine).
    /// </summary>
    private VimEntityType ExistingVMwareNodeEntity
    {
        get
        {
            if (propertyBag.ContainsKey("VIMEntityType") && propertyBag["VIMEntityType"] is VimEntityType)
                return (VimEntityType)propertyBag["VIMEntityType"];
            else
                return VimEntityType.Host;
        }
        set
        {
            propertyBag["VIMEntityType"] = value;
        }
    }

    private void SetExistingVMwareNodeMessage(bool visible)
    {
        vmwareNodeTable.Visible = visible;

        if (ExistingVMwareNodeEntity == VimEntityType.Host)
            litVMwareNodeTableText.Text = Resources.VIMWebContent.VIMWEBCODE_TM0_2;
        else if (ExistingVMwareNodeEntity == VimEntityType.VirtualMachine)
            litVMwareNodeTableText.Text = Resources.VIMWebContent.VIMWEBCODE_TM0_3;
        else
            vmwareNodeTable.Visible = false;
    }

    /// <summary>
    /// Set the properties according to URL parameter.
    /// </summary>
    private void SaveVIMEntity()
    {
        if (propertyBag.ContainsKey("VIMExistingVMwareNode"))
            return;

        propertyBag["VIMExistingVMwareNode"] = false;
        string vimEntity = this.Page.Request.QueryString["VMwareEntity"];
        if (!String.IsNullOrEmpty(vimEntity))
        {
            vimEntity = vimEntity.Trim();

            string entityType = vimEntity.Substring(0, 1);
            switch (entityType)
            {
                case SolarWinds.VIM.Web.Controls.ManageNodeLink.HostPrefix:
                    propertyBag["VIMEntityType"] = VimEntityType.Host;
                    break;
                case SolarWinds.VIM.Web.Controls.ManageNodeLink.GuestPrefix:
                    propertyBag["VIMEntityType"] = VimEntityType.VirtualMachine;
                    break;
                default:
                    return;
            }

            int entityId = -1;
            if (int.TryParse(vimEntity.Remove(0, 1), out entityId))
                propertyBag["VIMEntityID"] = entityId;
            else
                return;
        }
        else
        {
            return;
        }

        propertyBag["VIMExistingVMwareNode"] = true;
    }


    /// <summary>
    /// Set password fields values to asterisks and store plain password to PlainPassword property
    /// </summary>
    private void EncodePassword(string value)
    {
        string passBlock = GetPassBlock(value);

        if (tbPassword.Text == tbPasswordConfirmation.Text)
        {
            tbPassword.Text = passBlock;
            this.tbPassword.Attributes["value"] = passBlock;
            tbPasswordConfirmation.Text = passBlock;
            this.tbPasswordConfirmation.Attributes["value"] = passBlock;

        }
        else
        {
            value = 
            tbPassword.Text = 
            this.tbPassword.Attributes["value"] = 
            tbPasswordConfirmation.Text =
            this.tbPasswordConfirmation.Attributes["value"] = "";
        }
        this.PlainPassword = value;
    }

    private string GetPassBlock(string value)
    {
        return (!string.IsNullOrEmpty(value)) ? string.Empty.PadLeft(value.Length, '*') : "";
    }

    /// <summary>
    /// Shows valiadtion result message
    /// </summary>
    private void SetUIValidationState(bool validationSucceeded, string text)
    {
        VMwareValidationTrue.Visible = validationSucceeded;
        VMwareValidationFalse.Visible = !validationSucceeded;
        VMwareValidationFailedMessage.Text = text;
    }

    /// <summary>
    /// Creates new credentails and returns it's ID
    /// </summary>
    /// <returns>New credentials ID</returns>
    private long CreateNewCredentials(string description, string username, string password)
    {
        Credential credential = new Credential()
        {
            Description = description,
            Username = username,
            Password = password
        };

        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.AddVMwareCredential(credential);
        }
    }

    /// <summary>
    /// Validate new credentials. 
    /// If credentials are valid, true is returned and existingCredentialId is set to credential ID.
    /// It can be NonExistingVMCredentialID if credentials are new or it can be an existing credential ID if entered credentials
    /// are the same as existing credentials.
    /// 
    /// If credentails are not valid, false is returned and errorMessage is set.
    /// </summary>
    private bool ValidateNewCredentials(string description, string username, string password, out string errorMessage,
        out long existingCredentialId)
    {
        errorMessage = string.Empty;
        existingCredentialId = NonExistingVMCredentialID;

        CredentialInfo candidate = vmCredentials.FirstOrDefault(c =>
            c.Description.Equals(description, StringComparison.CurrentCultureIgnoreCase));

        if (candidate == null)
            return true;

        long? credentialId = null;

        if ((candidate.Username == username))
        {
            credentialId = MatchCredential(description, username, password);
        }

        if (credentialId.HasValue)
        {
            existingCredentialId = credentialId.Value;
            return true;
        }

        errorMessage = Resources.VIMWebContent.VIMWEBCODE_TM0_7;
        return false;
    }

    private long? MatchCredential(string description, string username, string password)
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.MatchVMwareCredential(description, username, password);
        }
    }

    private void RefreshControlsEnabledState()
    {
        bool enabled = ChangeForMultipleNodes;
        bool predefinedCredentials = (CredentialsId != NonExistingVMCredentialID);
        bool enabledCredentials = enabled && !predefinedCredentials;

        tbCaption.Enabled = enabledCredentials;
        tbLogin.Enabled = enabledCredentials;
        tbPassword.Enabled = enabledCredentials;
        tbPasswordConfirmation.Enabled = enabledCredentials;
        ddlVMwareCredentials.Enabled = enabled;
        cbEnableVMwarePolling.Enabled = enabled;
        imbtnValidateVMware.Enabled = enabled;

        ChangeAllValidatorsState(enabledCredentials);
    }

    private void LoadData()
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            try
            {
                vmCredentials = proxy.Api.GetAllVMwareCredentials("Description", true);
            }
            catch (ApiProxyException ex)
            {
                _log.Error("VIM Business Layer is not available.", ex);
                return;
            }

            vmCredentials.Insert(0, new CredentialInfo() {Id = NonExistingVMCredentialID, Description = VIMWebContent.VIMWEBCODE_TM0_5});

            ddlVMwareCredentials.DataSource = vmCredentials;
            ddlVMwareCredentials.DataTextField = "Description";
            ddlVMwareCredentials.DataValueField = "ID";
            ddlVMwareCredentials.DataBind();

            isVimBusinessLayerReady.Value = bool.TrueString;
        }
    }

    private void SetCredentialsFromDropDown()
    {
        long credentialId = CredentialsId;
        if (credentialId == NonExistingVMCredentialID)
        {
            this.CredentialName = string.Empty;
            this.Login = string.Empty;
            this.Password = string.Empty;
            this.tbLogin.Enabled = true;
            this.tbPassword.Enabled = true;
            this.tbCaption.Enabled = true;
            this.tbPasswordConfirmation.Enabled = true;

            ChangeAllValidatorsState(true);

            return;
        }

        CredentialInfo selected = vmCredentials.First(c => c.Id == credentialId);
        this.CredentialName = selected.Description;
        this.Login = selected.Username;
        this.Password = "**********";

        this.tbLogin.Enabled = false;
        this.tbPassword.Enabled = false;
        this.tbCaption.Enabled = false;
        this.tbPasswordConfirmation.Enabled = false;

        ChangeAllValidatorsState(false);

        // If any error occured during initialization (e.g. the IP address cannot be resolved) 
        // then 'propertyBag' property isn't set.
        if (propertyBag != null)
        {
            propertyBag["credentialsId"] = credentialId;
        }
    }

    private bool IsVMWareNode(Node node)
    {
        List<string> vmEntities = new List<string>(new[] { OrionNodeConstants.VMVCenterEntityType, OrionNodeConstants.VMHostEntityType });
        return vmEntities.Contains(node.EntityType);
    }

    private bool IsNodeVmWareTopLevelEntity(Node node)
    {
        var hostDal = new HostDAL();
        return node.EntityType == OrionNodeConstants.VMVCenterEntityType ||
               (node.MachineType.Equals(Strings.EsxMachineType, StringComparison.InvariantCultureIgnoreCase) &&
                hostDal.IsHostPolledDirectly(node.ID));
    }

    private bool AnyHyperVNode(IEnumerable<Node> nodes)
    {
        return nodes.Any(n => n.MachineType == Strings.HyperVMachineType);
    }

    private bool IsNodeUnknownAndNotMappedToVM(Node node)
    {
        // Filter only orion nodes with machine type Unknown
        if (node.EntityType.Equals(OrionNodeConstants.NormalNodeEntityType, StringComparison.InvariantCultureIgnoreCase)
            && node.MachineType.Equals(Strings.UnknownMachineType, StringComparison.InvariantCultureIgnoreCase))
        {
            var vmDal = new VirtualMachineDAL();
            return !vmDal.IsVMByNodeId(node.ID);
        }

        // Not an node with machine type Unknown
        return false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        LoadData();
    }

    #region INodePropertyPlugin Members

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        this.nodes = nodes;
        this.propertyBag = pluginState;
        this.mode = mode;
        this.plugin = NodePropertyPluginManager.Plugins.Single(p => p.Name == PluginName);

        VMwarePollingEnabledCheckboxVisible = ((mode == NodePropertyPluginExecutionMode.EditProperies) ||
                                               (mode == NodePropertyPluginExecutionMode.WizChangeProperties));

        // refill password for new credentials
        if ((CredentialsId == NonExistingVMCredentialID)
        && ((string.IsNullOrEmpty(this.PlainPassword) && string.IsNullOrEmpty(this.Password)) || (this.PlainPassword != this.Password))
        && GetPassBlock(this.PlainPassword) != this.Password)
        {
            EncodePassword(this.Password);
        }


        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                if (!this.IsPostBack)
                {
                    object credentialsId;
                    if ((propertyBag.TryGetValue("credentialsId", out credentialsId)) &&
                        ((long)credentialsId != NonExistingVMCredentialID))
                    {
                        CredentialsId = (long)credentialsId;
                        SetCredentialsFromDropDown();
                    }

                    // Check if we want to manage existing Host or VM. If yes then save properties from
                    // URL parameter to 'propertyBag', check 'Poll for VMware' checkbox,
                    // hide the credentials settings and show info message
                    SaveVIMEntity();
                    if (ExistingVMwareNode)
                    {
                        plugin.Visible = true;
                        SetExistingVMwareNodeMessage(true);
                        EditControlsVisible = false;
                    }
                }
                VMwarePollingEnabled = plugin.Visible;
                break;

            case NodePropertyPluginExecutionMode.WizChangeProperties:
                if (!this.IsPostBack)
                {
                    object vmwarePollingEnabled;
                    if (propertyBag.TryGetValue("vmwarePollingEnabled", out vmwarePollingEnabled))
                        VMwarePollingEnabled = (bool)vmwarePollingEnabled;

                    object credentialsId;
                    if ((propertyBag.TryGetValue("credentialsId", out credentialsId)) &&
                        ((long)credentialsId != NonExistingVMCredentialID))
                    {
                        CredentialsId = (long)credentialsId;
                        SetCredentialsFromDropDown();
                    }

                    // If we want to manage existing Host or VM, hide the credentials settings and show info message
                    if (ExistingVMwareNode)
                    {
                        SetExistingVMwareNodeMessage(true);
                        EditControlsVisible = false;
                    }
                }
                break;

            case NodePropertyPluginExecutionMode.EditProperies:
                // show plugin but real plugin content is controlled by itself
                plugin.Visible = true;

                if (!this.IsPostBack)
                {
                    if (IsMultipleNodes)
                    {
                        MultipleSelectionVisible = true;
                        pluginTitle.Text = "&nbsp;" + String.Format(VIMWebContent.VIMWEBCODE_TM0_4, VIMWebContent.VIMWEBDATA_TM0_27);
                        RefreshControlsEnabledState();
                    }
                    else
                    {
                        // Show the checkbox just for top level entities and for orion nodes not mapped to virtual machine. Machine type Unknown will filter nodes mapped to Nutanix, Hyper-v and other known entities.
                        VMwarePollingEnabledCheckboxVisible = IsNodeVmWareTopLevelEntity(nodes.First()) || IsNodeUnknownAndNotMappedToVM(nodes.First());
                        if (!VMwarePollingEnabledCheckboxVisible)
                        {
                            VMwarePollingEnabled = false;
                            divContent.Visible = false;
                            return;
                        }
                    }

                    // even if there are multiple nodes, set values from first node only
                    Node node = nodes.First();
                    if (IsVMWareNode(node))
                    {
                        VMwarePollingEnabled = true;
                        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
                        {
                            CredentialsId = proxy.Api.GetVMwareCredentialIdForNode(node.ID);

                            if (CredentialsId > 0)
                            {
                                SetCredentialsFromDropDown();
                            }
                            else
                            {
                                // If this is ESX and we don't have credentials then we poll it through vCenter
                                ExistingVMwareNode = true;
                                ExistingVMwareNodeEntity = node.EntityType == OrionNodeConstants.VMHostEntityType ? VimEntityType.Host : VimEntityType.VirtualMachine;
                                SetExistingVMwareNodeMessage(true);
                            }
                        }                        
                    }
                    else
                    {
                        VMwarePollingEnabled = false;
                    }
                }

                EditControlsVisible = VMwarePollingEnabled && !ExistingVMwareNode;
                break;
        }
    }

    public bool Update()
    {
        long credentialsId = CredentialsId;
        if (credentialsId == NonExistingVMCredentialID && !ExistingVMwareNode)
        {
            switch (mode)
            {
                case NodePropertyPluginExecutionMode.WizDefineNode:
                    credentialsId = CreateNewCredentials(CredentialName, Login, PlainPassword);
                    break;

                // Create new credentials only if "Poll for VMware" is checked
                case NodePropertyPluginExecutionMode.WizChangeProperties:
                case NodePropertyPluginExecutionMode.EditProperies:
                    if (VMwarePollingEnabled)
                    {
                        string _1; // i dont care about message
                        long credId;
                        // I'll check , if the credentials aren't already in DB, but user was warned about it (and clicked yes)
                        if (ValidateNewCredentials(CredentialName, Login, PlainPassword, out _1, out credId))
                        {
                            if (credId == NonExistingVMCredentialID)
                            {
                                credentialsId = CreateNewCredentials(CredentialName, Login, PlainPassword);
                            }
                            else
                            {
                                credentialsId = credId;
                            }
                        }
                        else // user entered existing credentials, but with bad login information. ignore them. (use previous)
                        {
                            credentialsId = CredentialsId; //
                        }
                    }
                    break;
            }
        }

        switch (mode)
        {
            case NodePropertyPluginExecutionMode.WizDefineNode:
                propertyBag["vmwarePollingEnabled"] = plugin.Visible;
                propertyBag["credentialsId"] = credentialsId;
                break;

            case NodePropertyPluginExecutionMode.WizChangeProperties:
                if (VMwarePollingEnabled)
                {
                    if (ExistingVMwareNode)
                    {
                        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
                        {
                            proxy.Api.AssignNodeToVIMEntity(NodeWorkflowHelper.Node.ID, (VimEntityType)propertyBag["VIMEntityType"], (int)propertyBag["VIMEntityID"]);
                        }
                    }
                    else
                    {
                        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
                        {
                            Guid taskId = proxy.Api.ScheduleVMwareProbeJobs(new[] { NodeWorkflowHelper.Node.ID }, credentialsId, Login, PlainPassword, false);
                            BusinessLayerTaskStatus taskStatus;
                            do
                            {
                                taskStatus = proxy.Api.GetTaskStatus(taskId);
                                Thread.Sleep(500);
                            } while (taskStatus == BusinessLayerTaskStatus.Running);
                        }
                    }
                }
                break;

            case NodePropertyPluginExecutionMode.EditProperies:
                // Skip when VMwarePolling checkbox is not visible or there is any Hyper-V node
                if (VMwarePollingEnabledCheckboxVisible && !AnyHyperVNode(nodes))
                {
                    if (!IsMultipleNodes || ChangeForMultipleNodes)
                    {
                        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
                        {
                            if (VMwarePollingEnabled)
                            {
                                if (!ExistingVMwareNode)
                                {
                                    Guid taskId = proxy.Api.ScheduleVMwareProbeJobs(nodes.Select(n => n.ID).ToArray(), credentialsId, Login, PlainPassword, true);
                                    BusinessLayerTaskStatus taskStatus;
                                    do
                                    {
                                        taskStatus = proxy.Api.GetTaskStatus(taskId);
                                        if (taskStatus == BusinessLayerTaskStatus.Running)
                                        {
                                            Thread.Sleep(500);
                                        }
                                    } while (taskStatus == BusinessLayerTaskStatus.Running);
                                }
                            }
                            else
                            {
                                proxy.Api.RemoveVMwarePollingFromNodes(nodes.Select(n => n.ID).ToArray());
                            }
                        }
                    }
                }
                break;
        }
        return true;
    }

    public bool Validate()
    {
        return Validate(true);
    }

    public bool Validate(bool throwExceptions)
    {
        if (!this.CheckAllValidators())
        {
            if (throwExceptions)
            {
                throw new ArgumentException(VIMWebContent.VIMWEBCODE_VB0_51);
            }
            return false;
        }

        if ((!VMwarePollingEnabled) || (!plugin.Visible) || (ExistingVMwareNode)
        || ((mode == NodePropertyPluginExecutionMode.EditProperies) && IsMultipleNodes && !ChangeForMultipleNodes))
            return true;

        string validationText = string.Empty;
        bool valid = false;
        bool credentialsValid = true;

        // check if host with same IP already exists (FB30226)
        HostDAL dal = new HostDAL();
        var vimSettings = new VimSettings();
        var node = nodes.First();
        int? engineIdFilter = vimSettings.CheckDuplicatesAcrossEnginesOnDiscovery ? (int?) null : node.EngineID;

        if (!NodeSubTypeString.Agent.Equals(node.ObjectSubType) && dal.HostWithIPAlreadyExists(node.IpAddress, node.ID, engineIdFilter))
        {
            validationText = String.Format(Resources.VIMWebContent.VIMWEBCODE_TM0_6, node.IpAddress);
        }
        else
        {
            if (CredentialsId == NonExistingVMCredentialID)
            {
                long tmpId;
                credentialsValid = ValidateNewCredentials(CredentialName, Login, PlainPassword, out validationText,
                                                          out tmpId);

                if ((credentialsValid) && (tmpId != CredentialsId))
                    CredentialsId = tmpId;
            }

            if (credentialsValid)
            {
                try
                {
                    using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
                    {
                        Guid taskId = Guid.Empty;
                        switch (mode)
                        {
                            case NodePropertyPluginExecutionMode.WizDefineNode:
                            case NodePropertyPluginExecutionMode.WizChangeProperties:
                                // in this step there is always only one node
                                if (node.NodeSubType == NodeSubType.Agent)
                                {
                                    var ag = NodeWorkflowHelper.AgentCredential;
                                    if (ag == null || ag.AgentGuid == Guid.Empty || !ag.Plugins.Contains(Strings.AgentPluginId))
                                    {
                                        // first validation round when agent is not fully deployed yet
                                        // if started by TEST button, show validation error message
                                        if (!throwExceptions)
                                            SetUIValidationState(false, VIMWebContent.VIMWEBCODE_PV0_1);
                                        // but allow continue to agent deployment
                                        // validation will be repeated after it finishes
                                        return true;
                                    }

                                    // credential validation via agent
                                    taskId = proxy.Api.ScheduleVMwareCredentialsTestProbeAgentJobs(
                                        new[] {ag.AgentGuid},
                                        node.EngineID,
                                        CredentialsId, Login, PlainPassword);
                                }
                                else
                                {
                                    // direct credential validation
                                    taskId = proxy.Api.ScheduleVMwareCredentialsTestProbeIPJobs(
                                        new[] {node.IpAddress},
                                        node.EngineID,
                                        CredentialsId, Login, PlainPassword);
                                }
                                break;
                            case NodePropertyPluginExecutionMode.EditProperies:
                                taskId = proxy.Api.ScheduleVMwareCredentialsTestProbeJobs(nodes.Select(n => n.ID).ToArray(),
                                    CredentialsId, Login, PlainPassword,
                                    false);
                                break;
                        }

                        if (taskId != Guid.Empty)
                        {
                            BusinessLayerTaskStatus taskStatus;
                            do
                            {
                                taskStatus = proxy.Api.GetTaskStatus(taskId);
                                if (taskStatus == BusinessLayerTaskStatus.Finished)
                                {
                                    var results = proxy.Api.GetVMwareCredentialsTestProbeJobsResults(taskId);
                                    valid = (results.Count > 0 && results.All(pair => pair.Value == HostStatus.Polling));
                                }
                                else
                                {
                                    Thread.Sleep(500);
                                }
                            } while (taskStatus == BusinessLayerTaskStatus.Running);
                        }
                    }

                    if (!valid)
                        validationText = Resources.VIMWebContent.VIMWEBCODE_TM0_8;
                }
                catch (Exception ex)
                {
                    validationText = ex.Message;
                }
            }
        }

        SetUIValidationState(valid, validationText);

        if ((!valid) && (throwExceptions))
            throw new ArgumentException((mode == NodePropertyPluginExecutionMode.EditProperies) ? validationText : string.Empty);

        return true;
    }

    #endregion
}
