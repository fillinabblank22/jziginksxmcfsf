﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Common.Thresholds;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controllers;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.Model.Thresholds;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Models;
using Threshold = SolarWinds.Orion.Core.Common.Models.Thresholds.Threshold;
using VimModels = SolarWinds.VIM.Common.Models;
using ThresholdType = SolarWinds.Orion.Core.Common.Models.Thresholds.ThresholdType;
using SolarWinds.VIM.Common.Constants;
using SolarWinds.VIM.Web.Common.Extensions;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Common.Licensing;

public partial class Orion_VIM_Controls_ThresholdControl : System.Web.UI.UserControl
{    
    private static readonly Log _log = new Log();
    private IList<IBaseEntity> _entities;
    private DataTable ExistingThresholds;    
    private Dictionary<string, string> controlsIds = new Dictionary<string, string>();
    private string _entitySwisName;
    private VimEntityType _entityType;
    private List<VimModels.Threshold> _globalThresholds;

    private static readonly List<String> _vimThresholds = new List<String>()
        {
            "VIM.Clusters.Stats.MemUsage",
            "VIM.Clusters.Stats.CPULoad",

            "VIM.Hosts.Stats.CPULoad",
            "VIM.Hosts.Stats.MemUsage",
            "VIM.Hosts.Stats.NetworkUtilization",

            "VIM.VirtualMachines.Stats.CPULoad",
            "VIM.VirtualMachines.Stats.MemUsage",
            "VIM.VirtualMachines.Stats.NetworkUsageRate"
        };

    public string submitButton = "imbtnSubmit";

    protected void BusinessLayerExceptionHandler(Exception ex)
    {
        _log.Error(ex);
    }

    private List<VimModels.Threshold> GlobalThresholds
    {
        get { return _globalThresholds ?? (_globalThresholds = new ThresholdDAL().GetThresholds().ToList()); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Initialize();

        if (!IsPostBack)
        {
            using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
            {
                var where = String.Format("tn.[EntityType] = '{0}'", _entitySwisName);

                if (!LicenseManager.Instance.IsVimFullyLicensed())
                    where += string.Format(" AND tn.[Name] IN ('{0}')", string.Join("','", _vimThresholds));

                DataTable thresholdsnames = service.Query(
                    string.Format(
                        @"SELECT tn.[Name], tn.[DisplayName], tn.[DefaultThresholdOperator], tn.[Unit], tls.[SettingID]
                          FROM Orion.ThresholdsNames tn
                          LEFT JOIN Orion.ThresholdsLevelSettings tls ON tn.[Name] = tls.[Name] AND tls.[ThresholdLevel] = 1
                          WHERE {0} ORDER BY [ThresholdOrder]",
                        where));
                
                repThresholds.DataSource = thresholdsnames.Rows;
                repThresholds.DataBind();

                CreateDictionaryFunction();
            }
        }
        else
        {
            InitThresholdControlAfterPostback();
        }
    }    


    private void InitThresholdControlAfterPostback()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "InitThresholdControlAfterPostback", "InitThresholdControlAfterPostback();", true);
    }

    private void CreateDictionaryFunction()
    {
        StringBuilder sb = new StringBuilder();


        foreach (KeyValuePair<string, string> keyValuePair in controlsIds)
        {
            sb.Append("if (key == '");
            sb.Append(keyValuePair.Key);
            sb.Append("') { return '");
            sb.Append(keyValuePair.Value);
            sb.Append("'; }");
            sb.Append(Environment.NewLine);
        }

        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GetThresholdSectionControlId",
                                                         "function GetThresholdSectionControlId(thresholdName, controlKey) { var key = thresholdName + '_' + controlKey; " + sb.ToString() + " return ''; }",
                                                         true);
    }
    private bool IsMultiEdit
    {        
        get
        {
            return (_entities.Count > 1);            
        }
    }

    private string[] GetControlsForJS()
    {
        return new string[]
            {
                "sectionThresholdDefinition",
                "sectionGeneralThresholds", 
                "sectionCustomThresholds",
                "sectionErrorMessage",
                "sectionWarningMessage",
                "sectionOverrideGeneral",

                "lblOperatorText",
                "lblComputedWarningValue",
                "lblComputedCriticalValue",
                "lblGeneralWarningValue",
                "lblGeneralCriticalValue",
                
                "chbOverrideGeneral",
                "chbOverrideMultipleObjects",

                "ddlOperator",

                "btnUseLatestBaseline",
                
                "tbWarningValue",
                "tbCriticalValue",

                "hfUnit",
                "hfIsValid"
            };
    }

    private void RegisterOnSubmitFunction(string thresholdName)
    {
        ScriptManager.RegisterOnSubmitStatement(this,this.GetType(),
                                                 "ValidateThreshold_" + thresholdName.Replace(".", "_"),
                                                 "if (IsThresholdValid('" + thresholdName + "') == false) return false;");
    }

    private void RegisterStartUpFunction(string thresholdName)
    {
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), string.Format("Recompute_{0}", thresholdName.Replace(".", "_")),
                                                string.Format("Recompute('{0}');", thresholdName),
                                                 true);

      
    }

    private void CreateControlsDictionary(string thresholdName, RepeaterItem item)
    {
        foreach (string controlId in GetControlsForJS())
        {
            Control c = item.FindControl(controlId);
            controlsIds.Add(thresholdName + "_" + c.ID, c.ClientID);
        }
    }

    private void FillDdlOperator(DropDownList ddlOperator)
    {
        ddlOperator.DataValueField = "Value";
        ddlOperator.DataTextField = "Text";
        ddlOperator.DataSource =
            Enum.GetValues(typeof(ThresholdOperatorEnum))
                .Cast<ThresholdOperatorEnum>()
                .Select(x => new { Value = string.Format("{0}|{1}", (int)x, x.ToString()), Text = x.LocalizedLabel() });
        ddlOperator.DataBind();
    }

    protected void repThresholds_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string thresholdName = ((DataRow)e.Item.DataItem)["Name"].ToString();
            string thresholUnit = ((DataRow)e.Item.DataItem)["Unit"].ToString();


            CreateControlsDictionary(thresholdName, e.Item);
            RegisterOnSubmitFunction(thresholdName);

            #region Get controls

            HiddenField hfThresholdName = e.Item.FindControl("hfThresholdName") as HiddenField;
            HiddenField hfUnit = e.Item.FindControl("hfUnit") as HiddenField;

            HtmlGenericControl sectionThresholdDefinition = e.Item.FindControl("sectionThresholdDefinition") as HtmlGenericControl;
            HtmlGenericControl sectionGeneralThreshold = e.Item.FindControl("sectionGeneralThresholds") as HtmlGenericControl;
            HtmlGenericControl sectionCustomThresholds = e.Item.FindControl("sectionCustomThresholds") as HtmlGenericControl;
            HtmlGenericControl sectionErrorMessage = e.Item.FindControl("sectionErrorMessage") as HtmlGenericControl;
            HtmlGenericControl sectionWarningMessage = e.Item.FindControl("sectionWarningMessage") as HtmlGenericControl;
            HtmlGenericControl sectionDetailHyperlink = e.Item.FindControl("sectionDetailHyperlink") as HtmlGenericControl;
            HtmlGenericControl sectionOverrideGeneral = e.Item.FindControl("sectionOverrideGeneral") as HtmlGenericControl;
            

            Label lblOperatorText = e.Item.FindControl("lblOperatorText") as Label;
            Label lblGeneralWarningValue = e.Item.FindControl("lblGeneralWarningValue") as Label;
            Label lblGeneralCriticalValue = e.Item.FindControl("lblGeneralCriticalValue") as Label;
            Label lblComputedWarningValue = e.Item.FindControl("lblComputedWarningValue") as Label;
            Label lblComputedCriticalValue = e.Item.FindControl("lblComputedCriticalValue") as Label;

            CheckBox chbOverrideGeneral = e.Item.FindControl("chbOverrideGeneral") as CheckBox;
            CheckBox chbOverrideMultipleObjects = e.Item.FindControl("chbOverrideMultipleObjects") as CheckBox;

            DropDownList ddlOperator = e.Item.FindControl("ddlOperator") as DropDownList;

            Button btnUseLatestBaseline = e.Item.FindControl("btnUseLatestBaseline") as Button;

            TextBox tbWarningValue = e.Item.FindControl("tbWarningValue") as TextBox;
            TextBox tbCriticalValue = e.Item.FindControl("tbCriticalValue") as TextBox;

            #endregion


            hfThresholdName.Value = thresholdName;
            hfUnit.Value = thresholUnit;

            FillDdlOperator(ddlOperator);

            lblOperatorText.Text = ddlOperator.SelectedItem.Text.HtmlEncode();

            btnUseLatestBaseline.Attributes.Add("OnClick", string.Format("UseBaseline(this, '{0}'); return false;", thresholdName));
           
            ddlOperator.Attributes.Add("OnChange", string.Format("ThresholdOperatorChanged(this, '{0}');", thresholdName));
            ddlOperator.Attributes.Add("data-form-name", "threshold-operator");
            ddlOperator.Attributes.Add("data-form-threshold", thresholdName);
            
            chbOverrideGeneral.Attributes.Add("OnClick", string.Format("ThresholdSectionCheckBoxChanged(this, '{0}');", thresholdName));
            chbOverrideGeneral.Attributes.Add("data-form-name", "threshold-overrideGeneral"); 
            chbOverrideGeneral.Attributes.Add("data-form-threshold", thresholdName); 
          
            tbWarningValue.Attributes.Add("OnChange", string.Format("TextBoxChanged(this, '{0}');", thresholdName));
            tbCriticalValue.Attributes.Add("OnChange", string.Format("TextBoxChanged(this,'{0}');", thresholdName));

            sectionErrorMessage.Style.Add("display", "none");
            sectionWarningMessage.Style.Add("display", "none");
            sectionCustomThresholds.Style.Add("display", "none");

            if (IsMultiEdit)
            {
                var globalThreshold = GlobalThresholds.First(t => t.SettingID == ((DataRow)e.Item.DataItem)["SettingID"].ToString());

                lblGeneralWarningValue.Text = string.Format("{0} {1}", globalThreshold.Warning, thresholUnit);
                lblGeneralCriticalValue.Text = string.Format("{0} {1}", globalThreshold.High, thresholUnit);

                hfIsMultiedit.Value = "1";
                chbOverrideMultipleObjects.Visible = true;
                chbOverrideMultipleObjects.Attributes.Add("OnClick", string.Format("OverrideMultipleObjectsChanged(this, '{0}');", thresholdName));
                chbOverrideMultipleObjects.Attributes.Add("data-form-name", "threshold-overrideMultipleObjects");
                chbOverrideMultipleObjects.Attributes.Add("data-form-threshold", thresholdName);

                chbOverrideGeneral.Enabled = false;
                sectionOverrideGeneral.Style.Add("display", "none");
                sectionThresholdDefinition.Style.Add("display", "none");
                sectionDetailHyperlink.Style.Add("display", "none");

                tbWarningValue.Text = globalThreshold.Warning.ToString();
                tbCriticalValue.Text = globalThreshold.High.ToString();
            }
            else
            {
                hfIsMultiedit.Value = "0";

                DataRow rowThreshold = GetRelatedThreshold(((DataRow)e.Item.DataItem), _entities[0].ID);

                lblGeneralWarningValue.Text = string.Format("{0} {1}", rowThreshold["GlobalWarningValue"].ToString().HtmlEncode(), thresholUnit);
                lblGeneralCriticalValue.Text = string.Format("{0} {1}", rowThreshold["GlobalCriticalValue"].ToString().HtmlEncode(), thresholUnit);

                if (rowThreshold != null)
                {

                    ThresholdType thrType = (ThresholdType) rowThreshold["ThresholdType"];
                    ThresholdOperatorEnum thrOperator = (ThresholdOperatorEnum) rowThreshold["ThresholdOperator"];

                    if (thrType == ThresholdType.Global)
                    {
                        sectionGeneralThreshold.Style.Add("display", "block");
                        sectionCustomThresholds.Style.Add("display", "none");

                        tbWarningValue.Text = rowThreshold["GlobalWarningValue"].ToString().HtmlEncode();
                        tbCriticalValue.Text = rowThreshold["GlobalCriticalValue"].ToString().HtmlEncode();
                    }
                    else
                    {
                        sectionGeneralThreshold.Style.Add("display", "none");
                        sectionCustomThresholds.Style.Add("display", "block");

                        chbOverrideGeneral.Checked = true;

                        ddlOperator.ClearSelection();
                        ddlOperator.Items.FindByValue(string.Format("{0}|{1}", (int)thrOperator, thrOperator.ToString())).Selected = true;

                        lblOperatorText.Text = ddlOperator.SelectedItem.Text.HtmlEncode();

                        if (thrType == ThresholdType.Static)
                        {
                            tbWarningValue.Text = ThresholdsHelper.FromNumeric((double) rowThreshold["Level1Value"]);
                            tbCriticalValue.Text = ThresholdsHelper.FromNumeric((double) rowThreshold["Level2Value"]);
                        }

                        if (thrType == ThresholdType.Dynamic)
                        {
                            tbWarningValue.Text = rowThreshold["Level1Formula"].ToString().HtmlEncode();
                            tbCriticalValue.Text = rowThreshold["Level2Formula"].ToString().HtmlEncode();
                        }

                        RegisterStartUpFunction(thresholdName);
                    }

                }
            }
        }
    }
    
    public void Initialize()
    {
        hfObjectIds.Value = string.Join(" ", _entities.Select(x => x.ID.ToString()).ToArray());
        LoadExistingThresholds();
    }     

    private void LoadExistingThresholds()
    {
        using (InformationServiceProxy service = InformationServiceProxy.CreateV3())
        {
            string tableName = string.Empty;
            switch (_entityType)
            {
                case VimEntityType.VirtualMachine:
                    tableName = "Orion.VIM.VirtualMachineThresholds";
                    break;
                case VimEntityType.Cluster:
                    tableName = "Orion.VIM.ClusterThresholds";
                    break;
                case VimEntityType.Host:
                    tableName = "Orion.VIM.HostThresholds";
                    break;
                case VimEntityType.DataStore:
                    tableName = "Orion.VIM.DatastoreThresholds";
                    break;
            }
            string query =
                string.Format(
                    "SELECT [Name], [Level1Value], [Level1Formula], [Level2Value], [Level2Formula], [InstanceId], [InstanceType], [ThresholdOperator], [ThresholdType], [EntityType], [CurrentValue], [GlobalWarningValue], [GlobalCriticalValue] FROM {2} WHERE [EntityType] = '{0}' AND [InstanceId] IN ({1})",
                    _entitySwisName, string.Join(",", _entities.Select(x => x.ID.ToString()).ToArray()), tableName);
            ExistingThresholds = service.Query(query);
        }
    }


    private bool IsExistingThreshold(Threshold threshold)
    {
        string condition;

        condition = string.Format("[EntityType] = '{0}' AND [InstanceId] = {1} AND [ThresholdType] = {2} AND [Name] = '{3}' ", _entitySwisName, threshold.InstanceId, (int)threshold.ThresholdType, threshold.ThresholdName);

        if (threshold.ThresholdType != ThresholdType.Global)
        {
            condition += string.Format(" AND [ThresholdOperator] = {0}", (int) threshold.ThresholdOperator);

            if (threshold.ThresholdType == ThresholdType.Static)
            {
                condition += string.Format(" AND [Level1Value] = {0} AND [Level2Value] = {1}", ThresholdsHelper.FromNumeric(threshold.Warning),ThresholdsHelper.FromNumeric(threshold.Critical));
            }

            if (threshold.ThresholdType == ThresholdType.Dynamic)
            {
                condition += string.Format(" AND [Level1Formula] = '{0}' AND [Level2Formula] = '{1}'", threshold.WarningFormula, threshold.CriticalFormula);
            }
        }

        return ExistingThresholds.Select(condition).Count() > 0;
    }

    private DataRow GetRelatedThreshold(DataRow rowFromThresholdNames, int instanceId)
    {
        var thresholdName = rowFromThresholdNames["Name"]?.ToString();
        string selectQuery = $"[Name] = '{thresholdName}' AND [InstanceId] = {instanceId}";
        DataRow[] result = ExistingThresholds.Select(selectQuery);

        return result.Count() == 1 ? result[0] : null;
    }

    public void SetEntities(IList<IBaseEntity> entities, VimEntityType type)
    {
        _entityType = type;
        switch (type)
        {
            case VimEntityType.VirtualMachine:
                _entitySwisName = SwisEntityNames.Vim.VirtualMachines;
                break;
            case VimEntityType.Cluster:
                _entitySwisName = SwisEntityNames.Vim.Clusters;
                break;
            case VimEntityType.Host:
                _entitySwisName = SwisEntityNames.Vim.Hosts;
                break;
            case VimEntityType.DataStore:
                _entitySwisName = SwisEntityNames.Vim.DataStores;
                break;
            default:
                return;
        }

        _entities = entities;
    }

    public bool Validate()
    {
        return true;
    }

    public bool UpdateThresholds()
    {
        if (_entities == null)
            return false;
 
        List<Threshold> thresholds = new List<Threshold>();

        foreach (RepeaterItem repItem in repThresholds.Items)
        {
            if (IsMultiEdit)
            {
                CheckBox chbOverrideMultipleObjects = repItem.FindControl("chbOverrideMultipleObjects") as CheckBox;

                if (!chbOverrideMultipleObjects.Checked)
                {
                    continue;
                }
            }

            CheckBox chb = repItem.FindControl("chbOverrideGeneral") as CheckBox;
            HiddenField hfThresholdName = repItem.FindControl("hfThresholdName") as HiddenField;

            if (chb.Checked)
            {
                
                DropDownList ddlOperator = repItem.FindControl("ddlOperator") as DropDownList;
                TextBox tbWarningValue = repItem.FindControl("tbWarningValue") as TextBox;
                TextBox tbCriticalValue = repItem.FindControl("tbCriticalValue") as TextBox;

                ThresholdOperatorEnum thresholdOperator = (ThresholdOperatorEnum)System.Enum.Parse(typeof(ThresholdOperatorEnum), ddlOperator.SelectedValue.Split('|')[1]);

                foreach (var vmTmp in _entities)
                {
                    double warningValue;
                    double criticalValue;
                    bool bothNumeric = ThresholdsHelper.IsNumeric(tbWarningValue.Text, out warningValue) & ThresholdsHelper.IsNumeric(tbCriticalValue.Text, out criticalValue);

                    Threshold threshold = new Threshold()
                        {
                            InstanceId = vmTmp.ID,
                            ThresholdName = hfThresholdName.Value,
                            ThresholdOperator = thresholdOperator,
                            ThresholdType = (bothNumeric ? ThresholdType.Static : ThresholdType.Dynamic),
                            Warning = null,
                            Critical = null,
                            WarningFormula = null,
                            CriticalFormula = null
                        };                    

                    if (threshold.ThresholdType == ThresholdType.Dynamic)
                    {
                        threshold.WarningFormula = tbWarningValue.Text;
                        threshold.CriticalFormula = tbCriticalValue.Text;
                        threshold = ComputeThresholdValues(threshold);

                        thresholds.Add(threshold);
                    }
                    else if (threshold.ThresholdType == ThresholdType.Static)
                    {
                        threshold.Warning = warningValue;
                        threshold.Critical = criticalValue;

                        if (threshold.Warning != null && threshold.Critical != null)
                        {
                            thresholds.Add(threshold);
                        }
                    }
                }
            }
            else
            {
                foreach (var vm in _entities)
                {
                    Threshold threshold = new Threshold()
                        {
                            InstanceId = vm.ID,
                            ThresholdName = hfThresholdName.Value,
                            ThresholdOperator = ThresholdOperatorEnum.Greater,
                            ThresholdType = ThresholdType.Global,
                            Warning = null,
                            Critical = null,
                            WarningFormula = null,
                            CriticalFormula = null
                        };

                    thresholds.Add(threshold);
                }
            }
        }

        using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(BusinessLayerExceptionHandler))
        {
            foreach (var threshold in thresholds)
            {
                if (!IsExistingThreshold(threshold))
                {
                    proxy.SetThreshold(threshold);
                }
            }
        }         
                
        return true;
    }

    private static Threshold ComputeThresholdValues(Threshold threshold)
    {
        try
        {
            ComputeThresholdRequest request = new ComputeThresholdRequest();

            request.ThresholdName = threshold.ThresholdName;

            request.InstancesId = new[] { threshold.InstanceId };
            request.Operator = threshold.ThresholdOperator;

            request.CriticalFormula = threshold.CriticalFormula;
            request.WarningFormula = threshold.WarningFormula;

            ComputeThresholdResponse response = new ThresholdsController().Compute(request);

            if (response.IsComputed)
            {
                threshold.Warning = response.WarningThreshold;
                threshold.Critical = response.CriticalThreshold;
            }
        }
        catch (Exception e)
        {
            _log.Error(string.Format("Can't compute current values for threshold {0}.", threshold), e);   
        }

        return threshold;
    }

    protected string GetGeneralThresholdPageURL()
    {
        return string.Format("/Orion/VIM/Admin/VIMThresholds.aspx?ReturnTo={0}",  HttpUtility.HtmlEncode(ReferrerRedirectorBase.GetReferrerUrl()));
    }
}
