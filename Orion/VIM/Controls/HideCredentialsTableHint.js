﻿$(function() {
    Sys.Application.add_load(function() {
        var icmpSelected = $("input:radio[value='rbICMP']").prop("checked");
        var agentSelected = $("input:radio[value='rbAgent']").prop("checked");

        if (!icmpSelected) {
            $("div[id='vim-credentialsTableHint']").show();
        } else {
            $("div[id='vim-credentialsTableHint']").hide();
        }

        if (agentSelected) {
            $("div[id='vim-agentPollingWarning']").show();
        } else {
            $("div[id='vim-agentPollingWarning']").hide();
        }

        var isVimBlReady = $("[id='isVimBusinessLayerReady']").val();

        if (isVimBlReady === 'True') {
            $("table[id='vim-businessLayerWarning']").hide();
            $("div[id='vim-credentialsTableContent']").show();
        } else {
            $("table[id='vim-businessLayerWarning']").show();
            $("div[id='vim-credentialsTableContent']").hide();
        }
    });
});
