using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using Node = SolarWinds.Orion.NPM.Web.Node;
using SolarWinds.VIM.Web.DAL;
using System.Web.UI.WebControls;

public partial class Orion_NetPerfMon_Controls_VMInfo : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string netObject = Request.QueryString["NetObject"];

        if (!String.IsNullOrEmpty(netObject))
        {
            string[] parts = netObject.Split(':');
            int nodeId;

            if (parts.Length > 1 && Int32.TryParse(parts[1], out nodeId))
            {
                string hostCaption;
                string hostDetailsUrl;
                
                // don't decorate vCenters
                if (new VCenterDAL().IsVCenterByNodeId(nodeId))
                    return;

                VmNodeType nt = (new VMInfoDAL()).GetNodeHarwareType(nodeId, out hostCaption, out hostDetailsUrl);

                switch (nt)
                {
                    case VmNodeType.HostUnknown:
                        {
                            this.textHolder.Controls.Add(new Literal() { Text = String.Format("{0} ", VIMWebContent.VIMWEBCODE_VB0_52) });
                        } break;
                    case VmNodeType.Hosted:
                        {
                            this.textHolder.Controls.Add(new Literal() { Text = String.Format("{0} ", VIMWebContent.VIMWEBCODE_VB0_53) });

                            if (hostDetailsUrl != null)
                            {
                                this.textHolder.Controls.Add(new HyperLink()
                                    {
                                        Text = hostCaption,
                                        NavigateUrl = hostDetailsUrl
                                    });
                            }
                            else
                            {
                                this.textHolder.Controls.Add(new Literal() {Text = hostCaption});
                            }
                        } break;
                }
            }
        }
    }
}
