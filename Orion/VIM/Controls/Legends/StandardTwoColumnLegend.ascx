﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StandardTwoColumnLegend.ascx.cs" Inherits="Orion_VIM_Controls_Legends_StandardTwoColumnLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.vman_standardTwoColumnLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        $('#<%= legend.ClientID %>').empty();
        SW.VMan.Charts.StandardTwoColumnLegend.createStandardLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
    };
        
</script>

<table runat="server" id="legend" class="chartLegend"></table>
<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png" alt=""/>
</div>
