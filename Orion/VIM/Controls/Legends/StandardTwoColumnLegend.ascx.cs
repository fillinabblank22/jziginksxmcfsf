﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_VIM_Controls_Legends_StandardTwoColumnLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer => "vman_standardTwoColumnLegendInitializer__" + legend.ClientID;

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("VIM", "js/Charts/Legends/StandardTwoColumnLegend.js", OrionInclude.Section.Middle);
    }
}