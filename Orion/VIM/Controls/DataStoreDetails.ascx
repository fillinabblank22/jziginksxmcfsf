﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataStoreDetails.ascx.cs" Inherits="Orion_VIM_Controls_DataStoreDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<style type="text/css">
    .link
    {
        color: #336699;
        text-decoration: underline;
    }
</style>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
<Content>
            <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= VIMWebContent.VIMWEBDATA_VB0_124%>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <table border="0" cellpadding="2" cellspacing="0" width="100%">
            <thead>
                <tr align="left">
                    <th colspan="2"><%= VIMWebContent.VIMWEBDATA_VB0_125%></th>
                    <th><%= VIMWebContent.VIMWEBDATA_VB0_126%></th>
                    <th><%= VIMWebContent.VIMWEBDATA_VB0_127%></th>
                    <th  colspan="2"><%= VIMWebContent.VIMWEBDATA_VB0_131%></th>
                </tr>
            </thead>
            <asp:Repeater runat="server" ID="fvESXDetailsFormView" >
                <ItemTemplate>
                    <tr>
                        <td class="Property"><%#Eval("Name")%>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="Property"><%#Eval("ESXCount")%>&nbsp;</td>
                        <td class="Property"><%#Eval("VMCount")%>&nbsp;</td>
                        <td class="Property"><%#Eval("UsagePercentage")%>&nbsp;</td>
                         <td><orion:PercentStatusBar runat="server" ID="StatusBar" Status='<%#SolarWinds.VIM.Web.Helpers.FormatHelper.GetPercentValue(Convert.ToDouble(Eval("UsagePercentage")))%>'
                              WarningLevel='<%#WarningLevel %>' ErrorLevel='<%#ErrorLevel %>' Show='<%#GetBarVisible()%>' /></td>

                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
</Content>
</orion:resourceWrapper>