﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_VIM_Controls_EditControls_RememberExpandedStateControl : System.Web.UI.UserControl
{
    public const string RememberExpandedStateKey = "RememberExpandedState";

    //public bool RememberExpandedGroups
    //{
    //    get { return chbRememberExpandedState.Checked; }
    //    set { chbRememberExpandedState.Checked = value; }
    //}

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties[RememberExpandedStateKey] = chbRememberExpandedState.Checked.ToString();
    }

    public void LoadFromResourceProperties(SolarWinds.Orion.Web.ResourcePropertyCollection properties)
    {
        chbRememberExpandedState.Checked = String.IsNullOrEmpty(properties[RememberExpandedStateKey]) ? true : properties[RememberExpandedStateKey].ToLowerInvariant() == Boolean.TrueString.ToLowerInvariant();
    }
}