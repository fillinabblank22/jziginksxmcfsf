﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterByPlatform.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_FilterByPlatform" %>
<p>
    <b><asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LB0_2%>" /></b><br />
    <asp:CheckBox runat="server" ID="chbVMware" /><br />
    <asp:CheckBox runat="server" ID="chbHyperV" /><br />
    <asp:CheckBox runat="server" ID="chbNutanix" />
</p>