﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RememberExpandedStateControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_RememberExpandedStateControl" %>
<p>
    <b><asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LB0_1%>" /></asp:Literal></b><br />
    <asp:CheckBox runat="server" ID="chbRememberExpandedState" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_110%>" />
</p>