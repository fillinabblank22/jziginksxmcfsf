﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Controls_EditControls_RelatedNodesTreeEdit : BaseResourceEditControl
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            ucRememberExpandedStateControl.LoadFromResourceProperties(this.Resource.Properties);
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            ucRememberExpandedStateControl.SaveProperties(properties);
            return properties;
        }
    }
}