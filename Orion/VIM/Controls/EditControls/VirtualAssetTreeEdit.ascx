﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualAssetTreeEdit.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_VirtualAssetTreeEdit" %>
<%@ Register src="FilterByPlatform.ascx" tagname="FilterByPlatform" tagprefix="vim" %>
<%@ Register src="RememberExpandedStateControl.ascx" tagname="RememberExpandedStateControl" tagprefix="vim" %>

<vim:FilterByPlatform ID="ucFilterByPlatform" runat="server" />
<vim:RememberExpandedStateControl ID="ucRememberExpandedStateControl" runat="server" />

