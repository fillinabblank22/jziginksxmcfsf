﻿using System;
using System.Collections.Generic;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_Controls_EditControls_FilterByPlatform : System.Web.UI.UserControl
{
    public bool VMwareEnabled
    {
        get { return chbVMware.Checked; }
    }

    public bool HyperVEnabled
    {
        get { return chbHyperV.Checked; }
    }

    public bool NutanixEnabled
    {
        get { return chbNutanix.Checked; }
    }

    protected override void OnInit(EventArgs e)
    {
        chbVMware.Text = PlatformHelper.VMwareName;
        chbHyperV.Text = PlatformHelper.HyperVName;
        chbNutanix.Text = PlatformHelper.NutanixName;

        base.OnInit(e);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        PlatformHelper.SaveProperties(properties, chbVMware.Checked, chbHyperV.Checked, chbNutanix.Checked);
    }

    public void SaveProperties(SolarWinds.Orion.Web.ResourcePropertyCollection properties)
    {
        PlatformHelper.SaveProperties(properties, chbVMware.Checked, chbHyperV.Checked, chbNutanix.Checked);
    }

    public void LoadFromResourceProperties(SolarWinds.Orion.Web.ResourcePropertyCollection properties)
    {
        List<VirtualizationPlatform> platforms = PlatformHelper.GetCollectionFromResourceProperties(properties);
        chbVMware.Checked = platforms.Contains(VirtualizationPlatform.Vmware);
        chbHyperV.Checked = platforms.Contains(VirtualizationPlatform.HyperV);
        chbNutanix.Checked = platforms.Contains(VirtualizationPlatform.Nutanix);
    }
}