﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Controls_EditControls_AssetTreeSummaryEdit : BaseResourceEditControl
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            ucFilterByPlatform.LoadFromResourceProperties(this.Resource.Properties);
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            ucFilterByPlatform.SaveProperties(properties);
            return properties;
        }
    }
}