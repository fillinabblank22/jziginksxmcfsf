﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_Controls_EditControls_ThresholdsEdit : BaseResourceEditControl
{

    const int DEFAULT_WarningThreshold = 80;
    const int DEFAULT_CriticalThreshold = 95;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.litWarningThreshold.Text = String.Format("[{0}-{1}]", this.RangeWarningThresholdValidator.MinimumValue, this.RangeWarningThresholdValidator.MaximumValue);
        this.litCriticalThreshold.Text = String.Format("[{0}-{1}]", this.RangeCriticalThresholdValidator.MinimumValue, this.RangeCriticalThresholdValidator.MaximumValue);

        if (!this.IsPostBack)
        {
            this.txtWarningThreshold.Text = DEFAULT_WarningThreshold.ToString();
            if (this.Resource.Properties["WarningThreshold"] != null)
            {
                int value;
                if (int.TryParse(this.Resource.Properties["WarningThreshold"], out value))
                {
                    if (value < 0)
                    {
                        value = 0;
                    }
                    if (value > 100)
                    {
                        value = 100;
                    }
                    this.txtWarningThreshold.Text = value.ToString();
                }
            }
            this.txtCriticalThreshold.Text = DEFAULT_CriticalThreshold.ToString();
            if (this.Resource.Properties["CriticalThreshold"] != null)
            {
                int value;
                if (int.TryParse(this.Resource.Properties["CriticalThreshold"], out value))
                {
                    if (value < 0)
                    {
                        value = 0;
                    }
                    if (value > 100)
                    {
                        value = 100;
                    }
                    this.txtCriticalThreshold.Text = value.ToString();
                }
            }
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            Page.Validate();
            if (Page.IsValid)
            {
                properties["WarningThreshold"] = this.txtWarningThreshold.Text;
                properties["CriticalThreshold"] = this.txtCriticalThreshold.Text;
            }
            return properties;
        }
    }

}