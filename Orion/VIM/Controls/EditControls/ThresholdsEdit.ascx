﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdsEdit.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_ThresholdsEdit" %>

<p>
    <b><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_DF0_7%>" /></b><br />
    <asp:TextBox runat="server" ID="txtWarningThreshold" /> <asp:Literal runat="server" ID="litWarningThreshold" />
    <asp:RequiredFieldValidator ID="RequiredWarningThresholdValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="txtWarningThreshold" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB1_10%>"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="RangeWarningThresholdValidator" Display="Dynamic" runat="server" ControlToValidate="txtWarningThreshold" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>" MinimumValue="0" MaximumValue="100" Type="Integer" ></asp:RangeValidator>
    <br />
</p>

<p>
    <b><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_DF0_8%>" /></b><br />
    <asp:TextBox runat="server" ID="txtCriticalThreshold" /> <asp:Literal runat="server" ID="litCriticalThreshold" />
    <asp:RequiredFieldValidator ID="RequiredCriticalThresholdValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="txtCriticalThreshold" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB1_9%>"></asp:RequiredFieldValidator>
    <asp:RangeValidator ID="RangeCriticalThresholdValidator" Display="Dynamic" runat="server" ControlToValidate="txtCriticalThreshold" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_VB1_8%>" MinimumValue="0" MaximumValue="100" Type="Integer" ></asp:RangeValidator>
    <br />
</p>


<asp:CompareValidator
    ID="ComparThresholdsValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="txtCriticalThreshold" Display="Dynamic" 
        ControlToValidate="txtWarningThreshold" Operator="LessThanEqual" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_DF0_9%>"></asp:CompareValidator>
