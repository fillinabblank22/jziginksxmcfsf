﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeWizardPlugin.ascx.cs" Inherits="Orion_VIM_Controls_NodeWizardPlugin" %>
<style type="text/css">
    .checkbox label
    {
        padding-left: 5px;
    }
</style>

<orion:Include runat="server" Module="VIM" File="/Controls/HideCredentialsTableHint.js" />

<div class="contentBlock" id="divContent" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="3" style="padding: 0px 0px 10px 10px;">
                    <div class="sw-pg-hint-yellow" id="vim-agentPollingWarning">
                        <div class="sw-pg-hint-body-info">
                            <%= Resources.VIMWebContent.General_Polling_NodePolledByAgentWarningMessage%>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:CheckBox runat="server" ID="cbMultipleSelection" AutoPostBack="True" Visible="false" OnCheckedChanged="cbMultipleSelection_CheckedChanged" Checked="false" /><asp:Literal ID="pluginTitle" runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_27%>" />
                </td>
                <td>
                    <asp:CheckBox ID="cbEnableVMwarePolling" runat="server" OnCheckedChanged="cbEnableVMwarePolling_CheckedChanged" 
                        Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_18%>" AutoPostBack="True" CssClass="checkbox"/>
                </td>
            </tr>
        </table>
    </div>

    <div class="blueBox" id="credentialsTable" runat="server">
        <table id="vim-businessLayerWarning">
            <tr>
                <td>
                    <div class="sw-pg-hint-yellow">
                        <div class="sw-pg-hint-body-warning">
                            <%= Resources.VIMWebContent.NodeWizardPlugin_CredentialsTable_VimServiceNotAvailableMessage%>
                        </div>
                        <asp:HiddenField ID="isVimBusinessLayerReady" Value="False" ClientIDMode="Static" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
        <div id="vim-credentialsTableContent">
            <table>
                <tr>
                    <td colspan="3" style="padding: 0px 0px 10px 10px;">
                        <div class="sw-pg-hint-blue" id="vim-credentialsTableHint">
                            <div class="sw-pg-hint-body-info">
                                <%= Resources.VIMWebContent.NodeWizardPlugin_CredentialsTable_IcmpInfoMessage%>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <asp:Label runat="server" ID="lblTemplateLabel"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_28%>&nbsp;</asp:Label>
                    </td>
                    <td class="rightInputColumn">
                        <asp:DropDownList ID="ddlVMwareCredentials" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlVMwareCredentials_IndexChanged" >
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td class="leftLabelColumn">
                        <asp:Label runat="server" ID="Label1"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_29%>&nbsp;</asp:Label>
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbCaption" runat="server" MaxLength=50 ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredCredentialNameValidator" runat="server" Display="Dynamic"  ErrorMessage="RequiredFieldValidator" ControlToValidate="tbCaption" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_32%>"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <asp:Label runat="server" ID="Label3"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_30%>&nbsp;</asp:Label>
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbLogin" runat="server" MaxLength=50></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredUserNameValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbLogin" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_33%>" ></asp:RequiredFieldValidator>
                    </td>
                    <td class="helpfulText">
                        <%= Resources.VIMWebContent.VIMWEBDATA_TM0_31%>
                    </td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <asp:Label runat="server" ID="Label5"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%>&nbsp;</asp:Label>
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="50" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPassword" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_34%>"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="leftLabelColumn">
                        <asp:Label runat="server" ID="Label7"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_36%>&nbsp;</asp:Label>
                    </td>
                    <td class="rightInputColumn">
                        <asp:TextBox ID="tbPasswordConfirmation" runat="server" TextMode="Password" MaxLength="50" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredConfirmPasswordValidator" runat="server" ErrorMessage="RequiredFieldValidator" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_34%>"></asp:RequiredFieldValidator>
                        <asp:CompareValidator
                            ID="CompareConfirmPasswordValidator" runat="server" ErrorMessage="CompareConfirmPasswordValidator" ControlToCompare="tbPassword" Display="Dynamic" ControlToValidate="tbPasswordConfirmation" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_35%>"></asp:CompareValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div runat="server" id="VMwareValidationTrue" style="padding-left: 118px; background-color: #D6F6C6;
                text-align: center" visible="false">
                <img runat="server" id="VMwareValidationTrueIMG" src="~/Orion/images/nodemgmt_art/icons/icon_OK.gif" />
                <%= Resources.VIMWebContent.VIMWEBDATA_TM0_37%>
            </div>
            <div runat="server" id="VMwareValidationFalse" style="padding-left: 118px; background-color: #FEEE90;
                text-align: center; color: Red" visible="false">
                <img runat="server" id="VMwareValidationFalseIMG" src="~/Orion/images/nodemgmt_art/icons/icon_warning.gif" />
                <%= Resources.VIMWebContent.VIMWEBDATA_TM0_38%> <asp:Label runat="server" ID="VMwareValidationFailedMessage" />
            </div>
            <asp:UpdatePanel runat="server" ID="vmwareUpdatePanel" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td class="leftLabelColumn">&nbsp;</td>
                            <td class="rightInputColumn">
                                <div class="sw-btn-bar">
                                    <orion:LocalizableButton runat="server" ID="imbtnValidateVMware" LocalizedText="Test" 
                                        OnClick="imbtnValidateVMware_Click" DisplayType="Small" />
                                </div>
                            </td>
                            <td>
                                <asp:UpdateProgress runat="server" ID="vmwareTestUpdateProgress" DynamicLayout="true" DisplayAfter="0" AssociatedUpdatePanelID="vmwareUpdatePanel" style="margin-left: 10px">
                                    <ProgressTemplate>
                                        <img src="/Orion/images/animated_loading_sm3_whbg.gif" />
                                        <span style="font-weight: bold;">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_8 %>" />
                                        </span>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="blueBox" id="vmwareNodeTable" runat="server" visible="false">
        <div style="padding-left: 5px; padding-right: 5px;"><asp:Literal ID="litVMwareNodeTableText" runat="server" /></div>
    </div>
</div>