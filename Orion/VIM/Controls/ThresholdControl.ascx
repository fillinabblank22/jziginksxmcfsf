﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ThresholdControl.ascx.cs" Inherits="Orion_VIM_Controls_ThresholdControl" %>
<%@ Import Namespace="SolarWinds.VIM.Base.Contract.Constants" %>
<%@ Register Src="~/Orion/Controls/BaselineDetails.ascx" TagPrefix="orion" TagName="BaselineDetails" %>

<style type="text/css">
.ddlInTable {width:100%;}
</style>

<orion:Include ID="Include1" runat="server" Module="VIM" File="ThresholdControl.css" />

<script language="javascript">
    function InitThresholdControlAfterPostback() {
        $('[data-form-name="threshold-overrideMultipleObjects"] input[type="checkbox"]').each(function (index, el) {
            var thresholdName = $(el).parent().data('form-threshold');
            OverrideMultipleObjectsChanged(el, thresholdName);
        });

        $('[data-form-name="threshold-overrideGeneral"] input[type="checkbox"]').each(function (index, el) {
            var thresholdName = $(el).parent().data('form-threshold');
            ThresholdSectionCheckBoxChanged(el, thresholdName);
        });

        $('[data-form-name="threshold-operator"]').each(function (index, el) {
            var thresholdName = $(el).data('form-threshold');
            ThresholdOperatorChanged(el, thresholdName, false);
        });
    }

    function GetThresholdSectionControl(thresholdName, controlKey) {
        return document.getElementById(GetThresholdSectionControlId(thresholdName, controlKey)); /// function  'GetThresholdSectionControlId' is generated from code
    }

    function ThresholdSectionCheckBoxChanged(sender, thresholdName) {

        var sectionGeneralThresholds = GetThresholdSectionControl(thresholdName, 'sectionGeneralThresholds');
        var sectionCustomThresholds = GetThresholdSectionControl(thresholdName, 'sectionCustomThresholds');

        if (sender.checked == true) {
            sectionGeneralThresholds.style.display = 'none';
            sectionCustomThresholds.style.display = 'block';
            Recompute(thresholdName);
        } else {
            sectionGeneralThresholds.style.display = 'block';
            sectionCustomThresholds.style.display = 'none';
            ClearErrorMessage(thresholdName);
            ClearWarningMessage(thresholdName);
        }
    }

    function ThresholdOperatorChanged(sender, thresholdName, recompute) {
        var lblOperatorText = GetThresholdSectionControl(thresholdName, 'lblOperatorText');
        lblOperatorText.innerHTML = sender.options[sender.selectedIndex].text;

        if (typeof (recompute) == 'undefined') {
            recompute = true;
        }

        if (recompute) {
            Recompute(thresholdName);
        }
    }

    function OverrideMultipleObjectsChanged(sender, thresholdName) {
        var chbOverrideGeneral = GetThresholdSectionControl(thresholdName, 'chbOverrideGeneral');
        var sectionThresholdDefinition = GetThresholdSectionControl(thresholdName, 'sectionThresholdDefinition');
        var sectionOverrideGeneral = GetThresholdSectionControl(thresholdName, 'sectionOverrideGeneral');

        if (sender.checked == true) {
            chbOverrideGeneral.disabled = false;
            sectionOverrideGeneral.style.display = 'block';
            sectionThresholdDefinition.style.display = 'block';
        } else {
            chbOverrideGeneral.disabled = true;
            sectionOverrideGeneral.style.display = 'none';
            sectionThresholdDefinition.style.display = 'none';
        }
    }

    function GetObjectsIds() {
        var hfObjectIds = document.getElementById('<%= hfObjectIds.ClientID %>');

        var objectIds = hfObjectIds.value.split(" ");
        for (var i = 0; i < objectIds.length; i++) { objectIds[i] = +objectIds[i]; }

        return objectIds;
    }

    function HandleError(thresholdName, errorMessages) {
        var sectionErrorMessage = GetThresholdSectionControl(thresholdName, 'sectionErrorMessage');
        var hfIsValid = GetThresholdSectionControl(thresholdName, 'hfIsValid');
        hfIsValid.value = '0';
        var errorMessage = "";
        var allMessageSame = true;

        for (var i = 0; i < errorMessages.length - 1; i++) {
            if (errorMessages[i] !== errorMessages[i + 1]) {
                allMessageSame = false;
                break;
            }
        }

        if (allMessageSame) {
            errorMessage = errorMessages[0];
        } else {
            for (var i = 0; i < errorMessages.length; i++) {
                errorMessage = errorMessage + errorMessages[i];

                if (i < errorMessages.length - 1) {
                    errorMessage = errorMessage + "</br></br>";
                }
            }
        }

        sectionErrorMessage.innerHTML = errorMessage;
        sectionErrorMessage.style.display = 'block';
    }

    function HandleWarning(thresholdName, warningMessage) {
        var sectionWarningMessage = GetThresholdSectionControl(thresholdName, 'sectionWarningMessage');

        sectionWarningMessage.innerHTML = warningMessage;
        sectionWarningMessage.style.display = 'block';
    }

    function ClearWarningMessage(thresholdName) {
        var sectionWarningMessage = GetThresholdSectionControl(thresholdName, 'sectionWarningMessage');
        sectionWarningMessage.style.display = 'none';
    }

    function ClearErrorMessage(thresholdName) {
        var sectionErrorMessage = GetThresholdSectionControl(thresholdName, 'sectionErrorMessage');
        var hfIsValid = GetThresholdSectionControl(thresholdName, 'hfIsValid');

        hfIsValid.value = '1';
        sectionErrorMessage.innerHTML = '';
        sectionErrorMessage.style.display = 'none';
    }

    function UseBaseline(sender, thresholdName) {
        var tbWarningValue = GetThresholdSectionControl(thresholdName, 'tbWarningValue');
        var tbCriticalValue = GetThresholdSectionControl(thresholdName, 'tbCriticalValue');
        tbWarningValue.value = "${USE_BASELINE_WARNING}";
        tbCriticalValue.value = "${USE_BASELINE_CRITICAL}";

        var lblComputedWarningValue = GetThresholdSectionControl(thresholdName, 'lblComputedWarningValue');
        var lblComputedCriticalValue = GetThresholdSectionControl(thresholdName, 'lblComputedCriticalValue');

        var hfIsMultiedit = $("#<%= hfIsMultiedit.ClientID %>").val();
        if (hfIsMultiedit == 1) {
            lblComputedWarningValue.innerHTML = "<%= Resources.CoreWebContent.WEBDATA_VB2_1 %>";
            lblComputedCriticalValue.innerHTML = "<%= Resources.CoreWebContent.WEBDATA_VB2_1 %>";
        }
        Recompute(thresholdName);
    }

    function TextBoxChanged(sender, thresholdName) {
        Recompute(thresholdName);
    }

    function Recompute(thresholdName) {

        ClearErrorMessage(thresholdName);
        ClearWarningMessage(thresholdName);

        var hfIsValid = GetThresholdSectionControl(thresholdName, 'hfIsValid');
        hfIsValid.value = '2';

        var tbWarningValue = GetThresholdSectionControl(thresholdName, 'tbWarningValue');
        var tbCriticalValue = GetThresholdSectionControl(thresholdName, 'tbCriticalValue');
        var ddlOperator = GetThresholdSectionControl(thresholdName, 'ddlOperator');
        var lblComputedWarningValue = GetThresholdSectionControl(thresholdName, 'lblComputedWarningValue');
        var lblComputedCriticalValue = GetThresholdSectionControl(thresholdName, 'lblComputedCriticalValue');
        var hfUnit = GetThresholdSectionControl(thresholdName, 'hfUnit');

        var operatorId = ddlOperator.options[ddlOperator.selectedIndex].value.split("|")[0];

        tbWarningValue.disabled = true;
        tbCriticalValue.disabled = true;
        ddlOperator.disabled = true;

        var request = {
            ThresholdName: thresholdName,
            InstancesId: GetObjectsIds(),
            CriticalFormula: tbCriticalValue.value,
            WarningFormula: tbWarningValue.value,
            Operator: operatorId
        };

        SW.Core.Services.callController("/api/Thresholds/Compute", request, function (response) {

            tbWarningValue.disabled = false;
            tbCriticalValue.disabled = false;
            ddlOperator.disabled = false;
            var hfIsMultiedit = $("#<%= hfIsMultiedit.ClientID %>").val();

            if (hfIsMultiedit == 0) {
                lblComputedWarningValue.innerHTML = "?";
                lblComputedCriticalValue.innerHTML = "?";
            }

            if (response.IsValid == false) {
                HandleError(thresholdName, response.ErrorMessages);
            } else {
                hfIsValid.value = '1';

                if (response.IsComputed) {
                    lblComputedWarningValue.innerHTML = '= ' + response.WarningThreshold + " " + hfUnit.value;
                    lblComputedCriticalValue.innerHTML = '= ' + response.CriticalThreshold + " " + hfUnit.value;
                }

                if (response.WarningMessage) {
                    HandleWarning(thresholdName, response.WarningMessage);
                }
            }
        });
    }

    function IsThresholdValid(thresholdName) {

        try {
            var btSuffix = '<%= submitButton %>';
            var target = $('#__EVENTTARGET').val();

            if (target.indexOf(btSuffix, target.length - btSuffix.length) == -1)
                return true;
        } catch (e) {
        }

        var hfIsValid = GetThresholdSectionControl(thresholdName, 'hfIsValid');

        if (hfIsValid.value == '1')
            return true;
        else
            return false;
    }

    function ShowBaselineDetailDialog(thresholdName, displayName, returnFunction) {
        var ddlOperator = GetThresholdSectionControl(thresholdName, 'ddlOperator');
        var operatorId = parseInt(ddlOperator.options[ddlOperator.selectedIndex].value.split("|")[0]);
        var instanceId = GetObjectsIds()[0];

        SW.Core.Baselines.showBaselineDetailsDialog(instanceId, thresholdName, displayName, operatorId, returnFunction);
    }
</script>

<div runat="server" id="ThresholdBlock" class="contentBlock">
    <table width="100%">
        <tr>
            <td class="contentBlockHeader">
                <asp:Literal ID="TrasholdsBlockTitle" runat="server" Text="<%$ Resources:CoreWebContent,WEBDATA_TK0_43  %>" /> <%-- Alerting Thresholds --%>
            </td>
            <td style="text-align: right">
                <span>
                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                    <a href="<%= GetGeneralThresholdPageURL() %>" id="manage" target="_blank" class="RenewalsLink"><%= Resources.VIMWebContent.VIMWEBDATA_LV0_29 %></a> <%-- Manage Virtualization General Thresholds --%>
                </span>
            </td>
        </tr>
    </table>
<asp:HiddenField runat="server" ID="hfObjectIds" />
<asp:HiddenField runat="server" ID="hfIsMultiedit" />
<asp:Repeater runat="server" ID="repThresholds" OnItemDataBound="repThresholds_ItemDataBound">
    <HeaderTemplate><table class="thresholds"></HeaderTemplate>
    <FooterTemplate><tr style="height: 15px"><td colspan="2"></td></tr></table></FooterTemplate>
    <ItemTemplate>
        <tr>
            <td class="leftLabelColumn bold" style="text-align: left;">
                <asp:CheckBox runat="server" ID="chbOverrideMultipleObjects" Visible="False" /><%#((System.Data.DataRow)Container.DataItem)["DisplayName"]%>
                <asp:HiddenField runat="server" ID="hfThresholdName"/>
                <asp:HiddenField runat="server" ID="hfUnit"/>
                <asp:HiddenField runat="server" ID="hfIsValid" Value="1"/>
            </td>
            <td class="rightInputColumn">
                <div runat="server" ID="sectionOverrideGeneral">
                    <asp:CheckBox Text="<%$ Resources:VimWebContent,WEBDATA_LV0_40  %>" runat="server" ID="chbOverrideGeneral" /> <%--Override Orion General Thresholds --%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-left: 20px; padding-right: 20px;">
                <div runat="server" ID="sectionThresholdDefinition" class="thresholdControl">
                    <table width="100%">
                        <tr>
                            <td>
                                <div ID="sectionGeneralThresholds" runat="server" class="thresholdGlobal">
                                    <table>
                                        <tr>
                                            <td style="width: 30px">
                                                <img alt="<%= Resources.CoreWebContent.WEBDATA_TK0_46 %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" />
                                            </td>
                                            <td style="width: 180px">
                                                <%= Resources.CoreWebContent.WEBDATA_TK0_46 %>: <%-- Warning --%>
                                            </td>
                                            <td>
                                                <%= Resources.CoreWebContent.WEBDATA_TK0_52 %> <asp:Label runat="server" ID="lblGeneralWarningValue"></asp:Label> <%-- greater than --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img alt="<%= Resources.CoreWebContent.WEBDATA_TK0_47 %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" />
                                            </td>
                                            <td>
                                                <%= Resources.CoreWebContent.WEBDATA_TK0_47 %>: <%-- Critical --%>
                                            </td>
                                            <td>
                                                <%= Resources.CoreWebContent.WEBDATA_TK0_52 %> <asp:Label runat="server" ID="lblGeneralCriticalValue"></asp:Label> <%-- greater than --%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div ID="sectionCustomThresholds" runat="server" class="thresholdCustom" style="width: 100%">
                                    <script type="text/javascript">
                                        function ApplyThresholdValuesOf<%#((System.Data.DataRow)Container.DataItem)["Name"].ToString().Replace(".","_")%>(result,warning,critical) 
                                        {
                                            if (result == true) {
                                                if (warning != null && warning != '') {
                                                    GetThresholdSectionControl('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>', 'lblComputedWarningValue').innerHTML = '';
                                                    GetThresholdSectionControl('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>', 'tbWarningValue').value = warning;
                                                }
                                                if (critical != null && critical != '') {
                                                    GetThresholdSectionControl('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>', 'lblComputedCriticalValue').innerHTML = '';
                                                    GetThresholdSectionControl('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>', 'tbCriticalValue').value = critical;
                                                }

                                                Recompute('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>');
                                            }
                                        }
                                    </script>
                                    <table style="width: 100%;" cellspacing="10px">
                                        <tr style="height: 30px;">
                                            <td style="width: 30px"><img alt="<%= Resources.CoreWebContent.WEBDATA_TK0_46 %>" src="/Orion/images/ThresholdControl/Small-Up-Warn.gif" /></td>
                                            <td style="width: 140px;"><%= Resources.CoreWebContent.WEBDATA_TK0_46 %>:</td>
                                            <td style="width: 180px;"><asp:DropDownList cssclass="ddlInTable" runat="server" ID="ddlOperator" /></td>
                                            <td style="width: 225px;"><asp:TextBox runat="server" ID="tbWarningValue" CssClass="formulaTextbox"></asp:TextBox><%#((System.Data.DataRow)Container.DataItem)["Unit"]%></td>
                                            <td ID="tdComputedWarningValue" runat="server"><asp:Label runat="server" ID="lblComputedWarningValue"></asp:Label></td>
                                            <td rowspan="2" style="width: 280px;">
                                                <div runat="server" ID="sectionLatestBaseline" class="baselineInfo">
                                                    <table>
                                                        <tr>
                                                            <td><asp:Button runat="server" ID="btnUseLatestBaseline" Text="<%$ Resources:CoreWebContent,WEBDATA_TK0_48  %>" UseSubmitBehavior="False" CssClass="button" ></asp:Button></td><%-- Use Dynamic Baseline Thresholds--%>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div runat="server" ID="sectionDetailHyperlink">
                                                                    <a href="" onclick="ShowBaselineDetailDialog('<%#((System.Data.DataRow)Container.DataItem)["Name"]%>','<%#((System.Data.DataRow)Container.DataItem)["DisplayName"]%>',ApplyThresholdValuesOf<%#((System.Data.DataRow)Container.DataItem)["Name"].ToString().Replace(".","_")%>);return false;" ><%= Resources.CoreWebContent.WEBDATA_TK0_49 %></a> <%--» Latest Baseline Details--%>
                                                                </div>
                                                                <div class="info">
                                                                    <%= Resources.CoreWebContent.WEBDATA_TK0_50 %> <a target="_blank" href="<%=SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, "OrionBaselineDataCalculation") %>"><%= Resources.CoreWebContent.WEBDATA_TK0_51 %></a>  <%-- Thresholds calculated using baseline data allow for more accurate alerting. --%> <%-- » Learn more --%>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 30px;">
                                            <td><img alt="<%= Resources.CoreWebContent.WEBDATA_TK0_47 %>" src="/Orion/images/ThresholdControl/Small-Up-Critical.gif" /></td>
                                            <td><%= Resources.CoreWebContent.WEBDATA_TK0_47 %>:</td>
                                            <td><asp:Label runat="server" ID="lblOperatorText"></asp:Label></td>
                                            <td><asp:TextBox runat="server" ID="tbCriticalValue" CssClass="formulaTextbox" ></asp:TextBox><%#((System.Data.DataRow)Container.DataItem)["Unit"]%></td>
                                            <td><asp:Label runat="server" ID="lblComputedCriticalValue"></asp:Label></td>
                                        </tr>
                                    </table>

                                </div>
                                <div runat="server" id="sectionErrorMessage" style="width: 600px;" class="sw-suggestion sw-suggestion-fail"></div>
                                <div runat="server" id="sectionWarningMessage" style="width: 600px;" class="sw-suggestion sw-suggestion-warn"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </ItemTemplate>
</asp:Repeater>
<orion:BaselineDetails runat="server" ID="BaselineDetails" />
</div>

