﻿using System;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.VIM.Web.Resources;
using System.Data;

public partial class Orion_VIM_Resources_VMobjectWithProblemsImpl : System.Web.UI.UserControl
{

    private VimVMObjectsWithProblemsControl _resource;
    public VimVMObjectsWithProblemsControl Resource { 
        get { 
            return _resource; 
        }

        set
        {
            _resource = value;
            if (value != null)
            {
                ProcessLoad();
            }
        }
    }


    protected void ProcessLoad()
    {
        DataTable table = Resource.LoadData();
        if (table != null)
        {
            SolarWinds.VIM.Web.Helpers.PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", this.Resource.NodeEntity);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();
        }
        else
        {
            this.SQLErrorPanel.Visible = true;
        }
    }

    /// <summary>
    /// Get Displayed VMObjectType name; i18n
    /// </summary>
    /// <param name="item">VMObjectType item</param>
    /// <returns>localized name of VMObjectType item</returns>
    internal string GetDisplayedVMObjectType(Enum item)
    {
        ResourceManagerRegistrar registrar = ResourceManagerRegistrar.Instance;
        string key = registrar.CleanResxKey(item.GetType().Name, item.ToString());
        string result = registrar.GetResourceString("VIM.Strings", key);
        return string.IsNullOrEmpty(result) ? item.ToString() : result;
    }  
}