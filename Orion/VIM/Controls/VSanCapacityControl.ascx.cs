﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.VIM.Web.Common.Helpers;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Models;

public partial class Orion_VIM_Controls_VSanCapacityControl : System.Web.UI.UserControl
{
    public long? VSanId { get; set; }

    public long TotalUsed { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsVSanEnabled())
        {
            IEnumerable<VSanObjectSpaceSummary>  objectSpaceSummaries = new VSanDal().GetVSanObjectSpaceSummaries(VSanId.Value);
            if (objectSpaceSummaries.IsNullOrEmpty())
            {
                return;
            }

            TotalUsed = objectSpaceSummaries.Sum(o => o.UsedCapacity);

            SpaceSummariesRepeater.DataSource = objectSpaceSummaries.OrderByDescending(o => o.UsedCapacity);
            SpaceSummariesRepeater.DataBind();
        }
    }

    public string ToDisplayName(SpaceUsageObjectType objectType)
    {
        switch (objectType)
        {
            case SpaceUsageObjectType.ChecksumOverhead:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_ChecksumOverhead;
            case SpaceUsageObjectType.Unknown:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Unknown;
            case SpaceUsageObjectType.DedupOverhead:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_DedupOverhead;
            case SpaceUsageObjectType.FileSystemOverhead:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_FileSystemOverhead;
            case SpaceUsageObjectType.IscsiLun:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_IscsiLun;
            case SpaceUsageObjectType.IscsiTarget:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_IscsiTarget;
            case SpaceUsageObjectType.Namespace:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Namespace;
            case SpaceUsageObjectType.Other:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Other;
            case SpaceUsageObjectType.SpaceUnderDedupConsideration:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_SpaceUnderDedupConsideration;
            case SpaceUsageObjectType.Statsdb:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Statsdb;
            case SpaceUsageObjectType.Vdisk:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Vdisk;
            case SpaceUsageObjectType.Vmem:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Vmem;
            case SpaceUsageObjectType.Vmswap:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Vmswap;
            default:
                return Resources.VIMWebContent.VsanBreakdown_ObjectType_Unknown;
        }
    }

    public string FormatValue(long usedSpace, long totalUsed)
    {
        string format = Resources.VIMWebContent.VsanBreakdown_ValueFormat;
        return String.Format(format, FormatHelper.FormatBytes(usedSpace, ByteDisplayValueFormat.Short), 
            FormatHelper.FormatPercentage(totalUsed != 0 ? (usedSpace / (float)totalUsed) : 0));
    }

    private bool IsVSanEnabled() => VSanId.HasValue;
}