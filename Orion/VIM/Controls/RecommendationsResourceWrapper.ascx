﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecommendationsResourceWrapper.ascx.cs" Inherits="Orion_VIM_Controls_RecommendationsResourceWrapper" %>
<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <asp:Panel runat="server" ID="NoContentPanel">
            <div class="sw-recommendations-disabled">
            <img src="<%= ImageUrl %>" alt="<%= ImageTitle %>" />
                <div class="sw-recommendations-disabled-text">
                    <ul>
                        <%= Description %>
                    </ul>
                </div>
            </div>
        </asp:Panel>
        <asp:PlaceHolder runat="server" ID="WrapperResourcePlaceHolder" />
    </Content>
</orion:resourceWrapper>