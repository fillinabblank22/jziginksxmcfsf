﻿SW = SW || {};
SW.VIM = SW.VIM || {};


SW.VIM.ManagementActionDialog = function (actionType, actionName, titleName, managementObjectId, returnUrl, cancelHandler, restartNedeed, parameters, vmName) {
    if (VIMIsDemoMode()) {
        demoAction("VIM_ManagementTools_" + actionType, this);
        return;
    }

    var dialog;
    if (!parameters)
        parameters = [];

    function onClose() {
         if (dialog) {
             dialog.dialog("close");
             dialog = null;
         }
    };

    function onManage() {
        if ($('#vimshowname').length > 0 && $('#vimshowname')[0].checked && $('#vimsnapshotname').length > 0)
            parameters = [$('#vimsnapshotname')[0].value];

        if (dialog) {
            dialog.dialog("close");
            dialog = null;
        }
        showManagementActionsStatusBar(managementObjectId, actionType, returnUrl, parameters, restartNedeed, vmName);
    };
    
    function refreshNameEditor() {
        $("#vimsnapshotname")[0].style.display = $('#vimshowname')[0].checked ? "block" : "none";
    }

    var textHtml =
            '<div>' +
                '<div id = "sw-vim-dialogcontent" style="height:70px; background-color:white; display:block;">' +
                    '<table>' +
                        '<tr>' +
                            '<td style="width:40px;">' +
                                '<img src="/Orion/VIM/Images/not_available_icon40x40.png" alt="" />' +
                            '</td>' +
                            '<td>' +
                                actionName +
                            '</td>' +
                        '</tr>' +
                        '{1}' +
                    '</table>' +
                    '{0}' +
                '</div>' +
            '</div>';
    var enhancement = '';
    if (actionType == 'TakeSnapshot') {
        enhancement = '<tr>' +
            '<td>' +
            '</td>' +
            '<td>' +
            '<input id="vimshowname" type="checkbox">@{R=VIM.Strings;K=VIMWEBJS_VZ0_9;E=js}<br>' +
            '<input id="vimsnapshotname" type="text" name="txt"  style="width:100%;display:none">' +
            '</td>' +
            '</tr>';
    } 
    var dialogButtons = '<br /><div style="float: right">';

    if (actionType == 'DeleteVM' || actionType == 'UnregisterVM' || actionType == 'DeleteDatastoreFile' ) {
        dialogButtons += SW.Core.Widgets.Button((actionType == 'DeleteVM' || actionType == 'DeleteDatastoreFile') ? "@{R=VIM.Strings;K=VIMWEBJS_VB0_16;E=js}" : "@{R=VIM.Strings;K=VIMWEBJS_ZS0_11;E=js}", { type: 'secondary', id: 'btnYes' });
        dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=CommonButtonType_Cancel;E=js}", { type: 'primary', id: 'btnNo' });
        dialogButtons += '</div>';
    } else {
        dialogButtons += SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_ZS0_2;E=js}", { type: 'primary', id: 'btnYes' });
        dialogButtons += "&nbsp;" + SW.Core.Widgets.Button("@{R=VIM.Strings;K=VIMWEBJS_ZS0_3;E=js}", { type: 'secondary', id: 'btnNo' });
        dialogButtons += '</div>';
    }
    textHtml = String.format(textHtml, dialogButtons, enhancement);

    var $dialog = $(textHtml);
    $dialog.find("#btnYes").click(function () { onManage(); });
    $dialog.find("#btnNo").click(function () { onClose(); });
    $dialog.find("#vimshowname").click(function () { refreshNameEditor(); });
    dialog = $dialog.dialog({
        width: 'auto',
        title: titleName,
        modal: true,
        resizable: false,
        open: function () {
            $('.ui-dialog-content').addClass('sw-vim-vmmanag-ui-dialog-content');
        },
        close: function () {
            if (typeof cancelHandler === 'function' && closeTrigger != "btnYes") {
                cancelHandler();
            }
            $(this).dialog('destroy').remove();
        }
    });
    if (actionType == 'DeleteVM' || actionType == 'UnregisterVM' || actionType == 'DeleteDatastoreFile') {
        $dialog.find("#btnNo").focus();
    }
}
