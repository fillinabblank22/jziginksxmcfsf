﻿using System;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
public partial class Orion_VIM_Controls_DataStoreDetails : ResourceControl
{
    public int NodeID;
    [PersistenceMode(PersistenceMode.Attribute)]
    public String netObjectType;
    [PersistenceMode(PersistenceMode.Attribute)]
    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }


    protected override string DefaultTitle
    {
        get { return VIMWebContent.VIMWEBCODE_VB0_50; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            table = (new VMHostDetailsDAL()).GetDataStoreDetails(this.NodeID, this.netObjectType);
        }
        catch (SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }
        if (table != null && table.Rows.Count > 0)
        {
            ErrorLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationError.SettingValue);
            WarningLevel = Convert.ToInt32(Thresholds.IfPercentUtilizationWarning.SettingValue);
            fvESXDetailsFormView.DataSource = table;
            fvESXDetailsFormView.DataBind();

        }


    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public Boolean GetBarVisible()
    {
        return true;
    }


}