﻿<%@ Control Language="C#" ClassName="ManagementActionButton" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Models" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Import Namespace="SolarWinds.VIM.Web.DAL" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Models" %>

<%@ Register TagPrefix="vim" TagName="ActionStatusBar" Src="~/Orion/VIM/Controls/ManagementActionsStatusBar.ascx" %>
<orion:include runat="server" module="VIM" file="Controls/ManagementActionDialog.js" />
<orion:include runat="server" module="Orion" file="NodeMNG.css" />

<vim:actionstatusbar runat="server" />
<script runat="server">
    private bool _isDemoMode = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
    private bool _encapsulateBySpan = true;

    public ManagementEnums.ActionName Action { get; set; }
    public string Text { get; set; }
    public int VmId { get; set; }

    public string ReturnUrl { get; set; }
    public bool RestartNedeed { get; set; }
    public bool WithWarning { get; set; }

    public bool IsDemoMode
    {
        get { return _isDemoMode; }
        set { _isDemoMode = value; }
    }

    public bool EncapsulateBySpan
    {
        get { return _encapsulateBySpan; }
        set { _encapsulateBySpan = value; }
    }

    public ManagementActionButton(ManagementEnums.ActionName action, int vmId)
    {
        Action = action;
        VmId = vmId;
    }

    protected void Page_Load(Object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Text))
        {
            Text = GetActionInnerText();
        }
        link.InnerHtml = String.Format("{0}&nbsp;{1}", GetActionIconCode(), Text);

        Uri uriResult;
        if (string.IsNullOrEmpty(ReturnUrl) || (!Uri.TryCreate(ReturnUrl, UriKind.RelativeOrAbsolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttp))
        {
            ReturnUrl = null;
        }

        var vmDal = new VirtualMachineDAL();
        VirtualMachineModel vm = vmDal.GetVMModelByVMId(VmId);

        if (!CanBePerformed(vm))
        {
            Visible = false;
            return;
        }

        var vmName = vm.Name != null ? HttpUtility.JavaScriptStringEncode(vm.Name) : null;

        switch (Action)
        {
            case ManagementEnums.ActionName.DeleteSnapshots:
                {
                    OrionInclude.ModuleFile("VIM", "/js/DeleteSnapshotDialog.js");
                    OrionInclude.ModuleFile("VIM", "/js/jsTree/jquery.jstree.js");
                    OrionInclude.ModuleFile("VIM", "/Styles/DeleteSnapshotDialog.css");
                    link.Attributes.Add("onclick", String.Format("SW.VIM.DeleteSnapshotDialog('{0}','{1}','{2}','{3}',{4}, '{5}')",
                        Action, GetActionName(vmName), GetActionTitle(), vmName, VmId, "2"));
                    break;
                }
            case ManagementEnums.ActionName.ChangeSettings:
                {
                    OrionInclude.ModuleFile("VIM", "/js/ChangeResourceSettingsDialog.js");
                    link.Attributes.Add("onclick", String.Format("SW.VIM.ChangeSettingsDialog('{0}','{1}','{2}',{3}, '{4}', {5}, '{6}', '{7}')",
                        Action, GetActionName(vmName), GetActionTitle(), VmId, vm.HostPlatform, "2", vm.PowerState.StateType != VirtualMachineStateType.PoweredOff, vmName));
                    break;
                }
            case ManagementEnums.ActionName.PerformMigration:
                {
                    OrionInclude.ModuleFile("VIM", "/js/MigrationDialog.js");
                    link.Attributes.Add("onclick", String.Format("SW.VIM.MigrationDialog('{0}','{1}','{2}','{3}',{4}, {5}, '{6}', '{7}')",
                        Action, GetActionName(vmName), GetActionTitle(), vmName, VmId, vm.HostID, vm.HostPlatform, "2"));
                    break;
                }
            case ManagementEnums.ActionName.PerformRelocation:
                {
                    var status = (new VMDetailsDAL()).GetVMDetails(VmId).Status;
                    var vimSettings = new VimSettings();
                    OrionInclude.ModuleFile("VIM", "/js/RelocationDialog.js");
                    link.Attributes.Add("onclick", String.Format("SW.VIM.RelocationDialog('{0}','{1}','{2}','{3}',{4}, {5}, '{6}', '{7}', '{8}', '{9}')",
                        Action, GetActionName(vmName), GetActionTitle(), vmName, VmId, vm.HostID, vm.HostPlatform, "2", status, vimSettings.DefaultHyperVRelocationPath));
                    break;
                }
            default:
                link.Attributes.Add("onclick", String.Format("SW.VIM.ManagementActionDialog('{0}','{1}','{2}',{3}, '{4}', {5}, '{6}', '', '{7}')",
                    Action, GetActionName(vmName), GetActionTitle(), VmId, ReturnUrl ?? vm.HostDetailsUrl, "2", RestartNedeed, vmName));
                break;
        }
    }

    private bool CanBePerformed(VirtualMachineModel vm)
    {
        if (vm == null || vm.PowerState.StateType == VirtualMachineStateType.Unknown)
            return false;

        if (!PlatformHelper.IsManagementActionSupported(Action, vm.HostPlatform))
            return false;

        bool canTakeSnapshot = vm.PowerState.StateType == VirtualMachineStateType.PoweredOn || vm.PowerState.StateType == VirtualMachineStateType.PoweredOff || vm.PowerState.StateType == VirtualMachineStateType.Suspended;

        switch (Action)
        {
            case ManagementEnums.ActionName.PowerOn:
                return VimProfile.AllowPowerManagement && vm.PowerState.StateType == VirtualMachineStateType.PoweredOff;
            case ManagementEnums.ActionName.Resume:
                return VimProfile.AllowPowerManagement && vm.PowerState.StateType == VirtualMachineStateType.Suspended || vm.PowerState.StateType == VirtualMachineStateType.Paused;
            case ManagementEnums.ActionName.Suspend:
            case ManagementEnums.ActionName.PowerOff:
                return VimProfile.AllowPowerManagement && vm.PowerState.StateType == VirtualMachineStateType.PoweredOn || vm.PowerState.StateType == VirtualMachineStateType.Paused;
            case ManagementEnums.ActionName.Reboot:
            case ManagementEnums.ActionName.Pause:
                return VimProfile.AllowPowerManagement && vm.PowerState.StateType == VirtualMachineStateType.PoweredOn;
            case ManagementEnums.ActionName.TakeSnapshot:
                return VimProfile.AllowSnapshotManagement && canTakeSnapshot;
            case ManagementEnums.ActionName.DeleteSnapshots:
                return VimProfile.AllowSnapshotManagement;
            case ManagementEnums.ActionName.ChangeSettings:
                return VimProfile.AllowResourcesSettingsManagement;
            case ManagementEnums.ActionName.DeleteVM:
            case ManagementEnums.ActionName.UnregisterVM:
                return VimProfile.AllowDeleteVMAction && vm.PowerState.StateType != VirtualMachineStateType.PoweredOn;
            case ManagementEnums.ActionName.PerformMigration:
            case ManagementEnums.ActionName.PerformRelocation:
                // VMware does not support migration without VCenter
                return VimProfile.AllowMigrationManagement && (vm.HostPlatform != VirtualizationPlatform.Vmware || vm.HostPollingMethod != HostPollingMethod.Directly);

            default:
                return false;
        }
    }

    private string GetActionName(string vmName)
    {
        switch (Action)
        {
            case ManagementEnums.ActionName.PowerOn:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_6, vmName);
            case ManagementEnums.ActionName.Resume:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_10, vmName);
            case ManagementEnums.ActionName.PowerOff:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_8, vmName);
            case ManagementEnums.ActionName.Suspend:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_12, vmName);
            case ManagementEnums.ActionName.Pause:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_14, vmName);
            case ManagementEnums.ActionName.Reboot:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_23, vmName);
            case ManagementEnums.ActionName.TakeSnapshot:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_16, vmName);
            case ManagementEnums.ActionName.DeleteSnapshots:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_18, vmName);
            case ManagementEnums.ActionName.ChangeSettings:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_50, vmName);
            case ManagementEnums.ActionName.DeleteVM:
                return String.Format(WithWarning ? Resources.VIMWebContent.VIMWEBDATA_ZS0_67 : Resources.VIMWebContent.VIMWEBDATA_ZS0_51, vmName);
            case ManagementEnums.ActionName.UnregisterVM:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_ZS0_53, vmName);
            case ManagementEnums.ActionName.PerformMigration:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_VZ0_4, vmName);
            case ManagementEnums.ActionName.PerformRelocation:
                return String.Format(Resources.VIMWebContent.VIMWEBDATA_VZ0_5, vmName);
            default:
                return String.Empty;
        }
    }

    private string GetActionTitle()
    {
        switch (Action)
        {
            case ManagementEnums.ActionName.PowerOn:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_5;
            case ManagementEnums.ActionName.Resume:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_9;
            case ManagementEnums.ActionName.PowerOff:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_7;
            case ManagementEnums.ActionName.Suspend:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_11;
            case ManagementEnums.ActionName.Pause:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_13;
            case ManagementEnums.ActionName.Reboot:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_22;
            case ManagementEnums.ActionName.TakeSnapshot:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_2;
            case ManagementEnums.ActionName.DeleteSnapshots:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_3;
            case ManagementEnums.ActionName.ChangeSettings:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_49;
            case ManagementEnums.ActionName.DeleteVM:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_52;
            case ManagementEnums.ActionName.UnregisterVM:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_54;
            case ManagementEnums.ActionName.PerformMigration:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_6;
            case ManagementEnums.ActionName.PerformRelocation:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_7;
            default:
                return String.Empty;
        }
    }

    private string GetActionInnerText()
    {
        switch (Action)
        {
            case ManagementEnums.ActionName.PowerOn:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_5;
            case ManagementEnums.ActionName.Resume:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_9;
            case ManagementEnums.ActionName.PowerOff:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_7;
            case ManagementEnums.ActionName.Suspend:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_11;
            case ManagementEnums.ActionName.Pause:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_13;
            case ManagementEnums.ActionName.Reboot:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_22;
            case ManagementEnums.ActionName.TakeSnapshot:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_15;
            case ManagementEnums.ActionName.DeleteSnapshots:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_17;
            case ManagementEnums.ActionName.ChangeSettings:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_49;
            case ManagementEnums.ActionName.DeleteVM:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_52;
            case ManagementEnums.ActionName.UnregisterVM:
                return Resources.VIMWebContent.VIMWEBDATA_ZS0_54;
            case ManagementEnums.ActionName.PerformMigration:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_6;
            case ManagementEnums.ActionName.PerformRelocation:
                return Resources.VIMWebContent.VIMWEBDATA_VZ0_7;
            default:
                return String.Empty;
        }
    }

    private string GetActionIconPath()
    {
        switch (Action)
        {
            case ManagementEnums.ActionName.PowerOn:
                return "/Orion/VIM/images/ManagementVM/power_on_VM_icons16x16.png";
            case ManagementEnums.ActionName.Resume:
                return "/Orion/VIM/images/ManagementVM/power_on_VM_icons16x16.png";
            case ManagementEnums.ActionName.PowerOff:
                return "/Orion/VIM/images/ManagementVM/power_off_VM_icons16x16.png";
            case ManagementEnums.ActionName.Suspend:
                return "/Orion/VIM/images/ManagementVM/suspend_icons16x16.png";
            case ManagementEnums.ActionName.Pause:
                return "/Orion/VIM/images/ManagementVM/pause_icons16x16.png";
            case ManagementEnums.ActionName.Reboot:
                return "/Orion/VIM/images/ManagementVM/rebootVM_icons16x16.png";
            case ManagementEnums.ActionName.TakeSnapshot:
                return "/Orion/VIM/images/ManagementVM/take_snapshot_icons16x16v2.png";
            case ManagementEnums.ActionName.DeleteSnapshots:
                return "/Orion/VIM/images/ManagementVM/delete_snapshots_icons16x16.png";
            case ManagementEnums.ActionName.ChangeSettings:
                return "/Orion/VIM/images/ManagementVM/Right-size_icons16x16.png";
            case ManagementEnums.ActionName.DeleteVM:
                return "/Orion/VIM/images/ManagementVM/delete_icons16x16.png";
            case ManagementEnums.ActionName.UnregisterVM:
                return "/Orion/VIM/images/ManagementVM/UnregisterVM_icon16x16.png";
            case ManagementEnums.ActionName.PerformMigration:
                return "/Orion/VIM/images/ManagementVM/vMotion_icons16x16.png";
            case ManagementEnums.ActionName.PerformRelocation:
                return "/Orion/VIM/images/ManagementVM/Storage_icons16x16.png";
            default:
                return String.Empty;
        }
    }

    private string GetActionIconCode()
    {
        return String.Format("<img src='{0}' />", GetActionIconPath());
    }



</script>

<% if (EncapsulateBySpan)
   { %><span class="sw-vim-vmantools-buttons"> <% } %>

       <a id="link" runat="server" href="javascript:void(0);"></a>

       <% if (EncapsulateBySpan)
          { %></span> <% } %>


