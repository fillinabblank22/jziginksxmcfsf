﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMGuestsTopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_VMGuestsTopXXEditControl" %>

<%@ Register src="~/Orion/VIM/Controls/TopXXEditControl.ascx" tagname="TopXXEditControl" tagprefix="vim" %>

<vim:TopXXEditControl runat="server" ID="editTopXX" 
                      MaxCountDisplayText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_59 %>"
                      FilterDevicesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_60 %>"
                      FilterText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_61 %>"
                      SamplePropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_62 %>"
                      ListOfPropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_63 %>" />