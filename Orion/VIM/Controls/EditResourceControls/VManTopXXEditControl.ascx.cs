﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Controls_EditResourceControls_VManTopXXEditControl : BaseResourceEditControl, IChartEditorSettings
{
    protected void Page_Load(object sender, EventArgs e)
    {
        editTopXX.Resource = this.Resource;
        editTopXX.NetObjectID = this.NetObjectID;
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return editTopXX.Properties;
        }
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        if (!string.IsNullOrEmpty(resourceInfo.Properties["MaxRecords"]))
            editTopXX.MaxRecords = resourceInfo.Properties["MaxRecords"];
        else
            editTopXX.MaxRecords = "10";
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        properties.Add("MaxRecords", editTopXX.MaxRecords);
    }
}