﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMVCenterTopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_VMVCenterTopXXEditControl" %>

<%@ Register src="~/Orion/VIM/Controls/TopXXEditControl.ascx" tagname="TopXXEditControl" tagprefix="vim" %>

<vim:TopXXEditControl runat="server" ID="editTopXX" 
                      MaxCountDisplayText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_44 %>"
                      FilterDevicesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_45 %>"
                      FilterText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_46 %>"
                      SamplePropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_47 %>"
                      ListOfPropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_48 %>" />