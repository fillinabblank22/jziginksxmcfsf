﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Base.Contract.Constants;

public partial class Orion_VIM_Controls_EditControls_VMDataCenterTopXXEditControl : BaseResourceEditControl
{
    protected const string HelpFragment = "OrionFilters_IVIM";

    protected void Page_Load(object sender, EventArgs e)
    {
        editTopXX.Resource = this.Resource;
        editTopXX.NetObjectID = this.NetObjectID;
        if (!this.IsPostBack)
        {
            editTopXX.SampleFiltersText = String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_10, 
                                                        String.Format("<a href=\"{0}\">", HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, HelpFragment)), "<a>");
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            return editTopXX.Properties;
        }
    }
}