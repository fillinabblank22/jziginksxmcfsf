﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManStorageSummaryEditControl.ascx.cs" Inherits="Orion_VIM_Controls_PlatformEditControl" %>
<%@ Register src="~/Orion/VIM/Controls/EditControls/FilterByPlatform.ascx" tagname="FilterByPlatform" tagprefix="vim" %>

<div>
    <vim:filterbyplatform id="FilterByPlatform" runat="server" />
</div>
