﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SprawlOrphanedVMDKsEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditResourceControls_SprawlOrphanedVMDKsEditControl" %>

<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>

<link rel="stylesheet" type="text/css" href="/Orion/VIM/VIM.css" />
<link rel="stylesheet" type="text/css" href="/Orion/styles/Events.css" />
<link rel="stylesheet" type="text/css" href="/WebEngine/Resources/SlateGray.css" />



<orion:Refresher ID="Refresher1" runat="server" />

<div style="padding-bottom: 10px;">
    <div>
        <b><%= Resources.VIMWebContent.VIMWEBDATA_JD0_22 %></b>
    </div>
    
    <div>
        <asp:TextBox runat="server" ID="maxCount" Columns="5" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCount"
            SetFocusOnError="true">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCount"
            Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1" ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_65%>"
            SetFocusOnError="true"></asp:RangeValidator>
    </div>
    <br />
    
    <div>
        <b><%= Resources.VIMWebContent.VIMWEBDATA_JD0_23 %></b>
    </div>
    
    <div>
        <asp:TextBox runat="server" ID="minDiskSize" Columns="5" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="minDiskSize"
            SetFocusOnError="true">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="minDiskSize"
            Display="Dynamic" Type="Integer" MaximumValue="1000000" MinimumValue="0" ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_65%>"
            SetFocusOnError="true"></asp:RangeValidator>
    </div>
    
</div>