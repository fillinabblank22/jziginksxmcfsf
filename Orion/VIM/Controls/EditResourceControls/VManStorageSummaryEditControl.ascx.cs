﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Controls_PlatformEditControl : BaseResourceEditControl
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            FilterByPlatform.LoadFromResourceProperties(Resource.Properties);
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();
            FilterByPlatform.SaveProperties(properties);
            return properties;
        }
    }

}