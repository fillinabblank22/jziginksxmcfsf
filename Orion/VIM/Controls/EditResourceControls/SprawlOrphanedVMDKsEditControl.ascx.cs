﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Controls_EditResourceControls_SprawlOrphanedVMDKsEditControl : BaseResourceEditControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            int intval;
            if (!Int32.TryParse(this.Resource.Properties["RowsPerPage"], out intval))
            {
                this.Resource.Properties["RowsPerPage"] = "10";
            }
            this.maxCount.Text = this.Resource.Properties["RowsPerPage"];

            if (!Int32.TryParse(this.Resource.Properties["MinDiskSize"], out intval))
            {
                this.Resource.Properties["MinDiskSize"] = "10";
            }
            this.minDiskSize.Text = this.Resource.Properties["MinDiskSize"];
        }
    }

    public override Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            properties["RowsPerPage"] = this.maxCount.Text;
            properties["MinDiskSize"] = this.minDiskSize.Text;

            return properties;
        }
    }
}