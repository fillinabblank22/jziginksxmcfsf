﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMDataCenterTopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_VMDataCenterTopXXEditControl" %>

<%@ Register src="~/Orion/VIM/Controls/TopXXEditControl.ascx" tagname="TopXXEditControl" tagprefix="vim" %>

<vim:TopXXEditControl runat="server" ID="editTopXX" 
                      MaxCountDisplayText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_49 %>"
                      FilterDevicesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_50 %>"
                      FilterText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_51 %>"
                      SamplePropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_52 %>"
                      ListOfPropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_53 %>" />