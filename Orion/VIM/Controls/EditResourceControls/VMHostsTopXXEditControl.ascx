﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMHostsTopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_VMHostsTopXXEditControl" %>

<%@ Register src="~/Orion/VIM/Controls/TopXXEditControl.ascx" tagname="TopXXEditControl" tagprefix="vim" %>

<vim:TopXXEditControl runat="server" ID="editTopXX" 
                      MaxCountDisplayText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_54 %>"
                      FilterDevicesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_55 %>"
                      FilterText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_56 %>"
                      SamplePropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_57 %>"
                      ListOfPropertiesText="<%$ Resources:VIMWebContent, VIMWEBDATA_VB0_58 %>" />