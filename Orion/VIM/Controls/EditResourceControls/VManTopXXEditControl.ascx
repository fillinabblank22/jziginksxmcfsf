﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManTopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditResourceControls_VManTopXXEditControl" %>

<%@ Register src="~/Orion/VIM/Controls/TopXXEditControl.ascx" tagname="TopXXEditControl" tagprefix="vim" %>

<vim:TopXXEditControl runat="server" ID="editTopXX" 
                      MaxCountDisplayText="<%$ Resources:VIMWebContent, VIMWEBDATA_LC0_50 %>"
                      EnableFiltering="False" />