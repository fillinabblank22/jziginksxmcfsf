﻿using System;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Models;

public partial class Orion_VIM_Controls_VSanDedupControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsVSanEnabled())
        {
            VSanInfo = new VSanDal().GetVSanInfo(this.VSanId.Value);
            if (VSanInfo == null)
            {
                DedupEnabledHeaderRow.Visible = false;
                return;
            }

            DedupEnabledValue.InnerText = Resources.VIMWebContent.VsanInfo_VsanEnabled;

            if (!this.HasCompressionAndDeduplicationEnabled())
            {
                DedupEnabledValue.InnerText = Resources.VIMWebContent.VsanInfo_VsanDisabled;
            }
        }
        else
        {
            DedupEnabledHeaderRow.Visible = false;
        }
    }

    public VSanInfo VSanInfo { get; private set; }

    public long? VSanId { get; set; }

    public bool IsVSanEnabled() => VSanId.HasValue;

    public bool HasCompressionAndDeduplicationEnabled() => VSanInfo != null ? VSanInfo.IsDedupEnabled : false;
}