using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.UI;

using SolarWinds.Orion.Web.DAL;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Common.Enums;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using SolarWinds.VIM.Web.Controls;

public partial class Orion_VIM_Controls_ThresholdSetting : ThresholdSettingBase
{
    

    [PersistenceMode(PersistenceMode.Attribute)]

    public override string WarningTxt 
    {
        get { return WarningTxtValue.Text; }
    }
    public override string HighTxt
    {
        get { return HighTextValue.Text; }
    }
     
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IntializeValues();
        }

        
    }

    

    private void IntializeValues()
    {
        string minMax = string.Format(Resources.VIMWebContent.VIMWEBCODE_VB1_1, 1, Setting.Unit, Setting.Maximum);

        //Threshold Header
        this.ThresholdHeader.Controls.Add(new LiteralControl(ThresholdHeaderValue));
        
        if (Description != null)
            HighDesc.Controls.Add(new LiteralControl(Description));

        //Warning Level
        this.WarningTxtValue.Text = Setting.Warning.ToString();
        if (HasWarning)
        {
            this.Warning.Controls.Add(new LiteralControl(Resources.VIMWebContent.VIMWEBCODE_VB1_2));
            this.WarnMinMax.Controls.Add(new LiteralControl(minMax));
            this.WarnDesc.Controls.Add(new LiteralControl(""));
        }
        else
        {
            this.WarningTxtValue.Visible = false;
        }
        //High Level
        this.High.Controls.Add(new LiteralControl(Resources.VIMWebContent.VIMWEBCODE_VB1_3));
        this.HighMinMax.Controls.Add(new LiteralControl(minMax));
        this.HighDesc.Controls.Add(new LiteralControl(""));
        this.HighTextValue.Text = Setting.High.ToString();
        this.RangeValidator1.MaximumValue =
        this.RangeValidator2.MaximumValue = Setting.Maximum.ToString();

    }





    

   
    
}
