<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditVimChartControl.ascx.cs"
    Inherits="Orion_VIM_Controls_EditResourceControls_EditVMVareChart" %>
<%@ Register TagPrefix="orion" TagName="EditSampleSize" Src="~/Orion/Controls/SampleSizeControl.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditPeriod" Src="~/Orion/Controls/TimePeriodControl.ascx" %>

<p style="margin:0">
    <b><%= Resources.VIMWebContent.VIMWEBDATA_VB0_93%>:</b><br/>
    <asp:TextBox runat="server" ID="chartSubTitle2" size="30"></asp:TextBox>
</p>


<table width="750px" cellpadding="8" border="0">
    <orion:EditPeriod runat="server" ID="timePeriodControl" />
    <br />
    <orion:EditSampleSize runat="server" ID="sampleSizeControl" />
</table>
