﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignVMWareCredentialsDialog.ascx.cs"
    Inherits="Orion_VIM_Controls_AssignVMWareCredentialsDialog" %>

<orion:Include runat="server" Module="VIM" File="/js/OrionCoreScriptServiceProxy.js" />
<orion:Include runat="server" Module="VIM" File="/js/ScriptServiceInvoker.js" />
<orion:Include runat="server" Module="VIM" File="/js/WebService.js" />
<orion:Include runat="server" Module="VIM" File="/js/DemoServerHelper.js" />
<orion:Include runat="server" Module="VIM" File="/Controls/AssignVmWareCredentials.js" />

<style type="text/css">
    .dialogBody table td
    {
        padding-top: 5px;
        padding-left: 10px;        
    }
    
    #assignDialog td
    {
        border-style: none;
        font-size: small;
        font-family: tahoma;
    }
    #testButton table td
    {
        padding: 0px !important;
    }
    #assignDialog td.x4-btn-center
    {
        text-align: center !important;
    }
    
    .no-close .ui-dialog-titlebar-close { display: none; }
</style>

<div id="assignDialog" class="x4-hidden" style="display: none">
    <div class="x4-window-header">
        <%= Resources.VIMWebContent.VIMWEBDATA_AK0_33%></div>
    <div id="assignDialogBody" class="x4-panel-body dialogBody">

        <%-- Render the DIV control only in demo mode --%>
        <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
            <div id="isDemoMode" style="display:none;"></div>
        <%}%>

        <table>
            <tr>
                <td colspan='2'>
                    <%= Resources.VIMWebContent.VIMWEBDATA_AK0_34%><br/><br/>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_4%></span>
                </td>
                <td>
                    <select id="assignList" style="width: 148px">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_3%></span>
                </td>
                <td>
                    <input id="assignDescription" maxlength="50"></input>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_7%></span>
                </td>
                <td>
                    <input id="assignUsername" maxlength="50"></input>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%></span>
                </td>
                <td>
                    <input id="assignPassword" maxlength="50" type="password"></input>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="Label"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_12%></span>
                </td>
                <td>
                    <input id="assignConfirm" maxlength="50" type="password"></input>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <div id="testButton">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <div class="Error">
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
