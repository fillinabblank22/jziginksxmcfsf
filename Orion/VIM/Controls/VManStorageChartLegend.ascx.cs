﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_VIM_Controls_VManStorageChartLegend : System.Web.UI.UserControl, IChartLegendControl
{
    public string LegendInitializer
    {
        get { return "vman_storageChartLegendInitializer__" + legend.ClientID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.ModuleFile("VIM", "js/Charts.VMan.StorageChartLegend.js", OrionInclude.Section.Middle);
    }

}