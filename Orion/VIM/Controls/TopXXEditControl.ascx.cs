﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using ASP;

using SolarWinds.Orion.Web;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_Controls_EditControls_TopXXEditControl : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // when this control is used on chart the resource will be null, so just bail
        if (this.Resource == null)
            return;

        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
            {
                this.Resource.Properties["MaxRecords"] = "10";
            }
            this.maxCount.Text = this.Resource.Properties["MaxRecords"];
            this.filter.Text = this.Resource.Properties["Filter"];

            ucFilterByPlatform.LoadFromResourceProperties(this.Resource.Properties);
        }

        this.DataColumns.Visible = true;
        string entityName = this.Resource.Properties["EntityName"];
        System.Data.DataTable table = new SolarWinds.VIM.Web.DAL.TopXXEditDAL().GetTableColumnNames(entityName);
        this.dataTableColumns.DataSource = table;
        this.dataTableColumns.DataBind();
    }

    #region Resource property

    public ResourceInfo Resource { get; set; }
    public string NetObjectID { get; set; }
    public string MaxRecords
    {
        get { return maxCount.Text; } 
        set { maxCount.Text = value; }
    }

    #endregion

    #region Configurable control properties
    private string sampleFiltersText = Resources.VIMWebContent.VIMWEBDATA_VB0_66;

    public string SampleFiltersText
    {
        get { return sampleFiltersText; }
        set { sampleFiltersText = value; }
    }

    private string maxCountDisplayText = Resources.VIMWebContent.VIMWEBDATA_VB0_54;

    public string MaxCountDisplayText
    {
        get { return maxCountDisplayText; }
        set { maxCountDisplayText = value; }
    }

    private string filterDevicesText = Resources.VIMWebContent.VIMWEBDATA_VB0_55;

    public string FilterDevicesText
    {
        get { return filterDevicesText; }
        set { filterDevicesText = value; }
    }

    private string filterText = Resources.VIMWebContent.VIMWEBDATA_VB0_56;

    public string FilterText
    {
        get { return filterText; }
        set { filterText = value; }
    }

    private string samplePropertiesText = Resources.VIMWebContent.VIMWEBDATA_VB0_57;

    public string SamplePropertiesText
    {
        get { return samplePropertiesText; }
        set { samplePropertiesText = value; }
    }


    private string listOfPropertiesText = Resources.VIMWebContent.VIMWEBDATA_VB0_58;

    public string ListOfPropertiesText
    {
        get { return listOfPropertiesText; }
        set { listOfPropertiesText = value; }
    }

    private bool enableFiltering = true;

    public bool EnableFiltering
    {
        get { return enableFiltering; }
        set { enableFiltering = value; }
    }
    #endregion

    public Dictionary<string, object> Properties
    {
        get
        {
            var properties = new Dictionary<string, object>();

            //TODO: FB235926 we need to remove this when we change Top XX resouces to properly use Resource.Title. 
            var titleEditControl = ControlHelper.FindControlRecursive(this.Page, "TitleEditControl");
            if (titleEditControl != null)
            {
                properties["Title"] = ((EditResourceTitle)titleEditControl).ResourceTitle;
                properties["SubTitle"] = ((EditResourceTitle)titleEditControl).ResourceSubTitle;
            }
            properties[PlatformHelper.VMwareEnabledKey] = ucFilterByPlatform.VMwareEnabled;
            properties[PlatformHelper.HyperVEnabledKey] = ucFilterByPlatform.HyperVEnabled;
            properties[PlatformHelper.NutanixEnabledKey] = ucFilterByPlatform.NutanixEnabled;
            properties["MaxRecords"] = this.maxCount.Text;
            properties["Filter"] = this.filter.Text;

            return properties;
        }
    }
}