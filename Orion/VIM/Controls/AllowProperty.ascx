﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllowProperty.ascx.cs" Inherits="Orion.VIM.Controls.AllowProperty" %>

<asp:RadioButtonList ID="ctrAllowProperty" runat="server">
    <asp:ListItem Selected="True" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_73 %>" Value="True" />
    <asp:ListItem Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_74 %>" Value="False" />
</asp:RadioButtonList>
