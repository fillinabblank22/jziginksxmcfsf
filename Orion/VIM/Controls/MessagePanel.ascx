﻿<%@ Control Language="C#" ClassName="MessagePanel" %>

<script runat="server">
    private string _iconUrl = "/Orion/images/getting_started_36x36.gif";

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Heading { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string Description { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string IconUrl
    {
        get { return _iconUrl; }
        set { _iconUrl = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.DataBind();
    }
</script>

<div class="sw-vman-gettingstarted">
    <div class="sw-vman-first">
        <img src="<%= IconUrl %>" alt="<%= Heading %>" />
        <h3><b><%= Heading %></b></h3>
    </div>

    <div>
        <p>
            <%= Description %>
        </p>
    </div>
</div>