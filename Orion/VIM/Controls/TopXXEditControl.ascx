﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXEditControl.ascx.cs" Inherits="Orion_VIM_Controls_EditControls_TopXXEditControl" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register src="~/Orion/VIM/Controls/EditControls/FilterByPlatform.ascx" tagname="FilterByPlatform" tagprefix="vim" %>
<%@ Register TagPrefix="orion" TagName="TitleEdit" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<link rel="stylesheet" type="text/css" href="/Orion/VIM/VIM.css" />
<link rel="stylesheet" type="text/css" href="/Orion/styles/Events.css" />
<link rel="stylesheet" type="text/css" href="/WebEngine/Resources/SlateGray.css" />

<script type="text/javascript">
            //<![CDATA[
    $(function () {
        $(".ExpanderLink + div").hide();
        $(".ExpanderLink").toggle(
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Minus.gif");
                        $(this).next().slideDown("fast");
                    },
                    function () {
                        $("img:first", this).attr("src", "/NetPerfMon/images/Icon_Plus.gif");
                        $(this).next().slideUp("fast");
                    }
                );
    });
        //]]>
</script>

<orion:Refresher ID="Refresher1" runat="server" />

<div style="padding-bottom: 10px;">
    <div>
        <b><%= MaxCountDisplayText %></b>
    </div>
    
    <div>
        <asp:TextBox runat="server" ID="maxCount" Columns="5" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCount"
            SetFocusOnError="true">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCount"
            Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1" ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_65%>"
            SetFocusOnError="true"></asp:RangeValidator>
    </div>
    
    <% if (EnableFiltering)
       { %>

        <div>
            <vim:filterbyplatform id="ucFilterByPlatform" runat="server" />
        </div>
    
        <div style="padding-top: 10px;">
            <b><%= FilterDevicesText %></b>
        </div>
    
        <div>
            <asp:TextBox runat="server" ID="filter" Columns="60" />
        </div>

        <div style="padding-top: 10px;">
            <%= FilterText %>
        </div>

    <% } %>
</div>

<% if (EnableFiltering)
   { %>

    <a href="#" class="ExpanderLink">
        <img src="/NetPerfMon/images/Icon_Plus.gif" alt="<%= Resources.VIMWebContent.VIMWEBDATA_VB0_92 %>" style="padding-right: 5px;" />
        <b><%= Resources.VIMWebContent.VIMWEBDATA_VB0_67 %></b> 
    </a>

    <div class="text" style="padding-left: 10px;">
        <div>
            <%= SampleFiltersText %>
        </div>

        <asp:Panel ID="DataColumns" runat="server">
            <a href="#" class="ExpanderLink">
                <img src="/NetPerfMon/images/Icon_Plus.gif" alt="<%= Resources.VIMWebContent.VIMWEBDATA_VB0_92%>" style="padding-right: 5px;" />
                <b><%= ListOfPropertiesText %></b> 
            </a>

            <div class="text" style="padding-left: 10px;">
                <%= SamplePropertiesText %>
                <br />

                <ul style="list-style-type: none;">
                    <asp:Repeater runat="server" ID="dataTableColumns">
                        <ItemTemplate>
                            <li>
                                <%#Eval("Name") %></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </asp:Panel>
    </div>
<% } %>
