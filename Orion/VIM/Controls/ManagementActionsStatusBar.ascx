﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManagementActionsStatusBar.ascx.cs" Inherits="Orion_VIM_Controls_ManagementActionsStatusBar"%>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>

<script type="text/javascript">
    //<![CDATA[
    function ProgressDialog(options) {
        var width = 600;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var statusText = $('<div></div>').addClass('NetObjectText').appendTo(statusContent);

        var closeButton = $('<orion:LocalizableButtonLink runat="server" LocalizedText="CustomText" Title="<%$ Resources: VIMWebContent, VIMWEBDATA_ZS0_45 %>" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LC0_62 %>" DisplayType="Secondary" style="float:right; margin-right:5px; margin-left:5px;" />').appendTo(statusText);
        closeButton.tooltip({
            extraClass: "sw-vim-progress-hide-tooltip",
            track: true,
            top: -30,
            left: 65
        });
        
        closeButton.click(function () {
            closeButton.tooltip('close');
            statusBox.dialog('destroy').remove();
        });
        
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;
        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / 1) * 100.0 + '%');
            if (succeeded && ++numSucceeded >= 1) {
                setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1000);
            }
        };

        var successFinish = function (action, hostDetailsUrl) {
            statusText.append('<div class="iconLink success">' + options.sucessMessage + '</div>');

            setTimeout(function () {
                if ((action == "<%= ManagementEnums.ActionName.DeleteVM %>" || action == "<%= ManagementEnums.ActionName.UnregisterVM %>") && hostDetailsUrl) {
                window.location.href = hostDetailsUrl;
                } else {
                window.location.reload(true);
                }
            }, 100);

            onFinished(true);
        };

        var finishedWithErrors = function (message, needRedirectToHost, hostDetailsUrl, actionsData) {
            if (actionsData) {
                var actionDetails = '';
                for (var i = 0; i < actionsData.length; i++) {
                    var action = actionsData[i];
                    if (action.Succeeded) {
                        actionDetails = actionDetails.concat('<div class="iconLink success">' + getSuccessMessage(action.ActionName) + '</div>');
                    } else {
                        actionDetails = actionDetails.concat('<div class="iconLink failed">' + getFailMessage(action.ActionName) + '</div>');
                        if (!action.FailureMessage.isEmpty) {
                            actionDetails = actionDetails.concat('<div style="color: red;padding-left: 25px;">' + action.FailureMessage + (action.ActionName == "<%= ManagementEnums.ActionName.PerformMigration %>" ? "<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_66%>" : "" + '</div>'));
                        }
                    }
                }
                statusText.append(actionDetails);
            } else {
                statusText.append('<div class="iconLink failed">' + options.failMessage + '</div>');

                if (message) {
                    statusText.append('<div style="color: red;padding-left: 25px;">' + message + '</div>');
                }
            }
            statusBox.on("dialogclose", function () { refreshOrRedirect(needRedirectToHost, hostDetailsUrl); });
            closeButton.click(function () { statusBox.dialog('destroy').remove(); refreshOrRedirect(needRedirectToHost, hostDetailsUrl); });
            onFinished(false);
        };

        var refreshOrRedirect = function (needRedirectToHost, hostDetailsUrl) {
            if (needRedirectToHost && hostDetailsUrl) {
                window.location.href = hostDetailsUrl;
            } else {
                window.location.reload(true);
            }
        };

        options.serverPerformActionMethod(options.objectId, options.action, options.parameters, options.restartNedeed, function (task) {
            var taskId = task;
            var gotResult = false;
            var finalResult = false;

            var getResult = setInterval(
                    function() {
                        options.serverGetResultMethod(taskId,
                            function(result) {
                                gotResult = result.GotResult;
                                if (result.Progress) {
                                    progressBarInner.width( result.Progress + '%');
                                }
                                if (gotResult == true) {
                                    finalResult = result.Succeeded;
                                    clearInterval(getResult);
                                    if (finalResult == true) {
                                        successFinish(options.action, options.hostDetailsUrl);
                                    } else {
                                        finishedWithErrors(result.FailureMessage, result.NeedRedirectToHost, options.hostDetailsUrl, result.ActionsData); 
                                    }
                                }
                            },
                            function () { clearInterval(getResult); finishedWithErrors(''); });
                    }, 3000); 
            },
            function(response) {
                finishedWithErrors(response);
            },
            options.vmName
        );
        statusBox.dialog({ width: width, height: "auto", minHeight: 0, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title });
    };


    function showManagementActionsStatusBar(managementObjectId, actionType, hostDetailsUrl, parameters, restartNedeed, vmName) {
        var startStop = typeof(restartNedeed) == "boolean" ? restartNedeed : restartNedeed.toLowerCase() == "true";
        ProgressDialog({
            objectId: managementObjectId,
            action: actionType,
            parameters: parameters,
            title: getTitle(actionType, vmName),
            serverPerformActionMethod: vimPerformManagementAction,
            serverGetResultMethod: vimGetActionManagementResultsDetails,
            sucessMessage: getSuccessMessage(actionType),
            failMessage: getFailMessage(actionType),
            hostDetailsUrl: hostDetailsUrl,
            restartNedeed: startStop,
            vmName: vmName
        });
        return false;
    }

    function getTitle(actionType, vmName) {
        if (actionType == "<%= ManagementEnums.ActionName.PowerOn %>") //>Power on VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_19) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PowerOff %>") //>Power off VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_24) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Resume %>") //>Resume VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_27) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Suspend %>") //>Suspend VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_30) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Pause %>") //>Pause VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_33) %>";
        if (actionType == "<%= ManagementEnums.ActionName.TakeSnapshot %>") //Take Snapshot of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_36) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteSnapshots %>") //Delete Snapshots
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_39) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Reboot %>") //>Reboot VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_42) %>";
        if (actionType == "<%= ManagementEnums.ActionName.ChangeSettings %>") //>Change Settings of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_46) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteVM %>") //>Delete VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_55) %>";
        if (actionType == "<%= ManagementEnums.ActionName.UnregisterVM %>") //>Unregister VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_58) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PerformMigration %>") 
            return SW.Core.String.Format('<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_8) %> ', vmName);
        if (actionType == "<%= ManagementEnums.ActionName.PerformRelocation %>")
            return SW.Core.String.Format('<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_11) %> ', vmName);
        if (actionType == "<%= ManagementEnums.ActionName.DeleteDatastoreFile %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_63) %>";
        return "";
    }
    
    function getSuccessMessage(actionType) {
        if (actionType == "<%= ManagementEnums.ActionName.PowerOn %>") //>Power on VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_20) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PowerOff %>") //>Power off VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_25) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Resume %>") //>Resume VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_28) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Suspend %>") //>Suspend VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_31) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Pause %>") //>Pause VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_34) %>";
        if (actionType == "<%= ManagementEnums.ActionName.TakeSnapshot %>") //>Take Snapshot of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_37) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteSnapshots %>") //Delete Snapshots
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_40) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Reboot %>") //>Reboot VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_43) %>";
        if (actionType == "<%= ManagementEnums.ActionName.ChangeSettings %>") //>Change Settings of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_47) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteVM %>") //>Delete VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_56) %>";
        if (actionType == "<%= ManagementEnums.ActionName.UnregisterVM %>") //>Unregister VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_59) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PerformMigration %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_9) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PerformRelocation %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_12) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteDatastoreFile %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_64) %>";
        return "";
    }

    function getFailMessage(actionType) {
        if (actionType == "<%= ManagementEnums.ActionName.PowerOn %>") //>Power on VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_21) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PowerOff %>") //>Power off VM
           return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_26) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Resume %>") //>Resume VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_29) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Suspend %>") //>Suspend VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_32) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Pause %>") //>Pause VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_35) %>";
        if (actionType == "<%= ManagementEnums.ActionName.TakeSnapshot %>") //>Take Snapshot of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_38) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteSnapshots %>") //Delete Snapshots
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_41) %>";
        if (actionType == "<%= ManagementEnums.ActionName.Reboot %>") //>Reboot VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_44) %>";
        if (actionType == "<%= ManagementEnums.ActionName.ChangeSettings %>") //>Change Settings of VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_48) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteVM %>") //>Delete VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_57) %>";
        if (actionType == "<%= ManagementEnums.ActionName.UnregisterVM %>") //>Unregister VM
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_58) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PerformMigration %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_10) %>";
        if (actionType == "<%= ManagementEnums.ActionName.PerformRelocation %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_VZ0_13) %>";
        if (actionType == "<%= ManagementEnums.ActionName.DeleteDatastoreFile %>")
            return "<%= ControlHelper.EncodeJsString(Resources.VIMWebContent.VIMWEBDATA_ZS0_65) %>";
        return "";
    }

    function removeBox() {

        $(".StatusDialog").dialog('destroy').remove();
    }
    
    function vimPerformManagementAction(objectId, actionType, parameters, restartNedeed, successCallback, failCallback, vmName) {
        SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "PerformManagementAction", { managementObjectId: objectId, actionType: actionType, parameters: parameters, restartNedeed: restartNedeed, vmName: vmName },
            successCallback, failCallback);

    }
    
    function vimGetActionManagementResultsDetails(taskId, successCallback, failCallback) {
        SW.Core.Services.callWebService("/Orion/VIM/Services/ActionsManagementService.asmx", "GetActionResultsDetails", { taskId: taskId, actionType: null },
            successCallback, failCallback);

    }

    //]]>    
</script>

<style>
    .iconLink
    {        
        background-position: left center;
        background-repeat: no-repeat;
        padding: 2px 0 2px 20px;
        margin-left: 5px;
        font-weight: bold;
    }
    
    .success
    {
        background-image: url(/Orion/images/ok_16x16.gif);    
        color:Green;
    }
    
    .failed
    {
        background-image: url(/Orion/images/failed_16x16.gif);    
        color:Red;
    }

    .sw-vim-progress-hide-tooltip {
        border: 1px solid #646464 !important;       
        background: #eeeeee ; 
        font-size: 8pt;
        width: 230px;
    }
    .sw-vim-progress-hide-tooltip h3 {
        font-weight: normal;
        padding: 5px;
    }
    
     .StatusDialog {
        margin-bottom: 7px;
    }
</style>

