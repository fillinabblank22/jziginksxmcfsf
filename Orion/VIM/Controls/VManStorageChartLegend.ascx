﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManStorageChartLegend.ascx.cs" Inherits="Orion_VIM_Controls_VManStorageChartLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.vman_storageChartLegendInitializer__<%= legend.ClientID %> = function (chart) {
        SW.VMan.Charts.StorageChartLegend.createStandardLegend(chart, '<%= legend.ClientID %>');
    };
</script>

<table runat="server" ID="legend" class="DataGrid sw-vim-vman-chart-legend NeedsZebraStripes" style="display: none;">
</table>

<div class="chartLegendLogo" style="float:right;">
    <img src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>