﻿<%@ WebService Language="C#" Class="VMwareServers" %>

using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.VIM.Base.Contract.Models;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class VMwareServers : System.Web.Services.WebService
{
    private static readonly HandleBusinessLayerException ExceptionHandler =
        ex => { throw ex; };

    [WebMethod]
    public void AssignVMwareCredential(int[] nodeIds, Int64 credentialID)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            proxy.Api.AssignVMwareCredential(nodeIds, credentialID);
        }
    }


    [WebMethod]
    public Int64 CreateAndAssignVMwareCredential(int[] nodeIds, string description, string username, string password)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            if (!proxy.Api.CheckVMwareCredentialName(description))
            {
                throw new ArgumentException(string.Format("Credential with the same description already exists: [description={0}]", description));
            }
            Int64 credentialID = proxy.Api.AddVMwareCredential(new Credential() { Description = description, Username = username, Password = password });
            proxy.Api.AssignVMwareCredential(nodeIds, credentialID);

            return credentialID;
        }
    }

    [WebMethod]
    public void UpdateVMwarePollingStatus(int[] nodeIds, bool enable)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            proxy.Api.UpdateVMwarePollingStatus(nodeIds, enable);
        }
    }

    [WebMethod]
    public List<CredentialInfo> GetAllVMwareCredentials()
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            List<CredentialInfo> crList = proxy.Api.GetAllVMwareCredentials("Description", true);
            return crList;
        }
    }

    [WebMethod]
    public PageableDataTable GetVMwareCredentials()
    {
        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("VIM_VMwareCredentials_PageSize"), out pageSize))
                pageSize = 10;
        }

        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            int totalRowCount;
            var ascending = sortDirection.ToLower() != "desc";
            var crList = proxy.Api.GetVMwareCredentials(sortColumn, ascending, startRowNumber, pageSize, out totalRowCount);

            DataTable table = new DataTable();
            table.Columns.Add("CredentialID", typeof(Int64));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Username", typeof(string));

            foreach (var cr in crList)
            {
                table.Rows.Add(cr.Id, cr.Description, cr.Username);
            }

            return new PageableDataTable(table, totalRowCount);
        }
    }

    [WebMethod]
    public PageableDataTable GetVMwareCredentialsCorruptedIncluded()
    {
        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("VIM_VMwareCredentials_PageSize"), out pageSize))
                pageSize = 10;
        }

        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            int totalRowCount;
            var ascending = sortDirection.ToLower() != "desc";
            var crList = proxy.Api.GetVMwareCredentialsCorruptedIncluded(sortColumn, ascending, startRowNumber, pageSize, out totalRowCount);

            DataTable table = new DataTable();
            table.Columns.Add("CredentialID", typeof(Int64));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Username", typeof(string));

            foreach (var cr in crList)
            {
                table.Rows.Add(cr.Id, cr.Description, cr.Username);
            }

            return new PageableDataTable(table, totalRowCount);
        }
    }

    [WebMethod]
    public bool CheckCredentialName(string credentialName)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.CheckVMwareCredentialName(credentialName);
        }
    }

    [WebMethod]
    public void DeleteVMwareCredentials(Int64[] credentialIDs)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            if (!((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement)
            {
                throw new InvalidOperationException("Access denied for current user");
            }
            else
            {
                proxy.Api.DeleteVMwareCredentials(credentialIDs);
            }
        }
    }

    [WebMethod]
    public bool AddVMwareCredential(string description, string username, string password)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            if (!proxy.Api.CheckVMwareCredentialName(description))
            {
                return false;
            }
            proxy.Api.AddVMwareCredential(new Credential()
            {
                Description = description,
                Username = username,
                Password = password
            });
            return true;
        }
    }

    [WebMethod]
    public void UpdateVMwareCredential(Int64 credentialID, string description, string username, string password, bool passwordChanged)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            proxy.Api.UpdateVMwareCredential(new Credential() { ID = credentialID, Description = description, Username = username, Password = password }, passwordChanged);
        }
    }

    [WebMethod]
    public Guid BeginTestCredentials(int[] nodeIds, long credentialId, string username, string password, bool updateStatus)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.ScheduleVMwareCredentialsTestProbeJobs(nodeIds, credentialId, username, password, updateStatus);
        }
    }

    [WebMethod]
    public Object GetTestCredentialResultsSummary(Guid taskId)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            var taskStatus = proxy.Api.GetTaskStatus(taskId);
            switch (taskStatus)
            {
                case BusinessLayerTaskStatus.Running:
                    return new
                    {
                        IsLastResult = false
                    };
                case BusinessLayerTaskStatus.Finished:
                    var results = proxy.Api.GetVMwareCredentialsTestProbeJobsResults(taskId);
                    return new
                    {
                        IsLastResult = true,
                        Succeeded = results.Count > 0 && results.All(pair => pair.Value == HostStatus.Polling)
                    };
                case BusinessLayerTaskStatus.NotFound:
                    return new
                    {
                        IsLastResult = true,
                        Succeeded = false
                    };
                //or exception???
                //throw new ApplicationException(string.Format("Task not found [taskId={0}]", taskId));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    [WebMethod]
    public Object GetTestCredentialResultsDetails(Guid taskId)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            var taskStatus = proxy.Api.GetTaskStatus(taskId);
            switch (taskStatus)
            {
                case BusinessLayerTaskStatus.Running:
                case BusinessLayerTaskStatus.Finished:
                    var results = proxy.Api.GetVMwareCredentialsTestProbeJobsResults(taskId);
                    return new
                    {
                        IsLastResult = taskStatus == BusinessLayerTaskStatus.Finished,
                        Results = results.Select(r => new {Key = r.Key, Value = r.Value}).ToArray()
                    };
                case BusinessLayerTaskStatus.NotFound:
                    return new
                    {
                        IsLastResult = true
                    };
                //or exception???
                //throw new ApplicationException(string.Format("Task not found [taskId={0}]", taskId));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void CheckNodeManagement()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement)
            throw new UnauthorizedAccessException("You must have the Allow Node Management right to perform this operation.");
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetTopLevelNodes()
    {
        int pageSize;
        int startRowNumber;

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);


        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("VIM_VMwareServers_PageSize"), out pageSize))
                pageSize = 10;
        }

        int rows;
        var table = new VMwareServersDAL().GetTopLevelNodes(startRowNumber, pageSize, out rows, true);
        return new PageableDataTable(table, rows);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetVCenterChildNodes(int vCenterID, int parentLevel)
    {
        var table = new VMwareServersDAL().GetVCenterChildNodes(vCenterID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetDatacenterChildNodes(int datacenterID, int parentLevel)
    {
        var table = new VMwareServersDAL().GetDatacenterChildNodes(datacenterID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetClusterChildNodes(int clusterID, int parentLevel)
    {
        var table = new VMwareServersDAL().GetClusterChildNodes(clusterID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public void ChangeTree(string item, bool expand)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(VMwareServersDAL.GridStateID));

        if (expand)
        {
            tm.Expand(item);
        }
        else
        {
            tm.Collapse(item);
        }
    }

    [WebMethod(EnableSession = true)]
    public bool IsExpanded(string item)
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(VMwareServersDAL.GridStateID));
        return tm.IsExpanded(item);
    }

    [WebMethod(EnableSession = true)]
    public void ClearTreeState()
    {
        TreeStateManager tm = new TreeStateManager(this.Session, new Guid(VMwareServersDAL.GridStateID));
        tm.Clear();
    }

    [WebMethod]
    public bool IsVimFullyLicensed()
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.IsVimFullyLicensed();
        }
    }

    [WebMethod]
    public IEnumerable<string> UpdateNodePollingSource(int[] nodeIds, PollingSource pollingSource)
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            var notChangedNodes = nodeIds.Except(proxy.Api.UpdateTopLevelEntityPollingSource(nodeIds, pollingSource, AccountContext.GetAccountID())).ToList();
            foreach (var nodeId in notChangedNodes)
            {
                var hostsDal = new HostDAL();
                var vCenterDal = new VCenterDAL();
                if (hostsDal.IsHostByNodeId(nodeId))
                {
                    var entityModel = hostsDal.GetHostModelByNodeId(nodeId);
                    if (entityModel.PollingSource != pollingSource)
                    {
                        yield return entityModel.Name;
                    }
                }
                else
                {
                    var entityModel = vCenterDal.GetVCenterModelByNodeId(nodeId);
                    if (entityModel.PollingSource != pollingSource)
                    {
                        yield return entityModel.Name;
                    }
                }
            }
        }
    }
}


