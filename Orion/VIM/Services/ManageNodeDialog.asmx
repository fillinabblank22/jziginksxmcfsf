﻿<%@ WebService Language="C#" Class="ManageNodeDialog" %>

using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Common.Extensions;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.NetObjects;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ManageNodeDialog : WebService
{
    [WebMethod]
    public object GetManageNodeDialog(string netObjectType, int netObjectId)
    {
        var prefix = String.Empty;
        var address = String.Empty;
        int? engineId = -1;
        var hostPlatform = VirtualizationPlatform.Unknown;
        var hasMultipleIPAddress = false;

        switch (netObjectType)
        {
            case VimVirtualMachine.Prefix:
                prefix = "G";
                var vmDal = new VirtualMachineDAL();
                var vm = vmDal.GetVMModelByVMId(netObjectId);

                if (vm == null || !vm.PollingSource.IsVanillaPolled())
                {
                    return null;
                }

                address = string.IsNullOrEmpty(vm.IPAddress) ? HttpUtility.UrlEncode(vm.Name) : vm.IPAddress;
                engineId = vmDal.GetParentEngineId(netObjectId);
                hostPlatform = vm.HostPlatform;
                break;

            case VimHost.Prefix:
                prefix = "H";
                var hostDal = new HostDAL();
                var host = hostDal.GetHostModelByHostId(netObjectId);

                if (host == null || !host.PollingSource.IsVanillaPolled())
                {
                    return null;
                }

                address = string.IsNullOrEmpty(host.IPAddress) ? host.Name : host.IPAddress;
                var list = new VMHostDetailsDAL().GetHostIpAddresses(netObjectId);

                if (list != null && list.Count() > 1)
                {
                    address = String.Join(";", list.ToArray());
                    hasMultipleIPAddress = true;
                }

                engineId = hostDal.GetParentEngineId(netObjectId);
                hostPlatform = host.Platform;

                break;
        }

        return new { Result = string.Format("ManageNodeDialog('{0}', '{1}','{2}{3}', {4}, '{5}');", engineId ?? -1,
            address, prefix, netObjectId,
            hasMultipleIPAddress.ToString().ToLower(CultureInfo.InvariantCulture), hostPlatform)};
    }
}
