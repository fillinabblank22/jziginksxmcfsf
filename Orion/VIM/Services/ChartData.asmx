﻿<%@ WebService Language="C#" Class="ChartData" %>

using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;
using ChartDataResults = SolarWinds.Orion.Web.Charting.v2.ChartDataResults;
using V2DataSeries = SolarWinds.Orion.Web.Charting.v2.DataSeries;
using V2DataPoint = SolarWinds.Orion.Web.Charting.v2.DataPoint;
using SolarWinds.VIM.Web.Charting2;
using Charting = SolarWinds.VIM.Web.Charting2;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
//[System.Web.Script.Services.ScriptService]
public class ChartData : ChartDataWebService
{       
    
    private const string _vmNameLookupSwql = "SELECT VirtualMachineID, Name FROM Orion.VIM.VirtualMachines WHERE VirtualMachineID IN @NodeID";
    private const string _clusterNameLookupSwql = "SELECT ClusterID, Name FROM Orion.VIM.Clusters WHERE ClusterID IN @NodeID";    
    private const string _hostNameLookupSwql = "SELECT HostID, HostName FROM Orion.VIM.Hosts WHERE HostID IN @NodeID";    
    private const string _datastoreNameLookupSwql = "SELECT DataStoreID, Name FROM Orion.VIM.Datastores WHERE DataStoreID IN @NodeID";    
    
    [WebMethod]
    public ChartDataResults MultiStatisticData(ChartDataRequest request)
    {
        return ChartDataHelper.MultiStatisticData(request);
    } 

    /// <summary>
    /// Method returns data points for charts AvgMemoryConsumption for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults MemoryConsumption(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgMemoryUsage", "MemoryConsumption");
    }

    /// <summary>
    /// Method returns data points for charts AvgCpuUsed for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults CpuConsumption(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgCPULoad", "CPUConsumption");        
    }

    /// <summary>
    /// Method returns data points for charts AvgMemoryUsageOfHost for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMMemoryConsumptionOfHost(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgMemoryUsageOfHost", "MemoryConsumption");
    }

    /// <summary>
    /// Method returns data points for charts AvgCPULoadOfHost for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMCpuConsumptionOfHost(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgCPULoadOfHost", "CPUConsumption");
    }

    /// <summary>
    /// Method returns data points for charts NetworkTraffic for period specified in request
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults NetworkTraffic(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgNetworkUsageRate*8*1024 AS AvgTraffic", "NetworkTraffic");        
    }

    /// <summary>
    /// Method returns data points for charts AvgCpuReady for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMCpuReady(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgCpuReady", "CPUReady");        
    }

    /// <summary>
    /// Method returns data points for charts VMIOPSTotal for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMIOPSTotal(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgIOPSTotal", "IOPSTotal");                        
    }

    /// <summary>
    /// Method returns data points for charts VMIOPSRead for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMIOPSRead(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgIOPSRead", "IOPSRead");        
    }

    /// <summary>
    /// Method returns data points for charts VMIOPSWrite for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMIOPSWrite(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgIOPSWrite", "IOPSWrite");        
    }

        /// <summary>
    /// Method returns data points for charts VMLatencyTotal for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMLatencyTotal(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgLatencyTotal", "LatencyTotal");        
    }

        /// <summary>
    /// Method returns data points for charts VMLatencyRead for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMLatencyRead(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgLatencyRead", "LatencyRead");        
    }

        /// <summary>
    /// Method returns data points for charts VMLatencyWrite for period specified in request.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults VMLatencyWrite(ChartDataRequest request)
    {
        return SimpleLoadVMData(request, "Stats.AvgLatencyWrite", "LatencyWrite");        
    }


    [WebMethod]
    public ChartDataResults ClustersCPUConsumption(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgCPULoad FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _clusterNameLookupSwql, DateTimeKind.Utc, "AvgCPULoad");
        return result;
    }

    [WebMethod]
    public ChartDataResults ClustersMemoryConsumption(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgMemoryUsage FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _clusterNameLookupSwql, DateTimeKind.Utc, "AvgMemoryUsage");
        return result;
    }

    [WebMethod]
    public ChartDataResults DatastoresIOPSTotal(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgIOPSTotal FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgIOPSTotal");
        return result;
    }
    
    [WebMethod]
    public ChartDataResults DatastoresIOPSRead(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgIOPSRead FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgIOPSRead");
        return result;
    }

    [WebMethod]
    public ChartDataResults DatastoresIOPSWrite(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgIOPSWrite FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgIOPSWrite");
        return result;
    }

    [WebMethod]
    public ChartDataResults DatastoresLatencyTotal(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgLatencyTotal FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgLatencyTotal");
        return result;
    }

    [WebMethod]
    public ChartDataResults DatastoresLatencyRead(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgLatencyRead FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgLatencyRead");
        return result;
    }

    [WebMethod]
    public ChartDataResults DatastoresLatencyWrite(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgLatencyWrite FROM VIM_DatastoreStatistics
                        WHERE DatastoreID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _datastoreNameLookupSwql, DateTimeKind.Utc, "AvgLatencyWrite");
        return result;
    }

    [WebMethod]
    public ChartDataResults HostCPUConsumption(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgCPULoad FROM VIM_HostStatistics
                        WHERE HostID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _hostNameLookupSwql, DateTimeKind.Utc, "AvgCPULoad");
        return result;
    }

    [WebMethod]
    public ChartDataResults HostMemoryConsumption(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgMemUsage FROM VIM_HostStatistics
                        WHERE HostID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _hostNameLookupSwql, DateTimeKind.Utc, "AvgMemUsage");
        return result;
    }

    [WebMethod]
    public ChartDataResults HostNetworkUtilization(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgNetworkUtilization FROM VIM_HostStatistics
                        WHERE HostID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _hostNameLookupSwql, DateTimeKind.Utc, "AvgNetworkUtilization");
        return result;
    }

    private ChartDataResults SimpleLoadVMData(ChartDataRequest request, string property, string seriesTag)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = string.Format(@"SELECT Stats.DateTime, {0}
                          FROM VIM_VMStatistics AS Stats
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime", property);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, _vmNameLookupSwql, DateTimeKind.Utc, seriesTag);
        
        return result;
    } 

}
