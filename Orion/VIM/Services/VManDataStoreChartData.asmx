﻿<%@ WebService Language="C#" Class="VManDataStoreChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Charting2;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.DAL.VMan.Datastore;
using SolarWinds.VIM.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class VManDataStoreChartData : ChartDataWebService
{
    [WebMethod]
    public ChartDataResults VManIOPSForDatastoreAndTopVMs(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new DatastoreChartDataDAL();
        var details = new List<object>();
        int datastoreId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out datastoreId);

        List<DataSeries> dataSeries = new List<DataSeries>();
        ChartDataResults result = new ChartDataResults(dataSeries);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        //DataStore series
        using (DataTable dataTable = chartDal.GetDatastoreIOPSData(datastoreId))
        {
            if (dataTable.Rows.Count == 0)
            {
                return new ChartDataResults(Enumerable.Empty<DataSeries>());
            }
            DataRow row = dataTable.Rows[0];

            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSTotal"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSRead"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new {columns = curDetails.ToArray()});

            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgIOPSTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            dataSeries.AddRange(DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "DatastoreIOPS",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true));

            if (dataSeries.Count == 1)
            {
                dataSeries[0].Label = row["Name"].ToString();
            }
        }

        //TOP VM series
        using (DataTable dataTable = chartDal.GetTopVMIOPSData(datastoreId, GetMaxRecords(request)))
        {
            Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
            foreach (DataRow row in dataTable.Rows)
            {
                string id = row["Id"].ToString();
                if (orderedData.ContainsKey(id))
                    continue;
                orderedData.Add(id, row);
                List<object> curDetails = new List<object>();
                curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["DisplayName"], row["Status"],
                    row["Platform"], true, row));
                curDetails.Add(FormatHelper.FormatIOPS(row["IOPSTotal"], 1));
                curDetails.Add(FormatHelper.FormatIOPS(row["IOPSRead"], 1));
                curDetails.Add(FormatHelper.FormatIOPS(row["IOPSWrite"], 1));
                curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                    row["ClusterStatus"], row["Platform"], true));
                details.Add(new {columns = curDetails.ToArray()});
            }

            request.NetObjectIds = orderedData.Keys.ToArray();
            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgIOPSTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            var curData = DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "VMIOPS",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true);
            foreach (DataSeries curSeries in curData)
            {
                if (orderedData.ContainsKey(curSeries.NetObjectId))
                {
                    curSeries.Label = orderedData[curSeries.NetObjectId]["DisplayName"].ToString();
                }
            }
            dataSeries.AddRange(curData);
        }

        if (result.DataSeries.FirstOrDefault() != null)
        {
            Threshold threshold = new ThresholdDAL().LoadThreshold(VimEntityType.DataStore, "VIM.Datastores.Stats.IOPSTotal", datastoreId);
            if (threshold != null)
            {
                ChartDataHelper.AddThresholdsOptions(result, 1);
                ChartDataHelper.AddThresholdsBands(result.DataSeries.FirstOrDefault(), threshold,
                    result.ChartOptionsOverride, 0);
            }
        }

        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_14,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());

        if (!dataSeries.Any() || !dataSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        return result;
    }

    [WebMethod]
    public ChartDataResults VManLatencyForDatastoreAndTopVMs(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new DatastoreChartDataDAL();
        var details = new List<object>();
        int datastoreId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out datastoreId);

        List<DataSeries> dataSeries = new List<DataSeries>();
        ChartDataResults result = new ChartDataResults(dataSeries);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        //DataStore series
        using (DataTable dataTable = chartDal.GetDatastoreLatencyData(datastoreId))
        {
            if (dataTable.Rows.Count == 0)
            {
                return new ChartDataResults(Enumerable.Empty<DataSeries>());
            }
            DataRow row = dataTable.Rows[0];

            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyTotal"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyRead"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new {columns = curDetails.ToArray()});

            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgLatencyTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            dataSeries.AddRange(DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "DatastoreLatency",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true));

            if (dataSeries.Count == 1)
            {
                dataSeries[0].Label = row["Name"].ToString();
            }
        }

        //TOP VM series
        using (DataTable dataTable = chartDal.GetTopVMLatencyData(datastoreId, GetMaxRecords(request)))
        {
            Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
            foreach (DataRow row in dataTable.Rows)
            {
                string id = row["Id"].ToString();
                if (orderedData.ContainsKey(id))
                    continue;
                orderedData.Add(id, row);
                List<object> curDetails = new List<object>();
                curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["DisplayName"], row["Status"],
                    row["Platform"], true, row));
                curDetails.Add(FormatHelper.FormatLatency(row["LatencyTotal"], 1));
                curDetails.Add(FormatHelper.FormatLatency(row["LatencyRead"], 1));
                curDetails.Add(FormatHelper.FormatLatency(row["LatencyWrite"], 1));
                curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                    row["ClusterStatus"], row["Platform"], true));
                details.Add(new {columns = curDetails.ToArray()});
            }

            request.NetObjectIds = orderedData.Keys.ToArray();
            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgLatencyTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            var curData = DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "VMLatency",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true);
            foreach (DataSeries curSeries in curData)
            {
                if (orderedData.ContainsKey(curSeries.NetObjectId))
                {
                    curSeries.Label = orderedData[curSeries.NetObjectId]["DisplayName"].ToString();
                }
            }
            dataSeries.AddRange(curData);
        }

        if (result.DataSeries.FirstOrDefault() != null)
        {
            Threshold threshold = new ThresholdDAL().LoadThreshold(VimEntityType.DataStore,
                "VIM.Datastores.Stats.LatencyTotal", datastoreId);
            if (threshold != null)
            {
                ChartDataHelper.AddThresholdsOptions(result, 1);
                ChartDataHelper.AddThresholdsBands(result.DataSeries.FirstOrDefault(), threshold,
                    result.ChartOptionsOverride, 0);
            }
        }

        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_15,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());

        if (!dataSeries.Any() || !dataSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        return result;
    }

    [WebMethod]
    public ChartDataResults VManThroughputForDatastoreAndTopVMs(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new DatastoreChartDataDAL();
        var details = new List<object>();
        int datastoreId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out datastoreId);

        List<DataSeries> dataSeries = new List<DataSeries>();
        ChartDataResults result = new ChartDataResults(dataSeries);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        //DataStore series
        using (DataTable dataTable = chartDal.GetDatastoreThroughputData(datastoreId))
        {
            if (dataTable.Rows.Count == 0)
            {
                return new ChartDataResults(Enumerable.Empty<DataSeries>());
            }
            DataRow row = dataTable.Rows[0];

            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputTotal"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputRead"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new {columns = curDetails.ToArray()});

            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgThroughputTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            dataSeries.AddRange(DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "DatastoreThroughput",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true));

            if (dataSeries.Count == 1)
            {
                dataSeries[0].Label = row["Name"].ToString();
            }
        }

        //TOP VM series
        using (DataTable dataTable = chartDal.GetTopVMThroughputData(datastoreId, GetMaxRecords(request)))
        {
            Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
            foreach (DataRow row in dataTable.Rows)
            {
                string id = row["Id"].ToString();
                if (orderedData.ContainsKey(id))
                    continue;
                orderedData.Add(id, row);
                List<object> curDetails = new List<object>();
                curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["DisplayName"], row["Status"],
                    row["Platform"], true, row));
                curDetails.Add(FormatHelper.FormatLatency(row["ThroughputTotal"], 1));
                curDetails.Add(FormatHelper.FormatLatency(row["ThroughputRead"], 1));
                curDetails.Add(FormatHelper.FormatLatency(row["ThroughputWrite"], 1));
                curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                    row["ClusterStatus"], row["Platform"], true));
                details.Add(new {columns = curDetails.ToArray()});
            }

            request.NetObjectIds = orderedData.Keys.ToArray();
            string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgThroughputTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
            var curData = DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                "VMThroughput",
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true);
            foreach (DataSeries curSeries in curData)
            {
                if (orderedData.ContainsKey(curSeries.NetObjectId))
                {
                    curSeries.Label = orderedData[curSeries.NetObjectId]["DisplayName"].ToString();
                }
            }
            dataSeries.AddRange(curData);
        }

        if (result.DataSeries.FirstOrDefault() != null)
        {
            Threshold threshold = new ThresholdDAL().LoadThreshold(VimEntityType.DataStore,
                "VIM.Datastores.Stats.ThroughputTotal", datastoreId); // no such threshold yet, to be added in future
            if (threshold != null)
            {
                ChartDataHelper.AddThresholdsOptions(result, 1);
                ChartDataHelper.AddThresholdsBands(result.DataSeries.FirstOrDefault(), threshold,
                    result.ChartOptionsOverride, 0);
            }
        }

        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIM_Website_ChartLegend_Throughput,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());

        if (!dataSeries.Any() || !dataSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }
        return result;
    }

    private void AddColumnsToChartLegend(ChartDataResults chartData, string[] columns, object[] legendData)
    {
        if (chartData.ChartOptionsOverride != null)
        {
            var chartOptions = chartData.ChartOptionsOverride as dynamic;
            chartOptions.chartColumns = columns;
            chartOptions.seriesInfo = legendData;
        }
        else
        {
            chartData.ChartOptionsOverride = new
                                              {
                                                  chartColumns = columns,
                                                  seriesInfo = legendData
                                              };
        }
    }


    private static int GetMaxRecords(ChartDataRequest request)
    {
        int maxRecords = 10;

        if (request.AllSettings.ContainsKey("MaxRecords"))
        {
            maxRecords = Convert.ToInt32(request.AllSettings["MaxRecords"]);
        }

        return maxRecords;
    }
}