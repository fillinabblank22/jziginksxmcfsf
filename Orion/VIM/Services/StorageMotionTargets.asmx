﻿<%@ WebService Language="C#" Class="StorageMotionTargets" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using FormatHelper = SolarWinds.VIM.Web.Common.Helpers.FormatHelper;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class StorageMotionTargets  : System.Web.Services.WebService {

    [WebMethod]
    public PageableDataTable GetEntities(string groupByCategory, string groupByValue, int vendor)
    {
        CheckPermission();

        int pageSize;
        int startRowNumber;
        string sortDirection = Context.Request.QueryString["dir"];
        string sortProperty = Context.Request.QueryString["sort"];
        
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        string search = Context.Request.QueryString["search"].Trim();
        sortDirection = sortDirection.Equals("DESC", StringComparison.OrdinalIgnoreCase) ? "DESC" : "ASC";

        VirtualizationPlatform vendorType = Enum.IsDefined(typeof (VirtualizationPlatform), vendor)
            ? (VirtualizationPlatform) vendor
            : VirtualizationPlatform.Unknown;

        DataTable table;
        
        if (string.IsNullOrEmpty(groupByCategory) && string.IsNullOrEmpty(groupByValue))
            table = new StorageMotionTargetsDAL().GetEntities(search, startRowNumber, pageSize, sortDirection, sortProperty, vendorType);           
        else
            table = new StorageMotionTargetsDAL().GetEntities(search, startRowNumber, pageSize, sortDirection, sortProperty, vendorType, groupByCategory, groupByValue);             
        
        var totalRows = 0;

        if (table != null)
        {
            table.Columns.Add(new DataColumn("CapacityValue"));
            foreach (DataRow row in table.Rows)
            {
                row["CapacityValue"] = FormatHelper.FormatBytes(row["FreeSpace"], ByteDisplayValueFormat.Short);
            }
            
            if (table.ExtendedProperties["TotalRows"] != null)
                Int32.TryParse(table.ExtendedProperties["TotalRows"].ToString(), out totalRows);
        }        
        
        return new PageableDataTable(table ?? new DataTable(), totalRows);
    }
        


    [WebMethod]
    public PageableDataTable GetEntitiesFacets(string groupByCategory, int vendor)
    {
        CheckPermission();

        VirtualizationPlatform vendorType = Enum.IsDefined(typeof(VirtualizationPlatform), vendor)
            ? (VirtualizationPlatform)vendor
            : VirtualizationPlatform.Unknown;

        var dt = new StorageMotionTargetsDAL().GetEntitiesFacets(groupByCategory, vendorType);
        
        if (dt == null)
            dt = new DataTable();
        
        return new PageableDataTable(dt, dt.Rows.Count);                                
    }

    private void CheckPermission()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAlertManagement)
            throw new UnauthorizedAccessLocalizedException();
    }

}