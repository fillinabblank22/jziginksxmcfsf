﻿<%@ WebService Language="C#" Class="VManStorageChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Web.Charting2;
using SolarWinds.VIM.Web.DAL.VMan.Storage;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Common.Enums;
using FormatHelper = SolarWinds.VIM.Web.Helpers.FormatHelper;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class VManStorageChartData : ChartDataWebService
{
    [WebMethod]
    public ChartDataResults VManTopVMIOPS(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);

        DataTable dataTable = chartDal.GetTopVMIOPSData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["VirtualMachineID"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["Name"], row["Status"],
                row["Platform"], true, row));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSTotal"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSRead"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgIOPSTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "IOPS");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }

        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_14,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());
        return result;
    }

    [WebMethod]
    public ChartDataResults VManVMsCpuUsageGroupedByTopDatastores(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);

        DataTable dataTable = chartDal.GetVMsCpuUsageGroupedByTopDatastoresData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataSeries> resultData = new Dictionary<string, DataSeries>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["DataStoreID"].ToString();
            if (resultData.ContainsKey(id))
                continue;
            resultData.Add(id, new DataSeries {
                TagName = id,
                NetObjectId = id,
                Label = DBHelper.GetDbValueOrDefault(row["Name"], String.Empty)
            });

            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatMhz(row["CpuUsageMHz"], 0));
            details.Add(new { columns = curDetails.ToArray() });
        }

        var statisticData = chartDal.GetVmsCpuUsageGroupedByDatastoresStatisticData(viewType, limitationId, objectId,
            request.SampleSizeInMinutes, DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad), resultData);
        var result = new ChartDataResults(statisticData);

        AddColumnsToChartLegend(result, new[]
        {
            Resources.VIMWebContent.VIMWEBDATA_VB0_125,
            Resources.VIMWebContent.VIM_CpuUtilizationGroupedByDatastores_LegendLabel_CPUUtilization
        }, details.ToArray());
        return result;
    }

    [WebMethod]
    public ChartDataResults VManTopVMLatency(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);
        DataTable dataTable = chartDal.GetTopVMLatencyData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["VirtualMachineID"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["Name"], row["Status"],
                row["Platform"], true, row));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyTotal"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyRead"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgLatencyTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "Latency");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }

        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_15,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13,
            Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());
        return result;
    }


    [WebMethod]
    public ChartDataResults VManTopVMThroughput(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);
        DataTable dataTable = chartDal.GetTopVMThroughputData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["VirtualMachineID"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["Name"], row["Status"],
                row["Platform"], true, row));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputTotal"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputRead"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["ThroughputWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgThroughputTotal
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "Throughput");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }

        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIM_Website_ChartLegend_Throughput,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13,
            Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());
        return result;
    }

    [WebMethod]
    public ChartDataResults VManTopDatastoreIOPS(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);
        DataTable dataTable = chartDal.GetTopDataStoreIOPSData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["Id"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSTotal"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSRead"], 1));
            curDetails.Add(FormatHelper.FormatIOPS(row["IOPSWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgIOPSTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "IOPS");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }

        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_14,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());
        return result;
    }

    [WebMethod]
    public ChartDataResults VManTopDatastoreThroughput(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);
        DataTable dataTable = chartDal.GetTopDataStoreThroughputData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["Id"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>
            {
                EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true),
                FormatHelper.FormatThroughput(row["ThroughputTotal"], 1),
                FormatHelper.FormatThroughput(row["ThroughputRead"], 1),
                FormatHelper.FormatThroughput(row["ThroughputWrite"], 1),
                EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"], row["ClusterStatus"], row["Platform"], true)
            };
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgThroughputTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "Throughput");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }
        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIM_Website_ChartLegend_Throughput,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());
        return result;
    }

    [WebMethod]
    public ChartDataResults VManTopDatastoreIOLatency(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new StorageChartDataDAL();
        var details = new List<object>();
        int objectId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out objectId);
        ViewType viewType = (ViewType)request.AllSettings["VMan-Storage-Chart-ViewType"];
        int? limitationId = GetViewLimitationId(request);
        DataTable dataTable = chartDal.GetTopDataStoreLatencyData(GetMaxRecords(request), viewType, limitationId, objectId);
        Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
        foreach (DataRow row in dataTable.Rows)
        {
            string id = row["Id"].ToString();
            if (orderedData.ContainsKey(id))
                continue;
            orderedData.Add(id, row);
            List<object> curDetails = new List<object>();
            curDetails.Add(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyTotal"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyRead"], 1));
            curDetails.Add(FormatHelper.FormatLatency(row["LatencyWrite"], 1));
            curDetails.Add(EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                row["ClusterStatus"], row["Platform"], true));
            details.Add(new { columns = curDetails.ToArray() });
        }
        request.NetObjectIds = orderedData.Keys.ToArray();
        string strSql = string.Format(@"SELECT Stats.DateTime,  Stats.AvgLatencyTotal
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime");
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "Latency");
        foreach (DataSeries dataSeries in result.DataSeries)
        {
            if (orderedData.ContainsKey(dataSeries.NetObjectId))
            {
                dataSeries.Label = orderedData[dataSeries.NetObjectId]["Name"].ToString();
            }

        }
        AddColumnsToChartLegend(result, new[]
        {
            Resources.CoreWebContent.WEBDATA_AK0_171, Resources.VIMWebContent.VIMWEBCODE_LC0_15,
            Resources.VIMWebContent.VIMWEBCODE_LC0_12,
            Resources.VIMWebContent.VIMWEBCODE_LC0_13, Resources.VIMWebContent.VIMWEBCODE_LC0_16
        }, details.ToArray());

        return result;
    }

    private void AddColumnsToChartLegend(ChartDataResults chartData, string[] columns, object[] legendData)
    {
        if (chartData.ChartOptionsOverride != null)
        {
            var chartOptions = chartData.ChartOptionsOverride as dynamic;
            chartOptions.chartColumns = columns;
            chartOptions.seriesInfo = legendData;
        }
        else
        {
            chartData.ChartOptionsOverride = new
            {
                chartColumns = columns,
                seriesInfo = legendData
            };
        }
    }

    private static int GetMaxRecords(ChartDataRequest request)
    {
        int maxRecords = 10;

        if (request.AllSettings.ContainsKey("MaxRecords"))
        {
            maxRecords = Convert.ToInt32(request.AllSettings["MaxRecords"]);
        }

        return maxRecords;
    }

    private static int? GetViewLimitationId(ChartDataRequest request)
    {
        ViewInfo viewInfo = null;
        if (request.AllSettings["VMan-Storage-Chart-ViewId"] != null)
        {
            int viewId;
            if (int.TryParse(request.AllSettings["VMan-Storage-Chart-ViewId"].ToString(), out viewId))
            {
                viewInfo = ViewManager.GetViewById(viewId);
            }
        }

        int? limitationId = null;
        if (viewInfo != null)
        {
            limitationId = viewInfo.LimitationID;
        }
        return limitationId;
    }
}