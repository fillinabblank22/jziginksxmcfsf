<%@ WebService Language="C#" Class="ClusterChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using log4net.Util;
using SolarWinds.Orion.Core.Common.Models.Thresholds;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Charting2;
using SolarWinds.VIM.Web.DAL;

/// <summary>
/// Class used for providing data for VIM charts
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ClusterChartData  : ChartDataWebService 
{
    /// <summary>
    /// Method returns points for Cluster percent memory used chart.
    /// </summary>
    /// <param name="request">Parameter by which will be retrieved data.</param>
    /// <returns>Data points for chart.</returns>
    [WebMethod]
    public ChartDataResults ClusterPercentMemoryUsed(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgMemoryUsage FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "PercentMemoryUsed");
        if (result.DataSeries.FirstOrDefault() != null)
        {
            int id = 0;
            if (request.NetObjectIds != null && request.NetObjectIds.Length > 0)
                int.TryParse(request.NetObjectIds[0], out id);
            Threshold threshold = new ThresholdDAL().LoadThreshold(VimEntityType.Cluster, "VIM.Clusters.Stats.MemUsage", id);
            if (threshold != null)
            {
                ChartDataHelper.AddThresholdsOptions(result, 1);
                ChartDataHelper.AddThresholdsBands(result.DataSeries.FirstOrDefault(), threshold,
                    result.ChartOptionsOverride, 0);
            }
        }
        return result;
    }

        /// <summary>
    /// Method returns points for Cluster effective memory load chart.
    /// </summary>
    /// <param name="request">Parameter by which will be retrieved data.</param>
    /// <returns>Data points for chart.</returns>
    [WebMethod]
    public ChartDataResults ClusterEffectiveMemoryLoad(ChartDataRequest request)
    {
        string strSql = @"SELECT DateTime, AvgMemoryUsageMB FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "EffectiveMemoryLoad");
        return result;
    }

    /// <summary>
    /// Method returns points for Cluster Effective CPU Load chart.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart.</returns>
    [WebMethod]
    public ChartDataResults ClusterMinMaxAvgCPULoad(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgCPULoad, MinCPULoad, MaxCPULoad FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "AvgCPULoad", "MinMaxCPULoad");
        if (result.DataSeries.FirstOrDefault() != null)
        {
            int id = 0;
            if (request.NetObjectIds != null && request.NetObjectIds.Length > 0)
                int.TryParse(request.NetObjectIds[0], out id);
            Threshold threshold = new ThresholdDAL().LoadThreshold(VimEntityType.Cluster, "VIM.Clusters.Stats.CPULoad", id);
            if (threshold != null)
            {
                ChartDataHelper.AddThresholdsOptions(result, 1);
                ChartDataHelper.AddThresholdsBands(result.DataSeries.FirstOrDefault(), threshold,
                    result.ChartOptionsOverride, 0);
            }
        }
        return result;
    }

    /// <summary>
    /// Method returns points for Cluster CPU Usage MHz chart.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart.</returns>
    [WebMethod]
    public ChartDataResults ClusterMinMaxAvgCPUUsage(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, AvgCPUUsageMHz, MinCPUUsageMHz, MaxCPUUsageMHz FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadMinMaxAvgData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "AvgCPUUsage", "MinMaxCPUUsage");
        return result;
    }

    /// <summary>
    /// Method returns points for Cluster Percent Availability chart.
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart.</returns>
    [WebMethod]
    public ChartDataResults ClusterPercentAvailability(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        string strSql = @"SELECT DateTime, PercentAvailability FROM VIM_ClusterStatistics
                        WHERE ClusterID = @NetObjectId
                        AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime";
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var result = SimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc, "PercentAvailability");
        return result;
    }
}
