﻿<%@ WebService Language="C#" Class="HyperVServers" %>

using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class HyperVServers : System.Web.Services.WebService
{
    [WebMethod]
    public void UpdateHyperVPollingStatus(int[] nodeIds, bool enable)
    {
        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            proxy.Api.UpdateHyperVPollingStatus(nodeIds, enable);
        }
    }

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetServers()
    {
        int pageSize;
        int startRowNumber;

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);


        if (pageSize < 10)
        {
            if (!Int32.TryParse(WebUserSettingsDAL.Get("VIM_HyperVServers_PageSize"), out pageSize))
                pageSize = 10;
        }

        int rows;
        var table = new HyperVServersDAL().GetServers(startRowNumber, pageSize, out rows, true);
        return new PageableDataTable(table, rows);
    }

    [WebMethod]
    public object UpdateNodePollingSource(int[] nodeIds, PollingSource pollingSource)
    {
        var hostsDal = new HostDAL();
        var hyperVServersDal = new HyperVServersDAL();
        var notChangedHostNamesLicenseBased = new List<string>();
        var notChangedHostNamesClusterBased = new List<string>();

        var invalidHosts = hyperVServersDal.GetHostsCausingInvalidClusterPolling(nodeIds, pollingSource).ToArray();
        var filteredIds = nodeIds.Except(invalidHosts).ToArray();

        if (filteredIds != null && filteredIds.Length != 0)
        {
            using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
            {
                var notChangedNodes = filteredIds.Except(proxy.Api.UpdateTopLevelEntityPollingSource(filteredIds, pollingSource, AccountContext.GetAccountID()));
                if (notChangedNodes != null && notChangedNodes.Count() != 0)
                {
                    var notChangedEntityModels = hostsDal.GetHostModelsByNodeIds(notChangedNodes.ToArray());
                    foreach (var model in notChangedEntityModels)
                    {
                        if (model.PollingSource != pollingSource)
                        {
                            notChangedHostNamesLicenseBased.Add(model.Name);
                        }
                    }
                }
            }
        }

        if (invalidHosts != null && invalidHosts.Length != 0) {
            var notChangedEntityModels = hostsDal.GetHostModelsByNodeIds(invalidHosts.ToArray());

            foreach (var model in notChangedEntityModels)
            {
                notChangedHostNamesClusterBased.Add(model.Name);
            }
        }

        return new
        {
            licenseBased = notChangedHostNamesLicenseBased,
            clusterBased = notChangedHostNamesClusterBased
        };
    }

    [WebMethod]
    public bool IsVimFullyLicensed()
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.IsVimFullyLicensed();
        }
    }
}