﻿<%@ WebService Language="C#" Class="AlertManagementSelectSpecificVM" %>

using System;
using System.Data;
using System.Web;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AlertManagementSelectSpecificVM  : System.Web.Services.WebService {

    [WebMethod]
    public PageableDataTable GetEntities(string groupByCategory, string groupByValue, int vendor)
    {
        CheckPermission();

        VimEntityType vimEntityType;                
        
        int pageSize;
        int startRowNumber;
        string sortDirection = Context.Request.QueryString["dir"];
        string sortProperty = Context.Request.QueryString["sort"];
        
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        string search = Context.Request.QueryString["search"].Trim();
        sortDirection = sortDirection.Equals("DESC", StringComparison.OrdinalIgnoreCase) ? "DESC" : "ASC";

        VirtualizationPlatform vendorType = Enum.IsDefined(typeof(VirtualizationPlatform), vendor)
           ? (VirtualizationPlatform)vendor
           : VirtualizationPlatform.Unknown;

        DataTable table;
        
        if (string.IsNullOrEmpty(groupByCategory) && string.IsNullOrEmpty(groupByValue))
            table = new AlertManagementSelectSpecificVmDAL().GetEntities(search, startRowNumber, pageSize, sortDirection, sortProperty, vendorType);           
        else
            table = new AlertManagementSelectSpecificVmDAL().GetEntities(search, startRowNumber, pageSize, sortDirection, sortProperty, vendorType, groupByCategory, groupByValue);             
        
        var totalRows = 0;

        if (table != null)
        {
            if (table.ExtendedProperties["TotalRows"] != null)
                Int32.TryParse(table.ExtendedProperties["TotalRows"].ToString(), out totalRows);
        }        
        
        return new PageableDataTable(table ?? new DataTable(), totalRows);
    }
        


    [WebMethod]
    public PageableDataTable GetEntitiesFacets(string groupByCategory, int vendor)
    {
        CheckPermission();

        VirtualizationPlatform vendorType = Enum.IsDefined(typeof(VirtualizationPlatform), vendor)
         ? (VirtualizationPlatform)vendor
         : VirtualizationPlatform.Unknown;

        var dt = new AlertManagementSelectSpecificVmDAL().GetEntitiesFacets(groupByCategory, vendorType);
        
        if (dt == null)
            dt = new DataTable();
        
        return new PageableDataTable(dt, dt.Rows.Count);                                
    }

    private void CheckPermission()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAlertManagement)
            throw new UnauthorizedAccessLocalizedException();
    }

}