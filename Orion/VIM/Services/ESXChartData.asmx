<%@ WebService Language="C#" Class="ESXChartData" %>

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using log4net.Util;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

/// <summary>
/// Class used for providing data for VIM charts
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ESXChartData  : ChartDataWebService 
{
    /// <summary>
    /// Method returns data points for charts Overall Number of Runnins vs Total VMS
    /// </summary>
    /// <param name="request">Parameters by which will be retrieved data.</param>
    /// <returns>Data points for chart</returns>
    [WebMethod]
    public ChartDataResults RunningVMCount(ChartDataRequest request)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);
        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        string strSql = @"SELECT MIN([T2].[DateTime]) AS [DateTime], SUM([T2].[Running]) AS [Running] 
                        FROM 
                        ( SELECT MIN([T].[DateTime]) AS [DateTime], [T].[IntervalNumber], AVG([T].[Running]) AS [Running], AVG([T].[Stopped]) AS [Stopped], [HostID] 
                        FROM ( SELECT H.HostID, DATEADD(minute,(DATEDIFF(minute, @StartTime, S.[DateTime]) / {SampleSize}) * {SampleSize}, @StartTime) AS [DateTime], 
                        S.[VmRunningCount] AS [Running], S.[VmCount] - S.[VmRunningCount] AS [Stopped], DATEDIFF(minute, @StartTime, [DateTime]) / {SampleSize} AS [IntervalNumber] 
                        FROM 
                        VIM_HostStatistics S 
                        INNER JOIN VIM_Hosts H ON H.HostID = S.HostID 
                        LEFT JOIN VIM_Clusters C ON H.ClusterID = C.ClusterID 
                        LEFT JOIN VIM_DataCenters DC ON C.DataCenterID = DC.DataCenterID 
                        LEFT JOIN VIM_DataCenters DC2 ON H.DataCenterID = DC2.DataCenterID 
                        WHERE (DC.DataCenterID = @NetObjectId OR DC2.DataCenterID = @NetObjectId)
                        AND [S].[DateTime] >= @StartTime AND [S].[DateTime] <= @EndTime
                         ) AS [T] 
                        GROUP BY [T].[IntervalNumber], [HostID] ) AS [T2] GROUP BY [T2].[IntervalNumber]  ORDER BY [DateTime] ".Replace("{SampleSize}", dynamicSampleSize.ToString());
        
        List<string> strSqls = new List<string>();
        strSqls.Add(strSql);
        
        strSql = @"SELECT MIN([T2].[DateTime]) AS [DateTime], SUM([T2].[Stopped]) AS [Stopped] 
                    FROM 
                    ( SELECT MIN([T].[DateTime]) AS [DateTime], [T].[IntervalNumber], AVG([T].[Running]) AS [Running], AVG([T].[Stopped]) AS [Stopped], [HostID] 
                    FROM ( SELECT H.HostID, DATEADD(minute,(DATEDIFF(minute, @StartTime, S.[DateTime]) / {SampleSize}) * {SampleSize}, @StartTime) AS [DateTime], 
                    S.[VmRunningCount] AS [Running], S.[VmCount] - S.[VmRunningCount] AS [Stopped], DATEDIFF(minute, @StartTime, [DateTime]) / {SampleSize} AS [IntervalNumber] 
                    FROM 
                    VIM_HostStatistics S 
                    INNER JOIN VIM_Hosts H ON H.HostID = S.HostID 
                    LEFT JOIN VIM_Clusters C ON H.ClusterID = C.ClusterID 
                    LEFT JOIN VIM_DataCenters DC ON C.DataCenterID = DC.DataCenterID 
                    LEFT JOIN VIM_DataCenters DC2 ON H.DataCenterID = DC2.DataCenterID 
                    WHERE (DC.DataCenterID = @NetObjectId OR DC2.DataCenterID = @NetObjectId)
                    AND [S].[DateTime] >= @StartTime AND [S].[DateTime] <= @EndTime
                     ) AS [T] 
                    GROUP BY [T].[IntervalNumber], [HostID] ) AS [T2] GROUP BY [T2].[IntervalNumber]  ORDER BY [DateTime] ".Replace("{SampleSize}", dynamicSampleSize.ToString());
        strSqls.Add(strSql);
        
        List<string> labelNames = new List<string>();
        labelNames.Add(Resources.VIMWebContent.VIMWEBCODE_PS0_4);
        labelNames.Add(Resources.VIMWebContent.VIMWEBCODE_PS0_5);
  
        var result = SimpleLoadDataVIM(request, dateRange, strSqls, labelNames, DateTimeKind.Utc, "RunningVMCount");

        return result;
    }


    private ChartDataResults SimpleLoadDataVIM(
             ChartDataRequest request, DateRange dateRange, IEnumerable<string> dataSqls, IEnumerable<string> labelNames,
             DateTimeKind dateFormatInDatabase, string mainSeriesTag,
             string secondarySeriesTag = null,
             Func<IEnumerable<DataPoint>, double> aggregationMethod = null,
             bool calculateSubSeries = true)
    {
        if (aggregationMethod == null)
            aggregationMethod = SampleMethod.Average;

        var allSeries = DoSimpleLoadDataVIM(request, dateRange, dataSqls, labelNames, dateFormatInDatabase, mainSeriesTag,
                                         secondarySeriesTag, aggregationMethod, calculateSubSeries);
        var result = new ChartDataResults(allSeries);
        result.AppliedLimitation = request.AppliedLimitation;
        //return new DynamicLoader(request, new ChartWidthSampleSizeCalculator()).SetDynamicChartOptions(result);
        return result;
    }
    
    private IEnumerable<DataSeries> DoSimpleLoadDataVIM(
            ChartDataRequest request, DateRange dateRange, IEnumerable<string> dataSqls, IEnumerable<string> labelNames,
            DateTimeKind dateFormatInDatabase, string mainSeriesTag,
            string secondarySeriesTag,
            Func<IEnumerable<DataPoint>, double> aggregationMethod,
            bool calculateSubSeries)
    {
    
        dateRange = this.SetCustomDateRangeBasedOnRequest(request, dateRange, dateFormatInDatabase);
        
        var netObjectId = request.NetObjectIds[0]; 
        var allSeries = new List<DataSeries>();
        var dynamicLoader = new DynamicLoader(request, new ChartWidthSampleSizeCalculator());
        int dynamicSampleSize = dynamicLoader.CalculateDynamicSampleSize(dateRange);
        
        for (int i = 0; i < dataSqls.Count(); i++)
        {
            string dataSql = dataSqls.ElementAt(i);
            var limitedSql = Limitation.LimitSQL(dataSql);
                                   
            string label = null;
           
            if (labelNames.Count() >= i)
            {
                label = labelNames.ElementAt(i);
            }

            var parameters = new[]
                {
                    new SqlParameter("NetObjectId", netObjectId),
                    new SqlParameter("StartTime", dateRange.StartDate),
                    new SqlParameter("EndTime", dateRange.EndDate)
                };

            List<DataSeries> rawSeries;

            if (String.IsNullOrEmpty(secondarySeriesTag))
                rawSeries = GetData(limitedSql, parameters, dateFormatInDatabase, netObjectId, mainSeriesTag);
            else
                rawSeries = GetData(limitedSql, parameters, dateFormatInDatabase, netObjectId, mainSeriesTag, secondarySeriesTag);          
            
            var rawMainSeries = rawSeries[0];
            rawMainSeries.Label = label;

            // From rawMainSeries we will create sampled series point which will be displayed on charts
            var sampledMainSeries = rawMainSeries.CreateSampledSeries(dateRange, dynamicSampleSize, aggregationMethod);

            allSeries.Add(sampledMainSeries);

            if (!String.IsNullOrEmpty(secondarySeriesTag))
            {
                var rawSecondarySeries = rawSeries[1];
                rawSecondarySeries.Label = label;

                var sampledSecondarySeries = rawSecondarySeries.CreateSampledSeries(dateRange, dynamicSampleSize, aggregationMethod);

                allSeries.Add(sampledSecondarySeries);
            }

            if (calculateSubSeries && request.Calculate95thPercentile && rawMainSeries.HasData)
                allSeries.Add(rawMainSeries.CreatePercentileSeries(sampledMainSeries.Dates));

            if (calculateSubSeries && request.CalculateTrendLine && rawMainSeries.HasData)
                allSeries.Add(rawMainSeries.CreateTrendSeries(dateRange, dynamicSampleSize));

            if (request.CalculateSum)
                allSeries.Add(DataSeries.CreateSumSeries(allSeries.Where(x => x.TagName == mainSeriesTag)));
        }

        return allSeries;
    }

 
}
