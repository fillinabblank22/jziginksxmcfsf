﻿<%@ WebService Language="C#"  Class="ActionsManagementService" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Batching.Contract.Enums;
using SolarWinds.Batching.Contract.Models;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Common.ManagementActions;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.DAL.VMan.Sprawl;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Models;

/// <summary>
/// Summary description for ActionsManagementService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ActionsManagementService : System.Web.Services.WebService
{
    public ActionsManagementService () {

        //Uncomment the following line if using designed components
        //InitializeComponent(); 
    }

    private static void HandleBLException(Exception ex)
    {
        var exception = ex as System.ServiceModel.FaultException<VirtualizationFaultContract>;

        if (exception != null && exception.Detail != null && exception.Detail.FaultType == VirtualizationFaultType.License)
        {
            throw new HttpException(exception.Message);
        }

        throw ex;
    }

    [WebMethod]
    public Guid PerformManagementAction(int managementObjectId, string actionType, object[] parameters, string restartNedeed, string vmName)//Guid
    {
        //for demo mode we don't want to perform job for getSnapshots, we just want to fake data.
        if (OrionConfiguration.IsDemoServer && ManagementEnums.ActionName.GetSnapshots.ToString().Equals(actionType))
        {
            return Guid.Empty;
        }

        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {

            var type = (ManagementEnums.ActionName) Enum.Parse(typeof (ManagementEnums.ActionName), actionType);
            CheckPermissions(type);
            var userId = AccountContext.GetAccountID();
            var action = ManagementActionFactory.CreateAction(type, managementObjectId, parameters, vmName);
            var actions = new List<ManagementAction>();
            bool restartNeededBool = bool.Parse(restartNedeed);
            if (restartNeededBool)
            {
                CheckPermissions(ManagementEnums.ActionName.PowerOff);
                actions.Add(ManagementActionFactory.CreatePowerOffAction(managementObjectId, vmName));
            }
            actions.Add(action);
            if (restartNeededBool)
            {
                actions.Add(ManagementActionFactory.CreatePowerOnAction(managementObjectId, vmName));
            }

            ValidationSummary validationResult = proxy.Api.ValidateActions(actions, userId);
            if (validationResult.Status == ValidationResultStatus.Error)
            {
                List<string> messages = new List<string>();
                foreach (var result in validationResult.ActionAggregateResults)
                {
                    messages.AddRange(result.ActionResults.Select(r => r.Message));
                }
                throw new HttpException(string.Join("<br />", messages.Distinct()));
            }

            return proxy.Api.CreateBatch(new BatchConfig() { Actions = actions, UserId = userId, StopOnFailure = false });
        }
    }

    [WebMethod]
    public HostModel[] GetMigrateDestinations(int vmId, VirtualizationPlatform platform)
    {
        if (!VimProfile.AllowMigrationManagement)
            throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
        return new HostDAL().GetStorageRelatedHosts(vmId, platform).ToArray();
    }

    [WebMethod]
    public HostModel GetHost(int hostId)
    {
        return new HostDAL().GetHostModelByHostId(hostId);
    }

    [WebMethod]
    public RelocationDatastoreModel[] GetRelocateDestinations(int vmId)
    {
        if (!VimProfile.AllowMigrationManagement)
            throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);

        var datastores = new List<RelocationDatastoreModel>();
        var dal = new DatastoreDAL();
        var destinations = dal.GetAccessibleDataStoresForVirtualMachine(vmId);
        var currentDatastores = dal.GetDatastoresForVirtualMachine(vmId);
        foreach (var destination in destinations)
        {
            var datastore = new RelocationDatastoreModel()
            {
                DataStoreModel = destination,
                IsSelected = currentDatastores.Any(cd => cd.ID == destination.ID)
            };
            datastores.Add(datastore);
        }
        return datastores.ToArray();
    }

    [WebMethod]
    public object GetActionResultsDetails(Guid taskId, string actionType)
    {
        // in demo mode we'll fake action data
        if (OrionConfiguration.IsDemoServer && taskId == Guid.Empty)
        {
            return PrepareDataForDemo(actionType);
        }

        using (var proxy =  BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            BatchResult result = proxy.Api.GetManagementActionsJobResults(taskId);
            switch (result.Status)
            {
                case BatchStatus.Pending:
                case BatchStatus.Running:
                    {
                        return new
                        {
                            GotResult = false,
                            Progress = result.OverallProgress
                        };
                    }
                case BatchStatus.FinishedSuccessfully:
                case BatchStatus.FinishedWithError:
                    {
                        return new
                        {
                            GotResult = true,
                            ActionsData = result.ActionsResults.Values
                                .Select(a => new
                                {
                                    Succeeded = a.Status == ActionStatus.FinishedSuccessfully,
                                    ActionName = a.ActionType,
                                    FailureMessage = a.FailureMessage
                                }),
                            Succeeded = result.Status == BatchStatus.FinishedSuccessfully,
                            Progress = 100,
                            Data = GetDataFromActionResults(result.ActionsResults, actionType)
                        };
                    }
                case BatchStatus.NotFound:
                    throw new ApplicationException(string.Format("Batch not found [taskId={0}]", taskId));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    /// <summary>
    /// Get data from action results
    /// </summary>
    private object GetDataFromActionResults(IDictionary<Guid, ActionResult> actionResults, string actionType)
    {
        return actionResults.Values.FirstOrDefault(ar => ar.ActionType == actionType)?.Data;
    }

    /// <summary>
    /// Get demo data object for specific action type
    /// </summary>
    private object PrepareDataForDemo(string actionType)
    {
        // in demo mode, we'll fake snapshot data
        if (actionType == ManagementEnums.ActionName.GetSnapshots.ToString())
        {
            return new
            {
                GotResult = true,
                Succeeded = true,
                FailureMessage = String.Empty,
                Progress = 100,
                NeedRedirectToHost = false,
                Data = new[]
                {
                    new SnapshotEntity()
                    {
                        CreationTime = new DateTime(2015, 4, 28, 10, 12, 10, 0),
                        IsCurrentState = false,
                        IsLatest = false,
                        Name = "Snapshot 1 - (28.04.2015 - 10:12:10)",
                        ParentSnapshot = null,
                        Path = "snaphot1",
                        Children = new[]
                        {
                            new SnapshotEntity()
                            {
                                CreationTime = new DateTime(2015, 5, 12, 11, 2, 31, 0),
                                IsCurrentState = false,
                                IsLatest = true,
                                Name = "Snapshot 1 Child 1 (12.05.2015 - 11:02:31)",
                                ParentSnapshot = "snaphot1",
                                Path = "snaphot1_child1",
                                Children = new[]
                                {
                                    new SnapshotEntity()
                                    {
                                        CreationTime = DateTime.Now,
                                        IsCurrentState = true,
                                        IsLatest = false,
                                        Name = "Snapshot",
                                        ParentSnapshot = "snaphot1_child1",
                                        Path = "snaphot1_child1_child1",
                                        Children = null,
                                    }
                                }
                            },
                            new SnapshotEntity()
                            {
                                CreationTime = new DateTime(2015, 9, 5, 12, 58, 12, 0),
                                IsCurrentState = false,
                                IsLatest = true,
                                Name = "Snapshot 1 Child 2 (05.09.2015 - 12:58:12)",
                                ParentSnapshot = "snaphot1",
                                Path = "snaphot1_child2",
                                Children = null,
                            }
                        }
                    },
                    new SnapshotEntity()
                    {
                        CreationTime = new DateTime(2015, 4, 29, 11, 5, 42, 0),
                        IsCurrentState = false,
                        IsLatest = false,
                        Name = "Snapshot 2 - (29.04.2015 - 11:05:42)",
                        ParentSnapshot = null,
                        Path = "snaphot2",
                        Children = null,
                    },
                    new SnapshotEntity()
                    {
                        CreationTime = new DateTime(2015, 5, 12, 9, 15, 23, 0),
                        IsCurrentState = false,
                        IsLatest = false,
                        Name = "Snapshot 3 - (12.05.2015 - 09:15:23)",
                        ParentSnapshot = null,
                        Path = "snaphot3",
                        Children = new[]
                        {
                            new SnapshotEntity()
                            {
                                CreationTime = new DateTime(2015, 5, 13, 2, 32, 5, 0),
                                IsCurrentState = false,
                                IsLatest = false,
                                Name = "Snapshot 3 - Child 1 (13.05.2015 - 02:32:05)",
                                ParentSnapshot = "snaphot3",
                                Path = "snaphot3_child1",
                                Children = null,
                            }
                        }
                    }
                }
            };
        }
        else
        {
            // not supported action type
            return null;
        }
    }

    [WebMethod]
    public Object GetLimitCurrentAndRecommendedCPUandRAMsize(int virtualMachineId)
    {
        var cpuAndMemoryDal = new SprawlManagementActionDAL();
        DataTable table = cpuAndMemoryDal.GetCpuAndMemoryDetails(virtualMachineId);
        DataRow sprawlData = null;
        if (table.Rows.Count > 0)
            sprawlData = table.Rows[0];

        if (sprawlData == null)
            return new {Succeeded = false};
        
        Int64 currentMemory = DBHelper.GetDbValueOrDefault <Int64>(sprawlData["MemoryConfigured"], 0);
        Int64? minimumMemory = DBHelper.GetDbValueOrDefault<Int64?>(sprawlData["MinimumMemory"], null);
        Int64 hostTotalRAM = DBHelper.GetDbValueOrDefault<Int64>(sprawlData["HostTotalMemory"], 0);
        double consumedMemLoad = DBHelper.GetDbValueOrDefault(sprawlData["ConsumedMemoryLoad"], 0.0);
        // We want to check if the host has enabled dynamic memory because
        // if not we are unable to find out its utilization this fixes issue VIM-3506
        bool dynamicMemoryEnabled = DBHelper.GetDbValueOrDefault <bool>(sprawlData["DynamicMemoryEnabled"], false);
        double consumedPercentMemoryLoad = dynamicMemoryEnabled ? DBHelper.GetDbValueOrDefault(sprawlData["ConsumedPercentMemoryLoad"], -1.0) : -1.0;
        double memoryUsage = DBHelper.GetDbValueOrDefault(sprawlData["MemoryLoad"], 0.0);

        int currentCPUcount = DBHelper.GetDbValueOrDefault(sprawlData["ProcessorCount"], 0);
        int hostTotalCPUcount = DBHelper.GetDbValueOrDefault(sprawlData["HostCoreCount"], 0);
        var cpuPeakUtilization = DBHelper.GetDbValueOrDefault(sprawlData["CpuLoadSumpeak"], -1.0f);
        var cpuLoad = DBHelper.GetDbValueOrDefault(sprawlData["CpuLoad"], 0.0);

        var cpuCountRecommendation = SprawlRecommendationsHelper.GetRecommendedCpuCount(cpuPeakUtilization, currentCPUcount, cpuLoad);
        var memoryRecommendation = SprawlRecommendationsHelper.GetRecommendedMemory(consumedMemLoad, currentMemory, consumedPercentMemoryLoad, memoryUsage);

        return new
        {
            Succeeded = true,
            RAM = currentMemory / 1024 / 1024,
            CPU = currentCPUcount,
            CpuRecommendation = cpuCountRecommendation,
            MemoryRecommendation = memoryRecommendation,
            CPULimit = hostTotalCPUcount,
            MaximumMemory = hostTotalRAM / 1024 / 1024,
            MinimumMemory = minimumMemory / 1024 / 1024
        };
    }

    private void CheckPermissions(ManagementEnums.ActionName actionType)
    {
        switch (actionType)
        {
            case ManagementEnums.ActionName.PowerOn:
            case ManagementEnums.ActionName.PowerOff:
            case ManagementEnums.ActionName.Suspend:
            case ManagementEnums.ActionName.Pause:
            case ManagementEnums.ActionName.Reboot:
            case ManagementEnums.ActionName.Resume:
                if (!VimProfile.AllowPowerManagement)
                    throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
                break;
            case ManagementEnums.ActionName.TakeSnapshot:
            case ManagementEnums.ActionName.DeleteSnapshots:
                if (!VimProfile.AllowSnapshotManagement)
                    throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
                break;
            case ManagementEnums.ActionName.ChangeSettings:
                if (!VimProfile.AllowResourcesSettingsManagement)
                    throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
                break;
            case ManagementEnums.ActionName.DeleteVM:
            case ManagementEnums.ActionName.UnregisterVM:
            case ManagementEnums.ActionName.DeleteDatastoreFile:
                if (!VimProfile.AllowDeleteVMAction)
                    throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
                break;
            case ManagementEnums.ActionName.PerformMigration:
            case ManagementEnums.ActionName.PerformRelocation:
                if (!VimProfile.AllowMigrationManagement)
                    throw new HttpException(Resources.VIMWebContent.VIMWEBDATA_VZ0_1);
                break;
        }
    }
}
