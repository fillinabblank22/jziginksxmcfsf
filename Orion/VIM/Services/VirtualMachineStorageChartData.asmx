﻿<%@ WebService Language="C#" Class="VirtualMachineStorageChartData" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.VIM.Web.DAL.VMan.Datastore;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class VirtualMachineStorageChartData : ChartDataWebService
{
    [WebMethod]
    public ChartDataResults VManIOPSForVirtualMachine(ChartDataRequest request)
    {
        return GetStatistics(request, "AvgIOPSTotal", "VMIOPS");
    }

    [WebMethod]
    public ChartDataResults VManLatencyForVirtualMachine(ChartDataRequest request)
    {
        return GetStatistics(request, "AvgLatencyTotal", "VMLatency");
    }

    [WebMethod]
    public ChartDataResults VManThroughputForVirtualMachine(ChartDataRequest request)
    {
        return GetStatistics(request, "AvgThroughputTotal", "VMThroughput");
    }

    private ChartDataResults GetStatistics(ChartDataRequest request, string metricColumnName, string metricIdentifier)
    {
        ApplyLimitOnNumberOfNetObjects(request);
        var chartDal = new VirtualMachineChartDataDAL();
        int virtualMachineId;
        Int32.TryParse(request.NetObjectIds.FirstOrDefault(), out virtualMachineId);

        List<DataSeries> dataSeries = new List<DataSeries>();
        ChartDataResults result = new ChartDataResults(dataSeries);
        var dateRange = DateRange.FromDaysAgoUtc(request.DaysOfDataToLoad);

        //VM series
        using (DataTable dataTable = chartDal.GetVMData(virtualMachineId))
        {
            if (dataTable.Rows.Count == 0)
            {
                return new ChartDataResults(Enumerable.Empty<DataSeries>());
            }

            DataRow row = dataTable.Rows[0];

            string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0}
                          FROM VIM_VMStatistics AS Stats 
                          WHERE Stats.VirtualMachineID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime", metricColumnName);
            dataSeries.AddRange(DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                metricIdentifier,
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true));

            if (dataSeries.Count == 1)
            {
                dataSeries[0].Label = row["DisplayName"].ToString();
            }
        }

        //DataStore series
        using (DataTable dataTable = chartDal.GetDataStoreLatencyData(virtualMachineId))
        {
            Dictionary<string, DataRow> orderedData = new Dictionary<string, DataRow>();
            foreach (DataRow row in dataTable.Rows)
            {
                string id = row["Id"].ToString();
                if (orderedData.ContainsKey(id))
                    continue;
                orderedData.Add(id, row);
            }
            request.NetObjectIds = orderedData.Keys.ToArray();

            string strSql = string.Format(@"SELECT Stats.DateTime, Stats.{0}
                          FROM VIM_DatastoreStatistics AS Stats 
                          WHERE Stats.DatastoreID = @NetObjectId
                          AND DateTime >= @StartTime AND DateTime <= @EndTime ORDER BY DateTime", metricColumnName);
            var curData = DoSimpleLoadData(request, dateRange, strSql, string.Empty, DateTimeKind.Utc,
                metricIdentifier,
                null, new Func<IEnumerable<DataPoint>, double>(SampleMethod.Average), true);

            foreach (DataSeries curSeries in curData)
            {
                if (orderedData.ContainsKey(curSeries.NetObjectId))
                {
                    curSeries.Label = orderedData[curSeries.NetObjectId]["Name"].ToString();
                }
            }
            dataSeries.AddRange(curData);
        }

        if (!dataSeries.Any() || !dataSeries.Any(s => s.Data.Any(dp => !dp.IsNullPoint)))
        {
            return new ChartDataResults(Enumerable.Empty<DataSeries>());
        }

        return result;
    }
}