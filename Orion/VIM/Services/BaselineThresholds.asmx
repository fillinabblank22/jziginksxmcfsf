﻿<%@ WebService Language="C#" Class="BaselineThresholds" %>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class BaselineThresholds  : System.Web.Services.WebService {

    [WebMethod]
    public PageableDataTable GetEntities(string entityType, string groupByCategory, string groupByValue)
    {
        CheckPermission();

        VimEntityType vimEntityType;                
        
        int pageSize;
        int startRowNumber;
        string search;
        string sortDirection = Context.Request.QueryString["dir"];
        string sortProperty = Context.Request.QueryString["sort"];
        
        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        search = Context.Request.QueryString["search"].Trim();
        sortDirection = sortDirection.Equals("DESC", StringComparison.OrdinalIgnoreCase) ? "DESC" : "ASC";

        DataTable table;
        
        if (string.IsNullOrEmpty(groupByCategory) && string.IsNullOrEmpty(groupByValue))            
            table = new BaselineThresholdsDAL().GetEntities(entityType, search, startRowNumber, pageSize, sortDirection, sortProperty);        
        else
            table = new BaselineThresholdsDAL().GetEntities(entityType, startRowNumber, pageSize, sortDirection, sortProperty, groupByCategory, groupByValue);        
        
        var totalRows = 0;

        if (table != null)
        {
            if (table.ExtendedProperties["TotalRows"] != null)
                Int32.TryParse(table.ExtendedProperties["TotalRows"].ToString(), out totalRows);
        }        
        
        return new PageableDataTable(table ?? new DataTable(), totalRows);
    }
        
    [WebMethod]
    public PageableDataTable GetVCenterChildNodes(int vCenterID, int parentLevel) 
    {
        var table = new BaselineThresholdsDAL().GetVCenterChildNodes(vCenterID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);    
    }

    [WebMethod]
    public PageableDataTable GetDataCenterChildNodes(int dataCenterID, int parentLevel)
    {
        var table = new BaselineThresholdsDAL().GetDataCenterChildNodes(dataCenterID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);
    }

    [WebMethod]
    public PageableDataTable GetClusterChildNodes(int clusterID, int parentLevel)
    {
        var table = new BaselineThresholdsDAL().GetClusterChildNodes(clusterID, parentLevel);        
        return new PageableDataTable(table, table.Rows.Count);        
    }

    [WebMethod]
    public PageableDataTable GetHostChildNodes(int hostID, int parentLevel)
    {
        var table = new BaselineThresholdsDAL().GetHostChildNodes(hostID, parentLevel);
        return new PageableDataTable(table, table.Rows.Count);

    }

    [WebMethod(EnableSession = true)]
    public void SetEntityIdsToEdit(IEnumerable<string> entityIds, string entityType)
    {
        CheckPermission();

        EntityWorkflowHelper.EntityMultiEditIDs = new List<string>(entityIds);
        EntityWorkflowHelper.EntityType = entityType;
    }

    [WebMethod]
    public PageableDataTable GetEntitiesFacets(string groupByCategory, string entityType)
    {
        CheckPermission();

        var dt = new BaselineThresholdsDAL().GetEntitiesFacets(groupByCategory, entityType);
        
        if (dt == null)
            dt = new DataTable();
        
        return new PageableDataTable(dt, dt.Rows.Count);                                
    }

    [WebMethod]
    public int? GetVMParentEngineID(int vmID)
    {
        CheckPermission();

        return new VirtualMachineDAL().GetParentEngineId(vmID);
    }

    [WebMethod]
    public DataTable GetHostManageNodeProperties(int hostID)
    {
        CheckPermission();
        
        return new BaselineThresholdsDAL().GetHostManageNodeProperties(hostID);
    }


    private void CheckPermission()
    {
        if (!((ProfileCommon)HttpContext.Current.Profile).AllowAdmin)
            throw new UnauthorizedAccessLocalizedException();
    }

}