﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true"
    CodeFile="HostDetails.aspx.cs" Inherits="Orion_VIM_Virtualization_HostDetails" %>
<%@ Import Namespace="SolarWinds.VIM.Web.StatusIcons" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
               <br />
                <div style="margin-left: 10px" >
                    <orion:PluggableDropDownMapPath ID="NodeSiteMapPath" runat="server" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
                </div>
                <%} %>

    <h1>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%>
            -
            <img style="border-width: 0px;" src="/Orion/StatusIcon.ashx?size=small&amp;entity=<%= VimIconProvider.HostEntityName %>&amp;hint=<%= this.Host.Model.Platform %>&amp;status=<%= this.Host.Model.Status %>" />
            <%= HttpUtility.HtmlEncode(this.Host.Name)%>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>
