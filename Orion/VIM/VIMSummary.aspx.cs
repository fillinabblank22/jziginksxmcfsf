using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Common;

public partial class Orion_VIM_Virtualization_VIMSummary : VimViewPage
{
    public override string ViewType
    {
        get 
        {
            return "VIM Integrated Summary"; 
        }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMAG_Views_VMwareSummary";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
