﻿using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Configuration;
using SolarWinds.VIM.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Base.Contract.Models;
using VMCredential = SolarWinds.Orion.Core.Common.Models.VMCredential;
using SolarWinds.VIM.Web.Common.Extensions;

public partial class Orion_VIM_Discovery_Controls_ESXCredentialDialog : System.Web.UI.UserControl
{
    private new static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public DiscoveryConfiguration Configuration { get; set; }

    public List<CredentialInfo> SelectedCredentials
    {
        get
        {
            if (this.ViewState["CredentialGrid"] == null)
            {
                LoadData();
            }

            return (List<CredentialInfo>)this.ViewState["CredentialGrid"];
        }

        set
        {
            this.ViewState["CredentialGrid"] = value;
        }
    }

    private List<CredentialInfo> AllCredentials
    {
        get
        {
            if (this.ViewState["ChooseCredential"] == null)
            {
                LoadData();
            }
            return (List<CredentialInfo>)this.ViewState["ChooseCredential"];
        }

        set
        {
            this.ViewState["ChooseCredential"] = value;
        }
    }

    public event EventHandler BindDataEvent;

    public void TextBoxesDisable(bool disable)
    {
        this.CredentialName.Enabled = !disable;
        this.UserName.Enabled = !disable;
        this.Password.Enabled = !disable;
        this.ConfirmPassword.Enabled = !disable;
    }

    public void AddCredential()
    {
        this.CredentialDialog.Visible = true;
        this.ActionHeader.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_1;

        this.ActionDropDown.Items.Clear();
        this.ActionDropDown.Items.Add(new ListItem()
        {
            Text = Resources.VIMWebContent.VIMWEBCODE_VB0_2,
            Value = "new",
            Selected = true
        });

        if (AllCredentials.Count > 0)
        {
            foreach (var credential in AllCredentials)
            {
                this.ActionDropDown.Items.Add(new ListItem()
                {
                    Text = credential.Description,
                    Value = AllCredentials.IndexOf(credential).ToString(),
                });
            }
        }

        this.CredentialName.Text = String.Empty;
        this.UserName.Text = String.Empty;
        this.Password.Attributes["value"] = String.Empty;
        this.ConfirmPassword.Attributes["value"] = String.Empty;
        TextBoxesDisable(false);

        this.SubmitDialog.CommandArgument = String.Empty;
        this.modalityDiv.ShowBlock = true;
    }

    public void LoadData()
    {
        var cfgforVim = Configuration.GetDiscoveryPluginConfiguration<VimDiscoveryPluginConfiguration>();

        // new run
        if (cfgforVim.SelectedVmWareCredentials == null)
        {
            SelectedCredentials = LoadCredentialsFromDB();
            AllCredentials = new List<CredentialInfo>();
        }
        else
        {
            AllCredentials = LoadCredentialsFromDB();
            SelectedCredentials = new List<CredentialInfo>();
            // check if selected credentials still exist
            foreach (var selectedCredId in cfgforVim.SelectedVmWareCredentials)
            {
                var found = this.AllCredentials.FirstOrDefault(item => item.Id == selectedCredId);

                if (found != null)
                {
                    // move from all to selected
                    this.AllCredentials.Remove(found);

                    found.Description = found.Description.HtmlEncode();
                    found.Username = found.Username.HtmlEncode();
                    this.SelectedCredentials.Add(found);
                }
                else
                {
                    // this credential was not found
                    log.WarnFormat("Credential with ID = {0} not found.", selectedCredId);
                }
            }
        }

        
    }

    protected void AddNewCredential()
    {
        CredentialInfo credentialInfo;

        if (ActionDropDown.SelectedValue == "new")
        {
            // add new credential to db
            using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
            {
                var credential = new Credential
                {
                    Description = CredentialName.Text,
                    Username = UserName.Text,
                    Password = Password.Text
                };

                long newCredentialId = proxy.Api.AddVMwareCredential(credential);

                credentialInfo = new CredentialInfo
                {
                    Description = credential.Description,
                    Username = credential.Username,
                    Id = newCredentialId
                };
            }
        }
        else
        {
            // existing credentials
            int i = int.Parse(ActionDropDown.SelectedValue);
            credentialInfo = AllCredentials[i];
            AllCredentials.RemoveAt(i);
        }

        // add new credential to the grid
        SelectedCredentials.Add(credentialInfo);

        CredentialDialog.Visible = false;
        modalityDiv.ShowBlock = false;

        if (BindDataEvent != null)
        {
            BindDataEvent(this, EventArgs.Empty);
        }
    }

    // move credentials up
    protected void MoveItemUp(int selectedIndex)
    {
        if (selectedIndex > 0)
        {
            SelectedCredentials.Reverse(selectedIndex - 1, 2);
            if (BindDataEvent != null)
                BindDataEvent(this, EventArgs.Empty);
        }
    }

    // move credentials down
    protected void MoveItemDown(int selectedIndex)
    {
        if (selectedIndex >= 0 && selectedIndex < SelectedCredentials.Count - 1) 
        {
            SelectedCredentials.Reverse(selectedIndex, 2);
            if (BindDataEvent != null)
                BindDataEvent(this, EventArgs.Empty);
        }
    }

    // delete credentials
    protected void DeleteCredential(int selectedIndex)
    {
        if (selectedIndex >= 0)
        {
            AllCredentials.Add(SelectedCredentials[selectedIndex]);
            SelectedCredentials.RemoveAt(selectedIndex);
            if (BindDataEvent != null)
                BindDataEvent(this, EventArgs.Empty);
        }
    }

    private List<CredentialInfo> LoadCredentialsFromDB()
    {
        using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
        {
            return proxy.Api.GetAllVMwareCredentials("Description", true);
        }
    }

    /// <summary>
    /// Method executed when user clicked on ADD button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void ActionItemCommand(object sender, CommandEventArgs e)
    {
        // TODO: Move content of method from ESXCredentials.aspx here
        int itemIndex = -1;

        if (e.CommandArgument != null)
            if (!int.TryParse(e.CommandArgument.ToString(), out itemIndex))
                itemIndex = -1;

        switch (e.CommandName)
        {
            case "MoveUp":
                MoveItemUp(itemIndex);
                break;
            case "MoveDown":
                MoveItemDown(itemIndex);
                break;
            case "DeleteItem":
                DeleteCredential(itemIndex);
                break;
            case "SubmitDialogCommand":
                if (Page.IsValid)
                {
                    AddNewCredential();
                }
                break;
        }
    }

    // loging for bussines layer
    protected new static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
    
    protected void ChooseCredentialChanged(object sender, EventArgs e)
    {
        // TODO: Move content of method from ESXCredentials.aspx here
        DropDownList dd = (DropDownList)sender;

        if (dd.SelectedValue == "new")
        {
            this.CredentialName.Text = String.Empty;
            this.UserName.Text = String.Empty;
            this.Password.Attributes["value"] = String.Empty;
            this.ConfirmPassword.Attributes["value"] = String.Empty;
            TextBoxesDisable(false);
        }
        else
        {
            int i = int.Parse(dd.SelectedValue);

            string pass = "**********";
            this.CredentialName.Text = AllCredentials[i].Description;
            this.UserName.Text = AllCredentials[i].Username;
            this.Password.Attributes["value"] = pass;
            this.ConfirmPassword.Attributes["value"] = pass;
            TextBoxesDisable(true);
        }
    }

    protected void CredentialNameDuplicityValidation(object sender, ServerValidateEventArgs e)
    {
        // TODO: Move content of method from ESXCredentials.aspx here
        e.IsValid = true;

        if (this.ActionDropDown.SelectedValue == "new")
        {
            using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
            {
                e.IsValid = proxy.Api.CheckVMwareCredentialName(e.Value);
            }
        }
    }

    /// <summary>
    /// Method executed when user clicked on CANCEL button
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CancelDialog_OnClick(object sender, EventArgs e)
    {
        // TODO: Move content of method from ESXCredentials.aspx here
        this.CredentialDialog.Visible = false;
        this.modalityDiv.ShowBlock = false;
        
        if (BindDataEvent != null)
            BindDataEvent(this, EventArgs.Empty);
    }
}