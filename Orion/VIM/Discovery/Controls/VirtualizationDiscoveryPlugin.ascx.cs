﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Constants;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;

public partial class Orion_Discovery_Controls_NetworkDiscoveryPlugin : System.Web.UI.UserControl
{
    // the setting contains integration even though we are no longer redirecting to integration
    // this is to avoid back compatibility issues
    private const string FirstLoadSettingName = "VIM_FirstLoadRedirectedToIntegration";
    private static Log Log = new Log();

    protected int Count { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (RedirectToAddNodeIfNeeded())
        {
            return;
        }

        try
        {
            using (var proxy = BusinessLayerProxyCreatorFactory.GetApiProxy())
            {
                Count = proxy.Api.GetTotalCountOfManagedEntities();
            }
        }
        catch (Exception ex) {
            Log.ErrorFormat("Cannot get number of managed objects. Details: {0}",ex);
        }

        DataBind();
    }

    protected string ReturnUrl
    {
        get
        {
            var currentUrl = Request.Url.AbsoluteUri;
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(currentUrl);
            return Convert.ToBase64String(plainTextBytes);
        }
    }

    private bool RedirectToAddNodeIfNeeded()
    {
        try
        {
            if (!WebSettingsDAL.GetValue(FirstLoadSettingName, false))
            {
                using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => Log.Error(ex)))
                {
                    var licenses = proxy.GetModuleLicenseInformation();

                    var excluded = new HashSet<string>
                    {
                        "orion",
                        "vim",
                        "cloudmonitoring"
                    };

                    if (licenses.All(l => excluded.Contains(l.ModuleName.ToLowerInvariant())))
                    {
                        Log.Info("Redirecting from discovery central to Add Node page.");
                        WebSettingsDAL.SetValue(FirstLoadSettingName, true);

                        DiscoveryCentralHelper.Leave();
                        Response.Redirect(Links.AddNode, false);
                        return true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Log.Error(ex);
        }

        return false;
    }

    protected void RedirectToAddNodeButton_OnClick(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect(Links.AddNode);
    }
}