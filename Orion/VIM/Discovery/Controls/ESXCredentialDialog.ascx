﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ESXCredentialDialog.ascx.cs" Inherits="Orion_VIM_Discovery_Controls_ESXCredentialDialog" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>

<asp:Panel ID="CredentialDialog" runat="server" BorderWidth="1"  Visible="false" CssClass="EditPanel">
        <p class="DialogHeaderText">
            <asp:Label ID="ActionHeader" runat="server" Text="" Font-Bold="true" Font-Size="Larger" />
        </p>
        
        <p class="DialogHeaderText">
        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_2%>abcd
        </p>
        
        <asp:Panel ID="CredentialControls" runat="server" BorderWidth="0" Visible="true" Class="EditControls">
            <%= Resources.VIMWebContent.VIMWEBDATA_VB0_4%><br />
            <asp:DropDownList ID="ActionDropDown" runat="server" Width="200" OnSelectedIndexChanged="ChooseCredentialChanged" AutoPostBack="true"></asp:DropDownList>
            
            <div class="smallTitle"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_3%></div>
            <asp:TextBox ID="CredentialName" runat="server" Width="195" Enabled="true" autocomplete="off" MaxLength="50" />
            <br />
            <asp:RequiredFieldValidator ID="CredentialNameTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_5%>" ControlToValidate="CredentialName" EnableClientScript="true" Display="Dynamic" />
            <asp:CustomValidator runat="server" id="CredentialNameDuplicityValidator" controltovalidate="CredentialName" onservervalidate="CredentialNameDuplicityValidation" errormessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_6%>" Display="Dynamic"/>

            <div class="smallTitle"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_7%></div>
            <asp:TextBox ID="UserName" runat="server" Width="195" Enabled="true" autocomplete="off" MaxLength="50" />
            <br />
            <div class="smallText"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_8%></div>
            <asp:RequiredFieldValidator ID="UserNameTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_9%>" ControlToValidate="UserName" EnableClientScript="true" Display="Dynamic" />

            <div class="smallTitle"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_10%></div>
            <asp:TextBox ID="Password" runat="server" Width="195" Enabled="true" autocomplete="off" MaxLength="50" TextMode="Password"/>
            <br />
            <asp:RequiredFieldValidator ID="PasswordTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_11%>" ControlToValidate="Password" EnableClientScript="true" Display="Dynamic" />

            <div class="smallTitle"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_12%></div>
            <asp:TextBox ID="ConfirmPassword" runat="server" Width="195" Enabled="true" autocomplete="off" MaxLength="50" TextMode="Password" />
            <br />
            <asp:RequiredFieldValidator ID="ConfirmPasswordTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_13%>" ControlToValidate="ConfirmPassword" EnableClientScript="true" Display="Dynamic" />
            <asp:CompareValidator runat="server" ID="ConfirmPasswordTextBoxCompareValidator" ControlToValidate="ConfirmPassword" ControlToCompare="Password" 
                EnableClientScript="true" ErrorMessage="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_14%>" />

            <div class="sw-btn-bar">
                <orion:LocalizableButton ID="SubmitDialog" runat="server" CausesValidation="true"                                                  
                    OnCommand="ActionItemCommand" CommandName="SubmitDialogCommand" LocalizedText = "CustomText" DisplayType="Primary" Text = "<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_27%>" />
                <orion:LocalizableButton ID="CancelDialog" runat="server" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClick="CancelDialog_OnClick" />
            </div>
        </asp:Panel>
    </asp:Panel>

<orion:TBox ID="modalityDiv" runat="server" />