﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.VIM.Base.Contract.Models;
using VMCredential = SolarWinds.Orion.Core.Common.Models.VMCredential;
using SolarWinds.VIM.Common.Configuration;

public partial class Orion_VIM_Discovery_Controls_ESXCredentials : System.Web.UI.UserControl
{

    private new static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    protected DiscoveryConfiguration configuration;

    public List<CredentialInfo> SelectedCredentials
    {
        get
        {
            return CredentialDialog.SelectedCredentials;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        CredentialDialog.Configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        CredentialDialog.BindDataEvent += (sender, evt) => { BindData(); };
        if (configuration == null)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails
            {
                Error = new Exception(Resources.VIMWebContent.VIMWEBCODE_VB0_54),
                Title = Resources.VIMWebContent.VIMWEBCODE_VB0_55
            });
        }

        if (!IsPostBack)
        {
            BindData();
        }
    }

    protected string Encode(object o)
    {
        return Server.HtmlEncode((string)o);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    // loging for bussines layer
    protected new static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    // binds credentials data to gridView
    private void BindData()
    {
        this.CredentialsGrid.DataSource = CredentialDialog.SelectedCredentials;
        this.CredentialsGrid.DataBind();
    }

    private void TextBoxesDisable(bool disable)
    {
        this.CredentialDialog.TextBoxesDisable(disable);
    }

    protected void AddCredential_OnClick(object sender, EventArgs e)
    {
        CredentialDialog.AddCredential();
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        CredentialDialog.ActionItemCommand(sender, e);
    }
}