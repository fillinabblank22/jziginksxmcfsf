﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ESXCredentials.ascx.cs" Inherits="Orion_VIM_Discovery_Controls_ESXCredentials" %>

<%@ Register Src="~/Orion/VIM/Discovery/Controls/ESXCredentialDialog.ascx" TagPrefix="orion" TagName="ESXCredentialDialog" %>

<orion:ESXCredentialDialog ID="CredentialDialog" runat="server" />

<div class="contentBlock" id="ESXContent" runat="server">
        <table width="100%">
            <tr class="ButtonHeader">                    
                <td style="padding-left: 10px;">
                    <asp:LinkButton ID="AddCredential" runat="server" OnClick="AddCredential_OnClick" CssClass="iconLink"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_19%></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="CredentialsGrid" runat="server" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">                        
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_24%>">
                                <ItemTemplate>
                                    <%#(Container.DataItemIndex + 1).ToString()%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_25%>">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Encode(Eval("Description"))%>' Width="378" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_26%>">
                                <ItemTemplate>
                                    <asp:ImageButton ID="MoveUpBtn" runat="server" ToolTip="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_20%>" ImageUrl="~/Orion/Discovery/images/icon_up.gif"
                                        OnCommand="ActionItemCommand" CommandName="MoveUp" CommandArgument="<%#Container.DataItemIndex %>" />
                                    <asp:ImageButton ID="MoveDownBtn" runat="server" ToolTip="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_21%>" ImageUrl="~/Orion/Discovery/images/icon_down.gif"
                                        OnCommand="ActionItemCommand" CommandName="MoveDown" CommandArgument="<%#Container.DataItemIndex %>" />
                                    <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_22%>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                                        OnCommand="ActionItemCommand" CommandName="DeleteItem" CommandArgument="<%#Container.DataItemIndex %>" />  
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <%= Resources.VIMWebContent.VIMWEBDATA_VB0_23%>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
