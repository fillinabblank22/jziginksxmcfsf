﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualizationDiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_NetworkDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<style type="text/css">
    .coreDiscoveryIcon
    {
        float:left;
        padding-left: 20px;
    }
    .coreDiscoveryPluginBody
    {
        padding:0px 75px 5px 75px;
    }
    a.helpLink
    {
        color: #336699;
        font-size:smaller;
    }

    a.helpLink:hover
    {
        color:Orange;
    }
</style>
<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/VIM/images/virtualization.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= Resources.VIMWebContent.VIMWEBDATA_TM0_21%></h2>
    <div>
        <%= Resources.VIMWebContent.VIMWEBDATA_TM0_22%></div>
    <orion:ManagedNetObjectsInfo ID="vwInfo" runat="server" EntityName="<%$ Resources: VIMWebContent, VIMWEBDATA_TM0_24%>" NumberOfElements="<%# Count %>" />

    <orion:LocalizableButton ID="RedirectToAddNodeButton" runat="server" LocalizedText="CustomText" OnClick="RedirectToAddNodeButton_OnClick"
                             Text="<%$ Resources: VIMWebContent, DiscoveryPlugin_RedirectToAddNode %>" DisplayType="Secondary"/>
</div>
