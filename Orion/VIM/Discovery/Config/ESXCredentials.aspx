﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" AutoEventWireup="true"
    CodeFile="ESXCredentials.aspx.cs" Inherits="Orion_Discovery_Config_ESXCredentials" Title="<%$ Resources: VIMWebContent,VIMWEBDATA_VB0_1%>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Web.Discovery"%>
<%@ Import Namespace="SolarWinds.VIM.Common"%>

<%@ Register Src="~/Orion/VIM/Discovery/Controls/ESXCredentials.ascx" TagPrefix="orion" TagName="ESXCredentials" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Constants" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <style type="text/css"> 
        .smallText {color:#979797;font-size:9px;}
        .smallTitle {padding-top:5px;}
        a.helpLink { color: #336699; font-size: smaller; }
        a.helpLink:hover { color: Orange; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
<asp:UpdatePanel ID="CredentialsUpdatePanel" runat="server">
    <ContentTemplate>

    <span class="ActionName"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_15%></span>
    <br />
    <br />
<% if (new VimSettings().DisableOldDiscovery)
    { %>
            <orion:LocalizableButtonLink ID="AddNewDeviceButton" runat="server" LocalizedText="CustomText" target="_blank"
                                         Text="<%$ Resources: VIMWebContent, Discovery_Virtualization_AddVCenterHyperVAndNutanix %>"/>
            <br />
    <p class="helpfulText"><%= Resources.VIMWebContent.Discovery_Virtualization_MonitorVMWareHyperVAndNutanix%></p>
<% }
   else {%>
    <span style="width:auto;"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_16%></span>
    <br />
    <br />
    <%= Resources.VIMWebContent.VIMWEBDATA_VB0_17%>
    <br />
    <br />

    <div style="margin-top: 15px; vertical-align: middle;">
        <asp:CheckBox runat="server" ID="ESXCheckBox" AutoPostBack="true" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_18 %>" OnCheckedChanged="PollESXChanged"/>
    </div>

    <orion:ESXCredentials ID="ESXCredentials" runat="server" />
    <% } %>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary"
            runat="server" ID="imgBack" OnClick="imgbBack_Click" CausesValidation="false" />
        <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"
            runat="server" ID="imgbNext" OnClick="imgbNext_Click" CssClass="NetworkNextButton"/>
        <orion:LocalizableButton class="CancelButton" LocalizedText ="Cancel" DisplayType="Secondary" runat="server"
            ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
    </div>  

    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
