﻿using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.VIM.Common.Constants;

public partial class Orion_Discovery_Config_ESXCredentials : ConfigWizardBasePage, IStep
{
    private new static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    protected DiscoveryConfiguration configuration;

    /// <summary>
    /// Save actual configuration from wizard to discovery plugin configuration.
    /// </summary>
    private void SaveConfigurationForDiscoveryPlugin()
    {
        if (this.configuration == null)
        {
            return;
        }

        // we get configuration for this plugin
        var cfgforVim = this.configuration.GetDiscoveryPluginConfiguration<VimDiscoveryPluginConfiguration>();
        cfgforVim.IsDiscoveryForVimEnabled = this.ESXCheckBox.Checked;

        // now we save all selected credentials
        cfgforVim.SelectedVmWareCredentials = new List<long>();
        cfgforVim.SelectedVmWareCredentials.AddRange(ESXCredentials.SelectedCredentials.Select(item => item.Id));        
    }

    // loging for bussines layer
    protected new static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    } 

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        AddNewDeviceButton.NavigateUrl = Links.AddNode;

        InitButtons(imgbCancel);

        configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;

        if (configuration == null)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails
                                                       {
                                                           Error = new Exception(Resources.VIMWebContent.VIMWEBCODE_VB0_54),
                                                           Title = Resources.VIMWebContent.VIMWEBCODE_VB0_55
                                                       });
        }

        var cfgforVim = this.configuration.GetDiscoveryPluginConfiguration<VimDiscoveryPluginConfiguration>();

        if (cfgforVim == null)
        {
            // configuration not created yet - we'll create it and register
            cfgforVim = new VimDiscoveryPluginConfiguration() {IsDiscoveryForVimEnabled = true};
            this.configuration.AddDiscoveryPluginConfiguration(cfgforVim);
        }

        if (!IsPostBack)
        {
            // values from new discovery configuration
            ESXCheckBox.Checked = cfgforVim.IsDiscoveryForVimEnabled;
            ESXCredentials.Visible = ESXCheckBox.Checked;
        }
    }         
   
    protected void PollESXChanged(object sender, EventArgs e)
    {
        this.ESXCredentials.Visible = ESXCheckBox.Checked;
        this.configuration.PollForESX = ESXCheckBox.Checked;
    }

    protected string Encode(object o)
    {
        return Server.HtmlEncode((string)o);
    }

    protected void SaveData(DiscoveryConfiguration cfg)
    {
        SaveConfigurationForDiscoveryPlugin();
    }

    protected override void Next()
    {
        SaveData(configuration);
    }

    protected override void Back()
    {
        SaveData(configuration);
    }

    #region IStep Members
    public string Step
    {
        get
        {
            return "ESX";
        }
    }
    #endregion
}
