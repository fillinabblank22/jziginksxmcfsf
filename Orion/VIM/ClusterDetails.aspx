﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true"
    CodeFile="ClusterDetails.aspx.cs" Inherits="Orion_VIM_Virtualization_ClusterDetails" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" runat="Server">
    <%if (!CommonWebHelper.IsBreadcrumbsDisabled)
               { %>
               <br />
                <div style="margin-left: 10px" >
                    <orion:PluggableDropDownMapPath ID="NodeSiteMapPath" runat="server" SiteMapKey="NetObjectDetails" LoadSiblingsOnDemand="true"/>
                </div>
                <%} %>

    <h1>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%>
            -
            <img style="border-width: 0px;" src="/Orion/StatusIcon.ashx?size=small&amp;entity=Orion.VIM.Clusters&amp;status=<%= this.Cluster.Model.Status %>" />
            <%= HttpUtility.HtmlEncode(this.Cluster.Name)%>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
    </h1>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>
