﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Common.Extensions;

public partial class Orion_VIM_Virtualization_HostDetails : VimViewPage, IVimHostProvider
{
    public override string ViewType
    {
        get { return "VIM Host Details"; }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMPH_HostDetails_VMwareHostDetails";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Host == null || Host.Model == null)
        {
            throw new AccountLimitationException();
        }
        if (Host.Model.NodeID.HasValue && Host.Model.NodeID.Value > 0)
        {
            Response.Redirect($"/Orion/View.aspx?NetObject=N:{Host.Model.NodeID}");
        }
        else if (Host.Model.PollingSource.IsVanillaPolled())
        {
            Response.Redirect($"/Orion/VIM/UnmanagedNode.aspx?NetObject={VimVirtualMachine.Prefix}:{Host.Model.ID}");
        }
    }

    protected string PageTitle { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        // Set the header "X-UA-Compatible" value to "IE=8"
        ModulePatchHelper.SetIECompatibilityRendering(8);

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    #region IVimHostProvider Members

    public VimHost Host
    {
        get { return (VimHost)this.NetObject; }
    }

    #endregion
}
