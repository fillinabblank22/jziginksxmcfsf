﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_TopXXEditPage : System.Web.UI.MasterPage
{

    #region Resource property
    private ResourceInfo resource;

    protected ResourceInfo Resource
    {
        get { return this.resource; }
        set { this.resource = value; }
    }

    private String netObjectID;
    protected String NetObjectID
    {
        get {return this.netObjectID;}
        set {this.netObjectID = value;}
        
    }
    #endregion

   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.Resource = ResourceManager.GetResourceByID(resourceID);
            
        }
        if (!String.IsNullOrEmpty(this.Request.QueryString["NetObjectID"]))
        {
            this.NetObjectID = Request.QueryString["NetObjectID"];
        }


        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
            {
                this.Resource.Properties["MaxRecords"] = "10";
            }
            this.maxCount.Text = this.Resource.Properties["MaxRecords"];
            this.filter.Text = this.Resource.Properties["Filter"];

            if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
            {
                this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
            }
            else
            {
                this.resourceTitleEditor.ResourceTitle = string.Empty;
            }

            if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
            {
                this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
            }
            else
            {
                this.resourceTitleEditor.ResourceSubTitle = string.Empty;
                
            }

            // If NetObjectID is provided then we can hide this section
            if (String.IsNullOrEmpty(this.NetObjectID))
            {
                ucFilterByPlatform.Visible = true;
                ucFilterByPlatform.LoadFromResourceProperties(this.Resource.Properties);
            }
            else
            {
                ucFilterByPlatform.Visible = false;
            }
        }
        this.DataColumns.Visible = true;
        string entityName = Request.QueryString["EntityName"];
        System.Data.DataTable table = new SolarWinds.VIM.Web.DAL.TopXXEditDAL().GetTableColumnNames(entityName);
        this.dataTableColumns.DataSource = table;
        this.dataTableColumns.DataBind();
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
            this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;

            this.Resource.Properties["MaxRecords"] = this.maxCount.Text;
            this.Resource.Properties["Filter"] = this.filter.Text;

            if (ucFilterByPlatform.Visible)
            {
                ucFilterByPlatform.SaveProperties(this.Resource.Properties);
            }

            if (!String.IsNullOrEmpty(this.NetObjectID))
            {
                Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", this.Resource.View.ViewID,this.NetObjectID));
            }
            else
            {
                Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}", this.Resource.View.ViewID));
            }
        }
    }
}
