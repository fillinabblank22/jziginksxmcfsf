﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Web.Common.Extensions;

public partial class Orion_VIM_Virtualization_VMDetails : VimViewPage, IVimVirtualMachineProvider
{
    public override string ViewType
    {
        get { return "VIM Virtual Machine Details"; }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMPH_VMDetails_VMwareVMDetails";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (VirtualMachine == null || VirtualMachine.Model == null)
        {
            throw new AccountLimitationException();
        }
        if (VirtualMachine.Model.NodeID > 0)
        {
            Response.Redirect($"/Orion/View.aspx?NetObject=N:{VirtualMachine.Model.NodeID}");
        }
        else if (VirtualMachine.Model.PollingSource.IsVanillaPolled())
        {
            Response.Redirect($"/Orion/VIM/UnmanagedNode.aspx?NetObject={VimVirtualMachine.Prefix}:{VirtualMachine.Model.ID}");
        }
    }

    protected string PageTitle { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        // Set the header "X-UA-Compatible" value to "IE=8"
        ModulePatchHelper.SetIECompatibilityRendering(8);

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    #region IVimVMProvider Members

    public VimVirtualMachine VirtualMachine
    {
        get { return (VimVirtualMachine)this.NetObject; }
    }

    #endregion
}
