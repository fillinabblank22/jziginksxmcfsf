﻿<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true" ClassName="AllAlertsProxy" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>


<script runat="server">
    private const string AllAlertsUrl = "/Orion/NetPerfMon/Alerts.aspx";
    private const string UrlParamName = "alertName";
    private const string ValueFormatter = @"{{""Name"":""Alert name"",""Value"":""AlertName"",""Type"":""System.String"",""SubGroupValue"":""{0}""}}";
    

    protected void Page_Init(object sender, EventArgs e)
    {
        var alertName = Request.QueryString[UrlParamName];
        if (String.IsNullOrEmpty(alertName)) return;
        
        WebUserSettingsDAL.Set("ActiveAlerts_GroupingValue", String.Format(ValueFormatter, alertName));
        Response.Redirect(AllAlertsUrl, true);
    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="Server">
    
    <div>
        <p style="width: 300px!important;white-space: nowrap">
            <img src="/Orion/images/stop_32x32.gif" />
            <span style="line-height: 32px;vertical-align: 31%;padding-left: 10px;font-weight: bold;font-size: 120%">
                <%= Resources.CoreWebContent.WEBDATA_JD0_14 %>
            </span>
        </p>
        <br />
        <p><orion:LocalizableButton ID="LocalizableButton1" LocalizedText="Back" DisplayType="Secondary" runat="server" OnClientClick="history.go(-1)" /></p>
    </div>
    
</asp:Content>