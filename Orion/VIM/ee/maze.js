/* ---------------------------------------------------- GENERAL SETTINGS ---------------------------------------------------- */
var passKeyCode = "vman";
var canvasElementId = "vimEasterEggMazeCanvas";
var overlayElementId = "vimEasterEggMazeOverlay";
var DEBUG = false; //turns on light, possibility to display FPS by hitting 'f' and adds free camera movement
var assetsPath = "ee/assets";


/* -------------------------------------------------- TRIGGERING EASTER EGG ---------------------------------------------------*/
document.addEventListener("keypress", vmanEventListener);
var keyPressIndex = 0;

function vmanEventListener(event) {
    if (event.target.nodeName == 'INPUT') return;
    if (keyPressIndex < 4){
        if (event.key == passKeyCode.charAt(keyPressIndex)) {
            keyPressIndex++;
            if (keyPressIndex >= 4) {
                displayOverlayAndStartGame();
            }
        }
        else {
            keyPressIndex=0;
        }
    }
}

/* ----------------------------------------------------- STARTING THE GAME --------------------------------------------------- */
var engine = undefined;
var canvas = undefined;
var scene = undefined;
var level = 0;
var score = 0;
var maxLives = 3;
var lives = maxLives;

function displayOverlayAndStartGame(){
    //displayOverlay
    document.getElementById(overlayElementId).style.display = "block";

    if (!canvas) {
        canvas = document.getElementById(canvasElementId);
    }
    if (!engine) {
        engine = new BABYLON.Engine(canvas, true, { stencil: true });
        engine.getCaps().parallelShaderCompile = null;

        // the canvas/window resize event handler
        window.addEventListener('resize', function(){
            engine.resize();
        });
    }
    startGame();
    $(function() {
        SW.Core.View.InitRefreshInterval(30000000, false);
    });
}

function startGame(){
    if (scene) {
        scene.dispose();
    }
    scene = createScene();
    // run the render loop
    engine.runRenderLoop(function(){
        scene.render();
    });
}

function stopGameAndHideOverlay(){
    if (scene) {
        scene.dispose();
    }

    //hide overlay
    document.getElementById(overlayElementId).style.display = "none";

    //reset trigger
    keyPressIndex = 0;
    
    $(function() {
        SW.Core.View.InitRefreshInterval(300000, false);
    });
}

/* ------------------------------------------------------- GAME ITSELF ------------------------------------------------------- */
function createScene(){
    scene = new BABYLON.Scene(engine);
    scene.collisionsEnabled = true;
    scene.gravity = new BABYLON.Vector3(0, -9.81, 0);

    var settings = {
        width: 7,
        height: 7,
        startX: 3,
        startY: 1
    };
    
    // GAME OBJECTS ---------------------------------------------
    var gameObjects = {
        camera: undefined,
        lightDimmingAnimation: undefined,
        cameraCollisionMesh: undefined,
        trophy: createTrophy(),
        floorMaterial: createFloorMaterial(),
        wallMaterial: createWallMaterial(),
        fireMaterial: createFireMaterial(),
        smashAnimation: createSmashAnimation(),
        crystalMaterial: createCrystalMaterial(),
        crystalAnimation: createCrystalAnimation(),
        wallMesh: undefined,
        walls: [],
        traps: [],
        crystals: [],
        fpsText: undefined,
        trophiesText: undefined,
        crystalsText: undefined,
        floor: undefined,
        ceiling: undefined,
        mazePoints: undefined,
        hearths: []
    };
    createCameraWithLight(gameObjects);
    
    startLevel(settings, gameObjects);
    if(DEBUG){
        var sceneLight = new BABYLON.DirectionalLight("sceneLight", new BABYLON.Vector3(-2, -5, 2), scene);
    }

    // GUI ------------------------------------------------------
    createGUI(gameObjects);

    // INPUT ----------------------------------------------------
    var keysDown = {}; 
    scene.actionManager = new BABYLON.ActionManager(scene);
    scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
        keysDown[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
    }));

    // UPDATE ---------------------------------------------------
    scene.registerAfterRender(function() {
        if(DEBUG){
            gameObjects.fpsText.text = engine.getFps().toFixed(2);
            
            if(keysDown["f"]) {
                gameObjects.fpsText.isVisible = !gameObjects.fpsText.isVisible;
                keysDown["f"] = false;
            };
        }
        
        showOnlyVisibleObjects(gameObjects)

        checkCollisions(gameObjects);

        if(gameObjects.camera.position.subtract(gameObjects.trophy.position).length() < 1.2){
            // victory!
            if(lives < maxLives){
                gameObjects.hearths[maxLives-lives].isVisible = true;
                lives++;
            }
            level++;
            gameObjects.trophiesText.text = level.toString();
            
            settings.height=7+level;
            if(settings.height>20)settings.height=20
            settings.width=7+level;
            if(settings.width>20)settings.height=20
            settings.startX=Math.floor(settings.width/2);
            startLevel(settings, gameObjects);
            gameObjects.deathText.text = "Level " + (level+1) + " (" + (7+level) + "x" + (7+level) + "). " + score + " crystals so far.";
            scene.beginAnimation(gameObjects.deathText, 0, 200, false, 0.5);
            
            gameObjects.camera.fadeColor = new BABYLON.Color3(0,0,0);
            scene.beginAnimation(gameObjects.camera, 0, 1, false);
        }

        if(!gameObjects.camera.isAlive){
            lives--;
            if(lives > 0){
                gameObjects.hearths[maxLives-lives].isVisible = false;
                gameObjects.camera.isAlive = true;
                gameObjects.camera.fadeColor = new BABYLON.Color3(1,0,0);
                scene.beginAnimation(gameObjects.camera, 0, 1, false);
            }
            else{
                // lost
                level = 0;
                gameObjects.trophiesText.text = "0";
                gameObjects.deathText.text = "Score: " + score + ". Let's try that again!";
                score = 0;
                lives = maxLives;
                for(var i=0;i<maxLives;i++){
                    gameObjects.hearths[i].isVisible = true;
                }
                gameObjects.crystalsText.text = "0";
                settings.height=7;
                settings.width=7;
                settings.startX=3;
                startLevel(settings, gameObjects);
                scene.beginAnimation(gameObjects.deathText, 0, 200, false, 0.5);
            
                gameObjects.camera.fadeColor = new BABYLON.Color3(0,0,0);
                scene.beginAnimation(gameObjects.camera, 0, 1, false);
            }
        }
        
        for(var i = gameObjects.crystals.length-1;i>=0;i--){
            if(gameObjects.crystals[i].take){
                gameObjects.crystals[i].dispose();
                gameObjects.crystals.splice(i, 1);
                score++;
                gameObjects.crystalsText.text = score.toString();
            }
        }
    });

    return scene;
}

function createGUI(gameObjects){
    var adt = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    
    var trophiesPanel = new BABYLON.GUI.StackPanel("trophiesPanel");
    trophiesPanel.width = "80px";
    trophiesPanel.height = "45px";
    trophiesPanel.left = "10px";
    trophiesPanel.isVertical = false;
    trophiesPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    trophiesPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
    adt.addControl(trophiesPanel);
    
    var trophiesImage = new BABYLON.GUI.Image("trophiesImage", assetsPath+"/trophies.png");
    trophiesImage.width = "20px";
    trophiesImage.height = "20px";
    trophiesPanel.addControl(trophiesImage);
    
    gameObjects.trophiesText = new BABYLON.GUI.TextBlock("trophiesText");
    gameObjects.trophiesText.text = "0";
    gameObjects.trophiesText.color = "white";
    gameObjects.trophiesText.fontSize = 20;
    trophiesPanel.addControl(gameObjects.trophiesText);
    
    var crystalsPanel = new BABYLON.GUI.StackPanel("crystalPanel");
    crystalsPanel.width = "80px";
    crystalsPanel.height = "45px";
    crystalsPanel.left = "100px";
    crystalsPanel.isVertical = false;
    crystalsPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    crystalsPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
    adt.addControl(crystalsPanel);
    
    var crystalsImage = new BABYLON.GUI.Image("crystalsImage", assetsPath+"/crystals.png");
    crystalsImage.width = "20px";
    crystalsImage.height = "20px";
    crystalsPanel.addControl(crystalsImage);
    
    gameObjects.crystalsText = new BABYLON.GUI.TextBlock("crystalText");
    gameObjects.crystalsText.text = "0";
    gameObjects.crystalsText.color = "white";
    gameObjects.crystalsText.fontSize = 20;
    crystalsPanel.addControl(gameObjects.crystalsText);
    
    gameObjects.deathText = new BABYLON.GUI.TextBlock("deathText");
    gameObjects.deathText.text = "Let's try that again!";
    gameObjects.deathText.color = "white";
    gameObjects.deathText.fontSize = 50;
    gameObjects.deathText.alpha = 0;
    adt.addControl(gameObjects.deathText);
    
    var keys = [
        {frame: 0, value:1},
        {frame: 100, value:1},
        {frame: 200, value:0}
    ];
    var deathTextAnimation = new BABYLON.Animation("crystalAnimation", "alpha", 200, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    deathTextAnimation.setKeys(keys);
    gameObjects.deathText.animations = [deathTextAnimation];
    
    
    var hearthPanel = new BABYLON.GUI.StackPanel("hearthPanel");
    hearthPanel.width = "80px";
    hearthPanel.height = "45px";
    hearthPanel.right = "100px";
    hearthPanel.isVertical = false;
    hearthPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
    hearthPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
    adt.addControl(hearthPanel);
    
    for(var i = 0;i<maxLives;i++){
        var heartImage = new BABYLON.GUI.Image("heartImage", assetsPath+"/hearth.png");
        heartImage.width = "20px";
        heartImage.height = "20px";
        hearthPanel.addControl(heartImage);
        gameObjects.hearths.push(heartImage);
    }    
    
    if(DEBUG){
        gameObjects.fpsText = new BABYLON.GUI.TextBlock();
        gameObjects.fpsText.text = "";
        gameObjects.fpsText.color = "white";
        gameObjects.fpsText.fontSize = 10;
        adt.addControl(gameObjects.fpsText);
        gameObjects.fpsText.isVisible = false;
    }
    
    // create postprocess
    BABYLON.Effect.ShadersStore["fadePixelShader"] = `
    #ifdef GL_ES
        precision highp float;
    #endif

    varying vec2 vUV;
    uniform sampler2D textureSampler;

    uniform float fadeLevel;
    uniform vec3 color;

    void main(void) 
    {
        vec3 baseColor = texture2D(textureSampler, vUV).rgb;
        gl_FragColor = vec4(baseColor+((color-baseColor) * fadeLevel), 1.0);
    }`;
    gameObjects.postProcess = new BABYLON.PostProcess("fade", "fade", ["fadeLevel", "color"], null, 1.0, gameObjects.camera);
    gameObjects.postProcess.onApply = (effect) => {
           effect.setFloat("fadeLevel", gameObjects.camera.fadeTime);
           effect.setFloat3("color", gameObjects.camera.fadeColor.r, gameObjects.camera.fadeColor.g, gameObjects.camera.fadeColor.b);
    };
}

/* -------------------------------------------------- GAME HELPER FUNCTIONS -------------------------------------------------- */
function showOnlyVisibleObjects(gameObjects){
    for(var i=0;i<gameObjects.walls.length;i++){
        var w = gameObjects.walls[i];
        w.isVisible = w.position.subtract(gameObjects.camera.position).length() < 35;
    }
    for(var i=0;i<gameObjects.crystals.length;i++){
        var c = gameObjects.crystals[i];
        c.isVisible = c.position.subtract(gameObjects.camera.position).length() < 35;
    }
    for(var i=0;i<gameObjects.traps.length;i++){
        var t = gameObjects.traps[i];
        t.isVisible = t.position.subtract(gameObjects.camera.position).length() < 35;
    }
}

function createWallMeshPrototype(gameObjects){
    var wallMesh = BABYLON.MeshBuilder.CreateBox("crate", {height: 4, width: 4, depth: 4}, scene);
    wallMesh.isVisible = false;
    wallMesh.material = gameObjects.wallMaterial;
    wallMesh.position = new BABYLON.Vector3(-10,-10, -10);
    wallMesh.checkCollisions = true;
    wallMesh.freezeWorldMatrix();
    wallMesh.cullingStrategy = BABYLON.AbstractMesh.CULLINGSTRATEGY_OPTIMISTIC_INCLUSION_THEN_BSPHERE_ONLY;    
    return wallMesh;
}

function createWall(x, y, gameObjects){
    //Simple crate
    var box = gameObjects.wallMesh.createInstance("w");
    box.position = new BABYLON.Vector3(x*4,0, y*4);
    box.checkCollisions = true;
    box.freezeWorldMatrix();
    box.cullingStrategy = BABYLON.AbstractMesh.CULLINGSTRATEGY_OPTIMISTIC_INCLUSION_THEN_BSPHERE_ONLY;
    return box;
}

function createTrophy(){
    var material = new BABYLON.StandardMaterial("trophyMat", scene);
    material.diffuseColor = new BABYLON.Color3(1, 0.7, 0);
    material.emissiveColor = new BABYLON.Color3(1, 0.7, 0);

    var shape = [
        new BABYLON.Vector3(0.7, -1.6, 0),
        new BABYLON.Vector3(0.05, -1.5, 0),
        new BABYLON.Vector3(0.05, 0, 0),
        new BABYLON.Vector3(0.4, 0.5, 0),
        new BABYLON.Vector3(1, 1, 0),
        new BABYLON.Vector3(0, 0.6, 0)
    ];

    var trophy = BABYLON.MeshBuilder.CreateLathe("trophy", {shape: shape}, scene);
    trophy.material = material;
    trophy.scaling = new BABYLON.Vector3(0.3,0.3,0.3);
    
    var highlight = new BABYLON.HighlightLayer("hl1", scene);
    highlight.addMesh(trophy, new BABYLON.Color3(255,215,0));
    
    return trophy;
}

function createCrystalAnimation(){
    var keys = [
        {frame: 0, value:0},
        {frame: 50, value:2*Math.PI}
    ];
    var animation = new BABYLON.Animation("crystalAnimation", "rotation.y", 10, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    animation.setKeys(keys);
    return animation;
}

function createCrystalMaterial(){
    var material = new BABYLON.StandardMaterial("crystalMaterial", scene);
    material.diffuseColor = new BABYLON.Color3(0, 0.7, 1);
    material.emissiveColor = new BABYLON.Color3(0, 0.7, 1);
    return material;
}

function createCrystal(x, y, gameObjects){
    var crystal = BABYLON.MeshBuilder.CreatePolyhedron("crystal", {type: Math.floor(Math.random()*14), size: 0.3}, scene);
    crystal.material = gameObjects.crystalMaterial;
    crystal.position = new BABYLON.Vector3(x*4,-1, y*4);
    crystal.checkCollisions = false;
    
    var highlight = new BABYLON.HighlightLayer("hl2", scene);
    highlight.addMesh(crystal, new BABYLON.Color3(0,255,255));
    
    crystal.animations.push(gameObjects.crystalAnimation);
    scene.beginAnimation(crystal, 0, 100, true);
    if(DEBUG){
        crystal.showBoundingBox = true;
    }
    
    return crystal;
}

function checkCollisions(gameObjects){
    var camCenter = gameObjects.cameraCollisionMesh.getBoundingInfo().boundingBox.centerWorld;
    for (var i=0;i<gameObjects.traps.length;i++){
        if(gameObjects.camera.fadeTime > 0){
            break;
        }
        var distance = 3;
        if(gameObjects.traps[i].isSmall){
            distance = 1.8;
        }
        if(gameObjects.traps[i].isVisible && camCenter.subtract(gameObjects.traps[i].getBoundingInfo().boundingBox.centerWorld).length() < distance){
            if(gameObjects.traps[i].isWall){
                var isCamUnderTrap = (Math.abs(gameObjects.traps[i].getBoundingInfo().boundingBox.centerWorld.x - camCenter.x) < 1.9) && (Math.abs(gameObjects.traps[i].getBoundingInfo().boundingBox.centerWorld.z - camCenter.z) < 1.9);
                if(isCamUnderTrap && gameObjects.traps[i].animations[0].runtimeAnimations[0].currentFrame > 11){
                    gameObjects.camera.isAlive = false;
                }
            }
            else{
                gameObjects.camera.isAlive = false;
            }
        }
    }
    for (var i=0;i<gameObjects.crystals.length;i++){
        if(gameObjects.crystals[i].isVisible && camCenter.subtract(gameObjects.crystals[i].getBoundingInfo().boundingBox.centerWorld).length() < 1){
            gameObjects.crystals[i].take = true;
        }
    }
}

function createFloorMaterial(){
    material = new BABYLON.StandardMaterial("groundMat", scene);
    material.diffuseTexture = new BABYLON.Texture(assetsPath+"/rustytiles02_diff.jpg", scene);
    material.diffuseTexture.uScale  = 20;
    material.diffuseTexture.vScale  = 20;
    material.bumpTexture = new BABYLON.Texture(assetsPath+"/rustytiles02_norm.jpg", scene);
    material.bumpTexture.uScale  = 20;
    material.bumpTexture.vScale  = 20;
    material.specularTexture = new BABYLON.Texture(assetsPath+"/rustytiles02_spec.jpg", scene);
    material.specularTexture.uScale = 20;
    material.specularTexture.vScale = 20;
    material.freeze();
    return material;
}

function createFireMaterial(){
    var fireMaterial = new BABYLON.StandardMaterial("fontainSculptur2", scene);
    var fireTexture = new BABYLON.FireProceduralTexture("fire", 256, scene);
    fireMaterial.diffuseTexture = fireTexture;
    //fireMaterial.opacityTexture = fireTexture;
    return fireMaterial;
}

function createLavaPondTrap(x,y,gameObjects){
    var box = BABYLON.MeshBuilder.CreateBox("lavaPondTrap", {height: 0.2, width: 2, depth: 2}, scene);
    box.material = gameObjects.fireMaterial;
    var light = new BABYLON.PointLight("lavaPondTrapLight", new BABYLON.Vector3(0, 1, 0), scene);
    light.range = 4;
    light.diffuse = new BABYLON.Color3(1.0, 0, 0);
    light.parent = box;
    box.position = new BABYLON.Vector3(x*4-1,-2, y*4-1);
    box.checkCollisions = false;
    box.isSmall = true;

    if(DEBUG){
        box.showBoundingBox = true;
    }

    return box;
}

function createSmashTrap(x, y, gameObjects, useFire){
    var box = BABYLON.MeshBuilder.CreateBox("smashTrap", {height: 4, width: 4, depth: 4}, scene);
    if(useFire){
        box.material = gameObjects.fireMaterial;
        var light = new BABYLON.PointLight("smashTrapLight", new BABYLON.Vector3(0, -3, 0), scene);
        light.range = 4;
        light.diffuse = new BABYLON.Color3(1, 0, 0);
        light.parent = box;
    }
    else{
        box.material = gameObjects.wallMaterial;
        box.isWall = true;
    }
    box.position = new BABYLON.Vector3(x*4,0, y*4);
    box.checkCollisions = true;
    
    if(DEBUG){
        box.showBoundingBox = true;
    }
    
    box.animations.push(gameObjects.smashAnimation);
    scene.beginAnimation(box, 0, 100, true, Math.min(2.5 + level/2, 6));

    return box;
}

function createSmashAnimation(){
    var keys = [
        {frame: 0, value:0},
        {frame: 2, value:0},
        {frame: 10, value:3.9},
        {frame: 11, value:3.9},
        {frame: 12, value:0},
        {frame: 14, value:0}
    ];
    var animation = new BABYLON.Animation("smashAnimation", "position.y", 1, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    animation.setKeys(keys);
    return animation;
}

function disposeObject(obj){
    var children = obj.getChildren();
    if(children){
        for(var i=0;i<children.length;i++){
            disposeObject(children[i]);
        }
    }
    obj.dispose();
}

var MAZE_OBJECTS = {
    empty: 0,
    wall: 1,
    smashWall: 10,
    smashFire: 11,
    firePond: 12,
    crystal: 20,
    trophy: 21
}

function startLevel(settings, gameObjects){
    for(var i=0;i<gameObjects.walls.length;i++){
        disposeObject(gameObjects.walls[i]);
    }
    for(var i=0;i<gameObjects.traps.length;i++){
        disposeObject(gameObjects.traps[i]);
    }
    for(var i=0;i<gameObjects.crystals.length;i++){
        if(gameObjects.crystals[i]){
            disposeObject(gameObjects.crystals[i]);
        }
    }
    gameObjects.walls=[];
    gameObjects.traps=[];
    gameObjects.crystals=[];
    if(gameObjects.wallMesh){
        gameObjects.wallMesh.dispose();
    }
    gameObjects.wallMesh = createWallMeshPrototype(gameObjects);
    if(gameObjects.floor){
        gameObjects.floor.dispose();
    }
    if(gameObjects.ceiling){
        gameObjects.ceiling.dispose();
    }
    createFloorAndCeiling(gameObjects);
    var mazePoints = generateMaze(settings);
    gameObjects.mazePoints = [];
    for(var i=0;i<mazePoints.length;i++){
        var x = i%settings.width;
        var y = Math.floor(i/settings.width);
        if(mazePoints[i]){
            gameObjects.walls.push(createWall(x, y, gameObjects))
            gameObjects.mazePoints.push(MAZE_OBJECTS.wall);
        }
        else if(x != settings.startX || y != settings.startY){
            //if empty non starting space
            var rand = Math.random();
            if(rand > 0.95 && !isNeighbourSmashTrap(gameObjects.mazePoints, x, y, settings)){
                gameObjects.traps.push(createSmashTrap(x,y,gameObjects, true));
                gameObjects.mazePoints.push(MAZE_OBJECTS.smashFire);
            }
            else if(rand > 0.9 && !isNeighbourSmashTrap(gameObjects.mazePoints, x, y, settings)){
                gameObjects.traps.push(createSmashTrap(x,y,gameObjects, false));
                gameObjects.mazePoints.push(MAZE_OBJECTS.smashWall);
            }
            else if(rand > 0.7 && y !== (settings.height-2)){
                gameObjects.crystals.push(createCrystal(x,y,gameObjects));
                gameObjects.mazePoints.push(MAZE_OBJECTS.crystal);
            }
            else if(rand > 0.6){
                gameObjects.traps.push(createLavaPondTrap(x,y,gameObjects));
                gameObjects.mazePoints.push(MAZE_OBJECTS.firePond);
            }
            else{
                gameObjects.mazePoints.push(MAZE_OBJECTS.empty);
            }
        }
    }
    gameObjects.camera.position = new BABYLON.Vector3(settings.startX*4, 0, settings.startY*4);
    gameObjects.camera.isAlive = true;

    gameObjects.trophy.position = findTrophyPosition(mazePoints, settings);
}

function isNeighbourSmashTrap(mazePoints, x, y, settings){
    return (x>0 && isPointSmashTrap(mazePoints, x-1, y, settings)) || (y>0 && isPointSmashTrap(mazePoints, x, y-1, settings));
}
function isPointSmashTrap(mazePoints, x, y, settings){
    var index = x+y*settings.width;
    return index < mazePoints.length && (mazePoints[index] === MAZE_OBJECTS.smashFire || mazePoints[index] === MAZE_OBJECTS.smashWall);
}

function findTrophyPosition(mazePoints, settings){
    for(var i=0;i<settings.width;i++){
        if(settings.startX-i >=0 && !mazePoints[settings.startX-i+(settings.height-2)*settings.width]){
            return new BABYLON.Vector3((settings.startX-i)*4, -1, (settings.height-2)*4);
        }
        if(settings.startX+i < settings.width && !mazePoints[settings.startX+i+(settings.height-2)*settings.width]){
            return new BABYLON.Vector3((settings.startX+i)*4, -1, (settings.height-2)*4);
        }
    }
    return new BABYLON.Vector3(settings.startX*4, -1, settings.height*4-8);
}

function createCameraWithLight(gameObjects){
    var camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(0,0,0), scene);
    camera.minZ = 0.1;
    camera.maxZ = 35;
    camera.fov = 1.5;
    camera.fovMode = BABYLON.Camera.fovmode_horizontal_fixed;

    camera.attachControl(canvas, false);
    camera.keysUp.push(87);//W
    camera.keysLeft.push(65);//A
    camera.keysDown.push(83);//S
    camera.keysRight.push(68);//D
    
    camera.isAlive = true;
    camera.speed = 0.2;
    camera.ellipsoid = new BABYLON.Vector3(0.7, 0.7, 0.7);
    camera.fadeTime = 0;
    camera.fadeColor = new BABYLON.Color3(0,0,0);

    var sphere = BABYLON.MeshBuilder.CreateSphere("cameraCollisionMesh", {diameter: 0.1}, scene);
    sphere.position = camera.getFrontPosition(0.1);
    sphere.parent = camera;
    sphere.isVisible=true;
    sphere.checkCollisions = false;
    if(!DEBUG){
        camera.checkCollisions = true;
        camera.applyGravity = true;
    }
    else{
        sphere.showBoundingBox = true;
    }
    
    var light = new BABYLON.PointLight("cameraLight", new BABYLON.Vector3(0, 0, 0), scene);
    light.range = 15;
    light.diffuse = new BABYLON.Color3(1, 0.81, 0.62);
    light.parent = camera;

    //create light dimming animation
    var animation = new BABYLON.Animation("lightDimming", "intensity", 1, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var keys = [];
    for (var i = 0; i < 20; i++) {
        // light intensity between 0.4 and 1
        keys.push({
            frame: i,
            value: 0.4 + Math.random()*0.6
        });
    }
    animation.setKeys(keys);
    light.animations.push(animation);
    scene.beginAnimation(light, 0, keys.length, true, 4);
    
    setupControl(camera);
    
    gameObjects.lightDimmingAnimation = animation;

    gameObjects.camera = camera;
    gameObjects.cameraCollisionMesh = sphere;
    
    gameObjects.fadeAnimation = new BABYLON.Animation("protection", "fadeTime", 1, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
    gameObjects.fadeAnimation.setKeys([
        {frame: 0, value:1},
        {frame: 1, value:0}
    ]);
    camera.animations.push(gameObjects.fadeAnimation);
}

function setupControl(camera){
    // Create our own manager:
    var FreeCameraKeyboardRotateInput = function () {
            this._keys = [];
            this.keysLeft = [39];
            this.keysRight = [37];
            this.sensibility = 0.01;
    }

    // Hooking keyboard events
    FreeCameraKeyboardRotateInput.prototype.attachControl = function (element, noPreventDefault) {
        var _this = this;
        if (!this._onKeyDown) {
            element.tabIndex = 1;
            this._onKeyDown = function (evt) {
                if (_this.keysLeft.indexOf(evt.keyCode) !== -1 ||
                    _this.keysRight.indexOf(evt.keyCode) !== -1) {
                    var index = _this._keys.indexOf(evt.keyCode);
                    if (index === -1) {
                        _this._keys.push(evt.keyCode);
                    }
                    if (!noPreventDefault) {
                        evt.preventDefault();
                    }
                }
            };
            this._onKeyUp = function (evt) {
                if (_this.keysLeft.indexOf(evt.keyCode) !== -1 ||
                    _this.keysRight.indexOf(evt.keyCode) !== -1) {
                    var index = _this._keys.indexOf(evt.keyCode);
                    if (index >= 0) {
                        _this._keys.splice(index, 1);
                    }
                    if (!noPreventDefault) {
                        evt.preventDefault();
                    }
                }
            };

            element.addEventListener("keydown", this._onKeyDown, false);
            element.addEventListener("keyup", this._onKeyUp, false);
            BABYLON.Tools.RegisterTopRootEvents([
                { name: "blur", handler: this._onLostFocus }
            ]);
        }
    };

    // Unhook
    FreeCameraKeyboardRotateInput.prototype.detachControl = function (element) {
        if (this._onKeyDown) {
            element.removeEventListener("keydown", this._onKeyDown);
            element.removeEventListener("keyup", this._onKeyUp);
            BABYLON.Tools.UnregisterTopRootEvents([
                { name: "blur", handler: this._onLostFocus }
            ]);
            this._keys = [];
            this._onKeyDown = null;
            this._onKeyUp = null;
        }
    };

    // This function is called by the system on every frame
    FreeCameraKeyboardRotateInput.prototype.checkInputs = function () {
        if (this._onKeyDown) {
            var camera = this.camera;
            // Keyboard
            for (var index = 0; index < this._keys.length; index++) {
                var keyCode = this._keys[index];
                if (this.keysLeft.indexOf(keyCode) !== -1) {
                    camera.cameraRotation.y += this.sensibility;
                }
                else if (this.keysRight.indexOf(keyCode) !== -1) {
                    camera.cameraRotation.y -= this.sensibility;
                }
            }
        }
    };
    FreeCameraKeyboardRotateInput.prototype.getTypeName = function () {
        return "FreeCameraKeyboardRotateInput";
    };
    FreeCameraKeyboardRotateInput.prototype._onLostFocus = function (e) {
        this._keys = [];
    };
    FreeCameraKeyboardRotateInput.prototype.getSimpleName = function () {
        return "keyboardRotate";
    };
    camera.inputs.add(new FreeCameraKeyboardRotateInput());
}

function createFloorAndCeiling(gameObjects){
    //Ground
    var ground = BABYLON.MeshBuilder.CreatePlane("ground", {height: 20*4, width: 20*4, size: 20}, scene);
    ground.material = gameObjects.floorMaterial;
    ground.position = new BABYLON.Vector3(20*2, -2, 20*2);
    ground.rotation = new BABYLON.Vector3(Math.PI / 2, 0, 0);
    ground.checkCollisions = true;
    ground.freezeWorldMatrix();

    //ceiling
    var ceiling = BABYLON.MeshBuilder.CreatePlane("ground", {height: 20*4, width: 20*4, size: 20}, scene);
    ceiling.material = gameObjects.floorMaterial;
    ceiling.position = new BABYLON.Vector3(20*2, 2, 20*2);
    ceiling.rotation = new BABYLON.Vector3(-Math.PI / 2, 0, 0);
    ceiling.freezeWorldMatrix();

    gameObjects.floor = ground;
    gameObjects.ceiling = ceiling;
}

function createWallMaterial(){
    var wallMaterial = new BABYLON.StandardMaterial("Mat", scene);
    wallMaterial.diffuseTexture = new BABYLON.Texture(assetsPath+"/stonetiles_001_diff.jpg", scene);
    wallMaterial.diffuseTexture.hasAlpha = false;
    wallMaterial.bumpTexture = new BABYLON.Texture(assetsPath+"/stonetiles_001_norm.jpg", scene);
    wallMaterial.specularTexture = new BABYLON.Texture(assetsPath+"/stonetiles_001_spec.jpg", scene);
    wallMaterial.freeze();
    return wallMaterial;
}


/* ----------------------------------------------------- GENERATING MAZE ----------------------------------------------------- */
function generateMaze(settings)
{        
    function Point(x,y){
        this.X = x;
        this.Y = y;
    }
    function isThereAWall(point, walls){
        if(point.X < 0 || point.X >= settings.width) return false;
        if(point.Y < 0 || point.Y >= settings.height) return false;
        return walls[point.X + point.Y*settings.width];
    }
    function isValidNeighbour(origin, offset, walls){
        var candidate = new Point(origin.X+offset.X, origin.Y+offset.Y);
        if(!isThereAWall(candidate, walls)) return false;

        if(offset.Y == 0){
            return isThereAWall(new Point(candidate.X, candidate.Y-1), walls) &&
                isThereAWall(new Point(candidate.X, candidate.Y+1), walls) &&
                isThereAWall(new Point(candidate.X + offset.X, candidate.Y-1), walls) &&
                isThereAWall(new Point(candidate.X + offset.X, candidate.Y), walls) &&
                isThereAWall(new Point(candidate.X + offset.X, candidate.Y+1), walls);
        }
        else if(offset.X == 0){
            return isThereAWall(new Point(candidate.X-1, candidate.Y), walls) &&
                isThereAWall(new Point(candidate.X+1, candidate.Y), walls) &&
                isThereAWall(new Point(candidate.X-1, candidate.Y+offset.Y), walls) &&
                isThereAWall(new Point(candidate.X, candidate.Y+offset.Y), walls) &&
                isThereAWall(new Point(candidate.X+1, candidate.Y+offset.Y), walls);
        }
        return false;
    }

    if (settings.startX < 0)
    {
        settings.startX = Math.floor(Math.random() * settings.width);
    }
    if (settings.startY < 0)
    {
        settings.startY = Math.floor(Math.random() * settings.height);
    }

    //initialize walls
    var walls = [];
    for(var i=0; i < settings.width*settings.height;i++){
        walls.push(true);
    }
    walls[settings.startX + settings.startY*settings.width] = false;
    var stack = [];
    stack.push(new Point(settings.startX,settings.startY));
    var neighbours = [];

    while(stack.length > 0){
        var peek = stack[stack.length-1];
        neighbours = [];

        if(isValidNeighbour(peek, new Point(1,0), walls)){
            neighbours.push(new Point(peek.X+1,peek.Y));
        }
        if(isValidNeighbour(peek, new Point(-1,0), walls)){
            neighbours.push(new Point(peek.X-1,peek.Y));
        }
        if(isValidNeighbour(peek, new Point(0,1), walls)){
            neighbours.push(new Point(peek.X,peek.Y+1));
        }
        if(isValidNeighbour(peek, new Point(0,-1), walls)){
            neighbours.push(new Point(peek.X,peek.Y-1));
        }

        if (neighbours.length == 0)
        {
            stack.pop();
        }
        else
        {
            var next = neighbours[Math.floor(Math.random() * neighbours.length)];
            walls[next.X + next.Y*settings.width] = false;
            stack.push(next);
        }
    }
    return walls;
}