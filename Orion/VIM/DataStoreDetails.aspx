﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true" CodeFile="DataStoreDetails.aspx.cs" Inherits="Orion_VIM_DataStoreDetails" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" Runat="Server">
    <div class="titleTable">
        <h1>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewGroupTitle : ViewInfo.ViewHtmlTitle%>
            -
            <img style="border-width: 0px;" src="/Orion/StatusIcon.ashx?size=small&amp;entity=<%=SolarWinds.VIM.Web.StatusIcons.VimIconProvider.DataStoreEntityName%>&amp;status=<%=DataStore.Status%>" />
            <%= HttpUtility.HtmlEncode(this.DataStore.Name)%>
            <%= ViewInfo.IsSubView ? ViewInfo.ViewHtmlTitle : string.Empty%>
        </h1>

    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="VimMainContentPlaceHolder" Runat="Server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
</asp:Content>

