using System;
using SolarWinds.VIM.Web.Exceptions;
using SolarWinds.VIM.Web.Views;

public partial class Orion_VIM_Virtualization_VManSprawl : VimViewPage
{
    public override string ViewType
    {
        get 
        {
            return "VMan Sprawl"; 
        }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVMPHSprawlView";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    protected override void OnInit(EventArgs e)
    {
        if (!SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
        {
            throw new VManUnauthorizedAccessException();
        }
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
