﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.Web;

public partial class Orion_VIM_Virtualization_ClusterDetails : VimViewPage, IVimClusterProvider
{
    public override string ViewType
    {
        get { return "VIM Cluster Details"; }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMPH_ClusterDetails_VMwareClusterDetails";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected string PageTitle { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        // Set the header "X-UA-Compatible" value to "IE=8"
        ModulePatchHelper.SetIECompatibilityRendering(8);

        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    #region IVimClusterProvider Members

    public VimCluster Cluster
    {
        get { return (VimCluster)this.NetObject; }
    }

    #endregion
}
