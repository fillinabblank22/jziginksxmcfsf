﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMCustomProperties.ascx.cs" Inherits="Orion_VIM_Resources_VMDetails_VMCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="VIM_VirtualMachinesCustomProperties"/>
    </Content>
</orion:resourceWrapper>
