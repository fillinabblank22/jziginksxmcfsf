<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_VMWare_VMDetails" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>
<%@ Register TagPrefix="vim" Namespace="SolarWinds.VIM.Web.Controls" Assembly="SolarWinds.VIM.Web" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Register TagPrefix="orion" TagName="PollingWarning" Src="~/Orion/VIM/Controls/PollingWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true">
    <Content>
        <div style="margin:12px;">
            <asp:Label ID="ErrorLabel" runat="server" Visible="false" CssClass="error" 
                Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_111%>" Font-Bold=true ForeColor="Red" />
        </div>   
        <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" />
        <table class="NeedsZebraStripes sw-vim-entityDetails">
            <tr>
                <td class="PropertyHeader" style="width: 50%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_21 %></td>
                <td class="Property">
                    <div>
                        <a href="<%= GetVirtualizationEntityEditLink() %>" id="editProperties" class="vim-edit-icon">
                            <%= Resources.VIMWebContent.VIMWEBDATA_LV0_28 %>
                        </a>
                        <a href="<%=GetPerfStackLink() %>" class="vim-detail-perfstack-icon">
                            <%= Resources.VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
                        </a>
                        <% if (VirtualMachine.HostPlatform == VirtualizationPlatform.Nutanix) { %>
                            <a href="<%= GetNutanixPrismConsoleLink() %>" data-automation="openPrismWebConsole" class="vim-nutanix-icon-mono" target="_blank" rel="noopener noreferrer" onclick="return vim_hostDetails_restrictDemoMode()">
                                <%= Resources.VIMWebContent.EntityDetails_Management_OpenPrismElementWebConsole %>
                            </a>
                        <% } %>
                    </div>
                </td>
            </tr>
            <% if (VirtualMachine.HostID != 0)
                { %>
            <tr>
                <td class="PropertyHeader"><asp:Literal runat="server" ID="litHostTypePropertyHeader" /></td>
                <td class="Property">
                    <orion:EntityStatusIcon ID="hostStatusIcon" runat="server" WrapInBox="false" />
                    <asp:HyperLink runat="server" ID="hostLink" />
                </td>
            </tr>
            <% } %>
            <% if (VirtualMachine.HostPlatform == SolarWinds.VIM.Common.VirtualizationPlatform.Vmware)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VmDetails_RecourcePool %></td>
                    <td class="Property"><%= HttpUtility.HtmlEncode(VirtualMachine.ResourcePoolID.HasValue ? VirtualMachine.ResourcePoolName : Resources.VIMWebContent.VmDetails_RecourcePool_Root) %></td>
                </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.EntityDetails_PollingSource %></td>
                <td class="Property"><%= PollingWarning.LocalizedPollingSource %></td>
            </tr>
            <% if (PollingWarning.Visible)
               { %>
                <tr></tr>
                <tr>
                    <td style="padding-top:0" colspan="2">
                        <orion:PollingWarning ID="PollingWarning" runat="server" />
                    </td>
                </tr>
            <% } %>

            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_113 %></td>
                <td class="Property"><vim:VmGuestState runat="server" ID="guestState" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_114 %></td>
                <%= ManagedStatusHelper.GetVmStatusTD(VirtualMachine.Status, VirtualMachine.HostPlatform) %>
            </tr>
      
            <% if (this.IsMemoryUsageAvailable)
               { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_115 %></td>
                <td class="Property">
                    <%= Math.Round(VirtualMachine.MemoryUsage) %>% (<%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_11,litMemUsageAbs,litMemTotalAbs) %>)
                </td>
            </tr>
            <% }
                else
                { %>
                <% if (VirtualMachine.HostPlatform != VirtualizationPlatform.Nutanix)
                    { %>
                    <tr>
                       <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_16 %></td>
                       <td class="Property"><%= litMemTotalAbs %></td>
                    </tr>
                <% } %>
            <% } %>
            <% if (VirtualMachine.TotalStorageSizeUsed != null)
                { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VirtualMachineDetails_TotalStorageSizeUsed %></td>
                    <td class="Property"><%= FormatBytes(VirtualMachine.TotalStorageSizeUsed) %></td>
                </tr>
            <% } %>
            <% if (VirtualMachine.HostPlatform != VirtualizationPlatform.Nutanix)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_36 %></td>
                    <td class="Property"><%= FormatHelper.FormatNetworkSpeed(VirtualMachine.NetworkUsageRate) %></td>
                </tr>
                <% if (VirtualMachine.HostID != 0)
                   { %>
                    <tr>
                        <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_37 %></td>
                        <td class="Property"><asp:Literal runat="server" ID="litCPUUsage" Text="0"></asp:Literal>&nbsp;%</td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_38 %></td>
                        <td class="Property"><asp:Literal runat="server" ID="litMemoryUsage" Text="0"></asp:Literal>&nbsp;%</td>
                    </tr>
                    <tr>
                        <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_39 %></td>
                        <td class="Property"><asp:Literal runat="server" ID="litTrafficUsage" Text="0"></asp:Literal>&nbsp;%</td>
                    </tr>
                <% } %>
            <% } %>
            
            <% if (VirtualMachine.HostPlatform == VirtualizationPlatform.Nutanix)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VZ0_2 %></td>
                    <td class="Property"><%= FormatBytes(VirtualMachine.MemoryConfigured) %> </td>
                </tr>
            <% } %>

            <asp:PlaceHolder ID="phVmwareRelatedProperties" runat="server">
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_40 %></td>
                    <td class="Property">
                        <%= (!String.IsNullOrEmpty(VirtualMachine.GuestVmWareToolsVersion))
                                        ? HttpUtility.HtmlEncode(VirtualMachine.GuestVmWareToolsVersion)
                                        : Resources.VIMWebContent.VIMWEBCODE_VB0_31 %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_41 %></td>
                    <td class="Property"><asp:Literal runat="server" ID="VMwareToolsStatus" Text="0"></asp:Literal></td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_42 %></td>
                    <td class="Property">
                        <%= (!String.IsNullOrEmpty(VirtualMachine.ConfigFilePath))
                                            ? HttpUtility.HtmlEncode(VirtualMachine.ConfigFilePath)
                                            : Resources.VIMWebContent.VIMWEBCODE_VB0_31 %>
                    </td>
                </tr>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VB0_43%></td>
                    <td class="Property">
                        <%= (!String.IsNullOrEmpty(VirtualMachine.LogDirectoryPath))
                                            ? HttpUtility.HtmlEncode(VirtualMachine.LogDirectoryPath)
                                            : Resources.VIMWebContent.VIMWEBCODE_VB0_31%>
                    </td>
                </tr>
                <% if (SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
                   { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VZ0_1 %></td>
                    <td class="Property"><%= FormatBytes(VirtualMachine.SwappedMemoryUtilization) %> </td>
                </tr>
                <% } %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VZ0_2 %></td>
                    <td class="Property"><%= FormatBytes(VirtualMachine.MemoryConfigured) %> </td>
                </tr>
                <% if (SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
                   { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VZ0_3 %></td>
                    <td class="Property"><%= FormatBytes(VirtualMachine.BalloonMemLoad) %> </td>
                </tr>
                <% }
                   if (VirtualMachine.BootTime != null)
                   { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBCODE_VZ0_4 %></td>
                    <td class="Property"><%= Utils.FormatDateTime(VirtualMachine.BootTime.Value.ToLocalTime(), true) %> </td>
                </tr>
                <% }
                   if (VirtualMachine.OSUptime != null)
                   { %>
                    <tr>
                        <td class="PropertyHeader"><%= Resources.VIMWebContent.VM_OSUptime %></td>
                        <td class="Property"><%= FormatHelper.FormatTimeSpan(TimeSpan.FromSeconds(VirtualMachine.OSUptime.Value)) %> </td>
                    </tr>
                <% } %>

            </asp:PlaceHolder>
        </table>
        <asp:PlaceHolder runat="server" ID="InterfaceTablePlaceholder"></asp:PlaceHolder>

    </Content>
</orion:resourceWrapper>
