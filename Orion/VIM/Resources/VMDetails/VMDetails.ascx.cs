using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.Controls;
using SolarWinds.VIM.Web.Controls.AssetTree;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Models;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.StatusIcons;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VMWare_VMDetails : VimBaseResource
{
    #region PROPERTIES

    /// <summary> Identifies the help link fragment. </summary>
    public override String HelpLinkFragment
    {
        get { return ("OrionPHResourceVMDetails"); }
    }

    /// <summary> Identifies the collection of the required interfaces. </summary>
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get {return new[] {typeof (SolarWinds.Orion.NPM.Web.INodeProvider), typeof(IVimVirtualMachineProvider)}; }
    }

    public bool RunningOnESX { get; set; }

    public int VMStatus { get; set; }

    public VirtualMachineModel VirtualMachine { get; set; }

    protected NutanixDiscoveryMetadataModel? NutanixDiscoveryMetadata { get; private set; }

    protected bool IsMemoryUsageAvailable
    {
        get
        {
            if (this.VirtualMachine.HostPlatform == VirtualizationPlatform.Vmware)
            {
                return true;
            }
            else
            {
                return this.VirtualMachine.PowerState.StateType == VirtualMachineStateType.PoweredOn && this.VirtualMachine.MemoryUsage > 0;
            }
        }
    }

    #endregion

    protected string litMemUsageAbs = "0";
    protected string litMemTotalAbs = "0";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (SolarWinds.Orion.NPM.Web.NodeHelper.IsResourceUnderExternalNode(this) || DetectedViewType != ViewType.Guest)
        {
            this.Visible = false;
        }
        else
        {
            LoadData();
            phVmwareRelatedProperties.Visible = (VirtualMachine.HostPlatform == VirtualizationPlatform.Vmware);
        }
    }


    /// <summary>
    /// Identifes the default title.
    /// </summary>
    protected override String DefaultTitle
    {
        get
        {
            return (Resources.VIMWebContent.VIMWEBCODE_VB0_30);
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    /// <summary>
    /// Loads details of VM.
    /// </summary>
    private void LoadData()
    {
        VirtualMachine = (new VMDetailsDAL()).GetVMDetails(NodeId);

        if (VirtualMachine == null)
        {
            this.Visible = false;
            return;
        }

        hostStatusIcon.Entity = VimIconProvider.CombineWithPlatform(VimIconProvider.HostEntityName, VirtualMachine.HostPlatform);
        hostStatusIcon.Status = VirtualMachine.HostStatus;

        hostLink.Text = HttpUtility.HtmlEncode(VirtualMachine.HostName);
        if (!ManageNodeLink.IsUnmanagedNodeUrl(VirtualMachine.HostDetailsUrl))
        {
            hostLink.NavigateUrl = VirtualMachine.HostDetailsUrl;
        }
        else
        {
            hostLink.Attributes.Add("onclick", ManageNodeLink.GetOnClickCode(VirtualMachine.EngineID, VirtualMachine.HostIP, AssetTreeNodeType.Host, VirtualMachine.HostID, VirtualMachine.HostPlatform));
        }

        if (VirtualMachine.HostPlatform == VirtualizationPlatform.Vmware)
        {
            litHostTypePropertyHeader.Text = Resources.VIMWebContent.VIMWEBDATA_VB0_112;
        }
        else if (VirtualMachine.HostPlatform == VirtualizationPlatform.HyperV)
        {
            litHostTypePropertyHeader.Text = Resources.VIMWebContent.VIMWEBDATA_LB0_10;
        }
        else
        {
            if (VirtualMachine.HostPlatform == VirtualizationPlatform.Nutanix)
            {
                if (VirtualMachine.ClusterID > 0)
                {
                    NutanixDiscoveryMetadata = new ClusterDAL().GetNutanixDiscoveryMetadataForCluster(VirtualMachine.ClusterID);
                }
            }

            litHostTypePropertyHeader.Text = Resources.VIMWebContent.VIMWEBDATA_LB0_11;
        }

        //vmState.RenderedControlType = VMState.ControlType.Icon | VMState.ControlType.Text;
        //vmState.State = VirtualMachine.PowerState;

        guestState.RenderedControlType = VmGuestState.ControlType.Icon | VmGuestState.ControlType.Text;
        if ((VirtualMachine.GuestVmWareToolsStatus.StatusType == VMToolsStatusType.ToolsOk)
         || (VirtualMachine.GuestVmWareToolsStatus.StatusType == VMToolsStatusType.ToolsOld)
         || VirtualMachine.HostPlatform != VirtualizationPlatform.Vmware)
        {
            guestState.State = VirtualMachine.GuestState;
        }
        else
        {
            guestState.State = new GuestState(GuestStateType.VMToolsNotInstalled);
        }

        litMemUsageAbs = SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(VirtualMachine.MemoryUsageMB * 1024 * 1024, ByteDisplayValueFormat.Short);
        litMemTotalAbs = SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(VirtualMachine.MemoryConfigured, ByteDisplayValueFormat.Short);

        if ((VirtualMachine.HostTotalCPU > 0) && (VirtualMachine.HostCoreCount > 0))
        {
            float val = (float)((double) VirtualMachine.CPUShares/(VirtualMachine.HostTotalCPU*VirtualMachine.HostCoreCount)*100);
            litCPUUsage.Text = Math.Round(val).ToString();
        }
        if (VirtualMachine.HostTotalMemory > 0)
        {
            float val = (float)(((double) VirtualMachine.MemoryShares*1024*1024)/VirtualMachine.HostTotalMemory*100);
            litMemoryUsage.Text = Math.Round(val).ToString();
        }
        if (VirtualMachine.HostTotalNetworkCapacityKbps > 0)
        {
            float val = (float)((double) VirtualMachine.NetworkUsageRate/VirtualMachine.HostTotalNetworkCapacityKbps*100);
            litTrafficUsage.Text = Math.Round(val, 2).ToString();
        }

        switch (this.VirtualMachine.GuestVmWareToolsStatus.StatusType)
        {
            case VMToolsStatusType.Unknown: this.VMwareToolsStatus.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_31; break;
            case VMToolsStatusType.ToolsOld: this.VMwareToolsStatus.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_32; break;
            case VMToolsStatusType.ToolsOk: this.VMwareToolsStatus.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_33; break;
            case VMToolsStatusType.ToolsNotRunning: this.VMwareToolsStatus.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_34; break;
            case VMToolsStatusType.ToolsNotInstalled: this.VMwareToolsStatus.Text = Resources.VIMWebContent.VIMWEBCODE_VB0_35; break;
        }

        if (VirtualMachine.HostNodeID.HasValue && SolarWinds.Orion.Core.Common.ModulesCollector.IsModuleInstalled("NPM"))
        {
            AddHostInterfacesTable(VirtualMachine.HostNodeID.Value);
        }

        this.ErrorLabel.Visible = (VirtualMachine.HostPlatform == VirtualizationPlatform.Vmware) && VirtualMachine.ShowCredentialCheckLabel;

        PollingWarning.EntityId = VirtualMachine.ID;
        PollingWarning.PollingSource = VirtualMachine.PollingSource;
        PollingWarning.ViewType = ViewType.Guest;
    }

    private void AddHostInterfacesTable(int hostNodeId)
    {
        if (new RemoteFeatureManager().AllowInterfaces == false)
            return;

        if (hostNodeId <= 0)
            return;

        const string controlPath = "~/Orion/NPM/Controls/VMDetailsInterfaceList.ascx";

        if (!File.Exists(MapPath(controlPath)))
            return;

        Control control = LoadControl(controlPath);
        IPropertyProvider provider = control as IPropertyProvider;

        if ((control == null) || (provider == null))
            return;

        provider.Properties["NodeId"] = hostNodeId;

        InterfaceTablePlaceholder.Controls.Add(control);
    }

    protected string GetVirtualizationEntityEditLink()
    {
        ReferrerRedirectorBase.SetReturnUrl(ToAbsoluteUrl(VirtualMachine.DetailsUrl));
        return String.Format("/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?NetObject=VVM:{0}&ReturnTo={1}", NodeId, ReferrerRedirectorBase.GetReturnUrl());
    }

    protected string GetPerfStackLink()
    {
        return PerfStackLinkBuilder.GetVirtualMachineLink(VirtualMachine.ID, VirtualMachine.HostPlatform);
    }

    protected string GetNutanixPrismConsoleLink()
    {
        if (!NutanixDiscoveryMetadata.HasValue || OrionConfiguration.IsDemoServer)
        {
            return string.Empty;
        }

        return string.Format(
                "https://{0}:{1}/console/#page/vms/table/?action=details&actionTargetName={2}&actionTarget=vm",
                NutanixDiscoveryMetadata.Value.IpAddress, NutanixDiscoveryMetadata.Value.ApiPort, VirtualMachine.Name);
    }

    protected string GetNutanixPrismCentralLink()
    {
        if (OrionConfiguration.IsDemoServer || !NutanixDiscoveryMetadata.HasValue || string.IsNullOrEmpty(NutanixDiscoveryMetadata.Value.PrismCentralIpAddress))
        {
            return string.Empty;
        }

        return string.Format("https://{0}:{1}/console/#page/explore/s/{2}",
            NutanixDiscoveryMetadata.Value.PrismCentralIpAddress,
            NutanixDiscoveryMetadata.Value.PrismCentralApiPort,
            VirtualMachine.Name);
    }
}
