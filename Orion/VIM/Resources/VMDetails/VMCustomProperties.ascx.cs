﻿using System;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_VMDetails_VMCustomProperties : VimBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (SolarWinds.Orion.NPM.Web.NodeHelper.IsResourceUnderExternalNode(this) || DetectedViewType != ViewType.Guest)
        {
            this.Visible = false;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        customPropertyList.Resource = Resource;
        customPropertyList.NetObjectId = NodeId;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return new VirtualMachineDAL().GetCustomProperties(NodeId, displayProperties);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            if (Request.UrlReferrer != null)
            {
                redirectUrl = UrlHelper.ToSafeUrlParameter(Request.UrlReferrer.PathAndQuery);
            }
            return String.Format("/Orion/Admin/CPE/Default.aspx");
        };
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.Web_VMCustomProperties_DefaultTitle;
        }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}