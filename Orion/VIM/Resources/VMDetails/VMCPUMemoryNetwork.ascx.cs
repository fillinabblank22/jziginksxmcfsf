﻿using System;
using System.Data;
using System.Diagnostics;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Models;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.NetObjects;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_VMDetails_VMCPUMemoryNetwork : VimVirtualMachineBaseResource
{
    private string _gaugeStyle;
    private int _gaugeScale;
    private string _gaugeRangeMethod;
    private string _gaugeRangeMethod_NetworkUsageRate;
    private string _gaugeRangeValue;
    private DataTable _propertiesTable;

    protected string RedirectLink
    {
        get
        {
            return HttpContext.Current.Request.Url.AbsoluteUri;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _gaugeStyle = GetStringValue("Style", "Elegant Black");
        _gaugeScale = GetIntProperty("Scale", 100);
        _gaugeRangeMethod = GetStringValue("RangeMethod", "static");

        switch (_gaugeRangeMethod)
        {
            case "static":
                _gaugeRangeValue = GetIntProperty("StaticRange", 100).ToString();
                break;
            case "dynamic":
                _gaugeRangeValue = GetStringValue("DynamicRange", "10, 20, 40, 60, 100, 200, 400, 600, 1000");
                break;
            case "thresholds":
                _gaugeRangeValue = GetStringValue("ThresholdsRange", "1.3");
                break;
        }

        var vmNetObjectId = string.Format("{0}:{1}", VimVirtualMachine.Prefix, NodeId);
        var chartLink = string.Format("/Orion/DetachResource.aspx{0}", HttpContext.Current.Request.Url.Query);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder1,
            _gaugeScale,
            _gaugeStyle,
            "CPU",
            chartLink,
            vmNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_LM0_05,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_LM0_08);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder2,
            _gaugeScale,
            _gaugeStyle,
            "MEMORY",
            chartLink,
            vmNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_LM0_06,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_LM0_08);
        
        if (IsNutanixVm())
        {
            return;
        }

        GaugeHelper.CreateGauge(
            gaugePlaceHolder3,
            _gaugeScale,
            _gaugeStyle,
            "NETWORK",
            chartLink,
            vmNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_ZS0_02,
            "Radial",
            GetStringValue("RangeMethod", "dynamic"),
            0,
            GetStringValue("DynamicRange", "100, 200, 400, 600, 1000, 2000, 4000, 10000, 20000, 40000, 100000, 400000, 1000000"),
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_ZS0_03);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVM_VMCPUMemoryNetwork"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGauge.ascx"; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_ZS0_01;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }

    private bool IsNutanixVm()
    {
        var vmDal = new VirtualMachineDAL();
        VirtualMachineModel vmModel = vmDal.GetVMModelByVMId((int)EntityId);
        
        return vmModel.HostPlatform == VirtualizationPlatform.Nutanix;
    }
}