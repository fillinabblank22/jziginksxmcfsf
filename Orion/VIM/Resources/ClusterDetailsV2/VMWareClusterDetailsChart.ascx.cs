﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_ClusterDetailsV2_VMWareClusterDetailsChart : StandardChartResource
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    /// <summary>
    /// Gets a value indicating whether [allow customization].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [allow customization]; otherwise, <c>false</c>.
    /// </value>
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    /// <summary>
    /// Gets the net object prefix.
    /// </summary>
    protected override string NetObjectPrefix
    {
        get { return "VMC"; }
    }


    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected override IEnumerable<string> GetElementIdsForChart()
    {
        var vimProvider = GetInterfaceInstance<IVimClusterProvider>();
        if (vimProvider != null)
        {
            return new string[] { vimProvider.Cluster.Id.ToString() };
        }

        return new string[0];
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    /// <summary>
    /// Gets the default title for resource which will be displayed in the case that template will not override this.
    /// </summary>
    protected override string DefaultTitle
    {
        get { return notAvailable; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}