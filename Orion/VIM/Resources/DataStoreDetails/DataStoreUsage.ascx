﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataStoreUsage.ascx.cs" Inherits="Orion_VIM_Resources_DataStoreDetails_DataStoreUsage" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="vim" TagName="VSanDedupControl" Src="../../Controls/VSanDedupControl.ascx" %>
<%@ Register TagPrefix="vim" TagName="VSanCapacityControl" Src="../../Controls/VSanCapacityControl.ascx" %>

<vman:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="sw-vim-entityDetails NeedsZebraStripes">
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_45%></td>
                <td class="Property"><%= FormatBytes(DataStore.Model.Capacity)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_46%></td>
                <td class="Property"><%= FormatBytes(DataStore.Model.FreeSpace)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_56 %></td>
                <td class="Property"><%= FormatHelper.GetPercentValue(DataStore.Model.DiskOverallocatedRatio) %>&nbsp;%</td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_57 %></td>
                <td class="Property"><%= FormatBytes(DataStore.Model.ProvisionedSpace) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_LC0_55 %></td>
                <td class="Property"><%= FormatDate(DataStore.Model.DepletionDate)%></td>
            </tr>
            <vim:VSanDedupControl runat="server" ID="vsanControl"/>
            <vim:VSanCapacityControl runat="server" ID="vsanCapacityControl"/>
        </table>
    </Content>
</vman:resourceWrapper>