﻿using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_DatastoreCustomProperties : VManDataStoreBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        customPropertyList.Resource = Resource;
        customPropertyList.NetObjectId = NodeId;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return new DatastoreDAL().GetCustomProperties(NodeId, displayProperties);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            return "/Orion/Admin/CPE/Default.aspx";
        };
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.Web_DatastoreCustomProperties_DefaultTitle;
        }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}