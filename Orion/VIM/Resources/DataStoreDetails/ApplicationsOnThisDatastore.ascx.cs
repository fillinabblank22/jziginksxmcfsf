﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Web.VManResources;
using SolarWinds.VIM.Web.DAL.VMan.Datastore;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_ApplicationsOnThisDatastore : VManDataStoreBaseResource
{

    private ApplicationsOnThisDatastoreDAL _applicationsOnThisDatastoreDAL;

    private ApplicationsOnThisDatastoreDAL DAL
    {
        get { return _applicationsOnThisDatastoreDAL ?? (_applicationsOnThisDatastoreDAL = new ApplicationsOnThisDatastoreDAL()); }
    }


    private Boolean? _isAPMInstalled;
    public Boolean IsAPMInstalled 
    {
        get
        {
            if (!_isAPMInstalled.HasValue)
            {
                _isAPMInstalled = DAL.IsAPMInstalled();
            }
            return _isAPMInstalled.Value;
        }
    }


    public int PageSize
    {
        get;
        set;
    }

    public int PageNumber
    {
        get
        {
            int pageIntex = 0;
            int.TryParse(hfPageIndex.Value, out pageIntex);
            return pageIntex;
        }
        set
        {
            hfPageIndex.Value = value.ToString();
        }
    }

    public int ItemsCount
    {
        get;
        set;
    }

    public override void DataBind()
    {
        if (IsAPMInstalled && Wrapper.CanShowContent)
        {
            var table = DAL.GetPageDataTable(this.DataStore.Id, PageNumber + 1, PageSize);

            resourceTable.DataSource = table;
            resourceTable.DataBind();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // if integration is not enabled bail
        this.Visible = IsAPMInstalled && Wrapper.CanShowContent;

        this.resourceTable.ItemCreated += new RepeaterItemEventHandler(resourceTable_ItemCreated);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (IsAPMInstalled && Wrapper.CanShowContent)
        {
            var pageSize = Resource.Properties["MaxRecords"];
            PageSize = (!string.IsNullOrEmpty(pageSize)) ? int.Parse(pageSize) : 10;

            SetItemsCount();

            gridFooter.Visible = ItemsCount > PageSize;

            DataBind();
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_DF0_1; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHApplicationsOnDstore"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    private void SetItemsCount()
    {
        ItemsCount = DAL.GetCount(this.DataStore.Id);
    }


    protected void Grid_IndexChanging(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLowerInvariant())
        {
            case "next":
                SetItemsCount();
                if (PageNumber < (double)ItemsCount / PageSize - 1)
                    PageNumber += 1;
                break;
            case "prev":
                SetItemsCount();
                if (PageNumber > 0)
                    PageNumber -= 1;
                break;
        }

        DataBind();
    }

    void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    if (e.Item.DataItem is System.Data.DataRowView)
                    {
                        System.Data.DataRow row = (e.Item.DataItem as System.Data.DataRowView).Row;
                        if (row != null)
                        {
                            Literal litStatusDescription = e.Item.FindControl("litStatusDescription") as Literal;
                            Orion_Controls_EntityStatusIcon esiApplication = e.Item.FindControl("esiApplication") as Orion_Controls_EntityStatusIcon;
                            HyperLink hlApplication = e.Item.FindControl("hlApplication") as HyperLink;
                            Orion_Controls_EntityStatusIcon esiNode = e.Item.FindControl("esiNode") as Orion_Controls_EntityStatusIcon;
                            HyperLink hlNode = e.Item.FindControl("hlNode") as HyperLink;
                            
                            esiApplication.Status = Convert.ToInt32(row["Status"]);

                            String applicationUrl = GetViewLink(String.Format("AA:{0}", row["ApplicationID"]));
                            String applicationName = row["Name"] as String;
                            hlApplication.Text = applicationName;
                            hlApplication.NavigateUrl = applicationUrl;

                            String statusDescription = row["StatusDescription"] as String;
                            litStatusDescription.Text = statusDescription;

                            esiNode.Status = Convert.ToInt32(row["NodeStatus"]);

                            String nodeUrl = GetViewLink(String.Format("N:{0}", row["NodeID"]));
                            String nodeName = row["DisplayName"] as String;
                            hlNode.Text = nodeName;
                            hlNode.NavigateUrl = nodeUrl;
                        }
                    }
                    break;
                }
        }
    }



}
