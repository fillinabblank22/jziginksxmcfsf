﻿using System;
using System.Data;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Common.Extensions;
using SolarWinds.VIM.Web.DAL.VMan.Datastore;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_DatastoreIOPSAndLatency : VManDataStoreBaseResource
{
    private string _gaugeStyle;
    private int _gaugeScale;
    private string _gaugeRangeMethod;
    private string _gaugeRangeValue;
    private DataTable _iopsTable;

    protected string IOPSChartLink {
        get
        {
            return String.Format("/Orion/Charts/CustomChart.aspx?chartName=VManIOPSForDatastoreAndTopVMs&NetObject={0}&Period=Today&ChartTitle={1}", Request["NetObject"].UrlEncode(),
                                 HttpUtility.UrlEncode(Resources.VIMWebContent.VIMWEBCODE_LC0_34)).HtmlEncode();
        }
    }

    private DataTable IOPSInfo
    {
        get { return _iopsTable ?? (_iopsTable = new DatastoreIOPSAndLatencyDAL().GetIOPS(DataStore.Id)); }
    }

    protected string LatestIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Latest"], 1); } }

    protected string HourIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Hourly"], 1); } }
    
    protected string DayIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Daily"], 1); } }
    
    protected string MonthIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Monthly"], 1); } }

    protected void Page_Load(object sender, EventArgs e)
    {

        _gaugeStyle = GetStringValue("Style", "Elegant Black");
        _gaugeScale = GetIntProperty("Scale", 100);
        _gaugeRangeMethod = GetStringValue("RangeMethod", "dynamic");

        switch (_gaugeRangeMethod)
        {
            case "static":
                _gaugeRangeValue = GetIntProperty("StaticRange", 50).ToString();
                break;
            case "dynamic":
                _gaugeRangeValue = GetStringValue("DynamicRange", "10, 20, 40, 60, 100, 200, 400, 600, 1000");
                break;
            case "thresholds":
                _gaugeRangeValue = GetStringValue("ThresholdsRange", "1.3");
                break;
        }

        GaugeHelper.CreateGauge(
            gaugePlaceHolder,
            _gaugeScale,
            _gaugeStyle,
            "LATENCY",
            String.Format("/Orion/Charts/CustomChart.aspx?chartName=VManLatencyForDatastoreAndTopVMs&NetObject={0}&Period=Today&ChartTitle={1}", Request["NetObject"],
                          HttpUtility.UrlEncode(Resources.VIMWebContent.VIMWEBCODE_LC0_34)),
            DataStore.NetObjectID,
            Resources.VIMWebContent.VIMWEBCODE_LC0_27,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBDATA_VB0_40);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHIOPSLatency"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGauge.ascx"; }
    }

    protected override string DefaultTitle
    {
        get
        { 
            return Resources.VIMWebContent.VIMWEBCODE_LC0_28;
        }
    }

}