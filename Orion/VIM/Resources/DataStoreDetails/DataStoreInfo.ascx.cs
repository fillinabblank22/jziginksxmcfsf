﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_DataStoreInfo : VManDataStoreBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Load LUN info
        LoadLUN();

        // Load NAS info
        LoadNAS();
    }

    private void LoadLUN()
    {
        var lunDataTable = new LunDAL().GetDataStoreLuns(DataStore.Model.ID);

        if (lunDataTable.Rows.Count == 0)
        {
            lunTableRow.Visible = false;
        }
        else
        {
            foreach (DataRow row in lunDataTable.Rows)
            {
                var linkDAL = new StorageEntityLinkDAL();
                DataTable linkDetails = linkDAL.GetLUNLinkDetails((int)row["LunID"]);

                string link;

                if (linkDetails.Rows.Count == 0)
                {
                    link = EntityLinkHelper.GetLunLink(null, row["Name"] as string, VimManagedStatus.WithoutStatus, true);
                }
                else
                {
                    link = EntityLinkHelper.GetLunLink(linkDetails.Rows[0]["Link"] as string, row["Name"] as string,
                        (int) linkDetails.Rows[0]["Status"], true);
                }

                lunData.Controls.Add(new Label() { Text = link});
                lunData.Controls.Add(new System.Web.UI.LiteralControl("<br />"));
            }

            lunData.Controls.RemoveAt(lunData.Controls.Count - 1);  // remove the last line break
        }
    }

    private void LoadNAS()
    {
        var nasDataTable = new NasDAL().GetDataStoreNas(DataStore.Model.ID);

        if (nasDataTable.Rows.Count == 0)
        {
            nasTableRow.Visible = false;
        }
        else
        {
            foreach (DataRow row in nasDataTable.Rows)
            {
                var linkDAL = new StorageEntityLinkDAL();
                DataTable linkDetails = linkDAL.GetNASLinkDetails((int)row["NasID"]);

                string link;
                if (linkDetails.Rows.Count == 0)
                {
                    link =  EntityLinkHelper.GetNasLink(null, row["Name"] as string, VimManagedStatus.WithoutStatus, true);
                }
                else
                {
                    link = EntityLinkHelper.GetNasLink(linkDetails.Rows[0]["Link"] as string, row["Name"] as string, (int)linkDetails.Rows[0]["Status"], true);
                }

                nasData.Controls.Add(new Label() { Text = link });
                nasData.Controls.Add(new System.Web.UI.LiteralControl("<br />"));
            }

            nasData.Controls.RemoveAt(nasData.Controls.Count - 1);  // remove the last line break
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHDstoreInfo"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_23; }
    }

    protected string GetVirtualizationEntityEditLink()
    {
        ReferrerRedirectorBase.SetReturnUrl(GetViewLinkAbsolute(String.Format("VMS:{0}", NodeId)));
        return String.Format("/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?NetObject=VMS:{0}&ReturnTo={1}", NodeId, ReferrerRedirectorBase.GetReturnUrl());
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected string GetPerfStackLink()
    {
        return PerfStackLinkBuilder.GetDataStoreLink(DataStore.Id);
    }
}
