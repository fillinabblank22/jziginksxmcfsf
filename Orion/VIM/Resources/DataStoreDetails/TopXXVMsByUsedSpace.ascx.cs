﻿using System;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL.VMan.Datastore;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_TopXXVMsByUsedSpace : VManDataStoreTopXXResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;

        try
        {
            var dal = new TopXXVMsByUsedSpaceDAL();
            table = dal.GetTopXXVMsByUsedSpace(DataStore.Id, MaxRecords);
        }
        catch (SwisQueryException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_29; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHTopXXVMUsedSpace"; }
    }
}