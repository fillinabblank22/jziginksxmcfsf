﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataStoreInfo.ascx.cs" Inherits="Orion_VIM_Resources_DataStoreDetails_DataStoreInfo" %>
<orion:Include runat="server" Module="VIM" File="VIM.css" />
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="sw-vman-infotable NeedsZebraStripes">
            <tr>                
                <th>
                    <%= Resources.VIMWebContent.VIMWEBDATA_AK0_21%>
                </th>
                <td>                
                    <span>
                        <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                        <a href="<%=GetVirtualizationEntityEditLink() %>" id="editProperties" >
                              <%= Resources.VIMWebContent.VIMWEBDATA_LV0_37 %> 
                        </a>
                        <a href="<%=GetPerfStackLink() %>" class="vim-perfstack-icon vim-perfstack-icon-position-on-top">
                            <%= Resources.VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
                        </a>
                    </span>             
                </td>
            </tr>
            <tr>
                <th><%= Resources.CoreWebContent.WEBDATA_AK0_47%></th>
                <td><%= DataStore.Model.Accessible.HasValue && DataStore.Model.Accessible.Value ? Resources.VIMWebContent.VIMWEBCODE_LC0_10 : Resources.VIMWebContent.VIMWEBCODE_LC0_11%></td>
            </tr>
            <tr>
                <th><%= Resources.CoreWebContent.WEBDATA_AK0_379%></th>
                <td><%= HttpUtility.HtmlEncode(DataStore.Model.Type) %></td>
            </tr>
            <tr>
                <th><%= Resources.CoreWebContent.WEBDATA_VB0_128%></th>
                <td><%= HttpUtility.HtmlEncode(DataStore.Model.Location) %></td>
            </tr>
            <tr runat="server" ID="nasTableRow">
                <th><%= Resources.VIMWebContent.VIMWEBDATA_LC0_70%></th>
                <td><asp:PlaceHolder runat="server" ID="nasData" /></td>
            </tr>
            <tr runat="server" ID="lunTableRow">
                <th><%= Resources.VIMWebContent.VIMWEBDATA_LC0_54%></th>
                <td><asp:PlaceHolder runat="server" ID="lunData" /></td>
            </tr>
        </table>
    </Content>
</vman:resourceWrapper>