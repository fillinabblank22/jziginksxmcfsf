﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatastoreCustomProperties.ascx.cs" Inherits="Orion_VIM_Resources_DataStoreDetails_DatastoreCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="VIM_DataStoresCustomProperties"/>
    </Content>
</orion:resourceWrapper>
