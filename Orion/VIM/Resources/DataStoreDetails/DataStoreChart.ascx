﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataStoreChart.ascx.cs" Inherits="Orion_VIM_Resources_DataStoreDetails_DataStoreChart" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</vman:resourceWrapper>