﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_DataStoreChart : StandardChartResource, IResourceIsInternal
{
    private VManDataStore _dataStore;

    protected void Page_Init(object sender, EventArgs e)
    {
        _dataStore = GetInterfaceInstance<IVManDataStoreProvider>().DataStore;

        if (Resource?.Properties != null && _dataStore?.Model != null)
        {
            var unsupportedPlatforms = PlatformHelper.GetUnsupportedPlatformsFromResourceProperties(Resource.Properties);
            if (unsupportedPlatforms.Contains(_dataStore.Model.Platform))
            {
                Visible = false;
            }
        }

        HandleInit(WrapperContents);
    }

    protected override string DefaultTitle
    {
        get { return notAvailable; }
    }

    protected override IEnumerable<string> GetElementIdsForChart()
    {
        if (_dataStore != null)
        {
            return new[] {_dataStore.Id.ToString()};
        }

        return Enumerable.Empty<string>();
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return VManDataStore.Prefix; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] {typeof (IVManDataStoreProvider)}; }
    }

    public bool IsInternal { get { return true; } }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}