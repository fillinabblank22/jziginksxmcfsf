﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_DataStoreDetails_DataStoreUsage : VManDataStoreBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DatastoreDAL _datastoreDAL = new DatastoreDAL();
        long? vsanId = _datastoreDAL.GetVSanId(DataStore.Model.ID);
        vsanControl.VSanId = vsanId;
        vsanCapacityControl.VSanId = vsanId;
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHDstoreUsage"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_24; }
    }
}