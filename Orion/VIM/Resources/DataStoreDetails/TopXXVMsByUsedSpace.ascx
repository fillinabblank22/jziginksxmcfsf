﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMsByUsedSpace.ascx.cs" Inherits="Orion_VIM_Resources_DataStoreDetails_TopXXVMsByUsedSpace" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes"  border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst vimReportHeader"><%#Resources.CoreWebContent.NetworkObjectType_Node%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_58%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_45%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_47%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property"><%#EntityLinkHelper.GetVMEntityLink(Eval("DetailsUrl"), Eval("Name"), 
                                                Eval("Status"), Eval("PlatformID"), true, Container.DataItem)%>
                    </td> 
                    <td class="Property"><%#FormatBytes(Eval("VolumeUsedSpace"))%></td>
                    <td class="Property"><%#FormatBytes(Eval("VolumeSummaryCapacity"))%></td>
                    <td class="Property"><%#FormatDate(Eval("VolumeSummaryCapacityDepletionDate"))%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</vman:resourceWrapper>
