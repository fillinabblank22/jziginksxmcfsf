﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_VIM_Resources_TopXX_VMHostsTopXXEditPage : System.Web.UI.Page
{
    protected const string HelpFragment = "OrionFilters_IVIM";
}