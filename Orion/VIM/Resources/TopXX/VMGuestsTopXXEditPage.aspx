<%@ Page Language="C#" MasterPageFile="~/Orion/VIM/TopXXEditPage.master"  AutoEventWireup="true" CodeFile="VMGuestsTopXXEditPage.aspx.cs" Inherits="Orion_VIM_Resources_TopXX_VMGuestsTopXXEditPage" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Base.Contract.Constants" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SampleFilters" Runat="Server">
<%=String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_10, String.Format("<asp:HyperLink ID=\"HelpLink\" NavigateUrl=\"{0}\" runat=\"server\">", HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, HelpFragment)), "</asp:HyperLink>")%>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MaxCountDisplay" Runat="Server">
<b><%= Resources.VIMWebContent.VIMWEBDATA_VB0_59%></b><br />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FilterDevices" Runat="Server">
 <b><%= Resources.VIMWebContent.VIMWEBDATA_VB0_60%></b><br />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FilterText" Runat="Server">
  <%= Resources.VIMWebContent.VIMWEBDATA_VB0_61%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="SampleProperties" Runat="Server">
  <%= Resources.VIMWebContent.VIMWEBDATA_VB0_62%><br />
</asp:Content>
     
<asp:Content ID="Content6" ContentPlaceHolderID="ListOfProperties" Runat="Server">
 <b><%= Resources.VIMWebContent.VIMWEBDATA_VB0_63%></b>
</asp:Content>