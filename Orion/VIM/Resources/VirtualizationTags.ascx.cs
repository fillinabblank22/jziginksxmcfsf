﻿using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Resources;
using System;
using SolarWinds.Logging;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Models;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;

public partial class Orion_VIM_Resources_VirtualizationTags : VimBaseResource
{
    private const string Swql = @"SELECT Name, Description FROM Orion.VIM.Tags t ";
    private static readonly Log log = new Log();

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    private PollingSource _pollingSource;
    private VirtualizationPlatform _virtualizationPlatform;

    protected void Page_Load(object sender, EventArgs e)
    {
        string entityCondition;

        try
        {
            entityCondition = GetEntityCondition();
        }
        catch (Exception ex)
        {
            log.ErrorFormat("Could not load condition for entity type: {0}, widget will be hidden.", DetectedViewType, ex);
            entityCondition = null;
        }

        if (string.IsNullOrEmpty(entityCondition)
            || _pollingSource != PollingSource.LicenseBased
            || _virtualizationPlatform != VirtualizationPlatform.Vmware
            || !IsLicensed())
        {
            Visible = false;
            return;
        }

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = Swql + entityCondition;
        CustomTable.OrderBy = OrderBy;
    }

    private string GetEntityCondition()
    {
        switch (DetectedViewType)
        {
            case ViewType.vCenter:
                var vCenterDal = new VCenterDAL();
                VCenterModel vCenter = vCenterDal.GetVCenterModelById(NodeId);
                _pollingSource = vCenter.PollingSource;
                _virtualizationPlatform = VirtualizationPlatform.Vmware;
                return string.Format(@"WHERE t.VCenter.VCenterID = {0}", vCenter.ID);
            case ViewType.DataCenter:
                VimDataCenter dataCenter = GetInterfaceInstance<IVimDataCenterProvider>().DataCenter;
                _pollingSource = dataCenter.Model.PollingSource;
                _virtualizationPlatform = VirtualizationPlatform.Vmware;
                return string.Format(@"WHERE t.DataCenters.DataCenterID = {0}", dataCenter.Id);

            case ViewType.Cluster:
                VimCluster cluster = GetInterfaceInstance<IVimClusterProvider>().Cluster;
                _pollingSource = cluster.Model.PollingSource;
                _virtualizationPlatform = cluster.Model.Platform;
                return string.Format(@"WHERE t.Clusters.ClusterID = {0}", cluster.Id);

            case ViewType.Host:
                var hostDal = new HostDAL();
                HostModel host = hostDal.GetHostModelByHostId(NodeId);
                _pollingSource = host.PollingSource;
                _virtualizationPlatform = host.Platform;
                return string.Format(@"WHERE t.Hosts.HostID = {0}", host.ID);

            case ViewType.Guest:
                var vmDal = new VirtualMachineDAL();
                VirtualMachineModel vm = vmDal.GetVMModelByVMId(NodeId);
                _pollingSource = vm.PollingSource;
                _virtualizationPlatform = vm.HostPlatform;
                return string.Format(@"WHERE t.VirtualMachines.VirtualMachineID = {0}", vm.ID);

            case ViewType.VManDatastore:
                VManDataStore dataStore = GetInterfaceInstance<IVManDataStoreProvider>().DataStore;
                _pollingSource = PollingSource.LicenseBased;
                _virtualizationPlatform = (VirtualizationPlatform)dataStore.Model.Platform;
                return string.Format(@"WHERE t.Datastores.DataStoreID = {0}", dataStore.Id);

            default:
                _log.WarnFormat("Following view type is unsupported for virtualization tags widgeted {0}. Widget is hidden.", DetectedViewType);
                _pollingSource = PollingSource.Vanilla;
                _virtualizationPlatform = VirtualizationPlatform.Unknown;
                return string.Empty;
        }
    }

    public string OrderBy
    {
        get { return "Name ASC"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIM_VMwareTagsList"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VirtualizationTagsResource_Title; }
    }
}