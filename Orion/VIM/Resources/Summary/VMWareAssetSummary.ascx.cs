using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Virtualization_Asset_Summary : PluggableResourceBase
{
    // Note: This resource was previously plugable for VRM purpose. We removed this plugability when we reworked it for Hyper-V.
    //       Please look at Perforce history when you would like to implement plugability at the future.

    public VmwareAssetSummary VMwareAssetSummary { get; set; }
    public NutanixAssetSummary NutanixAssetSummary { get; set; }
    public AssetSummary HyperVAssetSummary { get; set; }
    public AssetSummary OverallAssetSummary { get; set; }
    public int NoOfVsanClusters { get; set; }

    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public override string ResourceName
    {
        get { return "VMWareAssetSummary"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditControls/AssetTreeSummaryEdit.ascx"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        divVMware.Visible = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.VMwareEnabledKey, true);
        divHyperV.Visible = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.HyperVEnabledKey, true);
        divNutanix.Visible = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.NutanixEnabledKey, true);

        try
        {
            this.VMwareAssetSummary = (new VmwareDAL()).LoadSummaryData();
        }
        catch (Exception ex)
        {
            log.Error("Unable to load VMware asset summary data from database.", ex);
            this.VMwareAssetSummary = new VmwareAssetSummary();
        }

        try
        {
            HyperVAssetSummary = (new HyperVDAL()).LoadSummaryData();
        }
        catch (Exception ex)
        {
            log.Error("Unable to load VMware asset summary data from database.", ex);
            HyperVAssetSummary = new AssetSummary();
        }

        try
        {
            NutanixAssetSummary = new NutanixDAL().LoadSummaryData();
        }
        catch (Exception ex)
        {
            log.Error("Unable to load Nutanix Asset Summary data from database.", ex);
        }

        // We need to load both platforms to get overall summary
        this.OverallAssetSummary = new AssetSummary()
        {
            NoOfVMHosts = this.VMwareAssetSummary.NoOfVMHosts + this.HyperVAssetSummary.NoOfVMHosts + this.NutanixAssetSummary.NoOfAhvHosts,
            NoOfTotalVMs = this.VMwareAssetSummary.NoOfTotalVMs + this.HyperVAssetSummary.NoOfTotalVMs + this.NutanixAssetSummary.NoOfTotalAhvVms,
            NoOfRunningVMs = this.VMwareAssetSummary.NoOfRunningVMs + this.HyperVAssetSummary.NoOfRunningVMs + this.NutanixAssetSummary.NoOfRunningAhvVMs,
            NoOfTotalPhysicalCPUCores = this.VMwareAssetSummary.NoOfTotalPhysicalCPUCores + this.HyperVAssetSummary.NoOfTotalPhysicalCPUCores,
            TotalRAM = this.VMwareAssetSummary.TotalRAM + this.HyperVAssetSummary.TotalRAM,
            LastPollInMin = IdentifyOverallLastPoll()
        };

        NoOfVsanClusters = 0;
        try
        {
            if (LicenseManager.Instance.IsVimFullyLicensed())
            {
                NoOfVsanClusters = (new ClusterDAL()).GetNumberOfVsanClusters();
            }
        }
        catch (Exception ex)
        {
            log.Error("Unable to load vSAN data from database.", ex);
            NoOfVsanClusters = 0;
        }

        SolarWinds.Orion.Web.OrionInclude.ModuleFile("VIM", "VIM.css");
        SolarWinds.Orion.Web.OrionInclude.ModuleFile("VIM", "/js/AssetSummary.js");
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_22; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Virtualization_VMwareAssetSummary"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    private int IdentifyOverallLastPoll()
    {
        var overallLastPoll = Math.Min(VMwareAssetSummary.LastPollInMin,
            Math.Min(HyperVAssetSummary.LastPollInMin, NutanixAssetSummary.LastPollInMin));

        if (VMwareAssetSummary.NoOfVMHosts + VMwareAssetSummary.NoOfVirtualCenters != 0)
        {
            overallLastPoll = VMwareAssetSummary.LastPollInMin;
        }

        if (HyperVAssetSummary.NoOfVMHosts != 0 &&
            HyperVAssetSummary.LastPollInMin < overallLastPoll)
        {
            overallLastPoll = HyperVAssetSummary.LastPollInMin;
        }

        if (NutanixAssetSummary.NoOfNutanixClusters != 0 &&
            NutanixAssetSummary.LastPollInMin < overallLastPoll)
        {
            overallLastPoll = NutanixAssetSummary.LastPollInMin;
        }

        return overallLastPoll;
    }

    protected string FormatLastPollValueLabel(int minutesSinceLastPoll)
    {
        var lastPollLabelFormat = minutesSinceLastPoll == 1
            ? Resources.VIMWebContent.VIMWEBCODE_VB0_27
            : Resources.VIMWebContent.VIMWEBCODE_VB0_26;

        return string.Format(lastPollLabelFormat, minutesSinceLastPoll);
    }
}

