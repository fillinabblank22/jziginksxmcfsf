﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_VIM_Resources_Summary_GettingStarted" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Constants" %>

<orion:Include runat="server" Module="VIM" File="VIM.css"/>
<orion:Include runat="server" Module="VIM" File="Resources/GettingStarted.js" />

<orion:resourceWrapper runat="server" ID="Wrapper" ShowHeaderBar="False">
    <Content>
        <div class="vim-gettingstarted">
            <div class="vim-gettingstarted-header HeaderBar">
                <div class="vim-gettingstarted-remove">
                    <a ID="RemoveResourceLink" runat="server"><%= VIMWebContent.GettingStarted_RemoveResource %></a>
                    <orion:LocalizableButton runat="server" ID="RemoveResourceButton" CssClass="hidden" OnClick="OnRemoveResourceButtonClicked" DisplayType="Secondary"/>
                </div>
                <div class="vim-gettingstarted-icon left"></div>
                <h1><%= VIMWebContent.GettingStarted_ResourceTitle %></h1>
            </div>
            <div class="vim-gettingstarted-doc">
                <% if (DetectedViewType != ViewType.OrionSummary) { %>
                    <div class="vim-gettingstarted-doc-item">
                        <span><%= GetDocumentationText() %></span>
                    </div>
                <% } %>
            </div>
            <div class="vim-gettingstarted-description">
                <div class="vim-gettingstarted-doc-item">
                    <div class="vim-gettingstarted-description-icon"></div>
                    <h4><%= VIMWebContent.GettingStarted_DescriptionTitle %></h4>
                    <p><%= VIMWebContent.GettingStarted_Description %></p>
                </div>
            </div>
            <div class="vim-gettingstarted-button">
                <orion:LocalizableButtonLink runat="server" ID="addNodeButton" NavigateUrl="<%# Links.AddNode %>" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, GettingStarted_AddNodeButton %>"/>
            </div>
            <% if (DetectedViewType != ViewType.OrionSummary) { %>
                <div class="vim-gettingstarted-content">
                    <div id="RecommendationsRow" class="vim-gettingstarted-row-even" onclick="SW.VIM.GettingStarted.onExpanderChanged(this);">
                        <img class="vim-gettingstarted-expander-icon" src="/Orion/VIM/images/GettingStarted/arrow-collapsed.png" alt="[+]" />
                        <span class="vim-gettingstarted-gear-icon"></span>
                        <span class="vim-gettingstarted-label"><%= VIMWebContent.GettingStarted_ConfigureRecommendations %></span>
                        <span class="vim-gettingstarted-step-counter <%= RecommendationCounterCssClass %>"><%= RecommendationStepsCounter  %>/<%= RecommendationStepsCounterMax  %><span> </span><%= VIMWebContent.GettingStarted_StepsCompleted %></span>
                    </div>
                    <div id="Drawer_RecommendationsRow" class="vim-gettingstarted-drawer">
                        <div class="vim-gettingstarted-drawer-row">
                            <table><tr><td>
                                    <div id="ManageStrategies_RowData" class="vim-gettingstarted-drawer-row-data">
                                        <table>
                                            <tr>
                                                <td><p><%= VIMWebContent.GettingStarted_ReviewStrategies %></p></td>
                                                <td class="vim-gettingstarted-manage-btn"><orion:LocalizableButton ID="ManageStrategies" CssClass="sw-btn-primary" runat="server" OnClientClick="return controler.OnStepButtonClicked('ManageStrategies')" Text="<%$ Resources: VIMWebContent, GettingStarted_ManageStrategies %>"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td class="vim-gettingstarted-info">
                                    <div class="vim-gettingstarted-info-tooltip">
                                        <span class="icon"></span>
                                        <span class="vim-gettingstarted-tooltip-text"><%= VIMWebContent.GettingStarted_StrategiesTooltip %></span>
                                    </div>
                                </td></tr>
                            </table>
                        </div>
                        <div class="vim-gettingstarted-drawer-row">
                            <table><tr><td>
                                    <div id="ManageRecommendations_RowData" class="vim-gettingstarted-drawer-row-data vim-gettingstarted-drawer-row-data-last">
                                        <table>
                                            <tr>
                                                <td><p><%= VIMWebContent.GettingStarted_Recommendations %></p></td>
                                                <td class="vim-gettingstarted-manage-btn"><orion:LocalizableButton ID="ManageRecommendations" CssClass="sw-btn-primary" runat="server" OnClientClick="return controler.OnStepButtonClicked('ManageRecommendations')" Text="<%$ Resources: VIMWebContent, GettingStarted_ManageRecommendations %>" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td class="vim-gettingstarted-info">
                                    <div class="vim-gettingstarted-info-tooltip">
                                        <span class="icon"></span>
                                        <span class="vim-gettingstarted-tooltip-text"><%= VIMWebContent.GettingStarted_DefineRecommendationsTooltip %></span>
                                    </div>
                                </td></tr></table>
                        </div>
                    </div>
                    <div id="CapacityPlanningRow" class="vim-gettingstarted-row-even" onclick="SW.VIM.GettingStarted.onExpanderChanged(this);">
                        <img class="vim-gettingstarted-expander-icon" src="/Orion/VIM/images/GettingStarted/arrow-collapsed.png" alt="[+]" />
                        <span class="vim-gettingstarted-gear-icon"></span>
                        <span class="vim-gettingstarted-label"><%=  VIMWebContent.GettingStarted_CapacityPlanning %></span>
                        <span class="vim-gettingstarted-step-counter <%= CapacityPlanningCounterCssClass %>"><%= CapacityPlanningStepsCounter  %>/<%= CapacityPlanningStepsCounterMax  %><span> </span><%= VIMWebContent.GettingStarted_StepsCompleted %></span>
                    </div>
                    <div id="Drawer_CapacityPlanningRow" class="vim-gettingstarted-drawer">
                        <div class="vim-gettingstarted-drawer-row">
                            <table><tr><td>
                                    <div id="CapacityPlanningFirstReport_RowData" class="vim-gettingstarted-drawer-row-data">
                                        <table>
                                            <tr>
                                                <td><p><%= VIMWebContent.GettingStarted_CreateYourFirstReport %></p></td>
                                                <td class="vim-gettingstarted-manage-btn"><orion:LocalizableButton ID="CapacityPlanning_CreateReport" CssClass="sw-btn-primary" runat="server" OnClientClick="return controler.OnStepButtonClicked('CapacityPlanning_CreateReport')" Text="<%$ Resources: VIMWebContent, GettingStarted_Btn_CapacityPlanning %>"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td class="vim-gettingstarted-info">
                                    <div class="vim-gettingstarted-info-tooltip">
                                        <span class="icon"></span>
                                        <span class="vim-gettingstarted-tooltip-text"><%= VIMWebContent.GettingStarted_Tooltip_CreateYourFirstReport %></span>
                                    </div>
                                </td></tr>
                            </table>
                        </div>
                        <div class="vim-gettingstarted-drawer-row">
                            <table><tr><td>
                                    <div id="CapacityPlanningViewReports_RowData" class="vim-gettingstarted-drawer-row-data vim-gettingstarted-drawer-row-data-last">
                                        <table>
                                            <tr>
                                                <td><p><%= VIMWebContent.GettingStarted_ViewReports %></p></td>
                                                <td class="vim-gettingstarted-manage-btn"><orion:LocalizableButton ID="CapacityPlanning_ViewReports" CssClass="sw-btn-primary" runat="server" OnClientClick="return controler.OnStepButtonClicked('CapacityPlanning_ViewReports')" Text="<%$ Resources: VIMWebContent, GettingStarted_Btn_CapacityPlanning %>" /></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td class="vim-gettingstarted-info">
                                    <div class="vim-gettingstarted-info-tooltip">
                                        <span class="icon"></span>
                                        <span class="vim-gettingstarted-tooltip-text"><%= VIMWebContent.GettingStarted_Tooltip_ViewReports %></span>
                                    </div>
                                </td></tr>
                            </table>
                        </div>
                    </div>
                </div>
            <% } %>
        </div>
        <script type="text/javascript">
            var controler = new SW.VIM.GettingStarted();
            $(function() {
                controler.init(<%= StepsToJavascriptArray() %>);
            });
        </script>
    </Content>
</orion:resourceWrapper>