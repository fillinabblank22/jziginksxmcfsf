VIM = {};

VIM.AssetTree = {

    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        contentsDiv.html(result);
        contentsDiv.slideDown('fast');
        VIM.AssetTree.AutoClickLinks(contentsDiv);
    },

    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = "@{R=VIM.Strings;K=VIMWEBJS_VB0_6;E=js}" + error.get_message();
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Apps" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded        
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },


    Click: function (rootId, resourceId, keys) {
        return VIM.AssetTree.HandleClick(rootId, function (contentDiv) {
            VMwareAssetTree.GetTreeSection(resourceId, rootId, keys, 0, VIM.AssetTree.Succeeded, VIM.AssetTree.Failed, contentDiv);
        }, function () {
            VMwareAssetTree.CollapseTreeSection(resourceId, keys);
        });
    },

    Components: function (rootId, resourceId, applicationId) {
        return VIM.AssetTree.HandleClick(rootId, function (contentDiv) {
            AssetTree.GetComponents(resourceId, applicationId, VIM.AssetTree.Succeeded, VIM.AssetTree.Failed, contentDiv);
        }, function () {
            AssetTree.CollapseApplication(resourceId, applicationId);
        });
    },

    LoadRoot: function (treeDiv, resourceId) {
        treeDiv.html("<div>@{R=VIM.Strings;K=VIMWEBJS_VB0_7;E=js}</div>");
        AssetTree.GetTreeSection(resourceId, "AppTree_r" + resourceId, [], 0, VIM.AssetTree.Succeeded, VIM.AssetTree.Failed, treeDiv.get(0));
    },

    HandleClick: function (rootId, expandCallback, collapseCallback) {
        var toggleImg = $('#' + rootId + '-toggle');
        var contentsDiv = $('#' + rootId + '-contents');

        if (!contentsDiv.is(':hidden')) {
            contentsDiv.slideUp('fast');
            toggleImg.attr("src", "/Orion/images/Button.Expand.gif");
            collapseCallback();
        } else {
            contentsDiv.html("@{R=VIM.Strings;K=VIMWEBJS_VB0_7;E=js}");
            contentsDiv.slideDown('fast');
            toggleImg.attr('src', "/Orion/images/Button.Collapse.gif");
            expandCallback(contentsDiv.get(0));
        }

        return false;
    },

    GetMore: function (linkId, resourceId, keys, startFrom) {
        var contentsDiv = $get(linkId);
        contentsDiv.innerHTML = "@{R=VIM.Strings;K=VIMWEBJS_VB0_7;E=js}";
        AssetTree.GetTreeSection(resourceId, linkId, keys, startFrom, VIM.AppTree.Succeeded, VIM.AppTree.Failed, contentsDiv);
        return false;
    }
};