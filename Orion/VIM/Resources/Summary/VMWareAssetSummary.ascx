<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMWareAssetSummary.ascx.cs"
    Inherits="Orion_VIM_Virtualization_Asset_Summary" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="divOverall" runat="server" style="width: 100%">
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <colgroup>
                    <col width="20px" />
                    <col width="50%" align="left" class="PropertyHeader" />
                    <col align="left" class="Property" />
                </colgroup>
                <tr>
                    <td class="vimAssetFirstColumn">
                        <img src="/Orion/images/Button.Collapse.gif" alt="Expand/Collapse" onclick="return SW.VIM.AssetSummary.ExpandCollapse(this);" style="cursor: pointer;" />
                    </td>
                    <td>
                        <b><%= Resources.VIMWebContent.VIMWEBDATA_LB0_13%></b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_LB0_9%>
                    </td>
                    <td>
                        <%= OverallAssetSummary.NoOfVMHosts%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_104%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_29, OverallAssetSummary.NoOfRunningVMs, OverallAssetSummary.NoOfTotalVMs)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_105%>
                    </td>
                    <td>
                        <%= OverallAssetSummary.NoOfTotalPhysicalCPUCores%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_106%>
                    </td>
                    <td>
                        <%=SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNumber(OverallAssetSummary.TotalRAM / (1024 * 1024 * 1024), 1)%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_107%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_109%>
                    </td>
                    <td>
                        <%= FormatLastPollValueLabel(OverallAssetSummary.LastPollInMin)%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divVMware" runat="server" style="width: 100%; margin-top: 10px;">
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <colgroup>
                    <col width="20px" />
                    <col width="50%" align="left" class="PropertyHeader" />
                    <col align="left" class="Property" />
                </colgroup>
                <tr>
                    <td class="vimAssetFirstColumn">
                        <img src="/Orion/images/Button.Collapse.gif" alt="Expand/Collapse" onclick="return SW.VIM.AssetSummary.ExpandCollapse(this);" style="cursor: pointer;" />
                    </td>
                    <td>
                        <img height="16px" width="16px" style="margin-right: 2px;" alt="VMware" src="/Orion/VIM/images/VmLogo.png" /><b><%= Resources.VIMWebContent.VIMWEBDATA_LB0_14%></b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_100%>
                    </td>
                    <td>
                        <%= VMwareAssetSummary.NoOfVirtualCenters%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_101%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.Clusters_Total_And_VSan, VMwareAssetSummary.NoOfClusters, NoOfVsanClusters)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_102%>
                    </td>
                    <td>
                        <%= VMwareAssetSummary.NoOfResourcePools%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_103%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBDATA_VB0_108, VMwareAssetSummary.NoOfClusteredVMHosts, VMwareAssetSummary.NoOfNonClusteredVMHosts)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_104%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_29, VMwareAssetSummary.NoOfRunningVMs, VMwareAssetSummary.NoOfTotalVMs)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_105%>
                    </td>
                    <td>
                        <%= VMwareAssetSummary.NoOfTotalPhysicalCPUCores%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_106%>
                    </td>
                    <td>
                        <%=SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNumber(VMwareAssetSummary.TotalRAM / (1024 * 1024 * 1024), 1)%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_107%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_109%>
                    </td>
                    <td>
                        <%= FormatLastPollValueLabel(VMwareAssetSummary.LastPollInMin)%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divHyperV" runat="server" style="width: 100%; margin-top: 10px;">
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <colgroup>
                    <col width="20px" />
                    <col width="50%" align="left" class="PropertyHeader" />
                    <col align="left" class="Property" />
                </colgroup>
                <tr>
                    <td class="vimAssetFirstColumn">
                        <img src="/Orion/images/Button.Collapse.gif" style="margin-right: 2px; cursor: pointer;" alt="Expand/Collapse" onclick="return SW.VIM.AssetSummary.ExpandCollapse(this);" />
                    </td>
                    <td>
                        <img height="16px" width="16px" style="margin-right: 2px;" alt="Hyper-V" src="/Orion/VIM/images/hyper_v_icon.png" /><b><%= Resources.VIMWebContent.VIMWEBDATA_LB0_15%></b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_LB0_9%>
                    </td>
                    <td>
                        <%= HyperVAssetSummary.NoOfVMHosts%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_104%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_29, HyperVAssetSummary.NoOfRunningVMs, HyperVAssetSummary.NoOfTotalVMs)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_105%>
                    </td>
                    <td>
                        <%= HyperVAssetSummary.NoOfTotalPhysicalCPUCores%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_106%>
                    </td>
                    <td>
                        <%=SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNumber(HyperVAssetSummary.TotalRAM / (1024 * 1024 * 1024), 1)%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_107%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_109%>
                    </td>
                    <td>
                        <%= FormatLastPollValueLabel(HyperVAssetSummary.LastPollInMin)%>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divNutanix" runat="server" style="width: 100%; margin-top: 10px;">
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <colgroup>
                    <col width="20px" />
                    <col width="50%" align="left" class="PropertyHeader" />
                    <col align="left" class="Property" />
                </colgroup>
                <tr>
                    <td class="vimAssetFirstColumn">
                        <img src="/Orion/images/Button.Collapse.gif" alt="Expand/Collapse" onclick="return SW.VIM.AssetSummary.ExpandCollapse(this);" style="cursor: pointer;" />
                    </td>
                    <td>
                        <img height="16px" width="16px" style="margin-right: 2px;" alt="Nutanix" src="/Orion/VIM/images/nutanix_icon.png" /><b><%= Resources.VIMWebContent.VirtualizationPlatform_Nutanix%></b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_101%>
                    </td>
                    <td>
                        <%= NutanixAssetSummary.NoOfNutanixClusters%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_103%>
                    </td>
                    <td>
                        <%= NutanixAssetSummary.NoOfVMHosts %>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VirtualizationAssetSummary_NumberOfAhvHosts%>
                    </td>
                    <td>
                        <%= NutanixAssetSummary.NoOfAhvHosts %>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VirtualizationAssetSummary_NumberOfEsxVMs%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_29, NutanixAssetSummary.NoOfRunningVMs, NutanixAssetSummary.NoOfTotalVMs)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VirtualizationAssetSummary_NumberOfAhvVMs%>
                    </td>
                    <td>
                        <%= String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_29, NutanixAssetSummary.NoOfRunningAhvVMs, NutanixAssetSummary.NoOfTotalAhvVms)%>
                    </td>
                </tr>
                <tr class="vimCollapsableTableRow">
                    <td class="vimAssetFirstColumn">
                        &nbsp;
                    </td>
                    <td>
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_109%>
                    </td>
                    <td>
                        <%= FormatLastPollValueLabel(NutanixAssetSummary.LastPollInMin)%>
                    </td>
                </tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>
