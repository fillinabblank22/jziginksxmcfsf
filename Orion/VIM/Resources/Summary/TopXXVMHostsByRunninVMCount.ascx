<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByRunninVMCount.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_TopXXVMHostsByRunninVMCount" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
    <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
         <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_31%></td>
                    <td class="ReportHeader" width="80px" ><%= Resources.VIMWebContent.VIMWEBDATA_LB0_3%></td>
                    <td class="ReportHeader" width="100px"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_94%></td>
                </tr>
         </HeaderTemplate>
         <ItemTemplate>
            <tr>
                <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("HostStatus")%>' />
                      <a href="<%# Eval("DetailsUrl") %>">
                         <%# HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown))%>
                    </a>
                </td> 
                <td class="Property vimTopXXProperty">
                    <%# SolarWinds.VIM.Web.Helpers.PlatformHelper.GetVirtualizationTypeName(Eval("PlatformID"))%>
                </td>
                <td class="Property vimTopXXProperty"><%#  String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_11,Eval("VmRunningCount"),Eval("VmCount"))%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
      </asp:Repeater>
    </Content>    
</orion:resourceWrapper>