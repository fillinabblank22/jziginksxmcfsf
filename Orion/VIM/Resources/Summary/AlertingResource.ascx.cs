using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Common.Extensions;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;
using System.Text;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_Summary_AlertingResource : VimBaseResource
{
    private static readonly IEnumerable<ViewType> _supportedViewTypes = new[] {ViewType.Cluster, ViewType.Guest, ViewType.Host, ViewType.VManDatastore};

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.WEBDATA_LM0_25; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
                return Resource.SubTitle;

                return Resources.CoreWebContent.WEBCODE_IB0_6;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!_supportedViewTypes.Contains(DetectedViewType))
        {
            Visible = false;
            return;
        }

        DataTable table = null;
        try
        {
            AlertingDAL alertingDAL = new AlertingDAL();
            table = alertingDAL.GetAlerting(this.DetectedViewType, (int)this.EntityId);
        }
        catch (System.Data.SqlClient.SqlException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }

        if (table != null && table.Rows.Count > 0)
        {
            resourceTable.DataSource = table;
            resourceTable.DataBind();
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHActiveVirtualizationAlerts"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }

    protected string FormatTime(object time)
    {
        return Utils.FormatDateTime((DateTime)time, true);
    }

    protected string GetLink(object entityType, object entityId, object name, object platform, object detailURL, object status)
    {
        switch (entityType.ToString())
        {
            case "Virtual Datastore":
                return EntityLinkHelper.GetEntityLink(name, status, platform, true, VimEntityType.DataStore, detailURL);
            case "Virtual Machine":
                return EntityLinkHelper.GetEntityLink(name, status, platform, true, VimEntityType.VirtualMachine, detailURL);
            case "Virtual Host":
                return EntityLinkHelper.GetEntityLink(name, status, platform, true, VimEntityType.Host, detailURL);
            case "Virtual Cluster":
                return EntityLinkHelper.GetEntityLink(name, status, VirtualizationPlatform.Vmware, true, VimEntityType.Cluster, detailURL);
        }
        return name.ToString();
    }


    protected void gdvDatabound(object sender, EventArgs e)
    {
        Dictionary<string, int> categories = new Dictionary<string, int>();
        DataTable table = resourceTable.DataSource as DataTable;
        if (table == null || table.Rows.Count != resourceTable.Rows.Count)
            return;
        foreach (GridViewRow dgRow in resourceTable.Rows)
        {
            DataRow row = table.Rows[dgRow.DataItemIndex];

            string name = row["ObjectType"].ToString().HtmlEncode();
            dgRow.CssClass = name.Replace(' ', '_');
            
            if (categories.ContainsKey(name))
            {
                categories[name]++;
            }
            else
            {
                categories.Add(name, 1);
            }
        }
        
        int inexToInsert = 1;
        foreach (var category in categories)
        {
            Table tbl = resourceTable.Rows[0].Parent as Table;
            if (tbl != null)
            {
                GridViewRow row = new GridViewRow(-1, -1, DataControlRowType.DataRow, DataControlRowState.Normal);
                row.Style.Add("background-color", "#f0f0f0");
                
                TableCell cell = new TableCell();
                cell.ColumnSpan = 2;

                HtmlImage image = new HtmlImage();
                image.Src = "/Orion/images/Button.Collapse.gif";
                image.Style.Add(HtmlTextWriterStyle.VerticalAlign, "bottom");
                image.Style.Add(HtmlTextWriterStyle.PaddingRight, "3px");
                image.Attributes["onclick"] = "javascript:rowClicked(this, '" + category.Key.Replace(' ', '_') + "');";
                cell.Controls.Add(image);

                HtmlGenericControl span = new HtmlGenericControl("span");
                span.InnerHtml = GetCategoryInnerHtml(category.Value, category.Key);
                cell.Controls.Add(span);
                row.Cells.Add(cell);

                cell = new TableCell();
                cell.Style.Add("text-align", "right");
                cell.Style.Add("font-size", "12px");
                cell.Style.Add("font-weight", "bold");

                span = new HtmlGenericControl("span");
                span.InnerHtml = "<img src='/Orion/VIM/images/VManAlerts/fire_icon16x16v2.png' style='vertical-align:bottom'/ >" + category.Value;
                cell.Controls.Add(span);
                row.Cells.Add(cell);

                tbl.Rows.AddAt(inexToInsert, row);
                inexToInsert += category.Value + 1;
            }
            
        }
    }

    protected string GetCategoryInnerHtml(int categoryValue, string categoryKey)
    {
        string entityTypePluralLocalized = string.Empty;

        switch (categoryKey)
        {
            case "Virtual Datastore":
                entityTypePluralLocalized = Resources.VIMWebContent.Web_ActiveVirtualizationAlertsResource_VirtualDatastores;
                break;
            case "Virtual Machine":
                entityTypePluralLocalized = Resources.VIMWebContent.Web_ActiveVirtualizationAlertsResource_VirtualMachines;
                break;
            case "Virtual Host":
                entityTypePluralLocalized = Resources.VIMWebContent.Web_ActiveVirtualizationAlertsResource_VirtualHosts;
                break;
            case "Virtual Cluster":
                entityTypePluralLocalized = Resources.VIMWebContent.Web_ActiveVirtualizationAlertsResource_VirtualClusters;
                break;
        }

        StringBuilder innerHtml = new StringBuilder();
        innerHtml.Append(string.Format("<span style='font-weight:bold'>{0}</span> ", categoryValue));
        innerHtml.Append(string.Format(Resources.VIMWebContent.Web_ActiveVirtualizationAlertsResource_AlertsOnRelated, entityTypePluralLocalized));

        return innerHtml.ToString();
    }
}
