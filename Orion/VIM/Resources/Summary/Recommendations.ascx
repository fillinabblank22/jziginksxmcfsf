﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Recommendations.ascx.cs" Inherits="Orion_VIM_Resources_Summary_Recommendations" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="recommendations" TagName="RecommendationsResourceWrapper" Src="~/Orion/VIM/Controls/RecommendationsResourceWrapper.ascx" %>

<orion:Include ID="Include1" runat="server" File="VIM/js/Resources/SummaryRecommendationsController.js" />


<recommendations:RecommendationsResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div id="sw-vim-recommendations-groupBy">
            <%= Resources.VIMWebContent.VIMWEBDATA_TK0_36%> 
        </div>
        <orion:CustomQueryTable runat="server" ID="RecommendationsCustomTable"/>
 
        <script type="text/javascript">
            $(function () {

                var vimRecommendationsController = new SW.VIM.Summary.Recommendations.SummaryRecommendationsController({
                    resourceId: '<%=Resource.ID%>',
                    countFormat: '<%=Resources.CoreWebContent.Resource_ActiveAlerts_TitleCount%>'
                });

                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= RecommendationsCustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        allowSort: true,
                        onLoad: vimRecommendationsController.onLoad,
                        columnSettings: {
                            "Risk_by_Priority": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_TK0_38 %>',
                                formatter: vimRecommendationsController.formatRisksCount,
                                isHtml: true
                            },
                            "Name": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_TK0_39 %>',
                                formatter: vimRecommendationsController.formatName
                            },
                            "RecommendationCount": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_TK0_40 %>',
                                formatter: vimRecommendationsController.formatRecommendations
                            }
                        }
                    });

                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= RecommendationsCustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</recommendations:RecommendationsResourceWrapper>
