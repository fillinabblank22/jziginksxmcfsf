﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_VIM_Resources_Summary_GettingStarted : VimBaseResource
{
    public struct StepNames
    {
        public const string ManageRecommendations = "ManageRecommendations";
        public const string ManageStrategies = "ManageStrategies";
        public const string ViewReports = "CapacityPlanningViewReports";
    }

    private const string CompletedCssClass = "vim-gettingstarted-counter-completed";
    private List<string> _completedStepsToHighlight;
    private ISet<string> _completedStepsFromWebSettings;

    protected override string DefaultTitle => VIMWebContent.GettingStarted_DefaultResourceTitle;
    public const int RecommendationStepsCounterMax = 2, CapacityPlanningStepsCounterMax = 2;
    public int IntegrationCounter { get; set; } = 0;
    public int RecommendationStepsCounter { get; set; } = 0;
    public int CapacityPlanningStepsCounter { get; set; } = 0;
    public string IntegrationCounterCssClass { get; set; } = string.Empty;
    public string RecommendationCounterCssClass { get; set; } = string.Empty;
    public string CapacityPlanningCounterCssClass { get; set; } = string.Empty;

    public string GetDocumentationText()
    {
        string documentationText = VIMWebContent.GettingStarted_DocumentationText;
        return string.Format(CultureInfo.InvariantCulture, documentationText,
            @"<a href=""https://support.solarwinds.com/@api/deki/files/32049/VMAN-GettingStarted-Guide.pdf"" target=""_blank"">",
            @"<a href=""http://www.solarwinds.com/documentation/kbloader.aspx?kb=MT74262"" target=""_blank"">",
            "</a>"
            );
    }

    public string StepsToJavascriptArray()
    {
        return $"['{string.Join("','", _completedStepsToHighlight ?? new List<string>())}']";
    }

    protected string RetireVmanApplianceKbLink
    {
        get { return KnowledgebaseHelper.GetMindTouchKBUrl(119586); }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!Profile.AllowAdmin || !SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
        {
            Visible = false;
            return;
        }
    }
    
    protected override void OnLoad(EventArgs e)
    {
        RemoveResourceLink.HRef = Page.ClientScript.GetPostBackClientHyperlink(RemoveResourceButton, String.Empty);

        var dal = new GettingStartedDAL();

        _completedStepsFromWebSettings = dal.GetCompletedSteps();
        _completedStepsToHighlight = new List<string>(_completedStepsFromWebSettings);

        RecommendationStepsCounter = 0;
        CapacityPlanningStepsCounter = 0;

        if (_completedStepsFromWebSettings.Contains(StepNames.ManageRecommendations))
        {
            RecommendationStepsCounter++;
        }
        if (_completedStepsFromWebSettings.Contains(StepNames.ManageStrategies))
        {
            RecommendationStepsCounter++;
        }
        if (_completedStepsFromWebSettings.Contains(StepNames.ViewReports))
        {
            CapacityPlanningStepsCounter++;
        }


        if (RecommendationStepsCounter == RecommendationStepsCounterMax)
        {
            RecommendationCounterCssClass = CompletedCssClass;
        }

        if (dal.IsThereAnyCapacityPlanningReport())
        {
            CapacityPlanningStepsCounter++;
            _completedStepsToHighlight.Add("CapacityPlanningFirstReport");
        }

        if (CapacityPlanningStepsCounter == CapacityPlanningStepsCounterMax)
        {
            CapacityPlanningCounterCssClass = CompletedCssClass;
        }
    }

    protected void OnRemoveResourceButtonClicked(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }
}
