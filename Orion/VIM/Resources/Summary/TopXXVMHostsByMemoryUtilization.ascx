﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByMemoryUtilization.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_TopXXVMHostsByMemoryUtilization" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
    <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_31%></td>
                    <td class="ReportHeader" width="80px" ><%= Resources.VIMWebContent.VIMWEBDATA_LB0_3%></td>
                    <td class="ReportHeader" colspan="3" width="10%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_37%></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("HostStatus")%>' />
                      <a href="<%# Eval("DetailsUrl") %>">
                         <%# HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown))%>
                    </a>
                </td> 
                <td class="Property vimTopXXProperty">
                    <%# SolarWinds.VIM.Web.Helpers.PlatformHelper.GetVirtualizationTypeName(Eval("PlatformID"))%>
                </td>
                <td class="Property vimTopXXProperty" style="white-space:nowrap;"><%# FormatBytes((Int64)Eval("MemoryUsed") * 1024 * 1024)%></td>
                <td class="Property vimTopXXProperty"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercent(Eval("PercentMemoryUsed"), this.type)%></td>
                <td class="Property vimTopXXProperty"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(Eval("PercentMemoryUsed"), this.type)%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
          </table>
        </FooterTemplate>
    </asp:Repeater>
    </Content>    
</orion:resourceWrapper>