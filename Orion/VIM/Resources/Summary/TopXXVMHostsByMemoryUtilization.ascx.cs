using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_TopXXVMHostsByMemoryUtilization : VimTopXXResourceControl
{
    public ThresholdType type { get { return ThresholdType.PercentMemoryUsed; } }
    public int ErrorLevel { get; set; }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_20; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;

        try
        {
            var dal = new TopXXVMHostsByMemoryUtilizationDAL();
            table = dal.GetTopXXVMByMemoryUtilization(this.MaxRecords, this.Resource.Properties["Filter"], PlatformHelper.GetCollectionFromResourceProperties(this.Resource.Properties));
            //this.Visible = table.Rows.Count > 0;
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.HostEntityName);

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> { { "EntityName", "Orion.VIM.Hosts" }}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMHostsTopXXEditControl.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Virtualization_TopXXVMwareHostsByPercentMemoryUsed"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

}
