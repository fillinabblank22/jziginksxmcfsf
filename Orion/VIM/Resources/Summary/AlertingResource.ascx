<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertingResource.ascx.cs"
    Inherits="Orion_VIM_Resources_Summary_AlertingResource" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <script type="text/javascript">

            function rowClicked(sender, cssclass) {
                if (sender.src.indexOf("Button.Collapse.gif") != -1)
                    sender.src = sender.src.replace("Button.Collapse.gif", "Button.Expand.gif");
                else
                    sender.src = sender.src.replace("Button.Expand.gif", "Button.Collapse.gif");
                $("." + cssclass).toggle("slow");
            }

            function setOrder(column) { $("#orderBy").val(column); return true; }
            
            $(function () {
                var refresh = function () { __doPostBack('<%= ctrResUP.ClientID %>', ''); };
                SW.Core.View.AddOnRefresh(refresh, '<%= ctrResUP.ClientID %>');
            });

        </script>
        <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
        </asp:Panel>

        <asp:UpdatePanel ID="ctrResUP" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                 
                 <asp:GridView ID="resourceTable" runat="server" class="NeedsZebraStripes"
                  AutoGenerateColumns="False" BorderWidth="0" GridLines ="None" OnDataBound="gdvDatabound" >
                         <HeaderStyle cssclass="ReportHeader" BorderWidth="0" />
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources: VIMWebContent, Web_ActiveVirtualizationAlertsResource_TimeOfAlert %>">
                                 <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<% #FormatTime(Eval("TriggerTimeStamp")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources: VIMWebContent, Web_ActiveVirtualizationAlertsResource_Name %>">
                                 <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<% #GetLink(Eval("ObjectType"), Eval("Id"), Eval("Name"), Eval("PlatformID"), Eval("DetailsUrl"), Eval("Status")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="AlertText" HeaderText="<%$ Resources: VIMWebContent, Web_ActiveVirtualizationAlertsResource_Message %>"/>
                         </Columns>
                         
                        </asp:GridView>
                </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
