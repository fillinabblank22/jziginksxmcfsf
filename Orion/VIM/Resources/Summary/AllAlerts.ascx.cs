﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_AllAlerts : VimBaseResource
{
    private const string SWQL = @"
        SELECT
            ac.Severity AS [_Severity],
            ' ' + ac.Name AS Name,
            '/Orion/View.aspx?NetObject=AAT:' + TOSTRING(aa.AlertObjectID) AS [_LinkFor_Name],
            '/Orion/images/ActiveAlerts/' + CASE ac.Severity
                WHEN 1 THEN 'Warning.png'
                WHEN 2 THEN 'Critical.png'
                WHEN 3 THEN 'Serious.png'
            END AS [_IconFor_Name], 
            ac.Description, 
            ao.EntityCaption, 
            ao.EntityDetailsUrl AS [_LinkFor_EntityCaption],
            SecondDiff(aa.TriggeredDateTime, GetUtcDate()) AS ActiveTimeInSeconds
        FROM 
            Orion.AlertObjects ao
            INNER JOIN Orion.AlertActive aa ON ao.AlertObjectID = aa.AlertObjectID
            INNER JOIN Orion.AlertConfigurations ac ON ac.AlertID = ao.AlertID
        WHERE 
            (ao.RealEntityType LIKE 'Orion.VIM%' OR ao.RealEntityType LIKE 'Cortex.Orion.Virtualization%' OR ao.Context LIKE '%VMwareEvents%') AND 
            ac.Severity IN (1, 2, 3)";

    private const string OrderBy = "aa.TriggeredDateTime DESC";
    private const string UnacknowledgedAlertsCondition = " AND (Acknowledged IS NULL OR Acknowledged = False)";

    private bool showAcknowledgedAlerts;

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string SubTitle
    {
        get
        {
            if (!string.IsNullOrEmpty(Resource.SubTitle.Trim()))
            {
                return Resource.SubTitle;
            }

            // if subtitle is not specified
            if (showAcknowledgedAlerts)
            {
                return Resources.CoreWebContent.WEBCODE_IB0_5;
            }
            else
            {
                return Resources.CoreWebContent.WEBCODE_IB0_6;
            }
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualizationAlerts"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditAllAlerts.ascx"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_JD0_25; }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        showAcknowledgedAlerts = !String.IsNullOrEmpty(Resource.Properties["ShowAcknowledgedAlerts"]) &&
            Resource.Properties["ShowAcknowledgedAlerts"].Equals("true", StringComparison.OrdinalIgnoreCase);

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = (showAcknowledgedAlerts) ? SWQL : SWQL + UnacknowledgedAlertsCondition;
        CustomTable.OrderBy = OrderBy;

        Wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
        Wrapper.ManageButtonTarget = "/Orion/VIM/AllAlertsProxy.aspx?alertName=[All]";
        Wrapper.ShowManageButton = true;
    }
}
