<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByNetworkUtilization.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_TopXXVMHostsByNetworkUtilization" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Web"%>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
    <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
        <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst" colspan="2" width="5%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_31%></td>
                    <td class="ReportHeader" style="white-space:nowrap; padding-right: 10px"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_3%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_41%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_42%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_43%></td>
                    <td class="ReportHeader" width="15%" colspan="2" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_96%></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("HostStatus")%>' />
                </td> 
                <td width="100%">
                      <a href="<%# Eval("DetailsUrl") %>">
                         <%# HttpUtility.HtmlEncode(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown)))%>
                      </a>
                </td>
                <td class="Property vimTopXXProperty">
                    <%# SolarWinds.VIM.Web.Helpers.PlatformHelper.GetVirtualizationTypeName(Eval("PlatformID"))%>
                </td>
                <td class="Property vimTopXXProperty" style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
                <td class="Property vimTopXXProperty" style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("xmitKBps"), (float)Eval("rcvKBps"))%></td>
                <td class="Property vimTopXXProperty" style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("usageKBps"), (float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
                <td class="Property vimTopXXProperty" ><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercent(Eval("utilization"), ThresholdType.NetworkUtilization)%></td>
                <td class="Property vimTopXXProperty" ><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(Eval("utilization"), ThresholdType.NetworkUtilization)%></td>
            </tr>
        </ItemTemplate>
            
        <FooterTemplate>
            </table>
        </FooterTemplate>
      </asp:Repeater>
    </Content>    
</orion:resourceWrapper>