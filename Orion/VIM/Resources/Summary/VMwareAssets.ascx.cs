using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.AjaxTree;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Controls.AssetTree;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_VMwareAsset : VimBaseResource
{
    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_24; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Virtualization_VMwareAssets"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditControls/VirtualAssetTreeEdit.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnInit(EventArgs e)
    {
        Wrapper.ManageButtonText = Resources.VIMWebContent.VIMWEBCODE_LB0_1;
        Wrapper.ShowManageButton = (Profile.AllowAdmin && Profile.AllowNodeManagement) || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;

        SetPlatformVisibility();

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        var manager = ScriptManager.GetCurrent(this.Page);

        if (manager != null)
        {
            manager.Scripts.Add(new ScriptReference(PathResolver.GetVirtualPath("VIM", "TreeControl.js")));
        }

        base.OnPreRender(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (pnlVMware.Visible)
        {
            CreateTree(VMwareAssetTree, VirtualizationPlatform.Vmware);
        }

        if (pnlHyperV.Visible)
        {
            CreateTree(HyperVAssetTree, VirtualizationPlatform.HyperV);
        }

        if (pnlNutanix.Visible)
        {
            CreateTree(NutanixAssetTree, VirtualizationPlatform.Nutanix);
        }
    }

    private void CreateTree(SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl tree, VirtualizationPlatform platform)
    {
        var dataProviderIdentifier = String.Format("VIM_ASSET_TREE_DATA-{0}", platform); // data provider identifier ! resource id is used to uniquely identify provider in application scope
        var expandedStateProviderIdentifier = String.Format("VIM_ASSET_TREE_STATE-{0}-{1}-{2}-{3}",
            this.Resource.ID,
            this.Resource.View.ViewID,
            Request.QueryString["NetObject"] ?? String.Empty,
            platform); //identifies this instance in session scope

        this.Session[String.Format("{0}-remember", expandedStateProviderIdentifier)] = GetBooleanResourceProperty(Orion_VIM_Controls_EditControls_RememberExpandedStateControl.RememberExpandedStateKey);

        if (!AppScopeTreeDataManager.ContainsProvider(dataProviderIdentifier))
        {
            AppScopeTreeDataManager.RegisterProvider(dataProviderIdentifier, new AssetTreeNodeProvider());
        }
        if (!SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
        {
            SessionScopeTreeExpandedStateManager.RegisterProvider(expandedStateProviderIdentifier);
        }

        tree.LoadTree(new AssetTreeService(), GetPrimaryNode(dataProviderIdentifier, expandedStateProviderIdentifier, platform));
    }

    private object[] GetPrimaryNode(string dataProviderIdentifier, string expandedStateProviderIdentifier, VirtualizationPlatform platform)
    {
        var definition = new List<object>() 
        { 
            dataProviderIdentifier, 
            expandedStateProviderIdentifier,
            0 // tree level
        };

        string prefix = String.Empty;
        int id = 0;

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        var clusterProvider = GetInterfaceInstance<IVimClusterProvider>();
        var datacenterProvider = GetInterfaceInstance<IVimDataCenterProvider>();
        var hostProvider = GetInterfaceInstance<IVimHostProvider>();
        var vmProvider = GetInterfaceInstance<IVimVirtualMachineProvider>();

        if (nodeProvider != null)
        {
            prefix = "N";
            id = nodeProvider.Node.NodeID;
        }
        else if (clusterProvider != null)
        {
            prefix = VimCluster.Prefix;
            id = clusterProvider.Cluster.Id;
        }
        else if (datacenterProvider != null)
        {
            prefix = VimDataCenter.Prefix;
            id = datacenterProvider.DataCenter.Id;
        }
        else if (hostProvider != null)
        {
            prefix = VimHost.Prefix;
            id = hostProvider.Host.Id;
        }
        else if (vmProvider != null)
        {
            prefix = VimVirtualMachine.Prefix;
            id = vmProvider.VirtualMachine.Id;
        }

        // add node type
        if (id > 0)
        {
            int managedObjectId;
            AssetTreeNodeType nodeType = (new VMwareAssetTreeDAL()).GetAsseTreeNodeType(prefix, id, out managedObjectId);
            id = managedObjectId;
            definition.Add((int)nodeType);
        }
        else
        {
            // no suitable netobject found, we display all
            definition.Add((int)AssetTreeNodeType.Root);
        }

        // netObjectID
        definition.Add(id);

        // parent path   
        definition.Add("root");

        // caption
        definition.Add(String.Empty);

        // resource ID
        definition.Add(this.Resource.ID);

        // virtualization platform
        definition.Add((int)platform);

        return definition.ToArray();
    }

    private void SetPlatformVisibility()
    {
        bool vmwareEnabled = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.VMwareEnabledKey, true);
        bool hypervEnabled = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.HyperVEnabledKey, true);
        bool nutanixEnabled = PlatformHelper.GetBooleanPropertyValue(this.Resource.Properties, PlatformHelper.NutanixEnabledKey, true);

        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        var clusterProvider = GetInterfaceInstance<IVimClusterProvider>();
        var datacenterProvider = GetInterfaceInstance<IVimDataCenterProvider>();
        var hostProvider = GetInterfaceInstance<IVimHostProvider>();
        var vmProvider = GetInterfaceInstance<IVimVirtualMachineProvider>();
        var dal = new VMwareAssetTreeDAL();

        VirtualizationPlatform? platform = null;

        if (clusterProvider != null)
        {
            platform = clusterProvider.Cluster.Model.Platform;
        }
        else if (hostProvider != null)
        {
            platform = hostProvider.Host.Model.Platform;
        }
        else if (datacenterProvider != null)
        {
            // we support only vmWare data centers
            platform = VirtualizationPlatform.Vmware;
        }
        else if (vmProvider != null)
        {
            platform = vmProvider.VirtualMachine.Model.HostPlatform;
        }
        else if (nodeProvider != null)
        {
            platform = dal.GetPlatformByNode(nodeProvider.Node.NodeID);
        }

        if (platform != null)
        {
            pnlVMware.Visible = vmwareEnabled && (platform == VirtualizationPlatform.Vmware);
            pnlHyperV.Visible = hypervEnabled && (platform == VirtualizationPlatform.HyperV);
            pnlNutanix.Visible = nutanixEnabled && (platform == VirtualizationPlatform.Nutanix);
        }
        else // try to show both
        {
            pnlVMware.Visible = vmwareEnabled && dal.IsPlatformAvailable(VirtualizationPlatform.Vmware);
            pnlHyperV.Visible = hypervEnabled && dal.IsPlatformAvailable(VirtualizationPlatform.HyperV);
            pnlNutanix.Visible = nutanixEnabled && dal.IsPlatformAvailable(VirtualizationPlatform.Nutanix);
        }

        const string vmwURL = "~/Orion/VIM/Admin/VMwareServers.aspx";
        const string hvURL = "~/Orion/VIM/Admin/HyperVServers.aspx";
        const string nutURL = "~/apps/vim/settings/nutanix";

        if (pnlVMware.Visible)
        {
            Wrapper.ManageButtonTarget = vmwURL;
        }
        else if (pnlHyperV.Visible)
        {
            Wrapper.ManageButtonTarget = hvURL;
        }
        else if (pnlNutanix.Visible)
        {
            Wrapper.ManageButtonTarget = nutURL;
        }
        else if (vmwareEnabled)
        {
            Wrapper.ManageButtonTarget = vmwURL;
        }
        else if (hypervEnabled)
        {
            Wrapper.ManageButtonTarget = hvURL;
        }
        else if (nutanixEnabled)
        {
            Wrapper.ManageButtonTarget = nutURL;
        }
        else
        {
            Wrapper.ManageButtonTarget = string.Empty;
        }
    }

    private bool GetBooleanResourceProperty(string key)
    {
        return (this.Resource.Properties[key] ?? "true").ToLowerInvariant() == "true";
    }
}

