<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByCPULoad.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_TopXXVMHostsByCPULoad" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
   <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
   </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
       <HeaderTemplate>
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_31%></td>
                    <td class="ReportHeader" width="80px" ><%= Resources.VIMWebContent.VIMWEBDATA_LB0_3%></td>
                    <td class="ReportHeader" width="10%" colspan="2"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_76%></td>
                </tr>
       </HeaderTemplate>
        <ItemTemplate>
          <tr>
             <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("Status")%>' />
                  <a href="<%# HttpUtility.HtmlEncode(Eval("DetailsUrl")) %>"><%# HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown))%></a>
             </td> 
             <td class="Property vimTopXXProperty">
                 <%# SolarWinds.VIM.Web.Helpers.PlatformHelper.GetVirtualizationTypeName(Eval("PlatformID"))%>
             </td>
             <td class="Property vimTopXXProperty"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercent(Eval("CPULoad"), this.type)%></td>
             <td class="Property vimTopXXProperty"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(Eval("CPULoad"), this.type)%></td>
          </tr>
      </ItemTemplate>
      <FooterTemplate>
            </table>
      </FooterTemplate>
     </asp:Repeater>
   </Content>    
</orion:resourceWrapper>

