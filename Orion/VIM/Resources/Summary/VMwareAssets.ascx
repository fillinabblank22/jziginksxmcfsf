<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMwareAssets.ascx.cs"
    Inherits="Orion_VIM_Resources_Virtualization_VMwareAsset" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Register src="~/Orion/VIM/Controls/EditControls/RememberExpandedStateControl.ascx" tagname="RememberExpandedStateControl" tagprefix="vim" %>
<%@ Register TagPrefix="AjaxTree" Assembly="SolarWinds.VIM.Web" Namespace="SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl" %>

<script type="text/javascript">
    $(function () {
        var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
    </script>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Always">
        <ContentTemplate>
        <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
        <asp:Panel ID="pnlVMware" runat="server" style="margin-bottom: 10px">
            <%= PlatformHelper.GetPlatformIcon(VirtualizationPlatform.Vmware) %><span style="font-weight: bold"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_14%></span>
            <AjaxTree:TreeControl ID="VMwareAssetTree" runat="server">
                <WebService Path="/Orion/VIM/Services/AssetTreeService.asmx" />
            </AjaxTree:TreeControl>
        </asp:Panel>
        <asp:Panel ID="pnlHyperV" runat="server" style="margin-bottom: 10px">
            <%= PlatformHelper.GetPlatformIcon(VirtualizationPlatform.HyperV) %><span style="font-weight: bold"><%= Resources.VIMWebContent.VIMWEBDATA_LB0_15%></span>
            <AjaxTree:TreeControl ID="HyperVAssetTree" runat="server">
                <WebService Path="/Orion/VIM/Services/AssetTreeService.asmx" />
            </AjaxTree:TreeControl>
        </asp:Panel>
            <asp:Panel ID="pnlNutanix" runat="server" style="margin-bottom: 10px">
                <%= PlatformHelper.GetPlatformIcon(VirtualizationPlatform.Nutanix) %><span style="font-weight: bold"><%= Resources.VIMWebContent.VirtualizationPlatform_Nutanix%></span>
                <AjaxTree:TreeControl ID="NutanixAssetTree" runat="server">
                    <WebService Path="/Orion/VIM/Services/AssetTreeService.asmx"/>
                </AjaxTree:TreeControl>
            </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>

<orion:Include runat="server" Module="VIM" File="ee/babylonjs/babylon.js" />
<orion:Include runat="server" Module="VIM" File="ee/maze.js" />

<div id="vimEasterEggMazeOverlay" class="ui-dialog ui-widget ui-widget-content ui-corner-all LogInCustomerPortal ui-draggable" style="display: none; z-index: 1002; position: fixed; top: 15px; left: 50vw; transform: translate(-500px, 0px); width: 960px; padding: 10px; background-color: rgb(255, 255, 255);">
    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
        <span class="ui-dialog-title"><%= Resources.VIMWebContent.Web_MazeEasterEgg_Title%></span>
        <a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" onclick="stopGameAndHideOverlay()"><span class="ui-icon ui-icon-closethick">close</span></a>
    </div>
    <div style="width:100%; height:100%">
        <canvas id="vimEasterEggMazeCanvas" style="width: 960px; height: 540px; touch-action: none;"></canvas>
    </div>
</div>
