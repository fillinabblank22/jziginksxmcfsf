<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllAlerts.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_AllAlerts" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<style type="text/css">
    table#Grid-<%= CustomTable.UniqueClientID %> {
        table-layout: fixed;
    }
</style>
<vman:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="True">
    <Content>
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize({
                    uniqueId: <%= CustomTable.UniqueClientID %>,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                    onLoad: setAlertsNumber,
                    columnSettings: {
                        "Name": {
                            cellCssClassProvider: cellCssClassProvider,
                            header: '<%= VIMWebContent.VIMWEBDATA_JD0_14 %>'
                        },
                        "Description": {
                            cellCssClassProvider: cellCssClassProvider,
                            header: '<%= VIMWebContent.VIMWEBDATA_JD0_15 %>'
                        },
                        "EntityCaption": {
                            cellCssClassProvider: function(value, row, cellInfo) {
                                return cellCssClassProvider(value, row, cellInfo) + ' sw-vim-activeAlerts-entityCell';
                            },
                            header: '<%= VIMWebContent.VIMWEBDATA_SH0_3 %>'
                        },
                        "ActiveTimeInSeconds": {
                            cellCssClassProvider: cellCssClassProvider,
                            header: '<%= VIMWebContent.VIMWEBDATA_JD0_16 %>',
                            formatter: timeFormatter
                        }
                    }
                });

                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
                
                // Change resource subtitle by adding labels with attached tooltips
                (function addSubtitleLables() {
                    // subtitle is changed already
                    if ($("div.ResourceWrapper[resourceid=<%= Resource.ID %>] .sw-vim-subtitleContainer").length > 0) {
                        return;
                    }

                    var subtitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h2:first");
                
                    // save original subtitle
                    var leftSubtitle = subtitle.css("float", "left")[0].outerHTML;
                    var rightSubtitle = 
                        "<div style='float: right;'>" +
                            "<span class='sw-vim-tooltipLabel' alertLegend><%= VIMWebContent.VIMWEBDATA_SH0_5%></span>" + 
                        "</div>";                      

                    subtitle.replaceWith("<div class='sw-vim-subtitleContainer'>" + leftSubtitle + rightSubtitle + "</div>");
                
                    var legendTooltipMarkup =
                        "<table class='sw-vim-nowrap'>" +
                            "<tr>" +
                                "<td colspan='2'><b><%= VIMWebContent.VIMWEBDATA_SH0_7%></b></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Critical.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_8%></td>" +
                                "<td><img src='/Orion/images/ActiveAlerts/InformationalAlert.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_11%></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Serious.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_9%></td>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Notice.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_12%></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td colspan='2'><img src='/Orion/images/ActiveAlerts/Warning.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_10%></td>" + 
                            "</tr>" +
                        "</table>";
                    
                    $(".sw-vim-tooltipLabel[alertLegend]").attr("title", legendTooltipMarkup);
                    $(".sw-vim-tooltipLabel").tooltip({
                        id: "sw-vim-allAlertsResourceTooltip"
                    });
                })();



                function cellCssClassProvider(value, row, cellInfo) {
                    var result = null;
                    if (row[0] == 1) {
                        result = "sw-vim-activeAlerts-rowWarning";
                    } else if (row[0] == 2) {
                        result =  "sw-vim-activeAlerts-rowError";
                    } else if (row[0] == 3) {
                        result = "sw-vim-activeAlerts-rowSerious";
                    }

                    if (cellInfo.name == "Description") {
                        result += " sw-vim-activeAlerts-descriptionColumn";
                    }
                    return result;
                };

                function setAlertsNumber() {
                    var $resourceWrapperTitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h1:first");
                    if ($resourceWrapperTitle.find("span").length == 0) {
                        $resourceWrapperTitle.append(SW.Core.String.Format("<span><%= CoreWebContent.Resource_ActiveAlerts_TitleCount %></span>", getTotalRows()));
                    } else {
                        $resourceWrapperTitle.find("span").text(SW.Core.String.Format("<%= CoreWebContent.Resource_ActiveAlerts_TitleCount %>", getTotalRows()));
                    }
                };
                
                function getTotalRows() {
                    var resourcePager = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] div.ResourcePagerInfo");
                    var arr = resourcePager.text().split(' ');
                    return (arr[arr.length - 2] != '') 
                        ? arr[arr.length - 2] 
                        : 0;
                };

                function timeFormatter(secondDiff) {
                    var days = Math.floor(secondDiff / (60 * 60 * 24));
                    secondDiff -=  days * (60 * 60 * 24);

                    var hours = Math.floor(secondDiff / (60 * 60));
                    secondDiff -= hours * (60 * 60);

                    var minutes = Math.floor(secondDiff / 60 );
                                    
                    var result = '';
                    if (days > 0) {
                        result += days + '<%= VIMWebContent.VIMWEBDATA_SH0_15%> ';
                    }
                    if (result.length > 0) {
                        result += hours + '<%= VIMWebContent.VIMWEBDATA_SH0_14%> ';
                    } else {
                        if (hours > 0) {
                            result += hours + '<%= VIMWebContent.VIMWEBDATA_SH0_14%> ';
                        }
                    }
                    if (minutes > 0) {
                        result += minutes + '<%= VIMWebContent.VIMWEBDATA_SH0_13%>';
                    }
                    return result;
                }
            });
        </script>
    </Content>
</vman:resourceWrapper>

