﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_VIM_Resources_Summary_Recommendations : VimBaseResource
{
    const string AllRecommendationsButtonTarget = "/ui/recommendations/current";

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_TK0_41; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Virtualization_Recommendations"; }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!LicenseManager.Instance.IsVimFullyLicensed() || !VimProfile.AllowRecommendations)
        {
            Visible = false;
            return;
        }

        Wrapper.ManageButtonText = Resources.VIMWebContent.VIMWEBDATA_TK0_42;
        Wrapper.ShowManageButton = true;
        Wrapper.ManageButtonTarget = AllRecommendationsButtonTarget;

        RecommendationsCustomTable.UniqueClientID = ScriptFriendlyResourceID;
        RecommendationsCustomTable.SWQL = SWQL;
        RecommendationsCustomTable.OrderBy = "[_EntityType]";
    }

    public string SWQL => @"
SELECT
    RE.EntityID AS [_EntityID],
    RE.EntityType AS [_EntityType],
    TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations
            WHERE Priority=1 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL))
        + ':' + TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
            WHERE Priority=2 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL))
        + ':' + TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
            WHERE Priority=3 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL)) AS [Risk_by_Priority],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=1 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL) AS [_PriorityHigh],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=2 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL) AS [_PriorityMedium],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=3 AND EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL) AS [_PriorityLow],
    C.Name,
    C.DetailsUrl AS [_LinkFor_Name],
    Count(*) AS RecommendationCount,
    (SELECT DISTINCT EntityID AS EntityCount FROM Orion.VIM.Recommendations 
        WHERE EntityID=RE.EntityID AND EntityType=0 AND BatchID IS NULL) AS [_EntityCount],
    '/Orion/StatusIcon.ashx?entity=Orion.VIM.Clusters&size=Small&status=' + TOSTRING(C.Status) AS [_IconFor_Name],
    '/ui/recommendations/current?groupId=' + TOSTRING(RE.GroupID) AS [_LinkFor_RecommendationCount],
    (SELECT Count(*) AS OverallCount FROM Orion.VIM.Recommendations WHERE BatchID IS NULL) AS [_OverallCount]
FROM Orion.VIM.Recommendations RE
INNER JOIN Orion.VIM.Clusters C ON RE.EntityID = C.ClusterID
WHERE RE.EntityType = 0 AND RE.BatchID IS NULL
GROUP BY C.Name, C.Status, C.DetailsUrl, RE.EntityType, RE.EntityID, RE.GroupID
UNION(
SELECT
    RE.EntityID AS [_EntityID],
    RE.EntityType AS [_EntityType],
    TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
            WHERE Priority=1 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL)) 
        + ':' + TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
            WHERE Priority=2 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL)) 
        + ':' + TOSTRING((SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
            WHERE Priority=3 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL)) AS [Risk_by_Priority],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=1 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL) AS [_PriorityHigh],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=2 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL) AS [_PriorityMedium],
    (SELECT COUNT(*) AS pr FROM Orion.VIM.Recommendations 
        WHERE Priority=3 AND EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL) AS [_PriorityLow],
    H.HostName,
    H.DetailsUrl AS [_LinkFor_Name],
    Count(*) AS RecommendationCount,
    (SELECT DISTINCT EntityID AS EntityCount FROM Orion.VIM.Recommendations 
        WHERE EntityID=RE.EntityID AND EntityType=1 AND BatchID IS NULL) AS [_EntityCount],
    '/Orion/StatusIcon.ashx?entity=Orion.VIM.Hosts&size=Small&status=' + TOSTRING(H.Status) AS [_IconFor_Name],
    '/ui/recommendations/current?groupId=' + TOSTRING(RE.GroupID) AS [_LinkFor_RecommendationCount],
    (SELECT Count(*) AS OverallCount FROM Orion.VIM.Recommendations WHERE BatchID IS NULL) AS [_OverallCount]
FROM Orion.VIM.Recommendations RE
INNER JOIN Orion.VIM.Hosts H ON RE.EntityID = H.HostID
WHERE RE.EntityType = 1 AND RE.BatchID IS NULL
GROUP BY H.HostName, H.Status, H.DetailsUrl, RE.EntityType, RE.EntityID, RE.GroupID)
ORDER BY _PriorityHigh DESC, _PriorityMedium DESC, _PriorityLow DESC
";
}