﻿using System;
using System.Globalization;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_PotentialIssues : VimBaseResource
{
    private readonly PotentialIssuesDAL _potentialIssuesDAL = new PotentialIssuesDAL();

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_JD0_26; }
    }

    public override string DisplayTitle
    {
        get 
        {
            return base.DisplayTitle.Replace("XX", GetPotentialIssuesCount().ToString(CultureInfo.InvariantCulture));
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected int PotentialIssuesTableUniqueID
    {
        get { return ScriptFriendlyResourceID; }
    }

    protected int WithoutIssuesTableUniqueID
    {
        get { return ScriptFriendlyResourceID * ScriptFriendlyResourceID; }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PotentialIssuesTable.UniqueClientID = PotentialIssuesTableUniqueID;
        PotentialIssuesTable.SWQL = PotentialIssuesSWQL;
        PotentialIssuesTable.OrderBy = PotentialIssuesOrderBy;

        WithoutIssuesTable.UniqueClientID = WithoutIssuesTableUniqueID;
        WithoutIssuesTable.SWQL = WithoutIssuesSWQL;
        WithoutIssuesTable.OrderBy = WithoutIssuesOrderBy;

        Wrapper.ManageButtonText = Resources.CoreWebContent.WEBDATA_BV0_0043;
        Wrapper.ManageButtonTarget = "/Orion/VIM/AllAlertsProxy.aspx?alertName=[All]";
        Wrapper.ShowManageButton = true;
    }

    public string PotentialIssuesSWQL
    {
        get
        {
            return @"SELECT 
                        '/Orion/images/ActiveAlerts/InformationalAlert.png' AS Icon,
                        t.cnt,
                        ac.Name AS Name, 
                        ac.Description
                    FROM Orion.AlertConfigurations ac
                    INNER JOIN 
                        (SELECT 
                            COUNT(aa.AlertObjectID) as cnt, 
                            ao.AlertID as ao_AlertID, 
                            MAX(aa.TriggeredDateTime) as TriggeredDateTime
                         FROM Orion.AlertActive (nolock=true) aa
                         INNER JOIN Orion.AlertObjects (nolock=true) ao
                         ON aa.AlertObjectID=ao.AlertObjectID
                         WHERE ao.RealEntityType LIKE 'Orion.VIM%'
                         GROUP BY ao.AlertID
                         HAVING COUNT(aa.AlertObjectID)>0) t
                    ON ac.AlertID=t.ao_AlertID
                    WHERE Severity=0
                    ORDER BY t.TriggeredDateTime DESC, ac.Name ASC";
        }
    }

    public string PotentialIssuesOrderBy
    {
        get
        {
            return "t.TriggeredDateTime DESC, ac.Name ASC";
        }
    }

    public string WithoutIssuesSWQL
    {
        get
        {
            return @"SELECT DISTINCT 
                        '/Orion/Admin/Accounts/images/icons/ok_enabled.png' AS Icon,
                        0 AS cnt,
                        ac.Name, 
                        ac.Description
                     FROM Orion.AlertConfigurations ac
                     LEFT OUTER JOIN Orion.AlertObjects ao ON ac.AlertID = ao.AlertID
                     WHERE ao.AlertID IS NULL AND ac.ObjectType LIKE 'Virtual%' AND ac.Enabled=1
                     ORDER BY ac.Name ASC";
        }
    }

    public string WithoutIssuesOrderBy
    {
        get
        {
            return "ac.Name ASC";
        }
    }

    protected int GetPotentialIssuesCount()
    {
        return _potentialIssuesDAL.GetPotentialIssuesCount();
    }

    protected int GetWithoutIssuesCount()
    {
        return _potentialIssuesDAL.GetWithoutIssuesCount();
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionVMPHPotentialIssuesResource";
        }
    }
}
