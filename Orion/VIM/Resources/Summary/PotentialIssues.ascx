<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PotentialIssues.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_PotentialIssues" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<style type="text/css">
    .sw-vim-alert-icon {
        width: 23px;
        text-align: center;
    }
    .sw-vim-alert-cnt {
        font-weight: bold;
        width: 23px;
        text-align: center;
    }
    .sw-vim-alert-name {
        max-width: 30%;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    .sw-vim-alert-description {
        max-width: 350px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding-right: 8px;
    }
    .sw-vim-semi-title {
        font-size: 100%;
        margin-bottom: 8px;
        font-weight: normal;
    }
    table#Grid-<%= PotentialIssuesTable.UniqueClientID %> {
        table-layout: fixed;
    }
    #Grid-<%= PotentialIssuesTable.UniqueClientID %> td {
        background-color: #f2f2f2;
        vertical-align: central;
    }
    table#Grid-<%= WithoutIssuesTable.UniqueClientID %> {
        table-layout: fixed;
    }
    #Grid-<%= WithoutIssuesTable.UniqueClientID %> td {
        background-color: #DAF4CF;
        color: gray;
        vertical-align: central;
    }
</style>
 
<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div ID="sw-vim-SuccessMessageDiv" class="sw-suggestion sw-suggestion-pass hidden"><span class="sw-suggestion-icon"></span><span id="sw-SuccessMessage"><%= VIMWebContent.VIMWEBDATA_JD0_28 %></span><br/></div>
        <orion:CustomQueryTable runat="server" ID="PotentialIssuesTable"/>
        <h4 ID="sw-vim-WithoutIssuesDiv" class="sw-vim-semi-title hidden"></h4>
        <orion:CustomQueryTable runat="server" ID="WithoutIssuesTable"/>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                {
                    uniqueId: <%= PotentialIssuesTable.UniqueClientID %>,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                    onLoad: function () {
                        $('#Grid-<%= PotentialIssuesTable.UniqueClientID %> .HeaderRow').hide();
                        setPotentialIssuesNumbers();
                    },
                    columnSettings: {
                        "Icon": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-icon';
                            },
                            formatter: function (value) { return '<img src="' + value + '" />'; },
                            isHtml: true
                        },
                        "cnt": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-cnt';
                            }
                        },
                        "Name": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-name';
                            },
                            formatter: function(value) {
                                return '<a href="/Orion/VIM/AllAlertsProxy.aspx?alertName=' + encodeURIComponent(value) + '">' + value + '</a>';
                            },
                            isHtml: true
                        },
                        "Description": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-description';
                            },
                            formatter: function(value, row) {
                                return '<a href="/Orion/VIM/AllAlertsProxy.aspx?alertName=' + encodeURIComponent(row[2]) + '"><span title=\'' + value + '\'>' + value + '</span></a>';
                            },
                            isHtml: true
                        }
                    }
                });
                
                SW.Core.Resources.CustomQuery.initialize(
                {
                    uniqueId: <%= WithoutIssuesTable.UniqueClientID %>,
                    rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                    onLoad: function () {
                        $('#Grid-<%= WithoutIssuesTable.UniqueClientID %> .HeaderRow').hide();
                        setWithoutIssuesNumber();
                    },
                    columnSettings: {
                        "Icon": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-icon';
                            },
                            formatter: function (value) { return '<img src="' + value + '" />'; },
                            isHtml: true
                        },
                        "cnt": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-cnt';
                            }
                        },
                        "Name": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-name';
                            }
                        },
                        "Description": {
                            cellCssClassProvider: function() {
                                return 'sw-vim-alert-description';
                            },
                            formatter: function(value) {
                                return '<span title=\'' + value + '\'>' + value + '</span>';
                            },
                            isHtml: true
                        }
                    }
                });
 
                
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() {
                    SW.Core.Resources.CustomQuery.refresh(<%= PotentialIssuesTableUniqueID %>);
                    SW.Core.Resources.CustomQuery.refresh(<%= WithoutIssuesTableUniqueID %>);
                };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= PotentialIssuesTable.ClientID %>');
                SW.Core.View.AddOnRefresh(refresh, '<%= WithoutIssuesTable.ClientID %>');
                // And finally loads initial data
                refresh();
                

                // Change resource subtitle by adding labels with attached tooltips
                (function addSubtitleLables() {
                    // subtitle is changed already
                    if ($("div.ResourceWrapper[resourceid=<%= Resource.ID %>] .sw-vim-subtitleContainer").length > 0) {
                        return;
                    }

                    var subtitle = $("div.ResourceWrapper[resourceid=<%= Resource.ID %>] h2:first");
                
                    // save original subtitle
                    var leftSubtitle = subtitle.css("float", "left")[0].outerHTML;
                    var rightSubtitle = 
                        "<div style='float: right;'>" +
                            "<span class='sw-vim-tooltipLabel' alertLegend><%= VIMWebContent.VIMWEBDATA_SH0_5%></span>" + 
                        "</div>";                      

                    subtitle.replaceWith("<div class='sw-vim-subtitleContainer'>" + leftSubtitle + rightSubtitle + "</div>");
                
                    var legendTooltipMarkup =
                        "<table>" +
                            "<tr>" +
                                "<td colspan='2'><b><%= VIMWebContent.VIMWEBDATA_SH0_7%></b></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Critical.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_8%></td>" +
                                "<td><img src='/Orion/images/ActiveAlerts/InformationalAlert.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_11%></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Serious.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_9%></td>" +
                                "<td><img src='/Orion/images/ActiveAlerts/Notice.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_12%></td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td colspan='2'><img src='/Orion/images/ActiveAlerts/Warning.png' />&nbsp;<%= VIMWebContent.VIMWEBDATA_SH0_10%></td>" + 
                            "</tr>" +
                        "</table>";
                    
                    $(".sw-vim-tooltipLabel[alertLegend]").attr("title", legendTooltipMarkup);
                    $(".sw-vim-tooltipLabel").tooltip({
                        id: "sw-vim-allAlertsResourceTooltip"
                    });
                })();

                function setPotentialIssuesNumbers() {
                    var itemCounts = $("#Pager-<%= PotentialIssuesTableUniqueID %> div:first").text();
                    var potentialIssues = itemCounts.substr(itemCounts.trimEnd().lastIndexOf(' ')).trim(); //get number of items from pager
                    
                    if (potentialIssues == 0 || isNaN(potentialIssues))
                    {
                        $("#sw-vim-SuccessMessageDiv").removeClass('hidden');
                    }
                };
                
                function setWithoutIssuesNumber() {
                    var itemCounts = $("#Pager-<%= WithoutIssuesTableUniqueID %> div:first").text();
                    var withoutIssues = itemCounts.substr(itemCounts.trimEnd().lastIndexOf(' ')).trim(); //get number of items from pager
                    
                    if (withoutIssues > 0)
                    { 
                        $("#sw-vim-WithoutIssuesDiv").text(SW.Core.String.Format("<%= Resources.VIMWebContent.VIMWEBDATA_JD0_27 %>", withoutIssues));
                        $("#sw-vim-WithoutIssuesDiv").removeClass('hidden');
                    }
                };
                
            });
        </script>
    </Content>
</vman:resourceWrapper>

