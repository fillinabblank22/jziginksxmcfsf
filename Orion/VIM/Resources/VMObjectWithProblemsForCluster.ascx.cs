﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Web.Resources;
using System.Data;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.VIM.Web.Providers;

public partial class Orion_VIM_Resources_VMObjectWithProblemsForCluster : VimVMObjectsWithProblemsControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        impl.Resource = this;
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get {
            return new Type[] { typeof(IVimClusterProvider) };
        }
    }

    protected override int GetParentId()
    {

        return this.GetInterfaceInstance<IVimClusterProvider>().Cluster.Id;
           
    }

}
