﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL.Interfaces;
using SolarWinds.VIM.Web.Helpers;

public partial class Orion_VIM_Resources_Problems_VMObjectWithProblemsEditPage : System.Web.UI.Page
{

    #region Resource property
    private ResourceInfo resource;

    protected ResourceInfo Resource
    {
        get { return this.resource; }
        set { this.resource = value; }
    }

    private String netObjectID;
    protected String NetObjectID
    {
        get { return this.netObjectID; }
        set { this.netObjectID = value; }

    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(this.Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this.Resource = ResourceManager.GetResourceByID(resourceID);

        }
        if (!String.IsNullOrEmpty(this.Request.QueryString["NetObjectID"]))
        {
            this.NetObjectID = Request.QueryString["NetObjectID"];
        }




        if (!this.IsPostBack)
        {
            int max;
            if (!Int32.TryParse(this.Resource.Properties["MaxRecords"], out max))
            {
                this.Resource.Properties["MaxRecords"] = VimVMObjectsWithProblemsControl.DefaultMaxRecords.ToString();
            }
            this.maxCount.Text = this.Resource.Properties["MaxRecords"];

            if (!Int32.TryParse(this.Resource.Properties["StatusFilter"], out max))
            {
                this.Resource.Properties["StatusFilter"] = ((int)(VimVMObjectsWithProblemsControl.DefaultFilter)).ToString();
            }
            var filter = (VimManagedStatusFlagged)(int.Parse(this.Resource.Properties["StatusFilter"]));

            downSelected.Checked = ((filter & VimManagedStatusFlagged.Down) != 0);
            upSelected.Checked = ((filter & VimManagedStatusFlagged.Up) != 0);
            warningSelected.Checked = ((filter & VimManagedStatusFlagged.Warning) != 0);
            unknownSelected.Checked = ((filter & VimManagedStatusFlagged.Unknown) != 0);





            if (!string.IsNullOrEmpty(Resource.Properties["Title"]))
            {
                this.resourceTitleEditor.ResourceTitle = Resource.Properties["Title"];
            }
            else
            {
                this.resourceTitleEditor.ResourceTitle = string.Empty;
            }

            if (!string.IsNullOrEmpty(Resource.Properties["SubTitle"]))
            {
                this.resourceTitleEditor.ResourceSubTitle = Resource.Properties["SubTitle"];
            }
            else
            {
                this.resourceTitleEditor.ResourceSubTitle = string.Empty;
            }

            ucFilterByPlatform.Visible = String.IsNullOrEmpty(this.Request.QueryString["PlatformFilter"]) ? false : this.Request.QueryString["PlatformFilter"].ToLowerInvariant() == Boolean.TrueString.ToLowerInvariant();

            if (ucFilterByPlatform.Visible)
            {
                ucFilterByPlatform.LoadFromResourceProperties(this.Resource.Properties);
            }
        }

        // display GuestType (vCenter/Cluster/ .....) instead of generic 'Node'
    }

    protected VimVMObjectsWithProblemsControl.VMObjectType ControlObjectType()
    {

        string rv = Resource.Properties["VMObjectType"];
        if (string.IsNullOrEmpty(rv))
        {
            return VimVMObjectsWithProblemsControl.DefaultVMObjectType;
        }
        else
        {
            return (VimVMObjectsWithProblemsControl.VMObjectType)Enum.Parse(typeof(VimVMObjectsWithProblemsControl.VMObjectType), rv);
        }
    }



    protected void SubmitClick(object sender, EventArgs e)
    {
        if (this.Page.IsValid)
        {
            this.Resource.Properties["Title"] = this.resourceTitleEditor.ResourceTitle;
            this.Resource.Properties["SubTitle"] = this.resourceTitleEditor.ResourceSubTitle;

            this.Resource.Properties["MaxRecords"] = this.maxCount.Text;

            VimManagedStatusFlagged filter =
                ((downSelected.Checked ? VimManagedStatusFlagged.Down | VimManagedStatusFlagged.Critical : 0) |
                (upSelected.Checked ? VimManagedStatusFlagged.Up : 0) |
                (warningSelected.Checked ? VimManagedStatusFlagged.Warning : 0) |
                (unknownSelected.Checked ? (VimManagedStatusFlagged.Unknown | VimManagedStatusFlagged.Undefined) : 0));

            this.Resource.Properties["StatusFilter"] = ((int)filter).ToString();

            if (ucFilterByPlatform.Visible)
            {
                ucFilterByPlatform.SaveProperties(this.Resource.Properties);
            }

            if (!String.IsNullOrEmpty(this.NetObjectID))
            {
                Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}&NetObject={1}", this.Resource.View.ViewID, this.NetObjectID));
            }
            else
            {
                Response.Redirect(String.Format("/Orion/View.aspx?ViewID={0}", this.Resource.View.ViewID));
            }
        }
    }
}