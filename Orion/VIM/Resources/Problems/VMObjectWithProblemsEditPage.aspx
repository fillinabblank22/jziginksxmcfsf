﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="VMObjectWithProblemsEditPage.aspx.cs" Inherits="Orion_VIM_Resources_Problems_VMObjectWithProblemsEditPage" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Extensions" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register src="~/Orion/VIM/Controls/EditControls/FilterByPlatform.ascx" tagname="FilterByPlatform" tagprefix="vim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <orion:Include runat="server" Module="VIM" File="VIM.css" />
    <orion:Include runat="server" File="Events.css" />
    <orion:Include runat="server" File="WebEngine/Resources/SlateGray.css" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        
    <orion:Refresher ID="Refresher1" runat="server" />
  
    <div style="padding-left: 10px;">
        <br />
        <h1 style="padding-left: 0px;">
            <span>
                <%= string.Format(Resources.VIMWebContent.VIMWEBDATA_AK0_41, CommonWebHelper.EncodeHTMLTags(Macros.ParseMacros(this.Resource.Title, Request["netobject"].UrlEncode())))  %>
            </span>
        </h1>
        <orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="true" ShowSubTitleHintMessage="false" />
        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_64%>
        <br /><br />
        <b><%= Resources.VIMWebContent.VIMWEBDATA_AK0_43%></b><br />
        <asp:TextBox runat="server" ID="maxCount" Columns="5"/>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="maxCount"
                            SetFocusOnError="true">*</asp:RequiredFieldValidator>
        <asp:RangeValidator ID="integerValueValidate" runat="server" ControlToValidate="maxCount"
                            Display="Dynamic" Type="Integer" MaximumValue="10000" MinimumValue="1" ErrorMessage="<%$ Resources:VIMWebContent,VIMWEBDATA_VB0_65%>"
                            SetFocusOnError="true"></asp:RangeValidator>
        <br /><br />
        <div>
            <b><%= Resources.VIMWebContent.VIMWEBDATA_AK0_44%></b>
        </div>
        <div>
            <asp:CheckBox ID="upSelected" runat="server" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_AK0_37%>" /><br />
            <asp:CheckBox ID="warningSelected" runat="server" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_AK0_38%>" /><br />
            <asp:CheckBox ID="downSelected" runat="server" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_AK0_39%>" /><br />
            <asp:CheckBox ID="unknownSelected" runat="server" Text="<%$ Resources:VIMWebContent,VIMWEBDATA_AK0_40%>" /><br />
        </div>
        <div>
            <vim:FilterByPlatform ID="ucFilterByPlatform" runat="server" />
        </div>
        <br /><br />
        <div class="sw-btn-bar">
            <orion:LocalizableButton id="btnSubmit" runat="server" DisplayType="Primary" LocalizedText="Submit"  OnClick="SubmitClick"/>      
        </div>              
        <br /><br />
    </div>
</asp:Content>
