﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Licensing.Framework;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Common.Licensing;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManVolumeDetails_VManVolumeDetails : VimBaseResource
{
    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new [] { typeof(IVolumeProvider) }; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] {typeof (IVolumeProvider)}; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected override String DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_TK0_19; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualizationVolumeDetail"; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!LoadData())
        {
            Visible = false;
            return;
        }

        Wrapper.ManageButtonText = Resources.VIMWebContent.VIMWEBCODE_TK0_18;
        Wrapper.ShowManageButton = true;
        Wrapper.ManageButtonTarget = "/Orion/VIM/VManStorageSummary.aspx";
    }

    /// <summary>
    /// Loads details of volume.
    /// </summary>
    private bool LoadData()
    {
        if (!SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
            return false;

        var interfaceInstance = GetInterfaceInstance<IVolumeProvider>();
        if (interfaceInstance == null)
            return false;

        Volume volume = interfaceInstance.Volume;
        DataTable virtualDiskAndDatastore = new VirtualMachineVolumeDAL().GetVirtualDiskAndDatastoreByCoreVolume(volume.VolumeID);

        if (virtualDiskAndDatastore == null || virtualDiskAndDatastore.Rows.Count < 1)
            return false;

        //at this point we know, we have some data
        DataRow row = virtualDiskAndDatastore.Rows[0];

        String datastoreLinkHtml = EntityLinkHelper.GetDatastoreEntityLink(row["DatastoreDetailsUrl"],
            row["DataStoreName"], row["DataStoreStatus"], true);
        litDatastore.Text = datastoreLinkHtml;


        String virtualDisk = EntityLinkHelper.GetVirtualDiskLink(null, row["VirtualDiskLabel"],
            VimManagedStatus.WithoutStatus, true);
        litVirtualDisk.Text = virtualDisk;

        litVolume.Text = volume.Name;
        return true;
    }
}