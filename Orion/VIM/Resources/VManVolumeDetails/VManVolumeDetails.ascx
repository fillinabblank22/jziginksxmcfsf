﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeFile="VManVolumeDetails.ascx.cs" Inherits="Orion_VIM_Resources_VManVolumeDetails_VManVolumeDetails" %>
<orion:Include runat="server" Module="VIM" File="VIM.css" />

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true">
    <Content>
  
        <table class="sw-vman-infotable NeedsZebraStripes">
            <tr>
                <th><%= Resources.VIMWebContent.VIMWEBCODE_TK0_20%></th>    
                <td><asp:Literal runat="server" ID="litVolume" /></td>
            </tr>
            <tr>
                <th><%= Resources.VIMWebContent.VIMWEBDATA_LC0_43%></th>    
                <td><asp:Literal runat="server" ID="litDatastore" /></td>
            </tr>
            <tr>
                <th><%= Resources.VIMWebContent.VIMWEBDATA_TK0_4%></th>    
                <td><asp:Literal runat="server" ID="litVirtualDisk" /></td>
            </tr>
        </table>   

    </Content>
</orion:resourceWrapper>

