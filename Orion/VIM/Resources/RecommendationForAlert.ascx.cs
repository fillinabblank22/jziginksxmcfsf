﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Alerts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_Virtualization_RecommendationForAlert : VimBaseResource
{
    protected string RecommendationMessage;
    protected string DetailsLink;
    protected string TriggeringObjectName;

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_JD0_29; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionVMPHRecommendationFromVMan";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Static; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!LicenseManager.Instance.IsVimFullyLicensed())
        {
            Visible = false;
            return;
        }

        RecommendationResourceWrapper.ShowEditButton = false;

        var result = new RecommendationForAlertDAL().GetAlertDetails(NetObjectId);

        if (result.Rows.Count != 1 || result.Rows[0].IsNull("Recommendation"))
        {
            Visible = false;
            return;
        }

        RecommendationMessage = (string)result.Rows[0]["Recommendation"];
        if (string.IsNullOrEmpty(RecommendationMessage))
        {
            Visible = false;
            return;
        }

        DetailsLink = (string)result.Rows[0]["EntityDetailsUrl"];
        TriggeringObjectName = (string)result.Rows[0]["EntityCaption"];
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IActiveAlertProvider) }; }
    }
}
