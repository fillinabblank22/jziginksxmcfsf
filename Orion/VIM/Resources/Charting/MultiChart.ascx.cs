﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Common.Constants;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.Charting2;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.UI.Formatters;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Charting_MultiChart : VimMultiChartBaseResource, IResourceIsInternal
{
    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] {typeof (INodeProvider), typeof (IVimHostProvider), typeof (IVimVirtualMachineProvider)}; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Resource.Properties["yUnits"] = string.Join(";", Entities.Select(m => m.UnitFormatter));

        // chart width and height
        this.Width = this.Resource.Width - NameColumnWidth - LastValueColumnWidth - 30 - 2; // resource width - name cell width - last value cell width - resource padding - column padding

        this.Height = this.ForceDisplayOfNeedsEditPlaceholder ? 125 : 5 + 26 + 35; // chart top margin + range selector + scrollbar

        if (this.Page is ITimePeriodProvider && ((ITimePeriodProvider)this.Page).TimePeriodIsSetByUser() && ((ITimePeriodProvider)this.Page).TimePeriodStartDate.HasValue && ((ITimePeriodProvider)this.Page).TimePeriodEndDate.HasValue)
        {
            DateTime dateStart = ((ITimePeriodProvider)this.Page).TimePeriodStartDate.Value;
            DateTime dateEnd = ((ITimePeriodProvider)this.Page).TimePeriodEndDate.Value;
            Resource.SubTitle = string.Format(CultureInfo.CurrentCulture, "{0:f} - {1:f}", dateStart, dateEnd);
        }
        else
        {
            int dateSpan;
            if (int.TryParse(Resource.Properties["chartdatespan"], out dateSpan))
            {
                Resource.SubTitle = string.Format(CultureInfo.CurrentCulture, "{0:f} - {1:f}", DateTime.Now.AddDays(-dateSpan), DateTime.Now);
            }
        }

        if (this.Entities != null)
        {
            this.Height += this.Entities.Count * CounterDivHeight;
        }

        this.pnlLeft.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;
        this.pnlRight.Visible = !this.ForceDisplayOfNeedsEditPlaceholder;

        if (!this.ForceDisplayOfNeedsEditPlaceholder)
        {
            pnlMiddle.Style.Add("width", this.Width + "px");
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);

        if (this.Entities != null)
        {
            this.rptEntities.DataSource = this.Entities;
            this.rptEntities.DataBind();

            if (this.Entities.Count > 0)
            {
                this.ForceDisplayOfNeedsEditPlaceholder = false;
            }
        }

        if (Resource.Properties.ContainsKey("ChartType"))
            switch (Resource.Properties["ChartType"])
            {
                case "IO":
                {
                    if (this.ViewDetectionHelper.DetectedViewType != ViewType.Guest)
                    {
                        // hide resource on views where it should not be visible (all views other than VM)
                        this.Visible = false;
                    }

                    break;
                }
                case "Resource":
                {
                    if (this.ViewDetectionHelper.DetectedViewType != ViewType.Guest && this.ViewDetectionHelper.DetectedViewType != ViewType.Host &&
                        this.ViewDetectionHelper.DetectedViewType != ViewType.Cluster)
                    {
                        // hide resource on views where it should not be visible (all views other than VM and Host)
                        this.Visible = false;
                    }
                    else if (this.ViewDetectionHelper.DetectedViewType == ViewType.Cluster)
                    {
                        ClusterDAL clusterDal = new ClusterDAL();
                        var platform = clusterDal.GetClusterModel(this.ViewDetectionHelper.NodeId).Platform;
                        if (platform == VirtualizationPlatform.HyperV || 
                            platform == VirtualizationPlatform.Vmware && !clusterDal.GetVSanId(this.ViewDetectionHelper.NodeId).HasValue)
                        {
                            Visible = false;
                        }
                    }

                    break;
                }
            }

        HideByPlatform();
        OrionInclude.ModuleFile("VIM", "Charts/Charts.Vim.Chart.Formatters.js", OrionInclude.Section.Middle);
    }

    public override string DisplayTitle
    {
        get
        {
            if (string.IsNullOrEmpty(Title) || Title == DefaultTitle)
            {
                if (ViewDetectionHelper.DetectedViewType == ViewType.Cluster)
                {
                    ClusterDAL clusterDal = new ClusterDAL();
                    var platform = clusterDal.GetClusterModel(ViewDetectionHelper.NodeId).Platform;
                    if (platform == VirtualizationPlatform.Vmware)
                    {
                        return Resources.VIMWebContent.ResourceUtilization_VSAN_DefaultTitle;
                    }

                    if (platform == VirtualizationPlatform.Nutanix)
                    {
                        return Resources.VIMWebContent.ResourceUtilization_Nutanix_DefaultTitle;
                    }
                }
            }

            return base.DisplayTitle;
        }
    }

    protected override IEnumerable<VimMultipleChartEntity> LoadEntities()
    {
        switch (this.ViewDetectionHelper.DetectedViewType)
        {
            case ViewType.Host:
                return LoadEntitiesForThisHost(ViewDetectionHelper.NodeId);
            case ViewType.Guest:
                return LoadEntitiesForThisVM(ViewDetectionHelper.NodeId);
            case ViewType.Cluster:
                return LoadEntitiesForThisCluster(ViewDetectionHelper.NodeId);
            default:
                return Enumerable.Empty<VimMultipleChartEntity>();
        }
    }

    protected string GetPerfStackLink()
    {
        if (ViewDetectionHelper == null)
            return string.Empty;

        switch (ViewDetectionHelper.DetectedViewType)
        {
            case ViewType.Host:
                return PerfStackLinkBuilder.GetHostLink(ViewDetectionHelper.NodeId);
            case ViewType.Guest:
                var virtualMachineDal = new VirtualMachineDAL();
                return PerfStackLinkBuilder.GetVirtualMachineLink(ViewDetectionHelper.NodeId, virtualMachineDal.GetPlatformByVirtualMachineID(ViewDetectionHelper.NodeId));
            case ViewType.Cluster:
            {
                var clusterDal = new ClusterDAL();
                var platform = clusterDal.GetClusterModel(ViewDetectionHelper.NodeId).Platform;
                return PerfStackLinkBuilder.GetClusterStorageLink(ViewDetectionHelper.NodeId, platform);
            }
            default:
                return string.Empty;
        }
    }

    private IEnumerable<VimMultipleChartEntity> LoadEntitiesForThisVM(int virtualMachineId)
    {
        if (Resource.Properties.ContainsKey("ChartType") && Resource.Properties["ChartType"] == "IO")
            return LoadIOEntitiesForThisVM(virtualMachineId);
        return LoadResEntitiesForThisVM(virtualMachineId);
    }

    private IEnumerable<VimMultipleChartEntity> LoadEntitiesForThisHost(int hostId)
    {
        if (Resource.Properties.ContainsKey("ChartType") && Resource.Properties["ChartType"] == "IO")
            return LoadIOEntitiesForThisHost(hostId);
        return LoadResEntitiesForThisHost(hostId);
    }

    private IEnumerable<VimMultipleChartEntity> LoadEntitiesForThisCluster(int clusterId)
    {
        var clusterDal = new ClusterDAL();
        var cluster = clusterDal.GetClusterModel(this.ViewDetectionHelper.NodeId);

        if (cluster.Platform == VirtualizationPlatform.Vmware)
        {
            var vsanId = clusterDal.GetVSanId(clusterId);
            if (vsanId.HasValue)
            {
                return CreateVsanIoPerfMetrics(SwisEntityNames.Vim.ClusterStorageStatistics, "Cluster.ClusterId", clusterId, false, VimCluster.Prefix);
            }
        }
        else if (cluster.Platform == VirtualizationPlatform.Nutanix)
        {
            return CreateNutanixClusterStorageMetrics(SwisEntityNames.Vim.ClusterStorageStatistics, "Cluster.ClusterId", clusterId);
        }

        return Enumerable.Empty<VimMultipleChartEntity>();
    }

    private List<VimMultipleChartEntity> CreateNutanixHostStorageMetrics(string swisEntityName, string swisEntityIdName, long objectId)
    {
        return new List<VimMultipleChartEntity>
        {
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_ReadIOPS,
                Counter = "IOPSRead",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_WriteIOPS,
                Counter = "IOPSWrite",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_TotalLatency,
                Counter = "LatencyTotal",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.MilliSec
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_ReadThroughput,
                Counter = "ThroughputRead",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_WriteThroughput,
                Counter = "ThroughputWrite",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            }
        };
    }

    private List<VimMultipleChartEntity> CreateNutanixClusterStorageMetrics(string swisEntityName, string swisEntityIdName, int objectId)
    {
        return new List<VimMultipleChartEntity>
        {
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_ReadIOPS,
                Counter = "IOPSRead",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_WriteIOPS,
                Counter = "IOPSWrite",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_ReadLatency,
                Counter = "LatencyRead",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.MilliSec
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_WriteLatency,
                Counter = "LatencyWrite",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.MilliSec
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_ReadThroughput,
                Counter = "ThroughputRead",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.ResourceUtilization_Nutanix_WriteThroughput,
                Counter = "ThroughputWrite",
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            }
        };
    }

    private IEnumerable<VimMultipleChartEntity> LoadResEntitiesForThisHost(int hostId)
    {
        HostDAL hostDAL = new HostDAL();


        // todo:: Do we really want this to say "Top VM"? That's not very descriptive and could be any vm.
        // how will the user be able to tell which is the problem vm? SAM version allows you to click on the entity,
        // we could provide the same kind of link


        var entities = new List<VimMultipleChartEntity>
        {
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_39,
                Counter = "AvgCpuLoad",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.Percent
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_40,
                Counter = "AvgCpuLoad",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.Percent
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_41,
                Counter = "AvgMemUsage",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.Percent
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_42,
                Counter = "AvgMemoryUsage",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.Percent
            },
            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_43,
                Counter = "AvgNetworkUtilization",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.Percent
            }
        };

        if (hostDAL.GetHostPlatformByHostId(hostId) != VirtualizationPlatform.Nutanix)
        {
            entities.Add(new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_44,
                Counter = "AvgNetworkUsageRate",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.KBytesPerSecond
            });
        }

        entities.Add(new VimMultipleChartEntity
        {
            Name = Resources.VIMWebContent.VIMWEBCODE_LC0_45,
            Counter = "AvgIOPSTotal",
            NetObjectPrefix = VimVirtualMachine.Prefix,
            ObjectId = 0,
            UnitFormatter = ChartFormatter.PerSecond
        });
        entities.Add(new VimMultipleChartEntity
        {
            Name = Resources.VIMWebContent.VIMWEBCODE_LC0_46,
            Counter = "AvgLatencyTotal",
            NetObjectPrefix = VimVirtualMachine.Prefix,
            ObjectId = 0,
            UnitFormatter = ChartFormatter.MilliSec,
        });

        if (hostDAL.IsHostUnderVsan(hostId))
        {
            entities.AddRange(CreateVsanIoPerfMetrics(SwisEntityNames.Vim.HostStorageStatistics, "Host.HostId", hostId, true, VimHost.VsanPrefix));
        }
        else
        {
            var platform = hostDAL.GetHostPlatformByHostId(hostId);
            if (platform == VirtualizationPlatform.Nutanix)
            {
                entities.AddRange(CreateNutanixHostStorageMetrics(SwisEntityNames.Vim.HostStorageStatistics, "Host.HostId", hostId));
            }
        }

        return entities;
    }

    private List<VimMultipleChartEntity> CreateVsanIoPerfMetrics(string swisEntityName, string swisEntityIdName, long objectId, bool forHost, string netObjectPrefix)
    {
        return new List<VimMultipleChartEntity>
        {
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_ReadIOPS : Resources.VIMWebContent.ResourceUtilization_vSan_ReadIOPS,
                Counter = "IOPSRead",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_WriteIOPS : Resources.VIMWebContent.ResourceUtilization_vSan_WriteIOPS,
                Counter = "IOPSWrite",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.PerSecond
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_ReadLatency : Resources.VIMWebContent.ResourceUtilization_vSan_ReadLatency,
                Counter = "LatencyRead",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.MilliSec
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_WriteLatency : Resources.VIMWebContent.ResourceUtilization_vSan_WriteLatency,
                Counter = "LatencyWrite",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.MilliSec
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_ReadThroughput : Resources.VIMWebContent.ResourceUtilization_vSan_ReadThroughput,
                Counter = "ThroughputRead",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_WriteThroughput : Resources.VIMWebContent.ResourceUtilization_vSan_WriteThroughput,
                Counter = "ThroughputWrite",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.BytesPerSecond
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_Congestions : Resources.VIMWebContent.ResourceUtilization_vSan_Congestions,
                Counter = "Congestions",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.Statistic
            },
            new VimMultipleChartEntity
            {
                Name = forHost ? Resources.VIMWebContent.HostResourceUtilization_vSan_OutstandingIO : Resources.VIMWebContent.ResourceUtilization_vSan_OutstandingIO,
                Counter = "OutstandingIO",
                NetObjectPrefix = netObjectPrefix,
                SwisEntityName = swisEntityName,
                SwisEntityIdName = swisEntityIdName,
                ObjectId = objectId,
                UnitFormatter = ChartFormatter.Statistic
            }
        };
    }

    private IEnumerable<VimMultipleChartEntity> LoadResEntitiesForThisVM(int virtualMachineId)
    {
        var entities = new List<VimMultipleChartEntity>();

        bool isNutanix = new VirtualMachineDAL().GetPlatformByVirtualMachineID(virtualMachineId) == VirtualizationPlatform.Nutanix;

        var e1 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_47,
                Counter = "AvgCpuLoad",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.Percent
            };

        entities.Add(e1);

        var eh1 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_48,
                Counter = "AvgCpuLoad",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.Percent
            };

        entities.Add(eh1);

        var e2 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_49,
                Counter = "AvgMemoryUsage",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.Percent
            };

        entities.Add(e2);

        var eh2 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_50,
                Counter = "AvgMemUsage",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.Percent
            };

        entities.Add(eh2);

        if (!isNutanix)
        {
            var e3 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_51,
                Counter = "AvgNetworkUsageRate",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.KBytesPerSecond
            };

            entities.Add(e3);
        }

        var eh3 = new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_LC0_52,
                Counter = "AvgNetworkUtilization",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = 0,
                UnitFormatter = ChartFormatter.Percent
            };

        entities.Add(eh3);

        var e4 = new VimMultipleChartEntity
        {
            Name = Resources.VIMWebContent.VIMWEBCODE_LC0_53,
            Counter = "AvgIOPSTotal",
            NetObjectPrefix = VimVirtualMachine.Prefix,
            ObjectId = virtualMachineId,
            UnitFormatter = ChartFormatter.PerSecond
        };

        entities.Add(e4);

        var e5 = new VimMultipleChartEntity
        {
            Name = Resources.VIMWebContent.VIMWEBCODE_LC0_54,
            Counter = "AvgLatencyTotal",
            NetObjectPrefix = VimVirtualMachine.Prefix,
            ObjectId = virtualMachineId,
            UnitFormatter = ChartFormatter.MilliSec
        };

        entities.Add(e5);

        VimMultipleChartEntity e6 = new VimMultipleChartEntity
        {
            Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_12,
            Counter = "AvgCpuReady",
            NetObjectPrefix = VimVirtualMachine.Prefix,
            ObjectId = virtualMachineId,
            UnitFormatter = ChartFormatter.Percent
        };

        entities.Add(e6);

        return entities;
    }

    private IEnumerable<VimMultipleChartEntity> LoadIOEntitiesForThisHost(int hostId)
    {
        //TODO We have no IO statistic for Hosts

        return new List<VimMultipleChartEntity>()
        {

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_6,
                Counter = "AvgIOPSTotal",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_7,
                Counter = "AvgIOPSRead",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_8,
                Counter = "AvgIOPSWrite",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_9,
                Counter = "AvgLatencyTotal",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.MilliSec
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_10,
                Counter = "AvgLatencyRead",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.MilliSec
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_11,
                Counter = "AvgLatencyWrite",
                NetObjectPrefix = VimHost.Prefix,
                ObjectId = hostId,
                UnitFormatter = ChartFormatter.MilliSec
            }
        };
    }

    private IEnumerable<VimMultipleChartEntity> LoadIOEntitiesForThisVM(int virtualMachineId)
    {
        var virtualMachineDal = new VirtualMachineDAL();

        var entities = new List<VimMultipleChartEntity>
        {

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_6,
                Counter = "AvgIOPSTotal",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_7,
                Counter = "AvgIOPSRead",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_8,
                Counter = "AvgIOPSWrite",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.PerSecond
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_9,
                Counter = "AvgLatencyTotal",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.MilliSec
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_10,
                Counter = "AvgLatencyRead",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.MilliSec
            },

            new VimMultipleChartEntity
            {
                Name = Resources.VIMWebContent.VIMWEBCODE_VZ0_11,
                Counter = "AvgLatencyWrite",
                NetObjectPrefix = VimVirtualMachine.Prefix,
                ObjectId = virtualMachineId,
                UnitFormatter = ChartFormatter.MilliSec
            }
        };

        if (virtualMachineDal.GetPlatformByVirtualMachineID(virtualMachineId) == VirtualizationPlatform.Nutanix)
        {
            entities.AddRange(new List<VimMultipleChartEntity>
            {
                new VimMultipleChartEntity
                {
                    Name = Resources.VIMWebContent.VMResourceIOUtilization_TotalThroughput,
                    Counter = "AvgThroughputTotal",
                    NetObjectPrefix = VimVirtualMachine.Prefix,
                    ObjectId = virtualMachineId,
                    UnitFormatter = ChartFormatter.BytesPerSecond
                },
                new VimMultipleChartEntity
                {
                    Name = Resources.VIMWebContent.VMResourceIOUtilization_ReadThroughput,
                    Counter = "AvgThroughputRead",
                    NetObjectPrefix = VimVirtualMachine.Prefix,
                    ObjectId = virtualMachineId,
                    UnitFormatter = ChartFormatter.BytesPerSecond
                },
                new VimMultipleChartEntity
                {
                    Name = Resources.VIMWebContent.VMResourceIOUtilization_WriteThroughput,
                    Counter = "AvgThroughputWrite",
                    NetObjectPrefix = VimVirtualMachine.Prefix,
                    ObjectId = virtualMachineId,
                    UnitFormatter = ChartFormatter.BytesPerSecond
                }
            });
        }

        return entities;
    }

    #region IResourceIsInternal members

    public bool IsInternal
    {
        get { return true; }
    }

    #endregion
}
