﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiChart.ascx.cs" Inherits="Orion_VIM_Resources_Charting_MultiChart" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<orion:Include runat="server" File="VIM/VIM.css"/>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>   
        <div class="sw-vim-mc-container-toolbar">
            <a href="<%=GetPerfStackLink() %>" class="vim-perfstack-icon vim-perfstack-icon-no-margin">
                <%= Resources.VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
            </a> 
        </div>
        <div class="sw-vim-mc-container" style="height: <%= this.Height %>px;">
            <asp:Panel ID="pnlLeft" runat="server" class="sw-vim-mc-left-column sw-vim-mc-header">
                <span class="title">
                    <%=Resources.VIMWebContent.VIMWEBDATA_LC0_71%>
                </span>
            </asp:Panel>
            <asp:Panel ID="pnlMiddle" runat="server" CssClass="sw-vim-mc-middle-column sw-vim-mc-header">
                <!-- This element is used to access ResourceWrapper element for this resource so that 
                we can add extra CSS class to it to fix chart tooltips clipping. (FB109023) -->
                <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
            </asp:Panel>
            <asp:Panel ID="pnlRight" runat="server" class="sw-vim-mc-right-column sw-vim-mc-header">
                <span class="title">
                    <%=Resources.VIMWebContent.VIMWEBDATA_LC0_72%>
                </span>
            </asp:Panel>
            <asp:Repeater ID="rptEntities" runat="server">
                <ItemTemplate>
                    <div class="sw-vim-mc-counter" style="height: <%= this.CounterDivHeight %>px;">
                        <table>
                             <tr>
                                 <td class="name" style="width: <%= this.NameColumnWidth %>px">                                                                                           
                                     <a class="<%# this.Entities[Container.ItemIndex].GetSeriesTag() %>" href="#"><%# AddLineBreaksToString(Eval("Name").ToString()) %> </a>                                                         
                                 </td>
                                 <td class="chart"></td>
                                 <td class="lastValue" style="width: <%= this.LastValueColumnWidth %>px">
                                    <span class="lastValue"></span>
                                 </td>
                             </tr>    
                        </table>  
                        <hr />              
                    </div>                      
                </ItemTemplate>  
            </asp:Repeater>
        </div> 
    </Content>
</vman:resourceWrapper> 
