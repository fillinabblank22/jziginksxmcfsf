﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ComponentVolumes.ascx.cs" Inherits="Orion_VIM_Resources_VMStorageDetails_ComponentVolumes" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="sw-vman-componentvolumes NeedsZebraStripes" cellpadding="2" cellspacing="0">
                    <tr>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_3%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_4%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_5%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_6%></td>
                        <td class="ReportHeader vimReportHeader"></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_TK0_4%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="Property sw-vim-vman-resource-row"><%#Eval("MountPoint")%></td>      
                    <td class="Property sw-vim-vman-resource-row"><asp:PlaceHolder runat="server" ID="phCapacity" /></td>
                    <td class="Property sw-vim-vman-resource-row"><asp:PlaceHolder runat="server" ID="phFreeSpace" /></td>
                    <td class="Property sw-vim-vman-resource-row" style="width: 40px;"><asp:PlaceHolder runat="server" ID="phUsedPercentage" /></td>
                    <td class="Property sw-vim-vman-resource-row"><asp:PlaceHolder runat="server" ID="phUsedPercentBar" /></td>
                    <td class="Property sw-vim-vman-resource-row"><asp:PlaceHolder runat="server" ID="phVirtualDisk" /></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</vman:resourceWrapper>

