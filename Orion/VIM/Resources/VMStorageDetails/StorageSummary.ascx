﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StorageSummary.ascx.cs" Inherits="Orion_VIM_Resources_VMStorageDetails_StorageSummary" %>
<orion:Include runat="server" Module="VIM" File="VIM.css" />
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="sw-vman-storagesummary">
            <tr runat="server" id="trCluster">
                <th><%= Resources.VIMWebContent.VIMWEBDATA_DF0_14%></th>
                <td colspan="4"><asp:Literal runat="server" ID="litCluster" /></td>
            </tr>
            <tr>
                <th><%= Resources.VIMWebContent.VIMWEBDATA_DF0_15%></th>
                <td colspan="4"><asp:Literal runat="server" ID="litHostServer" /></td>
            </tr>
            <asp:Repeater runat="server" ID="repDatastores">
                <ItemTemplate>
                    <tr class="sw-vman-storagesummary-topborder">
                        <th><%= Resources.VIMWebContent.VIMWEBDATA_DF0_16%></th>
                        <td colspan="4"><asp:PlaceHolder runat="server" ID="phDatastoreName" /></td>
                    </tr>
                    <tr class="sw-vman-storagesummary-bar">
                        <td></td>
                        <td><orion:InlineBar runat="server" ID="barDatastoreUsage" /></td>
                        <td><span class="sw-vman-storagesummary-used" style="margin-left: 5px;"><asp:Literal runat="server" ID="litDatastoreUsedSpace" /></span> / 
                            <span class="sw-vman-storagesummary-gray"><asp:Literal runat="server" ID="litDatastoreFreeSpace" /></span> / 
                            <asp:Literal runat="server" ID="litDatastoreSize" /> <%=Resources.VIMWebContent.VIMWEBDATA_TK0_3 %></td>
                        <td><asp:Literal runat="server" ID="litDatastoreIOPS" /> <%=Resources.VIMWebContent.VIMWEBCODE_LC0_14 %></td>
                        <td><span class="sw-vman-storagesummary-gray"><%= Resources.VIMWebContent.VIMWEBCODE_LC0_15%></span> <asp:Literal runat="server" ID="litDatastoreLatency" /></td>
                    </tr>
                    <asp:Repeater runat="server" ID="repLuns">
                        <ItemTemplate>
                            <tr>
                                <th><asp:Literal runat="server" ID="litLunLabel" /></th>
                                <td colspan="4"><asp:Literal runat="server" ID="litLun" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Repeater runat="server" ID="repNas">
                        <ItemTemplate>
                            <tr>
                                <th><asp:Literal runat="server" ID="litNasLabel" /></th>
                                <td colspan="4"><asp:Literal runat="server" ID="litNas" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <div id="sw-vman-storagesummary-legend">
            <table>
                <tr class="sw-vman-storagesummary-bar sw-vman-storagesummary-bar-legend">
                    <td><asp:Literal runat="server" ID="litLegendLabelCapacity"/></td>
                    <td><orion:InlineBar runat="server" ID="barLabelCapacity" /></td>
                    <td><asp:Literal runat="server" ID="litLegendLabelUsed" /></td>
                    <td><orion:InlineBar runat="server" ID="barLabelRem" /></td>
                    <td><asp:Literal runat="server" ID="litLegendLabelRem" /></td>
                </tr>
            </table>
        </div>
    </Content>
</vman:resourceWrapper>