﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IopsAndLatency.ascx.cs" Inherits="Orion_VIM_Resources_VMStorageDetails_IopsAndLatency" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<orion:Include ID="Include1" runat="server" File="GaugeStyle.css" />

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
            <tr>
                <td style="vertical-align: middle;font-size: 13px;">
                    <table class="sw-vman-iops">
                        <tr>
                            <td colspan="3" class="sw-vman-iops-current"><a href="<%=IOPSChartLink%>"><%=LatestIOPS%></a></td>
                        </tr>
                        <tr>
                            <td class="sw-vman-iops-icon"><img src="/Orion/VIM/images/last_hour_icon16x16.png"/></td>
                            <td class="sw-vman-color-black sw-vman-align-right"><%=HourIOPS%></td>
                            <td class="sw-vman-align-left"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_64%></td>
                        </tr>
                        <tr>
                            <td class="sw-vman-iops-icon"><img src="/Orion/VIM/images/last_24hour_icon16x16.png"/></td>
                            <td class="sw-vman-color-black sw-vman-align-right"><%=DayIOPS%></td>
                            <td class="sw-vman-align-left"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_65%></td>
                        </tr>
                        <tr>
                            <td class="sw-vman-iops-icon"><img src="/Orion/VIM/images/last_30days_icon16x16.png"/></td>
                            <td class="sw-vman-color-black sw-vman-align-right"><%=MonthIOPS%></td>
                            <td class="sw-vman-align-left"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_66%></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="sw-vman-color-black" style="padding-top: 12px;"><%=Resources.VIMWebContent.VIMWEBCODE_DF0_4%></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:PlaceHolder runat="server" ID="gaugePlaceHolder" />
                </td>
            </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
</vman:resourceWrapper>

