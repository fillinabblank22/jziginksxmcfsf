﻿using System;
using System.Data;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL.VMan.VirtualMachine;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.VManResources;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Common.Extensions;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VMStorageDetails_IopsAndLatency : VManVMStorageDetailsBaseResource
{
    private DataTable _iopsTable;

    protected string IOPSChartLink
    {
        get
        {
            return String.Format("/Orion/Charts/CustomChart.aspx?chartName=VManIOPSForVirtualMachine&NetObject={0}&Period=Today&ChartTitle={1}", Request["NetObject"].UrlEncode(),
                                 HttpUtility.UrlEncode(Resources.VIMWebContent.VIMWEBCODE_LC0_36)).HtmlEncode();
        }
    }

    private DataTable IOPSInfo
    {
        get { return _iopsTable ?? (_iopsTable = new IopsAndLatencyDAL().GetIops(NodeId)); }
    }

    protected string LatestIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Latest"], 1); } }

    protected string HourIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Hour"], 1); } }

    protected string DayIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Day"], 1); } }

    protected string MonthIOPS { get { return FormatHelper.FormatNumber(IOPSInfo.Rows[0]["Month"], 1); } }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed() || DetectedViewType != ViewType.Guest)
        {
            return;
        }

        string gaugeStyle = GetStringValue("Style", "Elegant Black");
        int gaugeScale = GetIntProperty("Scale", 100);
        string gaugeRangeMethod = GetStringValue("RangeMethod", "dynamic");
        string gaugeRangeValue = String.Empty;

        switch (gaugeRangeMethod)
        {
            case "static":
                gaugeRangeValue = GetIntProperty("StaticRange", 50).ToString();
                break;
            case "dynamic":
                gaugeRangeValue = GetStringValue("DynamicRange", "10, 20, 40, 60, 100, 200, 400, 600, 1000");
                break;
            case "thresholds":
                gaugeRangeValue = GetStringValue("ThresholdsRange", "1.3");
                break;
        }

        string netObject = String.Concat(VimVirtualMachine.Prefix, ":", NodeId);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder,
            gaugeScale,
            gaugeStyle,
            "LATENCY",
            String.Format("/Orion/Charts/CustomChart.aspx?chartName=VManLatencyForVirtualMachine&NetObject={0}&Period=Today&ChartTitle={1}", netObject,
                HttpUtility.UrlEncode(Resources.VIMWebContent.VIMWEBCODE_LC0_37)),
            netObject,
            Resources.VIMWebContent.VIMWEBCODE_DF0_5,
            "Radial",
            gaugeRangeMethod,
            0,
            gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBDATA_VB0_40);
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGauge.ascx"; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_DF0_3;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualMachineIOPSLatency"; }
    }

}