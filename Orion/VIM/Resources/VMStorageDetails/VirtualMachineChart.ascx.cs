﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VMStorageDetails_VirtualMachineChart : VManBaseStorageChart, IResourceIsInternal
{
    private readonly Log _log = new Log();
    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
        if (ViewDetectionHelper.DetectedViewType != ViewType.Guest)
        {
            this.Visible = false;
        }
        else
        {
            HideByPlatform();
        }
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    protected override string NetObjectPrefix
    {
        get { return "N"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { return new[] {typeof (INodeProvider), typeof (IVimVirtualMachineProvider)}; }
    }

    public bool IsInternal { get { return true; } }

    protected override string DefaultTitle
    {
        get
        {
            try
            {
                string netObjectID = Request.Params["NetObject"];

                if (!string.IsNullOrEmpty(netObjectID))
                {
                    NetObject netObject = NetObjectFactory.Create(netObjectID);

                    if (netObject != null)
                    {
                        if (netObject.ObjectProperties.ContainsKey("FullName"))
                        {
                            return netObject["FullName"].ToString();
                        }
                        else if (netObject.ObjectProperties.ContainsKey("Caption"))
                        {
                            return netObject["Caption"].ToString();
                        }
                    }
                }

                return base.DefaultTitle;
            }
            catch (Exception ex)
            {
                _log.Warn("Unable to determine resource title based on netObject.", ex);
                return base.DefaultTitle;
            }
        }
    }
}