﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualDisks.ascx.cs" Inherits="Orion_VIM_Resources_VMStorageDetails_VirtualDisks" %>
<orion:Include runat="server" Module="VIM" File="VIM.css" />
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Register TagPrefix="AjaxTree" Assembly="SolarWinds.VIM.Web" Namespace="SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>

<script type="text/javascript">
    $(function () {
        var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
    </script>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="sw-vman-virtual-disks">
        <table border="0" cellpadding="2" cellspacing="0" width="100%">
            <tr>        
                <td style = "width: 30%;" class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_TK0_4%></td>
                <td style = "width: 31px;" class="ReportHeader vimReportHeader"></td>
                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_45%></td>            
            </tr>
        </table>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Always">
            <ContentTemplate>
                <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
                <AjaxTree:TreeControl ID="virtualDisksNodesTree" runat="server">
                    <WebService Path="/Orion/VIM/Services/VirtualDisksTreeService.asmx" />
                </AjaxTree:TreeControl>
            </ContentTemplate>
        </asp:UpdatePanel>
            </div>
    </Content>
</vman:resourceWrapper>
