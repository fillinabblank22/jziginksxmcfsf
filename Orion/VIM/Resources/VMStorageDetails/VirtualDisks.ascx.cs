﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.AjaxTree;
using SolarWinds.VIM.Web.Controls.VirtualDisksTree;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VMStorageDetails_VirtualDisks : VManVMStorageDetailsBaseResource
{

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualDisksAndRelatedStorage"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_TK0_5; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var manager = ScriptManager.GetCurrent(this.Page);

        if (manager != null)
        {
            manager.Scripts.Add(new ScriptReference(PathResolver.GetVirtualPath("VIM", "TreeControl.js")));
        }

        base.OnPreRender(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool shouldShow = CreateTree(virtualDisksNodesTree);

        if (!shouldShow)
        {
            Visible = false;
        }
        else
        {
            HideWhenEntityNotPolled(nameof(VirtualDisk));
        }
    }

    private bool CreateTree(SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl tree)
    {
        object entityId;

        switch (DetectedViewType)
        {
            case ViewType.Guest:
                entityId = NodeId;
                break;
            default:
                return false;
        }

        var dataProviderIdentifier = String.Format("VIM_VIRTUAL_DISKS_TREE_DATA"); // data provider identifier ! resource id is used to uniquely identify provider in application scope
        var expandedStateProviderIdentifier = String.Format("VIM_VIRTUAL_DISKS_TREE_STATE-{0}-{1}-{2}",
            this.Resource.ID,
            this.Resource.View.ViewID,
            Request.QueryString["NetObject"] ?? String.Empty
            );

        Session[String.Format("{0}-remember", expandedStateProviderIdentifier)] = GetBooleanResourceProperty("RememberExpandedState");

        if (!AppScopeTreeDataManager.ContainsProvider(dataProviderIdentifier))
        {
            AppScopeTreeDataManager.RegisterProvider(dataProviderIdentifier, new VirtualDisksTreeNodeProvider());
        }
        if (!SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
        {
            SessionScopeTreeExpandedStateManager.RegisterProvider(expandedStateProviderIdentifier);
        }

        tree.LoadTree(new VirtualDisksTreeService(), GetPrimaryNode(dataProviderIdentifier, expandedStateProviderIdentifier, entityId));

        return true;
    }

    private object[] GetPrimaryNode(string dataProviderIdentifier, string expandedStateProviderIdentifier, object entityId)
    {
        var definition = new List<object>()
            {
                dataProviderIdentifier,
                expandedStateProviderIdentifier,
                0, // tree level
                (int) VirtualDisksTreeNodeType.Root,
                entityId,
                "root", // path
                String.Empty, // caption
                Resource.ID,
                (int) VirtualizationPlatform.Unknown,
                (int) DetectedViewType
            };

        return definition.ToArray();
    }

    private bool GetBooleanResourceProperty(string key)
    {
        return (this.Resource.Properties[key] ?? "true").ToLowerInvariant() == "true";
    }
}