﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.VManResources;
using SolarWinds.VIM.Web.DAL.VMan.VirtualMachine;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Common.Enums;
using System.Web.UI;
using SolarWinds.VIM.Web.Common.Extensions;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VMStorageDetails_StorageSummary : VManVMStorageDetailsBaseResource
{

    private StorageSummaryDAL _storageSummaryDAL;

    private StorageSummaryDAL DAL
    {
        get { return _storageSummaryDAL ?? (_storageSummaryDAL = new StorageSummaryDAL()); }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            return new[] {typeof (INodeProvider), typeof (IVimVirtualMachineProvider)};
        }
    }

    public override void DataBind()
    {
        if (!Wrapper.CanShowContent || DetectedViewType != ViewType.Guest) 
            return;

        // cluster / host server info
        trCluster.Visible = false;
        var table = DAL.GetStorageInfo(this.NodeId);

        if (table != null && table.Rows.Count > 0)
        {
            DataRow row = table.Rows[0];

            String clusterLinkHtml = EntityLinkHelper.GetClusterEntityLink(row["ClusterDetailsUrl"], row["ClusterName"],
                                                                           row["ClusterStatus"], row["PlatformID"], true);
            litCluster.Text = clusterLinkHtml;
            trCluster.Visible = !String.IsNullOrEmpty(clusterLinkHtml);

            String hostLinkHtml = EntityLinkHelper.GetHostEntityLink(row["HostDetailsUrl"], row["HostName"], row["Status"], row["PlatformID"], true, row);
            litHostServer.Text = hostLinkHtml;
        }

        // datastores
        var datastoresTable = DAL.GetDatastores(this.NodeId);

        var lunDal = new LunDAL();
        var nasDal = new NasDAL();

        datastoresTable.Columns.Add("Luns", typeof (DataTable));
        datastoresTable.Columns.Add("Nas", typeof (DataTable));
        foreach (DataRow row in datastoresTable.Rows)
        {
            int dataStoreId = Convert.ToInt32(row["DataStoreID"]);

            DataTable luns = lunDal.GetDataStoreLuns(dataStoreId);
            row["Luns"] = luns;
            DataTable nas = nasDal.GetDataStoreNas(dataStoreId);
            row["Nas"] = nas;
        }

        repDatastores.DataSource = datastoresTable;
        repDatastores.DataBind();

        FillLegend();
    }

    private void FillLegend()
    {
        litLegendLabelCapacity.Text = String.Concat(Resources.VIMWebContent.VIMWEBDATA_LC0_45, ":");
        litLegendLabelUsed.Text = Resources.VIMWebContent.VIMWEBDATA_TK0_1;
        litLegendLabelRem.Text = Resources.VIMWebContent.VIMWEBDATA_TK0_2;
        barLabelRem.Percentage = 0;
        barLabelCapacity.Percentage = 100;
        barLabelCapacity.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        repDatastores.ItemCreated += new RepeaterItemEventHandler(repDatastores_ItemCreated);
    }

    

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        DataBind();
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHStorageSummary"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_DF0_6; }
    }


    void repDatastores_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    if (e.Item.DataItem is System.Data.DataRowView)
                    {
                        System.Data.DataRow row = (e.Item.DataItem as System.Data.DataRowView).Row;
                        PlaceHolder phDatastoreName = e.Item.FindControl("phDatastoreName") as PlaceHolder;
                        Literal litDatastoreIOPS = e.Item.FindControl("litDatastoreIOPS") as Literal;
                        Literal litDatastoreLatency = e.Item.FindControl("litDatastoreLatency") as Literal;


                        if (phDatastoreName != null)
                            phDatastoreName.Controls.Add(new LiteralControl(EntityLinkHelper.GetDatastoreEntityLink(row["DetailsUrl"], row["Name"], row["ManagedStatus"], true)));
                        if (litDatastoreIOPS != null)
                            litDatastoreIOPS.Text = FormatHelper.FormatIOPS(row["IOPSTotal"], 1);
                        if (litDatastoreLatency != null)
                            litDatastoreLatency.Text = FormatHelper.FormatLatency(row["LatencyTotal"], 0);

                        AddDatastoreSpaceInfo(e, row);
                        AddLuns(e, row);
                        AddNas(e, row);
                    }
                    break;
                }
        }

    }

    private void AddNas(RepeaterItemEventArgs e, DataRow row)
    {
        Repeater repNas = e.Item.FindControl("repNas") as Repeater;
        if (repNas != null)
        {
            repNas.DataSource = row["Nas"] as DataTable;
            repNas.DataBind();
            repNas.ItemCreated += new RepeaterItemEventHandler(repNas_ItemCreated);
        }
    }

    private void repNas_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem is System.Data.DataRowView)
        {
            System.Data.DataRow row = (e.Item.DataItem as System.Data.DataRowView).Row;
            Literal litNas = e.Item.FindControl("litNas") as Literal;
            Literal litNasLabel = e.Item.FindControl("litNasLabel") as Literal;

            if (litNas != null)
                litNas.Text = GenerateNasLink(row);
            if (litNasLabel != null)
                litNasLabel.Text = e.Item.ItemIndex == 0 ? Resources.VIMWebContent.VIMWEBDATA_LC0_70 : "";
        }
    }



    private void AddLuns(RepeaterItemEventArgs e, DataRow row)
    {
        Repeater repLuns = e.Item.FindControl("repLuns") as Repeater;
        if (repLuns != null)
        {
            repLuns.DataSource = row["Luns"] as DataTable;
            repLuns.DataBind();
            repLuns.ItemCreated += new RepeaterItemEventHandler(repLuns_ItemCreated);
        }
    }

    private void repLuns_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem is System.Data.DataRowView)
        {
            System.Data.DataRow row = (e.Item.DataItem as System.Data.DataRowView).Row;
            Literal litLun = e.Item.FindControl("litLun") as Literal;
            Literal litLunLabel = e.Item.FindControl("litLunLabel") as Literal;

            litLun.Text = GenerateLunLink(row);
            litLunLabel.Text = e.Item.ItemIndex == 0 ? Resources.VIMWebContent.VIMWEBDATA_DF0_17 : "";
        }
    }

    private string GenerateLunLink(DataRow row)
    {
        var linkDAL = new StorageEntityLinkDAL();
        DataTable linkDetails = linkDAL.GetLUNLinkDetails((int)row["LunID"]);

        if (linkDetails.Rows.Count == 0)
        {
            return EntityLinkHelper.GetLunLink(null, row["Name"] as string, VimManagedStatus.WithoutStatus, true);
        }
        return EntityLinkHelper.GetLunLink(linkDetails.Rows[0]["Link"] as string, row["Name"] as string, (int)linkDetails.Rows[0]["Status"], true);
    }

    private string GenerateNasLink(DataRow row)
    {
        var linkDAL = new StorageEntityLinkDAL();
        DataTable linkDetails = linkDAL.GetNASLinkDetails((int)row["NasID"]);

        if (linkDetails.Rows.Count == 0)
        {
            return EntityLinkHelper.GetNasLink(null, row["Name"] as string, VimManagedStatus.WithoutStatus, true);
        }
        return EntityLinkHelper.GetNasLink(linkDetails.Rows[0]["Link"] as string, row["Name"] as string, (int)linkDetails.Rows[0]["Status"], true);
    }


    private void AddDatastoreSpaceInfo(RepeaterItemEventArgs e, System.Data.DataRow row)
    {
        Literal litDatastoreSize = e.Item.FindControl("litDatastoreSize") as Literal;
        Literal litDatastoreFreeSpace = e.Item.FindControl("litDatastoreFreeSpace") as Literal;
        Literal litDatastoreUsedSpace = e.Item.FindControl("litDatastoreUsedSpace") as Literal;
        InlineBar barDatastoreusage = e.Item.FindControl("barDatastoreusage") as InlineBar;

        long totalSize = Convert.ToInt64(row["Capacity"]);
        long freeSpace = Convert.ToInt64(row["FreeSpace"]);
        long usedSpace = totalSize - freeSpace;

        double usedPercent = ((double)usedSpace/(double)totalSize)*100;
        barDatastoreusage.Percentage = usedPercent;
        barDatastoreusage.ThresholdLevelValue = InlineBar.ThresholdLevel.Normal;
        litDatastoreSize.Text = SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(totalSize, SolarWinds.Orion.Web.DisplayTypes.ByteDisplayValueFormat.Short);
        litDatastoreFreeSpace.Text = SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(freeSpace, SolarWinds.Orion.Web.DisplayTypes.ByteDisplayValueFormat.Short);
        litDatastoreUsedSpace.Text = SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(usedSpace, SolarWinds.Orion.Web.DisplayTypes.ByteDisplayValueFormat.Short);
    }
}