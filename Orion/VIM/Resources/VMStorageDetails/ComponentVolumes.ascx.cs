﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.VManResources;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.Orion.Web.DisplayTypes;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VMStorageDetails_ComponentVolumes : VManVMStorageDetailsBaseResource
{
    const int DEFAULT_WarningThreshold = 80;
    const int DEFAULT_CriticalThreshold = 95;

    private int? _warningThreshold;
    private int WarningThreshold
    {
        get
        {
            if (!_warningThreshold.HasValue)
            {
                int value;
                if (Int32.TryParse(this.Resource.Properties["WarningThreshold"], out value))
                {
                    _warningThreshold = value;
                }
                else
                {
                    _warningThreshold = DEFAULT_WarningThreshold;
                }
            }
            return _warningThreshold.Value;
        }
    }

    private int? _criticalThreshold;
    private int CriticalThreshold
    {
        get
        {
            if (!_criticalThreshold.HasValue)
            {
                int value;
                if (Int32.TryParse(this.Resource.Properties["CriticalThreshold"], out value))
                {
                    _criticalThreshold = value;
                }
                else
                {
                    _criticalThreshold = DEFAULT_CriticalThreshold;
                }
            }
            return _criticalThreshold.Value;
        }
    }

    private void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
            {
                if (e.Item.DataItem is System.Data.DataRowView)
                {
                    var row = (e.Item.DataItem as System.Data.DataRowView).Row;
                    if (row == null)
                    {
                        return;
                    }

                    if (row["Capacity"] != DBNull.Value)
                    {
                        var phCapacity = e.Item.FindControl("phCapacity") as PlaceHolder;
                        phCapacity.Controls.Add(new LiteralControl(SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(row["Capacity"], ByteDisplayValueFormat.Short)));
                    }

                    if(row["FreeSpace"] != DBNull.Value)
                    {
                        var phFreeSpace = e.Item.FindControl("phFreeSpace") as PlaceHolder;
                        phFreeSpace.Controls.Add(new LiteralControl(SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(row["FreeSpace"], ByteDisplayValueFormat.Short)));
                    }

                    if(row["SpaceUtilization"] != DBNull.Value)
                    {
                        var phUsedPercentage = e.Item.FindControl("phUsedPercentage") as PlaceHolder;
                        var phUsedPercentBar = e.Item.FindControl("phUsedPercentBar") as PlaceHolder;
                        
                        String percentageHtml = FormatHelper.FormatPercent(row["SpaceUtilization"], WarningThreshold, CriticalThreshold);
                        String percentBarHtml = FormatHelper.FormatPercentBar(row["SpaceUtilization"], WarningThreshold, CriticalThreshold);

                        phUsedPercentage.Controls.Add(new LiteralControl(percentageHtml));
                        phUsedPercentBar.Controls.Add(new LiteralControl(percentBarHtml));
                    }

                    if (row["Label"] != DBNull.Value)
                    {
                        var phVirtualDisk = e.Item.FindControl("phVirtualDisk") as PlaceHolder;
                        phVirtualDisk.Controls.Add(new LiteralControl(EntityLinkHelper.GetVirtualDiskLink(null, row["Label"], VimManagedStatus.WithoutStatus, true)));
                    }
                }
                break;
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.resourceTable.ItemCreated += new RepeaterItemEventHandler(resourceTable_ItemCreated);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        HideWhenEntityNotPolled(nameof(Volume));
        DataBind();
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_DF0_2; }
    }

    public override void DataBind()
    {
        if (Wrapper.CanShowContent && DetectedViewType == ViewType.Guest)
        {
            var table = new VirtualMachineVolumeDAL().GetVirtualMachineVolumes(this.NodeId);
            resourceTable.DataSource = table;
            resourceTable.DataBind();
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHComponentVolumes"; }
    }

    public override string EditControlLocation
    {
        get 
        {
            return "/Orion/VIM/Controls/EditControls/ThresholdsEdit.ascx";
        }
    }
}
