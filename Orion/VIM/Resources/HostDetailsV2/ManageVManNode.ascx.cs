﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Base.Contract.Constants;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_VIM_Resources_HostDetailsV2_ManageVManNode : VimBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        Visible = IsSupportedView && ((ProfileCommon)HttpContext.Current.Profile).AllowAdmin;        
        ResourceWrapper.ShowManageButton = true;
        ResourceWrapper.ManageButtonText = Resources.CoreWebContent.ResourcesAll_ManageNodes;
        ResourceWrapper.ManageButtonTarget = "/Orion/Nodes/Default.aspx";

        if (Visible)
            InitManageNodeDialog();
    }
    
    private bool _hasMultipleIPAddress;
    private string _manageNodeDialogParams;
    private string _manageNodeUrl;

    private bool IsSupportedView
    {
        get { return DetectedViewType == ViewType.Guest || DetectedViewType == ViewType.Host ; }                
    }

    private void InitManageNodeDialog()
    {
        var entityId = (int)EntityId;        
        var prefix = String.Empty;
        var address = String.Empty;
        int? engineId = -1;
        var hostPlatform = VirtualizationPlatform.Unknown;
        
        _hasMultipleIPAddress = false;

        switch (DetectedViewType)
        {
            case ViewType.Guest:
                prefix = "G";
                var vmDal = new VirtualMachineDAL();
                var vm = vmDal.GetVMModelByVMId(entityId);
                address = HttpUtility.HtmlEncode(string.IsNullOrEmpty(vm.IPAddress) ? HttpUtility.UrlEncode(vm.Name) : vm.IPAddress);
                engineId = vmDal.GetParentEngineId(entityId);
                hostPlatform = vm.HostPlatform;                      
                break;
            case ViewType.Host:
                prefix = "H";
                var hostDal = new HostDAL();
                var host = hostDal.GetHostModelByHostId(entityId);
                address = HttpUtility.HtmlEncode(string.IsNullOrEmpty(host.IPAddress) ? host.Name : host.IPAddress);
                var list = new VMHostDetailsDAL().GetHostIpAddresses(entityId);
                
                if (list != null && list.Count() > 1)
                {
                    address = String.Join(";", list.ToArray());
                    _hasMultipleIPAddress = true;
                }

                engineId = hostDal.GetParentEngineId(entityId);
                hostPlatform = host.Platform;                                      
                
                break;
        }

        _manageNodeDialogParams = string.Format("'{0}', '{1}','{2}{3}', {4}, '{5}'", engineId ?? -1,
                                                address, prefix, entityId,
                                                _hasMultipleIPAddress.ToString().ToLower(), hostPlatform);

        _manageNodeUrl = String.Format("/Orion/Nodes/Add/Default.aspx?EngineID={0}&IPAddress={1}", engineId, HttpUtility.UrlPathEncode(address));

        if (hostPlatform == VirtualizationPlatform.Vmware)
            _manageNodeUrl = string.Concat(_manageNodeUrl, string.Format("&VMwareEntity={0}{1}", prefix, entityId));
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LV0_7; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualizationManagerNode"; }
    }

    public override string EditURL
    {
        get { return null; }
    }

    protected string GetHelpLink
    {
        get { return HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, HelpLinkFragment); }
    }

    protected bool HasMultipleIPAddress
    {
        get { return _hasMultipleIPAddress; }
    }

    protected string ManageNodeDialogParams
    {
        get { return _manageNodeDialogParams; }
    }

    protected string ManageNodeUrl
    {
        get { return _manageNodeUrl; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimManagableVirtualEntityProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}