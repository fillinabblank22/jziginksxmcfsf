﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Common.Helpers;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_HostDetailsV2_VMHostDetailsAreaChart : StandardChartResource
{
    private static readonly Log _log = new Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
    }

    /// <summary>
    /// Gets a value indicating whether [allow customization].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [allow customization]; otherwise, <c>false</c>.
    /// </value>
    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    /// <summary>
    /// Gets the net object prefix.
    /// </summary>
    protected override string NetObjectPrefix
    {
        get { return "N"; }
    }


    /// <summary>
    /// Gets the list of uniqueidentifier elements for which we will need to retrieve data points for charts
    /// </summary>
    /// <returns>List of uniqueidentifiers of elements for which we via webservice will want to retrieved data.</returns>
    protected override IEnumerable<string> GetElementIdsForChart()
    {      
        var nodeProvider = GetInterfaceInstance<INodeProvider>();
        string key = String.Format("__GetElementIdsForChart_{0}_{1}", ViewType.Host.ToString(), nodeProvider.Node.NodeID);
        IEnumerable<string> cacheResult;
        if (CacheHelper.TryGetFromCache(key, out cacheResult))
        {
            return cacheResult;
        }

        if (nodeProvider != null)
        {
            const string swql = @"SELECT VM.VirtualMachineID FROM Orion.VIM.VirtualMachines VM 
                                  INNER JOIN Orion.VIM.Hosts H ON VM.HostID = H.HostID
                                  LEFT JOIN Orion.Nodes N ON H.NodeID = N.NodeID
                                  WHERE (VM.PowerState = 'poweredOn') AND N.NodeID = @NodeId";

            IEnumerable<string> result = GetElementsFromSwql(swql, "NodeId", nodeProvider.Node.NodeID);
            CacheHelper.SaveToCache(key, result);
            return result;
        }

        return new string[0];
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    /// <summary>
    /// Gets the default title for resource which will be displayed in the case that template will not override this.
    /// </summary>
    protected override string DefaultTitle
    {
        get { return notAvailable; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
}