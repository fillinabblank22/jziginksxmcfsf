﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageVManNode.ascx.cs" Inherits="Orion_VIM_Resources_HostDetailsV2_ManageVManNode" %>
<%@ Register TagPrefix="vim" TagName="ManageNodeDialog" Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper">
    <Content>
        <asp:Panel runat="server">
            <script type="text/javascript">
                function ManageThisNode() {                                        
                    if (<%= HasMultipleIPAddress.ToString().ToLower() %>)
                        ManageNodeDialog(<%= ManageNodeDialogParams %>);
                    else
                        window.location.href = "<%= ManageNodeUrl %>";
                    
                    return false;
                }

                function ExpandResource() {
                    $("#collapsedState").hide();
                    $("#expandedState").show();
                }
            </script>
            
            <div class="sw-vim-resource-managenode">                
                <div id="collapsedState">
                    <div class="sw-vim-float-left">
                        <%= Resources.VIMWebContent.VIMWEBDATA_LV0_30 %>
                    </div>                    
                    <div class="sw-vim-float-left-auto-width">
                        <div class="sw-vman-buttons" style="clear:both;">                    
                            <orion:LocalizableButton ID="ManageNode2" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_31 %>" DisplayType="Primary" runat="server" CausesValidation="false" OnClientClick="return ManageThisNode();" />
                            <span class="LinkArrow">&#0187;</span>&nbsp;<a href="javascript:void(0);" onclick="return ExpandResource();" class="linkLabel"><%= Resources.VIMWebContent.VIMWEBDATA_LV0_32 %></a>
                        </div>                                        
                    </div>
                </div>

                <div id="expandedState" style="display:none;">                    
                    <div class="sw-vim-float-left">
                        <img src="/Orion/images/getting_started_36x36.gif" alt="<%= DefaultTitle %>" />
                    </div>                                   
                    <div class="sw-vim-float-left-auto-width">
                        <p>
                            <%= Resources.VIMWebContent.VIMWEBDATA_LV0_33 %>
                        </p>                        
                        <span class="LinkArrow">&#0187;</span>&nbsp;<a href="<%= GetHelpLink %>" class="linkLabel" target="_blank"><%= Resources.VIMWebContent.VIMWEBDATA_LV0_34 %></a>
                    </div>                                                                                                                     
                    <div class="sw-vman-buttons" class="sw-vim-clear">                    
                        <orion:LocalizableButton ID="ManageNode" LocalizedText="CustomText" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_31 %>" DisplayType="Primary" runat="server" CausesValidation="false" OnClientClick="return ManageThisNode();" />
                    </div>                                                                                    
                </div>
            </div>
        </asp:Panel>
        <vim:ManageNodeDialog runat="server"/>
    </Content>
</orion:resourceWrapper>
