﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByRunningVMCountForDataCenter.ascx.cs" Inherits="Orion_VIM_Resources_DataCenterDetails_TopXXVMHostsByRunningVMCountForDataCenter" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
   <asp:Panel ID="ErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                         <asp:Literal runat="server" ID="ErrorMessage" />
                    </td>
                </tr>
            </table>
   </asp:Panel>
   <asp:Repeater runat="server" ID="resourceTable">
       <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_4%></td>
                    <td class="ReportHeader" width="10%" style="white-space:nowrap;"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_11%></td>
                </tr>
       </HeaderTemplate>
        <ItemTemplate>
          <tr>
             <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("Status")%>' />
                  <a href="<%#BaseResourceControl.GetViewLink((string)Eval("NodeID", "N:{0}"))%>">
                      <%# HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown))%>
                  </a>
              </td> 
             <td class="Property vimTopXXProperty"><%# string.Format(Resources.VIMWebContent.VIMWEBDATA_AK0_13, Eval("VmRunningCount"), Eval("VmCount")) %></td>
          </tr>
      </ItemTemplate>
      <FooterTemplate>
            </table>
      </FooterTemplate>
     </asp:Repeater>
   </Content>    
</orion:resourceWrapper>


