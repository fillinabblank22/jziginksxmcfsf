﻿using System;
using System.Collections.Generic;
using System.Data;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_DataCenterDetails_TopXXVMHostsByRunningVMCountForDataCenter : VimTopXXResourceControl
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_AK0_5; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            VimDataCenter dataCenter = GetInterfaceInstance<IVimDataCenterProvider>().DataCenter;
            table = (new DataCenterDAL()).GetTopXXVMWareHostsByRunningVMCount(this.MaxRecords, this.Resource.Properties["Filter"], dataCenter.Id);
            this.Visible = table.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            this.ErrorPanel.Visible = true;
            this.ErrorMessage.Text = ex.Message;
            log.Error("Error in Resource Top XX VMware Hosts by Number Of Running VMs For Datacenter", ex);
            return;
        }

        PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.HostEntityName);

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> {{"EntityName", "Orion.VIM.Hosts"}}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMHostsTopXXEditControl.ascx"; }
    }

    //TODO: Once we get the help file link from Roger wong (FB#24577) , Nandha have to replace it with appropriate link
    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Datacenter_TopXXVMwareHostsByNumberOfRunningVMs"; }
    }

    public Boolean GetBarVisible()
    {
        return true;
    }
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimDataCenterProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
