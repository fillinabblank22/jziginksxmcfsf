﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Resources_DataCenterDetails_RunningVMChart : GraphResource
{
    protected override void OnInit(EventArgs e)
    {
        CreateChart(null, GetInterfaceInstance<IVimDataCenterProvider>().DataCenter.NetObjectID, "DataCenterRunningVMCount", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("DataCenterRunningVMCount", Resource, string.Format("{0}{1}", VimDataCenter.Prefix, GetTitlePrefix()));
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_AK0_1;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Datacenter_OverallNumberOfRunningVsTotalVMs"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimDataCenterProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditVimChartControl.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}