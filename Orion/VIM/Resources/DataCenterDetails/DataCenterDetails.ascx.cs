﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Resources;
using System;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_DataCenterDetails_DataCenterDetails : VimDataCenterBaseResource
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        PollingWarning.DataBind();
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_AK0_2; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_Datacenter_VMwareDatacenter"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}