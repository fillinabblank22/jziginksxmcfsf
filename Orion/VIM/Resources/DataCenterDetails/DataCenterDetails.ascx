﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DataCenterDetails.ascx.cs" Inherits="Orion_VIM_Resources_DataCenterDetails_DataCenterDetails" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="PollingWarning" Src="~/Orion/VIM/Controls/PollingWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table class="sw-vim-entityDetails NeedsZebraStripes">
            <tr>
                <td class="PropertyHeader" style="width: 50%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_1%></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(this.DataCenter.Model.Name)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_2%></td>
                <%= ManagedStatusHelper.GetDataCenterStatusTD(this.DataCenter.Model.Status)%>
            </tr>
            <% if (DataCenter.Model.VCenterNodeID > 0)
                { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_3 %></td>
                <td class="Property">
                    <a href="<%= BaseResourceControl.GetViewLink(String.Format("N:{0}", this.DataCenter.Model.VCenterNodeID)) %>">
                        <%= HttpUtility.HtmlEncode(this.DataCenter.Model.VCenterName) %>
                    </a>
                </td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.EntityDetails_PollingSource%></td>
                <td class="Property"><%= PollingWarning.LocalizedPollingSource%></td>
            </tr>
            <% if (PollingWarning.Visible)
                { %>
            <tr></tr>
            <tr>
                <td style="padding-top: 0" colspan="2">
                    <orion:PollingWarning ID="PollingWarning" runat="server" EntityId='<%# DataCenter.Id%>' PollingSource='<%# DataCenter.Model.PollingSource%>' ViewType='<%# ViewType.DataCenter%>' />
                </td>
            </tr>
            <% } %>
        </table>
    </Content>
</orion:resourceWrapper>
