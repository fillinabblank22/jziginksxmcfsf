﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMHostsByNetworkUtilizationForDataCenter.ascx.cs" Inherits="Orion_VIM_Resources_DataCenterDetails_TopXXVMHostsByNetworkUtilizationForDataCenter" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
   <asp:Panel ID="ErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                         <asp:Literal runat="server" ID="ErrorMessage" />
                    </td>
                </tr>
            </table>
   </asp:Panel>
   <asp:Repeater runat="server" ID="resourceTable">
       <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst" width="40%" colspan="2"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_4%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_AK0_5%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_AK0_6%></td>
                    <td class="ReportHeader" width="15%" ><%= Resources.VIMWebContent.VIMWEBDATA_AK0_7%></td>
                    <td class="ReportHeader" width="15%" colspan="2" ><%= Resources.VIMWebContent.VIMWEBDATA_AK0_8%></td>
                </tr>
       </HeaderTemplate>
        <ItemTemplate>
          <tr>
             <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("Status")%>' />
             </td>
             <td width="100%">
                  <a href="<%#BaseResourceControl.GetViewLink((string)Eval("NodeID", "N:{0}"))%>">
                      <%# HttpUtility.HtmlEncode(SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(DBHelper.GetDbValueOrDefault(Eval("DisplayName"), Resources.VIMWebContent.Web_VIMEntity_Unknown)))%>
                  </a>
              </td> 

             <td class="Property vimTopXXProperty"  style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
             <td class="Property vimTopXXProperty"  style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("xmitKBps"), (float)Eval("rcvKBps"))%></td>
             <td class="Property vimTopXXProperty"  style="white-space:nowrap;"><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatNetworkUtilizationForResource((float)Eval("usageKBps"), (float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
             <td class="Property vimTopXXProperty" ><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercent(Eval("utilization"), ThresholdType.NetworkUtilization)%></td>
             <td class="Property vimTopXXProperty" ><%# SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(Eval("utilization"), ThresholdType.NetworkUtilization)%></td>
          </tr>
      </ItemTemplate>
      <FooterTemplate>
            </table>
      </FooterTemplate>
     </asp:Repeater>
   </Content>    
</orion:resourceWrapper>


