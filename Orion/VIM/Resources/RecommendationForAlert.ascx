<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecommendationForAlert.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_RecommendationForAlert" %>
<%@ Import Namespace="Resources" %>

<style type="text/css">
     div.sw-vim-recommendationLinks {
         padding: 10px;
     }
     div.sw-vim-recommendationLinks div {
         width: 90%;
         padding-bottom: 10px;
     }
    .sw-vim-recommendationLinks a {
        color: #336699; 
        text-decoration: underline;
        display: inline-block;
        padding-left: 15px;
        margin-bottom: 5px;
    }
</style>
 
<orion:resourceWrapper runat="server" ID="RecommendationResourceWrapper">
    <Content>
        <div class="sw-vim-recommendationLinks">
            <div><%= HttpUtility.HtmlEncode(RecommendationMessage) %></div>
            <hr />
            <h4><%= VIMWebContent.VIMWEBDATA_JD0_30 %></h4>
            <a href="<%= DetailsLink %>"><%= HttpUtility.HtmlEncode(String.Format(VIMWebContent.VIMWEBDATA_JD0_31,TriggeringObjectName)) %></a>
        </div>
    </Content>
</orion:resourceWrapper>

