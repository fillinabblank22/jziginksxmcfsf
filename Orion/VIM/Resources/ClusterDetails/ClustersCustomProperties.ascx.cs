﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Resources;
using System;
using SolarWinds.VIM.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_ClusterDetails_ClustersCustomProperties : VimClusterBaseResource
{
    protected void Page_Load(object sender, EventArgs e)
    {
        customPropertyList.Resource = Resource;
        customPropertyList.NetObjectId = Cluster.Model.ID;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) => { return new ClusterDAL().GetCustomProperties(NodeId, displayProperties); };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) => { return "/Orion/Admin/CPE/Default.aspx"; };
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.Web_ClusterCustomProperties_DefaultTitle; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}