<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenManagedVMwareGuestsByPercentMemoryUsed.ascx.cs" Inherits="Orion_VIM_Resources_Cluster_TopXXManagedVMwareGuestsByPercentMemoryUsed" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
   <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
   </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
       <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                    <td class="ReportHeader" colspan="2" width="10%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_37%></td>
                </tr>
       </HeaderTemplate>
       <ItemTemplate>
          <tr>
          <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("GuestStatus")%>' />
                      <%#GetNodeNameWithLink((string)Eval("GuestName"), Eval("NodeID"))%>
             </td> 
             <td class="Property vimTopXXProperty"><%# FormatHelper.FormatPercent(Eval("PercentMemoryUsed"), this.type)%></td>
             <td class="Property vimTopXXProperty"><%# FormatHelper.FormatPercentBar(Eval("PercentMemoryUsed"), this.type)%></td>
          </tr>
      </ItemTemplate>    
      <FooterTemplate>
            </table>
      </FooterTemplate>
     </asp:Repeater>
   </Content>    
</orion:resourceWrapper>