using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Cluster_TopTenVmsByVsanDiskLatency : VimTopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.TopTenVmsByVsanDiskLatency_ResourceName; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            VimCluster cluster = GetInterfaceInstance<IVimClusterProvider>().Cluster;
            var clusterDal = new ClusterDAL();
            bool isVSanCluster = clusterDal.GetVSanId(cluster.Id).HasValue;

            if (!isVSanCluster)
            {
                this.Visible = false;
                return;
            }

            var dal = new TopXxVmsByVsanDiskLatencyDal();
            table = dal.GetTopXxVmsByVsanDiskLatency(this.MaxRecords, cluster.Id);
            GenerateLinksForVms(table);
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        if (table != null && table.Rows.Count > 0)
        {
            PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.VirtualMachineEntityName);
            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();
        }
        else
        {
            this.resourceTable.Visible = false;
        }
    }

    private void GenerateLinksForVms(DataTable table)
    {
        table.Columns.Add("vmLink", typeof(string));
        foreach (DataRow row in table.Rows)
        {
            row["vmLink"] = GetVmNameLink(row["NodeID"], row["VirtualMachineID"]);
        }
    }

    private string GetVmNameLink(object nodeId, object vmId)
    {
        if (nodeId != null && nodeId != DBNull.Value && nodeId is int && (int)nodeId > 0)
        {
            return String.Format("/Orion/View.aspx?NetObject=N:{0}", nodeId);
        }
        if (vmId != null && vmId != DBNull.Value && vmId is int && (int)vmId > 0)
        {
            return String.Format("/Orion/VIM/VMDetails.aspx?NetObject=VVM:{0}", vmId);
        }
        return String.Empty;
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> {{"EntityName", "Orion.VIM.VirtualMachines"}}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMGuestsTopXXEditControl.ascx"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new[] { typeof(IVimClusterProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
