<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenVmsByVsanDiskLatency.ascx.cs" Inherits="Orion_VIM_Resources_Cluster_TopTenVmsByVsanDiskLatency" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Extensions" %>

<orion:resourceWrapper ID="ResourceWrapper1" runat="server">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable" ItemType="System.Data.DataRowView">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                    <td class="ReportHeader" width="40%"><%= Resources.VIMWebContent.TopTenVmsByVsanDiskLatency_Disk%></td>
                    <td class="ReportHeader" width="20%"><%= Resources.VIMWebContent.TopTenVmsByVsanDiskLatency_Latency%></td>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Item.Row.Field<string>("EntityName")%>' Status='<%#Item.Row.Field<int>("GuestStatus")%>' />
                        <i runat="server" Visible='<%#String.IsNullOrEmpty(Item.Row.Field<string>("vmLink"))%>'><%#Item.Row.Field<string>("GuestName").HtmlEncode()%></i>
                        <a runat="server" Visible='<%#!String.IsNullOrEmpty(Item.Row.Field<string>("vmLink"))%>' href='<%#Item.Row.Field<string>("vmLink")%>'><%#Item.Row.Field<string>("GuestName").HtmlEncode()%></a>
                    </td>
                    <td class="Property vimTopXXProperty"><%# HttpUtility.HtmlEncode(Item.Row.Field<string>("DiskName"))%></td>
                    <td class="Property vimTopXXProperty"><%# FormatHelper.FormatLatency(Item.Row.Field<double>("DiskLatency"), 1)%></td>

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
