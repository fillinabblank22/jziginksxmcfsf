using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Charting;
using SolarWinds.Orion.Web;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.Web.UI;

public partial class Orion_VIM_Resources_Cluster_VMWareClusterEffectiveMemoryLoadGraph : GraphResource
{

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_16; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_VMwareClusterEffectiveMemoryLoadGraph"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CreateChart(null, GetInterfaceInstance<IVimClusterProvider>().Cluster.NetObjectID, "ClusterEffectiveMemoryLoad", chartPlaceHolder);
        Wrapper.SetDrDownMenuParameters("ClusterEffectiveMemoryLoad", Resource, VimCluster.Prefix);
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditVimChartControl.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
