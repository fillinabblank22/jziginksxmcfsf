using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_VIM_Resources_Virtualization_VirtualHostsResyncInfo : VimClusterBaseResource
{
    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isVSanCluster = new ClusterDAL().GetVSanId(Cluster.Id).HasValue;

        if (!IsLicensed()
            || !isVSanCluster
            || Cluster.Model.Platform == VirtualizationPlatform.HyperV
            || Cluster.Model.PollingSource == PollingSource.Vanilla)
        {
            Visible = false;
            return;
        }

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.OrderBy = OrderBy;
    }

    public string SWQL
    {
        get
        {
            return string.Format(@"
                SELECT
                    i.OrionHost.HostName as HostName,
                    i.OrionHost.DetailsUrl as _LinkFor_HostName,
                    '/Orion/StatusIcon.ashx?entity=Orion.VIM.Hosts&size=Small&status=' + TOSTRING(i.OrionHost.Status) as _IconFor_HostName,
                    i.ResyncingElementsCount,
                    i.RemainingBytes,
                    MinuteDiff(GetUtcDate(), i.Eta) AS EtaMinutes
                FROM
                    Cortex.Orion.Virtualization.VSanResyncInfo i
                WHERE
                    MinuteDiff(GetUtcDate(), i.Eta) > 0 AND i.OrionHost.ClusterId = {0}", Cluster.Id);
        }
    }

    public string OrderBy
    {
        get { return "Eta ASC"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_VirtualHostsResyncInfo"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VirtualHostsResyncInfoResource_Title; }
    }
}
