﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClustersCustomProperties.ascx.cs" Inherits="Orion_VIM_Resources_ClusterDetails_ClustersCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="VIM_ClustersCustomProperties"/>
    </Content>
</orion:resourceWrapper>