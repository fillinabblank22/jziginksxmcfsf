<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMWareClusterPercentAvailabilityGraph.ascx.cs"
    Inherits="Orion_VIM_Resources_Cluster_VMWareClusterPercentAvailabilityGraph" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>
