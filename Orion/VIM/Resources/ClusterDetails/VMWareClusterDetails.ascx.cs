using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SolarWinds.VIM.Web.Common.Licensing;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.Models;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_ClusterDetails : VimClusterBaseResource
{
    public DataStoreModel VSanDataStoreModel { get; private set; }
    public VSanClusterSpace VSanClusterSpace { get; private set; }
    public List<Cluster> RelatedClusters { get; private set; }


    protected string GetLocalizedBool(bool boolVariable)
    {
        return (boolVariable)
            ? Resources.VIMWebContent.VIMWEBCODE_VB0_46
            : Resources.VIMWebContent.VIMWEBCODE_VB0_47;
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_12; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_VMwareClusterDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected IDictionary<PollingSource, int> HostPollingSourceStats { get; private set; }
    protected NutanixPollingEngineDescription? NutanixEngineDescription { get; private set; }
    protected NutanixDiscoveryMetadataModel? NutanixDiscoveryMetadata { get; private set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        var clusterDal = new ClusterDAL();

        if (LicenseManager.Instance.IsVimFullyLicensed())
        {
            var dataStoreId = clusterDal.GetVSanDataStoreId(Cluster.Id);
            if (dataStoreId.HasValue)
            {
                VSanDataStoreModel = new DatastoreDAL().GetDataStoreModel(dataStoreId.Value);
                VSanClusterSpace = clusterDal.GetVSanClusterSpace(Cluster.Id);
            }
            vsanControl.VSanId = clusterDal.GetVSanId(Cluster.Id);
        }

        RelatedClusters = clusterDal.GetRelatedClusters(Cluster.Id).ToList();

        HostPollingSourceStats = clusterDal.GetHostPollingSourceStatsInCluster(Cluster.Id);
        PollingWarning.DataBind();

        if (Cluster.Model.Platform == SolarWinds.VIM.Common.VirtualizationPlatform.Nutanix)
        {
            NutanixEngineDescription = clusterDal.GetNutanixClusterPollingEngineDescription(Cluster.Id);
            NutanixDiscoveryMetadata = clusterDal.GetNutanixDiscoveryMetadataForCluster(Cluster.Id);
        }
    }

    protected string FormatHostPollingSources()
    {
        if (HostPollingSourceStats == null) return null;

        // convert all sources to format "name ({count of hosts}x)"
        //  it is also ordered so java poller is displayed first
        var statsFormatted = HostPollingSourceStats.Keys
            .OrderByDescending(ps => (int) ps)
            .Select(ps => $"{LocalizePollingSource(ps)} ({FormatHelper.FormatNumber(HostPollingSourceStats[ps], 0)}&times;)");

        return string.Join(", ", statsFormatted);
    }

    protected string GetHostsPollingSourceMisconfigurationWarning()
    {
        if (HostPollingSourceStats == null) return null;

        string settingsPageUrl;

        switch (Cluster.Model.Platform)
        {
            case SolarWinds.VIM.Common.VirtualizationPlatform.Vmware:
                settingsPageUrl = "~/Orion/VIM/Admin/VMwareServers.aspx";
                break;
            case SolarWinds.VIM.Common.VirtualizationPlatform.HyperV:
                settingsPageUrl = "~/Orion/VIM/Admin/HyperVServers.aspx";
                break;
            default:
                settingsPageUrl = "~/Orion/VIM/Admin/VirtualizationSettings.aspx";
                break;
        }

        return string.Format(Resources.VIMWebContent.ClusterDetails_HostsPollingSourceMisconfiguration_Warning,
            Page.ResolveUrl(settingsPageUrl));
    }

    protected string GetVirtualizationEntityEditLink()
    {
        ReferrerRedirectorBase.SetReturnUrl(GetViewLinkAbsolute(string.Format("VMC:{0}", NodeId)));
        return string.Format("/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?NetObject=VMC:{0}&ReturnTo={1}", NodeId, ReferrerRedirectorBase.GetReturnUrl());
    }

    protected string GetNutanixPrismConsoleLink()
    {
        return !NutanixDiscoveryMetadata.HasValue || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer
            ? string.Empty
            : string.Format("https://{0}:{1}/", NutanixDiscoveryMetadata.Value.IpAddress, NutanixDiscoveryMetadata.Value.ApiPort);
    }

    protected string GetNutanixPrismCentralLink()
    {
        if (!NutanixDiscoveryMetadata.HasValue || string.IsNullOrEmpty(NutanixDiscoveryMetadata.Value.PrismCentralIpAddress) || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return string.Empty;
        }

        return string.Format("https://{0}:{1}/console/#page/explore/s/{2}",
            NutanixDiscoveryMetadata.Value.PrismCentralIpAddress,
            NutanixDiscoveryMetadata.Value.PrismCentralApiPort,
            Cluster.Name);
    }

    protected string GetPerfStackLink()
    {
        return PerfStackLinkBuilder.GetClusterLink(Cluster.Id);
    }

    protected bool HasVSanEnabled()
    {
        return VSanDataStoreModel != null;
    }

    protected string GetNutanixClusterPollingEngine()
    {
        if (!NutanixEngineDescription.HasValue)
        {
            return string.Empty;
        }

        return string.Format(
            Resources.VIMWebContent.Web_NutanixClusterDetail_PollingEngineDescriptionFormat,
            NutanixEngineDescription.Value.Hostname,
            NutanixEngineDescription.Value.IpAddress);
    }
}
