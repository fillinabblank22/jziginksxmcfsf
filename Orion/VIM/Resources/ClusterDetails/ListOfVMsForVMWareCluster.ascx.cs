using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Controls;
using SolarWinds.VIM.Web.Helpers;

//TODO: Rename this file as it does not match resource name
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_ListOfVMsForVMWareCluster : VimClusterBaseResource
{
    private String _orderBy;
    private String _key;

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_5; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public String Key
    {
        get { return _key ?? (_key = String.Format("ListOfVMHosts_OrderBy_{0}", this.Resource.ID)); }
    }

    private String OrderBy
    {
        get
        {
            if (_orderBy == null)
            {
                _orderBy = Request["orderBy"];
                if (String.IsNullOrEmpty(_orderBy))
                {
                    if (Session[Key] == null)
                    {
                        Session[Key] = _orderBy = "HostName";
                    }
                    _orderBy = Session[Key].ToString();
                    return _orderBy;
                }

                String sessOrderBy = (Session[Key] ?? String.Empty).ToString();
                if (sessOrderBy.Contains(_orderBy))
                {
                    if (sessOrderBy == _orderBy)
                    {
                        _orderBy = String.Format("{0} DESC", _orderBy);
                    }
                }
                Session[Key] = _orderBy;
            }
            return _orderBy;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void itemRepeater_Load(object sender, EventArgs e)
    {
        DataTable table = null;
        try
        {
            var instance = GetInterfaceInstance<IVimClusterProvider>();
            if (instance != null)
            {
                var dal = new ListOfHostsForClusterDAL(); 
                table = dal.GetHosts(instance.Cluster.Id, this.Resource.Properties["Filter"], OrderBy);
            }
            this.Visible = (table != null) && table.Rows.Count > 0;
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        if (table != null && table.Rows.Count > 0)
        {
            PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.HostEntityName);
            
            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();
        }
    }

    protected void OnRepeater_ItemCreated(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header)
        {
            if (OrderBy.StartsWith("HostName"))
            {
                OnResource_SortClick(e.Item.FindControl("HostName"), null);
            }
            else if (OrderBy.StartsWith("VmRunningCount"))
            {
                OnResource_SortClick(e.Item.FindControl("VmRunningCount"), null);
            }
            else if (OrderBy.StartsWith("CPULoad"))
            {
                OnResource_SortClick(e.Item.FindControl("CPULoad"), null);
            }
            else if (OrderBy.StartsWith("PercentMemoryUsed"))
            {
                OnResource_SortClick(e.Item.FindControl("PercentMemoryUsed"), null);
            }
            else if (OrderBy.StartsWith("PercentNetworkUsed"))
            {
                OnResource_SortClick(e.Item.FindControl("PercentNetworkUsed"), null);
            }
            /*else if (OrderBy.StartsWith("StoragePercentageUsage"))
            {
                OnResource_SortClick(e.Item.FindControl("StoragePercentageUsage"), null);
            }*/
        }
    }

    protected void OnResource_SortClick(Object sender, EventArgs e)
    {
        if (e == null)
        {
            var ctr = sender as HtmlAnchor;
            var literal = new Literal
                              {
                                  Text = String.Format(
                                      "&nbsp;&nbsp;&nbsp;<img alt='' src='/Orion/images/nodemgmt_art/icons/arrow_sort{0}.png'/>",
                                      OrderBy.EndsWith("DESC") ? String.Empty : "_up")
                              };
            ctr.Controls.Add(literal);
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_ListOfVMwareHosts"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    protected void OnRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;

            int hostID = DBHelper.GetDbValueOrDefault(row["HostID"], -1);
            int nodeStatusValue = DBHelper.GetDbValueOrDefault(row["Status"], -1);
            string displayName = HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault(row["HostName"], String.Empty));
            string ipAddress = DBHelper.GetDbValueOrDefault(row["IPAddress"], String.Empty);
            int engineID = DBHelper.GetDbValueOrDefault(row["HostEngineID"], -1);
            string url = DBHelper.GetDbValueOrDefault(row["DetailsUrl"], string.Empty);

            Orion_Controls_EntityStatusIcon icon = e.Item.FindControl("hostStatusIcon") as Orion_Controls_EntityStatusIcon;
            Literal hostLink = e.Item.FindControl("hostLink") as Literal;

            if (nodeStatusValue != -1)
            {
                icon.Status = (int)row["Status"];
            }
            else
            {
                icon.Visible = false;
            }

            if (!ManageNodeLink.IsUnmanagedNodeUrl(url))
            {
                hostLink.Text = string.Format("<a href=\"{0}\" >{1}</a>", url, displayName);
            }                        
            else if (Profile.AllowNodeManagement)
            {
                hostLink.Text = string.Format("<a href=\"javascript:void(0)\" onclick=\"{0}\">{1}</a>",
                                             ManageNodeLink.GetOnClickCode(engineID, ipAddress, SolarWinds.VIM.Web.Controls.AssetTree.AssetTreeNodeType.Host, hostID, VirtualizationPlatform.Vmware),
                                             displayName);
            }
            else
            {
                hostLink.Text = displayName;
            }

        }
    }

}
