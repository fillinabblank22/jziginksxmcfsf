using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Cluster_TopXXManagedVMWareGuestsByCPULoad : VimTopXXResourceControl
{
    public ThresholdType type { get { return ThresholdType.CPULoad; } }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_7; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;

        try
        {
            VimCluster cluster = GetInterfaceInstance<IVimClusterProvider>().Cluster;
            var dal = new TopXXManagedVMWareGuestsByCPULoadDAL();
            table = dal.GetTopXXVMWareGuestsByCPULoad(this.MaxRecords, this.Resource.Properties["Filter"],cluster.Id);
            this.Visible = table.Rows.Count > 0;
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.VirtualMachineEntityName);

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    public Boolean GetBarVisible()
    {
        return true;
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> { { "EntityName", "Orion.VIM.VirtualMachines" }}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMGuestsTopXXEditControl.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_TopXXManagedVMwareGuestsByCPU"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
