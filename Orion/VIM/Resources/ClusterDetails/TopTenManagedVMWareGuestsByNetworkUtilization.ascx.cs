using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Cluster_TopTenManagedVMWareGuestsByNetworkUtilization : VimTopXXResourceControl
{
    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_8; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            HideWhenEntityPropertyNotPolled(nameof(VirtualMachine), nameof(VirtualMachine.NetworkUsageRate));
            if (!Visible)
            {
                return;
            }
            VimCluster cluster = GetInterfaceInstance<IVimClusterProvider>().Cluster;
            var dal = new TopTenManagedVMGuestsByNetworkUtilizationDAL();
            table = dal.GetTopTenVMWareGuestsByNetworkUtilization(this.MaxRecords, this.Resource.Properties["Filter"], cluster.Id);
            this.Visible = table.Rows.Count > 0;
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.VirtualMachineEntityName);

        this.resourceTable.DataSource = table;
        this.resourceTable.DataBind();
    }

    protected string FormatValue(object value)
    {
        if (value is DBNull)
        {
            return "?";
        }

        return String.Format("{0:0.000}", value);
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> {{"EntityName", "Orion.VIM.VirtualMachines"}}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMGuestsTopXXEditControl.ascx"; }
    }

    //TODO: Once we get the help file link from Roger wong (FB#18562) , Nandha have to replace it with appropriate link
    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_TopXXManagedVMwareGuestsByNetworkUtilization"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected string GetNodeNameWithLink(string nodeName, object nodeID)
    {
        nodeName = UIHelper.Escape(nodeName);

        string nodeNameWithLink;
        if ((nodeID != null) && (nodeID != DBNull.Value) && (nodeID.GetType() == typeof(System.Int32)) && ((int)nodeID > 0))
        {
            nodeNameWithLink = string.Format("<a href=\"{0}\">{1}</a>", GetViewLink(string.Format("N:{0}", nodeID)), nodeName);
        }
        else
        {
            nodeNameWithLink = string.Format("<i>{0}</i>", nodeName);
        }
        return nodeNameWithLink;
    }
}
