<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualHostsResyncInfo.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_VirtualHostsResyncInfo" %>
<%@ Import Namespace="Resources" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>

<orion:Include runat="server" Module="VIM" File="Resources/Formatters.js" />

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="sw-suggestion" id="sw-vim-virtualhostsresyncinforesource-noresyncdiv" style="display:none;">
            <span class="sw-suggestion-icon"></span>
            <span><%= VIMWebContent.VirtualHostsResyncInfoResource_NoResyncInProgressInfoText %></span> 
        </div>
        
        <div id="sw-vim-virtualhostsresyncinforesource-customtable">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        allowSort: true,
                        onLoad: function (rows, columns) {
                            if (rows.length == 0) {
                                $("#sw-vim-virtualhostsresyncinforesource-noresyncdiv").show();
                                $("#sw-vim-virtualhostsresyncinforesource-customtable").hide();
                            }
                        },
                        columnSettings: {
                            "HostName": {
                                header: '<%= VIMWebContent.VirtualHostsResyncInfoResource_Host_ColumnName %>'
                            },
                            "ResyncingElementsCount": {
                                header: '<%= VIMWebContent.VirtualHostsResyncInfoResource_ResyncingElementsCount_ColumnName %>'
                            },
                            "RemainingBytes": {
                                header: '<%= VIMWebContent.VirtualHostsResyncInfoResource_RemainingBytes_ColumnName %>',
                                formatter: SW.VIM.Formatters.bytesFormatter
                            },
                            "EtaMinutes": {
                                header: '<%= VIMWebContent.VirtualHostsResyncInfoResource_Eta_ColumnName %>',
                                formatter: SW.VIM.Formatters.minutesFormatter
                            }
                        }
                    });

                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();

            });
        </script>
    </Content>
</orion:ResourceWrapper>
