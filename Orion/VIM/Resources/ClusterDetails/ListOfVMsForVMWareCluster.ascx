<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfVMsForVMWareCluster.ascx.cs"
    Inherits="Orion_VIM_Resources_Virtualization_ListOfVMsForVMWareCluster" %>
<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <script type="text/javascript">
            function setOrder(column) { $("#orderBy").val(column); return true; }
            
            $(function () {
                var refresh = function () { __doPostBack('<%= ctrResUP.ClientID %>', ''); };
                SW.Core.View.AddOnRefresh(refresh, '<%= ctrResUP.ClientID %>');
            });
        </script>
        <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
        </asp:Panel>


        <asp:UpdatePanel ID="ctrResUP" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <input id="orderBy" name="orderBy" type="hidden" />
                <asp:Repeater ID="resourceTable" runat="server" OnLoad="itemRepeater_Load" OnItemCreated="OnRepeater_ItemCreated" OnItemDataBound="OnRepeater_ItemDataBound">
                    <HeaderTemplate>
                        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr>
                                <td class="ReportHeader"  style="padding-left: 20px;">
                                   <a id="HostName" onclick="return setOrder('HostName');" onserverclick="OnResource_SortClick" runat="server">
                                        <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_31%>"></asp:Literal></a>
                                </td>
                                 <td class="ReportHeader">
                                   <a id="VmRunningCount" onclick="return setOrder('VmRunningCount');" onserverclick="OnResource_SortClick" runat="server">
                                    <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_32%>"></asp:Literal>
                                    </a>
                                 </td> 
                                 <td class="ReportHeader">
                                   <a id="CPULoad" onclick="return setOrder('CPULoad');" onserverclick="OnResource_SortClick" runat="server">
                                            <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_33%>"></asp:Literal></a>
                                </td> 
                                <td class="ReportHeader">
                                   <a id="PercentMemoryUsed" onclick="return setOrder('PercentMemoryUsed');" onserverclick="OnResource_SortClick" runat="server">
                                             <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_34%>"></asp:Literal></a>
                                </td>
                                 <td class="ReportHeader">
                                   <a id="PercentNetworkUsed" onclick="return setOrder('PercentNetworkUsed');" onserverclick="OnResource_SortClick" runat="server">
                                            <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_35%>"></asp:Literal></a>
                                </td>
                                <!--<td class="ReportHeader">
                                   <a id="StoragePercentageUsage" onclick="return setOrder('StoragePercentageUsage');" onserverclick="OnResource_SortClick" runat="server">
                                            <asp:Literal runat="server" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_VB0_36%>"></asp:Literal></a>
                                </td> -->
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                             
                            <td>
                                <orion:EntityStatusIcon ID="hostStatusIcon" runat="server" Entity='<%#Eval("EntityName")%>' />
                                <asp:Literal runat="server" ID="hostLink" />
                            </td>

                            <td class="Property">
                                <%# String.Format(Resources.VIMWebContent.VIMWEBCODE_VB0_11, Eval("VmRunningCount"), Eval("VmCount"))%>
                            </td>
                            <td class="Property">
                                <%# SolarWinds.VIM.Web.Helpers.FormatHelper.GetPercentValue((System.Single)Eval("CPULoad"))%>&nbsp;%
                            </td>
                            <td class="Property">
                                <%# SolarWinds.VIM.Web.Helpers.FormatHelper.GetPercentValue((System.Single)Eval("PercentMemoryUsed"))%>&nbsp;%
                            </td>
                            <td class="Property">
                                <%# SolarWinds.VIM.Web.Helpers.FormatHelper.GetPercentValue(Convert.ToDouble(Eval("PercentNetworkUsed")))%>&nbsp;%
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</orion:resourceWrapper>
