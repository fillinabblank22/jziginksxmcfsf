<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenManagedVMwareGuestsByCurrentResponseTime.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_TopXXManagedVMwareGuestsByCurrentResponseTime" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
    <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
    </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
    <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst" width="60%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                    <td class="ReportHeader" width="20%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_38%></td>
                    <td class="ReportHeader" width="20%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_39%></td>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("GuestStatus")%>' />
                    <a href="<%#BaseResourceControl.GetViewLink((string)Eval("NodeID", "N:{0}"))%>">
                         <%# HttpUtility.HtmlEncode(Eval("GuestName"))%>
                    </a>
                </td> 
                <td class="Property vimTopXXProperty"><%# Eval("ResponseTime")%>&nbsp;<%= Resources.VIMWebContent.VIMWEBDATA_VB0_40%></td>
                <td  class="Property vimTopXXProperty"><%# Eval("PercentLoss")%>&nbsp;%</td>
           </tr>
       </ItemTemplate>
       <FooterTemplate>
           </table>
       </FooterTemplate>
      </asp:Repeater>
    </Content>    
</orion:resourceWrapper>
