<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMWareClusterMemoryUsageGraph.ascx.cs" Inherits="Orion_APM_Resources_Cluster_VMWareClusterMemoryUsageGraph" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="chartPlaceHolder" />
    </Content>
</orion:resourceWrapper>