<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMWareClusterDetails.ascx.cs"
    Inherits="Orion_VIM_Resources_Virtualization_ClusterDetails" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Constants" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Extensions" %>
<%@ Import Namespace="SolarWinds.VIM.Web.NetObjects" %>
<%@ Register TagPrefix="orion" TagName="PollingWarning" Src="~/Orion/VIM/Controls/PollingWarning.ascx" %>
<%@ Register TagPrefix="vim" TagName="VSanDedupControl" Src="~/Orion/VIM/Controls/VSanDedupControl.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <script type="text/javascript">
            function restrictDemoMode() {
                var isDemo = '<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>';
                if (isDemo && isDemo == "True") {
                    demoAction(null, null);
                }
            }
        </script>

        <% if (Cluster != null)
           {%>
        <table class="NeedsZebraStripes sw-vim-entityDetails">
            <tr>
                <td class="PropertyHeader" style="width: 50%">
                    <%= VIMWebContent.VIMWEBDATA_AK0_21 %>
                </td>
                <td class="Property">
                    <span>
                        <% if (Cluster.Model.Platform == VirtualizationPlatform.Vmware || Cluster.Model.Platform == VirtualizationPlatform.HyperV)
                           { %>
                        <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                        <a href="<%= GetVirtualizationEntityEditLink() %>" id="editProperties" >
                            <%= VIMWebContent.VIMWEBDATA_LV0_36 %>
                        </a>
                        <a href="<%= GetPerfStackLink() %>" class="vim-perfstack-icon vim-perfstack-icon-position-on-top">
                            <%= VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
                        </a>
                        <% } %>
                        <% if (Cluster.Model.Platform == VirtualizationPlatform.Nutanix)
                            { %>
                            <a href="<%= GetNutanixPrismConsoleLink() %>" data-automation="openPrismWebConsole" class="vim-nutanix-icon-mono" target="_blank" rel="noopener noreferrer" onclick="restrictDemoMode()">
                                <%= VIMWebContent.EntityDetails_Management_OpenPrismElementWebConsole %>
                            </a>
                        <% } %>
                    </span>
                </td>
            </tr>
            <% if (Cluster.Model.Platform == VirtualizationPlatform.Nutanix)
               { %>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.Web_NutanixClusterDetail_PollingEngine %></td>
                <td class="Property"><%= GetNutanixClusterPollingEngine() %></td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_77%></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(Cluster.Model.Name)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_78%></td>
                <%= ManagedStatusHelper.GetClusterStatusTD(Cluster.Model.Status, Cluster.Model.Platform)%>
            </tr>

            <% if (!string.IsNullOrEmpty(Cluster.Model.StatusDetails))
               { %>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.Web_ClusterStatusDetails_Title %></td>
                    <td class="Property"><%= HttpUtility.HtmlEncode(Cluster.Model.StatusDetails) %></td>
                </tr>
                <% } %>


            <% if (Cluster.Model.Platform == VirtualizationPlatform.Vmware) { %>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.Web_Virtual_SAN %></td>
                    <td class="Property"><%= HasVSanEnabled() ? VIMWebContent.Web_Enabled : VIMWebContent.Web_Disabled %></td>
                </tr>

                <% if (VSanClusterSpace != null) { %>
                    <tr>
                        <td class="PropertyHeader"><%= VIMWebContent.Web_Virtual_SAN_Total_Used_Capacity %></td>
                        <td class="Property">
                            <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_11,
                                    SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(VSanClusterSpace.Used, ByteDisplayValueFormat.Short),
                                    SolarWinds.VIM.Web.Common.Helpers.FormatHelper.FormatBytes(VSanClusterSpace.Total, ByteDisplayValueFormat.Short)
                                    ) %>
                            <%= SolarWinds.VIM.Web.Helpers.FormatHelper.FormatPercentBar(VSanClusterSpace.UsedPercentage, ThresholdType.SpaceUsage)%>
                        </td>
                    </tr>
                <% } %>

                <% if (VSanDataStoreModel != null) { %>
                    <tr>
                        <td class="PropertyHeader"><%= VIMWebContent.Web_Related_vSAN_Datastore%></td>
                        <td class="Property">
                            <%= ManagedStatusHelper.GetStatusIcon(VSanDataStoreModel.OrionStatus, SwisEntityNames.Vim.DataStores, VirtualizationPlatform.Vmware) %>
                            <a href="<%=BaseResourceControl.GetViewLink(String.Format("VMS:{0}", VSanDataStoreModel.ID))%>">
                                <%= HttpUtility.HtmlEncode(VSanDataStoreModel.Name)%>
                            </a>
                        </td>
                    </tr>
                <% } %>
                <vim:VSanDedupControl runat="server" ID="vsanControl"/>
            <% } %>

            <%-- single polling source --%>
            <% if (HostPollingSourceStats.Count <= 1)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.EntityDetails_PollingSource %></td>
                    <td class="Property"><%= PollingWarning.LocalizedPollingSource%></td>
                </tr>
            <% } %>
            <%-- multiple polling sources resolved from contained hosts - warning needed --%>
            <% if (HostPollingSourceStats.Count > 1)
               { %>
                <tr>
                    <td class="PropertyHeader" style="vertical-align: top">
                        <%= VIMWebContent.EntityDetails_PollingSource %>
                    </td>
                    <td class="Property">
                        <%= FormatHostPollingSources() %>

                        <div class="warning">
                            <%= GetHostsPollingSourceMisconfigurationWarning() %>
                        </div>
                    </td>
                </tr>
            <% } %>

            <% if (PollingWarning.Visible)
               { %>
                <tr></tr>
                <tr>
                    <td style="padding-top:0" colspan="2">
                        <orion:PollingWarning ID="PollingWarning" runat="server" EntityId='<%# Cluster.Id%>' PollingSource='<%# Cluster.Model.PollingSource%>' ViewType='<%# ViewType.Cluster%>' />
                    </td>
                </tr>
            <% } %>

            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_79%></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(Cluster.Model.OSVersion)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_80%></td>
                <td class="Property"><%= Cluster.Model.CPUCoreCount%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_81%></td>
                <td class="Property"><%= Cluster.Model.CPUThreadCount%></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_82%></td>
                <td class="Property">
                    <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_44, Cluster.Model.TotalCPU)%>
                </td>
            </tr>

            <% if (Cluster.Model.Platform == VirtualizationPlatform.Vmware)
               {%>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_83%></td>
                <td class="Property">
                    <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_44, Cluster.Model.EffectiveCPU)%>
                </td>
            </tr>
            <% } %>

            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_84%></td>
                <td class="Property">
                    <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_45, Math.Round((float)Cluster.Model.TotalMemory / 1024 / 1024 / 1024,2))%>
                </td>
            </tr>

            <% if (Cluster.Model.Platform == VirtualizationPlatform.Vmware)
               { %>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_85 %></td>
                <td class="Property">
                    <%= String.Format(VIMWebContent.VIMWEBCODE_VB0_45, Math.Round((float) Cluster.Model.EffectiveMemory / 1024, 2)) %>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_86 %></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(SolarWinds.VIM.Web.Helpers.FormatHelper.GetDRSBehaviourText(Cluster.Model.DRSBehaviour)) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_87 %></td>
                <td class="Property"><%= Cluster.Model.DRSVmotionRate %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_88 %></td>
                <td class="Property"><%= GetLocalizedBool(Cluster.Model.HAStatus) %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_89 %></td>
                <td class="Property"><%= Cluster.Model.FailOverLevel %></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_90 %></td>
                <td class="Property">
                    <a href="<%= BaseResourceControl.GetViewLink(String.Format("N:{0}", Cluster.Model.VCenterNodeID)) %>">
                        <%= HttpUtility.HtmlEncode(Cluster.Model.VCenterName) %>
                    </a>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= VIMWebContent.VIMWEBDATA_VB0_91 %></td>
                <td class="Property">
                    <a href="<%= BaseResourceControl.GetViewLink(String.Format("VMD:{0}", Cluster.Model.DataCenterID)) %>">
                        <%= HttpUtility.HtmlEncode(Cluster.Model.DataCenterName) %>
                    </a>
                </td>
            </tr>
                <% } %>
            <% if(RelatedClusters.Any()) { %>
                <tr>
                    <td class="PropertyHeader"><%= VIMWebContent.Web_ClusterDetails_RelatedClusters%></td>
                    <td class="Property">
                        <% for (int i = 0; i < RelatedClusters.Count; i++) { %>
                            <div>
                                <% var relatedCluster = RelatedClusters[i]; %>
                                <%= PlatformHelper.GetPlatformIcon(relatedCluster.Platform) %>
                                <%= ManagedStatusHelper.GetStatusIcon((int) relatedCluster.ManagedStatus, SwisEntityNames.Vim.Clusters, relatedCluster.Platform) %>
                                <a href="<%=BaseResourceControl.GetViewLink($"{VimCluster.Prefix}:{relatedCluster.ID}")%>"> <%= HttpUtility.HtmlEncode(relatedCluster.Name)%> </a>
                            </div>
                        <% } %>
                    </td>
                </tr>
           <% } %>
           </table>
           <% } %>
    </Content>
</orion:resourceWrapper>
