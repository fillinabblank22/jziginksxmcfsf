<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenManagedVMwareGuestsByPercentPacketLoss.ascx.cs" Inherits="Orion_VIM_Resources_Cluster_TopTenManagedVMwareGuestsByPercentPacketLoss" %>
<%@ Register TagPrefix="orion" TagName="PercentStatusBar" Src="~/Orion/Controls/PercentStatusBar.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI"%>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
   <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
   </asp:Panel>
    <asp:Repeater runat="server" ID="resourceTable">
       <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="ReportHeader vimTopXXHeaderFirst" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                    <td class="ReportHeader" colspan="2" width="10%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_30%></td>
                </tr>
       </HeaderTemplate>
       <ItemTemplate>
          <tr>
             <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("GuestStatus")%>' />
                      <a href="<%#BaseResourceControl.GetViewLink((string)Eval("NodeID", "N:{0}"))%>">
                         <%# HttpUtility.HtmlEncode(Eval("GuestName"))%>
                    </a>
                </td> 
             <td class="Property vimTopXXProperty"><%#Eval("PercentLoss")%>&nbsp;%</td>
             <td>
                <orion:PercentStatusBar ID="PercentStatusBar1" runat="server" visible='<%#GetBarVisible()%>' Status=<%#SolarWinds.VIM.Web.Helpers.FormatHelper.GetPercentValue(Convert.ToDouble(Eval("PercentLoss")))%> />
            </td>
          </tr>
      </ItemTemplate>    
      <FooterTemplate>
            </table>
      </FooterTemplate>
     </asp:Repeater>
   </Content>    
</orion:resourceWrapper>
