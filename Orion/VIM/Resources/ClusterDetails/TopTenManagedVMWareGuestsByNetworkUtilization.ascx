<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenManagedVMWareGuestsByNetworkUtilization.ascx.cs"
    Inherits="Orion_VIM_Resources_Cluster_TopTenManagedVMWareGuestsByNetworkUtilization" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_28%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst" width="40%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                        <td class="ReportHeader" width="20%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_41%></td>
                        <td class="ReportHeader" width="20%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_42%></td>
                        <td class="ReportHeader" width="20%" ><%= Resources.VIMWebContent.VIMWEBDATA_VB0_43%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>'
                            Status='<%#Eval("Status")%>' />
                        <%#GetNodeNameWithLink((string)Eval("GuestName"), Eval("NodeID"))%>
                    </td>
                    <td class="Property vimTopXXProperty" ><%# FormatHelper.FormatNetworkUtilizationForResource((float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
                    <td class="Property vimTopXXProperty" ><%# FormatHelper.FormatNetworkUtilizationForResource((float)Eval("xmitKBps"), (float)Eval("rcvKBps"))%></td>
                    <td class="Property vimTopXXProperty" ><%# FormatHelper.FormatNetworkUtilizationForResource((float)Eval("usageKBps"), (float)Eval("rcvKBps"), (float)Eval("xmitKBps"))%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
