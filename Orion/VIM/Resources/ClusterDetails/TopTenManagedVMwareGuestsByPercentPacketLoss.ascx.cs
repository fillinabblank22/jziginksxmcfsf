using System;
using System.Collections.Generic;
using System.Data;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Cluster_TopTenManagedVMwareGuestsByPercentPacketLoss : VimTopXXResourceControl
{
    public int WarningLevel { get; set; }
    public int ErrorLevel { get; set; }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_VB0_4; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table;
        try
        {
            VimCluster cluster = GetInterfaceInstance<IVimClusterProvider>().Cluster;
            var dal = new TopTenManagedVMGuestsByPercentPacketLossDAL();
            table = dal.GetTopTenVMWareGuestsByPercentPacketLoss(this.MaxRecords, this.Resource.Properties["Filter"],cluster.Id);
        }
        catch (System.Data.SqlClient.SqlException)
        {
            this.SQLErrorPanel.Visible = true;
            return;
        }

        if (table != null && table.Rows.Count > 0)
        {
            ErrorLevel = Convert.ToInt32(Thresholds.PacketLossError.SettingValue);
            WarningLevel = Convert.ToInt32(Thresholds.PacketLossWarning.SettingValue);

            PlatformHelper.AddEntityNameColumn(table, "EntityName", "PlatformID", SolarWinds.VIM.Web.StatusIcons.VimIconProvider.VirtualMachineEntityName);

            this.resourceTable.DataSource = table;
            this.resourceTable.DataBind();
        }
        else
        {
            this.resourceTable.Visible = false;
        }
    }

    public override Dictionary<string, string> InitialProperties
    {
        get { return new Dictionary<string, string> {{"EntityName", "Orion.VIM.VirtualMachines"}}; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VMGuestsTopXXEditControl.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVIMPH_ClusterDetails_TopXXManagedVMwareGuestsByPercentPacketLoss"; }
    }

    public Boolean GetBarVisible()
    {
        return true;
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IVimClusterProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
