<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTenManagedVMWareGuestsByCPULoad.ascx.cs" Inherits="Orion_VIM_Resources_Cluster_TopXXManagedVMWareGuestsByCPULoad" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_29%></td>
                        <td class="ReportHeader" colspan="2" width="10%"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_76%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity='<%#Eval("EntityName")%>' Status='<%#Eval("GuestStatus")%>' />
                          <%#GetNodeNameWithLink((string)Eval("VMWareGuest"), Eval("NodeID"))%>
                    </td> 
                    <td class="Property vimTopXXProperty"><%# FormatHelper.FormatPercent(Eval("CPULoad"), this.type)%></td>
                    <td class="Property vimTopXXProperty"><%# FormatHelper.FormatPercentBar(Eval("CPULoad"),this.type)%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</orion:resourceWrapper>
