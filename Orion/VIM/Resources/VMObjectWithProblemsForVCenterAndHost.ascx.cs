﻿using System;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_VIM_Resources_VMObjectWithProblemsForVCenterAndHost : VimVMObjectsWithProblemsControl
{
    protected void Page_Load(object sender, EventArgs ea)
    {
        this.impl.Resource = this;
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get {
            return new Type[] { typeof(INodeProvider) };
        }
    }

    protected override int GetParentId()
    {
        var nodeId = ParentNodeId;
        switch (DetectedViewType)
        {
            case SolarWinds.VIM.Web.Common.Enums.ViewType.vCenter:
                {
                    var vc = new VCenterDAL();
                    return vc.GetVCenterModelByNodeId(nodeId).ID;
                }
            case SolarWinds.VIM.Web.Common.Enums.ViewType.Host:
                {
                    var nc = new HostDAL();
                    return nc.GetHostModelByNodeId(nodeId).ID;
                }
            default:
                throw new NotImplementedException();
        }
        
    }

    protected override int GetParentNodeId()
    {
        var inp = this.GetInterfaceInstance<INodeProvider>();
        if (inp != null)
            return inp.Node.NodeID;
        else
            return -1; // in some cases (EditCusotmObjectResource) this fails, but it doesn't bother us.
    }

}