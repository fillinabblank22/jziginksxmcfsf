<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListOfVMs.ascx.cs" Inherits="Orion_VIM_Resources_HostDetails_ListOfVMs" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Extensions" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Register Src="~/Orion/VIM/Controls/MessagePanel.ascx" TagPrefix="vim" TagName="MessagePanel" %>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="false">
    <Content>
        <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
        <vim:MessagePanel ID="AlternativeContent" Heading="<%# Resources.VIMWebContent.VIMWEBDATA_SH0_1%>" Description="<%# Resources.VIMWebContent.VIMWEBDATA_SH0_2%>" runat="server" />
        <asp:Panel runat="server" ID="Content">
            <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                <thead>
                    <tr align="left">
                        <th colspan="2"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_14%></th>
                        <th><%= Resources.VIMWebContent.VIMWEBDATA_AK0_15%></th>
                        <th colspan="2"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_16%></th>
                        <th><%= Resources.VIMWebContent.VIMWEBDATA_AK0_17%></th>
                        <th><%= Resources.VIMWebContent.VIMWEBDATA_AK0_18%></th>
                        <th><%= Resources.VIMWebContent.VIMWEBDATA_AK0_19%></th>
                    </tr>
                </thead>
                <asp:Repeater runat="server" ID="repVms" OnItemDataBound="repVms_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td runat="server" id="nodeStatusImage"></td>
                            <td runat="server" id="litName"> </td>
                            <td><%# HttpUtility.HtmlEncode(SolarWinds.VIM.Common.Helpers.DBHelper.GetDbValueOrDefault<string>(Eval("GuestName"), String.Empty).Replace("Microsoft", "").Replace("Standard Edition", "Std.").Replace("Enterprise Edition", "Ent."))%></td>
                            <td id="powerStateImageTd" runat="server"></td>
                            <td id="powerStateTextTd" runat="server" style="width: 70px;"></td>
                            <td style="text-align: right; padding-right: 10px; width: 55px;"><%# new SolarWinds.Orion.Web.DisplayTypes.Bytes(SolarWinds.VIM.Common.Helpers.DBHelper.GetDbValueOrDefault<Int64>(Eval("MemoryConfigured"), 0)).ToString("{0:Short}", new SolarWinds.Orion.Web.DisplayTypes.Byte1024FormatInfo())%></td>
                            <td style="text-align: right; padding-right: 15px; width: 50px;"><asp:Literal ID="litCpuShares" runat="server" />&nbsp; </td>
                            <td style="width: 65px;"><%# FormatIpAddress(SolarWinds.VIM.Common.Helpers.DBHelper.GetDbValueOrDefault<string>(Eval("IPAddress"), String.Empty)).HtmlEncode()%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </asp:Panel>
    </Content>
</orion:resourceWrapper>
