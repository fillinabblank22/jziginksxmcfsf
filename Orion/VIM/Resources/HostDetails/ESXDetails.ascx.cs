using SolarWinds.Orion.Common;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.Controls;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Models;
using SolarWinds.VIM.Web.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.VIM.Common.Constants;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Common.Extensions;
using WebHelpers = SolarWinds.VIM.Web.Common.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VMWare_ESXDetails : VimBaseResource
{
    private HostModel host;
    protected NutanixDiscoveryMetadataModel? NutanixDiscoveryMetadata { get; private set; }
    protected PollingSource PollingSource { get; private set; }
    protected int HostId { get; private set; }
    protected int? HostsNodeId { get; private set; }
    public HostModel Host
    {
        get
        {
            if(host == null)
            {
                host = (new VMHostDetailsDAL()).GetHostDetails(NodeId);
            }
            return host;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get { yield return typeof (INodeProvider); }
    }

    protected override string DefaultTitle
    {
        get
        {
            string defaultTitle = Resources.VIMWebContent.HostDetails_DefaultHostDetailsTitle;
            if (Host != null)
            {
                if (Host.Platform == VirtualizationPlatform.HyperV)
                {
                    defaultTitle = Resources.VIMWebContent.VIMWEBCODE_DO0_1;
                }
                else if (Host.Platform == VirtualizationPlatform.Nutanix)
                {
                    defaultTitle = Resources.VIMWebContent.WidgetTitle_ahv_host_details;
                }
                else if (Host.Platform == VirtualizationPlatform.Vmware)
                {
                    defaultTitle = Resources.VIMWebContent.VIMWEBCODE_AK0_8;
                }
            }
            return defaultTitle;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected bool IsDemoServer
    {
        get { return OrionConfiguration.IsDemoServer; }
    }

    public override string HelpLinkFragment { get { return "OrionVIMPH_ESXDetails_ESXDetails"; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Host != null && DetectedViewType == ViewType.Host)
        {
            HostId = Host.ID;
            HostsNodeId = Host.NodeID;
            PollingSource = Host.PollingSource;

            esxOperationalState.RenderedControlType = EsxOperationalState.ControlType.Icon | EsxOperationalState.ControlType.Text;
            esxOperationalState.State = new ConnectionState(Host.ConnectionState);

            if (HasVCenter)
            {
                vCenterStatusIcon.Entity = "Orion.VIM.VCenters";
                vCenterStatusIcon.Status = Host.VCenterStatus;

                vCenterLink.Text = Host.VCenterName.HtmlEncode();
                vCenterLink.NavigateUrl = String.Format("/Orion/View.aspx?NetObject=N:{0}", Host.VCenterNodeID);
            }

            if (Host.Platform == VirtualizationPlatform.Nutanix)
            {
                clusterStatusIcon.Entity = SwisEntityNames.Vim.Clusters;
                clusterStatusIcon.Status = Host.ClusterStatus;
                clusterLink.Text = Host.ClusterName.HtmlEncode();
                clusterLink.NavigateUrl = String.Format("/Orion/View.aspx?NetObject=VMC:{0}", Host.ClusterID);

                if (Host.ClusterID.HasValue)
                {
                    NutanixDiscoveryMetadata = new ClusterDAL().GetNutanixDiscoveryMetadataForCluster(Host.ClusterID.Value);
                }
            }

            if (HasAdditionalConsoles)
            {
                candidateNodesRepeater.DataSource = Host.AdditionalServiceConsoles;
                candidateNodesRepeater.DataBind();
            }
        }
        else
        {
            // hide for non ESX
            this.Visible = false;
            return;
        }

        PollingWarning.DataBind();
    }

    protected string GetNutanixPrismConsoleLink()
    {
        if (OrionConfiguration.IsDemoServer || !NutanixDiscoveryMetadata.HasValue)
        {
            return string.Empty;
        }

        return string.Format("https://{0}:{1}/console/#page/hardware/table/?action=details&actionTargetName={2}&actionTarget=host", NutanixDiscoveryMetadata.Value.IpAddress, NutanixDiscoveryMetadata.Value.ApiPort, host.Name);
    }

    protected string GetNutanixPrismCentralLink()
    {
        if (OrionConfiguration.IsDemoServer || !NutanixDiscoveryMetadata.HasValue || string.IsNullOrEmpty(NutanixDiscoveryMetadata.Value.PrismCentralIpAddress))
        {
            return string.Empty;
        }

        return string.Format("https://{0}:{1}/console/#page/explore/s/{2}",
            NutanixDiscoveryMetadata.Value.PrismCentralIpAddress,
            NutanixDiscoveryMetadata.Value.PrismCentralApiPort,
            Host.Name);
    }

    protected bool HasVCenter
    {
        get
        {
            return (Host.PollingMethod == HostPollingMethod.ThroughVCenter)
                   && (Host.VCenterID != 0);
        }
    }

    protected bool HasAdditionalConsoles
    {
        get
        {
            return Host.AdditionalServiceConsoles.Any();
        }
    }

    public string GetMemoryUsage(Int64 memorySize)
    {
        return WebHelpers.FormatHelper.FormatBytes(memorySize);
    }

    public string GetNodeDetailsUrl(int id)
    {
        return String.Format("/Orion/View.aspx?NetObject=N:{0}", id);
    }

    protected string GetVirtualizationEntityEditLink()
    {
        ReferrerRedirectorBase.SetReturnUrl(ToAbsoluteUrl(host.DetailsUrl));
        return String.Format("/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?NetObject=VH:{0}&ReturnTo={1}", NodeId, ReferrerRedirectorBase.GetReturnUrl());
    }

    protected string GetPerfStackLink()
    {
        return PerfStackLinkBuilder.GetHostLink(Host.ID);
    }
}
