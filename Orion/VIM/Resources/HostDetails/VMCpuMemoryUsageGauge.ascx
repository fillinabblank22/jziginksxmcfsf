﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMCpuMemoryUsageGauge.ascx.cs"
    Inherits="Orion_NetPerfMon_Resources_VMWare_VMCpuMemoryUsageGauge" %>
<%@ Register Src="~/Orion/NetPerfMon/Controls/ThresholdButton.ascx" TagPrefix="orion" TagName="ThresholdButton" %>
<orion:Include runat="server" File="GaugeStyle.css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
            <tr>
                <td>
                    <a href="/Orion/NetPerfMon/CustomChart.aspx?chartName=HostAvgCPULoad&NetObject=<%=GetCurrentNetObject.Node.NetObjectID%>&Period=Today" target="_blank">
                        <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
                    </a>
                </td>
                <td>
                    <a href="/Orion/NetPerfMon/CustomChart.aspx?chartName=HostAvgPercentMemoryUsed&NetObject=<%=GetCurrentNetObject.Node.NetObjectID%>&Period=Today" target="_blank">
                        <asp:PlaceHolder runat="server" ID="gaugePlaceHolder2" />
                    </a>
                </td>
            </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
    <HeaderButtons>
        <orion:ThresholdButton runat="server"/>
    </HeaderButtons>
</orion:resourceWrapper>
