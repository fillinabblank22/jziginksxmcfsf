﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostCPUMemoryNetwork.ascx.cs" Inherits="Orion_VIM_Resources_HostDetails_HostCPUMemoryNetwork" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>

<orion:Include ID="Include1" runat="server" File="GaugeStyle.css" />

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder ID="Source" runat="server">
        <br />
        <table class="GaugeTable">
            <tr>
                <td>
                    <asp:PlaceHolder runat="server" ID="gaugePlaceHolder1" />
                </td>
                <td>
                    <asp:PlaceHolder runat="server" ID="gaugePlaceHolder2" />
                </td>
                <td>
                    <asp:PlaceHolder runat="server" ID="gaugePlaceHolder3" />
                </td>
            </tr>
       </table>
       </asp:PlaceHolder>
       <asp:Literal ID="ErrorLiteral" runat="server" />
    </Content>
</vman:resourceWrapper>