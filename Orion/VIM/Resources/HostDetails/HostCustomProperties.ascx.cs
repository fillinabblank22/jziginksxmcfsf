﻿using System;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_HostDetails_HostCustomProperties : VimBaseResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (SolarWinds.Orion.NPM.Web.NodeHelper.IsResourceUnderExternalNode(this) || DetectedViewType != ViewType.Host)
        {
            this.Visible = false;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        customPropertyList.Resource = Resource;
        customPropertyList.NetObjectId = NodeId;

        customPropertyList.CustomPropertyLoader = (netObjectId, displayProperties) =>
        {
            return new HostDAL().GetCustomProperties(NodeId, displayProperties);
        };

        customPropertyList.EditCustomPropertiesLinkGenerator = (netObjectId, redirectUrl) =>
        {
            return "/Orion/Admin/CPE/Default.aspx";
        };
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.Web_HostCustomProperties_DefaultTitle;
        }
    }
    
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}