using System;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.Charting;
using SolarWinds.Orion.Web.Charting;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class VMCPUAreaChart : GraphResource
{
    protected override void OnInit(EventArgs e)
    {
        Node node = GetInterfaceInstance<INodeProvider>().Node;

        if (node != null && (new VMHostDetailsDAL()).IsNodeEsxHost(node.NodeID))
        {
            CreateChart(null, node.NetObjectID, "HostVMCPUConsumptionArea", chartPlaceHolder);
            Wrapper.SetDrDownMenuParameters("HostVMCPUConsumptionArea", Resource, string.Format("N{0}", GetTitlePrefix()));
        }
        else
        {
            // Node is not ESX from VIM stand point, resource will not be showed at all.
            this.Visible = false;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_PS0_2;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVMCPUConsumptionAreaChart"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditVimChartControl.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
