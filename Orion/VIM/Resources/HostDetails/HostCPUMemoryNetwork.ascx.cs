﻿using System;
using System.Data;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.NetObjects;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Gauges)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_HostDetails_HostCPUMemoryNetwork : VimHostBaseResource
{
    private string _gaugeStyle;
    private int _gaugeScale;
    private string _gaugeRangeMethod;
    private string _gaugeRangeValue;
    private DataTable _propertiesTable;

    protected string RedirectLink
    {
        get
        {
            return HttpContext.Current.Request.Url.AbsoluteUri; // absolute URL
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (DetectedViewType != ViewType.Host ||
            (Host != null && (!_pollingMetadataProvider.HostPropertyIsPolled(h => h.CpuLoad, Host.Model.Platform) ||
                              !_pollingMetadataProvider.HostPropertyIsPolled(h => h.MemUsage, Host.Model.Platform) ||
                              !_pollingMetadataProvider.HostPropertyIsPolled(h => h.NetworkUsageRate, Host.Model.Platform))))
        {
            this.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _gaugeStyle = GetStringValue("Style", "Elegant Black");
        _gaugeScale = GetIntProperty("Scale", 100);
        _gaugeRangeMethod = GetStringValue("RangeMethod", "static");

        switch (_gaugeRangeMethod)
        {
            case "static":
                _gaugeRangeValue = GetIntProperty("StaticRange", 100).ToString();
                break;
            case "dynamic":
                _gaugeRangeValue = GetStringValue("DynamicRange", "10, 20, 40, 60, 100, 200, 400, 600, 1000");
                break;
            case "thresholds":
                _gaugeRangeValue = GetStringValue("ThresholdsRange", "1.3");
                break;
        }

        var hostNetObjectId = string.Format("{0}:{1}", VimHost.Prefix, NodeId);
        var chartLink = string.Format("/Orion/DetachResource.aspx{0}", HttpContext.Current.Request.Url.Query);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder1,
            _gaugeScale,
            _gaugeStyle,
            "CPU",
            chartLink,
            hostNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_LM0_05,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_LM0_08);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder2,
            _gaugeScale,
            _gaugeStyle,
            "MEMORY",
            chartLink,
            hostNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_LM0_06,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_LM0_08);

        GaugeHelper.CreateGauge(
            gaugePlaceHolder3,
            _gaugeScale,
            _gaugeStyle,
            "NETWORK",
            chartLink,
            hostNetObjectId,
            Resources.VIMWebContent.VIMWEBCODE_LM0_07,
            "Radial",
            _gaugeRangeMethod,
            0,
            _gaugeRangeValue,
            1,
            " " + Resources.VIMWebContent.VIMWEBCODE_LM0_08);
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHIOPSLatency"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGauge.ascx"; }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_LM0_04;
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }
}