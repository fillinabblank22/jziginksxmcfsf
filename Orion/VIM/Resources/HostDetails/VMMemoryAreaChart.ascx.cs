using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.Orion.Web.UI;

public partial class VMMemoryAreaChart : GraphResource
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Node node = GetInterfaceInstance<INodeProvider>().Node;

        if (node != null && (new VMHostDetailsDAL()).IsNodeEsxHost(node.NodeID))
        {
            CreateChart(null, node.NetObjectID, "HostVMMemoryConsumptionArea", chartPlaceHolder);
            Wrapper.SetDrDownMenuParameters("HostVMMemoryConsumptionArea", Resource, string.Format("N{0}", GetTitlePrefix()));
        }
        else
        {
            // Node is not ESX from VIM stand point, resource will not be showed at all.
            this.Visible = false;
        }
    }

    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBCODE_PS0_1;
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVMMemoryConsumptionAreaChart"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditVimChartControl.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
