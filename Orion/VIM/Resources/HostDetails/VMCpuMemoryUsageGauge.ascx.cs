﻿using System;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_NetPerfMon_Resources_VMWare_VMCpuMemoryUsageGauge : GaugeResource<INodeProvider>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((new VMHostDetailsDAL()).IsNodeEsxHost(GetInterfaceInstance<INodeProvider>().Node.NodeID))
        {
            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Elegant Black"),
                Resources.VIMWebContent.VIMWEBCODE_AK0_10, gaugePlaceHolder1, "/Orion/NetPerfMon/CustomChart.aspx?chartName=HostAvgCPULoad&NetObject=" + GetCurrentNetObject.Node.NetObjectID + "&Period=Today",
                GetCurrentNetObject.Node.NetObjectID, "TotalVMCPULoad", "Radial", " %", 0, 100);


            CreateGauge(GetIntProperty("Scale", 100), GetStringValue("Style", "Elegant Black"),
                Resources.VIMWebContent.VIMWEBCODE_AK0_11, gaugePlaceHolder2, "/Orion/NetPerfMon/CustomChart.aspx?chartName=HostAvgPercentMemoryUsed&NetObject=" + GetCurrentNetObject.Node.NetObjectID + "&Period=Today",
                GetCurrentNetObject.Node.NetObjectID, "TotalVMMemoryUsage", "Radial", " %", 0, 100);
        }
        else
        {
            this.Visible = false;
        }
    }

    public override Control GetSourceControl() { return Source; }

    public override ITextControl GetErrorControl() { return (ITextControl)ErrorLiteral; }

    public override string GetErrorText() { return string.Format(Resources.VIMWebContent.VIMWEBCODE_AK0_12, "<a href=\"/Orion/Nodes/Default.aspx\">", "</a>"); }

    protected override string DefaultTitle { get { return Resources.VIMWebContent.VIMWEBCODE_AK0_13; } }

    public override string HelpLinkFragment { get { return "OrionPHResourceCPULoadGauge"; } }

    public override bool isError()
    {
        if (base.isError()) return true;

        return false;
    }

    public override string EditControlLocation
    {
        get { return "/Orion/NetPerfMon/Controls/EditResourceControls/EditGauge.ascx"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}
