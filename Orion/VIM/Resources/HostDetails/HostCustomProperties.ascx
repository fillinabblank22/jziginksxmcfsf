﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostCustomProperties.ascx.cs" Inherits="Orion_VIM_Resources_HostDetails_HostCustomProperties" %>
<%@ Register TagPrefix="orion" TagName="CustomPropertyList" Src="~/Orion/NetPerfMon/Controls/CustomPropertyList.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <orion:CustomPropertyList runat="server" ID="customPropertyList" CustomPropertyTableName="VIM_HostsCustomProperties"/>
    </Content>
</orion:resourceWrapper>
