<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ESXDetails.ascx.cs" Inherits="Orion_NetPerfMon_Resources_VMWare_ESXDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.VIM.Common" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Models" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Models" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>
<%@ Register TagPrefix="VIM" TagName="AssignCredentialsDialog" Src="~/Orion/VIM/Controls/AssignVMWareCredentialsDialog.ascx" %>
<%@ Register TagPrefix="vim" Namespace="SolarWinds.VIM.Web.Controls" Assembly="SolarWinds.VIM.Web" %>
<%@ Register TagPrefix="vim" Namespace="SolarWinds.VIM.Web.Helpers" Assembly="SolarWinds.VIM.Web" %>
<%@ Register TagPrefix="orion" TagName="PollingWarning" Src="~/Orion/VIM/Controls/PollingWarning.ascx" %>
<style type="text/css">
    .link
    {
        color: #336699;
        text-decoration: underline;
    }
</style>
<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true">
    <Content>
        <script type="text/javascript">
            function vim_hostDetails_restrictDemoMode() {
                var isDemo = '<%= OrionConfiguration.IsDemoServer %>';
                if (isDemo && isDemo == "True") {
                    demoAction(null, null);
                    return false;
                }
            }
        </script>
        <VIM:AssignCredentialsDialog ID="AssignCredentialsDialog" runat="server" debug="false" />

        <table class="sw-vim-entityDetails NeedsZebraStripes">
            <%if (Profile.AllowNodeManagement && this.Host != null && this.Host.NodeID.HasValue && this.Host.Platform == VirtualizationPlatform.Vmware && !this.IsDemoServer)
            {%>
               <tr>
                    <td rowspan = "2" class="PropertyHeader" style="width: 50%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_21%></td>
                    <td class="Property">
                        <span onclick="ShowAssignCredentialDialog(<%= this.Host.NodeID %>); return false;" style="cursor:pointer;" >
                            <img src='/Orion/images/edit_16x16.gif' alt='' />&nbsp;<%= Resources.VIMWebContent.VIMWEBDATA_AK0_33%>
                        </span>
                    </td>
                </tr>
                <tr>
            <%}
            else
            {%>
            <tr>
                 <td class="PropertyHeader" style="width: 50%"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_21%></td>
            <%}%>
                 <td class="Property">
                    <div>
                        <a href="<%=GetVirtualizationEntityEditLink() %>" id="editProperties" class="vim-edit-icon">
                              <%= Resources.VIMWebContent.VIMWEBDATA_LV0_38 %>
                        </a>
                        <a href="<%=GetPerfStackLink() %>" class="vim-detail-perfstack-icon">
                            <%= Resources.VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
                        </a>
                        <% if (Host.Platform == VirtualizationPlatform.Nutanix) { %>
                            <a href="<%= GetNutanixPrismConsoleLink() %>" data-automation="openPrismWebConsole" class="vim-nutanix-icon-mono" target="_blank" rel="noopener noreferrer" onclick="return vim_hostDetails_restrictDemoMode()">
                                <%= Resources.VIMWebContent.EntityDetails_Management_OpenPrismElementWebConsole %>
                            </a>
                        <% } %>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader">
                <%if (this.Host != null && this.Host.Platform != VirtualizationPlatform.Vmware)
                  {%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_DO0_1%>
                <% }
                  else
                  {%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_AK0_22%>
                <%} %>
                </td>
                <%= ManagedStatusHelper.GetHostStatusTD(Host.Status, Host.Platform)%>
            </tr>
            <% if (!String.IsNullOrEmpty(Host.StatusMessage))
                { %>
            <tr>
                <td class="PropertyHeader">
                <%if (this.Host != null && this.Host.Platform == VirtualizationPlatform.HyperV)
                  {%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_DO0_3%>
                <% }
                  else
                  {%>
                        <%= Resources.VIMWebContent.VIMWEBDATA_AK0_23%>
                <%} %>
                </td>
                <td class="Property"><%= HttpUtility.HtmlEncode(Host.StatusMessage) %></td>
            </tr>
            <% } %>
            <% if (this.Host != null && this.Host.Platform == VirtualizationPlatform.Vmware)
               {%>
            <tr>
                <td class="PropertyHeader"><%=Resources.VIMWebContent.VIMWEBDATA_AK0_25%></td>
                <td class="Property"><vim:EsxOperationalState runat="server" ID="esxOperationalState" /></td>
            </tr>
            <% }%>
            <% if (HasVCenter)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_26%></td>
                    <td class="Property">
                        <orion:EntityStatusIcon ID="vCenterStatusIcon" runat="server" WrapInBox="false" />
                        <asp:HyperLink runat="server" ID="vCenterLink" />
                    </td>
                </tr>
            <% } %>
            <% if (this.Host != null && Host.Platform == VirtualizationPlatform.Nutanix)
               { %>
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.Web_HostDetailsResource_PollingTroughCluster%></td>
                    <td class="Property">
                        <orion:EntityStatusIcon ID="clusterStatusIcon" runat="server" WrapInBox="false" />
                        <asp:HyperLink runat="server" ID="clusterLink" />
                    </td>
                </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.EntityDetails_PollingSource%></td>
                <td class="Property"><%= PollingWarning.LocalizedPollingSource %></td>
            </tr>

            <% if (PollingWarning.Visible){ %>
            <tr style="visibility: collapse"></tr>
            <tr>
                <td style="padding-top:0" colspan="2">
                    <orion:PollingWarning ID="PollingWarning" runat="server" EntityId="<%# HostId%>" PollingSource="<%# PollingSource%>" ViewType="<%# ViewType.Host%>" NodeId="<%# HostsNodeId%>" />
                </td>
            </tr>
            <% } %>
            <% if (this.Host != null && this.Host.Platform != VirtualizationPlatform.Nutanix) {%>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_27%></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(Host.VMwareProductName)%></td>
            </tr>

            <tr>
                <td class="PropertyHeader" valign="center"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_28%></td>
                <td class="Property"><%= HttpUtility.HtmlEncode(Host.VMwareProductVersion) %></td>
            </tr>
            <% } %>
            <tr>
                <td class="PropertyHeader" valign="center"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_29%></td>
                <td class="Property"><%= GetMemoryUsage(Host.MemorySize)%></td>
            </tr>
            <tr>
                <td class="PropertyHeader" valign="center"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_30%></td>
                <td class="Property"><%=Host.NumberOfAllVMs%> (<%=string.Format(Resources.VIMWebContent.VIMWEBDATA_AK0_42, Host.NumberOfRunningVMs)%>)</td>
            </tr>
            <% if (Host.BootTime != null)
               { %>
            <tr>
                <td class="PropertyHeader"> <%= Resources.VIMWebContent.VIMWEBCODE_VZ0_5 %></td>
                <td class="Property"> <%= Utils.FormatDateTime(Host.BootTime.Value.ToLocalTime(), true) %> </td>
            </tr>
            <% }
               if (HasAdditionalConsoles)
               { %>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_AK0_31%></td>
                <td class="Property">
                    <asp:Repeater runat="server" ID="candidateNodesRepeater">
                        <ItemTemplate>
                            <orion:EntityStatusIcon ID="EntityStatusIcon" runat="server" WrapInBox="false"
                                Entity="Orion.Nodes"
                                Status="<%# ((HostModel.ServiceConsole)Container.DataItem).Status %>"
                                IconSize="small"
                                EntityId="<%# ((HostModel.ServiceConsole)Container.DataItem).ID %>"
                                Visible="<%# ((HostModel.ServiceConsole)Container.DataItem).ID != 0 %>" />&nbsp;
                            <asp:Literal runat="server" Text="<%# ((HostModel.ServiceConsole)Container.DataItem).Name %>"
                                Visible="<%# ((HostModel.ServiceConsole)Container.DataItem).ID == 0 %>" />
                            <asp:HyperLink runat="server" NavigateUrl="<%# GetNodeDetailsUrl(((HostModel.ServiceConsole)Container.DataItem).ID) %>"
                                Visible="<%# ((HostModel.ServiceConsole)Container.DataItem).ID != 0 %>"><%# ((HostModel.ServiceConsole)Container.DataItem).Name %></asp:HyperLink>
                            <br />
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <% } %>
        </table>
    </Content>
</orion:resourceWrapper>
