using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;
using System.Data;
using SolarWinds.VIM.Web.Controls;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Common.Helpers;
using System.Net;
using System.Net.Sockets;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_HostDetails_ListOfVMs : VimHostBaseResource
{
    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_AK0_7; }
    }

    /// <summary>
    /// Identifies HelpLinkFragment.
    /// </summary>
    public override String HelpLinkFragment
    {
        get
        {
            return ("OrionPHResourceListVMs");
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AlternativeContent.Visible = false;

        if ((new VMHostDetailsDAL()).IsNodeEsxHost(this.NodeID))
        {
            var dat = (new ListOfVirtualMachinesDAL()).GetVirtualMachines(this.NodeID);
            repVms.DataSource = dat;
            repVms.DataBind();

            if (!this.Resource.IsInReport)
            {
                // standart behaviour: hide/show whole resource
                this.Visible = (dat.Rows.Count > 0);
            }
        }
        else // node type is incorrect
        {
            if (this.Resource.IsInReport)
            {
                this.Content.Visible = false;
                this.AlternativeContent.Visible = true;
            }
            else
            {
                this.Visible = false;
            }
        }
    }

    protected void repVms_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            var lit = e.Item.FindControl("litName");

            int nodeID = DBHelper.GetDbValueOrDefault<int>(row["NodeID"], -1);
            int virtualMachineID = DBHelper.GetDbValueOrDefault<int>(row["VirtualMachineID"], -1);
            int nodeStatusValue = DBHelper.GetDbValueOrDefault<int>(row["NodeStatus"], -1);
            string displayName = HttpUtility.HtmlEncode(DBHelper.GetDbValueOrDefault<string>(row["Name"], String.Empty));
            string ipAddress = DBHelper.GetDbValueOrDefault<string>(row["IPAddress"], displayName); // if no IP is found lets hope diplay name is host name
            int engineID = DBHelper.GetDbValueOrDefault<int>(row["HostEngineID"], -1);
            VirtualizationPlatform hostPlatform = (VirtualizationPlatform)DBHelper.GetDbValueOrDefault<int>(row["HostPlatform"], 0);

            int cpuShares = DBHelper.GetDbValueOrDefault<int>(row["CPUShares"], 0);
            Literal litCpuShares = e.Item.FindControl("litCpuShares") as Literal;
            litCpuShares.Text = cpuShares.ToString();
            string url = DBHelper.GetDbValueOrDefault(row["DetailsUrl"], string.Empty);

            if (!ManageNodeLink.IsUnmanagedNodeUrl(url))
            {
                Control c = e.Item.FindControl("nodeStatusImage");

                if (nodeStatusValue != -1)
                {
                    c.Controls.Add(new Image() { ImageUrl = String.Format("/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={0}&id={1}&size=small", nodeStatusValue, nodeID) });
                }
                else
                {
                    c.Controls.Add(new Literal() { Text = "&nbsp;" });
                }

                lit.Controls.Add(new Literal { Text = string.Format("<a href=\"{0}\" >{1}</a>", url, displayName) });
            }
            else if (Profile.AllowNodeManagement)
            {
                lit.Controls.Add(new Literal()
                {
                    Text = string.Format("<a href=\"javascript:void(0)\" onclick=\"{0}\">{1}</a>",
                                             ManageNodeLink.GetOnClickCode(engineID, ipAddress, SolarWinds.VIM.Web.Controls.AssetTree.AssetTreeNodeType.Guest, virtualMachineID, hostPlatform), 
                                             displayName)
                });
            }
            else
            {
                lit.Controls.Add(new Literal() { Text = displayName });
            }

            var state = new VirtualMachineState(DBHelper.GetDbValueOrDefault<string>(row["PowerState"], String.Empty), VirtualMachineStateType.Unknown);
            e.Item.FindControl("powerStateImageTd").Controls.Add(new VMState() { RenderedControlType = VMState.ControlType.Icon, State = state });
            e.Item.FindControl("powerStateTextTd").Controls.Add(new VMState() { RenderedControlType = VMState.ControlType.Text, State = state });
        }
    }

    protected string FormatIpAddress(string ipAddress)
    {
        if ( String.IsNullOrEmpty(ipAddress))
            return "&nbsp;";

        //We want to make breakable strings only for IPv6 addresses
        IPAddress parsedIp;
        if ( IPAddress.TryParse(ipAddress, out parsedIp))
        {
            if ( parsedIp.AddressFamily == AddressFamily.InterNetwork)
                return ipAddress;
        }

        return SolarWinds.Orion.Core.Web.FormatHelper.MakeBreakableString(ipAddress);
    }
}
