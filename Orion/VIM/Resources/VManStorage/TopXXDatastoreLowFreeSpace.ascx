﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXDatastoreLowFreeSpace.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_TopXXDatastoreLowFreeSpace" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_43%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_44%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_45%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_46%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <orion:EntityStatusIcon ID="EntityStatusIcon1" runat="server" Entity="<%#SolarWinds.VIM.Web.StatusIcons.VimIconProvider.DataStoreEntityName%>" Status='<%# Eval("ManagedStatus")%>' />
                        <a href="<%#GetViewLink(Eval("DataStoreID", "VMS:{0}"))%>"><%# HttpUtility.HtmlEncode(Eval("Name"))%></a>
                    </td> 
                    <td class="Property vimTopXXProperty"><%#FormatPercent(Eval("FreeSpacePercentage"))%></td>
                    <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("Capacity"))%></td>
                    <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("FreeSpace"))%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</vman:resourceWrapper>
