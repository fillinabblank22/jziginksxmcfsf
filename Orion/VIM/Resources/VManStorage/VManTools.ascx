﻿﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManTools.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_VManTools" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Models" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" Src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Reference Control="~/Orion/VIM/Controls/ManagementActionButton.ascx" %>

<orion:include id="Include1" runat="server" module="VIM" file="VIM.css" />

<vman:resourcewrapper runat="server" id="ResourceWrapper1">
    <Content>
        <div class="sw-vman-tools">            
            <div class="sw-vman-tools-perfstack">
                <a href="<%=GetPerfStackLink() %>" class="vim-perfstack-icon">
                    <%= Resources.VIMWebContent.EntityDetails_PerformanceAnalyzer_ButtonTitle %>
                </a>
            </div>
            <div class="sw-vman-tools-row">
                <asp:PlaceHolder ID="PowerActionPlaceHolder" runat="server" />
            </div>
            <div class="sw-vman-tools-row">
                 <asp:PlaceHolder ID="SnapshotActionsPlaceHolder" runat="server" />
            </div>
            <div class="sw-vman-tools-row">
                 <asp:PlaceHolder ID="ConfigurationActionPlaceHolder" runat="server" />
            </div>
            <div class="sw-vman-tools-row">
                <asp:PlaceHolder ID="MigrationPlaceHolder" runat="server" />
            </div>
        </div>
    </Content>
</vman:resourcewrapper>
