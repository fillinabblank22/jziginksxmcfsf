﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL.VMan.Storage;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_VManStorage_VManStorageSummary : VimBaseResource
{
    private Dictionary<StorageSummaryDAL.Properties, object> _summaryData;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private Dictionary<StorageSummaryDAL.Properties, object> SummaryData
    {
        get
        {
            return _summaryData ?? (_summaryData = new StorageSummaryDAL().GetSummaryData(DetectedViewType, NodeId, PlatformHelper.GetCollectionFromResourceProperties(Resource.Properties)));
        }
    }

    protected int DatastoreCount
    {
        get { return (int) SummaryData[StorageSummaryDAL.Properties.DatastoreCount]; }
    }

    protected int VirtualMachineCount
    {
        get { return (int) SummaryData[StorageSummaryDAL.Properties.VirtualMachineCount]; }
    }

    protected string DatastoreCapacity
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.DatastoreCapacity]); }
    }

    protected string DatastoreFreeSpace
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.DatastoreFreespace]); }
    }

    protected string DatastoreOversubscribed
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.DatastoreOversubscribed]); }
    }

    protected string DatastoreStorageFullPercentage
    {
        get { return FormatHelper.FormatPercent(SummaryData[StorageSummaryDAL.Properties.DatastoreStorageFullPercentage], 0); }
    }

    protected int VirtualMachinePoweredOnCount
    {
        get { return (int) SummaryData[StorageSummaryDAL.Properties.VirtualMachinePoweredOnCount]; }
    }

    protected string VirtualMachineAvgSize
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.VirtualMachineAvgSize]); }
    }

    protected string VirtualMachineAvgIops
    {
        get { return FormatHelper.FormatIOPS(SummaryData[StorageSummaryDAL.Properties.VirtualMachineAvgIops], 1); }
    }

    protected string VirtualMachineAvgLatency
    {
        get { return FormatHelper.FormatLatency(SummaryData[StorageSummaryDAL.Properties.VirtualMachineAvgLatency], 1); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_20; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualizationStorageSummary"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/VManStorageSummaryEditControl.ascx"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return Enumerable.Empty<Type>();
        }
    }

    protected int VSanDatastoresCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.VSanDatastoresCount]; }
    }

    protected string VSanDatastoresTotalSpace
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.VSanDatastoresTotalSpace]); }
    }

    protected string VSanDatastoresFreeSpace
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.VSanDatastoresFreeSpace]); }
    }

    protected string VSanDatastoresStorageFullPercentage
    {
        get { return FormatHelper.FormatPercent(SummaryData[StorageSummaryDAL.Properties.VSanDatastoresStorageFullPercentage], 0); }
    }

    protected string VSanDatastoresUsedByVms
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.VSanDatastoresUsedByVms]); }
    }

    protected string VSanDatastoresUsedBySystem
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.VSanDatastoresUsedBySystem]); }
    }

    protected int VSanHostsCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.VSanHostsCount]; }
    }

    protected int VSanHostsMaintenanceModeCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.VSanHostsMaintenanceModeCount]; }
    }

    protected int VSanHostsResyncComponentsCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.VSanHostsResyncComponentsCount]; }
    }

    protected string VSanHostsAverageLatency
    {
        get { return FormatHelper.FormatLatency(SummaryData[StorageSummaryDAL.Properties.VSanHostsAverageLatency], 1); }
    }

    protected string VSanHostsAverageIops
    {
        get { return FormatHelper.FormatIOPS(SummaryData[StorageSummaryDAL.Properties.VSanHostsAverageIops], 1); }
    }

    protected string VSanHostsAverageThroughput
    {
        get
        {
            var value = (double) SummaryData[StorageSummaryDAL.Properties.VSanHostsAverageThroughput];
            return FormatHelper.FormatThroughput(value / 1024, 1);
        }
    }

    protected int StorageContainersCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.StorageContainersCount]; }
    }
    protected string StorageContainersTotalSpace
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.StorageContainersCapacity]); }
    }

    protected string StorageContainersFreeSpace
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.StorageContainersFreeSpace]); }
    }

    protected string StorageContainersOversubscription
    {
        get { return FormatBytes(SummaryData[StorageSummaryDAL.Properties.StorageContainersOversubscription]); }
    }

    protected string StorageContainersStorageUsagePercentage
    {
        get { return FormatHelper.FormatPercent(SummaryData[StorageSummaryDAL.Properties.StorageContainersStorageUsagePercentage], 0); }
    }

    protected int AhvHostsCount
    {
        get { return (int) SummaryData[StorageSummaryDAL.Properties.AhvHostsCount]; }
    }

    protected int AhvHostsMaintenanceModeCount
    {
        get { return (int)SummaryData[StorageSummaryDAL.Properties.AhvHostsMaintenanceModeCount]; }
    }

    protected string AhvHostsAverageLatency
    {
        get { return FormatHelper.FormatLatency(SummaryData[StorageSummaryDAL.Properties.AhvHostsAverageLatency], 1); }
    }

    protected string AhvHostsAverageIops
    {
        get { return FormatHelper.FormatIOPS(SummaryData[StorageSummaryDAL.Properties.AhvHostsAverageIops], 1); }
    }

    protected string AhvHostsAverageThroughput
    {
        get
        {
            var value = (double) SummaryData[StorageSummaryDAL.Properties.AhvHostsAverageThroughput];
            return FormatHelper.FormatThroughput(value / 1024, 1);
        }
    }
}