﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PredictedVMSpaceDepletion.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_PredictedVMSpaceDepletion" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
    
<script type="text/javascript">
    $(function () {
        var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
    </script>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="resourceTable">
                    <HeaderTemplate>
                        <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                            <tr>
                                <td class="ReportHeader vimTopXXHeaderFirst vimReportHeader"><%#Resources.CoreWebContent.NetworkObjectType_Node%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_46%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_45%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_47%></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#EntityLinkHelper.GetVMEntityLink(Eval("DetailsUrl"), Eval("Name"),  Eval("Status"), Eval("PlatformID"), true, Container.DataItem)%>
                            </td> 
                            <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("FreeSpace"))%></td>
                            <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("Capacity"))%></td>
                            <td class="Property vimTopXXProperty"><%#FormatDate(Eval("CapacityDepletionDate"))%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel">
                    <ProgressTemplate>
                        <img alt="" src="/Orion/images/AJAX-Loader.gif" /></ProgressTemplate>
                </asp:UpdateProgress>
                <table width="100%" runat="server" id="gridFooter" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" class="TopologyFooter">
                            <div class="topologyDiv" style="white-space: nowrap; padding-right: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false"
                                    CommandArgument="Prev" ID="lbPrev" runat="server">
                                <img src="/Orion/images/TopologyItems/Arrows/arrow1Left.gif" />&nbsp;<%= Resources.CoreWebContent.WEBDATA_VB0_259 %>&nbsp;<%=PageSize %></asp:LinkButton>
                            </div>
                            <div class="topologyDiv" style="white-space: nowrap; padding-right: 5px; padding-left: 5px; height: 15px;">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false"
                                    CommandArgument="Next" ID="lbNext" runat="server">
                                <%= Resources.CoreWebContent.WEBDATA_VB0_260 %>&nbsp;<%=PageSize%>&nbsp;<img src="/Orion/images/TopologyItems/Arrows/arrow1Right.gif" /></asp:LinkButton>
                            </div>
                            <div style="height: 0px; line-height: 0px; width: 0px; clear: both;">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="hfPageIndex" />
                <asp:HiddenField runat="server" ID="hfShowAll" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbPrev" />
                <asp:AsyncPostBackTrigger ControlID="lbNext" />
            </Triggers>
        </asp:UpdatePanel>
    </Content>
</vman:resourceWrapper>
