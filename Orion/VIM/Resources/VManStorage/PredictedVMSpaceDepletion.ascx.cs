﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL.VMan.Storage;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManStorage_PredictedVMSpaceDepletion : VimBaseResource
{
    private PredictedVMSpaceDepletionDAL _depletionDAL;

    private PredictedVMSpaceDepletionDAL DatastoreDepletionDal
    {
        get { return _depletionDAL ?? (_depletionDAL = new PredictedVMSpaceDepletionDAL()); }
    }

    public int TopXX
    {
        get;
        set;
    }

    public int PageSize
    {
        get;
        set;
    }

    public int PageNumber
    {
        get
        {
            int pageIntex = 0;
            int.TryParse(hfPageIndex.Value, out pageIntex);
            return pageIntex;
        }
        set
        {
            hfPageIndex.Value = value.ToString();
        }
    }

    public int ItemsCount
    {
        get;
        set;
    }

    public override void DataBind()
    {
        if (!Wrapper.CanShowContent)   // if we can't show content bail
            return;

        var table = DatastoreDepletionDal.GetPageDataTable(DetectedViewType, PageNumber + 1, PageSize, NodeId);

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    public override string FormatDate(object value)
    {
        try
        {
            if (value is DateTime)
            {
                var dateTime = (DateTime)value;
                if (dateTime.Date == SqlDateTime.MinValue.Value.Date)
                {
                    return Resources.VIMWebContent.VIMWEBCODE_JD0_2;
                }
                if (dateTime.Date == SqlDateTime.MaxValue.Value.Date)
                {
                    return Resources.VIMWebContent.VIMWEBCODE_JD0_3;
                }
            }
            return base.FormatDate(value);
        }
        catch
        {
            return String.Empty;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!Wrapper.CanShowContent)    // if we can't show content bail
            return;

        HideWhenEntityNotPolled(nameof(VirtualDisk));

        var topXX = Resource.Properties["MaxRecords"];
        TopXX = (!string.IsNullOrEmpty(topXX)) ? int.Parse(topXX) : 10;

        PageSize = TopXX;

        SetItemsCount();

        gridFooter.Visible = ItemsCount > PageSize;

        DataBind();
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_21; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHPredictedVMDiskDepletion"; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    private void SetItemsCount()
    {
        ItemsCount = DatastoreDepletionDal.GetCount(DetectedViewType, NodeId);
    }

    protected void Grid_IndexChanging(object sender, CommandEventArgs e)
    {
        switch (e.CommandArgument.ToString().ToLowerInvariant())
        {
            case "next":
                SetItemsCount();
                if (PageNumber < (double)ItemsCount / PageSize - 1)
                    PageNumber += 1;
                break;
            case "prev":
                SetItemsCount();
                if (PageNumber > 0)
                    PageNumber -= 1;
                break;
        }

        DataBind();
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }
}