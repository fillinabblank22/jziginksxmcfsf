﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManStorageSummary.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_VManStorageSummary" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<orion:Include runat="server" Module="VIM" File="VIM.css"/>

<vman:resourceWrapper runat="server" ID="ResourceWrapper1">
    <Content>
        <table class="sw-vman-summary-table" id="resourceTable">
            <tr>
                <td colspan="2" class="sw-vman-summary-rightborder">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/datastore_icon40x40.png" alt="<%=Resources.VIMWebContent.VIMWEBDATA_LC0_21%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=DatastoreCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_21%></td></tr>
                    </table>
                </td>
                <td colspan="2">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/virtual-machine_icon40x40.png" alt="<%=Resources.VIMWebContent.VIMWEBDATA_LC0_22%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=VirtualMachineCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_22%></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_23%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=DatastoreCapacity%></td>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_27%></td>
                <td class="sw-vman-summary-value"><%=VirtualMachinePoweredOnCount%></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_24%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=DatastoreFreeSpace%></td>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_28%></td>
                <td class="sw-vman-summary-value"><%=VirtualMachineAvgSize%></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_25%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=DatastoreOversubscribed%></td>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_29%></td>
                <td class="sw-vman-summary-value"><%=VirtualMachineAvgIops%></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_26%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=DatastoreStorageFullPercentage%></td>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_30%></td>
                <td class="sw-vman-summary-value"><%=VirtualMachineAvgLatency%></td>
            </tr>
            <% if (VSanDatastoresCount > 0) { %>
            <tr>
                <td colspan="2" class="sw-vman-summary-rightborder">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/vsan-datastores_icon40x40.png" alt="<%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanDatastores%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=VSanDatastoresCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanDatastores%></td></tr>
                    </table>
                </td>
                <td colspan="2">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/vsan-host_icon40x40.png" alt="<%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHosts%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=VSanHostsCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHosts%></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanTotalSpace %></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%= VSanDatastoresTotalSpace %></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHostsMaintenanceMode %></td>
                <td class="sw-vman-summary-value"><%= VSanHostsMaintenanceModeCount %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanFreeSpace %></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%= VSanDatastoresFreeSpace %></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHostsResyncComponents %></td>
                <td class="sw-vman-summary-value"><%= VSanHostsResyncComponentsCount %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanStorageFull %></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%= VSanDatastoresStorageFullPercentage %></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHostsAverageLatency %></td>
                <td class="sw-vman-summary-value"><%= VSanHostsAverageLatency %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanUsedByVms %></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%= VSanDatastoresUsedByVms %></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHostsAverageIops %></td>
                <td class="sw-vman-summary-value"><%= VSanHostsAverageIops %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanUsedBySystem %></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%= VSanDatastoresUsedBySystem %></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_vSanHostsAverageThroughput %></td>
                <td class="sw-vman-summary-value"><%= VSanHostsAverageThroughput %></td>
            </tr>
            <% } %>
            <% if (StorageContainersCount > 0) { %>
            <tr>
                <td colspan="2" class="sw-vman-summary-rightborder">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/nutanix-datastores_icon40x40.png" alt="<%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_StorageContainers%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=StorageContainersCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_StorageContainers%></td></tr>
                    </table>
                </td>
                <td colspan="2" class="sw-vman-summary-rightborder">
                    <table>
                        <tr>
                            <td rowspan="2"><img src="/Orion/VIM/images/ahv-host_icon40x40.png" alt="<%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHosts%>" /></td>
                            <td class="sw-vman-summary-headervalue"><%=AhvHostsCount%></td>
                        </tr>
                        <tr><td class="sw-vman-summary-headertitle"><%=Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHosts%></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_23%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=StorageContainersTotalSpace%></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHostsMaintenanceMode %></td>
                <td class="sw-vman-summary-value"><%= AhvHostsMaintenanceModeCount %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_24%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=StorageContainersFreeSpace%></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHostsAverageLatency %></td>
                <td class="sw-vman-summary-value"><%= AhvHostsAverageLatency %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_25%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=StorageContainersOversubscription%></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHostsAverageIops %></td>
                <td class="sw-vman-summary-value"><%= AhvHostsAverageIops %></td>
            </tr>
            <tr>
                <td class="sw-vman-summary-name"><%=Resources.VIMWebContent.VIMWEBDATA_LC0_26%></td>
                <td class="sw-vman-summary-value sw-vman-summary-rightborder"><%=StorageContainersStorageUsagePercentage%></td>
                <td class="sw-vman-summary-name"><%= Resources.VIMWebContent.VirtualizationStorageSummaryResource_AhvHostsAverageThroughput %></td>
                <td class="sw-vman-summary-value"><%= AhvHostsAverageThroughput %></td>
            </tr>
            <% } %>
        </table>
    </Content>
</vman:resourceWrapper>