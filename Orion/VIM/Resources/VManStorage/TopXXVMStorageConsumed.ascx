﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMStorageConsumed.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_TopXXVMStorageConsumed" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" TagName="EntityStatusIcon" Src="~/Orion/Controls/EntityStatusIcon.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst vimReportHeader"><%#Resources.CoreWebContent.NetworkObjectType_Node%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_48%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_49%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_43%></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#EntityLinkHelper.GetVMEntityLink(Eval("DetailsUrl"), Eval("Name"), Eval("Status"), Eval("PlatformID"), true, Container.DataItem)%></td> 
                    <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("TotalStorageSizeUsed"))%></td>
                    <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("SnapshotStorageSize"))%></td>
                    <td class="Property vimTopXXProperty"><%#EntityLinkHelper.GetDatastoreEntityLink(Eval("DataStoreDetailsUrl"), Eval("DataStoreName"), Eval("DataStoreStatus"), true) %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </Content>
</vman:resourceWrapper>
