﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Web.VManResources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
public partial class Orion_VIM_Resources_VManStorage_VManStorageChart : VManBaseStorageChart, IResourceIsInternal
{
    protected void Page_Init(object sender, EventArgs e)
    {
        HandleInit(WrapperContents);
        HideByPlatform();
    }

    protected override bool AllowCustomization
    {
        get { return Profile.AllowCustomize; }
    }

    public bool IsInternal { get { return true; } }
}