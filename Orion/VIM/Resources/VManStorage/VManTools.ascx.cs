﻿using System;
using ASP;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Common.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManStorage_VManTools : VimBaseResource
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        OrionInclude.CoreFile("/styles/NodeMNG.css");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed() || DetectedViewType != ViewType.Guest)
        {
            Visible = false;
            return;
        }

        Visible = true;
        InitializeVmActions(NodeId);
        DataBind();
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LV0_1; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVirtualizationManagerTools"; }
    }

    protected string GetPerfStackLink()
    {
        var vmDal = new VirtualMachineDAL();
        return PerfStackLinkBuilder.GetVirtualMachineLink(NodeId, vmDal.GetPlatformByVirtualMachineID(NodeId));
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    public bool isPolledByVman { get; private set; }

    private void InitializeVmActions(int vmId)
    {
        Page.LoadControl("~/Orion/VIM/Controls/ManagementActionButton.ascx");
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.PowerOn, vmId));
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.Resume, vmId));
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.PowerOff, vmId));
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.Suspend, vmId));
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.Pause, vmId));
        PowerActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.Reboot, vmId));

        SnapshotActionsPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.TakeSnapshot, vmId));
        SnapshotActionsPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.DeleteSnapshots, vmId));

        ConfigurationActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.ChangeSettings, vmId));
        ConfigurationActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.DeleteVM, vmId));
        ConfigurationActionPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.UnregisterVM, vmId));

        MigrationPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.PerformMigration, vmId));
        MigrationPlaceHolder.Controls.Add(new ManagementActionButton(ManagementEnums.ActionName.PerformRelocation, vmId));
    }
}