﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.AjaxTree;
using SolarWinds.VIM.Web.Controls.RelatedNodesTree;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManStorage_RelatedNodes : VimBaseResource
{
    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_32; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHRelatedNodes"; }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditControls/RelatedNodesTreeEdit.ascx"; }
    }

    public override IEnumerable<Type> SupportedInterfaces
    {
        get
        {
            return new[]
            {
                typeof (INodeProvider), typeof (IVimClusterProvider), typeof (IVManDataStoreProvider), 
                typeof (IVimHostProvider),typeof (IVimVirtualMachineProvider)
            };
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected override void OnPreRender(EventArgs e)
    {
        var manager = ScriptManager.GetCurrent(this.Page);

        if (manager != null)
        {
            manager.Scripts.Add(new ScriptReference(PathResolver.GetVirtualPath("VIM", "TreeControl.js")));
        }

        base.OnPreRender(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool shouldShow = CreateTree(relatedNodesTree);

        if (!shouldShow)
        {
            Visible = false;
        }
    }

    private bool CreateTree(SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl.TreeControl tree)
    {
        object entityId;

        switch (DetectedViewType)
        {
            case ViewType.Cluster:
            case ViewType.Host:
            case ViewType.Guest:
                entityId = NodeId;
                break;
            case ViewType.VManDatastore:
                var datacenterProvider = GetInterfaceInstance<IVManDataStoreProvider>();
                entityId = datacenterProvider.DataStore.Id;
                break;
            default:
                return false;
        }

        var dataProviderIdentifier = String.Format("VIM_RELATED_NODES_TREE_DATA"); // data provider identifier ! resource id is used to uniquely identify provider in application scope
        var expandedStateProviderIdentifier = String.Format("VIM_RELATED_NODES_TREE_STATE-{0}-{1}-{2}",
            this.Resource.ID,
            this.Resource.View.ViewID,
            Request.QueryString["NetObject"] ?? String.Empty
            );

        Session[String.Format("{0}-remember", expandedStateProviderIdentifier)] = GetBooleanResourceProperty("RememberExpandedState");

        if (!AppScopeTreeDataManager.ContainsProvider(dataProviderIdentifier))
        {
            AppScopeTreeDataManager.RegisterProvider(dataProviderIdentifier, new RelatedNodesTreeNodeProvider());
        }
        if (!SessionScopeTreeExpandedStateManager.ContainsProvider(expandedStateProviderIdentifier))
        {
            SessionScopeTreeExpandedStateManager.RegisterProvider(expandedStateProviderIdentifier);
        }

        tree.LoadTree(new RelatedNodesTreeService(), GetPrimaryNode(dataProviderIdentifier, expandedStateProviderIdentifier, entityId));

        return true;
    }

    private object[] GetPrimaryNode(string dataProviderIdentifier, string expandedStateProviderIdentifier, object entityId)
    {
        var definition = new List<object>()
            {
                dataProviderIdentifier,
                expandedStateProviderIdentifier,
                0, // tree level
                (int) RelatedNodesTreeNodeType.Root,
                entityId,
                "root", // path
                String.Empty, // caption
                Resource.ID,
                (int) VirtualizationPlatform.Unknown,
                (int) DetectedViewType
            };

        return definition.ToArray();
    }

    private bool GetBooleanResourceProperty(string key)
    {
        return (this.Resource.Properties[key] ?? "true").ToLowerInvariant() == "true";
    }
}