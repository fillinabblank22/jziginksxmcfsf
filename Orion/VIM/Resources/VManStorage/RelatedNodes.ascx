﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelatedNodes.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_RelatedNodes" %>
<%@ Register Src="~/Orion/VIM/Controls/ManageNodeDialog.ascx" TagPrefix="vim" TagName="ManageNodeDialog" %>
<%@ Register TagPrefix="AjaxTree" Assembly="SolarWinds.VIM.Web" Namespace="SolarWinds.VIM.Web.AjaxTree.AjaxTreeControl" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>

<script type="text/javascript">
    $(function () {
        var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
    </script>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Always">
            <ContentTemplate>
                <vim:ManageNodeDialog ID="manageNodeDialog1" runat="server" ></vim:ManageNodeDialog>
                <AjaxTree:TreeControl ID="relatedNodesTree" runat="server">
                    <WebService Path="/Orion/VIM/Services/RelatedNodesTreeService.asmx" />
                </AjaxTree:TreeControl>
            </ContentTemplate>
        </asp:UpdatePanel>
    </Content>
</vman:resourceWrapper>
