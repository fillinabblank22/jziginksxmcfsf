﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.Common.Enums;
using SolarWinds.VIM.Web.DAL.VMan.Storage;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManStorage_TopXXVMStorageConsumed : VimTopXXResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Wrapper.CanShowContent)   // if we can't show content bail
            return;

        if (DetectedViewType == ViewType.Cluster)
        {
            HideWhenEntityPropertyNotPolled(nameof(VirtualMachine), nameof(VirtualMachine.TotalStorageSizeUsed));
        }

        DataTable table;

        try
        {
            var dal = new TopXXVMStorageConsumedDAL();
            table = dal.GetTopXXVMStorageConsumed(MaxRecords, DetectedViewType, NodeId);
        }
        catch (SwisQueryException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LC0_19; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHTopXXVMStoreageConsumed"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return Enumerable.Empty<Type>(); }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}