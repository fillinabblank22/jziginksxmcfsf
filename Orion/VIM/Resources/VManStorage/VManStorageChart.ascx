﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VManStorageChart.ascx.cs" Inherits="Orion_VIM_Resources_VManStorage_VManStorageChart" %>
<orion:Include runat="server" Module="VIM" File="VIM.css"/>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:PlaceHolder runat="server" ID="WrapperContents"></asp:PlaceHolder>
    </Content>
</vman:resourceWrapper>