﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Web.DAL.VMan.Sprawl;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManSprawl_TopXXVMCpuOverallocated : VimTopXXResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Wrapper.CanShowContent)
            return;
        
        DataTable table;

        try
        {
            var dal = new TopXXVMCpuAllocatedDAL();
            table = dal.GetTopXXVMCpuOverallocated(MaxRecords);
        }
        catch (SwisQueryException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }
                
        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);    

        this.resourceTable.ItemCreated += new RepeaterItemEventHandler(resourceTable_ItemCreated);    
    }

    private void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {        
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    if (e.Item.DataItem is DataRowView)
                    {
                        var row = (e.Item.DataItem as DataRowView).Row;
                        
                        if (row != null)
                        {
                            var recommendTipLabel = e.Item.FindControl("recommendTipLabel") as Label;
                            var cpuCount = DBHelper.GetDbValueOrDefault(row["ProcessorCount"], 0);
                            var cpuPeakUtilization = DBHelper.GetDbValueOrDefault(row["CpuLoadSumpeak"], 0.0f);

                            if (cpuCount > 0)
                            {
                                recommendTipLabel.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_LV0_15, cpuCount,
                                                                   SprawlRecommendationsHelper.GetRecommendedReducedCpuCount(cpuCount, cpuPeakUtilization));
                            }
                        }
                    }
                }
                break;
        }
    }

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LV0_2; }
    }    

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHTopXXVMOverAllocatedCPU"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}