﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMsIdle.ascx.cs" Inherits="Orion_VIM_Resources_VManSprawl_VMsIdle" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="vman" TagName="ManagementActionButton" src="../../Controls/ManagementActionButton.ascx" %>

<script type="text/javascript">
    $(function () {
        var refresh = function () { __doPostBack('<%= updatePanel.ClientID %>', ''); };
        SW.Core.View.AddOnRefresh(refresh, '<%= updatePanel.ClientID %>');
    });
    </script>

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Repeater runat="server" ID="resourceTable">
                    <HeaderTemplate>
                        <table border="0" cellpadding="2" cellspacing="0" width="100%" class="sw-vman-wmsprawl-table NeedsZebraStripes">
                            <tr>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_18%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_19%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_20%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_DF0_21%></td>
                                <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_JD0_17%></td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="Property sw-vim-vman-resource-row"><asp:Literal runat="server" ID="litNode" /></td>
                            <td class="Property sw-vim-vman-resource-row"><asp:Literal runat="server" ID="litAvgCPU" /></td>
                            <td class="Property sw-vim-vman-resource-row"><asp:Literal runat="server" ID="litAvgIOPS" /></td>
                            <td class="Property sw-vim-vman-resource-row"><asp:Literal runat="server" ID="litAvgNet" /></td>
                            <td class="Property sw-vim-vman-resource-row"><vman:ManagementActionButton runat="server" ID="btnPowerOff" /></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel">
                    <ProgressTemplate>
                        <img alt="" src="/Orion/images/AJAX-Loader.gif" /></ProgressTemplate>
                </asp:UpdateProgress>
                <table width="100%" runat="server" id="gridFooter" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3" class="sw-vman-wmsprawl-footer">
                            <div class="sw-vman-wmsprawl-footer-div">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false" CommandArgument="First" ID="lbFirst" runat="server">
                                    <asp:Image runat="server" ID="imgFirst" ImageUrl="/Orion/VIM/images/Arrows/page-first.gif" />
                                </asp:LinkButton>
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false" CommandArgument="Prev" ID="lbPrev" runat="server">
                                    <asp:Image runat="server" ID="imgPrev" ImageUrl="/Orion/VIM/images/Arrows/page-prev.gif" />
                                </asp:LinkButton>
                            </div>
                            <div class="sw-vman-wmsprawl-footer-div">
                                <%#Resources.VIMWebContent.VIMWEBDATA_DF0_22%> <asp:TextBox runat="server" ID="txtPage" Width="30" AutoPostBack="true" ValidationGroup="Page" /> <%#Resources.VIMWebContent.VIMWEBDATA_DF0_23%> <asp:Literal runat="server" ID="litPageCount" />
                            </div>
                            <div class="sw-vman-wmsprawl-footer-div">
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false" CommandArgument="Next" ID="lbNext" runat="server">
                                    <asp:Image runat="server" ID="imgNext" ImageUrl="/Orion/VIM/images/Arrows/page-next.gif" alt="" />
                                </asp:LinkButton>
                                <asp:LinkButton CommandName="Page" OnCommand="Grid_IndexChanging" CausesValidation="false" CommandArgument="Last" ID="lbLast" runat="server">
                                    <asp:Image runat="server" ID="imgLast" ImageUrl="/Orion/VIM/images/Arrows/page-last.gif" />
                                </asp:LinkButton>
                            </div>
                            <div class="sw-vman-wmsprawl-footer-div" style="padding-left: 10px;">
                                <%#Resources.VIMWebContent.VIMWEBDATA_DF0_24%> <asp:TextBox runat="server" ID="txtPageSize" Width="30" AutoPostBack="true" />
                            </div>
                            <div style="height: 0px; line-height: 0px; width: 0px; clear: both;">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="hfPageIndex" />
                <asp:HiddenField runat="server" ID="hfShowAll" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbPrev" />
                <asp:AsyncPostBackTrigger ControlID="lbNext" />
                <asp:AsyncPostBackTrigger ControlID="lbFirst" />
                <asp:AsyncPostBackTrigger ControlID="lbLast" />
                <asp:AsyncPostBackTrigger ControlID="txtPage" />
                <asp:AsyncPostBackTrigger ControlID="txtPageSize" />
            </Triggers>
        </asp:UpdatePanel>
    </Content>
</vman:resourceWrapper>
