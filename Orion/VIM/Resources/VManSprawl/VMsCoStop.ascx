﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMsCoStop.ascx.cs" Inherits="Orion_VIM_Resources_VManSprawl_VMsCoStop" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="vim" TagName="ActionStatusBar" Src="~/Orion/VIM/Controls/ManagementActionsStatusBar.ascx" %>

<orion:Include runat="server" Module="VIM" File="ChangeResourceSettingsDialog.js" />
<orion:Include runat="server" Module="VIM" File="Controls/ManagementActionDialog.js" />
<orion:Include runat="server" Module="Orion" File="NodeMNG.css" />


<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <vim:ActionStatusBar runat="server" />     
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable" />

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,    
                        allowSort: true,   
                        columnSettings: {
                            "Node": {
                                header: '<%= Resources.CoreWebContent.NetworkObjectType_Node %>',               
                                formatter: function (value, row, cellInfo){                              
                                    return String.format('<img src="{0}" />&nbsp;<a href="/Orion/View.aspx?NetObject=VVM:{1}">{2}</a>', row[5], row[4], value);
                                },
                                isHtml: true
                            },
                            "CpuLoad": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_IY0_05 %>',               
                                formatter: function (value, row, cellInfo){                                    
                                    return String.format('<table><tr><td style="border: none !important; width: 30%">'+ value + ' %' + '</td><td style="border: none !important"><div class="BarBackground" style="height:10px;width:100px;"><div class="GreenBar" style="background-color:White;font-size:1px;width:{0}px;">&nbsp;</div></div>', row[1]) + '</td></tr></table>';                                    
                                },
                                isHtml: true
                            },
                            "ProcessorCount": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_IY0_06 %>',               
                                formatter: function (value, row, cellInfo) {
                                    return value;
                                },
                                isHtml: true
                            },
                            "CpuCostop": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_IY0_07 %>',               
                                formatter: function (value, row, cellInfo){
                                    return value + ' %';
                                },
                                isHtml: true
                            },
                            "Action": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_JD0_17 %>',  
                                formatter: function (value, row, cellInfo){                                   
                                    if ('<%= VimProfile.AllowResourcesSettingsManagement %>' == 'True') {
                                        return String.format('<a href="javascript:void(0);" onclick="SW.VIM.ChangeSettingsDialog(\'<%=ManagementEnums.ActionName.ChangeSettings%>\', \'<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_50%>\',\'<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_49%>\',{1}, \'{2}\', 2,\'{3}\',\'{4}\')">' +
                                            '<img src="/Orion/VIM/images/ManagementVM/Right-size_icons16x16.png">&nbsp;<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_49%></a>',
                                            value, row[4], row[7], row[8] != 'poweredOff', row[4]);
                                    }
                                    return '';
                                },
                                isHtml: true                                
                            }
                        }
                    });
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });    
        </script>
    </Content>
</vman:resourceWrapper>


