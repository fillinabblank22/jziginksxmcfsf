﻿using System;
using System.Data;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.DAL.VMan.Sprawl;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManSprawl_TopXXVMSnapshotDiskUsage : VimTopXXResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Wrapper.CanShowContent)
            return;

        DataTable table;

        try
        {
            var dal = new TopXXVMSnapshotDiskUsageDAL();
            table = dal.GetTopXXVMBySnapshotDiskUsage(MaxRecords);
        }
        catch (SwisQueryException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }      

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LV0_6; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHTopXXVMSnapshotDiskUsage"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}