﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXVMSnapshotDiskUsage.ascx.cs" Inherits="Orion_VIM_Resources_VManSprawl_TopXXVMSnapshotDiskUsage" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>

<%@ Register TagPrefix="vman" TagName="VManResourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="vman" TagName="ManagementActionButton" src="../../Controls/ManagementActionButton.ascx" %>

<vman:VManResourceWrapper ID="Wrapper" runat="server">
    <Content>       
        <asp:Panel ID="SQLErrorPanel" runat="server" Visible="false">
            <table cellpadding="10px">
                <tr>
                    <td style="font-weight: bold; font-size: small; color: Red">
                        <%= Resources.VIMWebContent.VIMWEBDATA_VB0_75%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <asp:Repeater runat="server" ID="resourceTable">
            <HeaderTemplate>
                <table class="NeedsZebraStripes" border="0" cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td class="ReportHeader vimTopXXHeaderFirst vimReportHeader"><%#Resources.CoreWebContent.NetworkObjectType_Node%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_49%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LV0_24%></td>
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_LC0_43%></td>   
                        <td class="ReportHeader vimReportHeader"><%#Resources.VIMWebContent.VIMWEBDATA_JD0_17%></td>                     
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#EntityLinkHelper.GetVMEntityLink(Eval("DetailsUrl"), Eval("Name"), 
                        Eval("Status"), Eval("PlatformID"), true, Container.DataItem)%></td>
                    <td class="Property vimTopXXProperty"><%#FormatBytes(Eval("SnapshotStorageSize"))%></td>                    
                    <td class="Property"><%#EntityLinkHelper.GetClusterEntityLink(Eval("ClusterDetailsUrl"), Eval("ClusterName"), 
                        Eval("ClusterStatus"), Eval("PlatformID"), true)%></td>
                    <td class="Property vimTopXXProperty"><%#EntityLinkHelper.GetDatastoreEntityLink(Eval("DataStoreDetailsUrl"), 
                        Eval("DataStoreName"), Eval("DataStoreStatus"), true) %></td>   
                    <td class="Property vimTopXXProperty">                        
                        <vman:ManagementActionButton runat="server" EncapsulateBySpan="false" VmId='<%# Eval("VirtualMachineID") %>' Action=<%#ManagementEnums.ActionName.DeleteSnapshots%> />    
                    </td>    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>        
    </Content>
</vman:VManResourceWrapper>

