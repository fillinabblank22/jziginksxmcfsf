﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Enums;


[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManSprawl_VMsCoStop : BaseResourceControl
{
    public ThresholdType type { get { return ThresholdType.CPULoad; } }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBDATA_IY0_02; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionVMPHVMCostopLastWeek";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to displaydbm_InterfaceForecast_DeleteOrphanss
        CustomTable.SWQL = SWQL;
    }

    public string SWQL
    {
        get
        {
            const string swql = @"
              SELECT 
                vm.Name AS [Node], 
                ROUND(vm.CpuLoad, 2) AS [CpuLoad], 
                vm.ProcessorCount AS [ProcessorCount], 
                ROUND(vm.CpuCostop, 2) AS [CpuCostop], 
                vm.VirtualMachineID AS [_VirtualMachineID],
                '/Orion/StatusIcon.ashx?entity=Orion.VIM.VirtualMachines&size=Small&id=' + TOSTRING(vm.VirtualMachineID) + '&status=' + TOSTRING(vm.Status) + '&hint=' + TOSTRING(vm.PlatformID) AS [_Node_Icon],
                vm.Name AS [Action],
                vm.Platform.Name AS [_PlatformName],
                vm.PowerState AS [_PowerState]
              FROM Orion.VIM.VirtualMachines vm
              WHERE vm.CpuCostop >= 3 AND vm.ProcessorCount > 1
              ORDER BY vm.CpuCostop DESC";

            return swql;
        }
    }
}