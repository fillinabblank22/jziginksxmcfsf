<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrphanedVMDKs.ascx.cs" Inherits="Orion_VIM_Resources_Virtualization_OrphanedVMDKs" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="vman" TagName="resourceWrapper" src="../../Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="vman" TagName="ManagementActionsStatusBar" src="../../Controls/ManagementActionsStatusBar.ascx" %>

<orion:Include runat="server" Module="VIM" File="Controls/ManagementActionDialog.js" />
<orion:Include runat="server" Module="VIM" File="Resources/Formatters.js" />
<orion:Include runat="server" Module="Orion" File="NodeMNG.css" />

<vman:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <vman:ManagementActionsStatusBar runat="server" />
        <div class="sw-suggestion sw-suggestion-warn" runat="server" style="margin-bottom: 5px;">
            <span class="sw-suggestion-icon" style="top: 0;bottom: 0;margin: auto;position: absolute;left: 5px;"></span>
            <span style="height: 100%;vertical-align: sub;"><%=VIMWebContent.VIMWEBDATA_JS0_1%></span>
        </div>
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        onLoad: function () {
                            <% if(!ShowDeleteButton){ %>
                            $('#Grid-<%= CustomTable.UniqueClientID %> tr td:last-child').hide();
                            <% } %>
                        },
                        columnSettings: {
                            "FileName": {
                                header: '<%= VIMWebContent.VIMWEBDATA_JD0_18 %>',
                                formatter: function(value) {
                                    //display only file name withouth the path
                                    return '<span title="' + value + '">' + value.replace(/^.*[\\\/]/, '') + '</span>';
                                },
                                isHtml: true,
                                allowSort: false
                            },
                            "Label": {
                                header: '<%= VIMWebContent.VIMWEBDATA_JD0_19 %>'
                            },
                            "DatastoreName": {
                                header: '<%= VIMWebContent.VIMWEBDATA_JD0_20 %>'
                            },
                            "Size": {
                                header: '<%= VIMWebContent.VIMWEBDATA_JD0_21 %>',
                                formatter: SW.VIM.Formatters.bytesFormatter
                            },
                            "Action": {
                                header: '<%= Resources.VIMWebContent.VIMWEBDATA_JD0_17 %>',  
                                formatter: function (value, row, cellInfo){
                                    <% if(ShowDeleteButton){ %>
                                    return String.format('<a href="javascript:void(0);" onclick="SW.VIM.ManagementActionDialog(\'<%=ManagementEnums.ActionName.DeleteDatastoreFile%>\', \'<%=Resources.VIMWebContent.VIMWEBDATA_JS0_2%>\',\'<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_61%>\',{1},\'\', 2, \'false\', [\'{2}\',{3},\'{4}\',{5}], \'\')"><img src="/Orion/VIM/images/ManagementVM/delete_VMDK_icon16x16.png">&nbsp;<%=Resources.VIMWebContent.VIMWEBDATA_ZS0_61%></a>', row[0].replace(/^.*[\\\/]/, ''), value, row[0], row[5], row[6], row[7], "");
                                    <% } else { %>
                                    return "";
                                    <% } %>
                                },
                                isHtml: true
                            }
                        }
                    });
 
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
        </script>
    </Content>
</vman:resourceWrapper>

