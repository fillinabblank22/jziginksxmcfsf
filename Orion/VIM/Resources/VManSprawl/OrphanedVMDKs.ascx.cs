﻿using System;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_Virtualization_OrphanedVMDKs : VimBaseResource
{
    protected override string DefaultTitle
    {
        get
        {
            return Resources.VIMWebContent.VIMWEBDATA_JD0_24;
        }
    }

    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        CustomTable.SWQL = SWQL;
        CustomTable.OrderBy = OrderBy;
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public bool ShowDeleteButton
    {
        get { return VimProfile.AllowDeleteVMAction; }
    }

    public string SWQL
    {
        get
        {
            //return only discs with size grater then 10 MB
            return String.Format(@"SELECT DISTINCT
                        vd.Name AS FileName, 
                        d.Name AS DatastoreName,
                        d.DetailsUrl AS [_LinkFor_DatastoreName],
                        vd.Size,
                        d.DataStoreID as Action,
                        vd.DiskFileID as [_DiskFileID],
                        d.Name as [_DataStoreName],
                        Min(d.Hosts.HostID) as [_DataStoreHostID],
                        '/Orion/StatusIcon.ashx?entity=Orion.VIM.Datastores&size=Small&status=' + TOSTRING(Status) AS [_IconFor_DatastoreName]
                    FROM Orion.VIM.DiskFiles vd
                    LEFT JOIN Orion.Vim.Datastores d ON vd.DatastoreID = d.DatastoreId
                    WHERE 
                        (vd.Orphaned IS NULL OR vd.Orphaned <> 0)
                        AND vd.VirtualDiskID IS NULL 
                        AND vd.Type='DiskFile' 
                        AND vd.Size > {0} 
                        AND d.Name <> ''
                    GROUP BY FileName, d.Name, d.DetailsUrl, vd.Size, d.DataStoreID, _DiskFileID, Status
                    ORDER BY vd.Size DESC, vd.Name ASC", GetMinimalSize());
        }
    }

    private long GetMinimalSize()
    {
        Int64 size;
        if (!Int64.TryParse(Resource.Properties["MinDiskSize"], out size))
        {
            Resource.Properties["MinDiskSize"] = "10";
            size = 10;
        }
        return size*1024*1024;
    }

    public string OrderBy
    {
        get
        {
            return "vd.Size DESC, vd.Name ASC";
        }
    }

    public override string EditControlLocation
    {
        get { return "/Orion/VIM/Controls/EditResourceControls/SprawlOrphanedVMDKsEditControl.ascx"; }
    }

    public override string HelpLinkFragment
    {
        get 
        {
            return "OrionVMPHOrphanedVMDKs";
        }
    }
}
