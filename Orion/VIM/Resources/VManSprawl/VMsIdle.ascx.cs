﻿using System;
using System.Web.UI.WebControls;
using ASP;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.DAL.VMan.Sprawl;
using SolarWinds.VIM.Web.Helpers;
using SolarWinds.VIM.Common.Helpers;
using FormatHelper = SolarWinds.VIM.Web.Helpers.FormatHelper;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManSprawl_VMsIdle : VimSprawlPagedResourceControl
{

    private VMsIdleDAL _vmsIdleDAL;

    private VMsIdleDAL DAL
    {
        get { return _vmsIdleDAL ?? (_vmsIdleDAL = new VMsIdleDAL()); }
    }


    override protected System.Web.UI.WebControls.HiddenField PageIndexField
    {
        get
        {
            return this.hfPageIndex;
        }
    }

    protected override System.Web.UI.UpdatePanel MainUpdatePanel
    {
        get
        {
            return this.updatePanel;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.resourceTable.ItemCreated += new RepeaterItemEventHandler(resourceTable_ItemCreated);
        this.txtPageSize.TextChanged += new EventHandler(txtPageSize_TextChanged);
        this.txtPage.TextChanged += new EventHandler(txtPage_TextChanged);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (IsPostBack)
        {
            if (Wrapper.CanShowContent)
            {
                bindDataToSource();
            }
        }

        if (!IsPostBack || IsTimedBackgroundRefresh())
        {
            if (Wrapper.CanShowContent)
            {
                setItemsCount();

                setPageSize();
                gridFooter.Visible = ItemsCount > PageSize;

                setTotalPages();
                setPageNumber();
                setNavigationArrows();
                bindDataToSource();
            }
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_DF0_7; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHVMIdleLastWeek"; }
    }

    private void setItemsCount()
    {
        ItemsCount = DAL.GetCount();
    }

    private void setTotalPages()
    {
        litPageCount.Text = TotalPages.ToString();
    }

    private void setPageSize()
    {
        var pageSizeResourceProperty = Resource.Properties["MaxRecords"];
        if (!IsPostBack)
        {
            PageSize = (!string.IsNullOrEmpty(pageSizeResourceProperty)) ? int.Parse(pageSizeResourceProperty) : PAGESIZE_DEFAULT;
        }
        else
        {
            PageSize = PAGESIZE_DEFAULT;
            int pageSize;
            if (Int32.TryParse(txtPageSize.Text, out pageSize))
            {
                if (pageSize < PAGESIZE_MIN)
                {
                    pageSize = PAGESIZE_MIN;
                }
                if (pageSize > PAGESIZE_MAX)
                {
                    pageSize = PAGESIZE_MAX;
                }
                PageSize = pageSize;
            }
        }
        txtPageSize.Text = PageSize.ToString();
    }

    private void setPageNumber()
    {
        txtPage.Text = (PageNumber + 1).ToString();
    }

    private void setNavigationArrows()
    {
        lbFirst.Enabled = true;
        lbPrev.Enabled = true;
        lbNext.Enabled = true;
        lbLast.Enabled = true;

        if (ItemsCount <= 0)
        {
            lbFirst.Enabled = false;
            lbPrev.Enabled = false;
            lbNext.Enabled = false;
            lbLast.Enabled = false;
        }
        else
        {
            if (PageNumber == 0)
            {
                lbFirst.Enabled = false;
                lbPrev.Enabled = false;
            }
            if (PageNumber >= (TotalPages - 1))
            {
                lbNext.Enabled = false;
                lbLast.Enabled = false;
            }
        }

        imgFirst.ImageUrl = lbFirst.Enabled ? "/Orion/VIM/images/Arrows/page-first.gif" : "/Orion/VIM/images/Arrows/page-first-disabled.gif";
        imgPrev.ImageUrl = lbPrev.Enabled ? "/Orion/VIM/images/Arrows/page-prev.gif" : "/Orion/VIM/images/Arrows/page-prev-disabled.gif";
        imgNext.ImageUrl = lbNext.Enabled ? "/Orion/VIM/images/Arrows/page-next.gif" : "/Orion/VIM/images/Arrows/page-next-disabled.gif";
        imgLast.ImageUrl = lbLast.Enabled ? "/Orion/VIM/images/Arrows/page-last.gif" : "/Orion/VIM/images/Arrows/page-last-disabled.gif";
    }

    private void bindDataToSource()
    {
        setPageSize();
        if (ItemsCount == 0)
        {
            setItemsCount();
        }

        var table = DAL.GetVMsIdle(PageNumber + 1, PageSize);

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    protected void Grid_IndexChanging(object sender, CommandEventArgs e)
    {
        setPageSize();
        setItemsCount();

        switch (e.CommandArgument.ToString().ToLowerInvariant())
        {
            case "next":
                {
                    if (PageNumber < (double)ItemsCount / PageSize - 1)
                        PageNumber += 1;
                    break;
                }
            case "prev":
                {
                    if (PageNumber > 0)
                        PageNumber -= 1;
                    break;
                }
            case "first":
                {
                    PageNumber = 0;
                    break;
                }
            case "last":
                {
                    PageNumber = (int)Math.Ceiling((double)ItemsCount / PageSize) - 1;
                    if (PageNumber < 0)
                    {
                        PageNumber = 1;
                    }
                    break;
                }
        }

        setTotalPages();
        setPageNumber();
        setNavigationArrows();
        bindDataToSource();
    }

    void txtPageSize_TextChanged(object sender, EventArgs e)
    {
        setItemsCount();
        setPageSize();

        PageNumber = 0;
        setTotalPages();
        setPageNumber();
        setNavigationArrows();
        bindDataToSource();
    }

    void txtPage_TextChanged(object sender, EventArgs e)
    {
        setItemsCount();
        setPageSize();

        int page = 0;
        if (Int32.TryParse(txtPage.Text, out page))
        {
            page--;
            if (page < 0)
            {
                page = 0;
            }
            if (page >= TotalPages)
            {
                page = TotalPages - 1;
            }
        }

        PageNumber = page;
        setTotalPages();
        setPageNumber();
        setNavigationArrows();
        bindDataToSource();
    }

    void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) &&
            e.Item.DataItem is System.Data.DataRowView)
        {
            System.Data.DataRow row = (e.Item.DataItem as System.Data.DataRowView).Row;
            if (row != null)
            {
                Literal litNode = e.Item.FindControl("litNode") as Literal;
                Literal litAvgCPU = e.Item.FindControl("litAvgCPU") as Literal;
                Literal litAvgIOPS = e.Item.FindControl("litAvgIOPS") as Literal;
                Literal litAvgNet = e.Item.FindControl("litAvgNet") as Literal;
                ManagementActionButton btn = e.Item.FindControl("btnPowerOff") as ManagementActionButton;

                String nodeLink = EntityLinkHelper.GetVMEntityLink(row["DetailsUrl"], row["Name"], row["Status"], row["PlatformID"], true, row);
                if (!String.IsNullOrEmpty(nodeLink))
                {
                    litNode.Text = nodeLink;
                }
                litAvgCPU.Text = FormatHelper.FormatPercent(row["CpuLoadWeek"], 2);
                litAvgIOPS.Text = FormatHelper.FormatIOPS(row["IOPSWeek"], 2);
                litAvgNet.Text = FormatNetworkSpeed((float)(DBHelper.GetDbValueOrDefault<double>(row["NetworkTransmitRateWeek"], 0.0)));

                btn.VmId = row["VirtualMachineID"] != DBNull.Value ? Convert.ToInt32(row["VirtualMachineID"]) : 0;
                btn.Action = ManagementEnums.ActionName.PowerOff;
                btn.EncapsulateBySpan = false;
            }
        }
    }

}