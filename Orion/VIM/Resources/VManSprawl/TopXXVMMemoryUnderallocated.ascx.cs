﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Web.DAL.VMan.Sprawl;
using SolarWinds.VIM.Web.Resources;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VManSprawl_TopXXVMMemoryUnderallocated : VimTopXXResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Wrapper.CanShowContent)
            return;

        DataTable table;

        try
        {
            var dal = new TopXXVMMemoryAllocatedDAL();
            table = dal.GetTopXXVMMemoryUnderallocated(MaxRecords);
        }
        catch (SwisQueryException)
        {
            SQLErrorPanel.Visible = true;
            return;
        }

        resourceTable.DataSource = table;
        resourceTable.DataBind();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.resourceTable.ItemCreated += new RepeaterItemEventHandler(resourceTable_ItemCreated);
    }

    private void resourceTable_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    if (e.Item.DataItem is System.Data.DataRowView)
                    {                        
                        var row = (e.Item.DataItem as System.Data.DataRowView).Row;
                       
                        if (row != null)
                        {
                            var recommendTipLabel = e.Item.FindControl("recommendTipLabel") as Label;
                            Int64 memorySize = DBHelper.GetDbValueOrDefault(row["MemoryConfigured"], (Int64)0);
                            
                            recommendTipLabel.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_LV0_20, FormatBytes(memorySize),
                                                                   FormatBytes(memorySize * 2));
                        }                         
                    }
                }
                break;
        }
    }    

    protected override string TitleTemplate
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_LV0_4; }
    }

    public override string EditControlLocation
    {
        get
        {
            return "/Orion/VIM/Controls/EditResourceControls/VManTopXXEditControl.ascx";
        }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionVMPHTopXXVMUnderallocatedMemory"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }
}