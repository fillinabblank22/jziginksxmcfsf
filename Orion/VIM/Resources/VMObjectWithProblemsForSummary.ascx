﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VMObjectWithProblemsForSummary.ascx.cs" Inherits="Orion_VIM_Resources_VMObjectWithProblemsForSummary" %>
<%@ Register Src="~/Orion/VIM/Controls/VMobjectWithProblemsImpl.ascx" TagPrefix="vim" TagName="VMObjectWithProblemsImpl" %>

<orion:resourceWrapper runat="server" ID="ResourceWrapper1">
   <Content>
    <vim:VMObjectWithProblemsImpl runat="server" ID="impl" />
   </Content>    
</orion:resourceWrapper>

