﻿using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Common.Helpers;
using SolarWinds.VIM.Common.Models;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Resources;
using SolarWinds.VIM.Web.StatusIcons;
using System;
using System.Data;
using System.Web;
using SolarWinds.VIM.Web.Common.Extensions;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
public partial class Orion_VIM_Resources_VCenterDetails : VimVCenterBaseResource
{
    protected int HostStatus { get; private set; }
    protected int VCenterStatus { get; private set; }
    protected PollingSource PollingSource { get; private set; }
    protected int VCenterId { get; private set; }
    protected int NoOfClusters { get; set; }
    protected int NoOfVsanClusters { get; set; }

    protected override string DefaultTitle
    {
        get { return Resources.VIMWebContent.VIMWEBCODE_TM0_1; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionPHResourceVCenterDetails"; }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INodeProvider) }; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.RenderControl; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable table = (new VCenterDetailsDAL()).GetVCenterDetails(this.ManagedNode.NodeID);

        if (table.Rows.Count == 0)
        {
            // hide for non vCenters
            this.Visible = false;
            return;
        }

        DataRow row = table.Rows[0];

        PollingSource = (PollingSource) Convert.ToInt32(row["PollingSource"]);

        NodeLink1.Node = ManagedNode;
        NodeStatusIcon.ImageUrl = String.Format("/Orion/StatusIcon.ashx?size=small&entity=Orion.Nodes&status={0}", row["Status"]);
        Image1.ImageUrl = String.Format("/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}", VimIconProvider.VCenterEntityName, row["Status"]);
        VCenterStatus = DBHelper.GetDbValueOrDefault(row["Status"], 0);

        string statusMessage = Convert.ToString(row["StatusMessage"]).HtmlEncode();
        VMStateMessage.Text = statusMessage;
        phStateMessage.Visible = !String.IsNullOrEmpty(statusMessage);

        ProductNameLit.Text = HttpUtility.HtmlEncode(Convert.ToString(row["VMwareProductName"]));
        ProductVersionLit.Text = HttpUtility.HtmlEncode(Convert.ToString(row["VMwareProductVersion"]));
        NumberOfDataCentersLit.Text = Convert.ToString(row["DataCenterCount"]);
        NoOfClusters = DBHelper.GetDbValueOrDefault(row["ClusterCount"], 0);
        NoOfVsanClusters = DBHelper.GetDbValueOrDefault(row["VSanClusterCount"], 0);
        NumberOfEsxHostsLit.Text = Convert.ToString(row["HostCount"]);
        NumberOfVMsLit.Text = Convert.ToString(row["VMCount"]);
        HostStatus = Convert.ToInt32(row["HostStatus"]);
        VCenterId = Convert.ToInt32(row["VCenterID"]);

        DataBind();
    }
}