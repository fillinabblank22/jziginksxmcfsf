﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VCenterDetails.ascx.cs" Inherits="Orion_VIM_Resources_VCenterDetails" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Register TagPrefix="orion" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="VIM" TagName="AssignCredentialsDialog" Src="~/Orion/VIM/Controls/AssignVMWareCredentialsDialog.ascx" %>
<%@ Register TagPrefix="orion" TagName="PollingWarning" Src="~/Orion/VIM/Controls/PollingWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="true">
    <Content>
        <VIM:AssignCredentialsDialog ID="AssignCredentialsDialog" runat="server" debug="false" />

        <table class="sw-vim-entityDetails NeedsZebraStripes">
            <asp:PlaceHolder ID="phNodeManagement" runat="server" Visible="<%# Profile.AllowNodeManagement || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>">
                <tr>
                    <td class="PropertyHeader" style="width: 50%"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_1%></td>
                    <td class="Property">
                        <span onclick="ShowAssignCredentialDialog(<%= this.NodeID %>); return false;" style="cursor:pointer;" >
                            <img src='/Orion/images/edit_16x16.gif' alt='' />&nbsp;<%= Resources.VIMWebContent.VIMWEBDATA_TM0_11%>
                        </span>
                    </td>
                </tr>
            </asp:PlaceHolder>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_2%></td>
                <td class="Property" id="tdvmHost">
                    <asp:Image runat="server" ID="NodeStatusIcon" /><orion:NodeLink ID="NodeLink1" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_3%></td>
                <%= ManagedStatusHelper.GetVCenterStatusTD(this.VCenterStatus) %>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.EntityDetails_PollingSource%></td>
                <td class="Property"><%= PollingWarning.LocalizedPollingSource%></td>
            </tr>

            <% if (PollingWarning.Visible) { %>
            <tr style="visibility: collapse"></tr>
            <tr>
                <td style="padding-top:0" colspan="2">
                    <orion:PollingWarning ID="PollingWarning" runat="server" EntityId="<%# VCenterId%>" PollingSource="<%# PollingSource%>" ViewType="<%# ViewType.vCenter%>" NodeId="<%# NodeID%>" />
                </td>
            </tr>
            <% } %>

            <asp:PlaceHolder ID="phStateMessage" runat="server">
                <tr>
                    <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_4%></td>
                    <td class="Property">
                        <asp:Image runat="server" ID="Image1" />&nbsp;<asp:Literal runat="server" ID="VMStateMessage" />
                    </td>
                </tr>
            </asp:PlaceHolder>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_6%></td>
                <td class="Property"><asp:Literal runat="server" ID="ProductNameLit" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_7%></td>
                <td class="Property"><asp:Literal runat="server" ID="ProductVersionLit" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_8%></td>
                <td class="Property"><asp:Literal runat="server" ID="NumberOfDataCentersLit" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_VB0_101%></td>
                <td class="Property">
                    <%= String.Format(Resources.VIMWebContent.Clusters_Total_And_VSan, NoOfClusters, NoOfVsanClusters)%>
                </td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_9%></td>
                <td class="Property"><asp:Literal runat="server" ID="NumberOfEsxHostsLit" /></td>
            </tr>
            <tr>
                <td class="PropertyHeader"><%= Resources.VIMWebContent.VIMWEBDATA_TM0_10%></td>
                <td class="Property"><asp:Literal runat="server" ID="NumberOfVMsLit" /></td>
            </tr>
        </table>
    </Content>
</orion:resourceWrapper>