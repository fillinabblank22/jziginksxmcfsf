﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualizationTags.ascx.cs" Inherits="Orion_VIM_Resources_VirtualizationTags" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/VIM/Controls/VManResourceWrapper.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<orion:Include runat="server" Module="VIM" File="Resources/Formatters.js" />

<orion:ResourceWrapper runat="server" ID="Wrapper">
    <Content>
        <div class="sw-suggestion" id="sw-vim-virtualizationtagsresource-notagsdiv" style="display:none;">
            <span class="sw-suggestion-icon"></span>
            <span><%= VIMWebContent.VirtualizationTagWidget_NoTagsAssignedInfoText %></span> 
        </div>

        <div id="sw-vim-virtualizationtagsresource-customtable">
            <orion:CustomQueryTable runat="server" ID="CustomTable" />
        </div>

        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        uniqueId: <%= CustomTable.UniqueClientID %>,
                        initialPage: 0,
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "10" %>,
                        allowSort: true,
                        onLoad: function (rows, columns) {
                            if (rows.length == 0) {
                                $("#sw-vim-virtualizationtagsresource-notagsdiv").show();
                                $("#sw-vim-virtualizationtagsresource-customtable").hide();
                            }
                        },
                        columnSettings: {
                            "Name": {
                                header: '<%= VIMWebContent.VirtualizationTagWidget_TagColumn_Name %>'
                            },
                            "Description": {
                                header: '<%= VIMWebContent.VirtualizationTagWidget_TagDescription_ColumnName %>'
                            }
                        }
                    });

                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                refresh();
            });
        </script>
    </Content>
</orion:ResourceWrapper>