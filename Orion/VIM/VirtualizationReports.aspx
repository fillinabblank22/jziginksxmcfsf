﻿<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="Orion Website Error"%>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Exceptions" %>

<script runat="server">
    protected override void OnInit(EventArgs e)
    {
        if (!SolarWinds.VIM.Web.Common.Licensing.LicenseManager.Instance.IsVimFullyLicensed())
        {
            throw new VManUnauthorizedAccessException();
        }
        base.OnInit(e);

        //this will redirect to Virtualization Reports group
        WebUserSettingsDAL.Set("ReportManager_GroupingValue", "Category,Virtualization Reports,");
        Response.Redirect("/Orion/Reports/Default.aspx", true);
    }
</script>

<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" Runat="Server">
</asp:Content>
