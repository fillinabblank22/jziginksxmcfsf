﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.VIM.Web.Views;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.Orion.Web;

public partial class Orion_VIM_Virtualization_DataCenterDetails : VimViewPage, IVimDataCenterProvider
{
    public override string ViewType
    {
        get { return "VIM DataCenter Details"; }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVIMAG_Views_VMwareDatacenterDetails";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string PageTitle { get; set; }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    #region IVimDataCenterProvider Members

    public VimDataCenter DataCenter
    {
        get { return (VimDataCenter)this.NetObject; }
    }

    #endregion
}
