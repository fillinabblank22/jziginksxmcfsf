﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Infragistics.WebUI.UltraWebGauge;
using SolarWinds.NPM.Web.Gauge.V1;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.DAL.VMan.VirtualMachine;
using WebCommonThresholdDAL = SolarWinds.VIM.Web.Common.DAL.ThresholdDAL;


public partial class Orion_VIM_GaugePage : System.Web.UI.Page
{
    private string _style;
    private string _gaugeType;
    private string _unitsLabel;
    private string _netObject;
    private string _property;

    private int _scale;
    private int _units;
    private int _min;
    private int _max;
    private string _rangeMethod;
    private string _rangeValue;
    private float _warningThreshold;
    private float _criticalThreshold;
    private bool _reverseThreshold;
    private double _value;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadParameters();

        if (!string.IsNullOrEmpty(_netObject))
        {
            string netObjectId = _netObject.Substring(_netObject.IndexOf(":") + 1, _netObject.Length - _netObject.IndexOf(":") - 1);
            string netObjectPrefix = _netObject.Substring(0, _netObject.IndexOf(":"));
            GetDataByName(netObjectPrefix, netObjectId, _property, ref  _value, ref _warningThreshold, ref _criticalThreshold);
        }

        // Make maximum of gauge dynamic depending on thresholds. If there are no thresholds,
        // make if dynamic depending only on current value.
        double resultValue = _value * _units;

        // fallback to dynamic method if current is thresholds and there are not thresholds
        if (_rangeMethod.Equals("thresholds", StringComparison.OrdinalIgnoreCase))
        {
            if (_criticalThreshold == Int32.MaxValue && _warningThreshold == Int32.MaxValue)
            {
                _rangeMethod = "dynamic";
                _rangeValue = "10, 20, 40, 60, 100, 200, 400, 600, 1000";
            }
        }

        switch (_rangeMethod)
        {
            case "static":
                if (!Int32.TryParse(_rangeValue, out _max))
                    _max = 30;
                break;
            case "dynamic":
                string[] dynamicRangeStr = _rangeValue.Split(',');
                var dynamicRange = new List<int>(dynamicRangeStr.Length);
                foreach (string strVal in dynamicRangeStr)
                {
                    int val;
                    if (Int32.TryParse(strVal.Trim(), out val))
                    {
                        dynamicRange.Add(val);
                    }
                }

                int numScales = dynamicRange.Count;
                int i = 0;
                _max = dynamicRange.FirstOrDefault();
                while (resultValue > _max && i < numScales)
                {
                    _max = dynamicRange[i++];
                }
                break;
            case "thresholds":
                Double thresholdsMultiplier;
                if (!Double.TryParse(_rangeValue, out thresholdsMultiplier))
                    thresholdsMultiplier = 1.3;

                if (_criticalThreshold != Int32.MaxValue)
                    _max = (int)(_criticalThreshold * thresholdsMultiplier);
                else
                    _max = (int)(_warningThreshold * thresholdsMultiplier);
                break;
        }

        // if max is for some reason smaller than zero, set it to one to make gauge work
        _max = Math.Max(1, _max);

        Image image = null;
        if (_gaugeType.Equals("Radial"))
        {
            var generator = new GaugeGenerator();

            UltraGauge gauge = generator.GenerateGauge(_scale, _style, resultValue, _min, _max, string.Format("{0:0.##}{1}", resultValue, _unitsLabel),
                                                       Request.QueryString["GaugeName"], _warningThreshold, _criticalThreshold, _reverseThreshold);

            image = gauge.GetImage(Infragistics.UltraGauge.Resources.GaugeImageType.Png, new Size((int) (gauge.Width.Value), (int) (gauge.Height.Value + 15)));
        }

        using (image)
        using (var stream = new MemoryStream())
        {
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
            Response.ContentType = "image/png";
            stream.WriteTo(Response.OutputStream);
            Response.End();
        }
    }

    private void LoadParameters()
    {
        _style = Request.QueryString["Style"];
        _gaugeType = Request.QueryString["GaugeType"];
        _unitsLabel = Request.QueryString["UnitsLabel"];
        _netObject = Request.QueryString["NetObject"];
        _property = Request.QueryString["Property"];

        if (string.IsNullOrEmpty(_unitsLabel)) _unitsLabel = " s";

        if (!Single.TryParse(Request.QueryString["WarningThreshold"], out _warningThreshold)) { _warningThreshold = 0; }
        if (!Single.TryParse(Request.QueryString["CriticalThreshold"], out _criticalThreshold)) { _criticalThreshold = 0; }
        if (!Boolean.TryParse(Request.QueryString["ReverseThreshold"], out _reverseThreshold)) { _reverseThreshold = false; }
        if (!Int32.TryParse(Request.QueryString["Min"], out _min)) { _min = 0; }
        if (!Int32.TryParse(Request.QueryString["Max"], out _max)) { _max = 10; }
        if (!Int32.TryParse(Request.QueryString["Scale"], out _scale)) { _scale = 1; }
        if (!Int32.TryParse(Request.QueryString["Units"], out _units)) { _units = 1; }
        if (!Double.TryParse(Request.QueryString["Value"], out _value)) { _value = 0; }

        _rangeMethod = Request.QueryString["RangeMethod"];
        _rangeValue = Request.QueryString["RangeValue"];

        if (string.IsNullOrEmpty(_style))
        {
            _style = "Elegant Black";
        }
    }

    private void GetDataByName(string netObjectPrefix, string netObjectId, string property, ref double value, ref float warningThreshold, ref float criticalThreshold)
    {
        switch (netObjectPrefix.ToUpperInvariant())
        {
            case VManDataStore.Prefix:
                {
                    GetDataForDataStoreGauge(Convert.ToInt32(netObjectId), property, ref value, ref warningThreshold, ref criticalThreshold);
                    break;
                }
            case VimHost.Prefix:
                {
                    GetDataForHostGauge(Convert.ToInt32(netObjectId), property, ref value, ref warningThreshold, ref criticalThreshold);
                    break;
                }
            case VimVirtualMachine.Prefix:
                {
                    GetDataForVMGauge(Convert.ToInt32(netObjectId), property, ref value, ref warningThreshold, ref criticalThreshold);
                    break;
                }
        }
    }

    private void GetDataForDataStoreGauge(int netObjectId, string property, ref double value, ref float warningThreshold, ref float criticalThreshold)
    {
        var dataStore = new VManDataStore(String.Format("{0}:{1}", VManDataStore.Prefix, netObjectId));

        switch (property.ToUpperInvariant())
        {
            case "IOPS":
            {
                value = dataStore.Model.IOPS;

                var dal = new ThresholdDAL();
                dal.GetDatastoreThresholds(netObjectId, WebCommonThresholdDAL.IOPSTotalThreshold, ref warningThreshold, ref criticalThreshold);

                break;
            }
            case "LATENCY":
            {
                value = dataStore.Model.Latency;

                var dal = new ThresholdDAL();
                dal.GetDatastoreThresholds(netObjectId, WebCommonThresholdDAL.LatencyTotalThreshold, ref warningThreshold, ref criticalThreshold);

                break;
            }
        }
    }

    private void GetDataForHostGauge(int id, string property, ref double value, ref float warningThreshold, ref float criticalThreshold)
    {
        System.Data.DataTable dataTable = new HostDAL().GetCPUMemNetByHostId(id);
        if (dataTable != null && dataTable.Rows.Count > 0)
        {
            switch (property.ToUpperInvariant())
            {
                case "CPU":
                {
                    Double.TryParse(dataTable.Rows[0]["CpuLoad"].ToString(), out value);

                    var dal = new WebCommonThresholdDAL();
                    dal.GetHostThresholds(id, WebCommonThresholdDAL.CpuLoadThreshold, ref warningThreshold, ref criticalThreshold);

                    break;
                }
                case "MEMORY":
                {
                    Double.TryParse(dataTable.Rows[0]["MemUsage"].ToString(), out value);

                    var dal = new WebCommonThresholdDAL();
                    dal.GetHostThresholds(id, WebCommonThresholdDAL.MemUsageThreshold, ref warningThreshold, ref criticalThreshold);

                    break;
                }
                case "NETWORK":
                {
                    Double.TryParse(dataTable.Rows[0]["NetworkUtilization"].ToString(), out value);

                    var dal = new WebCommonThresholdDAL();
                    dal.GetHostThresholds(id, WebCommonThresholdDAL.NetworkThreshold, ref warningThreshold, ref criticalThreshold);

                    break;
                }
            }
        }
    }

    private void GetDataForVMGauge(int id, string property, ref double value, ref float warningThreshold, ref float criticalThreshold)
    {
        switch (property.ToUpperInvariant())
        {
            case "CPU":
                {
                    var dataTable = new VirtualMachineDAL().GetCPUMemNetByVMId(id);
                    if (dataTable == null || dataTable.Rows.Count == 0)
                        return;

                    Double.TryParse(dataTable.Rows[0]["CpuLoad"].ToString(), out value);

                    var dal = new ThresholdDAL();
                    dal.GetVMThresholds(id, WebCommonThresholdDAL.CpuLoadThreshold, ref warningThreshold, ref criticalThreshold);

                    return;
                }
            case "MEMORY":
                {
                    var dataTable = new VirtualMachineDAL().GetCPUMemNetByVMId(id);
                    if (dataTable == null || dataTable.Rows.Count == 0)
                        return;

                    Double.TryParse(dataTable.Rows[0]["MemUsage"].ToString(), out value);

                    var dal = new ThresholdDAL();
                    dal.GetVMThresholds(id, WebCommonThresholdDAL.MemUsageThreshold, ref warningThreshold, ref criticalThreshold);

                    return;
                }
            case "NETWORK":
                {
                    var dataTable = new VirtualMachineDAL().GetCPUMemNetByVMId(id);
                    if (dataTable == null || dataTable.Rows.Count == 0)
                        return;

                    Double.TryParse(dataTable.Rows[0]["NetworkUsageRate"].ToString(), out value);

                    var dal = new ThresholdDAL();
                    dal.GetVMThresholds(id, WebCommonThresholdDAL.NetworkUsageRateThreshold, ref warningThreshold, ref criticalThreshold);

                    return;
                }
            case "IOPS":
                {
                    var dataTable = new IopsAndLatencyDAL().GetIopsAndLatency(id);
                    if (dataTable == null || dataTable.Rows.Count == 0)
                        return;

                    Double.TryParse(dataTable.Rows[0]["IOPSTotal"].ToString(), out value);

                    var dal = new ThresholdDAL();
                    dal.GetVMThresholds(id, WebCommonThresholdDAL.IOPSTotalThreshold, ref warningThreshold, ref criticalThreshold);

                    return;
                }
            case "LATENCY":
                {
                    var dataTable = new IopsAndLatencyDAL().GetIopsAndLatency(id);
                    if (dataTable == null || dataTable.Rows.Count == 0)
                        return;

                    Double.TryParse(dataTable.Rows[0]["LatencyTotal"].ToString(), out value);

                    var dal = new ThresholdDAL();
                    dal.GetVMThresholds(id, WebCommonThresholdDAL.LatencyTotalThreshold, ref warningThreshold, ref criticalThreshold);

                    return;
                }
        }
    }
}
