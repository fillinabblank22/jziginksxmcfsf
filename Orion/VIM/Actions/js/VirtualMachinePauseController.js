﻿SW.Core.namespace("SW.VIM.Actions").VirtualMachinePauseController = function (config) {
    "use strict";

    var selectionTypeController = null;

    var createConfiguration = function () {
        var configuration = {
        };
        selectionTypeController.addProperties(configuration);
        return configuration;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {
        SW.Core.Services.callController("/api/VIMVirtualMachinePause/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    this.init = function () {
        $container = $('#' + config.containerID);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
       
        if (config.isValid == false) {
            var validator = new SW.VIM.Actions.Validation.Validator({});
            validator.hideData();
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = selectionTypeController.validate(sectionID);

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        
        selectionTypeController.addPropertiesToArray(jsonProperties);

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {
        var configuration = createConfiguration();

        // Edit mode
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    this.setSelectionTypeController = function(controller) {
        selectionTypeController = controller;
    };

    var self = this;
    var $container = null;
}
