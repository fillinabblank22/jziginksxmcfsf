﻿var datastoreTargetGrid;

SW.Core.namespace("SW.VIM.Actions").VirtualMachineStorageMotionController = function (config) {
    "use strict";

    var selectionTypeController = null;
    var datastoreGridInitialed = false;
    var powerBatchController;
    var vendor;

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var validateManageActionSection = function() {
        var isValid = true;
        if (isEditable(config.dataFormMapping.targetDatastoreId)) {
            isValid = datastoreTargetGrid.validate();
        }
        return isValid;
    };

    var createConfiguration = function () {
        var datastore = getTargetDatastore();
        var configuration = {
            TargetDatastoreId: datastore.datastoreId,
            TargetDatastorePath: datastore.datastorePath,
            TargetDatastoreName: datastore.datastoreName,
        };

        selectionTypeController.addProperties(configuration);

        return configuration;
    };

    var getTargetDatastore = function() {
        return datastoreTargetGrid.getSelectedDatastore();
    };


    var createActionAndExecuteCallback = function(configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function(action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function(action, param, callback) {
        SW.Core.Services.callController("/api/VIMVirtualMachineStorageMotion/" + action, param,
            // On success
            function(response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
            // On error
            function(msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var initDatastoreGrid = function () {
        if (datastoreGridInitialed)
            return;
        datastoreGridInitialed = true;
        datastoreTargetGrid = new SW.VIM.Actions.DatastoreTargetGrid();
        datastoreTargetGrid.init({
            targetDatastoreId: config.targetDatastoreId,
            targetDatastorePlatform: config.targetDatastorePlatform,
            targetDatastoreName: config.targetDatastoreName,
            targetDatastorePath: config.targetDatastorePath,
            containerID: config.containerID,
            vendor: vendor
        }); 
    }

    var renderDatastoreGrid = function () {
        if (datastoreGridInitialed)
            return;
        var timer = setInterval(function () {
            initDatastoreGrid();
            clearInterval(timer);
        }, 50);
    }

    this.init = function() {
        $container = $('#' + config.containerID);

        //very dirty hack to render our grid after section is rendered active and full visible. We are waiting for implementation of CORE-638
        $('#datastoreSelection').click(function() {
            renderDatastoreGrid();
        });
        $('.dialogContainer').on("click", "#nextSectionInActionDefinitionBtn", function () { renderDatastoreGrid(); });

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }

        if (config.isValid == false) {
            var validator = new SW.VIM.Actions.Validation.Validator({});
            validator.hideData();
        } else {
            if (config.multiEditMode) {
                for (var key in config.dataFormMapping) {
                    var dataform = config.dataFormMapping[key];
                    if (dataform == config.dataFormMapping.targetDatastoreId) {
                        datastoreTargetGrid.setDisabled(true);
                        $container.find('[data-edited="' + dataform + '"]').click(function() {
                            datastoreTargetGrid.setDisabled(!$(this).find("input").is(':checked'));
                        });
                    }
                }
            }
        }

    };

    //this.onAccordionHeaderClicked = function (sectionId) {
    //    if (sectionId === 'manageActionSection')
    //        initDatastoreGrid();
    //    return "";
    //};

    // Validate action configuration. 
    this.validateSectionAsync = function(sectionID, callback) {
        var valid = true;
        valid = selectionTypeController.validate(sectionID);
        if (sectionID === 'manageActionSection') {
            valid = valid & validateManageActionSection();
        }

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function(callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.targetDatastoreId) {
                    var datastore = getTargetDatastore();
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: datastore.datastoreId });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.targetDatastorePath, PropertyValue: datastore.datastorePath });
                    jsonProperties.push({ PropertyName: config.dataFormMapping.targetDatastoreName, PropertyValue: datastore.datastoreName });
                }
        };

        selectionTypeController.addPropertiesToArray(jsonProperties);

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    }

    this.getActionDefinitionAsync = function(callback) {
        var configuration = createConfiguration();
        if (powerBatchController !== undefined)
            powerBatchController.addProperties(configuration);

        // Edit mode
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    this.setSelectionTypeController = function (controller) {
        selectionTypeController = controller;
    }

    this.setPowerBatchController = function (controller) {
        powerBatchController = controller;
    }

    this.setVendor = function (vendorType) {
        if (datastoreGridInitialed) {
            datastoreTargetGrid.setVendor(vendorType);
        }

        vendor = vendorType;
    }

    var self = this;
    var $container = null;
}
