﻿SW.Core.namespace('SW.VIM.Actions').SelectionTypeController = function (config) {
    "use strict";

    var gridInitialized = false;
    var lastSelectedSpecificVM;
    var selectionTypeSpecific = 'Specific';
    var selectionTypeContext = 'Context';
    var vendorVMware = 'Vmware';
    var vendorHyperV = 'HyperV';
    var vendorUnknown = 'Unknown';
    var invalidField = null;
    var $container = null;
    var specificGrid = null;
    var multiEditMode;
    var mainController;
    
    var mapVendorNameToInt = function(vendorName)
    {
        switch (vendorName) {
        case vendorVMware:
            return 1;
        case vendorHyperV:
            return 2;
        default:
            return 0;
        }
    }

    var initHandlers = function() {
        $container.find("input").on("change", function() {
            toggleInputs($(this));
        });

        $container.find("#dplSelectionTypeContextVendor").on("change", function () {
            toggleVendorComboBox($(this));
        });

    }

    var toggleVendorComboBox = function(comboBox) {
        var val = comboBox.val();
        setVendor(mapVendorNameToInt(val));
    }

    var toggleInputs = function (input) {
        if (input.val() == selectionTypeSpecific) {
            $container.find('#sw-vim-action-vmselection-vendorWrapper').hide();
            $container.find('#selectionGridWrapper').show();
            $container.find('#selectionContextWrapper').hide();
            $container.find('#selectedOption').val(selectionTypeSpecific);
            $('#lbSelectedVM').text(lastSelectedSpecificVM);
            setVendor(mapVendorNameToInt(vendorUnknown));
            initSpecificSelectionGrid();
        } else if (input.val() == selectionTypeContext) {
            setVendor(mapVendorNameToInt(getVendor()));
            $container.find('#sw-vim-action-vmselection-vendorWrapper').show();
            $container.find('#selectionGridWrapper').hide();
            $container.find('#selectionContextWrapper').show();
            $container.find('#selectedOption').val(selectionTypeContext);
            if (invalidField != null)
                invalidField.removeClass('invalidValue');

            var lbSelectedVM = $('#lbSelectedVM');
            lastSelectedSpecificVM = lbSelectedVM.text();
            lbSelectedVM.text('');
        } else if (input.attr('id') == 'chxbSelectionTypeContextVendor') {
            var vendorComboBox = findVendorComboBox();
            var isChecked = !input.is(':checked');
            vendorComboBox.attr('disabled', isChecked);
            setVendor(mapVendorNameToInt(getVendor()));
        }
    }

    var setVendor = function (vendor) {
        if (jQuery.isFunction(mainController.setVendor))
            mainController.setVendor(vendor);
    }

    var findVendorComboBox = function()
    {
        return $container.find('#dplSelectionTypeContextVendor');
    }

    var findVendorCheckbox = function () {
        return $container.find('#chxbSelectionTypeContextVendor');
    }

    var initValues = function (config) {
        if (config.selectedVirtualMachineName != null)
            $('#lbSelectedVM').text(config.selectedVirtualMachineName);

        if (config.selectionType == selectionTypeSpecific) {
            $container.find('#sw-vim-action-vmselection-vendorWrapper').hide();
            $container.find('#selectionGridWrapper').show();
            initSpecificSelectionGrid();
        }
        findVendorComboBox().attr('disabled', !findVendorCheckbox().is(':checked'));
    }

    var initSpecificSelectionGrid = function() {
        if (!gridInitialized) {
            gridInitialized = true;

            specificGrid = new SW.VIM.Actions.SelectSpecificVMGrid();
            specificGrid.init({
                container: $container,
                vendor: mapVendorNameToInt(config.vendor)
            });
        }
    };

    var initMultiEditModeHandlers = function()
    {
        if (!multiEditMode)
            return;

        $container.find('#chbxSelection').on("change", function() {
            toggleDisabled($(this));
        });
    }

    var toggleDisabled = function(checkbox) {
        var checked = checkbox.is(':checked');
        specificGrid.setDisabled(!checked);
        $container.find("input[type=radio]").attr('disabled', !checked);
    }

    var getVendor = function() {
        var checkedInput = getSelectedModificationRadioButton();
        if (checkedInput.val() == selectionTypeContext && findVendorCheckbox().is(':checked')) {
                return findVendorComboBox().find('option:selected').val();
        }
        return null;
    }

    var getSelectedModificationRadioButton = function()
    {
        return $container.find("input:checked[type=radio]");
    }

    //public methods
    this.init = function () {
        gridInitialized = false;
        multiEditMode = config.multiEditMode;
        $container = $('#' + config.containerID);
        mainController = window[config.mainController];
        setVendor(mapVendorNameToInt(getVendor()));

        initValues(config);
        initHandlers();
        initMultiEditModeHandlers();
    };

    this.validate = function(sectionId) {
        if (sectionId !== 'selectionWrapper')
            return true;
        var checkedInput = getSelectedModificationRadioButton();
        if (checkedInput.val() != selectionTypeContext) {
            var virtualMachineId = $container.find('#selectedVirtualMachineId').val();
            if (virtualMachineId.length == 0 || virtualMachineId == -1) {
                if (checkedInput.val() == selectionTypeSpecific) {
                    invalidField = checkedInput.parent();
                    invalidField.addClass('invalidValue');
                } else {
                    $('#selectionValidationErrorMessage').show();
                    $('#selectionValidationErrorMessage').html("@{R=VIM.Strings;K=VIMWEBJS_ZS0_16;E=js}");
                }                
                return false;
            } else
                checkedInput.parent().removeClass('invalidValue');
        }
        return true;
    };

    this.addProperties = function(configuration) {
        configuration.SelectionType = $('#selectedOption').val();
        configuration.SelectedVirtualMachineId = $('#selectedVirtualMachineId').val();
        configuration.Vendor = getVendor();
    };

    this.addPropertiesToArray = function(jsonProperties) {
        if ($container.find('#chbxSelection').is(":checked")) {
            jsonProperties.push({ PropertyName: 'SelectionType', PropertyValue: $('#selectedOption').val() });
            jsonProperties.push({ PropertyName: 'SelectedVirtualMachineId', PropertyValue: $('#selectedVirtualMachineId').val() });
            jsonProperties.push({ PropertyName: 'Vendor', PropertyValue: getVendor() });
        }
    };
}