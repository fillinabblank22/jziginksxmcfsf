﻿SW.Core.namespace("SW.VIM.Actions").VirtualMachineDeleteSnapshotController = function (config) {
    "use strict";

    var selectionTypeController = null;

    var getSnapshotConfiguration = function () {
        return $container.find('[data-form="' + config.dataFormMapping.snapshotType + '"] :checked').val();
    };

    var createConfiguration = function () {

        var configuration = {
            snapshotType: getSnapshotConfiguration()
        };
        selectionTypeController.addProperties(configuration);

        return configuration;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function (action, param, callback) {

        SW.Core.Services.callController("/api/VIMVirtualMachineDeleteSnapshot/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    this.init = function () {
        $container = $('#' + config.containerID);

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }

        if (config.isValid == false) {
            var validator = new SW.VIM.Actions.Validation.Validator({});
            validator.hideData();
        } else {
            if (config.multiEditMode) {
                var dataform = config.dataFormMapping.snapshotType;
                $container.find('[data-form="' + dataform + '"] input').attr("disabled", true);
                $container.find('[data-edited="' + dataform + '"]').click(function () {
                    $container.find('[data-form="' + dataform + '"] input').attr("disabled", !$(this).find("input").is(':checked'));
                });
            }
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;
        valid = selectionTypeController.validate(sectionID);

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = new Array();
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                jsonProperties.push({ PropertyName: dataform, PropertyValue: getSnapshotConfiguration() });
        }
        selectionTypeController.addPropertiesToArray(jsonProperties);

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    this.getActionDefinitionAsync = function (callback) {

        var configuration = createConfiguration();

        // Edit mode
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    this.setSelectionTypeController = function (controller) {
        selectionTypeController = controller;
    };

    var self = this;
    var $container = null;
}
