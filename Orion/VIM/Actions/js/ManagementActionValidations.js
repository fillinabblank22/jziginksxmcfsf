﻿SW.Core.namespace("SW.VIM.Actions.Validation").ManagementActionValidator = function (config) {
    "use strict";

    var numericInputOnKeyPress = function functionx(event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    };

    var validateRequired = function () {
        var isValid = true;
        var requiredInputs = $container.find('[data-required="true"]');
        requiredInputs.each(function (index) {
            var inpObj = $(this);
            if (inpObj.val().trim().length == 0) {
                inpObj.addClass('invalidValue');
                isValid = isValid && false;
            } else {
                inpObj.removeClass('invalidValue');
            }
        });
        return isValid;
    }
        
    this.init = function () {

        $container = $('#' + config.containerID);

        var numericInputs = $container.find('[data-type="numeric"]');
        numericInputs.each(function (index) {
            var inpObj = $(this);
            inpObj.unbind("keypress");
            inpObj.keypress(function (event) { numericInputOnKeyPress(event); });
        });
    };

    this.validate = function() {
        return validateRequired();
    }

    var $container = null;

}