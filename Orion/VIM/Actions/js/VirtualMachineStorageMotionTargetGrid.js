﻿SW.Core.namespace("SW.VIM.Actions").DatastoreTargetGrid = function() {
    "use strict";

    var swisDatastoreEntity = 'Orion.VIM.Datastores';
    var groupingPanel;
    var unknownVendor = 0;
    var vendor;
    var selectedGroupByFacet;

    // gridba
    var grid;
    var dataStore;
    var dsColumnMapping = [
        { name: 'ID', mapping: 0 },
        { name: 'Name', mapping: 1 },
        { name: 'IPAddress', mapping: 2 },
        { name: 'Status', mapping: 3 },
        { name: 'InstanceType', mapping: 4 },
        { name: 'PlatformID', mapping: 5 },
        { name: 'ManagedObjectID', mapping: 6 },
        { name: 'ChildsCount', mapping: 7 },
        { name: 'Level', mapping: 8 },
        { name: 'CapacityPercentage', mapping: 9 },
        { name: 'Capacity', mapping: 10 },
        { name: 'FreeSpace', mapping: 11 },
        { name: 'CapacityValue', mapping: 12}
    ];

    // grid paging    
    var pagingToolbar;
    var pageSizeBox;
    var pageSize = 40;
    var pagingToolbarFormat = "@{R=VIM.Strings;K=VIMWEBJS_TK0_3;E=js}";
    var noDataPagingToolbarText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_3;E=js}";

    var loadingMask;

    var loadingText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_1;E=js}";

    // grouping
    var groupByCombo;
    var groupByStore;
    var groupByList;
    var useGrouping = false;
    var filterText;
    var selectedDatastoreId = null;
    var selectedDatastorePlatformId = null;
    var selectedDatastoreName = null;
    var container;
    var $container = null;

    var renderEntityName = function(value, meta, record) {
        return String.format('<span>{0}</span>', getEntityLabel(record, true));
    };

    var getEntityLabel = function(record, isCommonContent) {
        var netObject = '';

        var link = '/Orion/View.aspx';

        switch (record.data.InstanceType) {
        case swisDatastoreEntity:
            netObject = 'VMS:' + record.data.ID;
            break;
        }
        
        var content = (isCommonContent) ? getStatusIcon(record) + ' ' + record.data.Name : renderDataStoreContent(record);
        
        return getEntityLink(link, netObject, content);
    };

    var getEntityLink = function (url, netObject, content) {
        return String.format('<a href="{0}?NetObject={1}" target="_blank">{2}</a>', url, netObject, content);
    };

    var renderDataStoreContent = function (record) {
        return String.format("<div class=\"sw-vim-action-storageMotion-capacityValue\">{0} </div><div class=\"BarBackground sw-vim-action-storageMotion-capacityBar\"><div class=\"sw-vim-action-storageMotion-capacityBar_fill\" style=\"width:{1}px;\">&nbsp;</div></div>", record.data.CapacityValue, record.data.CapacityPercentage);
    }

    var renderDataStoreCapacity = function (value, metadata, record) {
        return getEntityLabel(record, false);
    };

    var getStatusIcon = function(record) {
        var entityName = record.data.InstanceType;
        return String.format('<img src="/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}" />', entityName, record.data.Status);
    };

    var toPlatformName = function(platformId) {
        switch (platformId) {
        case 1:
            return 'Vmware';
        case 2:
            return 'HyperV';
        default:
            return '';
        }
    };


    var toVimEntityType = function(swisInstanceType) {
        switch (swisInstanceType) {

        case swisDatastoreEntity:
            return "DataStore";
        default:
            return null;
        }
    };


    var redirectToEditPropertiesPage = function() {
        window.location.href = "/Orion/VIM/Admin/VirtualizationEntityProperties.aspx?&ReturnTo=QmFzZWxpbmVUaHJlc2hvbGRzLmFzcHg=";
    };


    var refreshGroupByList = function (groupByCategory, handler) {
        if (groupByCategory === undefined)
            return;
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory, vendor: typeof (vendor) == 'undefined' ? unknownVendor : vendor };
        if (handler) {
            var internalHandler = function() {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    };

    var initGroupingPanel = function() {

        var list = getGroupByList();

        groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: list
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            width: 150,
            margins: '2 0 0 0',
            listeners: {
                select: function (record, index) {
                    selectedGroupByFacet = record.value;
                    refreshGroupByList(selectedGroupByFacet);

                    if (record.value == 'none') {
                        useGrouping = false;
                        refreshGrid();
                    }
                }
            }
        });

        groupByCombo.setValue('none');

        var selectPanel = new Ext.Container({
            region: 'north',
            height: 51,
            layout: 'vbox',
            cls: 'sw-vim-form-groupByPanel',
            items: [groupByCombo]
        });

        groupByStore = new ORION.WebServiceStore("/Orion/VIM/Services/StorageMotionTargets.asmx/GetEntitiesFacets",
        [
            { name: "Value", mapping: 0 },
            { name: "ValueCount", mapping: 1 },
            { name: "Name", mapping: 2 },
            { name: "InstanceType", mapping: 3 },
            { name: "Status", mapping: 4 }
        ], "Value");

        var template = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="sw-vim-form-groupByItem">',
                    '<tpl if={[datastoreTargetGrid.isVendorGrouping()]} == true>',
                        '<tpl if="Value == 0"><img src="/Orion/VIM/images/Vendors/unknown.gif" /></tpl>',
                        '<tpl if="Value == 1"><img src="/Orion/VIM/images/Vendors/vmware.gif" /></tpl>',
                        '<tpl if="Value == 2"><img src="/Orion/VIM/images/Vendors/hyperv.gif" /></tpl>',
                        ' {[datastoreTargetGrid.renderVendorFacetLabel(values.Value, values.ValueCount)]}',
                    '</tpl>',
                '</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        );

        groupByList = new Ext.DataView({
            border: true,
            store: groupByStore,
            singleSelect: true,
            itemSelector: "div.sw-vim-form-groupByItem",
            tpl: template,
            autoHeight: false,
            autoScroll: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            listeners: {
                selectionchange: function(view, selections) {
                    if (selections.length > 0) {
                        useGrouping = true;
                        refreshGrid();
                    }
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 160,
            split: true,
            items: [
                selectPanel,
                groupByList
            ],
            collapsible: true,
            title: '@{R=VIM.Strings;K=VIMWEBJS_TK0_6;E=js}',
            cls: 'x-panel-bwrap'
        });

        groupingPanel.on('bodyresize', function() {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    this.isVendorGrouping = function () {
        return groupByCombo.getValue() == 'vendor';
    };

   this.renderVendorFacetLabel = function(platform, count) {
        var name;
        switch (platform) {
        case 1:
            name = "VMware";
            break;
        case 2:
            name = "Hyper-V";
            break;
        default:
            name = "Unknown";
            break;
        }

        return String.format('{0} ({1})', name, count);
    };


    var showLoadingMask = function() {
        if (!grid.el.isMasked()) {
            loadingMask.show();
        }
    };

    var refreshGrid = function() {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingText
            });
        }

        grid.store.on('beforeload', function() {
            showLoadingMask();
        });


        grid.store.proxy.conn.jsonData = getJsonData();

        grid.store.baseParams = getBaseParams();

        grid.store.on('load', function() {
            loadingMask.hide();
        });

        grid.store.on('exception', function() {
            showError("@{R=VIM.Strings;K=VIMWEBJS_LV0_2;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_LV0_1;E=js}");
            loadingMask.hide();
        });

        grid.store.load();
    };

    var getJsonData = function() {

        var records = groupByList.getSelectedRecords();
        var groupByItem = '';

        if (records.length > 0)
            groupByItem = records[0].data.Value;

        return {
            groupByCategory: useGrouping ? groupByCombo.getValue() : '',
            groupByValue: useGrouping ? groupByItem : '',
            vendor: typeof (vendor) == 'undefined' ? unknownVendor : vendor
        };
    }

    var getBaseParams = function() {
        return {
            start: 0 * pageSize,
            limit: pageSize,
            search: filterText
        };
    }

    var gridSelectionChange = function(selectionModel, rowIndex, record) {
        togglePathSelection(record.data.PlatformID);
        toggleDatastorePath(true);

        selectedDatastorePlatformId = record.data.PlatformID;
        selectedDatastoreId = record.data.ID;
        selectedDatastoreName = record.data.Name;

        $container.find('#lbDefaultDatastoreName').text(record.data.ManagedObjectID);
        $container.find('#lbSpecificDatastoreName').text(record.data.ManagedObjectID);
        $container.find('#lbSelectedDatastore').text(record.data.Name);
    }

    var toggleDatastorePath = function(value) {
        $container.find('#rbDefaultPath').prop("checked", value);
        $container.find('#rbSpecificPath').prop("checked", !value);
        $container.find('#tbDatastorePath').attr("disabled", value);
    }

    var togglePathSelection = function(platformId) {
        $container.find('#pathSelection').show();

        //hyper-V
        if (platformId == 2) {
            $container.find('#pathSelectionHyperV').show();
            $container.find('#pathSelectionVmWare').hide();
        } else if (platformId == 1)
        //vmware
        {
            $container.find('#pathSelectionHyperV').hide();
            $container.find('#pathSelectionVmWare').show();
        }
    }

    var initGrid = function() {
        var selectorModel = new Ext.sw.grid.RadioSelectionModel();
        selectorModel.on("rowselect", gridSelectionChange);

        dataStore = new ORION.WebServiceStore(
            "/Orion/VIM/Services/StorageMotionTargets.asmx/GetEntities",
            dsColumnMapping,
            "Name");

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 500,
            value: pageSize
        });

        pageSizeBox.on('change', function(f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 500) {
                showError("@{R=VIM.Strings;K=VIMWEBJS_JH0_14;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_15;E=js}");
                return;
            }

            if (pagingToolbar.pageSize != pSize) {
                pagingToolbar.pageSize = pSize;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }
        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: pagingToolbarFormat,
            emptyMsg: noDataPagingToolbarText,
            items: [
                '-',
                '@{R=VIM.Strings;K=VIMWEBJS_JH0_16;E=js}',
                pageSizeBox
            ]
        });

        grid = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            store: dataStore,
            columns: [
                selectorModel,
                { header: '@{R=VIM.Strings;K=VIMWEBJS_TK0_4;E=js}', width: 130, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderEntityName },
                { header: '@{R=VIM.Strings;K=VIMWEBJS_TK0_7;E=js}', hideable: true, sortable: true, dataIndex: 'CapacityValue', renderer: renderDataStoreCapacity }
            ],
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            tbar: [
                ' ', '->',
                new Ext.ux.form.SearchField({
                    id: 'searchFieldStorageMotion',
                    store: dataStore,
                    width: 200,
                    onTrigger2Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        filterText = this.getRawValue();
                        if (filterText.length < 1) {
                            this.onTrigger1Click();
                            return;
                        }

                        var tmpfilterText = "";
                        tmpfilterText = filterText;
                        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                            tmpfilterText = "";
                        }

                        if (tmpfilterText.length > 0) {
                            if (!tmpfilterText.match("^%"))
                                tmpfilterText = "%" + tmpfilterText;

                            if (!tmpfilterText.match("%$"))
                                tmpfilterText = tmpfilterText + "%";
                        }

                        // provide search string for Id param when loading the page
                        this.store.proxy.conn.jsonData = getJsonData();

                        this.store.baseParams = getBaseParams();

                        this.store.load();

                        this.hasSearch = true;
                        this.triggers[0].show();

                        this.fireEvent('searchStarted', filterText);
                    },

                    //  this is the cancel icon button in the search field
                    onTrigger1Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        filterText = "";
                        // provide search string for Id param when loading the page
                        this.store.proxy.conn.jsonData = getJsonData();

                        this.store.baseParams = getBaseParams();

                        this.store.load();
                        this.setValue('');
                        this.hasSearch = false;
                        this.triggers[0].hide();

                        this.fireEvent('searchStarted', filterText);
                    }

                })
            ],
            bbar: pagingToolbar,
            listeners: {
                rowmousedown: function(grid, index, e) {
                    if (e.target.className == "tree-grid-expander-expand"
                        || e.target.className == "tree-grid-expander-collapse") {
                        selectorModel.lock();
                    }
                }
            }
        });
    };

    var showError = function(title, message) {
        Ext.Msg.alert(title, message).setIcon(Ext.Msg.ERROR);
    };

    var initLayout = function() {
        container = new Ext.Container({
            renderTo: 'gridSelectTargetDatastore',
            layout: 'border',
            items: [groupingPanel, grid],
            height: 300,
            width: 617
        });

        Ext.EventManager.onWindowResize(function() {
            container.doLayout();
        }, container);
    };

    var initDatastorePath = function(selectedPath) {
        if (selectedPath != null && selectedPath != '') {
            togglePathSelection(2);
            toggleDatastorePath(false);
            $container.find('#tbDatastorePath').valueOf(selectedPath);
        }

        $container.find('#rbDefaultPath').change(function() {
            var isChecked = $(this).is(':checked');
            $container.find('#tbDatastorePath').attr("disabled", isChecked);
        });

        $container.find('#rbSpecificPath').change(function() {
            var isChecked = $(this).is(':checked');
            $container.find('#tbDatastorePath').attr("disabled", !isChecked);
        });

    }

    var getSelectedDatastorePath = function() {
        if (selectedDatastorePlatformId == 2) {
            var isChecked = $container.find('#rbSpecificPath').is(':checked');
            if (isChecked) {
                var datastorePath = $container.find('#tbDatastorePath').val();
                while (datastorePath.charAt(datastorePath.length - 1) == "\\")
                    datastorePath = datastorePath.substr(0, datastorePath.length - 1);

                return datastorePath;
            }
        }
        return '';
    }

    var getGroupByList = function () {
        var data = [];
        data.push(['none', "@{R=VIM.Strings;K=VIMWEBJS_JH0_6;E=js}"]);

        if (typeof (vendor) == 'undefined' || vendor == unknownVendor)
            data.push(['vendor', "@{R=VIM.Strings;K=VIMWEBJS_JH0_7;E=js}"]);        

        return data;
    }

    //public methods
    this.init = function(config) {
        $container = $('#' + config.containerID);
        if (config.targetDatastoreId == -1) {
            selectedDatastoreId = null;
            selectedDatastoreName = null;
            selectedDatastorePlatformId = null;
        } else {
            selectedDatastoreId = config.targetDatastoreId;
            selectedDatastoreName = config.targetDatastoreName;
            selectedDatastorePlatformId = config.targetDatastorePlatform;
        }

        vendor = config.vendor;

        initGroupingPanel();
        initGrid();
        initLayout();
        refreshGrid();
        initDatastorePath(config.targetDatastorePath);
    };

    this.getSelectedDatastore = function() {
        var selectedDatastore =
        {
            datastoreId: selectedDatastoreId,
            datastoreName: selectedDatastoreName,
            datastorePath: getSelectedDatastorePath(),
            datastorePlatformId: selectedDatastorePlatformId,
        };
        return selectedDatastore;
    };

    this.validate = function() {
        if (selectedDatastoreId == null) {
            $container.find('#sw-vim-action-selectHostHelp').addClass('invalidValue');
            return false;
        } else {
            $container.find('#sw-vim-action-selectHostHelp').removeClass('invalidValue');
        }

        if (selectedDatastorePlatformId == 2) {
            var isChecked = $container.find('#rbSpecificPath').is(':checked');
           
            if (isChecked) {
                var specificPathInput = $container.find('#tbDatastorePath');
                if (specificPathInput.val().length == 0 || !windowsPathValidation(specificPathInput.val())) {
                    specificPathInput.addClass("invalidValue");
                    specificPathInput.click(function() {
                        $(this).removeClass("invalidValue");
                        $container.find('#errorMessageInvalidPath').hide();
                    });
                    return false;
                } else {
                    specificPathInput.removeClass("invalidValue");
                }
            }
        }
        return true;
    };

    function windowsPathValidation(specificPath) {
        var specificPathErrorMessage = $container.find('#errorMessageInvalidPath');
        var pattern = /^([A-Za-z_\-\s0-9\.\\\^\&\'\@\{\}\[\]\,\$\=\!\#\(\)\%\+\~]+)+$/;
        var isValidPath = pattern.test(specificPath);
        if (!isValidPath) {
            specificPathErrorMessage.show();
        } else {
            specificPathErrorMessage.hide();
        }
        return isValidPath;
    }

    this.setDisabled = function(value) {
        if (value)
            $container.find('#pathSelection').hide();
        else
            $container.find('#pathSelection').show();
        container.setDisabled(value);
    };

    this.setVendor = function (value) {
        vendor = value;
        var list = getGroupByList();
        groupByCombo.store.loadData(list);
        refreshGroupByList(selectedGroupByFacet);
        refreshGrid();
    }
}
