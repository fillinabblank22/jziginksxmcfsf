﻿var hostTargetGrid;

SW.Core.namespace("SW.VIM.Actions").VirtualMachineMotionController = function (config) {
    "use strict";

    var selectionTypeController;
    var hostGridInitialed;
    var vendor;

    function isEditable(key) {
        return !config.multiEditMode || $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    }

    var powerBatchController;

    var validateManageActionSection = function() {
        var isValid = true;
        if (isEditable(config.dataFormMapping.targetHostId)) {
            var targetHostId = getTargetHost();
            if (targetHostId == null) {
                $container.find('#sw-vim-action-selectHostHelp').addClass('invalidValue');
                isValid = isValid && false;
            } else {
                $container.find('#sw-vim-action-selectHostHelp').removeClass('invalidValue');
                isValid = isValid && true;
            }
        }
        return isValid;
    };

    var createConfiguration = function() {
        var configuration = {
            TargetHostId: getTargetHost(),
        };

        selectionTypeController.addProperties(configuration);
        return configuration;
    };

    var getTargetHost = function() {
        return hostTargetGrid.getSelectedHostId();
    };

    var createActionAndExecuteCallback = function(configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function(action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    var callActionAndExecuteCallback = function(action, param, callback) {
        SW.Core.Services.callController("/api/VIMVirtualMachineMotion/" + action, param,
            // On success
            function(response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
            // On error
            function(msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var renderDatastoreGrid = function () {
        if (hostGridInitialed)
            return;
        var timer = setInterval(function () {
            initDatastoreGrid();
            clearInterval(timer);
        }, 50);
    }

    var initDatastoreGrid = function() {
        hostTargetGrid = new SW.VIM.Actions.TargetGrid();
        hostTargetGrid.init({
            targetHostId: config.targetHostId,
            vendor: vendor
        });
        hostGridInitialed = true;
    }


    this.init = function() {
        $container = $('#' + config.containerID);

        //very dirty hack to render our grid after section is rendered active and full visible. We are waiting for implementation of CORE-638
        $('#hostSelection').click(function () {
            renderDatastoreGrid();
        });
        $('.dialogContainer').on("click", "#nextSectionInActionDefinitionBtn", function () { renderDatastoreGrid(); });

        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }

        if (config.isValid == false) {
            var validator = new SW.VIM.Actions.Validation.Validator({});
            validator.hideData();
        }
        else {
            if (config.multiEditMode) {
                for (var key in config.dataFormMapping) {
                    var dataform = config.dataFormMapping[key];
                    if (dataform == config.dataFormMapping.targetHostId) {
                        hostTargetGrid.setDisabled(true);
                        $container.find('[data-edited="' + dataform + '"]').click(function() {
                            hostTargetGrid.setDisabled(!$(this).find("input").is(':checked'));
                        });
                    }
                }
            }
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function(sectionID, callback) {
        var valid = true;
        valid = selectionTypeController.validate(sectionID);
        if (sectionID === 'manageActionSection') {
            valid = validateManageActionSection();
        }

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function(callback) {
        var jsonProperties = new Array();

        selectionTypeController.addPropertiesToArray(jsonProperties);

        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.targetHostId) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: getTargetHost() });
                }
        };
        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    }

    this.getActionDefinitionAsync = function(callback) {
        var configuration = createConfiguration();

        if (powerBatchController !== undefined)
            powerBatchController.addProperties(configuration);

        // Edit mode
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    this.setSelectionTypeController = function(controller) {
        selectionTypeController = controller;
    };

    this.setPowerBatchController = function (controller) {
        powerBatchController = controller;
    }

    this.setVendor = function (vendorType) {
        if (hostGridInitialed) {
            hostTargetGrid.setVendor(vendorType);
        }

        vendor = vendorType;
    }

    var self = this;
    var $container = null;
}
