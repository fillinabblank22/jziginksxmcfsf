SW.Core.namespace("SW.VIM.Actions").PowerControlBatchController = function () {
    var multiEditMode;
    var $container = null;
    var checkbox;

    this.init = function (config) {
            multiEditMode = config.multiEditMode;          
            $container = $('#' + config.containerID);
            checkbox = $container.find('#powerControlCheckbox');
            checkbox.prop("checked", config.powerControlEnabled == "true");
        },

        this.addProperties = function (configuration) {            
            configuration.PowerControlBatchEnabled = checkbox.prop("checked");
        };

        this.addPropertiesToArray = function (jsonProperties) {
            if ($container.find('#PowerControlBatchEnabled').prop("checked")) {
                jsonProperties.push({ PropertyName: 'PowerControlBatchEnabled', PropertyValue: checkbox.prop("checked") });
            }
        };
    }
