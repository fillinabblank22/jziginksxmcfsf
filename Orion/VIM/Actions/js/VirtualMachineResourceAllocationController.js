SW.Core.namespace("SW.VIM.Actions").VirtualMachineResourceAllocationController = function (config) {
    "use strict";

    var powerBatchController;
    var selectionTypeController = null;

    var modificationTypeSpecific = "Specific";
    var modificationTypeIncrease = "Increase";

    var memoryComboBoxCustom = "Custom";
    var memoryComboBoxNone = "None";

    // Private methods
        
    var validateManageActionSection = function () {
        var isValid = inputValidator.validate();
        //Validation if at least one value change is selected
        var checked = $container.find('#sw-vim-action-allocation-formWrapper').find('input:checked');
        if (checked.length == 0) {
            $container.find('#sw-vim-sprawlinfotext').addClass('invalidValue');
            isValid = false;
        } else {
            $container.find('#sw-vim-sprawlinfotext').removeClass('invalidValue');
        }

        //check if some value is selected from memory combo box
        if (isEditable(config.dataFormMapping.memory)) {
            var val = getMemoryComboBoxSelectedVal();
            if (val == memoryComboBoxNone) {
                $container.find("#dpMemoryAmount").addClass('invalidValue');
                isValid = false;
            } else {
                $container.find("#dpMemoryAmount").removeClass('invalidValue');
                if (val == memoryComboBoxCustom) {
                    var memoryTextBox = getDataForm(config.dataFormMapping.memory);
                    if (memoryTextBox.val() == '') {
                        memoryTextBox.addClass('invalidValue');
                        isValid = false;
                    } else {
                        memoryTextBox.removeClass('invalidValue');
                    }
                }
            }
        }

        //check if any value is entered in number of processors field
        if (isEditable(config.dataFormMapping.numberOfProcessors)) {
            var processorTextBox = getDataForm(config.dataFormMapping.numberOfProcessors);
            if (processorTextBox.val() == '') {
                processorTextBox.addClass('invalidValue');
                isValid = false;
            } else {
                processorTextBox.removeClass('invalidValue');
            }
        }

        return isValid;
    };

    var createActionAndExecuteCallback = function (configuration, callback) {
        callActionAndExecuteCallback("Create", configuration, callback);
    };

    var updateActionAndExecuteCallback = function (action, configuration, callback) {
        configuration.Action = action;
        callActionAndExecuteCallback("Update", configuration, callback);
    };

    function isEditable(key) {
        return $container.find('[data-edited="' + key + '"] input:checked').length == 1;
    };

    var callActionAndExecuteCallback = function (action, param, callback) {        
        SW.Core.Services.callController("/api/VIMVirtualMachineResourceAllocation/" + action, param,
        // On success
            function (response) {
                if ($.isFunction(callback)) {
                    callback({ isError: false, actionDefinition: response });
                }
            },
        // On error
            function (msg) {
                if ($.isFunction(callback)) {
                    callback({ isError: true, ErrorMessage: msg });
                }
            }
        );
    };

    var initControlsHandlers = function() {
        $container.find(":radio").on("change", function () {
            toggleRadioButtons($(this).val());
        });

        $container.find("#dpMemoryAmount").on("change", function () {
            toggleMemoryComboBox($(this));
        });
    };

    var toggleRadioButtons = function(val) {
        if (val == modificationTypeSpecific) {
            $container.find('.sw-vim-action-allocation-add').hide();
        } else if (val == modificationTypeIncrease) {
            $container.find('.sw-vim-action-allocation-add').show();
        }
    }

    var toggleMemoryComboBox = function (comboBox) {
        var val = comboBox.val();
        toggleCustomTextBox(val == memoryComboBoxCustom);
    }

    var toggleCustomTextBox = function(value) {
        if (value) {
            $container.find('.sw-vim-action-acllocation-memory-custom').show();
        } else {
            $container.find('.sw-vim-action-acllocation-memory-custom').hide();
        }
    }

    var getModificationTypeVal = function() {
        var selectedInput = $container.find('#sw-vim-action-allocation-modification-type').find('input:checked');
        return selectedInput.val();
    }

    // Public methods
    //method is called on the initializing of our custom alert action control (creating or editing)
    this.init = function() {
        $container = $('#' + config.containerID);
        initControlsHandlers();
        toggleCustomTextBox(getMemoryComboBoxSelectedVal() == memoryComboBoxCustom);
        toggleRadioButtons(getModificationTypeVal());


        if ($.isFunction(config.onReadyCallback)) {
            config.onReadyCallback(self);
        }
        
        if (config.isValid == false) {
            var validator = new SW.VIM.Actions.Validation.Validator({});
            validator.hideData();
        } else {
            inputValidator = new SW.VIM.Actions.Validation.ManagementActionValidator({
                containerID: config.containerID
            });

            inputValidator.init();

            for (var key in config.dataFormMapping) {
                var dataformId = config.dataFormMapping[key];
                var dataform = $container.find('[data-form="' + dataformId + '"]');
                var dataedited = $container.find('[data-edited="' + dataformId + '"]');
                dataform.attr("disabled", !dataedited.find('input').is(':checked'));

                $container.find('[data-edited="' + dataformId + '"]').click(function() {
                    dataedited = $(this).attr("data-edited");
                    $container.find('[data-form="' + dataedited + '"]').attr("disabled", !$(this).find("input").is(':checked'));
                });
            }
        }
    };

    // Validate action configuration. 
    this.validateSectionAsync = function (sectionID, callback) {
        var valid = true;
        valid = selectionTypeController.validate(sectionID);
        if (sectionID === 'sw-vim-manageActionSection') {
            valid = valid & validateManageActionSection();
        }

        if ($.isFunction(callback)) {
            callback(valid);
        }
    };

    this.getUpdatedActionProperies = function (callback) {
        var jsonProperties = getPropertyValues();

        if (callback) {
            callback(JSON.stringify(jsonProperties));
        }
    };

    var getPropertyValues = function() {
        var jsonProperties = new Array();

        selectionTypeController.addPropertiesToArray(jsonProperties);
        for (var key in config.dataFormMapping) {
            var dataform = config.dataFormMapping[key];
            if ($container.find('[data-edited="' + dataform + '"] input:checked').length == 1)
                if (dataform == config.dataFormMapping.memory) {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: getMemoryValue() });
                } else {
                    jsonProperties.push({ PropertyName: dataform, PropertyValue: $container.find('[data-form="' + dataform + '"]').val() });
                }
        }

        if (jsonProperties.length > 0)
            jsonProperties.push({ PropertyName: 'ModificationType', PropertyValue: getModificationTypeVal() });

        return jsonProperties;
    };

    var getMemoryValue = function () {
        var val = getMemoryComboBoxSelectedVal();
        if (val == memoryComboBoxCustom) {
            val = getDataForm(config.dataFormMapping.memory).val();
        }
        return val;
    }

    var getMemoryComboBoxSelectedVal = function() {
        return $container.find("#dpMemoryAmount").find(":selected").val();
    }

    var getDataForm = function(dataFormMapping) {
        return $container.find('[data-form="' + dataFormMapping + '"][type="text"]');
    }

    //method called after clicking on button add or save changes
    this.getActionDefinitionAsync = function(callback) {
        var jsonProperties = getPropertyValues();
        
        var configuration = {
            Memory: "",
            NumberOfProcessors: "",
            ModificationType: ""
        };

        if (powerBatchController !== undefined)
            powerBatchController.addProperties(configuration);

        selectionTypeController.addProperties(configuration);

        for (var i = 0 ; i < jsonProperties.length; i++) {
            configuration[jsonProperties[i].PropertyName] = jsonProperties[i].PropertyValue;
        }

        // Edit mode
        if (config.actionDefinition) {

            // We have action definition we just want to updated existing one.
            updateActionAndExecuteCallback(config.actionDefinition, configuration, callback);

        } else {

            // New action definition mode.
            createActionAndExecuteCallback(configuration, callback);
        }
    };

    this.setPowerBatchController = function (controller) {
        powerBatchController = controller;
    }

    this.setSelectionTypeController = function (controller) {
        selectionTypeController = controller;
    }

    // Constructor

    var self = this;
    var $container = null;
    var inputValidator = null;
}
