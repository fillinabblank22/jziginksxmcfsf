﻿SW.Core.namespace("SW.VIM.Actions.Validation").Validator = function () {
    "use strict";
    
    this.hideData = function() {
        $("#createActionDefinitionBtn").hide();
        $("#nextSectionInActionDefinitionBtn").hide();
        $("#addActionDefinitionBtn").hide();
        $('#_basicActionPlugins_').remove();
        $('#_topActionPlugins_').remove();
        $('#_timeOfDayActionPlugins_').remove();
        $('#cancelActionDefinitionBtn span:first-child span:first-child').text("@{R=VIM.Strings;K=VIMWEBJS_LV0_9;E=js}");
    };
}