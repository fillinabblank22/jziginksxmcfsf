﻿var vimSpecificGrid;

SW.Core.namespace("SW.VIM.Actions").SelectSpecificVMGrid = function () {
    "use strict";

    var swisVirtualMachineEntity = 'Orion.VIM.VirtualMachines';
    var swisHostEntity = 'Orion.VIM.Hosts';
    var pollingSourceVMan = 1;

    var grid;
    var dataStore;
    var vendor;
    var unknownVendor = 0;
    var dsColumnMapping = [
                { name: 'ID', mapping: 0 },
                { name: 'Name', mapping: 1 },
                { name: 'IPAddress', mapping: 2 },
                { name: 'Status', mapping: 3 },
                { name: 'InstanceType', mapping: 4 },
                { name: 'PlatformID', mapping: 5 },
                { name: 'ChildsCount', mapping: 6 },
                { name: 'Level', mapping: 7 },
                { name: 'NodeID', mapping: 8 },
                { name: 'PollingSource', mapping: 9 }

    ];

    // grid paging    
    var pagingToolbar;
    var pageSizeBox;
    var pageSize = 40;
    var pagingToolbarFormat = "@{R=VIM.Strings;K=VIMWEBJS_TK0_3;E=js}";
    var noDataPagingToolbarText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_3;E=js}";

    var loadingMask;

    var loadingText = "@{R=VIM.Strings;K=VIMWEBJS_JH0_1;E=js}";

    // grouping
    var groupByCombo;
    var groupByStore;
    var groupingPanel;
    var groupByList;
    var useGrouping = false;
    var filterText;
    var selectedVirtualMachineId = null;
    var container;
    var $container = null;


    var renderEntityName = function (value, meta, record) {
        return String.format('<span>{0}</span>', getEntityLabel(record));
    };

    var getEntityLabel = function (record) {
        var netObject = '';

        var link = '/Orion/View.aspx';
        var ipAddress = '';

        switch (record.data.InstanceType) {
            case swisVirtualMachineEntity:
                if (record.data.NodeID == null && record.data.PollingSource == pollingSourceVMan) {
                    netObject = 'VVM:' + record.data.ID;
                    link = '/Orion/VIM/VMDetails.aspx';
                }
                else if (record.data.NodeID != null) {
                    netObject = 'N:' + record.data.NodeID;
                }
                else {
                    return String.format('<span class="sw-vim-assetTree-unmanagedEntityLabel">' +
                        '<a href="javascript:void(0);">{0} {1}</a></span>', getStatusIcon(record), record.data.Name);
                }
                break;
        }

        return String.format('<a href="{0}?NetObject={1}" target="_blank">{2} {3}</a>', link, netObject, getStatusIcon(record), record.data.Name);
    };


    var getStatusIcon = function (record) {
        var entityName;
        switch (record.data.InstanceType) {
            case swisVirtualMachineEntity:
            case swisHostEntity:
                entityName = String.format('{0}.{1}', record.data.InstanceType, toPlatformName(record.data.PlatformID));
                break;
            default:
                entityName = record.data.InstanceType;
                break;
        }
        return String.format('<img src="/Orion/StatusIcon.ashx?size=small&entity={0}&status={1}" />', entityName, record.data.Status);
    };

    var toPlatformName = function (platformId) {
        switch (platformId) {
            case 1:
                return 'Vmware';
            case 2:
                return 'HyperV';
            default:
                return '';
        }
    };


    var refreshGroupByList = function (groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory, vendor: vendor };
        if (handler) {
            var internalHandler = function () {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }
        groupByStore.load();
    };

    var InitGroupingPanel = function () {
       
        groupByCombo = new Ext.form.ComboBox({
            store: new Ext.data.ArrayStore({
                fields: ['itemValue', 'itemLabel'],
                data: [
                    ['none', "@{R=VIM.Strings;K=VIMWEBJS_JH0_6;E=js}"],
                    ['host', "@{R=VIM.Strings;K=VIMWEBJS_LM0_1;E=js}"],
                    ['cluster', "@{R=VIM.Strings;K=VIMWEBJS_LM0_2;E=js}"]
                ]
            }),
            valueField: 'itemValue',
            displayField: 'itemLabel',
            triggerAction: 'all',
            mode: 'local',
            editable: false,
            width: 170,
            margins: '2 0 0 0',
            listeners: {
                select: function (record, index) {
                    refreshGroupByList(record.value);
                    if (record.value == 'none') {
                        useGrouping = false;
                        refreshGrid();
                    }                
                }
            }
        });

        groupByCombo.setValue('none');

        var selectPanel = new Ext.Container({
            region: 'north',
            height: 51,
            layout: 'vbox',
            cls: 'sw-vim-form-groupByPanel',
            items: [groupByCombo]
        });

        groupByStore = new ORION.WebServiceStore("/Orion/VIM/Services/AlertManagementSelectSpecificVM.asmx/GetEntitiesFacets",
            [
                { name: "Value", mapping: 0 },
                { name: "ValueCount", mapping: 1 },
                { name: "Name", mapping: 2 },
                { name: "InstanceType", mapping: 3 },
                { name: "Status", mapping: 4 },
                { name: "Platform", mapping: 5 }
            ], "Value");

        var template = new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="sw-vim-form-groupByItem">',
                '<tpl if="vimSpecificGrid.IsHostGrouping() == true">',
                    ' {[vimSpecificGrid.renderFacetLabel(values)]}',
                '</tpl>',
                '<tpl if="vimSpecificGrid.IsClusterGrouping() == true">',
                    ' {[vimSpecificGrid.renderFacetLabel(values)]}',
                '</tpl>',
                '</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        );

        groupByList = new Ext.DataView({
            border: true,
            store: groupByStore,
            singleSelect: true,
            itemSelector: "div.sw-vim-form-groupByItem",
            tpl: template,
            autoHeight: false,
            autoScroll: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0) {
                        useGrouping = true;
                        refreshGrid();
                    }
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 180,
            split: true,
            items: [
                selectPanel,
                groupByList
            ],
            collapsible: true,
            title: '@{R=VIM.Strings;K=VIMWEBJS_TK0_6;E=js}',
            cls: 'x-panel-bwrap'
        });

        groupingPanel.on('bodyresize', function () {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    this.IsHostGrouping = function () {
        return groupByCombo.getValue() == 'host';
    };

    this.IsClusterGrouping = function () {
        return groupByCombo.getValue() == 'cluster';
    };

    this.renderFacetLabel = function (data) {
        if (data.Name == null) {
            data.Value = 'null';
            data.Name = "@{R=VIM.Strings;K=VIMWEBJS_TK0_2;E=js}";
        }
        var str = String.format('{0} ({1})', renderFacetName(data), data.ValueCount);
        return str;
    };

    var renderFacetName = function (data) {
        var record = {
            data: {
                InstanceType: data.InstanceType,
                Status: data.Status,
                PlatformID: data.Platform
            },
        };
        var str = String.format('{0} {1}', getStatusIcon(record), data.Name);
        return str;
    };


    var showLoadingMask = function () {
        if (!grid.el.isMasked()) {
            loadingMask.show();
        }
    };

    var refreshGrid = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: loadingText
            });
        }

        grid.store.on('beforeload', function () {
            showLoadingMask();
        });


        grid.store.proxy.conn.jsonData = getSpecificJsonData();

        grid.store.baseParams = getSpecificBaseParams();

        grid.store.on('load', function () {
            loadingMask.hide();
        });

        grid.store.on('exception', function () {
            showError("@{R=VIM.Strings;K=VIMWEBJS_LV0_2;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_LV0_1;E=js}");
            loadingMask.hide();
        });

        grid.store.load();
    };

    var getSpecificJsonData = function () {
    
        var records = groupByList.getSelectedRecords();
        var groupByItem = '';
        if (records.length > 0)
            groupByItem = records[0].data.Value;

        return {
            groupByCategory: useGrouping ? groupByCombo.getValue() : '',
            groupByValue: useGrouping ? groupByItem : '',
            vendor: typeof (vendor) == 'undefined' ? unknownVendor : vendor
        };
    };

    var getSpecificBaseParams = function () {
        return {
            start: 0 * pageSize,
            limit: pageSize,
            search: filterText
        };
    }

    var gridSelectionChange = function (selectionModel, rowIndex, record) {
        selectedVirtualMachineId = record.data.ID;
        $container.find('#selectedVirtualMachineId').val(record.data.ID);
        $('#lbSelectedVM').text(record.data.Name);
    }


    var initGrid = function () {
        var selectorModel = new Ext.sw.grid.RadioSelectionModel();
        selectorModel.on("rowselect", gridSelectionChange);
        
        dataStore = new ORION.WebServiceStore(
            "/Orion/VIM/Services/AlertManagementSelectSpecificVM.asmx/GetEntities",
            dsColumnMapping,
            "Name");

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 500,
            value: pageSize
        });

        pageSizeBox.on('change', function (f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 500) {
                showError("@{R=VIM.Strings;K=VIMWEBJS_JH0_14;E=js}", "@{R=VIM.Strings;K=VIMWEBJS_JH0_15;E=js}");
                return;
            }

            if (pagingToolbar.pageSize != pSize) {
                pagingToolbar.pageSize = pSize;
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }
        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: pageSize,
            displayInfo: true,
            displayMsg: pagingToolbarFormat,
            emptyMsg: noDataPagingToolbarText,
            items: [
                '-',
                '@{R=VIM.Strings;K=VIMWEBJS_JH0_16;E=js}',
                pageSizeBox
            ]
        });

        grid = new Ext.grid.GridPanel({
            viewConfig: {
                forceFit: true
            },
            store: dataStore,
            columns: [
                selectorModel,
                { header: '@{R=VIM.Strings;K=VIMWEBJS_TK0_4;E=js}', width: 246, hideable: false, sortable: true, dataIndex: 'Name', renderer: renderEntityName },
                { header: '@{R=VIM.Strings;K=VIMWEBJS_TK0_5;E=js}', width: 130, hideable: true, sortable: true, dataIndex: 'IPAddress' },
                { id: 'filler', hidden: false, hideable: false, width: 0, sortable: false, menuDisabled: true, dataIndex: -1 }
            ],
            sm: selectorModel,
            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            tbar: [
                ' ', '->',
                new Ext.ux.form.SearchField({
                    id: 'searchFieldSpecificVM',
                    store: dataStore,
                    width: 200,
                    onTrigger2Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        filterText = this.getRawValue();
                        if (filterText.length < 1) {
                            this.onTrigger1Click();
                            return;
                        }

                        var tmpfilterText = "";
                        tmpfilterText = filterText;
                        if (tmpfilterText == "@{R=Core.Strings;K=WEBJS_TM0_53;E=js}") {
                            tmpfilterText = "";
                        }

                        if (tmpfilterText.length > 0) {
                            if (!tmpfilterText.match("^%"))
                                tmpfilterText = "%" + tmpfilterText;

                            if (!tmpfilterText.match("%$"))
                                tmpfilterText = tmpfilterText + "%";
                        }

                        // provide search string for Id param when loading the page
                        this.store.proxy.conn.jsonData = getSpecificJsonData();

                        this.store.baseParams = getSpecificBaseParams();

                        this.store.load();

                        this.hasSearch = true;
                        this.triggers[0].show();

                        this.fireEvent('searchStarted', filterText);
                    },

                    //  this is the cancel icon button in the search field
                    onTrigger1Click: function () {
                        //set the global variable for the SQL query and regex highlighting
                        filterText = "";
                        // provide search string for Id param when loading the page
                        this.store.proxy.conn.jsonData = getSpecificJsonData();

                        this.store.baseParams = getSpecificBaseParams();

                        this.store.load();
                        this.setValue('');
                        this.hasSearch = false;
                        this.triggers[0].hide();

                        this.fireEvent('searchStarted', filterText);
                    }
                })],
            bbar: pagingToolbar,
            listeners: {
                rowmousedown: function (grid, index, e) {
                    if (e.target.className == "tree-grid-expander-expand"
                        || e.target.className == "tree-grid-expander-collapse") {
                        selectorModel.lock();
                    }
                }
            }
        });
    };


    var showError = function (title, message) {
        Ext.Msg.alert(title, message).setIcon(Ext.Msg.ERROR);
    };

    var initLayout = function () {
        container = new Ext.Container({
            renderTo: 'selectionGrid',
            layout: 'border',
            items: [groupingPanel, grid],
            height: 300,
            width: 617
        });

        Ext.EventManager.onWindowResize(function () {
            container.doLayout();
        }, container);
    };

    //public methods
    this.init = function (config) {
        vimSpecificGrid = this;
        $container = config.container;
        vendor = config.vendor;

        InitGroupingPanel();
        initGrid();
        initLayout();
        refreshGrid();
    };

    this.setDisabled = function(value) {
        if (container != null)
            container.setDisabled(value);
    };
}
