﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualMachineStorageMotion.ascx.cs" Inherits="VirtualMachineStorageMotionView" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register TagPrefix="vim" TagName="SelectionType" Src="~/Orion/VIM/Actions/Controls/SelectionType.ascx" %>
<%@ Register TagPrefix="vim" TagName="PowerControlBatch" Src="~/Orion/VIM/Actions/Controls/PowerControlBatch.ascx" %>
<%@ Register TagPrefix="vim" TagName="ValidationWrapper" Src="~/Orion/VIM/Actions/Controls/ValidationWrapper.ascx" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Import Namespace="SolarWinds.VIM.Base.Contract.Constants" %>

<orion:IncludeExtJs runat="server" Debug="False" Version="3.4" />
<orion:Include ID="Include2" runat="server" File="OrionCore.js" />
<orion:Include ID="Include3" runat="server" File="VIM/Actions/js/VirtualMachineStorageMotionTargetGrid.js" />
<orion:Include ID="Include1" runat="server" File="VIM/Actions/js/VirtualMachineStorageMotionController.js" />
<orion:include id="Include4" runat="server" file="VIM/Actions/js/ValidationsWrapper.js" />

<style> 
    .manageVMAction .sw-vim-action-selectedDatastore{
        margin: 0 10px 0 10px;
    }

    .manageVMAction #targetDatastorePath .sw-vim-action-pathPattern{
        font-size: 90%;
        color: gray;
    }

    .manageVMAction #targetDatastorePath .sw-vim-action-pathPatternInput{
        padding: 3px 0 3px 2px;
        width: 95%;
    }

    .suggestionText{
        margin: 10px;
    }

    .sw-vim-selection-path {
        background-color: #ecf6fb;
        float: left; 
        width: 100%;
    }

    .sw-vim-action-storageMotion-capacityBar {
        width:100px; 
        float:right; 
        margin-right: 10px;
    }

    .sw-vim-action-storageMotion-capacityBar_fill {
        background-color:#006CA9; 
        font-size:1px; 
        height:10px;
    }

    .sw-vim-action-storageMotion-capacityValue {
        float: left;
        color: #666666;
    }

    .manageVMAction .sw-vim-selection-path-table {
        margin: 5px 5px 5px 5px; 
        width: 98%;
    }

    .manageVMAction .sw-vim-selection-path-table tbody tr {
        margin: 20px; 
    }

    .manageVMAction .sw-vim-selection-path-table tbody tr td {
        text-overflow: ellipsis; 
        overflow: hidden;
        white-space: nowrap;
        max-width: 150px;
    } 

    #sw-vim-action-selectHostHelp{
        display: inline-block;
        margin: 0 0 5px 0;
    }

    #gridSelectTargetDatastore{
        margin: 3px 0 5px 0;
    }

    #errorMessageInvalidPath td{
        white-space: normal;
    }
</style>

<script type="text/javascript">

    var storageMotionController;
    (function() {

        storageMotionController = new SW.VIM.Actions.VirtualMachineStorageMotionController({
            
            containerID : '<%= container.ClientID %>', 
            onReadyCallback : <%= OnReadyJsCallback %>,
            actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
            viewContext :  <%= ToJson(ViewContext, true) %>,
            multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
            targetDatastoreId: '<%= TargetDatastore.ID %>',
            targetDatastorePlatform: '<%= TargetDatastore.Platform %>',
            targetDatastoreName: '<%= TargetDatastore.Name %>',
            targetDatastorePath: '<%= TargetDatastorePath %>',
            isValid: <%= validationWrapper.IsValid.ToString().ToLower() %>,
            dataFormMapping : {
                targetDatastoreId: "<%= ManagementActionConstants.StorageMotion.TargetDatastoreIdPropertyKey %>",
                targetDatastorePath: "<%= ManagementActionConstants.StorageMotion.TargetDatastorePathPropertyKey %>",
                targetDatastoreName: "<%= ManagementActionConstants.StorageMotion.TargetDatastoreNamePropertyKey %>"
            }
        });

        storageMotionController.init();
    })();
    
</script>

<vim:ValidationWrapper runat="server" id="validationWrapper" ActionName="<%$ Code: ManagementEnums.ActionName.PerformRelocation %>">
    <Content>
        <div runat="server" id="container" class="manageVMAction">
        
            <h3><asp:Label runat="server" ID="lbHeaderText"></asp:Label><asp:Label runat="server" ID="lbSelectedVM" ClientIDMode="Static" CssClass="sw-vim-action-selectedVM"></asp:Label></h3>
            <div class="section required" runat="server" ID="selectionWrapper" ClientIDMode="Static">
                <vim:SelectionType runat="server" ID="SelectionType"></vim:SelectionType>        
                 <vim:ValidationWrapper runat="server" id="powerPermissionValidationWrapper" ErrorMessageVisibility="False" ActionName="<%$ Code: ManagementEnums.ActionName.PowerOff %>">
                     <Content>
                         <vim:PowerControlBatch runat="server" ID="PowerBatchControl" PowerControlLabel="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_52 %>" />
                     </Content>
                 </vim:ValidationWrapper> 
            </div>
        
            <h3 id="datastoreSelection"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_13 %> <asp:Label runat="server" ID="lbSelectedDatastore" ClientIDMode="Static" CssClass="sw-vim-action-selectedDatastore"></asp:Label></h3>
    
            <div id="manageActionSection" class="section required">
                <div class="sw-vim-standard-resizable-wrapper">               
                     <% if (MultiEditEnabled)
                               { %>
                                    <asp:CheckBox ID="chbxSelectTargetDatastore" runat="server" data-edited ="<%$ Code: ManagementActionConstants.StorageMotion.TargetDatastoreIdPropertyKey %>"/>
                            <% }%>

                    <span id="sw-vim-action-selectHostHelp"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_14 %></span> 
                                                               
                        <div id="gridSelectTargetDatastore" class="sw-vim-wizard-extjs-grid"></div>
                        <div id="targetDatastorePath">
                            <div id="pathSelection" class="sw-vim-selection-path">
                                <div id="pathSelectionHyperV" style="display: none;">
                                    <table class="sw-vim-selection-path-table">
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rbDefaultPath" Checked="True" ClientIDMode="Static" GroupName="PathMode" runat="server"/>
                                                <label><%= Resources.VIMWebContent.VIMWEBDATA_TK0_18 %></label>  
                                            </td>
                                            <td style="text-align: right; max-width: 200px;"><asp:Label runat="server" ID="lbDefaultDatastoreName" ClientIDMode="Static"></asp:Label><span>\</span></td>
                                            <td><label id="tbDefaultDatastorePath" runat="server"></label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rbSpecificPath" ClientIDMode="Static" GroupName="PathMode" runat="server"/>
                                                <label><%= Resources.VIMWebContent.VIMWEBDATA_TK0_15 %></label> 
                                            </td>
                                            <td style="text-align: right; max-width: 200px;"><asp:Label runat="server" ID="lbSpecificDatastoreName" ClientIDMode="Static"></asp:Label><span>\</span></td>
                                            <td><asp:TextBox ID="tbDatastorePath"  CssClass="sw-vim-action-pathPatternInput" ClientIDMode="Static" runat="server" MaxLength="400"></asp:TextBox></td>
                                        </tr>
                                        <tr style="display: none" id="errorMessageInvalidPath">
                                            <td colspan="3">
                                                <div class="sw-suggestion suggestionText">
                                                    <span class="sw-suggestion-icon"></span>
                                                    <span id="sw-vim-message"><%= Resources.VIMWebContent.VIMWEBDATA_RB0_10 %></span> 
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="pathSelectionVmWare">
                                    <div class="sw-suggestion suggestionText">
                                        <span class="sw-suggestion-icon"></span>
                                         <asp:Label runat="server"><b><%= Resources.VIMWebContent.VIMWEBDATA_RB0_8 %></b></asp:Label>
                                        <br/>
                                        <asp:Label runat="server"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_17 %></asp:Label>
                                        <br/>
                                        <a href="<%=SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl(GeneralConstants.VmanModuleShortName, "OrionCoreAG-AlertsTriggerActionStorageMotion") %>" target="_blank"> &raquo; <%=  Resources.VIMWebContent.VIMWEBDATA_RB0_9%></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

            </div>
        </div>
    </Content>
</vim:ValidationWrapper>

