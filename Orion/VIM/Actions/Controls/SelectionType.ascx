﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectionType.ascx.cs" Inherits="Orion_VIM_Actions_Controls_SelectionType" %>

<orion:Include ID="Include1" runat="server" File="VIM/Actions/js/SelectionType.js" />
<orion:Include ID="Include2" runat="server" File="VIM/Actions/js/SelectSpecificVMGrid.js" />

<style> 
    .manageVMAction .sw-vim-action-selectedVM{
        margin: 0 10px 0 10px;
    }
    .sw-vim-aciton-vmselection-vendorWrapper {
        margin: 5px;
        padding: 5px;
        width: 90%;
        border: 1px solid silver;
    }

    #sw-vim-action-vmselection-vendorWrapper {
        margin: 15px 0px 10px 0px;
        padding: 8px;
        width: 582px;
        border: 1px solid silver;
    }
    
    .sw-vim-action-vmselection-vendorWrapper-helpText {
        margin-left: 15px;
    }

    .sw-vim-action-vmselection-vendorWrapper-chbx {
        font-weight: bold;
    }

    #sw-vim-helpLabelForVendor {
        max-width: 600px;
    }

    .suggestionText{
        margin: 10px;
    }

</style>

    <div runat="server" ID="selectionContainer" ClientIDMode="Static">
        <div>
            
            <% if (MultiEditMode)
                { %>
                    <asp:CheckBox ID="chbxSelection" runat="server" ClientIDMode="Static"/>
                <% }%>                 
            <span id="selectionValidationErrorMessage" class="invalidValue" style="display: none"></span>
                   
            <div runat="server" ID="selectionType">
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton runat="server" Value="Context" GroupName="selectionType" ID="selectionTypeContext"></asp:RadioButton>
                            <label><%= Resources.VIMWebContent.VIMWEBDATA_TK0_20 %></label> 
                       </td>
                    </tr>
                    <tr>
                        <td>
                             <div runat="server" ID="helpLabelVendorWrapper">
                                 <div id="sw-vim-helpLabelForVendor">
                                     <div class="sw-suggestion suggestionText">
                                         <span class="sw-suggestion-icon"></span>
                                         <asp:Label ID="helpLabelForVendor" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div runat="server" ID="vendorWrapper">
                                <div id="sw-vim-action-vmselection-vendorWrapper">
                                    <span class="sw-vim-action-vmselection-vendorWrapper-chbx">
                                        <asp:CheckBox runat="server" ID="chxbSelectionTypeContextVendor" ClientIDMode="Static"/>
                                        <label><%= Resources.VIMWebContent.VIMWEBDATA_TK0_33 %></label> 
                                    </span>
                                    <span><asp:DropDownList runat="server" ID="dplSelectionTypeContextVendor" ClientIDMode="Static"/></span>
                                    <div class="sw-vim-action-vmselection-vendorWrapper-helpText"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_32 %></div>
                                </div>
                            </div>
                        <td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton runat="server" Value="Specific" GroupName="selectionType" ID="selectionTypeSpecific"></asp:RadioButton>
                             <label><%= Resources.VIMWebContent.VIMWEBDATA_TK0_21 %></label> 
                        </td>
                    </tr>
                </table>
            </div>  
        </div>
        
        
        <div id="selectionGridWrapper">
            <div id="selectionGrid"></div>    
        </div>
        
        <div id="selectionContextWrapper">
        </div>
        
        <asp:HiddenField runat="server" ID="selectedVirtualMachineId" ClientIDMode="Static"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="selectedOption" ClientIDMode="Static"></asp:HiddenField>

    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            var selectionTypeController = new SW.VIM.Actions.SelectionTypeController({
                containerID: '<%= selectionContainer.ClientID %>',
                multiEditMode: '<%= MultiEditMode %>',
                selectedVirtualMachineId: '<%= SelectedVirtualMachineId %>',
                selectedVirtualMachineName: '<%= SelectedVirtualMachineName %>',
                selectionType: '<%= SelectionType.ToString() %>',
                mainController: '<%= ControllerName %>',
                vendor: '<%= Vendor %>'
            });

            selectionTypeController.init();
            <%= ControllerName %>.setSelectionTypeController(selectionTypeController);

        });
    
</script>