﻿using System;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.VIM.Actions.VmManagement;
using SolarWinds.VIM.Actions.VmManagement.StorageMotion;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Web.DAL.VMan;
using SolarWinds.VIM.Web.Models;

public partial class VirtualMachineStorageMotionView : VirtualMachineManagementControl
{
    private DataStoreModel _targetDatastore;
    private string _targetDatastorePath;
 
    public DataStoreModel TargetDatastore
    {
        get { return _targetDatastore; }
    }

    public string TargetDatastorePath
    {
        get { return _targetDatastorePath; }
    }

    public override string ActionTypeID
    {
        get { return VirtualMachineStorageMotionPlugin.ActionTypeId; }
    }

    protected override void SetDefaultValues()
    {
        _targetDatastore = new DataStoreModel();
        _targetDatastore.ID = -1;

        var vimSettings = new VimSettings();
        tbDefaultDatastorePath.InnerText = vimSettings.DefaultHyperVRelocationPath + "\\" + Resources.VIMWebContent.VIMWEBDATA_TK0_16;
        tbDatastorePath.Text = vimSettings.DefaultHyperVRelocationPath + "\\";
    }

    protected override void BindConfiguration()
    {
        if (MultiEditEnabled)
        {
            SetDefaultValues();
            return;
        }

        ActionProperty actionProperty = ActionDefinition.Properties[ManagementActionConstants.StorageMotion.TargetDatastoreIdPropertyKey];
        if (actionProperty != null)
        {
            var targetDatastoreId = Convert.ToInt32(actionProperty.PropertyValue);
            _targetDatastore = new DatastoreDAL().GetDataStoreModel(targetDatastoreId);
            if (_targetDatastore != null)
            {
                lbSelectedDatastore.Text = _targetDatastore.Name;
                lbDefaultDatastoreName.Text = _targetDatastore.ManagedObjectID;
                lbSpecificDatastoreName.Text = _targetDatastore.ManagedObjectID;      
            }
        }
        actionProperty = ActionDefinition.Properties[ManagementActionConstants.StorageMotion.TargetDatastorePathPropertyKey];
        if (actionProperty != null)
        {
            _targetDatastorePath = actionProperty.PropertyValue;
            tbDatastorePath.Text = actionProperty.PropertyValue;
        }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, Resources.VIMWebContent.VIMWEBDATA_JD0_34);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "storageMotionController";
        SelectionType.RenderVendorComboBox = true;

        PowerBatchControl.MultiEditMode = MultiEditEnabled;
        PowerBatchControl.ActionDefinition = ActionDefinition;
        PowerBatchControl.MainControllerName = "storageMotionController";
    }
}