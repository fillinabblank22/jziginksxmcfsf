﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualMachineMotion.ascx.cs" Inherits="VirtualMachineMotionView" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement.Motion" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>
<%@ Register TagPrefix="vim" TagName="SelectionType" Src="~/Orion/VIM/Actions/Controls/SelectionType.ascx" %>
<%@ Register TagPrefix="vim" TagName="PowerControlBatch" Src="~/Orion/VIM/Actions/Controls/PowerControlBatch.ascx" %>
<%@ Register TagPrefix="vim" TagName="ValidationWrapper" Src="~/Orion/VIM/Actions/Controls/ValidationWrapper.ascx" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>

<orion:includeextjs runat="server" debug="False" version="3.4" />
<orion:include id="Include2" runat="server" file="OrionCore.js" />
<orion:include id="Include3" runat="server" file="VIM/Actions/js/VirtualMachineMotionTargetGrid.js" />
<orion:include id="Include1" runat="server" file="VIM/Actions/js/VirtualMachineMotionController.js" />
<orion:include id="Include4" runat="server" file="VIM/Actions/js/ValidationsWrapper.js" />

<style>
    .manageVMAction .sw-vim-action-selectedHost {
        margin: 0 10px 0 10px;
    }

    #sw-vim-action-selectHostHelp {
        display: inline-block;
        margin: 0 0 5px 0;
    }
</style>

<script type="text/javascript">

    var motionController = new SW.VIM.Actions.VirtualMachineMotionController({
        containerID: '<%= container.ClientID %>',
        onReadyCallback: <%= OnReadyJsCallback %>,
        actionDefinition: <%= ToJson(ActionDefinition, true) %>,
        viewContext: <%= ToJson(ViewContext, true) %>,
        multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
        targetHostId: '<%= TargetHostId %>',
        isValid: <%= ValidationWrapper.IsValid.ToString().ToLower() %>,
        dataFormMapping: {
            targetHostId: "<%= VirtualMachineMotionConstants.TargetHostIdPropertyKey %>"
        }
    });
    motionController.init();
    
</script>

<vim:ValidationWrapper runat="server" id="ValidationWrapper" actionname="<%$ Code: ManagementEnums.ActionName.PerformMigration %>">
    <Content>
        <div runat="server" id="container" class="manageVMAction">
   
            <h3><asp:Label runat="server" ID="lbHeaderText"></asp:Label><asp:Label runat="server" ID="lbSelectedVM" ClientIDMode="Static" CssClass="sw-vim-action-selectedVM"></asp:Label></h3>
            <div class="section required" runat="server" ID="selectionWrapper" ClientIDMode="Static">
                <vim:SelectionType runat="server" ID="SelectionType"></vim:SelectionType>       
                  <vim:ValidationWrapper runat="server" id="powerPermissionValidationWrapper" ErrorMessageVisibility="False" ActionName="<%$ Code: ManagementEnums.ActionName.PowerOff %>">
                      <Content>
                          <vim:PowerControlBatch runat="server" ID="PowerBatchControl" PowerControlLabel="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_51 %>" />
                     </Content>
                  </vim:ValidationWrapper> 
            </div>
        
            <h3 id="hostSelection"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_11 %> <asp:Label runat="server" ID="lbSelectedHost" ClientIDMode="Static" CssClass="sw-vim-action-selectedHost"></asp:Label></h3>
    
            <div id="manageActionSection" class="section required">
          
                <div class="sw-vim-standard-resizable-wrapper">
            
                     <% if (MultiEditEnabled)
                        { %>
                                    <asp:CheckBox ID="chbxSelectTargetHost" runat="server" data-edited ="<%$ Code: VirtualMachineMotionConstants.TargetHostIdPropertyKey %>"/>
                            <% }%>

                    <span id="sw-vim-action-selectHostHelp"><%= Resources.VIMWebContent.VIMWEBDATA_TK0_12 %></span> 
                    <div id="tabPanel" class="tab-top">
                
               
                                                                         
                        <div id="gridSelectTargetHost" class="sw-vim-wizard-extjs-grid"></div>
                    </div>
                </div>              
            </div>
        </div>
     </Content>
</vim:ValidationWrapper>
