﻿using System;
using SolarWinds.VIM.Actions.VmManagement;

public partial class VirtualMachinePowerOffView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachinePowerOffPlugin.ActionTypeId; }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachinePowerOffExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "powerOffController";
    }
}