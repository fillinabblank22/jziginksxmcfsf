﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.Common.Licensing;

public partial class ValidationWrapper : System.Web.UI.UserControl
{
    public PlaceHolder Content
    {
        get { return WrapperContent; }
    }

    public ManagementEnums.ActionName ActionName { get; set; }

    public bool IsValid { get; private set; }

    private bool _errorMessageVisibility = true;

    public bool ErrorMessageVisibility
    {
        get
        {
            return _errorMessageVisibility;
        }
        set
        {
            _errorMessageVisibility = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Validate();
    }

    private void ShowContent(bool valid, string message)
    {
        WrapperContent.Visible = valid;
        noPermissionsErrorMsg.Visible = !valid && ErrorMessageVisibility;
        dialogErrorMessage.Text = message;
    }

    private void ShowPermissionContent(bool allowPermissions)
    {
        ShowContent(allowPermissions, Resources.VIMWebContent.VIMWEBDATA_RB0_7);
    }

    private void ShowIntegrationContent(bool valid)
    {
        ShowContent(valid, Resources.VIMWebContent.VIMWEBDATA_TK0_34);
    }

    public void Validate()
    {
        if (!LicenseManager.Instance.IsVimFullyLicensed())
        {
            IsValid = false;
            ShowIntegrationContent(false);
            return;
        }

        switch (ActionName)
        {
            case ManagementEnums.ActionName.TakeSnapshot:
            case ManagementEnums.ActionName.DeleteSnapshots:
            {
                IsValid = VimProfile.AllowSnapshotManagement;
                break;
            }
            case ManagementEnums.ActionName.Reboot:
            case ManagementEnums.ActionName.PowerOn:
            case ManagementEnums.ActionName.PowerOff:
            case ManagementEnums.ActionName.Suspend:
            case ManagementEnums.ActionName.Pause:
                {
                    IsValid = VimProfile.AllowPowerManagement;
                    break;
                }
            case ManagementEnums.ActionName.PerformRelocation:
            case ManagementEnums.ActionName.PerformMigration:
                {
                    IsValid = VimProfile.AllowMigrationManagement;
                    break;
                }
            case ManagementEnums.ActionName.ChangeSettings:
                {
                    IsValid = VimProfile.AllowResourcesSettingsManagement;
                    break;
                }
        }
        ShowPermissionContent(IsValid);
    }
}