﻿using System;
using SolarWinds.VIM.Actions.VmManagement;
using SolarWinds.VIM.Actions.VmManagement.TakeSnapshot;

public partial class VirtualMachineTakeSnapshotView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachineTakeSnapshotPlugin.ActionTypeId; }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachineTakeSnapshotExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "takeSnapshotController";
    }
}
