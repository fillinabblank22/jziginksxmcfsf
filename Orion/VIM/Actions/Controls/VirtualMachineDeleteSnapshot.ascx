﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualMachineDeleteSnapshot.ascx.cs" Inherits="VirtualMachineDeleteSnapshotView" %>
<%@ Register TagPrefix="vim" TagName="SelectionType" Src="~/Orion/VIM/Actions/Controls/SelectionType.ascx" %>
<%@ Register TagPrefix="vim" TagName="ValidationWrapper" Src="~/Orion/VIM/Actions/Controls/ValidationWrapper.ascx" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>

<script type="text/javascript">

    var deleteSnapshotController = new SW.VIM.Actions.VirtualMachineDeleteSnapshotController({
            
        containerID : '<%= container.ClientID %>', 
        onReadyCallback : <%= OnReadyJsCallback %>,
        actionDefinition :  <%= ToJson(ActionDefinition, true) %>,
        viewContext :  <%= ToJson(ViewContext, true) %>,
        isValid: <%= validationWrapper.IsValid.ToString().ToLower() %>,
        multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
        dataFormMapping : {
            snapshotType:  "<%= ManagementActionConstants.DeleteSnapshot.SnapshotTypeKey %>",
        }
    });

    deleteSnapshotController.init();

</script>

<style> 
     .sw-vim-radioButton {
         margin: 0px 3px 3px 0px;
         display: block;
     }

     .sw-vim-radioButton input[type='radio'] {
         margin: 3px 3px 3px 0px;
     }
</style>

<orion:include id="Include1" runat="server" file="VIM/Actions/js/VirtualMachineDeleteSnapshotController.js" />
<orion:include id="Include2" runat="server" file="VIM/Actions/js/ValidationsWrapper.js" />

<vim:ValidationWrapper runat="server" id="validationWrapper" ActionName="<%$ Code: ManagementEnums.ActionName.DeleteSnapshots %>">
    <Content>
        <div runat="server" id="container" class="manageVMAction">
           <h3><asp:Label runat="server" ID="lbHeaderText"></asp:Label><asp:Label runat="server" ID="lbSelectedVM" ClientIDMode="Static" CssClass="sw-vim-action-selectedVM"></asp:Label></h3>
           <div class="section required" runat="server" ID="selectionWrapper" ClientIDMode="Static">
               <vim:SelectionType runat="server" ID="SelectionType"></vim:SelectionType>
            </div>
            <h3><%= Resources.VIMWebContent.VIMWEBDATA_RB0_5 %></h3>
            <div class="paragraph">
                <div>
                    <asp:checkbox id="snapshotEditEnabled" runat="server" data-edited="<%$ Code: ManagementActionConstants.DeleteSnapshot.SnapshotTypeKey  %>" />
                    <asp:radiobuttonlist id="snapshotType" runat="server" CssClass="sw-vim-radioButton" data-form="<%$ Code: ManagementActionConstants.DeleteSnapshot.SnapshotTypeKey  %>">
                        <asp:ListItem Value="0" Selected="True" Text=" <%$ Resources:VIMWebContent, VIMWEBDATA_RB0_6 %>"></asp:ListItem>
                    </asp:radiobuttonlist>
               </div>
            </div>
        </div>
    </Content>
</vim:ValidationWrapper>

