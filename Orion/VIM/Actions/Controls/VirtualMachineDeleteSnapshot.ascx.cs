﻿using System;
using SolarWinds.VIM.Actions.VmManagement;

public partial class VirtualMachineDeleteSnapshotView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {

        get { return VirtualMachineDeleteSnapshotPlugin.ActionTypeId; }
    }
    
    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachineDeleteSnapshotExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "deleteSnapshotController";

        ApplyMultiEditMode();
    }

    private void ApplyMultiEditMode()
    {
        snapshotEditEnabled.Visible = MultiEditEnabled;
    }


    protected override void BindConfiguration()
    {
        string snapshotTypeKey = ActionDefinition.Properties.GetPropertyValue(ManagementActionConstants.DeleteSnapshot.SnapshotTypeKey);

        if (string.IsNullOrEmpty(snapshotTypeKey))
        {
            snapshotType.SelectedIndex = 0;
        }
    }

}