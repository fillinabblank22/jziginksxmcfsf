﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Extensions;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.Orion.Core.Models.Actions.Contexts;
using SolarWinds.VIM.Actions.VmManagement;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Constants;
using SolarWinds.VIM.Common.Enums;
using SolarWinds.VIM.Web.DAL;

public partial class Orion_VIM_Actions_Controls_SelectionType : System.Web.UI.UserControl
{
    public AlertingActionContext AlertingActionContext { get; set; }
    public ActionDefinition ActionDefinition { get; set; }
    public bool MultiEditMode { get; set; }
    public string ControllerName { get; set; }
    public bool RenderVendorComboBox { get; set; }
    public ManagementEnums.ActionName Action { get; set; }

    public ManagementEnums.SelectionType SelectionType
    {
        get { return _selectionType; }
    }

    public int SelectedVirtualMachineId
    {
        get { return _selectedVirtualMachineId; }
    }

    public string SelectedVirtualMachineName
    {
        get { return _selectedVirtualMachineName; }
    }

    private int _selectedVirtualMachineId;
    private string _selectedVirtualMachineName;
    private ManagementEnums.SelectionType _selectionType;
    private VirtualizationPlatform _vendor = VirtualizationPlatform.Unknown;

    public VirtualizationPlatform Vendor
    {
        get
        {
            return _vendor;
        }
        set
        {
            _vendor = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Initialize();
        // Edit Mode
        if (ActionDefinition != null && !MultiEditMode)
        {
            BindConfiguration();
        }
        else // New mode
        {
            SetDefaultValues();
        }
    }

    private void Initialize()
    {
        if (RenderVendorComboBox)
            InitVendorComboBox();
        vendorWrapper.Visible = RenderVendorComboBox;

        if (Vendor != VirtualizationPlatform.Unknown)
        {
            helpLabelVendorWrapper.Visible = true;
            helpLabelForVendor.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_RB0_11, Action.LocalizedLabel(),
                Vendor.LocalizedLabel());
        }
        else
        {
            helpLabelVendorWrapper.Visible = false;
        }
        
        if (AlertingActionContext.EntityType != SwisEntityNames.Vim.VirtualMachines)
        {
            selectionTypeSpecific.Enabled = true;
            selectionType.Visible = false;
        }
        if (MultiEditMode)
            selectionTypeSpecific.Enabled = false;
    }

    private void BindConfiguration()
    {
        ActionProperty vendorProperty = ActionDefinition.Properties[ManagementActionConstants.VendorPropertyKey];
        if (vendorProperty != null)
        {
            var vendorStr = vendorProperty.PropertyValue;
            dplSelectionTypeContextVendor.SelectedValue = vendorStr;
            chxbSelectionTypeContextVendor.Checked = true;
        }

        var selectionTypeStr = AlertingActionContext.EntityType != SwisEntityNames.Vim.VirtualMachines ? ManagementEnums.SelectionType.Specific.ToString() : ActionDefinition.Properties[ManagementActionConstants.SelectionTypePropertyKey].PropertyValue;
        if (!String.IsNullOrEmpty(selectionTypeStr))
        {
            _selectionType = (ManagementEnums.SelectionType)Enum.Parse(typeof(ManagementEnums.SelectionType), selectionTypeStr);
            if (_selectionType == ManagementEnums.SelectionType.Specific)
            {

                SetRadioButtonsValue(ManagementEnums.SelectionType.Specific);
                _selectedVirtualMachineId = Convert.ToInt32(ActionDefinition.Properties[ManagementActionConstants.SelectedVirtualMachineIdPropertyKey].PropertyValue);
                selectedVirtualMachineId.Value = _selectedVirtualMachineId.ToString();
                var vmModel = new VirtualMachineDAL().GetVMModelByVMId(_selectedVirtualMachineId);
                _selectedVirtualMachineName = vmModel.Name;
            }
            else
            {
                SetDefaultValues();
            }
        }
    }


    private void SetDefaultValues()
    {
        if (AlertingActionContext.EntityType != SwisEntityNames.Vim.VirtualMachines && !MultiEditMode && ActionDefinition == null)
        {
            SetRadioButtonsValue(ManagementEnums.SelectionType.Specific);
            _selectionType = ManagementEnums.SelectionType.Specific;
        }
        else
        {
            _selectionType = ManagementEnums.SelectionType.Context;
            SetRadioButtonsValue(ManagementEnums.SelectionType.Context);
        }

        _selectedVirtualMachineId = -1;
    }

    private void SetRadioButtonsValue(ManagementEnums.SelectionType selType)
    {
        switch (selType)
        {
            case ManagementEnums.SelectionType.Context:
                selectionTypeContext.Checked = true;
                break;
            case ManagementEnums.SelectionType.Specific:
                selectionTypeSpecific.Checked = true;
                break;
        }
        selectedOption.Value = selType.ToString();
    }

    private void InitVendorComboBox()
    {
        dplSelectionTypeContextVendor.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_LB0_15, VirtualizationPlatform.HyperV.ToString()));
        dplSelectionTypeContextVendor.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_LB0_14, VirtualizationPlatform.Vmware.ToString()));
    }
}