﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.VIM.Actions.VmManagement;

public partial class Orion_VIM_Actions_Controls_PowerControlBatch : System.Web.UI.UserControl
{
    public ActionDefinition ActionDefinition { get; set; }
    public bool MultiEditMode { get; set; }
    public string ActionName { get; set; }
    public string MainControllerName { get; set; }
    public string PowerControlLabel { get; set; }
    private bool _powerControlEnabled;

    public bool PowerControlEnabled
    {
        get { return _powerControlEnabled; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Edit Mode
        if (ActionDefinition != null && !MultiEditMode)
        {
            BindConfiguration();
        }
        else // New mode
        {
            SetDefaultValues();
        }
    }

    private void BindConfiguration()
    {
        var prop = ActionDefinition.Properties[ManagementActionConstants.PowerControlBatchEnabledKey];
        
        if (prop != null)        
            bool.TryParse(prop.PropertyValue, out _powerControlEnabled);
        else
            _powerControlEnabled = false;
    }

    private void SetDefaultValues()
    {
       _powerControlEnabled = false;
    }    
}