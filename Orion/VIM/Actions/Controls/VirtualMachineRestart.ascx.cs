﻿using System;
using SolarWinds.VIM.Actions.VmManagement;
using SolarWinds.VIM.Actions.VmManagement.Restart;

public partial class VirtualMachineRestartView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachineRestartPlugin.ActionTypeId; }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachineRestartExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "restartController";
    }
}