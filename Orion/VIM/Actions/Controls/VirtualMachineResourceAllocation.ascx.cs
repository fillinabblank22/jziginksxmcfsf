﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.VIM.Actions.VmManagement;
using SolarWinds.VIM.Actions.VmManagement.ResourceAllocation;

public partial class VirtualMachineResourceAllocation : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachineResourceAllocationPlugin.ActionTypeId; }
    }
 
    protected override void BindConfiguration()
    {
        var memory = ActionDefinition.Properties.GetPropertyValue(ManagementActionConstants.ResourceAllocation.MemoryPropertyKey);
        tbMemory.Text = memory;
        chbxMemory.Checked = !string.IsNullOrEmpty(tbMemory.Text);
        tbNumberOfProcessors.Text = ActionDefinition.Properties.GetPropertyValue(ManagementActionConstants.ResourceAllocation.NumberOfProcessorsPropertyKey);
        chbxNumberOfProcessors.Checked = !string.IsNullOrEmpty(tbNumberOfProcessors.Text);

        var modType = ActionDefinition.Properties.GetPropertyValue(ManagementActionConstants.ResourceAllocation.ModificationTypePropertyKey);
        rblModificationType.SelectedValue = modType;

        dpMemoryAmount.SelectedValue = dpMemoryAmount.Items.FindByValue(memory) != null ? memory : ManagementActionConstants.ResourceAllocation.MemoryComboBoxCustom;
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachineResourceAllocationExecutor.ActionNameStatic);

        PowerBatchControl.MultiEditMode = MultiEditEnabled;
        PowerBatchControl.ActionDefinition = ActionDefinition;
        PowerBatchControl.MainControllerName = "vimResourceAllocationController";

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "vimResourceAllocationController";
    }

    protected override void Page_Load(object sender, EventArgs e)
    {
        InitMemoryComboBox();
        base.Page_Load(sender, e);
        
    }

    private void InitMemoryComboBox()
    {
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_29, "None"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_23, "1024"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_24, "2048"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_25, "4096"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_26, "8192"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_27, "16384"));
        dpMemoryAmount.Items.Add(new ListItem(Resources.VIMWebContent.VIMWEBDATA_TK0_28, "Custom"));
    }
}