﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualMachineTakeSnapshot.ascx.cs" Inherits="VirtualMachineTakeSnapshotView" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Register TagPrefix="vim" TagName="SelectionType" Src="~/Orion/VIM/Actions/Controls/SelectionType.ascx" %>
<%@ Register TagPrefix="vim" TagName="ValidationWrapper" Src="~/Orion/VIM/Actions/Controls/ValidationWrapper.ascx" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>

<orion:include id="Include1" runat="server" file="VIM/Actions/js/VirtualMachineTakeSnapshotController.js" />
<orion:include id="Include2" runat="server" file="VIM/Actions/js/ValidationsWrapper.js" />

<script type="text/javascript">

    var takeSnapshotController = new SW.VIM.Actions.VirtualMachineTakeSnapshotController({            
        containerID: '<%= container.ClientID %>',
        onReadyCallback: <%= OnReadyJsCallback %>,
        actionDefinition: <%= ToJson(ActionDefinition, true) %>,
        viewContext: <%= ToJson(ViewContext, true) %>,
        multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
        isValid: <%= validationWrapper.IsValid.ToString().ToLower() %>,
        dataFormMapping: {}
    });

    takeSnapshotController.init();
    
</script>

<vim:ValidationWrapper runat="server" id="validationWrapper" actionname="<%$ Code: ManagementEnums.ActionName.TakeSnapshot %>">
    <Content>
        <div runat="server" id="container" class="manageVMAction">
            <h3><asp:Label runat="server" ID="lbHeaderText"></asp:Label><asp:Label runat="server" ID="lbSelectedVM" ClientIDMode="Static" CssClass="sw-vim-action-selectedVM"></asp:Label></h3>
            <div class="section required" runat="server" ID="selectionWrapper" ClientIDMode="Static">
                <vim:SelectionType runat="server" ID="SelectionType"></vim:SelectionType>
           </div>
       </div>
    </Content>
</vim:ValidationWrapper>

