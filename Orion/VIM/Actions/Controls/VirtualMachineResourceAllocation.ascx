﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VirtualMachineResourceAllocation.ascx.cs" Inherits="VirtualMachineResourceAllocation" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement.ResourceAllocation" %>
<%@ Import Namespace="SolarWinds.VIM.Web" %>
<%@ Import Namespace="SolarWinds.VIM.Common.Enums" %>
<%@ Register TagPrefix="vim" TagName="SelectionType" Src="~/Orion/VIM/Actions/Controls/SelectionType.ascx" %>
<%@ Register TagPrefix="vim" TagName="PowerControlBatch" Src="~/Orion/VIM/Actions/Controls/PowerControlBatch.ascx" %>
<%@ Register TagPrefix="vim" TagName="ValidationWrapper" Src="~/Orion/VIM/Actions/Controls/ValidationWrapper.ascx" %>

<orion:Include ID="Include1" runat="server" File="VIM/Actions/js/VirtualMachineResourceAllocationController.js" />
<orion:Include ID="Include2" runat="server" File="VIM/Actions/js/ManagementActionValidations.js" />
<orion:include id="Include4" runat="server" file="VIM/Actions/js/ValidationsWrapper.js" />

<style>
   .manageVMAction #sw-vim-manageActionSection {
       width: 600px
   } 
    
   #sw-vim-sprawlinfotext {
       padding-bottom: 16px
   }

   div#sw-vim-manageActionSection input[type="text"]
   {
       text-align: right;
       width: 60px
   } 

   div#sw-vim-manageActionSection input[type="checkbox"] {
       margin-top: 2px
   }

   td.sw-vim-action-allocation-td-add {
       width: 10px;
       text-align: center;
   }

   .sw-vim-action-allocation-add {
       /*display: block;
       width: 16px;
       height: 16px;
       background-image: url(/Orion/VIM/images/add_16x16.gif);*/
       font-weight: bold;
   }

   div#sw-vim-action-allocation-modification-type {
       margin: 0 0 10px 0;
   }
   
   .sw-vim-radioButton {
       margin: 0px 3px 3px 0px;
       display: block;
   }

   .sw-vim-radioButton input[type='radio'] {
       margin: 3px 3px 3px 0px;
   }
</style>

<script type="text/javascript">
    var vimResourceAllocationController = new SW.VIM.Actions.VirtualMachineResourceAllocationController({
        containerID: '<%= container.ClientID %>',
        onReadyCallback: <%= OnReadyJsCallback %>,
        actionDefinition: <%= ToJson(ActionDefinition, true) %>,
        viewContext: <%= ToJson(ViewContext, true) %>,
        multiEditMode: <%= MultiEditEnabled.ToString().ToLower() %>,
        isValid: <%= validationWrapper.IsValid.ToString().ToLower() %>,
        dataFormMapping: {
            numberOfProcessors: "<%= ManagementActionConstants.ResourceAllocation.NumberOfProcessorsPropertyKey %>",
            memory: "<%= ManagementActionConstants.ResourceAllocation.MemoryPropertyKey %>",
        }
    });
    vimResourceAllocationController.init();    
</script>




<vim:ValidationWrapper runat="server" id="validationWrapper" ActionName="<%$ Code: ManagementEnums.ActionName.ChangeSettings %>">
    <Content>
        <div runat="server" id="container" class="manageVMAction">
    
            <h3><asp:Label runat="server" ID="lbHeaderText"></asp:Label><asp:Label runat="server" ID="lbSelectedVM" ClientIDMode="Static" CssClass="sw-vim-action-selectedVM"></asp:Label></h3>
            <div class="section required" runat="server" ID="selectionWrapper" ClientIDMode="Static">
                <vim:SelectionType runat="server" ID="SelectionType"></vim:SelectionType>        
                <vim:ValidationWrapper runat="server" id="powerValidationWrapper" ErrorMessageVisibility="False" ActionName="<%$ Code: ManagementEnums.ActionName.PowerOff %>">
                    <Content>
                        <vim:PowerControlBatch runat="server" ID="PowerBatchControl" PowerControlLabel="<%$ Resources: VIMWebContent, VIMWEBDATA_LV0_49 %>" />
                    </Content>
                </vim:ValidationWrapper> 
            </div>
        
            <h3><%= Resources.VIMWebContent.VIMWEBDATA_TK0_9 %></h3>
            <div id="sw-vim-manageActionSection" class="section required">
                <div id="sw-vim-sprawlinfotext" >
                <%=Resources.VIMWebContent.VIMWEBDATA_VZ0_16%>
                </div>
                
                <div id="sw-vim-action-allocation-modification-type">
                    <asp:RadioButtonList ID="rblModificationType" runat="server" CssClass="sw-vim-radioButton" >
                        <asp:ListItem  Value="Increase" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TK0_30 %>" Selected="True"></asp:ListItem>
                        <asp:ListItem  Value="Specific" Text="<%$ Resources: VIMWebContent, VIMWEBDATA_TK0_31 %>"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>

                <table id="sw-vim-action-allocation-formWrapper">
                    <tbody>
                        <tr>
                            <td><asp:CheckBox ID="chbxNumberOfProcessors" runat="server" data-edited ="<%$ Code: ManagementActionConstants.ResourceAllocation.NumberOfProcessorsPropertyKey %>"/></td>
                            <td><%=Resources.VIMWebContent.VIMWEBDATA_TK0_6%></td>
                            <td class="sw-vim-action-allocation-td-add"><span class="sw-vim-action-allocation-add">+</span></td>
                            <td><asp:TextBox ID="tbNumberOfProcessors" runat="server" data-form ="<%$ Code: ManagementActionConstants.ResourceAllocation.NumberOfProcessorsPropertyKey %>" MaxLength="400" data-type="numeric"></asp:TextBox></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="chbxMemory" runat="server" data-edited ="<%$ Code: ManagementActionConstants.ResourceAllocation.MemoryPropertyKey %>"/></td>
                            <td><%=Resources.VIMWebContent.VIMWEBDATA_TK0_7%></td>
                            <td class="sw-vim-action-allocation-td-add"><span class="sw-vim-action-allocation-add">+</span></td>
                            <td>
                                <asp:DropDownList ID="dpMemoryAmount" runat="server" ClientIDMode="Static" data-form ="<%$ Code: ManagementActionConstants.ResourceAllocation.MemoryPropertyKey %>">         
                                </asp:DropDownList>
                            </td>
                            <td><span class="sw-vim-action-acllocation-memory-custom"><asp:TextBox ID="tbMemory" runat="server" data-form ="<%$ Code: ManagementActionConstants.ResourceAllocation.MemoryPropertyKey %>" MaxLength="400" data-type="numeric"></asp:TextBox></span></td>
                            <td><span class="sw-vim-action-acllocation-memory-custom">MB</span></td>
                        </tr>
                    </tbody>
                </table>                        
            </div>
        </div>
   </Content>
</vim:ValidationWrapper>