﻿using System;
using SolarWinds.Orion.Core.Models.Actions;
using SolarWinds.VIM.Actions.VmManagement.Motion;
using SolarWinds.VIM.Web.DAL;
using SolarWinds.VIM.Web.Models;

public partial class VirtualMachineMotionView : VirtualMachineManagementControl
{
    private int? _targetHostId;

    public int? TargetHostId
    {
        get { return _targetHostId; }
    }

    public override string ActionTypeID
    {
        get { return VirtualMachineMotionPlugin.ActionTypeId; }
    }

    protected override void BindConfiguration()
    {
        ActionProperty actionProperty = ActionDefinition.Properties[VirtualMachineMotionConstants.TargetHostIdPropertyKey];
        if (actionProperty != null)
        {
            _targetHostId = Convert.ToInt32(actionProperty.PropertyValue);
            HostModel host = new HostDAL().GetHostModelByHostId(_targetHostId.Value);
            if (host != null)
                lbSelectedHost.Text = host.Name;
        }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, Resources.VIMWebContent.VIMWEBDATA_JD0_33);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "motionController";
        SelectionType.RenderVendorComboBox = true;

        PowerBatchControl.MultiEditMode = MultiEditEnabled;
        PowerBatchControl.ActionDefinition = ActionDefinition;
        PowerBatchControl.MainControllerName = "motionController";

    }

}