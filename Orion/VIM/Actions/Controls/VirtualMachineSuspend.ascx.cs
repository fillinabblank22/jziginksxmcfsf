﻿using System;
using SolarWinds.VIM.Actions.Suspend;
using SolarWinds.VIM.Actions.VmManagement;

public partial class VirtualMachineSuspendView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachineSuspendPlugin.ActionTypeId; }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachineSuspendExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "suspendController";
    }
}