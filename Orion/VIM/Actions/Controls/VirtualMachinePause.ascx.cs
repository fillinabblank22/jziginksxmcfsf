﻿using System;
using SolarWinds.VIM.Actions.VmManagement.Pause;
using SolarWinds.VIM.Common;
using SolarWinds.VIM.Common.Enums;

public partial class VirtualMachinePauseView : VirtualMachineManagementControl
{
    public override string ActionTypeID
    {
        get { return VirtualMachinePausePlugin.ActionTypeId; }
    }

    protected override void Initialize()
    {
        base.Initialize();

        lbHeaderText.Text = String.Format(Resources.VIMWebContent.VIMWEBDATA_TK0_19, VirtualMachinePauseExecutor.ActionNameStatic);

        SelectionType.ActionDefinition = ActionDefinition;
        SelectionType.AlertingActionContext = AlertingViewContext;
        SelectionType.MultiEditMode = MultiEditEnabled;
        SelectionType.ControllerName = "pauseController";
        SelectionType.Vendor = VirtualizationPlatform.HyperV;
        SelectionType.Action = ManagementEnums.ActionName.Pause;
    }
}