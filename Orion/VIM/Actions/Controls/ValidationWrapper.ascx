﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ValidationWrapper.ascx.cs" Inherits="ValidationWrapper" %>
<%@ Reference Control="~/Orion/VIM/Actions/Controls/PowerControlBatch.ascx" %>

<style type="text/css">
    .item-container {
        padding: 5px;
        width: 650px;
    }

    .sw-vim-item-box {
        margin-top: 15px;
        min-height: 50px;
    }   

    .sw-vim-item-box table td:first-child {
        padding-right: 10px;
    }    
    
    .sw-vim-item-box table td {
        vertical-align: middle;
    }
        
</style>

<div class="sw-vim-item-box" id="noPermissionsErrorMsg" runat="server">
    <table>
        <tr>
        <td><img src="/Orion/Images/stop_32x32.gif" alt="error" /></td>  
        <td><asp:Label runat="server" ID="dialogErrorMessage"></asp:Label></td>       
        </tr>
    </table>
</div>
<asp:PlaceHolder runat="server" ID="WrapperContent" />