﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PowerControlBatch.ascx.cs" Inherits="Orion_VIM_Actions_Controls_PowerControlBatch" %>
<%@ Import Namespace="SolarWinds.VIM.Actions.VmManagement" %>

<orion:Include ID="Include1" runat="server" File="VIM/Actions/js/PowerControlBatch.js" />

<style>
   .sw-vim-bold {
       font-weight: bold;
   }

   .sw-vim-powerControlBox {
       margin: 15px 0px 10px 2px;
       padding: 8px;
       width: 582px;
   }
</style>

<div class="sw-vim-powerControlBox sw-suggestion-info">
    <div id="powerControlContainer">           
        <asp:CheckBox ID="powerControlCheckbox" runat="server" ClientIDMode="Static" />        
        <strong><label class="sw-vim-bold"><%= PowerControlLabel%></label></strong>
        <div class="sw-text-helpful" style="margin-left: 17px"><%= Resources.VIMWebContent.VIMWEBDATA_LV0_50 %></div>                
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var controller = new SW.VIM.Actions.PowerControlBatchController();
        controller.init({
            containerID: 'powerControlContainer',
            multiEditMode: '<%= MultiEditMode %>',
                powerControlEnabled: '<%= PowerControlEnabled.ToString().ToLower() %>'
            });            
            <%= MainControllerName %>.setPowerBatchController(controller);
        });

</script>