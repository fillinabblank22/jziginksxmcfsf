<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true"
    CodeFile="VIMHyperVSummary.aspx.cs" Inherits="Orion_VIM_VIMHyperVSummary" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Common.Helpers" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="vim" TagName="LicenseExpirationMessage" Src="~/Orion/VIM/Controls/LicenseExpirationMessage.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="VimPageTitle" runat="Server">
    <div class="titleTable">
        <h1><%= this.ViewInfo.ViewTitle %></h1>
    </div>
    <%if (VimFipsHelper.IsPollingFipsRestricted)
    {%>
    <div style="margin-left: 1em;" class="vim-hyperv-fips-restriction-warning sw-suggestion sw-suggestion-info">
        <span class="sw-suggestion-icon"></span>
        <span><%= String.Format(Resources.VIMWebContent.Web_FIPSRestrictionEnabled_HyperV_Warning, VmanPollingKbLink)%></span>
    </div>
    <%}%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
    <vim:LicenseExpirationMessage ID="_licenseExpirationMessage" runat="server" />
</asp:Content>
