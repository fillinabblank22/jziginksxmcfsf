﻿using System;
using SolarWinds.VIM.Web;
using SolarWinds.VIM.Web.Exceptions;
using SolarWinds.VIM.Web.NetObjects;
using SolarWinds.VIM.Web.Providers;
using SolarWinds.VIM.Web.Views;

public partial class Orion_VIM_DataStoreDetails : VimViewPage, IVManDataStoreProvider
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override string ViewType
    {
        get { return "VMan DataStore Details"; }
    }
    public override string HelpFragment
    {
        get
        {
            return "OrionVMPHDstoreDetailsView";
        }
    }
    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }

    public VManDataStore DataStore { get { return (VManDataStore) this.NetObject; } }
}