<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/VIM/VimView.master" AutoEventWireup="true"
    CodeFile="Summary.aspx.cs" Inherits="Orion_VIM_Virtualization_Summary" %>
<%@ Import Namespace="SolarWinds.VIM.Web.Helpers" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="vim" TagName="LicenseExpirationMessage" Src="~/Orion/VIM/Controls/LicenseExpirationMessage.ascx" %>

<asp:Content ContentPlaceHolderID="VimPageTitle" runat="Server">
    <div class="titleTable">
        <h1><%= this.ViewInfo.ViewTitle %></h1>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="VimMainContentPlaceHolder" runat="server">
    <orion:ResourceHostControl ID="ResourceHostControl2" runat="server">
        <orion:ResourceContainer runat="server" ID="resContainer" />
    </orion:ResourceHostControl>
    <vim:LicenseExpirationMessage ID="_licenseExpirationMessage" runat="server" />
</asp:Content>
