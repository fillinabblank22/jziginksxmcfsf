﻿using System;
using System.Linq;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Web.Discovery;
using System.Text.RegularExpressions;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Models.Enums;

public partial class Orion_Discovery_DiscoveryNodePopup : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();


    static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    protected DiscoveryNodeUI DiscoveredNode { get; set; }
    protected String HeaderText { get; set; }

    public class BindItem
    {
        public String StatusUrl { get; set; }
        public String DisplayName;
        public int Count;

        public String Icon { get; set; }

        public String FullDescription
        {
            get
            {
                if (Count == 0)
                    return DisplayName;

                return String.Format("({0}) {1}", Count, DisplayName);
            }
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

        string Prefix = String.Empty;
        long discoveryNodeID = 0;
        long profileID = 0;

        string NetObject = Request["NetObject"];

        Regex regex = new Regex(@"(?<PREFIX>(\w+)):(?<ID>(\d+))");
        MatchCollection MatchCollectionNetObject = regex.Matches(NetObject);
        if (MatchCollectionNetObject.Count > 0)
        {
            Prefix = MatchCollectionNetObject[0].Groups["PREFIX"].Value;
            discoveryNodeID = Convert.ToInt64(MatchCollectionNetObject[0].Groups["ID"].Value);            
        }

        long mask = 0xffffffff;
        profileID = discoveryNodeID & mask; 
        discoveryNodeID = discoveryNodeID >> 32;

        DiscoveredNode = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.Find(d => d.DiscoveryNode.NodeID == discoveryNodeID && d.DiscoveryNode.ProfileID==profileID);

        // loach child data
        DiscoveredNode.LoadVolumesAndInterfaces();

        if (Prefix == ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX)
        {         
            HeaderText = Resources.CoreWebContent.WEBCODE_AK0_14;
            InterfacesAndVolumes.DataSource = DiscoveredNode.GetInterfaceGroups()                
                .Select( n =>                    
                    new BindItem()
                    {
                        StatusUrl = GetStatusUrl(n.Status == DiscoveryInterfaceStatus.Imported ? NetObjectStatus.Imported : NetObjectStatus.NotImported),
                        Icon = n.Icon,
                        DisplayName = n.Description,
                        Count = n.Count
                    });

            InterfacesAndVolumes.DataBind();        
        }
        else if (Prefix == ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX)
        {       
            HeaderText = Resources.CoreWebContent.WEBCODE_AK0_15;
            InterfacesAndVolumes.DataSource = DiscoveredNode.GetVolumegroups()
                .Select( n =>                    
                        new BindItem()
                        {
                            StatusUrl = GetStatusUrl(n.Status == DiscoveryVolumeStatus.Imported ? NetObjectStatus.Imported : NetObjectStatus.NotImported),
                            Icon = n.Icon,
                            DisplayName = n.Description,
                            Count = n.Count
                        });
            InterfacesAndVolumes.DataBind();        
        }                
        else
        {
            var group = ScheduleDiscoveryHelper.GetDiscoveryGroupFromNetobject(Prefix);

            HeaderText = group.Group.DisplayName;
            InterfacesAndVolumes.DataSource = DiscoveredNode.DiscoveryNode.NodeResult.PluginResults
                .SelectMany( n => n.GetDiscoveredObjects())
                .Where( n => group.Group.IsMyGroupedObjectType(n))
                .Select( n =>
                    {
                        IDiscoveredObjectWithIcon icon = n as IDiscoveredObjectWithIcon;
                        IDiscoveredObjectWithImportStatus status = n as IDiscoveredObjectWithImportStatus;
                        return new BindItem()
                        {
                            StatusUrl = GetStatusUrl(status == null ? NetObjectStatus.Nothing : status.ImportStatus),
                            Icon = icon == null ? "" : icon.Icon,
                            DisplayName = n.DisplayName,
                            Count = 0,
                        };
                    });
            InterfacesAndVolumes.DataBind();        
        }
    }      

    protected string GetStatusUrl(NetObjectStatus status)
    {                
        if( status.Contains( NetObjectStatus.Imported))
        {
            return @"/Orion/Discovery/images/StatusIcons/icon_imported_16x16.gif";
        }

        return SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "Discovery/images/StatusIcons/icon_found_16x16.gif");
    }   


}
