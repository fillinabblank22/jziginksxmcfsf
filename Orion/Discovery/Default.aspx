<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/DiscoveryPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Orion_Discovery_Default" Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_50%>" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Discovery/Controls/DiscoveryTabBar.ascx" TagPrefix="discovery" TagName="DiscoveryTabBar" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register TagPrefix="orion" TagName="LicenseLimitControl" Src="~/Orion/Controls/LicenseLimitControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" Debug="false" />
    <script type="text/javascript">
        //global text variables | Internationalization team
        var IntTM = {
            txt1: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_17)%>', //Unable to start discovery.\n\n
            txt2: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_18)%>', //An error has occured.\nSee discovery log for more details.\n
            txt3: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_19)%>', //Network discovery complete. Processing results...
            txt4: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_20)%>', //An error while processing results has occured.\nSee discovery log for more details.\n
            txt5: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_21)%>', //An error has occured.\nSee discovery log for more details.
            txt6: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_22)%>', //Are you sure you want to cancel this discovery?
            txt7: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_23)%>', //An error while cancelling discovery has occured.\nSee discovery log for more details.\n
            txt8: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_24)%>', //Discovering Network...
            txt9: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_43)%>'  //Deleting discovery profile causes removing topology data as well. Are you sure you want to remove this discovery profile?            
        };
    </script>
    <script type="text/javascript">
    //<![CDATA[
        var timeoutHandle;
        var cancelling = false;
        var finishing = false;
        var loadingKey = null;
        var baseUrl = '<%= DefaultSanitizer.SanitizeHtml(this.ResolveClientUrl("~/Orion/Discovery/Default.aspx")) %>';
        var nextStepUrl = '<%= DefaultSanitizer.SanitizeHtml(this.ResolveClientUrl("~/Orion/Discovery/Results/Default.aspx")) %>';
        var orionPageRefreshMilSecs = <%= SolarWinds.Orion.Web.DAL.WebSettingsDAL.AutoRefreshSeconds * 1000 %>;
        var orionPageRefreshTimeout;
        var cancelEnabled = false;
        var aborting = false;

        var ShowProgressDialog = function() {
            if (orionPageRefreshTimeout) clearTimeout(orionPageRefreshTimeout);
            var encodedTitle = $("<div/>").text(IntTM.txt8).html();
            $("#progressDialog").show().dialog({
                width: '550px', height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
            });
            $(".ui-dialog-titlebar-close").unbind('click');
            $(".ui-dialog-titlebar-close").bind('click', CancelDiscovery);
        };

        var StartDiscovery = function() {
            cancelEnabled = false;
            ShowProgressDialog();
            $("#statusText").text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_9) %>");
            PageMethods.StartDiscovery(
                GetDiscoveryProgress,
                function(error) {
                    $("#progressDialog").dialog("close");
                    alert(IntTM.txt1);
                    window.location = window.location;
                });
        };

        var TryCreateScheduledDiscoveryJob = function () {
            PageMethods.TryCreateScheduledDiscoveryJob(
                function() {
                    // Discovery job created successfully, nothing more to do
                },
                function (error) {
                    // Discovery job rescheduler will try to plan job again
                });
        };

        var ContinueDiscovery = function() {
            ShowProgressDialog();
            GetDiscoveryProgress();
        };

        var GetDiscoveryProgress = function() {
            PageMethods.GetDiscoveryProgress(
                OnProgressReceived,
                function(error) {
                    if(aborting) return;
                    alert(IntTM.txt2);
                    window.location = baseUrl;
                });
        };

        var GetLabel = function(status, endpoint, hopNumber) {            
            var description = status.Description;
            switch (status.Phase)
            {
                case 1: //Detection
                    description = "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_10) %>";
                    break;
                case 2: //Inventory
                    description = "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_11) %>";
                    break;
                case 3: //Post-Processing
                    description = "<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_12) %>";
                    break;
            }

            return String.format("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_13) %>", hopNumber, description, endpoint);
        };

        var OnProgressReceived = function(result) {
            if (result == null) {
                timeoutHandle = setTimeout(GetDiscoveryProgress, 1000);
                return;
            }

            if (!result.Starting)
                cancelEnabled = true;

            if (result.Status.Status == 1) { // In progress
                if( result.Status.Phase == 4 ) // Importing in game
                {
                    if(result.ImportProgress != null)
                    {
                        SetProgressBar(1, result.ImportProgress.OverallProgress, 100);
                        SetProgressBar(2, result.ImportProgress.PhaseProgress, 100);
                                                
                        $("#statusText").text(result.ImportProgress.PhaseName);
                    }
                }
                else
                {
                    SetProgressBar(1, result.Primary, result.TotalPrimary);
                    SetProgressBar(2, result.ProcessedElementCount, result.TotalElementCount);
                    if (!result.Starting && !result.CanceledByUser) {
                        $("#statusText").text(GetLabel(result.Status,result.EndPointName,result.HopNumber));//
                    }                  
                    $("#nodes").text(result.DiscoveredNetObjects["Nodes"]);
                    $("#subnets").text(result.DiscoveredNetObjects["Subnets"]);
                }

                //schedule next progress checking 
                timeoutHandle = setTimeout(GetDiscoveryProgress, 1000);
            }            
            else if (result.Status.Status != 1) { // finished or error
                finishing = true;                
                clearTimeout(timeoutHandle);
                $("#statusText").text(IntTM.txt3);
                SetProgressBar(1, 1, 1);
                SetProgressBar(2, 0, 1);

                if( result.Status.Phase == 4 || result.IsAutoImport ) // last phase was import, we won't go again to import results
                {
                    window.location = baseUrl;

                }
                else
                {
                    if (result.Status.Status == 7){ // cancel
                        alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_14) %>');
                    }

                    if (!cancelling && result.Status.Status == 6)
                        alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_15) %>");                                                         

                    LoadDiscoveryResultsActive();
                }
            }
            else {
                alert(IntTM.txt5);
                window.location = baseUrl;
            }
        };

        var LoadDiscoveryResultsActive = function()
        {            
            PageMethods.ProcessResultAsyncActive(                    
                function(key) { 
                    loadingKey = key;

                    // periodicaly ask for progress
                    timeoutHandle = setTimeout(GetLoadDiscoveryStatus, 1000);
                },
                function(error) {
                    alert(IntTM.txt4);
                    window.location = baseUrl;
                });            
        };

        var LoadDiscoveryResults = function(onlyNotImported)        
        {
            Page_ClientValidate(); 

            if(Page_IsValid) 
            {        
                if (orionPageRefreshTimeout) 
                {
                    clearTimeout(orionPageRefreshTimeout);
                }
                                        
                var profileID = $("input[@name=RBGroup]:checked").val();

                PageMethods.ProcessResultAsync(
                        profileID, onlyNotImported, 
                        function(key) { 
                            loadingKey = key;

                            // periodicaly ask for progress
                            timeoutHandle = setTimeout(GetLoadDiscoveryStatus, 1000);
                        },
                        function(error) {
                            alert(IntTM.txt4);
                            window.location = baseUrl;
                        });

                $('.TransparentBoxID').show(); 
            }
        };

        var GetLoadDiscoveryStatus = function() {
            PageMethods.GetLoadDiscoveryStatus(
                loadingKey,
                function(result) { // success       
                                         
                    if( result == true ) {
                        clearTimeout(timeoutHandle);
                        SetProgressBar(2, 1, 1);                         
                                 
                        <%= this.ClientScript.GetPostBackEventReference(RedirectLinkButton, "redirect") %>;                                                                                                                        
                    }
                    else {
                        // ask for next
                        timeoutHandle = setTimeout(GetLoadDiscoveryStatus, 1000);
                    }
                },
                function(error) {
                    alert(IntTM.txt4);
                    window.location = baseUrl;
                });
         };                         

        var SetProgressBar = function(id, value, maxValue) {
            if (value > maxValue) value = maxValue;
            if (value == 0) {
                $("#progressBar" + id).empty();
            }
            else {
                if ($("#progressBar" + id + " img").length == 0) {
                    $("#progressBar" + id).append(CreateBarImage());
                }
                $("#progressBar" + id + " img").width((300 / maxValue) * value);
            }
        };

        var CreateBarImage = function() {
            return $("<img />").attr("src", "/Orion/Discovery/images/progBar_on.gif").height(20);
        };        

        var RunInBackground = function() {
            aborting = true;
            clearTimeout(timeoutHandle);
            window.location = baseUrl;            
        }

        var CancelDiscovery = function() {
            if (!cancelEnabled) {
                alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_16) %>");
                return;
            }
            
            if (cancelling || finishing) return;
            clearTimeout(timeoutHandle);
            if (confirm(IntTM.txt6)) {
                cancelling = true;
                $("#statusText").text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_17) %>");
                PageMethods.CancelDiscovery(
                    function() { // cancel initiated
                        GetDiscoveryProgress();
                    },
                    function(error) {
                        alert(IntTM.txt7);
                        window.location = baseUrl;
                });
            }
            else {
                GetDiscoveryProgress();
            }
        };

        var CloseDiscovery = function() {
            window.location = baseUrl;
        };

        var RefreshPage = function() {
            <%= this.ClientScript.GetPostBackEventReference(this, "", false) %>
        };

        var ValidateProfileSelected = function(source, clientArgs) {
            clientArgs.IsValid = $('.RadioButtons:checked').length==1;
        };

        $(function() {
            orionPageRefreshTimeout = setTimeout(RefreshPage, orionPageRefreshMilSecs);
        });
//]]>
    </script>
    <style type="text/css">
      .x-window.x-window-plain.x-window-dlg {z-index:20001!important;}
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:IconLinkExportToPDF runat="server" />
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHDiscoveryHome" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        .progressLabel
        {
            white-space: nowrap !important;
            width: auto !important;
        }
    </style>
    <asp:LinkButton ID="RedirectLinkButton" runat="server" Visible="false" OnClick="RedirectLinkButton_Click"></asp:LinkButton>
    <div>
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
        <h1 class="sw-hdr-title">
            <%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
        <div id="notExistingProfileWarningDiv" class="sw-suggestion sw-suggestion-warn" style="margin-bottom: 10px;" Visible="False" runat="server">
            <span class="sw-suggestion-icon"></span>
            <%= DefaultSanitizer.SanitizeHtml(CoreWebContent.WEBDATA_PF0_15) %>
        </div>
        
        <orion:LicenseLimitControl runat="server" id="licenseLimitHint" Margin="5px 0px 10px 0px;" />  

        <orion:TBox ID="modalityDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_51 %>"
            DivVisible="false" MessageVisible="true" />
        <div style="width: 100%;">
        </div>
        <discovery:DiscoveryTabBar ID="DiscoveryTabBar" runat="server" Visible="true" />
        <div class="tab-top">
            <table style="border: solid 1px #dfdfde;">
                <tr class="ButtonHeader">
                    <td style="padding-left: 10px;">
                        <span class="toolbarButton">
                            <asp:ImageButton ID="AddDiscoveryButton" runat="server" ImageUrl="~/Orion/Discovery/images/icon_add.gif"
                                OnClick="AddDiscovery_OnClick" CausesValidation="false" />
                            <asp:LinkButton ID="AddDiscoveryLinkButton" runat="server" OnClick="AddDiscovery_OnClick"
                                CausesValidation="false"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_26) %></asp:LinkButton>
                        </span><span class="toolbarButton">
                            <asp:ImageButton ID="DiscoverNowButton" runat="server" ImageUrl="~/Orion/Nodes/images/icons/icon_discover.gif"
                                OnClick="DiscoverNow_OnClick" />
                            <asp:LinkButton ID="DiscoverNowLinkButton" runat="server" OnClick="DiscoverNow_OnClick"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_27) %></asp:LinkButton>
                        </span><span class="toolbarButton">
                            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="~/Orion/Discovery/images/icon_edit.gif"
                                OnClick="Edit_OnClick" />
                            <asp:LinkButton ID="EditLinkButton" runat="server" OnClick="Edit_OnClick"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_28) %></asp:LinkButton>
                        </span><span class="toolbarButton">
                            <asp:ImageButton ID="ResultsButton" runat="server" ImageUrl="~/Orion/Discovery/images/results.gif" 
                                OnClick="Results_OnClick" OnClientClick="LoadDiscoveryResults(false); return false;" />
                            <asp:LinkButton ID="ResultsLinkButton" runat="server" 
                                OnClick="Results_OnClick" OnClientClick="LoadDiscoveryResults(false); return false;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_29) %></asp:LinkButton>
                        </span><span class="toolbarButton">
                            <asp:ImageButton ID="NewResultsButton" runat="server" ImageUrl='<%# DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "Discovery/images/results_new.gif")) %>'
                                OnClick="NewResults_OnClick" OnClientClick="LoadDiscoveryResults(true); return false;" />
                            <asp:LinkButton ID="NewResultsLinkButton" runat="server" OnClick="NewResults_OnClick"
                                OnClientClick="LoadDiscoveryResults(true); return false;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_30) %></asp:LinkButton>
                        </span><span class="toolbarButton">
                            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                                OnClientClick="Page_ClientValidate(); return Page_IsValid && confirm(IntTM.txt9)"
                                OnClick="Delete_OnClick" />
                            <asp:LinkButton ID="DeleteLinkButton" runat="server" OnClientClick="Page_ClientValidate(); return Page_IsValid && confirm(IntTM.txt9)"
                                OnClick="Delete_OnClick"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_31) %></asp:LinkButton>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="ProfilesGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                            AllowSorting="true" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"
                            OnSorting="ProfilesSorting">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="20px">
                                    <ItemTemplate>
                                        <input class="RadioButtons" type="radio" name="RBGroup" value='<%# DefaultSanitizer.SanitizeHtml(Eval("ProfileID")) %>'
                                            <%# DefaultSanitizer.SanitizeHtml(IsChecked((int)Eval("ProfileID"))) %> onclick="Page_ClientValidate();" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_44%>" SortExpression="Name"
                                    ItemStyle-CssClass="wrapCell">
                                    <ItemTemplate>
                                        <asp:Label ID="NameLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Encode(Eval("Name"))) %>' Width="250px" CssClass='<%# DefaultSanitizer.SanitizeHtml(GetWarnCssClass(Eval("EngineExists"))) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_45%>" SortExpression="Description"
                                    ItemStyle-CssClass="wrapCell">
                                    <ItemTemplate>
                                        <asp:Label ID="DescriptionLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Encode(Eval("Description"))) %>'
                                            Width="350px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_46%>" SortExpression="Frequency">
                                    <ItemTemplate>
                                        <asp:Label ID="FrequencyLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Frequency")) %>' Width="150" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_47%>" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="StatusProgress" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(FormatedStatus(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)(int)Eval("Status")))) %>'
                                            Visible='<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) == SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.InProgress) %>'
                                            CommandArgument='<%# DefaultSanitizer.SanitizeHtml(Eval("ProfileID")) %>' OnCommand="ShowProgress_Click" CausesValidation="false"
                                            ForeColor="Navy" />
                                        <a href="#" onclick="alert('<%# Server.HtmlEncode(FormatedStatusDescription(Eval("StatusDescription").ToString())) %>')"
                                            style="color: Red; text-decoration: Underline">
                                            <%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) == SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.Error 
                                            || ((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) == SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.NotCompleted ?
                                               FormatedStatus(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)(int)Eval("Status"))) : String.Empty) %>
                                        </a>
                                        <asp:Label ID="StatusLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(FormatedStatus(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)(int)Eval("Status")))) %>'
                                            Visible='<%# DefaultSanitizer.SanitizeHtml(((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) != SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.InProgress 
                                                      && ((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) != SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.Error 
                                                      && ((SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus)Eval("Status")) != SolarWinds.Orion.Core.Models.Enums.DiscoveryStatus.NotCompleted) %>'
                                            Width="150" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_48%>" SortExpression="LastRun">
                                    <ItemTemplate>
                                        <asp:Label ID="LastRunLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(FormatedLastRun((int)Eval("ProfileID"))) %>'
                                             />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px;">
                        <asp:CustomValidator ID="ProfileSelectedValidator" runat="server" OnServerValidate="ProfileSelectedValidation"
                            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_52%>" EnableClientScript="true"
                            Display="Dynamic" ClientValidationFunction="ValidateProfileSelected">
                        </asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </div>
        <div id="progressDialog">
            <div id="progressContent">
                <div id="statusText">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_32) %></div>
                <table id="progressTable" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="progressLabel">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_244) %>
                        </td>
                        <td colspan="2">
                            <div id="progressBar1" class="progressBar" ></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="progressSeparator">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="progressLabel">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_245) %>
                        </td>
                        <td colspan="2">
                            <div id="progressBar2" class="progressBar" ></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="progressSeparator">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="progressSeparator">
                            &nbsp;
                        </td>
                    </tr>                    
                  <tr>
                        <td class="progressLabel">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_39) %>
                        </td>
                        <td class="progressData progressAltRow" id="nodes">
                            0
                        </td>
                        <td style="width: 149px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="progressLabel">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_40) %>
                        </td>
                        <td class="progressData" id="subnets">
                            0
                        </td>
                        <td style="width: 149px;">
                            &nbsp;
                        </td>
                    </tr>                  
                </table>
                <div style="text-align: right">
                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent,Discovery_Button_RunInBackground %>"
                        CausesValidation="false" OnClientClick="if ($(this)[0].href != undefined) $(this)[0].href='#'; RunInBackground(); return false;" />

                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                        CausesValidation="false" OnClientClick="if ($(this)[0].href != undefined) $(this)[0].href='#'; CancelDiscovery(); return false;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
