﻿(function (root) {

    root.SW = root.SW || {};
    root.SW.Orion = root.SW.Orion || {};
    root.SW.Orion.Discovery = root.SW.Orion.Discovery || {};
    root.SW.Orion.Discovery.Controls = root.SW.Orion.Discovery.Controls || {};
    root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog || {};
    root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.TestSnmpCredentials = function (evt) {


        function bindData(spec, target) {
            for (var propName in spec) {
                if (spec.hasOwnProperty(propName)) {
                    var prop = spec[propName];

                    if (prop && prop.id && prop.bind) {
                        target[prop.bind] = $("#" + prop.id).val();
                    }
                }
            }

        }


        var button = $(evt.target);

        if (button.hasClass("disabled")) {
            return;
        }

        
        var ctrls = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS;
        var successHint = $("#" + ctrls.LOGIN_SUCCEEDED_HINT);
        var failHint = $("#" + ctrls.LOGIN_FAILED_HINT);
        var spinner = $("#" + ctrls.TEST_SPINNER);
        
        var model = {};
        bindData(ctrls, model);

        spinner.hide();
        spinner.removeClass("hidden");



        failHint.hide();
        failHint.removeClass("hidden");

        successHint.hide();
        successHint.removeClass("hidden");
        

        spinner.show();
        button.addClass("disabled");

        PageMethods.TestSnmpCredentials(model,
            function (response) {
                button.removeClass("disabled");

                spinner.hide();

                if (response.Success) {
                    failHint.hide();
                    successHint.show();

                } else {
                    failHint.show();
                    successHint.hide();
                }
            },
            function (response) {
                button.removeClass("disabled");
                spinner.hide();
                failHint.show(); //TODO: think of a better way to reperesent server problem
            });
    };


    var dialogPosition = undefined;

    function InitializeRequest(sender, args) {

        var source = args._postBackElement;

        if (source.id.indexOf("ScanButton") > 0) {
            var ctrls = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS;
            var mainSpinner = $("#" + ctrls.SCAN_SPINNER);
            var errorMessage = $("#" + ctrls.ERROR_MESSAGE_CONTAINER);

            errorMessage.hide();
            mainSpinner.hide();
            mainSpinner.removeClass("hidden");
            mainSpinner.show();



        } else if (source.id.indexOf("TestCredentialsButton") > 0) {

            var ctrls = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS;
            var testSpinner = $("#" + ctrls.TEST_SPINNER);
            var loginSucceededHint = $("#" + ctrls.LOGIN_SUCCEEDED_HINT);
            var loginFailedHint = $("#" + ctrls.LOGIN_FAILED_HINT);

            loginSucceededHint.hide();
            loginFailedHint.hide();

            testSpinner.hide();
            testSpinner.removeClass("hidden");
            testSpinner.show();
        }


        var ctrls = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS;
        var dialog = $("#" + ctrls.DIALOG);
        //dialog.draggable();
        var w = $(window)
        if (!dialogPosition) {
            dialog.show();

            dialogPosition = { top:20, left: ( w.width() / 2 - 391 / 2) };


        } else {

            dialogPosition = dialog.position();
            if (dialogPosition) {
                var offset = window.scrollY || window.pageYOffset;
                dialogPosition.top -= offset;

            }
            
        }
        


    }

    function EndRequest(sender, args) {
        // fix issue when button needs to be clicked twice for postback to occur

        Page_BlockSubmit = false;

        // let user be able to move the dialog window around
        var ctrls = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS;
        var dialog = $("#" + ctrls.DIALOG);

        if (dialog) {
            dialog.draggable({
                handle: ".editor-dialog-header"
            });

            if (dialogPosition == null || dialogPosition == undefined) {
                dialogPosition = { top: 20, left: ($(window).width() / 2 - 391 / 2) };
            }
            try {
                dialog.css({ 'top': dialogPosition.top , 'left': dialogPosition.left });

            } catch (ignore) {
            
            }
        }


        // make sure form's auto complete is turned off

        $('form').attr('autocomplete', 'off');

        var txt = $('#' + ctrls.USERNAME_TEXTBOX.id);
        txt.on('change', function (evt) {
            if (txt.hasClass('sw-validation-error')) {
                txt.removeClass('sw-validation-error');
            }
        });

        var routerInput = $('#' + ctrls.SEED_ROUTER_TEXTBOX.id);
        routerInput.on('change', function (evt) {
            if (routerInput.hasClass('sw-validation-error')) {
                routerInput.removeClass('sw-validation-error');
            }
        });


        var selectAllSubnets = $("#selectAllSubnets");
        var grid = $("#" + ctrls.SUBNETS_GRID_VIEW);

        grid.find("input[type=checkbox]").on("change", function (evt) {
            selectAllSubnets.prop("indeterminate", true);
        });

        selectAllSubnets.on("click", function (evt) {
            var self = $(this);
            var checked = self.attr("checked");
            self.prop("indeterminate", false);

            if (checked) {
                grid.find("input[type=checkbox]").attr("checked", checked);
            } else {
                grid.find("input[type=checkbox]").removeAttr("checked");
            }
        });


    }


    $(document).ready(function () {



        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);

    });


})(window);