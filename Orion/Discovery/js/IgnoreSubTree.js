﻿function InterfaceSelectionChanged(checked, InterfaceID) {
    if (checked) {
        HideWarning();
    }
    ScheduleDiscovery.SelectIgnoredInterface(InterfaceID, checked, OnSucceeded, OnFailed);
}

function VolumeSelectionChanged(checked, VolumeID) {
    if (checked) {
        HideWarning();
    }
    ScheduleDiscovery.SelectIgnoredVolume(VolumeID, checked, OnSucceeded, OnFailed);
}

function ItemSelectionChanged(checked, IgnoredNodeID, GroupID, ItemIndex) {
    if (checked) {
        HideWarning();
    }
    ScheduleDiscovery.SelectIgnoredItem(IgnoredNodeID, GroupID, ItemIndex, checked, OnSucceeded, OnFailed);
}

function OnSucceeded(result) {
    //alert(result);
}

function OnFailed(error) {
    alert(error.get_message());
}