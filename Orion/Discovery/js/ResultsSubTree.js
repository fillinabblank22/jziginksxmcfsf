﻿function InterfaceSelectionChanged(checked, NodeID, ProfileID, InterfaceID) {
    ScheduleDiscovery.SelectInterface(NodeID, ProfileID, InterfaceID, checked, OnSucceeded, OnFailed);
}

function VolumeSelectionChanged(checked, NodeID, ProfileID, VolumeID) {
    ScheduleDiscovery.SelectVolume(NodeID, ProfileID, VolumeID, checked, OnSucceeded, OnFailed);
}

function ItemSelectionChanged(checked, NodeID, ProfileID, GroupID, ItemID) {
    ScheduleDiscovery.SelectGroupItem(NodeID, ProfileID, GroupID, ItemID, checked, OnSucceeded, OnFailed);
}

function OnSucceeded(result) {
    //alert(result);
}

function OnFailed(error) {
    alert(error.get_message());
}