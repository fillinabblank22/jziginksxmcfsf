﻿(function (root) {

    // Consider to make it a utility function
    function isValidIPAddress(value) {
        if (!value) {
            return false;
        }

        var result = true;

        var parts = value.split('.');
        if (parts.length != 4) {
            return false;
        }
        for (var i = 0; i < parts.length; i++) {
            var part = parts[i];
            if (part.trim() == '') {
                result = false;
                break;
            }

            var val = new Number(part);
            if (isNaN(val) || val < 0 || val > 255) {
                result = false;
                break;
            }
        }

        return result;

    }

    function convertIpToInt(input) {
        var parts = input.split('.');
        return ((((+parts[0]) * 256 + (+parts[1])) * 256) + (+parts[2])) * 256 + (+parts[3]);
    }

    function parse(value) {

        if (!value) {
            return undefined;
        }
        var parts = value.split('/');
        if (parts.length != 2) {
            return undefined;
        }

        if (!isValidIPAddress(parts[0])) {
            return undefined;
        }

        var bitCount = new Number(parts[1]);
        if (isNaN(bitCount) || bitCount <= 0 || bitCount > 32) {
            return undefined;
        }

       return { netmask: parts[0], cidr: bitCount };
    }

    function validate(value) {

        var parsed = parse(value);
        return parsed != undefined;
    }

    function validateClass(value) {

        var parsed = parse(value);
        if (!parsed) {
            return false;
        }

        var address = convertIpToInt(parsed.netmask);

        return ((address & 0x80000000) == 0) || ((address & 0x40000000) == 0) || ((address & 0x20000000) == 0);
    }


    root.ValidateCIDR = function (source, args) {
        args.IsValid = validate(args.Value);
        if (!args.IsValid) {


            source.textContent = SW.Orion.Discovery.Controls.SubnetsEditor.ERROR_MESSAGE;
            var input = $(source.previousElementSibling);
            if (!input.hasClass('sw-validation-error')) {
                input.addClass('sw-validation-error');
            }

        } else {
        
            args.IsValid = validateClass(args.Value);

            if (!args.IsValid) {
                source.textContent = SW.Orion.Discovery.Controls.SubnetsEditor.SUBNET_CLASS_ERROR_MESSAGE;
                var input = $(source.previousElementSibling);
                if (!input.hasClass('sw-validation-error')) {
                    input.addClass('sw-validation-error');
                }

            } else {
                source.textContent = "";
                $(source.previousElementSibling).removeClass('sw-validation-error');
            }
       } 
    }


    function InitializeRequest(sender, args) {


    }

    function EndRequest(sender, args) {
        var ctrls = SW.Orion.Discovery.Controls.SubnetsEditor.Controls ;
        var tbody = $('#' + ctrls.GRID).children('tbody');
        try {
            tbody.scrollTop(tbody[0].scrollHeight);
        } catch (error) {
            // forget it
        }
        
    }

    $(document).ready(function () {



        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);
        var ctrls = SW.Orion.Discovery.Controls.SubnetsEditor.Controls;
        var tbody = $('#' + ctrls.GRID).children('tbody');

    });

})(window);