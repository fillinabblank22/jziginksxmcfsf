<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" AutoEventWireup="true"
    CodeFile="Network.aspx.cs" Inherits="Orion_Discovery_Config_Network" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_61 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Web.Discovery" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<%@ Register Src="~/Orion/Discovery/Controls/IPRangesEditor.ascx" TagPrefix="orion" TagName="IPRangesEditor" %>
<%@ Register Src="~/Orion/Discovery/Controls/SubnetsEditor.ascx" TagPrefix="orion" TagName="SubnetsEditor" %>
<%@ Register Src="~/Orion/Discovery/Controls/IPAddressBatchEditor.ascx" TagPrefix="orion" TagName="IPAddressBatchEditor" %>
<%@ Register Src="~/Orion/Discovery/Controls/ActiveDirectoryEditor.ascx" TagPrefix="orion" TagName="ActiveDirectoryEditor" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include ID="Include2" runat="server" File="Discovery/Config/Network.js" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
    <h3 class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_89) %></h3>
    <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_84) %></p>

        <style type="text/css">
            #DiscoveryHintHandle {
                float:right;
            }

            #DiscoveryHintHandle img {
              width: 24px;
              height: 17px;
            }

                #DiscoveryHintHandle a, #DiscoveryHintHandle a:hover, #DiscoveryHintHandle a:active, #DiscoveryHintHandle a:visited {
                    color: #2E96B9;
                    text-decoration:none;
                }
            #DiscoveryHintHandle span {  
                position: relative;
                top: -4px;
            }

            #DiscoveryHint {
              background-color: #F0F8FA;
              height: 160px;
              padding-left: 16px;
              padding-top: 12px;
              padding-right: 12px;
              margin-top:12px;
            }

            div.header {
                clear:both;
            }

            div.header img {
              width: 35px;
              height: 26px;
            }

            div.header span {
                position: relative;
                top: -10px;
                left:12px;
                font-weight: 700;
                font-style: normal;
                font-size: 12px;
                color: #2E96B9;
            }

            div.content {
              margin-top: -12px;
              }

        div.content > div, div.content > div > p {
            display:inline-block;
        }

            div.tip, div.trunk, div.forked {
                display:inline-block;
            }

            div.tree-left, div.tree-right {
               width: 20px;
            }

            div.tree-left {
            margin-left:20px;
            }

        div.tip img {
            transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            width: 1px;
            height: 20px;
            position: relative;
            top: -20px;
            left: -5px;
        }

        div.trunk img {
            display:block;
            width:1px;
            height:30px;
        }

        div.forked img {
            display:block;
            transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            width: 1px;
            height: 20px;
            position:relative;
            left: 5px;
        }

        div.forked img:first-of-type {
            top: -30px;
        }

        div.forked img:last-of-type {
            top: 10px;
        }

        .tree-right div.trunk img {
            position:relative;
            left:10px;
        }

        .tree-right div.tip img {
            position:relative;
            left:15px;
        }
            div.left-text {
              width: 100px;
              text-transform: uppercase;
              text-align: left;
              position: relative;
              top: -24px;
              padding-left: 50px;
              padding-right: 40px;
            }
            div.middle-text {
              width: 260px;
              position: relative;
              top: 12px;
              padding-right: 30px;
            }

            div.right-text {
              width: 220px;
              padding-left: 20px;
              position: relative;
            }

            div.content strong {
                font-weight:bold;
            }

            div.content span.divider {
                display:block;
                text-transform:uppercase;
                text-align:center;
            }

            div.icons {
                width: 40px;
                margin-left: 20px;
            }

            img.network {
                position: relative;
                top: -6px;
            }

            img.network-small {
              position: relative;
              top: -18px;
              left: 20px;
              width: 16px;
              height: 16px;
            }

            img.ip {
              position: relative;
              top: 22px;
              left: -16px;
              width: 20px;
              height: 20px;
              transform: rotate(-45deg);
              -ms-transform: rotate(-45deg);
            }

            img.ip-small {
            position: relative;
              top: 15px;
              left: 18px;
              width: 16px;
              height: 16px;
            }
            
            img.ip-smaller {
              position: relative;
              top: 20px;
              left: -18px;
              width: 14px;
              height: 14px;
              transform: rotate(30deg);
              -ms-transform: rotate(30deg);
            }

            a.close {
              background-image: url('../images/remove_Blue.png');
              height: 25px;
              width: 25px;
              display: inline-block;
              text-decoration: initial !important;
              float: right;
              position: relative;
              top: -4px;
              left: 0px;
            }
    </style>
    <script type="text/javascript">
    (function(window) {
        var showHint = <%= this.ShowHint ? "true" : "false" %>;
        var settingName = "<%= this.ShowDiscoveryHintSettingName %>";
        var webService = "/Orion/Services/DiscoveryWizard.asmx";

        function toggleDiscoveryHint(success, failure) {
            failure = failure || function () { };
            ORION.callWebService(webService, "ToggleDiscoveryHint", {"settingName": settingName}, success, failure);
        }

        $(document).ready(function () {
            var hint = $("#DiscoveryHint");
            var hintHandle = $('#DiscoveryHintHandle');
            hint.hide();
            hintHandle.hide();
            hint.removeClass('hidden');
            hintHandle.removeClass('hidden');

            hint.find("a.close").on("click", function (evt) {

                toggleDiscoveryHint(function () {
                    hint.hide();
                    hintHandle.show();
                });
            });

            hintHandle.on("click", function (evt) {
                toggleDiscoveryHint(function () {
                    hint.show();
                    hintHandle.hide();
                });
            });

            if (showHint) {
                hintHandle.hide();
                hint.show();
            } else {
                hintHandle.show();
                hint.hide();
            }

            SW.Orion.Discovery.SetEngineData('<%= enginesHidden.ClientID %>', '<%= initialValue.ClientID %>');
            });
    })(window);
    </script>
    
    <input type="hidden" runat="server" ID="enginesHidden"/>
    <input type="hidden" runat="server" ID="initialValue"/>
    <div id="DiscoveryHintHandle" class="hidden" >
        <a href="#"><img src="../images/Hat.png" />
        <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_85) %></span>
        </a>
    </div>

    <div id="DiscoveryHint" class="hidden"> <!-- Discovery hint  -->
        <div class="header"> <!-- header -->
            <img src="../images/Hat.png" />
            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_85) %></span> <!-- Using discovery for the first time? -->

            <a href="#" class="close" >&nbsp;</a>

        </div>
        <div class="content">
            <div class="left-text">
                <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_86) %></p> <!-- We recommend scanning... -->
            </div>
            <div class="tree-left">
                <div class="tip"> <!-- Images -->
                    <img src="../images/vert_line_01.png" />

                </div>
                <div class="trunk">
                    <img src="../images/vert_line_01.png" />
                    <img src="../images/vert_line_01.png" />
                </div>
                <div class="forked">
                    <img src="../images/vert_line_01.png" />
                    <img src="../images/vert_line_01.png" />
                </div>
            </div>
            <div class="icons">
                <img class="network" src="../images/NetworkIcon.png" />
                <img class="network-small" src="../images/NetworkIcon.png" />
                <img class="ip" src="../images/IP_Icon.png" />
                <img class="ip-small" src="../images/IP_Icon.png" />
                <img class="ip-smaller" src="../images/IP_Icon.png" />
            </div>
            <div class="middle-text"><p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_88) %></p> </div>
            <div class="tree-right">

                <div class="forked">
                    <img src="../images/vert_line_01.png" />
                    <img src="../images/vert_line_01.png" />
                </div>

                <div class="trunk">
                    <img src="../images/vert_line_01.png" />
                    <img src="../images/vert_line_01.png" />
                </div>
                <div class="tip"> <!-- Images -->
                    <img src="../images/vert_line_01.png" />
                </div>
            </div>
            <div class="right-text">
                <p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_89) %></p>
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="EnginesPanel" ClientIDMode="Static">

            <div>
                <label><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_96) %></label>
<%--                <a href="#" class="info-tooltip">
                    <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
                    <span>
                        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_17%>"></asp:Literal>
                    </span>
                </a>--%>
            </div>

        <asp:DropDownList ID="EngineSelect" runat="server" DataValueField="EngineID" onchange="return SW.Orion.Discovery.EngineChanged(this);" DataTextField="ServerName" OnSelectedIndexChanged="EngineSelect_SelectedIndexChanged"/>
    </asp:Panel>

    <div class="panel" id="discoveryInputPanel">
        
        <ul class="stack">
            <li><orion:IPRangesEditor ID="IPRangesEditor" runat="server" /></li>
            <li><orion:SubnetsEditor ID="SubnetsEditor" runat="server" /></li>
            <li><orion:IPAddressBatchEditor ID="IPAddressesBatchEditor" runat="server" /></li>
            <li><orion:ActiveDirectoryEditor ID="ActiveDirectoryEditor" runat="server" /></li>
        </ul>

    </div>

    <asp:UpdatePanel runat="server" ID ="NavButtonsUpdatePanel">
        <ContentTemplate>
            <table class="NavigationButtons">
                <tr>
                    <td>
                        <div class="sw-btn-bar-wizard">
                            <asp:CustomValidator runat="server" ID="DiscoveryConfigurationNotEmpty" CssClass="sw-validation-error" OnServerValidate="DiscoveryConfigurationNotEmpty_ServerValidate" Display="Dynamic" ValidationGroup="ConfigValidation" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_102 %>"></asp:CustomValidator>
                            <asp:LinkButton ID="imgbNext" runat="server" OnClick="imgbNext_Click" CausesValidation="true" ValidationGroup="ConfigValidation" CssClass="sw-btn sw-btn-primary"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_94) %></asp:LinkButton>
                            <asp:LinkButton ID="imgbCancel" runat="server"  OnClick="imgbCancel_Click" CausesValidation="false" CssClass="sw-btn sw-btn-secondary" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_95) %></asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>

        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imgbNext" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
