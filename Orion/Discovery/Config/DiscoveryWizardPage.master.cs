using System;
using System.Web.UI;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.UI;


public partial class Orion_Discovery_Config_DiscoveryWizardPage : MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckRights();

        //TODO odl discovery - remove comments to prevent uncessaary loading
        //if (ConfigWorkflowHelper.Steps.Count == 0)
        //{
            ConfigWorkflowHelper.LoadWizardSteps(new DiscoveryConfigWizardStepsManager());
        //}
        this.ProgressIndicator1.Reload();

        AdminSiteMapRenderer.OverrideBreadcrumbURL = "~/Orion/Admin/DiscoveryCentral.aspx";
    }

    // <summary> Check user rights. If they are not sufficient, then redirects to error page. </summary>
    private void CheckRights()
    {
        if (OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_AK0_17), Title = Resources.CoreWebContent.WEBDATA_VB0_567 }); 
        }
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
