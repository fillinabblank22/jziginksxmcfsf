<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master"
    AutoEventWireup="true" CodeFile="DiscoveryScheduling.aspx.cs" Inherits="Orion_Discovery_Config_DiscoveryScheduling"
    Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_61 %>" %>
    
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register TagPrefix="orion" TagName="FrequencyControl" Src="~/Orion/Controls/FrequencyControl.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:include file="ReportSchedule.css" runat="server" />
    <style type="text/css">
        .border
        {
            padding: 20px;
            display: inline;
        }
        .pLeft
        {
            padding: 0px;
            padding-left: 10px;
        }
        .width150
        {
            width: 150px;
        }
        .invisible
        {
            display: none;
        }
        .alignedRight
        {
            text-align: right;
        }
        .discovery_canceling
        {
        	padding: 5px 5px 5px 5px;
        }
        .discovery_canceling h3
        {
            background-image: url(/Orion/images/AJAX-Loader.gif);
            background-position: left center;
            background-repeat: no-repeat;
            padding: 4px 0 4px 40px;
        }
    </style>
    <script type="text/javascript">
        var submitted = false;

        var PreventMultiSubmit = function() {
            return !submitted && (submitted = true);
        };

        var RefreshSubmittedFlag = function() {
            submitted = false;
        }

        $(function (){  
            $("input[name*='discoveryNow']").click(SetExecImmediatelyButtons);
            $("input[id$='txtTimePicker']").change(RefreshSubmittedFlag);
        });
        
        var FrequencyOnChange = function() {
			RefreshSubmittedFlag();
            switch (document.getElementById("<%=this.frequency.ClientID%>").value) {
            case "Once":
                document.getElementById("hourlyDiv").style.display = "none";
                document.getElementById("hourTextBoxValidation").style.display = "none";
                document.getElementById("dailyDiv").style.display = "none";
                document.getElementById("<%=this.ScheduleTime.TimeValidatorClientId%>").enabled = false;
                document.getElementById("cronDiv").style.display = "none";
                document.getElementById("freqtr").style.display = "table-row";
                break;
            case "Custom":
                document.getElementById("hourlyDiv").style.display = "inline";
                document.getElementById("hourTextBoxValidation").style.display = "inline";
                document.getElementById("dailyDiv").style.display = "none";
                document.getElementById("<%=this.ScheduleTime.TimeValidatorClientId%>").enabled = false;
                document.getElementById("cronDiv").style.display = "none";
                document.getElementById("freqtr").style.display = "table-row";
                break;
            case "Daily":
                document.getElementById("hourlyDiv").style.display = "none";
                document.getElementById("hourTextBoxValidation").style.display = "none";
                document.getElementById("dailyDiv").style.display = "inline";
                document.getElementById("<%=this.ScheduleTime.TimeValidatorClientId%>").enabled = true;
                document.getElementById("cronDiv").style.display = "none";
                document.getElementById("freqtr").style.display = "table-row";
                break;
            case "Cron":
                document.getElementById("hourlyDiv").style.display = "none";
                document.getElementById("hourTextBoxValidation").style.display = "none";
                document.getElementById("dailyDiv").style.display = "none";
                document.getElementById("<%=this.ScheduleTime.TimeValidatorClientId%>").enabled = false;
                document.getElementById("cronDiv").style.display = "inline";
                document.getElementById("freqtr").style.display = "none";
                   
                var frequency = <%= SchedulesListJson %>;                   
                SW.Orion.FrequencyControllerDefinition = new SW.Orion.FrequencyController({
                    container: "#createSchedule",
                    frequencies: frequency,
                    useTimePeriodMode: false, 
                    singleScheduleMode: true,
                });
                SW.Orion.FrequencyControllerDefinition.init();
                break;
            }

            SetExecImmediatelyButtons();
        };
        
        var SetExecImmediatelyButtons = function() {
            $('#<%=this.ScheduleButton.ClientID%>, #<%=this.DiscoverButton.ClientID%>, #<%=this.StoreButton.ClientID%>').hide();
            var radioButton =$("input[name*='discoveryNow']:checked");
            var sel = document.getElementById("<%=this.frequency.ClientID%>").value;
            
            if(radioButton.val()=="discoverNow" && sel != "Cron")
            {
                $('#<%=this.DiscoverButton.ClientID%>').show();
            }   
            else
            {
                if (sel == "Once"){
                    $('#<%=this.StoreButton.ClientID%>').show();
                }
                else {
                    $('#<%=this.ScheduleButton.ClientID%>').show();
                }
                $('#<%=this.DiscoverButton.ClientID%>').hide();
            }                    
        };       

        var ShowConfirmation = function() {
            FrequencyOnChange();
            if (confirm('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_5) %>') == true) {
                document.getElementById("<%=this.DialogResult.ClientID%>").value = 'true';
            }
            else {
                document.getElementById("<%=this.DialogResult.ClientID%>").value = 'false';
            }

            <%= PostBackStr %>
        };
        
        var ProfileID;
        var WaitForCancelDiscovery = function(profileID) {

            FrequencyOnChange();
            ProfileID= profileID;                        
            PageMethods.IsDiscoveryRunning(ProfileID,OnSuceed);
            // TODO: [localization] {comment out}   
            //alert("We are waiting for cancel discovery ... here will be some waiting progress with checking current status by web method: IsDiscoveryRunning(int profileID). ProfileID: " + profileID.toString());            
        };
        
        var OnSuceed = function(result) {
            
            //alert(result);            
            if(result==false)
            {            
                document.getElementById("<%=this.DialogResult.ClientID%>").value = 'true';
                <%= PostBackStr %>
            }
            else
            {
                setTimeout('PageMethods.IsDiscoveryRunning(ProfileID,OnSuceed)',500);
            }
        };
    </script>        
    <orion:tbox id="modalityDiv" runat="server" />
    <asp:Panel ID="CredEdit" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel ProgressPanel">        
        <div class="discovery_canceling">
            <h3 class="discovery_canceling">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_104) %></h3>
        </div>
    </asp:Panel>
    <asp:HiddenField ID="DialogResult" runat="server" Value="" />
    <div class="ActionName">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_105) %></div>
    <div class="TextWithSpaceAfter">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_106) %>
    </div>
    <div class="blueBox">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right; width: 25%">
                        <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_112) %></b>
                    </td>
                    <td style="text-align: left;">
                        <select id="frequency" name="frequency" runat="server" style="min-width: 100px" onchange="FrequencyOnChange();">
                            <option value="Once" text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_113 %>" />
                            <option value="Custom" text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_TM0_308 %>" />
                            <option value="Daily" text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AK0_115 %>" />
                            <option value="Cron" text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TM0_216 %>" />
                        </select>
                        <%--// TODO: [localization] {Huge logic for constructig text is temporary ignored}--%>
                        <div id="hourlyDiv">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_116) %>
                            <asp:TextBox ID="HoursTextBox" runat="server" MaxLength="4" Text="6" Width="25"
                                        CssClass="alignedRight"/>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_117) %>
                            <div class="debugMode" style="display: none">
                                <asp:DropDownList ID="MinutesDropDown" runat="server">
                                    <asp:ListItem>00</asp:ListItem>
                                    <asp:ListItem>02</asp:ListItem>
                                    <asp:ListItem>05</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>45</asp:ListItem>
                                </asp:DropDownList>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_48) %>
                                <div id="hourTextBoxValidation">
                                <asp:RangeValidator ID="HoursRangeValidator" runat="server" Type="Integer" 
                                MinimumValue="0" MaximumValue="3600" ControlToValidate="HoursTextBox" ValidationGroup="Schedule" 
                                ErrorMessage="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_AB0_73 %>" />
                                </div>
                            </div>
                        </div>
                        <div id="dailyDiv">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_118) %>
                            <orion:datetimepicker runat="server" id="ScheduleTime" datepickervisibility="false" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="cronDiv" style="padding: 10px 10px 0 10px; width: 100%">
                            <asp:Panel ID="FreqPicker" runat="server">
                                <div class="GroupBox" style="width: 100% !important">
                                    <div id="createSchedule" style="padding: 10px 0;">
                                        <orion:frequencycontrol runat="server" id="FrequencyControl" singleschedulemode="True" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr id="freqtr">
                    <td style="text-align: right; width: 25%">
                        <b>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_119) %></b>
                    </td>
                    <td>
                        <%--<input runat="server" id="executeImediatelyCB" type="checkbox" onchange="SetExecImediatelyButtons();" checked="checked" />--%>
                        <div>
                            <input id="runNowRB" runat="server" name="discoveryNow" value="discoverNow" type="radio"
                                title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_108 %>" checked="true" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_120) %>
                        </div>
                        <div>
                            <input id="dontRunNowRB" runat="server" name="discoveryNow" value="discoverLater"
                                type="radio" title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_109 %>" />
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_121) %>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    <table class="NavigationButtons">
        <tr>
            <td>
    <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                
                <orion:LocalizableButton id="DiscoverButton" runat="server" DisplayType="Primary"  LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_123 %>" OnClientClick="return PreventMultiSubmit();" OnClick="Discover_Click"/>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmDiscoverButton" runat="server" 
                        TargetControlID="DiscoverButton"                        
                        ConfirmText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_110 %>"
                        />                         
                                
                <orion:LocalizableButton id="ScheduleButton" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_122 %>" OnClientClick="return PreventMultiSubmit();" OnClick="Schedule_Click" ValidationGroup="Schedule" CausesValidation="True"/>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmScheduleButton" runat="server" 
                        TargetControlID="ScheduleButton"
                        ConfirmText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_111 %>"
                        />
                
                <orion:LocalizableButton id="StoreButton" runat="server" DisplayType="Primary" LocalizedText="Save" OnClientClick="return PreventMultiSubmit();" OnClick="Schedule_Click"/>
                <ajaxToolkit:ConfirmButtonExtender ID="ConfirmStoreButton" runat="server" 
                        TargetControlID="StoreButton"
                        ConfirmText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_111 %>"
                        />         
                              
                <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
    </div>            
            </td>
        </tr>
    </table>
</asp:Content>
