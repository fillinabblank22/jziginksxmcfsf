(function(root) {
    root.SW = root.SW || {};
    root.SW.Orion = root.SW.Orion || {};
    root.SW.Orion.Discovery = root.SW.Orion.Discovery || {};

    var hiddenID = '';
    var enginesDDLId = '';

    SW.Orion.Discovery.SetEngineData = function(hidID, engId) {
        hiddenID = hidID;
        enginesDDLId = engId;
    };

    SW.Orion.Discovery.EngineChanged = function(ddlId) {
                var controlName = document.getElementById(ddlId.id);
                var data = document.getElementById(hiddenID).value;
                var initial = document.getElementById(enginesDDLId).value;

                var obj = JSON.parse(data);

                var from = obj[initial];
                var to = obj[controlName.value];
                
                if (from.length != to.length) {
                    Ext.Msg.show({
                        title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}',
                        msg: '@{R=Core.Strings.2;K=WEBJS_SO0_8;E=js}',
                        buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                        icon: Ext.Msg.Warning,
                        fn: function(button) {
                            if (button == "yes") {
                                return true;
                            } else {
                                controlName.value = initial;
                                return false;
                            }
                        }
                    });
                }

                for (var index = 0; index < from.length; index++) {
                    if (to.indexOf(from[index]) == -1) {
                        Ext.Msg.show({
                            title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}',
                            msg: '@{R=Core.Strings.2;K=WEBJS_SO0_8;E=js}',
                            buttons: { yes: '@{R=Core.Strings;K=CommonButtonType_Ok; E=js}', cancel: '@{R=Core.Strings;K=CommonButtonType_Cancel; E=js}' },
                            icon: Ext.Msg.Warning,
                            fn: function(button) {
                                if (button == "yes") {
                                    return true;
                                } else {
                                    controlName.value = initial;
                                    return false;
                                }
                            }
                        });
                    }
                }

                return true;
        };
})(window)