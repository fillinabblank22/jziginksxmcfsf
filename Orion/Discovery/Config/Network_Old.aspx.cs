using System;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Models.Discovery;

public partial class Orion_Discovery_Config_Network_Old : ConfigWizardBasePage, IStep, IPostBackEventHandler
{

    public struct HostnamesValidationInfo
    {
        public int StartLine { get; set; }
        public string ResultText { get; set; }
        public int RemainingNodes { get; set; }
    }
    
	//	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	protected DiscoverySelectionMethod SelectionMethod
	{
		get
		{
            return ConfigWorkflowHelper.DiscoveryConfigurationInfo.SelectionMethod;
		}
		set
		{
            ConfigWorkflowHelper.DiscoveryConfigurationInfo.SelectionMethod = value;
		}
	}

	private System.Web.UI.WebControls.MenuItem lastMenuItem;
    private CoreDiscoveryPluginConfiguration configuration;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        InitButtons(imgbCancel);

        DiscoveryConfiguration discoveryConfiguration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        if (discoveryConfiguration == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_348);
        }
        configuration = discoveryConfiguration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (configuration == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_348);
        }

		if (!IsPostBack)
		{
			CreatePageMenu();

			//sets visibility according to session (IPRange is default)
			this.ShowSelectionMethod();
			//sets appropriate menu item
			foreach (System.Web.UI.WebControls.MenuItem item in this.NetworkPageMenu.Items)
			{
				if (item.Value == this.SelectionMethod.ToString())
				{
					item.Selected = true;
					break;
				}
			}
		}

		this.lastMenuItem = this.NetworkPageMenu.SelectedItem;
	}

    /// <summary> Called when left menu item is clicked </summary>
    protected void NetworkPageMenu_Clicked(object sender, MenuEventArgs e)
    {
		if (e.Item.Value == this.lastMenuItem.Value)
		{
			return;
		}

		this.Validate();
		if (!this.IsValid)
		{
			this.lastMenuItem.Selected = true;
			return;
		}

		this.SelectionMethod = (DiscoverySelectionMethod)Enum.Parse(typeof(DiscoverySelectionMethod), e.Item.Value);
		this.ShowSelectionMethod();

//        this.ClientScript.RegisterClientScriptBlock(typeof(Orion_Discovery_Wizard_Network),"confirmtabchange",
//@"<script type=""text/javascript"">
//$(function() {
//	if(confirm('Are you sure?'))
//	{
//		" + this.ClientScript.GetPostBackEventReference(this, e.Item.Value) +@"
//	}
//}); 
//</script>");

//        this.lastMenuItem.Selected = true;
	}

	public void RaisePostBackEvent(string argument)
	{
		foreach (System.Web.UI.WebControls.MenuItem item in this.NetworkPageMenu.Items)
		{
			if (item.Value == argument)
			{
				item.Selected = true;
				this.SelectionMethod = (DiscoverySelectionMethod)Enum.Parse(typeof(DiscoverySelectionMethod), argument);
				this.ShowSelectionMethod();
				break;
			}
		}
	}

	protected void ShowSelectionMethod()
	{
		DiscoverySelectionMethod method = this.SelectionMethod;
		ipRangeListTd.Visible = (method == DiscoverySelectionMethod.IPRanges);
		subnetListTd.Visible = (method == DiscoverySelectionMethod.Subnets);
		bulkUploadTd.Visible = (method == DiscoverySelectionMethod.SpecificNodes);
        activeDirectoryList.Visible = (method == DiscoverySelectionMethod.ActiveDirectory);
	}

    /// <summary> </summary>
    private void CreatePageMenu()
    {
        NetworkPageMenu.Title.TextLabel.Text = Resources.CoreWebContent.WEBCODE_AK0_55;
        NetworkPageMenu.Title.CSSClass =  "SelMeth";
        NetworkPageMenu.Title.TextBackImageUrl = "/Orion/Discovery/images/bkgd_selectionMethod.gif";

		NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem(Resources.CoreWebContent.WEBCODE_AK0_24, DiscoverySelectionMethod.IPRanges.ToString()));
		NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem(Resources.CoreWebContent.WEBCODE_AK0_26, DiscoverySelectionMethod.Subnets.ToString()));
		NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem(Resources.CoreWebContent.WEBCODE_AK0_25, DiscoverySelectionMethod.SpecificNodes.ToString()));
        NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem("AD Computers", DiscoverySelectionMethod.ActiveDirectory.ToString()));

        // TODO: [localization] {comment out} 
		//NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem("Network Neighborhood"));
        //NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem("DNS Zone"));

        NetworkPageMenu.Items[0].Selected = true;
    }



    /// <summary> Stores hostnames from UI textbox to the session </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static void StoreHostnames(string bulkText)
    {
        try
        {
            Orion_Discovery_Wizard_Controls_BulkUpload.FillBulkUploadListFromContent(bulkText);
        }
        catch (Exception ex)
        {
            log.Error("Error in StoreHostnames method", ex);
        }
    }

    /// <summary> Validates part of bulk list. <see cref="Orion_Discovery_Wizard_Controls_BulkUpload.ValidateUserInput"/> </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static HostnamesValidationInfo ProcessValidation(int startLine)
    {
        string message = null;
        try
        {
            message = Orion_Discovery_Wizard_Controls_BulkUpload.ValidateUserInput(startLine);
        }
        catch (Exception ex)
        {
            log.Error("Error in ProcessValidation method", ex);
        }
         
        return new HostnamesValidationInfo()
        {
               ResultText = message,
               StartLine = startLine + Orion_Discovery_Wizard_Controls_BulkUpload.BULK_VALIDATION_BATCH,
               //RemainingNodes = ConfigWorkflowHelper.DiscoveryConfigurationInfo.BulkList.Count - startLine
               RemainingNodes = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList.Count - startLine
        };
        

    }


    /// <summary> </summary>
    protected override bool ValidateUserInput()
	{
        // If discovery contains agents, user does not have to enter any other nodes if he does not want to
        if (configuration.DiscoverAgentNodes)
            return true;

		switch (this.SelectionMethod)
		{
			case DiscoverySelectionMethod.SpecificNodes:
                return bulkUpload.FillBulkUploadList();
			case DiscoverySelectionMethod.IPRanges:
				return ipRangeList.CheckEmpty();
			case DiscoverySelectionMethod.Subnets:
				return subnetList.CheckEmpty();
            case DiscoverySelectionMethod.ActiveDirectory:
                return activeDirectoryList.CheckEmpty();
			default:
				return true;
		}
	}

    protected override void Next()
	{
        this.bulkUpload.FillBulkUploadList();
		ipRangeList.DeleteEmptyRows();
    }
  

	#region IStep Members
	public string Step
	{
		get
		{
			return "Network_Old";
		}
	}
	#endregion
    
}
