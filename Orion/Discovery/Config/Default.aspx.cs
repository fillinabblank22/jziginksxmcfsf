﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Discovery_Config_Default : ConfigWizardBasePage, IStep, IPostBackEventHandler
{
    protected readonly string ShowDiscoveryHintSettingName = "ShowDiscoveryHint";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            optOutToggle.Visible = stickyPad.Visible = ((Request.UrlReferrer != null) &&
                                    (
                                        Request.UrlReferrer.LocalPath.Equals("/Orion/Login.aspx",
                                            StringComparison.InvariantCultureIgnoreCase)
                                        ||
                                        Request.UrlReferrer.LocalPath.Equals("/Orion/Discovery/Config/Network.aspx",
                                            StringComparison.InvariantCultureIgnoreCase)
                                        ));
        }
    }

    public bool ShowHint
    {
        get
        {
            var value = WebUserSettingsDAL.Get(ShowDiscoveryHintSettingName);
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }

            return Convert.ToBoolean(value);
        }
    }

    public void RaisePostBackEvent(string eventArgument)
	{
	}

	public string Step
	{
		get { return "Welcome"; }
	}

	protected override void Next()
	{
		if (!ValidateUserInput())
		{
			return;
		}
	}

    protected void DoNotRedirectToDiscoveryWizard_CheckedChanged(object sender, EventArgs e)
    {
        WebUserSettingsDAL.Set("DoNotRedirectToDiscoveryWizard", DoNotRedirectToDiscoveryWizard.Checked.ToString());
    }

}