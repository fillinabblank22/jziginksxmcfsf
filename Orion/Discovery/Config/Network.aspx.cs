using System;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Web.Script.Services;
using SolarWinds.Orion.Core.Models.Discovery;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Discovery_Config_Network : ConfigWizardBasePage, IStep, IPostBackEventHandler
{
    private const uint DefaultSnmpPort = 161;
    private const string OrionHSBServerType = "Hot-Standby";
    protected readonly string ShowDiscoveryHintSettingName = "ShowDiscoveryHint";

    public struct HostnamesValidationInfo
    {
        public int StartLine { get; set; }
        public string ResultText { get; set; }
        public int RemainingNodes { get; set; }
        public bool HasUnresolvedNodes { get; set; }
    }

    public class EngineInfo {
        public string ServerName { get; set; }
        public int EngineID { get; set; }
    }

    public class TestSnmpCredentialsModel {
        public string SNMPVersion { get; set; }
        public string CommunityString { get; set; }

        public string IPAddress { get; set; }

        public string AuthKeyPassword { get; set; }

        public string SNMPv3AuthType { get;set;}

        public string SNMPv3PrivacyType { get; set; }

        public SNMPVersion GetSNMPVersion() {
            if (string.IsNullOrWhiteSpace(SNMPVersion)) {
                return SolarWinds.Orion.Core.Common.Models.SNMPVersion.None;
            }
            return (SNMPVersion)Enum.Parse(typeof(SNMPVersion), SNMPVersion);
        }

        public SNMPv3AuthType GetSNMPAuthType() {
            if (string.IsNullOrWhiteSpace(SNMPv3AuthType)) {
                return SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None;
            }

            return (SNMPv3AuthType)Enum.Parse(typeof(SNMPv3AuthType), SNMPv3AuthType);
        }

        public SNMPv3PrivacyType GetSNMPv3PrivacyType()
        {
            if (string.IsNullOrWhiteSpace(SNMPv3PrivacyType)) {
                return SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None;
            }

            return (SNMPv3PrivacyType)Enum.Parse(typeof(SNMPv3PrivacyType), SNMPv3PrivacyType);
        }

        public string PrivacyKeyPassword { get; set; }

        public string SNMPv3Context { get; set; }

        public string SNMPv3Username { get; set; }

        public bool AuthKeyIsPwd { get; set; }

        public bool PrivKeyIsPwd { get; set; }
    }

    public class TestSnmpCredentialsResult {
        public bool Success { get; set; }
        public string Error { get; set; }
    }

	//	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	protected DiscoverySelectionMethod SelectionMethod
	{
		get
		{
            return ConfigWorkflowHelper.DiscoveryConfigurationInfo.SelectionMethod;
		}
		set
		{
            ConfigWorkflowHelper.DiscoveryConfigurationInfo.SelectionMethod = value;
		}
	}

	private System.Web.UI.WebControls.MenuItem lastMenuItem;
    private CoreDiscoveryPluginConfiguration configuration;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        IPRangesEditor.AddUpdateTrigger(imgbNext);
        SubnetsEditor.AddUpdateTrigger(imgbNext);
        IPAddressesBatchEditor.AddUpdateTrigger(imgbNext);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        InitButtons(imgbCancel);

        var sm = ScriptManager.GetCurrent(this);
        sm.RegisterAsyncPostBackControl(imgbNext);

        DiscoveryConfiguration discoveryConfiguration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        if (discoveryConfiguration == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_348);
        }
        configuration = discoveryConfiguration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (configuration == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_348);
        }

		if (!IsPostBack)
		{
            var engines = GetEngines();
            EngineSelect.DataSource = engines;
            EngineSelect.DataBind();
            EnginesPanel.Visible = engines.Count > 1;

		    if (engines.Count > 0)
		    {
		        EngineSelect.SelectedValue = ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineId.ToString();
                enginesHidden.Value = DictionaryToJson(new ModuleGlobalDAL().GetAllModules(true));
		    }

            initialValue.Value = ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineId.ToString();
		}
	}

    string DictionaryToJson(Dictionary<int, List<string>> dict)
    {
        var entries = dict.Select(d =>
            string.Format("\"{0}\": [\"{1}\"]", d.Key, string.Join("\",\"", d.Value )));
        return "{" + string.Join(",", entries) + "}";
    }

    /// <summary> Stores hostnames from UI textbox to the session </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static void StoreHostnames(string bulkText)
    {
        try
        {
            var items = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration
            <SolarWinds.Orion.Core.Models.Discovery.CoreDiscoveryPluginConfiguration>().BulkList;
            items.Clear();

            if (string.IsNullOrWhiteSpace(bulkText)) {
                return;
            }

            var lines = new List<string>();
            StringReader reader = new StringReader(bulkText);
            for (;;) {
                var line = reader.ReadLine();

                if (line == null) {
                    break;
                }
                if (!string.IsNullOrWhiteSpace(line)) {
                    lines.Add(line);
                }
            }

            items.AddRange(lines.Distinct(new EqualityComparer()));
        }
        catch (Exception ex)
        {
            log.Error("Error in StoreHostnames method", ex);
        }
    }
    
    const int BULK_VALIDATION_BATCH = 5;

    /// <summary> Validates part of bulk list. <see cref="Orion_Discovery_Wizard_Controls_BulkUpload.ValidateUserInput"/> </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static HostnamesValidationInfo ProcessValidation(int startLine)
    {
        string message = null;
        var remainingNodes = 0;
        bool hasUnresolvedNodes = false;
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        try
        {
            if ((ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList.Count == 0))
            {
                message = Resources.CoreWebContent.WEBCODE_AK0_27;
            }
            else
            {
                try
                {
                    using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineID))
                    {
                        List<string> subBulkList = new List<string>(ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList.Skip(startLine));
                        var validate = subBulkList.Take(BULK_VALIDATION_BATCH).ToList();

                        if (validate.Count > 0) {

                            message = businessProxy.ValidateBulkList(validate);
                            hasUnresolvedNodes = !string.IsNullOrEmpty(message);
                        }

                        startLine += validate.Count;
                        remainingNodes = subBulkList.Count - validate.Count;
                    }
                }
                catch (Exception ex)
                {
                    message = Resources.CoreWebContent.WEBCODE_AK0_28;
                    log.Error("Cannot validate bulk list", ex);
                }
            }

        }
        catch (Exception ex)
        {
            log.Error("Error in ProcessValidation method", ex);
        }
         
        return new HostnamesValidationInfo()
        {
               
               ResultText = message,
               StartLine =  startLine,
               HasUnresolvedNodes = hasUnresolvedNodes,
               RemainingNodes = remainingNodes
        };
    }

    [ScriptMethod]
    [WebMethod(true)]
    public static TestSnmpCredentialsResult TestSnmpCredentials(TestSnmpCredentialsModel model) {

        try
        {
            ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
            using (var proxy = blProxyCreator.Create(BusinessLayerExceptionHandler)) // Main engine id , should be a specific engine id defined for discovery
            {
                var result = NodeWorkflowHelper.ValidateSNMPNode(proxy, model.GetSNMPVersion(), model.IPAddress, DefaultSnmpPort, model.CommunityString, string.Empty,
                    model.AuthKeyPassword, string.Empty, model.GetSNMPAuthType(), SNMPv3AuthType.None, model.GetSNMPv3PrivacyType(), SNMPv3PrivacyType.None,
                    model.PrivacyKeyPassword, string.Empty, model.SNMPv3Context, string.Empty, model.SNMPv3Username, string.Empty,
                    model.AuthKeyIsPwd, false, model.PrivKeyIsPwd, false);
                return new TestSnmpCredentialsResult { Success = !result.ValidationReadFailed };
            }

        }
        catch (Exception ex) {
            return new TestSnmpCredentialsResult { Success = false, Error = ex.Message };        
        }
    }

    private IList<EngineInfo> GetEngines() {

        using (WebDAL informationService = new WebDAL())
        {
            DataTable engines = informationService.GetPollingEngineStatus();
            return engines.Rows.OfType<DataRow>().Where(p =>
            {
                var serverType = p["ServerType"] as string;
                return !(!string.IsNullOrEmpty(serverType) && serverType.Trim().Contains(OrionHSBServerType));
            }).Select(e => new EngineInfo { 
                EngineID = Convert.ToInt32(e["EngineID"]), 
                ServerName = (string)e["ServerName"] 
            }).ToList();
        }
    }

    protected override void Next()
	{
    }
  

	#region IStep Members
	public string Step
	{
		get
		{
			return "Network";
		}
	}
	#endregion


    class EqualityComparer : IEqualityComparer<string> {

        public bool Equals(string x, string y)
        {
            return string.Equals(x, y, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(string obj)
        {
            return obj.ToLowerInvariant().GetHashCode();
        }
    }

    public bool ShowHint {
        get {
            var value = WebUserSettingsDAL.Get(ShowDiscoveryHintSettingName);
            if (string.IsNullOrEmpty(value)) {
                return true;
            }

            return Convert.ToBoolean(value); 
        }
    }

    protected void EngineSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        var dropDown = (DropDownList)sender;
        var config = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        config.EngineId = Convert.ToInt32( dropDown.SelectedValue );
    }
    protected void DiscoveryConfigurationNotEmpty_ServerValidate(object source, ServerValidateEventArgs args)
    {
        IPAddressesBatchEditor.UpdateBulkList();

        DiscoveryConfiguration discoveryConfiguration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        CoreDiscoveryPluginConfiguration coreConfig = discoveryConfiguration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
         
        args.IsValid =
        ((CustomValidator)source).IsValid = coreConfig.ActiveDirectoryList.Count > 0
            || coreConfig.SubnetList.Count > 0
            || coreConfig.BulkList.Count > 0
            || coreConfig.ActiveDirectoryList.Count > 0
            || coreConfig.AddressRange.Count > 0;
    }

    public void RaisePostBackEvent(string eventArgument)
    {
    }
}
