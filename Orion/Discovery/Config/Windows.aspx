<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Windows.aspx.cs" Inherits="Orion_Discovery_Config_Windows" 
MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_510 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register Src="~/Orion/Discovery/Controls/CredentialsGrid.ascx" TagName="CredentialGrid" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Discovery/Controls/CredentialDetail.ascx" TagName="CredentialDetail" tagPrefix="orion"%>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
    <asp:UpdatePanel ID="CredentialsUpdatePanel" runat="server">
        <ContentTemplate>
            <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_178) %></span>
            <div style="width: 100%;">
                <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_AK0_511, string.Format("<a target='_blank' href='{0}'>",HelpHelper.GetHelpUrl("OrionPHAboutWMI")), "</a>")) %>
            </div>
            <orion:CredentialGrid ID="CredentialGrid" runat="server" OnAddButton_Click="OnAddButton_Click">
            </orion:CredentialGrid>
            <orion:CredentialDetail ID="CredentialDetail" runat="server" OnDialogCanceled="DialogCanceled">
            </orion:CredentialDetail>
            <table class="NavigationButtons">
                <tr>
                    <td>
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton ID="BackButton" runat="server" DisplayType="Secondary"
                                LocalizedText="Back" OnClick="imgbBack_Click" CausesValidation="false" />
                            <orion:LocalizableButton ID="NextButton" runat="server" DisplayType="Primary"
                                LocalizedText="Next" OnClick="imgbNext_Click" />
                            <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                                LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
