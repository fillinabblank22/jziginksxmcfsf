<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master"
    AutoEventWireup="true" CodeFile="MonitoringSettings.aspx.cs" Inherits="Orion_Discovery_Config_MonitoringSettings"
    Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_61 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/AutoImportWelcomeStep.ascx" TagPrefix="wizard" TagName="AutoImportWelcomeStep" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style type="text/css">
        table.ConfigurationControls {    width: 100%; }
        table.ConfigurationControls > td {
            padding-bottom: 20px;
            white-space: nowrap;
        }
        .info-tooltip:hover span {
            max-width: 240px;
            font-size: 11px;
            color: #515151;
        }
        .toggleOn, .toggleOff {
            margin: 0 16px;
            text-decoration: none !important;
        }
        .toggleOff {
            background-color: #696969 !important;
            color: #e9eaea !important;
        }
        .polling-methods { margin-left: 24px; }        
        .icmp-on-info { float: right; max-width: 380px; margin: 16px; }
        .MonitoringSettings_Description {
            padding-left: 15px;
            color: #848484;
        }
        .MonitoringSettingsContainer {
            float: left;
            width: 95%;
        }
        .sw-btn-ms-wizard.sw-btn {
            box-sizing: content-box;
            padding: 10px 16px;
            line-height: 11px;
            min-width: 80px;
            color: #297994 !important;
            text-align: center !important;
        }
        .sw-btn-ms-disabled.sw-btn {
            background-color: #ddd;
            border-color: #ddd;
            color: #777 !important;
            cursor: default;
            pointer-events: none;
        }
        #AutoImportDialogFrame .sw-wizard-wrapper {  	width: 100%; }
        #AutoImportDialogFrame .sw-wizard-wrapper .sw-wizard-navigation li { width: <%= this.WizardNavigationStepWidthPercent %>%; }
        #AutoImportDialogFrame .sw-wizard-wrapper .sw-wizard-steps div.sw-wizard-steps { min-height: 350px; }
    </style>

    <script type="text/javascript">
(function () {

    function MonitoringSettingsDialog() {
        var self = this;

        this.wizard = window.<%= this.WizardPanel.ClientID %>;
        this.dialog = '#AutoImportDialogFrame';

        // force to do a postback when dialog is re-opened
        this.doPostBackOnReopen = false;
        this.saveDataSwitch = '#<%= SaveAutoImportWizard.ClientID %>';

        this._showdialog = function () {
            $(this.dialog).dialog({
                width: '700',
                modal: true,
                draggable: true,
                title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_Discovery_MonitoringSettingsDialogTitle) %>",
                show: { duration: 0 },
                dialogClass: 'ui-overflow-visible',
                open: function () {
                    var t = $(this).parent(), w = $(window);
                    t.css('position', 'fixed');
                    t.offset({
                        top: w.scrollTop() + 20,
                        left: (w.width() / 2) - (t.width() / 2)
                    });
                }
            });
            $(this.dialog).parent().appendTo($("form#aspnetForm"));
        };

        this.show = function(){
            if(!self.doPostBackOnReopen)
                return self._showdialog.call(self);
            // do postback to clear / save data after first run of wizard
            <%=this.Page.ClientScript.GetPostBackEventReference(this.ReopenButton,"") %>;
        };
        this.close = function(saveData){
            self.doPostBackOnReopen = true;
            $(self.saveDataSwitch).attr('checked', saveData);
            $(self.dialog).dialog('close');
        };
    };

    var onText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_149) %>';
    var offText = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_150) %>';
    function toggleButton(renderAs, toggleFn) {
        var elm = $(renderAs).hide(),
            value = elm.prop('checked');
        toggleFn && toggleFn.call(this, value);
        return $('<a href="#"></a>' )
            .addClass(value ? 'toggleOn' : 'toggleOff')
            .text(value ? onText : offText)
            .insertAfter(elm)
            .off('click.toggleBtn')
            .on('click.toggleBtn', function(){
                var enable = $(this).toggleClass('toggleOn toggleOff').hasClass('toggleOn');
                $(this).text(enable ? onText : offText);
                elm.prop('checked', enable);

                toggleFn && toggleFn.call(this, enable);
            });
    };

    SW.MonitoringSettingsDlg = SW.MonitoringSettingsDlg || new MonitoringSettingsDialog();

    $(document).ready(function () {
        var automatic = $('#<%=this.AutomaticallyMonitorRB.ClientID %>'),
            manual = $('#<%=this.ManuallySetRB.ClientID %>'),
            button = $('#<%=MonitoringSettingsButton.ClientID %>');

        function setButtonEnableState() {
            if(automatic.is(':checked'))
                button.removeClass('sw-btn-ms-disabled').on('click.msWizzard', function(){ SW.MonitoringSettingsDlg.show(); });
            else
                button.addClass('sw-btn-ms-disabled').off('click.msWizzard');
        };

        automatic.off('click.msBtn').on('click.msBtn', function(ev){ setButtonEnableState(); });
        manual.off('click.msBtn').on('click.msBtn', function(ev){ setButtonEnableState(); });
        setButtonEnableState();

        toggleButton('#<%=this.SnmpOnlyCheckBox.ClientID %>', function(v){
            if(v) 
            { 
                $('.icmp-on-info').hide(); 
            }
            else 
            {                 
                $('.icmp-on-info').show(); 
            }
        });
    });
})();
    </script>

    <div class="ActionName">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings) %>
    </div>
    <div class="TextWithSpaceAfter">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_StepDescription) %>
    </div>
    
    <asp:Panel ID="ConfigurationSettings" runat="server" CssClass="blueBox">
        <span class="sw-suggestion icmp-on-info">
            <span class="sw-suggestion-icon"></span>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_ICMP_Info2) %><br />
            <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCoreSonarDiscMonitorLM")) %>" target="_blank" rel="noopener noreferrer" class="sw-link">&#0187; <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_LearnMore) %></a>
        </span>
        <h1 class="editor-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_DeviceHeadline) %></h1>
        <p class="ConfigurationControls">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_ICMP) %>
            <asp:CheckBox runat="server" ID="SnmpOnlyCheckBox" />            
            <br/>
            <span style="color: #848484;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_ICMP_Description) %></span>
        </p>
        <p class="ConfigurationControls">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.DiscoverySettings_PreferredMethod) %>
            <span id="polling-method-snmp"><asp:RadioButton runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,DiscoverySettings_PreferredMethod_SNMP %>" ID="rbSNMP" CssClass="polling-methods" GroupName="PollingMethod"/></span>
            <span id="polling-method-wmi"><asp:RadioButton runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,DiscoverySettings_PreferredMethod_WMI %>" ID="rbWMI" CssClass="polling-methods" GroupName="PollingMethod"/></span>
            &nbsp; &nbsp; &nbsp; 
            <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCoreSonarDiscMonitorInfo")) %>" target="_blank" rel="noopener noreferrer" class="info-tooltip">
                <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" /><span style="max-width: 320px !important;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_PreferredMethod_Info) %>
            </span></a>
        </p>
        <br/>
        <h1 class="editor-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Headline) %></h1>
        <div class="MonitoringSettings_Description"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Headsubline) %></div>
        <table class="ConfigurationControls">
            <tr>
                 <td>
                    <input id="ManuallySetRB" runat="server" name="setUpMonitoring" type="radio" style="float: left;" title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_109 %>" />
                    <div class="MonitoringSettingsContainer">
                        <label for="<%= this.ManuallySetRB.ClientID %>">
                            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Paragraph2Headline) %></strong>
                        </label>
                        &nbsp;
                        <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCoreSonarDiscMonitorManualImport")) %>" target="_blank" rel="noopener noreferrer" class="info-tooltip">
                            <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
                            <span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Info2) %>
                            </span>
                        </a>
                        <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Paragraph2) %></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <input id="AutomaticallyMonitorRB" runat="server" name="setUpMonitoring" type="radio" style="float: left" title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_108 %>"/> 
                    <div class="MonitoringSettingsContainer">
                        <label for="<%= this.AutomaticallyMonitorRB.ClientID %>">
                            <strong><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Paragraph1Headline) %></strong>
                        </label>
                        &nbsp;
                        <a href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionCoreSonarDiscMonitorAutoImport")) %>" target="_blank" rel="noopener noreferrer" class="info-tooltip">
                            <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
                            <span>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Info1) %>
                            </span>
                        </a>
                        <div><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_Paragraph1) %></div>
                        <br /><br />
                        <orion:LocalizableButtonLink ID="MonitoringSettingsButton" runat="server" LocalizedText="CustomText"
                            Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_NetworkSonar_MonitoringSettings_Button %>" DisplayType="Secondary" CssClass="sw-btn-ms-wizard"/>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--AutoImportDialogFrame sw-wizard-step--%>
    <div id="AutoImportDialogFrame" style="display: none;" class="editor-dialog ui-overflow-visible">
        <div class="editor-dialog-section">
            <orion:WizardPanelControl id="WizardPanel" BackwardNavigationEnabled="true" ActiveStepIndex="1" runat="server"
                OnCancelClientClick="SW.MonitoringSettingsDlg.close(false);" 
                OnFinishClientClick="SW.MonitoringSettingsDlg.close(true);">
            <Steps>
                <wizard:AutoImportWelcomeStep runat="server" ID="AutoImportStep" Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_NetworkSonar_MonitoringSettings_WelcomeStep %>" Index="1" />
            </Steps>
            </orion:WizardPanelControl>
        </div>
    </div>
    <asp:Button runat="server" ID="ReopenButton" OnClick="MonitoringSettings_Reopen" style="display:none" />
    <asp:CheckBox runat="server" ID="SaveAutoImportWizard" style="display:none" />

    <table class="NavigationButtons"><tr><td>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="imgbBack_Click" CausesValidation="false"/>
            <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
            <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
        </div>
    </td></tr>
    </table>
</asp:Content>