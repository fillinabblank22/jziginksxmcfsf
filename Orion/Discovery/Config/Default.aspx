<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master"
	AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Orion_Discovery_Config_Default"
	Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_BV0_0051 %>" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <orion:Include ID="Include1" runat="server" File="exporttopdf.js"/>
    <a href="#" id="ExportToPdf" class="exportToPdf sw-hdr-links-export" onclick=" ExportToPDF(); return false; "><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_62) %></a>
    <orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHDiscoveryWizard"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <orion:Include runat="server" File="Admin.css" />
    <orion:Include runat="server" File="Discovery.css"/>

    <script type="text/javascript">

        (function (window) {
            var showHint = <%= this.ShowHint ? "true" : "false" %>;
            var settingName = "<%= this.ShowDiscoveryHintSettingName %>";
            var webService = "/Orion/Services/DiscoveryWizard.asmx";

            function toggleDiscoveryHint(success, failure) {
                failure = failure || function () { };
                SW.Core.Services.callWebService(webService, "ToggleDiscoveryHint", { "settingName": settingName }, success, failure);
            }

            $(document).ready(function () {
                var hint = $("#message");
                var hintHandle = $('#DiscoveryHintHandle');
                hint.hide();
                hintHandle.hide();

                hint.find("a.close").on("click", function (evt) {
                    toggleDiscoveryHint(function () {
                        hint.hide();
                        hintHandle.show();
                    });
                });

                hintHandle.on("click", function (evt) {
                    toggleDiscoveryHint(function () {
                        hint.show();
                        hintHandle.hide();
                    });
                });

                if (showHint) {
                    hintHandle.hide();
                    hint.show();
                } else {
                    hintHandle.show();
                    hint.hide();
                }
            });
        })(window);
    </script>

    <div class="mainBody cleared">
        <div id="contentBox">
            <div id="welcomeHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_001) %></div>
            <div id="welcomeSubHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_002) %></div>
            <ul class="iconContainer">
                <li class="centerIconTile">
                    <div class="centerIconFrame" >
                        <span class="verticalcenterhelper"></span>
                        <img class="centerIcon" src="/Orion/images/discovery/outline-network.svg"/>
                    </div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_003) %>
                </li>
                <li class="centerIconArrow">
                    <img class="nextArrow" src="/Orion/images/discovery/nextarrow.svg"/>
                </li>
                <li class="centerIconTile">
                    <div class="centerIconFrame">
                        <span class="verticalcenterhelper"></span>
                        <img class="centerIcon" src="/Orion/images/discovery/credentials.svg"/>
                    </div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_004) %>
                </li>
                <li class="centerIconArrow">
                    <img class="nextArrow" src="/Orion/images/discovery/nextarrow.svg"/>
                </li>
                <li class="centerIconTile">
                    <div class="centerIconFrame">
                        <span class="verticalcenterhelper"></span>
                        <img class="centerIcon" src="/Orion/images/discovery/discovering.svg"/>
                    </div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_005) %>
                </li>
                <li class="centerIconArrow">
                    <img class="nextArrow" src="/Orion/images/discovery/nextarrow.svg"/>
                </li>
                <li class="centerIconTile">
                    <div class="centerIconFrame">
                        <span class="verticalcenterhelper"></span>
                        <img class="centerIcon" src="/Orion/images/discovery/review-results.svg"/>
                    </div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_006) %>
                </li>
                <li class="centerIconArrow">
                    <img class="nextArrow" src="/Orion/images/discovery/nextarrow.svg"/>
                </li>
                <li class="centerIconTile">
                    <div class="centerIconFrame">
                        <span class="verticalcenterhelper"></span>
                        <img class="centerIcon" src="/Orion/images/discovery/import-devices.svg"/>
                    </div>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_007) %>
                </li>
            </ul>

            <div class="cleared"></div>

             <div id="DiscoveryHintHandle" class="hidden">
                <a href="#">
                    <img src="/Orion/images/discovery/graduation-cap.svg" />
                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_85) %></span>
                </a>
            </div>

            <div id="message" class="sw-suggestion-mod sw-suggestion-info hidden">
                <div class="messageRow">
                    <img class="learningIcon" src="/Orion/images/discovery/graduation-cap.svg" />
                    <span id="learningHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_85) %></span>
                    <a class="close">
                        <img class="hideButton" src="/Orion/images/discovery/hide.svg" />
                    </a>
                </div>
                <div class="messageTable">
                    <span class="messageText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_86) %></span> <!-- We recommend scanning... -->
                    <img class="doubleArrow arrowMod" src="/Orion/images/discovery/arrows-02.svg" />
                    <div class="middleMessageGroup">
                        <div class="middleMessageGroupItem">
                            <img class="middleMessageGroupIcon" src="/Orion/images/discovery/subnet.svg" />
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_RV0_1) %></span> <!-- ... a small subnet (/24) with your test environment -->
                        </div>
                        <div class="middleMessageGroupItem">
                            <img class="middleMessageGroupIcon" src="/Orion/images/discovery/ip.svg" />
                            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_RV0_2) %></span> <!--  ... a few individual IP addresses for servers, routers and switches, and VMs -->
                        </div>
                    </div>
                    <img class="doubleArrow" src="/Orion/images/discovery/arrows-03.svg" />
                    <span class="messageTextBig"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_89) %></span> <!-- This will let you see the wealth of data.. -->
                </div>
            </div>

            <div class="hintMessage">
                <div id="optOutToggle" runat="server" clientidmode="Static">
                    <asp:CheckBox ID="DoNotRedirectToDiscoveryWizard" Checked="false" Text="" runat="server" ClientIDMode="Static" OnCheckedChanged="DoNotRedirectToDiscoveryWizard_CheckedChanged" AutoPostBack="true" />
                    <label for="DoNotRedirectToDiscoveryWizard"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_010) %></label>
                </div>
                <div id="stickyPad" class="stickyPad" runat="server">
                    <orion:HelpHint runat="server">
                        <HelpContent>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_008) %>
                        </HelpContent>
                    </orion:HelpHint>
                </div>
            </div>
           
            <asp:HyperLink class="sw-btn-primary sw-btn startButton" NavigateUrl="/Orion/Discovery/Config/Network.aspx" runat="server"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VOLTRONDICOVERY_009) %></asp:HyperLink>

            <div class="cleared"></div>
        </div>

    </div>

</asp:Content>
