<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master"
    AutoEventWireup="true" CodeFile="DiscoverySettings.aspx.cs" Inherits="Orion_Discovery_Config_DiscoverySettings"
    Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_61 %>" %>

<%@ Register TagPrefix="orion" TagName="Slider" Src="~/Orion/Controls/Slider.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">

    <div class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_134) %></div>
    <div class="TextWithSpaceAfter"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_135) %></div>

    <asp:Panel ID="ConfigurationControls" runat="server" CssClass="blueBox">
        <h1 class="editor-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_DiscoverySettings_DetailsHeadline) %></h1>
        <table class="ConfigurationControls">
            <tr>
                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_14) %></td>
                <td>
                    <asp:TextBox ID="NameTB" runat="server" CssClass="DescriptionBox" MaxLength="255" />
                    <asp:RequiredFieldValidator ID="NameRequiredFieldValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_133 %>"
                        ControlToValidate="NameTB" />
                </td>
            </tr>
            <tr>
                <td><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_136) %></td>
                <td>
                    <asp:TextBox ID="DecriptionTB" runat="server" CssClass="DescriptionBox" MaxLength="255" />
                </td>
            </tr>
        </table>
        <h1 class="editor-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_DiscoverySettings_RetriesAndTimeoutsHeadline) %></h1>
        <table class="ConfigurationControls">
            <tr>
                <orion:Slider runat="server" ID="SNMPTimeoutSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_132 %>"
                    RangeFrom="100" RangeTo="5000" Unit="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_151 %>" ValidateRange="true" Step="10" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_131 %>" />
            </tr>
            <tr>
                <orion:Slider runat="server" ID="SearchTimeoutSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_129 %>"
                    RangeFrom="100" RangeTo="5000" Unit="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_151 %>" ValidateRange="true" Step="10" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_130 %>" />
            </tr>
            <tr>
                <orion:Slider runat="server" ID="SNMPRetriesSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_128 %>"
                    RangeFrom="0" RangeTo="10" ValidateRange="true" Unit="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_Retry_Unit %>" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_127 %>" />
            </tr>            
            <tr>
                <orion:Slider runat="server" ID="WMIRetriesSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_WMIRetriesSlider_Label %>"
                    RangeFrom="0" RangeTo="5" ValidateRange="true" Unit="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_Retry_Unit %>" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_WMIRetriesSlider_ErrorMessage %>" />
            </tr>
            <tr>
                <orion:Slider runat="server" ID="WMIRetryIntervalSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_WMIRetryInterval_Label %>"
                    RangeFrom="1000" RangeTo="15000" Step="500" ValidateRange="true" Unit="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_151 %>" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_WMIRetryIntervalSlider_ErrorMessage %>" />
            </tr>
            <tr>
                <orion:Slider runat="server" ID="HopCountSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_126 %>"
                    RangeFrom="0" RangeTo="10" ValidateRange="true" Unit="<%$ HtmlEncodedResources: CoreWebContent, DiscoverySettings_Hop_Unit %>" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_125 %>" />
            </tr>
            <tr>
                <orion:Slider runat="server" ID="JobTimeoutSlider" TableCellCount="3" Label="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_124 %>" Unit="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_152 %>"
                    RangeFrom="10" ValidateRange="true" />
            </tr>
            <asp:Panel ID="pluginContainer" runat="server" />
        </table>
<%--        <table class="ConfigurationControls">
            <tr>
                <td>
                    <asp:Label ID="PollingLabel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_IB0_26 %>" />
                </td>
                <td>
                    <asp:DropDownList ID="EnginesList" runat="server" Width="150" /><br />
                </td>
            </tr>
        </table>--%>
        <asp:Label ID="MissingEngine" runat="server" Text="" ForeColor="Red" Visible="false" class="ConfigurationControls" />
    </asp:Panel>
    <table class="NavigationButtons"><tr><td>
                <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="imgbBack_Click" CausesValidation="false"/>
        <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
        <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
                </div>
    </td></tr></table>

</asp:Content>
