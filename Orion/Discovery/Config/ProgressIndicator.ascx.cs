using System;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Web.Discovery;

public enum DiscoveryWizardMode
{
	Standard,
	ICMPOnly
} ;

[Obsolete]
public enum DiscoveryStep
{
    SNMP,
    Windows,
    ESX,
	Network,
	Discovery,
    DiscoverySettings,
    DiscoveryScheduling
};

public partial class Orion_Discovery_Config_ProgressIndicator : System.Web.UI.UserControl
{
    private const string indicatorImageFolderPath = "../../images/nodemgmt_art/progress_indicator/background/";

	protected void Page_Init(object sender, EventArgs e)
	{
		
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			Reload(); 
	}

	#region public properties
	[Browsable(true)]
	[Category("Appearence")]
	public DiscoveryWizardMode Mode
	{
		get
		{
            object o = ViewState["DiscoveryWizardMode"];
            return (o == null ? DiscoveryWizardMode.Standard : (DiscoveryWizardMode)ViewState["DiscoveryWizardMode"]);
		}
		set
		{
            ViewState["DiscoveryWizardMode"] = value;
		}
	}

    [Obsolete]
	public DiscoveryStep Step
	{
		get
		{
			return (DiscoveryStep)ViewState["AddNodeStep"];
		}
		set
		{
			ViewState["AddNodeStep"] = value;
		}
	}
	#endregion

	#region public methods
	public void Reload()
	{
		//LoadFromUrl();
		LoadFromCurrentStep();
		RenderProgressImages();
	}
	#endregion

	#region private methods
	private void RenderProgressImages()
	{
		phPluginImages.Controls.Clear();

		List<SolarWinds.Orion.Web.WizardStep> steps = ConfigWorkflowHelper.Steps;
		int count = steps.Count;		

		for (int i=0; i<count; i++)
		{
			bool nextActive;
			Image imgStep = new Image();
			imgStep.ID = string.Format("imgStep{0}",i+1);

			Image imgSeparator = new Image();
			imgSeparator.ID = string.Format("imgSep{0}", i+1);
            bool isCurrentStep = false;

            string endString = "";

			if (ConfigWorkflowHelper.GetCurrentWizardStep().Equals(steps[i]))
			{
                isCurrentStep = true;
				if (!steps[i].IsPluginStep)
				{
					imgStep.ImageUrl = string.Format("{0}PI_{1}_on.gif", indicatorImageFolderPath, steps[i].Name);					
				}
				else
				{
					imgStep.ImageUrl = steps[i].IndicatorActiveImage;
				}
				nextActive = ((i < count - 1) && ConfigWorkflowHelper.GetCurrentWizardStep().Equals(steps[i + 1]) ? true : false);

                if (nextActive)
                {
                    imgSeparator.ImageUrl =
                        string.Format(string.Format("{0}pi_sep_{1}{2}_{3}.gif", indicatorImageFolderPath, endString, "on", "on"));
                }
                else
                {
                    imgSeparator.ImageUrl = string.Format(string.Format("{0}pi_sep_{1}{2}_{3}.gif", indicatorImageFolderPath, endString, "on", "off"));
                }
			}
			else
			{
				if (!steps[i].IsPluginStep)
				{
					imgStep.ImageUrl = string.Format("{0}PI_{1}_off.gif", indicatorImageFolderPath, steps[i].Name);					
				}
				else
				{
					imgStep.ImageUrl = steps[i].IndicatorImage;
				}

                

				nextActive = ((i < count - 1) && ConfigWorkflowHelper.GetCurrentWizardStep().Equals(steps[i + 1]) ? true : false);
                if (nextActive)
                {
                    imgSeparator.ImageUrl =
                        string.Format(string.Format("{0}pi_sep_{1}{2}_{3}.gif", indicatorImageFolderPath, endString, "off", "on"));
                }
                else
                {
                    imgSeparator.ImageUrl = 
                        string.Format(string.Format("{0}pi_sep_{1}{2}_{3}.gif", indicatorImageFolderPath, endString, "off", "off"));
                }
			}

            Label tb = new Label();
            tb.Text = steps[i].Title;
            tb.CssClass = string.Format("PI_{0}", isCurrentStep ? "on" : "off");
            tb.Attributes["automation"] = tb.Text;
            // Text of first step must be move to the right a little 
            if (i == 0)
            {
                tb.Style.Add("padding-left", "10px;");
            }
            
            phPluginImages.Controls.Add(tb);
			phPluginImages.Controls.Add(imgSeparator);			
		}		
	}

	

	private void LoadFromCurrentStep()
	{
		SolarWinds.Orion.Web.WizardStep wstep = ConfigWorkflowHelper.GetCurrentWizardStep();
        if (wstep == null)
        {
            throw new Exception(string.Format("Wizard step {0} not found", ConfigWorkflowHelper.CurrentStep));
        }		
	}
    	
	#endregion
}
