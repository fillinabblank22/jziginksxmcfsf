﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Models.Discovery;


public partial class Orion_Discovery_Config_Windows : ConfigWizardBasePage, IStep
{
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public List<UsernamePasswordCredential> AllCredentials
    {
        get
        {
            if (this.ViewState["CredentialGrid"] == null)
            {
                LoadData();
            }
            return (List<UsernamePasswordCredential>)this.ViewState["CredentialGrid"];
        }

        set
        {
            this.ViewState["CredentialGrid"] = value;
        }
    }

    public List<UsernamePasswordCredential> SelectedCredentials
    {
        get
        {
            if (this.ViewState["EditCredential"] == null)
            {
                LoadData();
            }
            return (List<UsernamePasswordCredential>)this.ViewState["EditCredential"];
        }

        set
        {
            this.ViewState["EditCredential"] = value;
        }
    }

    protected DiscoveryConfiguration configuration;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        InitButtons(CancelButton);

        if (IsPostBack)
        {
            SaveSelectedCredentials();
        }

        this.CredentialGrid.Credentials = SelectedCredentials;
        this.CredentialGrid.AllCredentials = AllCredentials;
        this.CredentialDetail.AllCredentials = AllCredentials;
        this.CredentialDetail.SelectedCredentials = SelectedCredentials;
        this.CredentialDetail.CredentialSubmited += (send, ee) => { SaveSelectedCredentials(); this.CredentialGrid.Refresh(); };
    }

    protected void OnAddButton_Click(object sender, EventArgs e)
    {
        this.CredentialDetail.Display();
    }

    protected void DialogCanceled(object sender, EventArgs e)
    {
        this.CredentialGrid.Refresh();
    }

    private void LoadData()
    {
        SelectedCredentials = new List<UsernamePasswordCredential>();
        var wmiCreds = this.configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().WMICredentials;

        this.AllCredentials = this.LoadCredentialsFromDB();
        foreach (var wmi in wmiCreds)
        {
            if (wmi.CredentialID > 0)
            {
                var found = this.AllCredentials.FirstOrDefault(item => item.ID == wmi.CredentialID);

                if (found != null)
                {
                    // move from all to selected
                    this.AllCredentials.Remove(found);
                    SelectedCredentials.Add(found);
                }
            }
            else if (wmi.Credential != null)
            {
                //creds created in this config, that dont have assigned IDs yet
                SelectedCredentials.Add(wmi.Credential);
            }
        }
    }

    private void SaveSelectedCredentials()
    {
        if (configuration == null)
            return;

        var coreCfg = this.configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        if (coreCfg == null)
            return;

        coreCfg.WMICredentials.Clear();

        foreach (var item in SelectedCredentials)
        {
            var wmiAccess = new WMIAccess { Credential = item };
            wmiAccess.CredentialID = item.ID.HasValue ? item.ID.Value : 0;
            coreCfg.WMICredentials.Add(wmiAccess);

            if (coreCfg.Credentials.FirstOrDefault(p => p.Name == item.Name) == null)
            {
                coreCfg.Credentials.Add(item);
            }
        }
    }

    private List<UsernamePasswordCredential> LoadCredentialsFromDB()
    {
        using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            List<UsernamePasswordCredential> wmiCredentials = proxy.GetSharedWmiCredentials(CoreConstants.CoreCredentialOwner);

            return wmiCredentials;
        }
    }

    protected override bool ValidateUserInput()
    {
        return true;
    }

    public string Step
    {
        get
        {
            return "Windows";
        }
    }

    protected override void Next()
    {
        if (!ValidateUserInput())
        {
            return;
        }
    }


}