using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SnmpCredentials = SolarWinds.Orion.Core.Models.Credentials.SnmpCredentials;
using SnmpCredentialsV2 = SolarWinds.Orion.Core.Models.Credentials.SnmpCredentialsV2;
using SNMPv3AuthType = SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType;
using SNMPv3PrivacyType = SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType;
using SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.Models.Discovery;

public partial class Orion_Discovery_Config_SNMP : ConfigWizardBasePage, IStep
{
    private bool? fipsModeEnabled = null;
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    #region Properties
    
    protected DiscoveryConfiguration configuration;

    protected List<SnmpCredentialsV2> SnmpCredentialsV2
    {
        get
        {
            if (this.ViewState["snmpV2credentials"] == null)
                this.ViewState["snmpV2credentials"] = LoadFilteredSNMPV2SharedCredentials();
            return (List<SnmpCredentialsV2>)this.ViewState["snmpV2credentials"];
        }

        set
        {
            this.ViewState["snmpV2credentials"] = value;
        }
    }

    protected List<SnmpCredentialsV3> SnmpCredentialsV3
    {
        get
        {
            if (this.ViewState["snmpV3credentials"] == null)
                this.ViewState["snmpV3credentials"] = LoadFilteredSNMPV3SharedCredentials();
            return (List<SnmpCredentialsV3>)this.ViewState["snmpV3credentials"];
        }

        set
        {
            this.ViewState["snmpV3credentials"] = value;
        }
    }

    protected List<SnmpCredentials> Credentials
    {
        get
        {
            return (List<SnmpCredentials>)this.ViewState["credentials"];
        }

        set
        {
            this.ViewState["credentials"] = value;
        }
    }

    protected int EditedCredentialsIndex
    {
        get
        {
            return (int)this.ViewState["EditSetIndex"];
        }

        set
        {
            this.ViewState["EditSetIndex"] = value;
        }
    }

    protected SnmpCredentials EditedCredentials
    {
        get
        {
            if (EditedCredentialsIndex >= 0)
            {
                return Credentials[EditedCredentialsIndex];
            }
            else
            {
                return null;
            }
        }
    }

    protected string AuthPwdInSession
    {
        get
        {
            return (string)this.Session["AuthPwdInSession"];
        }

        set
        {
            this.Session["AuthPwdInSession"] = value;
        }
    }

    protected string PrivPwdInSession
    {
        get
        {
            return (string)this.Session["PrivPwdInSession"];
        }

        set
        {
            this.Session["PrivPwdInSession"] = value;
        }
    }

    private bool FIPSModeEnabled
    {
        get
        {
            if (fipsModeEnabled == null)
            {
                fipsModeEnabled = EnginesDAL.IsFIPSModeEnabledOnAnyEngine();
            }
            return fipsModeEnabled.Value;
        }
    }

    #endregion

    #region PageEvents

    // binds credentials data to gridView
    private void BindData()
    {
        this.CredentialsGrid.DataSource = this.Credentials;
        this.CredentialsGrid.DataBind();
    }

    // on init event
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // load profile credentials list
        this.configuration = null;
        this.configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo;

        if (this.configuration == null)
        {
            throw new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(()=>CoreWebContent.WEBCODE_VB0_348);
        }

        if (this.configuration.Snmp == null)
        {
            this.configuration.Snmp = new List<SnmpEntry>();
        }

        this.SnmpVersion.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_18, "SNMPv1or2c"));
        this.SnmpVersion.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_AK0_19, "SNMPv3"));
        
        this.AuthMetodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_SO0_9, SNMPv3AuthType.None.ToString()));
        if (!FIPSModeEnabled)
        {
            this.AuthMetodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_75, SNMPv3AuthType.MD5.ToString()));
        }
        this.AuthMetodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_76, SNMPv3AuthType.SHA1.ToString()));

        this.PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_SO0_9, SNMPv3PrivacyType.None.ToString()));
        if (!FIPSModeEnabled)
        {
            this.PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_77, SNMPv3PrivacyType.DES56.ToString()));
        }
        this.PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_78, SNMPv3PrivacyType.AES128.ToString()));
        this.PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_85, SNMPv3PrivacyType.AES192.ToString()));
        this.PrivMethodDropList.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_IB0_86, SNMPv3PrivacyType.AES256.ToString()));
    }

    // on load event
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        InitButtons(imgbCancel);

        var coreConfiguration = this.configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        if (!IsPostBack)
        {
            this.EditedCredentialsIndex = -1;

            if (this.configuration.IsNew() && !ConfigWorkflowHelper.VisitedPages.HadUserVisit(this))
            {
                // we must load all credentials
                var snmpv2 = this.LoadSNMPV2SharedCredentials().Cast<SnmpCredentials>();
                var snmpv3 = this.LoadSNMPV3SharedCredentials().Cast<SnmpCredentials>();
                this.Credentials = new List<SnmpCredentials>();
                this.Credentials.AddRange(snmpv2);
                this.Credentials.AddRange(snmpv3);

                //empty dropdownlists sources
                SnmpCredentialsV2 = new List<SnmpCredentialsV2>();
                SnmpCredentialsV3 = new List<SnmpCredentialsV3>();
            }
            else
            {
                // we load credentials from database
                this.Credentials = new List<SnmpCredentials>();
                coreConfiguration.Credentials
                    .Where(c => c is SnmpCredentialsV2 || c is SnmpCredentialsV3).ToList().ForEach(
                        c => this.Credentials.Add((SnmpCredentials)c));
            }
        }

        coreConfiguration.Credentials
                .RemoveAll(c => c is SnmpCredentialsV2 || c is SnmpCredentialsV3);

        this.Credentials.ForEach(c =>
              coreConfiguration.Credentials.Add(c));

        // bind credentials to datagrid
        if (!IsPostBack)
        {
            var snmpCrendetialsV3 = this.Credentials.OfType<SnmpCredentialsV3>();
            foreach (var snmpCredentialV3 in snmpCrendetialsV3)
            {
                if (string.IsNullOrWhiteSpace(snmpCredentialV3.Name))
                    snmpCredentialV3.Name = FormatUserNameAndContext(snmpCredentialV3.UserName, snmpCredentialV3.Context);
            }

            InitV3Controls();
            BindData();
        }

        this.AuthPwdInSession = this.AuthPassword.Text;
        this.PrivPwdInSession = this.PrivPassword.Text;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.AuthPassword.Text = this.AuthPwdInSession;
        this.PrivPassword.Text = this.PrivPwdInSession;

        if (this.EditedCredentialsIndex != -1 && this.EditedCredentials != null && this.EditedCredentials.ID.HasValue)
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                if (businessProxy.GetProfileIDsUsingCredentials(new List<int>() { this.EditedCredentials.ID.Value })
                    .Where((id) => id != configuration.ProfileId).Count() > 0)
                    SaveCred.OnClientClick = String.Format("if (!confirm('{0}')) return false;",
                        ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_AK0_177));
            }
        }
        else
        {
            SaveCred.OnClientClick = String.Empty;
        }
    }

    #endregion

    #region GUI

    protected string FormatSNMPVersion(SNMPVersion version)
    {
        switch (version)
        {
            case SNMPVersion.SNMP1:
            case SNMPVersion.SNMP2c: return Resources.CoreWebContent.WEBCODE_AK0_18;
            case SNMPVersion.SNMP3: return Resources.CoreWebContent.WEBCODE_AK0_19;
            default: return Resources.CoreWebContent.WEBCODE_SO0_9;
        }
    }

    protected string Encode(object o)
    {
        return Server.HtmlEncode((string)o);
    }

    protected void ValidateUsername(object source, ServerValidateEventArgs args)
    {
        var credV3 = new SnmpCredentialsV3();

        UpdateCredentialFromEditor(credV3);

        args.IsValid = CredentialsCanBeSaved(credV3, this.EditedCredentials);
    }

    private bool CredentialsCanBeSaved(SnmpCredentials entryToCheck, SnmpCredentials editedCredentials)
    {
        var snmpV3Credentials = Credentials.OfType<SnmpCredentialsV3>().ToList();

        return snmpV3Credentials.FindAll((entry) => entry != editedCredentials && entry.Match(entryToCheck)).Count == 0
               && (this.SnmpCredentialsV3.FindAll((entry) => entry != editedCredentials && entry.Match(entryToCheck)).Count == 0 || editedCredentials == null);
    }

    // shows editor dialog for adding a new set
    protected void AddBtn_OnClick(object sender, EventArgs e)
    {
        this.AddCred.Visible = true;
        this.SaveCred.Visible = false;

        this.EditedCredentialsIndex = -1;
        this.CredEdit.Visible = true;
        this.EmptyCommunityStringErrorPanel.Visible = false;
        this.AmbigousCommunityString.Visible = false;
        this.CommunityStringTB.Text = String.Empty;
        this.ActionHeader.Text = Resources.CoreWebContent.WEBDATA_AK0_71;
        this.SNMPVersionPanel.Visible = true;

        InitV2Credentials();
        InitV3Controls();
        ChooseV3Credentials.Visible = true;

        this.modalityDiv.ShowBlock = true;
    }

    // shows editor dialog for editing existing set
    protected void EditItem(int selectedIndex)
    {
        this.AddCred.Visible = false;
        this.SaveCred.Visible = true;


        ChooseV3Credentials.Visible = false;
        ChooseV2Credentials.Visible = false;
        SetSnmpV3Editability(true);
        CommunityStringTB.Enabled = true;

        if (selectedIndex < 0)
        {
            // no credentials selected for editing
            return;
        }

        this.EditedCredentialsIndex = selectedIndex;

        // set version
        switch (this.EditedCredentials.Version)
        {
            case SolarWinds.Orion.Core.Models.Credentials.SNMPVersion.SNMP3: this.SnmpVersion.SelectedIndex = 1; break;
            default:
                this.SnmpVersion.SelectedIndex = 0;
                break;
        }

        // upadate controls
        SnmpVersion_OnSelectedIndexChanged(null, new EventArgs());


        if (this.SnmpVersion.SelectedIndex == 0)
        {
            // edited set is v1 or v2c

            // hide error panels
            this.EmptyCommunityStringErrorPanel.Visible = false;
            this.AmbigousCommunityString.Visible = false;

            // fill in values
            this.CommunityStringTB.Text = this.EditedCredentials.Name;

        }
        else
        {
            // edited set is v3
            SnmpCredentialsV3 editedSNMPV3Credential = this.EditedCredentials as SnmpCredentialsV3;
            if (editedSNMPV3Credential == null)
            {
                log.Error("Edited credential is not SNMPv3 Credential");
                throw new ArgumentException("Edited credential is not SNMPv3 Credential");
            }

            this.UserNameTextBox.Text = editedSNMPV3Credential.UserName;
            this.ContextTextBox.Text = editedSNMPV3Credential.Context;

            // authentification method
            this.AuthMetodDropList.SelectedIndex = 0;

            if (editedSNMPV3Credential.AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthNoPriv ||
                editedSNMPV3Credential.AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthPriv)
            {
                SetAuthMethodInDropDownList(editedSNMPV3Credential.AuthenticationType);
            }

            if (this.AuthMetodDropList.SelectedIndex > 0)
            {
                this.AuthPassword.IsEnabledPasswordTextBox = true;
                this.AuthPasswordIsKey.Enabled = true;
                this.PrivMethodDropList.Enabled = true;
                this.AuthPasswordIsKey.Checked = !editedSNMPV3Credential.AuthenticationKeyIsPassword;
                this.AuthPwdInSession = editedSNMPV3Credential.AuthenticationPassword;
            }
            else
            {
                this.AuthPassword.IsEnabledPasswordTextBox = false;
                this.AuthPasswordIsKey.Enabled = false;
                this.AuthPwdInSession = String.Empty;
                this.PrivMethodDropList.Enabled = false;
            }

            // privacy method
            this.PrivMethodDropList.SelectedIndex = 0;

            if (editedSNMPV3Credential.AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthPriv)
            {
                SetPrivacyMethodDropDownList(editedSNMPV3Credential.PrivacyType);
            }

            if (this.PrivMethodDropList.SelectedIndex > 0)
            {
                this.PrivPassword.IsEnabledPasswordTextBox = true;
                this.PrivPasswordIsKey.Enabled = true;
                this.PrivPwdInSession = editedSNMPV3Credential.PrivacyPassword;
                this.PrivPasswordIsKey.Checked = !editedSNMPV3Credential.PrivacyKeyIsPassword;
            }
            else
            {
                this.PrivPassword.IsEnabledPasswordTextBox = false;
                this.PrivPasswordIsKey.Enabled = false;
                this.PrivPwdInSession = String.Empty;
            }
        }

        this.CredEdit.Visible = true;
        this.ActionHeader.Text = Resources.CoreWebContent.WEBCODE_AK0_20;
        this.SNMPVersionPanel.Visible = false;
        this.modalityDiv.ShowBlock = true;
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        int itemIndex = -1;

        if (e.CommandArgument != null)
            int.TryParse(e.CommandArgument.ToString(), out itemIndex);

        switch (e.CommandName)
        {
            case "MoveUp":
                MoveItemUp(itemIndex);
                break;
            case "MoveDown":
                MoveItemDown(itemIndex);
                break;
            case "DeleteItem":
                DeleteItem(itemIndex);
                break;
            case "EditItem":
                EditItem(itemIndex);
                break;
        }

    }


    // delete selected credentials
    protected void DeleteItem(int selectedIndex)
    {
        if (selectedIndex >= 0)
        {
            if (Credentials[selectedIndex] is SnmpCredentialsV2)
            {
                this.SnmpCredentialsV2.Add((SnmpCredentialsV2)Credentials[selectedIndex]);
            }
            if (Credentials[selectedIndex] is SnmpCredentialsV3)
            {
                var credentials = (SnmpCredentialsV3) Credentials[selectedIndex];
                if (FIPSModeEnabled)
                {
                    // add only FIPS compatible credentials if FIPS is enabled
                    if (CredentialHelper.IsFIPSCompatible(credentials))
                    {
                        this.SnmpCredentialsV3.Add(credentials);
                    }
                }
                else
                {
                    this.SnmpCredentialsV3.Add(credentials);
                }
            }
            Credentials.RemoveAt(selectedIndex);
            BindData();
        }
    }

    // move credentials up
    protected void MoveItemUp(int selectedIndex)
    {
        if (selectedIndex > 0)
        {
            Credentials.Reverse(selectedIndex - 1, 2);
            BindData();
        }
    }

    // move credentials down
    protected void MoveItemDown(int selectedIndex)
    {
        if (selectedIndex >= 0 && selectedIndex < this.CredentialsGrid.Rows.Count - 1)
        {
            Credentials.Reverse(selectedIndex, 2);
            BindData();
        }
    }

    #endregion

    #region editor

    #region SNMPv3

    protected void InitV3Controls()
    {
        this.SnmpCredentialsV3ddl.Items.Clear();
        this.SnmpCredentialsV3ddl.Items.Add(new ListItem()
        {
            Text = Resources.CoreWebContent.WEBCODE_AK0_176,
            Value = "new",
            Selected = true
        });

        if (this.SnmpCredentialsV3.Count > 0)
        {
            foreach (var credential in this.SnmpCredentialsV3)
            {
                this.SnmpCredentialsV3ddl.Items.Add(new ListItem()
                {
                    Text = credential.Name,
                    Value = SnmpCredentialsV3.IndexOf(credential).ToString()
                });
            }
        }

        this.AuthMetodDropList.SelectedIndex = 0;

        this.PrivMethodDropList.SelectedIndex = 0;
        this.PrivMethodDropList.Enabled = false;

        this.AuthPwdInSession = String.Empty;
        this.AuthPassword.IsEnabledPasswordTextBox = false;
        this.AuthPasswordIsKey.Enabled = false;
        this.AuthPasswordIsKey.Checked = false;
        
        this.PrivPwdInSession = String.Empty;
        this.PrivPassword.IsEnabledPasswordTextBox = false;
        this.PrivPasswordIsKey.Enabled = false;
        this.PrivPasswordIsKey.Checked = false;

        this.UserNameTextBox.Text = String.Empty;
        this.ContextTextBox.Text = String.Empty;

        if (SnmpCredentialsV3ddl.SelectedIndex == 0)
        {
            this.UserNameTextBox.Enabled = true;
            this.ContextTextBox.Enabled = true;
            this.AuthMetodDropList.Enabled = true;
        }
    }

    protected void AuthMetodDropListOnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.AuthMetodDropList.SelectedIndex == 0)
        {
            this.PrivMethodDropList.SelectedIndex = 0;
            this.PrivMethodDropList.Enabled = false;

            this.AuthPwdInSession = String.Empty;
            this.AuthPassword.IsEnabledPasswordTextBox = false;
            this.AuthPasswordIsKey.Enabled = false;
            this.AuthPasswordIsKey.Checked = false;

            this.PrivPwdInSession = String.Empty;
            this.PrivPassword.IsEnabledPasswordTextBox = false;
            this.PrivPasswordIsKey.Enabled = false;
            this.PrivPasswordIsKey.Checked = false;
        }

        if (this.AuthMetodDropList.SelectedIndex == 1 || this.AuthMetodDropList.SelectedIndex == 2)
        {
            this.PrivMethodDropList.Enabled = true;

            if (this.AuthPassword.IsEnabledPasswordTextBox != true)
            {
                this.AuthPassword.IsEnabledPasswordTextBox = true;
                this.AuthPasswordIsKey.Enabled = true;
            }
        }
    }

    protected void PrivMethodDropListOnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.PrivMethodDropList.SelectedIndex == 0)
        {
            this.PrivPwdInSession = String.Empty;
            this.PrivPassword.IsEnabledPasswordTextBox = false;
            this.PrivPasswordIsKey.Enabled = false;
            this.PrivPasswordIsKey.Checked = false;
        }
        else
        {
            if (!this.PrivPassword.IsEnabledPasswordTextBox)
            {
                this.PrivPassword.IsEnabledPasswordTextBox = true;
                this.PrivPasswordIsKey.Enabled = true;
            }
        }
    }

    private void SetAuthMethodInDropDownList(SNMPv3AuthType authenticationType)
    {
        switch (authenticationType)
        {
            case SNMPv3AuthType.MD5:// MD5 is not present when FIPS mode is enabled
                if (this.AuthMetodDropList.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3AuthType.MD5.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    this.AuthMetodDropList.SelectedValue = authenticationType.ToString();
                }
                else
                {
                    this.AuthMetodDropList.SelectedIndex = 0;
                }
                break;
            case SNMPv3AuthType.SHA1:
                this.AuthMetodDropList.SelectedValue = authenticationType.ToString(); 
                break;
        }
    }

    private void SetPrivacyMethodDropDownList(SNMPv3PrivacyType privacyType)
    {
        switch (privacyType)
        {
            case SNMPv3PrivacyType.DES56:
                if (this.PrivMethodDropList.Items.Cast<ListItem>().Any(item => string.Equals(item.Value, SNMPv3PrivacyType.DES56.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    this.PrivMethodDropList.SelectedValue = privacyType.ToString(); 
                }
                else
                {
                    this.PrivMethodDropList.SelectedIndex = 0;
                }
                break;
            case SNMPv3PrivacyType.AES128: 
            case SNMPv3PrivacyType.AES192: 
            case SNMPv3PrivacyType.AES256: 
                this.PrivMethodDropList.SelectedValue = privacyType.ToString(); 
                break;
        }
    }


    #endregion

    #region Common

    private void InitV2Credentials()
    {
        ChooseV2Credentials.Visible = true;
        this.snmpV2Credentials.Items.Clear();
        this.snmpV2Credentials.Items.Add(new ListItem()
        {
            Text = Resources.CoreWebContent.WEBCODE_AK0_176,
            Value = "new",
            Selected = true
        });

        if (this.SnmpCredentialsV2.Count > 0)
        {
            foreach (var credential in this.SnmpCredentialsV2)
            {
                this.snmpV2Credentials.Items.Add(new ListItem()
                {
                    Text = credential.Community,
                    Value = SnmpCredentialsV2.IndexOf(credential).ToString(),
                });
            }
        }

        this.CommunityStringTB.Enabled = true;
    }

    private List<SnmpCredentialsV2> LoadSNMPV2SharedCredentials()
    {
        List<SnmpCredentialsV2> result = new List<SnmpCredentialsV2>();
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            result = businessProxy.GetSharedSnmpV2Credentials(CoreConstants.CoreCredentialOwner);
        }

        return result;
    }

    private List<SnmpCredentialsV2> LoadFilteredSNMPV2SharedCredentials()
    {
        var result = LoadSNMPV2SharedCredentials();

        var coreConfiguration = configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

        var credentialsNotStoredInConfiguration = result.Where(c => !coreConfiguration.Credentials.Any(cc => cc.ID.HasValue && cc.ID.Value == c.ID.Value));
        
        var temporarySnmpCreds = configuration.Snmp.Where( s=> s.Version == SNMPVersion.SNMP1 || s.Version == SNMPVersion.SNMP2c).Select( s=> ConvertToV2(s)).ToList();
        var temporaryFiltered = credentialsNotStoredInConfiguration.ToList();
        temporaryFiltered.AddRange(temporarySnmpCreds);
        return temporaryFiltered;
    }

    private SnmpCredentialsV2 ConvertToV2(SnmpEntry s)
    {
        var result = new SnmpCredentialsV2();
        result.Community = s.Name;
        result.Name = s.Name;
        return result;
    }

    private List<SnmpCredentialsV3> LoadSNMPV3SharedCredentials()
    {
        List<SnmpCredentialsV3> result = new List<SnmpCredentialsV3>();
        
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            result = businessProxy.GetSharedSnmpV3Credentials(CoreConstants.CoreCredentialOwner);
        }

        if (this.FIPSModeEnabled)
        {
            // filter out non FIPS compatible credentials
            result = result.Where(CredentialHelper.IsFIPSCompatible).ToList();
        }

        return result;
    }

    private List<SnmpCredentialsV3> LoadFilteredSNMPV3SharedCredentials()
    {
        List<SnmpCredentialsV3> result = this.LoadSNMPV3SharedCredentials();
        
        var coreConfiguration = configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        var credentialsNotStoredInConfiguration = result.Where(c => !coreConfiguration.Credentials.Any(cc => cc.ID.HasValue && cc.ID.Value == c.ID.Value));
        var temporarySnmpCreds = configuration.Snmp.Where(s => s.Version == SNMPVersion.SNMP3).Select(s => ConvertToV3(s));
        var credsList = credentialsNotStoredInConfiguration.ToList();
        
        credsList.AddRange(temporarySnmpCreds);

        return credsList;
    }

    private SnmpCredentialsV3 ConvertToV3(SnmpEntry s)
    {
        var result = new SnmpCredentialsV3();
        result.Name = s.Name;
        result.AuthenticationKeyIsPassword = s.AuthIsKey;
        result.AuthenticationPassword = s.AuthPassword;
        result.AuthenticationType = Convert (s.AuthMethod);
        result.Context = s.Context;
        result.PrivacyKeyIsPassword = s.PrivIsKey;
        result.PrivacyPassword = s.PrivPassword;
        result.PrivacyType = Convert(s.PrivMethod);
        result.UserName = s.UserName;

        return result;
    }

    private SNMPv3PrivacyType Convert(SnmpPrivMethod input)
    {
        switch (input) { 
            case SnmpPrivMethod.AES:
                return SNMPv3PrivacyType.AES128;
            case SnmpPrivMethod.AES192:
                return SNMPv3PrivacyType.AES192;
            case SnmpPrivMethod.AES256:
                return SNMPv3PrivacyType.AES256;
            case SnmpPrivMethod.DES:
                return SNMPv3PrivacyType.DES56;
            default:
                return default(SNMPv3PrivacyType);
        }
    }

    private SNMPv3AuthType Convert(SnmpAuthMethod input)
    {
        switch (input) { 
            case SnmpAuthMethod.MD5:
                return SNMPv3AuthType.MD5;
            case SnmpAuthMethod.SHA:
                return SNMPv3AuthType.SHA1;
            default:
                return default(SNMPv3AuthType);
        }
    }

    // changing of dialog design for v1 and v2c or v3
    protected void SnmpVersion_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.SnmpVersion.SelectedValue == "SNMPv1or2c")
        {
            this.EmptyCommunityStringErrorPanel.Visible = false;
            this.AmbigousCommunityString.Visible = false;
            this.CredEditV1Controls.Visible = true;
            this.CredEditV3Controls.Visible = false;
            this.CredEdit.Width = Unit.Empty;
        }
        else
        {
            InitV3Controls();

            this.CredEditV1Controls.Visible = false;
            this.CredEditV3Controls.Visible = true;
            this.CredEdit.Width = Unit.Empty;
            //this.DeleteV3Template.Enabled = false;
        }
    }

    protected void SnmpV2Credential_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dd = (DropDownList)sender;

        if (dd.SelectedValue == "new")
        {
            this.CommunityStringTB.Text = String.Empty;
            this.CommunityStringTB.Enabled = true;
        }
        else
        {
            int i = int.Parse(dd.SelectedValue);
            this.CommunityStringTB.Text = this.SnmpCredentialsV2[i].Community;
            this.CommunityStringTB.Enabled = false;
        }
    }

    private void SetSnmpV3Editability(bool isEditable)
    {
        this.PrivMethodDropList.Enabled = isEditable;
        this.AuthMetodDropList.Enabled = isEditable;

        //this.AuthPwdInSession = String.Empty;
        this.AuthPassword.IsEnabledPasswordTextBox = isEditable;

        //this.PrivPwdInSession = String.Empty;
        this.PrivPassword.IsEnabledPasswordTextBox = isEditable;

        this.UserNameTextBox.Enabled = isEditable;
        this.ContextTextBox.Enabled = isEditable;
    }

    protected void SnmpCredentialV3ddl_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dd = (DropDownList)sender;
        if (dd.SelectedValue == "new")
        {
            SetSnmpV3Editability(true);
            this.AuthMetodDropList.SelectedIndex = 0;

            this.PrivMethodDropList.SelectedIndex = 0;
            this.PrivMethodDropList.Enabled = false;

            this.AuthPwdInSession = String.Empty;
            this.AuthPassword.IsEnabledPasswordTextBox = false;

            this.PrivPwdInSession = String.Empty;
            this.PrivPassword.IsEnabledPasswordTextBox = false;

            //this.V3TemplateName.Text = String.Empty;
            this.UserNameTextBox.Text = String.Empty;
            this.ContextTextBox.Text = String.Empty;
        }
        else
        {
            int i = int.Parse(dd.SelectedValue);

            this.UserNameTextBox.Text = this.SnmpCredentialsV3[i].UserName;
            this.ContextTextBox.Text = this.SnmpCredentialsV3[i].Context;

            // authentification method
            this.AuthMetodDropList.SelectedIndex = 0;

            if (this.SnmpCredentialsV3[i].AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthNoPriv ||
                this.SnmpCredentialsV3[i].AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthPriv)
            {
                SetAuthMethodInDropDownList(this.SnmpCredentialsV3[i].AuthenticationType);
            }

            if (this.AuthMetodDropList.SelectedIndex > 0)
            {
                this.AuthPassword.IsEnabledPasswordTextBox = true;
                this.PrivMethodDropList.Enabled = true;
                this.AuthPwdInSession = this.SnmpCredentialsV3[i].AuthenticationPassword;
                this.AuthPasswordIsKey.Checked = !this.SnmpCredentialsV3[i].AuthenticationKeyIsPassword;
            }
            else
            {
                this.AuthPassword.IsEnabledPasswordTextBox = false;
                this.AuthPwdInSession = String.Empty;
                this.PrivMethodDropList.Enabled = false;
            }

            // privacy method
            this.PrivMethodDropList.SelectedIndex = 0;

            if (this.SnmpCredentialsV3[i].AuthenticationLevel == SolarWinds.Orion.Core.Models.Enums.SnmpAuthenticationLevel.AuthPriv)
            {
                SetPrivacyMethodDropDownList(this.SnmpCredentialsV3[i].PrivacyType);
            }

            if (this.PrivMethodDropList.SelectedIndex > 0)
            {
                this.PrivPassword.IsEnabledPasswordTextBox = true;
                this.PrivPwdInSession = this.SnmpCredentialsV3[i].PrivacyPassword;
                this.PrivPasswordIsKey.Checked = !this.SnmpCredentialsV3[i].PrivacyKeyIsPassword;
            }
            else
            {
                this.PrivPassword.IsEnabledPasswordTextBox = false;
                this.PrivPwdInSession = String.Empty;
            }
            SetSnmpV3Editability(false);
        }
    }

    protected bool SaveCredentials(SnmpCredentials entry)
    {
        if (this.EditedCredentials == null)
        {
            this.Credentials.Add(entry);
            return true;
        }
        else
        {
            Credentials[Credentials.IndexOf(this.EditedCredentials)] = entry;
            return false;
        }
    }

    private string ReduceUserName(string userName, int length)
    {
        if (userName.Length > length)
        {
            return userName.Substring(1, length) + "...";
        }
        else
        {
            return userName;
        }
    }

    private string FormatUserNameAndContext(string userName, string context)
    {
        if (String.IsNullOrEmpty(context))
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_AK0_21, ReduceUserName(userName, 32));
        }
        else
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_AK0_22, ReduceUserName(userName, 16), ReduceUserName(context, 16));
        }
    }

    protected string FormatCommunity(string userName, SNMPVersion version)
    {
        if (version == SNMPVersion.SNMP1)
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_AK0_23, userName);
        }
        else
        {
            return userName;
        }
    }

    private void UpdateCredentialFromEditor(SnmpCredentialsV3 entry)
    {
        entry.UserName = this.UserNameTextBox.Text;
        entry.Context = this.ContextTextBox.Text;

        if (string.IsNullOrWhiteSpace(entry.Name))
            entry.Name = FormatUserNameAndContext(entry.UserName, entry.Context);

        if (this.AuthMetodDropList.SelectedIndex > 0)
        {
            entry.AuthenticationType = (SNMPv3AuthType)Enum.Parse(typeof(SNMPv3AuthType), this.AuthMetodDropList.SelectedValue);
            // AuthenticationKeyIsPassword needs to be set to true when pasword is not a key and will be encrypted
            entry.AuthenticationKeyIsPassword = !this.AuthPasswordIsKey.Checked;
            entry.AuthenticationPassword = this.AuthPwdInSession;
        }
        else
        {
            entry.AuthenticationType = SNMPv3AuthType.None;
            entry.AuthenticationKeyIsPassword = true;
            entry.AuthenticationPassword = string.Empty;
        }
        
        if (this.PrivMethodDropList.SelectedIndex > 0)
        {
            entry.PrivacyType = (SNMPv3PrivacyType)Enum.Parse(typeof(SNMPv3PrivacyType), this.PrivMethodDropList.SelectedValue);
            // AuthenticationKeyIsPassword needs to be set to true when pasword is not a key and will be encrypted
            entry.PrivacyKeyIsPassword = !this.PrivPasswordIsKey.Checked;
            entry.PrivacyPassword = this.PrivPwdInSession;
        }
        else
        {
            entry.PrivacyType = SNMPv3PrivacyType.None;
            entry.PrivacyKeyIsPassword = true;
            entry.PrivacyPassword = string.Empty;
        }
    }

    // button for adding filled new credentials
    protected void AddCredentials_OnClick(object sender, EventArgs e)
    {
        if (!this.IsValid)
        {
            return;
        }

        bool added = false;

        // add a credentials here
        if (this.SnmpVersion.SelectedValue == "SNMPv1or2c")
        {
            // remove selected credentials from drop down list
            if (snmpV2Credentials.SelectedValue != "new" && this.EditedCredentials == null)
            {
                int i = int.Parse(this.snmpV2Credentials.SelectedValue);
                SaveCredentials(SnmpCredentialsV2[i]);
                SnmpCredentialsV2.RemoveAt(i);
            }
            else
            {
                // validation
                if (String.IsNullOrEmpty(this.CommunityStringTB.Text.Trim()))
                {
                    this.EmptyCommunityStringErrorPanel.Visible = true;
                    this.CredEditV1Controls.Focus();
                    this.CommunityStringTB.Text = String.Empty;
                    return;
                }

                string temp = this.CommunityStringTB.Text.TrimStart().TrimEnd();

                foreach (var entry in this.Credentials)
                {
                    if (entry.Name == temp)
                    {
                        if (this.EditedCredentials == null || this.EditedCredentials != entry)
                        {
                            this.AmbigousCommunityString.Visible = true;
                            this.CredEditV1Controls.Focus();
                            return;
                        }
                    }
                }

                foreach (var credential in SnmpCredentialsV2)
                {
                    if (credential.Name == temp)
                    {
                        if (this.EditedCredentials == null || this.EditedCredentials != credential)
                        {
                            this.AmbigousCommunityString.Visible = true;
                            this.CredEditV1Controls.Focus();
                            return;
                        }
                    }
                }
                
                SnmpCredentialsV2 credentialsToSave;

                if (this.EditedCredentials == null)
                {
                    credentialsToSave = new SnmpCredentialsV2();
                    added = SaveCredentials(new SnmpCredentialsV2()
                    {
                        Community = temp,
                        Description = String.Empty,
                        Name = temp
                    });
                }
                else
                {
                    credentialsToSave = (SnmpCredentialsV2)this.EditedCredentials;

                    credentialsToSave.Community = temp;
                    credentialsToSave.Description = String.Empty;
                    credentialsToSave.Name = temp;

                    added = false;
                }
            }
        }
        else
        {
            if (this.EditedCredentials != null)
            {
                UpdateCredentialFromEditor((SnmpCredentialsV3)this.EditedCredentials);
                added = true;
            }
            else
            {
                var newV3Entry = new SnmpCredentialsV3();

                if (SnmpCredentialsV3ddl.SelectedValue == "new")
                {
                    UpdateCredentialFromEditor(newV3Entry);
                }
                else
                {
                    int i = int.Parse(SnmpCredentialsV3ddl.SelectedValue);
                    newV3Entry = SnmpCredentialsV3[i];
                    SnmpCredentialsV3.RemoveAt(i);
                }

                added = SaveCredentials(newV3Entry);
            }
        }

        this.CredEdit.Visible = false;
       
        BindData();
        this.modalityDiv.ShowBlock = false;
        this.EditedCredentialsIndex = -1;
    }

    // cancel adding new credentials
    protected void CancelCredential_OnClick(object sender, EventArgs e)
    {
        this.CredEdit.Visible = false;
        this.modalityDiv.ShowBlock = false;

        // forget edited credential index
        this.EditedCredentialsIndex = -1;
        BindData();
    }

    #endregion

    #endregion

    #region Wizzard

    protected override bool ValidateUserInput()
    {
        return true;
    }

    protected override void Next()
    {
        if (!ValidateUserInput())
        {
            return;
        }
        ConfigWorkflowHelper.VisitedPages.Visit(this);
    }

    protected override void Back()
    {
        ConfigWorkflowHelper.VisitedPages.Visit(this);
        base.Back();
    }

    #endregion

    #region IStep Members
    public string Step
    {
        get
        {
            return "SNMP";
        }
    }
    #endregion
}
