<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master"
    AutoEventWireup="true" CodeFile="SNMP.aspx.cs" Inherits="Orion_Discovery_Config_SNMP"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_72 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>

<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Controls/PasswordTextBox.ascx"  TagPrefix="orion" TagName="PasswordTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="TopRightPageLinks" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
    <asp:UpdatePanel ID="CredentialsUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="CredEdit" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel" style = "max-width:900;" >
                <p class="DialogHeaderText">
                    <asp:Label ID="ActionHeader" runat="server" Text="" Font-Bold="true"
                        Font-Size="Larger" />
                </p>
                <asp:Panel ID="SNMPVersionPanel" runat="server">                    
                    <p class="SectionHeader">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_63) %></p>
                    <asp:DropDownList ID="SnmpVersion" runat="server" OnSelectedIndexChanged="SnmpVersion_OnSelectedIndexChanged"
                        AutoPostBack="true" CssClass="EditControls" />
                </asp:Panel>
                <asp:Panel ID="CredEditV1Controls" runat="server" BorderWidth="0" Visible="true"                        
                    Class="EditControls">                    
                    <asp:Panel runat="server" ID="ChooseV2Credentials" style="padding-bottom:5px;">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_509) %><br />
                    <asp:DropDownList ID="snmpV2Credentials" runat="server" OnSelectedIndexChanged="SnmpV2Credential_OnSelectedIndexChanged" AutoPostBack="true" >
                    </asp:DropDownList>                    
                    </asp:Panel>
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_64) %><br />
                    <asp:TextBox ID="CommunityStringTB" runat="server" Width="270" Enabled="true" autocomplete="off"
                        MaxLength="250" />
                    <asp:Panel ID="EmptyCommunityStringErrorPanel" runat="server" BorderWidth="0" Visible="false">
                        <span class="ErrorLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_65) %></span>
                    </asp:Panel>
                    <asp:Panel ID="AmbigousCommunityString" runat="server" BorderWidth="0" Visible="false">
                        <span class="ErrorLabel"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_66) %></span>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="CredEditV3Controls" runat="server" Visible="false">
<%--                // TODO: [localization] {comment out}    
                    <p class="SectionHeader">
                        Credential Set</p>
                    <table class="ControlsTable blueBox">
                        <tr>
                            <td class="SectionFirstColumn">
                                Name:
                            </td>
                            <td style="width: 160px;">
                                <asp:TextBox ID="V3TemplateName" runat="server" Width="150" Enabled="true" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Name of a set is required."
                                    ControlToValidate="V3TemplateName" ValidationGroup="template" Display="Dynamic" />
                            </td>
                            <td>
                                <asp:ImageButton ID="SaveV3Template" runat="server" ImageUrl="~/Orion/images/Button.Add.gif"
                                    OnClick="SaveV3Template_OnClick" ValidationGroup="template" OnClientClick="if (Page_IsValid) { showAlert = 1; }" />
                            </td>
                        </tr>
                        <tr>
                            <td class="SectionFirstColumn">
                                Saved Credential Sets:
                            </td>
                            <td style="width: 160px;">
                                <asp:DropDownList ID="V3Template" runat="server" Width="155" AutoPostBack="true"
                                    OnSelectedIndexChanged="V3Template_OnSelectedIndexChanged" />
                            </td>
                            <td>
                                <asp:ImageButton ID="DeleteV3Template" runat="server" ImageUrl="~/Orion/images/Button.Delete.gif"
                                    OnClick="DeleteV3Template_OnClick" OnClientClick="showAlert = 2;" CausesValidation="false"
                                    Enabled="false" />
                            </td>
                        </tr>
                    </table>--%>
                    <p class="SectionHeader">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_67) %></p>
                    <asp:Panel runat="server" ID="ChooseV3Credentials" class="SectionLabel">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_509) %><br />
                    <asp:DropDownList ID="SnmpCredentialsV3ddl" runat="server" OnSelectedIndexChanged="SnmpCredentialV3ddl_OnSelectedIndexChanged" AutoPostBack="true" >
                    </asp:DropDownList>
                    </asp:Panel>
                    <table class="ControlsTable blueBox">
                        <tr>
                            <td class="SectionFirstColumn">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %>
                            </td>
                            <td style="text-align: left;" colspan="4">
                                <asp:TextBox ID="UserNameTextBox" runat="server" Width="150" Enabled="true" autocomplete="off" />
                                <asp:RequiredFieldValidator ID="UserNameTextBoxRequiredFieldValidator" runat="server"
                                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_87 %>" ControlToValidate="UserNameTextBox" EnableClientScript="false" Display="Dynamic" />
                                <asp:CustomValidator ID="DuplicateUserNameValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_88 %>"
                                    ControlToValidate="UserNameTextBox" EnableClientScript="false" OnServerValidate="ValidateUsername" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="SectionFirstColumn" >
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_74) %>
                            </td>
                            <td colspan = "4">
                                <asp:TextBox ID="ContextTextBox" runat="server" Width="150" Enabled="true" autocomplete="off" />
                            </td>
                        </tr> <br/>
                        <tr>
                            <td class="SectionFirstColumn" >
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_75) %>
                            </td>
                            <td>
                                <asp:DropDownList ID="AuthMetodDropList" runat="server" Width="100" AutoPostBack="true"
                                    OnSelectedIndexChanged="AuthMetodDropListOnSelectedIndexChanged" />
                            </td>
                            <td class="SectionSecondColumn">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_76) %>
                            </td>
                            <td>
                                 <orion:PasswordTextBox ID="AuthPassword" runat="server" Width="150" IsEnabledPasswordTextBox="True"/>
                            </td>
                            <td>
                                <asp:CheckBox ID="AuthPasswordIsKey" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="SectionFirstColumn">
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_77) %>
                            </td>
                            <td>
                                <asp:DropDownList ID="PrivMethodDropList" runat="server" Width="100" AutoPostBack="true"
                                    OnSelectedIndexChanged="PrivMethodDropListOnSelectedIndexChanged" />
                            </td>
                            <td class="SectionSecondColumn" >
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_78) %>
                            </td>
                            <td>
                                 <orion:PasswordTextBox ID="PrivPassword" runat="server" Width="150" IsEnabledPasswordTextBox="True"/>
                            </td>
                            <td>
                                <asp:CheckBox ID="PrivPasswordIsKey" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_57 %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButton id="AddCred" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_138 %>"  OnClick="AddCredentials_OnClick" OnClientClick="showAlert = 0;"/>
                        <orion:LocalizableButton id="SaveCred" runat="server" DisplayType="Primary" LocalizedText="Save" OnClick="AddCredentials_OnClick" OnClientClick="showAlert = 0;"/>
                        <orion:LocalizableButton id="CancelAddCred" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelCredential_OnClick" CausesValidation="false" OnClientClick="showAlert = 0;"/></div>
                </asp:Panel>
            </asp:Panel>
            
            <orion:TBox ID="modalityDiv" runat="server" />
            <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_68) %></span>
            <br />
            <div style="width: 100%;">
                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_69) %>
                <br />
                <a target="_blank" rel="noopener noreferrer" href="<%= DefaultSanitizer.SanitizeHtml(HelpHelper.GetHelpUrl("OrionPHAboutSNMP")) %>">
                    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_70) %>
                </a>
            </div>
            <div class="contentBlock">
                <table width="100%">
                    <tr class="ButtonHeader">                    
                        <td style="padding-left: 10px;">
                            <asp:LinkButton ID="AddLinkButton" runat="server" OnClick="AddBtn_OnClick" CssClass="iconLink"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_71) %></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView Width="100%" ID="CredentialsGrid" runat="server" AutoGenerateColumns="False" GridLines="None"
                                CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_86 %>">
                                        <ItemTemplate>
                                        <%# DefaultSanitizer.SanitizeHtml(Container.DataItemIndex + 1) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_85 %>">
                                        <ItemTemplate>
                                            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Encode(FormatCommunity((string)Eval("Name"),(SolarWinds.Orion.Core.Common.Models.SNMPVersion)Eval("Version")))) %>' Width="378" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_84 %>">
                                        <ItemTemplate>
                                            <asp:Label ID="SnmpVersionLabel" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(FormatSNMPVersion((SolarWinds.Orion.Core.Common.Models.SNMPVersion)Eval("Version"))) %>'
                                                Width="100" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_83 %>">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_82 %>" ImageUrl="~/Orion/Discovery/images/icon_up.gif"
                                                OnCommand="ActionItemCommand" CommandName="MoveUp" CommandArgument="<%# Container.DataItemIndex %>" />
                                            <asp:ImageButton ID="MoveDownBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_81 %>" ImageUrl="~/Orion/Discovery/images/icon_down.gif"
                                                OnCommand="ActionItemCommand" CommandName="MoveDown" CommandArgument="<%# Container.DataItemIndex %>" />
                                            <asp:ImageButton ID="EditImageButton" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_80 %>" ImageUrl="~/Orion/Discovery/images/icon_edit.gif"
                                                OnCommand="ActionItemCommand" CommandName="EditItem" CommandArgument="<%# Container.DataItemIndex %>" />
                                            <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_79 %>" ImageUrl="~/Orion/Discovery/images/icon_delete.gif"
                                                OnCommand="ActionItemCommand" CommandName="DeleteItem" CommandArgument="<%# Container.DataItemIndex %>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="NavigationButtons"><tr><td>
            <div class="sw-btn-bar-wizard">
				<orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="imgbBack_Click" CausesValidation="false"/>
                <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
                <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
            </div>
            </td></tr></table>
     </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        //<![CDATA[
        var showAlert = 0;

        customAlerts = function() {

            switch (showAlert) {

                case 1: alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_2) %>'); break;
                case 2: alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_3) %>'); break;
            }

            showAlert = 0;
        }

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(customAlerts);
        //]]>
    </script>

</asp:Content>
