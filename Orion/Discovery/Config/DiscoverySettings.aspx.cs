using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Xml;

public partial class Orion_Discovery_Config_DiscoverySettings : ConfigWizardBasePage, IStep
{
    private const string OrionHSBServerType = "Hot-Standby";
    private const string PrimaryEngineType = "Primary";
    private readonly string MissingEngineLableText1E = Resources.CoreWebContent.WEBCODE_AK0_33;
    private readonly string MissingEngineLableText2EPlus = Resources.CoreWebContent.WEBCODE_AK0_34;

    private new static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    /// <summary> </summary>
    protected new static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

	protected DiscoveryConfiguration Config
	{
		get { return ConfigWorkflowHelper.DiscoveryConfigurationInfo; }
	}

    protected override void  OnInit(EventArgs e)
    {
 	    base.OnInit(e);

        int jobTimeout = WebSettingsDAL.DiscoveryMaximumTimeout;
        this.JobTimeoutSlider.RangeTo = jobTimeout;
        this.JobTimeoutSlider.ErrorMessage = String.Format(Resources.CoreWebContent.WEBCODE_AK0_35, jobTimeout);

        LoadPlugins();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        InitButtons(imgbCancel);

        DiscoveryConfiguration cfg = ConfigWorkflowHelper.DiscoveryConfigurationInfo;

        if (cfg == null)
        {
            throw new ArgumentNullException("Unable to load configuration from session.");
        }


        if (String.IsNullOrEmpty(cfg.Name))
        {
            DateTime now = DateTime.Now;
            cfg.Name = String.Format("{0}: {1}, {2}", this.Profile.UserName, Utils.FormatCurrentCultureDate(now), Utils.FormatToLocalTime(now));
        }

        if (!IsPostBack)
        {
			this.SNMPTimeoutSlider.Value = (int)cfg.SnmpTimeout.TotalMilliseconds;
			this.SearchTimeoutSlider.Value = (int)cfg.SearchTimeout.TotalMilliseconds;
			this.SNMPRetriesSlider.Value = cfg.SnmpRetries;
			this.HopCountSlider.Value = cfg.HopCount;
			this.JobTimeoutSlider.Value = (int)cfg.JobTimeout.TotalMinutes;
            this.NameTB.Text = WebSecurityHelper.SanitizeAngular(cfg.Name);
            this.DecriptionTB.Text = WebSecurityHelper.SanitizeAngular(cfg.Description);
			
            var coreConfig = cfg.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
            if (coreConfig != null)
			{
                this.WMIRetriesSlider.Value = coreConfig.WmiRetries;
                this.WMIRetryIntervalSlider.Value = (int)coreConfig.WmiRetryInterval.TotalMilliseconds;                
            }
        }
        else
        {
			this.Validate();
			if (this.IsValid)
			{
				this.SaveData(cfg);
			}
        }
    }

    private void LoadPlugins()
    {
        var loadedControls = new Dictionary<Control, int>();

        // load plugins from config files
        foreach (OrionModule module in OrionModuleManager.GetModules())
        {
            try
            {
                var xmlDoc = SolarWinds.Orion.Web.Helpers.XmlHelper.ToXmlDocument(module.GetModuleConfiguration());
                var xmlNodes = xmlDoc.DocumentElement.SelectNodes("discoverySettingsPlugins/discoverySettingsPlugin");

                foreach (XmlNode xmlNode in xmlNodes)
                {
                    int order = int.Parse(xmlNode.Attributes["order"].Value);
                    var control = (DiscoverySettingsPluginBase)LoadControl(xmlNode.Attributes["control"].Value);

                    loadedControls.Add(control, order);
                }
            }
            catch (System.Threading.ThreadAbortException threadEx)
            {
                log.DebugFormat("Thread aborted while loading module config file '{0}', possibly a side-effect of Response.Redirect. Details: {1}", module.ConfigFilePath, threadEx);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error while loading discovery settings plugin from '{0}'. Details: {1}", module.ConfigFilePath, ex);
            }
        }
        
        // place ordered collection on page
        var orderedControls = loadedControls.OrderBy(kvp => kvp.Value).Select(kvp => kvp.Key);
        foreach (Control control in orderedControls)
        {
            pluginContainer.Controls.Add(control);
        }
    }

	protected void SaveData(DiscoveryConfiguration cfg)
	{
		cfg.SearchTimeout = TimeSpan.FromMilliseconds(this.SearchTimeoutSlider.Value);
		cfg.SnmpTimeout = TimeSpan.FromMilliseconds(this.SNMPTimeoutSlider.Value);
		cfg.SnmpRetries = this.SNMPRetriesSlider.Value;
		cfg.HopCount = this.HopCountSlider.Value;
		cfg.JobTimeout = TimeSpan.FromMinutes(this.JobTimeoutSlider.Value);
        cfg.Name = WebSecurityHelper.SanitizeAngular(this.NameTB.Text);
        cfg.Description = WebSecurityHelper.SanitizeAngular(this.DecriptionTB.Text);

        var coreConfig = cfg.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (coreConfig != null)
        {
            coreConfig.WmiRetries = this.WMIRetriesSlider.Value;
            coreConfig.WmiRetryInterval = TimeSpan.FromMilliseconds(this.WMIRetryIntervalSlider.Value);
        }
	}

    #region Wizzard

    protected override bool ValidateUserInput()
    {
        return true;
    }

    protected override void Next()
    {
        if (!ValidateUserInput())
        {
            return;
        }
    }

    #endregion

    #region IStep Members
    public string Step
    {
        get
        {
            return "DiscoverySettings";
        }
    }
    #endregion
}
