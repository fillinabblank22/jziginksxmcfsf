﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Resources;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Core.Models.Interfaces;

public partial class Orion_Discovery_Config_DiscoveryScheduling : ConfigWizardBasePage, IStep
{
    public string SchedulesListJson
    {
        get
        {
            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Objects };
            var initialSchedule = new List<ReportSchedule>();
            if (Configuration.CronSchedule != null)
            {
                initialSchedule.Add(Configuration.CronSchedule);
                return JsonConvert.SerializeObject(new List<ISchedule>(initialSchedule), settings);
            }
            return JsonConvert.SerializeObject(initialSchedule, settings);
        }
    }

    private new static Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    // method used for scheduling
    private ScheduleMethod method = ScheduleMethod.NotScheduled;

    // result of conformation dialog 
    private bool? dialogResult = false;

    protected string PostBackStr
    {
        get { return Page.ClientScript.GetPostBackEventReference(this.DialogResult, String.Empty); }
    }

    // loging for bussines layer
    protected new static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    // configuration
    protected DiscoveryConfiguration Configuration
    {
        get { return ConfigWorkflowHelper.DiscoveryConfigurationInfo; }
    }

    protected Boolean HasResults
    {
        get
        {
            if (Configuration.LastRun != null && Configuration.LastRun!=DateTime.MinValue)
                return true;
            return false;
        }
    }

    protected void SaveData()
    {
        switch (this.frequency.Value)
        {
            case "Custom": { this.method = ScheduleMethod.Frequency; } break;
            case "Daily": { this.method = ScheduleMethod.RunAtTime; } break;
            case "Once": { this.method = ScheduleMethod.NotScheduled; } break;
            case "Cron": { this.method = ScheduleMethod.CronExpression; } break;
            default: throw new NotImplementedException(String.Format("Case for frequency: {0} is not implemented.", this.frequency.Value));
        }

        Validate();
        if (!this.IsValid)
        {
            return;
        }

        switch (this.method)
        {
            case ScheduleMethod.RunAtTime:
                {
                    this.Configuration.ScheduleRunAtTime =
                        DateTime.Now.Date.AddHours(this.ScheduleTime.Value.Hour).AddMinutes(this.ScheduleTime.Value.Minute).ToUniversalTime();

                    this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                    this.Configuration.CronSchedule = null;
                } break;

            case ScheduleMethod.Frequency:
                {
                    this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                    this.Configuration.ScheduleRunFrequency =
                        new TimeSpan(Convert.ToInt32(this.HoursTextBox.Text), Convert.ToInt32(this.MinutesDropDown.Text), 0);
                    this.Configuration.CronSchedule = null;
                } break;

            case ScheduleMethod.NotScheduled:
                {
                    this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                    this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                    this.Configuration.CronSchedule = null;
                } break;
            case ScheduleMethod.CronExpression:
                {
                    this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                    this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                    var schedules = CastListToReportSchedule(FrequencyControl.Schedules);
                    if (schedules != null && schedules.Count > 0)
                        this.Configuration.CronSchedule = schedules[0];
                } break;

            default:
                {
                    var exception = new LocalizedExceptionBase(() => CoreWebContent.WEBCODE_VB0_349);
                    log.Error(exception.Message);
                    throw exception;
                } 
        }

        ConfigWorkflowHelper.ExecuteDiscoveryImmediately = this.runNowRB.Checked;
    }
    #region Page Methods

    protected override void OnInit(EventArgs e)
    {
        if (!IsPostBack && (Session["SchedulerStorageEntranceGuid"] == null || Session["SchedulerStorageEntranceGuid"].ToString() == Guid.Empty.ToString()))
        {
            Session["SchedulerStorageEntranceGuid"] = Guid.NewGuid();
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        if (this.Configuration == null)
        {
            throw new ArgumentNullException("Unable to load configuration from session.");
        }

        base.OnLoad(e);
        InitButtons(imgbCancel);
        InitConfirmValidators();
        
        if (!IsPostBack)
        {
            FrequencyControl.WizardGuid = Guid.Parse(Session["SchedulerStorageEntranceGuid"].ToString());
        }
        
        #if DEBUG     
        string script = @"<script language=JavaScript>$('.debugMode').css('display','inline');</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Info", script);    
        #endif

        if (IsPostBack)
        {
            switch (this.DialogResult.Value)
            {
                case "true": this.dialogResult = true; break;
                case "false": this.dialogResult = false; break;
                default: this.dialogResult = null; break;
            }

            this.DialogResult.Value = String.Empty;

            if (dialogResult.HasValue && dialogResult.Value == true)
            {
                bool executeImmediately = this.runNowRB.Checked == true ? true : false;
                StoreValues(executeImmediately);
            }
        }
        else
        {
            this.ScheduleTime.Value = DateTime.MinValue;

            switch (Configuration.ScheduleMethod)
            {
                case ScheduleMethod.RunAtTime:                    
                    this.frequency.Value = "Daily";
                    this.ScheduleTime.Value = this.Configuration.ScheduleRunAtTime.ToLocalTime();
                    break;
                case ScheduleMethod.Frequency:                    
                    this.frequency.Value = "Custom";
                    int minutes = this.Configuration.ScheduleRunFrequency.Minutes;
                    this.MinutesDropDown.Text = minutes < 10 ? String.Format("0{0}", minutes) : minutes.ToString();
                    this.HoursTextBox.Text = ((int)this.Configuration.ScheduleRunFrequency.TotalHours).ToString();
                    break;
                case ScheduleMethod.NotScheduled:                    
                    this.frequency.Value = "Once";
                    break;
                case ScheduleMethod.CronExpression:
                    this.frequency.Value = "Cron";
                    break;
                default:
                    throw new NotSupportedException(String.Format("Frequency {0} is not supported", Configuration.ScheduleMethod.ToString()));
            }

            if (!ConfigWorkflowHelper.ExecuteDiscoveryImmediately)
            {
                dontRunNowRB.Checked = true;
            }
            
            this.ClientScript.RegisterStartupScript(this.Page.GetType(),"SetValues", "FrequencyOnChange();", true);
        }
    }
    private List<ReportSchedule> CastListToReportSchedule(IEnumerable<ISchedule> listOfSchedules)
    {
        var reportSchedulesList = new List<ReportSchedule>();
        foreach (var schedule in listOfSchedules)
        {
            reportSchedulesList.Add(schedule as ReportSchedule);
        }
        return reportSchedulesList;
    }
    private void InitConfirmValidators()
    {
        ConfirmDiscoverButton.Enabled = HasResults;
        ConfirmScheduleButton.Enabled = HasResults;
        ConfirmStoreButton.Enabled = HasResults;
    }

    #endregion

    #region Wizzard
    
    protected override bool ValidateUserInput()
    {
        return true;
    }
    
    private void registerScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    private bool ValidateCronSchedule()
    {
        if (FrequencyControl.Schedules.ToList().Count == 0)
        {
            this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_SO0_141, Resources.CoreWebContent.WEBDATA_AB0_45));
            return false;
        }
        return true;
    }

    protected void Discover_Click(object sender, EventArgs e)
    {
        if (!this.IsValid)
        {
            return;
        }
        StoreValues(true);
    }

    protected void Schedule_Click(object sender, EventArgs e)
    {
        if ( (this.frequency.Value == "Cron" && ValidateCronSchedule()) || (this.frequency.Value != "Cron" && this.IsValid ))
        {
            StoreValues(false);
        }
        else
        {
            return;
        }
    }

    protected void StoreValues(bool executeImediately)
    {
        // save selected data
        SaveData();

        Session["SchedulerStorageEntranceGuid"] = null;
        //TODO old discovery cleanup
        this.Configuration.SipSocket = 0;
        //TODO old discovery cleanup

        //check if profile is not running 
        bool isProfileRunning = false;

        if (this.Configuration.ProfileID.HasValue)
        {            
            isProfileRunning = IsDiscoveryRunning(this.Configuration.ProfileID.Value);            
        }

        if (isProfileRunning && !dialogResult.HasValue)
        {
            //profile is running and dialog has not been shown
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "showConfirm", "ShowConfirmation();", true);
            return;
        }
        
        if (isProfileRunning && dialogResult.HasValue && !dialogResult.Value)
        {
            // user does not confirmed lets jump out
            this.DialogResult.Value = String.Empty;
            return;
        }

        if (isProfileRunning && dialogResult.HasValue && dialogResult.Value)
        {
            // user confirmed lets cancel running discovery
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                businessProxy.CancelOrionDiscovery(Configuration.ProfileID.Value);
            }
            this.modalityDiv.ShowBlock = true;
            this.CredEdit.Visible = true;

            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "waitForCancelDiscovery", String.Format("WaitForCancelDiscovery({0});", Configuration.ProfileID.Value), true);
            return;
        }

        switch (Configuration.ScheduleMethod)
        {
            case ScheduleMethod.RunAtTime:                
                this.Configuration.ScheduleRunAtTime = DateTime.Now.Date.AddHours(this.ScheduleTime.Value.Hour).AddMinutes(this.ScheduleTime.Value.Minute).ToUniversalTime();
                this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                this.Configuration.CronSchedule = null;
                break;

            case ScheduleMethod.Frequency:                
                this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                this.Configuration.ScheduleRunFrequency = new TimeSpan(Convert.ToInt32(this.HoursTextBox.Text), Convert.ToInt32(this.MinutesDropDown.Text), 0);
                this.Configuration.CronSchedule = null;
                break;

            case ScheduleMethod.NotScheduled:
                this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                this.Configuration.CronSchedule = null;
                break;
            case ScheduleMethod.CronExpression:
                this.Configuration.ScheduleRunAtTime = DateTime.MinValue;
                this.Configuration.ScheduleRunFrequency = TimeSpan.Zero;
                var schedules = CastListToReportSchedule(FrequencyControl.Schedules);
                if (schedules != null && schedules.Count > 0)
                    this.Configuration.CronSchedule = schedules[0];
                break;

            default:
                string message = "Undefined type of scheduling: " + Configuration.ScheduleMethod.ToString();
                log.Error(message);
                throw new Exception(message);                
        }

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, this.Configuration.EngineID))
        {
            //Try connection with JobScheduler
            string jobSchedulerErrorMsg;
            if (!businessProxy.TryConnectionWithJobSchedulerV2(out jobSchedulerErrorMsg))
            {
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "JobSchedulerErrorMessage", ControlHelper.JsFormat("FrequencyOnChange();alert('{0}');",string.Format(Resources.CoreWebContent.WEBCODE_AK0_32,jobSchedulerErrorMsg.Replace(Environment.NewLine, " "))), true);
                return;
            }

            if (this.Configuration.ProfileID.HasValue)
            {
                // run old discovery
                DiscoveryConfiguration dbDiscoveryConfiguration = businessProxy.GetOrionDiscoveryConfigurationByProfile(this.Configuration.ProfileID.Value);
                if (dbDiscoveryConfiguration == null)
                {
                    log.ErrorFormat("Discovery configuration is missing for profile {0}",this.Configuration.ProfileID);
                    throw new ApplicationException(String.Format("Discovery configuration is missing for profile {0}", this.Configuration.ProfileID));
                }
                this.Configuration.Status = dbDiscoveryConfiguration.Status;
                businessProxy.UpdateOrionDiscoveryProfile(this.Configuration);
                //TODO delete discovery Results
                businessProxy.DeleteDiscoveryResultsByProfile(this.Configuration.ProfileID.Value);
            }
            else
            {                
                // run new discovery
                this.Configuration.ProfileID = businessProxy.CreateOrionDiscoveryProfile(this.Configuration);                
            }

            // write values into session as processing flags
            this.Session["ProcessProfileID"] = this.Configuration.ProfileID;
            this.Session["ProcessEngineID"] = this.Configuration.EngineId;

			if (executeImediately && !Configuration.ScheduleMethod.Equals(ScheduleMethod.CronExpression))
            {
                this.Session["ExecuteImediately"] = true;
            }

            ConfigWorkflowHelper.Reset();

            Response.Redirect("/Orion/Discovery/Default.aspx");
        }
    }
    
    [ScriptMethod]
    [WebMethod(true)]
    public static bool IsDiscoveryRunning(int profileID)
    {
        using (WebDAL dal = new WebDAL())
        {
            bool isDiscoveryRunning = dal.IsDiscoveryProfileRunning(profileID);
            return isDiscoveryRunning;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        this.ClientScript.RegisterStartupScript(this.Page.GetType(), "updateControlStatus", "FrequencyOnChange();",true);
    }

    #endregion

    #region Wizzard Steps
    protected override void Next()
    {
        if (!ValidateUserInput())
        {
            return;
        }
    }  
    #endregion

    #region IStep Members
    public string Step
    {
        get
        {
            return "DiscoveryScheduling";
        }
    }
    #endregion
}
