﻿using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using System;
using System.Web.UI;

public partial class Orion_Discovery_Config_MonitoringSettings : ConfigWizardBasePage, IStep
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private DiscoveryConfiguration Configuration
    {
        get { return ConfigWorkflowHelper.DiscoveryConfigurationInfo; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        int i = 2;
        foreach (string pluginPath in AutoImportPluginLoader.Instance.Plugins)
        {
            try
            {
                AutoImportWizardStep step = LoadControl(pluginPath) as AutoImportWizardStep;
                if (step == null)
                {
                    log.ErrorFormat("Can't load auto import plugin {0}", pluginPath);
                    continue;
                }
                step.Index = i++;
                this.WizardPanel.Steps.Add(step);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error loading auto import plugin {0}\n{1}", pluginPath, ex);
            }
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        InitButtons(imgbCancel);

        if (!IsPostBack)
        {
            LoadData();
            CallWizardSteps(s => s.LoadData());
        }
        else
        {
            SaveData();

            if (this.SaveAutoImportWizard.Checked)
            {
                CallWizardSteps(s => s.SaveData());
            }
            else
            {
                CallWizardSteps(s => s.LoadData());
            }
        }

        if (ModuleManager.InstanceWithCache.IsThereModule("APM")
            || ModuleManager.InstanceWithCache.IsThereModule("SCM")) // the same logic as in ConfigWorkflowHelper
        {
            // UIM-1163. If there is APM, switch order of the radio buttons
            // this is not nice, but cheapest solution (and we are not going to maintain this forever)
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), this.UniqueID + "2", @"$(document).ready(function () {

var _swap = function(a, b) {
    a = $(a); b = $(b);
    var tmp = $('<span>').hide();
    a.before(tmp);
    b.before(a);
    tmp.replaceWith(b);
};
_swap($('#polling-method-snmp'), $('#polling-method-wmi'));
});", true);
        }
    }

    private void LoadData()
    {
        AutomaticallyMonitorRB.Checked = Configuration.IsAutoImport;
        ManuallySetRB.Checked = !Configuration.IsAutoImport;
        SnmpOnlyCheckBox.Checked = !Configuration.DisableICMP;

        var coreConfig = Configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (coreConfig != null)
            this.rbWMI.Checked = !(this.rbSNMP.Checked = coreConfig.PreferredPollingMethod == PreferredPollingMethod.SNMP);
    }

    private void SaveData()
    {
        Configuration.IsAutoImport = AutomaticallyMonitorRB.Checked;
        Configuration.DisableICMP = !SnmpOnlyCheckBox.Checked;						

        var coreConfig = Configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        if (coreConfig != null)
            coreConfig.PreferredPollingMethod = this.rbSNMP.Checked ? PreferredPollingMethod.SNMP : PreferredPollingMethod.WMI;
    }

    private void CallWizardSteps(Action<AutoImportWizardStep> action)
    {
        foreach (var step in this.WizardPanel.Steps)
        {
            var wizardStep = step as AutoImportWizardStep;
            if (wizardStep != null)
            {
                action(wizardStep);
            }
        }
    }

    public void MonitoringSettings_Reopen(object sender, EventArgs e)
    {
        // TODO: clear or store monitoring settings wizard data before wizard is re-opened ...

        // NOTE: this script will re-open wizard after postback
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), this.UniqueID + "1", @"$(document).ready(function () {  SW.MonitoringSettingsDlg.show();  });", true);
    }

    protected int WizardNavigationStepWidthPercent
    {
        get { return (int)Math.Floor(96.0 / Math.Max(this.WizardPanel.Steps.Count, 1.0)); }
    }

    #region Wizard

    protected override bool ValidateUserInput()
    {
        return true;
    }

    protected override void Next()
    {
        if (!ValidateUserInput())
        {
            return;
        }
    }

    #endregion

    #region IStep Members
    public string Step
    {
        get
        {
            return "MonitoringSettings";
        }
    }
    #endregion
}