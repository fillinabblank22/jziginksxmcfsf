<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Config/DiscoveryWizardPage.master" AutoEventWireup="true"
    CodeFile="Network_Old.aspx.cs" Inherits="Orion_Discovery_Config_Network_Old" Title="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_61 %>" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Web.Discovery"%>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Src="~/Orion/Controls/PageMenu.ascx" TagPrefix="orion" TagName="PageMenu" %>
<%@ Register Src="~/Orion/Discovery/Controls/BulkUpload.ascx" TagPrefix="orion" TagName="BulkUpload" %>
<%@ Register Src="~/Orion/Discovery/Controls/IPRangeList.ascx" TagPrefix="orion" TagName="IPRangeList" %>
<%@ Register Src="~/Orion/Discovery/Controls/SubnetList.ascx" TagPrefix="orion" TagName="SubnetList" %>
<%@ Register Src="~/Orion/Discovery/Controls/ActiveDirectoryList.ascx" TagPrefix="orion" TagName="ActiveDirectoryList" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>


<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<style type="text/css">
    .SelMeth
    {
        color: #fff; 
        font-weight: bold; 
        background: #faa61a top right no-repeat;  
        padding:3px 7px 3px 5px;
        font-size:9pt;
        white-space:nowrap;
    }  
    #NormalText .MenuItem { padding-left: 3px !Important;}
    #NormalText .SelectedMenuItem { padding-left: 3px !Important;}  
    .PageMenu{ width:100% !Important; }
    /* asp.net renders markup differently for chrome. */
    .sw-is-safari .PageMenu div > span { display: block; }
    .sw-is-safari .PageMenu div > br { display: none; }
    .sw-is-safari .PageMenu .PageMenu { border: none; }
</style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />            
    <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_89) %></span>
    <br />
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_90) %>
    <br />
    <orion:HelpHint runat="server">
        <HelpContent>
            <b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_79) %></b> 
            <%= DefaultSanitizer.SanitizeHtml(string.Format(Resources.CoreWebContent.WEBDATA_IB0_81, string.Format("<a class='coloredLink' href=\"{0}\">{1}</a>",
                this.ClientScript.GetPostBackClientHyperlink(this, SolarWinds.Orion.Core.Common.Models.DiscoverySelectionMethod.SpecificNodes.ToString()),
                Resources.CoreWebContent.WEBDATA_IB0_80))) %>
        </HelpContent>
    </orion:HelpHint>
	<div class="contentBlock">
	    <table>
	        <tr>
	            <td class="SelectionMethod">
	                <orion:PageMenu ID="NetworkPageMenu" runat="server" OnClicked="NetworkPageMenu_Clicked" />
	            </td>
	            <td class="IPRange" valign="top" id="ipRangeListTd" runat="server">
	                <orion:IPRangeList runat="server" id="ipRangeList" />
	            </td>
	            <td id="subnetListTd" runat="server" valign="top">
	                <orion:SubnetList runat="server" ID="subnetList" />
	            </td>
                <td id="activeDirectoryTd" runat="server" valign="top">
	                <orion:ActiveDirectoryList runat="server" ID="activeDirectoryList" />
	            </td>   
	            <td id="bulkUploadTd" runat="server">
					<orion:BulkUpload ID="bulkUpload" runat="server" />
	            </td>
	        </tr>
	    </table>
	</div>
	
	<table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
        <asp:HyperLink class="sw-btn-secondary sw-btn" NavigateUrl="/Orion/Discovery/Config/" runat="server">Back</asp:HyperLink>
        <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="imgbNext_Click"/>
        <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="imgbCancel_Click" CausesValidation="false"/>
    </div>
    </td></tr></table>
    
    <%--<asp:UpdatePanel ID="NextUpdatePanel" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="imgbNext" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
