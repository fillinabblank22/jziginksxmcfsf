﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using OrinonCommonModels = SolarWinds.Orion.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.DAL;
using System.Data;
using System.Web.Services;
using CreateDiscoveryJobResult = SolarWinds.Orion.Core.Common.Models.CreateDiscoveryJobResult;
using System.Web;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Models.Discovery;
using Enums = SolarWinds.Orion.Core.Models.Enums;
using SolarWinds.Orion.Web.Discovery;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using SolarWinds.InformationService.Linq;
using SolarWinds.Orion.Core.Common.Swis;

public partial class Orion_Discovery_Default : System.Web.UI.Page
{
    private const string stamp = "requestTimeStamp";
    private const string DiscoverAgentsActionName = "discoverAgents";
    private const string AgentAddressesParameterName = "agentAddresses";
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    #region Properties

    private static Log log = new Log();
    protected int SelectedProfileID { get; set; }
    private DataTable profiles;

    public string CurrentSortExpression
    {
        get
        {
            return Session["ProfilesCurrentSortExpression"] as string;
        }
        set
        {
            Session["ProfilesCurrentSortExpression"] = value;
        }
    }

    public string LastSortExpression
    {
        get
        {
            return Session["ProfilesLastSortExpression"] as string;
        }
        set
        {
            Session["ProfilesLastSortExpression"] = value;
        }
    }

    public string SortDirect
    {
        get
        {
            return Session["ProfilesSortDirection"] as string;
        }
        set
        {
            string sortDir = null;

            if (value == SortDirection.Ascending.ToString())
                sortDir = " ASC";
            else
                if (value == SortDirection.Descending.ToString())
                sortDir = " DESC";
            else
                throw new ArgumentException(String.Format("Direction type {0} is not supported", value));

            Session["ProfilesSortDirection"] = sortDir;
        }
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    #endregion

    private DiscoveryConfigWizardStepsManager stepsManager = new DiscoveryConfigWizardStepsManager();


    #region PageEvents

    protected string FormatedStatus(Enums.DiscoveryStatus status)
    {
        switch (status)
        {
            case Enums.DiscoveryStatus.Error: return Resources.CoreWebContent.WEBCODE_AK0_1;
            case Enums.DiscoveryStatus.Finished: return Resources.CoreWebContent.WEBCODE_AK0_2;
            case Enums.DiscoveryStatus.InProgress: return Resources.CoreWebContent.WEBCODE_AK0_3;
            case Enums.DiscoveryStatus.NotScheduled: return Resources.CoreWebContent.WEBCODE_AK0_4;
            case Enums.DiscoveryStatus.Scheduled: return Resources.CoreWebContent.WEBCODE_AK0_5;
            case Enums.DiscoveryStatus.Unknown: return Resources.CoreWebContent.WEBCODE_AK0_6;
            case Enums.DiscoveryStatus.NotCompleted: return Resources.CoreWebContent.WEBCODE_AK0_7;
            // TODO: [localization] {localize new status}
            case Enums.DiscoveryStatus.Canceling: return Resources.CoreWebContent.WEBCODE_TM0_97;
            case Enums.DiscoveryStatus.ReadyForImport: return Resources.CoreWebContent.WEBCODE_ET0_08;
            default: return "?";
        }
    }

    protected string FormatedStatusDescription(string statusDesc)
    {
        if (string.IsNullOrEmpty(statusDesc))
        {
            log.Warn("Discovery status is undefined (empty)");
            return statusDesc;
        }

        if (statusDesc.Equals("WEBDATA_TP0_ERROR_DURING_DISCOVERY", StringComparison.OrdinalIgnoreCase))
        { // An error has occurred during Network Discovery cancellation: there are no Network Discovery jobs to cancel.&lt;/br&gt;This error may be due to either a lost database connection or a business layer fault condition.
            return Resources.CoreWebContent.WEBDATA_TP0_ERROR_DURING_DISCOVERY;
        }
        else
        if (statusDesc.Equals("WEBDATA_TP0_ERROR_DURING_DISCOVER_NO_JOB", StringComparison.OrdinalIgnoreCase))
        { // An error has occurred during Network Discovery cancellation: there are no Network Discovery jobs to cancel.
            return Resources.CoreWebContent.WEBDATA_TP0_ERROR_DURING_DISCOVER_NO_JOB;
        }
        else
        if (statusDesc.StartsWith("WEBDATA_TP0_DISCOVERY_CANCELLED_BY_USER", StringComparison.OrdinalIgnoreCase))
        { // Discovery has been canceled by user. Results of this discovery are not complete.
            return Resources.CoreWebContent.WEBDATA_TP0_DISCOVERY_CANCELLED_BY_USER;
        }
        else
        if (statusDesc.StartsWith("WEBDATA_TP0_DISCOVERY_INTERRUPTED_BY_TIMEOUT", StringComparison.OrdinalIgnoreCase))
        { // Discovery has been interrupted by timeout. Results of this discovery are not complete.
            return Resources.CoreWebContent.WEBDATA_TP0_DISCOVERY_INTERRUPTED_BY_TIMEOUT;
        }
        else
        if (statusDesc.StartsWith("LIBCODE_TM0_25", StringComparison.OrdinalIgnoreCase))
        { // error message from business layer
            return Resources.CoreWebContent.WEBDATA_TP0_DISCOVERY_INTERRUPTED_BY_TIMEOUT;
        }
        else
        if (statusDesc.StartsWith("WEBDATA_TP0_DISCOVERY_JOB_FAILED", StringComparison.OrdinalIgnoreCase))
        { // error message from business layer
            return Resources.CoreWebContent.WEBDATA_TP0_DISCOVERY_JOB_FAILED;
        }

        log.Warn("Discovery profile message is not localized by website: " + statusDesc);
        return statusDesc;
    }


    private void LoadData()
    {
        using (WebDAL dal = new WebDAL())
        {
            try
            {
                this.profiles = dal.GetAllDiscoveryProfiles();

                this.profiles.Columns.Add("Frequency", typeof(Frequency));

                foreach (DataRow row in this.profiles.Rows)
                {
                    row["Frequency"] = new Frequency(row["ScheduleRunAtTime"], row["ScheduleRunFrequency"], row["CronSchedule"]!=null ? row["CronSchedule"].ToString() : null);
                }

                //if there are no profiles defined, go immediately to the wizard
                if ((this.profiles == null || this.profiles.Rows.Count == 0) && IsAutoRedirectionToWizardEnabled())
                {
                    this.ClearProfile();
                    Response.Redirect("Config");
                }
            }
            catch (ThreadAbortException)
            {
                // Do nothing - ThreadAbortException is always thrown by Redirect method (see http://www.c6software.com/articles/ThreadAbortException.aspx)
            }
            catch (Exception ex)
            {
                log.Error(ex);
                throw;
            }
        }
    }

    // binds credentials data to gridView
    private void BindData(bool detect)
    {
        if (this.profiles != null)
        {
            if (detect)
            {
                string indexString = Request.Form["RBGroup"];

                if (indexString == null)
                {
                    SelectedProfileID = -1;
                }
                else
                {
                    SelectedProfileID = Convert.ToInt32(indexString);
                }
            }

            if (CurrentSortExpression != null)
            {
                this.profiles.DefaultView.Sort = " " + CurrentSortExpression + SortDirect;
            }

            // exists any profile with not existing engine
            bool anyProfileWithNotExistingEngine = this.profiles.Rows.Cast<DataRow>().Any(r => (r["EngineExists"] as int?) == 0);
            notExistingProfileWarningDiv.Visible = anyProfileWithNotExistingEngine;

            this.ProfilesGrid.DataSource = profiles;
            this.ProfilesGrid.DataBind();
        }
    }

    protected bool IsPageExpired()
    {
        if (ViewState[stamp] == null || Session[stamp] == null)
            return false;
        if (ViewState[stamp].ToString() != Session[stamp].ToString())
            return true;
        return false;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        OrionInclude.CoreFile("OrionMaster.js").AddJsInit("$(function() { SW.Core.Header.SyncTo($('.tab-top')); });");
        Response.ExpiresAbsolute = DateTime.Now;
        Response.Expires = 0;
        Response.CacheControl = "no-cache";
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.UtcNow);
        Response.Cache.SetNoStore();
        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

        // demoedit
        // added 
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
        {
            AddDiscoveryButton.OnClientClick = "demoAction('Core_Discovery_Add',this); return false;";
            AddDiscoveryLinkButton.OnClientClick = "demoAction('Core_Discovery_Add',this); return false;";
            DiscoverNowButton.OnClientClick = "demoAction('Core_Discovery_Discover',this); return false;";
            DiscoverNowLinkButton.OnClientClick = "demoAction('Core_Discovery_Discover',this); return false;";
            EditButton.OnClientClick = "demoAction('Core_Discovery_Edit',this); return false;";
            EditLinkButton.OnClientClick = "demoAction('Core_Discovery_Edit',this); return false;";
            DeleteButton.OnClientClick = "demoAction('Core_Discovery_Delete',this); return false;";
            DeleteLinkButton.OnClientClick = "demoAction('Core_Discovery_Delete',this); return false;";
        }
        // / demoedit
    }
    // on load event
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        LicenseLimitNotification();

        if (IsPostBack)
        {
            if (IsPageExpired())
            {
                Response.Redirect(Request.UrlReferrer.AbsolutePath);
            }
            else
            {
                Session[stamp] = ViewState[stamp] = DateTime.Now.Ticks.ToString();
            }
        }
        else
        {
            //set up back button control
            Session[stamp] = ViewState[stamp] = DateTime.Now.Ticks.ToString();
            this.NewResultsButton.DataBind();

            CreateAndRunDiscoveryIfRequested();
        }

        // we have recieved profile to process
        if (this.Session["ProcessProfileID"] != null)
        {
            DiscoveryProgressHelper.ProfileID = (int)this.Session["ProcessProfileID"];
            DiscoveryProgressHelper.EngineID = (int)this.Session["ProcessEngineID"];
            this.Session["ProcessProfileID"] = null;
            this.Session["ProcessEngineID"] = null;

            if (this.Session["ExecuteImediately"] != null)
            {
                // run it now with full dialog
                this.ClientScript.RegisterClientScriptBlock(typeof(Orion_Discovery_Default), "AutoStart", "<script type='text/javascript'> $(StartDiscovery); </script>", false);

                this.Session["ExecuteImediately"] = null;
            }
            else
            {
                // Try to create scheduled job on background
                this.ClientScript.RegisterClientScriptBlock(typeof(Orion_Discovery_Default), "AutoStart", $"<script type='text/javascript'> $(TryCreateScheduledDiscoveryJob); </script>", false);
            }
        }
        else if (!IsPostBack)
        {
            this.Session["ResultImportReturnUrl"] = null;
        }

        try
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                businessProxy.ValidateProfilesTimeout();
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Cannot validate timed-out profiles.\r\n {0}", this.SelectedProfileID), ex);
            throw;
        }

        // load profiles from database
        LoadData();

        // bind data
        BindData(true);
    }

    private void CreateAndRunDiscoveryIfRequested()
    {
        if (this.Request.HttpMethod == "POST")
        {
            string action = this.Request["action"];
            if (string.IsNullOrEmpty(action))
                return;

            if (action.Equals(DiscoverAgentsActionName, StringComparison.InvariantCultureIgnoreCase))
            {
                string agentAddressesStr = this.Request[AgentAddressesParameterName];
                List<string> agentAddresses = agentAddressesStr.Split(',').ToList();
                CreateAndRunOneTimeAgentDiscoveryProfile(agentAddresses);
            }
        }
    }

    private void CreateAndRunOneTimeAgentDiscoveryProfile(List<string> agentAddresses)
    {
        const string AgentTemporaryDiscoveryProfileName = "AgentTemporaryDiscoveryProfile";

        DiscoveryConfiguration configuration = new DiscoveryConfiguration();
        ConfigWorkflowHelper.FillDiscoveryConfiguration(configuration);

        configuration.IsHidden = true;
        configuration.EngineID = EnginesDAL.GetPrimaryEngineId();
        configuration.Name = AgentTemporaryDiscoveryProfileName;

        CoreDiscoveryPluginConfiguration coreConfiguration = configuration.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
        coreConfiguration.AgentsAddresses = agentAddresses;
        coreConfiguration.DiscoverAgentNodes = true;

        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, configuration.EngineID))
        {
            //Try connection with JobScheduler
            string jobSchedulerErrorMsg;
            if (!businessProxy.TryConnectionWithJobSchedulerV2(out jobSchedulerErrorMsg))
            {
                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "JobSchedulerErrorMessage", ControlHelper.JsFormat("FrequencyOnChange();alert('{0}');", string.Format(Resources.CoreWebContent.WEBCODE_AK0_32, jobSchedulerErrorMsg.Replace(Environment.NewLine, " "))), true);
                return;
            }

            businessProxy.DeleteHiddenOrionDiscoveryProfilesByName(AgentTemporaryDiscoveryProfileName);

            configuration.ProfileID = businessProxy.CreateOrionDiscoveryProfile(configuration);

            // write values into session as processing flags
            this.Session["ProcessProfileID"] = configuration.ProfileID;
            this.Session["ProcessEngineID"] = configuration.EngineId;
            this.Session["ResultImportReturnUrl"] = "/Orion/AgentManagement/Admin/ManageAgents.aspx";
            this.Session["ExecuteImediately"] = true;
            DisableAutoRedirectionToWizard();

            Response.Redirect("/Orion/Discovery/Default.aspx");
        }
    }

    /// <summary> </summary>
    protected void ProfileSelectedValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        args.IsValid = (SelectedProfileID >= 0);
    }

    protected void ProfilesSorting(object sender, GridViewSortEventArgs e)
    {
        if (profiles != null && this.profiles.DefaultView.Table.Rows.Count > 1)
        {
            if (this.LastSortExpression == e.SortExpression)
            {
                e.SortDirection = SortDirection.Descending;
                LastSortExpression = null;
            }
            else
            {
                LastSortExpression = e.SortExpression;
            }

            CurrentSortExpression = e.SortExpression;
            this.SortDirect = e.SortDirection.ToString();

            BindData(true);
        }
    }

    protected void ShowProgress_Click(object sender, CommandEventArgs e)
    {
        DiscoveryProgressHelper.ProfileID = Convert.ToInt32(e.CommandArgument);
        DiscoveryProgressHelper.EngineID = new WebDAL().GetEngineIdForDiscoveryProfile(DiscoveryProgressHelper.ProfileID);
        this.ClientScript.RegisterClientScriptBlock(typeof(Orion_Discovery_Default), "AutoStart", "<script type='text/javascript'> $(ContinueDiscovery); </script>");
    }

    protected void RedirectLinkButton_Click(object sender, EventArgs e)
    {
        EnableAutoRedirectionToWizard();


        Response.Redirect("~/Orion/Discovery/Results/Default.aspx");
    }

    private void DisableAutoRedirectionToWizard()
    {
        this.Session["SupressWizardRedirection"] = true;
    }

    private void EnableAutoRedirectionToWizard()
    {
        this.Session["SupressWizardRedirection"] = null;
    }

    private bool IsAutoRedirectionToWizardEnabled()
    {
        return this.Session["SupressWizardRedirection"] == null || !(bool)this.Session["SupressWizardRedirection"];
    }

    #endregion

    #region GUI

    protected string Reduce(object text, int length)
    {
        if (text == null || text == DBNull.Value)
        {
            text = String.Empty;
        }
        if (((string)text).Length > length)
        {
            return ((string)text).Substring(0, length) + "...";
        }
        else
        {
            return (string)text;
        }
    }

    protected int GetProfileIDIndex(int profileID)
    {
        for (int i = 0; i < this.profiles.DefaultView.Count; i++)
        {
            if ((int)this.profiles.DefaultView[i]["ProfileID"] == profileID)
            {
                return i;
            }
        }

        return -1;
    }

    protected string FormatedLastRun(int profileID)
    {
        Object lastRun = this.profiles.DefaultView[this.GetProfileIDIndex(profileID)]["LastRun"];

        if (lastRun == DBNull.Value)
        {
            return String.Empty;
        }

        return ((DateTime)lastRun).ToLocalTime().ToString("f");
    }

    protected string IsChecked(int index)
    {
        if (index == this.SelectedProfileID)
        {
            return " checked=\"checked\"";
        }
        return String.Empty;
    }

    public string Encode(object o)
    {
        if (o == DBNull.Value) return string.Empty;

        return Server.HtmlEncode((string)o);
    }

    protected string GetWarnCssClass(object o)
    {
        return (o as int?) == 0 ? "sw-icon-warn" : string.Empty;
    }

    // Button Events

    private void GoToFirstDiscoveryPageWizard()
    {
        var steps = stepsManager.GetStepsForDiscoveryWizardWorkflow();
        if (steps.Count > 0)
        {
            Response.Redirect(steps.OrderBy(s => s.Order).First().Url);
        }
        else
        {
            Response.Redirect("Config");
        }
    }

    private bool DoesExistAnyNodeExceptAutoDeployed()
    {
        using (SwisContext context = SwisContextFactory.CreateContext())
        {
            return context.Entity<SolarWinds.InformationService.Linq.Plugins.Core.Orion.Nodes>()
                       .Where(n => n.OrionServer.NodeID == null || n.OrionServer.AgentAutoDeploy == false)
                       .Select(n => new {n.NodeID})
                       .Take(1).ToArray().Length > 0;
            // Used .Take(1).ToArray().Length > 0, because .Any or .FirstOrDefault doesn't work see https://jira.solarwinds.com/browse/L2S-28
        }
    }

    protected void AddDiscovery_OnClick(object sender, EventArgs e)
    {
        //let's create a new profile and run the wizard
        this.ClearProfile();
        if (this.profiles == null || this.profiles.Rows.Count == 0 || !DoesExistAnyNodeExceptAutoDeployed())
        {
            Response.Redirect("Config");
        }
        else
        {
            GoToFirstDiscoveryPageWizard();
        }
    }

    private void registerScript(string errorsMessage)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "myalert", string.Format("Ext.Msg.show({0});", errorsMessage), true);
    }

    protected void DiscoverNow_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            var status = (Enums.DiscoveryStatus)(int)this.profiles.DefaultView[GetProfileIDIndex(this.SelectedProfileID)].Row["Status"];

            if (status == Enums.DiscoveryStatus.InProgress)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "deleteAlert", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_AK0_8), true);
                return;
            }

            ResultsWorkflowHelper.Reset();
            DiscoveryConfiguration cfg = null;

            try
            {
                using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                {
                    cfg = businessProxy.GetDiscoveryConfigurationByProfile(this.SelectedProfileID);
                }
            }
            catch (Exception ex)
            {
                log.Error(String.Format("Cannot load discovery profile {0}", this.SelectedProfileID), ex);
                throw;
            }

            if (cfg == null)
            {
                throw new ArgumentNullException("discovery configuration");
            }

            if (cfg.CronSchedule != null)
            {
                this.registerScript(string.Format("{{title: '{0}', msg: '{1}', minWidth: 500, buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR}}", Resources.CoreWebContent.WEBDATA_SO0_112, Resources.CoreWebContent.WEBDATA_SO0_113));
                return;
            }

            bool engineFound = false;
            using (WebDAL dal = new WebDAL())
            {
                DataTable engines = dal.GetPollingEngineStatus();

                foreach (DataRow row in engines.Rows)
                {
                    if (Convert.ToInt32(row["EngineID"]) == cfg.EngineID)
                    {
                        engineFound = true;
                        break;
                    }
                }
            }

            if (!engineFound)
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "startAlert", ControlHelper.JsFormat("alert('{0}');", String.Format(Resources.CoreWebContent.WEBCODE_AK0_9, cfg.Name)), true);
                return;
            }

            DiscoveryProgressHelper.ProfileID = this.SelectedProfileID;
            DiscoveryProgressHelper.EngineID = new WebDAL().GetEngineIdForDiscoveryProfile(DiscoveryProgressHelper.ProfileID);
            this.ClientScript.RegisterClientScriptBlock(typeof(Orion_Discovery_Default), "AutoStart", "<script type='text/javascript'> $(StartDiscovery); </script>", false);
        }
    }

    protected void Delete_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int index = GetProfileIDIndex(this.SelectedProfileID);
            if (index >= 0 && index < this.profiles.DefaultView.Count)
            {

                var status = (Enums.DiscoveryStatus)(int)this.profiles.DefaultView[index].Row["Status"];
                if (status == Enums.DiscoveryStatus.InProgress)
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "deleteAlert", ControlHelper.JsFormat("alert('{0}');", Resources.CoreWebContent.WEBCODE_AK0_10), true);
                    return;
                }

                try
                {
                    using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
                    {
                        businessProxy.DeleteOrionDiscoveryProfile(this.SelectedProfileID);
                    }
                }
                catch (Exception ex)
                {
                    log.Error(String.Format("Cannot delete discovery profile {0}", this.SelectedProfileID), ex);
                    throw;
                }
            }

            int actualIndex = GetProfileIDIndex(this.SelectedProfileID);

            if (actualIndex >= 0 && actualIndex == this.profiles.Rows.Count - 1)
            {
                if (actualIndex == 0)
                {
                    this.SelectedProfileID = -1;
                }
                else
                {
                    this.SelectedProfileID = (int)this.profiles.DefaultView[actualIndex - 1].Row["ProfileID"];
                }
            }
            else
            {
                this.SelectedProfileID = (int)this.profiles.DefaultView[actualIndex + 1].Row["ProfileID"];
            }

            LoadData();
            BindData(false);
        }
    }

    protected void Edit_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            LoadDiscoveryConfiguration(this.SelectedProfileID);
            GoToFirstDiscoveryPageWizard();
        }
    }
    
    protected void Results_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //log.DebugFormat("Loading results for profile {0}.", this.ProfileID);
            ResultsWorkflowHelper.Reset();
            ResultsWorkflowHelper.LoadDiscoveryResults(this.SelectedProfileID, false);

            //log.DebugFormat("Redirecting page.", this.ProfileID);
            Response.Redirect("Results");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void NewResults_OnClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ResultsWorkflowHelper.Reset();
            ResultsWorkflowHelper.LoadDiscoveryResults(this.SelectedProfileID, true);

            Response.Redirect("Results");
        }
    }

    protected static DiscoveryConfiguration LoadDiscoveryConfiguration(int profileID)
    {
        if (profileID < 0)
        {
            ConfigWorkflowHelper.FillDiscoveryConfiguration();
            return null;
        }

        DiscoveryConfiguration configuration = null;
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

        try
        {
            using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                configuration = businessProxy.GetOrionDiscoveryConfigurationByProfile(profileID);
            }
			
			if (!EnginesDAL.GetEngineIds().Contains(configuration.EngineID))
			{
				log.Warn("Discovery profile has EngineId that does not exist. Setting primary EngineId");
				configuration.EngineID = EnginesDAL.GetPrimaryEngineId();
			}
        }
        catch (Exception ex)
        {
            var exception =
                new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(
                    () => CoreWebContent.WEBCODE_VB0_350, ex);
            log.Error(exception.Message, exception.InnerException);
            throw exception;
        }

        if (configuration == null)
        {
            log.Error("Discovery configuration can't be null.");
            throw new ArgumentNullException("discoveryConfigurationInfo");
        }

        ConfigWorkflowHelper.DiscoveryConfigurationInfo = configuration;
        return configuration;
    }

    private void ClearProfile()
    {
        ConfigWorkflowHelper.DiscoveryConfigurationInfo = new DiscoveryConfiguration();
        ConfigWorkflowHelper.FillDiscoveryConfiguration();
    }

    #endregion

    #region PageMethods

    private static CreateDiscoveryJobResult StartJob(ICoreBusinessLayer businessProxy, int profileID, bool executeImediately)
    {
        try
        {
            return businessProxy.CreateOrionDiscoveryJob(profileID, executeImediately);
        }
        catch (Exception ex)
        {
            log.Error("Cannot start discovery", ex);
            throw;
        }
    }

    private static void CreateDiscoveryJob(bool executeImediately, int engineId, int profileId)
    {
        ResultsWorkflowHelper.Reset();
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
        {
            try
            {
                CreateDiscoveryJobResult result = StartJob(businessProxy, profileId, executeImediately);

                log.Info($"Create discovery job result: {result}");

                if (result == CreateDiscoveryJobResult.UnableToChangeRunningJob)
                {
                    string message = Resources.CoreWebContent.WEBCODE_PSR_2;
                    log.ErrorFormat(message, profileId);
                    throw new ApplicationException(String.Format(message, profileId));
                }
            }
            catch (Exception)
            {
                //We have to use new proxy instance, because the old one should be in Fault state
                using (var businessProxyInException = blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
                {
                    var cfg = businessProxyInException.GetDiscoveryConfigurationByProfile(profileId);
                    cfg.Status = new DiscoveryComplexStatus(Enums.DiscoveryStatus.Error, String.Format(Resources.CoreWebContent.WEBCODE_TM0_102, profileId));
                    businessProxyInException.UpdateOrionDiscoveryProfile(cfg);
                    throw new Exception(Resources.CoreWebContent.WEBCODE_PSR_1 + profileId);
                }
            }
        }
    }

    protected static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }


    /// <summary> Call Discovery Engine with configuration specified on UI. It is called through Job Engine. </summary>
    [WebMethod(true)]
    public static void StartDiscovery()
    {
        CreateDiscoveryJob(true, DiscoveryProgressHelper.EngineID, DiscoveryProgressHelper.ProfileID);
    }

    /// <summary> Call Discovery Engine to create discovery job for created/updated discovery profile. It is called through Job Engine. </summary>
    [WebMethod(true)]
    public static void TryCreateScheduledDiscoveryJob()
    {
        CreateDiscoveryJob(false, DiscoveryProgressHelper.EngineID, DiscoveryProgressHelper.ProfileID);
    }

    [WebMethod(true)]
    public static OrionDiscoveryJobProgressInfo GetDiscoveryProgress()
    {
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, DiscoveryProgressHelper.EngineID))
        {
            OrionDiscoveryJobProgressInfo info = businessProxy.GetOrionDiscoveryJobProgress(DiscoveryProgressHelper.ProfileID);
            return info;
        }
    }

    [WebMethod(true)]
    public static void ProcessResult()
    {
        //move to the results processing portion of discovery...
        ResultsWorkflowHelper.LoadDiscoveryResults(DiscoveryProgressHelper.ProfileID, false);
    }

    /// <summary>
    /// Start load of discovery results in asynchronous way.
    /// </summary>
    /// <returns>return key for function GetLoadDiscoveryStatus where you can monitor progress</returns>
    [WebMethod(true)]
    public static string ProcessResultAsyncActive()
    {
        return ResultsWorkflowHelper.LoadDiscoveryResultsAsync(DiscoveryProgressHelper.ProfileID, false);
    }

    /// <summary>
    /// Start load of discovery in asynchronous way.
    /// </summary>
    /// <returns>return key for function GetLoadDiscoveryStatus where you can monitor progress</returns>
    [WebMethod(true)]
    public static string ProcessResultAsync(int profileID, bool onlyNotImported)
    {
        ResultsWorkflowHelper.Reset();
        return ResultsWorkflowHelper.LoadDiscoveryResultsAsync(profileID, onlyNotImported);
    }

    /// <summary>
    /// Retrieve discovery load status
    /// </summary>
    /// <param name="key">key returned from function ProcessResultAsync</param>
    /// <returns>true - load of discovery finished</returns>
    [WebMethod(true)]
    public static bool GetLoadDiscoveryStatus(string key)
    {
        return ResultsWorkflowHelper.GetLoadDiscoveryStatus(key);
    }

    [WebMethod(true)]
    public static void CancelDiscovery()
    {
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, DiscoveryProgressHelper.EngineID))
        {
            businessProxy.CancelOrionDiscovery(DiscoveryProgressHelper.ProfileID);
        }
    }

    #endregion

    #region Nested Classes
    public enum ScheduleType { Daily, Custom, Manual };
    public class Frequency : IComparable
    {
        private ReportSchedule cronSchedule;
        public Frequency(object scheduleRunAtTimeValue, object scheduleRunFrequencyValue, string encodedCronSchedule)
        {
            scheduleType = ScheduleType.Manual;

            if (scheduleRunAtTimeValue is DateTime)
            {
                scheduleRunAtTime = (DateTime)scheduleRunAtTimeValue;
                scheduleType = ScheduleType.Daily;
            }
            else
            {
                scheduleRunAtTime = DateTime.MinValue;
            }

            if (scheduleRunFrequencyValue is int)
            {
                scheduleRunFrequency = TimeSpan.FromMinutes((int)scheduleRunFrequencyValue);
                scheduleType = ScheduleType.Custom;
            }
            else
            {
                scheduleRunFrequency = TimeSpan.FromMinutes(0);
            }

            this.cronSchedule = Parse(encodedCronSchedule);

            if (this.cronSchedule != null)
            {
                scheduleType = ScheduleType.Custom;
            }
        }

        private static ReportSchedule Parse(string encodedCronSchedule)
        {
            if (string.IsNullOrEmpty(encodedCronSchedule))
                return null;
            try
            {

                using (var reader = new StringReader(encodedCronSchedule))
                {
                    var xml = new XmlSerializer(typeof(ReportSchedule));
                    return (ReportSchedule)xml.Deserialize(reader);
                }

            }
            catch (Exception e)
            {
                log.ErrorFormat("Could not parse Cron schedule from encoded string '{0}': {1}", encodedCronSchedule, e);
                throw;
            }
        }

        private ScheduleType? scheduleType = null;
        public ScheduleType ScheduleType
        {
            get
            {
                return scheduleType.Value;
            }
        }

        private DateTime scheduleRunAtTime;
        public DateTime ScheduleRunAtTime
        {
            get
            {
                return scheduleRunAtTime;
            }
        }

        private TimeSpan scheduleRunFrequency;
        public TimeSpan ScheduleRunFrequency
        {
            get
            {
                return scheduleRunFrequency;
            }
        }

        public override string ToString()
        {
            if (ScheduleType == ScheduleType.Daily)
            {
                return String.Format(Resources.CoreWebContent.WEBCODE_AK0_12, scheduleRunAtTime.ToLocalTime().ToString("t"));

            }
            else
            {
                if (this.ScheduleType == ScheduleType.Custom)
                {
                    if (cronSchedule != null && !string.IsNullOrEmpty(cronSchedule.DisplayName))
                    {
                        return cronSchedule.DisplayName;
                    }

                    return (scheduleRunFrequency.Minutes > 0) ?
                        String.Format(Resources.CoreWebContent.WEBCODE_AK0_76, (int)scheduleRunFrequency.TotalHours, scheduleRunFrequency.Minutes) :
                            String.Format(Resources.CoreWebContent.WEBCODE_AK0_77, (int)scheduleRunFrequency.TotalHours);
                }
                else
                {
                    return Resources.CoreWebContent.WEBCODE_AK0_13;
                }
            }
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            Frequency frequency = obj as Frequency;
            if (frequency == null)
                throw new NotSupportedException(String.Format("Type {0} can't be compared to {1}", obj.GetType(), this.GetType()));

            if (this.ScheduleType == ScheduleType.Daily && frequency.ScheduleType == ScheduleType.Daily)
                return this.ScheduleRunAtTime.CompareTo(frequency.ScheduleRunAtTime);
            if (this.ScheduleType == ScheduleType.Custom && frequency.ScheduleType == ScheduleType.Custom)
                return this.ScheduleRunFrequency.CompareTo(frequency.ScheduleRunFrequency);
            if (this.ScheduleType == ScheduleType.Manual && frequency.ScheduleType == ScheduleType.Manual)
                return 0;
            if (this.ScheduleType == ScheduleType.Daily)
            {
                if (frequency.ScheduleType == ScheduleType.Custom)
                    return -1;
                if (frequency.ScheduleType == ScheduleType.Manual)
                    return 1;
            }
            if (this.ScheduleType == ScheduleType.Custom)
            {
                if (frequency.ScheduleType == ScheduleType.Daily)
                    return 1;
                if (frequency.ScheduleType == ScheduleType.Manual)
                    return 1;
            }

            // this is manual
            return -1;
        }

        #endregion
    }
    #endregion

    #region Notification

    private void LicenseLimitNotification()
    {
        using (var proxy = _blProxyCreator.Create(ex => log.Error(ex)))
        {
            var moduleSaturationInfo = proxy.GetModuleSaturationInformation();

            if (moduleSaturationInfo != null && moduleSaturationInfo.Count > 0)
            {
                var firstColumnList = moduleSaturationInfo.SelectMany(moduleInfo =>
                    moduleInfo.ElementList).ToDictionary(
                    elementInfo => String.Format(Resources.CoreWebContent.WEBDATA_ZS0_010, elementInfo.ElementType.ToLower()),
                    elementInfo => "{0} " + (elementInfo.MaxCount - elementInfo.Count) + " {1} " + Resources.CoreWebContent.WEBDATA_ZS0_009 + elementInfo.MaxCount);

                this.licenseLimitHint.AreTwoColumns = true;
                this.licenseLimitHint.HintColumnRows = new Dictionary<string, string>() { { Resources.CoreWebContent.WEBDATA_ZS0_006, "/Orion/Nodes/Default.aspx?CustomPropertyId=Orion.Nodes:Nodes" } };
                this.licenseLimitHint.LicenseLimitInfoList = firstColumnList;
            }
        }
    }

    #endregion
}
