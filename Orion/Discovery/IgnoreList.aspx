<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_169 %>" Language="C#" MasterPageFile="~/Orion/Discovery/DiscoveryPage.master" AutoEventWireup="true" CodeFile="IgnoreList.aspx.cs" Inherits="Orion_Discovery_IgnoreList" %>
<%@ Register Src="~/Orion/Discovery/Controls/DiscoveryPageHeader.ascx" TagPrefix="discovery" TagName="DiscoveryPageHeader" %>
<%@ Register Src="~/Orion/Discovery/Controls/IgnoreList.ascx" TagPrefix="discovery" TagName="IgnoreList" %>
<%@ Register Src="~/Orion/Discovery/Controls/DiscoveryTabBar.ascx" TagPrefix="discovery" TagName="DiscoveryTabBar"%>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="discovery" TagName="Search" Src="~/Orion/Discovery/Controls/Search.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
<orion:TBox ID="modalityDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_59 %>" DivVisible="false"
            MessageVisible="true" />
    <orion:IncludeExtJs ID="IncludeExtJs1" runat="server" Debug="false" />
    <orion:Include runat="server" File="Discovery.css" />
    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function() {
           // demoedit - added IsDemoServer condition
            <%if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer || Profile.AllowAdmin) {%>
                $(".removeButton").click(function() {
                    var anySelected = false;
                    $(".Selector input[type=checkbox]").each(function() {
                        if (this.checked) {
                            anySelected = true;
                        }
                    });
                    $(".NetObjectSelector").each(function() {
                        if (this.checked) {
                            anySelected = true;
                        }
                    });
                    if (anySelected == false) {
                        $(".warning").css("display", "inline");
                        return false;
                    }

                    if (confirm('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_1) %>') != true) {
                        return false;
                    } else {
                        $('.TransparentBoxID').show();
                    }
                });
           <% }%>
        });
        //]]>
    </script>
  
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
	<orion:IconLinkExportToPDF runat="server" />
	<orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHDiscoveryHome" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div>

        <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

        <discovery:DiscoveryTabBar ID="DiscoveryTabBar" runat="server" Visible="true" />
        <div class="tab-top">
            <div class="contentBlock" >
                <table style="border: solid 1px #dfdfde; width: 100%;">
                    <tr class="ButtonHeader">
                        <td style="padding-left: 10px;">
                            <span class="toolbarButton">
                                <asp:ImageButton ID="RemoveButton" 
                                    class="removeButton" 
                                    runat="server" 
                                    ImageUrl="~/Orion/Discovery/images/remove.gif" 
                                    CausesValidation="false" 
                                    OnClick="ibRemove_OnClick" 
                                    />
                                <asp:LinkButton ID="RemoveLinkButton" 
                                    class="removeButton" 
                                    runat="server" 
                                    CausesValidation="false" 
                                    OnClick="ibRemove_OnClick" 
                                    ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_57) %></asp:LinkButton>
                                 <span id="searchForNodes" style="display: inline; float: right">
                                     <discovery:Search ID="SearchNodes" SelectorForSearch="[search=\'searchableRow\'] *" OnEnterKeyDown="RunSearchNodes_Click" OnSeacrhClicked="RunSearchNodes_Click" OnCleanClicked="RunCleanSearchNodes_Click" runat="server"/>
                                 </span>
                            </span>
                        </td>
                    </tr>
                </table>
                <discovery:IgnoreList ID="IgnoreList" runat="server" />
            </div>
            <%--<table class="NavigationButtons">
            <tbody>
                <tr>
                    <td>
                        <asp:ImageButton CssClass="removeButton" ID="ibRemove" runat="server" ImageUrl="~/Orion/Discovery/images/Button.Remove.gif"
                            ToolTip="Remove selected nodes from the Ignore List"  OnClick="ibRemove_OnClick"/> // TODO: [localization] {comment out}
                    </td>
                </tr>                
            </tbody>
        </table>    --%>
            <span class="warning"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_58) %></span>
        </div>
    </div>
</asp:Content>
