﻿using System;
using System.Linq;
using System.Web.UI;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Web.Discovery;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Interfaces;

[ToolboxData("<{0}:IgnoreSubTree runat=server></{0}:IgnoreSubTree>")]
public partial class Orion_Discovery_Controls_IgnoreSubTree : System.Web.UI.UserControl
{
    [Obsolete("Don't use this field - will be removed.", true)]
    public static bool isInterfaceAllowed = new RemoteFeatureManager().AllowInterfaces;

    private static bool isNPMInstalled = ModuleManager.InstanceWithCache.IsThereModule("NPM");

    private DiscoveryIgnoredNodeUI discoveryIgnoredNodeUI;

    public int DiscoveryIgnoredNodeID
    {
        get
        {
            if (ViewState["DiscoveryIgnoredNodeID"] != null)
                return Convert.ToInt32(ViewState["DiscoveryIgnoredNodeID"]);
            else
                throw new ArgumentNullException("Discovery Ignored Node ID is not set");
        }
        set
        {
            ViewState["DiscoveryIgnoredNodeID"] = value;
        }

    }
    
    public bool IsExpanded
    {
        get
        {
            if (ViewState["IsExpanded"] != null)
                return Convert.ToBoolean(ViewState["IsExpanded"]);
            else
                return false; 
        }
        set
        {
            ViewState["IsExpanded"] = value;
        }
    }

    protected DiscoveryIgnoredNodeUI DiscoveryIgnoredNodeUI
    {
        get
        {
            if (discoveryIgnoredNodeUI == null)
            {
                discoveryIgnoredNodeUI = ScheduleDiscoveryHelper.IgnoredNodes.SingleOrDefault(n => n.DiscoveryIgnoredNode.ID == DiscoveryIgnoredNodeID);
            }
            return discoveryIgnoredNodeUI;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    { 
    }    

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if ((DiscoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredInterfaces.Count < 1 || !isNPMInstalled) &&
            discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredVolumes.Count < 1 && 
            discoveryIgnoredNodeUI.DiscoveryIgnoredNode.NodeResult.PluginResults.Any( n => n.GetDiscoveredObjects().Any()) == false 
            )
        {
            ExpandImageButton.Visible = false;
        }

        if (IsExpanded)
        {
            ExpandImageButton.ImageUrl = "~/Orion/images/Button.Collapse.gif";            
            if (DiscoveryIgnoredNodeUI != null)
            {
                InterfaceListRepeater.Visible = isNPMInstalled;
                if (isNPMInstalled)
                {
                    InterfaceListRepeater.DataSource = DiscoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredInterfaces;
                    InterfaceListRepeater.DataBind();
                }

                VolumeListRepeater.DataSource = DiscoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredVolumes;
                VolumeListRepeater.DataBind();

                PluginsRepeater.DataSource = DiscoveryIgnoredNodeUI.GroupDefinitions;
                PluginsRepeater.DataBind();
            }
        }
        else
        {
            ExpandImageButton.ImageUrl = "~/Orion/images/Button.Expand.gif";
        }        
    }

    protected void ExpandImageButton_Click(Object sender, EventArgs e)
    {                
        DiscoveryIgnoredNodeUI.IsExpanded = !DiscoveryIgnoredNodeUI.IsExpanded;
    }      

    protected string GetId(int netObjectID)
    {
        return String.Format("IsSelected_{0}_{1}", DiscoveryIgnoredNodeID, netObjectID);
    }

    protected IEnumerable<object> GetGroupItems(object item)
    {
        var groupdef = (DiscoveryItemGroupDefinition)item;

        return this.DiscoveryIgnoredNodeUI.DiscoveryIgnoredNode.NodeResult.PluginResults
            .SelectMany(n => n.GetDiscoveredObjects())
            .Where(groupdef.Group.IsMyGroupedObjectType);      
    }

    protected string GetItemTypeIcon(IDiscoveredObject item)
    {
        IDiscoveredObjectWithIcon itemWithIcon = item as IDiscoveredObjectWithIcon;

        if (itemWithIcon == null)
            return String.Empty; // < some default icon ??

        return itemWithIcon.Icon;
    }

    protected string GetId(RepeaterItem container)
    {
        var group = (DiscoveryItemGroupDefinition)((RepeaterItem)container.Parent.Parent).DataItem;
        //var item = (IDiscoveredObject)container.DataItem;

        return String.Format("IsSelected_{0}_{1}_{2}", DiscoveryIgnoredNodeID, group.GroupID, container.ItemIndex);
    }
    protected string GetOnClick(RepeaterItem container)
    {
        var group = (DiscoveryItemGroupDefinition)((RepeaterItem)container.Parent.Parent).DataItem;
        //var item = (IDiscoveredObject)container.DataItem;

        return String.Format("ItemSelectionChanged(this.checked, {0}, \"{1}\", {2});", DiscoveryIgnoredNodeID, group.GroupID, container.ItemIndex);
    }
}
