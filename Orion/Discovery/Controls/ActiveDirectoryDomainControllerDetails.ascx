<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveDirectoryDomainControllerDetails.ascx.cs" Inherits="Orion_Discovery_Controls_ActiveDirectoryDomainControllerDetails" %>


<div class="editor-dialog-section">
    <div class="description"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_64) %></div>
    <div class="section-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_65) %></div>
    <div class="editor-dialog-control-group">
        <input type="text" id="ADHostName" />
        <span id="ADHostNameEmpty" class="sw-validation-error" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_80) %></span>
        <div class="description"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_66) %></div>
                
    </div>
</div>

<div class="editor-dialog-section">
    <div class="section-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_68) %></div>
    <div class="editor-dialog-control-group" id="radioCreds">
        <input type="radio" id="radioCredsSelect" name="choice" /><label for="radioCredsSelect"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_70) %></label>
                
       <input type="radio" id="radioCredsCreate" name="choice"  /><label for="radioCredsCreate"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_69) %></label>
    </div>
    <div id="dialogCredsCreate">
        <div class="editor-dialog-control-group">
            <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_71) %></div>
            <input type="text" id="CredsName" />
            <span id="CredsNameEmpty" class="sw-validation-error" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_81) %></span>
            <span id="CredsNameExists" class="sw-validation-error" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_82) %></span>
        </div>

        <div class="editor-dialog-control-group">
            <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_72) %></div>
            <input type="text" id="CredsUser" />
            <span id="CredsUserEmpty" class="sw-validation-error" ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_83) %></span>
            <div class="description"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_73) %> <br /> <a href="<%= DefaultSanitizer.SanitizeHtml(HelpLink) %>" target="_blank" rel="noopener noreferrer" class="sw-link">&raquo;<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WhatPrivilegesDoINeedHelpLinkLabel) %></a></div>

        </div>

        <div class="editor-dialog-control-group">
            <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_74) %></div>
            <input type="password" id="CredsPwd" autocomplete="off" aria-autocomplete="none" />
        </div>
    </div>

    <div id="dialogCredsSelect">
        <div class="editor-dialog-control-group">
            <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_75) %></div>
            <select id="CredsSelect"></select>
        </div>
    </div>


    <div class="editor-dialog-control-group">
        <asp:Button ID="btnCredsTest" runat="server" ClientIDMode="Static" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_43 %>" />
        <img id="testSpinner" src="../images/spinner.gif" class="spinner hidden" />
        <span id="testSuccess" class="success hidden"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_44) %></span>
        <span id="testFailed" class="error hidden"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_45) %></span>

        <div id="testError" class="error hidden"></div>
                
    </div>

</div>
