<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSubnetDialog.ascx.cs" Inherits="Orion_Discovery_Controls_AddSubnetDialog" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<orion:TBox ID="ModalityDiv" runat="server" />

<asp:Panel ID="AddSubnetDialog" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel" style="left: 177px; top: 266px;">
    <div class="DialogHeaderText IEWidth100">
        <asp:Label ID="ActionHeader" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_146 %>" Font-Bold="true" Font-Size="Larger" />
    </div>
    <div class="EditControls IEWidth100">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_142) %><br />
        <asp:TextBox ID="SubnetAddressText" runat="server" Width="270" Enabled="true" autocomplete="off"
            MaxLength="16" class="SubnetAddressText" /><br />
        <asp:RequiredFieldValidator
            ID="SubnetAddressRequiredValidator" 
            runat="server" 
            ControlToValidate="SubnetAddressText"
            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_147 %>" 
            Display="Dynamic" ValidationGroup="AddSubnetDialog" />
        <orion:IPAddressValidator runat="server" ID="SubnetAddressValidator" ControlToValidate="SubnetAddressText" 
            ValidationGroup="AddSubnetDialog" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_148 %>"/>
        <asp:CustomValidator runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_149 %>" Display="Dynamic" 
            OnServerValidate="CheckSubnet" EnableClientScript="false" ValidationGroup="AddSubnetDialog"/>
        <br />
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_143) %><br />
        <asp:TextBox ID="SubnetMaskText" runat="server" Width="270" Enabled="true" autocomplete="off"
            MaxLength="16" class="SubnetMaskText" onfocus="PrepareSubnetMask();" /><br />
        <asp:RequiredFieldValidator
            ID="SubnetMaskRequiredValidator" 
            runat="server" 
            ControlToValidate="SubnetMaskText"
            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_150 %>" 
            Display="Dynamic" ValidationGroup="AddSubnetDialog" />
        <orion:IPAddressValidator runat="server" ID="SubnetMaskValidator" ControlToValidate="SubnetMaskText" 
            ValidationGroup="AddSubnetDialog" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_148 %>"/>
    </div>
    <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons IEWidth100">
        <div class="sw-btn-bar">
            <orion:LocalizableButton id="AddButton" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_138 %>" OnClick="AddButton_OnClick" ValidationGroup="AddSubnetDialog" />
            <orion:LocalizableButton id="CancelAddButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelAddButton_OnClick" CausesValidation="false"/></div>
    </asp:Panel>
</asp:Panel>

<script type="text/javascript">
    //<![CDATA[
    var PrepareSubnetMask = function() {
        var address = $(".SubnetAddressText").val();
        if ($(".SubnetMaskText").val() == "" && address.indexOf(".") > 0) {
            var firstByte = parseInt(address.substr(0, address.indexOf(".")));
            if (!isNaN(firstByte)) {
                var mask;
                if (firstByte < 128) mask = "255.0.0.0"
                else if (firstByte < 192) mask = "255.255.0.0"
                else mask = "255.255.255.0";
                $(".SubnetMaskText").val(mask);    
            }
        }
    }
    //]]>
</script>