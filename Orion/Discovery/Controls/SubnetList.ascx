﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubnetList.ascx.cs" Inherits="Orion_Discovery_Controls_SubnetList" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register Src="~/Orion/Discovery/Controls/AddSubnetDialog.ascx" TagPrefix="orion" TagName="AddSubnetDialog" %>
<%@ Register Src="~/Orion/Discovery/Controls/AddSeedRouterDialog.ascx" TagPrefix="orion" TagName="AddSeedRouterDialog" %>

<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<style type="text/css">
    .NetObjectTable TH {white-space:normal !important;}
</style>
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
<asp:UpdatePanel ID="SubnetUpdatePanel" runat="server">
    <ContentTemplate>        
        <orion:AddSubnetDialog ID="AddSubnetDialog" runat="server" OnSubnetAdded="AddSubnetDialog_SubnetAdded"/>
        <orion:AddSeedRouterDialog ID="AddSeedRouterDialog" runat="server" OnSeedRouterAdded="AddSeedRouterDialog_SeedRouterAdded" />
    
        <table class="NetObjectTable SubnetTable" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
            <tr class="ButtonHeader">
                <th colspan="4" style="border-bottom: solid 1px #FFFFFF;">
                    <asp:ImageButton ID="AddSubnetBtn" runat="server" ImageUrl="~/Orion/Discovery/images/icon_add.gif" 
                        OnClick="AddSubnetBtn_OnClick" CausesValidation="false" />
                    <asp:LinkButton ID="AddSubnetLinkBtn" runat="server" OnClick="AddSubnetBtn_OnClick" CausesValidation="false" >
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_99) %>
                    </asp:LinkButton>
                    <asp:ImageButton ID="AddSeedBtn" runat="server" ImageUrl="~/Orion/Discovery/images/icon_add.gif" 
                        OnClick="AddSeedBtn_OnClick"  CausesValidation="false" />
                    <asp:LinkButton ID="AddSeedLinkBtn" runat="server" OnClick="AddSeedBtn_OnClick" CausesValidation="false" >
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_100) %>
                    </asp:LinkButton>
                </th>
            </tr>
            <asp:ListView runat="server" ID="SubnetGrid" OnItemDataBound="SubnetGrid_ItemDataBound">
                <LayoutTemplate>
                        <tr class="SubnetGridHeader">
                            <th style="width: 10px;">&nbsp;</th>
                            <th style="width: 200px; border-right: solid 1px #FFFFFF;" colspan="2">
                                <asp:Literal ID="Subnet" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_102 %>" />
                            </th>
                            <th style="width: 200px;">
                                <asp:Literal ID="SubnetMask" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_101 %>" />
                            </th>
                        </tr>
                        <asp:PlaceHolder runat="server" id="itemPlaceholder" />
                </LayoutTemplate>   
                <ItemTemplate>
                    <tr class="GroupRow" runat="server" ID="groupHeader" Visible="false">
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="CollapseGroupButton" ImageUrl="/Orion/images/Button.Collapse.gif" OnCommand="CollapseGroup_Click" />
                        </td>
                        <td colspan="2">
                            <asp:Label runat="server" ID="groupLabel" />
                        </td>
                        <td>
                            <orion:LocalizableButton id="SelectGroupButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_140 %>" OnCommand="SelectGroup_Click"/>
                        </td>
                    </tr>
                    <tr runat="server" ID="DataRow">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                            <orion:CustomCheckBox runat="server" ID="SubnetCheck" Checked='<%# Eval("Selected") %>' CustomValue='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetID")) %>' OnCheckedChanged="SubnetCheck_Changed" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetIP")) %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetMask")) %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="GroupRow" runat="server" ID="groupHeader" Visible="false">
                        <td style="width: 10px;">
                            <asp:ImageButton runat="server" ID="CollapseGroupButton" ImageUrl="/Orion/images/Button.Collapse.gif" OnCommand="CollapseGroup_Click" />
                        </td>
                        <td colspan="2">
                            <asp:Label runat="server" ID="groupLabel" />
                        </td>
                        <td>
                            <orion:LocalizableButton id="SelectGroupButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_140 %>" OnCommand="SelectGroup_Click"/>
                        </td>
                    </tr>
                    <tr class="NetObjectAlternatingRow" runat="server" ID="DataRow">
                        <td style="width: 10px;">&nbsp;</td>
                        <td style="width: 10px;">
                            <orion:CustomCheckBox runat="server" ID="SubnetCheck" Checked='<%# Eval("Selected") %>' CustomValue='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetID")) %>' OnCheckedChanged="SubnetCheck_Changed" />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetIP")) %>' />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("SubnetMask")) %>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
            </asp:ListView>
    </table>
    <asp:CustomValidator
        ID="SubnetGridRequiredValidator" 
        runat="server" 
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_103 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
    </ContentTemplate>
</asp:UpdatePanel>