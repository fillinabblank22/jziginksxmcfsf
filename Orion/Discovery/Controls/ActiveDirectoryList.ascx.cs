﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_Discovery_Controls_ActiveDirectoryList : System.Web.UI.UserControl
{
    protected static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

	protected void Page_Load(object sender, EventArgs e)
	{
        DataBind();		
	}

    public void DataBind()
    {
        ModalityDiv.ShowBlock = false;
        this.DataGrid.DataSource = ActiveDirectoryList;
        this.DataGrid.DataBind();

        ActiveDirectoryDetail.ValidateCredentialName = ValidateCredential;
    }
    
    protected void AddBtn_OnClick(object sender, EventArgs e)
    {
        ModalityDiv.ShowBlock = true;
        ActiveDirectoryDetail.AllCredentials = LoadCredentialsFromDB();
        ActiveDirectoryDetail.Display();        
    }

    protected void DialogCanceled(object sender, EventArgs e)
    {
        DataBind();
    }

    protected void DialogSubmited(object sender, EventArgs e)
    {
        if (this.ActiveDirectoryDetail.ActiveItem != null)
        {
            this.ActiveDirectoryList.Add(this.ActiveDirectoryDetail.ActiveItem);
        }

        DataBind();
    }

    public bool ValidateCredential(string credentialName)
    {
        //TODO: ...

        return true;
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        int itemIndex = -1;

        if (e.CommandArgument != null)
            int.TryParse(e.CommandArgument.ToString(), out itemIndex);

        switch (e.CommandName)
        {
            case "DeleteItem":
                DeleteItem(itemIndex);
                break;
        }
    }
   
    void DeleteItem(int selectedIndex)
    {        
        ActiveDirectoryList.RemoveAt(selectedIndex);
        DataBind();        
    }

    List<ActiveDirectoryAccess> ActiveDirectoryList
    {
        get
        {
            var configuration = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();

            if (configuration.ActiveDirectoryList == null)
                configuration.ActiveDirectoryList = new List<ActiveDirectoryAccess>();

            return configuration.ActiveDirectoryList;
        }
    }
    
    public bool CheckEmpty()
    {
        GridValidator.IsValid = ActiveDirectoryList.Count != 0;

        return GridValidator.IsValid;
    }

    private List<UsernamePasswordCredential> LoadCredentialsFromDB()
    {
        using (var proxy = _blProxyCreator.Create((exception) => log.Error(exception)))
        {
            List<UsernamePasswordCredential> wmiCredentials = proxy.GetSharedWmiCredentials(CoreConstants.CoreCredentialOwner);

            return wmiCredentials;
        }
    }
}
