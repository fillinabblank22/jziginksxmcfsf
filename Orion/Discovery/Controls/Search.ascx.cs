﻿using System;

public partial class Orion_Discovery_Controls_Search : System.Web.UI.UserControl
{
    public event EventHandler SeacrhClicked;
    public event EventHandler CleanClicked;
    public event EventHandler EnterKeyDown;

    public string SearchString
    {
        get { return SearchTextBox.Text; }
        set { SearchTextBox.Text = value; }
    }

    public string SelectorForSearch { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClearSearch.Visible = false;
            //always should be invisible
            EnterPressHandle.Visible = false;
        }
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        if (SeacrhClicked != null)
        {
            SeacrhClicked(this, e); 
        }
    }

    protected void ClearButton_Click(object sender, EventArgs e)
    {
        if (CleanClicked != null)
        {
            CleanClicked(this, e);
        } 
    }

    protected void EnterPressHandle_Click(object sender, EventArgs e)
    {
        if (EnterKeyDown != null)
        {
            EnterKeyDown(this, e);
        }  
    }

    public void SetVisibleCleanSearch(bool isVisible)
    {
        if(!string.IsNullOrEmpty(SearchString))
        ClearSearch.Visible = isVisible;
    }

    public void ClearSerachField()
    {
        SearchString = string.Empty;
    }
}