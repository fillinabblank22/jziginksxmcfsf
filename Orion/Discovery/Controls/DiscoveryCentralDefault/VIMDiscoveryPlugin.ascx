<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VIMDiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryCentralDefault_VIMDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="VimImage" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/VmLogo.png"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_43) %></h2>
    <div>
         <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_44) %>
    </div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHDiscoveryHome"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_45 %>" CssClass="helpLink" />
    <br />
    <br />
    <orion:LocalizableButton ID="discoverNetworkButton" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_55 %>" OnClick="Button_Click"
     CommandArgument="~/Orion/Discovery/Default.aspx" runat="server" />

    <orion:LocalizableButton ID="addNodeButton" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_56 %>" OnClick="Button_Click"
     CommandArgument="~/Orion/Nodes/Add/Default.aspx" runat="server" />
</div>
