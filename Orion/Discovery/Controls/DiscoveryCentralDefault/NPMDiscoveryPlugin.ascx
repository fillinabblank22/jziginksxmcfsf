<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NPMDiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryCentralDefault_NPMDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/interfaces_32x32.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_49) %></h2>
    <div>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_50) %>
    </div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHDiscoveryHome"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_45 %>" CssClass="helpLink" />
    <br/>       
    <br/>    
    <orion:LocalizableButton ID="discoverNetworkButton" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_55 %>" DisplayType="Secondary" OnClick="Button_Click" 
    CommandArgument="~/Orion/Discovery/default.aspx" runat="server" /> 
</div>

