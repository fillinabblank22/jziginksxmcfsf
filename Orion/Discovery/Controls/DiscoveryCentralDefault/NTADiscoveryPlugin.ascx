<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NTADiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryCentralDefault_NTADiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/NTA_discovery.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_46) %></h2>
    <div>
       <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_47) %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="NTADiscoveryHelpLink" runat="server" HelpUrlFragment="OrionNetFlowPHSummaryActiveSources"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_48 %>" CssClass="helpLink" />
    <br />
    <br />
    <orion:LocalizableButtonLink NavigateUrl="<%# DefaultSanitizer.SanitizeHtml(HelpUrl) %>" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_VB1_33 %>" runat="server" />
</div>
