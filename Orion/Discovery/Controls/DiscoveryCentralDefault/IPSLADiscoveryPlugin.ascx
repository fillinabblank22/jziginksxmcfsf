<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPSLADiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryCentralDefault_IPSLADiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="IPSLAImage" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/IPSLA_discovery.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_52) %></h2>
    <div>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_53) %>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="IPSLADiscoveryHelpLink" runat="server" HelpUrlFragment="OrionIPSLAManagerAGAddingNodes"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_54 %>" CssClass="helpLink" /></div>
    <br />
    <orion:LocalizableButton ID="discoverIPSLAButton" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_57 %>" DisplayType="Secondary" runat="server" 
    OnClick="AddDeviceButton_Click" />
    <orion:LocalizableButton ID="discoverIPSLAButton2" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_58 %>" DisplayType="Secondary" runat="server" 
    OnClick="MonitorOperationsButton_Click" />
</div>
