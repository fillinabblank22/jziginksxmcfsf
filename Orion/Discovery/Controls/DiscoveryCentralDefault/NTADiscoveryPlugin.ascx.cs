﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Discovery_Controls_DiscoveryCentralDefault_NTADiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();
    }

    protected string HelpUrl
    {
        get { return HelpHelper.GetHelpUrl("OrionNetFlowPHSetupFirstNetFlow"); }
    }
}