﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Discovery_Controls_DiscoveryCentralDefault_IPSLADiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();
    }

    protected void AddDeviceButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/ManageNodes/Default.aspx");
    }

    protected void MonitorOperationsButton_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Leave();
        Response.Redirect("~/Orion/Voip/Admin/OperationsWizard.aspx");
    }

}