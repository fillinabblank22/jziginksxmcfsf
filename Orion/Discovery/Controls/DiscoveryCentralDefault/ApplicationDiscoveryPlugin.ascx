<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationDiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryCentralDefault_ApplicationDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Discovery/Controls/DiscoveryCentralDefault/images/application_discovery.gif"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_59) %></h2>
    <div>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_60) %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="ApplicationDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionAPMAGApplicationDiscovery"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_61 %>" CssClass="helpLink" />
    <br />
    <br />
    <orion:LocalizableButton ID="dsicoverApplicationsButton" LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_62 %>" runat="server" OnClick="DiscoverApplicationsButton_Click" />
</div>
