<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Search.ascx.cs" Inherits="Orion_Discovery_Controls_Search" %>

<script type="text/javascript" language="javascript">
    function EnterEvent(e) {
        if (e.keyCode === 13) {
             __doPostBack("<%=EnterPressHandle.UniqueID%>", "");
            return false;
        }
    }
    function hidePlaceHolder(textBox) {
        textBox.placeholder = '';
    }
    function showPlaceHolder(textBox) {
        textBox.placeholder = '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AY0_104) %>';
    }

    var quoteForRegExp = function(str) {
        var toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
        for (var i = 0; i < toQuote.length; ++i)
            str = str.replace(toQuote.charAt(i), '\\' + toQuote.charAt(i));
        return str;
    }

    $(document).ready(function () {
        var query = $(".textBoxAlign").val();
        var containerId = '<%= SelectorForSearch%>';
        if (containerId.length > 0) {
            var re = new RegExp('(' + quoteForRegExp(query) + ')', 'ig');
            if ($.trim(query) != '') {
                $(containerId).each(function() {
                    if ($(this).children().size() > 0) return;
                    var html = $(this).html();
                    var newhtml = html.replace(re, '<span class="searchterm">$1</span>');
                    if (html) {
                        $(this).html(newhtml);
                    }
                });
            }
        }
    });
</script>

<style type="text/css">
    .textBoxAlign{vertical-align: top;}
</style>

<div>
    <asp:TextBox ID="SearchTextBox" CssClass="textBoxAlign" Width="200" Height="20" onblur="showPlaceHolder(this)" onkeydown = "return EnterEvent(event)" onfocus="hidePlaceHolder(this)" CausesValidation="false" runat="server" Placeholder="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AY0_104 %>" onkeypress="return EnterEvent(event)"></asp:TextBox>
    <asp:Button runat="server" ID="EnterPressHandle" OnClick="EnterPressHandle_Click"/>
    <asp:ImageButton ID="ClearSearch"  runat="server" ImageUrl="~/Orion/images/clear_button.gif" OnClick="ClearButton_Click" />
    <asp:ImageButton ID="SearchButton" runat="server" ImageUrl="~/Orion/images/search_button.gif" OnClick="SearchButton_Click" />
</div>