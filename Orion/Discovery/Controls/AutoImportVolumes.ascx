<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoImportVolumes.ascx.cs" Inherits="Orion_Discovery_Controls_AutoImportVolumes" %>

<style type="text/css">
.volume-types-title 
{
    padding: 5px 0 5px 0;
}
ul.volume-types 
{
    list-style: none;
    padding: 0; 
    margin: 0;
    max-height: 320px;
}
ul.volume-types li 
{
    padding: 2px 0;
}
ul.volume-types li img
{
    padding-right: 5px;
    position: relative;
    top: 2px;
}
</style>

<div class="volume-types-title"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.AutoImportWizardVolumesTitle) %></div>

<asp:CheckBoxList runat="server" ID="VolumeTypeCheckboxes" RepeatLayout="UnorderedList" CssClass="volume-types"/>
