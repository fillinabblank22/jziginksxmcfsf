﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using SolarWinds.Orion.Web.DAL;
using SettingsDAL = SolarWinds.Orion.Web.DAL.SettingsDAL;


public partial class Orion_Discovery_Controls_ScheduleDiscoveryResultsList : System.Web.UI.UserControl
{
    private static string DISCOVERY_NODE_POPUP = @"/Orion/Discovery/DiscoveryNodePopup.aspx";
    private static string NodeID = "NodeID";
    private static string ProfileID = "ProfileID";

    [Obsolete("Don't use this field - will be removed", true)]
	public static bool _isInterfaceAllowed = new RemoteFeatureManager().AllowInterfaces;

    private static bool isNPMInstalled = ModuleManager.InstanceWithCache.IsThereModule("NPM");

    #region Properties    
    
    private int nrOfRecords;
    protected int NrOfRecords
    {
        get { return nrOfRecords; }
        set { nrOfRecords = value; }
    }

    protected int DisplayedNodes { get; set; }
    
    protected int SelectedIndex
    {
        get
        {
            return ViewState["SelectedIndex"] != null ?
              Convert.ToInt32(ViewState["SelectedIndex"]) : 0;
        }
        set {
            ViewState["SelectedIndex"] = value.ToString();
        }
    }
    
    #endregion

    #region Page event handlers

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);        
        this.ScheduleDiscoveryResultsTableGrid.OnGetDataSource+= ScheduleDiscoveryResultsTableGrid_GetDataSource;

        Dictionary<string, string> newNetObjectTipMap = new Dictionary<string, string>() 
            {   {ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, DISCOVERY_NODE_POPUP },                 
                {ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, DISCOVERY_NODE_POPUP},                
            };

        var groups = ScheduleDiscoveryHelper.GetDiscoveryGroupDefinitions();

        groups.ForEach(n => newNetObjectTipMap.Add( ScheduleDiscoveryHelper.GetDiscoveryGroupNetobject(n.GroupID), DISCOVERY_NODE_POPUP));        

        NetObjectFactory.ChangeNetObjectTipMap(newNetObjectTipMap);
    }

    private List<DiscoveryNodeUI> ScheduleDiscoveryResultsTableGrid_GetDataSource()
    {        
        return  ScheduleDiscoveryHelper.ScheduleDiscoveryResults;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        OrionSetting sett = SettingsDAL.GetSetting("DiscoveredNodesLimit");

        bool doLimitount = true;
        if (sett != null)
            doLimitount = Convert.ToBoolean(sett.SettingValue);

        NrOfRecords = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.Count;
        if (ScheduleDiscoveryHelper.ThereIsMoreNodes && doLimitount)
            Warning.Visible = true;
        else
            Warning.Visible = false;
    }    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ScheduleDiscoveryResultsTableGrid.Sort("DateFound",SortDirection.Descending);            
        }

        bool allNodesSelected = false;
        if (AllNodesSelected.Value != String.Empty)
        {
            allNodesSelected = Convert.ToBoolean(AllNodesSelected.Value);
        }
                
        StoreSelection(allNodesSelected);        
    }          

    protected void PageDropDownList_SelectedIndexChanged(Object sender, EventArgs e)
    {
        // Retrieve the pager rows.
        GridViewRow bottomPagerRow = this.ScheduleDiscoveryResultsTableGrid.BottomPagerRow;
        GridViewRow topPagerRow = this.ScheduleDiscoveryResultsTableGrid.TopPagerRow;

        // Retrieve the PageDropDownList DropDownList from the bottom pager row.
        DropDownList bottomPageList = (DropDownList)bottomPagerRow.Cells[0].FindControl("PageDropDownList");
        // Retrieve the PageDropDownList DropDownList from the top pager row.
        DropDownList topPageList = (DropDownList)topPagerRow.Cells[0].FindControl("PageDropDownList");

        if (bottomPageList.SelectedIndex == SelectedIndex)
        {
            // Set the PageIndex property to display that page selected by the user.
            ScheduleDiscoveryResultsTableGrid.PageIndex = topPageList.SelectedIndex;
        }
        else
        {
            // Set the PageIndex property to display that page selected by the user.
            ScheduleDiscoveryResultsTableGrid.PageIndex = bottomPageList.SelectedIndex;
        }

        // discard all selection
        AllNodesSelected.Value = "False";
        // discard already made selection
        DiscardSelection();
    }

    protected void ResultsGridView_DataBound(Object sender, EventArgs e)
    {           
        // Retrieve the pager row.
        GridViewRow pagerRow = ScheduleDiscoveryResultsTableGrid.BottomPagerRow;        
        SetPager(pagerRow);
        pagerRow = ScheduleDiscoveryResultsTableGrid.TopPagerRow;
        SetPager(pagerRow);

        SavePagerStatus();
    }

    protected void ResultsGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("search", "searchableRow");
        }
    }

    protected void ResultsGridView_DataBinding(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ScheduleDiscoveryResultsTableGrid.PageSize = ScheduleDiscoveryHelper.PageSize;
            //this.ScheduleDiscoveryResultsTableGrid.PageIndex = ScheduleDiscoveryHelper.ActualPage < this.ScheduleDiscoveryResultsTableGrid.PageCount ? ScheduleDiscoveryHelper.ActualPage : 0;
            this.ScheduleDiscoveryResultsTableGrid.PageIndex = ScheduleDiscoveryHelper.ActualPage;
        }        
    }
   
    protected void Pager_Click(Object sender, EventArgs e)
    {
        ImageButton pagerAction = sender as ImageButton;

        if(pagerAction!=null)
        {
            switch (pagerAction.CommandArgument)
            {
                case "first":
                    ScheduleDiscoveryResultsTableGrid.PageIndex = 0;
                    break;
                case "last":
                    ScheduleDiscoveryResultsTableGrid.PageIndex = ScheduleDiscoveryResultsTableGrid.PageCount;
                    break;
                case "prev":
                    if(ScheduleDiscoveryResultsTableGrid.PageIndex>0)
                        ScheduleDiscoveryResultsTableGrid.PageIndex--;
                    break;
                case "next":
                    if(ScheduleDiscoveryResultsTableGrid.PageIndex < ScheduleDiscoveryResultsTableGrid.PageCount)
                        ScheduleDiscoveryResultsTableGrid.PageIndex++;
                    break;
                default:
                    throw new NotSupportedException(String.Format("Pager action \"{0}\" is not supported", pagerAction.CommandArgument)); 
            }
        }

        // discard all selection
        AllNodesSelected.Value = "False";
        // discard already made selection
        DiscardSelection();
    }

    protected void pageSizeTextBox_OnChange(Object sender, EventArgs e)
    {        
        TextBox pageSizeTextBox = sender as TextBox;
        int pageSize = Convert.ToInt32(pageSizeTextBox.Text);        
        ScheduleDiscoveryResultsTableGrid.PageSize = pageSize;

        // discard all selection
        AllNodesSelected.Value = "False";
        // discard already made selection
        DiscardSelection();
    }
    #endregion

    #region Public methods
    
    public void BindData(DiscoveryNodeStatus discoveryNodeStatus, DiscoveryResultsFilterType discoveryFilterType, string filterValue)
    {
        object filterObject = null;
        if (filterValue != null)
        {
            switch (discoveryFilterType)
            {
                case DiscoveryResultsFilterType.DateFound:
                    filterObject = (object)Convert.ToDateTime(filterValue);
                    break;
                case DiscoveryResultsFilterType.DiscoveredBy:
                    filterObject = (object)Convert.ToInt32(filterValue);
                    break;
                case DiscoveryResultsFilterType.MachineType:
                    filterObject = (object)filterValue;
                    break;
            }
        }
        bool thereIsMoreNodes;

        OrionSetting sett = SettingsDAL.GetSetting("DiscoveredNodesLimit");

        bool doLimitount = true;
        if (sett != null)
            doLimitount = Convert.ToBoolean(sett.SettingValue);

        ScheduleDiscoveryHelper.ScheduleDiscoveryResults =
                        ScheduleDiscoveryHelper.GetDiscoveredNodes(discoveryNodeStatus, discoveryFilterType, filterObject, out thereIsMoreNodes, doLimitount);
       
        if (!string.IsNullOrEmpty(ScheduleDiscoveryHelper.Search))
        {
            var searchFilter = ScheduleDiscoveryHelper.Search.ToLower();

            ScheduleDiscoveryHelper.ScheduleDiscoveryResults =
                ScheduleDiscoveryHelper.ScheduleDiscoveryResults.Where(x =>
                        x.Name.ToLower().Contains(searchFilter)
                        || x.IPAddress.Contains(searchFilter)
                        || x.MachineType.ToLower().Contains(searchFilter)).ToList();
        }

        ScheduleDiscoveryHelper.ThereIsMoreNodes = thereIsMoreNodes;
    }    

    #endregion

    #region utility methods

    private static readonly Log log = new Log();

    protected bool IsSelectionEnabled(object o)
    {
        DiscoveryNodeUI discoveryNodeUI = o as DiscoveryNodeUI;
        if (discoveryNodeUI == null)
        {
            throw new NotSupportedException(String.Format("Method except instance of {0} class", typeof(DiscoveryNodeUI).Name));
        }

        if (discoveryNodeUI.DiscoveryNode.IsSelected)
            return false;
        else
            return true;                
    }

    private void SavePagerStatus()
    {
        ScheduleDiscoveryHelper.ActualPage = ScheduleDiscoveryResultsTableGrid.PageIndex;
        ScheduleDiscoveryHelper.PageSize = ScheduleDiscoveryResultsTableGrid.PageSize;
    }

    private void SetPager(GridViewRow pagerRow)
    {
        if (pagerRow == null) return;

        pagerRow.Visible = true;

        // Retrieve the DropDownList and Label controls from the row.        
        DropDownList pageList = (DropDownList)pagerRow.Cells[0].FindControl("PageDropDownList");
        Label pageLabel = (Label)pagerRow.Cells[0].FindControl("CurrentPageLabel");
        Label pageCountLabel = (Label)pagerRow.Cells[0].FindControl("PageCountLabel");
        TextBox pageSizeTextBox = pagerRow.Cells[0].FindControl("pageSizeTextBox") as TextBox;

        if (pageList != null)
        {

            // Create the values for the DropDownList control based on 
            // the  total number of pages required to display the data
            // source.
            for (int i = 0; i < ScheduleDiscoveryResultsTableGrid.PageCount; i++)
            {

                // Create a ListItem object to represent a page.
                int pageNumber = i + 1;
                ListItem item = new ListItem(pageNumber.ToString());

                // If the ListItem object matches the currently selected
                // page, flag the ListItem object as being selected. Because
                // the DropDownList control is recreated each time the pager
                // row gets created, this will persist the selected item in
                // the DropDownList control.   
                if (i == ScheduleDiscoveryResultsTableGrid.PageIndex)
                {
                    item.Selected = true;
                    SelectedIndex = i;
                }

                // Add the ListItem object to the Items collection of the 
                // DropDownList.
                pageList.Items.Add(item);

            }

        }

        int upperBoundary;
        if (pageLabel != null)
        {            
            // Calculate the current page number.
            int lowerBoundary = (ScheduleDiscoveryResultsTableGrid.PageIndex) * ScheduleDiscoveryResultsTableGrid.PageSize + 1;
            int temp = (ScheduleDiscoveryResultsTableGrid.PageIndex + 1) * ScheduleDiscoveryResultsTableGrid.PageSize;
            upperBoundary = temp > NrOfRecords ? NrOfRecords : temp;

            DisplayedNodes = upperBoundary - lowerBoundary + 1;

            // Update the Label control with the current page information.
            pageLabel.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_8, lowerBoundary, upperBoundary, NrOfRecords); 

        }

        if (pageCountLabel != null)
        {
            pageCountLabel.Text = String.Format(Resources.CoreWebContent.WEBCODE_VB0_9, ScheduleDiscoveryResultsTableGrid.PageCount);
        }

        if (pageSizeTextBox != null)
        {
            pageSizeTextBox.Text = ScheduleDiscoveryResultsTableGrid.PageSize.ToString();
        }

        if (ScheduleDiscoveryResultsTableGrid.PageIndex == 0)
        {
            ImageButton firstPageImageButton = (ImageButton) pagerRow.Cells[0].FindControl("FirstButton");
            firstPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-first-disabled.gif";

            ImageButton previousPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("PreviousButton");
            previousPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-prev-disabled.gif";
        }

        if (ScheduleDiscoveryResultsTableGrid.PageIndex == (ScheduleDiscoveryResultsTableGrid.PageCount-1))
        {
            ImageButton lastPageImageButton = (ImageButton) pagerRow.Cells[0].FindControl("LastButton");
            lastPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-last-disabled.gif";

            ImageButton nextPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("NextButton");
            nextPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-next-disabled.gif";
        }
    }

    protected string WrapDescription(object item)
    {
        DiscoveryNodeUI discoveredNode = item as DiscoveryNodeUI;
        if (discoveredNode != null)
        {            
            if (discoveredNode.NodeStatus.Contains(DiscoveryNodeStatus.Ignored))
            {                
                return String.Empty;
            }

            if (discoveredNode.NodeStatus.Contains(DiscoveryNodeStatus.Changed))
            {
                //“XX new interfaces, YY removed interfaces, ZZ new volumes, WW removed volumes�?
                StringBuilder sb = new StringBuilder();

                if (isNPMInstalled && discoveredNode.DiscoveryNode.NewInterfacesCount != 0) 
                {
                    sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, discoveredNode.DiscoveryNode.NodeID); 
                    sb.AppendFormat("{0} new Interfaces", discoveredNode.DiscoveryNode.NewInterfacesCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                    sb.Append(@"</a>");

                    if (discoveredNode.DiscoveryNode.NewVolumesCount != 0)
                    {
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat(", {0} Volumes", discoveredNode.DiscoveryNode.NewVolumesCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");                        
                    }
                }
                else
                {
                    if (discoveredNode.DiscoveryNode.NewVolumesCount != 0)
                    {
                        //this.Description += discoveredNode.DiscoveryNode.NewVolumesCount + " new Volumes";
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat("{0} new Volumes", discoveredNode.DiscoveryNode.NewVolumesCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");                        
                    }
                }
                return sb.ToString();
            }

            if (discoveredNode.DiscoveryNode.NodeStatus.Contains(DiscoveryNodeStatus.Imported))
            {
                StringBuilder sb = new StringBuilder();
                if (isNPMInstalled && discoveredNode.InterfaceCount != 0)
                {
                    //this.Description = interfaceCount + " Interfaces";
                    sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                    sb.AppendFormat("{0} Interfaces", discoveredNode.InterfaceCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                    sb.Append(@"</a>");                        

                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += ", " + volumeCount + " Volumes";
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat(", {0} Volumes", discoveredNode.VolumeCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");
                    }
                }
                else
                {
                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += volumeCount + " Volumes";\
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat("{0} Volumes", discoveredNode.VolumeCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");
                    }
                }
                return sb.ToString();;
            }

            if (discoveredNode.DiscoveryNode.NodeStatus.Contains(DiscoveryNodeStatus.NotImported))
            {
                StringBuilder sb = new StringBuilder();

                if (isNPMInstalled && discoveredNode.InterfaceCount != 0)
                {
                    //this.Description = "New node with " + interfaceCount + " Interfaces";
                    sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                    sb.AppendFormat("New node with {0} Interfaces", discoveredNode.InterfaceCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                    sb.Append(@"</a>");

                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += ", " + volumeCount + " Volumes";
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat(", {0} Volumes", discoveredNode.VolumeCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");
                    }

                }
                else
                {
                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += "New node with " + volumeCount + " Volumes";
                        sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, discoveredNode.DiscoveryNode.NodeID);
                        sb.AppendFormat("New node with {0} Volumes", discoveredNode.VolumeCount);// TODO: [localization] {Huge logic for constructiong text is temporary ignored}
                        sb.Append(@"</a>");
                    }
                }
                return sb.ToString();
            }  
        }
        return String.Empty;
    }

    protected string GetDateFound(DateTime dateFound)
    {        
        return dateFound.ToLocalTime().ToString("f");
    }    

    private void StoreSelection(bool allNodesSelected)
    {
        if (allNodesSelected)
        {
            foreach (DiscoveryNodeUI discoveryNodeUI in ScheduleDiscoveryHelper.ScheduleDiscoveryResults)
            {
                discoveryNodeUI.DiscoveryNode.IsSelected = true;
            }
        }
        else
        {
            for (int i = 0; i < this.ScheduleDiscoveryResultsTableGrid.Rows.Count; i++)
            {
                Control item = ScheduleDiscoveryResultsTableGrid.Rows[i];
                CustomCheckBox checkBox = (CustomCheckBox)item.FindControl("CustomSelector");

                Dictionary<string, string> keys = checkBox.CustomValue as Dictionary<string, string>;

                if (ScheduleDiscoveryHelper.ScheduleDiscoveryResults.Count <= i) break;

                DiscoveryNodeUI discoveryNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults
                    .SingleOrDefault(n => (n.DiscoveryNode.NodeID == Convert.ToInt32(keys[NodeID])) && (n.DiscoveryNode.ProfileID == Convert.ToInt32(keys[ProfileID])));

                if (discoveryNodeUI != null)
                {
                    discoveryNodeUI.DiscoveryNode.IsSelected = checkBox.Checked;
                }
            }
        }
    }

    private void DiscardSelection()
    {
        foreach (DiscoveryNodeUI discoveryNodeUI in ScheduleDiscoveryHelper.ScheduleDiscoveryResults)
        {
            discoveryNodeUI.DiscoveryNode.IsSelected = false;
            discoveryNodeUI.DiscoveryNode.Interfaces.ForEach(i => i.IsSelected =false);
            discoveryNodeUI.DiscoveryNode.Volumes.ForEach(v => v.IsSelected = false);
        }                
    }

    protected object GetKey(object o)
    {
        DiscoveryNodeUI discoveryNodeUI = o as DiscoveryNodeUI;
        if (discoveryNodeUI != null)
        {
            return new Dictionary<string, string> { { NodeID, discoveryNodeUI.DiscoveryNode.NodeID.ToString() }, { ProfileID, discoveryNodeUI.DiscoveryNode.ProfileID.ToString() } };
        }
        else
        {
            log.Error("Discovery Node missing");
            throw new NotSupportedException("Discovery Node missing");
        }
    }
    #endregion
   
}
