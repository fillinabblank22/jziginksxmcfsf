<%@ Import Namespace="SolarWinds.Orion.Core.Web.Discovery"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BulkUpload.ascx.cs" Inherits="Orion_Discovery_Wizard_Controls_BulkUpload" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>

<div class="ValidationDiv">
    <orion:TBox ID="ValidationDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_92 %>" MessageVisible="true" />
</div>    

 <script type="text/javascript">
     //<![CDATA[
     var StartClientValidation = function(sender, args) {
         args.IsValid = false;
         StartValidation();
     }

     var StartValidation = function() {
        $('.ValidationDiv .TransparentBoxID').show();
        $(".HostsTextBoxValidator").text("").css({ color: 'red' }); ;
        PageMethods.StoreHostnames($(".HostTextBox").val(), OnStartProcessed, OnError);
     }

     var OnStartProcessed = function(result) {
        StartHostnamesValidation(0);
     }

     var StartHostnamesValidation = function(startLine) {
        PageMethods.ProcessValidation(startLine, OnValResultsProcessed, OnError);
     }

     var OnValResultsProcessed = function(result) {
         $(".HostsTextBoxValidator").append(result.ResultText);
         if (result.RemainingNodes > 0) {
             StartHostnamesValidation(result.StartLine);
         }
         else {
             $('.ValidationDiv .TransparentBoxID').hide();
             if ($.trim($(".HostsTextBoxValidator").text()).length == 0)
                 $(".HostsTextBoxValidator").text('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_4) %>').css({color:'green'});
         }
     }

    var OnError = function(error) {
        alert(error.get_message() + '\n' + error.get_stackTrace());
    }
    //]]>
</script>

<div class="BulkUpload">
    <p class="SectionTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_91) %></p>
    <div id="HostTextBoxDiv">
        <asp:TextBox ID="HostsTextBox" runat="server" TextMode="MultiLine" CssClass="HostTextBox" />
        <br />
        <orion:LocalizableButton id="ValidateButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_139 %>" OnClientClick="StartValidation(); return false;" />
        <div class="HostsTextBoxValidator"></div>
    </div>
    
</div>
    
