﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Models.Discovery;

public partial class Orion_Discovery_Controls_AddSubnetDialog : System.Web.UI.UserControl
{
	public delegate void SubnetAddedEventHandler(Subnet subnet);

	/// <summary> Called when AddSeddRouter dialog is closed and its subnets should be added. </summary>
	public event SubnetAddedEventHandler SubnetAdded;

	/// <summary>
	/// Called from parent control to show the dialog
	/// </summary>
	public void OpenDialog()
	{
		this.AddSubnetDialog.Visible = true;
		this.ModalityDiv.ShowBlock = true;

		this.SubnetAddressText.Text = String.Empty;
		this.SubnetMaskText.Text = String.Empty;
	}

	protected void AddButton_OnClick(object sender, EventArgs e)
	{
		if (!this.Page.IsValid)
		{
			return;
		}

		if (this.SubnetAdded != null)
		{
			this.SubnetAdded(new Subnet(this.SubnetAddressText.Text, this.SubnetMaskText.Text));
		}

		this.CloseDialog();
	}

	protected void CancelAddButton_OnClick(object sender, EventArgs e)
	{
		this.CloseDialog();
	}

	protected void CloseDialog()
	{
		this.AddSubnetDialog.Visible = false;
		this.ModalityDiv.ShowBlock = false;
	}

	protected void CheckSubnet(object sender, ServerValidateEventArgs e)
	{
		e.IsValid = true;
		try
		{
			new Subnet(this.SubnetAddressText.Text, this.SubnetMaskText.Text);
		}
		catch (ArgumentOutOfRangeException)
		{
			e.IsValid = false;
		}
	}
}
