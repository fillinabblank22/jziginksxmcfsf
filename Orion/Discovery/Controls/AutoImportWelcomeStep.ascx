<%@ Control Language="C#" ClassName="AutoImportWelcomeStep" Inherits="SolarWinds.Orion.Web.UI.WizardPanelStepBase" %>

<p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_WelcomeStep_Paragraph1) %></p>
<p><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_NetworkSonar_MonitoringSettings_WelcomeStep_Paragraph2) %></p>
