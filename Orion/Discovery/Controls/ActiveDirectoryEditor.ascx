<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveDirectoryEditor.ascx.cs" Inherits="Orion_Discovery_Controls_ActiveDirectoryEditor" Debug="true"%>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register Src="~/Orion/Discovery/Controls/ActiveDirectoryDomainControllerDetails.ascx" TagPrefix="orion" TagName="DomainControllerDetails" %>
<%@ Register Src="~/Orion/Discovery/Controls/ActiveDirectoryOrganizationalUnits.ascx" TagPrefix="orion" TagName="OrganizationalUnits" %>


<%--<orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />--%>

<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="ActiveDirectoryEditor.css" />

<orion:Include ID="IncludeExtJs1" runat="server" Framework="Ext" FrameworkVersion="3.4" />


<script type="text/javascript" >
    (function () {
        SW.ActiveDirectoryEditorDlg = undefined;

        var webService = "/Orion/Services/DiscoveryWizard.asmx";
        var orgUnits = [];
        var orgUnitsPaths = {};

        var editorControl = function () {
            return {
                reload: function () { grid.getStore().reload(); },
                init: function () {
                    var storeData = new Ext.data.JsonStore(
                    {
                        autoLoad: true,
                        proxy: new Ext.data.HttpProxy({
                            url: '/Orion/Services/DiscoveryWizard.asmx/ADGetEntries',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json; charset=utf-8'
                            }

                        }),
                        root: 'd',
                        fields: [
                            'CredentialID', 
                            'HostName', 
                            'OrganizationalUnits', 
                            'OrganizationalUnitsToDisplayInGrid', 
                            'NumberOfSelectedOUs', 
                            'Name', 
                            'UserName', 
                            'Password', 
                            'Index', 
                            'ProfileIndex', 
                            'CredentialIndex', 
                            'PasswordChanged', 
                            'UserNameChanged', 
                            'NameChanged',
                            'ServersOnly',
                            'DeviceCount']
                    });

                    var listTpl = _.template($("#listTemplate").html());
                    var grid = new Ext.grid.GridPanel({
                        store: storeData,
                        listeners: {
                            'cellclick': function(grid, rowIndex, columnIndex, e) {
                                if (grid && grid.view) {
                                    if (e && e.target && (e.target.className == 'sw-oulist-hostname') || e.target.className == 'sw-oulist-hostname sw-oulist-hostname-collapsed') {
                                        $(grid.view.getRow(rowIndex)).toggleClass("sw-oulist-expanded ");
                                        $(e.target).toggleClass("sw-oulist-hostname-collapsed");

                                        $(grid.view.getRow(rowIndex)).find("#sw-oulist-items").toggleClass('sw-oulist-hidden');
                                        $(grid.view.getRow(rowIndex)).find("#sw-oulist-summary").toggleClass('sw-oulist-hidden');

                                    } else if (e && e.target && (e.target.className == 'sw-oulist-action')) {
                                        $("#sw-AdOus").hide(); 
                                        $("#sw-AdOus-loading").show();
                                        $('#sw-ou-nbrOfSelectedOUs').hide();
                                        $('#countOfDevicesWrappper').hide();

                                        var wizard = window.<%= this.WizardPanel.ClientID %>;
                                        if (wizard && wizard.btns && wizard.btns.finish) { //disable finish button
                                            wizard.btns.finish.addClass("sw-btn-disabled");
                                        }

                                        SW.ActiveDirectoryEditorDlg.show(grid.getStore().getAt(rowIndex).data);
                                    }
                                }
                            },
                            'mouseover': function(e) {
                                if (e && e.target && (e.target.className == 'sw-oulist-info-tooltip-title')) {
                                    $(e.target).closest('span.sw-oulist-info-tooltip').find("span.tooltip").css({left: $(e.target).position().left, top: $(e.target).position().top, position:'absolute'});
                                    $(e.target).closest('span.sw-oulist-info-tooltip').find("span.tooltip").show();
                                }
                            },
                            'mouseout': function(e) {
                                if (e && e.target && (e.target.className == 'sw-oulist-info-tooltip-title')) {
                                    $(e.target).closest('span.sw-oulist-info-tooltip').find("span.tooltip").hide();
                                }
                            }
                        },
                        
                        columns: [
                            {
                                header: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_58) %>", dataIndex: 'HostName', width: 180,
                                renderer: function (value, metaData, record, rowIndex, colIndex) {
                                    var cls = record.data.OrganizationalUnitsToDisplayInGrid.length == 1 ? 'sw-oulist-hostname-without-expander' : 'sw-oulist-hostname';
                                    return '<div class="' + cls + '">' + value + '</div>';
                                }
                            }, //renderer + span + css
                            { header: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_59) %>", dataIndex: 'OrganizationalUnitsToDisplayInGrid', width: 200,
                                renderer: function (value, metaData, record, rowIndex, colIndex) {
                                    return listTpl({ous: value, seeall: record.data.NumberOfSelectedOUs > 5, count: record.data.NumberOfSelectedOUs });
                                }
                            },
                            { header: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceType) %>", dataIndex: 'ServersOnly', width: 140,
                                renderer: function (value, metadata, record, rowIndex, colIndex) {
                                    return SW.Core.String.Format(
                                        '<span class="sw-oulist-info-tooltip"><span id="sw-ou-grid-device-type" class="sw-oulist-info-tooltip-title">{0}</span><span class="tooltip">{0}</span></span>',
                                         value ? "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceType_Servers) %>" : "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceType_ServersAndWorkstations) %>");
                                }
                            },
                            { header: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceCount) %>", dataIndex: 'DeviceCount', width: 100,
                                renderer: function (value, metadata, record, rowIndex, colIndex) {
                                    return SW.Core.String.Format(
                                        '<span id="sw-ou-grid-device-count">{0}</span>', value);
                                }
                            },
                            {
                                header: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_60) %>", dataIndex: 'Name', width: 100,
                                renderer: function(value, metaData, record, rowIndex, colIndex) {
                                    return SW.Core.String.Format(
                                        '<span class="sw-oulist-info-tooltip"><span id="sw-ou-grid-credential-name" class="sw-oulist-info-tooltip-title">{0}</span><span class="tooltip">{0}</span></span>', 
                                        value);
                                }
                            },
                            {
                                xtype: 'actioncolumn',
                                renderer: function (value, metaData, record, rowIndex, colIndex) {
                                    // render cell manually so that we can add title for images
                                    return '<img height="20" alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_90) %>" title="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_90) %>" src="../images/TrashBin.png" class="x-action-col-icon x-action-col-1  " />';
                                },
                                items: [
                                    {
                                        // don't remove this or else delete action will not work
                                    },
                                    {
                                        /* icon: '../images/TrashBin.png',*/
                                        handler: function (grid, rowIndex, colindex) {
                                            var msg = SW.Core.String.Format("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_91) %>", grid.getStore().getAt(rowIndex).data.HostName);
                                            if (confirm(msg)) {
                                                ORION.callWebService(webService, "ADDeleteEntry",
                                                    { entry: grid.getStore().getAt(rowIndex).data },
                                                    function (result) {
                                                        grid.getStore().reload();
                                                    },
                                                    function (error) {
                                                        // TODO: handle error
                                                    });

                                            }
                                        }
                                    }
                                ],
                                width: 80
                            },
                        ],
                        disableSelection: true,
                        autoHeight: true,
                        viewConfig: {
                            autoFill: true
                        }
                    });
                    grid.hide();

                    storeData.on('load', function (records, options) {
                        if (records.totalLength == 0) {

                            grid.hide();
                        } else {

                            grid.show();
                        }
                    });

                    grid.getStore().reload();
                    grid.render('ADEntriesGrid');
                    if (!SW.ActiveDirectoryEditorDlg) {
                        SW.ActiveDirectoryEditorDlg = new ActiveDirectoryDialog(grid);

                        var panel = new Ext.Panel({
                            title: '<span class="sw-ou-treepanel-heading"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_Heading) %></span>',
                            frame: true,
                            border: true,
                            renderTo: "sw-ou-treeWrapper",
                            items:[
                                {
                                    region: 'center',
                                    contentEl: "sw-ou-treePanel"
                                }
                            ]
                        });
                       
                        $('#DirectoryAddItem').on('click', function (evt) {
                            evt.preventDefault();
                            SW.ActiveDirectoryEditorDlg.show();
                        });
                    }
                }
            }
        }();

        function isSelect() {
            return $('#radioCredsSelect').attr('checked');
        }

        function showHidePanels() {
            if (isSelect()) {
                $('#dialogCredsCreate').hide();
                $('#dialogCredsSelect').show();
                $('#btnCredsTest').removeAttr('disabled');

            } else {
                $('#dialogCredsCreate').show();
                $('#dialogCredsSelect').hide();

                var index = $('#CredsSelect').val();
                var creds = credentials[index];

                if ($('#radioCredsEdit').attr('checked')) {

                    if (creds.Index < 0 || creds.ID > 0) {

                        $('#CredsName').attr('disabled', 'disabled');
                        $('#CredsUser').attr('disabled', 'disabled');
                        $('#CredsPwd').attr('disabled', 'disabled');
                    } else {

                        $('#CredsName').removeAttr('disabled');
                        $('#CredsUser').removeAttr('disabled');
                        $('#CredsPwd').removeAttr('disabled');
                        $('#btnCredsTest').removeAttr('disabled');

                    }


                    $('#CredsName').val(creds.Name);
                    $('#CredsUser').val(creds.UserName);
                    $('#CredsPwd').val(creds.Password);


                } else {

                    $('#CredsName').removeAttr('disabled');
                    $('#CredsUser').removeAttr('disabled');
                    $('#CredsPwd').removeAttr('disabled');
                    $('#btnCredsTest').removeAttr('disabled');


                    $('#CredsName').val('');
                    $('#CredsUser').val('');
                    $('#CredsPwd').val('');
                }
            }
        }

        function initializeControls() {
            $('#testSpinner').hide().removeClass('hidden');
            $('#saveSpinner').hide().removeClass('hidden');
            $('#testSuccess').hide().removeClass('hidden');
            $('#testFailed').hide().removeClass('hidden');
            $('#testError').hide().removeClass('hidden');
            $('#saveSuccess').hide().removeClass('hidden');
            $('#saveFailed').hide().removeClass('hidden');

            $('#radioCredsEdit').hide().next().hide();

            $('#radioCredsCreate').attr('checked', undefined);
            $('#radioCredsSelect').attr('checked', 'checked');

            $('#ADHostName').removeClass(VALIDATION_ERROR_CLASS);
            $('#ADHostNameEmpty').hide();

            $('#CredsName').removeClass(VALIDATION_ERROR_CLASS);
            $('#CredsNameEmpty').hide();
            $('#CredsNameExists').hide();

            $('#CredsUser').removeClass(VALIDATION_ERROR_CLASS);
            $('#CredsUserEmpty').hide();

            orgUnitsPaths = [];
        }

        var VALIDATION_ERROR_CLASS = 'sw-validation-error';

        function validateData() {
            var result = true;

            $('#ADHostNameEmpty').hide();
            $('#ADHostName').removeClass(VALIDATION_ERROR_CLASS);
            if (!$('#ADHostName').val()) {
                $('#ADHostNameEmpty').show();
                $('#ADHostName').addClass(VALIDATION_ERROR_CLASS);
                result = false;
            }

            $('#CredsName').removeClass(VALIDATION_ERROR_CLASS);
            $('#CredsNameEmpty').hide();
            $('#CredsNameExists').hide();
            if (!$('#radioCredsSelect').attr('checked') && !$('#CredsName').val()) {
                $('#CredsNameEmpty').show();
                $('#CredsName').addClass(VALIDATION_ERROR_CLASS);
                result = false;
            }

            $('#CredsUser').removeClass(VALIDATION_ERROR_CLASS);
            $('#CredsUserEmpty').hide();
            if (!$('#radioCredsSelect').attr('checked') && !$('#CredsUser').val()) {
                $('#CredsUserEmpty').show();
                $('#CredsUser').addClass(VALIDATION_ERROR_CLASS);
                result = false;
            }
            
            return result;
        }

        function updateValidationErrors(result) {
            $('#CredsNameExists').hide();

            if (result == undefined || !result.ValidationErrors) return;

            for (var i = 0; i < result.ValidationErrors.length; i++) {
                var error = result.ValidationErrors[i];
                switch (error.Key) {
                case 'Name':
                    $('#CredsNameExists')
                        .text(error.Value)
                        .show();
                    break;
                default: break;
                }
            }
        }

        var RESULT_TYPE = {
            Success: 0,
            ValidationError: 1,
            ProcessingError: 2
        }

        function ActiveDirectoryDialog(grid) {
            var self = this;
            this.grid = grid;
            this.dialog = $('#ActiveDirectoryDialogFrame');
            this.finishBtn = undefined;
            this.WarningLimit = 0;
            this.WarningByLicense = false;

            this.activeDirectoryRecord = {
                PasswordChanged: false
            };

            self.countOfComputers = (function() {
                var countOfComputersPollingInterval = undefined;
                var countOfComputersTaskId = undefined;
                var pollingForCountsOfComputers = false;

                function stopPolling() {
                    if (pollingForCountsOfComputers) {
                        
                        //clear polling interval, so the polling function is not called anymore
                        if (countOfComputersPollingInterval) {
                            clearInterval(countOfComputersPollingInterval);
                        }
                        
                        // hide spinner
                        $("#sw-ou-tree-nbr-loading-spinner").hide();

                        pollingForCountsOfComputers = false;
                    }
                }

                function getCountOfStations() {
                    ORION.callWebService(
                        webService,
                        "GetCountOfComputersInAD",
                        { "taskId": countOfComputersTaskId },
                        function(result) {
                            stopPolling();

                            _.each(result, function(item) {
                                var rowContainer = $('.sw-ou-container[data-fqdn="' + item.DistinguishedName + '"] > div.sw-ou-tree-row-container');
                                rowContainer.find(".sw-ou-tree-number-of-computers").first().html(item.CountOfComputers);
                                rowContainer.find(".sw-ou-tree-number-of-servers").first().html(item.CountOfServers);

                                rowContainer.find(".sw-ou-number-of-stations").first().data("nbr-of-computers", item.CountOfComputers);
                                rowContainer.find(".sw-ou-number-of-stations").first().data("nbr-of-servers", item.CountOfServers);
                            });

                            SW.Orion.Core.Discovery.AD.CountOfStationsColumn.fillEmptyRows();
                            SW.Orion.Core.Discovery.AD.CountOfStationsColumn.setColumn();

                            // refresh number of selected computers/servers
                            SW.Orion.Core.Discovery.AD.DeviceCount.setCount();

                            // show number of devices
                            $('#countOfDevicesWrappper').show();
                        },
                        function(error) {
                            stopPolling();
                        });
                }

                function pollForCountOfComputers() {
                    ORION.callWebService(
                        webService,
                        "GetCountOfComputersTaskResult",
                        { "taskId": countOfComputersTaskId },
                        function(result) {
                            //result codes:
                            //0 - still running
                            //1 - successfully completed
                            //2 - failed

                            if (result == 1) { // the task has successfullly completed
                                getCountOfStations();
                            } else if (result == 2) { // failed
                                stopPolling();
                            }
                        },
                        function(error) {
                            stopPolling();
                        });
                }

                function setCountOfStations (entry) {
                    stopPolling(); //if the polling is in progress, we have to cancel it 

                    $("#sw-ou-tree-nbr-loading-spinner").show();

                    ORION.callWebService(
                        webService,
                        "StartCountingComputersInAD",
                        { "entry": entry },
                        function(result) {
                            pollingForCountsOfComputers = true;
                            countOfComputersTaskId = result;
                            countOfComputersPollingInterval = setInterval(pollForCountOfComputers, 1000); // set the polling interval to 1 second
                        },
                        function(error) {
                            stopPolling();
                        });
                }

            $('#ActiveDirectoryDialogFrame').on('dialogclose', function(event) {
                stopPolling();
            });

            return {
                'resetPolling': stopPolling,
                'startPolling': setCountOfStations
            }
            })();



            // called when something is changed
            // if testSucceeded is undefined, test was not run
            self.setTestStatus = function (testSucceeded, clearOUs) {
                orgUnits = []; // clear autocomplete (will be filled after, if test succeeded)
                orgUnitsPaths = {};

                var testSuccess = $('#testSuccess');
                var testFailed = $('#testFailed');
                var testError = $('#testError');

                testSuccess.data('succeeded', (testSucceeded === true));
                
                if (clearOUs && self.activeDirectoryRecord)
                    self.activeDirectoryRecord.OrganizationalUnits = undefined;

                if (testSucceeded === true) {
                    testSuccess.show();
                    testFailed.hide();
                    testError.hide();

                } else if (testSucceeded === false) {
                    testSuccess.hide();
                    testFailed.show();
                    testError.hide();

                } else if (testSucceeded === undefined) {
                    testSuccess.hide();
                    testFailed.hide();
                    testError.hide();
                }
            };

            self.getLastTestStatus = function(){
                return ($('#testSuccess').data('succeeded') === true);
            };

            self.setTestStatus(undefined);

            /* Initial control setup */
            initializeControls();

            var showHidePanelsPlusSetTestStatus = function () {
                self.setTestStatus(undefined);
                showHidePanels();
            };

            // parses list of org units into dictionary [displayname => path]
            var getOrganizationalUnitsPaths = function (units) {
                var rv = {};
                $.each(units, function () {
                    rv[this.FullName] = this.Path;
                    return true;
                });
                return rv;
            }

            $('#radioCredsCreate').on('change', showHidePanelsPlusSetTestStatus);
            $('#radioCredsEdit').on('change', showHidePanelsPlusSetTestStatus);
            $('#radioCredsSelect').on('change', showHidePanelsPlusSetTestStatus);
            showHidePanels();

            $('#ADHostName').on('change', function () {
                self.setTestStatus(undefined, true);
            });

            $('#CredsName').on('change', function () {

                if ($('#radioCredsCreate').attr('checked')) {
                    self.activeDirectoryRecord.NameChanged = true;
                }

                if ($('#radioCredsEdit').attr('checked')) {
                    var index = $('#CredsSelect').val();

                    if (index >= 0 && index < credentials.length) {
                        var creds = credentials[index];
                        creds.NameChanged = true;
                    }
                }
                
                self.setTestStatus(undefined, true);
            });


            $('#CredsUser').on('change', function () {

                if ($('#radioCredsCreate').attr('checked')) {
                    self.activeDirectoryRecord.UserNameChanged = true;
                }


                if ($('#radioCredsEdit').attr('checked')) {

                    var index = $('#CredsSelect').val();

                    if (index >= 0 && index < credentials.length) {
                        var creds = credentials[index];
                        creds.UserNameChanged = true;
                    }
                }

                self.setTestStatus(undefined, true);
            });

            $('#CredsPwd').on('change', function () {

                if ($('#radioCredsCreate').attr('checked')) {
                    self.activeDirectoryRecord.PasswordChanged = true;
                }


                if ($('#radioCredsEdit').attr('checked')) {
                    var index = $('#CredsSelect').val();
                    if (index >= 0 && index < credentials.length) {
                        var creds = credentials[index];
                        creds.PasswordChanged = true;
                    }
                }

                self.setTestStatus(undefined, true);
            });

            $('#CredsSelect').on('change', function () {
                if (this.value >= 0 && this.value < credentials.length) {
                    var creds = credentials[this.value];
                    $('#CredsName').val(creds.Name);
                    $('#CredsUser').val(creds.UserName);
                    $('#CredsPwd').val(creds.Password);
                    self.activeDirectoryRecord.PasswordChanged = creds.PasswordChanged;
                }
                
                self.setTestStatus(undefined, true);
            });
            
            $('#CredsName').on('blur', function () { toModel(self.activeDirectoryRecord, [spec.Name, spec.NameChanged]); });
            $('#CredsUser').on('blur', function () { toModel(self.activeDirectoryRecord, [spec.UserName, spec.UserNameChanged]); });
            $('#CredsPwd').on('blur', function () { toModel(self.activeDirectoryRecord, [spec.Password, spec.PasswordChanged]); });

            function createOuTree(fullName, scope, fqdn) {
                var root = fullName.split(".")[0];

                var r = _.find(scope.childList, function(child) {
                    return child.text == root;
                });

                if (r == undefined) {
                    scope.childList.push({
                        "text": fullName,
                        "childList": [],
                        "fqdn": fqdn
                    });
                    scope.childList.sort(function(a, b) {
                        return (a.text < b.text) ? -1 : (a.text > b.text) ? 1 : 0;
                    });
                } else {
                    var newFullName = fullName.substring(root.length + 1, fullName.length);
                    createOuTree(newFullName, r, fqdn);
                }
            }

            function areSelectedAllCheckboxes() {
                var nbrOfChecked = $("#sw-AdOus").find('.sw-ou-container').find('.sw-ou-checked').length;
                var nbrOfAllCheckboxes = $("#sw-AdOus").find('.sw-ou-container').find('.sw-ou-name').length;

                return (nbrOfChecked == nbrOfAllCheckboxes);
            }
            
            function setSelectedOUsNumber() {
                if (self.finishBtn)
                    $(self.finishBtn).removeClass("sw-btn-disabled");
                var nbrOfChecked = $("#sw-AdOus").find(".sw-ou-checked").length;
                if (areSelectedAllCheckboxes()) {
                    $("#sw-ou-nbrOfSelectedOUs").html(
                        SW.Core.String.Format(
                            "<%= Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_NbrOfSelectedOus_SelectedAll %>", 
                            nbrOfChecked)
                    );
                } else if (nbrOfChecked == 1) {
                    $("#sw-ou-nbrOfSelectedOUs").html(
                        "<%= Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_NbrOfSelectedOus_SelectedOne %>"
                    );
                } else {
                    $("#sw-ou-nbrOfSelectedOUs").html(
                        SW.Core.String.Format(
                            "<%= Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_NbrOfSelectedOus_SelectedCustomNumber %>", 
                            nbrOfChecked)
                    );

                    if (self.finishBtn && nbrOfChecked == 0) {
                        $(self.finishBtn).addClass("sw-btn-disabled");
                    }
                }
            }

            function uncheckParents(ou) {
                var parentOuContainers = $(ou).parents(".sw-ou-container");
                _.each(parentOuContainers, function(parentContainer) {
                    var checkBoxContainer = $(parentContainer).find(".sw-ou-tree-include-future-child-chkbox-container").first();
                    var checkBox = $(parentContainer).find(".sw-ou-tree-include-future-child-chkbx").first();

                    if (checkBox.hasClass("sw-ou-include-future-child-checked")) { //if the "include future ..." checkbox is checked, highlight it and uncheck
                        checkBoxContainer.effect("highlight", { color: "#FFFF00" }, 2000);
                        checkBox.removeClass("sw-ou-include-future-child-checked").addClass("sw-ou-include-future-child-unchecked");
                    }
                });
            }

            function bindCheckboxes() {
                $(".sw-ou-name").unbind('click').bind('click', function (event) {
                    if ($(this).hasClass("sw-ou-checked")) {
                        $(this).closest('.sw-ou-container').find(".sw-ou-name").removeClass("sw-ou-checked"); //uncheck all ous in the subtree (container)

                        $(this).closest('.sw-ou-container'). //hide all "include future .." checkboxes in the container
                            find(".sw-ou-tree-include-future-child-chkbx").
                            removeClass("sw-ou-include-future-child-checked").
                            removeClass("sw-ou-include-future-child-unchecked");

                        uncheckParents(this);
                    } else {
                        var ouContainer = $(this).closest('.sw-ou-container');
                        $(ouContainer).find(".sw-ou-name").addClass("sw-ou-checked"); //check all OUs (including childs)
                        $(ouContainer).find(".sw-ou-tree-include-future-child-chkbx") //check all "include future ..." checkboxes (including childs)
                            .removeClass("sw-ou-include-future-child-unchecked")
                            .addClass("sw-ou-include-future-child-checked");
                    }

                    // refresh the number of selected OUs
                    setSelectedOUsNumber();

                    SW.Orion.Core.Discovery.AD.DeviceCount.setCount();
                    
                    // we want only the first div to be clicked
                    event.stopPropagation();
                });

                $(".sw-ou-tree-include-future-child-chkbx").unbind('click').bind('click', function(event) {
                    if ($(this).hasClass("sw-ou-include-future-child-checked")) {
                        $(this).removeClass("sw-ou-include-future-child-checked").addClass("sw-ou-include-future-child-unchecked"); //uncheck this checkbox

                        uncheckParents(this);
                    } else if ($(this).hasClass("sw-ou-include-future-child-unchecked")){
                        $(this).removeClass("sw-ou-include-future-child-unchecked").addClass("sw-ou-include-future-child-checked"); //check this checkbox

                        var ouContainer = $(this).closest('.sw-ou-container');
                        $(ouContainer).find(".sw-ou-name").addClass("sw-ou-checked"); //check all OUs (including childs)
                        $(ouContainer).find(".sw-ou-tree-include-future-child-chkbx"). //check all "include future ..." checkboxes in the subtree
                            removeClass("sw-ou-include-future-child-unchecked").
                            addClass("sw-ou-include-future-child-checked");
                    }

                    // refresh the number of selected OUs
                    setSelectedOUsNumber();

                    SW.Orion.Core.Discovery.AD.DeviceCount.setCount();

                    event.stopPropagation();
                });
            }

            function bindExpanders() {
                $('.sw-ou-expander').bind("click", function(event) {
                    var isExpanded = $(this).hasClass("sw-ou-expanded");

                    if (isExpanded) {
                        $(this).removeClass("sw-ou-expanded").addClass("sw-ou-collapsed");
                        $($(this).closest('.sw-ou-container').find(".sw-ou-childElements")[0]).slideUp(500);
                    } else {
                        $(this).removeClass("sw-ou-collapsed").addClass("sw-ou-expanded");
                        $($(this).closest('.sw-ou-container').find(".sw-ou-childElements")[0]).slideDown(500);
                    }

                    // we want only the first div to be clicked
                    event.stopPropagation();
                });
            }

            function checkAll(container) {
                $(container).addClass("sw-ou-checked");
            }

            function expandAll(container) {
                $(container).removeClass("sw-ou-collapsed").addClass("sw-ou-expanded");
            }

            function renderOUTree(orgUnits) {
                var units = {
                    "text": "",
                    "childList": [],
                    "fqdn": ""
                };
                _.each(orgUnits, function (unit) {
                    if (units.text == "" && unit.Depth == 1) {
                        var parts = unit.ParentPath.split(',');
                        var dn = "";
                        _.each(parts, function (part) {
                            dn += part.replace("DC=", "") + ".";
                        });
                        units.text = dn.substring(0, dn.length - 1);
                        units.fqdn = unit.ParentPath;
                    }
                    createOuTree(unit.FullName, units, unit.Path);
                });

                var templateFn = _.template($("#sw-ou-treeTemplate").html());

                $("#sw-AdOus").html(templateFn({ units: units, templateFn: templateFn }));

                bindCheckboxes();
                bindExpanders();
                
                var ouTree = $("#sw-AdOus");
                if (self.activeDirectoryRecord.OrganizationalUnits !== undefined)
                    OrganizationalUnits_set(self.activeDirectoryRecord.OrganizationalUnits);
                else {
                    checkAll(ouTree.find('.sw-ou-container').find('.sw-ou-name'));
                    ouTree.find(".sw-ou-tree-include-future-child-chkbx").addClass("sw-ou-include-future-child-checked"); //check all "include future .." checkboxes
                }

                expandAll($("#sw-AdOus").find('.sw-ou-container').find('.sw-ou-expander'));
                setSelectedOUsNumber();
            }

            self.TestCredentials = function (success, err) {
                if (!validateData())
                    return err && err(false, null);

                var temp = shallowCopy(self.activeDirectoryRecord);
                toModel(temp);

                var spinner = $('#testSpinner');
                var testError = $('#testError');

                self.setTestStatus(undefined);

                updateValidationErrors();
                spinner.show();
                SW.Orion.Core.Discovery.AD.CountOfStationsColumn.setColumn();
                $('#countOfDevicesWrappper').hide();

                ORION.callWebService(
                    webService,
                    "ADTestConnection",
                    { "entry": temp },
                    function (result) {
                        spinner.hide();
                        
                        //show panel with OUs and hide panel with loading icon
                        $("#sw-AdOus").show(); 
                        $("#sw-AdOus-loading").hide(); 

                        if (result.Type == RESULT_TYPE.Success) {
                            self.setTestStatus(true);

                            orgUnitsPaths = getOrganizationalUnitsPaths(result.OrganizationalUnits);
                            orgUnits = result.OrganizationalUnits;
                            self.WarningLimit = result.WarningLimit;
                            self.WarningByLicense = result.WarningByLicense;

                            renderOUTree(orgUnits);

                            var wizard = window.<%= this.WizardPanel.ClientID %>;
                            if (wizard && wizard.btns && wizard.btns.finish) { //enable the finish button
                                wizard.btns.finish.removeClass("sw-btn-disabled");
                            }
                            $('#sw-ou-nbrOfSelectedOUs').show();

                            self.countOfComputers.startPolling(temp);

                            return success && success(result);
                        } else if (result.Type == RESULT_TYPE.ValidationError) {
                            self.setTestStatus(false);
                            updateValidationErrors(result);
                        }
                        else if (result.Type == RESULT_TYPE.ProcessingError) {
                            self.setTestStatus(undefined);
                            testError.text(result.ProcessingErrors[0].Value);
                            testError.show();
                        }
                        return err && err(true, result);
                    },
                    function (error) {
                        spinner.hide();
                        if (error) {
                            testError.text(error);
                        } else {
                            testError.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_57) %>");
                        }
                        testError.show();
                        return err && err(false, error);
                    });
            };

            self.StoreModel = function (success, err) {
                var saveError = $('#saveFailed').hide();
                if (!validateData()) 
                    return err && err(false, null);

                var model = self.activeDirectoryRecord;
                if (model.Index !== undefined) model = shallowCopy(model);

                self.countOfComputers.resetPolling();

                toModel(model);
                updateValidationErrors();

                var spinner = $('#saveSpinner').show();

                ORION.callWebService(
                    webService,
                    (model.Index === undefined) ? "ADAddEntry" : "ADSaveEntry",
                    { "entry": model },
                    function (result) {
                        spinner.hide();
                        if (result.Type == RESULT_TYPE.Success) {
                            if (grid) grid.getStore().reload();
                            self.dialog.dialog('close');
                            return success && success(result);
                        } else if (result.Type == RESULT_TYPE.ValidationError) {
                            updateValidationErrors(result);
                        } else if (result.Type == RESULT_TYPE.ProcessingError) {
                            saveError.text(result.ProcessingErrors[0].Value).show();
                        }
                        return err && err(true, result);
                    },
                    function (error) {
                        spinner.hide();
                        saveError.text(error).show();
                        return err && err(false, error);
                    });
            };

            $('#btnCredsTest').on('click', function(){ self.TestCredentials(); });
        }

        var credentials = [];
    
        function checkbox_get() {
            return $(this.id).prop('checked');
        }
    
        function checkbox_set(value) {
            if (value == undefined)
                value = true;

            $(this.id).prop('checked', value);
        }

        function deviceCount_get() {
            if (typeof $(this.id).data('device-count') !== 'undefined') {
                return $(this.id).data("device-count");
            }

            return 0;
        }

        function value_get() {
            var value = $(this.id).val();

            if (typeof (value) == "string") {
                return value.trim();
            }

            return value;
        }

        function value_set(value) {
            $(this.id).val(value);
        }

        function OrganizationalUnitsToDisplay_get() {
            var checked = $("#sw-AdOus").find('.sw-ou-checked').slice(0,5);

            return $.map(checked, function(val) {
                var name = $(val).text();
                var fqdn = $(val).closest(".sw-ou-container").data('fqdn');
                
                return {
                    Name: (name || '').trim(),
                    FQDN: (fqdn || '').trim(),
                };
            });
        }

        function NumberOfSelectedOUs_get() {
            return $("#sw-AdOus").find('.sw-ou-container').find('.sw-ou-checked').length;
        }
        
        function OrganizationalUnits_get() {
            var checked = $("#sw-AdOus").find('.sw-ou-checked');

            return $.map(checked, function(val) {
                var name = $(val).text();
                var fqdn = $(val).closest(".sw-ou-container").data('fqdn');
                var includeFutureChild = $(val).closest(".sw-ou-container > .sw-ou-tree-row-container").find(".sw-ou-tree-include-future-child-chkbx");

                //if the checkbox's parent has checked "include future .." dont add to the collection, because 
                //it is obvious that it belongs there and it will be searched the whole subtree
                var parentContainer = $(val).parents(".sw-ou-container")[1]; //we want to verify the parent of current sw-ou-container

                if (!$(parentContainer) 
                    .find(".sw-ou-tree-include-future-child-chkbx").first()
                    .hasClass("sw-ou-include-future-child-checked")) {
                    return {
                        Name: (name || '').trim(),
                        FQDN: (fqdn || '').trim(),
                        IncludeFutureChild: (includeFutureChild && includeFutureChild.first().hasClass("sw-ou-include-future-child-checked"))
                    };
                }
            });
        }

        function OrganizationalUnits_set(value) {
            if (value == undefined) {
                return;
            }
            var treeView = $("#sw-AdOus");
            treeView.find('.sw-ou-checked').removeClass('sw-ou-checked');
            $.each(value, function(){
                var fqdn = this.FQDN.replace(/\\/g, '\\\\').replace(/\"/g, '\\\"');

                if (this.IncludeFutureChild) {
                    var ous = treeView.find('.sw-ou-container[data-fqdn="' + fqdn +'"]');
                    $(ous).find('div.sw-ou-tree-include-future-child-chkbx').addClass('sw-ou-include-future-child-checked');
                    $(ous).find('span.sw-ou-name').addClass('sw-ou-checked');
                } else {
                    treeView.find('.sw-ou-container[data-fqdn="' + fqdn +'"] > div.sw-ou-tree-row-container > div.sw-ou-nameWrapper > span.sw-ou-name').addClass('sw-ou-checked');
                    treeView.find('.sw-ou-container[data-fqdn="' + fqdn +'"] > ' +
                            'div.sw-ou-tree-row-container > ' +
                            'div.sw-ou-include-future-child-column > ' +
                            'div.sw-ou-tree-include-future-child-chkbox-container > ' +
                            'div.sw-ou-tree-include-future-child-chkbx')
                        .addClass('sw-ou-include-future-child-unchecked');
                }
            });
        }

        function ID_get() {
            if ($('#radioCredsCreate').attr('checked')) {
                return 0;

            } else {
                var index = $(this.id).val();
                return credentials[index].ID;
            }
        }

        function ID_set(value) {
            if (value == undefined || value == 0) {
                return;
            }

            var index = -1;
            for (var i = 0; i < credentials.length; i++) {
                if (credentials[i].ID == value) {
                    index = i;
                    break;
                }
            }

            $(this.id).val(index);
        }

        function CredentialIndex_get() {
            if (isSelect() || $('#radioCredsCreate').attr('checked')) {
                return -1;

            } else {
                var index = $(this.id).val();
                return credentials[index].Index;
            }
        }

        function CredentialIndex_set(value) {
            if (value == undefined || value < 0) {
                return;
            }
            var index = -1;
            for (var i = 0; i < credentials.length; i++) {
                if (credentials[i].Index == value) {
                    index = i;
                    break;
                }
            }

            $(this.id).val(index);
        }

        function ProfileIndex_get() {
            if (isSelect()) {

                var index = $(this.id).val();
                return credentials[index].Index;
            } else {
                return -1;
            }
        }

        function ProfileIndex_set(value) {
            if (value == undefined || value < 0) {
                return;
            }
            var index = -1;
            for (var i = 0; i < credentials.length; i++) {
                if (credentials[i].Index == value) {
                    index = i;
                    break;
                }
            }

            $(this.id).val(index);
        }

        function Password_get() {
            if (isSelect()) {
                var index = $('#CredsSelect').val();

                if (index >= 0 && index < credentials.length) {
                    var selected = credentials[index];
                    return selected.Password;
                }

            }
            var value = $('#CredsPwd').val();

            if (typeof value == "string") {
                return value.trim();
            }

            return value;

        }

        function Password_set(value) {

            var index = $('#CredsSelect').val();
            if (($('#radioCredsEdit').attr('checked') || $('#radioCredsSelect').attr('checked')) && index >= 0 && index < credentials.length) {
                var creds = credentials[index];
                creds.Password = value;
            }

            $('#CredsPwd').val(value);
        }

        function PasswordChanged_get() {

            if ($('#radioCredsEdit').attr('checked')) {
                var index = $('#CredsSelect').val();
                var creds = credentials[index];
                return creds.PasswordChanged;

            } else if ($('#radioCredsSelect').attr('checked')) {
                return false;
            }
            return true;
        }

        function PasswordChanged_set(value) {
            // blank
        }

        function UserNameChanged_get() {
            if ($('#radioCredsEdit').attr('checked')) {
                var index = $('#CredsSelect').val();
                var creds = credentials[index];
                return creds.UserNameChanged;

            } else if ($('#radioCredsSelect').attr('checked')) {
                return false;
            }
            return true;
        }

        function UserNameChanged_set(value) {
            // blank
        }

        function NameChanged_get() {
            if ($('#radioCredsEdit').attr('checked')) {
                var index = $('#CredsSelect').val();
                var creds = credentials[index];
                return creds.NameChanged;

            } else if ($('#radioCredsSelect').attr('checked')) {
                return false;
            }
            return true;
        }

        function NameChanged_set(value) {
            // blank
        }

        function setRadioButtonsOrderAndStatus(spec) {
            var container = $('#radioCreds');
            var elements = [];

            // first pass, detach all radio buttons with respective labels
            // the resulting array will contain everything in desired order
            for (var i = 0; i < spec.length; i++) {
                var item = spec[i];
                var radio = $(item.id);
                var label = radio.next();
                elements.push({ spec: item, radio: radio, label: label });
                radio.detach();
                label.detach();
            }

            // second pass, update DOM
            for (var i = 0; i < elements.length; i++) {
                var item = elements[i];
                container.append(item.radio);
                container.append(item.label);
            }

            // third pass, set visual state
            while (elements.length > 0) {
                var item = elements.shift();

                if (item.spec.checked) {
                    item.radio.attr('checked', 'checked');
                } else {
                    // item.radio.removeAttr('checked');
                }

                if (item.spec.disabled) {
                    item.radio.attr('disabled', 'disabled');
                    item.label.addClass('disabled');
                } else {
                    item.radio.removeAttr('disabled');
                    item.label.removeClass('disabled');
                }

                if (item.spec.hide) {
                    item.radio.hide();
                    item.label.hide();
                } else {
                    item.radio.show();
                    item.label.show();
                }
            }
        }

        var spec = {
            HostName: { id: '#ADHostName', get: value_get, set: value_set },
            Name: { id: '#CredsName', get: value_get, set: value_set },
            UserName: { id: '#CredsUser', get: value_get, set: value_set },
            Password: { id: '#CredsPwd', get: Password_get, set: Password_set },
            OrganizationalUnits: {id: '#sw-AdOus', get: OrganizationalUnits_get, set: OrganizationalUnits_set},
            OrganizationalUnitsToDisplayInGrid: {get: OrganizationalUnitsToDisplay_get, set: function() {} },
            NumberOfSelectedOUs: { get: NumberOfSelectedOUs_get, set: function() {} },
            CredentialID: { id: '#CredsSelect', get: ID_get, set: ID_set },
            CredentialIndex: { id: '#CredsSelect', get: CredentialIndex_get, set: CredentialIndex_set },
            ProfileIndex: { id: '#CredsSelect', get: ProfileIndex_get, set: ProfileIndex_set },
            PasswordChanged: { get: PasswordChanged_get, set: PasswordChanged_set },
            UserNameChanged: { get: UserNameChanged_get, set: UserNameChanged_set },
            NameChanged: { get: NameChanged_get, set: NameChanged_set },
            ServersOnly: { id: '#ServersOnly', get: checkbox_get, set: checkbox_set },
            DeviceCount: { id: '#DeviceCount', get: deviceCount_get, set: function() {} },
        };

        // populate controls with data from model
        function fromModel(model) {
            if (typeof (model) == "undefined") {
                // clear everything
                for (var prop in spec) {
                    var setter = spec[prop];
                    setter.set.apply(setter, [undefined]);
                }
                return;
            }

            for (var prop in spec) {
                var setter = spec[prop];

                if (model.hasOwnProperty(prop)) {
                    var value = model[prop];
                    setter.set.apply(setter, [value]);
                } else {
                    setter.set.apply(setter, [undefined]);
                }
            }

            if (credentials.length == 0) {
                // swap order
                if (model.Index >= 0) {
                    // Edit:checked, Add, Select:disabled
                    setRadioButtonsOrderAndStatus([{ id: '#radioCredsSelected', checked: true }, { id: '#radioCredsCreate' }]);
                } else {
                    // Add:checked, Select:disabled
                    setRadioButtonsOrderAndStatus([{ id: '#radioCredsCreate', checked: true }, { id: '#radioCredsSelect', disabled: true }]);
                }

            } else {
                if (model.Index >= 0) {
                    // Select:checked?, Edit:checked?, Add
                    var selectChecked = model.ID == 0 && model.ProfileIndex < 0;
                    setRadioButtonsOrderAndStatus([{ id: '#radioCredsSelect', checked: !selectChecked }, { id: '#radioCredsCreate' }]);
                } else {
                    // Select:checked, Add
                    setRadioButtonsOrderAndStatus([{ id: '#radioCredsSelect', checked: true }, { id: '#radioCredsCreate' }]);
                }
            }

            showHidePanels();
        }

        // populate transfer data to model from controls 
        function toModel(model, customSpecs) {
            if (typeof (model) == "undefined") {
                return;
            }
            
            if (customSpecs === undefined)
                customSpecs = spec;

            for (var prop in customSpecs) {
                if (customSpecs.hasOwnProperty(prop)) {
                    var getter = customSpecs[prop];
                    model[prop] = getter.get.apply(getter, [model[prop]]);
                }
            }
        }

        function shallowCopy(source) {
            var result = {};
            for (prop in source) {
                result[prop] = source[prop];
            }

            return result;
        }

        ActiveDirectoryDialog.prototype.show = function (record) {
            var self = this;

            var wizard = window.<%= this.WizardPanel.ClientID %>;
            if(wizard) {
                var index = record ? 2 : 1;
                wizard.setStepIndex(index);
                wizard.btns.next.removeClass('sw-btn-disabled');
                self.finishBtn = wizard.btns.finish;
            }

            self.activeDirectoryRecord = record ? shallowCopy(record) : {
                CredentialIndex: -1
            };
            self.activeDirectoryRecord.Password = "         ";
            self.setTestStatus(undefined);
            initializeControls();

            //data binding

            //get credential list
            ORION.callWebService(webService, "ADGetCredentials", { self: self.activeDirectoryRecord.CredentialIndex },
                function (result) {
                    credentials = result;

                    var options = $('#CredsSelect');
                    options.empty();

                    for (var index = 0; index < credentials.length; index++) {
                        var cred = credentials[index];
                        cred.Password = "         ";
                        var option = $("<option></option>")
                            .attr("value", index)
                            .text(cred.Name);

                        options.append(option);
                    }

                    fromModel(self.activeDirectoryRecord);

                    // test credentials in advance to populate OU tree
                    if(record && !self.getLastTestStatus())
                        self.TestCredentials();
                });

            //show dialog
            this.dialog.dialog({
                width: '700',
                modal: true,
                draggable: true,
                resizable: true,
                title: "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_67) %>",
                show: { duration: 0 },
                dialogClass: 'ui-overflow-visible ad-dialog',
                open: function () {
                    var t = $(this).parent(), w = $(window);
                    t.css('position', 'absolute');
                    t.offset({
                        top: w.scrollTop() + 20,
                        left: (w.width() / 2) - (t.width() / 2)
                    });
                }
            });

            $(self.dialog).bind( "dialogresize", function(event, ui) {
                $(this).height('auto');
                $(this).parents('.ui-dialog').first().height('auto');
            });
        }

        Ext.onReady(editorControl.init, editorControl);
    })();
</script>

<style>
    div#dialogCredsSelect { margin-bottom: 52px; }

    #ActiveDirectoryDialogFrame .sw-wizard-wrapper {  	width: 100%; }
    #ActiveDirectoryDialogFrame .sw-wizard-wrapper li {	width: 49%; }
    #ActiveDirectoryDialogFrame .sw-wizard-wrapper .sw-wizard-steps div.sw-wizard-steps {
        min-height: 500px;
    }

    #ActiveDirectoryDialogFrame { height: auto !important;}
</style>

<input style="display: none">
<!-- this will perevent Chrome from randomly auto filling controls -->
<input type="password" style="display: none">
<!-- this will perevent Chrome from randomly auto filling controls -->

<div class="editor" id="ActiveDirectoryEditor">
    <h1 class="editor-label">
        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_101 %>" />
        <a href="#" class="info-tooltip">
            <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
            <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_63) %></span>
        </a>
    </h1>

    <div class="editor-content">
        <div id="ADEntriesGrid"></div>
        <div class="add-button">
            <a id="DirectoryAddItem" href="#" class="link-button editor-content"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_62) %></a>
        </div>
    </div>

    <div id="ActiveDirectoryDialogFrame" style="display: none;" class="editor-dialog ui-overflow-visible">
        <orion:WizardPanelControl ID="WizardPanel" ActiveStepIndex="1" BackwardNavigationEnabled="true" runat="server"
            OnCancelClientClick="$('#ActiveDirectoryDialogFrame').dialog('close');"
            OnFinishClientClick="SW.ActiveDirectoryEditorDlg.StoreModel();"
            OnMoveNext="
function(e, scope, next){
    if(SW.ActiveDirectoryEditorDlg.getLastTestStatus())
        return next.call(scope, e);

    scope.btns.next.addClass('sw-btn-disabled');
    SW.ActiveDirectoryEditorDlg.TestCredentials(function(){ 
        scope.btns.next.removeClass('sw-btn-disabled');
        next.call(scope, e);
    }, function(){ 
        scope.btns.next.removeClass('sw-btn-disabled'); 
    });
}">
            <Steps>
                <orion:DomainControllerDetails runat="server" ID="DomainControllerDetails" Name="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_ActiveDirectoryDialogFrame_DomainControllerDetails %>" Index="1" />
                <orion:OrganizationalUnits runat="server" ID="OrganizationalUnits" Name="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnits %>" Index="2" />
            </Steps>
        </orion:WizardPanelControl>
    </div>
</div>

<script id="sw-ou-treeTemplate" type="text/template">
    <ul>
        <li class="sw-ou-organizationalUnit">            
            <div data-fqdn="{{{ units.fqdn }}}" class="sw-ou-container">
                <div class="sw-ou-tree-row-container">
                    
                    <div class="sw-ou-number-of-stations" data-nbr-of-computers="-1" data-nbr-of-servers="-1">
                        <span class="sw-ou-tree-number-of-computers">&nbsp;</span>
                        <span class="sw-ou-tree-number-of-servers">&nbsp;</span>
                    </div>

                    <div class="sw-ou-include-future-child-column">
                        <div class="sw-ou-tree-include-future-child-chkbox-container">
                            <div class="sw-ou-tree-include-future-child-chkbx"></div>
                        </div>
                    </div>

                    <div class="sw-ou-nameWrapper">
                        {# if (units.childList.length > 0) { #}
                            <div class="sw-ou-expander"></div>
                        {# } #}
                        <span class="sw-ou-name">{{{ units.text }}}</span>
                    </div>
                   
                    <div style="clear: both"></div>
                </div>
                
                <div class="sw-ou-childElements">
                    {# _.each(units.childList, function(c) { #}                    
                        {{ templateFn({units: c, templateFn: templateFn}) }}                    
                    {# }); #}
                </div>
            </div>
        </li>
    </ul>
</script>

<script id="listTemplate" type="text/template">
    
    {# if (count > 1) { #} 
    <ul id="sw-oulist-summary" class="sw-oulist-leftContent">          
        <li><span id="sw-ou-grid-number-of-ous">{{ count }}</span> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_OuList_NbrOfSelectedOus) %></li>        
    </ul>
    {# } #}

    <ul id="sw-oulist-items" class="sw-oulist-leftContent {# if (count > 1) { #} sw-oulist-hidden {# } #}">
        {# _.each(ous, function(ou) { #}  
            <li class="sw-oulist-item">
                <span class="sw-oulist-info-tooltip">
                    <span class="sw-oulist-info-tooltip-title">{{{ ou.Name }}}</span>
                    <span class="tooltip">{{{ ou.FQDN.replace(/,/g , ", ") }}}</span>
                </span>
            </li>
        {# }); 
        if (seeall) { #}
                <li class="sw-oulist-action"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_OuList_SeeAll) %></li>
        {# } #}
               
    </ul>
    
    <ul class="sw-oulist-rightContent">
         <li class="sw-oulist-action"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_OuList_Edit) %></li>
    </ul> 

</script>
