using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.Common.Snmp;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.NPM.Common.Models;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Models.DiscoveredObjects;

public partial class Orion_Discovery_Wizard_Controls_VolumeList : UserControl
{
    private static readonly Log log = new Log();

    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        ResultsWorkflowHelper.CheckAllVolumeIsSelected =
            ((CheckBox)this.VolumeTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox)this.VolumeTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked =
            ResultsWorkflowHelper.CheckAllVolumeIsSelected;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.VolumeTableGrid.OnGetDataSource += VolumeTableGrid_GetDataSource;
    }

    List<DiscoveryVolumeTypeGroup> VolumeTableGrid_GetDataSource()
    {
        return ResultsWorkflowHelper.DiscoveryVolumeTypeList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // clear list of VolumeTypeList in Session, because I can enter this page after action, which changed DiscoveredNodes collection            
            if (ResultsWorkflowHelper.DiscoveryOnlyNotImportedResults)
            {
                ResultsWorkflowHelper.DiscoveryVolumeTypeList = null;
            }

            CreateVolumeTypeList();
            this.VolumeTableGrid.DataSource = ResultsWorkflowHelper.DiscoveryVolumeTypeList;
            this.VolumeTableGrid.DataBind();
        }

        StoreSelectedVolumeTypes();
    }

    /// <summary> Creates data object with volume types used for displaying table </summary>
    private void CreateVolumeTypeList()
    {        
        // Return if there is no result at all
        if (ResultsWorkflowHelper.DiscoveryResults == null || ResultsWorkflowHelper.DiscoveryResults.Count == 0)
        {
            log.Error("DiscoveryResult is null");
            return;
        }

        if (ResultsWorkflowHelper.DiscoveryVolumeTypeList != null)
        {
            // we have some previous result - we must check it is actual
            if (ResultsWorkflowHelper.DiscoveryVolumeTypeListIgnoreListFlag == ResultsWorkflowHelper.IgnoreListChanges.ModifyFlag)
            {
                // still the same flag - no change required
                return;
            }
            else
            {
                // flag was changed - no new filtering required, we only need create new list               
            }
        }
        else
        {
            // no result yet - we must create it            
        }

        // let's do grouping independently for each result        
        // then we group them together
        var fromAllResults = new List<DiscoveryVolumeTypeGroup>();

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

            if (coreResult == null)
            {
                log.Error("Unable to get Core discovery plugin result for profile");
                return;
            }

            var selectedNodeIDs = new List<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));

            // get volumes groups            
            var actualResult = new List<DiscoveryVolumeTypeGroup>(
                coreResult.DiscoveredVolumes
                // all volumes refering to selected nodes
                    .Where(v => selectedNodeIDs.Contains(v.DiscoveredNodeID))
                // group by volume type
                    .GroupBy(v => v.VolumeType).Select(g => new DiscoveryVolumeTypeGroup()
                    {
                        Type = g.Key,
                        Icon = "/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Volumes/" + g.First().VolumeType + ".gif",
                        Count = g.Count(),
                        Description = VolumeTypeHelper.GetLocalizedVolumeType(g.Key),
                        IsSelected = !VolumeTypeHelper.IsItRemovableType(g.Key)
                    })
            );            

            fromAllResults.AddRange(actualResult);
        }

        // let's join them together
        var final = fromAllResults
            .GroupBy(item => item.Type)
            .Select(item => new DiscoveryVolumeTypeGroup()
                                {
                                    Type = item.Key,
                                    Icon = item.First().Icon,
                                    Count = item.Sum(inner => inner.Count),
                                    Description = item.First().Description,
                                    IsSelected = item.First().IsSelected
                                })
            .ToList();

        ResultsWorkflowHelper.DiscoveryVolumeTypeListIgnoreListFlag = ResultsWorkflowHelper.IgnoreListChanges.ModifyFlag;
        ResultsWorkflowHelper.DiscoveryVolumeTypeList = final;
    }

    /// <summary> Stores selected volume types to Session property </summary>
    private void StoreSelectedVolumeTypes()
    {
        for (int i = 0; i < VolumeTableGrid.Rows.Count; i++)
        {
            Control item = VolumeTableGrid.Rows[i];
            CheckBox checkBox = (CheckBox)item.FindControl("Selector");
            ResultsWorkflowHelper.DiscoveryVolumeTypeList[i].IsSelected = checkBox.Checked;
        }

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            CoreDiscoveryPluginResult coreResult = discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

            List<int> selectedNodeIDs =
                new List<int>(coreResult.DiscoveredNodes.Where(n => n.IsSelected).Select(n => n.NodeID));

            // filter by nodes
            foreach (DiscoveredVolume volume in coreResult.DiscoveredVolumes)
            {
                if (selectedNodeIDs.Contains(volume.DiscoveredNodeID))
                    volume.IsSelected = true;
                else
                    volume.IsSelected = false;
            }

            // then remove volumes of not selected types
            foreach (DiscoveryVolumeTypeGroup group in ResultsWorkflowHelper.DiscoveryVolumeTypeList)
            {
                foreach (DiscoveredVolume volume in coreResult.DiscoveredVolumes.Where(v => v.VolumeType == group.Type))
                {
                    if (!group.IsSelected)
                        volume.IsSelected = false;
                }
            }
        }
    }
}
