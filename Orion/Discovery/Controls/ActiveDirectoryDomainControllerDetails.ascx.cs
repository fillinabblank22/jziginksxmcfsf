﻿using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Discovery_Controls_ActiveDirectoryDomainControllerDetails : WizardPanelStepBase
{

    public string HelpLink
    {
        get { return HelpHelper.GetHelpUrl("OrionCore_Active_Directory_Credentials");  }
    }
}