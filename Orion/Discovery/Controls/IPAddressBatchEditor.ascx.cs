﻿using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Discovery_Controls_IPAddressBatchEditor : System.Web.UI.UserControl
{

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public const int BULK_VALIDATION_BATCH = 5;


    private IList<string> IPAddressList
    {
        get
        {
            return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration
            <SolarWinds.Orion.Core.Models.Discovery.CoreDiscoveryPluginConfiguration>().BulkList;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var items = IPAddressList;
            IPAddressListTextBox.Text = WebSecurityHelper.SanitizeHtmlV2(string.Join("\n", items.ToArray()));
            InputPanel.Visible = !string.IsNullOrWhiteSpace(IPAddressListTextBox.Text);
            AddItemButton.Visible = !InputPanel.Visible;
        }
    }
    protected void AddItemButton_Click(object sender, EventArgs e)
    {
        InputPanel.Visible = true;
        AddItemButton.Visible = false;
    }
    protected void ClearButton_Click(object sender, ImageClickEventArgs e)
    {
        IPAddressListTextBox.Text = string.Empty;
        IPAddressList.Clear();
        InputPanel.Visible = false;
        AddItemButton.Visible = true;
    }

    protected void ValidateButton_Click(object sender, EventArgs e)
    {

    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
    public static string ValidateUserInput(int startLine)
    {
        string message;
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

        if ((ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList.Count == 0))
        {
            return string.Empty;
        }
        else
        {
            try
            {
                using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineID))
                {
                    var subBulkList = new List<string>(ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList);
                    if (startLine > subBulkList.Count)
                    {
                        return null;
                    }

                    subBulkList.RemoveRange(0, startLine);
                    if (subBulkList.Count > BULK_VALIDATION_BATCH)
                    {
                        subBulkList.RemoveRange(BULK_VALIDATION_BATCH, subBulkList.Count - BULK_VALIDATION_BATCH);
                    }

                    message =
                        businessProxy.ValidateBulkList(subBulkList);
                }
            }
            catch (Exception ex)
            {
                message = Resources.CoreWebContent.WEBCODE_AK0_28;
                log.Error("Cannot validate bulk list", ex);
            }
        }

        return message;
    }

    public void UpdateBulkList()
    {

        try
        {
            var items = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration
            <SolarWinds.Orion.Core.Models.Discovery.CoreDiscoveryPluginConfiguration>().BulkList;
            items.Clear();

            if (string.IsNullOrWhiteSpace(IPAddressListTextBox.Text))
            {
                return;
            }

            var lines = new List<string>();
            var reader = new StringReader(IPAddressListTextBox.Text);
            for (; ; )
            {
                var line = reader.ReadLine();

                if (line == null)
                {
                    break;
                }
                if (!string.IsNullOrWhiteSpace(line))
                {
                    lines.Add(line);
                }
            }

            items.AddRange(lines.Distinct(new EqualityComparer()));

        }
        catch (Exception ex)
        {
            log.Error("Error storing IP addresses and hostnames in discovery configuration", ex);
        }
    }

    protected void IPAddressListTextBox_TextChanged(object sender, EventArgs e)
    {
        UpdateBulkList();
    }

    class EqualityComparer : IEqualityComparer<string>
    {

        public bool Equals(string x, string y)
        {
            return string.Equals(x, y, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(string obj)
        {
            return obj.ToLowerInvariant().GetHashCode();
        }
    }

    protected void IPAddressListTextBoxValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var validator = ControlHelper.FindControlRecursive(Page.Master, "HostsTextBoxValidator") as HtmlGenericControl;
        if (validator != null)
        {
            if (IPAddressListTextBox.Visible && string.IsNullOrWhiteSpace(args.Value))
            {
                args.IsValid = false;
                validator.InnerText = Resources.CoreWebContent.WEBCODE_AK0_27;
            }
            else
            {
                args.IsValid = true;
                validator.InnerText = string.Empty;
            }
        }
    }

    public void AddUpdateTrigger(WebControl triggeringControl)
    {
        var trigger = new AsyncPostBackTrigger { ControlID = triggeringControl.UniqueID, EventName = "Click" };
        UpdatePanel.Triggers.Add(trigger);
    }
}