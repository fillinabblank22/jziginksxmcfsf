using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Models.DiscoveredObjects;

public partial class Orion_Discovery_Wizard_Controls_DeviceList : UserControl
{
    /// <summary> Empty result list flag </summary>
    [PersistenceMode(PersistenceMode.Attribute)]
    public bool IsEmpty 
    { 
        get
        {
            return (this.DeviceTableGrid.Rows.Count == 0);
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.DeviceTableGrid.OnGetDataSource += DeviceTableGrid_GetDataSource;
    }

    List<DiscoveryDeviceTypeGroup> DeviceTableGrid_GetDataSource()
    {
        return ResultsWorkflowHelper.DiscoveryDeviceTypeList;
    }

    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        ResultsWorkflowHelper.CheckAllDeviceIsSelected = 
            ((CheckBox) this.DeviceTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox) this.DeviceTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked =
            ResultsWorkflowHelper.CheckAllDeviceIsSelected;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        CreateDeviceTypeList();
        if (!IsPostBack)
        {
            // clear list of DeviceTypeList in Session, because I can enter this page after action, which changed DiscoveredNodes
            if (ResultsWorkflowHelper.DiscoveryOnlyNotImportedResults)
            {
                ResultsWorkflowHelper.DiscoveryDeviceTypeList = null;
            }

            CreateDeviceTypeList();
            this.DeviceTableGrid.DataSource = ResultsWorkflowHelper.DiscoveryDeviceTypeList;
            this.DeviceTableGrid.DataBind();
        }

        StoreSelectedDeviceTypes();
    }

    /// <summary> Creates data object with device types used for displaying table </summary>
    private void CreateDeviceTypeList()
    {
        if (ResultsWorkflowHelper.DiscoveryResults == null || ResultsWorkflowHelper.DiscoveryResults.Count == 0) return;                

        if(ResultsWorkflowHelper.DiscoveryDeviceTypeList != null)
        {
            // we have some previous result - we must check it is actual
            if(ResultsWorkflowHelper.DiscoveryDeviceTypeListIgnoreListFlag == ResultsWorkflowHelper.IgnoreListChanges.ModifyFlag)
            {
                // still the same flag - no change required
                return;                
            }
            else
            {
                // flag was changed - no new filtering required, we only need create new list               
            }            
        }
        else
        {
            // no result yet - we must perform filtering at first
            ResultsWorkflowHelper.FilterResultByIgnoreList();                            
        }

        var fromAllResults = new List<DiscoveryDeviceTypeGroup>();

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            var actualResult = new List<DiscoveryDeviceTypeGroup>(
                discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>().DiscoveredNodes
                    .GroupBy(node => node.MachineType).
                    Select(g => new DiscoveryDeviceTypeGroup
                                    {
                                        VendorType = g.Key,
                                        VendorIcon =
                                            "/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Vendors/" +
                                            g.First().VendorIcon,
                                        DeviceType = g.First().MachineType,
                                        Count = g.Count(),
                                        IsSelected = true
                                    }
                    ));

            fromAllResults.AddRange(actualResult);
        }

        // let's put them together
        var final = fromAllResults
            .GroupBy(item => item.VendorType)
            .Select(item => new DiscoveryDeviceTypeGroup()
            {
                VendorType = item.Key,
                VendorIcon = item.First().VendorIcon,
                Count = item.Sum(inner => inner.Count),
                DeviceType = item.First().DeviceType,
                IsSelected = item.First().IsSelected
            })
            .ToList();

        ResultsWorkflowHelper.DiscoveryDeviceTypeListIgnoreListFlag = ResultsWorkflowHelper.IgnoreListChanges.ModifyFlag;
        ResultsWorkflowHelper.DiscoveryDeviceTypeList = final;
    }

    /// <summary> Stores selected deviceTypes to Session property </summary>
    private void StoreSelectedDeviceTypes()
    {
        bool wasSelectionChanged = false;
        for (int i = 0; i < DeviceTableGrid.Rows.Count; i++)
        {
            Control item = DeviceTableGrid.Rows[i];
            CheckBox checkBox = (CheckBox)item.FindControl("Selector");
            if (i >= ResultsWorkflowHelper.DiscoveryDeviceTypeList.Count) break;
            if (ResultsWorkflowHelper.DiscoveryDeviceTypeList[i].IsSelected != checkBox.Checked)
            {
                wasSelectionChanged = true;
            }
            ResultsWorkflowHelper.DiscoveryDeviceTypeList[i].IsSelected = checkBox.Checked;
        }

        foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
        {
            foreach (DiscoveredNode node in discoveryResult.GetPluginResultOfType<CoreDiscoveryPluginResult>().DiscoveredNodes)
            {
                if (ResultsWorkflowHelper.IsDeviceTypeSelected(node.MachineType))
                    node.IsSelected = true;
                else
                    node.IsSelected = false;
            }            
        }
        
        if (wasSelectionChanged)
        {
            ResultsWorkflowHelper.DiscoveryVolumeTypeList = null;
        }
    }

    
}
