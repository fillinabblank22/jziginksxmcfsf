﻿using SolarWinds.Common.Net;
using SolarWinds.Orion.Core.Web.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Extensions;
using System.Text;
using SolarWinds.Orion.Core.Common;

public partial class Orion_Discovery_Controls_IPRangesEditor : System.Web.UI.UserControl
{
    const string VALIDATION_ERROR_CSS_CLASS = "sw-validation-error";
    int skipValidateIndex = -1;
    enum ErrorCode { 
        None = 0,
        Format = 1,
        InvalidRange = 2,
        OverlappingRange = 3
    }

    private IList<SolarWinds.Orion.Core.Models.Discovery.IPAddressRange> AddressRangeList {
        get {
            return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration
            <SolarWinds.Orion.Core.Models.Discovery.CoreDiscoveryPluginConfiguration>().AddressRange;
        }
    }


    public string RenderIPRangesItems() {
        StringBuilder js = new StringBuilder();
        var items = AddressRangeList;
        foreach (var item in items) {
            if (js.Length > 0) {
                js.Append(',');
            }
            js.Append(item.ToJSONString());
        }

        js.Insert(0, '[').Append(']').Append(';');
        return js.ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        GridView.AutoGenerateColumns = false;
        GridView.RowCommand += GridView_RowCommand;
        GridView.DataBound += GridView_DataBound;
        GridView.PreRender += GridView_PreRender;
       

    }

    void GridView_PreRender(object sender, EventArgs e)
    {

        var items = AddressRangeList;
        StringBuilder script = new StringBuilder("ipRangesEditor_ip_ranges = [");


        foreach (GridViewRow row in GridView.Rows)
        {
            row.Attributes["rowid"] = Convert.ToString(row.DataItemIndex);
            row.Attributes["data-attrs"] = GetAllAttributes(row);

            if (row.DataItemIndex >= 0 && row.DataItemIndex < items.Count)
            {
                var item = items[row.DataItemIndex];

                if (row.DataItemIndex > 0)
                {
                    script.Append(',');
                }

                script.Append(item.ToJSONString());
            }
        }
        script.Append("];");

        ScriptManager.RegisterStartupScript(this, this.GetType(), this.ID, script.ToString(), true);
    }

    private string GetAllAttributes(GridViewRow row)
    {
        StringBuilder sb = new StringBuilder();

        foreach (string key in row.Attributes.Keys) {

            sb.Append(key).Append("=").Append(row.Attributes[key]).Append("&");
        }

        return sb.ToString();
    }



    void GridView_DataBound(object sender, EventArgs e)
    {
        ValidateGrid();
    }

    void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow") {
            DeleteItem(Convert.ToInt32(e.CommandArgument));
        }
    }

    private void DeleteItem(int index)
    {
        var items = AddressRangeList;
        if (index >= 0 && index < items.Count) {
            items.RemoveAt(index);
            GridView.DataSource = items;
            GridView.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {


            GridView.DataSource = AddressRangeList;
            GridView.DataBind();



        }
        else {

            UpdateData(); // Transfer data from controls no matter what
        }

    }

    private void UpdateData()
    {
        var items = AddressRangeList;

        foreach (GridViewRow row in GridView.Rows)
        {
            if (row.DataItemIndex >= 0 && row.DataItemIndex < items.Count) {

                var item = items[row.DataItemIndex];
                var beginTextBox = (TextBox)row.FindControl("BeginIPTextBox");
                var endTextBox = (TextBox)row.FindControl("EndIPTextBox");
                item.RangeBegin = beginTextBox.Text;
                item.RangeEnd = endTextBox.Text;
            }

        }

        GridView.DataSource = items;
        GridView.DataBind();

    }

    

    protected void AddButton_Click(object sender, EventArgs e)
    {
        var items = AddressRangeList;
        
        items.Add(new SolarWinds.Orion.Core.Models.Discovery.IPAddressRange());
        skipValidateIndex = items.Count - 1;
        GridView.DataSource = items;
        GridView.DataBind();
    }

    private void ValidateGrid() {

        foreach (GridViewRow row in GridView.Rows)
        {
            // Clear any errors
            row.Cells[2].Attributes["title"] = Resources.CoreWebContent.WEBDATA_AF0_90;

            row.CssClass = row.CssClass.Replace(VALIDATION_ERROR_CSS_CLASS, "").Trim();
            var beginTextBox = (TextBox)row.FindControl("BeginIPTextBox");
            beginTextBox.CssClass = beginTextBox.CssClass.Replace(VALIDATION_ERROR_CSS_CLASS, "").Trim();

            var endTextBox = (TextBox)row.FindControl("EndIPTextBox");
            endTextBox.CssClass = endTextBox.CssClass.Replace(VALIDATION_ERROR_CSS_CLASS, "").Trim();

            var beginIPValidator = (CustomValidator)row.FindControl("BeginIPTextBoxValidator");
            beginIPValidator.IsValid = true;
            beginIPValidator.ErrorMessage = string.Empty;
            beginIPValidator.Validate();

            var endIPValidator = (CustomValidator)row.FindControl("EndIPTextBoxValidator");
            endIPValidator.IsValid = true;
            endIPValidator.ErrorMessage = string.Empty;
            endIPValidator.Validate();
        }
    }



    protected void IPTextBox_TextChanged(object sender, EventArgs e)
    { 
        
        var textBox = (TextBox)sender;
        var gridViewRow = (GridViewRow)textBox.Parent.Parent;
        var items = AddressRangeList;

        if (gridViewRow.RowIndex >= 0 && gridViewRow.RowIndex < items.Count) {

            var item = items[gridViewRow.RowIndex];
            if (textBox.ID == "BeginIPTextBox") {
                item.RangeBegin = HostHelper.NormalizeIPAddress(textBox.Text);
            }
            else if (textBox.ID == "EndIPTextBox") {
                item.RangeEnd = HostHelper.NormalizeIPAddress(textBox.Text);
            }
        }

    }

    protected void IPTextBoxValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var validator = (CustomValidator)source;
        var items = AddressRangeList;
        var gridViewRow = (GridViewRow)validator.FindControl(validator.ControlToValidate).Parent.Parent;
        if (skipValidateIndex == gridViewRow.DataItemIndex)
        {
            var addressRange = items[gridViewRow.DataItemIndex];
            if ((string.IsNullOrWhiteSpace(addressRange.RangeBegin) && validator.ID == "EndIPTextBoxValidator" && string.IsNullOrWhiteSpace(args.Value))
                || (string.IsNullOrWhiteSpace(addressRange.RangeEnd) && validator.ID == "BeginIPTextBoxValidator" && string.IsNullOrWhiteSpace(args.Value)))
            {
                // skip validating recently added empty item
                return;
            }
        }


        var control = (TextBox)validator.FindControl(validator.ControlToValidate);
        // Check value is an IP address 
        if (args.Value == null || !args.Value.IsValidIPv4Format()) {
            validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_1;
            validator.IsValid = false;
            args.IsValid = false;
            gridViewRow.Attributes["errorcode"] = ErrorCode.Format.ToString();

            if (!(!string.IsNullOrEmpty(control.CssClass) && control.CssClass.Contains(VALIDATION_ERROR_CSS_CLASS)))
            {
                control.CssClass = VALIDATION_ERROR_CSS_CLASS + (string.IsNullOrEmpty(control.CssClass) ? String.Empty : (" " + control.CssClass));
            }
            control.Attributes["hasformaterror"] = true.ToString();
            return;
        }

        control.Attributes["hasformaterror"] = string.Empty;
        // Make sure range is correct, BeginIp <= EndIp

        ;


        if (gridViewRow.RowIndex >= 0 && gridViewRow.RowIndex < items.Count && validator.ID == "BeginIPTextBoxValidator" ) { 
            // Do it once for BeginIpTextBox validator

            var addressRange = items[gridViewRow.RowIndex];

            // Do not check validity if range is not properly filled

            if (!addressRange.IsFilledRange()) {
                return;
            }

            if (!addressRange.IsValidRange()) {
                args.IsValid = false;
                validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AK0_97;
                validator.IsValid = false;
                gridViewRow.Attributes["errorcode"] = ErrorCode.InvalidRange.ToString();


                if (!(!string.IsNullOrEmpty(gridViewRow.CssClass) && gridViewRow.CssClass.Contains(VALIDATION_ERROR_CSS_CLASS)))
                {
                    gridViewRow.CssClass = VALIDATION_ERROR_CSS_CLASS + (string.IsNullOrEmpty(gridViewRow.CssClass) ? String.Empty : (" " + gridViewRow.CssClass));

                }


                return;
            }

            // Make sure there are no duplicates and overlaps in all ranges
            

            foreach (var item in items)
            {
                if (addressRange.Equals(item) || !item.IsValidRange())
                {
                    continue;
                }

                if (addressRange.IsOverlapping(item))
                {
                    args.IsValid = false;
                    validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_2;
                    validator.IsValid = false;
                    if (!(!string.IsNullOrEmpty(gridViewRow.CssClass) && gridViewRow.CssClass.Contains(VALIDATION_ERROR_CSS_CLASS)))
                    {
                        gridViewRow.CssClass = VALIDATION_ERROR_CSS_CLASS + (string.IsNullOrEmpty(gridViewRow.CssClass) ? String.Empty : (" " + gridViewRow.CssClass));
                    }
                    gridViewRow.Attributes["errorcode"] = ErrorCode.OverlappingRange.ToString();
                    return;
                }
            }

            gridViewRow.Attributes["errorcode"] = ErrorCode.None.ToString();
        }
    }


    public void AddUpdateTrigger(WebControl triggeringControl) { 
    
        var trigger = new AsyncPostBackTrigger();
        trigger.ControlID = triggeringControl.ID;

        IPRangesUpdatePanel.Triggers.Add(trigger);
    }

    
}