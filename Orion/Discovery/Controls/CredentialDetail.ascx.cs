﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_Discovery_Controls_CredentialDetail : System.Web.UI.UserControl
{
    public event EventHandler DialogCanceled;
    public event EventHandler CredentialSubmited;
    public List<UsernamePasswordCredential> AllCredentials;
    public List<UsernamePasswordCredential> SelectedCredentials;
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Display()
    {
        this.CredentialDialog.Visible = true;        

        this.ActionDropDown.Items.Clear();
        this.ActionDropDown.Items.Add(new ListItem()
        {
            Text = Resources.CoreWebContent.WEBCODE_AK0_176,
            Value = "new",
            Selected = true
        });

        if (AllCredentials.Count > 0)
        {
            foreach (var credential in AllCredentials)
            {
                this.ActionDropDown.Items.Add(new ListItem()
                {
                    Text = credential.Name,
                    Value = AllCredentials.IndexOf(credential).ToString(),
                });
            }
        }

        this.CredentialName.Text = String.Empty;
        this.UserName.Text = String.Empty;
        this.Password.Attributes["value"] = String.Empty;
        this.ConfirmPassword.Attributes["value"] = String.Empty;
        TextBoxesDisable(false);        
    }

    protected void CredentialNameDuplicityValidation(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;
        
        if (this.ActionDropDown.SelectedValue == "new")
        {
            //TODO implement check for credential duplicity
            var duplicityInSelected = SelectedCredentials.Where(c => c.Name == e.Value);
            var duplicityInAll =AllCredentials.Where(c => c.Name == e.Value);
            e.IsValid = (duplicityInSelected.Count() == 0) && (duplicityInAll.Count()==0);            
        }
    }

    protected void CancelDialog_OnClick(object sender, EventArgs e)
    { 
        this.CredentialDialog.Visible = false;
        if (DialogCanceled != null)
            DialogCanceled(this,null);
    }    

    protected void AddNewCredential_OnClick(object sender,EventArgs e)
    {
        if (!Page.IsValid)
            return;

        UsernamePasswordCredential credential = null;

        if (this.ActionDropDown.SelectedValue == "new")
        {
            credential = new UsernamePasswordCredential()
            {
                Name= this.CredentialName.Text,
                Username = this.UserName.Text,
                Password = this.Password.Text
            };            
        }
        else
        {
            // existing credentials
            int i = int.Parse(this.ActionDropDown.SelectedValue);
            credential = AllCredentials[i];
            AllCredentials.RemoveAt(i);
        }

        // add new credential to the grid
        SelectedCredentials.Add(credential);        

        if(CredentialSubmited!=null)
        {
            CredentialSubmited(this, null);
        }

        this.CredentialDialog.Visible = false;                
    }

    protected void ChooseCredentialChanged(object sender, EventArgs e)
    {
        DropDownList dd = (DropDownList)sender;

        if (dd.SelectedValue == "new")
        {
            this.CredentialName.Text = String.Empty;
            this.UserName.Text = String.Empty;
            this.Password.Attributes["value"] = String.Empty;
            this.ConfirmPassword.Attributes["value"] = String.Empty;
            TextBoxesDisable(false);
        }
        else
        {
            int i = int.Parse(dd.SelectedValue);

            string pass = Regex.Replace(AllCredentials[i].Password, ".", "*");
            this.CredentialName.Text = AllCredentials[i].Name;
            this.UserName.Text = AllCredentials[i].Username;
            this.Password.Attributes["value"] = pass;
            this.ConfirmPassword.Attributes["value"] = pass;
            TextBoxesDisable(true);
        }
    }

    private void TextBoxesDisable(bool disable)
    {
        this.CredentialName.Enabled = !disable;
        this.UserName.Enabled = !disable;
        this.Password.Enabled = !disable;
        this.ConfirmPassword.Enabled = !disable;
    }


   
}