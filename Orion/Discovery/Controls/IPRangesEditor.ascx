<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPRangesEditor.ascx.cs" Inherits="Orion_Discovery_Controls_IPRangesEditor" Debug="true" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Extensions" %>

<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<style type="text/css">
    .jasmine_html-reporter {
    
        margin-bottom:100px;
    }
</style>

<div id="IPRangesEditor" class="editor">
    <asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy" ></asp:ScriptManagerProxy>

    <script type="text/javascript">
        //<![CDATA[

        (function (root) {

            var ERROR_CODE = {
                NONE: 'None',
                FORMAT: 'Format',
                INVALID_RANGE: 'InvalidRange',
                OVERLAPPING_RANGE: 'OverlappingRange'
            };

            var ERROR_CLASS = 'sw-validation-error';

            function isValidIPAddress(value) {
                if (!value) {
                    return false;
                }

                var result = true;

                var parts = value.split('.');
                if (parts.length != 4) {
                    return false;
                }
                for (var i = 0; i < parts.length; i++) {
                    var part = parts[i];
                    if (part.trim() == '') {
                        result = false;
                        break;
                    }

                    var val = new Number(part);
                    if (isNaN(val) || val < 0 || val > 255) {
                        result = false;
                        break;
                    }
                }

                return result;

            }

            function convertIpToInt(input) {
                var parts = input.split('.');
                return ((((+parts[0]) * 256 + (+parts[1])) * 256) + (+parts[2])) * 256 + (+parts[3]);
            }

            function isOverlapping(target, other) {

                var beginIp = convertIpToInt(target.begin);
                var endIp = convertIpToInt(target.end);

                var otherBeginIp = convertIpToInt(other.begin);
                var otherEndIp = convertIpToInt(other.end);

                return (beginIp <= otherBeginIp && otherEndIp <= endIp)  // target range contains other range
                    || (endIp >= otherBeginIp && endIp <= otherEndIp)   // target range leftmost overlap 
                    || (beginIp >= otherBeginIp && beginIp <= otherEndIp)  // target range rightmost overlap
                    || (beginIp >= otherBeginIp && endIp <= otherEndIp);  // other range contains target range

            }

            function isValidRange(target) {
                if (isValidIPAddress(target.begin) && isValidIPAddress(target.end)) {
                    var beginIp = convertIpToInt(target.begin);
                    var endIp = convertIpToInt(target.end);
                    return beginIp <= endIp;
                }

                return false;
            }

            function isFilledInRange(target) {
                return target.begin && target.end && isValidIPAddress(target.begin) && isValidIPAddress(target.end);
            }


            function Row(target) {
                this.row = target;
            }

            Row.prototype.getRowId = function () {
                this.rowId = this.rowId || new Number(this.row.attributes.rowid.value);
                return this.rowId;
            }

            Row.prototype.clearError = function () {
                var wrapped = $(this.row);
                wrapped.find('span').each(function (index, item) {
                    if (item.id.indexOf('Validator') >= 0) {
                        item.textContent = '';
                        item.style.display = "none";
                    }
                });

                wrapped.find('input[type=text]').each(function (index, item) {
                    if (item.id.indexOf('TextBox') >= 0 ) {
                        $(item).removeClass(ERROR_CLASS);
                    }
                });

                wrapped.removeClass(ERROR_CLASS);
                this.row.attributes.errorcode = ERROR_CODE.NONE;
            }
        
            Row.prototype.setError = function (message) {
                var wrapped = $(this.row);
                if (!wrapped.hasClass(ERROR_CLASS)) {
                    wrapped.addClass(ERROR_CLASS);
                }
                var firstValidator = wrapped.find('span.'+ ERROR_CLASS)[0];
                firstValidator.textContent = message;
                firstValidator.style.visibility = "visible";

            }

            Row.prototype.setErrorCode = function (errorCode) {
                this.row.attributes.errorcode = errorCode;
            }

            Row.prototype.hasFormatError = function () {
                return this.row.attributes.errorcode && this.row.attributes.errorcode == ERROR_CODE.FORMAT;
            }

            Row.prototype.updateFormatErrorState = function () {
                var wrapped = $(this.row);
                var tbx = wrapped.find('input[type=text]');
                if (tbx[0].attributes.hasformatError || tbx[1].attributes.hasformatError) {
                    //clear any row error

                    wrapped.removeClass(ERROR_CLASS);
                    tbx.each(function(index, item){
                        if (!item.attributes.hasformatError) {
                            // clear validator text for any other errors
                            var validator = item.nextElementSibling;
                            validator.style.visibility = "collapsed";
                            validator.textContent = "";
                        }
                    });
                                       

                    this.setErrorCode(ERROR_CODE.FORMAT);

                } else if (this.row.attributes.errorcode == ERROR_CODE.FORMAT) {
                        // clear only format error in text boxes

                        this.row.attributes.errorcode = ERROR_CODE.NONE;
                }

            }

            Row.prototype.getRange = function () {
                var rowid = this.getRowId();

                if (rowid >= 0 && rowid < ipRangesEditor_ip_ranges.length) {
                    return ipRangesEditor_ip_ranges[rowid];
                }
                return undefined;
            }

            Row.prototype.getRowContainer = function () {
                return new RowContainer(this.row.parentElement);
            }

            function Cell(target) {
                this.cell = target;
            }

            Cell.prototype.setFormatError = function (message) {
                var wrapped = $(this.cell);
                var textBox = $(wrapped.find('input[type=text]')[0]);
                textBox.attr('hasformaterror', 'True');

                if (!textBox.hasClass(ERROR_CLASS)) {
                    textBox.addClass(ERROR_CLASS);
                }

                var validator = wrapped.find('span.' + ERROR_CLASS)[0];
                validator.textContent = message;
                validator.style.visibility = 'visible';
            }

            Cell.prototype.clearFormatError = function() {
                var wrapped = $(this.cell);
                var textBox = $(wrapped.find('input[type=text]')[0]);
                textBox.removeAttr('hasformaterror');

                textBox.removeClass(ERROR_CLASS);

                var validator = wrapped.find('span.' + ERROR_CLASS)[0];
                validator.textContent = '';
                validator.style.visibility = 'collapsed';
            }

            function RowContainer(target) {
                this.rowContainer = target;
            }

            RowContainer.prototype.getRowById = function (id) {
                var row = $(this.rowContainer).find('tr[rowid="' + id + '"]')[0];
                if (row) {
                    return new Row(row);
                }
                return undefined;
            }

            function validate(source, args) {

                //Update changed value

                var row = new Row(source.parentElement.parentElement);
                var rowContainer = row.getRowContainer();

                var currentRange = row.getRange();
                if (!currentRange) {
                    return;
                }

                var cell = new Cell(source.parentElement);


                if (source.id.indexOf('BeginIPTextBoxValidator') >= 0) {
                    currentRange.begin = args.Value;
                } else if (source.id.indexOf('EndIPTextBoxValidator') >= 0) {
                    currentRange.end = args.Value;
                }


                if (!isValidIPAddress(args.Value)) {

                    row.setErrorCode(ERROR_CODE.FORMAT);
                    cell.setFormatError("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_1) %>");
                    row.updateFormatErrorState();
                    args.IsValid = false;
                    return;
                } else {

                    cell.clearFormatError();
                    row.updateFormatErrorState();
                }


                if (row.hasFormatError() || !isFilledInRange(currentRange)) {
                    // There are still format errors, or range is not filled completeley do nothing
                    return;
                }

                // Clear only non-format errors
                row.clearError();

                // Make sure begin < end

                if (!isValidRange(currentRange)) {

                    row.setErrorCode(ERROR_CODE.INVALID_RANGE);
                    row.setError("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_12) %>");

                    // nothing to validate here more, prevent from postback
                    args.IsValid = false;
                    return;
                }

                // Make sure there are no overlapping ranges 

                for (var i = 0; i < ipRangesEditor_ip_ranges.length ; i++) {

                    currentRange = ipRangesEditor_ip_ranges[i];
                    if (!isFilledInRange(currentRange) || !isValidRange(currentRange)) {
                        continue;
                    }

                    // Clear error for current row

                    var currentRow = rowContainer.getRowById(i);
                    if (!currentRow) {
                        continue;
                    }

                    currentRow.clearError();

                    for (var j = 0; j < ipRangesEditor_ip_ranges.length; j++) {
                        if (i == j) {
                            continue;
                        }

                        var range = ipRangesEditor_ip_ranges[j];

                        if (!isFilledInRange(range) || !isValidRange(range)) {
                            continue;
                        }

                        if (isOverlapping(currentRange, range)) {

                            currentRow.setErrorCode(ERROR_CODE.OVERLAPPING_RANGE);
                            currentRow.setError("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_2) %>");
                            args.IsValid = false;
                            break;

                        }
                    }

                }

                return;
            }

            root.IPRangesEditorValidate = validate;

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function (evt) {
                

                ipRangesEditor_ip_ranges = [];
                $("#<%=GridView.ClientID%>").find("tr").each(function (index, item) {
                    if (item.attributes.rowid != null) {
                        var textBoxes = $(item).find("input[type=text]");

                        ipRangesEditor_ip_ranges.push({ begin: textBoxes[0].value, end: textBoxes[1].value , error: item.attributes.errorcode ? item.attributes.errorcode.value : ERROR_CODE.NONE });

                    }
                });
            });

        })(window);


        //]]>

    </script>
<%--    <% if (this.IsEmbeddedJavaScriptTestsEnabled()) { %>
    <script type="text/javascript">
        // Inline tests

        describe("IP Ranges Editor", function () {
            it("should expose validation function", function () {
                expect(IPRangesEditorValidate).toBeDefined();
            });

        });

    </script>
    <% } %>--%>

    <h1 class="editor-label"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_AK0_24%>"></asp:Literal> </h1>
    <div class="editor-content">
        <asp:UpdatePanel ID="IPRangesUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="GridView" runat="server" automation="iprangelist">
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_98 %>">
                            <ItemTemplate>
                                <asp:TextBox ID="BeginIPTextBox" runat="server" autocomplete="off"
                                    Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "RangeBegin")) %>'
                                    OnTextChanged="IPTextBox_TextChanged" CausesValidation="true" ValidationGroup="IPRanges">

                                </asp:TextBox>
                                <asp:CustomValidator ID="BeginIPTextBoxValidator" runat="server"
                                    ControlToValidate="BeginIPTextBox"
                                    ValidateEmptyText="true"
                                    OnServerValidate="IPTextBoxValidator_ServerValidate"
                                    Display="Dynamic"
                                    ClientValidationFunction="IPRangesEditorValidate" ValidationGroup="IPRanges"
                                    EnableClientScript="true">

                                </asp:CustomValidator>

                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_95 %>">
                            <ItemTemplate>
                                <asp:TextBox ID="EndIPTextBox" runat="server" autocomplete="off"
                                    Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "RangeEnd")) %>'
                                    OnTextChanged="IPTextBox_TextChanged" ValidationGroup="IPRanges"
                                    CausesValidation="true">

                                </asp:TextBox>

                                <asp:CustomValidator ID="EndIPTextBoxValidator" runat="server"
                                    ControlToValidate="EndIPTextBox"
                                    ValidateEmptyText="true"
                                    OnServerValidate="IPTextBoxValidator_ServerValidate"
                                    Display="Dynamic"
                                    ClientValidationFunction="IPRangesEditorValidate" ValidationGroup="IPRanges"
                                    EnableClientScript="true">

                                </asp:CustomValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/TrashBin.png" CommandName="DeleteRow" />
                    </Columns>

                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="AddItemButton" />
                <asp:AsyncPostBackTrigger ControlID="GridView" EventName="RowCommand" />
            </Triggers>

        </asp:UpdatePanel>


        <div class="add-button">

            <asp:LinkButton runat="server" ID="AddItemButton" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AF0_11 %>" OnClick="AddButton_Click" CausesValidation="true" CssClass="link-button editor-content" ValidationGroup="IPRanges"></asp:LinkButton>

        </div>
    </div>



</div>
