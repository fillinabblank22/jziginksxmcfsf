﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_Discovery_Controls_ActiveDirectoryDetail : System.Web.UI.UserControl
{
    public event EventHandler DialogCanceled;
    public event EventHandler DialogSubmited;

    public Func<string, bool> ValidateCredentialName;
    public List<UsernamePasswordCredential> AllCredentials;

    public ActiveDirectoryAccess ActiveItem;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Page.IsPostBack)
        {
            // recovery
            this.AllCredentials = (List<UsernamePasswordCredential>)ViewState["AllCredentials"];
            this.ActiveItem = (ActiveDirectoryAccess)ViewState["ActiveItem"];
        }
    }

    public void Display()
    {
        this.CredentialDialog.Visible = true;

        this.ActionDropDown.Items.Clear();
        this.ActionDropDown.Items.Add(new ListItem()
        {
            Text = Resources.CoreWebContent.WEBCODE_AK0_176,
            Value = "new",
            Selected = true
        });

        if (AllCredentials.Count > 0)
        {
            foreach (var credential in AllCredentials)
            {
                this.ActionDropDown.Items.Add(new ListItem()
                {
                    Text = credential.Name,
                    Value = AllCredentials.IndexOf(credential).ToString(),
                });
            }
        }

        this.CredentialName.Text = String.Empty;
        this.UserName.Text = String.Empty;
        this.Password1.Attributes["value"] = String.Empty;
        this.ConfirmPassword1.Attributes["value"] = String.Empty;
        TextBoxesDisable(false);

        if (this.ActiveItem != null)
        {
            this.HostName.Text = this.ActiveItem.HostName;

            int index = this.AllCredentials.FindIndex(n => n.Name == this.ActiveItem.Credential.Name);

            if (index != -1)
            {
                this.ActionDropDown.SelectedValue = index.ToString();
            }
        }

        ViewState["AllCredentials"] = this.AllCredentials;
        ViewState["ActiveItem"] = this.ActiveItem;
    }

    protected void CredentialNameDuplicityValidation(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = true;

        if (this.ActionDropDown.SelectedValue == "new")
        {
            if (ValidateCredentialName != null)
            {
                e.IsValid = ValidateCredentialName(e.Value);
            }
        }
    }

    protected void CancelDialog_OnClick(object sender, EventArgs e)
    {
        this.CredentialDialog.Visible = false;
        if (DialogCanceled != null)
            DialogCanceled(this, null);
    }

    protected void AddNew_OnClick(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        UsernamePasswordCredential credential = null;

        if (this.ActionDropDown.SelectedValue == "new")
        {
            credential = new UsernamePasswordCredential()
            {
                Name = this.CredentialName.Text,
                Username = this.UserName.Text,
                Password = this.Password1.Text
            };
        }
        else
        {
            // existing credentials
            int i = int.Parse(this.ActionDropDown.SelectedValue);
            credential = AllCredentials[i];
        }

        if (this.ActiveItem == null)
            this.ActiveItem = new ActiveDirectoryAccess();

        this.ActiveItem.HostName = this.HostName.Text;
        this.ActiveItem.Credential = credential;

        // event about finished operation
        if (DialogSubmited != null)
            DialogSubmited(this, null);

        ViewState.Remove("AllCredentials");
        ViewState.Remove("ActiveItem");

        this.CredentialDialog.Visible = false;
        this.AllCredentials = null;
        this.ActiveItem = null;
    }

    protected void ChooseCredentialChanged(object sender, EventArgs e)
    {
        DropDownList dd = (DropDownList)sender;

        if (dd.SelectedValue == "new")
        {
            this.CredentialName.Text = String.Empty;
            this.UserName.Text = String.Empty;
            this.Password1.Attributes["value"] = String.Empty;
            this.ConfirmPassword1.Attributes["value"] = String.Empty;            
            TextBoxesDisable(false);
        }
        else
        {
            int i = int.Parse(dd.SelectedValue);

            this.CredentialName.Text = AllCredentials[i].Name;
            this.UserName.Text = AllCredentials[i].Username;            
            this.Password1.Attributes["value"] = "*******************";
            this.ConfirmPassword1.Attributes["value"] = "*******************";            
        }
    }

    private void TextBoxesDisable(bool disable)
    {
        this.CredentialName.Enabled = !disable;
        this.UserName.Enabled = !disable;
        this.Password1.Enabled = !disable;
        this.ConfirmPassword1.Enabled = !disable;
    }
}