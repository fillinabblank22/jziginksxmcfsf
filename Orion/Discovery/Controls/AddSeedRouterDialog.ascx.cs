﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Web.Discovery;
using Credentials = SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.Models.Discovery;

public partial class Orion_Discovery_Controls_AddSeedRouterDialog : System.Web.UI.UserControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    public delegate void SeedRouterAddedEventHandler(List<Subnet> routerSubnets);

    /// <summary> Called when AddSeddRouter dialog is closed and its subnets should be added. </summary>
    public event SeedRouterAddedEventHandler SeedRouterAdded;

    public void OpenDialog()
    {
        this.AddSeedRouterDialog.Visible = true;
        this.ModalityDiv.ShowBlock = true;

        this.RouterText.Text = String.Empty;
    }

    protected void AddButton_OnClick(object sender, EventArgs e)
    {
        List<Subnet> subnets = FindRouterSubnets(RouterText.Text);
        if (subnets == null) return;
        if (SeedRouterAdded != null)
        {
            SeedRouterAdded(subnets);
        }
        CloseDialog();
    }

    /// <summary> </summary>
    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    /// <summary> Finds seed router's subnets. It is called through Job Engine. </summary>
    /// <param name="router">Router's IP or hostname</param>
    private List<Subnet> FindRouterSubnets(string router)
    {
        if (string.IsNullOrEmpty(router))
        {
            this.RouterTextHostValidator.ErrorMessage = Resources.CoreWebContent.WEBCODE_AK0_36;
            this.RouterTextHostValidator.IsValid = false;
            return null;
        }

        try
        {
            DiscoveryConfiguration conf = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
            CoreDiscoveryPluginConfiguration coreConf = conf.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
            List<SnmpEntry> snmpEntries =
                coreConf != null ? new List<SnmpEntry>(from c in coreConf.Credentials where c is Credentials.SnmpCredentials select CredentialHelper.GetSnmpEntry(c as Credentials.SnmpCredentials))
                : new List<SnmpEntry>() { new SnmpEntry() { Name = "public", Version = SNMPVersion.SNMP1 }, new SnmpEntry() { Name = "private", Version = SNMPVersion.SNMP1 } };

            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, conf.EngineID))
            {
                string errorMessage;
                List<Subnet> subnets = businessProxy.FindRouterSubnets(
                    router, snmpEntries, conf.EngineID, out errorMessage);

                if (errorMessage != null)
                {
                    this.RouterTextHostValidator.ErrorMessage = errorMessage;
                    this.RouterTextHostValidator.IsValid = false;
                    return null;
                }

                return subnets;
            }
        }
        catch (Exception ex)
        {
            log.Error("Cannot start router discovery", ex);
            throw;
        }
    }

    private void CloseDialog()
    {
        this.AddSeedRouterDialog.Visible = false;
        this.ModalityDiv.ShowBlock = false;
    }

    protected void CancelAddButton_OnClick(object sender, EventArgs e)
    {
        CloseDialog();
    }
}
