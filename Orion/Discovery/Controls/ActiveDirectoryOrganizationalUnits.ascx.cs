﻿using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_Discovery_Controls_ActiveDirectoryOrganizationalUnits : WizardPanelStepBase
{
    protected readonly string ShowIncludeFutureOuHintSettingName = "ShowIncludeFutureOuHint";

    protected bool ShowIncludeFutureOuHint
    {
        get
        {
            var value = WebUserSettingsDAL.Get(ShowIncludeFutureOuHintSettingName);
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }

            return Convert.ToBoolean(value); 
        }
    }
}