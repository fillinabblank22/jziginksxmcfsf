﻿using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.UI;
using System.Collections.Generic;

public partial class Orion_Discovery_Controls_ActiveDirectoryEditor : WizardPanelStepBase
{
    private IList<SolarWinds.Orion.Core.Models.Discovery.ActiveDirectoryAccess> ActiveDirectoryAccessList
    {
        get { return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().ActiveDirectoryList; }
    }
}