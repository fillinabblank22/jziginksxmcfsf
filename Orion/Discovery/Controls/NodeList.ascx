<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeList.ascx.cs" Inherits="Orion_Discovery_Wizard_Controls_NodeList" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<orion:Include ID="Include1" runat="server" File="Discovery.css" />

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $(".CheckAllSelector input[type=checkbox]").click(function () {
            var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function () {
                this.checked = checked_status;
            });
        });
    });
    //]]>
</script>


<div style="padding: 0 10px 0 10px;">
    <asp:ListView ID="NodeTableGrid" runat="server" AutoGenerateColumns="False" OnLayoutCreated="NodeTableGrid_LayoutCreated" OnItemDataBound="NodeTableGrid_ItemDataBound">
        <LayoutTemplate>
            <table class="NetObjectTable" cellspacing="0">
                <thead>
                    <tr>
                        <td>
                            <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged" OnPreRender="CheckAllSelector_OnPreRender" />
                        </td>
                        <td>&nbsp;</td>
                        <td><asp:Literal ID="Literal1" runat="server" Text="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_1 %>" /></td>
                        <td><asp:Literal ID="Literal2" runat="server" Text="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_12 %>" /></td>
                        <td><asp:Literal ID="Literal3" runat="server" Text="<%$HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_16 %>" /></td>
                        <asp:PlaceHolder ID="phPluginColumns" runat="server"></asp:PlaceHolder>
                    </tr>
                </thead>
                <tbody>
                    <tr runat="server" id="itemPlaceholder"/>
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:CheckBox ID="IsSelectedChB" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" /></td>
                <td><img src='<%# DefaultSanitizer.SanitizeHtml(Eval("VendorIcon")) %>' alt="" /> </td>
                <td nowrap><%# DefaultSanitizer.SanitizeHtml(Eval("IPAddress")) %></td>
                <td nowrap><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></td>
                <td class="wrapCell"><%# DefaultSanitizer.SanitizeHtml(Eval("DeviceType")) %></td>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="NetObjectAlternatingRow">
                <td><asp:CheckBox ID="IsSelectedChB" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" /></td>
                <td><img src='<%# DefaultSanitizer.SanitizeHtml(Eval("VendorIcon")) %>' alt="" /> </td>
                <td nowrap><%# DefaultSanitizer.SanitizeHtml(Eval("IPAddress")) %></td>
                <td nowrap><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></td>
                <td class="wrapCell"><%# DefaultSanitizer.SanitizeHtml(Eval("DeviceType")) %></td>
         </AlternatingItemTemplate>
    </asp:ListView>


<%--    <asp:Repeater runat="server" ID="DeviceTable">
        <HeaderTemplate>
            <table border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("IsSelected")) %></td>
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("Count")) %></td>
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></td>
                </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
                <tr style="background-color: #E4F1F8;">
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("IsSelected")) %></td>
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("Count")) %></td>
                    <td><%# DefaultSanitizer.SanitizeHtml(Eval("Name")) %></td>
                </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </tbody></table>
        </FooterTemplate>
    </asp:Repeater>--%>
</div>




