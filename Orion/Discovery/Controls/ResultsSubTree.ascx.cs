﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Core.Common.Enums;
using System.Text;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Common.Extensions;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Core.Models.Enums;


[ToolboxData("<{0}:NetObjectsSubTree runat=server></{0}:NetObjectsSubTree>")]
public partial class Orion_Discovery_Controls_ResultsSubTree : System.Web.UI.UserControl
{
    private static Log log = new Log();

    public bool SelectionEnabled
    {
        get;
        set;
    }

    public int NodeID
    {
        get
        {
            if (ViewState["ParentNodeID"] != null)
                return Convert.ToInt32(ViewState["ParentNodeID"]);
            else
                throw new ArgumentNullException("NodeID is missing");
        }
        set
        {
            ViewState["ParentNodeID"] = value;
        }

    }

    public int ProfileID
    {
        get
        {
            if (ViewState["ParentProfileID"] != null)
                return Convert.ToInt32(ViewState["ParentProfileID"]);
            else
                throw new ArgumentNullException("ProfileID missing");
        }
        set
        {
            ViewState["ParentProfileID"] = value;
        }
    }

    public bool IsExpanded
    {
        get
        {
            if (ViewState["IsExpanded"] != null)
                return Convert.ToBoolean(ViewState["IsExpanded"]);
            else
                return false;
        }
        set
        {
            ViewState["IsExpanded"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    private DiscoveryNodeUI discoveryNodeUI;
    protected DiscoveryNodeUI DiscoveryNodeUI
    {
        get
        {
            if (discoveryNodeUI == null)
            {
                discoveryNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.SingleOrDefault(n => n.DiscoveryNode.NodeID == NodeID && n.DiscoveryNode.ProfileID == ProfileID);
            }
            return discoveryNodeUI;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ExpandImageButton.Visible = 
            DiscoveryNodeUI.DiscoveryNode.TotalInterfacesCount > 0 || 
            DiscoveryNodeUI.DiscoveryNode.TotalVolumesCount > 0 || 
            DiscoveryNodeUI.DiscoveryNode.Groups.Any( n => n.TotalCount > 0);

        if (IsExpanded == true)
        {            
            ExpandImageButton.ImageUrl = "~/Orion/images/Button.Collapse.gif";            
            if (DiscoveryNodeUI != null)
            {
                try
                {
                    DiscoveryNodeUI.LoadVolumesAndInterfaces();                    
                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Unable to load Volumes and Interfaces for Node: NodeID:{0} ProfileID:{1}", DiscoveryNodeUI.DiscoveryNode.NodeID, DiscoveryNodeUI.DiscoveryNode.ProfileID);
                    throw new ApplicationException(String.Format("Unable to load Volumes and Interfaces for Node: NodeID:{0} ProfileID:{1}", DiscoveryNodeUI.DiscoveryNode.NodeID, DiscoveryNodeUI.DiscoveryNode.ProfileID), ex);
                }

                InterfaceListRepeater.DataSource = DiscoveryNodeUI.DiscoveryNode.Interfaces;
                InterfaceListRepeater.DataBind();

                VolumeListRepeater.DataSource = DiscoveryNodeUI.DiscoveryNode.Volumes;
                VolumeListRepeater.DataBind();

                // pluggin data
                PluginsRepeater.DataSource = DiscoveryNodeUI.DiscoveryNode.Groups;
                PluginsRepeater.DataBind();
            }
        }
        else
        {
            ExpandImageButton.ImageUrl = "~/Orion/images/Button.Expand.gif";
        }        
    }

    protected void ExpandImageButton_Click(Object sender, EventArgs e)
    {        
        DiscoveryNodeUI dNodeUI = ScheduleDiscoveryHelper.ScheduleDiscoveryResults.SingleOrDefault(n => n.DiscoveryNode.NodeID == NodeID && n.DiscoveryNode.ProfileID == ProfileID);
        dNodeUI.IsExpanded = !dNodeUI.IsExpanded;
    }

    #region Utility methods

    protected string GetInterfaceStatusIcon(DiscoveryInterfaceStatus status)
    {
        switch (status)
        {
            case DiscoveryInterfaceStatus.Imported:
                return @"/Orion/Discovery/images/StatusIcons/icon_imported_16x16.gif";
            case DiscoveryInterfaceStatus.NotImported:
                return SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "Discovery/images/StatusIcons/icon_found_16x16.gif");
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    protected string GetInterfaceStatusText(DiscoveryInterfaceStatus status)
    {
        switch (status)
        {
            case DiscoveryInterfaceStatus.Imported:
                return @"Imported";
            case DiscoveryInterfaceStatus.NotImported:
                return @"New";
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    protected string GetVolumeStatusIcon(DiscoveryVolumeStatus status)
    {
        switch (status)
        {
            case DiscoveryVolumeStatus.Imported:
                return @"/Orion/Discovery/images/StatusIcons/icon_imported_16x16.gif";
            case DiscoveryVolumeStatus.NotImported:
                return SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "Discovery/images/StatusIcons/icon_found_16x16.gif");
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    protected string GetVolumeStatusText(DiscoveryVolumeStatus status)
    {
        switch (status)
        {
            case DiscoveryVolumeStatus.Imported:
                return @"Imported";
            case DiscoveryVolumeStatus.NotImported:
                return @"New";
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    private void WrapText(StringBuilder sb, string format, int argument, string prefix, int id,int profileID, bool useTip)
    {
        if (useTip)
        {
            long wholeId = id;
            wholeId = wholeId << 32;
            wholeId += profileID;

            sb.AppendFormat("<a href=\"#?NetObject={0}:{1}\">", prefix, wholeId);
            sb.AppendFormat(format, argument);
            sb.Append(@"</a>");
        }
        else
        {
            sb.AppendFormat(format, argument);
        }
    }

    private void GetPluginDescription(StringBuilder bld, DiscoveryNodeUI node, bool newCount, bool useTips)
    {
        var pluginNewList = node.DiscoveryNode.Groups.Where(n => newCount ? n.NewCount > 0 : n.TotalCount > 0).Select(group =>
            {
                var def = node.GetGroupDefinition(group.GroupID);

                StringBuilder sb = new StringBuilder();

                // TODO, format in something
                WrapText(sb, string.Format(Resources.CoreWebContent.WEBCODE_ET_09, newCount ? group.NewCount : group.TotalCount, def.Group.DisplayName), 0, ScheduleDiscoveryHelper.GetDiscoveryGroupNetobject(def.GroupID), NodeID, ProfileID, useTips);

                return sb.ToString();
            }).ToList();


        if (bld.Length != 0 && pluginNewList.Count > 0)
            bld.Append(Resources.CoreWebContent.String_Separator_1_VB0_59);


        bld.Append(String.Join(Resources.CoreWebContent.String_Separator_1_VB0_59, pluginNewList));
    }

    protected string WrapDescription()
    {
        DiscoveryNodeUI discoveredNode = DiscoveryNodeUI;
        if (discoveredNode != null)
        {
            if (discoveredNode.NodeStatus.Contains(DiscoveryNodeStatus.Ignored))
            {
                return String.Empty;
            }
            
            // Don't use HRefs when prinable is true.
            bool useToolTip = Request["Printable"] == null;

            if (!useToolTip && Convert.ToString(Request["Printable"]).Equals("False", StringComparison.InvariantCultureIgnoreCase))
            {
                useToolTip = false;
            }

            if (discoveredNode.NodeStatus.Contains(DiscoveryNodeStatus.Changed))
            {
                //“XX new interfaces, YY removed interfaces, ZZ new volumes, WW removed volumes�?
                StringBuilder sb = new StringBuilder();

                if (discoveredNode.DiscoveryNode.NewInterfacesCount != 0)
                {
                    WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_56,String.Empty,"{0}",
                        String.Format("{0} ",Resources.CoreWebContent.WEBCODE_VB0_55)),
                        discoveredNode.DiscoveryNode.NewInterfacesCount, ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, NodeID, ProfileID, useToolTip);

                    if (discoveredNode.DiscoveryNode.NewVolumesCount != 0)
                    {
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, Resources.CoreWebContent.String_Separator_1_VB0_59, "{0}", String.Empty), 
                            discoveredNode.DiscoveryNode.NewVolumesCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }
                else
                {
                    if (discoveredNode.DiscoveryNode.NewVolumesCount != 0)
                    {
                        //this.Description += discoveredNode.DiscoveryNode.NewVolumesCount + " new Volumes";
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, String.Empty, "{0}",
                            String.Format("{0} ", Resources.CoreWebContent.WEBCODE_VB0_55)),
                            discoveredNode.DiscoveryNode.NewVolumesCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }

                GetPluginDescription(sb, discoveredNode, true, useToolTip);

                return sb.ToString();
            }

            if (discoveredNode.DiscoveryNode.NodeStatus.Contains(DiscoveryNodeStatus.Imported))
            {
                StringBuilder sb = new StringBuilder();
                if (discoveredNode.InterfaceCount != 0)
                {
                    //this.Description = interfaceCount + " Interfaces";
                    WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_56, String.Empty, "{0}", String.Empty),
                        discoveredNode.InterfaceCount, ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, NodeID, ProfileID, useToolTip);
 
                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += ", " + volumeCount + " Volumes";
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, Resources.CoreWebContent.String_Separator_1_VB0_59, "{0}", String.Empty),
                            discoveredNode.VolumeCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }
                else
                {
                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += volumeCount + " Volumes";\
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, String.Empty, "{0}", String.Empty),
                            discoveredNode.VolumeCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }

                GetPluginDescription(sb, discoveredNode, false, useToolTip);

                return sb.ToString(); ;
            }

            if (discoveredNode.DiscoveryNode.NodeStatus.Contains(DiscoveryNodeStatus.NotImported))
            {
                StringBuilder sb = new StringBuilder();

                if (discoveredNode.InterfaceCount != 0)
                {
                    //this.Description = "New node with " + interfaceCount + " Interfaces";
                    WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_56, Resources.CoreWebContent.WEBCODE_VB0_58, " {0}", String.Empty),
                        discoveredNode.InterfaceCount, ScheduleDiscoveryHelper.DISCOVERY_INTERFACE_PREFIX, NodeID, ProfileID, useToolTip);

                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += ", " + volumeCount + " Volumes";
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, Resources.CoreWebContent.String_Separator_1_VB0_59, "{0}", String.Empty),
                            discoveredNode.VolumeCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }
                else
                {
                    if (discoveredNode.VolumeCount != 0)
                    {
                        //this.Description += "New node with " + volumeCount + " Volumes";
                        WrapText(sb, String.Format(Resources.CoreWebContent.WEBCODE_VB0_57, Resources.CoreWebContent.WEBCODE_VB0_58, " {0}", String.Empty),
                            discoveredNode.VolumeCount, ScheduleDiscoveryHelper.DISCOVERY_VOLUME_PREFIX, NodeID, ProfileID, useToolTip);
                    }
                }

                GetPluginDescription(sb, discoveredNode, false, useToolTip);

                return sb.ToString();
            }
        }
        return String.Empty;
    }    

    protected string GetId(int NetObjectID)
    {
        return String.Format("IsSelected_{0}_{1}_{2}", NodeID, ProfileID, NetObjectID);
    }

    protected string GetId(RepeaterItem container)
    {
        var group = (DiscoveryItemGroup)((RepeaterItem)container.Parent.Parent).DataItem;
        var item = (IDiscoveredObject)container.DataItem;

        return String.Format("IsSelected_{0}_{1}_{2}_{3}", NodeID, ProfileID, group.GroupID, container.ItemIndex);
    }
    protected string GetOnClick(RepeaterItem container)
    {
        var group = (DiscoveryItemGroup)((RepeaterItem)container.Parent.Parent).DataItem;
        var item = (IDiscoveredObject)container.DataItem;

        return String.Format("ItemSelectionChanged(this.checked, {0}, {1}, \"{2}\", {3});", NodeID, ProfileID, group.GroupID, container.ItemIndex);
    }

    protected IEnumerable<object> GetGroupItems(object item)
    { 
        var group = (DiscoveryItemGroup)item;
        var groupdef = this.DiscoveryNodeUI.GetGroupDefinition(group.GroupID);

        return this.DiscoveryNodeUI.DiscoveryNode.NodeResult.PluginResults
            .SelectMany(n => n.GetDiscoveredObjects())
            .Where(groupdef.Group.IsMyGroupedObjectType)
            .Where(n => !GetItemStatus(n).Contains(NetObjectStatus.Ignored)); // < filter ignored 
    }

    NetObjectStatus GetItemStatus(IDiscoveredObject item)
    {
        var itemWithState = item as IDiscoveredObjectWithImportStatus;

        if (itemWithState == null)
            return NetObjectStatus.Nothing;

        return itemWithState.ImportStatus;
    }

    protected string GetItemStatusIcon(IDiscoveredObject item)
    {
        var status = GetItemStatus(item);
        switch (status)
        {
            case NetObjectStatus.Imported:
                return @"/Orion/Discovery/images/StatusIcons/icon_imported_16x16.gif";
            case NetObjectStatus.NotImported:
                return SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "Discovery/images/StatusIcons/icon_found_16x16.gif");
            case NetObjectStatus.Nothing:
                return "";
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    protected string GetItemStatusText(IDiscoveredObject item)
    {
        var status = GetItemStatus(item);
        switch (status)
        {
            case NetObjectStatus.Imported:
                return @"Imported";
            case NetObjectStatus.NotImported:
                return @"New";
            case NetObjectStatus.Nothing:
                return "";
            default:
                log.ErrorFormat("Status {0} is not supported", status);
                throw new NotSupportedException(String.Format("Status {0} is not supported", status));
        }
    }

    protected string GetItemTypeIcon(IDiscoveredObject item)
    {
        IDiscoveredObjectWithIcon itemWithIcon = item as IDiscoveredObjectWithIcon;

        if (itemWithIcon == null)
            return String.Empty; // < some default icon ??

        return itemWithIcon.Icon;    
    }


    #endregion
}
