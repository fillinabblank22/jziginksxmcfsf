<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPRangeList.ascx.cs" Inherits="Orion_Discovery_Controls_IPRangeList" %>
<%@ Import Namespace="SolarWinds.Orion.Web.UI.Localizer" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<asp:GridView ID="IPRangeGrid" runat="server" automation="iprangelist" AutoGenerateColumns="False" ShowFooter="true" GridLines="None" CssClass="blueBox" OnRowCommand="IPRangeGrid_RowCommand" OnDataBound="IPRangeGrid_OnDataBound" >
    <Columns>
        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_98 %>">
            <ItemTemplate>
                <asp:TextBox ID="BeginIPTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 128px;" Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "RangeBegin")) %>'></asp:TextBox><br/>
                <orion:IPAddressValidator runat="server" ID="BeginIPValidator" ControlToValidate="BeginIPTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_94 %>" />
                <asp:CustomValidator
                    ID="IPCompareValidator" 
                    runat="server" 
                    ControlToValidate="BeginIPTextBox"
                    OnServerValidate="CompareIPValidation"
                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_97 %>" 
                    EnableClientScript="false"
                    Display="Dynamic" ValidateEmptyText="true">
                </asp:CustomValidator>
                <asp:CustomValidator
                    ID="DuplicityValidator" 
                    runat="server" 
                    ControlToValidate="BeginIPTextBox"
                    OnServerValidate="DuplicityValidation"
                    ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_96 %>" 
                    EnableClientScript="false"
                    Display="Dynamic">
                </asp:CustomValidator>
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                <orion:LocalizableButton id="AndButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_170 %>"  OnClick="AndButton_Clicked"/>
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_95 %>">
            <ItemTemplate>
                <asp:TextBox ID="EndIPTextBox" runat="server" autocomplete="off" CssClass="sw-hatchbox-input" Style="width: 128px;" Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "RangeEnd")) %>'></asp:TextBox><br/>
                <orion:IPAddressValidator runat="server" ID="EndIPValidator" ControlToValidate="EndIPTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_94 %>" />
            </ItemTemplate>
            <FooterTemplate>
            <div class="sw-hatchbox" style="width: 128px;">
                &nbsp;
            </div>
            </FooterTemplate>
        </asp:TemplateField>
        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/button_X_04.gif" Text="x" CommandName="DeleteRow"/>
    </Columns>
</asp:GridView>
<div>
    <br /><br />
    <asp:CustomValidator
        ID="EmptyValidator" 
        runat="server" 
        ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_93 %>" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>
