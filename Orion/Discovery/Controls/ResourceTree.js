/// <reference path="~/Orion/js/jquery/jquery-1.7.1-vsdoc.js" />
/// <reference path="~/Orion/js/extjs/3.4/debug/ext-all-debug.js" />
// <reference path="~/Orion/js/extjs/3.4/debug/ext-jquery-adapter-debug.js" />

Ext.namespace('SW');
Ext.namespace('SW.Core');

SW.Core.ResourceTree = function () {
    var initialized;
    var toolbar;
    var resultsTimeout;
    var sessionInfoId;
    var selectIcon = '/Orion/images/nodemgmt_art/icons/icon_select.gif';
    var unselectIcon = '/Orion/images/nodemgmt_art/icons/icon_delete.gif';
    var returnUrl;
    var nodeName;
    var resultsRecieved = false;

    var onBeforeResultReceivedCallback = function () { };
    var onAfterResultReceivedCallback = function () { };

    // stolen from AlertManagerGrid.
    var renderDateTime = function (str) {

        var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
        var a = reMsAjax.exec(str);
        if (a) {
            var b = a[1].split(/[-,.]/);
            var val = new Date(+b[0]);
            return val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) +
                ' ' +
                val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern);
        }
    } 

    var showCacheDialog = function(result) {
        if (result.IsFromCache) {

            $('#cacheTooltip .text').text(String.format(
                "@{R=Core.Strings.2;K=WEBJS_VT0_1;E=js}", renderDateTime(result.CachedTime)));

            $('#cacheTooltip').show();
        }
    }

    // TREE -------------------------------------------------------------------
    GetSelectionArray = function (array) {
        $("li :checkbox, li :radio").each(function () {
            var bChecked = $(this).prop("checked");
            var mappingId = $(this).attr("mappingId");

            array.push({
                MappingIdString: mappingId,
                State: bChecked
            });
        });
    }

    function InitTree() {
        // GetRenderedTree();
        InitToolbar();
        InitLayout();
        // results arrived, we can bind it

        // get selection types
        ORION.callWebService("/Orion/Services/ResourceTree.asmx",
                            "GetSelectionTypes", { infoId: sessionInfoId },
                            function (result) {
                                if (typeof result != 'undefined' && result != null) {
                                    Ext.each(result, function (info) {
                                        var iconImage;

                                        if (info.ActionType == "Select") {
                                            iconImage = selectIcon;
                                        }
                                        else {
                                            iconImage = unselectIcon;
                                        }

                                        toolbar.add('', new Ext.Toolbar.Button({
                                            id: 'select_' + info.Caption.replace(" ", ""),
                                            icon: iconImage,
                                            text: info.Caption,
                                            handler: function () {
                                                if (info.ActionType == "Select") {
                                                    $(":checkbox[objecttype]").each(function () {
                                                        var objectType = $(this).attr("objecttype");
                                                        if ((objectType.indexOf(info.TypeName) > -1) || (info.TypeName == 'ANY')) {
                                                            $(this).prop("checked", true);
                                                            $(this).trigger("change");
                                                        }
                                                    });
                                                }
                                                else if (info.ActionType == "Unselect") {
                                                    $(":checkbox[objecttype]").each(function () {
                                                        var objectType = $(this).attr("objecttype");
                                                        if ((objectType.indexOf(info.TypeName) > -1) || (info.TypeName == 'ANY')) {
                                                            $(this).prop("checked", false);
                                                            $(this).trigger("change");
                                                        }
                                                    });
                                                }
                                                else {
                                                    alert("@{R=Core.Strings;K=WEBJS_TM0_47;E=js}" + " " + info.ActionType);
                                                }
                                            }
                                        }));
                                    });
                                }

                                // hide loading icon, show controls
                                $('#WaitingImage').hide();
                                $('#progressDiv').hide();
                                toolbar.show();
                            }
                         );
    }

    GetRenderedTree = function () {
        ORION.callWebService("/Orion/Services/ResourceTree.asmx",
                            "RenderTree", { infoId: sessionInfoId },
                            function (result) {
                                if (typeof result == 'undefined' || result == null) {
                                    resultsTimeout = setTimeout("GetRenderedTree();", 1000);
                                }
                                else {
                                    InitTree();
                                    document.getElementById("TreePlaceholder").innerHTML = result.Html;
                                    showCacheDialog(result);
                                    onAfterResultReceivedCallback();
                                }
                            });
    }

    GetResults = function () {
        onBeforeResultReceivedCallback();
        resultsRecieved = false;

        // try to get results
        ORION.callWebService("/Orion/Services/ResourceTree.asmx",
                         "GetTreeInfo", { infoId: sessionInfoId },
                         function (result) {
                             if (typeof result == 'undefined' || result == null) {
                                 // when no result, set timeout for next attempt
                                 resultsTimeout = setTimeout("GetResults();", 1000);
                             }
                             else {
                                 resultsRecieved = true;

                                 if (result.NodeIsNotAvailable) {
                                     $('#WaitingImage').hide();
                                     if (typeof (DisableSubmitButton) != "undefined") {
                                         DisableSubmitButton();
                                     }

                                     InitTree();
                                     document.getElementById("TreePlaceholder").innerHTML = "";
                                     showCacheDialog(result);
                                     onAfterResultReceivedCallback();
                                     eval(result.Caption);
                                     return;
                                 }

                                 // caption contains some javascript to be executed
                                 // it happens when there is an error
                                 if (result.Caption.length > 0) {
                                     eval(result.Caption);
                                     $('#WaitingImage').hide();
                                     onAfterResultReceivedCallback();
                                     return;
                                 }

                                 if (typeof (EnableSubmitButton) != "undefined") {
                                     EnableSubmitButton();
                                 }

                                 GetRenderedTree();
                             }
                         });
    }

    GetProgress = function () {
        // try to get results
        ORION.callWebService("/Orion/Services/ResourceTree.asmx",
                         "GetProgress", { infoId: sessionInfoId },
                         function (result) {
                             if (!resultsRecieved) {
                                 resultsTimeout = setTimeout("GetProgress();", 1000);
                                 $('#progressDiv').html("@{R=Core.Strings;K=WEBJS_TM0_48;E=js}" + "<br\>" +
                                                        "@{R=Core.Strings;K=WEBJS_TM0_49;E=js}" + " " + String(result.DiscoveredInterfaces) + "<br\>" +
                                                        "@{R=Core.Strings;K=WEBJS_TM0_50;E=js}" + " " + String(result.DiscoveredVolumes));
                             }
                         });
    }

    // TOOLBAR -------------------------------------------------------------------

    InitToolbar = function () {
        toolbar = new Ext.Toolbar({
            id: 'selectionToolbar',
            height: 30,
            region: 'north',
            hidden: true,
            items:
            [
                {
                    xtype: 'tbtext', text: '@{R=Core.Strings;K=WEBJS_TM0_51;E=js}'
                },
                '', ''
            ]
        });
    }

    // FORM -------------------------------------------------------------------

    InitLayout = function () {
        var panel = new Ext.Container({
            id: 'MainExtPanel',
            renderTo: 'TreeToolbarPlaceholder',
            items: [toolbar]
        });
        $(window).bind('resize', function () {
            panel.doLayout();
        });
    }

    return {
        // initialization function will create and set up ext tree
        init: function (infoId, returnTo, currentNodeName) {
            if (initialized)
                return;

            initialized = true;
            sessionInfoId = infoId;
            returnUrl = returnTo;
            nodeName = currentNodeName;
            GetResults();

            $('#progressDiv').show();

            GetProgress();
        },

        ForceRefresh: function () {
            window.location = window.location + '&ForceRefresh=true';
            return false;
        },

        UpdateSelectionStateInSession: function (onSuccess) {
            var selections = [];

            GetSelectionArray(selections);

            ORION.callWebService("/Orion/Services/ResourceTree.asmx", "UpdateSelection",
                                 {
                                     infoIdString: sessionInfoId,
                                     mappings: selections
                                 },
                                 function (result) {
                                     if (typeof (onSuccess) != 'undefined' && onSuccess != null) {
                                         onSuccess();
                                     }
                                 });
        },

        SetBeforeResultReceivedCallback: function (callback) {
            if ((callback != null) && (typeof callback == "function")) {
                onBeforeResultReceivedCallback = callback;
            }
        },

        SetAfterResultReceivedCallback: function (callback) {
            if ((callback != null) && (typeof callback == "function")) {
                onAfterResultReceivedCallback = callback;
            }
        }
    };
} ();
