﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Common.Snmp;
using SolarWinds.Logging;

public partial class Orion_Discovery_Controls_AutoImportVolumes : AutoImportWizardStep
{
	private static Log log = new Log();
	private const string VolumeTypeIcon = "<img src=\"/NetPerfMon/images/Volumes/{0}.gif\" />";

	protected CoreDiscoveryPluginConfiguration Config
	{
		get
		{
			return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();
		}
	}

	public override string Name
	{
		get
		{
			return Resources.CoreWebContent.AutoImportWizardVolumesStepName;
		}
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		EnsureDataBound();
	}

	private void EnsureDataBound()
	{
		if (VolumeTypeCheckboxes.Items.Count == 0)
		{
			using (LocaleThreadState.EnsurePrimaryLocale())
			{
				VolumeTypeCheckboxes.DataTextField = "DisplayName";
				VolumeTypeCheckboxes.DataValueField = "Value";
				VolumeTypeCheckboxes.DataSource =
					Enum
						.GetValues(typeof(VolumeType))
						.Cast<VolumeType>()
						.Select(t => new
							{
								DisplayName = string.Format(VolumeTypeIcon, t) + VolumeTypeHelper.GetLocalizedVolumeType(t),
								Value = (int)t
							})
						.OrderBy(c => c.DisplayName, StringComparer.CurrentCultureIgnoreCase);
				VolumeTypeCheckboxes.DataBind();
			}
		}
	}

	public override void LoadData()
	{
		EnsureDataBound();

		// set defaults - all but removable
		if (Config.AutoImportVolumeTypes == null)
		{
			Config.AutoImportVolumeTypes = Enum.GetValues(typeof(VolumeType)).Cast<VolumeType>().Except(VolumeTypeHelper.GetRemovableTypes()).ToList();
		}

		foreach (ListItem item in VolumeTypeCheckboxes.Items)
		{
			item.Selected = Config.AutoImportVolumeTypes.Contains(AsVolumeType(item.Value));
		}
	}

	public override void SaveData()
	{
		EnsureDataBound();

		var selectedTypes = new List<VolumeType>();
		foreach (ListItem item in VolumeTypeCheckboxes.Items)
		{
			if (item.Selected)
			{
				selectedTypes.Add(AsVolumeType(item.Value));
			}
		}
		Config.AutoImportVolumeTypes = selectedTypes;
	}

	private static VolumeType AsVolumeType(string value)
	{
		return (VolumeType)int.Parse(value);
	}
}