﻿using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Extensions;
using System.Threading;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.DAL;
using Resources;

public partial class Orion_Discovery_Controls_AddSubnetsWithSeedRouterDialog : System.Web.UI.UserControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    const string VALIDATION_ERROR_CSS_CLASS = "sw-validation-error";
    const string JAVASCRIPT_INCLUDE_KEY = "AddSubnetsWithSeedRouterDialog";
    const string SNMP_V1_OR_V2 = "SNMP1";
    const string SNMP_V3 = "SNMP3";
    const uint SNMP_PORT = 161;

    public event EventHandler CancelButtonClick;
    public event EventHandler DialogClosed;
    public event EventHandler AddSubnetsButtonClick;

    const string SUBNETS_SESSION_KEY = "AddSubnetsWithSeedRouterDialog_Subnets";
    const string SNMP_ENTRIES_SESSION_KEY = "AddSubnetsWithSeedRouterDialog_SnmpEntries";
    private static readonly SolarWinds.Orion.Core.Common.Models.SnmpEntry SnmpEmpty = new SolarWinds.Orion.Core.Common.Models.SnmpEntry { Name = "None" };

    private bool? isFipsEnabled;
    protected bool IsFipsEnabled
    {
        get 
        { 
            if (!this.isFipsEnabled.HasValue)
            {
                this.isFipsEnabled = EnginesDAL.IsFIPSModeEnabledOnAnyEngine();
            }

            return this.isFipsEnabled.Value;
        }
    }
    
    public IList<Subnet> Subnets
    {
        get
        {
            var items = Session[SUBNETS_SESSION_KEY] as IList<Subnet>;
            if (items == null)
            {
                items = new List<Subnet>();


                Session[SUBNETS_SESSION_KEY] = items;
            }
            return items;
        }

        set
        {
            Session[SUBNETS_SESSION_KEY] = value;
        }

    }


    private IList<SolarWinds.Orion.Core.Common.Models.SnmpEntry> SnmpEntries
    {
        get
        {
            var items = Session[SNMP_ENTRIES_SESSION_KEY] as IList<SolarWinds.Orion.Core.Common.Models.SnmpEntry>;
            if (items == null)
            {
                items = new List<SolarWinds.Orion.Core.Common.Models.SnmpEntry>();
                Session[SNMP_ENTRIES_SESSION_KEY] = items;
            }
            return items;
        }

        set
        {
            Session[SNMP_ENTRIES_SESSION_KEY] = value;
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        AuthKeyPassword.Attributes["value"] = AuthKeyPassword.Text;
        EncryptionKeyPassword.Attributes["value"] = EncryptionKeyPassword.Text;
        // make sure validation error class is cleared
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SubnetsGridView.AutoGenerateColumns = false;

        SnmpEntries = GetSharedSnmpCredentials();
        CredentialsSelect.SelectedIndex = -1;
        CredentialsSelect.SelectedValue = null;

        CredentialsSelect.DataSource = SnmpEntries.Count > 0 ? SnmpEntries : null;
        CredentialsSelect.DataBind();


        var cs = Page.ClientScript;
        if (!cs.IsClientScriptIncludeRegistered(JAVASCRIPT_INCLUDE_KEY))
        {
            cs.RegisterClientScriptInclude(JAVASCRIPT_INCLUDE_KEY, ResolveUrl("~/Orion/Discovery/js/AddSubnetsWithSeedRouterDialog.js"));
        }

        // Fill SNMPv3 authentication drop down
        this.AuthenticationMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_AF0_47, "None"));

        if (!this.IsFipsEnabled)
        {
            this.AuthenticationMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_75, "MD5"));
        }

        this.AuthenticationMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_76, "SHA"));
        this.AuthenticationMethodSelect.SelectedIndex = 0;

        // Fill SNMPv3 encryption drop down
        this.EncryptionMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_AF0_47 , "None"));

        if (!this.IsFipsEnabled)
        {
            this.EncryptionMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_77, "DES"));
        }

        this.EncryptionMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_78 , "AES"));
        this.EncryptionMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_85 , "AES192"));
        this.EncryptionMethodSelect.Items.Add(new ListItem(CoreWebContent.WEBDATA_IB0_86 , "AES256"));
        this.EncryptionMethodSelect.SelectedIndex = 0;
    }

    public void Display()
    {

        Dialog.Visible = true;
        SeedRouterSettings.Visible = true;
        ScanButton.Visible = true;
        SubnetsPanel.Visible = false;
        AddButton.Visible = false;

        SubnetsGridView.DataSource = null;
        SubnetsGridView.DataBind();

        

        ErrorMessage.Visible = false;
        LoginFailedHint.Visible = false;
        LoginSucceededHint.Visible = false;

        AddNewSNMPCreds.Checked =
        NewCredsPanel.Visible = SnmpEntries.Count == 0;

        SelectExistingSNMPCreds.Checked =
        SelectExistingSNMPCreds.Visible =
        SelectExistingCredsPanel.Visible = SnmpEntries.Count > 0;

        SeedRouterTextBox.Text = string.Empty;
        SeedRouterTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        SeedRouterValidator.IsValid = true;
        SeedRouterEmptyValidator.IsValid = true;
        SNMPPortTextBox.Text = System.Convert.ToString(SNMP_PORT);
        SNMPPortTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        SNMPPortValidator.IsValid = true;

        SnmpVersionSelect.SelectedIndex = 0;
        SNMPv1Or2Panel.Visible = true;
        SNMPv3Panel.Visible = false;

        SNMPCommunityStringTextBox.Text = string.Empty;
        SNMPCommunityStringTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        CommunityStringValidator.IsValid = true;

        UserNameTextBox.Text = string.Empty;
        UserNameTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        NonEmptyUserNameValidator.IsValid = true;

        ContextTextBox.Text = string.Empty;

        AuthenticationMethodSelect.SelectedIndex = 0;
        AuthKeyPassword.Text = string.Empty;
        AuthKePasswordPanel.Visible = false;
        AuthPasswordIsKey.Checked = true;
        AuthPasswordIsKey.Visible = false;


        EncryptionMethodSelect.SelectedIndex = 0;
        EncryptionKeyPassword.Text = string.Empty;
        EncryptionPanel.Visible = false;
        EncryptionPasswordIsKey.Checked = true;
        EncryptionPasswordIsKey.Visible = false;

        if (SnmpEntries.Count > 0)
        {
            CredentialsSelect.SelectedIndex = 0;

        }
        else
        {
            CredentialsSelect.SelectedIndex = -1;
        }
    }

    protected void SubnetsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == 0)
            {
                e.Row.Style.Add("display", "block");
                e.Row.Style.Add("padding-top", "2em");
            }
            else
            {
                e.Row.Style.Add("display", "block");
            }
        }
    }

    protected void AuthMetodDropList_SelectedIndexChanged(object sender, EventArgs e)
    {
        var select = (DropDownList)sender;

        AuthPasswordIsKey.Visible = select.SelectedIndex > 0;
        AuthKePasswordPanel.Visible = select.SelectedIndex > 0;
    }
    protected void PrivMethodDropList_SelectedIndexChanged(object sender, EventArgs e)
    {
        var select = (DropDownList)sender;
        EncryptionPanel.Visible = select.SelectedIndex > 0;
        EncryptionPasswordIsKey.Visible = select.SelectedIndex > 0;
    }

    protected void CredsTypeCheckedChanged(object sender, EventArgs e)
    {
        NewCredsPanel.Visible = AddNewSNMPCreds.Checked;
        SelectExistingCredsPanel.Visible = SelectExistingSNMPCreds.Checked;
        LoginFailedHint.Visible = false;
        LoginSucceededHint.Visible = false;

    }
    protected void SnmpVersionSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        var selected = SnmpVersionSelect.SelectedItem;
        SNMPv1Or2Panel.Visible = selected.Value == SNMP_V1_OR_V2;
        SNMPv3Panel.Visible = selected.Value == SNMP_V3;

    }
    protected void CloseDialog_Click(object sender, ImageClickEventArgs e)
    {
        FireCancelButtonClick();
        Hide();
    }

    public void Hide()
    {
        Dialog.Visible = false;



        FireDialogClosed();
    }
    protected void CancelButton_Click(object sender, EventArgs e)
    {
        FireCancelButtonClick();
        Hide();
    }

    private void FireCancelButtonClick()
    {
        var evt = CancelButtonClick;
        if (evt != null)
        {
            evt(this, EventArgs.Empty);
        }
    }

    private void FireDialogClosed()
    {

        var evt = DialogClosed;
        if (evt != null)
        {
            evt(this, EventArgs.Empty);
        }
    }

    private void FireAddSubnetsButtonClick()
    {
        var evt = AddSubnetsButtonClick;
        if (evt != null)
        {
            evt(this, EventArgs.Empty);
        }

    }

    protected void ScanButton_Click(object sender, EventArgs e)
    {
        ErrorMessage.Visible = false;

        if (!ValidateUserInput())
        {
            return;
        }


        try
        {
            var snmpEntry = GetSNMPEntry();
            var snmpEntries = new List<SolarWinds.Orion.Core.Common.Models.SnmpEntry>();
            snmpEntries.Add(snmpEntry);


            var conf = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
            CoreDiscoveryPluginConfiguration coreConf = conf.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>();


            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler, conf.EngineID))
            {
                string errorMessage;
                Subnets = businessProxy.FindRouterSubnets(
                    SeedRouterTextBox.Text, snmpEntries, conf.EngineID, out errorMessage);

                SubnetsGridView.RowDataBound += SubnetsGridView_RowDataBound;
                SubnetsGridView.DataSource = Subnets;
                SubnetsGridView.DataBind();

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    ErrorMessage.InnerText = errorMessage;
                    ErrorMessage.Visible = true;
                    return;
                }
                else
                {


                    if (AddNewSNMPCreds.Checked)
                    {
                        SnmpEntries.Add(snmpEntry);
                        conf.Snmp.Add(snmpEntry);
                        CredentialsSelect.DataSource = SnmpEntries;
                        CredentialsSelect.DataBind();
                    }




                }
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);

            ErrorMessage.InnerText = ex.Message;
            ErrorMessage.Visible = true;
            return;
        }



        SeedRouterSettings.Visible = false;
        SubnetsPanel.Visible = true;
        ScanButton.Visible = false;
        AddButton.Visible = true;
    }


    private SolarWinds.Orion.Core.Common.Models.SnmpEntry GetSNMPEntry()
    {
        if (SelectExistingSNMPCreds.Checked)
        {
            if (CredentialsSelect.SelectedIndex >= 0 && CredentialsSelect.SelectedIndex < SnmpEntries.Count)
            {

                return SnmpEntries[CredentialsSelect.SelectedIndex];
            }
            throw new InvalidOperationException("Snmp Credentials are not selected");
        }


        var entry = new SolarWinds.Orion.Core.Common.Models.SnmpEntry();
        entry.Version = (SolarWinds.Orion.Core.Common.Models.SNMPVersion)Enum.Parse(typeof(SolarWinds.Orion.Core.Common.Models.SNMPVersion), SnmpVersionSelect.SelectedValue);
        entry.AuthIsKey = AuthPasswordIsKey.Checked;

        if (AuthenticationMethodSelect.SelectedValue != "None")
        {
            entry.AuthMethod = (SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod)Enum.Parse(typeof(SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod), AuthenticationMethodSelect.SelectedValue);
        }

        entry.AuthPassword = AuthKeyPassword.Text;
        entry.Context = ContextTextBox.Text;

        if (entry.Version == SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP3)
        {
            entry.Name = FormatUserNameAndContext(UserNameTextBox.Text, ContextTextBox.Text);
        }
        else
        {
            entry.Name = SNMPCommunityStringTextBox.Text;
        }

        entry.PrivIsKey = EncryptionPasswordIsKey.Checked;
        if (EncryptionMethodSelect.SelectedValue != "None")
        {
            entry.PrivMethod = (SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod)Enum.Parse(typeof(SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod), EncryptionMethodSelect.SelectedValue);
        }

        entry.PrivPassword = EncryptionKeyPassword.Text;
        entry.UserName = UserNameTextBox.Text;

        entry.AuthIsKey = AuthPasswordIsKey.Checked;
        entry.PrivIsKey = EncryptionPasswordIsKey.Checked;

        return entry;
    }

    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }
    protected void AddButton_Click(object sender, EventArgs e)
    {
        var items = Subnets;
        if (items != null)
        {

            foreach (GridViewRow row in SubnetsGridView.Rows)
            {
                if (row.DataItemIndex >= 0 && row.DataItemIndex < items.Count)
                {
                    var item = items[row.DataItemIndex];
                    item.Selected = ((CheckBox)row.FindControl("SelectedCheckbox")).Checked;
                }
            }
        }

        FireAddSubnetsButtonClick();
    }


    private IList<SolarWinds.Orion.Core.Common.Models.SnmpEntry> GetSharedSnmpCredentials()
    {
        var result = new List<SolarWinds.Orion.Core.Common.Models.SnmpEntry>();
        
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            var snmpV2Creds = businessProxy.GetSharedSnmpV2Credentials(CoreConstants.CoreCredentialOwner);
            var snmpV3Creds = businessProxy.GetSharedSnmpV3Credentials(CoreConstants.CoreCredentialOwner);
            
            result.AddRange(snmpV2Creds.Select(c => CredentialHelper.GetSnmpEntry(c)));

            if (this.IsFipsEnabled)
            {
                result.AddRange(snmpV3Creds.Cast<SolarWinds.Orion.Core.Models.Credentials.SnmpCredentialsV3>()
                                           .Where(c => CredentialHelper.IsFIPSCompatible(c))
                                           .Select(c => CredentialHelper.GetSnmpEntry(c)));
            }
            else
            {
                result.AddRange(snmpV3Creds.Cast<SolarWinds.Orion.Core.Models.Credentials.SnmpCredentialsV3>()
                                           .Select(c => CredentialHelper.GetSnmpEntry(c)));
            }
        }

        var conf = ConfigWorkflowHelper.DiscoveryConfigurationInfo;
        result.AddRange(conf.Snmp);

        return result;
    }

    private static string ReduceUserName(string userName, int length)
    {
        if (userName.Length > length)
        {
            return userName.Substring(1, length) + "...";
        }
        else
        {
            return userName;
        }
    }

    private static string FormatUserNameAndContext(string userName, string context)
    {
        if (String.IsNullOrEmpty(context))
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_AK0_21, ReduceUserName(userName, 32));
        }
        else
        {
            return String.Format(Resources.CoreWebContent.WEBCODE_AK0_22, ReduceUserName(userName, 16), ReduceUserName(context, 16));
        }
    }

    private static SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod Convert(SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType input)
    {
        switch (input)
        {
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType.AES128:
                return SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES;
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType.AES192:
                return SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES192;
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType.AES256:
                return SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES256;
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3PrivacyType.DES56:
                return SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.DES;
            default:
                return default(SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod);
        }
    }


    private static SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod Convert(SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType input)
    {
        switch (input)
        {
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType.MD5:
                return SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod.MD5;
            case SolarWinds.Orion.Core.Models.Credentials.SNMPv3AuthType.SHA1:
                return SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod.SHA;
            default:
                return default(SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod);
        }

    }

    protected void TestCredentialsButton_Click(object sender, EventArgs e)
    {
        ErrorMessage.Visible = false;
        LoginSucceededHint.Visible = false;
        LoginFailedHint.Visible = false;
        if (!ValidateUserInput())
        {
            return;
        }

        try
        {
            var entry = GetSNMPEntry();

            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler)) // Main engine id , should be a specific engine id defined for discovery
            {
                var result = 
                    NodeWorkflowHelper.ValidateSNMPNode(proxy, 
                                                        entry.Version, 
                                                        SeedRouterTextBox.Text, 
                                                        SNMP_PORT, 
                                                        entry.Version == SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP3 ? string.Empty : entry.Name, string.Empty,
                                                        entry.AuthPassword, 
                                                        string.Empty, 
                                                        Convert(entry.AuthMethod), 
                                                        SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.None,
                                                        Convert(entry.PrivMethod), 
                                                        SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.None,
                                                        entry.PrivPassword, 
                                                        string.Empty, 
                                                        entry.Context, 
                                                        string.Empty, 
                                                        entry.UserName, 
                                                        string.Empty,
                                                        entry.AuthIsKey, 
                                                        false, 
                                                        entry.PrivIsKey, 
                                                        false);

                LoginSucceededHint.Visible = result.ValidationSucceeded;
                LoginFailedHint.Visible = !result.ValidationSucceeded;
            }
        }
        catch (Exception ex)
        {
            LoginFailedHint.Visible = true;
            log.Error(ex);
        }
    }

    private bool ValidateUserInput()
    {
        SeedRouterEmptyValidator.Validate();
        SeedRouterValidator.Validate();
        CommunityStringValidator.Validate();
        NonDuplicateCommunityStringValidator.Validate();
        NOonDuplicateUserNameValidator.Validate();

        return SeedRouterEmptyValidator.IsValid
            && SeedRouterValidator.IsValid
            && CommunityStringValidator.IsValid
            && NonDuplicateCommunityStringValidator.IsValid
            && NOonDuplicateUserNameValidator.IsValid;
    }

    private SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType Convert(SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod input)
    {
        switch (input)
        {
            case SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.AES128;
            case SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES192:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.AES192;
            case SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.AES256:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.AES256;
            case SolarWinds.Orion.Core.Common.Models.SnmpPrivMethod.DES:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType.DES56;
            default:
                return default(SolarWinds.Orion.Core.Common.Models.SNMPv3PrivacyType);
        }
    }

    private static SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType Convert(SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod input)
    {
        switch (input)
        {
            case SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod.MD5:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.MD5;
            case SolarWinds.Orion.Core.Common.Models.SnmpAuthMethod.SHA:
                return SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType.SHA1;
            default:
                return default(SolarWinds.Orion.Core.Common.Models.SNMPv3AuthType);
        }
    }
    protected void SeedRouterValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SeedRouterTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);

        args.IsValid = !string.IsNullOrWhiteSpace(args.Value) && args.Value.IsValidIPv4Format();

        if (!args.IsValid)
        {
            SeedRouterTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
    protected void CommunityStringValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!AddNewSNMPCreds.Checked)
        {
            return;
        }

        SNMPCommunityStringTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        args.IsValid = !string.IsNullOrWhiteSpace(args.Value);
        if (!args.IsValid)
        {
            SNMPCommunityStringTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
    protected void CredentialNameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //if (!AddNewSNMPCreds.Checked) {
        //    return;
        //}

        //CredentialNameTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);

        //args.IsValid = !string.IsNullOrWhiteSpace(args.Value);

        //if (!args.IsValid) {
        //    CredentialNameTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        //}

    }

    protected void NonDuplicateCredentialNameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //if (!AddNewSNMPCreds.Checked)
        //{
        //    return;
        //}

        //if (string.IsNullOrWhiteSpace(args.Value)) {
        //    return;
        //}
        //CredentialNameTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);

        //args.IsValid = SnmpEntries.Count(s => s.Name == args.Value) == 0;

        //if (!args.IsValid)
        //{
        //    CredentialNameTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        //}
    }
    protected void NonEmptyUserNameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!AddNewSNMPCreds.Checked)
        {
            return;
        }

        UserNameTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        args.IsValid = !string.IsNullOrWhiteSpace(args.Value);

        if (!args.IsValid)
        {
            UserNameTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
    protected void SNMPPortValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SNMPPortTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        int dummy = 0;
        args.IsValid = int.TryParse(args.Value, out dummy);

        if (!args.IsValid)
        {
            SNMPPortTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
    protected void NonDuplicateCommunityStringValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SNMPCommunityStringTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);

        if (SelectExistingSNMPCreds.Checked)
        {
            return;
        }

        args.IsValid = SnmpEntries.Count(s => s.Name == args.Value) == 0;
        if (!args.IsValid)
        {
            SNMPCommunityStringTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
    protected void NOonDuplicateUserNameValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        UserNameTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        if (SelectExistingSNMPCreds.Checked)
        {
            return;
        }

        var normalizedName = FormatUserNameAndContext(args.Value, ContextTextBox.Text);

        args.IsValid = SnmpEntries.Count(s => s.Name == normalizedName) == 0;

        if (!args.IsValid)
        {
            UserNameTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }

    }
    protected void SeedRouterTextBox_TextChanged(object sender, EventArgs e)
    {
        TextBox tb = (TextBox)sender;
        tb.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
    }
    protected void SeedRouterEmptyValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SeedRouterTextBox.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
        args.IsValid = !string.IsNullOrWhiteSpace(args.Value);

        if (!args.IsValid)
        {
            SeedRouterTextBox.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }
    }
}