using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using SolarWinds.Common.Collections;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Discovery.Results;
using SolarWinds.Orion.Core.Models.DiscoveredObjects;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Common.Net;

public partial class Orion_Discovery_Wizard_Controls_NodeList : System.Web.UI.UserControl
{
	protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
	{
		ResultsWorkflowHelper.CheckAllNodeIsSelected =
			((CheckBox)this.NodeTableGrid.FindControl("CheckAllSelector")).Checked;
	}

	protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
	{
		((CheckBox)this.NodeTableGrid.FindControl("CheckAllSelector")).Checked =
			ResultsWorkflowHelper.CheckAllNodeIsSelected;
	}

	private const string NodeUiListKey = "NodeUiListKey";

    [Obsolete("Don't use this field - will be removed.", true)]
	public static bool isInterfaceAllowed = new RemoteFeatureManager().AllowInterfaces;



	private List<DiscoveryPreviewResultsNodeUI> NodeUiList
	{
		get
		{
			return ViewState["NodeUiListKey"] as List<DiscoveryPreviewResultsNodeUI>;
		}
		set
		{
			ViewState["NodeUiListKey"] = value;
		}
	}

	/// <summary> Stores selected nodes from UI to specified results set.</summary>
	public void StoreSelectedNodes(IEnumerable<DiscoveryResultBase> targetResults)
	{
		// this is dictionary of results indexed by profileID
        var resultsForProfile = targetResults
			.Select(item => item.GetPluginResultOfType<CoreDiscoveryPluginResult>())
			.ToDictionary(item => item.ProfileId.Value);

		for (int i = 0; i < NodeTableGrid.Items.Count; i++)
		{
			Control item = NodeTableGrid.Items[i];
			CheckBox checkBox = (CheckBox)item.FindControl("IsSelectedChB");
			var actualRow = NodeUiList[i];

			// we must find correct result
			var coreDiscoveryPluginResult = resultsForProfile[actualRow.ProfileId];

			// now we can store information about selection
			coreDiscoveryPluginResult.DiscoveredNodes.Find(n => n.IP.ToStringIp() == NodeUiList[i].IpAddress && n.NodeID == NodeUiList[i].NodeID).
				IsSelected = checkBox.Checked;
		}
	}

    /// <summary>
    /// Return actually selected nodes.
    /// </summary>
    /// <returns></returns>
    public List<DiscoveredNode> GetSelectedNodes()
    {
        return this.GetActualNodes()
            .Where(item => item.Item2 == true)
            .Select(item => item.Item1)
            .ToList();
    }

    /// <summary>
    /// Return all actual nodes in list with information about selection.
    /// </summary>
    /// <returns></returns>
    public List<SWTuple<DiscoveredNode,bool>> GetActualNodes()
    {
        var res = new List<SWTuple<DiscoveredNode,bool>>();

        var resultsForProfile = ResultsWorkflowHelper.DiscoveryResults
            .Select(item => item.GetPluginResultOfType<CoreDiscoveryPluginResult>())
            .ToDictionary(item => item.ProfileId.Value);

        for (int i = 0; i < NodeTableGrid.Items.Count; i++)
        {
            Control item = NodeTableGrid.Items[i];
            CheckBox checkBox = (CheckBox)item.FindControl("IsSelectedChB");

            var actualRow = NodeUiList[i];

            // we must find correct result
            var coreDiscoveryPluginResult = resultsForProfile[actualRow.ProfileId];

            // now we can store information about selection
            var found =
                coreDiscoveryPluginResult.DiscoveredNodes.FirstOrDefault(
                    n => n.IP.ToStringIp() == NodeUiList[i].IpAddress && n.NodeID == NodeUiList[i].NodeID);

            if (found != null)
            {
                res.Add(SWTuple.Create(found,checkBox.Checked));
            }            
        }

        return res;
    }

    /// <summary>
    /// From session get info about selected nodes.
    /// 
    /// If it is null it means all items are selected. Otherwise only items in list are selected.
    /// </summary>
    /// <returns></returns>
    private static List<NodeSelectInfo> NodeSelectInfoItems
    {
        get
        {
            object res;
            ResultsWorkflowHelper.AutoResetItems.TryGetValue("CoreNodeSelectInfoItems", out res);
            return res as List<NodeSelectInfo>;
        }

        set { ResultsWorkflowHelper.AutoResetItems["CoreNodeSelectInfoItems"] = value; }
    }

    /// <summary>
    /// Return function which returns information about select status of given node. Node
    /// is identified by pair (ProfileId, NodeId).
    /// 
    /// Decision about status is based on NodeSelectInfoItems.
    /// </summary>
    /// <returns></returns>
    private static Func<int,int, bool> GetSelectionInfo()
    {
        var selectionInfo = NodeSelectInfoItems;

        if( selectionInfo == null)
        {
            // everything is selected
            return new Func<int, int, bool>((p,n) => true);
        }
        else
        {
            var byProfile = selectionInfo
                .GroupBy(item => item.ProfileId)
                .ToDictionary(item => item.Key);

            return new Func<int, int, bool>((p,n) =>
                                                {
                                                    if(byProfile.ContainsKey(p))
                                                    {
                                                        var items = byProfile[p];
                                                        return items.Any(item => item.NodeId == n);
                                                    }
                                                    else
                                                    {
                                                        return false;
                                                    }
                                                }
            );
        }
    }

	/// <summary> creates data object used for displaying table </summary>
	private void CreateNodeUiModel()
	{
		if (ResultsWorkflowHelper.DiscoveryResultInfo == null) return;

		var nodeList = new List<DiscoveryPreviewResultsNodeUI>();

	    var selectionFunction = GetSelectionInfo();

		foreach (var actualResult in ResultsWorkflowHelper.DiscoveryResults)
		{
			var coreDiscoveryPluginResult =
				actualResult.GetPluginResultOfType<CoreDiscoveryPluginResult>();

			foreach (var discoveredNode in coreDiscoveryPluginResult.DiscoveredNodes.Where(n => n.IsSelected)
				)
			{
				var nodeUi = new DiscoveryPreviewResultsNodeUI()
				{
                    NodeID = discoveredNode.NodeID,
					IsSelected = selectionFunction(discoveredNode.ProfileID, discoveredNode.NodeID),
					IpAddress = discoveredNode.IP.ToString(),
					ProfileId =
						coreDiscoveryPluginResult.ProfileId != null
							? coreDiscoveryPluginResult.ProfileId.Value
							: 0,
                                        Name = discoveredNode.DisplayName,
					VendorType = discoveredNode.VendorIcon,
					VendorIcon =
						"/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Vendors/" +
						discoveredNode.VendorIcon,
					DeviceType = discoveredNode.MachineType
				};

				nodeUi.InitializePluginProperties(actualResult, discoveredNode.NodeID);

				nodeList.Add(nodeUi);
			}
		}

        this.NodeUiList = nodeList.OrderBy((node) => HostHelper.ConvertIPAddressToLong(node.IpAddress)).ToList();
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (IsPostBack)
		{	
		    // we must keep information about selected items and store them into list of selected items
            // they will be used in CreateNodeUiModel method
		    StoreInformationAboutSelection();
		}

        CreateNodeUiModel();
        this.NodeTableGrid.DataSource = NodeUiList;
        this.NodeTableGrid.DataBind();

        // we remove selected items information
        this.ClearInformationAboutSelection();
	}

    /// <summary>
    /// Store information about actually selected nodes before new binding do grid is performed.
    /// </summary>
    private void StoreInformationAboutSelection()
    {
        var selected = this.GetSelectedNodes();

        NodeSelectInfoItems = selected
            .Select(item => new NodeSelectInfo() {NodeId = item.NodeID, ProfileId = item.ProfileID})
            .ToList();
    }

    /// <summary>
    /// Clear information about all items in list.
    /// </summary>
    private void ClearInformationAboutSelection()
    {
        NodeSelectInfoItems = null;
    }

	protected void NodeTableGrid_LayoutCreated(object sender, EventArgs e)
	{
		var pluginsPlaceHolder = (PlaceHolder)NodeTableGrid.FindControl("phPluginColumns");
		if (pluginsPlaceHolder != null)
		{
			foreach (var plugin in DiscoveryPreviewResultsPluginManager.Instance.GetDiscoveryResultsPlugins())
			{
				pluginsPlaceHolder.Controls.Add(new LiteralControl(string.Format("<td>{0}</td>", plugin.Value.Name)));
			}
		}
	}

	protected void NodeTableGrid_ItemDataBound(object sender, ListViewItemEventArgs e)
	{
		if (e.Item.ItemType == ListViewItemType.DataItem)
		{
			var dataItem = (ListViewDataItem)e.Item;

			if (dataItem.DataItem != null)
			{
				foreach (var plugin in DiscoveryPreviewResultsPluginManager.Instance.GetDiscoveryResultsPlugins())
				{
					e.Item.Controls.Add(new LiteralControl(string.Format("<td class=\"wrapCell\">{0}</td>",
						DataBinder.Eval(dataItem.DataItem, string.Format("Properties[\"{0}\"]", plugin.Key)))));
				}
				e.Item.Controls.Add(new LiteralControl("</tr>"));
			}
		}

	}

    /// <summary>
    /// Information about selected node. Node is identified by profile and node ID.
    /// </summary>
    public class NodeSelectInfo
    {
        public int NodeId { get; set; }
        public int ProfileId { get; set; }
    }
}
