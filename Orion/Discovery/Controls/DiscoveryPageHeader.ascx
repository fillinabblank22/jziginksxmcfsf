<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryPageHeader.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryPageHeader" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<table width="100%" id="breadcrumb">
		<tr>
			<td style="width:250px;"><a href="<%= DefaultSanitizer.SanitizeHtml(LinkBack) %>"><%= DefaultSanitizer.SanitizeHtml(TitleBack) %></a> &gt;
				<h1 style="white-space:nowrap"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
			</td>
			<td align="right" style="vertical-align: top;  width: auto;">
			    <table style="vertical-align: middle; width: 100%;">
			        <tr>
						<td style="text-align:right; white-space:nowrap;">
						<orion:IconLinkExportToPDF runat="server" />
						<orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionPHDiscoveryHome" />
						</td>
					</tr>    
	            </table>
			</td>
		</tr>
	</table>
