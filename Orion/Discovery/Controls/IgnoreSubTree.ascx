<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IgnoreSubTree.ascx.cs" Inherits="Orion_Discovery_Controls_IgnoreSubTree" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<%@ Import Namespace="SolarWinds.Orion.Core.Models.OldDiscoveryModels" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Models.Interfaces" %>

<asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="~/Orion/Services/ScheduleDiscovery.asmx"/>			
		</Services>
	</asp:ScriptManagerProxy>
<asp:UpdatePanel ID="UpdatePanelSubTree" runat="server" ChildrenAsTriggers="true"
    UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ImageButton ID="ExpandImageButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif"
            OnClick="ExpandImageButton_Click" />        
        <table>
        <asp:Repeater ID="InterfaceListRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td>                        
                        <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId((int)Eval("ID"))) %>' 
                                type="checkbox" 
                                <%# ((bool)Eval("IsSelected")) ? "checked" : String.Empty %>
                                onclick='<%# DefaultSanitizer.SanitizeHtml("InterfaceSelectionChanged(this.checked,"+Eval("ID")+");") %>' 
                                class="NetObjectSelector"
                                />
                    </td>
                    <td>
                        <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_56) %>" src="/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Interfaces/<%# DefaultSanitizer.SanitizeHtml(Eval("Type")) %>.gif" />
                    </td>
                    <td class="wrapCell">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("Caption")) %>
                    </td>
                </tr>
            </ItemTemplate>                        
        </asp:Repeater>
        <asp:Repeater ID="VolumeListRepeater" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                        <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId((int)Eval("ID"))) %>' 
                                type="checkbox" 
                                <%# ((bool)Eval("IsSelected")) ? "checked" : String.Empty %>
                                onclick='<%# DefaultSanitizer.SanitizeHtml("VolumeSelectionChanged(this.checked,"+Eval("ID")+");") %>' 
                                class="NetObjectSelector"
                                />
                    </td>
                    <td>
                        <img alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_56) %>" src="/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Volumes/<%# DefaultSanitizer.SanitizeHtml(Eval("Type")) %>.gif" />
                    </td>
                    <td class="wrapCell">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("Description")) %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="PluginsRepeater" runat="server">
            <ItemTemplate>                
                <asp:Repeater ID="PluginDataRepeater" runat="server" DataSource=' <%# DefaultSanitizer.SanitizeHtml(GetGroupItems(Container.DataItem)) %>' >
                    <ItemTemplate>
                        <tr>
                            <td>
                                <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId(Container)) %>'
                                        type="checkbox" 
                                        <%# ((IDiscoveredObject)Container.DataItem).IsSelected ? "checked" : String.Empty %>
                                        onclick='<%# DefaultSanitizer.SanitizeHtml(GetOnClick(Container)) %>' 
                                        class="NetObjectSelector"
                                        />
                            </td>
                            <td>
                                <img alt="Type Icon" src='<%# DefaultSanitizer.SanitizeHtml(GetItemTypeIcon((IDiscoveredObject)Container.DataItem)) %>' />
                            </td>
                            <td class="wrapCell">
                                <%# DefaultSanitizer.SanitizeHtml(((IDiscoveredObject)Container.DataItem).DisplayName) %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>                
            </ItemTemplate>
        </asp:Repeater>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
