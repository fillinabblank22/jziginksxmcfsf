<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSeedRouterDialog.ascx.cs" Inherits="Orion_Discovery_Controls_AddSeedRouterDialog" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>

<orion:TBox ID="ModalityDiv" runat="server" />


<asp:Panel ID="AddSeedRouterDialog" runat="server" BorderWidth="1" Visible="false" CssClass="EditPanel" style="left: 177px; top: 266px;" >
    <div class="DialogHeaderText IEWidth100">
        <asp:Label ID="ActionHeader" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_151 %>" Font-Bold="true" Font-Size="Larger" />
    </div>
    <div class="EditControls IEWidth100">
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_141) %><br />
        <div class="DiscoveringDiv">
            <orion:TBox ID="DiscoveringDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_144 %>" MessageVisible="true" />
        </div>    
        <asp:TextBox ID="RouterText" runat="server" Width="270" Enabled="true" autocomplete="off"/>
        <br />
        <asp:CustomValidator
            ID="RouterTextHostValidator" 
            runat="server" 
            ControlToValidate="RouterText"
            EnableClientScript="false"
            ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_145 %>"
            Display="Dynamic"
            ValidationGroup="AddSeedRouterDialog" >
        </asp:CustomValidator>
    </div>
    <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons IEWidth100" >
    <div class="sw-btn-bar">
        <orion:LocalizableButton id="AddButton" runat="server" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_138 %>" OnClick="AddButton_OnClick" ValidationGroup="AddSeedRouterDialog" OnClientClick="$('.DiscoveringDiv .TransparentBoxID').show();" />
        <orion:LocalizableButton id="CancelAddButton" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="CancelAddButton_OnClick" CausesValidation="false"/></div>
    </asp:Panel>
</asp:Panel>