﻿using SolarWinds.Orion.Core.Web.Discovery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Extensions;
using System.Configuration;


public partial class Orion_Discovery_Controls_SubnetsEditor : System.Web.UI.UserControl
{

    int skipItemValidationIndex = -1;
    const string JAVASCRIPT_INCLUDE_KEY = "SubnetsEditor";
    const string VALIDATION_ERROR_CSS_CLASS = "sw-validation-error";
    const string RESERVED_ADDRESSES_KEY = "ReserveSubnetsdAddresses";
    private HashSet<string> reservedSubnetAddresses;

    private IList<SolarWinds.Orion.Core.Models.Discovery.Subnet> SubnetList
    {
        get
        {
            return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<SolarWinds.Orion.Core.Models.Discovery.CoreDiscoveryPluginConfiguration>().SubnetList;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        GridView.AutoGenerateColumns = false;
        GridView.RowCommand += GridView_RowCommand;
        GridView.DataBound += GridView_DataBound;
        reservedSubnetAddresses = GetReservedSubnetAddresses();
    }

    private HashSet<string> GetReservedSubnetAddresses()
    {
        var value = ConfigurationManager.AppSettings[RESERVED_ADDRESSES_KEY];
        if (value == null) {
            return DefaultReservedAddresses();
        }
        var result = new HashSet<string>();

        var list = value.Split(',').Select( s => s.Trim());
        var subnet = new SolarWinds.Orion.Core.Models.Discovery.Subnet();
        foreach (var cidr in list) {
            subnet.SubnetCIDR = cidr;
            if (subnet.IsValidCIDR()) {
                result.Add(cidr);
            }
        }

        return result;
    }

    private static HashSet<string> DefaultReservedAddresses()
    {
        return new HashSet<string>() { 
            "0.0.0.0/8",
            "1.0.0.0/8",
            "2.0.0.0/8",
            "5.0.0.0/8",
            "14.0.0.0/8",
            "23.0.0.0/8",
            "27.0.0.0/8",
            "31.0.0.0/8",
            "37.0.0.0/8",
            "39.0.0.0/8",
            "41.0.0.0/8",
            "42.0.0.0/8",
            "58.0.0.0/8",
            "59.0.0.0/8",
            "60.0.0.0/8",
            "127.0.0.0/8",
            "128.0.0.0/16",
            "128.66.0.0/8",
            "169.254.0.0/16",
            "191.255.0.0/16",
            "192.0.0.0/16",
            "192.0.0.192/32",
            "192.0.1.0/24",
            "192.0.2.0/24",
            "197.0.0.0/8",
             "201.0.0.0/8",
            "223.255.255.0/24"
        };
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var cs = Page.ClientScript;
        if (!cs.IsClientScriptIncludeRegistered(JAVASCRIPT_INCLUDE_KEY))
        {
            cs.RegisterClientScriptInclude(JAVASCRIPT_INCLUDE_KEY, ResolveUrl("~/Orion/Discovery/js/SubnetsEditor.js"));
        }
        this.IncludeTests(new string[] { 
            "~/Orion/js/Tests/spec/SubnetsEditorSpec.js"
        });
    }


    void GridView_DataBound(object sender, EventArgs e)
    {
        if (GridView.HeaderRow != null)
        {
            GridView.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        if (GridView.Rows.Count > 5)
        {
            GridView.AddCssClass("table-vertical-scroll-bar");
        }
        else {
            GridView.RemoveCssClass("table-vertical-scroll-bar");
        }

        ValidateGrid();
    }

    private void ValidateGrid()
    {
        foreach (GridViewRow row in GridView.Rows)
        {
            // set tooltip

            row.Cells[1].Attributes["title"] = Resources.CoreWebContent.WEBDATA_AF0_90;

            if (row.DataItemIndex == skipItemValidationIndex) {
                continue;
            }
            var cidrValidator = (CustomValidator)row.FindControl("CIDRFormatValidator");
            cidrValidator.ErrorMessage = string.Empty;
            cidrValidator.IsValid = true;
            var input = (TextBox)row.FindControl("InputTextBox");
            input.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);
            cidrValidator.Validate();


        }
    }

    void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow") {
            OnDeleteRow(e);
        }
    }

    private void OnDeleteRow(GridViewCommandEventArgs e)
    {
        var index = Convert.ToInt32(e.CommandArgument);
        var items = SubnetList;
        if (index >= 0 && index < items.Count)
        {
            items.RemoveAt(index);
            GridView.DataSource = items;
            GridView.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            var items = SubnetList;
            GridView.DataSource = items;
            GridView.DataBind();
        }
        else {

            UpdateData();
            
        }
    }

    private void UpdateData()
    {
        var items = SubnetList;

        foreach (GridViewRow row in GridView.Rows)
        {
            if (row.DataItemIndex >= 0 && row.DataItemIndex < items.Count)
            {

                var item = items[row.DataItemIndex];
                var input = (TextBox)row.FindControl("InputTextBox");
                item.SubnetCIDR = input.Text;
            }

        }

        GridView.DataSource = items;
        GridView.DataBind();

    }
    protected void AddItemMenu_MenuItemClick(object sender, MenuEventArgs e)
    {
        if (e.Item.Value == "Subnet") {
            
            OnAddSubnet();
        }
        else if (e.Item.Value == "SeedRouter")
        {
            OnAddSeedRouter();

        }
    }

    private void OnAddSeedRouter()
    {
        ModalityDiv.ShowBlock = true;
        AddSubnetsDialog.Display();
    }

    private void OnAddSubnet()
    {
        var items = SubnetList;
        items.Add(new SolarWinds.Orion.Core.Models.Discovery.Subnet {  Selected = true });
        skipItemValidationIndex = items.Count - 1;
        GridView.DataSource = items;
        GridView.DataBind();
        
    }
    protected void CIDRFormatValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        var validator = (CustomValidator)source;
        var control = (TextBox)validator.FindControl(validator.ControlToValidate);
        control.RemoveCssClass(VALIDATION_ERROR_CSS_CLASS);

        try
        {
            var subnet = new SolarWinds.Orion.Core.Models.Discovery.Subnet();
            subnet.SubnetCIDR = args.Value;
            args.IsValid = subnet.IsValidCIDR();

            if (!args.IsValid) {
                validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_19;
            } else {
                // for D and E subnet classes CIDR is not defined
                var subnetClass = subnet.ClassName;

                args.IsValid = !subnetClass.Equals("D") && !subnetClass.Equals("E") && !reservedSubnetAddresses.Contains(args.Value);

                if (!args.IsValid) {
                    validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_100;
                }
            }

        } catch (ArgumentOutOfRangeException) {
            args.IsValid = false;
            validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_100;
        } 


        if (!args.IsValid) {
            control.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
            return;
        }


        args.IsValid = SubnetList.Count(s => args.Value == s.SubnetCIDR) < 2;
        if (!args.IsValid)
        {
            validator.ErrorMessage = Resources.CoreWebContent.WEBDATA_AF0_55;
            control.AddCssClass(VALIDATION_ERROR_CSS_CLASS);
        }


    }
    protected void InputTextBox_TextChanged(object sender, EventArgs e)
    {
        var textBox = (TextBox)sender;
        var gridViewRow = (GridViewRow)textBox.Parent.Parent;
        var items = SubnetList;

        if (gridViewRow.RowIndex >= 0 && gridViewRow.RowIndex < items.Count)
        {

            var item = items[gridViewRow.RowIndex];
            try
            {
                item.SubnetCIDR = textBox.Text;

            }
            catch (FormatException) { 
            
            }
            
        }
    }

    public void AddUpdateTrigger(WebControl triggeringControl)
    {

        var trigger = new AsyncPostBackTrigger();
        trigger.ControlID = triggeringControl.ID;

        SubnetsUpdatePanel.Triggers.Add(trigger);
    }
    protected void AddSubnetsDialog_DialogClosed(object sender, EventArgs e)
    {
        ModalityDiv.ShowBlock = false;
        SubnetsUpdatePanel.Update();
    }
    protected void AddSubnetsDialog_AddSubnetsButtonClick(object sender, EventArgs e)
    {
        var subnets = AddSubnetsDialog.Subnets;
        var items = SubnetList;
        var lookup = items.ToLookup(s => s.SubnetCIDR);


        if (subnets != null) {

            foreach (var subnet in subnets)
            {
                if (subnet.Selected && !lookup.Contains(subnet.SubnetCIDR))
                {
                    items.Add(subnet);
                }
            }





            GridView.DataSource = items;
            GridView.DataBind();
        }


        AddSubnetsDialog.Hide();
    }
}