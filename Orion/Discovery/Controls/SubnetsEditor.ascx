<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubnetsEditor.ascx.cs" Inherits="Orion_Discovery_Controls_SubnetsEditor" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Extensions" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Discovery/Controls/AddSubnetsWithSeedRouterDialog.ascx" TagPrefix="orion" TagName="AddSubnetsDialog" %>





<div id="SubnetsEditor" class="editor">
    <asp:ScriptManagerProxy runat="server"  ID="SriptManagerProxy" ></asp:ScriptManagerProxy>
    <script type="text/javascript">
        SW = SW || {};
        SW.Orion = SW.Orion || {};
        SW.Orion.Discovery = SW.Orion.Discovery || {};
        SW.Orion.Discovery.Controls = SW.Orion.Discovery.Controls || {};
        SW.Orion.Discovery.Controls.SubnetsEditor = SW.Orion.Discovery.Controls.SubnetsEditor || {};
        SW.Orion.Discovery.Controls.SubnetsEditor.ERROR_MESSAGE = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_19) %>";
        SW.Orion.Discovery.Controls.SubnetsEditor.SUBNET_CLASS_ERROR_MESSAGE = "<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_100) %>";
        SW.Orion.Discovery.Controls.SubnetsEditor.Controls = {
            GRID : "<%= this.GridView.ClientID %>"
        };

        $(document).ready(function () {
            $("#<%=AddItemMenu.ClientID %>").removeClass('hidden');
        });

    </script>

    <h1 class="editor-label"><asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_AK0_26%>"></asp:Literal></h1>
    <div id="subnetEditorContent"  class="editor-content">
        <asp:UpdatePanel ID="SubnetsUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <orion:TBox runat="server" ID="ModalityDiv" />
                <orion:AddSubnetsDialog runat="server" ID="AddSubnetsDialog" OnDialogClosed="AddSubnetsDialog_DialogClosed"  OnAddSubnetsButtonClick="AddSubnetsDialog_AddSubnetsButtonClick" />

                <asp:GridView ID="GridView" runat="server" CssClass="subnets-grid">
                    <Columns>
                        <asp:TemplateField >
                            <HeaderTemplate>
                                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_3%>" />
                                <a href="#" class="info-tooltip">
                                    <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
                                    <span>
                                        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_6%>"></asp:Literal>
                                    </span>
                                </a>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="InputTextBox" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(DataBinder.Eval(Container.DataItem, "SubnetCIDR")) %>' 
                                    ValidationGroup="Subnets" CausesValidation="true" OnTextChanged="InputTextBox_TextChanged"></asp:TextBox>
                                <asp:CustomValidator ID="CIDRFormatValidator" runat="server"
                                    ControlToValidate="InputTextBox"
                                    ValidateEmptyText="true"
                                    OnServerValidate="CIDRFormatValidator_ServerValidate"
                                    Display="Dynamic"
                                    ValidationGroup="Subnets"
                                    EnableClientScript="true" ClientValidationFunction="ValidateCIDR">

                                </asp:CustomValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ButtonType="Image" ImageUrl="~/Orion/Discovery/images/TrashBin.png" CommandName="DeleteRow" />

                    </Columns>

                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="GridView" EventName="RowCommand" />
                <asp:AsyncPostBackTrigger ControlID="AddItemMenu" />
            </Triggers>
        </asp:UpdatePanel>


    <asp:Menu ID="AddItemMenu" runat="server" Target="_blank" CssClass="menu hidden" Orientation="Horizontal" OnMenuItemClick="AddItemMenu_MenuItemClick" automation="Subnets_Editor_AddItemMenu">
        <StaticItemTemplate>
            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Text")) %>' automation='<%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>'></asp:Label>
            <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/DownArrow.png" />
        </StaticItemTemplate>
        <DynamicItemTemplate>
            <asp:Label runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Text")) %>' automation='<%# DefaultSanitizer.SanitizeHtml(Eval("Value")) %>'></asp:Label>
        </DynamicItemTemplate>
        <Items>
            <asp:MenuItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_8%>" Selectable="false" >
                <asp:MenuItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_9%>" Value="Subnet" ></asp:MenuItem>
                <asp:MenuItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_10%>" Value="SeedRouter" ></asp:MenuItem>
            </asp:MenuItem>

        </Items>
    </asp:Menu>
    </div>


</div>
