using System;
using System.Web.UI.WebControls;


public partial class Orion_Discovery_Wizard_Controls_Progress : System.Web.UI.UserControl
{
    private int progressValue;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        UpdateUi();
    }

    /// <summary> Actual progress value. From 0 to 100. </summary>
    public int ProgressValue { 
        get 
        {
            return progressValue;
        } 
        
        set
        {
            if (value > 100) value = 100;
            if (value < 0) value = 0;
            this.progressValue = value;
            this.UpdateUi();
        }
    }

    /// <summary> Updates UI controls according current progress status </summary>
    private void UpdateUi() 
    {
        ProgressImageFull.Width = new Unit(this.ProgressValue, UnitType.Percentage);
        ProgressImageEmpty.Width = new Unit(Math.Max(0, 98 - this.ProgressValue), UnitType.Percentage);
    }
}
