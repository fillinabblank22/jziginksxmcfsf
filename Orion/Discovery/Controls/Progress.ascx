<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Progress.ascx.cs" Inherits="Orion_Discovery_Wizard_Controls_Progress" %>

<asp:Panel ID="MainPanel" runat="server" Height="20" Width="100%">
    <asp:Image 
        ID="ProgressImageFull" 
        ImageUrl="~/Orion/Discovery/images/progBar_on.gif" 
        Height="100%" 
        runat="server"/><asp:Image 
        ID="ProgressImageEmpty" 
        ImageUrl="~/Orion/Discovery/images/progBar_off.gif" 
        Height="100%" 
        runat="server"/>
</asp:Panel>