<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveDirectoryOrganizationalUnits.ascx.cs" Inherits="Orion_Discovery_Controls_ActiveDirectoryOrganizationalUnits" %>

<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<div class="editor-dialog-section">
    <div class="description"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_64_OUStep) %></div>
    
    <span id="sw-ou-tree-show-help-link-wrapper" style="display: none;">
        <span class="sw-ou-tree-show-help-link-icon"></span>
        <a id="sw-ou-tree-help-link-open" href="#">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_Ou_Help_Header) %>
        </a>
    </span>
    
    <span id="sw-ou-tree-help-wrapper" class="sw-suggestion">
        <span class="sw-suggestion-icon"></span>
        
        <span class="sw-ou-tree-help sw-ou-tree-help-header">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_Ou_Help_Header) %>
        </span>
        
        <a id="sw-ou-tree-help-link-close" href="#"></a>
        
        <span class="sw-ou-tree-help">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_Ou_Help_Paragraph1) %>
        </span>
        
        <span class="sw-ou-tree-help">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectory_Ou_Help_Paragraph2) %>
            <orion:HelpLink ID="ManualImportHelpLink" 
                runat="server" 
                HelpUrlFragment="OrionCoreSonarDiscADOULM" 
                HelpDescription="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_ActiveDirectory_Ou_Help_LearnMore %>" 
                CssClass="sw-ou-tree-help-learn-more-link" />
        </span>
    </span>

    <div id="sw-ou-treeWrapper">
        <div id="sw-ou-treePanel">
            <div class="sw-ou-treeheader">
                <div id="sw-ou-tree-header-counts">
                    <span id="sw-ou-tree-header-computers">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_NbrOfComputersColumn) %>
                    </span>
                    <span id="sw-ou-tree-header-servers">
                        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_NbrOfServersColumn) %>
                    </span>
                </div>
                <div id="sw-ou-tree-header-future-child"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_IncludeFutureChildColumn) %></div>
                <div id="sw-ou-tree-header-ou"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_OuColumn) %></div>
            </div>

            <div id="sw-AdOus-loading">
                <div id="sw-ou-tree-panel-loading-spinner">
                    <span class="sw-ou-tree-loading-icon"></span>
                    <span class="sw-ou-tree-loading-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_LoadingOUs) %></span>
                </div>
            </div>
            <div id="sw-AdOus"></div>
        </div>
    </div>
    <div id="sw-ou-tree-footer">
        <div id="sw-ou-nbrOfSelectedOUs"></div>
        <div id="countOfDevicesWrappper">
            <span id="DeviceCount"></span>
            <span id="DeviceCountWarning" style="display: none;">
                <img src="/Orion/images/warning_16x16.gif" style="vertical-align: sub;" />
            </span>
        </div> 
        <div id="sw-ou-tree-nbr-loading-spinner">
            <span class="sw-ou-tree-loading-icon"></span>
            <span class="sw-ou-tree-loading-text"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_LoadingCounts) %></span>
        </div>
        <div style="clear: both"></div>
    </div>
    
    <div id="sw-ou-options">
        <div class="uppercase"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_OUPicker_Options) %></div>
        <label class="option"><input type="checkbox" id='ServersOnly' checked/><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_OUPickers_ServersOnly) %></label>
    </div>
    <div>
        <img id="saveSpinner" src="../images/spinner.gif" class="spinner hidden" />
        <div id="saveFailed" class="error hidden"></div>
    </div>
</div>

<script>
    $(function () {
        var showHint = <%= this.ShowIncludeFutureOuHint ? "true" : "false" %>;
        var webService = "/Orion/Services/DiscoveryWizard.asmx";
        var webMethod = "ToggleDiscoveryHint";
        var settingName = "<%= this.ShowIncludeFutureOuHintSettingName %>";

        if (showHint) {
            ShowOusHint();
        } else {
            HideOusHint();
        }

        $("#sw-ou-tree-help-link-close").unbind('click').bind('click', function () {
            ToggleOusHint(HideOusHint);
        });

        $("#sw-ou-tree-help-link-open").unbind('click').bind('click', function () {
            ToggleOusHint(ShowOusHint);
        });

        function HideOusHint() {
            $("#sw-ou-tree-help-wrapper").hide();
            $("#sw-ou-tree-show-help-link-wrapper").addClass("sw-ou-tree-show-help-link-wrapper-display");
        }

        function ShowOusHint() {
            $("#sw-ou-tree-help-wrapper").show();
            $("#sw-ou-tree-show-help-link-wrapper").removeClass("sw-ou-tree-show-help-link-wrapper-display");
        }

        function ToggleOusHint(success, failure) {
            ORION.callWebService(webService, webMethod, { "settingName" : settingName }, success, failure);
        }

        SW.Core.namespace("SW.Orion.Core.Discovery.AD");
        SW.Orion.Core.Discovery.AD.CountOfStationsColumn = (function() {
            function showServersCount() {
                $("#sw-ou-tree-header-computers").hide();
                $("#sw-ou-tree-header-servers").show();

                $(".sw-ou-tree-number-of-servers").show();
                $(".sw-ou-tree-number-of-computers").hide();
            }

            function showComputersCount() {
                $("#sw-ou-tree-header-computers").show();
                $("#sw-ou-tree-header-servers").hide();

                $(".sw-ou-tree-number-of-servers").hide();
                $(".sw-ou-tree-number-of-computers").show();
            }

            function fillEmptyRows() {
                $(".sw-ou-number-of-stations").find("span").filter(function () {
                    return $(this).closest(".sw-ou-number-of-stations").data("nbr-of-computers") < 0;
                }).text("0");
            }

            function setColumn() {
                if ($('#ServersOnly').is(':checked')) {
                    showServersCount();
                } else {
                    showComputersCount();
                }
            }

            $('#ServersOnly').change(function() {
                if (this.checked) {
                    showServersCount();
                } else {
                    showComputersCount();
                }
            });

            return {
                'setColumn': setColumn,
                'fillEmptyRows': fillEmptyRows
            }
        })();

        SW.Orion.Core.Discovery.AD.DeviceCount = (function() {
            function setCount() {
                var computersCount = 0;
                var serversCount = 0;
                var finalCount = 0;
                $('.sw-ou-checked').closest(".sw-ou-tree-row-container").find(".sw-ou-number-of-stations").each(function() {
                    if ($(this).data("nbr-of-computers") > 0) {
                        computersCount += $(this).data("nbr-of-computers");
                        serversCount += $(this).data("nbr-of-servers");
                    }
                });

                if ($('#ServersOnly').is(':checked')) {
                    if (serversCount == 1) {
                        $('#DeviceCount').html(
                            '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_DeviceCount_Servers_Single) %>');
                    } else {
                        $('#DeviceCount').html(
                            SW.Core.String.Format(
                                '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_DeviceCount_Servers_Multiple) %>', 
                                serversCount));
                    }
                    finalCount = serversCount;
                } else {
                    if (computersCount == 1) {
                        $('#DeviceCount').html(
                            '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_DeviceCount_Computers_Single) %>');
                    } else {
                        $('#DeviceCount').html(
                            SW.Core.String.Format(
                                '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_ActiveDirectoryDialogFrame_OrganizationalUnitsPanel_DeviceCount_Computers_Multiple) %>', 
                                computersCount));
                    }
                    finalCount = computersCount;
                }
                $('#DeviceCount').data('device-count', finalCount);

                if ((finalCount >= SW.ActiveDirectoryEditorDlg.WarningLimit) && (SW.ActiveDirectoryEditorDlg.WarningLimit >= 0)) {
                    var warningSpan = $('#DeviceCountWarning');
                    if (SW.ActiveDirectoryEditorDlg.WarningByLicense)
                        warningSpan.prop('title', '<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceCountWarning_License) %>');
                    else
                        warningSpan.prop('title', SW.Core.String.Format('<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.Discovery_AD_DeviceCountWarning) %>', SW.ActiveDirectoryEditorDlg.WarningLimit));
                    
                    warningSpan.show();
                }
                else
                    $('#DeviceCountWarning').hide();
            }

            $('#ServersOnly').change(function() {
                setCount(); //re-establish the sum of computers/servers
            });

            return {
                'setCount': setCount
            }
        })();

        SW.Orion.Core.Discovery.AD.CountOfStationsColumn.setColumn();
    });
    
</script>



