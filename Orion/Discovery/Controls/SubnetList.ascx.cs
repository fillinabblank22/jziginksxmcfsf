﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Core.Models.Discovery;

public partial class Orion_Discovery_Controls_SubnetList : System.Web.UI.UserControl
{
	private uint lastClassID = 0;

	/// <summary>
	/// collection of entered subnets in session
	/// </summary>
	protected List<Subnet> SubnetList
	{
		get
		{
			return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().SubnetList;
		}
	}


	/// <summary>
	/// collection of all group with their collapse status
	/// </summary>
	protected Dictionary<uint, bool> SubnetClasses
	{
		get 
		{
			Dictionary<uint, bool> sc = this.ViewState["SubnetClasses"] as Dictionary<uint, bool>;
			if (sc == null)
			{
				sc = new Dictionary<uint, bool>();
				this.ViewState["SubnetClasses"] = sc;
			}
			return sc;
		}
	}


	protected void Page_Load(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			this.BindSubnets();
		}
	}

	protected void BindSubnets()
	{
		this.SubnetList.Sort((a, b) => a.SubnetID.CompareTo(b.SubnetID));
		this.SubnetGrid.DataSource = this.SubnetList;
		this.SubnetGrid.DataBind();
	}

	protected void SubnetGrid_ItemDataBound(object sender, ListViewItemEventArgs e)
	{
		if(e.Item.ItemType == ListViewItemType.DataItem)
		{
			Subnet s = ((ListViewDataItem)e.Item).DataItem as Subnet;
			if(s != null)
			{
				if(s.SubnetClassID != this.lastClassID)
				{
					e.Item.FindControl("groupHeader").Visible = true;
					((Label)e.Item.FindControl("groupLabel")).Text = s.SubnetClass;
					this.lastClassID = s.SubnetClassID;
					ImageButton collapseButton = e.Item.FindControl("CollapseGroupButton") as ImageButton;
					collapseButton.CommandArgument = s.SubnetClassID.ToString();
					var selectButton = e.Item.FindControl("SelectGroupButton") as LocalizableButton;
					selectButton.CommandArgument = s.SubnetClassID.ToString();
					
					if (!this.SubnetClasses.ContainsKey(s.SubnetClassID))
					{
						this.SubnetClasses[s.SubnetClassID] = true;
					}
					if (!SubnetClasses[s.SubnetClassID])
						collapseButton.ImageUrl = "/Orion/images/Button.Expand.gif";
					else
						collapseButton.ImageUrl = "/Orion/images/Button.Collapse.gif";
				}
				e.Item.FindControl("DataRow").Visible = this.SubnetClasses[s.SubnetClassID];
			}
		}
	}

	protected void CollapseGroup_Click(object sender, CommandEventArgs e)
	{
			uint classID = Convert.ToUInt32(e.CommandArgument);
			if (this.SubnetClasses.ContainsKey(classID))
			{
				this.SubnetClasses[classID] = !this.SubnetClasses[classID];
				this.BindSubnets();
			}
	}

	protected void SelectGroup_Click(object sender, CommandEventArgs e)
	{
		uint classID = Convert.ToUInt32(e.CommandArgument);
		this.SubnetList.FindAll(s => s.SubnetClassID == classID).ForEach(s => s.Selected = true);
		this.BindSubnets();
	}

	protected void SubnetCheck_Changed(object sender, EventArgs e)
	{
		CustomCheckBox cb = sender as CustomCheckBox;
		if (cb == null)
		{
			throw new ArgumentException("sender");
		}

		Subnet s = this.SubnetList.Find(subnet => subnet.SubnetID == (ulong)cb.CustomValue);
		if (s != null)
		{
			s.Selected = cb.Checked;
		}
	}

	protected void AddSubnetBtn_OnClick(object sender, EventArgs e)
	{
		this.AddSubnetDialog.OpenDialog();
	}

	/// <summary> Called when AddSubnet dialog is closed and its subnet should be added </summary>
	protected void AddSubnetDialog_SubnetAdded(Subnet subnet)
	{
		if (!this.SubnetList.Contains(subnet))
		{
			this.SubnetList.Add(subnet);
		}

		this.BindSubnets();
	}

	protected void AddSeedBtn_OnClick(object sender, EventArgs e)
	{
	    AddSeedRouterDialog.OpenDialog();
	}

    /// <summary> Called when AddSeddRouter dialog is closed and its subnets should be added </summary>
    protected void AddSeedRouterDialog_SeedRouterAdded(List<Subnet> routerSubnets)
    {   
        foreach (Subnet subnet in routerSubnets)
        {
            if (!this.SubnetList.Contains(subnet))
            {
                subnet.Selected = false;
				this.SubnetList.Add(subnet);
            }
        }
        
        this.BindSubnets();
    }

    /// <summary> Checks if there are any subnets entered - only the checked ones </summary>
	/// <returns> true if there is at least one checked subnet </returns>
    public bool CheckEmpty()
    {
		this.SubnetGridRequiredValidator.IsValid = this.SubnetList.Exists(s => s.Selected);
		return this.SubnetGridRequiredValidator.IsValid;
    }
}
