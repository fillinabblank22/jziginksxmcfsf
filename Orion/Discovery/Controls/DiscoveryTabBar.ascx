<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryTabBar.ascx.cs" Inherits="Orion_Discovery_Controls_DiscoveryTabBar" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register Src="~/Orion/Controls/NavigationTabBar.ascx" TagPrefix="orion" TagName="NavigationTabBar" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" Assembly="OrionWeb" %>

<orion:NavigationTabBar ID="IgnoreListNavigationTabBar" runat="server" Visible="true">   
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_31 %>" Url="~/Orion/Discovery/Default.aspx" />
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_1 %>" Url="~/Orion/Discovery/Results/ScheduledDiscoveryResults.aspx" />
    <orion:NavigationTabItem Name="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_32 %>" Url="~/Orion/Discovery/IgnoreList.aspx" />
</orion:NavigationTabBar>