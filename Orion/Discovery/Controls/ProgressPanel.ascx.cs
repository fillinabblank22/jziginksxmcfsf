using System;
using System.Web.UI.WebControls;


public partial class Orion_Discovery_Wizard_Controls_ProgressPanel : System.Web.UI.UserControl
{
    /// <summary> </summary>
    public void SetNewStatus(int progress)
    {
        this.Progress.ProgressValue = progress;
    }

}
