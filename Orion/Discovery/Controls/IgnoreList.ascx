﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IgnoreList.ascx.cs" Inherits="Orion_Discovery_Controls_IgnoreList" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Discovery" Assembly="OrionWeb" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register TagPrefix="controls" TagName="IgnoreSubTree" Src="~/Orion/Discovery/Controls/IgnoreSubTree.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
        $(".CheckAllSelector input[type=checkbox]").click(function() {
            var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function() {
                this.checked = checked_status;
            });
            if (this.checked) {
                HideWarning();
            }
            DisplaySelectPan(checked_status);                   
        });
        $(".Selector input[type=checkbox]").change(function() {            
            if (this.checked) {
                HideWarning();
            }
            else{
                DisplaySelectPan(this.checked);
            }
        });        
    });

    function DisplayWarning() {
        if ($(".warning") != null) {
            $(".warning").css("display", "inline");
        }
        return true;
    }

    function HideWarning() {
        if($(".warning")!=null)
        {
            $(".warning").css("display", "none");
        }
        return true;
    }    

     function DisplaySelectPan(checked) {
        //alert(checked);
        $("#selectAllPan").empty();        
        if (checked && <%=(ScheduleDiscoveryHelper.IgnoredNodes.Count>DisplayedNodes) ? 1 : 0 %>) {
            $("#selectAllPan")
                .append("<span><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_6) %> ("+<%=DisplayedNodes%>+") </span><a href='#'><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_7) %> ("+<%=ScheduleDiscoveryHelper.IgnoredNodes.Count%> +")</a>")
                .removeClass()
                .addClass("select-whole-page");
            $("#selectAllPan a").click(function() {
                $("#selectAllPan").empty();
                $("#selectAllPan").append("<span><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_8) %></span>").addClass("select-all-objects");
                $("#selectAllPan a ").click(function() {

                });
                $("#<%=AllNodesSelected.ClientID%>").val("True");
            });
        }
        else {            
            $("#<%=AllNodesSelected.ClientID%>").val("False");
        }
    };    
    //]]>
</script>
<orion:Include runat="server" File="Discovery/js/IgnoreSubTree.js" Section="Bottom" />

<div>
    <asp:HiddenField ID="AllNodesSelected" runat="server" Value="False" />
    <div id="selectAllPan">
    </div>
    <orion:IgnoreListGridView ID="IgnoreNodesTableGrid" runat="server" AutoGenerateColumns="False"
        GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"
        AllowPaging="true" AllowSorting="true" OnRowDataBound="IgnoreListGridView_RowDataBound" OnDataBound="IgnoreListGridView_DataBound"
        OnDataBinding="IgnoreListGridView_DataBinding" Width="100%">
        <PagerSettings Position="TopAndBottom" />
        <PagerTemplate>
            <table width="100%" style="background-image: url(/Orion/Nodes/images/pager/bg.gif);
                background-repeat: repeat-x;">
                <tr>
                    <td>
                        <asp:ImageButton runat="server" ID="FirstButton" ImageUrl="~/Orion/Nodes/images/pager/page-first.gif"
                            OnClick="Pager_Click" CommandArgument="first" />
                        <asp:ImageButton runat="server" ID="PreviousButton" ImageUrl="~/Orion/Nodes/images/pager/page-prev.gif"
                            OnClick="Pager_Click" CommandArgument="prev" />
                        <asp:Image ID="SeparatorImage" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
                    </td>
                    <td>
                        <asp:Label ID="MessageLabel" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_158 %>" runat="server" />
                        <asp:DropDownList ID="PageDropDownList" AutoPostBack="true" OnSelectedIndexChanged="PageDropDownList_SelectedIndexChanged"
                            Width="40px" runat="server" />
                        <asp:Label ID="PageCountLabel" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
                        <asp:ImageButton runat="server" ID="NextButton" ImageUrl="~/Orion/Nodes/images/pager/page-next.gif"
                            OnClick="Pager_Click" CommandArgument="next" />
                        <asp:ImageButton runat="server" ID="LastButton" ImageUrl="~/Orion/Nodes/images/pager/page-last.gif"
                            OnClick="Pager_Click" CommandArgument="last" />
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
                    </td>
                    <td>
                        <asp:Label ID="pageSizeLabel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_156 %>"></asp:Label>
                        <asp:TextBox ID="pageSizeTextBox" runat="server" OnTextChanged="pageSizeTextBox_OnChange"
                            AutoPostBack="true" CssClass="pagerSize">
                        </asp:TextBox>
                    </td>
                    <td style="width: 70%; text-align: right">
                        <asp:Label ID="CurrentPageLabel" runat="server" />
                    </td>
                </tr>
            </table>
        </PagerTemplate>
        <EmptyDataTemplate>
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_157) %>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField ItemStyle-Width="20px">
                <HeaderTemplate>
                    <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged"
                        OnPreRender="CheckAllSelector_OnPreRender" />
                </HeaderTemplate>
                <ItemTemplate>
                    <orion:CustomCheckBox ID="CustomSelector" runat="server" Checked='<%# Eval("DiscoveryIgnoredNode.IsSelected") %>'
                        CssClass="Selector" CustomValue='<%# DefaultSanitizer.SanitizeHtml(GetKey(Container.DataItem)) %>' Visible='<%# (bool)Eval("DiscoveryIgnoredNode.IsIgnored") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_155 %>">
                <ItemTemplate>
                    <%# DefaultSanitizer.SanitizeHtml(Eval("DiscoveryIgnoredNode.Caption")) %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="50%">
                <HeaderTemplate>
                    <span style="padding-left: 20px">
                        <span runat="server" id="lblInterfaces" visible='<%# isNPMInstalled %>'> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_14) %>, </span>
                        <%# DefaultSanitizer.SanitizeHtml(GetChildObjectsColumnName()) %>
                    </span>
                </HeaderTemplate>
                <ItemTemplate>
                    <controls:IgnoreSubTree ID="SubTree" runat="server" DiscoveryIgnoredNodeID='<%# Eval("DiscoveryIgnoredNode.ID") %>'
                        IsExpanded='<%# Eval("IsExpanded") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_154 %>">
                <ItemTemplate>
                    <%# DefaultSanitizer.SanitizeHtml(Eval("DiscoveryIgnoredNode.IPAddress")) %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EngineName" HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_152 %>" SortExpression="EngineName"/>            
            <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_153 %>">
                <ItemTemplate>
                    <%# DefaultSanitizer.SanitizeHtml(string.Format("{0:F}", Eval("DiscoveryIgnoredNode.DateAdded"))) %>
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>       
    </orion:IgnoreListGridView>    
</div>
