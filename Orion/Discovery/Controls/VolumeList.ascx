<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VolumeList.ascx.cs" Inherits="Orion_Discovery_Wizard_Controls_VolumeList" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Discovery" Assembly="OrionWeb" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
    $(".CheckAllSelector input[type=checkbox]").click(function() {
        var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function() {
                this.checked = checked_status;
            });
        });
    });
    //]]>
</script>


<div>
    <orion:VolumeGridView ID="VolumeTableGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"  AllowSorting="true" >
        <Columns>
            <asp:TemplateField ItemStyle-Width="20px">
                <HeaderTemplate>
                    <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" OnCheckedChanged="CheckAllSelector_OnCheckedChanged" OnPreRender="CheckAllSelector_OnPreRender" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="Selector" runat="server" Checked='<%# Eval("IsSelected") %>' CssClass="Selector" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Count" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_39 %>" ItemStyle-Width="50px" SortExpression="Count" />
            <asp:ImageField DataImageUrlField="Icon" ItemStyle-Width="16px"></asp:ImageField>
            <asp:BoundField DataField="Description" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_41 %>" SortExpression="Description" />
        </Columns>
    </orion:VolumeGridView>
</div>

<script type="text/javascript">
    var checkCount = function (countAll) {
        var countSelectedDevices = 0;
        $(".Selector input[type=checkbox]").each(function () {
            if (this.checked == true || countAll) {
                var row = $(this).parent('span').parent('td').parent('tr');
                var count = parseInt(row.children("td:eq(1)").html());
                countSelectedDevices = countSelectedDevices + count;
            }
        });
        return countSelectedDevices;
    };

    $(document).ready(function () {
        var result = checkCount(false);
        SW.Core.LicenseLimitNotification.UpdateLicenseLimitNotification(result);
    });

    $(".Selector input[type=checkbox]").change(function () {
        var result = checkCount(false);
        SW.Core.LicenseLimitNotification.UpdateLicenseLimitNotification(result);
    });

    $(".CheckAllSelector input[type=checkbox]").change(function () {
        if (this.checked == false) {
            SW.Core.LicenseLimitNotification.UpdateLicenseLimitNotification(0);
        } else {
            var result = checkCount(true);
            SW.Core.LicenseLimitNotification.UpdateLicenseLimitNotification(result);
        }
    });

</script>


