using System;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Text;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Discovery;

/// <summary> UI component for bulk uload of IP addresses and host names </summary>
public partial class Orion_Discovery_Wizard_Controls_BulkUpload : UserControl
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public const int BULK_VALIDATION_BATCH = 5;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) 
        {
            FillUiControls();
        }

    }

    /// <summary> Creates data object with host file content </summary>
    private void FillUiControls()
    {
        StringBuilder sb = new StringBuilder();
        foreach (string line in ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList)
        {
            sb.AppendLine(line);
        }

        this.HostsTextBox.Text = sb.ToString();
    }

    /// <summary> Copy values from UI textbox for bulk list to DataObject </summary>
    /// <returns> Error message if any. </returns>
    public bool FillBulkUploadList()
    {
        bool result = FillBulkUploadListFromContent(this.HostsTextBox.Text);
        FillUiControls();
        return result;
    }
    
    /// <summary> Copy values from UI textbox for bulk list to DataObject </summary>
    public static bool FillBulkUploadListFromContent(string bulkText)
    {
        List<string> bulkList = ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList;
        bulkList.Clear();
        if (string.IsNullOrEmpty(bulkText)) 
            return false;

        string[] lines = bulkText.Split('\n');
        foreach(string line in lines)
        {
            AddBulkLine(bulkList, line);
        }

        return bulkList.Count > 0;
    }

    private static void AddBulkLine(List<string> bulkList, string line)
    {
        StringBuilder lineBuilder =  new StringBuilder(line.Trim());
        if (lineBuilder.Length == 0) return;
        if (lineBuilder[lineBuilder.Length - 1] == '\r')
        {
            lineBuilder.Remove(line.Length - 1, 1);
        }

        string normalizedLine = lineBuilder.ToString();

        if (!bulkList.Exists(l => l.Equals(normalizedLine, StringComparison.InvariantCultureIgnoreCase)))
        {
            bulkList.Add(normalizedLine);
        }
    }

    /// <summary> </summary>
    protected static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    /// <summary> Validate part of addresses specified in bulklist. 
    /// First validated record has index specified by startLine parameter. 
    /// Number of validated records is specified by <see cref="BULK_VALIDATION_BATCH"/> constant. </summary>
    public static string ValidateUserInput(int startLine)
    {
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        string message = null;

        if ((ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList.Count == 0))
        {
            message = Resources.CoreWebContent.WEBCODE_AK0_27;
        }
        else
        {
            try
            {
                using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, ConfigWorkflowHelper.DiscoveryConfigurationInfo.EngineID))
                {
                    List<string> subBulkList = new List<string>(ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration<CoreDiscoveryPluginConfiguration>().BulkList);
                    if (startLine > subBulkList.Count)
                    {
                        return null;
                    }
            
                    subBulkList.RemoveRange(0, startLine);
                    if (subBulkList.Count > BULK_VALIDATION_BATCH)
                    {
                        subBulkList.RemoveRange(BULK_VALIDATION_BATCH, subBulkList.Count - BULK_VALIDATION_BATCH);
                    }

                    message =
                        businessProxy.ValidateBulkList(subBulkList);
                }
            }
            catch (Exception ex)
            {
                message = Resources.CoreWebContent.WEBCODE_AK0_28;
                log.Error("Cannot validate bulk list", ex);
            }
        }

        return message;
    }

    
}
