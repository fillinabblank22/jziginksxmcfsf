﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SWUI = SolarWinds.Orion.Web.UI;

public partial class Orion_Discovery_Controls_NetworkDiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected int NodeCount { get; set; }
    protected int VolumeCount { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            using (WebDAL webDAL = new WebDAL())
            {
                NodeCount = webDAL.GetNodeCount();                
                VolumeCount = webDAL.GetVolumeCount();
            }
        }
        catch (Exception ex) {
            _logger.ErrorFormat("Cannot get number of managed objects. Details: {0}",ex);
        }

        DataBind();
    }

    protected void Button_Click(object sender, EventArgs e)
    {
        LocalizableButton localizableButton = sender as LocalizableButton;
        
        DiscoveryCentralHelper.Leave();

        Response.Redirect(localizableButton.CommandArgument);
    }


    protected SWUI.ButtonType GetDiscoveryButtonString(int nodeCount)
    {
        if (nodeCount > 0)
        {
            return SWUI.ButtonType.Secondary;
        }
        return SWUI.ButtonType.Primary;
    }
}