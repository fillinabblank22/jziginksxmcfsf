﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScheduleDiscoveryResultsList.ascx.cs" Inherits="Orion_Discovery_Controls_ScheduleDiscoveryResultsList" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Discovery" Assembly="OrionWeb" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Register TagPrefix="controls" TagName="ResultsSubTree" Src="~/Orion/Discovery/Controls/ResultsSubTree.ascx" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {
        $(".CheckAllSelector input[type=checkbox]").click(function() {
            var checked_status = this.checked;
            $(".Selector input[type=checkbox]").each(function() {
                this.checked = checked_status;
                var scope =$(this).parents("tr:first");
                if(this.checked)
                {                                  
                       $(".NetObjectSelector",scope).each(function() {                
                        $(this).attr("disabled","true");                      
                        });
                }
                else
                {
                    $(".NetObjectSelector",scope).each(function() {                
                        $(this).removeAttr("disabled");                      
                    });
                }   
            });
            DisplaySelectPan(checked_status);
        });
        $(".Selector input[type=checkbox]").click(function() {             
            var scope =$(this).parents("tr:first");
            if(this.checked)
            {                                  
                   $(".NetObjectSelector",scope).each(function() {                
                     $(this).attr("disabled","true");                      
                   });
            }
            else
            {
                $(".NetObjectSelector",scope).each(function() {                
                     $(this).removeAttr("disabled");                      
                   });
                  
                DisplaySelectPan(this.checked);
            }
        });
        $(".pagerSize").keypress(function(e){
             if (e.which == 13) {
                 if ($(this).val() < 1)
                    {
                        $(this).val(1);
                    }
                 else 
                    if ($(this).val() > 500) 
                    {
                        $(this).val(500);
                    }
             }
             else {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            }
        });
        $(".pagerSize").change(function() {            
            if($(this).val()<1)
                $(this).val(1);
            else if($(this).val()>500)
                    $(this).val(500);
        });                
    });

    function DisplaySelectPan(checked) {
        //alert(checked);
        $("#selectAllPan").empty();        
        if (checked && <%=(NrOfRecords>DisplayedNodes)? 1 : 0 %>) {
            $("#selectAllPan")
                .append("<span><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_6) %> ("+<%=DisplayedNodes%>+") </span><a href='#'><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_7) %> ("+<%=NrOfRecords%> +")</a>")
                .removeClass()
                .addClass("select-whole-page");
            $("#selectAllPan a").click(function() {
                $("#selectAllPan").empty();
                $("#selectAllPan").append("<span><%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_8) %></span>").addClass("select-all-objects");
                $("#selectAllPan a ").click(function() {

                });
                $("#<%=AllNodesSelected.ClientID%>").val("True");
            });
        }
        else {
            $("#<%=AllNodesSelected.ClientID%>").val("False");
        }
    };    
    //]]>
</script>
<orion:Include runat="server" File="Discovery/js/ResultsSubTree.js" Section="Bottom" />

<asp:HiddenField ID="AllNodesSelected" runat="server" Value="False" />
<div id="selectAllPan">
</div>
<orion:ScheduleDiscoveryResultsGridView ID="ScheduleDiscoveryResultsTableGrid" runat="server"
    AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow"
    AllowSorting="true" EnableViewState="true" AllowPaging="true" OnRowDataBound="ResultsGridView_RowDataBound"  OnDataBound="ResultsGridView_DataBound" OnDataBinding="ResultsGridView_DataBinding" Width="100%">    
    <PagerSettings Position="TopAndBottom" />
    <PagerTemplate>
     <table width="100%" style="background-image:url(/Orion/Nodes/images/pager/bg.gif);background-repeat:repeat-x;">                    
            <tr>
              <td>
                <asp:ImageButton runat="server" ID="FirstButton" ImageUrl="~/Orion/Nodes/images/pager/page-first.gif" OnClick="Pager_Click" CommandArgument="first" />
                <asp:ImageButton runat="server" ID="PreviousButton" ImageUrl="~/Orion/Nodes/images/pager/page-prev.gif" OnClick="Pager_Click" CommandArgument="prev" />
                <asp:Image ID="SeparatorImage" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
              </td>                        
              <td>
                
                <asp:label id="MessageLabel"                  
                  text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_9 %>" 
                  runat="server"/>
                <asp:dropdownlist id="PageDropDownList"
                  autopostback="true"
                  onselectedindexchanged="PageDropDownList_SelectedIndexChanged"                  
                  Width="40px"
                  runat="server"/>
                <asp:Label ID="PageCountLabel" runat="server"></asp:Label>
              </td>
              <td>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
                <asp:ImageButton runat="server" ID="NextButton" ImageUrl="~/Orion/Nodes/images/pager/page-next.gif" OnClick="Pager_Click" CommandArgument="next"/>
                <asp:ImageButton runat="server" ID="LastButton" ImageUrl="~/Orion/Nodes/images/pager/page-last.gif" OnClick="Pager_Click" CommandArgument="last"/>                
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Orion/Nodes/images/pager/grid-blue-split.gif" />
              </td>
              <td>
                  <asp:Label ID="pageSizeLabel" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_10 %>"></asp:Label>
                  <asp:TextBox ID="pageSizeTextBox" 
                        runat="server"                         
                        OnTextChanged="pageSizeTextBox_OnChange" 
                        AutoPostBack="true"
                        CssClass="pagerSize">
                        </asp:TextBox>
              </td>     
              <td style="width:70%; text-align:right">

                <asp:label id="CurrentPageLabel"                  
                  runat="server"/>
              </td>              
            </tr>                    
          </table>
    </PagerTemplate>
    <EmptyDataTemplate>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_11) %>
    </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField ItemStyle-Width="20px">
            <HeaderTemplate>
                <asp:CheckBox ID="CheckAllSelector" runat="server" class="CheckAllSelector" />
            </HeaderTemplate>
            <ItemTemplate>                
                <orion:CustomCheckBox ID="CustomSelector" runat="server" Checked='<%# Eval("DiscoveryNode.IsSelected") %>' CssClass="Selector" CustomValue='<%# DefaultSanitizer.SanitizeHtml(GetKey(Container.DataItem)) %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Name" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_12 %>" SortExpression="Name" />
        <asp:BoundField DataField="IPAddress" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_TP0_5 %>" SortExpression="IPAddress" />
        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_14 %>" SortExpression="NodeStatus">
            <ItemTemplate>
                <img alt='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeStatusText")) %>' src='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeStatusIcon")) %>' title='<%# DefaultSanitizer.SanitizeHtml(Eval("NodeStatusText")) %>' />
                <span class="td-text"><%# DefaultSanitizer.SanitizeHtml(Eval("NodeStatusText")) %></span>
            </ItemTemplate>
        </asp:TemplateField>                  
        <asp:TemplateField Headertext="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_15 %>" ItemStyle-Width="30%">
            <ItemTemplate>                            
                <controls:ResultsSubTree runat="server" 
                ID="SubTree" 
                NodeID='<%# Eval("DiscoveryNode.NodeID") %>' 
                ProfileID='<%# Eval("DiscoveryNode.ProfileID") %>'
                IsExpanded='<%# Eval("IsExpanded") %>'
                SelectionEnabled='<%# IsSelectionEnabled(Container.DataItem) %>'                
                ></controls:ResultsSubTree>
            </ItemTemplate>
        </asp:TemplateField>        
        <asp:ImageField DataImageUrlField="MachineIcon" ItemStyle-Width="16px">
        </asp:ImageField>
        <asp:BoundField DataField="MachineType" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_16 %>" SortExpression="MachineType" ItemStyle-CssClass="wrapCell" ItemStyle-Width="30%" />        
        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_17 %>" SortExpression="DateFound">
            <ItemTemplate>
                <%# DefaultSanitizer.SanitizeHtml(GetDateFound((DateTime)Eval("DateFound"))) %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="DiscoveredBy" HeaderText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_18 %>" SortExpression="DiscoveredBy" ItemStyle-CssClass="wrapCell" ItemStyle-Width="20%" />
    </Columns>
</orion:ScheduleDiscoveryResultsGridView>
<asp:Label ID="Warning" runat="server" 
    Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_19 %>"
    Visible="false"
    ForeColor="Red">
</asp:Label>
