<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceTree.ascx.cs"
    Inherits="Orion_Discovery_Controls_ResurceTree" %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="Discovery/Controls/ResourceTree.js" />
<style type="text/css">
    .no-icon
    {
        display: none;
        background-image: url('/Orion/js/extjs/resources/images/default/s.gif') !important;
    }
    
    .x-tree-selected
    {
        background-color: transparent;
    }
</style>
<asp:Label ID="ErrorLabel" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Medium"
    Visible="true" />

<asp:Panel ID="ResourceTreePanel" runat="server">
    
     <div class="sw-suggestion sw-suggestion-info" style="margin: 0px 10px; display: none;" id="cacheTooltip">
        <span class="sw-suggestion-icon"></span>
        <span class="text"></span>
         <button onclick="return SW.Core.ResourceTree.ForceRefresh()"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VT0_11) %></button>
    </div>



    <script type="text/javascript">
       <%
           SolarWinds.Orion.Web.UI.SWTreeList treeList = new SolarWinds.Orion.Web.UI.SWTreeList();
           treeList.Page = new Page();
           Response.Write(treeList.GetJavaScript());
        %>
    </script>
    <div id="WaitingImage">
        <img src="/Orion/images/AJAX-Loader.gif" />
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_11) %></div>
    <br />
    <div id="progressDiv" ></div>
    <div id="TreeToolbarPlaceholder" style="width: 100%; border-width: 0; margin-bottom: 3px;"></div>
    <div id="TreePlaceholder" style="width: 100%; border-width: 0; padding-top: 1px;">
    </div>
</asp:Panel>

<script type="text/javascript">
    <% if (!this.ErrorOccured)
       { 
           if (!string.IsNullOrEmpty(this.OnClientBeforeResultReceived))
           { %>
              SW.Core.ResourceTree.SetBeforeResultReceivedCallback(<%= this.OnClientBeforeResultReceived %>);
          <% }

          if (!string.IsNullOrEmpty(this.OnClientAfterResultReceived))
          { %>
              SW.Core.ResourceTree.SetAfterResultReceivedCallback(<%= this.OnClientAfterResultReceived %>);
        <%} %>
    
        SW.Core.ResourceTree.init('<%= this.InfoId %>', '<%= this.ReturnURL %>', '<%= Server.HtmlEncode(this.NodeName) %>');
   <% } %>

</script>
