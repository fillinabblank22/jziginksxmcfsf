<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetworkDiscoveryPlugin.ascx.cs" Inherits="Orion_Discovery_Controls_NetworkDiscoveryPlugin" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl= "~/Orion/images/network.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_7) %></h2>
    <div>
        <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_164) %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHDiscoveryHome"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_161 %>" CssClass="helpLink" />
        
    <orion:ManagedNetObjectsInfo ID="nodesInfo" runat="server" EntityName="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_162 %>" NumberOfElements="<%# DefaultSanitizer.SanitizeHtml(NodeCount) %>" />    
    
    <orion:ManagedNetObjectsInfo ID="volumesInfo" runat="server" EntityName="<%$ HtmlEncodedResources: CoreWebContent,WEBCODE_AK0_15 %>"
        NumberOfElements="<%# VolumeCount %>" />

    <div class="sw-btn-bar sw-btn-bar-flush">
        <orion:LocalizableButton id="discoverNetworkButton" runat="server" DisplayType="<%# DefaultSanitizer.SanitizeHtml(GetDiscoveryButtonString(NodeCount)) %>" 
                                 LocalizedText="DiscoverNodes" OnClick="Button_Click" CommandArgument="~/Orion/Discovery/Default.aspx"/>
        <orion:LocalizableButton id="addNodeButton" runat="server" DisplayType="Secondary" LocalizedText="AddDevice"  
                                 OnClick="Button_Click" CommandArgument="~/Orion/Nodes/Add/Default.aspx"/>
    </div>
</div>
