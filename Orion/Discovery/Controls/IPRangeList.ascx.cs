﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Common.Net;
using IPAddressRange=SolarWinds.Orion.Core.Models.Discovery.IPAddressRange;
using SolarWinds.Orion.Core.Models.Discovery;


public partial class Orion_Discovery_Controls_IPRangeList : UserControl
{
    protected List<IPAddressRange> AddressRange
{
    get
    {
        return ConfigWorkflowHelper.DiscoveryConfigurationInfo.GetDiscoveryPluginConfiguration
            <CoreDiscoveryPluginConfiguration>().AddressRange;
    }
}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!this.IsPostBack)
		{
			BindData(); 
		}
		Update();
	}

	protected void IPRangeGrid_OnDataBound(object sender, EventArgs e)
	{
		IPRangeGrid.Columns[2].Visible = (AddressRange.Count > 1);
	}

	private void BindData()
	{	    
		IPRangeGrid.DataSource = AddressRange;
        if (AddressRange.Count == 0)
		{
			AddressRange.Add(
                new IPAddressRange { RangeBegin = "", RangeEnd = "" });
		}
		IPRangeGrid.DataBind();
	}

	/// <summary> Stores selected nodes to Session property </summary>
	private void Update()
	{		
		for (int i = 0; i < IPRangeGrid.Rows.Count; i++)
		{
			if (i >= AddressRange.Count) break;
			Control item = IPRangeGrid.Rows[i];
			TextBox beginIPTextBox = (TextBox)item.FindControl("BeginIPTextBox");
			TextBox endIPTextBox = (TextBox)item.FindControl("EndIPTextBox");

			AddressRange[i].RangeBegin = HostHelper.NormalizeIPAddress(beginIPTextBox.Text);
			AddressRange[i].RangeEnd = HostHelper.NormalizeIPAddress(endIPTextBox.Text);
		}
	}

    private bool ValidateDuplicity(int rowIndex)
    {        
        IPAddressRange range = AddressRange[rowIndex];

        string beginIP = range.RangeBegin.Trim();
        string endIP = range.RangeEnd.Trim();

        foreach (IPAddressRange r in AddressRange)
        {
            if (!r.Equals(range))
            {
                if (r.RangeBegin.Trim() == beginIP && r.RangeEnd.Trim() == endIP)
                {
                    return false;
                }
            }
        }

        return true;
    }

	/// <summary> Called when Delete or Add IP Address Range button is clicked </summary>
	protected void IPRangeGrid_RowCommand(Object sender, GridViewCommandEventArgs e)
	{
		if (e.CommandName == "DeleteRow")
		{
			int index = Convert.ToInt32(e.CommandArgument);
			if (index < AddressRange.Count)
			{
				AddressRange.RemoveAt(index);
			}
			BindData();
		}

	}

	/// <summary> </summary>
	protected void AndButton_Clicked(object sender, EventArgs e)
	{
        AddressRange.Add(new IPAddressRange());
		BindData();
	}

	/// <summary> </summary>
	protected void CompareIPValidation(object source, ServerValidateEventArgs args)
	{
		CustomValidator cv = (CustomValidator)source;
		int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
		cv.ErrorMessage = String.Empty;
		IPAddressRange range = AddressRange[rowIndex];

		if (String.IsNullOrEmpty(range.RangeBegin) != String.IsNullOrEmpty(range.RangeEnd))
		{
			cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_AK0_29;
		}
		else
		{
			try
			{
				if (HostHelper.ConvertIPAddressToLong(range.RangeBegin) > HostHelper.ConvertIPAddressToLong(range.RangeEnd))
				{
					cv.ErrorMessage = Resources.CoreWebContent.WEBCODE_AK0_30;
				}
			}
			catch (FormatException)
			{
			}
		}
		args.IsValid = String.IsNullOrEmpty(cv.ErrorMessage);
	}

    /// <summary> </summary>
    protected void DuplicityValidation(object source, ServerValidateEventArgs args)
    {
        CustomValidator cv = (CustomValidator)source;
        int rowIndex = ((GridViewRow)cv.NamingContainer).RowIndex;
        args.IsValid = ValidateDuplicity(rowIndex);
    }


	/// <summary> </summary>
	public void DeleteEmptyRows()
	{
		AddressRange.RemoveAll(range => string.IsNullOrEmpty(range.RangeBegin) && string.IsNullOrEmpty(range.RangeEnd));
	}

	public bool CheckEmpty()
	{
		this.EmptyValidator.IsValid = AddressRange.All(range => !String.IsNullOrEmpty(range.RangeBegin) && !String.IsNullOrEmpty(range.RangeEnd));
		return this.EmptyValidator.IsValid;
	}
}
