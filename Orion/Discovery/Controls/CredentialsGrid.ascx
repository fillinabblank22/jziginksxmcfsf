﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialsGrid.ascx.cs" Inherits="Orion_Discovery_Controls_CredentialsGrid" %>
<%@ Reference Control="~/Orion/Controls/TransparentBox.ascx" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>

<orion:TBox runat="server" ID="ModalityDiv" />
<div class="contentBlock">
    <table width="100%">
        <tr class="ButtonHeader">
            <td style="padding-left: 10px;">
                <asp:LinkButton ID="AddLinkButton" runat="server" OnClick="AddBtn_OnClick" CssClass="iconLink"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_71) %></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView Width="100%" ID="CredentialsGrid" runat="server" AutoGenerateColumns="False"
                    GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_86 %>">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_85 %>">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#HttpUtility.HtmlEncode(Eval("Name"))%>'
                                    Width="378" />
                            </ItemTemplate>
                        </asp:TemplateField>                       
                        <asp:TemplateField HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_83 %>">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_82 %>"
                                    ImageUrl="~/Orion/Discovery/images/icon_up.gif" OnCommand="ActionItemCommand"
                                    CommandName="MoveUp" CommandArgument="<%# Container.DataItemIndex %>" />
                                <asp:ImageButton ID="MoveDownBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_81 %>"
                                    ImageUrl="~/Orion/Discovery/images/icon_down.gif" OnCommand="ActionItemCommand"
                                    CommandName="MoveDown" CommandArgument="<%# Container.DataItemIndex %>" />                                
                                <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_79 %>"
                                    ImageUrl="~/Orion/Discovery/images/icon_delete.gif" OnCommand="ActionItemCommand"
                                    CommandName="DeleteItem" CommandArgument="<%# Container.DataItemIndex %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                      <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_512) %>
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</div>
