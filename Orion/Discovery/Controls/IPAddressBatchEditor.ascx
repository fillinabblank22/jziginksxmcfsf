<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressBatchEditor.ascx.cs" Inherits="Orion_Discovery_Controls_IPAddressBatchEditor" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>


<script type="text/javascript">
    (function (root) {
        var hasUnresolvedNodes = false;
        $("#<%=HostsTextBoxValidator.ClientID %>").hide();
        $("#ValidationWarning").hide();

        //<![CDATA[
        var StartClientValidation = function (sender, args) {
            args.IsValid = false;
            StartValidation();
        };

        root.StartValidation = function () {
            hasUnresolvedNodes = false;
            var addressesTextBox = $("#<%= IPAddressListTextBox.ClientID %>");
            var validator = $("#<%=HostsTextBoxValidator.ClientID %>");

            validator.text(""); // need to clear text because of display override in stylesheet
            validator.hide();
            addressesTextBox.val(addressesTextBox.val().replace(/ /g, '')); // remove white spaces
            validator.removeClass('success');

            if (!validator.hasClass('sw-validation-error')) {
                validator.addClass('sw-validation-error');
            }

            if ($.trim(addressesTextBox.val()).length == 0) {
                validator.text("<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_AK0_27) %>");
                validator.show();
                return;
            }

            $('.ValidationDiv .TransparentBoxID').show();
            validator.text('').show();
            PageMethods.StoreHostnames(addressesTextBox.val(), OnStartProcessed, OnError);
        };

        var OnStartProcessed = function (result) {
            StartHostnamesValidation(0);
        };

        var StartHostnamesValidation = function (startLine) {
            PageMethods.ProcessValidation(startLine, OnValResultsProcessed, OnError);
        };

        var OnValResultsProcessed = function (result) {
            hasUnresolvedNodes = hasUnresolvedNodes || result.HasUnresolvedNodes;
            var hostsValidator = $("#<%=HostsTextBoxValidator.ClientID %>");
            var validationWarning = $("#ValidationWarning");
            if (result.ResultText) {
                hostsValidator.append(result.ResultText);
            }


            if (result.RemainingNodes > 0) {
                StartHostnamesValidation(result.StartLine);
            } else {
                $('.ValidationDiv .TransparentBoxID').hide();
                if ($.trim($("#<%=HostsTextBoxValidator.ClientID %>").text()).length == 0) {
                    validationWarning.hide();
                    hostsValidator.removeClass('sw-validation-error');

                    hostsValidator.text('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_AK0_4) %>').addClass('success')
                } else if (hasUnresolvedNodes) {
                    validationWarning.show();
                }
            }
        };

        var OnError = function (error) {
            alert(error.get_message() + '\n' + error.get_stackTrace());
        };

        function InitializeRequest() {
        };

        function EndRequest() {
            var hostsValidator = $("#<%=HostsTextBoxValidator.ClientID %>");
            var warning = $("#ValidationWarning");
            if (hostsValidator.hasClass('sw-validation-error') && $.trim(hostsValidator.text()).length != 0) {
                warning.show();
            } else {
                warning.hide();
            }
        }


        $(document).ready(function () {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

        });
    })(window);
    //]]>
</script>

<div class="editor" id="IPAddressEditor">

    <h1 class="editor-label">
        <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_13%>"></asp:Literal>
        <a href="#" class="info-tooltip">
            <asp:Image runat="server" ImageUrl="~/Orion/Discovery/images/InfoIcon.png" />
            <span>
                <asp:Literal runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_17%>"></asp:Literal>
            </span>
        </a>
    </h1>

    <div class="editor-content">

        <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
            <ContentTemplate>

                <div class="add-button">
                    <asp:LinkButton runat="server" ID="AddItemButton" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AF0_15 %>" OnClick="AddItemButton_Click" CausesValidation="true" CssClass="link-button editor-content"></asp:LinkButton>
                </div>

                <asp:Panel runat="server" ID="InputPanel">
                    <div class="editor-section">
                        <!-- ip-addresses-input-->
                        <!-- left column, label and textarea -->
                        <div class="section-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_14) %></div>

                        <div class="editor-control-group">
                            <asp:TextBox runat="server" ID="IPAddressListTextBox" TextMode="MultiLine" Rows="5" OnTextChanged="IPAddressListTextBox_TextChanged" />
                            <asp:ImageButton runat="server" ID="ClearButton" ImageUrl="~/Orion/Discovery/images/TrashBin.png" OnClick="ClearButton_Click" CssClass="delete-button" title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AF0_90 %>" />
                            <asp:CustomValidator runat="server" ID="IPAddressListTextBoxValidator" ControlToValidate="IPAddressListTextBox" OnServerValidate="IPAddressListTextBoxValidator_ServerValidate" ValidateEmptyText="true" Display="Dynamic"></asp:CustomValidator>
                        </div>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button runat="server" ID="ValidateButton" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AF0_16 %>" OnClientClick="StartValidation(); return false;" />
                                </td>
                                <td>
                                    <span id="HostsTextBoxValidator" runat="server" class="ipAddressHostValidator" clientidmode="Static" />
                                    <div id="ValidationWarning" class="hint hidden"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_18) %></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="AddItemButton" />
                <asp:AsyncPostBackTrigger ControlID="ClearButton" />
            </Triggers>
        </asp:UpdatePanel>

        <div class="ValidationDiv">
            <orion:TBox ID="ValidationDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_92 %>" MessageVisible="true" />
        </div>

    </div>
</div>
