﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActiveDirectoryList.ascx.cs" Inherits="Orion_Discovery_Controls_ActiveDirectoryList" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
<%@ Reference Control="~/Orion/Controls/TransparentBox.ascx" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Discovery/Controls/ActiveDirectoryDetail.ascx" TagName="ADDetail" tagPrefix="orion"%>

<orion:TBox runat="server" ID="ModalityDiv" />
<orion:ADDetail ID="ActiveDirectoryDetail" runat="server" OnDialogCanceled="DialogCanceled" OnDialogSubmited="DialogSubmited"/>
            
<div>
    <table width="600px">
        <tr class="ButtonHeader">
            <td style="padding-left: 10px;">
                <asp:LinkButton ID="AddLinkButton" runat="server" OnClick="AddBtn_OnClick" CssClass="iconLink">Add Data</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView Width="100%" ID="DataGrid" runat="server" AutoGenerateColumns="False" GridLines="None" CssClass="NetObjectTable" AlternatingRowStyle-CssClass="NetObjectAlternatingRow">
                    <Columns>
                        <asp:TemplateField HeaderText="TODO: test" Visible="false">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AD Server">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("HostName")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="UserName">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# DefaultSanitizer.SanitizeHtml(Eval("Credential.UserName")) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>                                
                                <asp:ImageButton ID="DelBtn" runat="server" ToolTip="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_79 %>"
                                    ImageUrl="~/Orion/Discovery/images/icon_delete.gif" OnCommand="ActionItemCommand"
                                    CommandName="DeleteItem" CommandArgument="<%# Container.DataItemIndex %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:CustomValidator
        ID="GridValidator" 
        runat="server" 
        ErrorMessage="TODO: You need to enter some item" 
        EnableClientScript="false"
        Display="Dynamic">
    </asp:CustomValidator>
</div>