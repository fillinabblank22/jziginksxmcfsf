<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddSubnetsWithSeedRouterDialog.ascx.cs" Inherits="Orion_Discovery_Controls_AddSubnetsWithSeedRouterDialog" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>
    <script type="text/javascript">
    (function(root){
        root.SW = root.SW || {};
        root.SW.Orion = root.SW.Orion || {};
        root.SW.Orion.Discovery = root.SW.Orion.Discovery || {};
        root.SW.Orion.Discovery.Controls = root.SW.Orion.Discovery.Controls || {};
        root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog = root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog || {};
        root.SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.CONTROLS = {
            DIALOG: "<%= Dialog.ClientID %>",
            LOGIN_SUCCEEDED_HINT: "<%= LoginSucceededHint.ClientID %>",
            LOGIN_FAILED_HINT: "<%= LoginFailedHint.ClientID %>",
            TEST_SPINNER: "AddSubnetsWithSeedRouterDialog_testSpinner",
            SCAN_SPINNER: "AddSubnetsWithSeedRouterDialog_scanSpinner",
            ERROR_MESSAGE_CONTAINER: "<%= ErrorMessage.ClientID %>",
            SEED_ROUTER_TEXTBOX: { id: "<%= SeedRouterTextBox.ClientID %>", bind: "IPAddress" },

            ADD_NEW_SNMP_CREDS_RADIO: "<%= AddNewSNMPCreds.ClientID %>",
            SELECT_EXISTING_SNMP_CREDS_RADIO: "<%= SelectExistingSNMPCreds.ClientID %>",
            SNMP_VERSION_SELECT: { id: "<%= SnmpVersionSelect.ClientID%>", bind: "SNMPVersion" },
            SNMP_COMMUNITY_STRING_TEXTBOX: { id: "<%= SNMPCommunityStringTextBox.ClientID%>", bind: "CommunityString" },
            USERNAME_TEXTBOX: { id: "<%= UserNameTextBox.ClientID%>", bind: "SNMPv3UserName" },
            CONTEXT_TEXTBOX: { id: "<%= ContextTextBox.ClientID %>", bind: "SNMPv3Context" },
            AUTH_METHOD_SELECT: { id: "<%= AuthenticationMethodSelect.ClientID %>", bind: "SNMPv3AuthType" },
            PRIVACY_METHOD_SELECT: { id: "<%= EncryptionMethodSelect.ClientID %>", bind: "SNMPv3PrivacyType" },
            AUTH_KEY_PASSWORD_TEXTBOX: { id: "<%= AuthKeyPassword.ClientID %>", bind: "AuthKeyPassword" },
            PRIVACY_KEY_PASSWORD_TEXTBOX: { id: "<%= EncryptionKeyPassword.ClientID %>", bind: "PrivacyKeyPassword" },
            AUTH_PASSWORD_ISA_KEY_CHECKBOX: { id: "<%= AuthPasswordIsKey.Checked %>", bind: "AuthKeyIsPwd" },
            PRIVACY_PASSWORD_ISA_KEY_CHECKBOX: { id: "<%= EncryptionPasswordIsKey.ClientID %>", bind: "PrivKeyIsPwd" },
            SUBNETS_GRID_VIEW : "<%=SubnetsGridView.ClientID %>"
        };
        
        
        
        
        
        
    })(window);


    </script>
<asp:UpdatePanel ID="UpdatePanel" runat="server">

    <ContentTemplate>
            <asp:Panel ID="Dialog"  runat="server" Visible="false" CssClass="editor-dialog add-subnets-dialog">
            <div class="editor-dialog-header">
                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_20) %></span>
                    <asp:ImageButton ID="CloseDialog" runat="server" ImageUrl="~/Orion/Discovery/images/remove_White.png"  OnClick="CloseDialog_Click"/>
             </div>
            
            <div class="editor-dialog-content">
                <asp:Panel ID="SeedRouterSettings" runat="server">
                <div class="editor-dialog-section">
                    <div class="section-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_24) %></div>
                    <div class="editor-dialog-control-group">
                    <asp:TextBox ID="SeedRouterTextBox" runat="server" ValidationGroup="SeedRouter" OnTextChanged="SeedRouterTextBox_TextChanged"></asp:TextBox>
                    <asp:CustomValidator ID="SeedRouterEmptyValidator" runat="server" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_93 %>" ControlToValidate="SeedRouterTextBox"  OnServerValidate="SeedRouterEmptyValidator_ServerValidate"
                        EnableClientScript="false" ValidateEmptyText="true" Display="Dynamic" ValidationGroup="SeedRouter" />
                    <asp:CustomValidator ID="SeedRouterValidator" runat="server"   EnableClientScript="false"
                        ControlToValidate="SeedRouterTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_25 %>" 
                        OnServerValidate="SeedRouterValidator_ServerValidate" ValidationGroup="SeedRouter" ValidateEmptyText="false" Display="Dynamic">

                        </asp:CustomValidator>
                    </div>

                <div class="editor-dialog-control-group" runat="server" visible="false"> 
                    <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_52) %></div>
                    <asp:TextBox ID="SNMPPortTextBox" runat="server" ValidationGroup="SeedRouter"></asp:TextBox>
                    <asp:CustomValidator ID="SNMPPortValidator" runat="server" 
                        ControlToValidate="SNMPPortTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_51 %>" 
                        OnServerValidate="SNMPPortValidator_ServerValidate" ValidationGroup="SeedRouter" ValidateEmptyText="false" Display="Dynamic">

                        </asp:CustomValidator>
                    </div>
                </div>
                <div class="editor-dialog-section">
                    <div class="section-label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_26) %></div>
                    <div class="editor-dialog-control-group">
                        <asp:RadioButton ID="SelectExistingSNMPCreds" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_28 %>" AutoPostBack="true" GroupName="CredsType"  Checked="true" OnCheckedChanged="CredsTypeCheckedChanged" CausesValidation="false" ValidationGroup="SeedRouter"/>
                        <asp:RadioButton ID="AddNewSNMPCreds" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_27 %>" AutoPostBack="true" OnCheckedChanged="CredsTypeCheckedChanged"  GroupName="CredsType" CausesValidation="false" ValidationGroup="SeedRouter" />
                    </div>

                    <asp:Panel ID="NewCredsPanel" runat="server" Visible="false">
                        <div class="editor-dialog-control-group">
                            <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_29) %></div>
                            <asp:DropDownList ID="SnmpVersionSelect" runat="server" OnSelectedIndexChanged="SnmpVersionSelect_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_48 %>" Value="SNMP1"  Selected="True"/>
                                <asp:ListItem Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_49 %>" Value="SNMP3" />
                            </asp:DropDownList>
                        </div>
                        <asp:Panel ID="SNMPv1Or2Panel" runat="server" Visible="true">
                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_30) %></div>
                                <asp:TextBox ID="SNMPCommunityStringTextBox" runat="server" ValidationGroup="SeedRouter"></asp:TextBox>
                                <asp:CustomValidator ID="CommunityStringValidator" runat="server" ControlToValidate="SNMPCommunityStringTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_31 %>" 
                        OnServerValidate="CommunityStringValidator_ServerValidate" ValidationGroup="SeedRouter" EnableClientScript="false" ValidateEmptyText="true" Display="Dynamic" ></asp:CustomValidator>
                                    <asp:CustomValidator ID="NonDuplicateCommunityStringValidator" runat="server" ControlToValidate="SNMPCommunityStringTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_54 %>" 
                        OnServerValidate="NonDuplicateCommunityStringValidator_ServerValidate" ValidationGroup="SeedRouter" ValidateEmptyText="false" Display="Dynamic" >
                        </asp:CustomValidator>
                            </div>

                        </asp:Panel>
                        <asp:Panel ID="SNMPv3Panel" runat="server" Visible="false">

                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_35) %></div>
                                <asp:TextBox ID="UserNameTextBox" runat="server"></asp:TextBox>
                                <asp:CustomValidator ID="NonEmptyUserNameValidator" runat="server" ControlToValidate="UserNameTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_36 %>" 
                                    OnServerValidate="NonEmptyUserNameValidator_ServerValidate"
                                     ValidationGroup="SeedRouter" ValidateEmptyText="true" Display="Dynamic" >  
                                    </asp:CustomValidator>
                                     <asp:CustomValidator ID="NOonDuplicateUserNameValidator" runat="server" ControlToValidate="UserNameTextBox" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_87 %>" 
                        OnServerValidate="NOonDuplicateUserNameValidator_ServerValidate" ValidationGroup="SeedRouter" ValidateEmptyText="true" Display="Dynamic" >
                        </asp:CustomValidator>
                                
                            </div>

                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_37) %></div>
                                <asp:TextBox ID="ContextTextBox" runat="server" autocomplete="off"></asp:TextBox>

                            </div>

                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_38) %></div>
                                <asp:DropDownList ID="AuthenticationMethodSelect" runat="server" OnSelectedIndexChanged="AuthMetodDropList_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true" />
                            </div>
                            <div runat="server" ID="AuthKePasswordPanel" visible="false"  class="editor-dialog-control-group condensed">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_39) %></div>
                                <asp:TextBox ID="AuthKeyPassword" runat="server" TextMode="Password" autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="editor-dialog-control-group">
                                <asp:CheckBox ID="AuthPasswordIsKey" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_40 %>" Visible="false"/>
                            </div>

                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_41) %></div>
                                <asp:DropDownList ID="EncryptionMethodSelect" runat="server" OnSelectedIndexChanged="PrivMethodDropList_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true" />
                            </div>

                            <div class="editor-dialog-control-group condensed" runat="server" id="EncryptionPanel" visible="false">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_53) %></div>
                                <asp:TextBox ID="EncryptionKeyPassword" runat="server" TextMode="Password"  autocomplete="off"></asp:TextBox>
                                
                            </div>
                            <div class="editor-dialog-control-group">
                                <asp:CheckBox ID="EncryptionPasswordIsKey" runat="server"  Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_40 %>" Visible="false"/>
                            </div>
                        </asp:Panel>


                    </asp:Panel>
                    <asp:Panel ID="SelectExistingCredsPanel" runat="server" Visible="true">
                            <div class="editor-dialog-control-group">
                                <div class="label"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_42) %></div>
                                <asp:DropDownList ID="CredentialsSelect" runat="server" DataTextField="Name">
                                    
                                </asp:DropDownList>
                            </div>
                        

                    </asp:Panel>
                    <div class="editor-dialog-control-group">
                        <!-- <a href="#" onclick="SW.Orion.Discovery.Controls.AddSubnetsWithSeedRouterDialog.TestSnmpCredentials(event)" class="button" >Test Credentials</a> -->
                        <asp:Button ID="TestCredentialsButton" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_43 %>" OnClick="TestCredentialsButton_Click" CausesValidation="true" ValidationGroup="SeedRouter"/>
                        <img id="AddSubnetsWithSeedRouterDialog_testSpinner" src="../images/spinner.gif" class="spinner hidden" />
                        <span ID="LoginSucceededHint" runat="server" class="success" visible="false"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_44) %></span>
                        <span id="LoginFailedHint" runat="server" class="error" visible="false"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AF0_45) %></span>
                    </div>
                </div>
                </asp:Panel>
                
                <asp:Panel ID="SubnetsPanel" runat="server" Visible="false">
                    <div class="editor-dialog-section">
                        <div class="editor-dialog-control-group">
                            <div style="width: 100%; max-height: 500px; overflow-x: hidden; overflow-y: auto">
                                <asp:GridView ID="SubnetsGridView" runat="server" HeaderStyle-CssClass="fixedHeader" ShowHeader="True" CssClass="subnets-grid">
                                    <Columns>
                                        <asp:TemplateField >
                                            <HeaderTemplate>
                                                <input id="selectAllSubnets" type="checkbox" checked="checked" /> 
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="SelectedCheckbox" Checked='<%# Eval("Selected") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderStyle-Width="290px" DataField="SubnetCIDR" HeaderText="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_46 %>" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                
            </div>
            
            <div class="editor-dialog-footer">
                <div class="editor-dialog-control-group">
                <asp:Button ID="ScanButton" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_21%>" CssClass="primary"  OnClick="ScanButton_Click" CausesValidation="true" ValidationGroup="SeedRouter"/>
                <asp:Button ID="AddButton" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_22%>" Visible="false" CssClass="primary" OnClick="AddButton_Click" />
                <asp:Button ID="CancelButton" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AF0_23%>" OnClick="CancelButton_Click" />
                <img id="AddSubnetsWithSeedRouterDialog_scanSpinner" src="../images/spinner.gif" class="spinner hidden" />
                </div>
                
                <div ID="ErrorMessage" runat="server" Visible="false" class="error"></div>
                
                

            </div>

                </asp:Panel>
        </ContentTemplate>

    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="TestCredentialsButton" />
    </Triggers>
</asp:UpdatePanel>




