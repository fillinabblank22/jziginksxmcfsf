<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CredentialDetail.ascx.cs"
    Inherits="Orion_Discovery_Controls_CredentialDetail" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

    <asp:Panel ID="CredentialDialog" runat="server" BorderWidth="1"  Visible="false" CssClass="EditPanel">
        <p class="DialogHeaderText">
            <asp:Label ID="ActionHeader" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_513 %>" Font-Bold="true" Font-Size="Larger" />
        </p>
        
        <%--<p class="DialogHeaderText">
        Add Windows Credential
        </p>--%>
        
        <asp:Panel ID="CredentialControls" runat="server" BorderWidth="0" Visible="true" Class="EditControls">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_509) %><br />
            <asp:DropDownList ID="ActionDropDown" runat="server" Width="190" OnSelectedIndexChanged="ChooseCredentialChanged" AutoPostBack="true"></asp:DropDownList>            
            
            <div class="smallTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_521) %></div>
            <asp:TextBox ID="CredentialName" runat="server" Width="185" Enabled="true" autocomplete="off" MaxLength="50" />
            <br />
            <asp:RequiredFieldValidator ID="CredentialNameTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_514 %>" ControlToValidate="CredentialName" EnableClientScript="true" Display="Dynamic" />
            <asp:CustomValidator runat="server" id="CredentialNameDuplicityValidator" controltovalidate="CredentialName" onservervalidate="CredentialNameDuplicityValidation" errormessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_515 %>" Display="Dynamic"/>

            <div class="smallTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_73) %></div>
            <asp:TextBox ID="UserName" runat="server" Width="185" Enabled="true" autocomplete="off" MaxLength="50" />
            <br />
            <div class="smallText"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_522) %></div>
            <div id="EditCredentialHelp">
             <span class="LinkArrow">&#0187;</span> 
             <orion:HelpLink ID="WmiDiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHWhatPrivilegesNeed"
        HelpDescription="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_516 %>" CssClass="helpLink" />
            </div>
            <asp:RequiredFieldValidator ID="UserNameTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_517 %>" ControlToValidate="UserName" EnableClientScript="true" Display="Dynamic" />

            <div class="smallTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_40) %></div>
            <asp:TextBox ID="Password" runat="server" Width="185" Enabled="true" autocomplete="off" MaxLength="50" TextMode="Password"/>
            <br />
            <asp:RequiredFieldValidator ID="PasswordTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_518 %>" ControlToValidate="Password" EnableClientScript="true" Display="Dynamic" />

            <div class="smallTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_182) %></div>
            <asp:TextBox ID="ConfirmPassword" runat="server" Width="185" Enabled="true" autocomplete="off" MaxLength="50" TextMode="Password" />
            <br />
            <asp:RequiredFieldValidator ID="ConfirmPasswordTextBoxRequiredFieldValidator" runat="server"
                ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_519 %>" ControlToValidate="ConfirmPassword" EnableClientScript="true" Display="Dynamic" />
            <asp:CompareValidator runat="server" ID="ConfirmPasswordTextBoxCompareValidator" ControlToValidate="ConfirmPassword" ControlToCompare="Password" 
                EnableClientScript="true" ErrorMessage="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_520 %>" />
        </asp:Panel>
        
        <asp:Panel ID="Buttons" runat="server" style="margin-left: 15px;" CssClass="EditButtons">           
            <div class="sw-btn-bar">
                <orion:LocalizableButton ID="SubmitDialog" runat="server" DisplayType="Primary" LocalizedText="CustomText"
                    Text="<%$ HtmlEncodedResources: CoreWebContent,WEBDATA_AK0_138 %>" OnClick="AddNewCredential_OnClick"
                    OnClientClick="showAlert = 0;" />                
                <orion:LocalizableButton ID="CancelAddCred" runat="server" DisplayType="Secondary"
                    LocalizedText="Cancel" OnClick="CancelDialog_OnClick" CausesValidation="false"
                    OnClientClick="showAlert = 0;" />
            </div>            
        </asp:Panel>
    </asp:Panel>
