﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASP;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;

public partial class Orion_Discovery_Controls_CredentialsGrid : System.Web.UI.UserControl
{
    public List<UsernamePasswordCredential> Credentials;
    public List<UsernamePasswordCredential> AllCredentials;
    public event EventHandler AddButton_Click;    

    protected void Page_Load(object sender, EventArgs e)
    {
        this.CredentialsGrid.DataSource = Credentials;
        this.CredentialsGrid.DataBind();
    }       
        
    public void Refresh()
    {
        ModalityDiv.ShowBlock = false;
        this.CredentialsGrid.DataSource = Credentials;
        this.CredentialsGrid.DataBind();
    }

    // shows editor dialog for adding a new set
    protected void AddBtn_OnClick(object sender, EventArgs e)
    {
        ModalityDiv.ShowBlock = true;              
        if(AddButton_Click!=null)
        {
            AddButton_Click(this, e);
        }
    }

    protected void ActionItemCommand(object sender, CommandEventArgs e)
    {
        int itemIndex = -1;

        if (e.CommandArgument != null)
            int.TryParse(e.CommandArgument.ToString(), out itemIndex);

        switch (e.CommandName)
        {
            case "MoveUp":
                MoveItemUp(itemIndex);
                break;
            case "MoveDown":
                MoveItemDown(itemIndex);
                break;
            case "DeleteItem":
                DeleteItem(itemIndex);
                break;            
        }

    }

    // delete selected credentials
    protected void DeleteItem(int selectedIndex)
    {
        if (selectedIndex >= 0)
        {
            UsernamePasswordCredential cred = Credentials[selectedIndex];
            Credentials.RemoveAt(selectedIndex);
            AllCredentials.Add(cred);
            Refresh();
        }
    }

    // move credentials up
    protected void MoveItemUp(int selectedIndex)
    {
        if (selectedIndex > 0)
        {
            Credentials.Reverse(selectedIndex - 1, 2);
            Refresh();
        }
    }

    // move credentials down
    protected void MoveItemDown(int selectedIndex)
    {
        if (selectedIndex >= 0 && selectedIndex < this.CredentialsGrid.Rows.Count - 1)
        {
           Credentials.Reverse(selectedIndex, 2);
           Refresh();
        }
    }
}