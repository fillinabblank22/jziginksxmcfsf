<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressPanel.ascx.cs"
    Inherits="Orion_Discovery_Wizard_Controls_ProgressPanel" %>
<%@ Register Src="~/Orion/Discovery/Controls/Progress.ascx" TagPrefix="orion" TagName="Progress" %>
<orion:Include ID="Include1" runat="server" File="ModalDialog.css" />
<asp:UpdatePanel ID="UpdatePanel" runat="server">
    <ContentTemplate>
        <div>
            <table>
                <tbody>
                    <tr class="header">
                        <th colspan="2">
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_167) %>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_168) %>
                        </th>
                        <td style="width: auto">
                            <orion:Progress ID="Progress" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="padding: 10px; text-align: right;">
                <orion:LocalizableButton ID="dialogOk" runat="server" DisplayType="Primary" LocalizedText="Ok"/>
                <orion:LocalizableButton ID="dialogCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false"/>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
