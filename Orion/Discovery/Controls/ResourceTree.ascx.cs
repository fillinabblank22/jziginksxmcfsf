﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using SolarWinds.Orion.Web.Discovery;
using System.Web.Services;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Web;

public partial class Orion_Discovery_Controls_ResurceTree : System.Web.UI.UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public Guid InfoId
    {
        get { return (Guid)(this.ViewState["infoId"] ?? Guid.Empty); }
        set { this.ViewState["infoId"] = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string NodeName
    {
        get { return (string)(this.ViewState["nodeName"] ?? String.Empty); }
        set { this.ViewState["nodeName"] = value; }
    }

    public bool ErrorOccured { get; set; }

    public string OnClientBeforeResultReceived { get; set; }

    public string OnClientAfterResultReceived { get; set; }

    protected ResourceTreeInfo TreeInfo = null;

    private const string defaultNodeManagementPageUrl = "/Orion/Nodes/Default.aspx";

    public string ReturnURL
    {
        get
        {
            return ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault(defaultNodeManagementPageUrl);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ResetErrorMessage();
        if (this.InfoId == Guid.Empty)
        {
            SetErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_98);
            return;
        }
        
        this.TreeInfo = ResourceTreeHelper.GetResourceTreeInfo(this.InfoId);

        if (this.TreeInfo == null)
        {
            SetErrorMessage(Resources.CoreWebContent.WEBCODE_TM0_99);
            return;
        }
    }

    private void ResetErrorMessage()
    {
        this.ErrorLabel.Text = string.Empty;
        this.ErrorLabel.Visible = false;
        this.ResourceTreePanel.Visible = true;
        this.ErrorOccured = false;
    }

    private void SetErrorMessage(string message)
    {
        this.ErrorLabel.Text = message;
        this.ErrorLabel.Visible = true;
        this.ResourceTreePanel.Visible = false;
        this.ErrorOccured = true;
    }
}