﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Common.Extensions;
using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.Controls;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using System.Text;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Discovery_Controls_IgnoreList : System.Web.UI.UserControl
{
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
    private static string IPAddress = "IPAddress";
    private static string EngineID = "EngineID";

    [Obsolete("Do not use this field - will be removed.", true)]
    public static bool isInterfaceAllowed = new RemoteFeatureManager().AllowInterfaces;
    protected static bool isNPMInstalled = ModuleManager.InstanceWithCache.IsThereModule("NPM");
    private string AreAllSelectedKey = "AreAllSelected";
    protected int DisplayedNodes { get; set; }    

    static void BusinessLayerExceptionHandler(Exception ex)
    {
        log.Error(ex);
    }

    #region Properties    

    private bool AreAllSelected
    {
        get
        {
            if (ViewState[AreAllSelectedKey] == null)
            {
                ViewState[AreAllSelectedKey] = false;
            }
            return Convert.ToBoolean(ViewState[AreAllSelectedKey]);
        }

        set
        {
            ViewState[AreAllSelectedKey] = value;
        }
    }

    protected int SelectedIndex
    {
        get
        {
            return ViewState["SelectedIndex"] != null ?
              Convert.ToInt32(ViewState["SelectedIndex"]) : 0;
        }
        set
        {
            ViewState["SelectedIndex"] = value.ToString();
        }
    }

    #endregion

    #region Page event handlers

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);        
        this.IgnoreNodesTableGrid.OnGetDataSource+= IgnoreNodesTableGrid_GetDataSource;
    }

    List<DiscoveryIgnoredNodeUI> IgnoreNodesTableGrid_GetDataSource()
    {        
        return ScheduleDiscoveryHelper.IgnoredNodes;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        bool allNodesSelected = false;
        if (AllNodesSelected.Value != String.Empty)
        {
            allNodesSelected = Convert.ToBoolean(AllNodesSelected.Value);
        }

        StoreSelection(allNodesSelected);        

        if (!IsPostBack)
        {
            ScheduleDiscoveryHelper.Search = null;     
            IgnoreNodesTableGrid_BindData();
        }

        if( ScheduleDiscoveryHelper.IgnoredNodes.Count > 0 )
        {
            var node = ScheduleDiscoveryHelper.IgnoredNodes[0];

            string partial = String.Join( ",", node.GroupDefinitions.Select( n => n.Group.DisplayName));

            if (!String.IsNullOrEmpty(partial))
                IgnoreNodesTableGrid.Columns[2].HeaderText = IgnoreNodesTableGrid.Columns[2].HeaderText + "," + partial;
        }
    }    

    protected string GetChildObjectsColumnName()
    {
        StringBuilder bld = new StringBuilder();

        bld.Append(Resources.CoreWebContent.WEBCODE_AK0_15);

        if (ScheduleDiscoveryHelper.IgnoredNodes.Count > 0)
        {
            var node = ScheduleDiscoveryHelper.IgnoredNodes[0];

            node.GroupDefinitions.ForEach(n => bld.AppendFormat(", {0}", n.Group.DisplayName));            
        }

        return bld.ToString();
    }
    
    protected void CheckAllSelector_OnCheckedChanged(object sender, EventArgs e)
    {
        this.AreAllSelected = ((CheckBox)this.IgnoreNodesTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked;
        
    }

    protected void CheckAllSelector_OnPreRender(object sender, EventArgs e)
    {
        ((CheckBox)this.IgnoreNodesTableGrid.HeaderRow.FindControl("CheckAllSelector")).Checked = this.AreAllSelected;        
    }


    protected void Pager_Click(Object sender, EventArgs e)
    {
        ImageButton pagerAction = sender as ImageButton;

        if (pagerAction != null)
        {
            switch (pagerAction.CommandArgument)
            {
                case "first":
                    IgnoreNodesTableGrid.PageIndex = 0;
                    break;
                case "last":
                    IgnoreNodesTableGrid.PageIndex = IgnoreNodesTableGrid.PageCount;
                    break;
                case "prev":
                    if (IgnoreNodesTableGrid.PageIndex > 0)
                        IgnoreNodesTableGrid.PageIndex--;
                    break;
                case "next":
                    if (IgnoreNodesTableGrid.PageIndex < IgnoreNodesTableGrid.PageCount)
                        IgnoreNodesTableGrid.PageIndex++;
                    break;
                default:
                    throw new NotSupportedException(String.Format("Pager action \"{0}\" is not supported", pagerAction.CommandArgument));
            }
        }

        // discard all selection        
        AllNodesSelected.Value = "False";
        // discard already made selection        
        DiscardSelection();
    }

    protected void PageDropDownList_SelectedIndexChanged(Object sender, EventArgs e)
    {
        // Retrieve the pager rows.
        GridViewRow bottomPagerRow = this.IgnoreNodesTableGrid.BottomPagerRow;
        GridViewRow topPagerRow = this.IgnoreNodesTableGrid.TopPagerRow;

        // Retrieve the PageDropDownList DropDownList from the bottom pager row.
        DropDownList bottomPageList = (DropDownList)bottomPagerRow.Cells[0].FindControl("PageDropDownList");
        // Retrieve the PageDropDownList DropDownList from the top pager row.
        DropDownList topPageList = (DropDownList)topPagerRow.Cells[0].FindControl("PageDropDownList");

        
        if (bottomPageList.SelectedIndex == SelectedIndex)
        {
            // Set the PageIndex property to display that page selected by the user.
            IgnoreNodesTableGrid.PageIndex = topPageList.SelectedIndex;
        }
        else
        {
            // Set the PageIndex property to display that page selected by the user.
            IgnoreNodesTableGrid.PageIndex = bottomPageList.SelectedIndex;
        }

        // discard all selection        
        AllNodesSelected.Value = "False";
        // discard already made selection        
        DiscardSelection();
    }

    protected void pageSizeTextBox_OnChange(Object sender, EventArgs e)
    {
        TextBox pageSizeTextBox = sender as TextBox;
        int pageSize = Convert.ToInt32(pageSizeTextBox.Text);
        IgnoreNodesTableGrid.PageSize = pageSize;

        // discard all selection        
        AllNodesSelected.Value = "False";
        // discard already made selection        
        DiscardSelection();
    }

    protected void IgnoreListGridView_DataBound(Object sender, EventArgs e)
    {
        // Retrieve the pager row.
        GridViewRow pagerRow = IgnoreNodesTableGrid.BottomPagerRow;
        SetPager(pagerRow,ScheduleDiscoveryHelper.IgnoredNodes.Count);
        pagerRow = IgnoreNodesTableGrid.TopPagerRow;
        SetPager(pagerRow,ScheduleDiscoveryHelper.IgnoredNodes.Count);
        SavePagerStatus();
    }

    protected void IgnoreListGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("search", "searchableRow");
        }
    }

    protected void IgnoreListGridView_DataBinding(Object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.IgnoreNodesTableGrid.PageSize = IgnoreNodesTableGrid.ActualPageSize;
            this.IgnoreNodesTableGrid.PageIndex = IgnoreNodesTableGrid.ActualPage;
        }
    }
    #endregion

    #region utility methods  
       
    protected bool IsSelectionEnabled(object o)
    {
        DiscoveryIgnoredNodeUI discoveryIgnoredNodeUI = o as DiscoveryIgnoredNodeUI;
        if (discoveryIgnoredNodeUI == null)
        {
            throw new NotSupportedException(String.Format("Method except instance of {0} class", typeof(DiscoveryIgnoredNodeUI).Name));
        }

        if (discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IsSelected)
            return false;
        else
            return true;
    }

    private void IgnoreNodesTableGrid_BindData()
    {
       ScheduleDiscoveryHelper.IgnoredNodes = GetIgnoredNodes();
       if (!string.IsNullOrEmpty(ScheduleDiscoveryHelper.Search))
       {
           var searchFilter = ScheduleDiscoveryHelper.Search.ToLower();

           ScheduleDiscoveryHelper.IgnoredNodes = 
               ScheduleDiscoveryHelper.IgnoredNodes.Where(x =>
                   x.DiscoveryIgnoredNode.Caption.ToLower().Contains(searchFilter)
                   || x.DiscoveryIgnoredNode.IPAddress.Contains(searchFilter)).ToList();
       }
        this.IgnoreNodesTableGrid.DataSource = ScheduleDiscoveryHelper.IgnoredNodes;
        this.IgnoreNodesTableGrid.DataBind();
    }

    private void StoreSelection(bool allNodesSelected)
    {
        if (allNodesSelected)
        {
            foreach (DiscoveryIgnoredNodeUI ignoredNodeUI in ScheduleDiscoveryHelper.IgnoredNodes)
            {
                ignoredNodeUI.DiscoveryIgnoredNode.IsSelected = true;
            }
        }
        else
        {
            for (int i = 0; i < this.IgnoreNodesTableGrid.Rows.Count; i++)
            {
                Control item = IgnoreNodesTableGrid.Rows[i];
                CustomCheckBox checkBox = item.FindControl("CustomSelector") as CustomCheckBox;

                if (checkBox == null)
                    continue;

                IDictionary<string, string> key = checkBox.CustomValue as Dictionary<string, string>;
                if (key == null)
                    continue;

                var discoveryIgnoredNodeUi = ScheduleDiscoveryHelper.IgnoredNodes.SingleOrDefault(n => n.DiscoveryIgnoredNode.IPAddress == key[IPAddress] && n.DiscoveryIgnoredNode.ID == Convert.ToInt32(key[EngineID]));
                if(discoveryIgnoredNodeUi != null)
                discoveryIgnoredNodeUi.DiscoveryIgnoredNode.IsSelected = checkBox.Checked;
            }
        }
    }

    private List<DiscoveryIgnoredNodeUI> GetIgnoredNodes()
    {
        List<DiscoveryIgnoredNode> discoveryIgnoredNodes = null;
        List<DiscoveryItemGroupDefinition> discoveryGroupDefinitions = null;
        using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
        {
            try
            {
                discoveryIgnoredNodes = businessProxy.GetDiscoveryIgnoredNodes();
                discoveryGroupDefinitions = businessProxy.GetDiscoveryScheduledImportGroupDefinitions();
            }
            catch (Exception ex)
            {
                log.Error("Unable to load Ignore list", ex);
                throw new ApplicationException("Unable to load Ignore list");
            }
        }

        List<DiscoveryIgnoredNodeUI> DiscoveryIgnoredNodeUIs = new List<DiscoveryIgnoredNodeUI>();

        foreach (DiscoveryIgnoredNode discoveryIgnoreNode in discoveryIgnoredNodes)
        {
            DiscoveryIgnoredNodeUI DiscoveryIgnoredNodeUI = new DiscoveryIgnoredNodeUI()
            {                
                EngineName = GetEngineName(discoveryIgnoreNode.EngineID),                                
                DiscoveryIgnoredNode = discoveryIgnoreNode,
                GroupDefinitions = discoveryGroupDefinitions
            };

            // isSelected = false
            DiscoveryIgnoredNodeUI.DiscoveryIgnoredNode.NodeResult.PluginResults
                .SelectMany(n => n.GetDiscoveredObjects())
                .Iterate(n => n.IsSelected = false);

            DiscoveryIgnoredNodeUIs.Add(DiscoveryIgnoredNodeUI);
        }
        return DiscoveryIgnoredNodeUIs;        
    }

    private static Dictionary<int, string> cachedEngines = new Dictionary<int, string>(); 
    protected string GetEngineName(int engineID)
    {
        if (cachedEngines.ContainsKey(engineID))
            return cachedEngines[engineID];

        string engineName = String.Empty;

        try
        {
            using (WebDAL webDAL = new WebDAL())
            {
                engineName = webDAL.GetEngineNameByEngineID(engineID);
            }

            cachedEngines[engineID] = engineName;
        }
        catch (Exception ex)
        {
            log.Error("Unable to get Engine Name for given EngineID", ex);            
        }
        return engineName; 
    }   

    protected object GetKey(object o)
    {
        DiscoveryIgnoredNodeUI discoveryIgnoredNodeUI = o as DiscoveryIgnoredNodeUI;
        if (discoveryIgnoredNodeUI != null)
        {
            return new Dictionary<string, string> { { IPAddress, discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IPAddress }, { EngineID, discoveryIgnoredNodeUI.DiscoveryIgnoredNode.ID.ToString() } };            
        }
        return null;
    }

    private void RegisterStartupScriptAlert(string message)
    {
        string script = ControlHelper.JsFormat("alert('{0}');", message);
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "IgnoreListInfo", script, true);
    }

    private void SetPager(GridViewRow pagerRow, int totalCount)
    {
        if (pagerRow == null) return;

        pagerRow.Visible = true;

        // Retrieve the DropDownList and Label controls from the row.        
        DropDownList pageList = (DropDownList)pagerRow.Cells[0].FindControl("PageDropDownList");
        Label pageLabel = (Label)pagerRow.Cells[0].FindControl("CurrentPageLabel");
        Label pageCountLabel = (Label)pagerRow.Cells[0].FindControl("PageCountLabel");
        TextBox pageSizeTextBox = pagerRow.Cells[0].FindControl("pageSizeTextBox") as TextBox;

        if (pageList != null)
        {

            // Create the values for the DropDownList control based on 
            // the  total number of pages required to display the data
            // source.
            for (int i = 0; i < IgnoreNodesTableGrid.PageCount; i++)
            {

                // Create a ListItem object to represent a page.
                int pageNumber = i + 1;
                ListItem item = new ListItem(pageNumber.ToString());

                // If the ListItem object matches the currently selected
                // page, flag the ListItem object as being selected. Because
                // the DropDownList control is recreated each time the pager
                // row gets created, this will persist the selected item in
                // the DropDownList control.   
                if (i == IgnoreNodesTableGrid.PageIndex)
                {
                    item.Selected = true;
                    SelectedIndex = i;
                }

                // Add the ListItem object to the Items collection of the 
                // DropDownList.
                pageList.Items.Add(item);

            }
        }

        if (pageLabel != null)
        {
            // Calculate the current page number.
            int lowerBoundary = (IgnoreNodesTableGrid.PageIndex) * IgnoreNodesTableGrid.PageSize + 1;
            int temp = (IgnoreNodesTableGrid.PageIndex + 1) * IgnoreNodesTableGrid.PageSize;
            int upperBoundary = temp > totalCount ? totalCount : temp;

            DisplayedNodes = upperBoundary - lowerBoundary + 1;

            // Update the Label control with the current page information.
            pageLabel.Text = string.Format(Resources.CoreWebContent.WEBCODE_AK0_38, lowerBoundary.ToString(), upperBoundary, totalCount);
        }

        if (pageCountLabel != null)
        {
            pageCountLabel.Text = string.Format(Resources.CoreWebContent.WEBCODE_AK0_39, IgnoreNodesTableGrid.PageCount.ToString());
        }

        if (pageSizeTextBox != null)
        {
            pageSizeTextBox.Text = IgnoreNodesTableGrid.PageSize.ToString();
        }

        if (IgnoreNodesTableGrid.PageIndex == 0)
        {
            ImageButton firstPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("FirstButton");
            firstPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-first-disabled.gif";

            ImageButton previousPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("PreviousButton");
            previousPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-prev-disabled.gif";
        }

        if (IgnoreNodesTableGrid.PageIndex == (IgnoreNodesTableGrid.PageCount - 1))
        {
            ImageButton lastPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("LastButton");
            lastPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-last-disabled.gif";

            ImageButton nextPageImageButton = (ImageButton)pagerRow.Cells[0].FindControl("NextButton");
            nextPageImageButton.ImageUrl = "~/Orion/Nodes/images/pager/page-next-disabled.gif";
        }
    }

    private void SavePagerStatus()
    {
        IgnoreNodesTableGrid.ActualPage = IgnoreNodesTableGrid.PageIndex;
        IgnoreNodesTableGrid.ActualPageSize = IgnoreNodesTableGrid.PageSize;        
    }

    private void DiscardSelection()
    {
        foreach (DiscoveryIgnoredNodeUI discoveryIgnoredNodeUI in ScheduleDiscoveryHelper.IgnoredNodes)
        {
            discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IsSelected = false;
            discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredInterfaces.ForEach(i => i.IsSelected = false);
            discoveryIgnoredNodeUI.DiscoveryIgnoredNode.IgnoredVolumes.ForEach(v => v.IsSelected = false);
        }
    }

    #endregion

    #region Public methods
    public void RemoveSelectedFromIgnoreList()
    {
        IEnumerable<DiscoveryIgnoredNodeUI> selectedNodes =
            ScheduleDiscoveryHelper.IgnoredNodes.Where(n => n.DiscoveryIgnoredNode.IsSelected ||
                                                            n.DiscoveryIgnoredNode.IgnoredInterfaces.Any(
                                                                ll => ll.IsSelected) ||
                                                            n.DiscoveryIgnoredNode.IgnoredVolumes.Any(
                                                                ll => ll.IsSelected) ||
                                                            n.DiscoveryIgnoredNode.NodeResult.PluginResults.SelectMany(
                                                                ll => ll.GetDiscoveredObjects())
                                                                .Any(ll => ll.IsSelected)).ToList();

        // no selection
        if (selectedNodes.Any() == false)
            return;

        try
        {
            using (var businessProxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                businessProxy.DeleteDiscoveryIgnoredNodes(selectedNodes.Select(t => t.DiscoveryIgnoredNode).ToList());
            }
        }
        catch (Exception e)
        {
            log.Error("Unable to delete Ignored Node, Volume or Interface from Ignore List", e);
            RegisterStartupScriptAlert(Resources.CoreWebContent.WEBCODE_AK0_37);
        }

        // refresh contain of table
        this.IgnoreNodesTableGrid_BindData();
    }

    public void RefreshIgnoredList()
    {
        this.IgnoreNodesTableGrid_BindData(); 
    }
    #endregion

}
