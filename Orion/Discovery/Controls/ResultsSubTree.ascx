<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResultsSubTree.ascx.cs" Inherits="Orion_Discovery_Controls_ResultsSubTree" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Controls" %>

<%@ Import Namespace="SolarWinds.Orion.Core.Models.OldDiscoveryModels" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Models.Interfaces" %>

<asp:ScriptManagerProxy ID="ScriptManager1" runat="server" >
		<Services>
			<asp:ServiceReference path="~/Orion/Services/ScheduleDiscovery.asmx"/>			
		</Services>
	</asp:ScriptManagerProxy>
<asp:UpdatePanel ID="UpdatePanelSubTree" runat="server" ChildrenAsTriggers="true"
    UpdateMode="Conditional">
    <ContentTemplate>
        <asp:ImageButton ID="ExpandImageButton" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif"
            OnClick="ExpandImageButton_Click"/>
        <span class="td-text"><%# DefaultSanitizer.SanitizeHtml(WrapDescription()) %></span>
        <table cellspacing="0">
        <asp:Repeater ID="InterfaceListRepeater" runat="server">
            <ItemTemplate>
                <tr class="<%# DefaultSanitizer.SanitizeHtml(GetInterfaceStatusText((DiscoveryInterfaceStatus)Eval("InterfaceStatus"))) %>NetObject">
                    <td>                        
                        <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId((int)Eval("InterfaceID"))) %>' 
                                type="checkbox" 
                                <%# ((bool)Eval("IsSelected")) ? "checked" : String.Empty %>
                                onclick='<%# DefaultSanitizer.SanitizeHtml("InterfaceSelectionChanged(this.checked,"+NodeID+","+ProfileID+","+Eval("InterfaceID")+");") %>' 
                                class="NetObjectSelector"
                                <%# SelectionEnabled ? String.Empty : "disabled" %> />
                    </td>
                    <td>
                        <img alt="<%# DefaultSanitizer.SanitizeHtml(GetInterfaceStatusText((DiscoveryInterfaceStatus)Eval("InterfaceStatus"))) %>" src='<%# DefaultSanitizer.SanitizeHtml(GetInterfaceStatusIcon((DiscoveryInterfaceStatus)Eval("InterfaceStatus"))) %>' />                        
                    </td>
                    <td>
                        <img alt="Type icon" src="/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Interfaces/<%# DefaultSanitizer.SanitizeHtml(Eval("InterfaceType")) %>.gif" />                        
                    </td>
                    <td class="wrapCell">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("InterfaceCaption")) %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="VolumeListRepeater" runat="server">
            <ItemTemplate>
                <tr class="<%# DefaultSanitizer.SanitizeHtml(GetVolumeStatusText((DiscoveryVolumeStatus)Eval("VolumeStatus"))) %>NetObject">
                    <td>
                        <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId((int)Eval("VolumeID"))) %>' 
                                type="checkbox" 
                                <%# ((bool)Eval("IsSelected")) ? "checked" : String.Empty %>
                                onclick='<%# DefaultSanitizer.SanitizeHtml("VolumeSelectionChanged(this.checked,"+NodeID+","+ProfileID+","+Eval("VolumeID")+");") %>' 
                                class="NetObjectSelector"
                                <%# SelectionEnabled ? String.Empty : "disabled" %> />
                    </td>
                    <td>
                        <img alt="<%# DefaultSanitizer.SanitizeHtml(GetVolumeStatusText((DiscoveryVolumeStatus)Eval("VolumeStatus"))) %>" src='<%# DefaultSanitizer.SanitizeHtml(GetVolumeStatusIcon((DiscoveryVolumeStatus)Eval("VolumeStatus"))) %>' />                        
                    </td>
                    <td>
                        <img alt="Type icon" src="/Orion/NetPerfMon/NetworkObjectIcon.aspx?src=/NetPerfmon/images/Volumes/<%# DefaultSanitizer.SanitizeHtml(Eval("VolumeType")) %>.gif" />
                    </td>
                    <td class="wrapCell">
                        <%# DefaultSanitizer.SanitizeHtml(Eval("VolumeDescription")) %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        <asp:Repeater ID="PluginsRepeater" runat="server">
            <ItemTemplate>                
                <asp:Repeater ID="PluginDataRepeater" runat="server" DataSource=' <%# DefaultSanitizer.SanitizeHtml(GetGroupItems(Container.DataItem)) %>' >
                <ItemTemplate>                                                              
                    <tr class="<%# DefaultSanitizer.SanitizeHtml(GetItemStatusText((IDiscoveredObject)Container.DataItem)) %>NetObject">
                        <td>

                             <input  id='<%# DefaultSanitizer.SanitizeHtml(GetId(Container)) %>' 
                                    type="checkbox" 
                                    <%# ((IDiscoveredObject)Container.DataItem).IsSelected ? "checked" : String.Empty %>
                                    onclick='<%# DefaultSanitizer.SanitizeHtml(GetOnClick(Container)) %>' 
                                    class="NetObjectSelector"
                                    <%# SelectionEnabled ? String.Empty : "disabled" %> />
                        </td>
                        <td>
                            <img alt="" src='<%# DefaultSanitizer.SanitizeHtml(GetItemStatusIcon((IDiscoveredObject)Container.DataItem)) %>' />
                        </td>
                        <td>
                            <img alt="Type Icon" src='<%# DefaultSanitizer.SanitizeHtml(GetItemTypeIcon((IDiscoveredObject)Container.DataItem)) %>' />
                        </td>
                        <td class="wrapCell">
                            <%# DefaultSanitizer.SanitizeHtml(((IDiscoveredObject)Container.DataItem).DisplayName) %>
                        </td>
                    </tr>
                    </ItemTemplate>
                </asp:Repeater>                
            </ItemTemplate>
        </asp:Repeater>        
        </table>
 </ContentTemplate>
</asp:UpdatePanel>
