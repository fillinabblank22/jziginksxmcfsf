<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscoveryNodePopup.aspx.cs" Inherits="Orion_Discovery_DiscoveryNodePopup" %>


</style>
<h3 class="StatusUp"><%= DefaultSanitizer.SanitizeHtml(HeaderText) %></h3>
<div runat="server" class="NetObjectTipBody">
    
    <asp:repeater id="InterfacesAndVolumes" runat="server">
      <HeaderTemplate>        
        <table class="tooltip" id="peripherals" cellspacing="1" cellpadding="1">
      </HeaderTemplate>
    <ItemTemplate>
    <tr>
        <td width="16px"><img src='<%# DefaultSanitizer.SanitizeHtml(Eval("StatusUrl")) %>' alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_55) %>" /></td>
        <td width="16px"><img src='<%# DefaultSanitizer.SanitizeHtml(Eval("Icon")) %>' alt="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_56) %>" /></td>
        <td>  <%# DefaultSanitizer.SanitizeHtml(((BindItem)Container.DataItem).FullDescription) %> </td>
    </tr>
    </ItemTemplate>
    <FooterTemplate>
    </table>
    </FooterTemplate>   
    </asp:repeater>
</div>
