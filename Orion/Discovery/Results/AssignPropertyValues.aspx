<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true"
    CodeFile="AssignPropertyValues.aspx.cs" Inherits="Orion_Discovery_Results_AssignPropertyValues" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_199%>"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_199) %></span>
    <br />
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_200) %>
    <table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton LocalizedText="Finish" DisplayType="Primary" OnClick="imgbFinish_Click" runat="server" ID="imgNext"/>
    </div>            
    </td></tr></table>
</asp:Content>
