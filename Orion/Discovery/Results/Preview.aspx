<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master"
    AutoEventWireup="true" CodeFile="Preview.aspx.cs" Inherits="Orion_Discovery_Results_Preview"
    Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_23 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/NodeList.ascx" TagPrefix="orion" TagName="NodeList" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
<style type="text/css">
    .NetObjectTable{width:860px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div style="width:870px;">
    <asp:Label ID="ActionLabel" runat="server" Text="" CssClass="ActionName"></asp:Label>
    <br />
    <% if (isNPMInstalled)
       {%>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_26) %>
    <%}
       else
       {%><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_27) %>
    <%} %>
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_28) %>
    <div id="duplicateNodesDiv" runat="server" visible="false">
        <p class="SectionTitle">
            <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_29) %></p>
        <asp:Panel ID="ConfigurationControls" runat="server" CssClass="blueBox">
            <asp:CheckBox ID="Duplicate" runat="server" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_30 %>" />
        </asp:Panel>
    </div>
    <asp:Panel ID="TopNavigation" runat="server">
        <table class="NavigationButtons">
            <tr>
                <td>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary" runat="server"
                            ID="imgTopBack" OnClick="imgbBack_Click" />
                        <orion:LocalizableButton LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_42 %>"
                            runat="server" ID="ibTopAddToIgnored" OnClick="ibAddToIgnored_Click" />
                        <orion:LocalizableButton LocalizedText="Import" DisplayType="Primary" runat="server"
                            ID="imgTopNext" OnClick="imgbNext_Click" />
                        <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server"
                            ID="imgTopCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div style="padding-left: 10px;">
        <asp:Panel ID="LicenseWarnings" runat="server" ForeColor="Red" />
    </div>
    </div>
    <div class="contentBlock">
        <orion:NodeList ID="NodeList" runat="server" style="min-width:860px;"/>
    </div>
    <div style="width:870px;">
    <asp:Panel ID="BottomNavigation" runat="server">
        <table class="NavigationButtons">
            <tr>
                <td>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary" runat="server"
                            ID="imgBack" OnClick="imgbBack_Click" />
                        <orion:LocalizableButton LocalizedText="CustomText" DisplayType="Secondary" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_42 %>"
                            runat="server" ID="ibAddToIgnored" OnClick="ibAddToIgnored_Click" />
                        <orion:LocalizableButton LocalizedText="Import" DisplayType="Primary" runat="server"
                            ID="imgbNext" OnClick="imgbNext_Click" />
                        <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server"
                            ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
                        <%-- <span style="float: left;">--%>
                        <%--  </span>--%>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </div>
</asp:Content>
