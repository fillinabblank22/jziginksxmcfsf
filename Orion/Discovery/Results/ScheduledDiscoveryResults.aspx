﻿<%@ Page Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_1 %>" Language="C#" MasterPageFile="~/Orion/Discovery/DiscoveryPage.master"
    AutoEventWireup="true" CodeFile="ScheduledDiscoveryResults.aspx.cs" Inherits="Orion_Discovery_Results_ScheduledDiscoveryResults" %>

<%@ Register Src="~/Orion/Discovery/Controls/DiscoveryPageHeader.ascx" TagPrefix="discovery"
    TagName="DiscoveryPageHeader" %>    
<%@ Register Src="~/Orion/Discovery/Controls/ScheduleDiscoveryResultsList.ascx" TagPrefix="discovery"
    TagName="ScheduleDiscoveryResultsList" %>
<%@ Register Src="~/Orion/Discovery/Controls/DiscoveryTabBar.ascx" TagPrefix="discovery" TagName="DiscoveryTabBar"%>
<%@ Register Src="~/Orion/Discovery/Controls/Search.ascx" TagPrefix="discovery" TagName="Search"%>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagPrefix="orion" TagName="IncludeExtJs" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Import Namespace="SolarWinds.Orion.Core.Models.OldDiscoveryModels" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
<orion:IncludeExtJs ID="IncludeExtJs1" runat="server" debug="false"/>
<%--<link rel="stylesheet" type="text/css" href="../../styles/Discovery.css" />--%>

<script type="text/javascript" language="javascript">
    $(function () {
        $(".selectedFilter").parent().css({ 'background-image': 'url(/Orion/Discovery/images/background/left_selection_gradient.gif)', 'background-repeat': 'repeat-x', 'background-color': '#97D6FF' });
        
        <%if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>

        $(".removeButton").click(function () {
            var anySelected = false;
            $(".Selector input[type=checkbox]").each(function () {
                if (this.checked) {
                    anySelected = true;
                }
            });
            $(".NetObjectSelector").each(function () {
                if (this.checked) {
                    anySelected = true;
                }
            });
            if (anySelected == false) {
                alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBCODE_VB0_1) %>");
                return false;
            }

            $('.TransparentBoxID').show();
        });

        <%} else {%>        
        $(".removeButton").bind('click', false);        
        <% } %>
    });  
</script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="TopRightPageLinks">
	<orion:IconLinkExportToPDF runat="server" />
	<orion:IconHelpButton runat="server" HelpUrlFragment="OrionPHDiscoveryHome" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<orion:TBox ID="modalityDiv" runat="server" Message="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_2 %>" DivVisible="false"
            MessageVisible="true" />
<div>

    <h1 class="sw-hdr-title"><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>

    <%-- 
    <discovery:DiscoveryPageHeader ID="discoveryPageHeader" runat="server" LinkBack="../../Admin"
        TitleBack="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_3 %>" />
    --%>
    <discovery:DiscoveryTabBar ID="DiscoveryTabBar" runat="server" Visible="true" />

    <div class="tab-top">
        <table class="NodeManagement" style="width: 100%">
            <tbody>
                <tr valign="top" align="left">
                    <td class="filterHeaderCell">                        
                        <div class="filterHeader" >
                            <div style="width: 180px"></div>
                            <div>
                                 <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_4) %>
                                 <br />
                                <asp:DropDownList Width="100%" ID="ddlStatus" runat="server" OnSelectedIndexChanged="ddlStatusChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div>
                                <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_5) %>
                                <br />
                                <asp:DropDownList Width="100%" ID="ddlFilterType" runat="server" OnSelectedIndexChanged="ddlFilterTypeChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <asp:Panel ID="panelFilters" runat="server" CssClass="filters" EnableViewState="false">
                            
                        </asp:Panel>
                    </td>
                    <td style="width: 950px">
                        <table style="border: solid 1px #dfdfde; width: 100%;">
                            <tr class="ButtonHeader">
                                <td style="padding-left: 10px;">
                                    <span class="toolbarButton">
                                        <asp:ImageButton ID="ImportNodesButton" runat="server" ImageUrl="~/Orion/Discovery/images/import_nodes.gif"
                                            CausesValidation="false" OnClick="ImportNodesButton_Click" />
                                        <asp:LinkButton ID="ImportNodesLinkButton" runat="server" CausesValidation="false" OnClick="ImportNodesButton_Click"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_6) %></asp:LinkButton>
                                    </span>
                                    <span id="addToIgnoreList" class="toolbarButton" style="<%= this.DiscoveryStatus.Contains(DiscoveryNodeStatus.Ignored) ? "display:none" : "display:inline" %>">
                                        <asp:ImageButton ID="AddToIgnoredButton" 
                                            runat="server" 
                                            ImageUrl="~/Orion/Discovery/images/ignore.gif" 
                                            OnClick="AddToIgnoredButton_Click" 
                                            class="removeButton"
                                            />
                                        <asp:LinkButton ID="AddToIgnoredLinkButton" 
                                            runat="server" 
                                            OnClick="AddToIgnoredButton_Click" 
                                            class="removeButton"
                                            ><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_7) %></asp:LinkButton>
                                    </span>
                                     <span id="searchForNodes" style="display: inline; float: right">
                                           <discovery:Search ID="SearchNodes" SelectorForSearch="[search=\'searchableRow\'] *" OnEnterKeyDown="RunSearchNodes_Click" OnSeacrhClicked="RunSearchNodes_Click" OnCleanClicked="RunCleanSearchNodes_Click" runat="server"/>
                                    </span>
                                    <span id="removeFromIgnoreList" class="toolbarButton"  style="<%= this.DiscoveryStatus.Contains(DiscoveryNodeStatus.Ignored) ? "display:inline" : "display:none" %>">
                                        <asp:ImageButton ID="RemoveFromIgnoredButton" runat="server" ImageUrl="~/Orion/Discovery/images/delete_16x16.gif" />
                                        <asp:LinkButton ID="RemoveFromIgnoredLinkButton" runat="server" OnClick="RemoveFromIgnoredLinButton_Click"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_8) %></asp:LinkButton>
                                    </span>
                                </td>
                            </tr>
                        </table>                       
                        <discovery:ScheduleDiscoveryResultsList ID="ScheduleDiscoveryResultsList" runat="server" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>	
</asp:Content>

