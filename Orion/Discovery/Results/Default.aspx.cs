using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Web;

public partial class Orion_Discovery_Results_Default : ResultsWizardBasePage, IStep
{
/*
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
*/

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);        
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        LicenseLimitNotification();
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);
		this.InitButtons(this.imgbCancel);
        this.imgbNext.Visible = !this.DeviceList.IsEmpty;
    }

    protected override bool ValidateUserInput()
	{
        if (this.DeviceList.IsEmpty)
        {
            return false;
        }

        if (!base.ValidateUserInput())
        {
            return false;
        }

        return true;
    }

	protected override void Next()
	{
        if (!ValidateUserInput())
        {
            return;
        }
    }
    

	#region IStep Members
	public string Step
	{
		get
		{
			return "Devices";
		}
	}
	#endregion

    #region Notification

    private void LicenseLimitNotification()
    {
        var featureManager = new FeatureManager();
        int nodesCount = SolarWinds.Orion.NPM.Web.Node.Count;
        int nodesMaxCount = featureManager.GetMaxElementCount(WellKnownElementTypes.Nodes);

        this.licenseLimitHint.Title = Resources.CoreWebContent.WEBDATA_ZS0_012;//"Not enough licenses available to import the selected nodes"
        this.licenseLimitHint.HintColumnTitle = Resources.CoreWebContent.WEBDATA_ZS0_013; //"You can unselect nodes that exceed the number remaining in your license, or..."

        this.licenseLimitHint.RemainingItemsCount = nodesMaxCount - nodesCount;

        this.licenseLimitHint.HintColumnRows = new Dictionary<string, string>() { { Resources.CoreWebContent.WEBDATA_ZS0_011, "/Orion/Nodes/Default.aspx?CustomPropertyId=Orion.Nodes:Nodes" } };
        this.licenseLimitHint.LicenseLimitInfoList = new Dictionary<string, string>() { { Resources.CoreWebContent.WEBDATA_ZS0_014, "{0}  {1}" + (nodesMaxCount - nodesCount) + Resources.CoreWebContent.WEBDATA_ZS0_015 + nodesMaxCount } };

    }

    #endregion
    
}
