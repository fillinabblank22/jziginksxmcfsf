using System;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;

public partial class Orion_Discovery_Results_Volumes : ResultsWizardBasePage, IStep
{
/*
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
*/

	protected override void Next()
	{
		if (!ValidateUserInput()) return;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        LicenseLimitNotification();
        InitButtons(imgbCancel);
    }
    

	#region IStep Members
	public string Step
	{
		get
		{
			return "Volumes";
		}
	}
	#endregion

    #region Notification

    private void LicenseLimitNotification()
    {
        var featureManager = new FeatureManager();
        int itemCount = SolarWinds.Orion.NPM.Web.Volume.Count;
        int itemMaxCount = featureManager.GetMaxElementCount(WellKnownElementTypes.Volumes);

        this.licenseLimitHint.Title = Resources.CoreWebContent.WEBDATA_ZS0_016;//"Not enough licenses available to import the selected volumes"
        this.licenseLimitHint.HintColumnTitle = Resources.CoreWebContent.WEBDATA_ZS0_022; //"You can unselect volumes that exceed the number remaining in your license, or..."

        this.licenseLimitHint.RemainingItemsCount = itemMaxCount - itemCount;
        this.licenseLimitHint.HideLicensePopupLink = itemMaxCount == 0;

        this.licenseLimitHint.HintColumnRows = new Dictionary<string, string>() { { Resources.CoreWebContent.WEBDATA_ZS0_006, "/Orion/Nodes/Default.aspx?CustomPropertyId=Orion.Nodes:Nodes" } };
        this.licenseLimitHint.LicenseLimitInfoList = new Dictionary<string, string>() { { Resources.CoreWebContent.WEBDATA_ZS0_017, "{0}  {1}" + (itemMaxCount - itemCount) + Resources.CoreWebContent.WEBDATA_ZS0_015 + itemMaxCount } };

    }

    #endregion
    
}
