using System;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using System.IO;
using System.Linq;

using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;

public class ImportInfo
{
    public bool ImportIsDone { get; set; }
    public string ResultText { get; set; }
    public int PercentComplete { get; set; }
    public int PhasePercentComplete { get; set; }
    public string PhaseName { get; set; }
}

public partial class Orion_Discovery_Results_Results : ResultsWizardBasePage, IStep
{
    private const int MAX_IMPORTED_NETOBJECTS = 5;
    private const int MAX_RESULTS_COUNT = 50;

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        imgNext.Visible = !string.IsNullOrEmpty(base.GetNextStepUrl);

        int nodesCount = 0;

        nodesCount =
            ResultsWorkflowHelper.DiscoveryResults
                .Select(
                    item => item.GetPluginResultOfType<CoreDiscoveryPluginResult>().DiscoveredNodes.Count)
                .Sum();            

        if (nodesCount == 0)
        {
            this.ResultsDiv.Controls.Add(new LiteralControl(Resources.CoreWebContent.WEBCODE_VB0_11));
        }
    }

    /// <summary> Takes nodes from result queue and imports them into db </summary>
    /// <returns>Import info</returns>
    [ScriptMethod]
    [WebMethod(true)]
    public static StartImportStatus ProcessNodes()
    {
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

        //TODO filter nodes according to selection
        try
        {
            StartImportStatus status = StartImportStatus.Started;
            ResultsWorkflowHelper.ImportEngineID = null;
            if (ResultsWorkflowHelper.DiscoveryResults.Any())
            {
                var engineId = ResultsWorkflowHelper.DiscoveryResults.First().EngineId;
                // Process individual results on engine where the result was created
                using (var profileBusinessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, engineId))
                {
                    foreach (var discoveryResult in ResultsWorkflowHelper.DiscoveryResults)
                    {
                        ResultsWorkflowHelper.ImportID = Guid.NewGuid();
                        ResultsWorkflowHelper.ImportEngineID = engineId;
                        ResultsWorkflowHelper.LastImportProgressUpdate = DateTime.MinValue;
                        status = profileBusinessProxy.ImportOrionDiscoveryResults(ResultsWorkflowHelper.ImportID, discoveryResult);
                    }
                }
            }

            return status;
        }
        catch (Exception ex)
        {
            log.Error("Cannot import Discovery Results.", ex);
            throw new ApplicationException("Cannot import Discovery Results. \n" + ex.Message, ex);
        }
        finally
        {
            // Process Global Status Update operation on Main poller
            using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                businessProxy.RequestScheduledDiscoveryNetObjectStatusUpdateAsync();
            }
        }
    }

    /// <summary> Takes nodes from result queue and imports them into db </summary>
    /// <returns>Import info</returns>
    [ScriptMethod]
    [WebMethod(true)]
    public static ImportInfo GetProgress()
    {
        if (ResultsWorkflowHelper.LastImportProgressUpdate.AddMilliseconds(500) > DateTime.Now)
        {
            return new ImportInfo() { ImportIsDone = false, ResultText = String.Empty, PercentComplete = -1 };
        }
        ResultsWorkflowHelper.LastImportProgressUpdate = DateTime.Now;
        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        var engineId = ResultsWorkflowHelper.ImportEngineID;
        if (!engineId.HasValue)
        {
            return new ImportInfo()
            {
                ImportIsDone = false,
                PhaseName = String.Empty,
                PhasePercentComplete = -1,
                PercentComplete = -1,
                ResultText = String.Empty
            };
        }

        DiscoveryImportProgressInfo importProgressInfo;
        try
        {
            using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler, engineId.Value))
            {
                importProgressInfo = businessProxy.GetOrionDiscoveryImportProgress(ResultsWorkflowHelper.ImportID);
            }
        }
        catch (Exception ex)
        {
            log.Error("Cannot get import results.", ex);
            throw new ApplicationException("Cannot get import results. \n" + ex.Message, ex);
        }

        // When we recieved null as pregress info it means there is nothing new
        if (importProgressInfo == null)
        {
            return new ImportInfo()
            {
                ImportIsDone = false,
                PhaseName = String.Empty,
                PhasePercentComplete = -1,
                PercentComplete = -1,
                ResultText = String.Empty
            };
        }

        StringBuilder resultTextBuilder = new StringBuilder();

        if (!String.IsNullOrEmpty(importProgressInfo.NewLogText))
        {
            using (StringReader reader = new StringReader(importProgressInfo.NewLogText))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    resultTextBuilder.AppendFormat("<span class=\"ResultItem\">{0}</span>", HttpUtility.HtmlEncode(line));
                }
            }
        }

        return new ImportInfo()
                   {
                       ImportIsDone = importProgressInfo.Finished,
                       PercentComplete = (int)importProgressInfo.OverallProgress,
                       ResultText = resultTextBuilder.ToString(),
                       PhaseName = importProgressInfo.PhaseName,
                       PhasePercentComplete = (int)importProgressInfo.PhaseProgress
                   };
    }

    /// <summary> Reload wizard with up to date results </summary>
    [ScriptMethod]
    [WebMethod(true)]
    public static string GetInitialPage()
    {
        return ResultsWorkflowHelper.InitialPageUrl;
    }

    protected override void Next()
    {
        if (!ValidateUserInput()) return;
    }

    protected void imgNext_Click(object sender, EventArgs e)
    {
        if (imgNext.Visible)
        {
            this.Response.Redirect(base.GetNextStepUrl);
        }
    }

    /// <summary> Called when "Finish" button is clicked </summary>
    protected void imgbFinish_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Return(ResultsWorkflowHelper.InitialPageUrl);
        //this.Response.Redirect(ResultsWorkflowHelper.InitialPageUrl);
    }

    #region IStep Members
    public string Step
    {
        get
        {
            return "Results";
        }
    }
    #endregion
}
