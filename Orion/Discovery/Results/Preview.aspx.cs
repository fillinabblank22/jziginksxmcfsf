using System;
using System.Data;
using System.Collections.Generic;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.ModuleManager;
using SolarWinds.Orion.Core.Models.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web.DAL;
using System.Linq;
using System.Web.UI.WebControls;
using System.Drawing;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Core.Models.Interfaces;
using SolarWinds.Orion.Web.Discovery;

public partial class Orion_Discovery_Results_Preview : ResultsWizardBasePage, IStep
{
    private string ActionTextWithoutEngine = Resources.CoreWebContent.WEBCODE_VB0_14;
    private string ActionTextWithEngine = Resources.CoreWebContent.WEBCODE_VB0_15;
    private string ErrorGettingLicensedElementsCount = Resources.CoreWebContent.WEBCODE_VB0_16;

    [Obsolete("Don't use this field - will be removed.", true)]
    public static bool isInterfacesAllowed = new RemoteFeatureManager().AllowInterfaces;

    protected static bool isNPMInstalled = ModuleManager.InstanceWithCache.IsThereModule("NPM");

    private string MainLicenseWarning = Resources.CoreWebContent.WEBCODE_VB0_20;
    private string HowManyLicensedMessage = Resources.CoreWebContent.WEBCODE_VB0_17;
    private string OverlappingMessage = Resources.CoreWebContent.WEBCODE_VB0_18;
    private string OnlySelectionOverMessage = Resources.CoreWebContent.WEBCODE_VB0_19;

    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();

    private bool EvaluationExpired()
    {
        var featureManager = new RemoteFeatureManager();
        return featureManager.GetMaxElementCount(WellKnownElementTypes.Nodes) <= 0;
    }

    protected List<ElementLicenseInfo> GetLicensedStatus(DiscoveryResultBase discoveryResult)
    {
        Dictionary<string, int> elementCounts;

        try
        {
            using (var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                elementCounts = proxy.GetElementsManagedCount();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Unhandled exception occured when getting license status.", ex);
        }

        List<ElementLicenseInfo> result = new List<ElementLicenseInfo>();

        try
        {
            var featureManager = new RemoteFeatureManager();
            Dictionary<string, int> elementMaxCounts = new Dictionary<string, int>();

            foreach (string element in elementCounts.Keys)
            {
                elementMaxCounts[element] = featureManager.GetMaxElementCount(element);
            }

            foreach (DiscoveryPluginResultBase pluginResult in discoveryResult.PluginResults)
            {
                List<ElementLicenseInfo> newInfos;

                if (!pluginResult.CheckLicensingStatusForImport(elementCounts, elementMaxCounts, out newInfos))
                {
                    result.AddRange(newInfos);
                }
            }

            return result;
        }
        catch (Exception ex)
        {
            var exception =
                new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(
                    () => CoreWebContent.WEBCODE_VB0_351, ex);
            log.Error(exception.Message,exception.InnerException);
            throw exception;
        }
    }

    private WebControl GetRedLabel(string message)
    {
        Label label = new Label()
        {
            ForeColor = Color.Red,
            Height = 12,
            Text = message
        };
        label.Style[System.Web.UI.HtmlTextWriterStyle.Display] = "inline";
        return label;
    }

    /// <summary>
    /// Create copy of actual results, update selected nodes from UI to this copy, performs fitlering
    /// of plugin results and return.
    /// 
    /// This is usefull for keeping original data object in memory.
    /// </summary>
    /// <returns></returns>
    private DiscoveryResultBase[] GetResultsCopyForImport()
    {
        // we need original and copy link together for later usage
        var resCopy = ResultsWorkflowHelper.DiscoveryResults
            .Select(item => item.Copy())
            .ToArray();        

        // we store actual selection to copy of data)
        this.NodeList.StoreSelectedNodes(resCopy);              

        // and perform filtering by plugins
        foreach (var discoveryResult in resCopy)
        {                        
            var copy = discoveryResult;

            // we must linearize plugin results 
            var linearizator = new PluginResultLinearizationHelper();
            var ordered = linearizator.Linearize(copy.PluginResults);

            foreach (var pluginResult in ordered)
            {
                var contextFilter = pluginResult as IDiscoveryPluginResultContextFiltering;
                DiscoveryPluginResultBase filtered;

                if (contextFilter != null)
                {
                    // filtering needs context information
                    filtered = contextFilter.GetFilteredPluginResultFromContext(copy);
                }
                else
                {
                    filtered = pluginResult.GetFilteredPluginResult();
                }

                copy.PluginResults.Remove(pluginResult);
                copy.PluginResults.Add(filtered);
            }
        }

        return resCopy.ToArray();
    }

    protected override bool ValidateUserInput()
    {
        if (!base.ValidateUserInput())
        {
            return false;
        }

        if (EvaluationExpired())
        {
            this.LicenseWarnings.Controls.Add(GetRedLabel(ErrorGettingLicensedElementsCount));
            return false;
        }

        // we need actual data from UI
        // testing will be performed on copy of primary data objects because of error possibility - in such case
        // we must return to previous state
        DiscoveryResultBase[] resultsCopy = GetResultsCopyForImport();

        // let's count it for all results independently
        List<ElementLicenseInfo> fromAllResults = resultsCopy
            .SelectMany(item => GetLicensedStatus(item))
            .ToList();

        Dictionary<string,int> managedElementsCount = new Dictionary<string, int>();
        try
        {
            using(var proxy = _blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                managedElementsCount = proxy.GetAlreadyManagedElementCount(resultsCopy.ToList());
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Unhandled exception occured when getting managed element counts", ex);
        }
        

        // and now group them by type
        var byName = fromAllResults.GroupBy(item => item.Name);        

        List<ElementLicenseInfo> exceededElements =
            byName
                .Select(typeGroup =>
                            {
                                var managed = typeGroup.First().ManagedCount;
                                var max = typeGroup.First().MaxCount;
                                int managedDuplicate = 0;
                                managedElementsCount.TryGetValue(typeGroup.Key, out managedDuplicate);
                                var res = new ElementLicenseInfo(
                                    typeGroup.Key,
                                    managed,
                                    typeGroup.Select(item => item.Count - managed).Sum() + managed - managedDuplicate,
                                    max);
                                return res;
                            })
                .ToList();

        var exceededElementsFiltered  =  exceededElements.Where(el => el.Count > el.MaxCount);

        if (exceededElementsFiltered.ToList().Count > 0)
        {
            foreach (ElementLicenseInfo info in exceededElementsFiltered)
            {
                Panel warningDiv = new Panel();
                warningDiv.Controls.Add(GetRedLabel(String.Format(MainLicenseWarning, info.Name)));
                warningDiv.Controls.Add(new Literal() { Text = "<br />" });
                warningDiv.Controls.Add(GetRedLabel(String.Format(HowManyLicensedMessage, info.MaxCount, info.Name)));
                warningDiv.Controls.Add(new Literal() { Text = "<br />" });

                warningDiv.Style[System.Web.UI.HtmlTextWriterStyle.PaddingBottom] = "10px";
                warningDiv.Style[System.Web.UI.HtmlTextWriterStyle.Height] = "auto";

                if (info.ManagedCount > info.MaxCount)
                {
                    // You are already {0} {1} over the limit allowed by your license, and you have selected to import {2} additional {1}.
                    // info.Count: count after import, info.MaxCount: licensed count
                    warningDiv.Controls.Add(GetRedLabel(String.Format(OverlappingMessage, info.ExceededByAlready, info.Name,
                                                                      info.Count - info.MaxCount- info.ExceededByAlready)));
                }
                else
                {
                    warningDiv.Controls.Add(GetRedLabel(String.Format(OnlySelectionOverMessage, info.ExceededBy, info.Name)));
                }

                warningDiv.Controls.Add(new Literal() { Text = "<br/>" });
                warningDiv.Controls.Add(GetRedLabel(ComposeCorrectErrorMsgWithLinks(info)));

                Table t = new Table();
                TableRow r = new TableRow();

                TableCell c1 = new TableCell();
                c1.Controls.Add(new System.Web.UI.WebControls.Image() { ImageUrl = "/Orion/images/failed_16x16.gif" });

                TableCell c2 = new TableCell();
                c2.Controls.Add(warningDiv);

                r.Controls.Add(c1);
                r.Controls.Add(c2);
                t.Controls.Add(r);
                this.LicenseWarnings.Controls.Add(t);
            }

            return false;
        }

        return true;
    }

    internal static string ComposeCorrectErrorMsgWithLinks(ElementLicenseInfo info)
    {
        string purchaseLicenseLink = String.Format("<a href=\"{0}products/orion/upgrade/upgrade.aspx\" class=\"redLink\">{1}</a>", Resources.CoreWebContent.SolarWindsComLocUrl, Resources.CoreWebContent.WEBDATA_MZ0_8);
        string removeElementsLink = String.Format("<a href=\"/Orion/Nodes/Default.aspx\" class=\"redLink\">{0}</a>", String.Format(Resources.CoreWebContent.WEBDATA_MZ0_7, info.Name));
        return String.Format(Resources.CoreWebContent.WEBDATA_MZ0_6, info.Name, removeElementsLink, purchaseLicenseLink);
    }

    protected override void Next()
    {
        // now we know everything is ok, we get final nodes prepared to import, store them into primary
        // data obejct and start import

        var results = this.GetResultsCopyForImport();
        ResultsWorkflowHelper.DiscoveryResults.Clear();
        ResultsWorkflowHelper.DiscoveryResults.AddRange(results);

        // we inform all steps about change
        ResultsWorkflowHelper.IgnoreListChanges = new IgnoreListInfo(Guid.NewGuid());

        ImportNodes();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        InitButtons(imgbCancel);
        InitButtons(imgTopCancel);

        using (WebDAL dal = new WebDAL())
        {
            if (dal.PrimaryEnginesCount() > 1)
            {
                // when more than one polling engine is present, allow to not import
                // nodes that are discovered but already monitored by another polling
                // engine
                this.duplicateNodesDiv.Visible = true;
            }
        }

        if (!IsPostBack)
        {
            this.Duplicate.Checked = ResultsWorkflowHelper.ImportDuplicateIP;
        }
        else
        {
            ResultsWorkflowHelper.ImportDuplicateIP = this.Duplicate.Checked;
        }

        string engineName = GetEngineName();

        if (String.IsNullOrEmpty(engineName))
        {
            this.ActionLabel.Text = ActionTextWithoutEngine;
        }
        else
        {
            this.ActionLabel.Text = String.Format(ActionTextWithEngine, engineName);
        }

        int totalCount = 0;
        var totalTypeCount = ResultsWorkflowHelper.DiscoveryResults
            .Select(item => item.GetPluginResultOfType<CoreDiscoveryPluginResult>().DiscoveredNodes);

        foreach (var result in totalTypeCount)
            totalCount += result.Count;

        TopNavigation.Visible = totalCount > 20;
    }

    private void ImportNodes()
    {
        foreach (var discoveryResultInfo in ResultsWorkflowHelper.DiscoveryResults)
        {
            discoveryResultInfo.DuplicateNodesFromOtherPollingEngines = ResultsWorkflowHelper.ImportDuplicateIP;
        }
        // redirect to results page to start processing
        Response.Redirect(ResultsWorkflowHelper.GetNextStepUrl(), true);
    }

    // gets a name of polling engine under which was the discovery performed
    protected string GetEngineName()
    {
        if (ResultsWorkflowHelper.DiscoveryResultInfo.Count != 1)
            return String.Empty;

        DiscoveryConfiguration cfg = LoadDiscoveryConfiguration(ResultsWorkflowHelper.DiscoveryResultInfo[0].ProfileID);

        if (cfg == null)
        {
            return String.Empty;
        }

        DataTable engines = null;

        using (WebDAL informationService = new WebDAL())
        {
            engines = informationService.GetPollingEngineStatus();
        }

        if (engines == null || engines.Rows.Count == 0)
        {
            return String.Empty;
        }

        foreach (DataRow row in engines.Rows)
        {
            if ((int)row["EngineID"] == cfg.EngineID)
            {
                return (string)row["ServerName"] ?? String.Empty;
            }
        }

        return String.Empty;
    }


    protected void ibAddToIgnored_Click(object sender, EventArgs e)
    {
        // get selected nodes and all nodes from list
        var allNodes = this.NodeList.GetActualNodes();
        var selected = this.NodeList.GetSelectedNodes();

        // selected must be ignored
        ResultsWorkflowHelper.AddCompletelyToIgnoreList(selected);

        // we must do new filter of result against new ignored items
        ResultsWorkflowHelper.FilterResultByIgnoreList();

        // and deselected - that means we must select all from table and then substract those which were ignored
        allNodes.ForEach(item => item.Item1.IsSelected = true);
        selected.ForEach(item => item.IsSelected = false);

        // lets redirect back to same page to show only valid nodes
        Response.Redirect(this.Request.UrlReferrer.AbsolutePath);
    }

    protected static DiscoveryConfiguration LoadDiscoveryConfiguration(int profileID)
    {
        if (profileID < 0)
        {
            ConfigWorkflowHelper.FillDiscoveryConfiguration();
            return null;
        }

        ICoreBusinessLayerProxyCreator blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
        DiscoveryConfiguration configuration = null;

        try
        {
            using (var businessProxy = blProxyCreator.Create(BusinessLayerExceptionHandler))
            {
                configuration = businessProxy.GetDiscoveryConfigurationByProfile(profileID);
            }
        }
        catch (Exception ex)
        {
            var exception =
                new SolarWinds.Internationalization.Exceptions.LocalizedExceptionBase(
                    () => CoreWebContent.WEBCODE_VB0_352, ex);
            log.Error(exception.Message, exception.InnerException);
            throw exception;
        }

        if (configuration == null)
        {
            log.Error("Discovery configuration cannot be null.");
            throw new ArgumentNullException("discoveryConfigurationInfo");
        }

        return configuration;
    }

    #region IStep Members
    public string Step
    {
        get
        {
            return "Preview";
        }
    }
    #endregion
}
