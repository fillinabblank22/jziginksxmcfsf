<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Orion_Discovery_Results_Default" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_23 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/DeviceList.ascx" TagPrefix="orion" TagName="DeviceList" %>
<%@ Register TagPrefix="orion" TagName="LicenseLimitControl" Src="~/Orion/Controls/LicenseLimitControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
   <%  
 Response.ExpiresAbsolute = DateTime.Now;  
 Response.Expires = 0;  
 Response.CacheControl = "no-cache";  
 Response.Buffer = true;  
 Response.Cache.SetCacheability(HttpCacheability.NoCache);  
 Response.Cache.SetExpires(DateTime.UtcNow);  
 Response.Cache.SetNoStore();  
 Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);  
%> 
    <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_33) %></span>
    <br />
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_34) %>
	<orion:LicenseLimitControl runat="server" id="licenseLimitHint" Margin="5px 0px 10px 0px;" />  	
	<div class="contentBlock">
	    <orion:DeviceList ID="DeviceList" runat="server" />
	</div>
	
	<table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary" runat="server" ID="imgbNext" OnClick="imgbNext_Click"/>
                <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
    </div>
    </td></tr></table>    
    
</asp:Content>


