<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs"
    Inherits="Orion_Discovery_Results_ProgressIndicator" %>

<orion:Include runat="server" File="ProgressIndicator.css" />

<div class="ProgressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>
