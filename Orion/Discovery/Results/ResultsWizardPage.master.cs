using System;
using System.Web.UI;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Collections.Generic;
using SolarWinds.Orion.NPM.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.UI;


public partial class Orion_Discovery_Results_ResultsWizardPage : MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CheckRights();

        if (ResultsWorkflowHelper.Steps == null || ResultsWorkflowHelper.Steps.Count == 0)
        {
            ResultsWorkflowHelper.LoadWizardSteps(new ResultsWizardStepsManager());
            if (this.Session["ResultImportReturnUrl"] != null)
            {
                ResultsWorkflowHelper.InitialPageUrl = this.Session["ResultImportReturnUrl"].ToString();
            }
            else if (Page.Request.UrlReferrer != null)
            {
                ResultsWorkflowHelper.InitialPageUrl = Page.Request.UrlReferrer.AbsolutePath.ToString();
            }
            else
            {
                ResultsWorkflowHelper.InitialPageUrl = "/Orion";
            }
        }
        this.ProgressIndicator1.Reload();
        AdminSiteMapRenderer.OverrideBreadcrumbURL = "~/Orion/Admin/DiscoveryCentral.aspx";
    }

    // <summary> Check user rights. If they are not sufficient, then redirects to error page. </summary>
    private void CheckRights()
    {
        if (OrionConfiguration.IsDemoServer || !Profile.AllowNodeManagement)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_3), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });    
        }
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }
}
