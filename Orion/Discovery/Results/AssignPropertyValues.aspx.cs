﻿using System;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Discovery_Results_AssignPropertyValues : ResultsWizardBasePage, IStep
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void Next()
    {
        if (!ValidateUserInput()) return;
    }

    protected void imgbFinish_Click(object sender, EventArgs e)
    {
        DiscoveryCentralHelper.Return(ResultsWorkflowHelper.InitialPageUrl);
    }

    public string Step
    {
        get { return "AssignPropertyValues"; }
    }
}