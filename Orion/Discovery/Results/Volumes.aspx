<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true" CodeFile="Volumes.aspx.cs" Inherits="Orion_Discovery_Results_Volumes" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_23 %>" %>

<%@ Register Src="~/Orion/Discovery/Controls/VolumeList.ascx" TagPrefix="orion" TagName="VolumeList" %>
<%@ Register TagPrefix="orion" TagName="LicenseLimitControl" Src="~/Orion/Controls/LicenseLimitControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_21) %></span>
    <br />
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_22) %>
	
    <orion:LicenseLimitControl runat="server" id="licenseLimitHint" Margin="5px 0px 10px 0px;" />  	
    	
	<div class="contentBlock">
	    <orion:VolumeList ID="VolumeList" runat="server" />
	</div>
	
	<table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary" runat="server" ID="imgBack" OnClick="imgbBack_Click" />
                    <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary"  runat="server" ID="imgbNext" OnClick="imgbNext_Click" />
                    <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server" ID="imgbCancel" OnClick="imgbCancel_Click" CausesValidation="false" />
    </div>
    </td></tr></table>    
</asp:Content>
