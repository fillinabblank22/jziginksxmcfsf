﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Logging;
using SolarWinds.Orion.Core.Common.Enums;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Core.Web.Discovery;
using SolarWinds.Orion.Core.Models.OldDiscoveryModels;
using SolarWinds.Common.Extensions;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

public partial class Orion_Discovery_Results_ScheduledDiscoveryResults : System.Web.UI.Page
{
    private string CHOOSE_NODE_ALERT = Resources.CoreWebContent.WEBCODE_VB0_2;
    private static readonly Log log = new Log();
    private readonly ICoreBusinessLayerProxyCreator _blProxyCreator = CoreBusinessLayerProxyCreatorFactory.GetCreator();
	private bool selectAgain = false;

    #region Properties    

    private string SelectedFilterValue
    {
        get
        {
            if (ViewState["SelectedFilterValue"] == null)        
                return null;

            return ViewState["SelectedFilterValue"].ToString();
        }

        set
        {
            ViewState["SelectedFilterValue"]=value;
        }
    }
    
    protected DiscoveryNodeStatus DiscoveryStatus
    {
        get
        {
            if (ddlStatus.SelectedItem == null)
                throw new NotSupportedException("Discovery status has to be selected");

            DiscoveryNodeStatus status = (DiscoveryNodeStatus)Convert.ToInt32(ddlStatus.SelectedValue);            
            return status;
        }
    }

    protected DiscoveryResultsFilterType DiscoveryFilterType
    {
        get
        {
            if (ddlFilterType.SelectedItem == null)
                throw new NotSupportedException("Discovery filter type has to be selected");

            return (DiscoveryResultsFilterType)Convert.ToInt32(ddlFilterType.SelectedValue);
        }
    }

    #endregion

    #region Page event handlers    

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // demoedit
        // added 
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
        {
            AddToIgnoredLinkButton.OnClientClick = "demoAction('Core_DiscoveryResults_Ignore',this); return false;";
            AddToIgnoredButton.OnClientClick = "demoAction('Core_DiscoveryResults_Ignore',this); return false;";
            ImportNodesButton.OnClientClick = "demoAction('Core_DiscoveryResults_Import',this); return false;";
            ImportNodesLinkButton.OnClientClick = "demoAction('Core_DiscoveryResults_Import',this); return false;";
        }
        // / demoedit
    }

    protected override void OnLoad(EventArgs e)
    {        
        base.OnLoad(e);
        OrionInclude.CoreFile("OrionMaster.js").AddJsInit("$(function() { SW.Core.Header.SyncTo($('.tab-top')); });");
        if (!IsPostBack)
        {
            ClearResults();
            InitDropDownLists();
            ParseQuery();            
        }

        GenerateFilters();

        if (!IsPostBack)
        {
            ScheduleDiscoveryHelper.Search = null;
            DisplayAllResults();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        foreach (Control control in this.panelFilters.Controls)
        {
            LinkButton lb = control as LinkButton;
            if (lb != null)
            {
                if (lb.CommandArgument == SelectedFilterValue)
                    lb.CssClass = "selectedFilter";
                else
                    lb.CssClass = "filter";                
            }
        }
    }

	protected void ImportNodesButton_Click(object sender, EventArgs e)
	{	    
        List<DiscoveryResult> discoveryResults = new List<DiscoveryResult>();
       
        List<DiscoveryNode> selectedScheduleDiscoveryResults = GetSelectedNetObjects();

        // check if any netobject is selected
        if (selectedScheduleDiscoveryResults.Count < 1)
        {
            RegisterStartupScriptAlert(CHOOSE_NODE_ALERT);
            return;
        }

        ResultsWorkflowHelper.Reset();

        // here we need prepare results from plugin - but actually this is not plugable
        // plugins get old results and they must transform them to new one
        for(int i=0;i<selectedScheduleDiscoveryResults.Count;i++)            
        {
            DiscoveryNode discoveryNode = selectedScheduleDiscoveryResults[i];

            if (!discoveryNode.InterfacesVolumesLoaded)
            {
                using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { log.Error(ex); }))
                {
                    discoveryNode = proxy.GetVolumesAndInterfacesForDiscoveryNode(discoveryNode);
                }
            }        

            DiscoveryResult discoveryResult = discoveryResults.Find(d => d.ProfileID == discoveryNode.ProfileID);
            if (discoveryResult == null)
            {
                discoveryResult = new DiscoveryResult(discoveryNode.ProfileID) {EngineId = discoveryNode.EngineID};
                discoveryResults.Add(discoveryResult);
            }

            discoveryResult.DiscoveryNodes.Add(discoveryNode);
        }

        // now we convert them to new items
        using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { log.Error(ex); }))
        {
            discoveryResults = proxy.ConvertScheduledDiscoveryResults(discoveryResults);
        }

        // put them to import wizard
        ResultsWorkflowHelper.LoadDiscoveryResults(discoveryResults);
        
        // and show it
        Response.Redirect("~/Orion/Discovery/Results/Default.aspx");
    }  

    protected void AddToIgnoredButton_Click(object sender, EventArgs e)
    {        
        string message = String.Empty;
        bool success = true;
        int ignoredNodesCount = 0;
        int ignoredInterfacesCount = 0;
        int ignoredVolumesCount = 0;
        
        List<DiscoveryItemGroupDefinition> definitions = null;
        Dictionary<Guid, int> ignoredItemsCount = null;

        // get definitions 
        if (ScheduleDiscoveryHelper.ScheduleDiscoveryResults.Count > 0)
        {
            definitions = ScheduleDiscoveryHelper.ScheduleDiscoveryResults[0].GroupsDefinition;
            ignoredItemsCount = new Dictionary<Guid, int>(definitions.ToDictionary( n => n.GroupID, k => (int)0));
        }

        List<DiscoveryNode> selectedDiscoveryNodes = GetSelectedNetObjects();        

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            message = Resources.CoreWebContent.WEBCODE_VB0_3;
        }
        else if (selectedDiscoveryNodes.Count < 1)
        {
            message = Resources.CoreWebContent.WEBCODE_VB0_4;
        }
        else
        {
            try
            {
                using (var proxy = _blProxyCreator.Create(delegate(Exception ex) { log.Error(ex); }))
                {                    
                    foreach (DiscoveryNode discoveryNode in selectedDiscoveryNodes)
                    {
                        if (!success) break;

                        // only selected items will be added
                        message = proxy.AddDiscoveryIgnoredNode(discoveryNode);
                        success = String.IsNullOrEmpty(message);

                        if (success)
                        {
                            if (discoveryNode.IsSelected)
                            {
                                ++ignoredNodesCount;
                            }
                            else
                            {
                                ignoredInterfacesCount += discoveryNode.Interfaces.Count;
                                ignoredVolumesCount += discoveryNode.Volumes.Count;


                                // how many of each was added
                                discoveryNode.NodeResult.PluginResults
                                    .SelectMany(n => n.GetDiscoveredObjects())
                                    .Where(n => n.IsSelected)
                                    .Select(n => definitions.First(k => k.Group.IsMyGroupedObjectType(n)))
                                    .GroupBy(n => n.GroupID)
                                    .Iterate(n => ignoredItemsCount[n.Key] += n.Count());                                
                            }
                        }
                    }                    
                }

                if (success)
                {                    
                    StringBuilder sb = new StringBuilder();
                    sb.Append(Resources.CoreWebContent.WEBCODE_VB0_137);
                    sb.Append("\n");
                    if (ignoredNodesCount > 0)
                    {
                        if (ignoredNodesCount > 1)
                        {
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_139, ignoredNodesCount); 
                        }
                        else
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_138, ignoredNodesCount);
                        sb.Append("\n");
                    }
                    if (ignoredInterfacesCount > 0)
                    {

                        if (ignoredInterfacesCount > 1)
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_141, ignoredInterfacesCount);
                        else
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_140, ignoredInterfacesCount);
                        sb.Append("\n");
                    }
                    if (ignoredVolumesCount > 0)
                    {

                        if (ignoredVolumesCount > 1)
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_143, ignoredVolumesCount); 
                        else
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_VB0_142, ignoredVolumesCount);
                        sb.Append("\n");
                    }

                    if (definitions != null)
                    {
                        foreach (var group in definitions)
                        {
                            var count = ignoredItemsCount[group.GroupID];

                            if (count == 0)
                                continue;

                            // ???: localization depends on order??
                            sb.AppendFormat(Resources.CoreWebContent.WEBCODE_ET_08, count, group.Group.DisplayName);
                            sb.Append("\n");
                        }
                    }

                    message = ControlHelper.EncodeJsString(sb.ToString());                        
                }
            }
            catch (Exception ex)
            {
                log.Error("Unable to add nodes to the Ignore List", ex);
                throw new ApplicationException("Unable to add nodes to the Discovery Ignore List");
            }           
        }
        LoadResults();
        RegisterStartupScriptAlert(message);
    }

    protected void RemoveFromIgnoredLinButton_Click(object sender, EventArgs e)
    {
        string message = ScheduleDiscoveryHelper.UnIgnoreSelectedNodes();
        LoadResults();
        RegisterStartupScriptAlert(message);
    }

    void lb_Click(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;        

        this.SelectedFilterValue = lb.CommandArgument;
        ScheduleDiscoveryHelper.Group = this.SelectedFilterValue;
        LoadResults();
    }


    protected void ddlFilterTypeChanged(object sender, EventArgs e)
    {
        ScheduleDiscoveryHelper.GroupBy = ddlFilterType.SelectedValue;
        ScheduleDiscoveryHelper.Group = null;
        DisplayAllResults();       
    }
    
    protected void ddlStatusChanged(object sender, EventArgs e)
    {
        ScheduleDiscoveryHelper.Status = ddlStatus.SelectedValue;
        DisplayAllResults();        
    }

    protected void RunSearchNodes_Click(object sender, EventArgs e)
    {
        string cleanSearch = WebSecurityHelper.SanitizeAngular(SearchNodes.SearchString);
		if (SearchNodes.SearchString == cleanSearch)
		{
            ScheduleDiscoveryHelper.Search = SearchNodes.SearchString;
            SearchNodes.SetVisibleCleanSearch(true);
            DisplayAllResults();
		}
		else
		{
			RunCleanSearchNodes_Click(null, null);
		}
    }

    protected void RunCleanSearchNodes_Click(object sender, EventArgs e)
    {
        ScheduleDiscoveryHelper.Search = null;
        SearchNodes.SetVisibleCleanSearch(false);
        SearchNodes.ClearSerachField();
        DisplayAllResults();
    }

    #endregion

    #region Data manipulation methods

    protected void LoadResults()
    {
        this.ScheduleDiscoveryResultsList.BindData(this.DiscoveryStatus, this.DiscoveryFilterType, SelectedFilterValue);
    }

    protected void ClearResults()
    {
        ScheduleDiscoveryHelper.ScheduleDiscoveryResults = null;
        ScheduleDiscoveryHelper.ThereIsMoreNodes = false;
    }

    protected void DisplayAllResults()
    {
        if (ddlFilterType.SelectedValue == "0" || selectAgain==true)
        {
            LoadResults();
        }
        else
        {
            ClearResults();
        }        
    }    

    #endregion

    #region Utility methods

    private List<DiscoveryNode> GetSelectedNetObjects()
    {        
        // get selected nodes        
        var selectedNodes =
            from dn in ScheduleDiscoveryHelper.ScheduleDiscoveryResults
            where dn.DiscoveryNode.IsSelected == true
            select dn.DiscoveryNode.Clone();

        List<DiscoveryNode> selectedScheduleDiscoveryResults = selectedNodes.ToList<DiscoveryNode>();

        // mark all childs as selected
        selectedScheduleDiscoveryResults
            .Where( n => n.NodeResult != null)
            .SelectMany(n => n.NodeResult.PluginResults)
            .SelectMany(n => n.GetDiscoveredObjects())
            .Iterate(n => n.IsSelected = true);

        // get selected interfaces
        var selectedInterfaces =
            from n in ScheduleDiscoveryHelper.ScheduleDiscoveryResults
            where n.DiscoveryNode.IsSelected == false
            from i in n.DiscoveryNode.Interfaces
            where i.IsSelected == true
            select new
            {
                discoveryNode = n.DiscoveryNode,
                discoveryInterface = i
            };

        foreach (var selectedInterface in selectedInterfaces)
        {
            DiscoveryNode discoveryNode =
                selectedScheduleDiscoveryResults.Find(n => n.NodeID == selectedInterface.discoveryNode.NodeID && n.ProfileID == selectedInterface.discoveryNode.ProfileID);

            // nodeis isn't yet in "to import" list
            if (discoveryNode == null)
            {
                discoveryNode = selectedInterface.discoveryNode.Clone();
                discoveryNode.Interfaces.Clear();
                discoveryNode.Volumes.Clear();
                selectedScheduleDiscoveryResults.Add(discoveryNode);
            }

            discoveryNode.Interfaces.Add(selectedInterface.discoveryInterface);
        }

        //get selected volumes
        var selectedVolumes =
            from n in ScheduleDiscoveryHelper.ScheduleDiscoveryResults
            where n.DiscoveryNode.IsSelected == false
            from v in n.DiscoveryNode.Volumes
            where v.IsSelected == true
            select new
            {
                discoveryNode = n.DiscoveryNode,
                discoveryVolume = v
            };

        foreach (var selectedVolume in selectedVolumes)
        {
            DiscoveryNode discoveryNode =
                selectedScheduleDiscoveryResults.Find(n => n.NodeID == selectedVolume.discoveryNode.NodeID && n.ProfileID == selectedVolume.discoveryNode.ProfileID);

            // nodeis isn't yet in "to import" list
            if (discoveryNode == null)
            {
                discoveryNode = selectedVolume.discoveryNode.Clone();
                discoveryNode.Volumes.Clear();
                discoveryNode.Interfaces.Clear();
                selectedScheduleDiscoveryResults.Add(discoveryNode);
            }

            discoveryNode.Volumes.Add(selectedVolume.discoveryVolume);
        }


        // get selected plugin data
        var selectedItems =
            (from n in ScheduleDiscoveryHelper.ScheduleDiscoveryResults
             where n.DiscoveryNode.IsSelected == false && n.DiscoveryNode.NodeResult != null
             from i in n.DiscoveryNode.NodeResult.PluginResults
             from k in i.GetDiscoveredObjects()
             where k.IsSelected == true
             select new
             {
                 discoveryNode = n.DiscoveryNode,
                 item = k,
             }).ToList();

        foreach (var selectedItem in selectedItems)
        {
            DiscoveryNode discoveryNode =
               selectedScheduleDiscoveryResults.Find(n => n.NodeID == selectedItem.discoveryNode.NodeID && n.ProfileID == selectedItem.discoveryNode.ProfileID);

            // nodeis isn't yet in "to import" list
            if (discoveryNode == null)
            {
                discoveryNode = selectedItem.discoveryNode.Clone();                
                discoveryNode.Volumes.Clear();
                discoveryNode.Interfaces.Clear();
                selectedScheduleDiscoveryResults.Add(discoveryNode);
            }

            // ?? some additional logic??
        }

        return selectedScheduleDiscoveryResults;
    }

    private void ParseQuery()
    {
        if (Request["Status"] != null)
        {
            if (ddlStatus.Items.FindByValue(Request["Status"]) != null)
            {
                ddlStatus.SelectedValue = Request["Status"];
                ScheduleDiscoveryHelper.Status = Request["Status"];

                ddlFilterType.SelectedValue = ((int)DiscoveryResultsFilterType.Nothing).ToString();
                ScheduleDiscoveryHelper.GroupBy = ddlFilterType.SelectedValue;

                ScheduleDiscoveryHelper.Group = null;
            }
            else
                throw new ApplicationException(String.Format("Value {0} is not supported Status", Request["Status"]));

            

            return;
        }        

        if (ScheduleDiscoveryHelper.Status != null)
        {
            if (ddlStatus.Items.FindByValue(ScheduleDiscoveryHelper.Status) != null)
            {
                ddlStatus.SelectedValue = ScheduleDiscoveryHelper.Status;
            }
        }

        if (ScheduleDiscoveryHelper.GroupBy != null)
        {
            if (ddlFilterType.Items.FindByValue(ScheduleDiscoveryHelper.GroupBy) != null)
            {
                ddlFilterType.SelectedValue = ScheduleDiscoveryHelper.GroupBy;
            }
        }
        if (ScheduleDiscoveryHelper.Group != null)
        {
            SelectedFilterValue = ScheduleDiscoveryHelper.Group;
            selectAgain = true;
        }                   
    }

    private void RegisterStartupScriptAlert(string message)
    {
        string script = @"<script language=JavaScript>alert('" + (message) + "');</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Info", script);
    }

    protected void InitDropDownLists()
    {
        //DropDownList for type of Nodes (Ignored, NotImported, Imported)
        foreach (DiscoveryNodeStatus discoveryNodeStatus in ScheduleDiscoveryHelper.StatusList)
        {
            this.ddlStatus.Items.Add( 
                new ListItem(ScheduleDiscoveryHelper.GetStatusText(discoveryNodeStatus),((int)discoveryNodeStatus).ToString()));
        }        


        //DropDownList for filter type {}
        this.ddlFilterType.Items.Add(new ListItem(Resources.CoreWebContent.WEBCODE_SO0_8, ((int)DiscoveryResultsFilterType.Nothing).ToString())); // TODO: [localization] {used shared resource}   
        this.ddlFilterType.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_17, ((int)DiscoveryResultsFilterType.DateFound).ToString()));// TODO: [localization] {used shared resource}
        this.ddlFilterType.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_16, ((int)DiscoveryResultsFilterType.MachineType).ToString()));// TODO: [localization] {used shared resource}
        this.ddlFilterType.Items.Add(new ListItem(Resources.CoreWebContent.WEBDATA_VB0_18, ((int)DiscoveryResultsFilterType.DiscoveredBy).ToString()));// TODO: [localization] {used shared resource}

        SelectedFilterValue = null;
    }

    protected void GenerateFilters()
    {
        this.panelFilters.Controls.Clear();
        this.panelFilters.Controls.Add(new LiteralControl("<ul>"));

        switch (this.DiscoveryFilterType)
        {
            case DiscoveryResultsFilterType.DateFound:
                GenerateDateTimeFilters(ScheduleDiscoveryHelper.GetDateFoundFilters(this.DiscoveryStatus));
                break;
            case DiscoveryResultsFilterType.DiscoveredBy:
                GenerateProfileFilters(ScheduleDiscoveryHelper.GetProfileFilters(this.DiscoveryStatus));
                break;
            case DiscoveryResultsFilterType.MachineType:
                GenerateMachineTypeFilters(ScheduleDiscoveryHelper.GetMachineTypeFilters(this.DiscoveryStatus));
                break;
        }

        this.panelFilters.Controls.Add(new LiteralControl("</ul>"));
    }

    protected void GenerateDateTimeFilters(List<DateTime> dates)
    {
        foreach (DateTime date in dates)
        {
            AddFilterItem(date.ToLocalTime().ToString("f"), date.ToString(), date.Ticks.ToString());
        }
    }

    protected void GenerateMachineTypeFilters(List<string> machineTypes)
    {
        foreach (string machineType in machineTypes)
        {
            AddFilterItem(machineType, machineType, machineType);
        }
    }

    protected void GenerateProfileFilters(List<int> profiles)
    {
        SortedList<int, string> fullProfiles = new SortedList<int, string>();        
        foreach (int profileId in profiles)
        {
            fullProfiles.Add(profileId, ScheduleDiscoveryHelper.GetProfileName(profileId));            
        }

        var sortedProfiles =  from fp in fullProfiles orderby fp.Value select fp;

        foreach (var item in sortedProfiles)
        {
            KeyValuePair<int, string> pair = (KeyValuePair<int, string>)item;
            AddFilterItem(pair.Value, pair.Key.ToString(), pair.Key.ToString());        
        }            
    }   

   private void AddFilterItem(string text, string argument, string id)
    {
        LinkButton lb = new LinkButton();
        lb.Text = text;
        lb.CommandArgument = argument;
        lb.Click += new EventHandler(lb_Click);
        lb.ID = id;
        lb.CssClass = "filter";

        panelFilters.Controls.Add(new LiteralControl("<li>"));
        panelFilters.Controls.Add(lb);
        panelFilters.Controls.Add(new LiteralControl("</li>"));

        if (argument == SelectedFilterValue)
        {
            selectAgain = true;
        }

        //AsyncPostBackTrigger apt = new AsyncPostBackTrigger();
        //apt.ControlID=lb.UniqueID;
        //apt.EventName="Click";            
        //upScheduleDiscoveryResults.Triggers.Add(apt);
    }

    #endregion
    
}
