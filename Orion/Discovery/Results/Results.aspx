<%@ Page Language="C#" MasterPageFile="~/Orion/Discovery/Results/ResultsWizardPage.master" AutoEventWireup="true"
    CodeFile="Results.aspx.cs" Inherits="Orion_Discovery_Results_Results" Title="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_23 %>"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<asp:ScriptManagerProxy runat="server" />            
    <span class="ActionName"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_35) %></span>
    <br />
	
	<div class="contentBlock">
	    <div id="progressHeader">
            <table>
                <tr><td><b><span id="importPhaseName"></span></b></td></tr>
                <tr><td><b><span id="phasePercentComplete">0</span>%</b> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_243) %></td></tr>
                <tr><td><b><span id="percentComplete">0</span>%</b> <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_242) %></td></tr>
            </table>
	    </div>
	    <div runat="server" id="ResultsDiv" class="Results">
	    </div>
	</div>
	
	<table class="NavigationButtons"><tr><td>
    <div class="sw-btn-bar-wizard">
        <orion:LocalizableButton LocalizedText="Back" DisplayType="Secondary"  runat="server" ID="imgBack" OnClick="imgbBack_Click" />
        <orion:LocalizableButton LocalizedText="Next" DisplayType="Primary" runat="server" ID="imgNext" OnClick="imgNext_Click" Visible="false" />
        <orion:LocalizableButton LocalizedText="Finish" DisplayType="Primary" runat="server" ID="imgbNext" OnClick="imgbFinish_Click" />
    </div>            
    </td></tr></table>    
    
    <script type="text/javascript">
        //<![CDATA[
        (function () {
            var ButtonsEnabled = false;
            var btnBack = $("#<%= imgBack.ClientID %>");
            var btnNext = $("#<%= imgNext.ClientID %>");
            var btnFinish = $("#<%= imgbNext.ClientID %>");
            var btns = [btnBack, btnNext, btnFinish];

            function disableClick(evt) {
                evt.preventDefault();
                return false;
            }

            function disableButtons(btns) {
                for (var i = 0; i < btns.length; i++) {
                    var btn = btns[i];
                    btn.bind('click', disableClick);
                    if (!btn.hasClass("sw-btn-disabled")) {
                        btn.addClass("sw-btn-disabled");
                    }
                };
            }

            function enableButtons(btns) {
                for (var i = 0; i < btns.length; i++) {
                    var btn = btns[i];
                    btn.off('click', disableClick);
                    btn.removeClass("sw-btn-disabled");

                };
            }

            disableButtons(btns);



            var CallProcessResults = function () {
                PageMethods.ProcessNodes(OnProcessProcessed, OnError);
            };

            var CallGetProgress = function () {
                PageMethods.GetProgress(OnResultsProcessed, OnError);
            };

            var OnProcessProcessed = function (result) {
                if (result == 1) {       //Started
                    CallGetProgress();
                }

                if (result == 2) {       //NothingToImport
                    ButtonsEnabled = true;
                    $("#progressHeader").hide();

                    enableButtons(btns);
                }

                if (result == 3) {      //OldResults
                    alert('<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_2) %>');
                    PageMethods.GetInitialPage(function (result) { window.location = result }, OnError);
                }
            };

            var OnResultsProcessed = function (result) {
                if (result.PercentComplete < 0 || result == null) {
                    CallGetProgress();
                    return;
                }

                var strTemp = result.ResultText.replace(/<(\/?\s*script.*?)>/ig, "&lt;$1&gt;")
                $(".Results").append(strTemp);
                $("#percentComplete").html('<span>' + result.PercentComplete + '</span>');
                $("#phasePercentComplete").html('<span>' + result.PhasePercentComplete + '</span>');
                $("#importPhaseName").html(result.PhaseName);

                var div = $(".Results").get(0);
                div.scrollTop = div.scrollHeight;

                if (!result.ImportIsDone) {
                    CallGetProgress();
                }
                else {
                    ButtonsEnabled = true;

                    enableButtons(btns);

                    $("#progressHeader").hide();
                    
                }
            }

            var OnError = function (error) {
                var rel = /ApplicationException/;
                if (error._exceptionType.search(rel) > -1)
                    alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_1) %>" + error.get_message());
                else
                    alert("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_VB0_1) %>" + error.get_message() + '\n' + error.get_stackTrace());
                ButtonsEnabled = true;

                enableButtons(btns);
            };

            $(function () {
                CallProcessResults();
            });
        })();
        //]]>
    </script>
</asp:Content>
