﻿using System;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Discovery;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Discovery_IgnoreList : System.Web.UI.Page
{    

    protected void Page_Load(object sender, EventArgs e)
    {
        OrionInclude.CoreFile("OrionMaster.js").AddJsInit("$(function() { SW.Core.Header.SyncTo($('.tab-top')); });");
        // demoedit
        // added
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
        {
            RemoveButton.OnClientClick = "demoAction('Core_Discovery_IgnoreList',this); return false;";
            RemoveLinkButton.OnClientClick = "demoAction('Core_Discovery_IgnoreList',this); return false;";
        }
        // /demoedit
    }

    protected void ibRemove_OnClick(object sender, EventArgs e)
    {
        //TODO get selected nodes from ignorelist

        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowAdmin)
        {
            string message = Resources.CoreWebContent.WEBCODE_AK0_16;
            RegisterStartupScriptAlert(message);
        }
        else
        {
            this.IgnoreList.RemoveSelectedFromIgnoreList();
        }
    }

    private void RegisterStartupScriptAlert(string message)
    {
        string script = ControlHelper.JsFormat("alert('{0}');", message);
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "Alert_Info", script, true);
    }

    protected void RunSearchNodes_Click(object sender, EventArgs e)
    {
        string cleanSearch = WebSecurityHelper.SanitizeAngular(SearchNodes.SearchString);
		if (SearchNodes.SearchString == cleanSearch)
		{
            ScheduleDiscoveryHelper.Search = SearchNodes.SearchString;
            this.IgnoreList.RefreshIgnoredList();
            SearchNodes.SetVisibleCleanSearch(true);
        }
        else
		{
			RunCleanSearchNodes_Click(null, EventArgs.Empty);
		}
    }

    protected void RunCleanSearchNodes_Click(object sender, EventArgs e)
    {
        ScheduleDiscoveryHelper.Search = null;
        SearchNodes.SetVisibleCleanSearch(false);
        SearchNodes.ClearSerachField();
        this.IgnoreList.RefreshIgnoredList();
    }
    
}
