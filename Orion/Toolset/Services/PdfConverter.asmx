﻿<%@ WebService Language="C#" Class="PdfConverter" %>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using EO.Pdf;
using EO.Pdf.Acm;
using SolarWinds.Toolset.Web.FileDownload;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class PdfConverter : System.Web.Services.WebService
{
    private const float FontSize = 7f;
    private const float CellPadding = 0.035F;
    
    [WebMethod]
    public string ConvertTableToPdf(string[][] table, string documentTitle, string documentHeader, string fileName)
    {
        int i = table.Length;
        var doc = CreateTableDocument(table, documentTitle, documentHeader);
        MemoryStream stream = new MemoryStream(256 * 1024);
        doc.Save(stream);

        BinaryContent content = new BinaryContent(stream.ToArray(), fileName, "pdf");
        string fileId = Guid.NewGuid().ToString();
        TemporaryContentContainer.Instance.AddItem(fileId, content);

        return fileId;
    }

    private void AddEoLicence()
    {
        EO.Pdf.Runtime.AddLicense("R/Ke3MKetZ9Zl6TNDOul5vvPuIlZl6Sxy59Zl8DyD+NZ6/0BELxbvNO/++Of" +
                                  "maQHEPGs4PP/6KFupbSzy653hI6xy59Zs7PyF+uo7sKetZ9Zl6TNGvGd3Pba" +
                                  "GeWol+jyH+R2mbzA3bBoqbTC3qFZ7ekDHuio5cGz4KFZpsKetZ9Zl6TNHuig" +
                                  "5eUFIPGetcr83cChudAE4tKs0OjK2rONrM7rD8B2tMDAHuig5eUFIPGetZGb" +
                                  "566l4Of2GfKetZGbdePt9BDtrNzCnrWfWZekzRfonNzyBBDInbW5xeO3aae3" +
                                  "x9+0dabw+g7kp+rp2g+9RoGkscufdePt9BDtrNzpz+eupeDn9hk=");
    }

    private PdfDocument CreateTableDocument(string[][] tableData, string title, string header)
    {
        AddEoLicence();

        PdfDocument pdfDoc = new PdfDocument();
        pdfDoc.Info.Title = title;
        pdfDoc.Info.Creator = "SolarWinds Toolset";
        AcmRender render = new AcmRender(pdfDoc);
        render.SetDefPageSize(new System.Drawing.SizeF(new System.Drawing.SizeF(PdfPageSizes.A4.Height, PdfPageSizes.A4.Width)));
        var columnLength = GetMaxColumnLength(tableData);
        float[] columnWidth = new float[columnLength.Length];
        AcmText hdr = new AcmText(header);
        hdr.Style.Height = 32;
        render.Render(hdr);
        for (int i = 0; i < columnLength.Length; i++)
        {
            columnWidth[i] = GetColumnWidthInches(columnLength[i]);
        }
        if (columnWidth.Sum() + GetColumnWidthInches(30) < PdfPageSizes.A4.Height)
        {
            AcmTable table = new AcmTable(columnWidth);

            table.GridLineType = AcmGridLineType.All;
            table.GridLineInfo = new AcmLineInfo(AcmLineStyle.Dashed, System.Drawing.Color.Black, 0.005f);
            table.CellPadding = CellPadding;
            table.Style.FontSize = FontSize;
            WriteTable(tableData, table);
            render.Render(table);
        }
        else
        {
            List<float> currentTableWidths = new List<float>();
            List<int[]> steps = new List<int[]>();
            int prevStep = 0;
            for (int i = 0; i < columnLength.Length; i++)
            {
                if (currentTableWidths.Sum() + GetColumnWidthInches(30) + GetColumnWidthInches(columnLength[i]) > PdfPageSizes.A4.Height)
                {
                    steps.Add(new int[]{prevStep, i - prevStep});
                    prevStep = i;
                    currentTableWidths.Clear();
                    currentTableWidths.Add(GetColumnWidthInches(columnLength[i]));
                }
                else
                {
                    currentTableWidths.Add(GetColumnWidthInches(columnLength[i]));
                }
                
                if (currentTableWidths.Count > 0 && i == columnLength.Length - 1)
                {
                    steps.Add(new int[] { prevStep, i - prevStep });
                }
                
            }
            List<AcmContent> tables = new List<AcmContent>();
            foreach (var step in steps)
            {
                if(step[1] <= 0)
                    continue;
                AcmTable table = new AcmTable(columnWidth.Skip(step[0]).Take(step[1]).ToArray());
                table.GridLineType = AcmGridLineType.All;
                table.GridLineInfo = new AcmLineInfo(AcmLineStyle.Dashed, System.Drawing.Color.Black, 0.005f);

                table.CellPadding = CellPadding;
                table.Style.FontSize = FontSize;
                var newTableData = new List<string[]>();
                for (int j = 0; j < tableData.GetLength(0); j++)
                {
                    newTableData.Add(tableData[j].Skip(step[0]).Take(step[1]).ToArray());
                }
                WriteTable(newTableData.ToArray(), table);
                tables.Add(table);
                tables.Add(new AcmLineBreak());
                tables.Add(new AcmText(" "));
                tables.Add(new AcmLineBreak());
            }
            render.Render(tables.ToArray());
        }
        return pdfDoc;
    }

    private int[] GetMaxColumnLength(string[][] tableData)
    {
        int numberOfColumns = tableData[0].Length;
        int numberOfRows = tableData.Length;
        int[] maxLength = new int[numberOfColumns];

        for (int i = 0; i < numberOfColumns; i++)
        {
            maxLength[i] = tableData[0][i].Length;
        }

        for (int i = 1; i < numberOfRows; i++)
        {
            for (int j = 0; j < numberOfColumns; j++)
            {
                int itemLength = tableData[i][j].Length;
                if (itemLength > maxLength[j])
                {
                    maxLength[j] = itemLength;
                }
            }
        }

        return maxLength;
    }

    private void WriteTable(string[][] tableData, AcmTable table)
    {
        int numberOfColumns = tableData[0].Length;
        int numberOfRows = tableData.Length;
        for (int i = 0; i < numberOfRows; i++)
        {
            AcmTableRow row = new AcmTableRow();
            for (int j = 0; j < numberOfColumns; j++)
            {
                AcmTableCell cell = null;
                string data =tableData[i][j];
                if (data.EndsWith(":RED") || data.EndsWith(":YELLOW"))
                {
                    cell = new AcmTableCell(new AcmText(data.Replace(":RED","").Replace(":YELLOW","")));  
                    cell.Style.BackgroundColor =  (data.EndsWith(":RED"))?  Color.Red :Color.Yellow;
                    cell.Style.ForegroundColor =  (data.EndsWith(":RED")) ? Color.White : Color.Black;                                        
                }
                else
                {
                    cell = new AcmTableCell(new AcmText(data));    
                }
                row.Cells.Add(cell);
            }
            table.Rows.Add(row);
        }
    }

/// <summary>
    /// Calculatex approx column width based on max column text length
    /// </summary>
    /// <param name="columnLengh"></param>
    /// <returns></returns>
    private float GetColumnWidthInches(int columnLengh)
    {
        return (columnLengh + 3.8f) *(FontSize*(0.45f + 0.028f)/72);
    }
}