﻿<%@ WebService Language="C#" Class="LatencyTestService" %>
using System;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.Toolset.Common.Tasks;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.LatencyTest;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class LatencyTestService : WebService
{
    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();

    protected int ToolId
    {
        get { return SupportedTools.LatencyTestToolId; }
    }

    [WebMethod]
    public void StopTest(string taskId)
    {
        new TaskService().StopTask(new Guid(taskId));
    }

    [WebMethod]
    public string RunTest(string pollingEngine)
    {
        var operationStartTime = DateTime.UtcNow;

        try
        {
            // Get necessary stuff
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (LatencyTestTool)toolFactory.CreateTool();
            var taskSettings = (LatencyTestTaskSettings)toolFactory.CreateTaskSettings();
            taskSettings.User = OrionAccountHelper.GetCurrentLoggedUserName();
            taskSettings.WebServiceStartTime = operationStartTime;
            var toolPermanentSettings = (LatencyTestPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName());

            // Create a new task
            var task = new Task
            {
                AccountId = OrionAccountHelper.GetCurrentLoggedUserName(),
                PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.PollingInterval),
                ToolPermanentSettings = toolPermanentSettings,
                Settings = taskSettings,
                Tool = tool,
                PollingEngineName = pollingEngine,
                WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
                SessionId = UserSessionHelper.GetSessionId(Context)
            };

            // Run task
            new TaskService().RunTask(task);
            return task.TaskId.ToString();
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
}
