﻿<%@ WebService Language="C#" Class="ConfigurationWizard" %>
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Script.Services;
using System.Web.Services;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Common.Tasks;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Tools.CpuMonitor;
using SolarWinds.Toolset.Tools.MemoryMonitor;
using SolarWinds.Toolset.Tools.ResponseTimeMonitor;
using SolarWinds.Toolset.Tools.InterfaceMonitor;
using SolarWinds.Toolset.Common.Demo;

/// <summary>
/// Summary description for CredentialManager
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class ConfigurationWizard : WebService
{
    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    private readonly CredentialsDAL credServc = new CredentialsDAL();
    private readonly CommonDAL commonDAL = new CommonDAL();
    private readonly ConfigurationWizardDAL confDAL = new ConfigurationWizardDAL();
    private readonly UserSessionsDAL sessDAL = new UserSessionsDAL();
    private static IEnumerable<string> _ips; 

    private static List<string> ips
    {
        get
        {
            if(!_ips.Any())
                _ips = DemoPredefinedInfo.GetAllHosts().Select(t => t.IPAddress);
            return _ips.ToList();
        }
    }

    [WebMethod]
    public void AddAutompleteIP(string controlID, string controlType, string value)
    {
        new AutocompleteDAL().AddOrUpdateAutocompleteItem(controlID, controlType, OrionAccountHelper.GetCurrentLoggedUserName(), value.Trim());
    }
    
    [WebMethod]
    public int GetNodeIdByIp(string ipAddress)
    {
        return confDAL.GetNodeIdByIp(ipAddress);
    }
    
    [WebMethod]
    public long GetInterfaceSpeedByNodeAndIndex(int nodeId, int index)
    {
        return confDAL.GetInterfaceSpeedByNodeAndIndex(nodeId, index);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public bool IsSessionRegistered()
    {
        var sessionId = UserSessionHelper.GetSessionId(Context);
        return sessDAL.IsSessionRegistered(sessionId);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public string GetDefaultDataView()
    {
        Dictionary<string, string> settigns =
             (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());
        return settigns.ContainsKey(PersonalSettingsKeys.DefaultDataRepresentation)? settigns[PersonalSettingsKeys.DefaultDataRepresentation]
                                                 : DefaultPersonalSettings.DefPersonalSettings[PersonalSettingsKeys.DefaultDataRepresentation];
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public List<ResponseListItem> GetAvailableEngines()
    {
        try
        {
            var items = new List<ResponseListItem>();

            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            if (availablePollingEngines != null)
            {
                foreach (var engine in availablePollingEngines)
                {
                    items.Add(new ResponseListItem() { ID = engine.EngineId.ToString(), Name = engine.ServerName, DisplayName = engine.DisplayName});
                }
            }

            return items;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public List<ResponseListItem> GetAvailableCredentials()
    {
        try
        {
            IList<SnmpCredentialsV2> credV2 = null;
            IList<SnmpCredentialsV3> credV3 = null;
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                credV2 = DemoPredefinedInfo.GetV2Credentials();
                credV3 = DemoPredefinedInfo.GetV3Credentials();
            }
            else
            {
                var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
                credV2 = credServc.GetToolsetAccountV2Credentials(currAcc).Cast<SnmpCredentialsV2>().ToList();
                credV3 = credServc.GetToolsetAccountV3Credentials(currAcc).Cast<SnmpCredentialsV3>().ToList();
            }
            
            
            var items = new List<ResponseListItem>();
            credV2.ForEach(c => items.Add(new ResponseListItem() { ID = c.ID.ToString(), Name = c.Name }));
            credV3.ForEach(c => items.Add(new ResponseListItem() { ID = c.ID.ToString(), Name = c.Name }));

            return items;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public List<SimpleTreeNodeInfo> GetNPMInterfaces(string GroupColumn, string searchValue, IList<NodeSelectedInfo> selectedInterfaces)
    {
        try
        {
            GroupColumn = GroupColumn.Replace("Orion.Nodes.", "").Replace("CustomProperties.", "").Replace("Orion.StatusInfo.", "").Replace(".", "_");
            var items = new List<SimpleTreeNodeInfo>();

            var table = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? DemoPredefinedInfo.GetInterfacesToTable() : confDAL.GetAvailableInterfaces();
            var rows = table.Rows;

            if (rows.Count <= 0)
                return items;

            DataTable snmpV3Table = null;
            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                var ids = new List<int>();
                foreach (DataRow row in table.Rows)
                {
                    var nodeId = (int)row["NodeID"];
                    ids.Add(nodeId);
                }
                snmpV3Table = confDAL.GetSNMPv3NameForNodes(String.Join<int>(",", ids));
                table.Columns.Add("SNMPv3Credentials_DisplayName", typeof(string));
            }

            foreach (DataRow row in table.Rows)
            {
                var childValue = row["DisplayName"] as string;
                var childinterface = row["InterfaceName"] as string;
                if (childinterface == null)
                    childinterface = String.Empty;
                if (!childValue.ToLowerInvariant().Contains(searchValue.ToLowerInvariant())
                    && !childinterface.ToLowerInvariant().Contains(searchValue.ToLowerInvariant()))
                    continue;

                var interfaceIndex = -1;
                var interfaceId = -1;
                if (row["InterfaceID"] != DBNull.Value && row["Index"] != DBNull.Value)
                {
                    interfaceIndex = (int)row["Index"];
                    interfaceId = (int)row["InterfaceID"];
                }
                
                var nodeId = (int)row["NodeID"];
                if (selectedInterfaces.Any(inter => (inter.NodeId == nodeId && inter.Indexes.Count == 0) ||(inter.NodeId == nodeId && inter.Indexes != null
                    && inter.Indexes.Any(index => index == interfaceIndex))))
                    continue;

                if (snmpV3Table != null)
                {
                    var snmpv3Name = String.Empty;
                    foreach (DataRow nameRow in snmpV3Table.Rows)
                    {
                        if ((int)nameRow["NodeID"] == nodeId)
                        {
                            snmpv3Name = nameRow["SNMPv3Credentials_DisplayName"] as string;
                            break;
                        }
                    }
                    row["SNMPv3Credentials_DisplayName"] = snmpv3Name;
                }
                
                var textFromDb = string.Empty;
                var groupText = string.Empty;
                if (!string.IsNullOrEmpty(GroupColumn))
                {
                    textFromDb = row[GroupColumn].ToString();
                    groupText = String.IsNullOrEmpty(textFromDb) ? "[Unknown]" : textFromDb;
                }
                
                if ((String.IsNullOrEmpty(groupText) && items.Any(item => item.NodeID == nodeId))
                    || items.Any(item => item.children.Any(itemChild => itemChild.NodeID == nodeId)))
                {
                    SimpleTreeNodeInfo editItem = null;
                    if (String.IsNullOrEmpty(groupText))
                    {
                        editItem = items.FirstOrDefault(item => item.NodeID == nodeId);
                    }
                    else
                    {
                        var groupItem = items.FirstOrDefault(item => item.children.Any(itemChild => itemChild.NodeID == nodeId));
                        editItem = groupItem.children.FirstOrDefault(item => item.NodeID == nodeId);
                    }
                    if (editItem != null && interfaceId >= 0)
                    {
                        editItem.children.Add(GetInterfaceDetails(row));
                    }
                }
                else
                {
                    if (!items.Any(item => item.text.Equals(groupText)) && !string.IsNullOrEmpty(groupText))
                    {
                        var nodeItem = new SimpleTreeNodeInfo() { text =  childValue, expanded = true, leaf = false, NodeID = nodeId, IpAddress = row["IPAddress"] as string, qtip = row["IPAddress"] as string,
                            StyleClass = GetCssClass(row),
                            iconCls = GetCssName(row),
                            children = new List<SimpleTreeNodeInfo>()
                        };

                        if (interfaceId >= 0)
                        {
                            nodeItem.children.Add(GetInterfaceDetails(row));
                        }
                        else { nodeItem.leaf = true; }
                        
                        var info = new SimpleTreeNodeInfo()
                        {
                            text = groupText,
                            expanded = true,
                            StyleClass = GroupColumn.Equals("Vendor", StringComparison.InvariantCultureIgnoreCase) ? GetVendorCssClass(row) : GetCssClass(row),
                            iconCls = GroupColumn.Equals("Vendor", StringComparison.InvariantCultureIgnoreCase) ? GetVendorCssName(row) : GetCssName(row),
                            children = new List<SimpleTreeNodeInfo>() { nodeItem }
                        };
                        items.Add(info);
                    }
                    else
                    {
                        var editItem = items.FirstOrDefault(item => item.text.Equals(groupText));
                        if (editItem != null)
                        {
                            var nodeItem = new SimpleTreeNodeInfo()
                            {
                                text = childValue,
                                leaf = false,
                                expanded = true,
                                NodeID = nodeId,
                                IpAddress = row["IPAddress"] as string,
                                qtip = row["IPAddress"] as string,
                                StyleClass = GetCssClass(row),
                                iconCls = GetCssName(row),
                                children = new List<SimpleTreeNodeInfo>()
                            };
                            if (interfaceId >= 0)
                            {
                                nodeItem.children.Add(GetInterfaceDetails(row));
                            }
                            else { nodeItem.leaf = true; }
                            editItem.children.Add(nodeItem);
                        }
                        else
                        {
                            var nodeItem = new SimpleTreeNodeInfo()
                               {
                                   text = childValue,
                                   leaf = false,
                                   expanded = true,
                                   NodeID = nodeId,
                                   IpAddress = row["IPAddress"] as string,
                                   qtip = row["IPAddress"] as string,
                                   StyleClass = GetCssClass(row),
                                   iconCls = GetCssName(row),
                                   children = new List<SimpleTreeNodeInfo>()
                               };
                            if (interfaceId >= 0)
                            {
                                nodeItem.children.Add(GetInterfaceDetails(row));
                            }
                            else { nodeItem.leaf = true; }
                            items.Add(nodeItem);
                        }
                    }
                }
            }

            foreach (var item in items)
            {
                if (item.children != null && item.children.Count > 0)
                {
                    item.text = String.Format("{0} ({1})", item.text, item.children.Count);
                    foreach (var child in item.children)
                    {
                        if (child.children != null && child.children.Count > 0)
                            child.text = String.Format("{0} ({1})", child.text, child.children.Count);
                    }
                }
                else
                {
                    item.leaf = true; 
                }
            }

            return items;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private SimpleTreeNodeInfo GetInterfaceDetails(DataRow row)
    {
        var interfaceId = (int)row["InterfaceID"];
        return new SimpleTreeNodeInfo()
        {
            leaf = true,
            text = row["InterfaceName"] as string,
            StyleClass = GetCssClass(row, "InterfaceStatusId"),
            iconCls = GetCssName(row, "InterfaceStatusId"),
            NodeID = (int)row["NodeID"],
            InterfaceID = interfaceId,
            InterfaceIndex = (int)row["Index"],
            IpAddress = row["IPAddress"] as string,
            qtip = Convert.ToString(interfaceId),
            NodeName = row["DisplayName"] as string,
            InterfaceSpeed = (int)row["Speed"],
        };
    }

    [WebMethod]
    public List<SimpleTreeNodeInfo> GetAvailableNodes(string GroupColumn, string searchValue, IList<int> selectedNodes)
    {
        try
        {
            GroupColumn = GroupColumn.Replace("Orion.Nodes.", "").Replace("CustomProperties.", "").Replace("Orion.StatusInfo.", "").Replace(".", "_");
            var items = new List<SimpleTreeNodeInfo>();

            var table = SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? DemoPredefinedInfo.GetHostsToTable() : confDAL.GetAvailableNodes();
            var rows = table.Rows;
            
            if (rows.Count <= 0)
                return items;

            DataTable snmpV3Table = null;
            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {   
                var ids = new List<int>();
                foreach (DataRow row in table.Rows)
                {
                    var nodeId = (int)row["NodeID"];
                    ids.Add(nodeId);
                }
                snmpV3Table = confDAL.GetSNMPv3NameForNodes(String.Join<int>(",", ids));
                table.Columns.Add("SNMPv3Credentials_DisplayName", typeof(string));
            }
            
            foreach (DataRow row in table.Rows)
            {
                var childValue = row["DisplayName"] as string;
                if (!childValue.ToLowerInvariant().Contains(searchValue.ToLowerInvariant()))
                    continue;

                var nodeId = (int)row["NodeID"];
                if (selectedNodes.Any(node => node == nodeId))
                    continue;

                if (snmpV3Table!=null)
                {
                    var snmpv3Name = String.Empty;
                    foreach (DataRow nameRow in snmpV3Table.Rows)
                    {
                        if ((int)nameRow["NodeID"] == nodeId)
                        {
                            snmpv3Name = nameRow["SNMPv3Credentials_DisplayName"] as string;
                            break;
                        }
                    }
                    row["SNMPv3Credentials_DisplayName"] = snmpv3Name;
                }
                var textFromDb = string.Empty;
                var groupText = string.Empty;
                if (!string.IsNullOrEmpty(GroupColumn))
                {
                    textFromDb = row[GroupColumn].ToString();
                    groupText = String.IsNullOrEmpty(textFromDb) ? "[Unknown]" : textFromDb;
                }
                if (!items.Any(item => item.text.Equals(groupText))&&!string.IsNullOrEmpty(groupText))
                {
                    var info = new SimpleTreeNodeInfo()
                    {
                        text = groupText,
                        expanded = true,
                        StyleClass = GroupColumn.Equals("Vendor", StringComparison.InvariantCultureIgnoreCase) ? GetVendorCssClass(row) : GetCssClass(row),
                        iconCls = GroupColumn.Equals("Vendor", StringComparison.InvariantCultureIgnoreCase) ? GetVendorCssName(row) : GetCssName(row),
                        children = new List<SimpleTreeNodeInfo>() { new SimpleTreeNodeInfo() { text =  childValue, leaf = true, NodeID = nodeId, IpAddress = row["IPAddress"] as string, qtip = row["IPAddress"] as string,
                            StyleClass = GetCssClass(row),
                            iconCls = GetCssName(row)
                    }
                        }
                    };
                    items.Add(info);
                }
                else
                {
                    var editItem = items.FirstOrDefault(item => item.text.Equals(groupText));
                    if (editItem != null)
                    {
                        editItem.children.Add(new SimpleTreeNodeInfo()
                        {
                            text = childValue,
                            leaf = true,
                            NodeID = nodeId,
                            IpAddress = row["IPAddress"] as string,
                            qtip = row["IPAddress"] as string,
                            StyleClass = GetCssClass(row),
                            iconCls = GetCssName(row)
                        });
                    }
                    else
                    {
                        items.Add(
                            new SimpleTreeNodeInfo()
                                {
                                    text = childValue,
                                    leaf = true,
                                    NodeID = nodeId,
                                    IpAddress = row["IPAddress"] as string,
                                    qtip = row["IPAddress"] as string,
                                    StyleClass = GetCssClass(row),
                                    iconCls = GetCssName(row)
                                });
                    }
                }
            }

            foreach (var item in items)
            {
                if (item.children != null && item.children.Count > 0)
                    item.text = String.Format("{0} ({1})", item.text, item.children.Count);
            }

            return items;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private static string GetCssName(DataRow row)
    {
        return GetCssName(row, "StatusId");
    }

    private static string GetCssName(DataRow row, string colName)
    {
        return String.Format("cwTreeStyle{0}", row[colName]);
    }

    private static string GetCssClass(DataRow row)
    {
        return GetCssClass(row, "StatusId");
    }
    
    private static string GetCssClass(DataRow row, string colName)
    {
        return String.Format(".{0} {{background-image: url(/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={1}&size=small) !important; background-size: 100%}}", GetCssName(row), row[colName]);
    }

    private static string GetVendorCssName(DataRow row)
    {
        var htmlFriendlyVendor = row["Vendor"] as string;
        htmlFriendlyVendor = htmlFriendlyVendor.Replace(" ", "").Replace(".", "");
        return String.Format("cwVendorStyle{0}", htmlFriendlyVendor);
    }

    private static string GetVendorCssClass(DataRow row)
    {
        return String.Format(".{0} {{background-image: url(../../../../NetPerfMon/images/Vendors/{1}) !important; background-size: 100%}}", GetVendorCssName(row), row["VendorIcon"]);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public bool IsNpmInstalled()
    {
        try
        {
            return commonDAL.IsNmpInstalled();
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public bool TestCredentialsFromCw(string ip, int id)
    {
        try
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                return false;
            }
            var customMessage = String.Empty;
            IPAddress parsedIp;
            if (!GetResolvedConnecionIPAddress(Server.HtmlEncode(ip), out parsedIp, out customMessage))
            {
                return false;
            }
            
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var snmpCred = GetMatchedCredentials(id, currAcc);
            if (snmpCred.Version == SNMPVersion.SNMP1 || snmpCred.Version == SNMPVersion.SNMP2c)
            {
                var snmpCredentialsV2 = snmpCred as SnmpCredentialsV2;
                if (snmpCredentialsV2 != null)
                {
                    return TestCredential(
                        credServc,
                        snmpCredentialsV2.Version,
                        parsedIp.ToString(),
                        161,
                        snmpCredentialsV2.Community,
                        String.Empty,
                        false,
                        SNMPv3AuthType.None,
                        SNMPv3PrivacyType.None,
                        String.Empty,
                        false,
                        String.Empty,
                        String.Empty);
                }
            }
            else if (snmpCred.Version == SNMPVersion.SNMP3)
            {
                var snmpCredentialsV3 = snmpCred as SnmpCredentialsV3;
                if (snmpCredentialsV3 != null)
                {
                    return TestCredential(
                        credServc,
                        SNMPVersion.SNMP3,
                        parsedIp.ToString(),
                        161,
                        String.Empty,
                        snmpCredentialsV3.AuthenticationPassword,
                        snmpCredentialsV3.AuthenticationKeyIsPassword,
                        snmpCredentialsV3.AuthenticationType,
                        snmpCredentialsV3.PrivacyType,
                        snmpCredentialsV3.PrivacyPassword,
                        snmpCredentialsV3.PrivacyKeyIsPassword,
                        snmpCredentialsV3.Context,
                        snmpCredentialsV3.UserName);
                }
            }

            return false;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    public SnmpCredentials GetMatchedCredentials(int id, string username)
    {
      
        var credV2 = credServc.GetToolsetAccountV2Credentials(username).Cast<SnmpCredentialsV2>();
        var snmpCredentialsV2 = credV2.FirstOrDefault(c => c.ID == id);
        if (snmpCredentialsV2 == null)
        {
            var credV3 = credServc.GetToolsetAccountV3Credentials(username).Cast<SnmpCredentialsV3>();
            return credV3.FirstOrDefault(c => c.ID == id);
        }
        return snmpCredentialsV2;
    }

    public bool TestCredential(CredentialsDAL credServc, SNMPVersion snmpVersion, string ip, uint snmpPort, string community, string authKey,
        bool authKeyIsPwd, SNMPv3AuthType authType, SNMPv3PrivacyType privacyType, string privacyPassword, bool privKeyIsPwd, string context, string username)
    {
        IPAddress[] addresslist = Dns.GetHostAddresses(ip);
        foreach (IPAddress theaddress in addresslist)
        {
            if (credServc.ValidateSNMP(snmpVersion, theaddress.ToString(), snmpPort, community, authKey, authKeyIsPwd, authType, privacyType,
            privacyPassword, privKeyIsPwd, context, username))
                return true;
        }
        return false;
    }

    private SNMPCredentialProperties ParseSNMPCredentials(SnmpCredentials snmpCredential)
    {
        var credential = new SNMPCredentialProperties() { SNMPVersion = 2, Community = "public" };

        if (snmpCredential.Version == SNMPVersion.SNMP1 || snmpCredential.Version == SNMPVersion.SNMP2c)
        {
            var snmpCredentialV2 = snmpCredential as SnmpCredentialsV2;
            credential = new SNMPCredentialProperties() { SNMPVersion = 2, Community = snmpCredentialV2.Community,  };
        }
        else if (snmpCredential.Version == SNMPVersion.SNMP3)
        {
            var snmpCredentialV3 = snmpCredential as SnmpCredentialsV3;
            credential = new SNMPCredentialProperties()
            {
                SNMPVersion = 3,
                Name = snmpCredentialV3.Name,
                UserName = snmpCredentialV3.UserName,
                ContextName = snmpCredentialV3.Context,
                AuthType = Enum.GetName(typeof(SNMPv3AuthType), snmpCredentialV3.AuthenticationType),
                AuthKey = snmpCredentialV3.AuthenticationPassword,
                AuthPassword = snmpCredentialV3.AuthenticationPassword,
                AuthPwdIsKey = snmpCredentialV3.AuthenticationKeyIsPassword,
                PrivacyType = Enum.GetName(typeof(SNMPv3PrivacyType), snmpCredentialV3.PrivacyType),
                PrivacyKey = snmpCredentialV3.PrivacyPassword,
                PrivacyPassword = snmpCredentialV3.PrivacyPassword,
                PrivacyPwdIsKey = snmpCredentialV3.PrivacyKeyIsPassword
            };
        }

        return credential;
    }

    private SNMPCredentialProperties ParseNodeCredentialV2(DataTable dataTable)
    {
        SNMPCredentialProperties credential = null;
        var community = String.Empty;
        var version = -1;
        foreach (DataRow row in dataTable.Rows)
        {
            community = String.IsNullOrEmpty(row["Community"] as string) ? row["RWCommunity"] as string : row["Community"] as string;
            int.TryParse(row["SNMPVersion"] as string, out version);
        }

        if (!String.IsNullOrEmpty(community))
        {
            credential = new SNMPCredentialProperties() { SNMPVersion = 2, Community = community, };
        }

        return credential;
    }

    private SNMPCredentialProperties ParseNodeCredentialV3(DataTable dataTable)
    {
        SNMPCredentialProperties credential = null;
        if (dataTable.Rows.Count > 0)
        {
            var row = dataTable.Rows[0];
            int nodeId;
            int.TryParse(row["NodeID"] as string, out nodeId);
            //checking for null before casting. setting default value to true if null as it comes as true if checkbox is not checked. 
            var authPwdIsKey = row["AuthenticationKeyIsPassword"] as bool? ?? true;
            var rwAuthenticationKeyIsPassword = row["RWAuthenticationKeyIsPassword"] as bool? ?? true;
            var privacyKeyIsPassword = row["PrivacyKeyIsPassword"] as bool? ?? true;
            var rwPrivacyKeyIsPassword = row["RWPrivacyKeyIsPassword"] as bool? ?? true;

            credential = new SNMPCredentialProperties()
            {
                SNMPVersion = 3,
                Name = string.Format("NodeCredential{0}", nodeId),
                UserName =
                    string.IsNullOrEmpty(row["Username"] as string)
                        ? row["RWUsername"] as string
                        : row["Username"] as string,
                ContextName =
                    string.IsNullOrEmpty(row["Context"] as string)
                        ? row["RWContext"] as string ?? string.Empty
                        : row["Context"] as string,

                AuthType =
                    string.IsNullOrEmpty(row["AuthenticationMethod"] as string)
                        ? row["RWAuthenticationMethod"] as string
                        : row["AuthenticationMethod"] as string,
                AuthKey =
                    string.IsNullOrEmpty(row["AuthenticationKey"] as string)
                        ? row["RWAuthenticationKey"] as string
                        : row["AuthenticationKey"] as string,
                AuthPassword =
                    string.IsNullOrEmpty(row["AuthenticationKey"] as string)
                        ? row["RWAuthenticationKey"] as string
                        : row["AuthenticationKey"] as string,
                AuthPwdIsKey = authPwdIsKey && !rwAuthenticationKeyIsPassword,
                PrivacyType =
                    string.IsNullOrEmpty(row["PrivacyMethod"] as string)
                        ? row["RWPrivacyMethod"] as string
                        : row["PrivacyMethod"] as string,
                PrivacyKey =
                    string.IsNullOrEmpty(row["PrivacyKey"] as string)
                        ? row["RWPrivacyKey"] as string
                        : row["PrivacyKey"] as string,
                PrivacyPassword =
                    string.IsNullOrEmpty(row["PrivacyKey"] as string)
                        ? row["RWPrivacyKey"] as string
                        : row["PrivacyKey"] as string,
                PrivacyPwdIsKey = privacyKeyIsPassword && !rwPrivacyKeyIsPassword
            };
        }
        return credential;
    }

    [WebMethod]
    public string PollCpus(string pollingEngine, IEnumerable<ItemToPoll> itemsToPoll)
    {
        try
        {
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return PollCpusInternal(engine.ServerName, itemsToPoll);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private string PollCpusInternal(string serverName, IEnumerable<ItemToPoll> itemsToPoll)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (CpuMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.CpuMonitorId, accountId);
        var pesonalSettigns = (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());
        
        int retries;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpRetries) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpRetries], out retries))
        {
            toolPermanentSettings.SnmpRetries = retries;
        }

        int timeout;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpTimeOut) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpTimeOut], out timeout))
        {
            toolPermanentSettings.SnmpTimeout = timeout;
        }
        
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.CpuMonitorId);
        var tool = (CpuMonitorTool)toolFactory.CreateTool();
        var taskSettings = (CpuMonitorTaskSettings)toolFactory.CreateTaskSettings();
        taskSettings.User = accountId;
        taskSettings.IsPollingRequest = true;

        var selectedGuids = new List<PollItemsInfo>();
        foreach (var pollItem in itemsToPoll)
        {
            IPAddress ip;
            if (!IPAddress.TryParse(pollItem.IP, out ip))
                continue;
            
            var snmpCredInfo = GetCredentilasInfo(accountId, pollItem.CredentialId, pollItem.NodeId);
            if (snmpCredInfo == null) continue;

            var device = new PollDeviceInfo()
            {
                IPAddress = IPAddress.Parse(pollItem.IP),
                CredProperties = snmpCredInfo
            };
            selectedGuids.Add(new PollItemsInfo() { Device = device, Items = pollItem.Items });
        }
        taskSettings.PollCpus = selectedGuids;

        var task = new Task()
        {
            AccountId = accountId,
            PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.CpuPollInterval),
            ToolPermanentSettings = toolPermanentSettings,
            Settings = taskSettings,
            Tool = tool,
            PollingEngineName = serverName,
            WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
            SessionId = UserSessionHelper.GetSessionId(Context)
        };

        var taskService = new TaskService();
        taskService.RunTask(task);

        return task.TaskId.ToString();
    }

    [WebMethod]
    public string PollMemories(string pollingEngine, IEnumerable<ItemToPoll> itemsToPoll)
    {
        try
        {
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return PollMemoriesInternal(engine.ServerName, itemsToPoll);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private string PollMemoriesInternal(string serverName, IEnumerable<ItemToPoll> itemsToPoll)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.MemoryMonitorToolId, accountId);
        var pesonalSettigns = (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());

        int retries;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpRetries) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpRetries], out retries))
        {
            toolPermanentSettings.SnmpRetries = retries;
        }

        int timeout;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpTimeOut) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpTimeOut], out timeout))
        {
            toolPermanentSettings.SnmpTimeout = timeout;
        }
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.MemoryMonitorToolId);

        var tool = (MemoryMonitorTool)toolFactory.CreateTool();
        var taskSettings = (MemoryMonitorTaskSettings)toolFactory.CreateTaskSettings();
        taskSettings.User = accountId;
        taskSettings.IsPollingRequest = true;

        var selectedGuids = new List<PollItemsInfo>();
        foreach (var pollItem in itemsToPoll)
        {
            IPAddress ip;
            if (!IPAddress.TryParse(pollItem.IP, out ip))
                continue;

            var snmpCredInfo = GetCredentilasInfo(accountId, pollItem.CredentialId, pollItem.NodeId);
            if (snmpCredInfo == null) continue;

            var device = new PollDeviceInfo()
            {
                IPAddress = IPAddress.Parse(pollItem.IP),
                CredProperties = snmpCredInfo
            };
            selectedGuids.Add(new PollItemsInfo() { Device = device, Items = pollItem.Items });
        }
        taskSettings.PollMemories = selectedGuids;

        var task = new Task()
        {
            AccountId = accountId,
            PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.MemoryPollInterval),
            ToolPermanentSettings = toolPermanentSettings,
            Settings = taskSettings,
            Tool = tool,
            PollingEngineName = serverName,
            WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
            SessionId = UserSessionHelper.GetSessionId(Context)
        };

        var taskService = new TaskService();
        taskService.RunTask(task);

        return task.TaskId.ToString();
    }
    

    [WebMethod]
    public string PollNodes(string pollingEngine, IEnumerable<ItemToPoll> itemsToPoll)
    {
        try
        {
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return PollNodesInternal(engine.ServerName, itemsToPoll);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private string PollNodesInternal(string serverName, IEnumerable<ItemToPoll> itemsToPoll)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.ResponseTimeMonitorToolId, accountId);
        var pesonalSettigns = (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());

        int retries;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpRetries) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpRetries], out retries))
        {
            toolPermanentSettings.SnmpRetries = retries;
        }

        int timeout;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpTimeOut) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpTimeOut], out timeout))
        {
            toolPermanentSettings.SnmpTimeout = timeout;
        }
        
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.ResponseTimeMonitorToolId);
        var tool = (ResponseTimeMonitorTool)toolFactory.CreateTool();
        var taskSettings = (ResponseTimeMonitorTaskSettings)toolFactory.CreateTaskSettings();
        taskSettings.User = accountId;
        taskSettings.IsPollingRequest = true;

        var selectedNodes = new List<PollDeviceInfo>();
        foreach (var pollItem in itemsToPoll)
        {
            var customMessage = String.Empty;
            IPAddress ip;
            if (GetResolvedConnecionIPAddress(Server.HtmlEncode(pollItem.IP), out ip, out customMessage))
            {
                var snmpCredInfo = GetCredentilasInfo(accountId, pollItem.CredentialId, pollItem.NodeId);
                if (snmpCredInfo == null) continue;

                var device = new PollDeviceInfo()
                {
                    IPAddress = ip,
                    CredProperties = snmpCredInfo
                };
                selectedNodes.Add(device);
            }
        }
        taskSettings.Hosts = selectedNodes;

        var task = new Task()
        {
            AccountId = accountId,
            PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.ResponsePollInterval),
            ToolPermanentSettings = toolPermanentSettings,
            Settings = taskSettings,
            Tool = tool,
            PollingEngineName = serverName,
            WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
            SessionId = UserSessionHelper.GetSessionId(Context)
        };

        var taskService = new TaskService();
        taskService.RunTask(task);

        return task.TaskId.ToString();
    }
    [WebMethod]
    public void StopTask(string taskId)
    {
        new TaskService().StopTask(new Guid(taskId));
    }
    [WebMethod]
    public string PollInterfaces(string pollingEngine, IEnumerable<InterfaceDeviceToPoll> itemsToPoll)
    {
        try
        {
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return PollInterfacesInternal(engine.ServerName, itemsToPoll);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private string PollInterfacesInternal(string serverName, IEnumerable<InterfaceDeviceToPoll> itemsToPoll)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (InterfaceMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.InterfaceMonitorId, accountId);
        
        var pesonalSettigns = (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());

        int retries;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpRetries) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpRetries], out retries))
        {
            toolPermanentSettings.SnmpRetries = retries;
        }

        int timeout;
        if (pesonalSettigns.ContainsKey(PersonalSettingsKeys.SnmpTimeOut) && int.TryParse(pesonalSettigns[PersonalSettingsKeys.SnmpTimeOut], out timeout))
        {
            toolPermanentSettings.SnmpTimeout = timeout;
        }
        
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.InterfaceMonitorId);
        var tool = (InterfaceMonitorTool)toolFactory.CreateTool();
        var taskSettings = (InterfaceMonitorTaskSettings)toolFactory.CreateTaskSettings();
        taskSettings.User = accountId;
        taskSettings.IsPollingRequest = true;

        var selectedInfo = new List<PollItemsInfo>();
        foreach (var pollItem in itemsToPoll)
        {
            IPAddress ip;
            if (!IPAddress.TryParse(pollItem.IP, out ip))
                continue;

            var snmpCredInfo = GetCredentilasInfo(accountId, pollItem.CredentialId, pollItem.NodeId);
            if (snmpCredInfo == null) continue;

            var device = new PollDeviceInfo()
            {
                IPAddress = IPAddress.Parse(pollItem.IP),
                CredProperties = snmpCredInfo
            };

            var indexes = new List<int>();
            var rxspeed = new Dictionary<int, long>();
            var txspeed = new Dictionary<int, long>();
            foreach (var inter in pollItem.Interfaces)
            {
                indexes.Add(inter.Index);
                rxspeed.Add(inter.Index, inter.RxSpeed);
                txspeed.Add(inter.Index, inter.TxSpeed);
            }
            selectedInfo.Add(new PollItemsInfo() { Device = device, Indexes = indexes, RxSpeed = rxspeed, TxSpeed = txspeed });
        }
        taskSettings.PollInterfaces = selectedInfo;

        var task = new Task()
        {
            AccountId = accountId,
            PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.InterfacePollInterval),
            ToolPermanentSettings = toolPermanentSettings,
            Settings = taskSettings,
            Tool = tool,
            PollingEngineName = serverName,
            WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
            SessionId = UserSessionHelper.GetSessionId(Context)
        };

        var taskService = new TaskService();
        taskService.RunTask(task);

        return  task.TaskId.ToString();
    }

    [WebMethod(EnableSession = true)]
    public void RunDemoTasks(List<TaskId> tasksToRun)
    {
        var demoTasks = GetDemoTasksFromSession();
        if (demoTasks != null)
        {
            var taskService = new TaskService();
            foreach (var taskId in tasksToRun)
            {
                var taskGuid = Guid.Parse(taskId.TaskID);
                Task task;
                if (demoTasks.TryGetValue(taskGuid, out task))
                {
                    taskService.RunTask(task);
                    demoTasks.TryRemove(taskGuid, out task);
                }
            }
        }
    }

    private ConcurrentDictionary<Guid, Task> GetDemoTasksFromSession()
    {
        if (Session["DemoTasks"] == null)
            Session["DemoTasks"] = new ConcurrentDictionary<Guid, Task>();

        return Session["DemoTasks"] as ConcurrentDictionary<Guid, Task>;
    }

    private void RunOrStoreTasks(List<Task> tasks)
    {
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            var taskService = new TaskService();
            foreach (var task in tasks)
            {
                taskService.RunTask(task);
            }
        }
        else
        {
            var demoTasks = GetDemoTasksFromSession();
            if (demoTasks != null)
            {
                foreach (var task in tasks)
                {
                    demoTasks.AddOrUpdate(task.TaskId, task, (key, oldValue) => task);
                }
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public List<TaskId> GetAvailableInterfaces(string pollingEngine, IEnumerable<ItemToDiscover> items)
    {
        try
        {
            if (!ValidateDemoInputIps(items))
            {
                return null;
            }
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return DiscoverInterfaces(engine.ServerName, items);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private List<TaskId> DiscoverInterfaces(string serverName, IEnumerable<ItemToDiscover> devicesToDiscover)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (InterfaceMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.InterfaceMonitorId, accountId);
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.InterfaceMonitorId);

        var taskIdReference = new List<TaskId>();
        var tasks = new List<Task>();

        foreach (var device in devicesToDiscover)
        {
            var customMessage = String.Empty;
            IPAddress ip;
            if (GetResolvedConnecionIPAddress(Server.HtmlEncode(device.IP), out ip, out customMessage))
            {
                var snmpCredInfo = GetCredentilasInfo(accountId, device.CredentialId, device.NodeId);
                if (snmpCredInfo == null) continue;

                var tool = (InterfaceMonitorTool)toolFactory.CreateTool();
                var taskSettings = (InterfaceMonitorTaskSettings)toolFactory.CreateTaskSettings();
                taskSettings.DeviceToPoll = new PollDeviceInfo()
                {
                    IPAddress = ip,
                    CredProperties = snmpCredInfo
                };
                taskSettings.IsPollingRequest = false;
                taskSettings.User = accountId;
                var task = new Task()
                {
                    AccountId = accountId,
                    PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.InterfacePollInterval),
                    ToolPermanentSettings = toolPermanentSettings,
                    Settings = taskSettings,
                    Tool = tool,
                    PollingEngineName = serverName,
                    WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
                    SessionId = UserSessionHelper.GetSessionId(Context)
                };
                tasks.Add(task);
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = task.TaskId.ToString(), CustomMessage = customMessage });
            }
            else
            {
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = String.Empty, CustomMessage = customMessage });
            }
        }

        RunOrStoreTasks(tasks);

        return taskIdReference;
    }


    private static bool ValidateDemoInputIps(IEnumerable<ItemToDiscover> items)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            if (items.Any(item => string.IsNullOrEmpty(item.IP) && ips.Contains(item.IP)))
                return false;
        }
        return true;
    }
    
    [WebMethod(EnableSession = true)]
    public List<TaskId> GetAvailableCpus(string pollingEngine, IEnumerable<ItemToDiscover> items)
    {
        try
        {
            if (!ValidateDemoInputIps(items))
            {
                return null;
            }
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return DiscoverCpus(engine.ServerName, items);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
    
    private List<TaskId> DiscoverCpus(string serverName, IEnumerable<ItemToDiscover> devicesToDiscover)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (CpuMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.CpuMonitorId, accountId);
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.CpuMonitorId);

        var taskIdReference = new List<TaskId>();
        var tasks = new List<Task>();

        foreach (var device in devicesToDiscover)
        {
            var customMessage = String.Empty;
            IPAddress ip;
            if (GetResolvedConnecionIPAddress(Server.HtmlEncode(device.IP), out ip, out customMessage))
            {
                var snmpCredInfo = GetCredentilasInfo(accountId, device.CredentialId, device.NodeId);
                if (snmpCredInfo == null) continue;

                var tool = (CpuMonitorTool)toolFactory.CreateTool();
                var taskSettings = (CpuMonitorTaskSettings)toolFactory.CreateTaskSettings();
                taskSettings.DeviceToPoll = new PollDeviceInfo()
                {
                    IPAddress = ip,
                    CredProperties = snmpCredInfo
                };
                taskSettings.IsPollingRequest = false;
                taskSettings.User = accountId;
                var task = new Task()
                {
                    AccountId = accountId,
                    PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.CpuPollInterval),
                    ToolPermanentSettings = toolPermanentSettings,
                    Settings = taskSettings,
                    Tool = tool,
                    PollingEngineName = serverName,
                    WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
                    SessionId = UserSessionHelper.GetSessionId(Context)
                };
                tasks.Add(task);
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = task.TaskId.ToString(), CustomMessage = customMessage });
            }
            else
            {
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = String.Empty, CustomMessage = customMessage });
            }
        }

        RunOrStoreTasks(tasks);

        return taskIdReference;
    }

    [WebMethod(EnableSession = true)]
    public List<TaskId> GetAvailableMemoryItems(string pollingEngine, IEnumerable<ItemToDiscover> items)
    {
        try
        {
            if (!ValidateDemoInputIps(items))
            {
                return null;
            }
            //Check if polling engine exists
            var availablePollingEngines = new CommonDAL().GetAvailablePollingEngines();
            var engine = availablePollingEngines.FirstOrDefault(e => e.ServerName == pollingEngine);
            if (engine == null) return null;

            return DiscoverMemories(engine.ServerName, items);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private List<TaskId> DiscoverMemories(string serverName, IEnumerable<ItemToDiscover> devicesToDiscover)
    {
        var accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        var toolPermanentSettings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.MemoryMonitorToolId, accountId);
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(SupportedTools.MemoryMonitorToolId);

        var taskIdReference = new List<TaskId>();
        var tasks = new List<Task>();

        foreach (var device in devicesToDiscover)
        {
            var customMessage = String.Empty;
            IPAddress ip;
            if (GetResolvedConnecionIPAddress(Server.HtmlEncode(device.IP), out ip, out customMessage))
            {
                var snmpCredInfo = GetCredentilasInfo(accountId, device.CredentialId, device.NodeId);
                if (snmpCredInfo == null) continue;

                var tool = (MemoryMonitorTool)toolFactory.CreateTool();
                var taskSettings = (MemoryMonitorTaskSettings)toolFactory.CreateTaskSettings();
                taskSettings.DeviceToPoll = new PollDeviceInfo()
                {
                    IPAddress = ip,
                    CredProperties = snmpCredInfo
                };
                taskSettings.IsPollingRequest = false;
                taskSettings.User = accountId;
                var task = new Task()
                {
                    AccountId = accountId,
                    PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.MemoryPollInterval),
                    ToolPermanentSettings = toolPermanentSettings,
                    Settings = taskSettings,
                    Tool = tool,
                    PollingEngineName = serverName,
                    WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
                    SessionId = UserSessionHelper.GetSessionId(Context) 
                };
                tasks.Add(task);
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = task.TaskId.ToString(), CustomMessage = customMessage });
            }
            else
            {
                taskIdReference.Add(new TaskId() { ID = device.ID, TaskID = String.Empty, CustomMessage = customMessage });
            }
        }

        RunOrStoreTasks(tasks);

        return taskIdReference;
    }

    private bool GetResolvedConnecionIPAddress(string serverNameOrURL, out IPAddress resolvedIPAddress, out string message)
    {
        bool isResolved = false;
        IPHostEntry hostEntry = null;
        IPAddress resolvIP = null;
        message = String.Empty;
        try
        {
            if (!IPAddress.TryParse(serverNameOrURL, out resolvIP))
            {
                hostEntry = Dns.GetHostEntry(serverNameOrURL);

                if (hostEntry != null && hostEntry.AddressList != null && hostEntry.AddressList.Length > 0)
                {
                    if (hostEntry.AddressList.Length == 1)
                    {
                        resolvIP = hostEntry.AddressList[0];
                        isResolved = true;
                    }
                    else
                    {
                        foreach (IPAddress var in hostEntry.AddressList)
                        {
                            if (var.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                resolvIP = var;
                                isResolved = true;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                isResolved = true;
            }
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            resolvedIPAddress = resolvIP;
        }

        return isResolved;
    }

    private SNMPCredentialProperties GetCredentilasInfo(string accountId, int credentilaId, int nodeId)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return new SNMPCredentialProperties();
        }
        SNMPCredentialProperties snmpCredInfo = null;
        if (credentilaId >= 0)
        {
            var snmpCred = GetMatchedCredentials(credentilaId, accountId);
            snmpCredInfo = ParseSNMPCredentials(snmpCred);
        }
        else if (nodeId >= 0)
        {
            snmpCredInfo = ParseNodeCredentialV2(confDAL.GetSNMPv2ForNode(nodeId));
            if (snmpCredInfo == null)
            {
                snmpCredInfo = ParseNodeCredentialV3(confDAL.GetSNMPv3ForNode(nodeId));
            }
        }
        return snmpCredInfo;
    }

    public class ResponseListItem
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    }

    public class TaskId
    {
        public string ID { get; set; }
        public string TaskID { get; set; }
        public string CustomMessage { get; set; }
    }
    
    public class ItemToDiscover
    {
        public string ID { get; set; }
        public string IP { get; set; }
        public int NodeId { get; set; }
        public int CredentialId { get; set; }
    }

    public class ItemToPoll
    {
        public string IP { get; set; }
        public int NodeId { get; set; }
        public int CredentialId { get; set; }
        public IList<string> Items { get; set; }
    }

    public class InterfaceDeviceToPoll
    {
        public string IP { get; set; }
        public int NodeId { get; set; }
        public int CredentialId { get; set; }
        public IList<InterfacePollInfo> Interfaces { get; set; }
    }

    public class InterfacePollInfo
    {
        public int Index { get; set; }
        public long RxSpeed { get; set; }
        public long TxSpeed { get; set; }
    }

    public class NodeSelectedInfo
    {
        public int NodeId { get; set; }
        public List<int> Indexes { get; set; } 
    }

    public class SimpleTreeNodeInfo
    {
        public SimpleTreeNodeInfo()
        {
            NodeID = -1;
            CredentialId = -1;
            InterfaceID = -1;
            InterfaceIndex = -1;
        }
        public string ID { get { return Guid.NewGuid().ToString(); } }
        public string text { get; set; }
        public bool leaf { get; set; }
        public bool expanded { get; set; }
        public bool @checked { get; set; }
        public List<SimpleTreeNodeInfo> children { get; set; }
        public string qtip { get; set; }
        public string iconCls { get; set; }
        public string StyleClass { get; set; }
        public int NodeID { get; set; }
        public string NodeName { get; set; }
        public int CredentialId { get; set; }
        public string IpAddress { get; set; }
        public string Icon { get; set; }
        public long InterfaceSpeed { get; set; }
        public int InterfaceID { get; set; }
        public int InterfaceIndex { get; set; }
        public string InterfaceName { get; set; }
    }
}
