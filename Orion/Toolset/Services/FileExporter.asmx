﻿<%@ WebService Language="C#" Class="FileExporter" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.FileDownload;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class FileExporter : System.Web.Services.WebService
{
    [WebMethod]
    public void SaveTemporaryTextFile(string fileId, string content, string fileName, string fileType)
    {
        var tempFile = new TextContent(new[]{content}, fileName, fileType);
        TemporaryContentContainer.Instance.AddItem(fileId, tempFile);
    }
}
