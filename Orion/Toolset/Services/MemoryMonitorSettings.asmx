﻿<%@ WebService Language="C#" Class="MemoryMonitorSettings" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.ResultColumnsSettings;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.MemoryMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Hubs;

/// <summary>
/// Summary description for CredentialManager
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class MemoryMonitorSettings : WebService
{

    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    private MonitorsSettingsHelper helper = new MonitorsSettingsHelper();
    public ToolsDAL toolsDal = new ToolsDAL();

    protected int ToolId
    {
        get
        {
            return SupportedTools.MemoryMonitorToolId;
        }
    }

    [WebMethod]
    public PageableDataTable GetSelectedColumns()
    {
        try
        {
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var visibleSetings = ((MemoryMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, currAcc));
            return helper.FormatSelectedColumnsTable(visibleSetings, false);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void SaveChartLimits(MonitorsSettingsHelper.LimitsSettings settingsJson, ref MemoryMonitorPermanentSettings settings)
    {
        ChartsRetentionSettings result;
        Enum.TryParse(settingsJson.LimitType, out result);
        settings.ChartDataLimitType = result;
        settings.ChartDataLimitByPoints = Convert.ToInt32(settingsJson.PointsValue);
        settings.ChartDataLimitByTimeMin = Convert.ToInt32(settingsJson.TimeValue);
        settings.MemoryPollInterval = Convert.ToInt32(settingsJson.PollingInterval) * 1000;
    }

    [WebMethod]
    public bool SaveThresholds(IEnumerable<MonitorsSettingsHelper.ThresholdSettings> settingsJson, ref MemoryMonitorPermanentSettings settings)
    {
        var elements = settings.GetAllColumns();

        foreach (var setting in settingsJson)
        {
            if (!string.IsNullOrEmpty(setting.Name))
            {
                var element = elements.Single(c => c.PropertyName.Equals(setting.Name, StringComparison.CurrentCultureIgnoreCase));
                element.UseThreshold = Convert.ToBoolean(setting.UseThresholds);

                ThresholdOperator result;
                Enum.TryParse(setting.WarningLevel, out result);
                element.WarningThresholdOperator = result;
                element.CriticalThresholdOperator = result;

                element.WarningThresholdValue = Convert.ToDouble(setting.WarningValue);
                element.CriticalThresholdValue = Convert.ToDouble(setting.CriticalValue);
            }
        }
        return true;
    }

    [WebMethod]
    public void SaveMetrics(string table, ref MemoryMonitorPermanentSettings settings)
    {
        try
        {
            var visibleSettings = settings.GetAllColumns();
            settings = (MemoryMonitorPermanentSettings)helper.SaveSettingsFromTable(table, visibleSettings, settings, OrionAccountHelper.GetCurrentLoggedUserName());
        }
        catch (Exception e)
        {
            _myLog.Error(e);
        }
    }

    [WebMethod]
    public bool SaveAllSettings(MonitorsSettingsHelper.LimitsSettings charts, string table,
        IEnumerable<MonitorsSettingsHelper.ThresholdSettings> thresholds)
    {
        try
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                return false;
            }
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, currAcc);

            SaveChartLimits(charts, ref settings);
            SaveMetrics(table, ref settings);
            SaveThresholds(thresholds, ref settings);

            toolsDal.SaveToolPermanentSettings(ToolId, currAcc, settings);
            ResultNotificationHubWrapper.Instance.ReloadSettings(currAcc);
            return true;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            return false;
        }
    }
  
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public List<ResultColumnSettings> GetResultColumns()
    {
        return ((MemoryMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName())).GetVisibleColumns();
    }

    [WebMethod]
    public List<ResultColumnSettings> GetVisibleResultColumns()
    {
        return ((MemoryMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName())).GetVisibleColumns();
    }

    [WebMethod]
    public List<MonitorsSettingsHelper.SimpleTreeNodeInfo> GetTreeGridCollumn()
    {
        try
        {
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var setings = ((MemoryMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, currAcc));
            var columns = setings.GetAllColumns();
            return helper.FormatTree(columns, false);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
}
