﻿<%@ WebService Language="C#" Class="ProfileManagement" %>
using System;
using System.Security;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Collections.Generic;
using System.ServiceModel;
using Resources;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Logging;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ProfileManagement : WebService
{
    Log log = new Log();

    [WebMethod(EnableSession = true)]
    public PageableDataTable GetProfiles()
    {
        var sortColumn = Context.Request.QueryString["sort"];
        var sortDirection = Context.Request.QueryString["dir"];

        var dt = GetProfilesDataTable(sortColumn, sortDirection);

        return new PageableDataTable(dt, dt.Rows.Count);
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, object> DeleteProfiles(int[] profileIDs)
    {
        try
        {
            var denyAccess = !((ProfileCommon)HttpContext.Current.Profile).AllowNodeManagement;

            if (denyAccess)
            {
                throw new SecurityException("Current account has insufficient rights for DeleteProfiles operation.");
            }
            var profileDAL = new ConnectionProfileDAL();
            profileDAL.DeleteMany(profileIDs);
            return null;
        }
        catch (Exception ex)
        {
            log.Error("Error in DeleteProfile", ex);

            var err = new Dictionary<string, object>();
            err["Error"] = true;
            err["Source"] = ToolsetWebContent.WEBDATA_SJ_01;
            if (ex is FaultException<InfoServiceFaultContract>)
                err["Msg"] = (ex as FaultException<InfoServiceFaultContract>).Detail.Message;
            else
                err["Msg"] = ex.Message;
            return err;
        }
    }

    private static DataTable GetProfilesDataTable(string sortColumn, string sortDirection)
    {
        var dt = new DataTable();
        dt.Columns.Add("ProfileID", typeof(int));
        dt.Columns.Add("ProfileName", typeof(string));
        var ProfileDAL = new ConnectionProfileDAL();
        ProfileDAL.FindAll().ForEach((profile) => dt.Rows.Add(new object[] { profile.ID, profile.Name}));
        dt.DefaultView.Sort = string.Format("{0} {1}", sortColumn, sortDirection);
        dt = dt.DefaultView.ToTable();
        return dt;
    }
}