﻿<%@ WebService Language="C#" Class="CredentialManager" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Services;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.Demo;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

/// <summary>
/// Summary description for CredentialManager
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class CredentialManager : System.Web.Services.WebService
{

    private CredentialsDAL CredServc = new CredentialsDAL();
    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    public ToolsDAL toolsDal = new ToolsDAL();

    [WebMethod]
    public PageableDataTable GetCredentials()
    {
        try
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Version", typeof(string));
            table.Columns.Add("Community", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("Context", typeof(string));
            table.Columns.Add("PrivacyType", typeof(string));
            table.Columns.Add("PrivacyPassword", typeof(string));
            table.Columns.Add("PrivacyKeyIsPassword", typeof(bool));
            table.Columns.Add("AuthenticationType", typeof(string));
            table.Columns.Add("AuthenticationPassword", typeof(string));
            table.Columns.Add("AuthenticationKeyIsPassword", typeof(bool));
            table.Columns.Add("CombineName", typeof(string));

            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            IEnumerable<SnmpCredentialsV2> credV2;
            if (OrionConfiguration.IsDemoServer)
                credV2 = DemoPredefinedInfo.GetV2Credentials();
            else
            credV2 = CredServc.GetToolsetAccountV2Credentials(currAcc).Cast<SnmpCredentialsV2>();
            credV2.ForEach(c =>
            {
                var r = table.NewRow();
                r["ID"] = c.ID;
                r["Name"] = c.Name;
                r["Version"] = SNMPVersion.SNMP2c.ToString();
                r["Community"] = c.Community;
                r["CombineName"] = string.Format("{0} [{1}]", Server.HtmlEncode(c.Name), "SNMPv1/2c");
                table.Rows.Add(r);
            });
            IEnumerable<SnmpCredentialsV3> credV3;
            if (OrionConfiguration.IsDemoServer)
                credV3 = DemoPredefinedInfo.GetV3Credentials();
            else
                credV3 = CredServc.GetToolsetAccountV3Credentials(currAcc).Cast<SnmpCredentialsV3>();
            
            credV3.ForEach(c =>
            {
                var r = table.NewRow();
                r["ID"] = c.ID;
                r["Name"] = c.Name;
                r["Version"] = SNMPVersion.SNMP3.ToString();
                r["UserName"] = c.UserName;
                r["Context"] = c.Context;
                r["PrivacyType"] = c.PrivacyType;
                r["PrivacyPassword"] = c.PrivacyPassword;
                r["PrivacyKeyIsPassword"] = c.PrivacyKeyIsPassword;
                r["AuthenticationType"] = c.AuthenticationType;
                r["AuthenticationPassword"] = c.AuthenticationPassword;
                r["AuthenticationKeyIsPassword"] = c.AuthenticationKeyIsPassword;
                r["CombineName"] = string.Format("{0} [{1}]", c.Name, "SNMPv3");
                table.Rows.Add(r);
            });
            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void DeleteCredential(int id)
    {
        try
        {
            if (OrionConfiguration.IsDemoServer)
            {
                return;
            }
            CredServc.DeleteToolsetCredential(id, OrionAccountHelper.GetCurrentLoggedUserName());
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void AddDefaultCredential(string name, string version, string community, string userName,
        string context,
        string privacyType, string privacyPassword, bool privacyKeyIsPassword, string authenticationType,
        string authenticationPassword, bool authenticationKeyIsPassword)
    {
        try
        {
            if(OrionConfiguration.IsDemoServer)
                return;
            int id = 0;
            SnmpCredentialsV2 credV2 = null;
            SnmpCredentialsV3 credV3 = null;
            ExtractType(id, name, version, community, userName, context, privacyType, privacyPassword, privacyKeyIsPassword, authenticationType, authenticationPassword, authenticationKeyIsPassword, ref credV2, ref credV3);
            if (credV2 != null)
            {
                credV2.SetIdToNull();
                CredServc.AddToolsetCredential(credV2, OrionAccountHelper.GetCurrentLoggedUserName());
            }
            else if (credV3 != null)
            {
                credV3.SetIdToNull();
                CredServc.AddToolsetCredential(credV3, OrionAccountHelper.GetCurrentLoggedUserName());
            }
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void CopyCredential(int id)
    {
        try
        {
            if (OrionConfiguration.IsDemoServer)
            {
                return;
            }
            CredServc.CopyToolsetCredential(id, OrionAccountHelper.GetCurrentLoggedUserName());
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void UpdateCredential(int id, string name, string version, string community, string userName,
        string context,
        string privacyType, string privacyPassword, bool privacyKeyIsPassword, string authenticationType,
        string authenticationPassword, bool authenticationKeyIsPassword)
    {
        try
        {
            if (OrionConfiguration.IsDemoServer)
            {
                return;
            }
            SnmpCredentialsV2 credV2 = null;
            SnmpCredentialsV3 credV3 = null;

            ExtractType(id, name, version, community, userName, context, privacyType, privacyPassword, privacyKeyIsPassword, authenticationType, authenticationPassword, authenticationKeyIsPassword, ref credV2, ref credV3);
            if (credV2 != null)
                CredServc.UpdateToolsetCredential(credV2);
            else if (credV3 != null)
                CredServc.UpdateToolsetCredential(credV3);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    private void ExtractType(int id, string name, string version, string community, string userName, string context,
        string privacyType, string privacyPassword, bool privacyKeyIsPassword, string authenticationType,
        string authenticationPassword, bool authenticationKeyIsPassword, ref SnmpCredentialsV2 credV2,
        ref SnmpCredentialsV3 credV3)
    {
        if (version.Equals(SNMPVersion.SNMP2c.ToString(), StringComparison.CurrentCultureIgnoreCase))
        {
            credV2 = new SnmpCredentialsV2();
            credV2.ID = id;
            credV2.Name = name;
            credV2.Community = community;
            credV2.Description = string.Empty;
        }
        else if (version.Equals(SNMPVersion.SNMP3.ToString(), StringComparison.CurrentCultureIgnoreCase))
        {
            var result = false;
            credV3 = new SnmpCredentialsV3();
            credV3.ID = id;
            credV3.Name = name;
            credV3.UserName = userName;
            credV3.Context = context;
            credV3.Description = string.Empty;

            SNMPv3AuthType tmpAuthType;
            result = SNMPv3AuthType.TryParse(authenticationType, true, out tmpAuthType);
            credV3.AuthenticationType = tmpAuthType;
            if (tmpAuthType != SNMPv3AuthType.None)
            {
                credV3.AuthenticationPassword = authenticationPassword;
                credV3.AuthenticationKeyIsPassword = authenticationKeyIsPassword;
            }
            else
            {
                credV3.AuthenticationPassword = "";
                credV3.AuthenticationKeyIsPassword = false;
            }
            SNMPv3PrivacyType tmpPrivType;
            result = SNMPv3PrivacyType.TryParse(privacyType, true, out tmpPrivType);
            if (tmpAuthType != SNMPv3AuthType.None)
            {
                credV3.PrivacyType = tmpPrivType;
                credV3.PrivacyPassword = privacyPassword;
                credV3.PrivacyKeyIsPassword = privacyKeyIsPassword;
            }
            else
            {
                credV3.PrivacyPassword = "";
                credV3.PrivacyKeyIsPassword = false;
            }
            if(!result)
                throw new InvalidCastException("Can't cast to SnmpCredentialV2 or SnmpCredentialV3 types");
        }
    }

    [WebMethod]
    public void ForceUpdateCredential(int id, string name, string version, string community, string userName,
        string context,
        string privacyType, string privacyPassword, bool privacyKeyIsPassword, string authenticationType,
        string authenticationPassword, bool authenticationKeyIsPassword)
    {
        SnmpCredentialsV2 credV2 = null;
        SnmpCredentialsV3 credV3 = null;

        try
        {
            if (OrionConfiguration.IsDemoServer)
            {
                return;
            }
            ExtractType(id, name, version, community, userName, context, privacyType, privacyPassword, privacyKeyIsPassword, authenticationType, authenticationPassword, authenticationKeyIsPassword, ref credV2, ref credV3);
            if (credV2 != null)
            {
                CredServc.DeleteToolsetCredential((int)credV2.ID, OrionAccountHelper.GetCurrentLoggedUserName());
                credV2.SetIdToNull();
                CredServc.AddToolsetCredential(credV2, OrionAccountHelper.GetCurrentLoggedUserName());
            }
            else if (credV3 != null)
            {
                CredServc.DeleteToolsetCredential((int)credV3.ID, OrionAccountHelper.GetCurrentLoggedUserName());
                credV3.SetIdToNull();
                CredServc.AddToolsetCredential(credV3, OrionAccountHelper.GetCurrentLoggedUserName());
            }
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public bool TestSnmpCredential(string snmpVersion, string ip, uint snmpPort, string community, string authKey, bool authKeyIsPwd, string authType, string privacyType, string privacyPassword, bool privKeyIsPwd, string context, string username)
    {
        if (OrionConfiguration.IsDemoServer)
        {
            return false;
        }
        if (snmpVersion == null) throw new ArgumentNullException("snmpVersion");
        if (ip == null) throw new ArgumentNullException("ip");
        if (community == null) throw new ArgumentNullException("community");
        if (authKey == null) throw new ArgumentNullException("authKey");
        if (authType == null) throw new ArgumentNullException("authType");
        if (privacyType == null) throw new ArgumentNullException("privacyType");
        if (privacyPassword == null) throw new ArgumentNullException("privacyPassword");
        if (context == null) throw new ArgumentNullException("context");
        if (username == null) throw new ArgumentNullException("username");
        
        var result = false;
        SNMPVersion _snmpVersion;
        SNMPv3AuthType _authType = SNMPv3AuthType.None;
        SNMPv3PrivacyType _privacyType = SNMPv3PrivacyType.None;

        result = SNMPVersion.TryParse(snmpVersion, true, out _snmpVersion);
        if (authType != "") result = SNMPv3AuthType.TryParse(authType, true, out _authType);
        if (privacyType != "") result = SNMPv3PrivacyType.TryParse(privacyType, true, out _privacyType);

        if (!result)
            throw new InvalidCastException("Can't cast to one of this type: SNMPVersion,SNMPv3AuthType,SNMPv3PrivacyType");
        try
        {
            AddIPToAcutoComplete(ip);
            
            IPAddress[] addresslist = Dns.GetHostAddresses(ip);
            foreach (IPAddress theaddress in addresslist)
            {
                if(CredServc.ValidateSNMP(_snmpVersion, theaddress.ToString(), snmpPort, community, authKey, authKeyIsPwd, _authType, _privacyType,
                privacyPassword, privKeyIsPwd, context, username))
                    return true;
            }
            return false;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
    
    private void AddIPToAcutoComplete(string ip)
    {
        new AutocompleteDAL().AddOrUpdateAutocompleteItem("snmpTest", "CredentialsManagmentSettings", OrionAccountHelper.GetCurrentLoggedUserName(), ip);
    }
}
