﻿<%@ WebService Language="C#" Class="TaskWebService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.Toolset.Common.Model;
using SolarWinds.Toolset.Common.Tasks;
using SolarWinds.Toolset.Common.Tools;
using SolarWinds.Toolset.Common.UserSessions;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Logging;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class TaskWebService  : System.Web.Services.WebService
{
    private static readonly Log _log = new Log();
    private TaskService _taskService = new TaskService();
    
    [WebMethod]
    public void CancelTask(string stringTaskId)
    {
        Guid taskId;
        if (Guid.TryParse(stringTaskId, out taskId))
        {
            _taskService.StopTask(taskId);
        }
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public IEnumerable<JobInfo> GetScheduledJobs()
    {
        var jobs =  _taskService.GetScheduledJobs();
        return jobs;
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public IEnumerable<JobInfo> GetAllJobs()
    {
        return _taskService.GetAllJobs();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public IEnumerable<TaskInfo> GetTaskInfos()
    {
        return _taskService.GetTaskInfos();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public IEnumerable<IUserSession> GetSessions()
    {
        return UserSessionHelper.GetSessions();
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public void StopTasks(string[] tasks)
    {
        foreach (var task in tasks)
        {
            _log.InfoFormat("Will stop task {0}", task);
            var id = new Guid(task);
            _taskService.StopTaskAndCloseSignalR(id);

        }
    }
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false, XmlSerializeString = false)]
    public void CancelJobs(string[] jobs)
    {
        foreach (var jobId in jobs)
        {
            _log.InfoFormat("Will cancel job {0}", jobId);
            var id = new Guid(jobId);
            _taskService.CancelJob(id);
        }
    }

    [WebMethod]
    public string CloneTasks(List<string> taskIds, int copies)
    {
        try
        {
            var runningTasks = _taskService.GetTasks().ToDictionary(x => x.TaskId);

            foreach (var id in taskIds)
            {
                Guid taskId;
                if (!Guid.TryParse(id, out taskId))
                {
                    continue;
                }

                if (!runningTasks.ContainsKey(taskId))
                {
                    continue;
                }

                var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(runningTasks[taskId].ToolId);

                for (var i = 0; i < copies; i++)
                {
                    CloneTask(toolFactory, runningTasks[taskId]);
                }
            }
            return null;
        }
        catch (Exception e)
        {
            _log.Error(e);
            return e.Message;
        }
    }

    private void CloneTask(IToolFactory toolFactory, Task task)
    {
        var tool = toolFactory.CreateTool();

        var newTask = new Task
        {
            AccountId = OrionAccountHelper.GetCurrentLoggedUserName(),
            PollInterval = task.PollInterval,
            ToolPermanentSettings = task.ToolPermanentSettings,
            Settings = task.Settings,
            Tool = tool,
            PollingEngineName = task.PollingEngineName,
            WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
            SessionId = UserSessionHelper.GetSessionId(Context)
        };

        new TaskService().RunTask(newTask);
    }
}