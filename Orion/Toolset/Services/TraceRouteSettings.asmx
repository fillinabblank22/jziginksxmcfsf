﻿<%@ WebService Language="C#" Class="TraceRouteSettings" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.ResultColumnsSettings;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.TraceRoute;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Hubs;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TraceRouteSettings : WebService
{
    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    public ToolsDAL toolsDal = new ToolsDAL();

    protected int ToolId
    {
        get
        {
            return SupportedTools.TraceRouteToolId;
        }
    }
    
    [WebMethod]
    public PageableDataTable GetSelectedColumns()
    {
        try
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("DisplayName", typeof(string));

            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var settings = ((TraceRoutePermanentSettings)toolsDal.GetToolPermanentSettings(1, currAcc));
            var allCols = settings.GetAllColumns();
            allCols.ForEach(c =>
            {
                if (settings.ColumnsOrder.Contains(c.PropertyName))
                {
                    var r = table.NewRow();
                    r["ID"] = c.PropertyName;
                    r["DisplayName"] = c.DisplayName;
                    table.Rows.Add(r);
                }
            });
            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public object SaveSettings(string table, string traceRoutePermanentSettingsJson)
    {
        try
        {
            if (OrionConfiguration.IsDemoServer)
            {
                return "It's demo version, settings cann't be saved";
            }
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var settings = ((TraceRoutePermanentSettings)toolsDal.GetToolPermanentSettings(1, currAcc));
            if (!string.IsNullOrEmpty(traceRoutePermanentSettingsJson))
            {
                settings = JSONDeserializer.DeserializeJSON<TraceRoutePermanentSettings>(
                    traceRoutePermanentSettingsJson);
            }

            var properties =
                JsonConvert.DeserializeObject<JArray>(table)
                    .SelectMany(o => ((JObject)o).Properties())
                    .Where(it => it.Name.Equals("id", StringComparison.InvariantCultureIgnoreCase))
                    .ToArray();

            settings.ResultColumnSettings.ForEach(it => it.Visible = false);
            List<ResultColumnSettings> columnSettings = new List<ResultColumnSettings>();
            foreach (var prop in properties)
            {
                var column = settings.ResultColumnSettings.FirstOrDefault(it => it.PropertyName.Equals(prop.Value.ToString()));
                if (column != null)
                {
                    column.Visible = true;
                    columnSettings.Add(column);
                }
            }
            settings.ColumnsOrder.Clear();
            columnSettings.ForEach(it => settings.ColumnsOrder.Add(it.PropertyName));
            
            settings.PollingInterval = settings.PollingInterval * 1000;
            toolsDal.SaveToolPermanentSettings(1, currAcc, settings);
            ResultNotificationHubWrapper.Instance.ReloadSettings(currAcc);
            return null;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            return e.Message;
        }
    }

    [WebMethod]
    public PageableDataTable GetColumns()
    {
        try
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("DisplayName", typeof(string));
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var userSettings = ((TraceRoutePermanentSettings)toolsDal.GetToolPermanentSettings(1, currAcc));
            var allCols = userSettings.GetAllColumns();
            allCols.ForEach(
                c =>
                    {
                        if (!userSettings.ColumnsOrder.Contains(c.PropertyName))
                        {
                            var r = table.NewRow();
                            r["ID"] = c.PropertyName;
                            r["DisplayName"] = c.DisplayName;
                            table.Rows.Add(r);
                        }
                    });
            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
    
    [WebMethod]
    public PageableDataTable GetAllColumns()
    {
        try
        {
            DataTable table = new DataTable();
            table.Columns.Add("selectedCol", typeof(bool));
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("DisplayName", typeof(string));
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var userSettings =
                ((TraceRoutePermanentSettings)toolsDal.GetToolPermanentSettings(1, currAcc));
            var allCols = userSettings.GetAllColumns();
            allCols.ForEach(c =>
            {
                var r = table.NewRow();
                r["selectedCol"] = userSettings.ColumnsOrder.Contains(c.PropertyName);
                r["ID"] = c.PropertyName;
                r["DisplayName"] = c.DisplayName;
                table.Rows.Add(r);
            });
            return new PageableDataTable(table, table.Rows.Count);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public List<ResultColumnSettings> GetResultColumns()
    {
        return ((TraceRoutePermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName())).GetVisibleColumns();
    }  
}
