﻿<%@ WebService Language="C#" Class="InterfaceMonitorSettings" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.ResultColumnsSettings;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.InterfaceMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Hubs;

/// <summary>
/// Summary description for CredentialManager
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class InterfaceMonitorSettings : WebService
{

    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    private MonitorsSettingsHelper helper = new MonitorsSettingsHelper();
    public ToolsDAL toolsDal = new ToolsDAL();

    protected int ToolId
    {
        get
        {
            return SupportedTools.InterfaceMonitorId;
        }
    }

    [WebMethod]
    public PageableDataTable GetSelectedColumns()
    {
        try
        {
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var visibleSetings = ((InterfaceMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, currAcc));
            return helper.FormatSelectedColumnsTable(visibleSetings,true);
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }

    [WebMethod]
    public void SaveChartLimits(MonitorsSettingsHelper.LimitsSettings settingsJson, ref InterfaceMonitorPermanentSettings settings)
    {
        ChartsRetentionSettings result;
        Enum.TryParse(settingsJson.LimitType, out result);
        settings.ChartDataLimitType = result;
        settings.ChartDataLimitByPoints = Convert.ToInt32(settingsJson.PointsValue);
        settings.ChartDataLimitByTimeMin = Convert.ToInt32(settingsJson.TimeValue);
        settings.InterfacePollInterval = Convert.ToInt32(settingsJson.PollingInterval) * 1000;
        settings.InterfacePollDriverInterval = Convert.ToInt32(settingsJson.PollingInterval) * 1000;
    }

    [WebMethod]
    public bool SaveThresholds(IEnumerable<MonitorsSettingsHelper.ThresholdSettings> settingsJson, ref InterfaceMonitorPermanentSettings settings)
    {
        var elements = settings.GetAllColumns();

        foreach (var setting in settingsJson)
        {
            if (!string.IsNullOrEmpty(setting.Name))
            {
                var element = elements.Single(c => c.PropertyName.Equals(setting.Name, StringComparison.CurrentCultureIgnoreCase));
                element.UseThreshold = Convert.ToBoolean(setting.UseThresholds);

                ThresholdOperator result;
                Enum.TryParse(setting.WarningLevel, out result);
                element.WarningThresholdOperator = result;
                element.CriticalThresholdOperator = result;

                element.WarningThresholdValue = Convert.ToDouble(setting.WarningValue);
                element.CriticalThresholdValue = Convert.ToDouble(setting.CriticalValue);
            }
        }
        return true;
    }

    [WebMethod]
    public void SaveMetrics(string table, ref InterfaceMonitorPermanentSettings settings)
    {
        try
        {
            var visibleSettings = settings.GetAllColumns();
            settings = (InterfaceMonitorPermanentSettings)helper.SaveSettingsFromTable(table, visibleSettings, settings, OrionAccountHelper.GetCurrentLoggedUserName());
        }
        catch (Exception e)
        {
            _myLog.Error(e);
        }
    }

    [WebMethod]
    public bool SaveAllSettings(MonitorsSettingsHelper.LimitsSettings charts, string table,
        IEnumerable<MonitorsSettingsHelper.ThresholdSettings> thresholds)
    {
        try
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                return false;
            }
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var settings = (InterfaceMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, currAcc);

            SaveChartLimits(charts, ref settings);
            SaveMetrics(table, ref settings);
            SaveThresholds(thresholds, ref settings);

            toolsDal.SaveToolPermanentSettings(ToolId, currAcc, settings);
            ResultNotificationHubWrapper.Instance.ReloadSettings(currAcc);
            return true;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            return false;
        }
    }
  
    [WebMethod]
    public List<ResultColumnSettings> GetVisibleResultColumns()
    {
        return ((InterfaceMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName())).GetVisibleColumns();
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public List<ResultColumnSettings> GetResultColumns()
    {
        //return two lines below once permanent settings are done and delete dummy column settings
        var toolPermanentSettings = (InterfaceMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName());
        return toolPermanentSettings.GetVisibleColumns();
    }

    [WebMethod]
    public List<MonitorsSettingsHelper.SimpleTreeNodeInfo> GetTreeGridCollumn()
    {
        try
        {
            var currAcc = OrionAccountHelper.GetCurrentLoggedUserName();
            var setings = ((InterfaceMonitorPermanentSettings)toolsDal.GetToolPermanentSettings(ToolId, currAcc));
            var columns = setings.GetAllColumns();
            var result = helper.FormatTree(columns, true);
            return result;
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
}
