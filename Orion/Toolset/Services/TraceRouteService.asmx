﻿<%@ WebService Language="C#" Class="TraceRouteService" %>
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Containers;
using SolarWinds.Toolset.Common.Demo;
using SolarWinds.Toolset.Common.Tasks;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.TraceRoute;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TraceRouteService : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();
    public ToolsDAL toolsDal = new ToolsDAL();

    protected int ToolId
    {
        get
        {
            return SupportedTools.TraceRouteToolId;
        }
    }

    [WebMethod]
    public bool GetDNSStatus()
    {
        return ((TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName())).IsDNSResolvingEnabled;
    }
    [WebMethod]
    public void StopTrace(string taskId)
    {
        new TaskService().StopTask(new Guid(taskId));
    }
    public static DataTable ToDataTable<T>(IList<T> data)
    {
        PropertyDescriptorCollection props =
            TypeDescriptor.GetProperties(typeof(T));
        DataTable table = new DataTable();
        for (int i = 0; i < props.Count; i++)
        {
            PropertyDescriptor prop = props[i];
            table.Columns.Add(prop.Name, prop.PropertyType);
        }
        object[] values = new object[props.Count];
        foreach (T item in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = props[i].GetValue(item);
            }
            table.Rows.Add(values);
        }
        return table;
    }
    
    [WebMethod]
    public string RunTrace(string input, string controlId, string controlType, string pollingEngine, bool resolveDNS)
    {
        try
        {
            input = input.Trim();
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            if (OrionConfiguration.IsDemoServer)
            {
                var hosts = DemoPredefinedInfo.GetAllHosts();
                if (!hosts.Any(it => it.DisplayName == input))
                {
                    return string.Empty;
                }
            }
            // Get necessary stuff
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (TraceRouteTool)toolFactory.CreateTool();
            var taskSettings = (TraceRouteTaskSettings)toolFactory.CreateTaskSettings();
            var toolPermanentSettings = (TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName());

            // Set task settings
            taskSettings.HostNameOrIp = Server.HtmlEncode(input);

            //Saving isDNSResolutionEnabled to DB
            toolPermanentSettings.IsDNSResolvingEnabled = resolveDNS;
            new ToolsDAL().SaveToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName(), toolPermanentSettings);
            
            if (!OrionConfiguration.IsDemoServer)
            {
                new AutocompleteDAL().AddOrUpdateAutocompleteItem(
                      controlId,
                      controlType,
                      OrionAccountHelper.GetCurrentLoggedUserName(),
                      input);
                new ToolsDAL().SaveToolPermanentSettings(ToolId, OrionAccountHelper.GetCurrentLoggedUserName(), toolPermanentSettings);
            }
            
            // Create a new task
            var task = new Task()
            {
                AccountId = OrionAccountHelper.GetCurrentLoggedUserName(),
                PollInterval = TimeSpan.FromMilliseconds(toolPermanentSettings.PollingInterval),
                ToolPermanentSettings = toolPermanentSettings,
                Settings = taskSettings,
                Tool = tool,
                PollingEngineName = pollingEngine,
                WebServerBaseUrl = UrlHelper.GetWebServerRootAddress(),
                SessionId = UserSessionHelper.GetSessionId(Context)
            };

            // Run task
            new TaskService().RunTask(task);
            return task.TaskId.ToString();
        }
        catch (Exception e)
        {
            _myLog.Error(e);
            throw;
        }
    }
}
