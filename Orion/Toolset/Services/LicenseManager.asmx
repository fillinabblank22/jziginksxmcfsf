﻿<%@ WebService Language="C#" Class="LicenseManager" %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class LicenseManager : System.Web.Services.WebService
{
    readonly LicenseSettingsDAL licenseSettingsDal = new LicenseSettingsDAL();

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public PageableDataTable GetAllUsers(string accountID)
    {
        int pageSize;
        int startRowNumber;

        string sortColumn = this.Context.Request.QueryString["sort"];
        string sortDirection = this.Context.Request.QueryString["dir"];

        Int32.TryParse(this.Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(this.Context.Request.QueryString["limit"], out pageSize);

        if (pageSize == 0)
        {
            pageSize = 10;
        }

        var dataSource = licenseSettingsDal.GetUsersTable(accountID, sortColumn, sortDirection, startRowNumber, pageSize);
        return dataSource;
    }

    [WebMethod]
    public void EnableRights(IEnumerable<string> ids)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return;
        }
        foreach (string name in ids)
        {
            if (licenseSettingsDal.CanEnableWebToolset())
            {
                licenseSettingsDal.EnableWebToolset(name, "true");
            }
            else
            {
                throw new Exception("Cant Enable WebToolset");
            }
        }
    }

    [WebMethod]
    public void DisableRights(IEnumerable<string> ids)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            return;
        foreach (string name in ids)
        {
            licenseSettingsDal.EnableWebToolset(name, "false");
        }
    }

    [WebMethod]
    public string GetCounts()
    {
        return string.Format("{0},{1}", licenseSettingsDal.UsersCountWithWebToolsetRight().ToString(), licenseSettingsDal.GetSeatsCount().ToString());
    }
}