﻿<%@ WebService Language="C#" Class="Toolset" %>
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Model;
using SolarWinds.Toolset.Common.Model;
/// <summary>
/// Summary description for Toolset
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Toolset : WebService
{
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public bool HasToolsEnabled()
    {
        return (new LicenseSettingsDAL()).HasToolsEnabled(Context.User.Identity.Name);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = true)]
    public bool HasToolsetIntegrationEnabledForUser()
    {
        return ((ProfileCommon)Context.Profile).ToolsetIntegration;
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public UserTool[] GetUserTools()
    {
        return (new LicenseSettingsDAL()).GetUserAllowedTools(Context.User.Identity.Name).ToArray();
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string[] GetNodeNetObjectIds(string[] ipaddress)
    {
        return (new CommonDAL()).GetNodeNetObjectIds(ipaddress);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string[] GetSNMPInformation(string[] ipaddress)
    {
        return (new CommonDAL()).GetSNMPInformation(ipaddress);
    }     

    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string[] GetInterfaceNetObjectIds(string[] interfaces, string[] ipaddress)
    {
        return (new CommonDAL()).GetInterfaceNetObjectIds(interfaces, ipaddress);
    }    
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string[] GetInterfaceNetObjectIdsById(string[] ids, string[] ipaddress)
    {
        return (new CommonDAL()).GetInterfaceNetObjectIdsById(ids, ipaddress);
    }

    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string[] GetInterfaceNetObjectIdsByIndex(string[] ids, string[] ipaddress)
    {
        return (new CommonDAL()).GetInterfaceNetObjectIdsByIndex(ids, ipaddress);
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = true)]
    public int GetColumnWidth()
    {
        return (new CommonDAL()).GetColumnWidth();
    }
    
    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public bool IsNumberOfOpenedToolDetailsReached(bool isMobileMode)
    {
        return (new UserSessionsDAL()).IsNumberOfOpenedToolDetailsReached(UserSessionHelper.GetSessionId(Context), isMobileMode);
    }
    

    [WebMethod]
    [ScriptMethod(UseHttpGet = false)]
    public string GetHelpLink(string fragment)
    {
        return HelpHelper.GetHelpUrl(fragment);
    }
}
