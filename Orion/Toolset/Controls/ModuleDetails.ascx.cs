﻿using System;
using System.Collections.Generic;
using System.Data;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Toolset.Common;

public partial class Orion_Toolset_Controls_ModuleDetails : System.Web.UI.UserControl
{
    const string Unlimited = "Unlimited";

    private static Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                // TODO: Use constant
                GetModuleAndLicenseInfo("Toolset");
            }
            catch (Exception ex)
            {
                _log.Error("Error while displaying details for WPM module.", ex);
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            ToolsetDetails.Name = module.ProductDisplayName;

            values.Add("Product Name", module.ProductName);
            values.Add("Version", module.Version);
            if (!string.IsNullOrWhiteSpace(Request.QueryString["BuildInfo"]))
            {
                values.Add("Build", typeof(IToolsetBusinessLayer).Assembly.GetName().Version.ToString());
            }
            values.Add("Service Pack", String.IsNullOrEmpty(module.HotfixVersion) ? "None" : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add("License", module.LicenseInfo);

            AddToolsetInfo(values);

            ToolsetDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }

    private void AddToolsetInfo(Dictionary<string, string> values)
    {
        DataTable licenseSummary;

        using (var proxy = BusinessLayerFactory.Create())
        {
            licenseSummary = proxy.GetLicenseSummary();
        }

        if (licenseSummary == null || licenseSummary.Rows.Count != 1)
            return;

        values.Add("Number of Activated Licenses", licenseSummary.Rows[0]["NumberOfLicenses"].ToString());
        values.Add("Nodes in License(s)", licenseSummary.Rows[0]["NodesInLicense"].ToString());
        values.Add("Volumes in License(s)", licenseSummary.Rows[0]["VolumesInLicense"].ToString());
        values.Add("Seats in License(s)", licenseSummary.Rows[0]["SeatsInLicense"].ToString());
        values.Add("Seats Taken", licenseSummary.Rows[0]["SeatsTaken"].ToString());
        values.Add("Seats Available", licenseSummary.Rows[0]["SeatsAvailable"].ToString());
    }
}