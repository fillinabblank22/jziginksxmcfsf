﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolIconCellContent.ascx.cs" Inherits="Orion_Toolset_Controls_ToolIconCellContent" %>

<a id="ToolLink" runat="server" style="display:block; padding: 5px; border: 2px solid #CCCCCC; float:left;">
    <img src="<%= ToolIconPath %>" alt="Tool Icon" width="32" height="32" />
</a>

<script type="text/javascript">
    function launchTool(url) {
        if (url != null && url.length != 0) {
            window.open(url);
        } else {
            alert("<%= Resources.ToolsetWebContent.WEBDATA_SL_05 %>");
        }
    }
</script>