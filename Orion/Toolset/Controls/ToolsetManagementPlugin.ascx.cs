﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Plugins;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Model;

public partial class Orion_Toolset_Controls_ToolsetManagementPlugin : UserControl, IManagementTasksPlugin
{
    internal bool WebToolsetEnabledForUser = true; 

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        OrionInclude.CoreFile("/js/jquery/timePicker.css");
        OrionInclude.CoreFile("/styles/UnmanageDialog.css");
        OrionInclude.CoreFile("/styles/NodeMNG.css");
    }

    public IEnumerable<ManagementTaskItem> GetManagementTasks(NetObject netObject, string returnUrl)
    {
        var node = netObject as Node;
        if (node == null)
            yield break;
        
        string nodeSectionName = Resources.ToolsetWebContent.WebData_MD0_38;

        string encryptedCommunityString = string.IsNullOrEmpty(node.CommunityString)
            ? string.Empty
            : System.Web.HttpServerUtility.UrlTokenEncode(Encoding.UTF8.GetBytes(node.CommunityString));

        LicenseSettingsDAL licenseSettingsDAL = new LicenseSettingsDAL();
        WebToolsetEnabledForUser = licenseSettingsDAL.WebToolsetEnabledForUser(Context.User.Identity.Name);
        if (WebToolsetEnabledForUser)
        {
            List<UserTool> tools = licenseSettingsDAL.GetUserAllowedTools(Context.User.Identity.Name);
            if (!tools.Any())
            {
                WebToolsetEnabledForUser = false;
                yield break;
            }

            foreach (UserTool userTool in tools)
            {
                yield return new ManagementTaskItem
                {
                    Section = nodeSectionName,
                    LinkInnerHtml = string.Format("<span  description=\"{2}\" details=\"{3}\" toolId=\"{0}\" class=\"toolsetButton\" ><img width=\"16px\" height=\"16px\" src='/Orion/Toolset/images/AppIcons/{4}' alt=''/>&nbsp;{1}</span>",
                            userTool.Key, userTool.Name, userTool.Description, userTool.DetailedDescription, userTool.IconFileName),
                    LinkUrl = string.Format("/Orion/Toolset/Tools/{3}/{3}.aspx?hostname={0}&ip={1}&community={2}", node.Hostname, node.IPAddress, encryptedCommunityString, userTool.Key),
                    NewWindow = true
                };
            }
            yield return new ManagementTaskItem
            {
                Section = nodeSectionName,
                LinkInnerHtml = string.Format("<img width=\"16px\" height=\"16px\" src='/Orion/Toolset/images/all-tools.png' alt='' />&nbsp;{0}", Resources.ToolsetWebContent.WebData_MD0_39),
                LinkUrl = "/Orion/Toolset/AllTools.aspx",
                NewWindow = true
            };
        }
    }
}