﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolIconWithNameContent.ascx.cs"
    Inherits="Orion_Toolset_Controls_ToolIconWithNameContent" %>
<div style="padding-top: 7px; padding-bottom: 2px;">
    <a id="ToolLink" runat="server">
        <img src="<%= ToolIconPath %>" height="16" width="16" style="padding-bottom: 3px; vertical-align: middle;"/> 
        <span style="font-size: 12px; font-family: Arial, Helvetica, Sans-Serif; font-weight: bold; padding-left: 10px">
        <%= ToolName %></span>
    </a>
</div>
<script type="text/javascript">
    function launchTool(url) {
        if (url != null && url.length != 0) {
            window.open(url);
        } else {
            alert("<%= Resources.ToolsetWebContent.WEBDATA_SL_05 %>");
        }
    }
</script>
