﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using FiftyOne.Foundation.Mobile.Redirection;
using SolarWinds.Toolset.Web.DAL;


public partial class Orion_Toolset_Controls_SelectNodesWizardPartControl : System.Web.UI.UserControl
{
    private int profileID;

    private string ReturnUrl
    {
        set
        {
            if (ViewState["ReturnUrl"] == null)
                ViewState["ReturnUrl"] = value;
        }
        get { return Convert.ToString(ViewState["ReturnUrl"]); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        profileID = Convert.ToInt32(Request.QueryString["ProfileID"]);
        if (!IsPostBack)
        {
            ReturnUrl = Page.Request.UrlReferrer != null ? Page.Request.UrlReferrer.ToString() : "/Orion/Toolset/Admin/ConnectionProfiles/ProfileManagement.aspx";

            var connectionProfDAL = new ConnectionProfileDAL();
            int[] nodeIDs = connectionProfDAL.FindNodesforProfile(profileID);
            nodesBrowser.SelectedNodes = (from int nodeID in nodeIDs
                                          select new Orion_Toolset_Controls_NodesBrowser_NodesBrowser.Node()
                                          {
                                              ID = nodeID
                                          }).ToList();
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        int[] nodeIDs = nodesBrowser.SelectedNodes.Select(ids => ids.ID).ToArray();
        var connectionProfDAL = new ConnectionProfileDAL();
        connectionProfDAL.AssignToNodes(nodeIDs, profileID);
        Response.Redirect(ReturnUrl);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
       Response.Redirect(ReturnUrl);
    }



}
