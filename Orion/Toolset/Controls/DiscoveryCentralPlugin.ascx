﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryCentralPlugin.ascx.cs" Inherits="Orion_Toolset_Controls_DiscoveryCentralPlugin" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="~/Orion/Toolset/Images/toolbox_icon32x32v2.png"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2><%=Resources.ToolsetWebContent.WEBDATA_TM0_01%></h2>
    <div>
        <%=Resources.ToolsetWebContent.WEBDATA_TM0_02%>
    </div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionPHDiscoveryHome"
        HelpDescription="<%$ Resources: ToolsetWebContent, WEBDATA_TM0_03 %>" CssClass="helpLink" />
        
    <!-- I can't use ManagedNetobjectInfo since seats are not netobjects -->
    <div class="managedObjInfoWrapper">   
        <span id="monitoredObjectsInfo" runat="server">
        <asp:Image ID="okImage" runat="server" ImageUrl="~/Orion/images/ok_16x16.gif" CssClass="stateImage"></asp:Image>
        <asp:Image ID="bulbImage" runat="server" ImageUrl="~/Orion/images/lightbulb_tip_16x16.gif" Visible="false" CssClass="stateImage"></asp:Image>
        <span style="font-weight:bold"><%= SeatsTakenCount %></span>
        <%= Resources.ToolsetWebContent.WEBDATA_TM0_04%></span>
    </div>
    
    <div class="sw-btn-bar">
        <orion:LocalizableButtonLink ID="LocalizableButtonLink3" runat="server" Text="<%$ Resources: ToolsetWebContent, WEBDATA_TM0_07 %>" 
                                     NavigateUrl="~/Orion/Toolset/AllTools.aspx" DisplayType="Secondary" />
    </div>
</div>
