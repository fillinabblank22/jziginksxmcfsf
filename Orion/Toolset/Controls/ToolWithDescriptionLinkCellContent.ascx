﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolWithDescriptionLinkCellContent.ascx.cs"
    Inherits="Orion_Toolset_Controls_ToolWithDescriptionCellContent" %>
<a id="ToolLink" runat="server" href=""><span class="ToolName" style="font-size: 12px;
    margin: 0 0 7px 0; display: block; cursor: pointer;">
    <%= ToolName %></span> </a><span class="ToolDescription" style="font-size: 12px;
        font-style: normal;">
        <%= ToolDescription %></span>
<br />
<a id="learnmore" runat="server" style="font-size: 12px; line-height: 13px; vertical-align: middle;
    color: #4D6897; cursor: pointer;"><span style="padding: 0 5px 0 0">&raquo;</span>Learn
    more</a>
<script type="text/javascript">
    $(document).ready(function() {
        var msgDiv = $('<div id="dialog-message" class="wrapper-content-popup" title="<%= ToolName %>" >' +
            '<div class="left-content-popup" style="overflow: hidden;">' +
            '<div>' +
            '<p style="padding-left: 10px">' +
            '<b><%= ToolDescription %></b>' +
            '</p>' +
            '<p>' +
            '<ul>' +
            <%= this.ToolDetailedDescription %>
        '</ul>' +
            '</p>' +
            '<p>' +
            '</p>' +
            '</div>' +
            '<div style="clear:both;overflow:hidden;margin:30px 0 0 0;">' +
            '<div style="display:inline-block;margin-Left:15px;">' +
            '<orion:LocalizableButton ID="RunButton" DisplayType="Primary" ClientIDMode="Static" LocalizedText="CustomText" runat="server"></orion:LocalizableButton>'+
        '<a target="_blank"  style="color:#5A99DC; margin-Left: 25px; bottom:0px; font-size: 12px; line-height: 13px; vertical-align: middle;" href="<%= this.ToolHelp %> ">» Read the Admin Guide for <%= ToolName %></a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="right-content-popup">' +
            '<img class="pop-image" src="images/AppsScreenshots/<%= ToolScreenshot %>" title="popupScreenshot"/>' +
            '</div>' +
            '</div>')
        ;
        $("#<%=learnmore.ClientID %>").click(function() {
            $(msgDiv).dialog({
                modal: true,
                width: 800
            });
        });
    });
</script>
