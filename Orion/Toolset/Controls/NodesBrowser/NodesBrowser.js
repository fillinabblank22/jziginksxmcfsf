﻿Ext.namespace('SW');
Ext.namespace('SW.VoIP');

SW.VoIP.NodesBrowser = function () {
    var errorTitle = "Error";

    var initialized;

    var grid;
    var selectorModel;
    var searchField;
    var pageSizeBox;
    var pagingToolbar;
    var dataStore;

    var initialPage = 1;
    var pageSize = 10;
    var userName = "";
    var loadingMask = null;

    var isMultiSelect = false;
    var preselectedGroup = 'Orion.Nodes.Vendor';

    var groupByCombo;
    var groupByStore;
    var groupByCategoriesStore;
    var groupByList;
    var groupingPanel;
    var selectedNodesFieldId;
    var selectedNodes = new Ext.util.MixedCollection(false, function (o) { return o.ID; });
    var excludedNodesIdsFieldId;
    var excludedNodes = [];

    var invalidPageSizeText = "Invalid Page size Text";

    var selectedHostsTemplate = '<li title="{0}"><table style="width: 100%"><td> <div>&#8226; {0}</div></td><td style="width: 20px;"><img src="/Orion/Discovery/images/icon_delete.gif" style="cursor:pointer;" title="Remove from list." onclick="SW.VoIP.NodesBrowser.RemoveHost({1});"></td></table></li>';

    GetPageSize = function () {
        var pSize = parseInt(getCookie(userName + '_VoIP_NodesBrowser_PageSize'), 10);
        return isNaN(pSize) ? pageSize : pSize;
    };

    RefreshObjects = function () {
        if (loadingMask == null) {
            loadingMask = new Ext.LoadMask(grid.el, {
                msg: 'Loading Nodes ...'
            });
        }

        var records = groupByList.getSelectedRecords();
        var groupByItem = '';
        if (records.length > 0)
            groupByItem = records[0].data.Value;

        grid.store.on('beforeload', function () { loadingMask.show(); });
        grid.store.proxy.conn.jsonData = {
            groupByCategory: groupByCombo.getValue(),
            groupByItem: groupByItem,
            excludedNodes: Ext.util.JSON.encode(excludedNodes)
        };
        grid.store.baseParams = { start: (initialPage - 1) * GetPageSize(), limit: GetPageSize(), search: searchField.getValue() };
        grid.store.on('load', function () {
            loadingMask.hide();
            RefreshSelected();
        });
        grid.store.load();
    };

    RefreshGroupByList = function (groupByCategory, handler) {
        groupByStore.proxy.conn.jsonData = { groupByCategory: groupByCategory };
        if (handler) {
            var internalHandler = function () {
                handler();
                groupByStore.un('load', internalHandler);
            };
            groupByStore.on('load', internalHandler);
        }

        groupByStore.load();
    };

    GetGridHeight = function () {
        var viewportHeight;

        // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
        if (typeof window.innerWidth != 'undefined') {
            viewportHeight = window.innerHeight;
        }
        // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
        else if (typeof document.documentElement != 'undefined'
            && typeof document.documentElement.clientWidth !=
                'undefined' && document.documentElement.clientWidth != 0) {
            viewportHeight = document.documentElement.clientHeight;
        }
        // older versions of IE
        else {
            viewportHeight = document.getElementsByTagName('body')[0].clientHeight;
        }

        var height = viewportHeight - 410;
        if (height >= 200)
            return height;
        else
            return 200;
    };

    InitGroupingPanel = function () {

        groupByCategoriesStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Controls/NodesBrowser/NodesBrowser.asmx/GetGroupByCategories",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 }
                ],
                "Label");
      
        groupByCategoriesStore.proxy.conn.jsonData = {};

        groupByCombo = new Ext.form.ComboBox({
            store: groupByCategoriesStore,
            valueField: 'Value',
            displayField: 'Label',
            triggerAction: 'all',
            lazyInit: false,
            width: 187,
            editable: false,
            transform: 'groupBySelect',
            hiddenId: 'groupBySelect',
            listeners: {
                select: function (record, index) {
                    RefreshGroupByList(record.value);

                    if (record.value == 'none')
                        RefreshObjects();
                }
            }
        });

        groupByCategoriesStore.load({
            callback: function (r, options, success) {
                groupByCombo.setValue(preselectedGroup);
                RefreshGroupByList(preselectedGroup);
            }
        });

        var selectPanel = new Ext.Container({
            applyTo: 'VoIP_GroupingPanel',
            region: 'north',
            height: 50,
            layout: 'form'
        });

        var tpl = new Ext.XTemplate(
		    '<tpl for=".">',
                '<div class="sw-voip-nodes-browser-groupby-item" id="{Value}">',
                '<tpl if="Type == \'Orion.Nodes.Status\'"><img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={Value}&amp;size=small" /> </tpl>',
                '<tpl if="Type == \'Orion.Nodes.Vendor\'"><img src="/NetPerfMon/images/Vendors/{Metadata}" /> </tpl>',
                '{Label}</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
	    );

        groupByStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Controls/NodesBrowser/NodesBrowser.asmx/GetGroupByItems",
                [
                    { name: 'Value', mapping: 0 },
                    { name: 'Label', mapping: 1 },
                    { name: 'Type', mapping: 2 },
                    { name: 'Metadata', mapping: 3 },
                ],
                "Label");


      
        groupByList = new Ext.DataView({
            cls: 'VoIP_GroupByList',
            border: true,
            store: groupByStore,
            tpl: tpl,
            height: GetGridHeight() - selectPanel.getSize().height,
            autoHeight: false,
            autoScroll: true,
            singleSelect: true,
            selectedClass: "x-grid3-row-selected",
            overClass: "x-grid3-row-over",
            itemSelector: 'div.sw-voip-nodes-browser-groupby-item',
            listeners: {
                selectionchange: function (view, selections) {
                    if (selections.length > 0)
                        RefreshObjects();
                }
            }
        });

        groupingPanel = new Ext.Panel({
            region: 'west',
            width: 200,
            split: true,
            items: [
                selectPanel,
                groupByList
            ]
        });

        groupingPanel.on('bodyresize', function () {
            groupByList.setSize(groupingPanel.getSize().width, groupingPanel.getSize().height - selectPanel.getSize().height);
        });
    };

    function renderName(value, meta, record) {
        return renderDefault(
                    String.format('<img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&amp;status={0}&amp;size=small" /> {1}', record.data.Status, value),
                meta, record);
    }

    function renderDefault(value, meta, record) {
        return value;
    }

    UpdateSelectedHostsList = function () {
        $('#sw-voip-nodes-count').text(String.format("{0} Node(s) selected", selectedNodes.getCount()));
        var listhtml = '';

        selectedNodes.each(function (item) {
            listhtml += String.format(selectedHostsTemplate, item.Name, item.ID);
        });

        $('#sw-voip-nodes-browser-list ul').html(listhtml);
    };

    RefreshSelected = function () {
        var toSelect = [];
        dataStore.data.each(function (record) {
            if (selectedNodes.containsKey(record.data.ID)) {
                toSelect.push(record);
            }
        }, this);
        selectorModel.selectRecords(toSelect);
    };

    UpdateSelectedNodeIds = function () {
        var ids = Array();
        selectedNodes.each(function (item) {
            ids.push(item);
        });

        $('#' + selectedNodesFieldId).val(Ext.util.JSON.encode(ids));

        UpdateSelectedHostsList();
    };

    InitLayout = function () {
        var panel = new Ext.Container({
            renderTo: 'sw-voip-nodes-browser-nodes-grid',
            height: GetGridHeight(),
            layout: 'border',
            items: [groupingPanel, grid]
        });
        Ext.EventManager.onWindowResize(function () {
            panel.setHeight(GetGridHeight());
            panel.doLayout();
        }, panel);
    };

    InitGrid = function () {
        
            selectorModel = new Ext.grid.CheckboxSelectionModel();
       
        selectorModel.on('rowselect', function (sm, rowIndex, r) {
           selectedNodes.add({ ID: r.data.ID, Name: r.data.DisplayName });
            UpdateSelectedNodeIds();
        });

        selectorModel.on('rowdeselect', function (sm, rowIndex) {
            var r = dataStore.data.itemAt(rowIndex);
            selectedNodes.removeKey(r.data.ID);
            UpdateSelectedNodeIds();
        });
     
        dataStore = new ORION.WebServiceStore(
                            "/Orion/Toolset/Controls/NodesBrowser/NodesBrowser.asmx/GetNodesPaged",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'IPAddress', mapping: 1 },
                                { name: 'DisplayName', mapping: 2 },
                                { name: 'Status', mapping: 3 }
                            ],
                            "DisplayName");
        
        dataStore.addListener("exception", function (dataProxy, type, action, options, response, arg) {
            var error = eval("(" + response.responseText + ")");
            Ext.Msg.show({
                title: errorTitle,
                msg: error.Message,
                icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
            });

            loadingMask.hide();
        });

        dataStore.on('datachanged', RefreshSelected);

        searchField = new Ext.ux.form.SearchField({
            store: dataStore,
            width: 220,
            emptyText: 'Search nodes',
            paramName: 'search'
        });

        pageSizeBox = new Ext.form.NumberField({
            id: 'PageSizeField',
            width: 40,
            allowBlank: false,
            minValue: 1,
            maxValue: 1000,
            value: GetPageSize()
        });

        pageSizeBox.on('change', function (f, numbox, o) {
            var pSize = parseInt(numbox, 10);
            if (isNaN(pSize) || pSize < 1 || pSize > 1000) {
                Ext.Msg.show({
                    title: errorTitle,
                    msg: invalidPageSizeText,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });

                return;
            }

            if (pagingToolbar.pageSize != pSize) { // update page size only if it is different
                pagingToolbar.pageSize = pSize;
                setCookie(userName + '_VoIP_NodesBrowser_PageSize', pSize, 'months', 1);
                pagingToolbar.doLoad(pagingToolbar.cursor);
            }

        });

        pagingToolbar = new Ext.PagingToolbar({
            store: dataStore,
            pageSize: GetPageSize(),
            displayInfo: true,
            displayMsg: 'Displaying nodes {0} - {1} of {2}',
            emptyMsg: 'No nodes to display',
            items: [
                    '-',
                    'Number of items per page:',
                    pageSizeBox
                ],
            listeners: {
                change: function (paging, params) { $('#VoIP_CurrentPage').val(params.activePage); }
            }
        });

        grid = new Ext.grid.GridPanel({

            store: dataStore,

            columns: [
                    selectorModel,
                    { header: 'ID', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                    { id: 'node-name', header: 'Node Name', width: 200, sortable: true, dataIndex: 'DisplayName', renderer: renderName },
                    { id: 'node-ip', header: 'Node IP', width: 500, sortable: true, dataIndex: 'IPAddress', renderer: renderDefault }
                ],

            sm: selectorModel,

            layout: 'fit',
            region: 'center',
            autoscroll: true,
            stripeRows: true,
            autoExpandColumn: 'node-ip',
            viewConfig: {
                emptyText: 'Either no nodes are available, or no nodes match the specified criteria.'
            },

            tbar: [
					'->',
                        searchField
                    ],

            bbar: pagingToolbar
        });

    };
    ORION.prefix = "VoIP_NodesBrowser_";
    return {
        SetPageSize: function (size) {
            pageSize = size;
        },
        SetUserName: function (user) {
            if (user)
                userName = user;
        },
        SetMultiSelect: function (allowMultiSelect) {
            isMultiSelect = allowMultiSelect;
        },
        SelectedNodesFieldId: function (id) {
            selectedNodesFieldId = id;
            var obj = Ext.util.JSON.decode($('#' + selectedNodesFieldId).val());

            selectedNodes.clear();
            $.each(obj, function (index, value) {
                selectedNodes.add(value);
            });
            UpdateSelectedNodeIds();
        },
        ExcludedNodesIdsFieldId: function (id) {
            excludedNodesIdsFieldId = id;
            var obj = Ext.util.JSON.decode($('#' + excludedNodesIdsFieldId).val());

            excludedNodes = [];
            $.each(obj, function (index, value) {
                excludedNodes.push(value);
            });
        },
        RemoveHost: function (id) {
            selectedNodes.removeKey(id);
            RefreshSelected();
            UpdateSelectedNodeIds();
        },
        GetSelectedNodeIds: function () {
            var ids = Array();
            selectedNodes.each(function (item) {
                ids.push(item);
            });

            return ids;
        },
        init: function () {

            if (initialized)
                return;

            initialized = true;

            var currentPage = parseInt($('#VoIP_CurrentPage').val());
            if (!isNaN(currentPage))
                initialPage = currentPage;

            InitGroupingPanel();
            InitGrid();
            InitLayout();

            RefreshObjects();
        }
    };
} ();

Ext.onReady(SW.VoIP.NodesBrowser.init, SW.VoIP.NodesBrowser);