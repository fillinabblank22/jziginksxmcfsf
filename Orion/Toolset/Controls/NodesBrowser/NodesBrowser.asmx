﻿<%@ WebService Language="C#" Class="NodesBrowser" %>
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Helpers;
using System.Web.Script.Serialization;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NodesBrowser : WebService
{
    private const string NodesEntity = "Orion.Nodes";
    private static ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
    private static readonly string UnknownValue = manager.SearchAll("WEBDATA_PS0_0", manager.GetAllResourceManagerIds());
    
    [WebMethod]
    public PageableDataTable GetNodesPaged(string groupByCategory, string groupByItem, string excludedNodes)
    {
        int pageSize = 0;
        int startRowNumber = 0;

        string search = Context.Request.QueryString["search"];
        string sortColumn = Context.Request.QueryString["sort"];
        string sortDirection = Context.Request.QueryString["dir"];

        Int32.TryParse(Context.Request.QueryString["start"], out startRowNumber);
        Int32.TryParse(Context.Request.QueryString["limit"], out pageSize);
        
        List<int> excludedIds = new List<int>();
        if (!string.IsNullOrEmpty(excludedNodes))
        {
            excludedIds.AddRange(new JavaScriptSerializer().Deserialize<List<int>>(excludedNodes));
        }

        var dal = new ContainersMetadataDAL();

        if (groupByItem == UnknownValue)
        {
            groupByItem = string.Empty;
        }
        
        DataTable data = dal.GetEntitiesForGrid(
            NodesEntity,
            groupByCategory,
            groupByItem,
            sortColumn,
            sortDirection,
            startRowNumber + 1, // SWIS uses 1-based row numbering but script uses zero based
            startRowNumber + pageSize,
            search);

        long count = dal.GetEntitiesCountForGrid(NodesEntity,
            groupByCategory,
            groupByItem,
            search);

        DataTable dt = data.Clone();
        foreach (DataRow row in data.Rows)
        {
            if (!excludedIds.Contains((int)row["ID"]))
            {
                dt.ImportRow(row);
            }
        }
        PageableDataTable pg = new PageableDataTable(dt, dt.Rows.Count);
        return pg;
        
    }
    
    [WebMethod]
    public PageableDataTable GetGroupByCategories()
    {
   
        DataTable table = new DataTable();
        table.Columns.Add("Value", typeof (string));
        table.Columns.Add("Label", typeof (string));
        
        ContainersMetadataDAL dal = new ContainersMetadataDAL();

        table.Rows.Add("none", Resources.CoreWebContent.WEBCODE_AK0_70);
        
        foreach (var property in dal.GetEntityProperties("Orion.Nodes", ContainersMetadataDAL.PropertyType.GroupBy))
        {
            table.Rows.Add(property.FullyQualifiedName, UIHelper.Escape(property.DisplayName));
        }

      //  return new PageableDataTable(table, table.Rows.Count);
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;

        foreach (DataRow dr in table.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in table.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
            PageableDataTable pg = new PageableDataTable(table, table.Rows.Count);
            return pg;
        
    }
    
    [WebMethod]
    public PageableDataTable GetGroupByItems(string groupByCategory)
    {
        DataTable table = new DataTable();
        table.Columns.Add("value", typeof(string));
        table.Columns.Add("label", typeof(string));
        table.Columns.Add("type", typeof(string));
        table.Columns.Add("metadata", typeof(string));

        var dal = new ContainersMetadataDAL();
        
        if(groupByCategory != "none")
        {
            foreach (KeyValuePair<string, int> pair in dal.GetEntityGroups(NodesEntity, groupByCategory))
            {
                if (groupByCategory == "Orion.Nodes.Status")
                    table.Rows.Add(pair.Value, pair.Key, groupByCategory);  
                else if (groupByCategory == "Orion.Nodes.DynamicIP")
                    table.Rows.Add(pair.Key, pair.Key == "False" ? "False" : "True", groupByCategory);
                else
                    table.Rows.Add(pair.Key, pair.Key, groupByCategory);
            }
            
            if (groupByCategory == "Orion.Nodes.Vendor")
            {
                Dictionary<string, string> vendorsIcons = new Dictionary<string, string>();
                DataTable dt = DALHelper.ExecuteQuery("SELECT DISTINCT Vendor, VendorIcon FROM Orion.Nodes");
                if (dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        vendorsIcons[row["Vendor"].ToString()] = row["VendorIcon"].ToString();
                    }
                }
                
                foreach (DataRow row in table.Rows)
                {
                    string icon;
                    if (vendorsIcons.TryGetValue(row["value"].ToString(), out icon))
                    {
                        row["metadata"] = icon;
                    }
                    else
                    {
                        row["metadata"] = "Unknown.gif";
                    }
                }
            }
        }
       PageableDataTable pg = new PageableDataTable(table, table.Rows.Count);
        return pg;
    }
}