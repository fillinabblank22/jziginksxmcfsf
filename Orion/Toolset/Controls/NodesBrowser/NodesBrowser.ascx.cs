﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.UI;

public partial class Orion_Toolset_Controls_NodesBrowser_NodesBrowser : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool test = IsMultiSelect;
    }

    public bool IsMultiSelect { get; set; }

    public IEnumerable<Node> SelectedNodes
    {
        get
        {
            return new JavaScriptSerializer().Deserialize<List<Node>>(selectedNodes.Value);
        }
        set { selectedNodes.Value = new JavaScriptSerializer().Serialize(value); }
    }

    public IEnumerable<int> ExcludedNodesIds 
    {
        get
        {
            return new JavaScriptSerializer().Deserialize<List<int>>(excludedNodes.Value);
        }
        set { excludedNodes.Value = new JavaScriptSerializer().Serialize(value); }
    }

    public class Node
    {
        public Node()
        {
        }

        public Node(int id, string name)
            : this()
        {
            ID = id;
            Name = name;
        }

        public int ID { get; set; }
        public string Name { get; set; }
    }
}