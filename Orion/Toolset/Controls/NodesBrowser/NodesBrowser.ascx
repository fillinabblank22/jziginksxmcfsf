﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodesBrowser.ascx.cs"
    Inherits="Orion_Toolset_Controls_NodesBrowser_NodesBrowser" %>
    
<orion:Include runat="server" File="OrionCore.js"/>
<orion:Include runat="server" Module="Voip" File="RadioSelectionModel.js"/>
<orion:Include runat="server" Module="Toolset" File="SearchField.js"/>
<orion:Include runat="server" Module="Toolset" File="../Controls/NodesBrowser/NodesBrowser.js"/>

<style type="text/css">
    .x-combo-list{ width: 185px !important;}
    .x-grid3-td-checker{ text-align: center !important;}
</style>

<table class="sw-voip-nodes-browser-table">
    <tr>
        <td>
            <div id="VoIP_GroupingPanel" class="sw-voip-grouping-panel">
                <div class="GroupSection">
                    <label for="groupBySelect">
                        Group by:</label>
                    <select id="groupBySelect">
                        <option value="test" />
                    </select>
                </div>
                <div id="TestID">
                </div>
            </div>
            <div id="sw-voip-nodes-browser-nodes-grid">
            </div>
            
            <asp:HiddenField ID="selectedNodes" runat="server" />
            <asp:HiddenField ID="excludedNodes" runat="server" />
        </td>
        <td class="sw-voip-nodes-browser-selected-items-cell">
            <div class="sw-voip-nodes-browser-selected-items-header">
                <a href="javascript:void(0);" id="sw-voip-expander-link">
                    <img src="/Orion/Images/Button.Collapse.gif" alt="+" />
                    <b><span id="sw-voip-nodes-count"></span> </b></a>
            </div>
            <div id="sw-voip-nodes-browser-list">
                <ul>
                </ul>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    // <![CDATA[
SW.VoIP.NodesBrowser.SetPageSize(20);
SW.VoIP.NodesBrowser.SetUserName("<%=Context.User.Identity.Name.Trim()%>");
SW.VoIP.NodesBrowser.SelectedNodesFieldId("<%= selectedNodes.ClientID %>");
SW.VoIP.NodesBrowser.ExcludedNodesIdsFieldId("<%= excludedNodes.ClientID %>");
SW.VoIP.NodesBrowser.SetMultiSelect(<%= IsMultiSelect.ToString().ToLowerInvariant() %>);

$(function () {
    $('#sw-voip-expander-link').toggle(
            function () {
                $('img:first', this).attr('src', '/Orion/Images/Button.Expand.gif');
                $('#sw-voip-nodes-browser-list').slideUp('fast');
            },
            function () {
                $('img:first', this).attr('src', '/Orion/Images/Button.Collapse.gif');
                $('#sw-voip-nodes-browser-list').slideDown('fast');
            }
        );
});
    // ]]>
</script>
