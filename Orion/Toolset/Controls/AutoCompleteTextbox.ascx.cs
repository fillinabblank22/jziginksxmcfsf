﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.MobileControls;
using SolarWinds.Orion.Core.Common;
using System.Text;

public partial class Orion_Toolset_Controls_AutoCompleteTextbox : System.Web.UI.UserControl
{
    public string getAutoCompleteText
    {
        get { return AutoCompleteTextbox.Text; }
        set { AutoCompleteTextbox.Text = value; }
    }

    public string ControlType { get; set; }
    public string ControlId { get; set; }

    protected override void OnInit(EventArgs e)
    {
        AutoCompleteTextbox.Attributes.Add("ControlType", ControlType);
        AutoCompleteTextbox.Attributes.Add("ControlId", ControlId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string FormatString(List<string> history)
    {
        return string.Join(",", history.Select(a => string.Format("'{0}'", FixString(a))));
    }

    protected string FixString(string value)
    {
        return value.Replace("\\", "\\\\").Replace("'", "\\'");
    }
}