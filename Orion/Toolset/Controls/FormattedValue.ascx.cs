﻿using System;
using SolarWinds.Toolset.Web.Controls.ResourceTable;

public partial class Orion_Toolset_Controls_FormattedValue : System.Web.UI.UserControl
{
    protected string FormattedValue
    {
        get
        {
            if (ValueFormatter != null)
                return ValueFormatter.Format(Value);

            if (!string.IsNullOrEmpty(FormatString))
                return string.Format(FormatString, Value);

            return Value.ToString();

        }
    }

    public IValueFormatter ValueFormatter { get; set; }

    public string FormatString
    {
        get;
        set;
    }

    public int Status
    {
        get;
        set;
    }

    public object Value { get; set; }
}