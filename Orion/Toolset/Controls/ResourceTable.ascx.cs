﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Web.Controls.ResourceTable;
using System.Data;

public partial class Orion_Toolset_Controls_ResourceTable : System.Web.UI.UserControl
{
    private DataTable _data;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Columns == null)
            //TODO: some error
            return;

        if (_data == null)
            //TODO: some error
            return;

        headerRepeater.DataSource = Columns;
        headerRepeater.DataBind();

        List<ResourceTableRow> tableRows = new List<ResourceTableRow>(_data.Rows.Count);

        foreach (DataRow dataRow in _data.Rows)
        {
            List<ResourceTableCell> cells = new List<ResourceTableCell>(Columns.Count());
            foreach (ResourceTableColumn column in Columns)
            {
                ResourceTableCell cell = new ResourceTableCell();
                cell.Column = column;
                cell.Value = dataRow[column.DataItemKey];
                cells.Add(cell);
            }

            ResourceTableRow row = new ResourceTableRow { Cells = cells };
            tableRows.Add(row);
        }

        resourceTable.DataSource = tableRows;
        resourceTable.DataBind();
    }

    protected void resourceTable_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        Repeater rowRepeater = (Repeater)item.FindControl("rowRepeater");
        rowRepeater.DataSource = item.DataItem;
        rowRepeater.DataBind();
    }

    public IEnumerable<ResourceTableColumn> Columns
    {
        get;
        set;
    }

    public void SetData(DataTable data)
    {
        if (data == null)
            throw new ArgumentNullException("data");

        _data = data;
    }
}