﻿using System;
using System.Collections.Generic;

public delegate void NetObjectChangedHandler();
public partial class Orion_Controls_NetObjectsGrid : System.Web.UI.UserControl
{
    public event NetObjectChangedHandler ObjectChanged;
    private string netObjectType = "Orion.Nodes";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
    }

    public string NetObjectType
    {
        get
        {
            return netObjectType;
        }
        set
        {
            netObjectType = value;
        }
    }

    public string SelectedObject
    {
        get
        {
            return selObjectId.Value;
        }
        set
        {
            selObjectId.Value = value;
        }
    }

    public bool IsParentDependency
    {
        get
        {
            return isParentDepenedency.Value.Equals("true", StringComparison.OrdinalIgnoreCase);
        }
        set
        {
            isParentDepenedency.Value = value ? "true" : "false"; ;
        }
    }

    public string EntityNameValue
    {
        get { return entityName.Value; }
        set { entityName.Value = value; }
    }

    public Dictionary<string, string> GetGridData()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        string groupingGridValue = this.groupingGridValue.Value;
        string groupingCellValue = this.groupingCellValue.Value;
        string groupingSelectedRow = this.groupingSelectedRow.Value;

        if (!string.IsNullOrEmpty(groupingGridValue) && !string.IsNullOrEmpty(groupingCellValue))
        {
            data.Add(this.groupingGridValue.ID, groupingGridValue);
            data.Add(this.groupingCellValue.ID, groupingCellValue);
            data.Add(this.groupingSelectedRow.ID, groupingSelectedRow);
        }

        return data;
    }

    public void SetGridData(Dictionary<string, string> data)
    {
        foreach (var item in data)
        {
            if (item.Key == groupingGridValue.ID)
            {
                this.groupingGridValue.Value = item.Value;
            }
            else if (item.Key == groupingCellValue.ID)
            {
                this.groupingCellValue.Value = item.Value;
            }
            else if (item.Key == groupingSelectedRow.ID)
            {
                this.groupingSelectedRow.Value = item.Value;
            }
        }
    }

    protected void ValueChanged(object sender, EventArgs e)
    {
        ObjectChanged();
    }
}
