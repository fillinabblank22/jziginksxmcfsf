﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolWithDescriptionCellContent.ascx.cs" Inherits="Orion_Toolset_Controls_ToolWithDescriptionCellContent" %>

<a id="ToolLink" runat="server">
    <span class="ToolName"><%= ToolName %></span>
</a>
<br/>
<span class="ToolDescription"><%= ToolDescription %></span>