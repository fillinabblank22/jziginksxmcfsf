﻿using System;
using System.Linq;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;

public partial class Orion_Toolset_Controls_DiscoveryCentralPlugin : System.Web.UI.UserControl
{
    private static Log _log= new Log();

    protected int SeatsTakenCount { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SeatsTakenCount = (new LicenseSettingsDAL()).UsersCountWithWebToolsetRight();
            if (SeatsTakenCount == 0)
            {
                monitoredObjectsInfo.Attributes["class"] = "noObjectsStyle";
                bulbImage.Visible = true;
                okImage.Visible = false;
            }
        }
        catch (Exception ex)
        {
            _log.Error("Cannot get number of seats taken.", ex);
        }

        DataBind();
    }
}