﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using System.Web.Services;

public partial class Orion_Toolset_Controls_ConfigurationWizard : System.Web.UI.UserControl
{
    public int ToolId { get; set; }

    public bool IsMobileView { get; set; }

    public bool IsDemoView 
    {
        get
        {
            return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer;
        }
    }

    public int GetMaximumSelectedRealtimeMonitorItems { get; set; }
    public int CacheInvalidationInterval { get; set; }

    public string PreselectedNode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var hostname = String.IsNullOrEmpty(Request["nodehostname"]) ? Request["hostname"] : Request["nodehostname"];
        var address = String.IsNullOrEmpty(Request["ip"]) ? hostname : Request["ip"];
        if (!String.IsNullOrEmpty(address))
        {
            PreselectedNode = String.Format("{{ip:'{0}', host:'{1}', interface:'{2}', interfaceName:'{3}'}}", address, hostname, Request["ifindex"], Request["ifname"]);
        }

        switch (ToolId)
        {
            case SupportedTools.InterfaceMonitorId:
                RunButton.Text = "Monitor Interfaces";
                RunButtonM.Value = "Monitor Interfaces";
                SelectedGridTitle.InnerText = "You can only monitor the interfaces that you selected from the Orion Nodes panel.";
                SelectedItemsTitle.InnerText = "Selected Interfaces";
                break;
            case SupportedTools.CpuMonitorId:
                RunButton.Text = "Monitor CPUs";
                RunButtonM.Value = "Monitor CPUs";
                SelectedGridTitle.InnerText = "You can only monitor the selected CPUs in the panel.";
                SelectedItemsTitle.InnerText = "Selected CPU(s)";
                break;
            case SupportedTools.MemoryMonitorToolId:
                RunButton.Text = "Monitor Items";
                RunButtonM.Value = "Monitor Items";
                SelectedGridTitle.InnerText = "You can only monitor the selected memory items in this panel.";
                SelectedItemsTitle.InnerText = "Selected Memory Items";
                break;
            case SupportedTools.ResponseTimeMonitorToolId:
                RunButton.Text = "Monitor Nodes";
                RunButtonM.Value = "Monitor Nodes";
                SelectedGridTitle.InnerText = "You can only monitor the nodes that you selected from the Orion Nodes panel.";
                SelectedItemsTitle.InnerText = "Selected Nodes";
                break;
        }

        var settings = new ToolsetSettingsDAL();
        GetMaximumSelectedRealtimeMonitorItems =
            settings.GetGlobalSettingsValue<int>(
                GlobalSettingsKeys.MaximumSelectedRealtimeMonitorItemsKey,
                DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.MaximumSelectedRealtimeMonitorItemsKey]);

        CacheInvalidationInterval =
            settings.GetGlobalSettingsValue<int>(
                GlobalSettingsKeys.CacheInvalidationIntervalKey,
                DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.CacheInvalidationIntervalKey])
            *1000;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(typeof(string), "cs_autocomplete",
            @"  <script type=""text/javascript"">
                        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
                            $('.AutoComplete').unautocomplete();
                            $('.AutoComplete').autocomplete([" + this.GetIPsForAutoComplete() + @"], {max : 1000, minChars : 0});
                            if(typeof(AutoCompleteChangedEvent) != 'undefined') {
                                $('.AutoComplete').result(AutoCompleteChangedEvent);
                            }
                        });
                </script>
                ");
    }

    protected string GetIPsForAutoComplete()
    {
        var temp = new AutocompleteDAL().GetAutocompleteList(txbInputAddress.ControlType, OrionAccountHelper.GetCurrentLoggedUserName());
        return txbInputAddress.FormatString(temp);
    }

    protected void BtnPollClick(object sender, EventArgs e)
    {

    }
}
