﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageTitleBar.ascx.cs"
    Inherits="Orion_Toolset_Controls_PageTitleBar" %>
<table class="titleTable">
    <tr>
        <td>
            <div class="dateArea">
                <%if (SolarWinds.Toolset.Web.ToolsetProfile.AllowAdmin)
                  { %>
                <span><a href="/Orion/Toolset/Admin/Default.aspx" class="Toolset_SettingsIcon">Toolset
                    Settings</a> </span>
                <%}%>
                <%else if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                  { %>
                <span><a onclick="alert('Module settings access has been disabled for the online demo.  You can access all administrative features in our free 30-day eval.'); return false;"
                    href="#" class="Toolset_SettingsIcon">Toolset Settings</a> </span>
                <%}%>
                <a id="A1" href="<%$ Code: GetRequestString() %>" class="custPageLink" runat="server"
                    visible="<%$ Code: !OrionMinReqsMaster.IsMobileView && Profile.AllowCustomize%>">
                    <%= Resources.CoreWebContent.WEBDATA_TM0_241 %></a>
                <div style="padding-top: 5px; font-size: 8pt;">
                    <%=DateTime.Now.ToString("F")%>
                </div>
            </div>
            <h1>
                <%=Page.Title %></h1>
        </td>
    </tr>
</table>
