﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceTableCell.ascx.cs" Inherits="Orion_Toolset_Controls_ResourceTableCell" %>
<%@ Register TagPrefix="Toolset" TagName="FormattedValue" Src="~/Orion/Toolset/Controls/FormattedValue.ascx" %>

<%  if(Cell.HasControl) { %>
    <asp:PlaceHolder runat="server" ID="controlPlaceholder" />
<% } else { %>
    <Toolset:FormattedValue runat="server" ID="formattedValue" />
<% } %> 