﻿using System;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Toolset_Controls_PageTitleBar : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string GetRequestString()
    {
        return
            "/Orion/Admin/CustomizeView.aspx?ViewID=" + ((OrionView)Page).ViewInfo.ViewID + (!string.IsNullOrEmpty(Request["NetObject"]) ? string.Format("&NetObject={0}", Request["NetObject"]) : "") + "&ReturnTo=" + ((OrionView)Page).ReturnUrl;
    }
}