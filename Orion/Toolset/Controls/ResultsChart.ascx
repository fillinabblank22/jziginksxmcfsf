﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResultsChart.ascx.cs"
    Inherits="Orion_Toolset_Controls_ResultsChart" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<script type="text/javascript" src="../../../Charts/js/highstock.src.js"></script>
 <%if (this.IsMobileView){%>
    <link rel="stylesheet" type="text/css" href="../../Styles/ToolsetResultChart.mobile.css" />
<%}else{%>
    <link rel="stylesheet" type="text/css" href="../../Styles/ToolsetResultChart.css" />
<%}%>

<script type="text/javascript" src="../../js/ToolsetChart.js"></script>
<script type="text/javascript">
    var chartContainer = 'chartHolder';
</script>
<div id="chartcontainer" style="display: none;">
    <div id="maxSeriesMessage" >
        <div id="maxSeriesMessageContent">
            <img src='/Orion/Toolset/images/warning-icon.png' style='width: 16px' />Maximum 12 metrics can be polled simultaneously. You can change the visible metrics in the right panel.
        </div>
    </div>
    <div id="chartHolder">
    </div>
    <div id="chartMetrics">
        <div id="chartMetricsHeader">
            <label>
                Metrics ({0} / {1})</label>
            <div class="managedObjInfoWrapper">
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <img id="messageInfoImage" class="stateImage" src="../../../images/lightbulb_tip_16x16.gif" />
                        </td>
                        <td class="metricsMessage">
                            There are 3 different types of metrics available in this panel, but the chart can
                            display only 2 types simultaneously. Select the metrics (maximum 12) you want to
                            monitor. <a href='<%=HelpHelper.GetHelpUrl("ToolsetOrionMetrics")%>' target="_blank">More info...</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="chartOptionsHolder">
        </div>
        <div id="chartMetricsThresholds">
            <img src="../../images/thresholdsIcon.png"/><div>Show thresholds:</div><select id="thresholdsCombo" onchange="thresholdsComboItemChanged()">
                                                                                      <option value="none">None</option>
                                                                                  </select>
        </div>
    </div>
<%if (!this.IsMobileView){%>
    <div id ="chartDetailsTableTitle">Detailed Statistics</div>
<%}%>
    <div id="chartDetailsTable">
    </div>
</div>
<br />

<div id="selectMetricsMessage" style="display: none">
    <img id="Img1" class="stateImage" src="../../../images/lightbulb_tip_16x16.gif" /> Select metrics to monitor<br/>
     <input type="button" value="Select metrics" style="text-transform: uppercase;" onclick="openSelectMetrics();"/>
</div>
