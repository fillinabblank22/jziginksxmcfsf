﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceList.ascx.cs"
    Inherits="Orion_Toolset_Controls_ResourceTable" %>
<%@ Import Namespace="SolarWinds.Toolset.Web.Controls.ResourceTable" %>
<%@ Register TagPrefix="Toolset" TagName="CellContent" Src="~/Orion/Toolset/Controls/ResourceTableCell.ascx" %>

<table class="DataList" cellspacing="0">
<asp:Repeater runat="server" ID="resourceTable" OnItemDataBound="resourceTable_ItemDataBound">
    <ItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="ListStyle">
                        <Toolset:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="ListStyle">
                        <Toolset:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </AlternatingItemTemplate>
</asp:Repeater>
</table>
