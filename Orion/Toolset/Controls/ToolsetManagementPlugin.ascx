﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ToolsetManagementPlugin.ascx.cs"
    Inherits="Orion_Toolset_Controls_ToolsetManagementPlugin" %>
    
    <%if (WebToolsetEnabledForUser)
      {%>
<link rel="stylesheet" href="/Orion/Toolset/Styles/ToolsetManagementPlugin.css">

<script type="text/javascript">

    $(document).ready(function () {

        var waitUntilToolsetSelectorsLoaded = function (selector, callback) {
            if (jQuery(selector).length) {
                callback();
            } else {
                setTimeout(function () {
                    waitUntilToolsetSelectorsLoaded(selector, callback);
                }, 100);
            }
        };

        var fillTooltipData = function(obj) {
            $($("#toolTipTitle")[0]).text(obj.lastChild.textContent);
            $($("#toolsetTooltipDescription")[0]).text($(obj).attr("description"));
            $($("#toolTipContent")[0]).find("img").attr("src", $(obj.firstChild).attr("src"));
            var lines = $(obj).attr("details").split("\n");
            var ul = $($("#toolsetTooltipDetailedDescription")[0]);
            $(ul).empty();
            for (var i = 0; i < lines.length; i++) {
                $(ul).append('<li>' + lines[i] + '</li>');
            }
        }

        var bindToolTipEvents = function () {

            $(".NodeManagementIcons .toolsetButton").bind("mouseover", function () {

                var position;
                if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                    position = $(this).offset();
                } else {
                    position = $(this).position();
                }
                $("#toolTipContent").css({
                    top: position.top + 26,
                    left: position.left,
                    display: "block"
                });

                fillTooltipData(this);

                $("#toolTipContent").delay(1000).animate({
                    opacity: 1.0
                }, 0);

                $(".NodeManagementIcons .toolsetButton").bind("mouseout", function () {
                    $("#toolTipContent").css({
                        display: "none"
                    });
                });

            });

            $(".NodeManagementIcons .toolsetButton").unbind("mouseover", function () {
                console.log('Unbound');
            });
        }

        waitUntilToolsetSelectorsLoaded(".NodeManagementIcons .toolsetButton", bindToolTipEvents);
    })

</script>

<div id="toolTipContent">
    <table width="100%" cellpadding="0px" cellspacing="0px" style="">
        <tr>
            <td class="toolsetTooltiptHeaderColor">
                <img alt="" src="" width="16px" height="16px" />
                <span id="toolTipTitle"></span><span class="toolsetTooltipClick">
                    <%= Resources.ToolsetWebContent.WebData_MD0_40 %></span>
            </td>
        </tr>
        <tr>
            <td style="background: #fff; padding: 10px;">
                <span id="toolsetTooltipDescription" style="font-style: italic; font-size: x-small">sometext data</span><br />
                <ul id="toolsetTooltipDetailedDescription" style="font-size: x-small">
                    <li>item1</li>
                </ul>
            </td>
        </tr>
    </table>
</div>

<% } %>