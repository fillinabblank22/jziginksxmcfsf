﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigurationWizard.ascx.cs"
    Inherits="Orion_Toolset_Controls_ConfigurationWizard" %>
<%@ Register TagPrefix="uc" TagName="AutoComplete" Src="~/Orion/Toolset/Controls/AutoCompleteTextbox.ascx" %>
<orion:Include ID="Include2" runat="server" ClientIDMode="Static" File="/Toolset/js/ConfigurationWizard.js" />
<% if (IsMobileView)
   {%>
<link href="../../Styles/ConfigurationWizard.mobile.css" rel="stylesheet" type="text/css" />
<% }
   else
   { %>
<link href="../../Styles/ConfigurationWizard.css" rel="stylesheet" type="text/css" />
<% } %>
<script type="text/javascript">
    $(document).on("keypress", 'form', function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
    function IsDemoView(){
        return <%= IsDemoView.ToString().ToLowerInvariant() %>;
    }

    function GetToolID() {
        return <%= ToolId %>;
    }

    function GetPresectedNode() { 
        return <%= PreselectedNode %>;
    }

    function GetTestCredentilasButtonID(){
        return '<%=testCredBtn.ClientID%>';
    }

    function GetAddDeviceButtonID(){
        return '<%=addBtn.ClientID%>';
    }

    function GetAddDeviceKeepButtonID(){
        return '<%=addKeepBtn.ClientID%>';
    }

    function GetIPDialogTextBoxID(){
        return '<%=txbInputAddress.ClientID%>_AutoCompleteTextbox';
    }

    function GetIPDialogValue(){
        var value = $('#' + GetIPDialogTextBoxID()).val().trim(); 
        return value;
    }

    function ShowCW(){
        $("#CW").show();
        getDefaultDataView();
        
        <% if (IsMobileView)
            {%>;
        if($("#nodePanelContent")) {
            $("#availableNodesDiv").appendTo("#nodePanelContent");
            $("#availableNodesMobile").appendTo("#nodePanelContent");
            $("#selectButton").appendTo("#nodePanelContent");
            $("#Div1").css("height", $("#Div1").children(0).css("height"));
            $("#Div1").css("width", $("#Div1").children(0).css("width"));
        }
        
        if (availableNodesPanel) {
            availableNodesPanel.expand();
        }

        if($("#selectedItemsContent")) {
            $("#selectedItemsDiv").appendTo("#selectedItemsContent");
            $("#Div2").css("height", $("#Div2").children(0).css("height"));
            $("#Div2").css("width", $("#Div2").children(0).css("width"));
        }
        
        if(selectedItemsPanel) {
            selectedItemsPanel.expand();
        }

        if ($("#selectedItemsBottomDiv")) {
            $("#selectedItemsBottomDiv").css("width", $(document).width() - 114);
        }

        $("#testCredDiv").appendTo("#credsDiv > div:nth-child(1)");
        
        var toolID = GetToolID();
        if (toolID != 5) {
        $($(".cwMenuItem")[1]).append(GetDiscoveryText());    
        } else {
            $("#commandsTable tr:nth-child(2)").css("display", "none");
        }
        
        <% } %>;
    }
 
    function IsMobileViewMode(){
        return <%= this.IsMobileView ? "true" : "false" %>;
    }
    
    function GetMaximumSelectedRealtimeMonitorItems() {
        return <%= this.GetMaximumSelectedRealtimeMonitorItems %>;
    }

    function GetCacheInvalidationInterval() {
        return <%= this.CacheInvalidationInterval %>;
    }

</script>
<asp:ScriptManagerProxy ID="ScriptManagerProxy" runat="server" />
<input type="hidden" value="" id="lastUpdate"/>
<div id="CW">
    <% if (!IsMobileView)
       {%>
    <%--display desktop grid--%>
    <div id="GroupedTreePlace">
    </div>
    <div id="Buttons">
        <a href="javascript:void(0);" class="next nav-btn" onclick="MoveRight()"></a><a href="javascript:void(0);"
            class="prev nav-btn" onclick="MoveLeft()"></a>
    </div>
    <div class="rigthContainer">
        <div id="GridSelectedPlace">
        </div>
        <div id="Footer">
            <div style="float: right; width: 100%">
                <div style="margin: 0 auto; display: table;">
                    <div id="PollingEngines">
                        <table>
                            <tr class="x42-form-item-input-row">
                                <td width="105" class="x42-field-label-cell">
                                    <label style="width: 100px; margin-right: 5px; font-size: 12px;">
                                        View Type<span role="separator">:</span></label>
                                </td>
                                <td>
                                    <select id="viewTypeSelect" style="font-family: Arial, Helvetica, Sans-Serif !important; font-size: 12px;">
                                         <option value="chart" selected="selected">Chart (Default)</option>
                                         <option value="table">Table</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="x42-form-item-input-row">
                                <td width="105" class="x42-field-label-cell">
                                    <label style="width: 100px; margin-right: 5px; font-size: 12px;">
                                        Polling Engine<span role="separator">:</span></label>
                                </td>
                                <td>
                                    <select id="pollingEnginesSelect" style="font-family: Arial, Helvetica, Sans-Serif !important; font-size: 12px;">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="float: right" id="monitoButtonsDiv">
                <orion:LocalizableButton ID="RunButton" runat="server" DisplayType="Secondary" LocalizedText="CustomText"
                    OnClientClick="return MonitorAll();" ClientIDMode="Static" />
                <orion:LocalizableButton ID="CancelButton" runat="server" DisplayType="Secondary"
                    LocalizedText="CustomText" OnClientClick="Cancel(); return false;" Text="Cancel"
                    ClientIDMode="Static">
                </orion:LocalizableButton>
            </div>
        </div>
    </div>
    <% }
       else
       { %>
    <div id="Div1" style="display: inline;">
    </div>
    <div id="Div2" style="display: inline;">
    </div>
    <div id="selectedItemsBottomDiv">
        <div>
            <label runat="server" id="SelectedItemsTitle">
                tit</label></div>
        <div>
            <label runat="server" id="SelectedGridTitle">
                You can only monitor the selected CPUs in the panel.</label></div>
        <table cellpadding="0" cellspacing="0" width="100%" id="commandsTable">
            <tr>
                <td>
                    <a class="cwMenuItem" onclick="addUnmanagedDevices();">
                        <img src="../../images/CWImages/add_16x16.png" />Add Non-Orion Device</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="cwMenuItem" onclick="DiscoverAll();">
                        <img src="../../images/CWImages/NPM_discovery_16x16.gif" /></a>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="cwMenuItem" onclick="RemoveSelected();">
                        <img src="../../images/CWImages/icon_delete.png" />Remove Selected</a>
                </td>
            </tr>
        </table>
        <div>
            <div id="GridSelectedPlace">
            </div>
        </div>
    </div>
    <div id="Footer">
        <div style="margin: 0 auto; display: table;">
                <div id="PollingEngines">
                <table>
                <tr class="x42-form-item-input-row">
                                <td width="105" class="x42-field-label-cell">
                                    <label style="width: 100px; margin-right: 5px;">
                                        View Type<span role="separator">:</span></label>
                                </td>
                                <td>
                                    <select id="viewTypeSelect">
                                         <option value="chart" selected="selected">Chart (Default)</option>
                                         <option value="table">Table</option>
                                    </select>
                                </td>
                            </tr>
                    <tr class="x42-form-item-input-row">
                        <td width="105" class="x42-field-label-cell">
                            <label style="width: 100px; margin-right: 5px;">
                                Polling Engines<span role="separator">:</span></label>
                        </td>
                        <td >
                            <select id="pollingEnginesSelect">
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="margin: 0 auto; display: table;" id="monitoButtonsDivM">
            <input type="button" runat="server" id="RunButtonM" onclick="MonitorAll();" value="Monitor CPUs" />
            <input type="button" runat="server" id="CancelButtonM" onclick="Cancel();" value="Cancel" />
        </div>
    </div>
    <div id="availableNodesDiv">
        <div id="groupSelectMobile" style="clear: both; float: left; width: 100%">
        </div>
        <div id="treeSearchMobile" style="clear: both; float: left; width: 100%">
        </div>
    </div>
    <div id="availableNodesMobile" style="clear: both; float: left; width: 100%">
    </div>
    <div id="selectButton" style="clear: both; float: left; width: 100%; text-align: center;">
        <input type="button" value="Add Selected Nodes" class="addBtns" onclick="MoveRight();"
            style="display: block;" />
    </div>
    <% } %>
    <div id="dialog" class="addDeviceDialog" title="Add Non-Orion Device">
        <div class="dialog-content">
           <% if (IsDemoView)
              { %>
           <div class="isDemoMsg">
                This feature is disabled on a demo server. Please select only from devices provided for the demo.
            </div>
            <% } %>
            <div class="sw-suggestion" style="width: 94%">
                <span class="sw-suggestion-icon"></span>
                <div>
                    This device will be temporary added to this configuration.<br>
                    If you want to start to monitor a node in Orion, you can<a href="/Orion/Nodes/Add/Default.aspx?&restart=false" class="link">add the node here</a>.
                </div>
            </div>
            <div class="dialogControls">
                <div style="margin-top: 10px;" class="addDevicesContent">
                    Enter an IP Address or Hostname:
                </div>
                <div class="addDevicesContent">
                    <div class="addDeviceIP">
                        <uc:AutoComplete ID="txbInputAddress" runat="server" ControlId="WizardHost" ControlType="Host"
                            CssClass="AutoComplete" EnableViewState="false" />
                    </div>
                    <%if (!IsMobileView)
                      {%>
                    <div style="width: 64%; float: left; margin-left: 10px;">
                        <span>Press the down arrow key to get a list of recently added devices.</span></div>
                    <% } %>
                </div>
                <div style="margin-top: 35px;" class="addDevicesContent">
                    Choose Credential:
                </div>
                <div class="addDevicesContent" id="credsDiv">
                    <div style="width: 34%; float: left;">
                        <select id="credenrialsSelect" style="width: 100%;">
                        </select>
                    </div>
                    <div id="manageLinks">
                        <div style="display: inline-block; width: 100%;">
                            <div class="manageCredLink">
                                <img class="pencil" src="../../images/CWImages/edit_16x16.png" />
                                <span style="float: left; margin-left: 5px;">
                                    <asp:HyperLink CssClass="link" runat="server" NavigateUrl="~/Orion/Toolset/Admin/SnmpCredentials/Default.aspx"
                                        Target="_blank" Text="Manage Credentials" /></span> <span class="someImg" ></span>
                            </div>
                            <div class="manageCredLink">
                                <img class="refresh" src="../../images/CWImages/Refresh_icon_16x16.png" />
                                <span style="float: left; margin-left: 5px;">
                                    <asp:HyperLink CssClass="link" runat="server" NavigateUrl="javascript:void(0);" onclick="RefreshCredentilas()"
                                        Text="Refresh Credential List" />
                                </span><span class="someImg" ></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 30px;" id="testCredDiv">
                    <%if (IsMobileView)
                      {%>
                    <div>
                        <input type="button" onclick="TestCredentilas(); return false;" value="Test Credentials"
                            id="testCredBtn" />
                    </div>
                    <%  }
                      else
                      {%>
                    <div style="width: 34%; clear: both; float: left">
                        <div style="float: right; width: 100%; clear: both;">
                            <orion:LocalizableButton Style="float: right;" ID="testCredBtn" runat="server" DisplayType="Secondary"
                                LocalizedText="CustomText" OnClientClick="TestCredentilas(); return false;" Text="Test Credentials" /></div>
                    </div>
                    <% } %>
                    <div style="width: 64%; float: right;" id="testCredDivStatus">
                        <div id="credProgress" style="float: left; display: none;">
                            <image src="/Orion/Toolset/images/animated_loading_16x16.gif" class="statusIcon" />
                            Trying credential...</div>
                        <div id="credSuccess" style="float: left; display: none;">
                            <image class="delete cursor-pointer statusIcon" src="/Orion/Toolset/images/Admin/ok_enabled.png" />
                            <b>Credential test passed</b></div>
                        <div id="credFailed" style="float: left; display: none;">
                            <image class="delete cursor-pointer statusIcon" src="/Orion/Toolset/images/Admin/disable.png" />
                            <b>Credential test failed</b></div>
                    </div>
                </div>
            </div>
            <%if (!IsMobileView)
              {%>
            <div class="button-group">
                <orion:LocalizableButton ID="addBtn" runat="server" DisplayType="Primary" LocalizedText="CustomText"
                    Text="Add Device" OnClientClick="AddDeviceAndClose(); return false;" />
                <orion:LocalizableButton ID="addKeepBtn" runat="server" DisplayType="Secondary" LocalizedText="CustomText"
                    Text="Add And Keep Window Open" OnClientClick="AddDevice(); return false;" />
                <orion:LocalizableButton ID="cancelBtn" runat="server" DisplayType="Secondary" LocalizedText="CustomText"
                    Text="Cancel" OnClientClick="CloseCredentilasDialog(); return false;" />
            </div>
            <% }
              else
              {%>
            <div style="width: 5%;">
                &nbsp;</div>
            <div class="button-group">
                <input type="button" value="Add Device" id="addDeviceBtn" class="addBtns" onclick="AddDeviceAndClose(); return false;"
                    style="display: block;" />
                <input type="button" value="Add And Keep Window Open" id="addDeviceNotCloseBtn" class="addBtns"
                    onclick="AddDevice(); return false;" />
                <input type="button" value="Cancel" id="Button1" class="addBtns" onclick="CloseCredentilasDialog(); return false;" />
            </div>
            <% } %>
        </div>
    </div>
    <div id="speedDialog" class="addDeviceDialog" title="Modify Interface Properties">
        <div class="dialog-content">
            <div class="dialogControls">
                 <% if (IsMobileView)
                    { %>
                   <div width="100%" id="speedDialogTitle">
                       <div style="float: left">Modify Interface Properties</div>
                       <div style="float: right">
                           <asp:HyperLink ID="speedDialogBack" CssClass="link" runat="server" OnClick="CloseSpeedDialog();"
                                        Text="Back to previous page" ClientIDMode="Static"/>
                           
                       </div>
                   </div>
                   <% } %>
                <div>
                    <div style="float: left;" id="speedDialogHeader">
                        <b>
                            <label id="interfaceName">
                            </label>
                        </b>
                    </div>
                    <div style="float: left;">
                        <label class="reportedMsg">
                            Reported Speed:
                        </label>
                        <label class="interfaceReportedSpeed reportedMsg">
                        </label>
                    </div>
                </div>
                <div class="imparamDiv">
                    Receive Bandwidth:</div>
                <div>
                    <div style="" class="imparam">
                     <% if (IsDemoView)
                          { %>
                             <input id="iterfaceRxSpeed" type="text" class="param" disabled />
                          <% } %>
                          <% else
                          { %>
                              <input id="iterfaceRxSpeed" type="text" class="param" />
                          <% } %>
                        <label class="interfaceSpeedSpeedMeasure" style="padding: 0 10px;">
                        </label>
                    </div>
                    <div>
                        <div>
                            <label>
                                Max value is
                            </label>
                            <label class="maxSpeed">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="imparamDiv">
                    Transmit Bandwidth:</div>
                <div>
                    <div style="" class="imparam">
                          <% if (IsDemoView)
                          { %>
                            <input id="iterfaceTxSpeed" type="text" class="param" disabled/>
                          <% } %>
                          <% else
                          { %>
                             <input id="iterfaceTxSpeed" type="text" class="param"/>
                          <% } %>
                          <label class="interfaceSpeedSpeedMeasure" style="padding: 0 10px;">
                        </label>
                    </div>
                    <div>
                        <div>
                            <label>
                                Max value is
                            </label>
                            <label class="maxSpeed">
                            </label>
                        </div>
                    </div>
                </div>
                <div id="errorTooltip">
                </div>
            </div>
            <input type="hidden" id="interfaceGridID" />
            <input type="hidden" id="interfaceRowIndex" />
            <input type="hidden" id="interfaceDiscoveredSpeed" />
            <input type="hidden" id="muliplyCoef" value="1" />
            <div class="button-group">
                <% if (IsMobileView)
                   { %>
            <div >
            <input type="button" id="saveSpeedM" onclick="SaveAndCloseSpeedDialog(); return false;" value="Save Changes" />
            <input type="button" id="cancelSpeedM" onclick="CloseSpeedDialog();" value="Cancel" />                   
                       </div>
                  <%  } 
                   else
                   { %>
                     <% if (IsDemoView)
                          { %>
                            <orion:LocalizableButton ID="LocalizableButton1" runat="server" Enabled="false" DisplayType="Primary" LocalizedText="CustomText"
                    Text="Save Changes" />
                          <% } %>
                          <% else
                          { %>
                              <orion:LocalizableButton ID="saveSpeed" runat="server" DisplayType="Primary" LocalizedText="CustomText"
                    Text="Save Changes" OnClientClick="SaveAndCloseSpeedDialog(); return false;" />
                          <% } %>
               
                <orion:LocalizableButton ID="cancelSpeed" runat="server" DisplayType="Secondary"
                    LocalizedText="CustomText" Text="Cancel" OnClientClick="CloseSpeedDialog(); return false;" />   

                <%    } %>
                
            </div>
        </div>
    </div>
</div>

<% if(IsMobileView) {%>
<div style="float:left; clear:both; width:95%; display: inline-block !important;" id="seardiv">
  <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
  <td style="width: 100%">
      <input id="searchText" type="text" class="searchTb" placeholder="Search node in groups"/>
  </td>
  <td style="width: 10%">
  <span id="searchBtn" type="button" class="searchCls">&nbsp;</span>
  </td>
  </tr>
  </table>
  </div>
<% } %>