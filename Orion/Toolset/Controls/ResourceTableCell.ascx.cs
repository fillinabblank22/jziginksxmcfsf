﻿using System;
using SolarWinds.Toolset.Web.Controls.ResourceTable;

public partial class Orion_Toolset_Controls_ResourceTableCell : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cell.HasControl)
        {
            controlPlaceholder.Controls.Add(Cell.Control);
        }
        else
        {
            formattedValue.Value = Cell.Value;
            formattedValue.FormatString = Cell.Column.FormatString;
            formattedValue.ValueFormatter = Cell.Column.ValueFormatter;
        }
    }

    public ResourceTableCell Cell
    {
        get;
        set;
    }
}