﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectNodesWizardPartControl.ascx.cs" Inherits="Orion_Toolset_Controls_SelectNodesWizardPartControl" %>
<%@ Register TagPrefix="Toolset" TagName="NodesBrowser" Src="~/Orion/Toolset/Controls/NodesBrowser/NodesBrowser.ascx" %>


<script type="text/javascript">
    function VoipValidateNodes(sender, args) {
        if (SW.VoIP.NodesBrowser.GetSelectedNodeIds().length == 0) {
            alert('Error in selecting nodes');
            args.IsValid = false;
            return;
        }

        args.IsValid = true;
    }
</script>

<Toolset:NodesBrowser runat="server" ID="nodesBrowser" IsMultiSelect ="True" />
<orion:LocalizableButton ID="btnNext" runat="server" DisplayType="Primary" LocalizedText="Submit" CausesValidation="True" OnClick="btnNext_Click" />
<orion:LocalizableButton ID="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="False" OnClick="btnCancel_Click" />
<asp:CustomValidator runat="server" ID="nodesValidator" ValidateEmptyText="True"></asp:CustomValidator>