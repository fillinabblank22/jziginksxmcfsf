﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceTable.ascx.cs"
    Inherits="Orion_Toolset_Controls_ResourceTable" %>
<%@ Import Namespace="SolarWinds.Toolset.Web.Controls.ResourceTable" %>
<%@ Register TagPrefix="Toolset" TagName="CellContent" Src="~/Orion/Toolset/Controls/ResourceTableCell.ascx" %>

<table class="DataGrid">
    <tr>
        <asp:Repeater runat="server" ID="headerRepeater">
            <ItemTemplate>
                <td class="ReportHeader Toolset_ReportHeader <%# ((ResourceTableColumn)Container.DataItem).CssClass %>">
                    <%# ((ResourceTableColumn)Container.DataItem).Name %>
                </td>
            </ItemTemplate>
        </asp:Repeater>
    </tr>
<asp:Repeater runat="server" ID="resourceTable" OnItemDataBound="resourceTable_ItemDataBound">
    <ItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="Property Toolset_Property <%# ((ResourceTableCell)Container.DataItem).Column.CssClass %> <%# ((ResourceTableCell)Container.DataItem).Column.HighlightClass %>">
                        <Toolset:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr>
            <asp:Repeater runat="server" ID="rowRepeater">
                <ItemTemplate>
                    <td class="Property Toolset_Property <%# ((ResourceTableCell)Container.DataItem).Column.CssClass %> <%# ((ResourceTableCell)Container.DataItem).Column.HighlightClass %>">
                        <Toolset:CellContent runat="server" Cell="<%# (ResourceTableCell)Container.DataItem %>" />
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
    </AlternatingItemTemplate>
</asp:Repeater>
</table>
