﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Toolset.Web.Controls.ResourceTable;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Controls_ToolIconCellContent : System.Web.UI.UserControl, IResourceTableControl
{
    private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

    public string ToolIconPath { get; private set; }

    public string ToolLaunchAddress { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_parameters.ContainsKey("Value"))
        {
            var value = (string) _parameters["Value"];

            var lines = value.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            ToolIconPath = UrlHelper.GetToolIconAddress(lines[0]);
            ToolLaunchAddress = string.Format(UrlHelper.GetToolDetailAddress(lines[2]) + "/{0}.aspx", lines[2]);
            ToolLink.Attributes.Add("onClick", string.Format("launchTool('{0}');", ToolLaunchAddress));
        }
    }

    #region IResourceTableControl Members

    public Dictionary<string, object> Parameters
    {
        get { return _parameters; }
    }

    public Control GetControl()
    {
        return this;
    }

    #endregion

    #region ICloneable Members

    public object Clone()
    {
        return LoadControl("~/Orion/Toolset/Controls/ToolIconCellContent.ascx");
    }

    #endregion
}