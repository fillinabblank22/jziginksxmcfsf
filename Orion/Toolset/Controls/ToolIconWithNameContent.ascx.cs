﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Controls.ResourceTable;

public partial class Orion_Toolset_Controls_ToolIconWithNameContent : System.Web.UI.UserControl, IResourceTableControl
{
    private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

    public string ToolIconPath { get; private set; }

    protected string ToolName { get; private set; }

    protected string ToolLaunchAddress { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_parameters.ContainsKey("Value"))
        {
            var value = (string)_parameters["Value"];
            if (string.IsNullOrWhiteSpace(value))
                throw new ApplicationException("Value not found.");
            var lines = value.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            ToolName = lines[1];
            ToolIconPath = UrlHelper.GetToolIconAddress(lines[0]);
            ToolLaunchAddress = string.Format(UrlHelper.GetToolDetailAddress(lines[2]) + "/{0}.aspx", lines[2]);
            ToolLink.Attributes.Add("style", "display: inline-block;cursor:pointer;padding-bottom:3px;");
            ToolLink.Attributes.Add("onClick", string.Format("launchTool('{0}');", ToolLaunchAddress));
        }
    }

    public Dictionary<string, object> Parameters
    {
        get { return _parameters; }
    }

    public Control GetControl()
    {
        return this;
    }

    public object Clone()
    {
        return LoadControl("~/Orion/Toolset/Controls/ToolIconWithNameContent.ascx");
    }


}