﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoCompleteTextbox.ascx.cs"
    Inherits="Orion_Toolset_Controls_AutoCompleteTextbox" %>
<orion:Include Framework="jQuery" FrameworkVersion="1.7.1" Section="Top"
    runat="server" />
<orion:Include runat="server" File="js/jquery/jquery.autocomplete.css" />
<orion:Include runat="server" File="jquery/jquery.autocomplete.js" />
<asp:TextBox ID="AutoCompleteTextbox" runat="server" CssClass="AutoComplete" EnableViewState="false"></asp:TextBox>
