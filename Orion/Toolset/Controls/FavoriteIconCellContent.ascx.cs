﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Web.Controls.ResourceTable;
using SolarWinds.Toolset.Web.DAL;

public partial class Orion_Toolset_Controls_FavoriteIconCellContent : System.Web.UI.UserControl, IResourceTableControl
{
    public const string FavoriteIconFilePath = @"/Orion/Toolset/images/FavoriteOn.png";

    private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();
    private int _toolId = 0;

    public virtual string IconPath
    {
        get { return FavoriteIconFilePath; }
    }

    public virtual string AlternateText
    {
        get { return "Favorite"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_parameters.ContainsKey("Value"))
        {
            _toolId = (int)_parameters["Value"];

            FavoriteIcon.ImageUrl = IconPath;
            FavoriteIcon.AlternateText = AlternateText;
        }
    }

    protected void FavoriteIcon_Click(object sender, ImageClickEventArgs e)
    {
        new ToolsDAL().RemoveFavorite(_toolId);
    }

    #region IResourceTableControl Members

    public Dictionary<string, object> Parameters
    {
        get { return _parameters; }
    }

    public Control GetControl()
    {
        return this;
    }

    #endregion

    #region ICloneable Members

    public object Clone()
    {
        return LoadControl("~/Orion/Toolset/Controls/FavoriteIconCellContent.ascx");
    }

    #endregion
}