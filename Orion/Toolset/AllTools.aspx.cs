using System;

using SolarWinds.Orion.Web.UI;

public partial class Orion_Toolset_AllTools : OrionView
{
    public override string ViewType
    {
        get
        {
            return "All Tools";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnInit(e);
    }
}
