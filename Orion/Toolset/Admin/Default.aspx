<%@ Page AutoEventWireup="true" Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master"
    Title="Toolset Settings" CodeFile="Default.aspx.cs" Inherits="Orion_Toolset_Admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" Runat="Server">
    <style type="text/css">
        #adminContent { padding: 0; margin: 0 10px; }       
        .LinkArrow + a {
            color: #336699 !important;
        }        
        .LinkArrow + a:hover {
            color: #EB9D1F !important;
        }        
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
<script type="text/javascript">
    function launchNewTab(url) {
        if (url != null && url.length != 0) {
            window.open(url);
        } else {
            alert("<%= Resources.ToolsetWebContent.WEBDATA_SL_05 %>");
        }
    }
</script>
    <table id="breadcrumb" style="margin-bottom:0px; padding-left: 10px;">
		<tr>
			<td>
				<h1><%= Resources.ToolsetWebContent.WebData_MD0_7 %></h1>
			</td>
		</tr>
	</table>
    
   	<table id="adminContent" style="padding-top: 0; padding-left: 0;" width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td class="column1of2">
                <!-- Begin Personal Settings Bucket -->
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/Toolset/images/Admin/personal_settings_icon32x32_v2.png" alt="" />
							</td>
							<td>
                                <div class="BucketHeader"><%= Resources.ToolsetWebContent.WebData_MD0_4 %></div>
								<p><%= Resources.ToolsetWebContent.WebData_MD0_5%></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Toolset/Admin/PersonalSettings/Default.aspx"><%= Resources.ToolsetWebContent.WebData_MD0_6 %></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End Personal Settings Bucket -->

                <!-- Begin SNMP Credentials Bucket -->			
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/Toolset/images/Admin/windows_credentials_icon32x32.png" alt="" />
							</td>
							<td>
                                <div class="BucketHeader"><%= Resources.ToolsetWebContent.WebData_MD0_8 %></div>
								<p><%= Resources.ToolsetWebContent.WebData_MD0_9 %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Toolset/Admin/SnmpCredentials/Default.aspx"><%= Resources.ToolsetWebContent.WebData_MD0_10 %></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End SNMP Credentials Bucket -->
            </td>
            
            <td class="column2of2">
                <!-- Begin Global Settings Bucket -->
                <% if (Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                   { %>	
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/Toolset/images/Admin/settings_icon32x32.png" alt="" />
							</td>
							<td>
                                <div class="BucketHeader"><%= Resources.ToolsetWebContent.WebData_MD0_23 %></div>
								<p><%= Resources.ToolsetWebContent.WebData_MD0_24 %></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Toolset/Admin/GlobalSettings/Default.aspx"><%= Resources.ToolsetWebContent.WebData_MD0_25 %></a></p>
							</td>
                            <td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Toolset/Admin/ConnectionProfiles/ProfileManagement.aspx"><%= Resources.ToolsetWebContent.WEBDATA_SJ_03 %></a></p>
							</td>
						</tr>
					</table>
				</div>
                <% } %>
				<!-- End Global Settings Bucket -->
                
                <!-- Begin License Summary Bucket -->		
                 <% if (Profile.AllowAdmin || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                   { %>		
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/Toolset/images/Admin/license_36x36.png" alt="" />
							</td>
							<td>
                                <div class="BucketHeader"><%= Resources.ToolsetWebContent.WebData_MD0_26 %></div>
								<p><%= Resources.ToolsetWebContent.WebData_MD0_27%></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Toolset/Admin/LicenseSettings/Default.aspx"><%= Resources.ToolsetWebContent.WebData_MD0_28 %></a></p>
							</td>
                            <td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Details/ModulesDetailsHost.aspx"><%= Resources.ToolsetWebContent.WebData_MD0_29 %></a></p>
							</td>
						</tr>
					</table>
				</div>
                 <% } %>
				<!-- End License Summary Bucket -->
                
                <!-- Begin thwack Community Bucket -->			
				<div class="ContentBucket">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="36" height="100%">
								<img class="BucketIcon" src="/Orion/Toolset/images/Admin/thwack_icon32x32_White2.png" alt="" />
							</td>
							<td>
                                <div class="BucketHeader"><%= Resources.ToolsetWebContent.WebData_MD0_11 %></div>
								<p><%= Resources.ToolsetWebContent.WebData_MD0_12%></p>
							</td>
						</tr>
					</table>
					<table class="BucketLinkContainer">
						<tr>
							<td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="#" onclick="launchNewTab('https://thwack.solarwinds.com/t5/Engineer-s-Toolset/ct-p/engineers-toolset')"><%= Resources.ToolsetWebContent.WebData_MD0_13 %></a></p>
							</td>
                              <td class="LinkColumn" width="33%">
								<p><span class="LinkArrow">&#0187;</span> <a href="#" onclick="launchNewTab('https://thwack.solarwinds.com/t5/user/userloginpage')"><%= Resources.ToolsetWebContent.WebData_MD0_53 %></a></p>
							</td>
						</tr>
					</table>
				</div>
				<!-- End thwack Community Bucket -->

            </td>
        </tr>
        </table>

</asp:Content>

