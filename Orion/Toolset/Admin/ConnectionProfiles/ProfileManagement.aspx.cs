﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.Helpers;
//using SolarWinds..Web.NCMModuleResources;

public partial class Orion_Toolset_Admin_ConnectionProfiles_ProfileManagement : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool denyAccess = !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && (!Profile.AllowNodeManagement && !(this.Page is IBypassAccessLimitation));

        if (denyAccess)
        {
            Server.Transfer("~/Orion/Error.aspx?Message=You must have node management role to access this page.");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = Resources.ToolsetWebContent.WEBDATA_SJ_02;

        //Nice idea NCM is doing common validator 
        //SWISValidator validator = new SWISValidator();
        //validator.ContentContainer = ContentContainer;
        //validator.RedirectIfValidationFailed = true;
        //ErrorContainer.Controls.Add(validator);
    }

    protected string AllowNodeManagement
    {
        get { return Profile.AllowNodeManagement.ToString().ToLowerInvariant(); }
    }
}