﻿
Ext.namespace('SW');
Ext.namespace('SW.Toolset');
Ext.QuickTips.init();

SW.Toolset.ProfileManagement = function () {

    $(window).resize(function () {
        setMainGridHeight();
    });

    function setMainGridHeight() {
        window.setTimeout(function () {
            var mainGrid = $('#gridCell');
            var maxHeight = calculateMaxGridHeight(mainGrid);
            var calculated = calculateGridHeight();
            var height = (calculated > 0) ? Math.min(calculated, maxHeight) : maxHeight;
            setHeightForGrids(height);
        }, 0);
    }

    function setHeightForGrids(height) {
        var mainGrid = Ext.getCmp('gridControl');
        mainGrid.setHeight(Math.max(height, 300));
        mainGrid.doLayout();
    }

    function calculateGridHeight() {
        if (grid.store.getCount() == 0)
            return 0;
        var defaultNumberOfRows = 20;
        var rowsHeight = Ext.fly(grid.getView().getRow(0)).getHeight() * (defaultNumberOfRows + 1);
        return grid.getHeight() - grid.getInnerHeight() + rowsHeight + 7;
    }

    function calculateMaxGridHeight(gridPanel) {
        var gridTop = gridPanel.offset().top;
        return $(window).height() - gridTop - $('#footer').height() - 75;
    }

    var refreshObjects = function (callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = {};
        grid.store.load({ callback: callback });
    };

    var createNewProfile = function (item) {
        window.location = "/Orion/Toolset/Admin/ConnectionProfiles/EditProfile.aspx";
    };

    var editProfile = function (item) {
        window.location = "/Orion/Toolset/Admin/ConnectionProfiles/EditProfile.aspx?ProfileID=" + item.data.ProfileID;
    };

    var deleteProfiles = function (items) {
        var profileIDs = $.map(items, function(item, i) {
            return item.data.ProfileID;
        });
        var waitMsg = Ext.Msg.wait('@{R=Toolset.Strings;K=WEBJS_SJ_11;E=js}');

        internalDeleteProfile(profileIDs, function (result) {
            waitMsg.hide();
            ErrorHandler(result);
            ReloadGridStore();
        });
    };

    var AssignProfileToNodes = function (item) {
        window.location = String.format("/Orion/Toolset/Admin/ConnectionProfiles/AssignToNodes.aspx?profileID={0}&profileName={1}", item.data.ProfileID, encodeURIComponent(item.data.ProfileName));
    };

    var internalDeleteProfile = function (profileIDs, onSuccess) {
        ORION.callWebService("/Orion/Toolset/Services/ProfileManagement.asmx",
        "DeleteProfiles", { profileIDs: profileIDs },
        function (result) {
            onSuccess(result);
        });
    };

    function profileNameClick(obj) {
        var profileID = obj.attr("value");
        window.location = "/Orion/Toolset/Admin/ConnectionProfiles/EditProfile.aspx?ProfileID=" + profileID;
    };

    function renderProfileName(value, meta, record) {
        return String.format('<a href="#" style="font-weight:bold;" class="profileName" value="{0}">{1}</a>', record.data.ProfileID, Ext.util.Format.htmlEncode(value));
    }

    function ErrorHandler(result) {
        if (result != null && result.Error) {
            Ext.Msg.show({
                title: result.Source,
                msg: result.Msg,
                minWidth: 500,
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        }
    }

    function ReloadGridStore() {
        var currentPageRecordCount = grid.store.getCount();
        var selectedRecordCount = grid.getSelectionModel().getCount();
        if (currentPageRecordCount > selectedRecordCount) {
            grid.store.reload();
        }
        else {
            grid.store.load();
        }
    }

    var updateToolbarButtons = function () {
        var selCount = grid.getSelectionModel().getCount();
        var map = grid.getTopToolbar().items.map;

        var needsOnlyOneSelected = (selCount != 1);

        map.EditProfile.setDisabled(needsOnlyOneSelected);
        map.DeleteProfiles.setDisabled(selCount < 1);
        map.AssignProfileBtn.setDisabled(needsOnlyOneSelected);

        var hd = Ext.fly(grid.getView().innerHd).child('div.x-grid3-hd-checker');
        if (grid.getStore().getCount() > 0 && selCount == grid.getStore().getCount()) {
            hd.addClass('x-grid3-hd-checker-on');
        } else {
            hd.removeClass('x-grid3-hd-checker-on');
        }
    };

    var selectorModel;
    var dataStore;
    var grid;

    return {
        init: function () {

            if ($("[id$='_ContentContainer']").length == 0)
                return false;

            $("#Grid").click(function (e) {

                var obj = $(e.target);

                if (obj.hasClass('profileName')) {
                    profileNameClick(obj);
                    return false;
                }
            });

            selectorModel = new Ext.grid.CheckboxSelectionModel({ singleSelect: false });
            selectorModel.on("selectionchange", updateToolbarButtons);

            dataStore = new ORION.WebServiceStore(
                                "/Orion/Toolset/Services/ProfileManagement.asmx/GetProfiles",
                                [
                                    { name: 'ProfileID', mapping: 0 },
                                    { name: 'ProfileName', mapping: 1 }
                                ],
                                "ProfileName");

            grid = new Ext.grid.GridPanel({
                id: 'gridControl',
                store: dataStore,

                columns: [selectorModel, {
                    header: 'ProfileID',
                    width: 80,
                    hidden: true,
                    hideable: false,
                    sortable: true,
                    dataIndex: 'ProfileID'
                }, {
                    header: '@{R=Toolset.Strings;K=WEBJS_SJ_10;E=js}',
                    width: 400,
                    sortable: true,
                    dataIndex: 'ProfileName',
                    renderer: renderProfileName
                }
                ],

                sm: selectorModel,
                layout: 'fit',
                autoScroll: 'true',
                loadMask: true,
                width: 425,
                height: 400,
                stripeRows: true,

                tbar: [{
                    id: 'CreateNewProfile',
                    text: '@{R=Toolset.Strings;K=WEBJS_SJ_04;E=js}',
                    tooltip: '@{R=Toolset.Strings;K=WEBJS_SJ_05;E=js}',
                    icon: '/Orion/Toolset/images/admin/icon_addsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { createNewProfile(); }
                }, '-', {
                    id: 'EditProfile',
                    text: '@{R=Toolset.Strings;K=WEBJS_SJ_15;E=js}',
                    tooltip: '@{R=Toolset.Strings;K=WEBJS_SJ_06;E=js}',
                    icon: '/Orion/Toolset/images/admin/icon_editsnippet.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () { editProfile(grid.getSelectionModel().getSelected()); }
                }, '-', {
                    id: 'DeleteProfiles',
                    text: '@{R=Toolset.Strings;K=WEBJS_SJ_16;E=js}',
                    tooltip: '@{R=Toolset.Strings;K=WEBJS_SJ_07;E=js}',
                    icon: '/Orion/Toolset/images/admin/icon_delete.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        var showDemo = SW.Toolset.IsDemoMode();
                        if (showDemo) {
                            demoAction('Toolset_ConnectionProfile_DeleteProfile', $('#DeleteProfiles'));
                        } else {
                            Ext.Msg.confirm('@{R=Toolset.Strings;K=WEBJS_SJ_08;E=js}',
                                '@{R=Toolset.Strings;K=WEBJS_SJ_09;E=js}',
                                function (btn, text) {
                                    if (btn == 'yes') {
                                        deleteProfiles(grid.getSelectionModel().getSelections());
                                    }
                                });
                        }
                    }
                }, '-', {
                    id: 'AssignProfileBtn',
                    text: '@{R=Toolset.Strings;K=WEBJS_SJ_14;E=js}',
                    tooltip: '@{R=Toolset.Strings;K=WEBJS_SJ_13;E=js}',
                    icon: '/Orion/Toolset/images/admin/assign_node.gif',
                    cls: 'x-btn-text-icon',
                    handler: function () {
                        AssignProfileToNodes(grid.getSelectionModel().getSelected());
                    }
                }
                ]
            });

            grid.render('Grid');

            updateToolbarButtons();

            $("form").submit(function () { return false; });

            refreshObjects();

            grid.store.on('load', function (store, records, options) {
                setMainGridHeight();
            });
        }
    };

}();
Ext.onReady(SW.Toolset.ProfileManagement.init, SW.Toolset.ProfileManagement);
