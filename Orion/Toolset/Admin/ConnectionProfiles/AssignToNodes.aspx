﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssignToNodes.aspx.cs" Inherits="Orion.Toolset.Admin.ConnectionProfiles.Orion_Toolset_Admin_ConnectionProfiles_AssignToNodes"
    MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master" %>
<%@ Register Src="~/Orion/Controls/IncludeExtJs.ascx" TagName="IncludeExtJs" TagPrefix="orion" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Toolset/Controls/SelectNodesWizardPartControl.ascx" TagPrefix="Toolset" TagName="SelectNodesControl" %>


<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:IncludeExtJs ID="IncludeExtJs" runat="server" Version="3.4"/>
    <orion:Include ID="Include2" runat="server" Module="Toolset" File="Admin.css" />
    <orion:Include ID="Include3" runat="server" Module="Toolset" File="Wizard.css" />
    <orion:Include ID="Include1" runat="server" Module="Toolset" File="Discovery.css" />
    <orion:Include ID="Include4" runat="server" Module="Toolset" File="Voip.css" />
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <div class="ipsla_WP_Title"><%= Title %></div>
     <Toolset:SelectNodesControl runat="server" id="SelectNodesControl1" />
</asp:Content>