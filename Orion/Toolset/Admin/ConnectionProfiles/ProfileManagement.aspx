﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master" AutoEventWireup="true" CodeFile="ProfileManagement.aspx.cs" Inherits="Orion_Toolset_Admin_ConnectionProfiles_ProfileManagement" %>

<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <!--Do we need this-->
    <!--<link rel="stylesheet" type="text/css" href="/Orion/NCM/Admin/PolicyReports/PolicyReport.css" />-->
    <link rel="stylesheet" type="text/css" href="/Orion/styles/MainLayout.css" />
    <script type="text/javascript" src="/Orion/js/OrionCore.js"></script>
    <!--Are we using this -->
    <!--<script type="text/javascript" src="/Orion/NCM/Resources/ConfigSnippets/ConfigSnippetsHelper.js"></script>-->
    <script type="text/javascript" src="ProfileManagement.js"></script>
    <script type="text/javascript">
        SW.Toolset.AllowNodeManagement = <%=Profile.AllowNodeManagement.ToString().ToLowerInvariant()%>;
        SW.Toolset.IsDemoMode = function(){
            var isDemo =<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLowerInvariant()%>;
            return isDemo;
        };
    </script>
    <style>
        #adminContent {
            padding: 0;
        }
         #adminContent td {
             padding: 0 !important;
             vertical-align: middle !important;
         }
        .toolset-page-title {
            font-family: Arial,Helvetica,Sans-Serif;
            font-size: large;
            font-weight: normal;
            margin: 0;
            padding-bottom: 10px;
            padding-left: 10px;
            padding-top: 5px;
        }
        .tab-top {
            border: 1px solid #DEDEDE;
            padding: 1em;
            background: white url('/Orion/images/tab.top.gif') repeat-x scroll left top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table id="toolset_admin_content" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="text-align: right; padding-right: 5px;">
                <div class="dateArea" style="padding-top: 10px;">
                    <orion:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNCMPHManageConnectionProfile" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;">
                <div class="toolset-page-title">
                    <%=Page.Title%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <!-- demoedit -->
                <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                  {%>
                <div id="isDemoMode" style="display: none;"></div>
                <%}%>

                <!-- / demoedit -->

                <asp:Panel Style="padding: 10px 10px 10px 10px;" ID="ContentContainer" runat="server">
                    <div id="tabPanel" class="tab-top">
                        <table class="ExistingElements" cellpadding="0" cellspacing="0" width="100%">
                            <tr valign="top" align="left">
                                <td id="gridCell">
                                    <div id="Grid"></div>
                                </td>
                            </tr>
                        </table>
                        <div id="originalQuery">
                        </div>
                        <div id="test">
                        </div>
                        <pre id="stackTrace"></pre>
                    </div>
                </asp:Panel>
                <!--NCM used this for Custom errors-->
                <div style="padding: 10px;" id="ErrorContainer" runat="server">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

