﻿using System;
using System.Web.UI;
using SolarWinds.Toolset.Common.Model;
using SolarWinds.Toolset.Web.DAL;

public partial class Orion_Toolset_Admin_ConnectionProfiles_EditProfile : System.Web.UI.Page
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        var denyAccess = !SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer && !Profile.AllowNodeManagement;
        if (denyAccess)
        {
            Server.Transfer("~/Orion/Error.aspx?Message=You must have node management role to access this page.");
        }
    }

    public int PageTitle { get; private set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadProfile();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SubmitProfile();
        Page.Response.Redirect("/Orion/Toolset/Admin/ConnectionProfiles/ProfileManagement.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("/Orion/Toolset/Admin/ConnectionProfiles/ProfileManagement.aspx");
    }

    private void LoadProfile()
    {

        var profile = LoadProfile(Page.Request.QueryString["ProfileID"]);
        txtProfileName.Text = profile.Name;
        txtUserName.PasswordText = profile.Username;
        txtPassword.PasswordText = profile.Password;
        txtSSHPort.Text = profile.SSHPort.ToString();

    }

    private ConnectionProfile LoadProfile(string profileId)
    {
        int profileID;
        ConnectionProfile profile;
        if (string.IsNullOrEmpty(profileId))
        {
            Page.Title = Resources.ToolsetWebContent.WEBDATA_JR_10;
            profile = new ConnectionProfile();
        }
        else
        {
            var profileDAL = new ConnectionProfileDAL();
            if (!int.TryParse(profileId, out profileID))
                Server.Transfer("~/Orion/Error.aspx?Message=Invalid Profile ID.");
            profile = profileDAL.Find(profileID);
            if (profile == null)
                Server.Transfer(String.Format("~/Orion/Error.aspx?Message=Profile ID {0} does not exist", Page.Request.QueryString["ProfileID"]));
            Page.Title = String.Format("Edit {0}", profile.Name);
        }
        return profile;
    }

    private void SubmitProfile()
    {
        var profile = new ConnectionProfile
        {
            Name = txtProfileName.Text,
            Username = txtUserName.PasswordText,
            Password = txtPassword.PasswordText,
            SSHPort = Convert.ToInt32(txtSSHPort.Text)
        };

        var profileDAL = new ConnectionProfileDAL();

        if (string.IsNullOrEmpty(Request.QueryString["ProfileID"]))
        {
            profileDAL.Create(profile);
        }
        else
        {
            profile.ID = Page.Request.QueryString["ProfileID"];
            profileDAL.Update(profile);
        }

    }
}