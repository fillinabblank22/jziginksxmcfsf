﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master" AutoEventWireup="true" CodeFile="EditProfile.aspx.cs"
    Inherits="Orion_Toolset_Admin_ConnectionProfiles_EditProfile" %>

<%@ Register TagPrefix="ts" Namespace="SolarWinds.Toolset.Web.Controls" Assembly="SolarWinds.Toolset.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceHolder" runat="Server">
    <link rel="stylesheet" type="text/css" href="Settings.css" />
    <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />

    <%--NCM Password Textbox fills up password with masked data(ex : ?????) , we should not let the user to edit the password box partially
        Make it empty on focusin and restore it on focusout.
        If he does any enter any value in the password box , stop the above step--%>
    <% if (!String.IsNullOrEmpty(Page.Request.QueryString["ProfileID"])){%>
        <script>
            $(function () {
                var self = $('#txtPassword'),
                    pwdEdit = false;
                self.focusin(function(evt) {
                    if (pwdEdit)
                        return;
                    <%--Store the masked password in jquery data object and empty the password box --%>
                    self.data('masked', self.val())
                        .val('');
                }).focusout(function(evt) {
                    if (pwdEdit)
                        return;
                    <%--if the password is not changed, fill the password box with the stored mask data --%>
                    if (self.val() === '')
                        self.val(self.data('masked'));
                }).keyup(function(evt) {

                    <%--if value is changed , we should not change the password on focusIn and focusout events 
                        cleanup the event listeners once the pwdbox value is edited--%>
                    if (self.val() !== self.data('masked')) {
                        pwdEdit = true;
                        self.off('focusin')
                            .off('focusout')
                            .off('keyup');
                    }
                });
            });
        </script>

    <%}%>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <style>
        .toolset-page-title {
            font-family: Arial,Helvetica,Sans-Serif;
            font-size: large;
            font-weight: normal;
            margin: 0;
            padding-bottom: 0;
            padding-left: 10px;
            padding-top: 5px;
        }

        #adminContent {
            padding-top: 0;
            padding-left: 0;
        }
    </style>
    <table id="toolset_admin_content" cellspacing="0" cellpadding="0" width="100%">
        <tbody>
            <tr>
                <td colspan="2" style="text-align: left;">
                    <div class="toolset-page-title">
                        <%=Page.Title%>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="ContentContainer" CssClass="ContentContainer" runat="server">
                        <div class="SettingContainer">
                            <div>
                                <div style="padding-top: 10px;"><%=Resources.ToolsetWebContent.WEBDATA_JR_01%></div>
                                <div style="padding-top: 5px">
                                    <asp:TextBox ID="txtProfileName" MaxLength="256" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                            ControlToValidate="txtProfileName"
                                            Display="Dynamic"
                                            ErrorMessage="<%$ Resources: ToolsetWebContent,WEBDATA_JR_05 %>"
                                            runat="server" />
                                    
                                </div>

                                <div style="padding-top: 10px;"><%=Resources.ToolsetWebContent.WEBDATA_JR_02%></div>
                                <div style="padding-top: 5px">
                                    <ts:PasswordTextBox ID="txtUserName" runat="server" TextMode="SingleLine" CssClass="SettingTextField" Width="200px" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                    ControlToValidate="txtUserName"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: ToolsetWebContent,WEBDATA_JR_08 %>"
                                    runat="server" />
                                </div>
                                

                                <div style="padding-top: 10px;"><%=Resources.ToolsetWebContent.WEBDATA_JR_03%></div>
                                <div style="padding-top: 5px">
                                    <ts:PasswordTextBox ID="txtPassword" ClientIDMode="Static" runat="server" TextMode="Password" CssClass="SettingTextField" Width="200px" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                    ControlToValidate="txtPassword"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: ToolsetWebContent,WEBDATA_JR_09 %>"
                                    runat="server" />
                                </div>

                                <div style="padding-top: 10px;"><%=Resources.ToolsetWebContent.WEBDATA_JR_04%></div>
                                <div style="padding-top: 5px">
                                    <asp:TextBox runat="server" ID="txtSSHPort" CssClass="SettingTextField" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                        ControlToValidate="txtSSHPort"
                                        Display="Dynamic"
                                        ErrorMessage="<%$ Resources: ToolsetWebContent,WEBDATA_JR_06 %>"
                                        runat="server" />
                                    <asp:RangeValidator runat="server" ID="RangeValidator1"
                                        ControlToValidate="txtSSHPort"
                                        MinimumValue="1" MaximumValue="65535"
                                        ErrorMessage="<%$ Resources: ToolsetWebContent,WEBDATA_JR_07 %>"
                                        Operator="GreaterThan" Type="Integer" Display="Dynamic" />
                                </div>
                                
                            </div>
                        </div>
                        <div style="padding-top: 20px;">
                            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit"
                                OnClick="btnSubmit_Click" DisplayType="Primary" CausesValidation="true" />
                            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel"
                                OnClick="btnCancel_Click" DisplayType="Secondary" CausesValidation="false" />
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

