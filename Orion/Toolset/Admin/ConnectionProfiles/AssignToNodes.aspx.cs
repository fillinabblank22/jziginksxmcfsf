﻿using System;
using SolarWinds.Toolset.Common.Model;
using SolarWinds.Toolset.Web.DAL;

namespace Orion.Toolset.Admin.ConnectionProfiles
{
    public partial class Orion_Toolset_Admin_ConnectionProfiles_AssignToNodes : System.Web.UI.Page
    {
        protected string Title = "Assign {0} to Nodes";
        protected void Page_Load(object sender, EventArgs e)
        {
            int profileID = Convert.ToInt32(Request.QueryString["ProfileID"]);
            var connectionProfDAL = new ConnectionProfileDAL();
            ConnectionProfile connProfile = connectionProfDAL.Find(profileID);
            Title = string.Format(Title, connProfile != null ? connProfile.Name : "");
        }
    }
}