﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Admin_ToolsetAdminPage : System.Web.UI.MasterPage
{
    private static readonly List<string> _allowedGuestPages = new List<string>
	        {
                "/Orion/Toolset/Admin/GlobalSettings/Default.aspx",
                "/Orion/Toolset/Admin/LicenseSettings/Default.aspx",
                "/Orion/Toolset/Admin/LicenseSettings/EditPermissions.aspx"
	        };

    public string IsDemo
    {
        get
        {
            return SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        MobileHelper.ApplyMobileView();

        base.OnInit(e);
        if (!Profile.AllowAdmin && !(this.Page is IBypassAccessLimitation))
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                if (_allowedGuestPages.Exists(s => String.Equals(s, Request.Path, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return;
                }
            }
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (!Profile.AllowAdmin)
        {
            this.RemoveMapItem(Resources.ToolsetWebContent.WebData_MD0_30);
        }
    }

    private void RemoveMapItem(string mapItemTitle)
    {
        string removeMapItems = "removeMapItems";
        if (!this.Page.IsClientScriptBlockRegistered(removeMapItems))
        {
            string cstext1 = "<script type=\"text/javascript\">" +
                "$(\".bc_submenulink[title='" + mapItemTitle + "']\").parent().remove();</" + "script>";
            this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), removeMapItems, cstext1);
        }
    }

    protected void OrionSiteMapPath_OnInit(object sender, EventArgs e)
    {
        if (!CommonWebHelper.IsBreadcrumbsDisabled)
        {
            string viewID = (Request["ViewID"] != null) ? Request["ViewID"].ToString() : null;
            string returnTo = (Request["ReturnTo"] != null) ? Request["ReturnTo"].ToString() : null;
            string accountID = (Request["AccountID"] != null) ? Request["AccountID"].ToString() : null;
            var renderer = new SolarWinds.Orion.NPM.Web.UI.AdminSiteMapRenderer();
            renderer.SetUpData(new KeyValuePair<string, string>("ViewID", viewID), new KeyValuePair<string, string>("ReturnTo", returnTo), new KeyValuePair<string, string>("AccountID", accountID));
            OrionSiteMapPath.SetUpRenderer(renderer);
        }
    }

}
