﻿using Resources;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class Orion_Toolset_Admin_PersonalSettings_Default : System.Web.UI.Page, IBypassAccessLimitation
{
    protected override void OnInit(EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            List<Type> controlTypes = new List<Type>(){typeof(TextBox), typeof(DropDownList)};
            new DemoHelper().ToggleControlsState(this.Controls, controlTypes, false);
            btnSaveChanges.Enabled = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Dictionary<string, string> settigns =
                (new ToolsetSettingsDAL()).GetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName());
            txtSnmpTimeOut.Text = settigns.ContainsKey(PersonalSettingsKeys.SnmpTimeOut)
                                      ? settigns[PersonalSettingsKeys.SnmpTimeOut]
                                      : DefaultPersonalSettings.DefPersonalSettings[PersonalSettingsKeys.SnmpTimeOut];
            txtSnmpRetries.Text = settigns.ContainsKey(PersonalSettingsKeys.SnmpRetries)
                                      ? settigns[PersonalSettingsKeys.SnmpRetries]
                                      : DefaultPersonalSettings.DefPersonalSettings[PersonalSettingsKeys.SnmpRetries];

            ddlDefaultResult.SelectedValue = settigns.ContainsKey(PersonalSettingsKeys.DefaultDataRepresentation)
                                                 ? settigns[PersonalSettingsKeys.DefaultDataRepresentation]
                                                 : DefaultPersonalSettings.DefPersonalSettings[PersonalSettingsKeys.DefaultDataRepresentation];
        }
    }

    protected void BtnSaveChangesClick(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return;
        }
        Dictionary<string, string> settings = new Dictionary<string, string>
            {
                { PersonalSettingsKeys.SnmpTimeOut, txtSnmpTimeOut.Text },
                { PersonalSettingsKeys.SnmpRetries, txtSnmpRetries.Text },
                { PersonalSettingsKeys.DefaultDataRepresentation, ddlDefaultResult.SelectedValue }
            };
        
        if (!(new ToolsetSettingsDAL()).SetUserPersonalSettings(OrionAccountHelper.GetCurrentLoggedUserName(), settings))
        {
            lblError.Text = ToolsetWebContent.WebData_MD0_37;
        }
        else
        {
            Response.Redirect(@"/Orion/Toolset/Admin/Default.aspx");
        }
    }
}
