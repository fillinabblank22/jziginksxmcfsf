﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master" Title="Personal Settings"
    CodeFile="Default.aspx.cs" Inherits="Orion_Toolset_Admin_PersonalSettings_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            if (!$(".highlightButton,.sw-btn-primary", "#adminContent").length) {
                $('#<%= btnSaveChanges.ClientID %>').addClass('sw-btn-primary');
            }
        });  
    </script>
    <style type="text/css">
        #adminContent
        {
            padding: 0;
            margin: 10px 10px;
        }
        #adminContent td
        {
            vertical-align: middle;
            font-size: 11px;
        }
        #sw-btn-c
        {
            margin: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table id="breadcrumb" style="margin-bottom: 0px; padding-left: 10px;">
        <tr>
            <td>
                <h1>
                    <%= Resources.ToolsetWebContent.WebData_MD0_6 %></h1>
            </td>
        </tr>
    </table>
      <form>
    <div style="padding-left:10px;">SNMP</div>      
    <table id="adminContent" style="padding-top: 0; padding-left: 10px;" width="70%" border="0"
        cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 10%;">
                <%= Resources.ToolsetWebContent.WebData_MD0_17 %>
            </td>
            <td style="width: 170px;">
                <asp:TextBox runat="server" ID="txtSnmpTimeOut" Style="text-align: right;"></asp:TextBox>
            </td>
            <td>
                <%= Resources.ToolsetWebContent.WebData_MD0_18 %>
                <asp:RangeValidator ID="RangeValidator1" runat="server" MaximumValue="10000" MinimumValue="50" ControlToValidate="txtSnmpTimeOut"
                    ErrorMessage="<%$ Resources: ToolsetWebContent, WebData_MD0_35 %>" Type="Integer" enableclientscript="True" SetFocusOnError="True" Display="Dynamic"/>
            </td>
        </tr>
        <tr>
            <td>
                <%= Resources.ToolsetWebContent.WebData_MD0_19 %>
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSnmpRetries" Style="text-align: right;"></asp:TextBox>
            </td>
            <td>
                <%= Resources.ToolsetWebContent.WebData_MD0_20 %>
                 <asp:RangeValidator ID="RangeValidator2" runat="server" MaximumValue="6" MinimumValue="2" ControlToValidate="txtSnmpRetries" 
                    ErrorMessage="<%$ Resources: ToolsetWebContent, WebData_MD0_36 %>" Type="Integer"  enableclientscript="True" SetFocusOnError="True" Display="Dynamic"/>
            </td>
        </tr>
    </table>
    <div style="padding-left:10px;">Data Presentation</div>
      <table id="adminContent" style="padding-top: 0; padding-left: 10px;" width="70%" border="0"
        cellpadding="0" cellspacing="0">
          <tr>
              <td style="width:200px">
                  Default Real-time Data Representation:
              </td>
              <td>
                  <asp:DropDownList runat="server" ID="ddlDefaultResult" style="min-width: 125px;">
                      <asp:ListItem Text="Table" Value="Table"></asp:ListItem>
                      <asp:ListItem Text="Chart" Value="Chart"></asp:ListItem>
                  </asp:DropDownList>
              </td>
          </tr>
        </table>
    <div class="sw-btn-bar" style="padding-left: 25px; height: 50px">
        <orion:LocalizableButton runat="server" ID="btnSaveChanges" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: ToolsetWebContent, WebData_MD0_21 %>" OnClick="BtnSaveChangesClick" />
        
        <orion:LocalizableButton runat="server" ID="btnDiscardChanges" PostBackUrl="~/Orion/Toolset/Admin/Default.aspx" CausesValidation="False"
            DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: ToolsetWebContent, WebData_MD0_22 %>" />
        
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    </div>
    </form>
</asp:Content>
