﻿using System;
using System.Collections.Generic;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Web.DAL;
using Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Admin_GlobalSettings_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            List<Type> controlTypes = new List<Type>() { typeof(TextBox) };
            new DemoHelper().ToggleControlsState(this.Controls, controlTypes, false);
            btnSaveChanges.Enabled = false;
        }

        if (DALHelper.IsNCMInstalled())
        {
            trncmp.Visible = true;
        }
        else
        {
            trncmp.Visible = false;
        }
        if (!Page.IsPostBack)
        {
            Dictionary<string, string> settigns =
                (new ToolsetSettingsDAL()).GetGlobalSettings();

            InactivityPeriod.Text = settigns.ContainsKey(GlobalSettingsKeys.InactivityPeriodSettingKey)
                                   ? settigns[GlobalSettingsKeys.InactivityPeriodSettingKey]
                                   : DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.InactivityPeriodSettingKey];
            ObjectCrossLinks.Checked = Convert.ToBoolean(settigns.ContainsKey(GlobalSettingsKeys.OrionObjectsCrossLinks)
                                   ? settigns[GlobalSettingsKeys.OrionObjectsCrossLinks]
                                   : DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.OrionObjectsCrossLinks]);
            ChkbxShowNCMProfile.Checked = Convert.ToBoolean(settigns.ContainsKey(GlobalSettingsKeys.ShowNCMConnectionProfileOnToolsetSSH)
                                   ? settigns[GlobalSettingsKeys.ShowNCMConnectionProfileOnToolsetSSH]
                                   : DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.ShowNCMConnectionProfileOnToolsetSSH]);
        }
    }

    protected void BtnSaveChangesClick(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return;
        }
        Dictionary<string, string> settings = new Dictionary<string, string>
            {
                {GlobalSettingsKeys.InactivityPeriodSettingKey, InactivityPeriod.Text},
                {GlobalSettingsKeys.OrionObjectsCrossLinks, ObjectCrossLinks.Checked.ToString()},
                {GlobalSettingsKeys.ShowNCMConnectionProfileOnToolsetSSH, ChkbxShowNCMProfile.Checked.ToString()}
            };

        if (!(new ToolsetSettingsDAL()).SetGlobalSettings(settings))
        {
            lblError.Text = ToolsetWebContent.WebData_MD0_37;
        }
        else
        {
            Response.Redirect(@"/Orion/Toolset/Admin/Default.aspx");
        }
    }
}