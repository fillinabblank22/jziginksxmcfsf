﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master" Title="<%$ Resources: ToolsetWebContent, WebData_MD0_30 %>"
    CodeFile="Default.aspx.cs" Inherits="Orion_Toolset_Admin_GlobalSettings_Default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <script type="text/javascript" src="..\..\js\NumberValidation.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            if (!$(".highlightButton,.sw-btn-primary", "#adminContent").length) {
                $('#<%= btnSaveChanges.ClientID %>').addClass('sw-btn-primary');
            }
        });
    </script>
    <style type="text/css">
        #adminContent
        {
            padding: 0;
            margin: 10px 10px;
        }
        #adminContent td
        {
            vertical-align: middle;
            font-size: 11px;
        }
        #sw-btn-c
        {
            margin: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table id="breadcrumb" style="margin-bottom: 0px; padding-left: 10px;">
        <tr>
            <td>
                <h1>
                    <%= Resources.ToolsetWebContent.WebData_MD0_30 %></h1>
            </td>
        </tr>
    </table>
    <table id="adminContent" style="padding-top: 0; padding-left: 0;" width="70%" border="0"
        cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 15%;">
               <%= Resources.ToolsetWebContent.WebData_MD0_71 %>
            </td>
            <td style="width: 170px;">
                <asp:TextBox runat="server" ID="InactivityPeriod"></asp:TextBox>
            </td>
            <td>
                <%= Resources.ToolsetWebContent.WebData_MD0_72 %>
                <asp:RangeValidator ID="RangeValidator2" runat="server" MaximumValue="2147483647" MinimumValue="90" ControlToValidate="InactivityPeriod" 
                    ErrorMessage="<%$ Resources: ToolsetWebContent, WEBDATA_SK_01 %>" Type="Integer"  enableclientscript="True" SetFocusOnError="True" Display="Dynamic"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="InactivityPeriod" 
                    ErrorMessage="<%$ Resources: ToolsetWebContent, WEBDATA_SK_02 %>" Type="Integer"  enableclientscript="True" SetFocusOnError="True" Display="Dynamic"/>
            </td>
        </tr>
        <tr>
          <td style="width: 15%;">
               <%= Resources.ToolsetWebContent.WebData_MD0_73 %>
            </td>  
            <td style="width: 170px;">
                <asp:CheckBox runat="server" ID="ObjectCrossLinks"/>
            </td>
            <td>
                <%= Resources.ToolsetWebContent.WebData_MD0_74 %> <a href="www.solarwinds.com/documentation/kbloader.aspx?kb=5670" target="_blank"><%= Resources.ToolsetWebContent.WebData_MD0_75 %></a>
            </td>
        </tr>
         <tr id ="trncmp" runat ="server">
          <td style="width: 15%;">
               <%= Resources.ToolsetWebContent.WebData_GK_04 %>
          </td>  
            <td style="width: 170px;">
                <asp:CheckBox runat="server" ID="ChkbxShowNCMProfile"/>
            </td>
            <td>
                <%= Resources.ToolsetWebContent.WebData_GK_05 %> 
            </td>
        </tr>
    </table>
    <div class="sw-btn-bar" style="padding-left: 25px;">
        <orion:LocalizableButton runat="server" ID="btnSaveChanges" DisplayType="Secondary"
            LocalizedText="CustomText" Text="<%$ Resources: ToolsetWebContent, WebData_MD0_21 %>" OnClick="BtnSaveChangesClick"/>
        <orion:LocalizableButton runat="server" ID="btnDiscardChanges" PostBackUrl="~/Orion/Toolset/Admin/Default.aspx"
            DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources: ToolsetWebContent, WebData_MD0_22 %>" />
        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
