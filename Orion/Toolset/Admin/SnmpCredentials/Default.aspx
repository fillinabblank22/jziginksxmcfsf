﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master"
    Title="SNMP Credentials" CodeFile="Default.aspx.cs" Inherits="Orion_Toolset_Admin_SnmpCredentials_Default" %>

<%@ Register TagPrefix="uc" TagName="AutoComplete" Src="~/Orion/Toolset/Controls/AutoCompleteTextbox.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpHint_1" Src="~/Orion/Controls/HelpHint.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="server"
    ClientIDMode="Static">
    <orion:Include ID="IncludeExtJs1" runat="server" ClientIDMode="Static" Framework="Ext"
        FrameworkVersion="3.4" />
    <orion:Include ID="Include1" runat="server" ClientIDMode="Static" File="OrionCore.js" />
    <orion:Include ID="Include2" runat="server" ClientIDMode="Static" File="/Toolset/Admin/SnmpCredentials/CredentialManager.js" />
    <link href="styles/CredentialManager.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server" ClientIDMode="Static">
    <h1 class="sw-hdr-title">Toolset Credential Management</h1>
    <table id="SnmpTable" width="800px">
        <tr>
            <td colspan="2"> 
                <div class="sw-suggestion" >
                <span class="sw-suggestion-icon"></span>
                        <%= String.Format("These credentials are only used for Engineer's Toolset on Web, and are associated with this user account only. They will not affect the other credentials saved in the Orion installation. If you don't set up any credentials here, the existing Orion credentials will be used.") %>
                    
			</div>
            </td>
        </tr>
        <tr>
            <tr>
                <th><h3>Credentials<span style="color: grey; font-size: 10px; margin-left: 8px;"> You can edit the selected credential in the Credential Details form.</span></h3></th>
               <th style="padding-left: 10px;"><h3>Credential details</h3></th> 
               </tr>
            <td id="snmpList" style="padding-top: 10px; width: 50%" >
                <div id="gridPanel">
                    <div id="Grid" />
                </div>
            </td>
            <td id="snmpDetails" style="padding: 10px 0 0 10px; width: 50%">
               <table id="_tableType" class="DetailsTables" style="border-bottom: none">
                    <tr>
                        <td colspan="2" style="padding: 5px !important">
                            <div class="sw-suggestion sw-suggestion-warn" id="insecureHTTP" style="display: none;" runat="server">
                                <span class="sw-suggestion-icon"></span>
                                <%= String.Format("The passwords you enter on this page can be saved as plain text, because you are using an insecure HTTP connection. We recommend switching to HTTPS.")%>    
                            </div>
                        </td>
                    </tr>
                   <tr>
                        <td>
                            <asp:Label ID="lblType" runat="server" ClientIDMode="Static" Text="Type:" CssClass="SizeTextBox"></asp:Label>
                            <br />
                            <asp:DropDownList ID="drpDwnType" runat="server" ClientIDMode="Static" CssClass="SizeTextBox">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="lblName" runat="server" ClientIDMode="Static" Text="Name:"></asp:Label><br />
                            <asp:TextBox ID="txtBxName" runat="server" ClientIDMode="Static" CssClass="SizeTextBox"></asp:TextBox><br />
                            <asp:Label ID="lblNameValidation" runat="server" ClientIDMode="Static"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="snmpV2" class="DetailsTables" style="border-top: none; padding:0 20px 20px 0;">
                    <tr>
                        <td>
                            <asp:Label runat="server" ClientIDMode="Static" ID="lblCommunity" Text="Community String"></asp:Label>
                            <br />
                            <span style="flex-wrap: nowrap;"  >
                            <input id="hiddenPass" name="hiddenPass" type="hidden">
                            <asp:TextBox ID="txtBxCommunity" runat="server" ClientIDMode="Static" TextMode="Password" Width="93%"></asp:TextBox>
                            <img style="padding-left: 3px; margin-left: 5px; position: absolute" src="/Orion/Toolset/images/Admin/Show_Last_Note_icon16x16.png" onclick="HideText('txtBxCommunity', this)"/><br />
                            </span>
                            <asp:Label ID="lblCommunityValidation" runat="server" ClientIDMode="Static"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="snmpV3" class="DetailsTables" style="border-top: none">
                    <tr style="border-bottom: white thin solid">
                        <td>
                            <asp:Label ID="lblUserName" runat="server" ClientIDMode="Static" Text="UserName:"></asp:Label><br />
                            <asp:TextBox ID="txtBxUserName" runat="server" ClientIDMode="Static" CssClass="SizeTextBox"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblContext" runat="server" ClientIDMode="Static" Text="Context:"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtBxContext" runat="server" ClientIDMode="Static" CssClass="SizeTextBox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="border-top: red thin solid ">
                        <td >
                            <asp:Label ID="lblAutentification" runat="server" ClientIDMode="Static" Text="Type:"></asp:Label><br />
                            <asp:DropDownList ID="drpDwnAuth" runat="server" ClientIDMode="Static" Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <div class="AuthIsNone">
                                <asp:Label ID="lblAutPass" runat="server" ClientIDMode="Static" Text="Password:"></asp:Label><br />
                                <asp:TextBox ID="txtBxAutPass" runat="server" ClientIDMode="Static" style="width:87%" TextMode="Password"></asp:TextBox>
                                <img style="padding-left: 3px;  margin-left: 5px; position: absolute" src="/Orion/Toolset/images/Admin/Show_Last_Note_icon16x16.png" onclick="HideText('txtBxAutPass', this)" /><br />
                                <asp:CheckBox ID="ChBxAuthKeyIsPass" runat="server" /><asp:Label ID="lblAutKeyIsPass"
                                    runat="server" ClientIDMode="Static" Text="Password is a key"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="AuthIsNone">
                                <asp:Label ID="lblEnc" runat="server" ClientIDMode="Static" Text="Encrypt:"></asp:Label><br />
                                <asp:DropDownList ID="drpDwnEnc" runat="server" ClientIDMode="Static" Width="100%">
                                </asp:DropDownList>
                            </div>
                        </td>
                        <td>
                            <div class="AuthIsNone EncIsNone">
                                <asp:Label ID="lbEnclPass" runat="server" ClientIDMode="Static" Text="Password:"></asp:Label><br />
                                <asp:TextBox ID="txtBxEncPass" runat="server" ClientIDMode="Static" TextMode="Password" style="width:87%"></asp:TextBox>
                                <img style="padding-left: 3px;  margin-left: 5px;position: absolute" src="/Orion/Toolset/images/Admin/Show_Last_Note_icon16x16.png" onclick="HideText('txtBxEncPass', this)"/><br />
                                <asp:CheckBox ID="ChBxEncKeyIsPass" runat="server" /><asp:Label ID="lblEncKeyIsPass"
                                    runat="server" ClientIDMode="Static" Text="Password is a key"></asp:Label>
                            </div>
                        </td>
                    </tr>
                </table>
                <table id="Test" runat="server" clientidmode="Static" class="DetailsTables" style="border-top: white thin solid; padding-bottom: 20px;">
                    <tr>
                        <td>
                            <asp:Label ID="lblSnmpTest" runat="server" ClientIDMode="Static" Text="Select an existing device or enter an IP address or hostname to test this credential against:" EnableViewState="True" CssClass="SpanPadding"></asp:Label><br />
                            <uc:AutoComplete ID="txtSnmpTest" runat="server" ControlId="CredentialsHost" ClientIDMode="Static"
                                ControlType="Host" CssClass="AutoComplete" EnableViewState="false" />
                            <orion:LocalizableButton runat="server" DisplayType="Secondary" Text="TEST CREDENTIALS"
                                ID="btnTest" OnClientClick="TestSnmp();"></orion:LocalizableButton><br />
                            <asp:Label ID="lblSnmpTestResult" runat="server" ClientIDMode="Static"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="Save_Discard" runat="server" clientidmode="Static" class="DetailsTables" style="border-top: white thin solid; padding-bottom: 20px">
                    <tr>
                        <td>
                            <orion:LocalizableButton runat="server" DisplayType="Primary" Text="SAVE CHANGES"
                                ID="btnSave" OnClientClick="SaveChanges();" />
                            <orion:LocalizableButton runat="server" DisplayType="Secondary" Text="DISCARD CHANGES"
                                ID="btnDiscard" OnClientClick="CancelChanges();" /><br />
                            <asp:Label ID="lblSaveResult" runat="server" ClientIDMode="Static"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
