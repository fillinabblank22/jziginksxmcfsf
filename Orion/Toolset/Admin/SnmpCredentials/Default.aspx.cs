﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Models.Credentials;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Admin_SnmpCredentials_Default : System.Web.UI.Page, IBypassAccessLimitation
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Request.IsSecureConnection)    
           insecureHTTP.Attributes.Add("style","display: block");

        RemoveButtonsHref();

        if (!Page.IsPostBack)
            InitialDropDownLists();
    }

    //Removing postback
    private void RemoveButtonsHref()
    {
        btnSave.Attributes["href"] = "#";
        btnDiscard.Attributes["href"] = "#";
        btnTest.Attributes["href"] = "#";
    }

    private void InitialDropDownLists()
    {
        drpDwnType.Items.Add(new ListItem("SNMPv1/2c", SNMPVersion.SNMP2c.ToString()));
        drpDwnType.Items.Add(new ListItem("SNMPv3", SNMPVersion.SNMP3.ToString()));

        drpDwnAuth.Items.Add(new ListItem(SNMPv3AuthType.None.ToString(), SNMPv3AuthType.None.ToString()));
        
        drpDwnEnc.Items.Add(new ListItem(SNMPv3PrivacyType.None.ToString(), SNMPv3PrivacyType.None.ToString()));
        drpDwnEnc.Items.Add(new ListItem(SNMPv3PrivacyType.AES128.ToString(), SNMPv3PrivacyType.AES128.ToString()));
        drpDwnEnc.Items.Add(new ListItem(SNMPv3PrivacyType.AES192.ToString(), SNMPv3PrivacyType.AES192.ToString()));
        drpDwnEnc.Items.Add(new ListItem(SNMPv3PrivacyType.AES256.ToString(), SNMPv3PrivacyType.AES256.ToString()));

        if (!SolarWinds.Common.Utility.FIPSHelper.FIPSRestrictionEnabled)
        {
            drpDwnAuth.Items.Add(new ListItem(SNMPv3AuthType.MD5.ToString(), SNMPv3AuthType.MD5.ToString()));
            drpDwnEnc.Items.Add(new ListItem(SNMPv3PrivacyType.DES56.ToString(), SNMPv3PrivacyType.DES56.ToString()));
        }

        drpDwnAuth.Items.Add(new ListItem(SNMPv3AuthType.SHA1.ToString(), SNMPv3AuthType.SHA1.ToString()));
    }

    public string GetLastIPs()
    {
        List<string> temp = new AutocompleteDAL().GetAutocompleteList("CredentialsManagmentSettings", OrionAccountHelper.GetCurrentLoggedUserName());
        return txtSnmpTest.FormatString(temp);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.ClientScript.RegisterStartupScript(typeof(string), "cs_autocomplete",
            @"  <script type=""text/javascript"">
		                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
			                $('.AutoComplete').unautocomplete();
                            $('.AutoComplete').autocomplete([" + GetLastIPs() + @"], {max : 1000, minChars : 0});
                            if(typeof(AutoCompleteChangedEvent) != 'undefined') {
                                $('.AutoComplete').result(AutoCompleteChangedEvent);
                            }
                        });
                    </script>
                ");
    }
}