﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
var saveChanges;
var testSnmp;
var cancelChanges;
var eyeIcon;

ToolsetCredentialManager = function () {
    var selectorModel;
    var dataStore;
    var grid;
    var lastSelected;
    
    function encodeHTML(htmlText) {
        
        return $('<div/>').text(htmlText).html();
    };
    
    // Column rendering functions
    function renderUserName(value, meta, record) {
        return encodeHTML(value);
    };
    
    //Get next number for default credential name
    function getNextNumber() {
        var result = 0;
        dataStore.each(function(rec) {
            var value = rec.get('Name');
            var intValue = parseInt(value.replace('Credential', ''),10);
            if (intValue > result)
                result = intValue;
        });
        return ++result;
    }

    //Check if Credential with this name already exists
    function checkIfNameExist(name) {
        var result = false;
        dataStore.each(function(rec) {
            if (name === rec.get('Name'))
                result = true;
        });
        return result;
    }

    //Checking if there are unsaved Credential before adding new one or copying it
    function checkUnsavedCredentials() {
        if (grid.getStore().getCount() == 0)
            return true;
        var lastRow = grid.getStore().getAt(grid.getStore().getCount()-1);
        if (!lastRow) {
            return false;
        }
        var id = lastRow.data["ID"];
        var cname = lastRow.data["CombineName"];
        var message = 'Would you like to save changes to ' + cname + ' ?';
        var saveResult = true;
        var result;
        if (!id) {
             result = confirm(message);
             if (result) {
                jQuery.ajaxSetup({async:false});
                 saveResult = saveChanges();
                jQuery.ajaxSetup({async:true});
             }
             if(!result)
             grid.getStore().remove(lastRow);
         }
        return saveResult;
    };

     function addDefaultCredential() {
         if (isDemo())
        return;
         if (!checkUnsavedCredentials())
             return;
         var defaulteName = 'Credential';
         var defaultNumber = getNextNumber();
         var recordType = Ext.data.Record.create(['Name', 'CombineName', 'Version', 'Community']);
         var rec = new recordType({"Name":defaulteName + defaultNumber, 'CombineName': defaulteName + defaultNumber,'Version': 'SNMP2c', 'Community': ''});
         grid.getStore().add(rec);
         grid.getSelectionModel().selectLastRow();
     }

    function copySelecetedCredential() {
        if (isDemo())
        return;
        var isNewOne = grid.getSelectionModel().getSelected().data["ID"];
        var name = Ext.getDom('txtBxName').value;
        if (!checkUnsavedCredentials())
             return;
        var rowIndex = grid.getStore().find("Name", name);
        var id = grid.getStore().getAt(rowIndex).data["ID"];
        ORION.callWebService("/Orion/Toolset/Services/CredentialManager.asmx", "CopyCredential", { id: id }, function (result) {
            grid.getStore().load({callback: function() {if(!isNewOne)saveMessage(rowIndex);}});
        });
    }
    
    function deleteSelecetedCredential() {
        if (isDemo())
        return;
        if (!confirm("Are you sure you want to remove selected credentials?"))
            return;
        var rec = grid.getSelectionModel().getSelected();
        var id;
        id = rec.data["ID"];
        if (id) {
            ORION.callWebService("/Orion/Toolset/Services/CredentialManager.asmx", "DeleteCredential", { id: id }, function(result) {
                grid.getStore().reload();
            });
        } else {
            grid.getStore().reload();
        }
    }

    //If Snmp type changed
    function onSnmpTypeChange() {
        var drop = Ext.get('drpDwnType');
        drop.on('change', function() { snmpTypeChanged(); });
    }

    function snmpTypeChanged() {
        if (Ext.get('drpDwnType').dom.selectedIndex == 0) {
            Ext.get('snmpV3').dom.style.display = 'none';
            Ext.get('snmpV2').dom.style.display = 'table';
            Ext.get('drpDwnAuth').dom.selectedIndex = 0;
            Ext.get('drpDwnEnc').dom.selectedIndex = 0;
            authTypeChanged();
            encTypeChanged();
        } else if (Ext.get('drpDwnType').dom.selectedIndex == 1) {
            Ext.get('snmpV3').dom.style.display = "table";
            Ext.get('snmpV2').dom.style.display = 'none';
        } else {
            Ext.get('snmpV3').dom.style.display = 'none';
            Ext.get('snmpV2').dom.style.display = 'table';
        }
    }
    
    //If Authentification type changed
    function onAuthTypeChange() {
        var drop = Ext.get('drpDwnAuth');
        drop.on('change', function() { authTypeChanged(); });
    }
    
    function authTypeChanged() {
          if (Ext.get('drpDwnAuth').dom.selectedIndex == 0) {
              Ext.select('.AuthIsNone').setStyle('display', 'none');
              Ext.getDom('txtBxAutPass').value = "";
              Ext.getDom('ChBxAuthKeyIsPass').checked = false;
              Ext.get('drpDwnEnc').dom.selectedIndex = 0;
              encTypeChanged();
          }
          else
          {
               Ext.select('.AuthIsNone').setStyle('display', 'block');
          }
    }
    
    function onEncTypeChange() {
        var drop = Ext.get('drpDwnEnc');
        drop.on('change', function() { encTypeChanged(); });
    }
    
    function encTypeChanged() {
          if (Ext.get('drpDwnEnc').dom.selectedIndex == 0) {
              Ext.getDom('txtBxEncPass').value = "";
              Ext.getDom('ChBxEncKeyIsPass').checked = false;
          } else {
              Ext.select('.EncIsNone').setStyle('display', 'block');
          }
    }
    function validationMessage(element, label, message) {
         Ext.getDom(label).innerHTML = '<span style="color:red"><b>' + message + '</b></span>';
            Ext.get(element).addClass('redBorder');
            setTimeout(function() { Ext.get(element).removeClass('redBorder'); }, 1000); 
    }
    //Validate if Credential name is null or Credential with this name exists
    function validateCredentialName(rec) {
        if (Ext.getDom('txtBxName').value === "") {
            validationMessage('txtBxName', 'lblNameValidation', 'Name is required');
            return true;
        }
        else if (checkIfNameExist(Ext.getDom('txtBxName').value) && Ext.getDom('txtBxName').value !== rec.data['Name']) {
                 validationMessage('txtBxName', 'lblNameValidation', 'This name already exists');
                 return true;
        }
    };
    //Validate if Community is empty
    function validateCommunity(version) {
            if(version === 'SNMP2c')
                if (Ext.getDom('txtBxCommunity').value === "") {
                     validationMessage('txtBxCommunity', 'lblCommunityValidation', 'Community is required');
                return true;
            }
    };

//Save changed Credentials params
    saveChanges = function() {
        var rec = grid.getSelectionModel().getSelected();
        var id = rec.data["ID"];
        var version = rec.data["Version"];
        var val = Ext.get('drpDwnType').dom.value;
        var name = Ext.getDom('txtBxName').value;
        if (validateCredentialName(rec) || validateCommunity(val))
            return false;
        if (!id) {
            updateOrAddDefaulteCredential("AddDefaultCredential",name,id);
         }
        else if (version === val) {
            updateOrAddDefaulteCredential("UpdateCredential",name,id);
        }
        else {
            //We can't update to another snmp type, so delete old and create new
            updateOrAddDefaulteCredential("ForceUpdateCredential",name,id);
        }
        return true;
    };

    function updateOrAddDefaulteCredential(method,name,id) {
        ORION.callWebService("/Orion/Toolset/Services/CredentialManager.asmx", method, {
                id: id,
                name: Ext.getDom('txtBxName').value,
                version: Ext.getDom('drpDwnType').value,
                community: Ext.getDom('txtBxCommunity').value,
                userName: Ext.getDom('txtBxUserName').value,
                context: Ext.getDom('txtBxContext').value,
                privacyType: Ext.getDom('drpDwnEnc').value,
                privacyPassword: Ext.getDom('txtBxEncPass').value,
                privacyKeyIsPassword: Ext.getDom('ChBxEncKeyIsPass').checked,
                authenticationType: Ext.getDom('drpDwnAuth').value,
                authenticationPassword: Ext.getDom('txtBxAutPass').value,
                authenticationKeyIsPassword: Ext.getDom('ChBxAuthKeyIsPass').checked
            }, function(result) {
                grid.getStore().load({
                    callback: function() {
                        var rowIndex = this.find("Name", name);
                        saveMessage(rowIndex);
                    }
                });
                return true;
            });
    }

    //Hide 'Changes Saved' message  
    function saveMessage(rowIndex) {
        selectorModel.selectRow(rowIndex);
        var row = Ext.fly(grid.getView().getRow(rowIndex));
        row.addClass('changesSaved');
        (function() {  
            row = Ext.fly(grid.getView().getRow(rowIndex));
            row.removeClass('changesSaved');
        }).defer(2000);
    }

    function cleanFields() {
        Ext.getDom('txtBxName').value = "";
        Ext.getDom('drpDwnType').value = "";
        Ext.getDom('txtBxCommunity').value = "";
        Ext.getDom('txtBxUserName').value = "";
        Ext.getDom('txtBxContext').value = "";
        Ext.get('drpDwnEnc').dom.selectedIndex = 0;
        Ext.getDom('txtBxEncPass').value = "";
        Ext.getDom('ChBxEncKeyIsPass').checked = false;
        Ext.get('drpDwnAuth').dom.selectedIndex = 0;
        Ext.getDom('txtBxAutPass').value = "";
        Ext.getDom('ChBxAuthKeyIsPass').checked = false;
        Ext.getDom('lblNameValidation').innerHTML = "";
        Ext.getDom('lblCommunityValidation').innerHTML = "";
        Ext.getDom('lblSnmpTestResult').innerHTML = "";
        Ext.getDom('AutoCompleteTextbox').value = "";
        setTimeout(function(){ Ext.getDom('lblSaveResult').innerHTML = "";}, 1500);
    }
    //Cancel button
    cancelChanges = function() {
        Ext.getDom('AutoCompleteTextbox').value = "";
        loadDetailsForm();
        selectorModel.selectRow(lastSelected);
    };
    //Loading Details form for selected row
    function loadDetailsForm() {
          var form = Ext.get('snmpDetails');
              var rec = grid.getSelectionModel().getSelected();
        
              Ext.getDom('txtBxName').value = rec.data["Name"];
              Ext.getDom('drpDwnType').value = rec.data["Version"];
              Ext.getDom('txtBxCommunity').value = rec.data["Community"];
              Ext.getDom('txtBxUserName').value = rec.data["UserName"] ? rec.data["UserName"] : "";
              Ext.getDom('txtBxContext').value = rec.data["Context"] ? rec.data["Context"] : "";
        
              if (rec.data["PrivacyType"] === "")
              Ext.getDom('drpDwnEnc').selectedIndex = 0;
              else
              Ext.getDom('drpDwnEnc').value = rec.data["PrivacyType"];
        
              Ext.getDom('txtBxEncPass').value = rec.data["PrivacyPassword"];
              Ext.getDom('ChBxEncKeyIsPass').checked = rec.data["PrivacyKeyIsPassword"];
        
              if (rec.data["AuthenticationType"] === "")
              Ext.getDom('drpDwnAuth').selectedIndex = 0;
              else
              Ext.getDom('drpDwnAuth').value = rec.data["AuthenticationType"];
        
              Ext.getDom('txtBxAutPass').value = rec.data["AuthenticationPassword"];
              Ext.getDom('ChBxAuthKeyIsPass').checked = rec.data["AuthenticationKeyIsPassword"];
              snmpTypeChanged();
              authTypeChanged();
    }
    //Selecting last selected row or first
    function selectLastSeceltedOrFirstRow() {
        grid.store.on('load', function() {
            if (lastSelected) {
                selectorModel.selectRow(lastSelected);
            }
            else
            selectorModel.selectFirstRow();
        });
    }

    function demoMode() {
        if (isDemo()) {
         $("#SnmpTable :input").attr("disabled", true);
         $("#btnSave").attr("disabled", true);
         $("#btnTest").attr("disabled", true); 
            
         $("#btnSave").addClass('sw-btn-disabled');
         $("#btnTest").addClass('sw-btn-disabled');
            
         $("#saveandcontinueButton").attr("disabled", true);
         Ext.getCmp('AddButton').setDisabled(true);
         Ext.getCmp('DeleteButton').setDisabled(true);
         Ext.getCmp('CopyButton').setDisabled(true);
            

        } 
    }

    //Test Snmp Credential
    testSnmp = function() {
        var element = Ext.get('AutoCompleteTextbox');
        var address = element.dom.value.trim();
        if (address === "") {
            validationMessage('AutoCompleteTextbox', 'lblSnmpTestResult', 'Please enter Hostname or IP address');
            return;
        }
        Ext.getDom('lblSnmpTestResult').innerHTML = '<div><image class="running cursor-pointer" src="/Orion/Toolset/images/animated_loading_16x16.gif"/> Trying credential...</div>';
        ORION.callWebService("/Orion/Toolset/Services/CredentialManager.asmx", "TestSnmpCredential", {
                snmpVersion: Ext.getDom('drpDwnType').value,
                ip: address,
                snmpPort: 161,
                community: Ext.getDom('txtBxCommunity').value,
                authKey: Ext.getDom('txtBxAutPass').value,
                authKeyIsPwd: Ext.getDom('ChBxAuthKeyIsPass').checked,
                authType: Ext.getDom('drpDwnAuth').value,
                privacyType: Ext.getDom('drpDwnEnc').value,
                privacyPassword: Ext.getDom('txtBxEncPass').value,
                privKeyIsPwd: Ext.getDom('ChBxEncKeyIsPass').checked,
                context: Ext.getDom('txtBxContext').value,
                username: Ext.getDom('txtBxUserName').value
            }, function(result) {
                if (result) {
                    Ext.getDom('lblSnmpTestResult').innerHTML = '<div><image class="delete cursor-pointer" src="/Orion/Toolset/images/Admin/ok_enabled.png" /><b> Credential test passed</b></div>';
                } else {
                    Ext.getDom('lblSnmpTestResult').innerHTML = '<div><image class="delete cursor-pointer" src="/Orion/Toolset/images/Admin/disable.png" /> <b>Credential test failed</b></div>';
                }
            }, function(result) {
                 validationMessage('AutoCompleteTextbox', 'lblSnmpTestResult', 'Please enter valid host name or ip address');
            }
        );
    };
    
    eyeIcon = function(element, eye) {
        var curr = Ext.get(eye);
        var prev = Ext.get(element);
            if (prev.getAttribute('type') === 'text') {
                curr.set({ src: '/Orion/Toolset/images/Admin/Show_Last_Note_icon16x16.png'});
                prev.set({ type: 'password' });
            } else {
                curr.set({ src: '/Orion/Toolset/images/Admin/Hide_Last_Note_icon16x16.png'});
                prev.set({ type: 'text' });
            }
    };

    return {
        reload: function () { grid.getStore().reload(); },
        init: function () {

    selectorModel = new Ext.grid.CheckboxSelectionModel({
    singleSelect: true,
    listeners: {
        'selectionchange': function() {
            var map = grid.getTopToolbar().items.map;
            lastSelected = this.lastActive;
            cleanFields();
            if (this.getCount() == 0) {
                map.DeleteButton.setDisabled(true);
                map.CopyButton.setDisabled(true);
                Ext.select('.DetailsTables').setStyle('display', 'none');
            } else {
                Ext.select('.DetailsTables').setStyle('display', 'table');
                if (!isDemo()) {
                    map.DeleteButton.setDisabled(false);
                    map.CopyButton.setDisabled(false);
                }
                loadDetailsForm();
            }
        }
    }
    }); 

            // define data store for credential grid - use web service (method) for load of data
            dataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/CredentialManager.asmx/GetCredentials",
                [
                    { name: 'ID', mapping: 0 },
                    { name: 'Name', mapping: 1 },
                    { name: 'Version', mapping: 2 },
                    { name: 'Community', mapping: 3 },
                    { name: 'UserName', mapping: 4 },
                    { name: 'Context', mapping: 5 },
                    { name: 'PrivacyType', mapping: 6 },
                    { name: 'PrivacyPassword', mapping: 7 },
                    { name: 'PrivacyKeyIsPassword', mapping: 8 },
                    { name: 'AuthenticationType', mapping: 9 },
                    { name: 'AuthenticationPassword', mapping: 10 },
                    { name: 'AuthenticationKeyIsPassword', mapping: 11 },
                    { name: 'CombineName', mapping: 12 }
                ],
                "");
                
            // define grid panel
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [
                     { header: 'Id', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'ID'},
                     { header: 'Community', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'Community' },
                     { header: 'UserName', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'UserName' },
                     { header: 'Context', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'Context' },
                     { header: 'PrivacyType', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'PrivacyType' },
                     { header: 'PrivacyPassword', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'PrivacyPassword' },
                     { header: 'PrivacyKeyIsPassword', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'PrivacyKeyIsPassword' },
                     { header: 'AuthenticationType', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'AuthenticationType' },
                     { header: 'AuthenticationPassword', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'AuthenticationPassword' },
                     { header: 'AuthenticationKeyIsPassword', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'AuthenticationKeyIsPassword' },
                     { header: 'RealName', width: 0, hidden: true, hideable: false, dataIndex: 'Name'},
                     { header: 'Name', dataIndex: 'CombineName', width: '400',sortable: false,  menuDisabled: true, render: renderUserName}
                ],
                sm: selectorModel,
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                    emptyText : 'There are no credentials. You can add new one.',
                    
                },
                autoScroll: true,
                width: 400,
                height: 315,
                stripeRows: true,

                tbar: [
                {
                    id: 'AddButton',
                    text: 'New',
                    iconCls: 'add',
                    handler: function () { addDefaultCredential(); }
                },'-',
                {
                    id: 'CopyButton',
                    text: 'Copy',
                    iconCls: 'copy',
                    handler: function () { copySelecetedCredential(); }
                }, '-',
                {
                    id: 'DeleteButton',
                    text: 'Delete',
                    iconCls: 'del',
                    handler: function () { deleteSelecetedCredential(); }
                }],
            });
            
            grid.render('Grid');
            selectorModel.fireEvent('selectionchange');
            grid.setWidth($('#gridPanel').width());
            grid.store.proxy.conn.jsonData = {};
            grid.store.load();
            onSnmpTypeChange();
            onAuthTypeChange();
            onEncTypeChange();
            cleanFields();
            selectLastSeceltedOrFirstRow();
            demoMode();
        }
    };
}();

function SaveChanges() {
    if (isDemo())
        return;
    saveChanges();
}

function TestSnmp() {
    if (isDemo())
        return;
    testSnmp();
}

function CancelChanges() {
    if (isDemo())
        window.location.href = "../Default.aspx";
    cancelChanges();
}

function HideText(element, eye) {
    eyeIcon(element, eye);
}
Ext.onReady(ToolsetCredentialManager.init, ToolsetCredentialManager);

