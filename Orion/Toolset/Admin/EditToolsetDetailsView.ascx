<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditToolsetDetailsView.ascx.cs"
    Inherits="Orion_Toolset_Admin_EditToolsetDetailsView" %>
<link href="/Orion/Toolset/Styles/Toolset.css" rel="stylesheet" type="text/css" />
<table>
    <tr>
        <td>
            <asp:Label ID="lbMessage" runat="server" Text="This user doesn't have Toolset enabled"></asp:Label><br />
            <asp:Label ID="Label1" runat="server" Text="If you want to enable the tools for this user, please visit"></asp:Label>
        </td>
        <td style="width: 50px;">
            &nbsp;
        </td>
        <td>
            <br />
            <a class="LicManagementLink" href="/Orion/Toolset/Admin/LicenseSettings/Default.aspx" target="_blank">
                <asp:Literal runat="server" Text="<%$ Resources: ToolsetWebContent, WebData_MD0_3%>" /></a>
        </td>
    </tr>
</table>
