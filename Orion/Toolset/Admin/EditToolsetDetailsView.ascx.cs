using System;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Toolset.Web.DAL;

public partial class Orion_Toolset_Admin_EditToolsetDetailsView : ProfilePropEditUserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        UpdateMessageLabel();
    }

    private void UpdateMessageLabel()
    {
        var settingsDal = new LicenseSettingsDAL();
        if (!settingsDal.WebToolsetEnabledForUser(Context.Request["AccountID"]))
        {
            lbMessage.Text = Resources.ToolsetWebContent.WebData_MD0_1;
            lbMessage.CssClass = "UserToolsetDisabled";
        }
        else
        {
            lbMessage.Text = Resources.ToolsetWebContent.WebData_MD0_2;
            lbMessage.CssClass = "UserToolsetEnabled";
        }
    }

    public override string PropertyValue
    {
        get
        {
            return "-2";
        }
        set
        {   
        }
    }
}
