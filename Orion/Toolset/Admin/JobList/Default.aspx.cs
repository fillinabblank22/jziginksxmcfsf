﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;

using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.TraceRoute;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Common.Demo;

public partial class Orion_Toolset_JobList : Page
{
    
    protected void Page_Init(object sender, EventArgs e)
    {
        //this.Master.Master.ImgLogoPath = "../images/AppIcons/TraceRoutelogo.png";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txbNumberOfCopies.Value = "0";
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

}
