﻿<%@ Page Title="Job List" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" MasterPageFile="~/Orion/OrionMinReqs.master"
    Inherits="Orion_Toolset_JobList"  %>


<%@ MasterType VirtualPath="~/Orion/OrionMinReqs.master" %>

<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>
<%@ Import Namespace="SolarWinds.Toolset.Web.Services" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../../js/ToolsetLogger.js"></script>
    <script type="text/javascript" src="/Orion/js/extjs/4.2.2/ext-all-sandbox-42.js"></script>
    <script type="text/javascript" src="/Orion/js/jquery.js"></script>
    
    <script type="text/javascript" src="../../js/UserSessionManagement.js"></script>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=0.25, maximum-scale=1.0, width=device-width, user-scalable=1">
    <link rel="stylesheet" type="text/css" href="../../Styles/TraceRouteStyles.css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/MonitorGrid.css" />
    <link rel="stylesheet" type="text/css" href="../../../js/extjs/4.2.2/resources/ext-theme-gray-sandbox-42-all.css" />
    <script type="text/javascript" src="../../js/ExportToPDF.js"></script>
    <script type="text/javascript" src="../../js/ExtJsGridExport.js"></script>

    <script type="text/javascript" src="../../js/JobList.js"></script>
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../js/OrionMasterPage.js/WebToolsetMenu.js"></script>
    <script src="../../js/NumberValidation.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../../Styles/DropDownButton.css" />
    <style>
        .btnSpan {
            padding: 4px;
            border: 1px solid #888;
        }
    </style>
</asp:Content>

<asp:Content ID="MyContent" ContentPlaceHolderID="BodyContent" runat="server">

    <form id="form" runat="server" style="position: relative; min-height: 100%;">
<% if (Profile.AllowAdmin)
   { %>
    <div id="jobList">
        <div id="taskButtons">
            <hr />
            <orion:LocalizableButton ID="ButtonListJobs" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Jobs" />
            <orion:LocalizableButton ID="ButtonListAllJobs" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="All Jobs (incl. deleted)" />
            <%--<input type="button" id="ButtonListJobsMobile" value="Trace" />--%>
            <orion:LocalizableButton ID="ButtonListTasks" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Tasks" />
            <%--<input type="button" id="ButtonListTasksMobile" value="Trace" />--%>
            <orion:LocalizableButton ID="ButtonListSessions" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="User Sessions" />
        </div>
        <hr />
        <div id="actionButtons">
            <orion:LocalizableButton ID="ButonSelectAll" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Select All" />
            <orion:LocalizableButton ID="ButonStopSelected" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Stop Selected" />
            <span id="taskControlButtons" class ="btnSpan">
                <orion:LocalizableButton ID="ButtonCloneSelected" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Clone Selected" />
                <label>#copies:</label><input type="text" onkeypress="return isNumber(event)" onblur="validateRange(this, 0, 500)" clientidmode="Static" runat="server" id="txbNumberOfCopies" />
            </span>
            <span id="jobControlButtons"  class ="btnSpan" style="visibility: hidden;">
                <orion:LocalizableButton ID="ButtonKillJob" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Stop Job By ID" />
                <label>Job GUID to stop:</label><input type="text" onkeypress="return false;" onblur="validateRange(this, 0, 500)" clientidmode="Static" runat="server" id="txbJobId" />
            </span>
            <hr />
        </div>
        <div class="gridElement" id="grid-example"></div>
    </div>
    <hr />
    <br />
        <% } %>
        </form>
    $(SW.Core.ThisPage.Init);
</asp:Content>

<%--<%@ Page Title="Trace Route" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Language="C#" %>

<script type="text/javascript" src="/Orion/js/jquery.js"></script>    
<script type="text/javascript" src="/Orion/js/extjs/4.2.2/ext-all-sandbox-42.js"></script>
    <script type="text/javascript" src="../../js/JobList.js"></script>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <div id="grid-example"></div>
    <div>
    
    </div>
</body>
</html>--%>
