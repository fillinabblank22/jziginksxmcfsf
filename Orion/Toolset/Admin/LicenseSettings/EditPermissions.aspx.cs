﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Web.DAL;
using Resources = SolarWinds.Orion.Core.Common.Models.Resources;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Admin_LicenseSettings_EditPermissions : System.Web.UI.Page
{
    private DataTable AccountsIds;
    readonly LicenseSettingsDAL licenseSettingsDal = new LicenseSettingsDAL();
    private List<Control> _checkboxes;
    public string HeaderTitle;
    private string LicenseManagerHomePage = "/Orion/Toolset/Admin/LicenseSettings/Default.aspx";

    protected override void OnInit(EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            List<Type> controlTypes = new List<Type>() { typeof(CheckBoxList), typeof(HtmlInputCheckBox), typeof(DropDownList) };
            new DemoHelper().ToggleControlsState(this.Controls, controlTypes, false);
            submitButton.Enabled = false;
        }
    }

    protected void Page_PreLoad(object sender, EventArgs e)
    {
        if (Request.Form["hdnValue"] != null)
        {
            reqAccountIds.Value = Request.Form["hdnValue"];
        }
        if (reqAccountIds.Value.ToLower() == "hidden")
        {
            Response.Redirect(LicenseManagerHomePage);
        }
        string[] accounts = reqAccountIds.Value.Split(',');
        AccountsIds = new DataTable();
        AccountsIds.Columns.Add("Account", typeof(string));
        DataRow row;
        foreach (string account in accounts)
        {
            row = AccountsIds.NewRow();
            row["Account"] = account;
            AccountsIds.Rows.Add(row);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadCheckBox();
            if (AccountsIds.Rows.Count == 1)
            {
                HeaderTitle = string.Format("Edit Toolset &quot;{0}&quot; User", AccountsIds.Rows[0][0].ToString());
                divAccountsList.Visible = false;
                ManageCheckboxes(false, null);
            }
            else if (AccountsIds.Rows.Count > 1)
            {
                HeaderTitle = "Edit Toolset User(s)";
                BindAccounts();
                ManageCheckboxes(true, null);
                ddlSumview.SelectedIndex = 1;
                ddlOrionIntegration.SelectedIndex = 1;
                string toggleScript = "$(function() { if(typeof(SetDefaultState)=='function') {SetDefaultState('" +
                                      checkboxesVisible.ClientID + "')}});";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script_" + System.Guid.NewGuid().ToString(),
                                                    toggleScript, true);
            }
        }
        else
        {
            HeaderTitle = "Edit Toolset User(s)";
        }
    }

    private void BindAccounts()
    {
        if (AccountsIds != null)
        {
            contentsSummaryLabel.Text = string.Format("{0} User(s)", AccountsIds.Rows.Count);
            contentsRepeater.DataSource = AccountsIds;
            contentsRepeater.DataBind();
        }
    }

    private void LoadCheckBox()
    {
        ToolsCheckboxList.Items.Clear();
        var tools = licenseSettingsDal.GetSupportedToolsNamesForUser();

        if (AccountsIds.Rows.Count == 1)
        {
            var settings = licenseSettingsDal.GetSettings(AccountsIds.Rows[0][0].ToString(), tools);
            foreach (var setting in settings)
            {
                ListItem item = new ListItem
                {
                    Text = displayText(setting.Key),
                    Value = setting.Key,
                    Selected = Convert.ToBoolean(setting.Value)
                };
                item.Attributes.Add("class", "childcheck");
                ToolsCheckboxList.Items.Add(item);
            }

            var addlSettingsKey = new List<string>() {"IsSummaryViewEnabled"};
            var addlSettings = licenseSettingsDal.GetSettings(AccountsIds.Rows[0][0].ToString(), addlSettingsKey);
            if (addlSettings.ContainsKey("IsSummaryViewEnabled"))
            {
                ddlSumview.SelectedIndex = addlSettings["IsSummaryViewEnabled"].ToLower() == "true" ? 1 : 0;
            }

            ddlOrionIntegration.SelectedIndex =
                licenseSettingsDal.IsToolsetOrionIntegrationEnabled(AccountsIds.Rows[0][0].ToString()) ? 1 : 0;
        }
        else if (AccountsIds.Rows.Count > 1)
        {
            foreach (string tool in tools)
            {
                ListItem item = new ListItem
                {
                    Text = displayText(tool),
                    Value = tool,
                    Selected = true
                };
                item.Attributes.Add("class", "childcheck");
                ToolsCheckboxList.Items.Add(item);
            }
            ddlSumview.SelectedIndex = 0;
            ddlOrionIntegration.SelectedIndex = 0;
        }
    }

    protected void ManageCheckboxes(bool visible, List<string> checkedNames)
    {
        // set value used by client type js (EditAccount.js)
        if (visible) checkboxesVisible.Value = "visible";
        else checkboxesVisible.Value = "hidden";

        // Get list of all checkboxes on this page
        _checkboxes = GetAllControls(this, typeof(HtmlInputCheckBox));

        // Set Visible for every checkbox
        foreach (HtmlInputCheckBox chbx in _checkboxes) chbx.Visible = visible;

        // Manage checked state
        if (checkedNames != null)
        {
            foreach (string name in checkedNames)
                foreach (HtmlInputCheckBox chbx in _checkboxes)
                    if (chbx.Name == name)
                        chbx.Checked = true;
        }
    }

    private List<Control> GetAllControls(Control parent, Type type)
    {
        List<Control> list = new List<Control>();
        AddControlToList(parent, type, ref list);
        return list;
    }
    private void AddControlToList(Control parent, Type type, ref List<Control> list)
    {
        foreach (Control c in parent.Controls)
        {
            if (c.GetType() == type) list.Add(c);
            else AddControlToList(c, type, ref list);
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(LicenseManagerHomePage);
    }

    protected void btnSubmit_Click(Object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            return;
        }
        var settings = new Dictionary<string, string>();
        if (!chbxAccountEnabled.Visible || chbxAccountEnabled.Checked)
        {
            settings = ToolsCheckboxList.Items.Cast<ListItem>()
                                            .ToDictionary(item => item.Value, item => item.Selected.ToString().ToLower());
        }
        if (!chbxSumview.Visible || chbxSumview.Checked)
        {
            bool issummaryview = ddlSumview.SelectedIndex > 0;
            settings.Add("IsSummaryViewEnabled", issummaryview.ToString().ToLower());

        }
        if (!chbxOrionInteg.Visible || chbxOrionInteg.Checked)
        {
            bool isorionintegration = ddlOrionIntegration.SelectedIndex != 0;
            settings.Add("IsWebToolsetOrionIntegrationEnabled", isorionintegration.ToString().ToLower());
        }
        for (var i = 0; i < AccountsIds.Rows.Count; i++)
        {
            licenseSettingsDal.SetSettings(AccountsIds.Rows[i][0].ToString(), settings);
        }
        Response.Redirect(LicenseManagerHomePage);
    }

    private string displayText(string key)
    {
        string dispText = string.Empty;
        switch (key)
        {
            case "CanRunTraceRoute":
                dispText = "Trace Route";
                break;
            case "CanRunResponseTimeMonitor":
                dispText = "Response Time Monitor";
                break;
            case "CanRunInterfaceMonitor":
                dispText = "Interface Monitor";
                break;
            case "CanRunCPUMonitor":
                dispText = "CPU Monitor";
                break;
            case "CanRunMemoryMonitor":
                dispText = "Memory Monitor";
                break;
        }
        return dispText;
    }
}