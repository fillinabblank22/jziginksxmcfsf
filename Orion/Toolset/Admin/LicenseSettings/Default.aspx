﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master"
    Title="User License Management for Toolset" CodeFile="Default.aspx.cs" Inherits="Orion_Toolset_Admin_LicenseSettings_Default" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <orion:Include ID="Include1" runat="server" File="Toolset/Admin/LicenseSettings/styles/LicenseSettings.css" />
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include runat="server" File="OrionCore.js" />
    <orion:Include runat="server" File="Toolset/Admin/LicenseSettings/js/SearchField.js" />
    <orion:Include runat="server" File="Toolset/Admin/LicenseSettings/js/LicenseManager.js" />
    <table id="breadcrumb">
        <tr>
            <td>
                <h1>
                    <%= Resources.ToolsetWebContent.WEBDATA_SL_06 %></h1>
            </td>
        </tr>
    </table>
    <div class="sw-pg-hint-inner-yellow">
        <div class="sw-pg-hint-body-yellow">
            <span class="bold">Cant see group users in the grid?</span>
            <br />
            Members of a group account appear in the grid after they logged in at least once
            <a href="<%= HelpHelper.GetHelpUrl("ToolsetOrionGroupAccounts") %>" class="links" target="_blank">» Learn More</a>
        </div>
    </div>
    <div>
        &nbsp;</div>
    <div class="sw-pg-hint-inner-red" style="display: none" id="ErrorBlock">
        <div class="sw-pg-hint-body-red">
            <span class="bold" style="color: red">The number of available seats is smaller than
                the number of users using Toolset on this<br />
                Orion installation. Disable Toolset for <label id="lblDisbleCount"></label> users, or open the <a href="<%= HelpHelper.GetHelpUrl("ToolsetOrionNeedMoreSeats") %>"
                    target="_blank" class="links">License Manager</a> to increase
                <br />
                the count of seats for this installation</span>
        </div>
    </div>
    <div>
        &nbsp;</div>
    <div>
        You have already allocated <span class="bold">
            <label id="lblToolsetRight">
            </label>
            out of the
            <label id="lblSeatCount">
            </label>
            seats </span>available for this installation <a href="<%= HelpHelper.GetHelpUrl("ToolsetOrionNeedMoreSeats") %>"
                target="_blank" class="links">» I need more seats</a>
    </div>
    <div>
        &nbsp;
    </div>
    <div id="tabPanel" class="tab-top">
        <table cellpadding="0" cellspacing="0" width="98%">
            <tr valign="top" align="left">
                <td id="gridCell" style="padding-right: 0px;">
                    <div id="LicenseManagerGrid" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
