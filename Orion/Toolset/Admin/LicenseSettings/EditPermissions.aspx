﻿<%@ Page Language="C#" MasterPageFile="~/Orion/Toolset/Admin/ToolsetAdminPage.master"
    AutoEventWireup="true" CodeFile="EditPermissions.aspx.cs" Inherits="Orion_Toolset_Admin_LicenseSettings_EditPermissions"
    Title="Edit Toolset User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="adminHeadPlaceholder" runat="Server">
    <orion:Include runat="server" File="Toolset/Admin/LicenseSettings/styles/LicenseSettings.css" />
    <orion:Include runat="server" File="Toolset/Admin/LicenseSettings/js/EditPermission.js" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="adminContentPlaceholder" runat="Server">
    <table id="breadcrumb">
        <tr>
            <td>
                <h1>
                    <%= HeaderTitle %>
                </h1>
            </td>
        </tr>
    </table>
    <div class="sw-pg-hint-inner-yellow">
        <div class="sw-pg-hint-body-yellow">
            These settings only apply to users who have a Toolset seat allocated to them.
        </div>
    </div>
    <div>
        &nbsp;</div>
    <div class="GroupBoxAccounts" id="AllPropertiesBox" style="min-width: 800px;">
        <div id="InerPropertyBox" style="padding: 0px 10px 5px 10px;">
            <input type="hidden" id="checkboxesVisible" name="checkboxesVisible" value="hidden"
                runat="server" />
            <input type="hidden" id="reqAccountIds" name="reqAccountIds" value="hidden" runat="server" />
            <div visible="true" runat="server" id="divAccountsList" style="border-bottom: #dbdcd7 1px solid;
                padding-bottom: 0px; width: 100%;">
                <table>
                    <tr>
                        <td style="padding-right: 15px; padding-top: 10px; padding-bottom: 10px;">
                            You are defining properties for:
                        </td>
                        <td style="padding-top: 10px; padding-bottom: 10px;">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Orion/images/Button.Expand.gif"
                                OnClientClick="ToggleContainerContents(this); return false;" CausesValidation="false" />
                            <asp:Label runat="server" ID="contentsSummaryLabel" CssClass="boldLabel" />
                            <div id="containerContentDiv" class="hidden">
                                <asp:Repeater runat="server" ID="contentsRepeater">
                                    <HeaderTemplate>
                                        <table id="ContainerMembersPreviewTable">
                                            <ul>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="padding-left: 20px;">
                                                <li style="min-width: 200px;">
                                                    <%# Eval("Account")%>
                                                </li>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul> </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                &nbsp;
            </div>
            <table id="accountPropTabel">
                <tr>
                    <td>
                        <input type="checkbox" runat="server" id="chbxAccountEnabled" onclick="togleRowState(this);" />
                    </td>
                    <td colspan="3">
                        Choose which web tools to enable for this user<div>
                            &nbsp;</div>
                        <asp:CheckBoxList ID="ToolsCheckboxList" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" runat="server" id="chbxSumview" onclick="togleRowState(this);" />
                    </td>
                    <td>
                        Summary View
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlSumview" Width="150px">
                            <asp:ListItem Text="None" Value="None"></asp:ListItem>
                            <asp:ListItem Text="Web Toolset Home" Value="Web Toolset Home"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        This allows the user to view the Toolset tab on the main navigation
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" runat="server" id="chbxOrionInteg" onclick="togleRowState(this);" />
                    </td>
                    <td>
                        Engineer's Toolset on Web Orion Integration
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlOrionIntegration" Width="150px">
                            <asp:ListItem Text="Disabled" Value="Disabled"></asp:ListItem>
                            <asp:ListItem Text="Enabled" Value="Enabled"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        This allows the user to instantly launch the tools for Orion nodes and interfaces from the specific dropdowns and resources.
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div class="sw-btn-bar">
                <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary"
                    CausesValidation="False" OnClick="btnBack_Click" />
                <orion:LocalizableButton runat="server" ID="submitButton" LocalizedText="Submit"
                    DisplayType="Primary" OnClick="btnSubmit_Click" />
            </div>
        </div>
    </div>
</asp:Content>
