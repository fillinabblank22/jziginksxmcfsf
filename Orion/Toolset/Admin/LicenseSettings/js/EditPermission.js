﻿function ToggleContainerContents(button) {
    var div = $("#containerContentDiv");
    div.toggleClass("hidden");
    if (div.hasClass("hidden")) {
        button.setAttribute("src", "/Orion/images/Button.Expand.gif");
    } else {
        button.setAttribute("src", "/Orion/images/Button.Collapse.gif");
    }
}
function togleRowState(checkBox) {
    if ((checkBox.tagName.toLowerCase() != "input")
        || (checkBox.type.toLowerCase() != "checkbox")) {
        return;
    }
    var tr = checkBox.parentNode;
    while (tr.tagName.toLowerCase() != "tr") {
        if (tr.tagName.toLowerCase() == "body") return;
        tr = tr.parentNode;
    }

    if ($(checkBox).hasClass("section-parentChbx")) {
        togleChildRows(checkBox, tr);
        return;
    }
    else if ($(checkBox).hasClass("section-childChbx")) {
        togleParentChbx(checkBox, tr);
    }

    var oCells = tr.cells;
    for (var i = 0; i < oCells.length; i++) {
        var cell = oCells[i];
        updateCellCssClass(cell, checkBox.checked);
        updateChild(cell, checkBox.checked);
    }
    return tr;
}

function togleChildRows(checkBox, tr) {
    var children = $(tr).find('input.section-childChbx');
    for (var i = 0; i < children.length; i++) {
        children[i].checked = checkBox.checked;

        var childtr = children[i].parentNode;
        //find child tr element
        while (childtr.tagName.toLowerCase() != "tr") {
            if (childtr.tagName.toLowerCase() == "body") {
                break;
            }
            childtr = childtr.parentNode;
        }
        if (childtr.tagName.toLowerCase() == "body") continue;

        var oCells = childtr.cells;
        for (var j = 0; j < oCells.length; j++) {
            var cell = oCells[j];
            updateCellCssClass(cell, checkBox.checked);
            updateChild(cell, checkBox.checked);
        }
    }
}

function togleParentChbx(checkBox, tr) {
    var parent = $(tr).parents('.section-containerChbx');
    var allChecked = true;
    var children = parent.find('input.section-childChbx');
    for (var i = 0; i < children.length; i++) {
        allChecked &= children[i].checked;
    }

    var parentChbx = parent.find('input.section-parentChbx');
    if (allChecked)
        parentChbx.attr('checked', 'checked');
    else
        parentChbx.removeAttr('checked');
}

function togleTabBarState(checkBox) {
    var tr = togleRowState(checkBox);   // enable/disable row with checkbox itself
    if (!tr) return;

    // enable/disable rows below checkbox until row with checkbox encountered
    while (true) {
        tr = tr.nextSibling;
        if (!tr.tagName || tr.tagName.toLowerCase() != "tr") // this line is required to fix FF adding input elemetns in DOM between <tr>
            continue;

        var oCells = tr.cells;
        if (oCells[0].children.length > 0) // if children present it must be checkbox and so stop
            return;

        for (var i = 0; i < oCells.length; i++) {
            var cell = oCells[i];
            updateCellCssClass(cell, checkBox.checked);
            updateChild(cell, checkBox.checked);
        }
    }
}

function updateCellCssClass(td, checked) {
    var current = td.className;
    if (current.length > 0) {
        if (current.indexOf(" ") > 0)
            current = current.replace(" cellDisabled", "");
        else current = current.replace("cellDisabled", "");
    }
    if (!checked) {
        if (current.length > 0)
            current = current + " cellDisabled";
        else current = "cellDisabled";
    }
    td.className = current;
}

function updateChild(td, enabled) {
    var oChildren = td.children;
    for (var i = 0; i < oChildren.length; i++) {
        var child = oChildren[i];
        if (child.tagName.toLowerCase() == "a") {
            if (child.id.length > 0) {
                if (enabled)
                    $('#' + child.id).css("visibility", "visible");
                else
                    $('#' + child.id).css("visibility", "hidden");
            }
        }

        if ((child.tagName.toLowerCase() == "select") || (child.tagName.toLowerCase() == "option") // option was added for FF ... 
                    || ((child.tagName.toLowerCase() == "input") && (child.type.toLowerCase() != "checkbox")))
            child.disabled = !enabled;
        else {
            if (child.parentElement != null) {
                if (child.parentElement.className == "childcheck") {
                    child.disabled = !enabled;
                    //                            child.parentElement.parentElement.className = "";
                }
            }
        }

        if (child.children && child.children.length > 0) {
            updateChild(child, enabled);
        }
    }
}

function SetDefaultState(checkBoxesVisibleId, tabMenuBarsId) {
    var showCheckboxes = document.getElementById(checkBoxesVisibleId);
    var inputs = document.getElementsByTagName("input");

    var chbxCnt = 0;
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type.toLowerCase() == "checkbox") {
            chbxCnt++;
        }
    }

    // if no checkboxes just enable in account properties table 
    if (chbxCnt == 0) {
        updateChild(document.getElementById("accountPropTabel"), true);
        return;
    }

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type.toLowerCase() == "checkbox") {
            var diableControls = true;
            if (inputs[i].parentElement != null) {
                if (inputs[i].parentElement.className == "childcheck") {
                    diableControls = false;
                }
            }

            if (diableControls) {
                if (showCheckboxes) {
                    if (showCheckboxes.value == "hidden") {
                        inputs[i].style.visibility = "hidden";
                    } else {
                        if (inputs[i].id == tabMenuBarsId)
                            togleTabBarState(inputs[i]);
                        else
                            togleRowState(inputs[i]);
                    }
                }
            }
        }
    }
}