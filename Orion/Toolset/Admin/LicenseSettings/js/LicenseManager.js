﻿Ext.namespace('SW');
Ext.namespace('SW.Core');

ToolsetLicenseManager = function () {
    function setFormFieldTooltip(id, tip) {
        var tt = new Ext.ToolTip({
            target: id,
            title: tip,
            plain: true,
            showDelay: 0,
            hideDelay: 0,
            trackMouse: false
        });
    }

    // Encoding value function
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    // rendering AccountID (Name) column applying highlighting to search text matches
    function renderAccountName(value) {

        var accName = value;
        var patternText = filterText;

        // replace any %'s with a *
        patternText = patternText.replace(/\%/g, "*");

        // replace multiple *'s with a single instance
        patternText = patternText.replace(/\*{2,}/g, "*");

        // check if the search string is now just down to a single *, and if so return without any further regex
        if (patternText == '*')
        { return '<span style=\"background-color: #FFE89E\">' + encodeHTML(accName) + '</span>'; }


        //check for trailing wildcard * on end of search string
        //var x = patternText.toString().substring(patternText.length - 1);
        //if (x == "*") { patternText = patternText.toString().substring(0, (patternText.length - 1)); }

        // replace \ with \\
        patternText = patternText.replace(/\\/g, '\\\\');
        // replace . with \.
        patternText = patternText.replace(/\./g, '\\.');
        // replace * with .*
        patternText = patternText.replace(/\*/g, '.*');

        // set regex pattern
        var x = '((' + patternText + ')+)\*';
        var content = accName,

        pattern = new RegExp(x, "gi"),
        //replaceWith = '<span style=\"background-color: #FFE89E\">$1</span>';

        //NOTE: we need to add literal SPAN 'markers' because otherwise the actual HTML span tags would get encoded as literal
        replaceWith = '{SPAN-START-MARKER}$1{SPAN-END-MARKER}';

        // do regex repleace
        accName = content.replace(pattern, replaceWith);

        //ensure highlighted content gets encoded
        accName = encodeHTML(accName);

        // now replace the literal SPAN markers with the HTML span tags
        accName = accName.replace(/{SPAN-START-MARKER}/g, '<span style=\"background-color: #FFE89E\">');
        accName = accName.replace(/{SPAN-END-MARKER}/g, '</span>');

        return accName;
    };

    // render AccountType column
    function renderAccountType(value) {
        if (value == '2') {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/windows.gif" /> Windows</div>';
        }
        else if (value == '4') {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Toolset/images/groupuser.png" /> Windows Group</div>';
        }
        else {
            var at = '<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/orion_generic_icon_orange.gif" /> Orion</div>';
        }
        return at;
    };

    // render the data columns which contain enabled/disabled icons
    function renderWebToolsetEnabled(value, meta, record) {
        if (value == 'true') {
            var yn = String.format('<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/ok_enabled.png" /> {0}</div>', 'Yes');
        } else {
            var yn = String.format('<div><image class="delete cursor-pointer" src="/Orion/Admin/Accounts/images/icons/disable.png" /> {0}</div>', 'No');
        }
        return yn;
    };

    //renderer for the permission column
    var renderPermissions = function (value) {
        return value + ' tools';
    };

    var updateLicenseCountDetails = function() {
        ORION.callWebService("/Orion/Toolset/Services/LicenseManager.asmx", "GetCounts",{}, function(result) {
            if ((result != null) && (result.length != 0)) {
                var count = result.split(',');
                if (!isDemo()) {
                    document.getElementById('lblToolsetRight').innerHTML = count[0];
                } else {
                    document.getElementById('lblToolsetRight').innerHTML = '3';
                }
                if (parseInt(count[1]) == 2147483647) {
                    document.getElementById('lblSeatCount').innerHTML = 'Unlimited';
                } else {
                    document.getElementById('lblSeatCount').innerHTML = count[1];
                }
                var ele = document.getElementById('ErrorBlock');
                if (parseInt(count[1]) < parseInt(count[0])) {
                    if (!ele) return true;
                    ele.style.display = "block";
                    document.getElementById('lblDisbleCount').innerHTML = parseInt(count[0]) - parseInt(count[1]);
                } else {
                    if (!ele) return true;
                    ele.style.display = "none";
                }
            }
        });
        };

    return {
        init: function () {
            updateToolbarButtons = function() {
            var selItems = grid.getSelectionModel(), count = selItems.getCount(), map = grid.getTopToolbar().items.map;

            map.EnableWebToolset.setDisabled(count === 0);
            map.DisableWebToolset.setDisabled(count === 0);
            map.EditPermissions.setDisabled(count === 0);
            
            if (isDemo()) {
                map.EnableWebToolset.setDisabled(true);
                map.DisableWebToolset.setDisabled(true);
            }
            };
            
            var dataStore;
            var grid;
            var pageSizeNum = parseInt(ORION.Prefs.load('PageSize', '10'));
            var selectorModel = new Ext.grid.CheckboxSelectionModel();
            selectorModel.on("selectionchange", updateToolbarButtons);

            function enableRight() {
                var ids = [];
                grid.getSelectionModel().each(function (rec) {
                    ids.push(rec.data["AccountID"]);
                });
                ORION.callWebService("/Orion/Toolset/Services/LicenseManager.asmx", "EnableRights", { ids: ids }, function (result) { grid.getStore().reload(); updateLicenseCountDetails(); }, function (result) {
                    if (result == 'Cant Enable WebToolset') {
                        grid.getStore().reload();
                        Ext.Msg.show({
                            title: 'Cannot execute this action',
                            msg: '<b>You\'re trying to enable Toolset for more <br/>users than you have licenses for.</b><br/><br/>Open the <a href="http://www.solarwinds.com/documentation/licenseManager/Help/LicenseManager.htm" target="_blank" class="links">License Manager</a> to activate more <br/>licenses',
                            icon: Ext.MessageBox.WARNING,
                            buttons: Ext.Msg.OK,
                        });
                    }
                });
                
            };

            function disableRight() {
                Ext.Msg.show({
                    title: 'Disable Toolset',
                    msg: 'Are you sure you want to disable Toolset for the selected users?',
                    buttons: Ext.Msg.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            var ids = [];
                            grid.getSelectionModel().each(function (rec) {
                                ids.push(rec.data["AccountID"]);
                            });
                            ORION.callWebService("/Orion/Toolset/Services/LicenseManager.asmx", "DisableRights", { ids: ids }, function (result) { grid.getStore().reload();updateLicenseCountDetails(); });
                        } else {
                            //TODO: if some action to be performed when user clicks on -No-
                        }
                    },
                    icon: Ext.MessageBox.WARNING
                });
            };

            function editPermission() {
                var ids = [];
                grid.getSelectionModel().each(function (rec) {
                    ids.push(rec.data["AccountID"]);
                });
                var stringForm = "<form id='accountDataForm' action='/Orion/Toolset/Admin/LicenseSettings/EditPermissions.aspx' method='POST'>"; 
                    stringForm += "<input type='hidden' name='hdnValue' value='"+ ids +"'> </input></form>";
                $('body').append(stringForm);
                $('#accountDataForm').submit();
            }
            Ext.override(Ext.PagingToolbar, {
                addPageSizer: function () {
                    // add a combobox to the toolbar
                    var store = new Ext.data.SimpleStore({
                        fields: ['pageSize'],
                        data: [[5], [10], [15], [20], [30], [50]]
                    });
                    var combo = new Ext.form.ComboBox({
                        regex: /^\d*$/,
                        store: store,
                        displayField: 'pageSize',
                        mode: 'local',
                        triggerAction: 'all',
                        selectOnFocus: true,
                        width: 50,
                        editable: false,
                        value: pageSizeNum
                    });
                    this.addField(new Ext.form.Label({ text: '@{R=Core.Strings;K=WEBJS_VB0_46;E=js}', style: 'margin-left: 5px; margin-right: 5px;vertical-align: middle' }));
                    this.addField(combo);

                    combo.on("select", function (c, record) {
                        this.pageSize = record.get("pageSize");
                        this.cursor = 0;
                        ORION.Prefs.save('PageSize', this.pageSize);
                        this.doRefresh();
                    }, this);
                }
            });

            //method to resize the grid according to the window size
            function gridResize() {
                grid.setWidth(1);
                grid.setWidth($('#gridCell').width());
            }

            //define datastore for license manager
            dataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/LicenseManager.asmx/GetAllUsers",
                [
                    { name: 'AccountID', mapping: 0 },
                    { name: 'AccountType', mapping: 1 },
                    { name: 'SettingValue', mapping: 2 },
                    { name: 'PermissionCount', mapping: 3 }
                ],
                "AccountID");

            //define grid panel
            grid = new Ext.grid.GridPanel({
                store: dataStore,
                columns: [selectorModel,
                    { header: 'Name', hidden: false, hideable: true, sortable: true, dataIndex: 'AccountID', renderer: renderAccountName, width: 200 },
                    { header: 'Account Type', hidden: false, hideable: true, sortable: true, dataIndex: 'AccountType', renderer: renderAccountType, width: 150 },
                    { header: 'Toolset Enabled', hidden: false, hideable: true, sortable: true, dataIndex: 'SettingValue', renderer: renderWebToolsetEnabled, width: 150 },
                    { header: 'Permissions', hidden: false, hideable: true, sortable: true, dataIndex: 'PermissionCount', renderer: renderPermissions, width: 150 }
                ],
                sm: selectorModel,
                autoScroll: true,
                height: 315,
                stripeRows: true,
                tbar: [{ id: 'EnableWebToolset', text: 'Enable', tooltip: 'Enable Toolset', icon: '/Orion/Toolset/images/Enable.png', handler: function () { enableRight(); } }, '-',
                       { id: 'DisableWebToolset', text: 'Disable', tooltip: 'Disable Toolset', icon: '/Orion/Toolset/images/Disable.png', handler: function () { disableRight(); } }, '-',
                       { id: 'EditPermissions', text: 'Edit Permissions', tooltip: 'Edit Permissions', icon: '/Orion/Toolset/images/Edit_Permission.png', handler: function () { editPermission(); } },
                       new Ext.ux.form.SearchField({ width: 260, store: dataStore })
                      ],
                cls: 'cls-of-my-grid',
                viewConfig: { forceFit: false },
                bbar: new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSizeNum,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_AK0_91;E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_AK0_88;E=js}"
                })
            });

            grid.getView().on('refresh', function () {
                var obj = $('.x-grid3-hd-checker');
                if (obj && obj.hasClass('x-grid3-hd-checker-on'))
                    obj.removeClass('x-grid3-hd-checker-on');
            });

            //rendering the grid on page
            grid.render('LicenseManagerGrid');
            updateToolbarButtons();
            grid.store.proxy.conn.jsonData = { accountID: '' };
            grid.store.load();

            setFormFieldTooltip('searchAccounts', '@{R=Core.Strings;K=WEBJS_AK0_92;E=js}');

            updateLicenseCountDetails();
            grid.bottomToolbar.addPageSizer();

            $("form").submit(function () { return false; });

            //to resize the grid according to the window size
            $(window).resize(gridResize);
        }
    };
} ();

Ext.onReady(ToolsetLicenseManager.init, ToolsetLicenseManager);