﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_ToolsetView : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        SolarWinds.Orion.Web.OrionInclude.ModuleFile("Toolset", "Toolset.css");
        //TODO: Remove this once we got working Orion for Android 4+
        if (MobileHelper.ApplyMobileView())
        {
            string url = Request.Url.GetLeftPart(UriPartial.Path);
            if (!Request.QueryString.ToString().ToLower().Contains("ismobileview"))
            {
                url += (Request.QueryString.ToString() == "") ? "?IsMobileView=true" : "?" + Request.QueryString.ToString() + "&IsMobileView=true";
                Response.Redirect(url);
            }
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}