<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/Toolset/ToolsetView.master"
    CodeFile="AllTools.aspx.cs" Inherits="Orion_Toolset_AllTools" Title="Engineer's Toolset on Web" %>

<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<asp:Content ID="ResourceContent" runat="server" ContentPlaceHolderID="ToolsetMainContentPlaceHolder">
    <%--TODO: FB332677 REMOVE following script when we got updated Orion HelpHelper() in new Orion Core version--%>
    <script>
        function GetUrl(helpLink) {
            var newLink;
            $.ajax({
                type: "POST",
                url: "/Orion/Toolset/Services/Toolset.asmx/GetHelpLink",
                data: JSON.stringify({ fragment: helpLink }),
                processData: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (msg) {
                    newLink = msg.d;
                },
                error: function(msg) {
                    return null;
                }
            });
            return newLink;
        }
                
        $(document).ready(function () {
            try {
                $(".sw-btn.sw-btn-resource.HelpButton").each(function (index, element) {
                    var helpLink = $(element).prop('href');
                    if (helpLink.indexOf("Toolset") !== -1 && helpLink.indexOf("helploader") == -1) {
                        var fragment = (helpLink.lastIndexOf("&") < helpLink.lastIndexOf("topic=")) ?
                            helpLink.substring(helpLink.lastIndexOf("topic=") + 6, helpLink.lastIndexOf(".htm")):
                            helpLink.substring(helpLink.lastIndexOf("topic=") + 6, helpLink.lastIndexOf("&"));
                        $(element).prop("href", GetUrl(fragment));
                    }
                });
            }
            catch (err) { 
                //I think we don't need to log anything here
            }
        });
    </script>
    <orion:ResourceContainer runat="server" ID="resContainer" />
</asp:Content>
