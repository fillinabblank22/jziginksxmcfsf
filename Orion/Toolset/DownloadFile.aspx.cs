﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Toolset.Web.FileDownload;

public partial class Orion_Toolset_DownloadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ProcessGetFileRequest();   
    }

    private void ProcessGetFileRequest()
    {
        Response.Clear();
        Response.ClearHeaders();
        Response.ClearContent();

        string fileId = ExtractFileId();
        
        var content = TemporaryContentContainer.Instance.GetItem(fileId);
        //firefox doesnt handle file names which has spaces properly,enclosing the filename with doubleQuoutes solves the problem
        Response.AddHeader("Content-Disposition", "attachment; filename=" + "\""+content.FileName+"\""); 
        Response.AddHeader("Content-Length", content.Length.ToString(CultureInfo.InvariantCulture));
        Response.ContentType = GetContentType(content.FileType);

        Response.Flush();

        content.WriteToResponse(Response);
        
        Response.Flush();
        Response.End();
        TemporaryContentContainer.Instance.RemoveItem(fileId);
    }

    private string ExtractFileId()
    {
        return Request["exportid"];
    }

    private string GetContentType(string fileExtention)
    {
        string contentType;

        switch (fileExtention)
        {
            case "txt":
                contentType = @"text/plain";
                break;
            case "csv":
                contentType = @"text/csv";
                break;
            case "pdf":
                contentType = @"application/pdf";
                break;
            default:
                contentType = "";
                break;
        }

        return contentType;
    }
}