﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyRecentTools.ascx.cs" Inherits="Orion_Toolset_Resources_Summary_MyRecentTools" %>
<%@ Register TagPrefix="Toolset" TagName="ResourceTable" Src="~/Orion/Toolset/Controls/ResourceTable.ascx" %>

<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true" OnLoad="Page_Load">
    <Content>
        <Toolset:ResourceTable runat="server" ID="table" />
    </Content>
</orion:resourceWrapper>