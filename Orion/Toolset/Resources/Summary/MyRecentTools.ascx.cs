﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.Controls.ResourceTable;
using SolarWinds.Toolset.Web.Controls.ResourceTable.ValueFormatters;
using SolarWinds.Toolset.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Toolset")]
public partial class Orion_Toolset_Resources_Summary_MyRecentTools : ToolsetBaseResource
{
    protected override string DefaultTitle
    {
        get { return "Recent Tools"; }

    }

    public override string HelpLinkFragment
    {
        get { return "ToolsetOrionResourceRecentTools"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var dal = new ToolsDAL();
            DataTable data = dal.GetMyRecentTools();

            List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>
            {
                new ResourceTableColumn
                {
                    DataItemKey = "IconToolNamewithLink",
                    Name = "TOOL",
                    CellControl = (IResourceTableControl)LoadControl("~/Orion/Toolset/Controls/ToolIconWithNameContent.ascx"),
                    CssClass="HeaderAlign"
                },
                new ResourceTableColumn
                {
                    DataItemKey = "LaunchedAtUtc",
                    Name = "LAST USED",
                    ValueFormatter = new LocalDateTimeValueFormatter()
                }
            };
            table.Columns = tableColumns;
            table.SetData(data);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceError(ResourceWrapper.Content);
        }
    }
}