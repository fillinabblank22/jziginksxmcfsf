﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyTools.ascx.cs" Inherits="Orion_Toolset_Resources_Summary_Tool" %>
<%@ Register TagPrefix="Toolset" TagName="ResourceTable" Src="~/Orion/Toolset/Controls/ResourceList.ascx" %>
<orion:resourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true"
    OnLoad="Page_Load">
    <Content>
         <Toolset:ResourceTable runat="server" ID="table" />
    </Content>
</orion:resourceWrapper>
