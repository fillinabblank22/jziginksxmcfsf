﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs"
    Inherits="Orion_Toolset_Resources_Summary_GettingStarted" %>
<orion:resourcewrapper runat="server" id="Wrapper" showeditbutton="False" showmanagebutton="True"
    cssclass="Toolset-gettingstarted-backgraound">
    <HeaderButtons>
        <orion:LocalizableButton ID="btnRemoveMe" CssClass="EditResourceButton" runat="server"
            OnClick="btnRemoveMe_OnClick" DisplayType="Resource" 
            Text='REMOVE RESOURCE' />
    </HeaderButtons>
    <Content>
        <table class="Toolset-gettingstarted-innertable" cellpadding="0" cellspacing="0">
             
            <tr>
                <td class="icon">
                    <img src="/Orion/Toolset/images/lightBulb.png" alt="" />
                </td>
                <td>
                    <div class="title">
                        <a target="_blank" href="<%= this.GetHelp %>">Learn the Basics of Toolset</a></div>
                    <div class="Toolset-text-helpful">
                        New to Engineer's Toolset on Web? Learn how Toolset works.
                    </div>
                </td>
                <td>
                    <a class="sw-btn" target="_blank" href="<%= this.GetHelp %>"><span class="sw-btn-c"><span class="sw-btn-t" style="display: inline-block;
                        min-width: 100px; text-align: center;">LEARN THE BASICS</span></span> </a>
                </td>
            </tr>
            <%if (Profile.AllowAdmin)
              { %>
            <tr>
                <td class="icon">
                    <img src="/Orion/Toolset/images/license.png" alt="" />
                </td>
                <td>
                    <div class="title">
                        <a target="_blank" href="/Orion/Toolset/Admin/LicenseSettings/Default.aspx">Manage User Licenses</a>
                    </div>
                    <div class="Toolset-text-helpful">
                        Set up users who will be able to use Toolset.
                    </div>
                </td>
                <td>
                    <a class="sw-btn" href="/Orion/Toolset/Admin/LicenseSettings/Default.aspx" target="_blank"><span class="sw-btn-c"><span class="sw-btn-t" style="display: inline-block;
                        min-width: 100px; text-align: center;">MANAGE LICENSES</span></span> </a>
                </td>
            </tr>
            <%} %>
        </table>
    </Content>
</orion:resourcewrapper>
