﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SolarWinds.InformationService.Contract;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.Controls.ResourceTable;
using SolarWinds.Toolset.Web.DAL;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Toolset")]
public partial class Orion_Toolset_Resources_Summary_Tool : ToolsetBaseResource
{
    protected override string DefaultTitle
    {
        get { return "My Tools"; }
    }

    public override string HelpLinkFragment
    {
        get { return "ToolsetOrionResourceMyTools"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var dal = new ToolsDAL();
            DataTable data = dal.GetMyTools();

            List<ResourceTableColumn> tableColumns = new List<ResourceTableColumn>
            {
                new ResourceTableColumn
                {
                    DataItemKey = "IconToolNamewithLink",
                    CellControl = (IResourceTableControl)LoadControl("~/Orion/Toolset/Controls/ToolIconCellContent.ascx")
                },
                new ResourceTableColumn
                {
                    DataItemKey = "NameDescriptionLinkScreenshot",
                    CellControl = (IResourceTableControl)LoadControl("~/Orion/Toolset/Controls/ToolWithDescriptionLinkCellContent.ascx")
                }
            };
            table.Columns = tableColumns;
            table.SetData(data);
        }
        catch (FaultException<InfoServiceFaultContract> ex)
        {
            SetResourceError(ResourceWrapper.Content);
        }
    }

    private string ToHtmlList(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return string.Empty;

        var lines = text.Replace("\r", "").Split('\n');
        var html = new StringBuilder();
        html.AppendLine("<ul><li>");
        html.AppendLine(lines.Aggregate((current, next) => current + "</li>" + Environment.NewLine + "<li>" + next));
        html.AppendLine("</li></ul>");

        return html.ToString();
    }
}