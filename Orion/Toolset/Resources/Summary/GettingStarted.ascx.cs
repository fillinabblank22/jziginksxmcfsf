﻿using System;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Toolset.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Toolset.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.Type, "Help")]
public partial class Orion_Toolset_Resources_Summary_GettingStarted : ToolsetBaseResource
{
    protected override string DefaultTitle
    {
        get { return "Getting Started with Engineer's Toolset on Web"; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string GetHelp
    {
        get {
            return HelpHelper.GetHelpUrl("ToolsetOrionLearnBasics");
        }
    }

    protected void btnRemoveMe_OnClick(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Page.Response.Redirect(Request.Url.ToString());
    }
}