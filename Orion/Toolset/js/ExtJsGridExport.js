﻿function ExportToCSV(extJsGrid, fileName, title) {
    exportToTextFile(extJsGrid, fileName, 'csv', ',', '\r\n', title);
}

function ExportToTxt(extJsGrid, fileName, title) {
    exportToTextFile(extJsGrid, fileName, 'txt',' ', '\r\n', title);
}

// fileType parameter used for setting correct content type on the web page
function exportToTextFile   (extJsGrid, fileName, fileType, separator, linebrake, title) {
    var dataArray = extJsGridToArray(extJsGrid, true);
    var exportFileContent = title + '\r\n' +  stringArrayToFile(dataArray, separator,linebrake);
    // forceFileDownload(fileName, exportFileContent);    
    forceFileDownloadFromServer(exportFileContent, fileName, fileType);
}

function getFieldText(fieldData) {
    var text;
    var dateFormat = 'Y-m-d g:i';
    
    if (fieldData == null || fieldData == undefined) {
        text = '';
    } else if (fieldData instanceof Date) {
        text = Ext42.Date.format(fieldData, dateFormat);
    } else if (typeof(fieldData) == 'number' || typeof(fieldData) == 'boolean'){
        text = fieldData;
    } else if (typeof(fieldData) == 'string') {
        if (/<.+?>/.test(fieldData.toString())){
            var element = $(fieldData);
            if (element.is('div') || element.is('span')) {
                text = element.text();
            } else {
                text = fieldData;
            }
            
        } else {
            text = fieldData;
        }
    }
    else {
        text = '';
    }

    return text;
}

function extJsGridToArray(grid, includeHeader) {
    
    var rows = [];
    var dataStore = grid.store;
    var cols = grid.columns;

    if(!IsMobile())
    {
        if (includeHeader) {
            var headers = [];
            Ext42.Array.each(cols, function(column) {
                if (column.hidden != true) {
                    headers.push(column.text);
                }
            });
            rows.push(headers);
        }
    }

    dataStore.each(function (record) {
        var row = [];
        Ext42.Array.each(cols, function(column, index) {
            if (column.hidden != true) {

           var fieldText ='';
           if(grid.getView().getCell(record, column).dom.innerHTML.indexOf("resGridErrorValue") > -1)
                fieldText = grid.getView().getCell(record, column).dom.textContent +':RED';
           else if(grid.getView().getCell(record, column).dom.innerHTML.indexOf("resGridWarningValue") > -1)
                fieldText = grid.getView().getCell(record, column).dom.textContent +':YELLOW';                    
           else
               fieldText = grid.getView().getCell(record, column).dom.textContent;
           
           if(!fieldText)
               fieldText = grid.getView().getCell(record, column).dom.innerText;
                
           row.push(fieldText.replace(/\r|\n/g, '').trim());
            
            }
        });
        rows.push(row);
    });

    return rows;
};

function stringArrayToFile(dataArray, separator, lineBreak) {
    var textFileContent = '';
    Ext42.Array.each(dataArray, function(row) {
        textFileContent = textFileContent + row.join(separator) + lineBreak;
    });
    return textFileContent;
};

function forceFileDownload(filename, content) {
    if ($.browser.msie) {
        forceFileDownloadMsie(filename, content);
    } else {
        forceFileDownloadFFChrome(filename, content);
    }
}

function forceFileDownloadFFChrome(filename, content){
     var element = document.createElement('a');
     element.href = 'data:attachment/csv, ' + escape(content);
     element.target = '_blank';
     element.download = filename;
     document.body.appendChild(element);
     element.click();
 }

function forceFileDownloadMsie(filename, content) {
	var csvContent=escape(content);
	var blob = new Blob([csvContent],{
		type: "text/csv;charset=utf-8;",
	});

    navigator.msSaveBlob(blob, filename);
}

function exportToTextFileServer (extJsGrid, fileName) {
    var dataArray = extJsGridToArray(extJsGrid, true);
    var exportFileContent = stringArrayToFile(dataArray,  ' ', '\r\n');
    forceFileDownloadFromServer(exportFileContent, fileName, 'txt');    
}    


function forceFileDownloadFromServer(content, filename, filetype) {
    var exportId = Math.random();
    
    $.ajax({
        type: 'POST',
        url: "/Orion/Toolset/Services/FileExporter.asmx/SaveTemporaryTextFile",
        data: JSON.stringify({ fileId: exportId.toString(), content: content, fileName: filename, fileType: filetype}),
        contentType: "application/json; charset=utf-8",    
        dataType: "json",        
        async: false,
        success: function(data) {
            var exportUrl = "/Orion/Toolset/DownloadFile.aspx?exportid=" + exportId;
            window.open(exportUrl);            
        },
        error: function(data) {
            alert('Sending file to server failed');
        }        
    });
    
}

function ExportToPdfServer(gridObject, fileName, title, header) {
    var dataArray = extJsGridToArray(gridObject, true);
    var exportUrl = "/Orion/Toolset/Services/PdfConverter.asmx/ConvertTableToPdf";

    $.ajax({
        type: 'POST',
        url: exportUrl,
        data: JSON.stringify({ table: dataArray, documentTitle: title, documentHeader: header, fileName: fileName}),
        contentType: "application/json; charset=utf-8",    
        dataType: "json",        
        async: false,
        success: function(data) {
            var exportUrl = "/Orion/Toolset/DownloadFile.aspx?exportid=" + data.d;
            window.open(exportUrl);             
        },
        error: function(data) {
            alert('Pdf file creation failed');
        }        
    });
    
}
