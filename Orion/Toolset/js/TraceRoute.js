﻿var toolTraceRoute = true;
var cachedResultColumnsPromise = null;
var cacheCleaner = null;
var toolInterfaceMonitor = false;

$(document).ready(function(){
    Ext42.require([
    'Ext42.data.*',
    'Ext42.grid.*',
    'Ext42.tree.*'
    ]);
});

(function(){

  var getTitle = function(){ return "TraceRoute to " + $("#labelAddrr").html(); };

  var currentdate = new Date(); 

    var fileName = "TraceRouteResults " 
                + currentdate.getFullYear() + "-" 
                +(currentdate.getMonth()+1) + "-" 
                + currentdate.getDate() + " " 
                + currentdate.getHours() + "."  
                + currentdate.getMinutes() + "." 
                + currentdate.getSeconds();


  SW.Toolset.Export.Init({ 
    txt: { domid: 'ExportToTxtId', grid: function(){ return resultsGridObj.grid; }, file: fileName+'.txt', title: getTitle },
    csv: { domid: 'ExportToCSVId', grid: function(){ return resultsGridObj.grid; }, file: fileName+'.csv', title: getTitle },
    pdf: { domid: 'ExportToPdf', grid: function(){ return resultsGridObj.grid; }, file: fileName+'.pdf', title: 'TraceRoute results', subtitle: getTitle, mode: 'table' }
  });

})();



$(document).ready(function () {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        InitResultNotificationClient(taskId, ReceiveTraceRouteResults, InvalidateResultColumnsCache);
        //delete cache once a 10 minutes
        cacheCleaner = setInterval(InvalidateResultColumnsCache, 600000);
    }

    $("#RunButton").bind("click", function () {
        if ($("#RunButton").attr("disabled") == "disabled") {
            return false;
        }
        if ($('#isSessionTerminated').val() == 'true') {
            document.location.reload(true);
            return false;
        }
        if ($("[id$='AutoCompleteTextbox']").attr('value') || $("[id$='txbInputAddressMobile']").attr('value')) {
            traceRouteTraceClick($("[id$='AutoCompleteTextbox']").attr('value'));
        } else {
            showEmptyMsg();
            return false;
        }
    });
    $("#imgbSelect").bind("click", function () {
        handleSelectNodeClick();
    });
    $("#RunButtonMobile").bind("click", function () {
        if ($("#RunButtonMobile").attr("disabled") == "disabled") {
            return false;
        }
        if ($('#isSessionTerminated').val() == 'true') {
            document.location.reload(true);
            return false;
        }
        if ($("[id$='txbInputAddressMobile']").attr('value')) {
            traceRouteTraceClick($("[id$='txbInputAddressMobile']").attr('value'));
        } else {
            showEmptyMsg();
            return false;
        }
    });

    $("#selectDevice").bind("click", function () {
        ShowSelectDeviceDialog();
    });

    function StopTask() {
        Ext42.MessageBox.confirm({
            title: 'Confirmation',
            width: 300,
            msg: 'Are you sure you want to stop the current real-time procedure? If you try to restart the procedure later, all data collected up to this point will be lost.',
            buttons: Ext42.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    traceRouteCancelClick();
                }
            },
            icon: Ext42.MessageBox.WARNING
        });
    }

    $("#cancelButton").bind("click", function (e) {
        e.preventDefault();
        StopTask();
    });
    $("#restartButton").bind("click", function (e) {
        e.preventDefault();
        traceRouteRestartClick();
        return false;
    });

    $(".hide-info").click(function () {
        PageMethods.HideTooltip();
        $(".default-message").hide();
    });
    $('#loading-cancel-div').css("display", "none");
    $('#statusExportDiv').css("display", "none");
    var ip = getURLParameter('ip') ? getURLParameter('ip') : getURLParameter('nodehostname');
    $("[id$='msetlink']").attr('href', 'TraceRouteSettings.aspx');
    $(".indent.setting").click(function () {
        window.location = "TraceRouteSettings.aspx";
    });
    $(".indent.setting").click(function () {
        window.location = "TraceRouteSettings.aspx";
    });
    $(".link").click(function () {
        window.location = "TraceRoute.aspx";
    });

    var renderEnginesPromise = RenderEnginesComboAsync();
    $.when(renderEnginesPromise).done(function () {
        {
            if (isDemoMode()) {
                if (typeof getIPsHistory == 'function') {
                    ip = getIPsHistory()[0];
                    $("[id$='AutoCompleteTextbox']").attr('value', ip);
                    $("[id$='txbInputAddressMobile']").attr('value', ip);
                    var fromNode = getURLParameter("ip") !== "null";
                    if (fromNode == true) {
                        traceRouteTraceClick();
                    }
                }
            } else {
                if (ip != 'null') {
                    $("[id$='AutoCompleteTextbox']").attr('value', ip);
                    $("[id$='txbInputAddressMobile']").attr('value', ip);

                    traceRouteTraceClick();
                }
            }
        }
    });
    preLoadImages();
});

function InvalidateResultColumnsCache() {
    cachedResultColumnsPromise = null;
}

function handleSelectNodeClick() {
    var address = $('#traceRouteHiddenField').val();
    if (address) {
        $("[id$='AutoCompleteTextbox']").attr('value', address);
        $("[id$='txbInputAddressMobile']").attr('value', address);
        traceRouteTraceClick(address);
        $('#select-dialog').dialog('close');
    }
    return false;
}

function validate(str) {
    return /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/.test(str);
}
function hasFinal(hops) {
    for (var i = 0; i < hops.length; i++) {
        if (hops[i].IsFinal == true) {
            return true;
        }
    }
    return false;
}

var msgDone = false;
function ShowSelectDeviceDialog() {
    if (isDemoMode()) {
        return false;
    }
    $("#searchdiv").show();
    $(".sw-btn-bar-wizard").show();
    $("#divGrid").show();

    $("#select-dialog").dialog({ modal: true, buttons: {}, width: 805, height: 548 });
    if (!msgDone) {
        SW.Orion.SelectNode.init(SW.Orion.NetObjects);
        msgDone = true;
    }
}

function showDiscoverMessage(isShown){
    if (isShown) {
        $('#discoverMsg').css("display", "inline");
        $('#discStopMsg').css("display", "inline");
        $('#tracingMsg').css("display", "none");
        $('#traceStopMsg').css("display", "none");
    } else {
        $('#discoverMsg').css("display", "none");
        $('#discStopMsg').css("display", "none");
        $('#tracingMsg').css("display", "inline");
        $('#traceStopMsg').css("display", "inline");
    }
}

function showEmptyMsg() {
    $("#dialog-message").dialog({ modal: true, buttons: { Ok: function () { $(this).dialog("close"); } }});
}
function showInvalidMsg() {
    $("#dialog-message-invalid").dialog({ modal: true, buttons: { Ok: function () { $(this).dialog("close"); } } });
}

function traceRouteRestartClick() {
    var mobile = getURLParameter("IsMobileView");
    if ('null' == mobile) {
        $("#RunButton").click();
    } else {
        $("#RunButtonMobile").click();
    }
    showDiscoverMessage(true);
}

var traceRouteCancelled = false;

function traceRouteCancelClick() {
    traceRouteCancelled = true;
    results = null;
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        CallServiceAsync("/Orion/Toolset/Services/TraceRouteService.asmx/StopTrace", "POST", JSON.stringify({ taskId: taskId }));
    }
    $('#completeStatus').html('TraceRoute Cancelled');
    $('#exportButton').css("display", "inline");
    var mobile = isMobileView();
    if (false == mobile) {
        $('.status-line').css("display", "inline");
        document.getElementById("restart-div").style.display = "inline";
    } else {
        console.log('Mobile');
        $('.status-line').css("display", "block");
        document.getElementById("restart-div").style.display = "block";
    }
    $('#exportButton').css("display", "none");
    if (lastResult != null) {
        if (lastResult.HOPs.length > 0) {
            $('#exportButton').css("display", "inline");
        }
    }
    $('#loading-cancel-div').css("display", "none");
    document.getElementById("completeStatus").style.display = "block";
    return false;
}

function traceRouteCompleted() {
    traceRouteCancelled = false;
    $('#completeStatus').html('The route has been discovered.');
    $('#exportButton').css("display", "inline");
    document.getElementById("restart-div").style.display = "none";
    var mobile = isMobileView();
    if (false == mobile) {
        $('.status-line').css("display", "inline");
    } else {
        $('.status-line').css("display", "block");
    }
    //display export button if results were received
    $('#exportButton').css("display", "none");
    if (lastResult != null) {
        if (lastResult.HOPs.length > 0) {
            $('#exportButton').css("display", "inline");
        }
    }

    showDiscoverMessage(false);
    document.getElementById("restart-div").style.display = "none";
    $('#statusExportDiv').css("display", "inline");
    document.getElementById("completeStatus").style.display = "block";
}

function validateTrace() {
    if ($("[id$='AutoCompleteTextbox']").attr('value')) { return true; }
    return false;
}
function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

function fillInputs(input) {
    $("#AutoCompleteTextbox").val(input);
    $("#txbInputAddressMobile").val(input);
}

function GetSelectedEngine() {
    return $("#pollingEnginesSelect option:selected").text();
}

//fills a list of available engines to a combobox asynchronously
function RenderEnginesComboAsync() {
    var fillEnginesCombo = function(data) {
        $.each(data.d, function(index, item) { $('#pollingEnginesSelect').append(new Option(item.Name, item.ID)); });
    };
    var enginesPromise = CallServiceAsync("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableEngines", "GET", {});
    return $.when(enginesPromise).pipe(fillEnginesCombo);
}

//starts/restarts the tool
function traceRouteTraceClick(input) {
    var address = input;
    if (!input) {
        if ($("[id$='txbInputAddressMobile']").attr('value')) {
            address = $("[id$='txbInputAddressMobile']").attr('value');
        } else {
            address = $("[id$='AutoCompleteTextbox']").attr('value');
        }
    }
    if (!validate(address)) {
        showInvalidMsg();
        return;
    }
    if (resultsGridObj.grid) {
        var widths = [];
        for (var i = 0; i < resultsGridObj.grid.columns.length; i++) {
            widths.push(resultsGridObj.grid.columns[i].width);
        }
        $("#txtResTblWidths").val(widths.join("|"));
    }
    var id = $("#taskIdTextbox").val();
    results = null;
    if (id) {
        StopResultNotificationClient(id);
        clearInterval(cacheCleaner);
        if (resultsGridObj.grid) {
            var dataStore = resultsGridObj.grid.getStore();
            dataStore.removeAll();
        }
        traceRouteCancelClick();
    }
    $('#completeStatus').html('');
    showDiscoverMessage(true);
    resultCount = 1;
   
     $("[id$='ipAddress']").attr('value', address);
    var lbl = document.getElementById('labelAddrr');
    lbl.innerHTML = address;
    $("[id$='loadDiv']").css("display", "inline");
    $("#hintDiv").css("display", "none");
    document.getElementById("restart-div").style.display = "none";
    $("#divSpinner").css("display", "block");
    $('#exportButton').css("display", "none");
    var mobile = isMobileView();
    if (false == mobile) {
        $('.status-line').css("display", "block");
        $('#loading-cancel-div').css("display", "inline");
    } else {
        $('#loading-cancel-div').css("display", "block");
        $('.status-line').css("display", "block");
    }
    $('#statusExportDiv').css("display", "inline");
    var selectedEngine = GetSelectedEngine();
    var dns = document.getElementById("cbDNSResolve").checked;
    traceRouteCancelled = false;

    //start the tool and register it in SignalR
    var jsonData = JSON.stringify({ input: address, controlId: "TraceRouteHost", controlType: "Host", pollingEngine: selectedEngine, resolveDNS: dns });
    var runPromise = CallServiceAsync("/Orion/Toolset/Services/TraceRouteService.asmx/RunTrace", "POST", jsonData);

    var initTaskId = function(data) {
        var taskId = data.d;
        if (taskId != "" && taskId) {
            SetUpTaskId(taskId);
            InitResultNotificationClient(taskId, ReceiveTraceRouteResults);
        }
    };

    runPromise.done(initTaskId);
    setTimeout(function () {
        if ((typeof resultCounter != 'undefined') && (resultCounter == 0)) {
            $("#InnerText").html("<div>Response delayed? See the Solarwinds <a href='http://knowledgebase.solarwinds.com/kb/questions/5604/Error%3A+%22Polling+has+stopped.+Please+close+this+tool%2C+and+contact+your+server+Administrator+for+assistance.%22' target='_blank' class='KBlinks'> Knowledge Base.</a></div>");
        }
    }, 60000);
}

var lastResult = null;
var traceRouteResultGrid;

var resultCount = 1;
var results;

//asynchronously prepares current/cached result columns
function getResultColumnsAsync() {
    if (cachedResultColumnsPromise == null) {
        cachedResultColumnsPromise = CallServiceAsync("/Orion/Toolset/Services/TraceRouteSettings.asmx/GetResultColumns", "GET", {});
    }
    return cachedResultColumnsPromise;
}

//processes data from SignalR
function ReceiveTraceRouteResults(newResult) {
    if (traceRouteCancelled)
        return;
    lastResult = newResult;

    $("#lastUpdate").val(new Date().getTime());
    
    if (newResult.CompletionInfo.CompletionStatus == 4) {

        traceRouteCompleted();
        $('#completeStatus').html('TraceRoute Failed with message: ' + newResult.CompletionInfo.FaultInfo);
        return;
    }
    if (newResult.HOPs) {
        if (hasFinal(newResult.HOPs)) {
            traceRouteCompleted();
            if (!results) {
                results = newResult.HOPs;
                for (var j = 0; j < newResult.HOPs.length; j++) {
                    if (newResult.HOPs[j].ResponseTime >= 0) {
                        results[j].AvgResponse = newResult.HOPs[j].ResponseTime;
                        results[j].MinResponse = newResult.HOPs[j].ResponseTime;
                        results[j].MaxResponse = newResult.HOPs[j].ResponseTime;
                        results[j].ResultsCount = 1;
                    }
                    if (newResult.HOPs[j].ResponseTime == null) {
                        results[j].PacketLoss = 100;
                        newResult.HOPs[j].PacketLoss = 100;
                    }
                }
            }
        }
    }
    if (results) {
        for (var j = 0; j < newResult.HOPs.length; j++) {
            if (newResult.HOPs[j].IsUpdated) {
                for (var k = 0; k < results.length; k++) {
                    if (results[k].HOP == newResult.HOPs[j].HOP) {
                        results[k].AvgResponse = results[k].AvgResponse + newResult.HOPs[j].ResponseTime;
                        results[k].ResultsCount++;
                        if (newResult.HOPs[j].ResponseTime < results[k].MinResponse) {
                            results[k].MinResponse = newResult.HOPs[j].ResponseTime;
                        }
                        if (newResult.HOPs[j].ResponseTime > results[k].MaxResponse) {
                            results[k].MaxResponse = newResult.HOPs[j].ResponseTime;
                        }
                    }
                }
            }
            for (var k = 0; k < results.length; k++) {
                if (results[k].HOP == newResult.HOPs[j].HOP) {
                    var avg = 0;
                    if (results[k].AvgResponse > 0) {
                        avg = Math.round(results[k].AvgResponse / results[k].ResultsCount);
                    }
                    if (newResult.HOPs[j].ResponseTime != null) {
                        newResult.HOPs[j].AvgResponse = avg;
                    }
                    newResult.HOPs[j].MinResponse = results[k].MinResponse;
                    newResult.HOPs[j].MaxResponse = results[k].MaxResponse;
                }
                if (newResult.HOPs[j].ResponseTime == null) {
                    newResult.HOPs[j].PacketLoss = 100;
                }
            }
        }
    }
    var widthVal = [];
    if ($("#txtResTblWidths").val()) {
        widthVal = $("#txtResTblWidths").val().split('|');
    }

    resultsGridObj.gridColWidth = [];
    for (var i = 0; i < widthVal.length; i++) {
        resultsGridObj.gridColWidth.push(parseInt(widthVal[i]));
    }
    resultsGridObj.sortable = false;

    //get result columns and update chart asynchronously
    var resultColumnsPromise = getResultColumnsAsync();
    var updateResultGridPromise = updateResultGridAsync(newResult, resultColumnsPromise);
}

//asynchronously renders result grid
function updateResultGridAsync(newResult, resultColumnsPromise) {
    return resultColumnsPromise.pipe(function (data) {
        var resultcolumns = data.d;

        if ($('#jsonData').css("display") == "none") {
SW.Toolset.Visibility.CallWhenVisible('grid', function(){
            CreateResultGridMobile(resultcolumns, newResult.HOPs, 'jsonDataMobile', 400, 400);
});
        } else {
SW.Toolset.Visibility.CallWhenVisible('grid', function(){
    CreateResultGrid(resultcolumns, newResult.HOPs, 'jsonData', 600, 700);
            
});
        }
    });
}

//
// Functions copied from ConfigWizard.js, because it is not included
//

//runs a specified web service asynchronously and returns its Promise
function CallServiceAsync(serviceUrl, method, passData, successCallback, errorCallback) {
    return $.ajax({
        type: method,
        url: serviceUrl,
        data: passData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (successCallback) === 'function') {
                successCallback(data.d);
            }
        },
        error: function (data, textStatus, xhr) {
            if (typeof (errorCallback) === 'function') {
                errorCallback(data, textStatus, xhr);
            }
        }
    });
}

var cachedRenderedNodes = null;
var cachedRenderedNodesCleaner = null;
//returns Node names with statuses as HTML fragments
//requires a list of IP addresses
//calls GetNodeNetObjectIds web service if the requested data is not already cached
function getNodeNames(list) {
    if (cachedRenderedNodesCleaner == null) {
        cachedRenderedNodesCleaner = setInterval(function() {
            cachedRenderedNodes = null;
        }, GetCacheInvalidationInterval());
    }

    var result = [];

    var fillErrorResult = function (addresses) {
        var newData = [];
        for (var i = 0; i < addresses.length; i++) {
            newData.push({ IP: addresses[i], HTML: addresses[i] });
        }
        return newData;
    }; 
	
	var callWebService = function (addresses) {        
		var newData = [];		
        $.ajax({
            type: "POST",
            url: "/Orion/Toolset/Services/Toolset.asmx/GetSNMPInformation",
            data: JSON.stringify({ ipaddress: addresses }),
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) 
			{
				if(addresses.length <2)
				{
					//This is first load
					for (var i = 0; i < addresses.length; i++) 
					{
						newData.push({ IP: addresses[i],Tool:1,HTML: msg.d[i] });
					}
				}
				else
				{
					//alert("refreshing second time ");
					for (var i = 0; i < addresses.length; i++) 
					{
						var data =findMsgData(addresses[i],msg);
						newData.push({ IP: addresses[i],Tool:1,HTML:data});
					}
					//alert("new data uploaded ");
				}
            },
            error: function (msg) {
                newData = fillErrorResult(addresses);
            }
        });		
        return newData;
    };

	var findMsgData =function(address,msg)
	{
		
		for (var i = 0; i < msg.d.length -1; i++) 
		{
			var textData =msg.d[i];
			if(textData) 
			{
				var arrayData = textData.split("£");	
				var keyValue = arrayData[1].split("¥");
				if(keyValue[1] ==address)									
				{
					return msg.d[i];
				}												
			}
		}
		return "";
	};

    var getDataFromCache = function (ipList, cache, resultList, newItemsList) {
        for (var i = 0; i < ipList.length; i++) {
            var found = false;
            for (var j = 0; j < cache.length; j++) {
                if (ipList[i] == cache[j].IP) {
                    resultList.push(cache[j]);
                    found = true;
                    break;
                }
            }
            if (found == false) {
                newItemsList.push(ipList[i]);
            }
        }
        return newItemsList.length == 0;
    };

    if (list.length != 0 && cachedRenderedNodes == null) {
        cachedRenderedNodes = callWebService(list);
        result = cachedRenderedNodes;
    }
    else if (list.length != 0 && cachedRenderedNodes != null) {
        var newItems = [];

        var success = getDataFromCache(list, cachedRenderedNodes, result, newItems);
        if (!success) {
            var newCache = callWebService(newItems);
            cachedRenderedNodes = newCache.concat(cachedRenderedNodes);
            result = [];
            newItems = [];
            success = getDataFromCache(list, cachedRenderedNodes, result, newItems);
            if (!success) {
                result = fillErrorResult(list);
            }
        }
    }
    return result;
}

//Temporary workaround to pre-load imgs, that aren`t displaying in mobile safari.
function preLoadImages() {
    var checkBoxIcon = new Image();
    var warningIcon = new Image();
    
    warningIcon.src = "../../../js/extjs/4.2.2/resources/images/shared/icon-warning.gif";
    checkBoxIcon.src = "../../../js/extjs/resources/images/default/grid/row-check-sprite.gif";
}
