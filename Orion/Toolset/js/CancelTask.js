﻿function CancelTask(taskId) {
    if (taskId){
        $.ajax({
            type: "POST",
            url: "/Orion/Toolset/Services/TaskWebService.asmx/CancelTask",
            data: JSON.stringify({ stringTaskId: taskId }),
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(data) {
            },
            error: function(data) {
            }
        });
        StopResultNotificationClient(taskId);
    }
}