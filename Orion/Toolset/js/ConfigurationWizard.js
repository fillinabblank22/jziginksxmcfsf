﻿var tree;
var currentTreeData;
var currentSelectedData;
var selectedGrid;
var toolCPUMonitor;
var toolMemoryMonitor;
var toolResponseMonitor;
var toolInterfaceMonitor;
var enginesCombo;
var taskIPMapping = [];
var isNPMInstalled = false;
var isNPMInstalledForPreload = false;

var availableNodesPanel;
var selectedItemsPanel;
var maxRealTimeMonitorItemsCount;
var reachedFlag = false;
var arSelectedColumnWidth = [] ;

function IsConfigurationWizardLoaded() {
    return (typeof(GetToolID) != "undefined");
}

function GetToolIDInternal() {
    if (typeof (GetToolID) != "undefined") {
        return GetToolID();
    } else {
        return -1;
    }
}

$(document).ready(function () {
    Ext42.require([
    'Ext42.data.*',
    'Ext42.grid.*',
    'Ext42.tree.*'
    ]);
    Ext42.Error.notify = false;
    window.onerror = function (e) {
        SW.Toolset.Log.log(e, 5);
        return true;
    };

    $("#RunButton").attr("disabled", "disabled");

    if (!IsConfigurationWizardLoaded())
        return;

    InitializeCustomToolProperties();
    isNPMInstalled = false;
    isNPMInstalledForPreload = toolInterfaceMonitor && GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/IsNpmInstalled", "GET", {});
    //Uncomment and delete two previous lines to enable interfaces in left panel if NPM is installed.
    //isNPMInstalled = toolInterfaceMonitor && GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/IsNpmInstalled", "GET", {});
    //isNPMInstalledForPreload = isNPMInstalled;
    maxRealTimeMonitorItemsCount = GetMaximumSelectedRealtimeMonitorItems();
    if (!IsMobileViewMode()) {
        InitializeAddDialog();
        RenderEnginesCombo();
        RenderCredentilasCombo();
        BuildSelectedTree();
        ShowCW();
    } else {
        $(".addDeviceDialog").hide();
        availableNodesPanel = new Ext42.panel.Panel({
            title: GetAvailableTitle(),
            collapsible: true,
            collapsed: true,
            width: $(document).width() - 30,
            height: isLowResoultionMobile() ? 535 : 710,
            html: '<div id="nodePanelContent"/>',
            renderTo: "Div1"
        });
        RenderEnginesCombo();
        RenderCredentilasCombo();
        ShowCW();
        BuildSelectedTree();

        $("#searchText").attr("placeholder", "Search node in groups");
        $("#searchBtn").html('&nbsp;');
    }
    var predefinedNode = GetPresectedNode();
    if (!IsDemoView() && predefinedNode && selectedGrid) {
        var id = GetGUID();
        var nodeId = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetNodeIdByIp", "POST", JSON.stringify({ ipAddress: predefinedNode.ip }));
        selectedGrid.getStore().add({ ID: id, IdKey: MakeIpHTMLAvailable(id), DisplayName: predefinedNode.host, IpAddress: predefinedNode.ip, NodeId: nodeId, CredentialId: -1 });
        if (predefinedNode.interface && $.isNumeric(predefinedNode.interface)) {
            var speed = 0;
            if (isNPMInstalledForPreload) {
                speed = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetInterfaceSpeedByNodeAndIndex", "POST", JSON.stringify({ "nodeId": nodeId, "index": predefinedNode.interface }));
            }
            setTimeout(function () {
                var index = selectedGrid.getStore().data.items.length - 1;
                ExpandNestedGrid([index]);

                var result = [];
                if (isNPMInstalledForPreload) {
                    result = GetJSONDataNoAlertWhenFails("/Orion/Toolset/Services/Toolset.asmx/GetInterfaceNetObjectIdsByIndex", "POST", JSON.stringify({ ids: [predefinedNode.interface], ipaddress: [predefinedNode.ip] }));
                }

                RenderNestedGrid([{ "polledItemID": predefinedNode.interface, "polledItemName": predefinedNode.interfaceName,
                    "newPolledItemName": result[0] ? result[0] : predefinedNode.interfaceName, "RxSpeed": speed, "TxSpeed": speed, "DiscoveredSpeed": speed
                }], MakeIpHTMLAvailable(id));
                setTimeout(function () {
                    SelectDesectAll(true);
                    EnableDisableSelectDesect();
                }, 100);

            }, 100);
        } else {
            setTimeout(function () {
                SelectDesectAll(true);
                EnableDisableSelectDesect();
            }, 100);
        }
    }

    BuildGroupedTree('', IsMobileViewMode());
    if (IsDemoView()) {
        ApplyDemoViewChanges();
    }

    preLoadImages();
});

function showWarningMessage(message, title) {
   Ext42.MessageBox.alert({
        title: title,
        width: 250,
        msg: message,
        buttons: Ext42.MessageBox.OK,
        icon: Ext42.MessageBox.WARNING
    }); 
}

function showMessage(message,title) {
    Ext42.MessageBox.alert({
        title: title,
        width: 250,
        msg: message,
        buttons: Ext42.MessageBox.OK,
        icon: Ext42.MessageBox.INFO
    });
}

function ApplyDemoViewChanges(){
    $("#pollingEnginesSelect").attr('disabled', true);
    $("#pollingEnginesSelect").addClass('sw-btn-disabled');
    $('#' + GetIPDialogTextBoxID()).attr('disabled', true);
    $('#' + GetIPDialogTextBoxID()).addClass('sw-btn-disabled');
    if(IsMobileViewMode())
    {
        $('#testCredBtn').attr('disabled', true);
        $('#testCredBtn').addClass('sw-btn-disabled');
        $('#addDeviceBtn').attr('disabled', true);
        $("#addDeviceBtn").addClass('sw-btn-disabled');
        $('#addDeviceNotCloseBtn').attr('disabled', true);
        $("#addDeviceNotCloseBtn").addClass('sw-btn-disabled');
    }
    else
    {
        $('#' + GetTestCredentilasButtonID()).attr('disabled', true);
        $('#' + GetTestCredentilasButtonID()).addClass('sw-btn-disabled');
        $('#' + GetAddDeviceButtonID()).attr('disabled', true);
        $('#' + GetAddDeviceButtonID()).addClass('sw-btn-disabled');
        $('#' + GetAddDeviceKeepButtonID()).attr('disabled', true);
        $('#' + GetAddDeviceKeepButtonID()).addClass('sw-btn-disabled');
    }
    $(".customDialog .isDemoMsg").show();
}

function CloseSpeedDialog(){
     if (IsMobileViewMode()) {
        $("#CWHolder > div:first").css("display", "");
        $("#Div1").css("display", "inline");
        $("#Div2").css("display", "inline");
        $("#selectedItemsBottomDiv").css("display", "");
        $("#Footer").css("display", "");
         $("#speedDialog").css("display", "none");
     } else {
        $("#speedDialog").dialog("close");
     }
}

function OpenSpeedDialog() {
    HideTooltip();
    if (IsMobileViewMode()) {
        $("#Div1").css("display", "none");
        $("#CWHolder > div:first").css("display", "none");
        $("#Div2").css("display", "none");
        $("#selectedItemsBottomDiv").css("display", "none");
        $("#Footer").css("display", "none");
        $("#speedDialog").css("display", "block");
    } else {
        $("#speedDialog").dialog("open");
    }
}

function OpenAddDeviceAccount() {
    $("#dialog").dialog("open");
}

function CloseCredentilasDialog() {
    if (IsMobileViewMode()) {
        $(".addDeviceDialog").css("display", "none");
        $("#Div1").css("display", "inline");
        $("#Div2").css("display", "inline");
        $("#selectedItemsBottomDiv").css("display", "");
        $("#Footer").css("display", "");
    } else {
        $("#dialog").dialog("close");
    }
}

function InitializeAddDialog() {
    $("#dialog").dialog({
        autoOpen: false,
        dialogClass: "customDialog",
        modal: true,
        width: 610
    });
    $( "#speedDialog" ).dialog({
      autoOpen: false,
	  dialogClass: "customDialog",
	  modal: true,
	  width: 500
    });
}

function AddDevice() {
    if (IsDemoView()) {
        return false;
    }
    if (!CheckSessionAndRefresh()) {
        ShowDisconnectionAlert();
        document.location.reload(true);
    }
    var ip = GetIPDialogValue();
    if (!ip) {
        showWarningMessage('@{R=Toolset.Strings;K=ToolsetGUICODE_CW_1;E=js}', '@{R=Toolset.Strings;K=ToolsetGUICODE_CW_2;E=js}');
        return false;
    }
    var credId = GetCredentilasID();
    if (!credId) {
        showWarningMessage('@{R=Toolset.Strings;K=ToolsetGUICODE_CW_3;E=js}', '@{R=Toolset.Strings;K=ToolsetGUICODE_CW_4;E=js}');
        return false;
    }
    if (ip && credId) {
        var id = GetGUID();
        var items = selectedGrid.getStore().data.items;
        var duplicate = false;
        $.each(items, function() {
             if (this.data.DisplayName == ip || this.data.IpAddress == ip) {
                 duplicate = true;
             }
         });
         if (!duplicate) {
            selectedGrid.getStore().add({ ID: id, IdKey: MakeIpHTMLAvailable(id), DisplayName: ip, IpAddress: ip, NodeId: -1, CredentialId: credId });
            SaveAutoCompleteIP("WizardHost", "Host", ip);
        } else {
            showWarningMessage('@{R=Toolset.Strings;K=ToolsetGUICODE_CW_5;E=js}', '@{R=Toolset.Strings;K=ToolsetGUICODE_CW_6;E=js}');
            return false;
        }
    }
    EnableDisableSelectDesect();
    return true;
}
function AddDeviceAndClose() {
    var dupl = AddDevice();
    if (!IsDemoView()) {
        if (dupl)
            CloseCredentilasDialog();
    }
}

function MoveRight() {
    if ($('#isSessionTerminated').val() == 'true') {
        document.location.reload(true);
    }
    Ext42.suspendLayouts();
    if (selectedGrid) {
        if (isNPMInstalled) {
            var records = tree.getView().getChecked();
            var nodes = [];
            var nodeIds = [];
            var interfaces = [];
            $.each(records, function (recIndex, rec) {
                if (rec.data.leaf) {
                    $.each(currentTreeData, function (index, item) {
                        if (rec.parentNode.internalId == "root") {
                            if ($.inArray(item.NodeID, nodeIds) < 0) {
                                nodeIds.push(rec.raw.NodeID);
                                var id = GetGUID();
                                nodes.push({ ID: id, IdKey: MakeIpHTMLAvailable(id), DisplayName: rec.raw.NodeName, IpAddress: rec.raw.IpAddress, NodeId: rec.raw.NodeID, CredentialId: rec.raw.CredentialId });
                            }
                        }
                        else if (item.InterfaceID == rec.data.qtip) {
                            if ($.inArray(item.NodeID, nodeIds) < 0) {
                                nodeIds.push(item.NodeID);
                                var id = GetGUID();
                                nodes.push({ ID: id, IdKey: MakeIpHTMLAvailable(id), DisplayName: item.NodeName, IpAddress: item.IpAddress, NodeId: item.NodeID, CredentialId: item.CredentialId });
                            }
                            if (item.InterfaceID >= 0) {
                                interfaces.push({ "NodeId": item.NodeID, "polledItemID": item.InterfaceIndex,
                                    "polledItemName": item.text, "RxSpeed": item.InterfaceSpeed, "TxSpeed": item.InterfaceSpeed,
                                    "DiscoveredSpeed": item.InterfaceSpeed, "IpAddress": item.IpAddress,
                                    "InterfaceID": item.InterfaceID
                                });
                            }
                        }
                        else if (item.InterfaceID == -1 && item.IpAddress == rec.data.qtip)
                        {
                            nodeIds.push(item.NodeID);
                            var id = GetGUID();
                            nodes.push({ ID: id, IdKey: MakeIpHTMLAvailable(id), DisplayName: item.NodeName, IpAddress: item.IpAddress, NodeId: item.NodeID, CredentialId: item.CredentialId });
                        }
                    });
                }
            });
            
            var ifnames = [], ips = [];
            for (var i = 0; i < interfaces.length; i++) {
                ifnames.push(interfaces[i].InterfaceID);
                ips.push(interfaces[i].IpAddress);
            }

            var result = [];
            if (isNPMInstalledForPreload) {
                result = GetJSONDataNoAlertWhenFails("/Orion/Toolset/Services/Toolset.asmx/GetInterfaceNetObjectIdsById", "POST", JSON.stringify({ ids: ifnames, ipaddress: ips }));
            }

            for (i = 0; i < interfaces.length; i++) {
                interfaces[i].newPolledItemName = result[i] ? result[i] : interfaces[i].polledItemName;
            }
            
            var store = selectedGrid.getStore();
            $.each(nodes, function (index, node) {
                var nodeIdKey = MakeIpHTMLAvailable(node.IdKey);
                $.each(store.data.items, function (index, item) {
                    if (item.data.NodeId == node.NodeId) {
                        nodeIdKey = item.data.IdKey;
                        return false;
                    }
                });
                var interfaceGridId = "InterfaceNestedGrid" + nodeIdKey;
                var grid = Ext42.getCmp(interfaceGridId);
                if (!grid) {
                    store.add(node);
                    var index = store.data.items.length - 1;
                    ExpandNestedGrid([index]);
                }
                var data = [];
                $.each(interfaces, function (index, interface) {
                    if (node.NodeId == interface.NodeId) {
                        var interfaceToPush = { "polledItemID": interface.polledItemID, "polledItemName": interface.polledItemName,
                            "newPolledItemName": interface.newPolledItemName, "RxSpeed": interface.RxSpeed, "TxSpeed": interface.TxSpeed,
                            "DiscoveredSpeed": interface.DiscoveredSpeed
                        };
                        if (!grid) {
                            data.push(interfaceToPush);
                        } else {
                            grid.getStore().add(interfaceToPush);
                        }
                    }
                });
                if(!grid){
                    RenderNestedGrid(data, nodeIdKey);
                }
            });

        }else{
            var records = tree.getView().getChecked();
            $.each(records, function (recIndex, rec) {
                if (rec.data.leaf) {
                    $.each(currentTreeData, function (index, item) {
                        if (item.IpAddress == rec.data.qtip) {
                            selectedGrid.getStore().add({ ID: item.ID, IdKey: MakeIpHTMLAvailable(item.ID),
                                DisplayName: item.text, IpAddress: item.IpAddress, NodeId: item.NodeID,
                                CredentialId: item.CredentialId, StyleClass: item.StyleClass
                            });
                        }
                    });
                }
            });
        }
    }
    RefreshTree($('#groupSelect').val(), $("#searchText").val());
    EnableDisableSelectDesect();
    Ext42.resumeLayouts(true);
}

function MoveLeft() {
    if ($('#isSessionTerminated').val() == 'true') {
        document.location.reload(true);
    }
    Ext42.suspendLayouts();
    var ips = [];
    $("input.parentIpCb:checked").each(function () {
        ips.push($(this).val());
    });
    var removeDone = false;
    if (ips.length > 0) {
        var itemsToRemove = [];
        $.each(selectedGrid.getStore().data.items, function (index, item) {
            if ($.inArray(item.data.ID, ips) > -1) {
                itemsToRemove.push(item);
            }
        });
        $.each(itemsToRemove, function () {
            selectedGrid.getStore().remove(this);
        });

        removeDone = true;
    }
    if(isNPMInstalled){
        var removeFromselctedGrid=[];
        $.each(selectedGrid.getStore().data.items, function (index, item) {
            var itemsToRemove = [];
            var interfaceGridId = "InterfaceNestedGrid" + item.data.IdKey;
            var grid = Ext42.getCmp(interfaceGridId);
            if (grid) {
                $("input.itemCb" + item.data.IdKey + ":checked").each(function () {
                    var index = $(this).val();
                    $.each(grid.getStore().data.items, function () {
                        if (this.data.polledItemID == index)
                            itemsToRemove.push(this);
                    });
                });
            }
            $.each(itemsToRemove, function () {

                grid.getStore().remove(this);
            });

            if (grid.getStore().count() <= 0) {
                removeFromselctedGrid.push(item);
            }
            removeDone = true;
        });
        $.each(removeFromselctedGrid, function () {
            selectedGrid.getStore().remove(this);
        });
    }

    if(removeDone)
        RefreshTree($('#groupSelect').val(), $("#searchText").val());
    
    EnableDisableRemoveDiscovery();
    EnableDisableSelectDesect();
    Ext42.resumeLayouts(true);
}

function InitializeCustomToolProperties() {

    var toolID = GetToolIDInternal();
    switch (toolID) {
        case 2:
            toolInterfaceMonitor = true;
            break;
        case 3:
            toolCPUMonitor = true;
            break;
        case 4:
            toolMemoryMonitor = true;
            break;
        case 5:
            toolResponseMonitor = true;
            break;
    }
}

function HideNestedGrid() {
    return toolResponseMonitor;
 }

function GetAvailableTitle(){
    if(isNPMInstalled){
        return 'Available Interfaces';
    }else{
        return 'Orion Nodes';
    }
}

function GetAvailableEmptyMessage(){
    if(isNPMInstalled){
        return 'All interfaces are selected';
    }else{
        return 'All nodes are selected';
    }
}

function GetSubheader(){
    var toolID = GetToolIDInternal();
    switch (toolID) {
        case 2:
            return 'You can only monitor interfaces that you selected in this panel.';
        case 3:
            return 'You can only monitor the selected CPUs in this panel.';
        case 4:
            return 'You can only monitor the selected memory items in this panel.';
        case 5:
            return 'You can only monitor the nodes that you selected from this panel.';
    }
}

function GetEmptyText() {
    var toolID = GetToolIDInternal();
    switch (toolID) {
        case 2:
            return 'No nodes or interfaces have been selected.';
        case 3:
            return 'No nodes or CPUs have been selected.';
        case 4:
            return 'No nodes or memory items have been selected.';
        case 5:
            return 'No nodes have been selected.';
    }
}

function GetTitle() {
    if (IsMobileViewMode()) return null;
    var toolID = GetToolIDInternal();
    switch (toolID) {
        case 2:
            return 'Selected Interfaces';
        case 3:
            return 'Selected CPUs';
        case 4:
            return 'Selected Memory Items';
        case 5:
            return 'Selected Nodes';
    }
}

function GetDiscoveryText() {
    var toolID = GetToolIDInternal();
    switch (toolID) {
        case 2:
            return 'Discover All Interfaces';
        case 3:
            return 'Discover All CPUs';
        case 4:
            return 'Discover All Memory Items';
    }
}

function InitResultHubForCW(taskId) {
    InitResultNotificationClient(taskId, ReceiveResultsForCW);
}

function ReceiveResultsForCW(newResult, taskId) {
    var idKey = "";

    $.each(taskIPMapping, function (index, item) {
        if (item.TaskID == taskId) {
            idKey = item.ID;
            return false;
        }
    });

    var idKey = MakeIpHTMLAvailable(idKey);
    if (idKey == "") {
        return false;
    }

    if (newResult.CompletionInfo.CompletionStatus == 2) {
        $.each(taskIPMapping, function (index, item) {
            if (MakeIpHTMLAvailable(item.ID) == idKey) {
                taskIPMapping.splice(index, 1);
                return false;
            }
        });
    }

    if (toolInterfaceMonitor) {
        InterfaceReceived(newResult, idKey);
    } else if (toolCPUMonitor || toolMemoryMonitor) {
        CpuOrMemoryReceived(newResult, idKey);
    }
}

function SaveAndCloseSpeedDialog() {
    if (SaveChangedSpeed()) {
        CloseSpeedDialog();
    }
}

function SaveChangedSpeed() {
    if (IsSutable()) {
        var rxVal = $("#iterfaceRxSpeed").val();
        var txVal = $("#iterfaceTxSpeed").val();
        var coef = $("#muliplyCoef").val();
        rxVal = rxVal * coef;
        txVal = txVal * coef;
        var discVal = $("#interfaceDiscoveredSpeed").val();
        var idKey = $("#interfaceGridID").val();
        var rowIndex = $("#interfaceRowIndex").val();
        var interfaceGridId = "InterfaceNestedGrid" + idKey;
        var grid=Ext42.getCmp(interfaceGridId);
        var store=grid.getStore();
        var rec = store.getAt(rowIndex);
        rec.set('RxSpeed', rxVal);
        rec.set('TxSpeed', txVal);
        rec.commit();
        return true;
    }
    return false;
}

function ShowTooltip(text, targetObject) {
    var tooltip = $("#errorTooltip");
    tooltip.html(text);
    tooltip.show();
}

function HideTooltip() {
    $("#iterfaceRxSpeed").removeClass("inputError");
    $("#iterfaceTxSpeed").removeClass("inputError");
    var tooltip = $("#errorTooltip");
    tooltip.hide();
}

function IsSutable(){
    var rxVal = $("#iterfaceRxSpeed").val();
    var txVal = $("#iterfaceTxSpeed").val();
    var discVal = $("#interfaceDiscoveredSpeed").val();
    var coef = $("#muliplyCoef").val();
    rxVal = rxVal * coef;
    txVal = txVal * coef;
    if(!IsFloat(rxVal))
    {
        ShowTooltip("Receive Bandwidth is not a number", $("#iterfaceRxSpeed"));
        $("#iterfaceRxSpeed").addClass("inputError");
        return false;
    }

    if(!IsFloat(txVal))
    {
        ShowTooltip("Transmit Bandwidth is not a number", $("#iterfaceTxSpeed"));
        $("#iterfaceTxSpeed").addClass("inputError");
        return false;
    }
    
    if (rxVal % 1 != 0) 
    {
        ShowTooltip("If the receive bandwidth is measured in bps, you can only enter integer values", $("#iterfaceRxSpeed"));
        $("#iterfaceRxSpeed").addClass("inputError");
        return false;
    }

    if (txVal % 1 != 0) 
    {
        ShowTooltip("If the transmit bandwidth is measured in bps, you can only enter integer values", $("#iterfaceTxSpeed"));
        $("#iterfaceTxSpeed").addClass("inputError");
        return false;
    }
    
    if (rxVal < 1 || rxVal > 10000000000000) 
    {
        ShowTooltip("The receive bandwidth should be less than 10 000 Gbps and greater than 1 bps", $("#iterfaceRxSpeed"));
        $("#iterfaceRxSpeed").addClass("inputError");
        return false;
    }
    
    if (txVal < 1 || txVal > 10000000000000)  
    {
        ShowTooltip("The transmit bandwidth should be less than 10 000 Gbps bps and greater than 1 bps", $("#iterfaceTxSpeed"));
        $("#iterfaceTxSpeed").addClass("inputError");
        return false;
    }
    HideTooltip();
    return true;
}

function IsFloat(value){
    var template = /^-?\d*(\.\d+)?$/;
    if (template.test(value) || !$.trim(value)) {
        return true;
    }
    return false;
}

function GetBitsValues(value) {
    if (value == null) return "";

    var precision = 3;
    if (value < 1000.0) {
        return {"coef":1, "val": value.toFixed(precision), "str": "Bps"};
    }

    value /= 1000.0;
    if (value < 1000) {
        return {"coef":1000, "val": value.toFixed(precision), "str": "Kbps"};
    }

    value /= 1000.0;
    if (value < 1000) {
        return {"coef":1000000, "val": value.toFixed(precision), "str": "Mbps"};
    }

    value /= 1000.0;
    if (value < 1000) {
        return {"coef":1000000000, "val": value.toFixed(precision), "str": "Gbps"};
    }
    value /= 1000.0;
    if (value < 1000) {
        return {"coef":1000000000000, "val": value.toFixed(precision), "str": "Tbps"};
    }

    value /= 1000.0;
    if (value < 1000) {
        return {"coef":1000000000000000, "val": value.toFixed(precision), "str": "Pbps"};
    }
}

function ChangeInterfaceSpeed(interfaceName, discoveredSpeed, rxSpeed, txSpeed, rowIndex, idKey)
{
    var maxBitVals = GetBitsValues(discoveredSpeed);
    if( !maxBitVals )
    {
        maxBitVals = {"coef":1, "val": discoveredSpeed, "str": "Bps"};
    }
    $("#interfaceName").text(interfaceName);
    $(".interfaceReportedSpeed").text(maxBitVals.val + maxBitVals.str);
    $(".maxSpeed").text('10 000 Gbps');
    $(".interfaceSpeedSpeedMeasure").text(maxBitVals.str);
    $("#muliplyCoef").val(maxBitVals.coef);
    $("#iterfaceRxSpeed").val(rxSpeed/maxBitVals.coef);
    $("#iterfaceTxSpeed").val(txSpeed/maxBitVals.coef);
    $("#interfaceDiscoveredSpeed").val(discoveredSpeed);
    $("#interfaceGridID").val(idKey);
    $("#interfaceRowIndex").val(rowIndex);
    OpenSpeedDialog();
}

function getInterfaceGridCols(idKey) {
    if(IsMobileViewMode()) {
        return [
            { dataIndex: 'polledItemID', hidden: true },
            {
                dataIndex: 'polledItemName',
                width: 250,
                renderer: function (value, meta, record, rowIndex, colIndex) {

                    var rx = record.get('RxSpeed');
                    var tx = record.get('TxSpeed');
                    if (!rx) {
                        rx = record.raw.RxSpeed;
                    }
                    if (!tx) {
                        tx = record.raw.TxSpeed;
                    }
                    var rxBits = GetBitsValues(rx);
                    var txBits = GetBitsValues(tx);

                    return stringFormat('<div id="mobileNestedGridRow" ><input class="itemCb{0}" type="checkbox" value="{1}" onclick="return CheckLimit(this,false)"/>'
                        + '<span class=interfaceName{0}{1}>{2}</span>'
                        + '<input class="rxSpeed{0}{1}" type="hidden" value="'
                        + '{3}" ></input><input class="txSpeed{0}{1}" type="hidden" value="'
                        + '{4}" ></input></div>'
                        + '<table width="100%" cellspacing="0" cellpadding="0">'
        + '<tr>'
            + '<td style="width: 30%; border: none !important">Rx Speed</td>'
            + '<td style="width: 70%; border: none !important">{7}</td>'
        + '</tr>'
        + '<tr>'
            + '<td style="border: none !important">Tx Speed</td>'
            + '<td style="border: none !important">{8}</td>'
        + '</tr>'
        + '<tr>'
            + '<td style="border: none !important">&nbsp;</td>'
            + '<td style="border: none !important"><a href="javascript:void(0);" onclick="ChangeInterfaceSpeed(\'{2}\', '
                            + '{5}, {3}, {4}, {6}, \'{0}\');" class="link" >Modify</a></td>'
        + '</tr>'
    + '</table>', idKey, record.get('polledItemID'), record.get('polledItemName'),
                        rx, tx, record.raw.DiscoveredSpeed, rowIndex,
                    rxBits.val + rxBits.str, txBits.val + txBits.str);
                }
            }
        ];
    }else {
        return [
            { dataIndex: 'polledItemID', hidden: true },
            {
                id: 'newPolledItemName',
                dataIndex: 'newPolledItemName',
                width: "54%",
                renderer: function(value, meta, record, rowIndex, colIndex) {
                    return stringFormat('<div><input class="itemCb{0}" type="checkbox" value="{1}" onclick="return CheckLimit(this,false)"/><span class=interfaceName{0}{1}>'
                        + '{2}</span><input class="rxSpeed{0}{1}" type="hidden" value="'
                        + '{3}" ></input><input class="txSpeed{0}{1}" type="hidden" value="'
                        + '{4}" ></input></div>',
                        idKey, record.get('polledItemID'), record.raw.newPolledItemName, record.get('RxSpeed'), record.get('TxSpeed'));
                }
            },
            {
                id: 'RxSpeed',
                dataIndex: 'RxSpeed',
                width: "16%",
                renderer: function(value, meta, record, rowIndex, colIndex) {
                    var rxBits = GetBitsValues(record.get('RxSpeed'));
                    return rxBits.val + rxBits.str;
                }
            },
            {
                id: 'TxSpeed',
                dataIndex: 'TxSpeed',
                width: "10%",
                renderer: function(value, meta, record, rowIndex, colIndex) {
                    var rxBits = GetBitsValues(record.get('TxSpeed'));
                    return rxBits.val + rxBits.str;
                }
            },
            { id: 'DiscoveredSpeed', dataIndex: 'DiscoveredSpeed', hidden: true },
            {
                xtype: 'actioncolumn',
                width: "10%",
                renderer: function(value, meta, record, rowIndex, colIndex) {
                    return '<a href="javascript:void(0);" onclick="ChangeInterfaceSpeed(\'' + record.get('polledItemName') + '\', '
                        + record.get('DiscoveredSpeed') + ', ' + record.get('RxSpeed') + ', ' + record.get('TxSpeed') + ', '
                            + rowIndex + ', \'' + idKey + '\');" class="link" >Modify</a>';
                }
            }
        ];
    }

}

function CheckLimit(checkbox, flag) {
    var popuptext;
    var title;
    if (toolCPUMonitor) {
        popuptext = "CPU's";
        title = 'CPU Monitor';
    }
    else if (toolMemoryMonitor) {
        popuptext = "memories";
        title = 'Memory Monitor';
    } else if (toolInterfaceMonitor) {
        popuptext = "interfaces";
        title = 'Interface Monitor';
    }

    if (flag) {
        if ($("input[class*='itemCb']:checked").length >= maxRealTimeMonitorItemsCount) {
            showMessage(String.format('@{R=Toolset.Strings;K=ToolsetGUICODE_SL_02;E=js}', popuptext, maxRealTimeMonitorItemsCount), title);
            return false;
        }
    } else {
        if ($("input[class*='itemCb']:checked").length > maxRealTimeMonitorItemsCount) {
            showMessage(String.format('@{R=Toolset.Strings;K=ToolsetGUICODE_SL_02;E=js}', popuptext, maxRealTimeMonitorItemsCount), title);
            return false;
        }
    }
    return true;
}

function getInterfaceGridFields() {
    if(IsMobileViewMode()) {
        return [
            { name: 'polledItemID' },
            { name: 'polledItemName' }
        ];
        } else {
        return [
            { name: 'polledItemID' },
            { name: 'polledItemName' },
            { name: 'RxSpeed' },
            { name: 'TxSpeed' },
            { name: 'DiscoveredSpeed' }
        ];
    }
}

function RenderNestedGrid(data, idKey) {
    var store = Ext42.create('Ext42.data.Store', {
        fields: getInterfaceGridFields(),
        data: data
    });
    var interfaceGridId = "InterfaceNestedGrid" + idKey;
    var itemsGrid = Ext42.create('Ext42.grid.GridPanel', {
        store: store,
        id: interfaceGridId,
        width: "98%",
        columns: getInterfaceGridCols(idKey),
        hideHeaders: true,
        border: false,
        listeners: {
            //                    cellclick : function(view, cell, cellIndex, record, row, rowIndex, e) {
            //                        var id = record.data.polledItemID;
            //                        if ($("input:checkbox[value='"+id+"']")) {
            //                            $("input:checkbox[value='"+id+"']").attr("checked", !$("input:checkbox[value='"+id+"']").attr("checked"));
            //                        }
            mouseover: {
                element: 'el',
                fn: function (event) {
                    event.stopPropagation();
                }
            },
            mousedown: {
                element: 'el',
                fn: function (event) {
                    event.stopPropagation();
                }
            },
            click: {
                element: 'el',
                fn: function (event) {
                    event.stopPropagation();
                }
            },
            mouseup: {
                element: 'el',
                fn: function (event) {
                    event.stopPropagation();
                }
            },
            dblclick: {
                element: 'el',
                fn: function (event) {
                    event.stopPropagation();
                }
            }
        }
    });
    var renderPlace = "NestedGridRow" + idKey;
    $("#" + renderPlace).empty();
    $("#" + renderPlace).show();
    itemsGrid.render(renderPlace);
}

function InterfaceReceived(newResult, idKey)
{
    if (newResult.CompletionInfo.CompletionStatus == 2) 
    {
        if (newResult.Interfaces[0]) {
            var data = [];

            var ip = $('input[idkey="' + idKey + '"]').attr('ip');

            var ifnames = [], ips = [];
            for (var i = 0; i < newResult.Interfaces.length; i++) {
                ifnames.push(newResult.Interfaces[i].InterfaceIndex);
                ips.push(ip);
            }

            var result = [];
            if (isNPMInstalledForPreload) {
                result = GetJSONDataNoAlertWhenFails("/Orion/Toolset/Services/Toolset.asmx/GetInterfaceNetObjectIdsByIndex", "POST", JSON.stringify({ ids: ifnames, ipaddress: ips }));
            }

            for (i = 0; i < newResult.Interfaces.length; i++) {
                newResult.Interfaces[i].newPolledItemName = result[i] ? result[i] : newResult.Interfaces[i].DeviceName;
            }

            $.each(newResult.Interfaces, function (index, item) {
                data.push({ "polledItemID": item.InterfaceIndex, "polledItemName": item.DeviceName, "newPolledItemName": item.newPolledItemName,
                    "RxSpeed": item.SpeedAtDiscovery, "TxSpeed": item.SpeedAtDiscovery, "DiscoveredSpeed": item.SpeedAtDiscovery });
            });

            RenderNestedGrid(data, idKey);
             var renderPlace = "#NestedStatusRow" + idKey;
             $(renderPlace).hide();
        }
        else {
            var renderPlace = "#NestedStatusRow" + idKey;
            $(renderPlace).show();
            $(renderPlace).html(newResult.CustomMessage);
        }
    }
    else {
        if(newResult.CustomMessage)
        {
            var renderPlace = "#NestedStatusRow" + idKey;
            $(renderPlace).show();
            $(renderPlace).html('<image class="mobileDiscoveryProgress" style="margin-top:4px;" src="/Orion/Toolset/images/animated_loading_16x16.gif"/><span style="margin-left: 5px; font-family: Arial, Helvetica, Sans-Serif;">' + newResult.CustomMessage + '</span>' +
            '<a class="link" style="margin-left:5px; font-family: Arial, Helvetica, Sans-Serif;" href="#" onclick="CancelDiscovery(\'' + idKey + '\')" >Cancel Discovery<a>');
        }
    }
}

function CpuOrMemoryReceived(newResult, idKey)
{
     if (newResult.CompletionInfo.CompletionStatus == 2) {
        var resultItems = [];
        if (toolCPUMonitor)
            resultItems = newResult.CpuDetails;
        else if (toolMemoryMonitor)
            resultItems = newResult.MemoryDetails;

        if (resultItems[0]) {
            var data = [];
            $.each(resultItems, function (index, item) {
                data.push({ "polledItemID": item.ID, "polledItemName": item.DisplayName });
            });

            var store = Ext42.create('Ext42.data.Store', {
                fields: [
        		    { name: 'polledItemID' },
        		    { name: 'polledItemName' }
        	    ],
                data: data
            });

            var itemsGrid = Ext42.create('Ext42.grid.GridPanel', {
                store: store,
                width: "98%",
                columns: [
        			    { dataIndex: 'polledItemID', hidden: true },
        			    { dataIndex: 'polledItemName', width: "100%", renderer: function (value, meta, record, rowIndex, colIndex) {
        			        return '<div  id="mobileNestedGridRow" ><input class="itemCb' + idKey + '" type="checkbox" value="' + record.get('polledItemName') + '" onclick="return CheckLimit(this,false)"/><span>'
                            + record.get('polledItemName') + '</span></div>';
        			    }
        			    }],
                hideHeaders: true,
                border: false
            });
            var renderPlace = "NestedGridRow" + idKey;
            $("#" + renderPlace).empty();
            $("#" + renderPlace).show();
            itemsGrid.render(renderPlace);

            renderPlace = "#NestedStatusRow" + idKey;
            $(renderPlace).hide();
        }
        else {
            var renderPlace = "#NestedStatusRow" + idKey;
            $(renderPlace).show();
            $(renderPlace).html(newResult.CustomMessage);
        }   
    }
    else {
        var renderPlace = "#NestedStatusRow" + idKey;
        $(renderPlace).show();
        $(renderPlace).html('<image class="mobileDiscoveryProgress" style="margin-top:4px;" src="/Orion/Toolset/images/animated_loading_16x16.gif"/><span style="margin-left: 5px; font-family: Arial, Helvetica, Sans-Serif; ">' + newResult.CustomMessage + '</span>' +
        '<a class="link" style="margin-left:5px; font-family: Arial, Helvetica, Sans-Serif;" href="#" onclick="CancelDiscovery(\'' + idKey + '\')" >Cancel Discovery<a>');
    }
}

function BuildRowBodyTpl(idKey, ipAddress, nodeId, credId) {
    return '<div style="float:fight; width:100%; clear:both; display:none;font-family:Arial, Helvetica, Sans-Serif !important;" id="NestedGridRow' + idKey + '"></div>' +
    '<div style="float:fight; width:100%; clear:both;"><div style="display:none;font-family:Arial, Helvetica, Sans-Serif !important;" id="NestedStatusRow' + idKey + '"></div></div>';
}

function CancelDiscovery(idKey) {
    $.each(taskIPMapping, function (index, item) {
        if (MakeIpHTMLAvailable(item.ID) == idKey) {
            taskIPMapping.splice(index, 1);
            return false;
        }
    });
    var renderPlace = "#NestedStatusRow" + idKey;
    $(renderPlace).html('<span>Discovery process was cancelled.</span><a class="link" style="margin-left:5px;" href="#" onclick="Rediscover(\'' + idKey + '\')" >Rediscover<a>');
}

function MakeIpHTMLAvailable(ID) {
    var val = ID.replace(/-/g, '').replace(/{/g, '').replace(/}/g, '');
    return val;
}

function toggleCheckbox(checkbox, key) {
    var retValue = checkBoxFunc("NestedGridRow" + key, key, checkbox.checked);
    if (retValue == false && toolResponseMonitor) {
        checkbox.checked = false;
    }
    EnableDisableRemoveDiscovery();
    if (toolResponseMonitor)
        return retValue;
    else
        return true;
}

function EnableDisableRemoveDiscovery() {
    var discoverBtn = Ext42.getCmp("discoverBtn");
    var removeBtn = Ext42.getCmp("RemoveNode");
    if($("input.parentIpCb:checked").length > 0) {
        if(removeBtn)
            removeBtn.enable();
        if (discoverBtn && IsSessionRegistered() && IsSignalRConnected())
            discoverBtn.enable();
    }else
    {
        if(removeBtn)
            removeBtn.disable();
        if(discoverBtn)
            discoverBtn.disable();
    }
}

function EnableDisableSelectDesect() {

    var slctBtn = Ext42.getCmp("selectBtn");
    var dslctBtn = Ext42.getCmp("deselectBtn");
    if ($("input.parentIpCb").length > 0) {
        if (slctBtn) slctBtn.enable();
        if (dslctBtn) dslctBtn.enable();
    } else {
        if (slctBtn) slctBtn.disable();
        if (dslctBtn) dslctBtn.disable();
    }
}

function checkBoxFunc(parentID, classStr, checked) {
    var newRegExpr = new RegExp(classStr + " " + "|" + classStr + "$", "g");
    $("#" + parentID + " input[type=checkbox]").each(function () {
        if (reachedFlag && checked==true) return;
        var currClass = $(this).attr("class");
        if (currClass.match(newRegExpr)) {
            if (checked) {
                if (CheckLimit(this,true)) {
                    $(this).prop("checked", checked);
                    reachedFlag = false;
                } else {
                    reachedFlag = true;
                }
            } else {
                $(this).prop("checked", checked);
                reachedFlag = false;
            }
        }
    });
    if (toolResponseMonitor && checked) {
        if ($("input[class*='parentIpCb']:checked").length > maxRealTimeMonitorItemsCount) {
            showMessage(String.format('@{R=Toolset.Strings;K=ToolsetGUICODE_SL_02;E=js}', 'nodes', maxRealTimeMonitorItemsCount), 'Response Time Monitor');
            return false;
        } else {
            return true;
        }
    } 
}

function SelectDesectAll(checked) {
    Ext42.suspendLayouts();
    $("input.parentIpCb").each(function () {
            $(this).prop("checked", checked);
            $(this).trigger("onclick");
    });
    EnableDisableRemoveDiscovery();
    Ext42.resumeLayouts(true);
}

function RemoveSelected() {
    if (!CheckSessionAndRefresh()) {
        ShowDisconnectionAlert();
        document.location.reload(true);
    }
    var items = $("input.parentIpCb:checked");
    if (items.length > 0) {
        var message = "Are you sure you want to remove the " + items.length + " selected " + (items.length > 1 ? "items?" : "item?");
        Ext42.MessageBox.confirm({
            title: 'Remove Selected Items',
            width: 250,
            msg: message,
            buttons: Ext42.MessageBox.OKCANCEL,
            fn: RemoveSelectedApproved,
            icon: Ext42.MessageBox.WARNING
        });
    }
}

function RemoveSelectedApproved(btn) {
    if (btn == "ok") {
        Ext42.suspendLayouts();
        var gridItems = selectedGrid.getStore().data.items;
        var refresh = false;
        $("input.parentIpCb:checked").each(function (index, item) {
            refresh = true;
            $(gridItems).each(function (gridIndex, gridItem) {
                if (gridItem.data.ID == $(item).val()) {
                    selectedGrid.getStore().remove(gridItem);
                }
            });
        });
        if (refresh)
            RefreshTree($('#groupSelect').val(), $("#searchText").val());
        Ext42.resumeLayouts(true);
    }

    EnableDisableRemoveDiscovery();
    EnableDisableSelectDesect();
}

function MonitorAll() {
    try {
        if ($("#RunButton").attr("disabled") == "disabled") {
            return false;
        }
        if ($('#isSessionTerminated').val()=='true') {
            document.location.reload(true);
        } else {
            if (!CheckSessionAndRefresh()) {
                ShowDisconnectionAlert();
            }
            StartMonitoring();
        }
    } catch(e) {}
    return false;
}

function StartMonitoring() {
    var itemsToPoll;
    if (toolCPUMonitor) {
        itemsToPoll = MonitorCpuAndMemory("/Orion/Toolset/Services/ConfigurationWizard.asmx/PollCpus");
        if (itemsToPoll.length > 0) initializeChartView(itemsToPoll, GetResultColumns("/Orion/Toolset/Services/CpuMonitorSettings.asmx/GetResultColumns"));
    } else if (toolResponseMonitor) {
        itemsToPoll = MonitorNodes();
        if (itemsToPoll.length > 0) initializeChartView(itemsToPoll, GetResultColumns("/Orion/Toolset/Services/ResponseTimeMonitorSettings.asmx/GetResultColumns"));
        setTimeout(function () {
            if ((typeof resultCounter != 'undefined') && (resultCounter == 0)) {
                $("#InnerText").html("<div>Response delayed? See the Solarwinds <a href='http://knowledgebase.solarwinds.com/kb/questions/5604/Error%3A+%22Polling+has+stopped.+Please+close+this+tool%2C+and+contact+your+server+Administrator+for+assistance.%22' target='_blank' class='KBlinks'> Knowledge Base.</a></div>");
            }
        }, 60000);
    } else if (toolMemoryMonitor) {
        itemsToPoll = MonitorCpuAndMemory("/Orion/Toolset/Services/ConfigurationWizard.asmx/PollMemories");
        if (itemsToPoll.length > 0) initializeChartView(itemsToPoll, GetResultColumns("/Orion/Toolset/Services/MemoryMonitorSettings.asmx/GetResultColumns"));
    } else if (toolInterfaceMonitor) {
        itemsToPoll = MonitorInterfaces();
        if (itemsToPoll.length > 0) initializeChartView(itemsToPoll, GetResultColumns("/Orion/Toolset/Services/InterfaceMonitorSettings.asmx/GetResultColumns"));
    }
}

function MonitorInterfaces() {
    var itemsToPoll = [];
    var count = 0;
    $("input.parentIpCb").each(function (index, ip) {
        var items = [];
        var interfaceNames = [];
        $("input.itemCb" + MakeIpHTMLAvailable($(ip).val()) + ":checked").each(function () {
            var index = $(this).val();
            var rx = $("input.rxSpeed" + MakeIpHTMLAvailable($(ip).val()) + index+":first").val();
            var tx = $("input.txSpeed" + MakeIpHTMLAvailable($(ip).val()) + index+":first").val();
            items.push({Index:index, RxSpeed:rx, TxSpeed:tx});
            
            var interfaceName = $("span.interfaceName" + MakeIpHTMLAvailable($(ip).val()) + index+":first").text();
            interfaceNames.push(interfaceName);
            count++;
        });
        if (items.length > 0) {
            var gridItems = selectedGrid.getStore().data.items;
            $.each(gridItems, function (index, item) {
                if (item.data.ID == $(ip).val()) {
                    itemsToPoll.push({ IP: item.data.IpAddress, NodeId: item.data.NodeId, CredentialId: item.data.CredentialId, Interfaces: items, Items: interfaceNames });
                }
            });
        }
    });
    if (count > 0) {
        var jsonData = JSON.stringify({ pollingEngine: GetSelectedEngine(), itemsToPoll: itemsToPoll });
        var taskId = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/PollInterfaces", "POST", jsonData);
        backToMainPage(taskId, count, itemsToPoll);
    }
    else {
        ShowNoSelectedMessage()
    }
    return itemsToPoll;
}

function MonitorNodes() {
    var itemsToPoll = [];
    var count = 0;
    $("input.parentIpCb:checked").each(function (index, ip) {
        var gridItems = selectedGrid.getStore().data.items;
        $.each(gridItems, function (index, item) {
            if (item.data.ID == $(ip).val()) {
                var items = [];
                items.push(item.data.DisplayName);
                itemsToPoll.push({ IP: item.data.IpAddress, NodeId: item.data.NodeId, CredentialId: item.data.CredentialId, Items: items });
                count++;
            }
        });
    });
    if (count > 0) {
        var jsonData = JSON.stringify({ pollingEngine: GetSelectedEngine(), itemsToPoll: itemsToPoll });
        var taskId = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/PollNodes", "POST", jsonData);
        backToMainPage(taskId, count, itemsToPoll);
    }
    else {
        ShowNoSelectedMessage()
    }

    return itemsToPoll;
}

function MonitorCpuAndMemory(path) {
    var itemsToPoll = [];
    var count = 0;
    $("input.parentIpCb").each(function (index, ip) {
        var items = [];
        $("input.itemCb" + MakeIpHTMLAvailable($(ip).val()) + ":checked").each(function () {
            items.push($(this).val());
            count++;
        });
        if (items.length > 0) {
            var gridItems = selectedGrid.getStore().data.items;
            $.each(gridItems, function (index, item) {
                if (item.data.ID == $(ip).val()) {
                    itemsToPoll.push({ IP: item.data.IpAddress, NodeId: item.data.NodeId, CredentialId: item.data.CredentialId, Items: items });
                }
            });
        }
    });
    if (count > 0) {
        var jsonData = JSON.stringify({ pollingEngine: GetSelectedEngine(), itemsToPoll: itemsToPoll });
        var taskId = GetJSONData(path, "POST", jsonData);
        backToMainPage(taskId, count, itemsToPoll);
    } else {
        ShowNoSelectedMessage();
    }
    return itemsToPoll;
}

function ShowNoSelectedMessage() {
    var message = 'Please select at least one Interface to monitor.';
    var title='Select Interface';
    if (toolCPUMonitor) {
        message = 'Please select at least one CPU item to monitor.';
        title = 'Select CPU items';
    } else if (toolMemoryMonitor) {
        message = 'Please select at least one Memory item to monitor.';
        title = 'Select Memory items';
    } else if (toolResponseMonitor) {
        message = 'Please select at least one Node to monitor.';
        title = 'Select Nodes';
    }
   
    Ext42.MessageBox.alert({
        title: title,
        width: 250,
        msg: message,
        buttons: Ext42.MessageBox.OK,
        icon: Ext42.MessageBox.INFO
    });
}

function Cancel() {
    if(isInApplyMode()){
        var message = 'Are you sure you want to leave this page without saving? All changes will be lost.';
        Ext42.MessageBox.confirm({
            title: 'Confirmation',
            width: 250,
            msg: message,
            buttons: Ext42.MessageBox.OKCANCEL,
            fn: CancelApproved,
            icon: Ext42.MessageBox.WARNING
        });
    }
    else {
        backToMainPage("", 0);
    }
}

function CancelApproved(btn){
    if(btn == "ok"){
        backToMainPage("", 0);
    }
}

function Rediscover(idKey) {
    var itemsToDiscover = [];
    $.each(selectedGrid.getStore().data.items, function (index, item) {
        if (item.data.IdKey == idKey) {
            itemsToDiscover.push({ ID: item.data.ID, IP: item.data.IpAddress, NodeId: item.data.NodeId, CredentialId: item.data.CredentialId });
        }
    });

    DiscoverInternal(itemsToDiscover);
}

function DiscoverAll() {
    var ips = [];
    var itemsToDiscover = [];
    var itemsToExpand = [];
    taskIPMapping.splice(0, taskIPMapping.length);

    $("input.parentIpCb:checked").each(function () {
        ips.push($(this).val());
    });

    $.each(selectedGrid.getStore().data.items, function (index, item) {
        if ($.inArray(item.data.ID, ips) > -1) {
            if (!isDiscoveryRunning(item.data.ID)) {
                itemsToDiscover.push({ ID: item.data.ID, IP: item.data.IpAddress, NodeId: item.data.NodeId, CredentialId: item.data.CredentialId });
                itemsToExpand.push(index);
            }
        }
    });

    ExpandNestedGrid(itemsToExpand);

    DiscoverInternal(itemsToDiscover);
    setTimeout(function () {
        if ((typeof resultCounter != 'undefined') && (resultCounter == 0)) {
            $("#InnerText").html("<div>Response delayed? See the Solarwinds <a href='http://knowledgebase.solarwinds.com/kb/questions/5604/Error%3A+%22Polling+has+stopped.+Please+close+this+tool%2C+and+contact+your+server+Administrator+for+assistance.%22' target='_blank' class='KBlinks'> Knowledge Base.</a></div>");
        }
    }, 60000);
}

function ExpandNestedGrid(itemsToExpand){
    $.each(itemsToExpand, function (index, item) {
        if (IsMobileViewMode()) {
            $.each($(".expanderImageRow" + item), function (imgindex, img) {
                toggleMobileImageChange(img, true);
            });
        }
        toggleExpanderRow(item, true);
    });
}

function DiscoverInternal(itemsToDiscover) {
    if (!CheckSessionAndRefresh()) {
        ShowDisconnectionAlert();
        document.location.reload(true);
    }
    if (toolCPUMonitor) {
        DiscoverCpuOrMemory("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableCpus", itemsToDiscover);
    } else if (toolInterfaceMonitor) {
        DiscoverCpuOrMemory("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableInterfaces", itemsToDiscover);
    } else if (toolMemoryMonitor) {
        DiscoverCpuOrMemory("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableMemoryItems", itemsToDiscover);
    } 
}

function isDiscoveryRunning(id) {
    var running = false;
    $.each(taskIPMapping, function (index, value) {
        if (value.ID == id) {
            running = true;
            return false;
        }
    });
    return running
 }

function DiscoverCpuOrMemory(path, itemsToDiscover) {
    if (itemsToDiscover.length > 0) {
        var jsonData = JSON.stringify({ pollingEngine: GetSelectedEngine(), items: itemsToDiscover });
        var taskIds = GetJSONData(path, "POST", jsonData);
        if (taskIds) {
            $.each(taskIds, function (index, item) {
                if (item.TaskID != "") {
                    taskIPMapping.push(item);
                    InitResultHubForCW(item.TaskID);
                }
                else {
                    var renderPlace = "#NestedStatusRow" + MakeIpHTMLAvailable(item.ID);
                    $(renderPlace).show();
                    $(renderPlace).html(item.CustomMessage);
                }
            });

            if (IsDemoView()) {
                jsonData = JSON.stringify({ tasksToRun: taskIds });
                GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/RunDemoTasks", "POST", jsonData);
            }
        }
    }
}

function toggleExpanderRow(rowIdx, expandOnly) {
    var isExpand = false;
    var rowNode = selectedGrid.getView().getNode(rowIdx);
    var row = Ext42.fly(rowNode);
    var expander = selectedGrid.plugins[0];
    var innerEl = row.down(expander.rowBodyTrSelector);
    var isCollapsed = row.hasCls(expander.rowCollapsedCls);
    if (isCollapsed) {
        row.removeCls(expander.rowCollapsedCls);
        innerEl.removeCls(expander.rowBodyHiddenCls);
        isExpand = true;
    }
    else {
        if (!expandOnly) {
            row.addCls(expander.rowCollapsedCls);
            innerEl.addCls(expander.rowBodyHiddenCls);
        }
    }

    return isExpand;
}

function startDiscoveryOnExpand (rowIdx) {
        var rowNode = selectedGrid.getStore().getAt(rowIdx);
        if (toggleExpanderRow(rowIdx, false) && !isDiscoveryRunning(rowNode.data.ID)) {
            DiscoverInternal([{ ID: rowNode.data.ID, IP: rowNode.data.IpAddress, NodeId: rowNode.data.NodeId, CredentialId: rowNode.data.CredentialId}]);
        }
    }

function getToolbarButtons(plugins, toolbarButtons) {
    var ipExpander = { ptype: "rowexpander", id: "ipExpander", border: false, expandOnDblClick: false, rowBodyTpl: [BuildRowBodyTpl('{IdKey}', '{IpAddress}', '{NodeId}', '{CredentialId}')]
    , toggleRow: startDiscoveryOnExpand
    };

     if (!HideNestedGrid()) {
            plugins.push(ipExpander);
     }

     if(IsMobileViewMode()) {
         toolbarButtons.push({ text: '', iconCls: '', xtype    : 'textfield' });
         return;
     }

    toolbarButtons.push({ text: 'Add Non-Orion Device', iconCls: 'addCls', handler: function () { OpenAddDeviceAccount(); } });
    toolbarButtons.push("-");
    if (!HideNestedGrid()) {
        toolbarButtons.push({id: "discoverBtn", disabled : true, text: GetDiscoveryText(), iconCls: 'discoverCls', hidden: HideNestedGrid(), handler: function () { DiscoverAll(); } });
        toolbarButtons.push("-");
    }
    toolbarButtons.push({id: "selectBtn", disabled : true, text: 'Select All', iconCls: 'selectCls', handler: function () { SelectDesectAll(true); } });
    toolbarButtons.push("-");
    toolbarButtons.push({id: "deselectBtn", disabled : true, text: 'Deselect All', iconCls: 'deselectCls', handler: function () { SelectDesectAll(false); } });
    toolbarButtons.push("-");
    toolbarButtons.push({id: "RemoveNode", disabled : true, text: 'Remove Selected', iconCls: 'removeCls', handler: function () { RemoveSelected(); } });

}

function buildMobileExpandImage(rowIndex) {
    return '<div class="expanderImage expanderImageRow' + rowIndex + '" onmouseup="toggleMobileExpand(this, ' + rowIndex + ');"></div>';
}

function toggleMobileExpand(img, rowIndex) {
    toggleMobileImageChange(img, false);
    startDiscoveryOnExpand(rowIndex);
}

function toggleMobileImageChange(img, expandOnly) {
    if ($(img).hasClass( "expanderImageMinus")) {
        if (!expandOnly) {
            $(img).removeClass("expanderImageMinus");
        }
    } else {
        $(img).addClass("expanderImageMinus");
    }
}

function BuildSelectedTree() {
    currentSelectedData = [];
    var store = Ext42.create('Ext42.data.Store', {
        autoDestroy: true,
        fields: [
				{ name: 'ID' },
                { name: 'IdKey' },
                { name: 'DisplayName' },
                { name: 'IpAddress' },
				{ name: 'NodeId', type: 'int' },
                { name: 'CredentialId', type: 'int' }
			],
        data: currentSelectedData
    });

    var plugins = [];
    var gridTitle = IsMobileViewMode() ? null : GetTitle();
    var cols = [];
    if (toolInterfaceMonitor) {
        if (IsMobileViewMode()) {
            cols = [{
                header: 'Interface',
                dataIndex: 'IpAddress',
                width: "100%",
                sortable: false,
                renderer: function (value, meta, record, rowIndex, colIndex) {
                    if (!record.raw.newDisplayName) {
                        record.raw.newDisplayName = getRenderedNodeName(record.raw);
                    }
                    return '<div>' + buildMobileExpandImage(rowIndex) + '<input class="parentIpCb" type="checkbox" ip="' + record.raw.IpAddress + '" idkey="' + record.get('IdKey') + '" value="'
                        + record.get('ID') + '" onclick="return toggleCheckbox(this, \'' + record.get('IdKey') + '\')"/><span class="cwSelectedNode">'
                            + record.raw.newDisplayName + '</span></div>';
                },
                hideable: false
            }];
        } else {
        cols = [{
            header: 'Interface',
            dataIndex: 'IpAddress',
            width: "50%",
            sortable: false,
            renderer: function (value, meta, record, rowIndex, colIndex) {
                if (!record.raw.newDisplayName) {
                    record.raw.newDisplayName = getRenderedNodeName(record.raw);
                }
                return '<div><input class="parentIpCb" type="checkbox" ip="' + record.raw.IpAddress + '" idkey="' + record.get('IdKey') + '" value="'
                    + record.get('ID') + '" onclick="return toggleCheckbox(this, \'' + record.get('IdKey') + '\')"/><span class="cwSelectedNode">'
                        + record.raw.newDisplayName + '</span></div>';
            },
            hideable: false,
            cls: 'headerstyle'
        }, { header: 'Rx Speed', width: "15%", sortable: false, hideable: false }, { header: 'Tx Speed', width: "30%", sortable: false, hideable: false, resizable: false}];
        }
        ;
    } else {
    cols = [{ header: 'Item', dataIndex: 'IpAddress', width: "97%", sortable: false,
        renderer: function (value, meta, record, rowIndex, colIndex) {
            if (!record.raw.newDisplayName) {
                record.raw.newDisplayName = getRenderedNodeName(record.raw);
            }
            var imageHtml = "";
            if (IsMobileViewMode()) {
                imageHtml = buildMobileExpandImage(rowIndex);
            }
            return '<div>' + imageHtml + '<input class="parentIpCb" type="checkbox" ip="' + record.raw.IpAddress + '" idkey="' + record.get('IdKey') + '" value="'
                    + record.get('ID') + '" onclick="return toggleCheckbox(this, \'' + record.get('IdKey') + '\')"/><span class="cwSelectedNode">'
                        + record.raw.newDisplayName + '</span></div>';
        }
    }];
    }
    selectedGrid = Ext42.create('Ext42.grid.GridPanel', {
        title: gridTitle,
        store: store,
        columns: cols,
        height: isLowResoultionMobile() ? 330 : 500,
        width: getGridWidth(),
        hideHeaders: !toolInterfaceMonitor | IsMobileViewMode(),
        border: false,
        scrollOffset: 0,
        forceFit: false,
        autoscroll: false,
        layout: 'fit',
        frame: !IsMobileViewMode(),
        emptyText: GetEmptyText(),
        viewConfig: {
            deferEmptyText: false,
            stripeRows: false,
            loadMask: false
        },
        plugins: plugins,
        dockedItems: getGridToolbars(plugins)
    });

    selectedGrid.on('columnresize', function () {
        //FB377909 configuratioan wizard breaks in IE8
        if (toolInterfaceMonitor)
        {
            var totalWidth = parseFloat($("#GridSelectedPlace").css("width"));
            if (arSelectedColumnWidth.length === 0) {
                for (var lngColIndex = 0; lngColIndex < selectedGrid.columns.length; lngColIndex++) {
                    var temp = selectedGrid.columns[lngColIndex].width.toString();
                    if (temp.indexOf('%') == -1) {
                        temp = temp - temp * 20 / 100;
                    } else {
                        temp = (totalWidth * parseInt(temp)) / 100;
                        temp = temp - ((parseInt(temp) * 20) / 100);
                    }
                    arSelectedColumnWidth[lngColIndex] = temp;
                    temp = 0;
                }
            }

            var colLengths = [];
            colLengths[0] = selectedGrid.columns[0].width.toString().indexOf('%') == -1 ? selectedGrid.columns[0].width : totalWidth * parseInt(selectedGrid.columns[0].width) / 100;
            colLengths[1] = selectedGrid.columns[1].width.toString().indexOf('%') == -1 ? selectedGrid.columns[1].width : totalWidth * parseInt(selectedGrid.columns[1].width) / 100;
            colLengths[2] = selectedGrid.columns[2].width.toString().indexOf('%') == -1 ? selectedGrid.columns[2].width : totalWidth * parseInt(selectedGrid.columns[2].width) / 100;

            for (var j = 0; j < selectedGrid.columns.length; j++) {
                if (colLengths[j] < arSelectedColumnWidth[j]) {
                    selectedGrid.columns[j].setWidth(arSelectedColumnWidth[j]);
                    $("[class^=x42-grid-cell-headerId-newPolledItemName]").width(arSelectedColumnWidth[0]);
                    $("[class^=x42-grid-cell-headerId-RxSpeed]").width(arSelectedColumnWidth[1]);
                    $("[class^=x42-grid-cell-headerId-TxSpeed]").width(arSelectedColumnWidth[2]);
                    return;
                }
            }

            $("[class^=x42-grid-cell-headerId-newPolledItemName]").width(selectedGrid.columns[0].width);
            $("[class^=x42-grid-cell-headerId-RxSpeed]").width(selectedGrid.columns[1].width);
            $("[class^=x42-grid-cell-headerId-TxSpeed]").width(selectedGrid.columns[2].width);
        }
    });

    selectedGrid.render('GridSelectedPlace');
    
    Ext42.EventManager.onWindowResize(selectedGrid.doLayout,selectedGrid);
    if(IsMobileViewMode()) {
        RenderSelectDeselectSelectedDevices($("#GridSelectedPlace tbody:first > tr:nth-child(1) td:nth-child(2)"), selectedGrid);
        $("#GridSelectedPlace .x42-field-toolbar tbody").css("width", "auto");
        $("#GridSelectedPlace .x42-field-toolbar").css("width", "auto");
    }
}

var cachedRenderedNodes = null;
var cachedRenderedNodesCleaner = null;
//returns Node names with statuses as HTML fragments
//requires a list of IP addresses
//calls GetNodeNetObjectIds web service if the requested data is not already cached
function getNodeNames(list) {
    if (cachedRenderedNodesCleaner == null) {
        cachedRenderedNodesCleaner = setInterval(function () {
            cachedRenderedNodes = null;
        }, GetCacheInvalidationInterval());
    }

    var result = [];

    var fillErrorResult = function (addresses) {
        var newData = [];
        for (var i = 0; i < addresses.length; i++) {
            newData.push({ IP: addresses[i], HTML: addresses[i] });
        }
        return newData;
    };

    var callWebService = function (addresses) {
        var newData = [];
        $.ajax({
            type: "POST",
            url: "/Orion/Toolset/Services/Toolset.asmx/GetNodeNetObjectIds",
            data: JSON.stringify({ ipaddress: addresses }),
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                for (var i = 0; i < addresses.length; i++) {
                    newData.push({ IP: addresses[i], HTML: msg.d[i] });
                }
            },
            error: function (msg) {
                newData = fillErrorResult(addresses);
            }
        });
        return newData;
    };

    var getDataFromCache = function (ipList, cache, resultList, newItemsList) {
        for (var i = 0; i < ipList.length; i++) {
            var found = false;
            for (var j = 0; j < cache.length; j++) {
                if (ipList[i] == cache[j].IP) {
                    resultList.push(cache[j]);
                    found = true;
                    break;
                }
            }
            if (found == false) {
                newItemsList.push(ipList[i]);
            }
        }
        return newItemsList.length == 0;
    };

    if (list.length != 0 && cachedRenderedNodes == null) {
        cachedRenderedNodes = callWebService(list);
        result = cachedRenderedNodes;
    }
    else if (list.length != 0 && cachedRenderedNodes != null) {
        var newItems = [];

        var success = getDataFromCache(list, cachedRenderedNodes, result, newItems);
        if (!success) {
            var newCache = callWebService(newItems);
            cachedRenderedNodes = newCache.concat(cachedRenderedNodes);
            result = [];
            newItems = [];
            success = getDataFromCache(list, cachedRenderedNodes, result, newItems);
            if (!success) {
                result = fillErrorResult(list);
            }
        }
    }
    return result;
}

function getRenderedNodeName(data) {
    if (!IsDemoView()) {
        var name = getNodeNames([data.IpAddress]);
        return name[0].HTML;
    } else {
        var cssClassName = data.StyleClass.split(' {');
        cssClassName = cssClassName[0].replace(".", "");
        return "<div style='width:16px; height:16px;' class='" + cssClassName + "' ></div><span>" + data.DisplayName + "</span>";
    }
}

function RenderSelectDeselectSelectedDevices(holder, tree) {
    var id = 'selectDesectAvailableDevicesChb';
    RenderSelectDeselect(id, holder);
    
    $('#' + id).on('change', function () { 
        var ischecked = $('#' + id).is(':checked');
        SelectDesectAll(ischecked); 
    });
}

function getGridToolbars(plugins) {
    var toolbarButtons = [];
    
    getToolbarButtons(plugins, toolbarButtons);
    
    if(IsMobileViewMode()) {
       return [
        {
            xtype: 'toolbar',
            items: toolbarButtons
        }];   
    }
    return [
        {
            xtype: 'toolbar',
            items: {
                xtype: 'label',
                text: GetSubheader(),
                cls: 'subheader'
            }
        },
        {
            xtype: 'toolbar',
            items: toolbarButtons
        }];   
}

function getGridWidth() {
    if(IsMobileViewMode()) {
         return parseFloat($("#GridSelectedPlace").css("width"));
    }
}

//Engine and Credentials
function GetSelectedEngine() {
    return $("#pollingEnginesSelect option:selected").text().replace(" (Primary)","");
}

function GetCredentilasID() {
    return $("#credenrialsSelect").val();
}

function RenderEnginesCombo() {
    var engines = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableEngines", "GET", {});
    $.each(engines, function (index, item) { $('#pollingEnginesSelect').append($("<option></option>").attr("value", item.ID).text(item.DisplayName)); });
}
function RenderCredentilasCombo() {
    var creds = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableCredentials", "GET", {});
    $.each(creds, function (index, item) { $('#credenrialsSelect').append($("<option></option>").attr("value", item.ID).text(item.Name)); });
}
function RefreshCredentilas() {
    if (!CheckSessionAndRefresh()) {
        ShowDisconnectionAlert();
        document.location.reload(true);
    }
    var creds = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableCredentials", "GET", {});
    $("#credenrialsSelect").empty();
    $.each(creds, function (index, item) { $('#credenrialsSelect').append($("<option></option>").attr("value", item.ID).text(item.Name)); });
}

function AddIconClasses(treeData) {
    var styles;
    var cssClassName;
    $.each(treeData, function (index, item) {
        if (item.hasOwnProperty("children") && item.children) {
            $.each(item.children, function (Index, item) {
                cssClassName = item.StyleClass.split(' {');
                styles = $("head").find("style").html();
                if (styles === null || styles.indexOf(cssClassName[0]) == -1) {
                    $("<style type='text/css'>" + item.StyleClass + "</style>").appendTo("head");
                }
            });
        }
        cssClassName = item.StyleClass.split(' {');
        styles = $("head").find("style").html();
        if (styles === null || styles.indexOf(cssClassName[0]) == -1) {
            $("<style type='text/css'>" + item.StyleClass + "</style>").appendTo("head");
        }
    });
}

function BuildGroupedTree(groupValue, ismobile) {
    var groups = [];
    var treeData = [];
    groups = GetJSONData("/Orion/Services/AddRemoveObjects.asmx/GetGroupByProperties", "POST", JSON.stringify({ entityType: "Orion.Nodes" }));
    if (isNPMInstalled) {
        treeData = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetNPMInterfaces", "POST", jsonData = JSON.stringify({ GroupColumn: groupValue, searchValue: "", selectedInterfaces: GetSelectedInterfacesIDs() }));
    }else{
        treeData = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableNodes", "POST", jsonData = JSON.stringify({ GroupColumn: groupValue, searchValue: "", selectedNodes: GetSelectedNodeIDs() }));
    }
    if(ismobile){
        tree = RenderNodesTree(treeData, "availableNodesMobile", true);
        RenderGroupsCombo("#groupSelectMobile", groups);
        RenderTreeSearch("#treeSearchMobile", tree);
    }
    else{
        tree = RenderNodesTree(treeData, "GroupedTreePlace", false);
        RenderGroupsCombo("#comboHolder", groups);
        RenderTreeSearch("#searchHolder", tree);
    }
}

function UpdateTreeData(data) {
    currentTreeData = [];
    $.each(data, function (index, item) {
        if (item.hasOwnProperty("children") && item.children) {
            $.each(item.children, function (index, child) {
                if (child.hasOwnProperty("children") && child.children && child.children.length > 0) {
                    $.each(child.children, function (subIndex, subChild) { currentTreeData.push(subChild); });
                }
                else {
                    currentTreeData.push(child);
                }
            });
        } else {
            currentTreeData.push(item);
        }
    });
}

function RenderNodesTree(data, renderPlace, usemobile) {
    UpdateTreeData(data);
    AddIconClasses(data);
    var store = Ext42.create('Ext42.data.TreeStore', {
        root: {
            expanded: true,
            children: data
        }
    });

    var toolbarButtons=[];

    if (!usemobile) {
        toolbarButtons.push({ xtype: 'toolbar', id: 'comboHolder', height: "4em" });
        toolbarButtons.push({ xtype: 'toolbar', id: 'searchHolder', height: "4em" });
    }

    var tree = Ext42.create('Ext42.tree.Panel', {
        height: isLowResoultionMobile() ? 330 : 500,
        store: store,
        //Commented through FB334519 
        //emptyText: GetAvailableEmptyMessage(),
        frame: !IsMobileViewMode(),
        rootVisible: false,
        useArrows: true,
        dockedItems: toolbarButtons,
        listeners: {
            itemclick: function (view, rec, item, index, eventObj) {
                Ext42.suspendLayouts();
                if (rec)
                    rec.set('checked', !rec.get('checked'));
                rec.cascadeBy(function (n) {
                    if (n != rec) {
                        n.set('checked', rec.get('checked'));
                    }
                });
                Ext42.resumeLayouts();
            }
        }
    });

    if (!usemobile) {
        tree.title = GetAvailableTitle();
    }

    tree.on('checkchange', function (node, checked, options) {
        Ext42.suspendLayouts();
        node.cascadeBy(function (n) {
            n.set('checked', checked);
        });
        Ext42.resumeLayouts();
    }, null);
    
    tree.render(renderPlace);
    return tree;
}

function RenderSelectDeselectAvailableNodes(holder, tree) {
    var id = 'selectDesectAvailableNodesChb';
    RenderSelectDeselect(id, holder);
    
    $('#' + id).on('change', function () { 
        var ischecked = $('#' + id).is(':checked');
        tree.getRootNode().cascadeBy(function (node) { node.set('checked', ischecked); });
    }
    );
}

function RenderSelectDeselect(id, holder){
    $(holder).empty();
    $(holder).append("<input id='"+ id + "' type='checkbox'/> Select/Deselect All");
}

function RenderTreeSearch(holder, tree) {
    $(holder).empty();
    if(!IsMobileViewMode()) {
        $(holder).append("<div style='float:left; clear:both; width:100%'>Search for:</div><div style='float:left; clear:both; width:95%'>" +
            "<input id='searchText' type='text' class='searchTb'/><span id='searchBtn' type='button' class='searchCls'/>"
            + "</div>");
    } else {
        $("#seardiv").appendTo(holder);
    }
    $('#searchBtn').on('click', function () { FilterTree(tree); });
}

function RenderGroupsCombo(holder, data) {
    $(holder).empty();
    $(holder).append("<div style='float:left; clear:both; width:100%'>Group by:</div><select id='groupSelect' style='float:left; clear:both; width:95%'/>");
    $.each(data, function (index, item) { $('#groupSelect').append($("<option></option>").attr("value",item.Column).text(item.DisplayName)); });

    $('#groupSelect').on('change', function () {
        $("#searchText").val('');
        $('#searchBtn').attr("class", "searchCls");
        var item = this.value;
        var searchVal = $("#searchText").val();
        RefreshTree(item, searchVal);
    });
}

function GetSelectedNodeIDs() {
    var ids = [];
    if(selectedGrid)
    {
        var gridItems = selectedGrid.getStore().data.items;
        $.each(gridItems, function () {
            ids.push(this.data.NodeId); 
        });
    }
    return ids;
}

function GetSelectedInterfacesIDs() {
    var selectedIfs = [];
    var nodeIds = [];
    if(selectedGrid)
    {
        var gridItems = selectedGrid.getStore().data.items;
        $.each(gridItems, function () {
            nodeIds.push({"nodeId":this.data.NodeId, "IdKey":this.data.IdKey}); 
        });
        $(nodeIds).each(function (index, node) {
            var rowIndex = $("#interfaceRowIndex").val();
            var interfaceGridId = "InterfaceNestedGrid" + MakeIpHTMLAvailable(node.IdKey);
            var grid = Ext42.getCmp(interfaceGridId);
            if(grid)
            {
                var ifs = [];
                var items = grid.getStore().data.items;
                 $.each(items, function () {
                    ifs.push(this.data.polledItemID); 
                });
                selectedIfs.push({"NodeId":node.nodeId, "Indexes":ifs});
            }
        });
    }
    return selectedIfs  ;
}

function RefreshTree(group, searchVal) {
    executeAsync(function () {
        var treeData = [];
        if (isNPMInstalled) {
            treeData = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetNPMInterfaces", "POST", jsonData = JSON.stringify({ GroupColumn: group, searchValue: searchVal, selectedInterfaces: GetSelectedInterfacesIDs() }));
        } else {
            treeData = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableNodes", "POST", jsonData = JSON.stringify({ GroupColumn: group, searchValue: searchVal, selectedNodes: GetSelectedNodeIDs() }));
        }
        UpdateTreeData(treeData);
        AddIconClasses(treeData);
        var newStore = Ext42.create('Ext42.data.TreeStore', {
            root: {
                expanded: true,
                children: treeData
            }
        });
        tree.reconfigure(newStore);
    });
}

function FilterTree(filterTree) {
    var value = $("#searchText").val();

    if ($("#searchBtn").attr("class") == "searchClsRemove")
        value = "";

    if (value == "") {
        $('#searchBtn').attr("class", "searchCls");
        $("#searchText").val('');
        RefreshTree($('#groupSelect').val(), value);
    } else {
        $('#searchBtn').attr("class", "searchClsRemove");
        RefreshTree($('#groupSelect').val(), value);
    }
}

function executeAsync(func) {
    setTimeout(func, 0);
}

//runs a specified web service asynchronously and returns its Promise
function CallServiceAsync(serviceUrl, method, passData, successCallback, errorCallback) {
    return $.ajax({
        type: method,
        url: serviceUrl,
        data: passData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (successCallback) === 'function') {
                successCallback(data.d);
            }
        },
        error: function (data, textStatus, xhr) {
            if (typeof (errorCallback) === 'function') {
                errorCallback(data, textStatus, xhr);
            }
        }
    });
}

function TestCredentilas() {
    if (!IsDemoView()) {
        var ipAddress = GetIPDialogValue();
        var credId = GetCredentilasID();
        if (ipAddress && credId) {
            $('#credSuccess').hide();
            $('#credFailed').hide();
            $('#credProgress').show();

            var jsonData = JSON.stringify({ ip: ipAddress, id: credId });
            var passed = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/TestCredentialsFromCw", "POST", jsonData);
            $('#credProgress').hide();
            if (passed) {
                $('#credSuccess').show();
            } else {
                $('#credFailed').show();
            }
        }
    }
}

function SaveAutoCompleteIP(cId, cType, newIp) {
    var jsonData = JSON.stringify({ controlID: cId, controlType: cType, value: newIp });
    var passed = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/AddAutompleteIP", "POST", jsonData);
    return passed;
}

function IsNPMInstalled() {
    var passed = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/IsNpmInstalled", "GET", {});
    return passed;
}

function GetGUID() {
    return "{" + (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase() + "}";
}
function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}

function addUnmanagedDevices() {
    $(".addDeviceDialog").css("display", "block");
    $("#Div1").css("display", "none");
    $("#Div2").css("display", "none");
    $("#selectedItemsBottomDiv").css("display", "none");
    $("#Footer").css("display", "none");
    $("#speedDialog").css("display", "none");
}

function getDefaultDataView() {
    var defView = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetDefaultDataView", "GET",{}).toLowerCase();
    if (defView == "chart") {
        $("#viewTypeSelect option:first").text("Chart (Default)");
        $("#viewTypeSelect option:last").text("Table");
    } else {
        $("#viewTypeSelect option:first").text("Chart");
        $("#viewTypeSelect option:last").text("Table (Default)");
    }
    $("#viewTypeSelect").val(defView);
}

function isLowResoultionMobile() {
    if (IsMobile) {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height();
        if (viewportHeight < 500)
            return true;
    }
    return false;
}

//Temporary workaround to pre-load imgs, that aren`t displaying in mobile safari.
function preLoadImages() {
    var changeView = new Image();
    var warningMsgIcon = new Image();
    var infoIcon = new Image();
    var criticalIcon = new Image();
    var warningIcon = new Image();
    var plusMinusIcon = new Image();
    var expandErrorIcon = new Image();

    changeView.src = "../../images/view-buttons-mobile.png"; 
    warningMsgIcon.src = "../../../js/extjs/4.2.2/resources/images/shared/icon-warning.gif";
    infoIcon.src = "../../../js/extjs/4.2.2/resources/images/shared/icon-info.gif";
    criticalIcon.src = "../../images/Critical_icon16x16.png";
    warningIcon.src = "../../images/Warning_icon16x16.png";
    plusMinusIcon.src = "../../images/CWImages/plusminus.png";
    expandErrorIcon.src = "../../../js/extjs/4.2.2/resources/images/tree/arrows.gif";
}
