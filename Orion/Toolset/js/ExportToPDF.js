﻿(function () {

    var 
  exportID,
  file,
  textExportToPDF = '@{R=Core.Strings;K=LIBCODE_PCC_27;E=js}',
  textExportingToPDF = '@{R=Core.Strings;K=WEBJS_PCC_5;E=js}',
  textError = '@{R=Core.Strings;K=WEBJS_PCC_6;E=js}';

    /* i18n:exempt-start */
    // direct reference instead of through i18n.ashx will leave text instrumented.
    var i18nkey = "@{";
    if (!textExportToPDF.indexOf(i18nkey)) textExportToPDF = 'Export to PDF';
    if (!textExportingToPDF.indexOf(i18nkey)) textExportingToPDF = 'Exporting to PDF...';
    if (!textError.indexOf(i18nkey)) textError = 'There was an error trying to export the page to PDF, please try again later.\r\n\r\n';
    /* i18n:exempt-end */

    var exportEvents = {
        beforeExport: [],
        afterExport: []
    };

    var callAllEvents = function (eventArray) {
        $.each(eventArray, function (index, callback) {
            if (typeof (callback) !== "function")
                return;

            callback();
        });
    };

    var startExport = function (rootElement, fileName, orientation, chartMode) {
        file = fileName;
        startExportFromUrl(window.location, rootElement, orientation, chartMode);
    };
    var startExportFromUrl = function (pageUrl, rootElement, orientation, chartMode) {
    
        var link = $('#ExportToPdf');
        if (!link[0] || link.hasClass('busyExportToPdf')) return;

        callAllEvents(exportEvents.beforeExport);
        // Re-fix FB27038. Remove the toolset integration javascript because it may block forever when the server tries to render this page.
        var pageText = document.documentElement.innerHTML;
        var toolsetIntPattern = new RegExp("<script src=\"http://localhost:17779/sw/toolset.*?</script>");
        pageText = pageText.replace(toolsetIntPattern, "");
        pageText = pageText.replace(/_pdfsig_4C5187C5165A4075878201F882534984_[\s\S]*?_4C5187C5165A4075878201F882534984_pdfsig_/g, '');

        exportID = Math.random();
        var temp = "<!DOCTYPE html PUBLIC ' - W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>\r\n";
        temp += "<html class='RenderPdf' xmlns='http://www.w3.org/1999/xhtml'>\r\n" + pageText + "</HTML>";
        temp = "pageHTML=" + encodeURIComponent(temp) + "\r\n";
        //Calculate width  - to decide if we're going to use "calculated width" or "minimal width"
        var x = $($("table.ResourceContainer")[0]);
        var width;
        if (x) {
            try {
                width = x.width() + x.offset().left * 2;
            } catch (err) {
                width = x.width() + 86; //FB27713 - FF .offset() bug
            }
        } else {
            width = $(window).width();
        }
        //Check is page is less than Winnovative A4 portrait vitrual device width (793x1122)
        if (width < 793) {
            width = 793;  //Use minimum width
        }

        var exportUrl = "/Orion/Toolset/ToolExportToPDF.aspx?ExportID=" + exportID;
        if (rootElement) {
            exportUrl += "&RootElement=" + rootElement;
        }
        if (orientation) {
            exportUrl += "&Orientation=" + orientation;
        }
        if (chartMode) {
            exportUrl += "&ChartMode=" + chartMode.toString();
            var element = $("#" + rootElement);
            var outputWidth = element.outerWidth();
            var outputHeight = element.outerHeight();
            exportUrl += "&Width=" + outputWidth;
            exportUrl += "&Height=" + outputHeight;
        }

        exportUrl += "&page=[" + pageUrl + "]";
        exportUrl += "&Title=" + document.title;

        $.ajax({
            url: exportUrl,
            type: 'POST',
            data: temp
        });

        callAllEvents(exportEvents.afterExport);

        link.addClass('busyExportToPdf').html(textExportingToPDF);
        setTimeout(checkProgress, 2000);
    };

    var checkProgress = function () {

        $.ajax({ url: "/orion/Toolset/ToolExportToPdf.aspx?ExportID=" + exportID + "&progresscheck=", type: 'POST', dataType: 'text', success: function (data, textStatus, xmlhttprequest) {
            
            if (data == "NOTYET") {
                setTimeout(checkProgress, 2000);
            }
            else if (data == "DONE") {
                SW.Toolset.Log.debug(document.title);
                window.open("/orion/Toolset/ToolExportToPdf.aspx?ExportID=" + exportID + "&gimmethefile="+file);
                $('#ExportToPdf').removeClass('busyExportToPdf').html(textExportToPDF);
            }
            else if (data.indexOf("ERROR") == 0) {
                $('#ExportToPdf').removeClass('busyExportToPdf').html(textExportToPDF);
                var errString = data.substr(5);
                alert(textError + errString);
            }
        }
        });
    };

    window.ExportToPDF = startExport;
    window.ExportToPDFFromUrl = startExportFromUrl;
    window.ExportToPDFEvents = exportEvents;
})();

