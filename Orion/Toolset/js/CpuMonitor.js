﻿var isInApplyModeValue = false;
var currentTaskID = null;
var cachedResultColumnsPromise = null;
var cacheCleaner = null;

$(document).ready(function(){
    Ext42.require([
    'Ext42.data.*',
    'Ext42.grid.*',
    'Ext42.tree.*'
    ]);
});

(function(){

  var getTitle = function(){ return "CPU Monitor results for " + $("#itemsCount").html(); };

  var currentdate = new Date(); 
  var fileName = "CPUMonitorResults " 
                + currentdate.getFullYear() + "-" 
                +(currentdate.getMonth()+1) + "-" 
                + currentdate.getDate() + " " 
                + currentdate.getHours() + "."  
                + currentdate.getMinutes() + "." 
                + currentdate.getSeconds();


  SW.Toolset.Export.Init({ 
    txt: { domid: 'ExportToTxtId', grid: function(){ return resultsGridObj.grid; }, file: fileName +'.txt', title: getTitle },
    csv: { domid: 'ExportToCSVId', grid: function(){ return resultsGridObj.grid; }, file: fileName +'.csv', title: getTitle },
    pdf: { domid: 'ExportToPdf', grid: function(){ return resultsGridObj.grid; }, file: fileName + '.pdf', title: 'CPU Monitor results', subtitle: getTitle, mode: function(){ return isTableView() ? "table" : "chart"; } }
  });

})();


function InvalidateResultColumnsCache() {
    cachedResultColumnsPromise = null;
}

function InitResultHub(taskId) {
    if (taskId) {
        InitResultNotificationClient(taskId, ReceiveCpuMonitorResults, InvalidateResultColumnsCache);
        //delete cache once a 10 minutes
        cacheCleaner = setInterval(InvalidateResultColumnsCache, 600000);
        SetUpTaskId(taskId);
        $("#stopSpinnerDiv").hide();
        $("#export-button-div").hide();
        $("#spinnerDiv").show();
    }
}

function StopTask() {
    Ext42.MessageBox.confirm({
        title: 'Confirmation',
        width: 300,
        msg: 'Are you sure you want to stop the current real-time procedure? If you try to restart the procedure later, all data collected up to this point will be lost.',
        buttons: Ext42.MessageBox.YESNO,
        fn: function (btn) {
            if (btn == 'yes') {
                CancelCpuMonitorPolling();
                if (resultsChart) {
                    resultsChart.render();
                }
            }
        },
        icon: Ext42.MessageBox.WARNING
    });
}

function CancelCpuMonitorPolling() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        CancelTask(taskId);
        $("#taskIdTextbox").val("");
    }

    $("#spinnerDiv").hide();
    $("#stopSpinnerDiv").show();
}

function StopCpuMonitorResultNotification() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        StopResultNotificationClient(taskId);
        clearInterval(cacheCleaner);
    }
}

//asynchronously prepares current/cached result columns
function getResultColumnsAsync() {
    if (cachedResultColumnsPromise == null) {
        cachedResultColumnsPromise = CallServiceAsync("/Orion/Toolset/Services/CpuMonitorSettings.asmx/GetResultColumns", "GET", {});
    }
    return cachedResultColumnsPromise;
}

//processes data from SignalR
function ReceiveCpuMonitorResults(newResult) {
    if (!IsValidResultsReceived(newResult.CpuDetails)) {
        return;
    }

    $("#lastUpdate").val(new Date().getTime());
    //get result columns, render result grid and update chart asynchronously
    var resultColumnsPromise = getResultColumnsAsync();
    var addResultToChartPromise = addResultToChartAsync(newResult.CpuDetails, newResult.CompletionInfo.CompletionTimeMsSinceEpoch, resultColumnsPromise);

    //when done, add toolset integration
    $.when(addResultToChartPromise)
        //rendering result grid destroys data used by chart - chart must be rendered first
        .pipe(updateResultGridAsync(newResult, resultColumnsPromise))
        .then(function () {
        $("#export-button-div").show();
    });
}

//asynchronously renders result grid
function updateResultGridAsync(newResult, resultColumnsPromise) {
    return resultColumnsPromise.pipe(function (data) {
        var resultcolumns = data.d;

        for (var i = 0; i < newResult.CpuDetails.length; i++) {
            newResult.CpuDetails[i].uiID = newResult.CpuDetails[i].DisplayName + newResult.CpuDetails[i].HostAddress;
        }
        if (IsMobile()) {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGridMobile(resultcolumns, newResult.CpuDetails, 'signalRTableDiv', 400, '90%');
});
        } else {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGrid(resultcolumns, newResult.CpuDetails, 'signalRTableDiv', 400, 750);
});
        }
    });
}

function backToMainPage(taskId, items, itemsToPoll) {
    $("#CWHolder").hide();
    $("#cpuMonitorResults").show();
    if (taskId != "" || (taskId == "" && items == 0)) {
        var itemsExist = items > 0;
        if (itemsExist) {
            $('#itemsCount').html(items + ' selected CPU(s)');
            if (currentTaskID) {
                StopResultNotificationClient(currentTaskID);
                CallServiceAsync("/Orion/Toolset/Services/ConfigurationWizard.asmx/StopTask", "POST", JSON.stringify({ taskId: currentTaskID }));
                if (resultsGridObj.grid) {
                    var dataStore = resultsGridObj.grid.getStore();
                    var indexes = new Array();
                    dataStore.each(function (record) {
                        var exId = record.get('uiID');
                        var exists = false;
                        for (var i = 0; i < itemsToPoll.length; i++) {
                            if (itemsToPoll[i].Items[0] + itemsToPoll[i].IP == exId) {
                                exists = true;
                                continue;
                            }
                        }
                        if (!exists) {
                            var index = dataStore.indexOf(record);
                            indexes.push(index);
                        }
                    }, this);
                    indexes = indexes.sort();
                    for (var j = indexes.length - 1; j >= 0; j--) {
                        dataStore.removeAt(indexes[j]);
                    }
                }
            }
            InitResultHub(taskId);
            currentTaskID = taskId;
            $("#resultsTable").show();
            $("#switchView").show();
            $("#noCpusMessage").hide();

            if ($("#viewTypeSelect").val() == "table") {
                switchToTableView();
            } else {
                switchToPlotView();
            }
        } else {
            $("#resultsTable").hide();
            $("#switchView").hide();
            $("#noCpusMessage").show();
        }
    } else {
        $("#switchView").hide();
    }

}

function isInApplyMode() {
    return isInApplyModeValue;
}

function backToConfigWizard() {
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("form").css("background", "#F5F5F5");
    $("#CWHolder").show();
    $("#cpuMonitorResults").hide();
    isInApplyModeValue = true;
}

function switchToTableView(){
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 1);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnActive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnInactive');
    $("#chartcontainer").hide();
    $("#signalRTableDiv").show();
    $("form").css("background", "#F5F5F5");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("#viewTypeSelect").val("table");
    if (resultsGridObj.grid) resultsGridObj.grid.getView().refresh();
    setTableView();
}

function switchToPlotView() {
    SW.Toolset.Visibility.Mask('chart', 1);
    SW.Toolset.Visibility.Mask('chart-grid', 1);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnInactive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnActive');
    $("#chartcontainer").show();
    $("#signalRTableDiv").hide();
    $("form").css("background", "white");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("#viewTypeSelect").val("chart");
    fixMetricsPanel();
    if (chartGridObj.grid) chartGridObj.grid.getView().refresh();
    if (resultsChart) resultsChart.render();
    setChartView();
}
