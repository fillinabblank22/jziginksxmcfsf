﻿$(document).ready(function () {
    ToolsetSettingsManager.init(ToolsetSettingsManager);

    $("#successMsg").css("display", "none");
    $("#failedMsg").css("display", "none");

    $(".trigger").click(function () {
        trigger(this);
    });
    $(".trigger").click();
    $(".ThresholdTitle").click(function () {
        var panel = $(this).closest('.RowHeaders').next('.panel:first');
        panel.toggle();
    });


    //If threshold checked, show/hide details panel
    function trigger(obj) {
        var panel = $(obj).closest('.RowHeaders').next('.panel:first');
        var checkbox = $(obj).find('input[type=checkbox]').prop('checked');
        if (checkbox) {
            panel.show();
        } else {
            panel.hide();
        }
    }

    function getURLParameter(name) {
        return decodeURI(
                    (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
                );
    };

    $(".WarningLevel").change(function () {
        var val = $(this).find('option:selected').text(); ;
        $(this).closest('.panel').find(".CriticalLevel").val(val);
    });
});

        //value textbox validation
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function demoMode() {
           $("#tabs :input").attr("disabled", true);
           $("#saveSettingsButton").attr("disabled", true);
           $("#saveandcontinueButton").attr("disabled", true);
           $("#saveSettingsMobile").attr("disabled", true);
           $("#saveAndContinueMobile").attr("disabled", true);
           
           $("#saveSettingsButton").addClass('sw-btn-disabled');
           $("#saveandcontinueButton").addClass('sw-btn-disabled');
           $("#saveSettingsMobile").addClass('sw-btn-disabled');
           $("#saveAndContinueMobile").addClass('sw-btn-disabled');
            
           Ext.getCmp('gridAll').setDisabled(true);
           Ext.getCmp('gridSel').setDisabled(true);
        }

        function disableElements(toDis, toEnabl) {
            //var divID = '#'+div+'';
            for (var i = 0; i < toDis.length; i++) {
                var id = '#' + toDis[i] + '';
                $(id).prop('disabled', true);
            }
            for (var i = 0; i < toEnabl.length; i++) {
                var id = '#' + toEnabl[i] + '';
                $(id).prop('disabled', false);
            }
            //var tmp = $(divID);
        }

        //Check percent value for min/max value
        function validateValue(input, unit) {
            var percentType = '%';
            var percentMaxValue = 100;
            var minValue = 1;
            if (input.value === '' || input.value <= 0)
                input.value = minValue; 
            if (unit === percentType && input.value > percentMaxValue ) 
                    input.value = percentMaxValue;
            compareValues(input);
        }

        function validateMaxMinValues(input, minValue, maxValue) {
            if (input.value < minValue) input.value = minValue;
            else if (input.value > maxValue) input.value = maxValue;
        }

        function validateTimeValue(input, minMin, maxMin, minHours, maxHours, drpdwn) {
            var e = document.getElementById(drpdwn);
            var unit = e.options[e.selectedIndex].value;
            if (unit === "Minutes") {
                if (input.value < minMin) input.value = minMin;
                else if (input.value > maxMin) input.value = maxMin;
            }
            else if (unit === "Hours") {
                if (input.value < minHours) input.value = minHours;
                else if (input.value > maxHours) input.value = maxHours;
            }
        }
        
        //Comparing if warning value isn't bigger then critical, else default
        function compareValues(input) {
            var warning = $(input).closest('.panel').find('.WarningValue');
            var warningValue = parseInt(warning.val(), 10);
            var critical = $(input).closest('.panel').find('.CriticalValue');
            var criticalValue = parseInt(critical.val(), 10);

            var warnDefVal = warning.prop("defaultValue");
            var critDefVal = critical.prop("defaultValue");

            var warningLvl = $(input).closest('.panel').find(".WarningLevel").val();

            if (warningLvl.toLowerCase().indexOf("greater") != -1) {
                if (warningValue > criticalValue) {
                    if (warnDefVal < criticalValue) {
                        warning.val(warnDefVal);
                    } else if (warningValue < critDefVal) {
                        critical.val(critDefVal);
                    } else {
                        warning.val(criticalValue);
                    }
                }
            } 
            if (warningLvl.toLowerCase().indexOf("less") != -1) {
                if (warningValue < criticalValue) {
                    if (warnDefVal > criticalValue) {
                        warning.val(warnDefVal);
                    } else if (warningValue > critDefVal) {
                        critical.val(critDefVal);
                    } else {
                        warning.val(criticalValue);
                    }
                }
            }
        }