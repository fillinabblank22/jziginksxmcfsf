﻿function GetJSONData(serviceUrl, method, passData) {
    var resultColumns;
    $.ajax({
        type: method,
        url: serviceUrl,
        data: passData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            resultColumns = data.d;
        },
        error: function (data, textStatus, xhr) {
            alert(xhr);
        }
    });
    return resultColumns;
}

function GetJSONDataNoAlertWhenFails(serviceUrl, method, settings) {
    var result = false;
    $.ajax({
        type: method,
        url: serviceUrl,
        data: settings,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            result = data.d;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            return false;
        }
    });
    return result;
}

function InitSessionManagement() {
    var sessionId = $("#sessionId").val();  //= document.getElementById("sessionId");
    var accountId = $("#accountId").val();  // = document.getElementById("accountId");
    RegisterUserSession(sessionId, accountId, false);
}

function CheckSessionAndRefresh() {
    if (!IsSignalRConnected() || !IsSessionRegistered()) {
        var sessionId = $("#sessionId").val();
        var accountId = $("#accountId").val();  
        RegisterUserSession(sessionId, accountId, true);
    }
    return IsSessionRegistered();
}


function CloseTool() {
    if (confirm('Are you sure want to close the current tab?')) {
        window.open('', '_self', '');
        window.close();
    }
}

function InitTerminateSession() {
    $('#initial').hide();
    $('#terminationStatus').show();
    var accountId = $("#accountId").val();
    if (IsSignalRConnected()) {
        DoTerminateSession(accountId);
    } else {
        connect(DoTerminateSession, accountId);
    }
    return false;
}

function SetUpTaskId(taskId) {
    $("#taskIdTextbox").val(taskId);
    ts.Log.info("TaskId={0}, SessionId={1}, AccountId={2}", taskId, $("#sessionId").val(),  $("#accountId").val());
}

hub.client.ConnectionTerminated = function () {
    document.location.reload(true);
};

hub.client.PostSessionRegistration = function () {
    if (toolTraceRoute) {
        if (isMobileView()) {
            $('#RunButtonMobile').removeAttr('disabled');
        } else {
            if ($('#RunButton').hasClass('sw-btn-secondary')) {
                $('#RunButton').removeClass('sw-btn-secondary').addClass('sw-btn-primary');
            }
            $('#RunButton').removeAttr('disabled');
        }
    } else {
        if ($('#RunButton').hasClass('sw-btn-secondary')) {
            $('#RunButton').removeClass('sw-btn-secondary').addClass('sw-btn-primary');
        }
        $('#RunButton').removeAttr('disabled');
        if (!toolResponseMonitor && $("input.parentIpCb:checked").length > 0) {
            var discoverBtn = Ext42.getCmp("discoverBtn");
            discoverBtn.enable();
        }
    }
};

function IsSessionRegistered() {
    var retVal = GetJSONData("/Orion/Toolset/Services/ConfigurationWizard.asmx/IsSessionRegistered", "GET", {});
    return retVal;
}

function RegisterUserSession(sessionId, accountId, callSync) {
    if (IsSignalRConnected()) {
        DoRegisterUserSession(sessionId, accountId, callSync);
    } else {
        connect(DoRegisterUserSession, sessionId, accountId);
    }
}

function DoRegisterUserSession(sessionId, accountId, callSync) {
    var url = window.location.href;
    var isDetailPos = url.indexOf("Settings.aspx");
    var isDetailFlag = !(isDetailPos >= 0);
    var isMobileMode = $("#mobileMode").val();
    var isLimitReached = $("#isLimiReached").val();
    ts.Log.debug("Registering new session: Id={0}, AccountId={1}", sessionId, accountId);
    if (callSync) {
        hub.server.registerSession(sessionId, accountId, isDetailFlag, isMobileMode, isLimitReached).done(function () { });
    } else {
        hub.server.registerSession(sessionId, accountId, isDetailFlag, isMobileMode, isLimitReached);
    }
}

function DoTerminateSession(accountId) {
    ts.Log.debug("Terminating session for account {0}", accountId);
    hub.server.terminateSession(accountId);
}