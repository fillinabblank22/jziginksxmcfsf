﻿var saveChanges;
var cancelChanges;
var selectorModelForAll;
var selectorModelForSelected;
var selectorModelForMobileGrid;
var allDataStore;
var mobileDataStore;
var selectedDataStore;
var gridAll;
var gridSelected;
var gridMobile;

function selectColumns() {
    var selections = gridAll.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        gridSelected.getStore().add(selections[i]);
        gridAll.getStore().remove(selections[i]);
    }
}
function deselectColumns() {
    var selections = gridSelected.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        gridAll.getStore().add(selections[i]);
        gridSelected.getStore().remove(selections[i]);
    }
}
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}
function saveSettings(obj) {
    var table;
    try {
        if ($('#MobileGrid').is(':visible')) {
            var records = Ext.pluck(gridMobile.getSelectionModel().getSelections(), 'data');
            var temp = new Array();
            Ext.each(records, function(record) {
                temp.push(record);
            });
            table = Ext.encode(temp);
        } else {
            table = Ext.encode(Ext.pluck(selectedDataStore.data.items, 'data'));
        }
        var settings = '{"PollingInterval":"{0}", "PayloadSize":"{1}"}'.format(Ext.getDom("txbPollingInterval").value, Ext.getDom("txbPayloadSize").value);
        ORION.callWebService("/Orion/Toolset/Services/LatencyTestSettings.asmx", "SaveSettings", {
                table: table,
                latencyTestPermanentSettingsJson: settings
            }, function(result) {
                if (!result) {
                    $("#successMsg").css("display", "block");
                    $("#failedMsg").css("display", "none");
                    if (obj !== "dummy") window.location.replace(obj);
                } else {
                    $("#successMsg").css("display", "none");
                    $("#failedMsg").css("display", "block");
                    $("#errorMsg").html("An Error occured during saving. " + result);
                }
            });
    } catch(e) {
        $("#successMsg").css("display", "none");
        $("#failedMsg").css("display", "block");
        $("#errorMsg").html("An Error occured during saving. " + e.message);
    }
}

ToolsetSettingsManager = function() {

    function encodeHTML(htmlText) {

        return $('<div/>').text(htmlText).html();
    }

    function renderColumn(value, meta, record) {
        return encodeHTML(value);
    }

    function selectColumns(all, selectionModel) {
        if (all) {
            selectionModel.selectAll();
        } else
            selectionModel.clearSelections();
    }

    function moveSelectedRow(grid, direction) {
        var records = grid.getSelectionModel().getSelections();
        records = records.sort(function(a,b){return grid.getStore().indexOf(a) - grid.getStore().indexOf(b)});
        var isEnd = false;

        if (direction > 0) {
            for (var j = 0; j < grid.getStore().getCount(); j++) {
                if (records.length - 1 - j < 0) {
                    break;
                }
                if (grid.getStore().getCount() - 1 - j != grid.getStore().indexOf(records[records.length - 1 - j])) {
                    isEnd = false;
                    continue;
                }
                isEnd = true;
            }
        }
        if (isEnd) {
            return;
        }
        if (direction < 0) {
            for (var k = 0; k < records.length; k++) {
                if (k != grid.getStore().indexOf(records[k])) {
                    isEnd = false;
                    continue;
                }
                isEnd = true;
            }
        }

        if (isEnd) {
            return;
        }

        for (var i = 0; i < records.length; i++) {
            var record;

            if (direction < 0) {
                record = records[i];
            } else {
                record = records[records.length - 1 - i];
            }
            if (!record) {
                continue;
            }
            var index = grid.getStore().indexOf(record);
            if (direction < 0) {
                index--;
                if (index < 0) {
                    continue;
                }
            } else {
                index++;
                if (index >= grid.getStore().getCount()) {
                    continue;
                }
            }
            grid.getStore().remove(record);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().selectRow(index, true);
        }
    }

    return {
        reload: function() { gridAll.getStore().reload(); },
        init: function() {

            selectorModelForAll = new Ext.grid.CheckboxSelectionModel({
                singleSelect: false,
                checkOnly: true,
            });

            allDataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/LatencyTestSettings.asmx/GetUnselectedColumns",
                [
                    { name: 'ID', mapping: 0 },
                    { name: 'DisplayName', mapping: 1 }
                ],
                "");

            // define grid panel
            gridAll = new Ext.grid.GridPanel({
                store: allDataStore,
                id: 'gridAll',
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                },
                columns: [
                    selectorModelForAll,
                    { header: 'Id', fixed: true, hideable: false, hidden: true, sortable: false, dataIndex: 'ID' },
                    { header: 'DisplayName', dataIndex: 'DisplayName', width: 298, resizable: true, sortable: false, menuDisabled: true }
                ],
                selModel: selectorModelForAll,
                hideHeaders: true,
                width: 300,
                height: 400,
                frame: true,
                bodyStyle: 'background-color:#fff;',
                title: 'Available Metrics',
                tbar: [
                    {
                        id: 'selectAllButtonAva',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectColumns(true, selectorModelForAll); }
                    }, '-',
                    {
                        id: 'selectNoneButtonAva',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectColumns(false, selectorModelForAll); }
                    }, '-'
                ],
                 listeners: {
                     cellclick: function(grid, rowIndex, columnIndex, e) {
                         if (columnIndex == 0)
                             return;
                         if (grid.getSelectionModel().isSelected(rowIndex)) {
                             grid.getSelectionModel().deselectRow(rowIndex);
                         } else {
                             grid.getSelectionModel().selectRow(rowIndex, true);
                         }
                     }
                 }
            });

            gridAll.render('Grid');
            gridAll.getView().fitColumns();
            selectorModelForAll.fireEvent('selectionchange');
            gridAll.store.proxy.conn.jsonData = {};
            gridAll.store.load();

            selectorModelForSelected = new Ext.grid.CheckboxSelectionModel({
                singleSelect: false,
                checkOnly: true
            });

            selectedDataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/LatencyTestSettings.asmx/GetSelectedColumns",
                [
                    { name: 'ID', mapping: 0 },
                    { name: 'DisplayName', mapping: 1 }
                ],
                "");

            // define grid panel
            gridSelected = new Ext.grid.GridPanel({
                store: selectedDataStore,
                columns: [
                    selectorModelForSelected,
                    { header: 'Id', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
                    { header: 'DisplayName', dataIndex: 'DisplayName', width: 798, sortable: false, menuDisabled: true, render: renderColumn }
                ],
                frame: true,
                id: 'gridSelected',
                bodyStyle: 'background-color:#fff;',
                hideHeaders: true,
                sm: selectorModelForSelected,
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                    stripeRows: false
                },
                autoScroll: true,
                
                layout: 'fit',
                height: 400,
                title: 'Selected Metrics',
                tbar: [
                    {
                        id: 'SelectAll',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectColumns(true, selectorModelForSelected); }
                    }, '-',
                    {
                        id: 'SelectNone',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectColumns(false, selectorModelForSelected); }
                    }, '-',
                    {
                        id: 'MoveUpButton',
                        text: 'Move Up',
                        iconCls: 'moveup',
                        handler: function() { moveSelectedRow(gridSelected, -1); }
                    }, '-',
                    {
                        id: 'MoveDownButton',
                        text: 'Move Down',
                        iconCls: 'movedown',
                        handler: function() { moveSelectedRow(gridSelected, 1); }
                    }
                ],
                listeners: {
                      cellclick: function(grid, rowIndex, columnIndex, e) {
                         if (columnIndex == 0)
                             return;
                         if (grid.getSelectionModel().isSelected(rowIndex)) {
                             grid.getSelectionModel().deselectRow(rowIndex);
                         } else {
                             grid.getSelectionModel().selectRow(rowIndex, true);
                         }
                     }
                 }
            });

            gridSelected.render('GridSelected');
            selectorModelForSelected.fireEvent('selectionchange');
            gridSelected.store.proxy.conn.jsonData = {};
            gridSelected.store.load();

            //Mobile grid
            selectorModelForMobileGrid = new Ext.grid.CheckboxSelectionModel({
                singleSelect: false,
                checkOnly: true
            });
            mobileDataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/LatencyTestSettings.asmx/GetAllColumns",
                [
                    { name: 'selectedCol', mapping: 0 },
                    { name: 'ID', mapping: 1 },
                    { name: 'DisplayName', mapping: 2 }
                ],
                "");

            // define grid panel
            gridMobile = new Ext.grid.GridPanel({
                store: mobileDataStore,
                id: 'gridMobile',
                columns: [
                    selectorModelForMobileGrid,
                    { header: 'Id', hidden: true, hideable: false, sortable: false, dataIndex: 'ID' },
                    { header: 'DisplayName', dataIndex: 'DisplayName', width: 2000, sortable: false, menuDisabled: true, render: renderColumn }
                ],
                columnHeaderCheckbox: true,
                bodyStyle: 'background-color:#fff;',
                hideHeaders: true,
                sm: selectorModelForMobileGrid,
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                },
                autoScroll: true,
                width: 300,

                tbar: [
                    {
                        xtype: 'checkbox',
                        name: 'SelectAll',
                        boxLabel: 'Select/Deselect All',
                        iconCls: '',
                        id: 'cbSelectAll',
                        listeners: { check: function() { selectColumns(Ext.getDom('cbSelectAll').checked, selectorModelForMobileGrid); } },
                    },
                    '->',
                    {
                        id: 'MoveUpButtonMob',
                        text: '',
                        iconCls: 'moveup',
                        handler: function() { moveSelectedRow(gridMobile, -1); }
                    },
                    {
                        id: 'MoveDownButtonMob',
                        text: '',
                        iconCls: 'movedown',
                        handler: function() { moveSelectedRow(gridMobile, 1); }
                    }
                ],
                 listeners: {
                     render: function(grid) {
                         grid.store.on('load', function(store, records, options) {
                             var array = new Array();
                             Ext.each(records, function(record) {
                                 if (record.data['selectedCol']) {
                                     array.push(record);
                                 }
                             });
                             grid.getSelectionModel().selectRecords(array);
                         });
                     },
                     cellclick: function(grid, rowIndex, columnIndex, e) {
                         if (columnIndex == 0)
                             return;
                         if (grid.getSelectionModel().isSelected(rowIndex)) {
                             grid.getSelectionModel().deselectRow(rowIndex);
                         } else {
                             grid.getSelectionModel().selectRow(rowIndex, true);
                         }
                     }
                 }
            });

            gridMobile.render('MobileGrid');
            selectorModelForMobileGrid.fireEvent('selectionchange');
            gridMobile.store.proxy.conn.jsonData = {};
            gridMobile.store.load();
            
        }
    };
}();

function SaveChanges() {
    saveChanges();
}

function CancelChanges() {
    cancelChanges();
}

function HideText(element, eye) {
    eyeIcon(element, eye);
}