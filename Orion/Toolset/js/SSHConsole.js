﻿; (function () {
    var isShown = false;
    var sshHub = $.connection.SSHToolHub,
        hub = $.connection.hub,
        Log = SW.Toolset.Log,
        isNetworkProblem = false;

    var States = {
        Disconnected: 0,
        Connecting: 1,
        Connected: 2,
        Disconnecting: 3
    };
    var statesText = ['Disconnected', 'Connecting', 'Connected', 'Disconnecting'];
    var terminalEventType = {
        Response: 0,
        ConnectionStatus: 1,
        Fault: 2,
    }
    var connectionStatus = States.Disconnected;
    var term,
        termMask,
        clientId,
        activeSession = false,
        isResumeProgress = false,
        sessionTimeout = 600;//Seconds;
    var messageId;
    hub.url = "@{R=Toolset.Startup;K=SignalrPath;E=js}"; // see: SolarWinds.Toolset.Web.Startup
    var hubSettings = { transport: ['webSockets', 'serverSentEvents', 'foreverFrame', 'longPolling'] };
    termMask = new Ext42.LoadMask(Ext42.getBody(), { msg: "Resuming session" });
    Log.LogLevel = SW.Toolset.LogLevelEnum.all;

    var requestId = 0;
    function terminalRequest(command) {
        requestId += 1;
        return {
            requestId: requestId,
            command: command
        }
    }
    function addStatusMessage(oldStatus, newStatus) {
        var $connect = $('#connect');
        $('.term-footer').empty().append(statesText[newStatus]);
        connectionStatus = newStatus;

        if (newStatus == States.Connecting) {
            isShown = true;
            ui.InprogressMsgShown(isShown);
            $connect.val('Disconnect');
            $connect.prop('disabled', false);
            activeSession = true;
        }
        else if (newStatus == States.Disconnected) {
            isShown = false;
            activeSession = false;
            ui.InprogressMsgShown(isShown);
            destroyTerm();
            $connect.val('Connect');
            $connect.prop('disabled', false);
           
            var selectedProfile = $("#ddrpConnectionProfile option:selected").text();
            if (selectedProfile == "None") {
                ui.InputFieldDisable(false);
            }
            else {
                ui.InputFieldDisable(true);
            }
            ui.UpdateConnectButtonStatus();
            //Clean up session information after disconnect status is received
            sshHub.server.removeSession(clientId);
        }
        else if (newStatus == States.Connected) {
            isShown = false;
            ui.InprogressMsgShown(isShown);
            ui.BtnConnectClick();
            initTerm();
            
        }
    }

    function Recieve(terminalEvent) {
        //ignore already received messages
        if (messageId >= terminalEvent.ID)
            return;
        messageId = terminalEvent.ID;
        //console.log("MessageId " + messageId + " Receieved MessageId" + terminalEvent.ID);
        switch (terminalEvent.MessageType) {
            case terminalEventType.Response:
                term.write(terminalEvent.TextResponse);
                break;
            case terminalEventType.ConnectionStatus:
                addStatusMessage(terminalEvent.OldState, terminalEvent.NewState);
                break;
            case terminalEventType.Fault:
                ui.InprogressMsgShown(false);
                $("div.sw-ssh-errortext").html("<p>" + terminalEvent.Message + "</p>");
                break;
            default:
                break;
        }
    }

    $.extend(sshHub.client, {
        receive: function (terminalEvents) {
            var i = 0;
            termMask.hide();
            for (; i < terminalEvents.length; i++) {
                Recieve(terminalEvents[i]);
            }
        },
        sendError: function (message, details) {
            termMask.hide();
            ui.InprogressMsgShown(false);
            $("div.sw-ssh-errortext").html("<p>" + message + "</p>");
        }
    });

    var initSession = function () {
        //Keep trying to resume session for till the sessionTimeout expires
        function resumeSession() {
            if (isResumeProgress)
                return;
            var timer;
            var timeOut = sessionTimeout;
            var period = 30;//seconds
            isResumeProgress = true;
            function onSuccess() {
                isResumeProgress = false;
                termMask.hide();
                console.log("Resume Session from message " + messageId);
                sshHub.server.resumeSession(clientId, messageId);
            }
            function onFail() {
                timeOut = timeOut - period;
                console.log('fail');
                if (timeOut > 0) {
                    startHub();
                    return;
                }
                isResumeProgress = false;
                termMask.hide();
                //show the message box to the user about disconnect
                Ext42.Msg.show({
                    title: 'Session Disconnected',
                    msg: 'Unable to Resume session, timed out',
                    buttons: Ext42.Msg.OK,
                    fn: function (option) {
                        if (option == "ok") {
                            //TODO:
                        }
                    },
                    icon: Ext42.window.MessageBox.ERROR
                });
            }

            function startHub() {
                timer = window.setTimeout(function () {
                    hub.start(hubSettings)
                        .done(onSuccess)
                        .fail(onFail);
                }, period * 1000);
            }
            startHub();
        }

        hub.reconnecting(function () {
            Log.warn("SignalR reconnect");
            isNetworkProblem = true;
            if (activeSession) {
                termMask.show();
            }

        });
        hub.reconnected(function () {
            Log.info("SignalR reconnected");
            isNetworkProblem = false;
            if (activeSession) {
                console.log("Resume Session from message " + messageId);
                sshHub.server.resumeSession(clientId, messageId);
                termMask.hide();
            }
        });
        hub.disconnected(function () {
            // Show a dialog using config options:
            if (isNetworkProblem) {
                if (!activeSession) {
                    Ext42.Msg.show({
                        title: 'Connection error',
                        msg: '@{R=Toolset.Strings;K=ToolsetGUICODE_PS_01;E=js}',
                        buttons: Ext42.Msg.OK,
                        fn: function (option) {
                            if (option == "ok") {
                                //TODO:
                            }
                        },
                        icon: Ext42.window.MessageBox.ERROR
                    });
                } else {
                    resumeSession();
                }
            }
            else /*Healthy disconnect, reload the page to avoid doubling up the terminal area in the page*/ {
                activeSession = false;
                //Most likely the user terminated the connection himself (eg: through 'exit' command). Remember the user can still connect again. Reserving this else block for any hub cleanup in the future
            }
        });
        hub.start(hubSettings).done(function () {
            Log.debug('Now connected, connection ID=' + hub.id);
            $('#connect').on('click', function () {
                if ($(this).attr('disabled') === "disabled") {
                    return;
                }
                var value = $("#txtPort").val();
                if ((parseInt(value) < 1) || (parseInt(value) > 65535)) {
                    return;
                }
                var self = $(this);
                //TODO: move the JSON generation, validation, etc. to a different class method
                var ui = new SSHUIManager();
                var validated = ui.validate();
                if (!validated) {
                    return;
                }
                var connectionDetails = ui.getConnectionDetails();
                if (connectionStatus == undefined || connectionStatus === States.Disconnected) {
                    self.prop('disabled', true);
                    sshHub.server.createSession(connectionDetails)
                        .done(function () {
                            //assign signalrId as connection id the first time
                            clientId = hub.id;
                            requestId = 0;
                            messageId = 0;
                            Log.debug("SSH connection request sent");
                        })
                        .fail(function (error) {
                            Log.error("SSH Connection request could not be send", error);
                            self.prop('disabled', false);
                        });
                }
            });
            $('#disconnect').on('click', function () {
                if (connectionStatus == States.Connected) {
                    var self = $(this);
                    self.prop('disabled', true);
                    sshHub.server.endSession(clientId)
                        .done(function () {
                            Log.debug("SSH Disconnect request sent");
                        })
                        .fail(function (error) {
                            Log.error("SSH Disconnect request failed", error);
                            self.prop('disabled', false);
                        });
                }
            });
        }).fail(function () {
            Log.warn("Could not Connect to SignalR");
        });

    };


    var initTerm = function () {
        var $termView = $('.term-view');
        var $termBody = $termView.find('.term-body');
        var $term = $termView.find('.term');
        var termWidth;
        if (term == undefined) {
            $termBody.empty();
            term = new Terminal({
                cols: SSHSettings.terminalColumns,
                rows: SSHSettings.terminalRows,
                useStyle: true,
                cursorBlink: true,
                convertEol: true,
                screenKeys: true
            });
            term.on('data', function (data) {
                //console.log(data);
                var request = terminalRequest(data);
                sshHub.server.receiveCommand(clientId, request);
            });
            //Header message need not be appended
            //term.on('title', function (title) {
            //    $('.term-header').empty().append(title);
            //});
            
            term.open($termBody[0]);
            $term.show();
            termWidth = $term.width() < $(window).width() ? $term.width() : $(window).width();
            $termView.find('.status-section').width(termWidth);
            $termView.find('.status-host').text($("#txbInputAddress").val());

            $('.term-form').hide();
            $termView.show();
        }
    };
    var destroyTerm = function () {
        if (term) {
            $('.term-view').hide();
            term.destroy();
            term = undefined;
            $('.term-form').show();
        }
    };
    //Init Signalr Session
    initSession();

})();

