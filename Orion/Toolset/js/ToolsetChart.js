﻿(function ($) {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (val) {
            return jQuery.inArray(val, this);
        };
    }
})(jQuery);

var transparentColor = (window.navigator.userAgent.indexOf("MSIE 8") > 0 || window.navigator.userAgent.indexOf("MSIE 7") > 0) ? "rgba(0,255,255,255)" : "transparent";
var ToolsetChartUserActions = {
    MetricUpdatedByUser: false //tracke the metric updates - to minimize the 0 height checks as the height problem occurs only when after the user has updated the metric selection/deselection
};

(function (Highcharts) {
    /**
    * Utility function to remove last occurence of an item from an array
    * @param {Array} arr
    * @param {Mixed} item
    */
    function erase(arr, item) {
        var i = arr.length;
        while (i--) {
            if (arr[i] === item) {
                arr.splice(i, 1);
                break;
            }
        }
        return i;
    }
    /**
    * Add an axis to the chart
    * @param {Object} options The axis option
    * @param {Boolean} isX Whether it is an X axis or a value axis
    */
    Highcharts.Chart.prototype.addAxis = function (options, isX) {
        var key = isX ? 'xAxis' : 'yAxis',
            axis = new Highcharts.Axis(this, Highcharts.merge(options, {
                index: resultsChart[key].length
            }));

        // Push the new axis options to the chart options
        resultsChart.options[key] = Highcharts.splat(resultsChart.options[key] || {});
        resultsChart.options[key].push(options);
    };

    /**
    * Remove an axis from the chart
    */
    Highcharts.Axis.prototype.remove = function () {
        if (this.series.length) {
            console.error('Highcharts error: Cannot remove an axis that has connected series');
        } else {
            var key = this.isXAxis ? 'xAxis' : 'yAxis';

            // clean up chart options
            var axisIndex = this.options.index;
            resultsChart.options[key].splice(axisIndex, 1);

            erase(resultsChart.axes, this);
            var index = erase(resultsChart[key], this);

            // clean up following axis options (indices)
            for (var i = index; i < resultsChart[key].length; i++) {
                resultsChart[key][i].options.index--;
            }

            this.destroy();
            resultsChart.isDirtyBox = true;
            resultsChart.redraw();
        }
    };
} (Highcharts));

function getColNameForDeviceTitle() {
    if (toolCPUMonitor) return "CPU Name";
    if (toolInterfaceMonitor) return "Interface";
    if (toolMemoryMonitor) return "Memory Item";
    if (toolResponseMonitor) return "Device Name";
    return "";
}

var detailsResultColumns;
$(function () {
    detailsResultColumns = [
        {
            ColumnType: 101,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Metrics",
            PropertyName: "displayName",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 3,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: getColNameForDeviceTitle(),
            GroupName: null,
            PropertyName: "deviceName",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 0,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Current value",
            GroupName: null,
            PropertyName: "currentValue",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 0,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Max value",
            GroupName: null,
            PropertyName: "maxValue",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 0,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Min value",
            GroupName: null,
            PropertyName: "minValue",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 0,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Average value",
            GroupName: null,
            PropertyName: "avgValue",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 100,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "<img src='../../images/Warning_icon16x16.png'/>Warning state",
            GroupName: null,
            PropertyName: "warningState",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        },
        {
            ColumnType: 100,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "<img src='../../images/Critical_icon16x16.png'/>Critical state",
            GroupName: null,
            PropertyName: "criticalState",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        }];
    if (toolInterfaceMonitor) {
        detailsResultColumns.push({
            ColumnType: 102,
            CriticalThresholdOperator: 0,
            CriticalThresholdValue: 0,
            DisplayName: "Total bytes transfered",
            GroupName: null,
            PropertyName: "totalBytesTransfered",
            SubGroupName: null,
            SupportCharts: false,
            Unit: "",
            UseThreshold: false,
            ValueType: 0,
            Visible: true,
            WarningThresholdOperator: 0,
            WarningThresholdValue: 0
        });
    }
});

var metrictree;
var maxSeriesCount = 12;
var totalMetrics = 0;
var usedMetrics = 0;
var metricsSummaryString = "Metrics ({0} / {1})";
var lastItemsToPoll;

var originalMaxSeriesWidth = -1;
var resultsChart;
var lastResultColumns;
var chartSettings = {
    series: [
    ],
    showThresholds: false,
    thresholdParam: ''
};
var availableChartColors = ["#006CA9", "#49D2F2", "#A87000", "#D8C679", "#515151",
    "#310091", "#9673EA", "#93008B", "#F149E7",
    "#828C38", "#C5DE00", "#874340", "#DBB0A0"];
var usedChartColors = [];
var chartContainerDiv = $("#chartcontainer");

function getSeriesColor() {
    var color = availableChartColors[0];
    availableChartColors.splice(0, 1);
    usedChartColors.push(color);
    return color;
}

function returnColor(color) {
    var index = usedChartColors.indexOf(color);
    availableChartColors.splice(0, 0, color);
    usedChartColors.splice(index, 1);
}

function resColumnsChanged(resultColumns) {
    if (lastResultColumns == null && resultColumns != null) return false;
    var lastChartCols = [];
    for (var i = 0; i < lastResultColumns.length; i++) {
        if (lastResultColumns[i].SupportCharts) {
            lastChartCols.push(lastResultColumns[i]);
        }
    }

    var newChartCols = [];
    for (i = 0; i < resultColumns.length; i++) {
        if (resultColumns[i].SupportCharts) {
            newChartCols.push(resultColumns[i]);
        }
    }

    if (lastChartCols.length != newChartCols.length) return true;

    for (i = 0; i < lastChartCols.length; i++) {
        if (lastChartCols[i].PropertyName != newChartCols[i].PropertyName) {
            return true;
        }
    }

    return false;
}

function CreateResultsChart(container) {
    chartContainerDiv = $("#chartcontainer");
    var config = {
        chart: {
            renderTo: container,
            plotBorderColor: '#C9C9C9',
            plotBorderWidth: 1
        },

        plotOptions: {
            series: {
                allowPointSelect: true,
                marker: {
                    states: {
                        select: {
                            fillColor: 'red',
                            lineWidth: 0
                        }
                    }
                },
                cursor: 'pointer'
            }
        },

        resetZoomButton: {
            theme: {
                display: 'none'
            }
        },

        xAxis: {
            labels: {
                formatter: function () {
                    var date = new Date(this.value);
                    return ("0" + date.getHours()).slice(-2) + ":" +
                            ("0" + date.getMinutes()).slice(-2) + ":" +
                            ("0" + date.getSeconds()).slice(-2);
                }
            },
            events: {
                afterSetExtremes: function (e) {
                    var minDistance = 1 * 1000; //1 second time
                    var xaxis = this;
                    if ((e.max - e.min) < minDistance) {
                        var min = e.max - minDistance;
                        var max = e.max;
                        window.setTimeout(function () {
                            xaxis.setExtremes(min, max);
                        }, 1);
                    }
                }
            }
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 3
        },

        scrollbar: {
            enabled: true
        },

        navigator: {
            enabled: true
        },

        tooltip: {
            formatter: tooltipFormatter
        },

        series: [
        {
            name: 'Default',
            color: transparentColor,
            enableMouseTracking: false,
            data: (function () {
                return [];
            })(),
            tooltip: {
                valueDecimals: 2
            }
        }
        ]
    };

    $("#" + container).html();
    resultsChart = new Highcharts.StockChart(config);
    var lbl = $("tspan:contains('Highcharts.com')");
    if (lbl) lbl.css('display', 'none');
    lbl = $("span:contains('Highcharts.com')");
    if (lbl) lbl.css('display', 'none');

    resultsChart.yAxis[0].setOptions({
        labels: {
            enabled: false
        },
        title: {
            text: null
        }
    });
}

function getResColumnByPropertyName(resultColumns, propName) {
    for (var i = 0; i < resultColumns.length; i++) {
        if (resultColumns[i].PropertyName == propName) {
            return resultColumns[i];
        }
    }
    return null;
}

function getAxisByName(name) {
    for (var i = 0; i < resultsChart.yAxis.length; i++) {
        if (resultsChart.yAxis[i].options.chartAxis == name) {
            return i;
        }
    }
    return 0;
}

var solidStyleUsed = false;
function getDashStyle() {
    //    var style = solidStyleUsed ? "solid" : "solid";
    //    solidStyleUsed = style == "solid";
    //    return style;
    //Maryan: I live current functionality as is. In such case we can anytime revert to using dash style for series
    return "solid";
}

//adds series to chart
function addSeries(hostAddress, displayName, propertyName, rec) {
    //prepare result columns asynchronously
    var resultColumnsPromise = getResultColumnsAsync().pipe(function (data) { return lastResultColumns ? lastResultColumns : data.d; });

    //when ready, use prepared data to add the series
    $.when(resultColumnsPromise).pipe(function (rescols) {

        //update axis
        var resCol = getResColumnByPropertyName(rescols, propertyName);
        var axisIndex = getAxisByName(resCol.ChartAxis);

        if (axisIndex == 0) {
            resultsChart.addAxis({
                min: 0,
                id: propertyName,
                title: {
                    text: resCol.ChartAxisTitle
                },
                labels: {
                    formatter: function() {
                        return getMobileDataRenderer(this.value, null, null, null, null, null, null, getColumnSettingsByPropertyName(this.axis.options.id));
                    }
                },
                lineWidth: 2,
                opposite: true,
                chartAxis: resCol.ChartAxis

            });
            resultsChart.yAxis[resultsChart.yAxis.length - 1].name = propertyName;
            resultsChart.yAxis[resultsChart.yAxis.length - 1].dashStyle = getDashStyle();
            axisIndex = resultsChart.yAxis.length - 1;
        } else {
            var hasVisibleSeries = false;
            for (var i = 0; i < resultsChart.yAxis[axisIndex].series.length; i++) {
                if (resultsChart.yAxis[axisIndex].series[i].visible) {
                    hasVisibleSeries = true;
                    break;
                }
            }
            if (!hasVisibleSeries) {
                resultsChart.yAxis[axisIndex].setTitle(resCol.DisplayName);
                resultsChart.yAxis[axisIndex].dashStyle = getDashStyle();
                resultsChart.yAxis[axisIndex].redraw();
                for (var m = 0; m < resultsChart.yAxis[axisIndex].series.length; m++) {
                    if (resultsChart.yAxis[axisIndex].series[m].graph) {
                        resultsChart.yAxis[axisIndex].series[m].graph.attr({ 'stroke-width': 2, dashstyle: resultsChart.yAxis[axisIndex].dashStyle });
                    }
                }
            }
        }

        usedMetrics++;
        $("#chartMetricsHeader label").text(stringFormat(metricsSummaryString, usedMetrics, totalMetrics));
        var seriesId = getSeriesId(hostAddress, displayName, propertyName, rec);
        var color = getSeriesColor();

        var seriesIndex = getSeriesIndexById(seriesId);
        if (seriesIndex < 0) {
            resultsChart.addSeries(
            {
                name: resCol.DisplayName + ' : ' + rec.raw.deviceName,
                deviceName: rec.raw.deviceName,
                deviceFullName: rec.raw.deviceFullName,
                hostAddress: hostAddress,
                id: seriesId,
                propertyName: propertyName,
                yAxis: axisIndex,
                color: color,
                ifindex: rec.raw.ifindex,
                dashStyle: resultsChart.yAxis[axisIndex].dashStyle,
                data: (function() {
                    // generate an array of random data
                    var data = [], time = (new Date()).getTime();
                    return data;
                })()
            }, false);
            chartSettings.series.push(
            {
                id: seriesId,
                highSeries: resultsChart.series[resultsChart.series.length - 1],
                propertyName: propertyName
            });
        } else {
            changeSeriesColor(chartSettings.series[seriesIndex].highSeries, color);
            chartSettings.series[seriesIndex].highSeries.graph.attr({ 'stroke-width': 2, dashstyle: resultsChart.yAxis[axisIndex].dashStyle });
            chartSettings.series[seriesIndex].highSeries.show();
        }
        //update metrics
        $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf').css('background', color);
        $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf ~ .x42-tree-node-text').css('color', 'black');
    });
}

function removeSeries(rec) {
    var axisIndex = getAxisByName(rec.raw.axis);

    $("#chartMetricsHeader label").text(stringFormat(metricsSummaryString, usedMetrics, totalMetrics));
    
    //ie8 has a knownbug when  inline style set to empty $.css(property,''), browser sets the default style which cant be overridden in css classes
    //refer http://stackoverflow.com/questions/4036857/jquery-remove-style-added-with-css-function
    $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf').removeAttr("style");
    $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf ~ .x42-tree-node-text').removeAttr('style');
    var seriesId = getSeriesId(rec.raw.hostaddress, rec.raw.deviceName, rec.raw.propertyName, rec);
    for (var i = 0; i < chartSettings.series.length; i++) {
        if (chartSettings.series[i].id == seriesId) {
            returnColor(chartSettings.series[i].highSeries.color);
            chartSettings.series[i].highSeries.remove(false);
            chartSettings.series.splice(i, 1);
            //chartSettings.series[i].highSeries.hide();
            usedMetrics--;
            $("#chartMetricsHeader label").text(stringFormat(metricsSummaryString, usedMetrics, totalMetrics));
            break;
        }
    }

    var allSeriesHidden = true;
    for (var j = 0; j < resultsChart.yAxis[axisIndex].series.length; j++) {
        if (resultsChart.yAxis[axisIndex].series[j].visible) {
            allSeriesHidden = false;
            break;
        }
    }

    if (allSeriesHidden) {
        if (resultsChart.yAxis[axisIndex].plotLinesAndBands.length > 0) {
            axisWithPlotBand.removePlotBand("warningPlotBand");
            axisWithPlotBand.removePlotBand("criticalPlotBand");
            axisWithPlotBand = null;
        }

        resultsChart.yAxis[axisIndex].remove();

     
        //resultsChart.yAxis[axisIndex].setTitle(null);
        //            if (resultsChart.yAxis[axisIndex].dashStyle == "solid") {
        //                solidStyleUsed = false;
        //            }
        //            resultsChart.yAxis[axisIndex].redraw();
    } else {
        if (axisWithPlotBand) {
            if (axisWithPlotBand.plotLinesAndBands.length > 0) {
                axisWithPlotBand.plotLinesAndBands[0].render();
            }
            if (axisWithPlotBand.plotLinesAndBands.length > 1) {
                axisWithPlotBand.plotLinesAndBands[1].render();
            }
        }
    }
}

function getSeriesIndexById(seriesId) {
    for (var i = 0; i < chartSettings.series.length; i++) {
        if (chartSettings.series[i].id == seriesId) {
            return i;
        }
    }
    return -1;
}

function getSeriesId(hostAddress, displayName, propertyName, rec) {
    if (toolResponseMonitor)
        return hostAddress + '_' + hostAddress + '_' + propertyName;

    if (toolInterfaceMonitor) {
        if (rec) {
            return hostAddress + '_' + rec.raw.ifindex + '_' + propertyName;
        } else {
            return hostAddress + '_' + displayName + '_' + propertyName;
        }
    }
    

    return hostAddress + '_' + displayName + '_' + propertyName;
}

function updateMetrics(resultColumns) {
    lastResultColumns = resultColumns;
}

function updateExistingChart(itemsToPoll, resultColumns) {
    totalMetrics = 0;
    usedMetrics = 0;
    //save checked items
    var checkedMetrics = [];
    //store current state
    metrictree.getRootNode().cascade(function (rec) {
        if (rec.raw.id == "root") {
            return;
        }
        if (rec.get('checked') && rec.raw.leaf) {
            checkedMetrics.push({
                axis: rec.raw.axis,
                checked: rec.raw.checked,
                deviceFullName: rec.raw.deviceFullName,
                deviceName: rec.raw.deviceName,
                displayName: rec.raw.displayName,
                expanded: rec.raw.displayName,
                hostaddress: rec.raw.hostaddress,
                iconCls: rec.raw.iconCls,
                leaf: rec.raw.leaf,
                propertyName: rec.raw.propertyName,
                text: rec.raw.text,
                color: colorToHex($('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf').css('background-color'))
            });
            rec.set('checked', false);
        }
    });

    for (var i = 2; i < resultsChart.series.length; i++) {
        resultsChart.series[i].hide();
    }

    createMetricsPanel(itemsToPoll, resultColumns);
    //restore metrics state
    metrictree.getRootNode().cascade(function (rec) {
        if (rec.raw.id == "root" || !rec.raw.leaf) {
            return;
        }

        for (var j = 0; j < checkedMetrics.length; j++) {
            if (checkedMetrics[j].deviceFullName == rec.raw.deviceFullName
                && checkedMetrics[j].deviceName == rec.raw.deviceName
                && checkedMetrics[j].displayName == rec.raw.displayName
                && checkedMetrics[j].hostaddress == rec.raw.hostaddress
                && checkedMetrics[j].propertyName == rec.raw.propertyName
                && checkedMetrics[j].text == rec.raw.text) {
                //existing metrics was found
                rec.set('checked', true);
                $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf').css('background-color', checkedMetrics[j].color);
                $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf ~ .x42-tree-node-text').css('color', 'black');
                //find line by color
                for (i = 2; i < resultsChart.series.length; i++) {
                    if (resultsChart.series[i].options.color == checkedMetrics[j].color) {
                        resultsChart.series[i].show();
                        usedMetrics++;
                        break;
                    }
                }
            }
        }
    });

    if (chartGridObj.grid) {
        updateDetailsTable();
    }

    $("#chartMetricsHeader label").text(stringFormat(metricsSummaryString, usedMetrics, totalMetrics));
}

function colorToHex(color) {
    if (color.substr(0, 1) === '#') {
        return color;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);

    function toHex(n) {
        n = parseInt(n, 10);
        if (isNaN(n)) return "00";
        n = Math.max(0, Math.min(n, 255));
        return "0123456789ABCDEF".charAt((n - n % 16) / 16)
      + "0123456789ABCDEF".charAt(n % 16);
    }

    return '#' + (toHex(red) + toHex(green) + toHex(blue)).toUpperCase();
}

function initializeChartView(itemsToPoll, resultColumns) {
    if (resultsChart && hasChartMetrics(resultColumns)) {
        // update existing chart only and metrics
        var selectedProp = $("#thresholdsCombo").val();
        updateExistingChart(itemsToPoll, resultColumns);
        $("#thresholdsCombo").val(selectedProp);
        return;
    }

    axisWithPlotBand = null;
    firstZoomOut = true;
    lastItemsToPoll = itemsToPoll;
    lastResultColumns = resultColumns;
    totalMetrics = 0;
    usedMetrics = 0;
    availableChartColors = ["#006CA9", "#49D2F2", "#A87000", "#D8C679", "#515151",
    "#B7B7B7", "#310091", "#9673EA", "#93008B", "#F149E7",
    "#828C38", "#C5DE00", "#874340", "#DBB0A0"];
    usedChartColors = [];
    resultsChart = null;
    chartSettings.series = [];
    solidStyleUsed = false;
    //check if metrics data available
    if (!hasChartMetrics(resultColumns)) {
        $("#chartOptionsHolder").html('No metrics have been selected.');
        $("#chartMetricsThresholds").css('display', 'none');
        $(".managedObjInfoWrapper").css('display', 'none');
        $("#chartOptionsHolder").css('border', '1px solid #C5C5C5');
        $("#chartHolder").css('border', '1px solid #C5C5C5');
        $("#chartOptionsHolder").css('background', 'white');
        $("#chartHolder").empty();
        $("#chartOptionsHolder").css('padding', '15px');
        $("#chartMetricsHeader label").text("Metrics");
        clearThreshouldsCombo();
        addSelectMetricsMessage();
        return;
    } else {
        $("#chartMetricsThresholds").css('display', 'inline-block');
        $("#chartOptionsHolder").css('border', '');
        $("#chartOptionsHolder").css('background', '');
        $("#chartOptionsHolder").css('padding', '');

        CreateResultsChart(chartContainer);
        createMetricsPanel(itemsToPoll, resultColumns);

        if (chartContainerDiv.css('display') != 'none') {
            var rec = metrictree.getStore().getRootNode().childNodes[0].childNodes[0];
            rec.set('checked', true);
            performMetricsCheck(rec);
            turnOnFirstMetric = false;
        } else {
            turnOnFirstMetric = true;
        }
    }
    chartGridObj.grid = null;
    chartGridObj.gridColWidth = null;
    $("#chartDetailsTable").html('');

    HideTooltipOnTouch();
}

function HideTooltipOnTouch() {
    var chartHolder = "#chartHolder";
    var chartContainerDiv = $("#chartcontainer");
    chartContainerDiv.on({
        'touchstart': function (evt) {
            if (!$(evt.target).parents(chartHolder).length) {
                if (resultsChart != null & resultsChart.tooltip != null)
                    if (!resultsChart.tooltip.isHidden) {
                        resultsChart.tooltip.hide();
                        resultsChart.tooltip.hideCrosshairs();
                    }
            }
        }
    });
}


var turnOnFirstMetric = false;

function clearThreshouldsCombo() {
    $("#thresholdsCombo option").not(':first').each(function () {
        $(this).remove();
    });
}

function addSelectMetricsMessage() {
    var msg = $("#selectMetricsMessage").clone().prop({ id: "selectMetricsMessage2" });
    $("#chartHolder").append(msg);
    $("#selectMetricsMessage2").css('display', 'block');
}

function hasThirdMetricsType(resultColumns) {
    for (var i = 0; i < resultColumns.length; i++) {
        if (resultColumns[i].SupportCharts && resultColumns[i].ChartAxis == 3) {
            return true;
        }
    }
    return false;
}

function hasChartMetrics(resultColumns) {
    for (var i = 0; i < resultColumns.length; i++) {
        if (resultColumns[i].SupportCharts) {
            return true;
        }
    }
    return false;
}

//ToDo: make async
function getMetricsData(itemsToPoll, resultColumns) {
    var renderedNodes = [];
    for (var l = 0; l < itemsToPoll.length; l++) {
        renderedNodes.push(itemsToPoll[l].IP);
    }
    var nodeNames = getNodeNames(renderedNodes);

    if (toolInterfaceMonitor) {
        var renderedInterfaces = [];
        var ips = [];
        for (var i = 0; i < itemsToPoll.length; i++) {
            for (var j = 0; j < itemsToPoll[i].Items.length; j++) {
                ips.push(itemsToPoll[i].IP);
                renderedInterfaces.push(itemsToPoll[i].Interfaces[j].Index);
            }
        }

        var interfaces;
        $.ajax({
            type: "POST",
            url: "/Orion/Toolset/Services/Toolset.asmx/GetInterfaceNetObjectIdsByIndex",
            data: JSON.stringify({ ids: renderedInterfaces, ipaddress: ips }),
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                interfaces = msg.d;
            },
            error: function (msg) {
                return null;
            }
        });
        var m = 0;
        for (i = 0; i < itemsToPoll.length; i++) {
            for (j = 0; j < itemsToPoll[i].Items.length; j++) {
                itemsToPoll[i].Items[j] = interfaces[m] ? interfaces[m] : itemsToPoll[i].Items[j];
                m++;
            }
        }
    }

    var metricsData = [];
    var pos = 0;
    for (i = 0; i < itemsToPoll.length; i++) {
        for (j = 0; j < itemsToPoll[i].Items.length; j++) {
            //var pollItemFullName = interfaces ? interfaces[pos++] : itemsToPoll[i].Items[j];
            var pollItemFullName = itemsToPoll[i].Items[j];
            var deviceItem = {
                displayName: itemsToPoll[i].Items[j] + ' ' + itemsToPoll[i].IP,
                hostaddress: itemsToPoll[i].IP,
                deviceName: itemsToPoll[i].Items[j] + ' ' + itemsToPoll[i].IP,
                propertyName: '',
                text: pollItemFullName + ' on ' + nodeNames[i].HTML,
                deviceFullName: pollItemFullName,
                leaf: false,
                expanded: true,
                checked: false,
                children: [],
                ifindex: itemsToPoll[i].Interfaces ? itemsToPoll[i].Interfaces[j].Index : 0
            };
            for (var k = 0; k < resultColumns.length; k++) {
                if (resultColumns[k].SupportCharts) {
                    totalMetrics++;
                    deviceItem.children.push({
                        displayName: resultColumns[k].DisplayName,
                        deviceFullName: pollItemFullName,
                        hostaddress: itemsToPoll[i].IP,
                        deviceName: itemsToPoll[i].Items[j],
                        propertyName: resultColumns[k].PropertyName,
                        text: resultColumns[k].DisplayName,
                        axis: resultColumns[k].ChartAxis,
                        leaf: true,
                        expanded: false,
                        checked: false,
                        iconCls: '',
                        ifindex: itemsToPoll[i].Interfaces ? itemsToPoll[i].Interfaces[j].Index : 0
                    });
                }
            }
            metricsData.push(deviceItem);
        }
    }
    return metricsData;
}

var axisWithPlotBand = null;
function thresholdsComboItemChanged() {
    if (!resultsChart) {
        return;
    }

    resultsChart.series[0].color = transparentColor;
    resultsChart.series[0].graph.attr({ stroke: transparentColor });
    
    if (axisWithPlotBand) {
        axisWithPlotBand.removePlotBand("warningPlotBand");
        axisWithPlotBand.removePlotBand("criticalPlotBand");
        axisWithPlotBand = null;
    }

    var selectedProp = $("#thresholdsCombo").val();

    //check for visible series
    for (var i = 2; i < resultsChart.series.length; i++) {
        if (resultsChart.series[i].options.propertyName == selectedProp && resultsChart.series[i].visible) {
            axisWithPlotBand = resultsChart.series[i].yAxis;
            break;
        }
    }

    if (!axisWithPlotBand) {
        return;
    }

    var resCol = getResColumnByPropertyName(lastResultColumns, selectedProp);
    axisWithPlotBand.resColumn = resCol;
    //add warning plotband
    axisWithPlotBand.addPlotBand({
        from: resCol.WarningThresholdValue,
        to: resCol.CriticalThresholdValue,
        color: 'rgba(255, 255, 0, 0.2)',
        id: 'warningPlotBand',
        label: {
            text: 'Warning threshold',
            style: {
                color: '#808080'
            }
        }
    });
    //add critical plotband
    axisWithPlotBand.addPlotBand({
        from: resCol.CriticalThresholdValue,
        to: (resCol.CriticalThresholdOperator <= 1) ?
            (resCol.CriticalThresholdValue > axisWithPlotBand.max ? resCol.CriticalThresholdValue : axisWithPlotBand.max) :
            (resCol.CriticalThresholdValue < axisWithPlotBand.min ? resCol.CriticalThresholdValue : axisWithPlotBand.min),
        color: 'rgba(255, 0, 0, 0.2)',
        id: 'criticalPlotBand',
        label: {
            text: 'Critical threshold',
            style: {
                color: '#808080'
            }
        }
    });
}

function updateThresholdsList(resultColumns) {
    clearThreshouldsCombo();

    for (var k = 0; k < resultColumns.length; k++) {
        if (resultColumns[k].SupportCharts && resultColumns[k].UseThreshold) {
            $('#thresholdsCombo').append($("<option></option>").attr("value", resultColumns[k].PropertyName).text(resultColumns[k].DisplayName));
        }
    }
}

var isTheSameMetricsType = true;
function createMetricsPanel(itemsToPoll, resultColumns) {
    if (hasThirdMetricsType(resultColumns)) {
        $(".managedObjInfoWrapper").css("display", "block");
    } else {
        $(".managedObjInfoWrapper").css("display", "none");
    }

    var metricsData = getMetricsData(itemsToPoll, resultColumns);
    isTheSameMetricsType = true;
    if (metricsData.length > 0) {
        for (var i = 0; i < metricsData[0].children.length - 1; i++) {
            if (metricsData[0].children[i].axis != metricsData[0].children[i + 1].axis) {
                isTheSameMetricsType = false;
                break;
            }
        }
    }

    var metricsStore = Ext42.create('Ext42.data.TreeStore', {
        root: {
            expanded: true,
            children: metricsData
        }
    });

    $("#chartOptionsHolder").empty();

    metrictree = Ext42.create('Ext42.tree.Panel', {
        width: "100%",
        height: $("#chartMetrics").height() - $("#chartMetricsHeader").height() - $("#chartMetricsThresholds").height(),
        store: metricsStore,
        emptyText: '',
        rootVisible: false,
        useArrows: false,
        frame: false,
        listeners: {
            itemclick: function (node, rec) {
                Ext42.suspendLayouts();
                rec.set('checked', !rec.get('checked'));
                ToolsetChartUserActions.MetricUpdatedByUser = true;
                performMetricsCheck(rec);
                Ext42.resumeLayouts();
            },

            checkchange: function (rec, check) {
                Ext42.suspendLayouts();
                performMetricsCheck(rec);
                Ext42.resumeLayouts();
            },

            itemexpand: function (node) {
                Ext42.suspendLayouts();
                node.cascadeBy(function (rec) {
                    if (rec.raw.leaf) {
                        if (!isTheSameMetricsType) {
                            appendTypeNum($('tr[data-recordid="' + rec.internalId + '"] > td > div'), rec.raw.axis);
                        }

                        var seriesId = getSeriesId(rec.raw.hostaddress, rec.raw.deviceName, rec.raw.propertyName, rec);
                        for (var j = 0; j < chartSettings.series.length; j++) {
                            if (chartSettings.series[j].id == seriesId) {
                                $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf').css('background', chartSettings.series[j].highSeries.color);
                                $('tr[data-recordid="' + rec.internalId + '"] .x42-tree-icon-leaf ~ .x42-tree-node-text').css('color', 'black');
                            }
                        }
                    }
                });

                $("#chartOptionsHolder div:has(>.x42-tree-expander)").addClass("metricsDeviceRow");
                $("#chartOptionsHolder div:has(>.x42-tree-icon-leaf)").addClass("metricsParameterRow");
                Ext42.resumeLayouts();
            }
        }
    });

    metrictree.render('chartOptionsHolder');

    metrictree.getRootNode().cascadeBy(function (n) {
        if (n.raw.leaf && !isTheSameMetricsType) {
            appendTypeNum($('tr[data-recordid="' + n.internalId + '"] > td > div'), n.raw.axis);
        }
    });
    $("#chartMetricsHeader label").text(stringFormat(metricsSummaryString, 0, totalMetrics));
    $("#chartOptionsHolder div:has(>.x42-tree-expander)").addClass("metricsDeviceRow");
    $("#chartOptionsHolder div:has(>.x42-tree-icon-leaf)").addClass("metricsParameterRow");

    updateThresholdsList(resultColumns);
    fixMetricsPanel();
}

function fixMetricsPanel() {
    var width = $("#chartMetrics").width();
    if ($("#chartOptionsHolder > div").width() + 50 < width || $("#chartOptionsHolder > div").height() == 0) {
        $("#chartOptionsHolder > div").css('width', (width + 2).toString() + 'px');
        $("#chartOptionsHolder > div").css('height', '342');
        $("#chartOptionsHolder > div > div").css('width', (width + 2).toString() + 'px');
        $("#chartOptionsHolder > div > div::nth-child(2)").css('height', '342');
        $("#chartOptionsHolder > div > div::nth-child(2) > div").css('height', '340');
        $("#chartOptionsHolder > div > div::nth-child(2) > div").css('width', (width + 1).toString() + 'px');
    }
}

function isAxisAllowed(propertyName, rec) {
    var column = getResColumnByPropertyName(lastResultColumns, propertyName);
    var visibleAxes = [];
    for (var i = 2; i < resultsChart.yAxis.length; i++) {
        var hasVisibleSeries = false;
        for (var k = 0; k < resultsChart.yAxis[i].series.length; k++) {
            if (resultsChart.yAxis[i].series[k].visible) {
                hasVisibleSeries = true;
                break;
            }
        }
        if (hasVisibleSeries) {
            if (resultsChart.yAxis[i].userOptions.chartAxis == column.ChartAxis) {
                return true;
            }
            visibleAxes.push(resultsChart.yAxis[i]);
        }
    }
    if (visibleAxes.length < 2) {
        return true;
    }

    var visible1 = 0;
    for (var j = 0; j < visibleAxes[0].series.length; j++) {
        if (visibleAxes[0].series[j].visible) {
            visible1++;
        }
    }

    var visible2 = 0;
    for (j = 0; j < visibleAxes[1].series.length; j++) {
        if (visibleAxes[1].series[j].visible) {
            visible2++;
        }
    }

    var helpLink;
    $.ajax({
        type: "POST",
        url: "/Orion/Toolset/Services/Toolset.asmx/GetHelpLink",
        data: JSON.stringify({ fragment: 'ToolsetOrionCharts' }),
        processData: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            helpLink = msg.d;
        },
        error: function (msg) {
            return null;
        }
    });
    var message = 'You\'re trying to select "' + rec.raw.displayName + '" metric(s).<br>' +
         'The chart can display only 2 types of metrics simultaneously. To able to select the metric(s), first deselect one of the following types:\n' +
             '<ul><li>' + visibleAxes[0].options.title.text + '(' + visible1 + ')items</li>' +
             '<li>' + visibleAxes[1].options.title.text + '(' + visible2 + ')items</li></ul><br>' +
            '<a href=' + helpLink + ' target="_blank">» Read more about chart limitations</a>';
    Ext42.MessageBox.confirm({
        title: 'The chart supports only 2 types of metrics',
        width: 400,
        msg: message,
        buttons: Ext42.MessageBox.OK,
        icon: Ext42.MessageBox.WARNING
    });

    return false;
}

function performMetricsCheckLeaf(rec, checked) {
    if (checked) {
        if (!isAxisAllowed(rec.raw.propertyName, rec)) {
            rec.set('checked', false);
            return false;
        }
        if (usedMetrics < maxSeriesCount) {
            addSeries(rec.raw.hostaddress, rec.raw.deviceName, rec.raw.propertyName, rec);
            if (usedMetrics == maxSeriesCount) {
                showMaxMetricsReached();
            }
        } else {
            rec.set('checked', false);
            return false;
        }
    } else {
        removeSeries(rec);
        if (usedMetrics == maxSeriesCount - 1) {
            hideMaxMetricsReached();
        }
    }
    return true;
}

function performMetricsCheck(rec) {
    newSeriesWasAdded = false;

    var checked = rec.get('checked');
    if (rec.raw.leaf) {
        if (!isTheSameMetricsType) {
            appendTypeNum($('tr[data-recordid="' + rec.internalId + '"] > td > div'), rec.raw.axis);
        }
        performMetricsCheckLeaf(rec, checked);
    } else {
        if (usedMetrics == maxSeriesCount) {
            rec.set('checked', false);
            showMaxMetricsReached();
        }
        else {
            for (var i = 0; i < rec.childNodes.length; i++) {
                if (rec.childNodes[i].get('checked') && checked) {
                    continue;
                }
                rec.childNodes[i].set('checked', checked);
                if (!isTheSameMetricsType) {
                    appendTypeNum($('tr[data-recordid="' + rec.childNodes[i].internalId + '"] > td > div'),
                        rec.childNodes[i].raw.axis);
                }
                if (!performMetricsCheckLeaf(rec.childNodes[i], checked)) {
                    break;
                }
            }
        }
    }
    resultsChart.render();
    if (chartGridObj.grid) {
        updateDetailsTable();
    }
    $("#chartOptionsHolder div:has(>.x42-tree-expander)").addClass("metricsDeviceRow");
    $("#chartOptionsHolder div:has(>.x42-tree-icon-leaf)").addClass("metricsParameterRow");
    fixMetricsPanel();
}

function appendTypeNum(el, num) {
    if (el.html().indexOf("type #") < 0) {
        el.append('<span class="metricTypeValue">(type #' + num + ')</span>');
    }
}

function showMaxMetricsReached() {
    var position = $("#highcharts-0").offset();
    var leftOffset = 170;
    var width = resultsChart.plotSizeX - leftOffset;
    if (originalMaxSeriesWidth < 0)
        originalMaxSeriesWidth = $("#maxSeriesMessage").width();
    var messageWidth = Math.min(originalMaxSeriesWidth, width);
    $("#maxSeriesMessage").width(messageWidth);

    $("#maxSeriesMessage").css("top", position.top);
    $("#maxSeriesMessage").css("left", leftOffset + (width - $("#maxSeriesMessage").width()) / 2);
    $("#maxSeriesMessage").show();
}

function hideMaxMetricsReached() {
    $("#maxSeriesMessage").hide();
}

function tooltipFormatter() {
    var s = Highcharts.dateFormat('%A, %b %e, %Y %l:%M %p', this.x) + '<br/>'; ;

    $.each(this.points, function (i, point) {
        s += '<br/><div class="tooltipSeriesName" style="width: 500px; color:' + point.series.color + '">' + point.series.name + '</div>' +
            '<div> : ' + getMobileDataRenderer(point.y, null, null, null, null, null, null, getColumnSettingsByPropertyName(point.series.options.propertyName)) + '</div>';
    });

    return s;
}

function getColumnSettingsByPropertyName(name) {
    if (lastResultColumns) {
        for (var i = 0; i < lastResultColumns.length; i++) {
            if (lastResultColumns[i].PropertyName == name) {
                return lastResultColumns[i];
            }
        }
    }
    return null;
}

var firstZoomOut = true;
var lastResultsReceived;
var initialRenderDone = false;
var LastTimeMeticRefresh = 0;

//add live results to chart asynchronously
function addResultToChartAsync(newResult, serverTimeMsSinceEpoch, resultColumnsPromise) {

    if (chartContainerDiv) {
        if (chartContainerDiv.css('display') == 'none') {
            return $.Deferred().resolve();
        } else {
            chartContainerDiv = null;
        }
    }

    var addResults = function (data) {
        var resultColumns = data.columns;
        var serverTime = data.timestamp;
        var maxConst = -900719925474099;
        var max = maxConst;
        var rerender = false;

        if (resColumnsChanged(resultColumns)) {
            lastResultColumns = resultColumns;
            initializeChartView(lastItemsToPoll, lastResultColumns);
            firstZoomOut = true;
        }
        else {
            LastTimeMeticRefresh = LastTimeMeticRefresh + 1;
            if (LastTimeMeticRefresh > 4) {

                var metricsData1 = getMetricsData(lastItemsToPoll, lastResultColumns);

                if (typeof metricsData1 != 'undefined' && metricsData1.length > 0) {

                    LastTimeMeticRefresh = 0;

                    var MetricDeviceList = $("#chartOptionsHolder").find("span[class='x42-tree-node-text ']");
                    for (var i = 0; i < MetricDeviceList.children.length; i++) {
                        for (j = 0; j < MetricDeviceList.length; j++) {
                            if (MetricDeviceList[j].children.length != 0) {
                                var myRegexp = /hostname="[^"]*"/g;
                                var match = myRegexp.exec(metricsData1[i].text);                              
                                var searchString = match[0];
                                if (MetricDeviceList[j].innerHTML.indexOf(searchString) > -1) {
                                    MetricDeviceList[j].innerHTML = metricsData1[i].text;
                                }

                            }

                        }
                    }
                }
            }
        }
        lastResultsReceived = newResult;
        if (resultsChart == null) return;

        for (var i = 0; i < newResult.length; i++) {
            if (!newResult[i]) {
                continue;
            }
            for (var k = 0; k < resultColumns.length; k++) {
                if (resultColumns[k].SupportCharts) {
                    var seriesId;
                    if (toolInterfaceMonitor) {
                        seriesId = getSeriesId(newResult[i].HostAddress, newResult[i].InterfaceIndex, resultColumns[k].PropertyName);
                    } else {
                        seriesId = getSeriesId(newResult[i].HostAddress, newResult[i].DisplayName, resultColumns[k].PropertyName);
                    }
                    for (var j = 0; j < chartSettings.series.length; j++) {
                        if (chartSettings.series[j].id == seriesId) {
                            var value = newResult[i][resultColumns[k].PropertyName];
                            if (value > max) max = value;
                            chartSettings.series[j].highSeries.addPoint([serverTime, value], false);
                            rerender = true;
                        }
                    }
                }
            }
        }

        if (rerender) {
            resultsChart.series[0].addPoint([serverTime, max], false); // [pc] set this to false so that we render the chart only once per pass.

            if (firstZoomOut) {
                firstZoomOut = false;
                resultsChart.xAxis[0].setExtremes(serverTime, serverTime);
                $('tspan, span', resultsChart.container).click(function (e) {
                    if (($(e.target).text() == '1M') ||
                    ($(e.target).text() == '5M') ||
                    ($(e.target).text() == 'All')) {
                        resultsChart.redraw(); // render on click is not a performance issue
                    }
                });
            }
            if (axisWithPlotBand == null && $("#thresholdsCombo").val() != 'none') {
                thresholdsComboItemChanged();
            }
        }

        if (axisWithPlotBand) {
            var resCol = axisWithPlotBand.resColumn;
            var valueTo = (resCol.CriticalThresholdOperator <= 1) ?
            (resCol.CriticalThresholdValue > axisWithPlotBand.max ? resCol.CriticalThresholdValue : axisWithPlotBand.max) :
            (resCol.CriticalThresholdValue < axisWithPlotBand.min ? resCol.CriticalThresholdValue : axisWithPlotBand.min);
            if (axisWithPlotBand.plotLinesAndBands[1]) {
                axisWithPlotBand.plotLinesAndBands[1].options.to = valueTo;
            } else {
                //add critical plotband
                axisWithPlotBand.addPlotBand({
                    from: resCol.CriticalThresholdValue,
                    to: valueTo,
                    color: 'rgba(255, 0, 0, 0.2)',
                    id: 'criticalPlotBand',
                    label: {
                        text: 'Critical threshold',
                        style: {
                            color: '#808080'
                        }
                    }
                });
            }
        }

        if (rerender) {
            if (!initialRenderDone) { initialRenderDone = true; resultsChart.render(); } // [pc] apparently, external code to this function expects the chart to render and have a size / dom objects.
            else SW.Toolset.Visibility.CallWhenVisible('chart', function () { resultsChart.redraw(); }); // [pc] call this only when the tab is visible or becomes visible.

            applyDataRetention(serverTime);
            updateDetailsTable();
        }
    };

    //prepare result columns and a timestamp of the new results asynchronously
    resultColumnsPromise = resultColumnsPromise.pipe(function (data) { return { columns: data.d, timestamp: serverTimeMsSinceEpoch }; });

    //when ready, add the data to the chart
    return $.when(resultColumnsPromise)
        .pipe(addResults);
}

function changeSeriesColor(series, color) {
    series.color = color;
    series.graph.attr({
        stroke: color
    });
}

function showNoDataMessage() {
    resultsChart.showLoading("Select metrics to monitor");
}

function openSelectMetrics() {
    if (settingsPageName) {
        window.open(settingsPageName);
    }
}

var chartGridObj = {
    grid: null,
    gridColWidth: null,
    draggable: false,
    sortable: false,
    menuDisabled: true
};

var chartDetailsData;

//create table with details
function updateDetailsTable() {

    chartDetailsData = getDetailsData();
    if (IsMobile()) {
SW.Toolset.Visibility.CallWhenVisible( 'chart-grid', function(){
        CreateResultGridMobile(detailsResultColumns, chartDetailsData, 'chartDetailsTable', "35rem", "95%", customCellRendererMobile, chartGridObj, true);
});
    } else {
SW.Toolset.Visibility.CallWhenVisible( 'chart-grid', function(){
        CreateResultGrid(detailsResultColumns, chartDetailsData, 'chartDetailsTable', 250, 1000, customCellRenderer, chartGridObj);
        //Fix FB328977 - metric options are hidden when as new result is arrived
        if (ToolsetChartUserActions.MetricUpdatedByUser == true && $("#chartOptionsHolder > div").height() === 0) {//keep checking for the height from when the user has updated the metric (selected/deselected). The metrics panel is getting hidden on second grid.getview().refresh()
            ToolsetChartUserActions.MetricUpdatedByUser = false;
            fixMetricsPanel();
        }
});

    }
}

function customCellRenderer(value, metaData, record, row, col, store, gridView) {
    switch (detailsResultColumns[col].ColumnType) {
        case 0:
            return getMobileDataRenderer(value, metaData, record, row, col, store, gridView, store.data.items[row].raw.columnSettings);
        case 100:
            return value === null ? "" : value.toFixed(2) + '% of time';
        case 101:
            return '<span style="font-weight: bold; color: ' + store.data.items[row].raw.color + '">' + value + '</span>';
        case 102:
            return GetByteString(value, metaData, record, row, col, store, gridView);
        default:
            return value;
    }
}

function customCellRendererMobile(value, metaData, record, row, col, store, gridView) {
    switch (detailsResultColumns[row % detailsResultColumns.length].ColumnType) {
        case 0:
            return getMobileDataRenderer(value, metaData, record, row, col, store, gridView,
                        chartDetailsData[(row - (row % 8)) / 8].columnSettings);
        case 100:
            return value === null ? "" : value.toFixed(2) + '% of time';
        case 101:
            return '<span style="font-weight: bold; color: ' + chartDetailsData[row / 8].color + '">' + value + '</span>';
        case 102:
            return GetByteString(value, metaData, record, row, col, store, gridView);
        default:
            return value;
    }
}

//ToDo: make async
function getDetailsData() {
    var detailsData = [];
    if (!resultsChart) return detailsData;

    //get captions/statuses/urls for all visible series and store them in deviceNames
    var renderedNodes = [];
    for (var i = 2; i < resultsChart.series.length; i++) {
        if (resultsChart.series[i].visible) {
            renderedNodes.push(resultsChart.series[i].options.hostAddress);
        }
    }

    var nodeNames = getNodeNames(renderedNodes);
    var deviceNames = [];
    for (i = 2; i < resultsChart.series.length; i++) {
        var series = resultsChart.series[i];
        if (!series.visible) {
            continue;
        }
        for (var j = 0; j < nodeNames.length; j++) {
            if (nodeNames[j].IP == series.options.hostAddress) {
                if (toolResponseMonitor) {
                    deviceNames.push(nodeNames[j].HTML);
                } else {
                    deviceNames.push(series.options.deviceFullName + ' on ' + nodeNames[j].HTML);
                }
                break;
            }
        }
    }

    var k = 0;
    for (i = 2; i < resultsChart.series.length; i++) {
        series = resultsChart.series[i];
        if (!series.visible || series.yData.length == 0) {
            continue;
        }
        var max = 0, min = 0, avg = 0;
            max = series.yData[0] ? series.yData[0] : 0;
            min = series.yData[0] ? series.yData[0] : 0;
            avg = 0;

        var resCol = getResColumnByPropertyName(lastResultColumns, series.options.propertyName);
        var warningCount = 0,
            criticalCount = 0;
        var multiplier = resCol.ValueType == 8 ? 1048576 : 1;
        for (j = 0; j < series.yData.length; j++) {
            var y = series.yData[j] || 0; //treat null points as zero

            if (min > y) {
                min = y;
            }
            if (max < y) {
                max = y;
            }

            avg += y / series.yData.length;
            if (resCol.UseThreshold) {
                if (IsValueCritical(y, resCol, multiplier)) {
                    criticalCount++;
                }
                if (IsValueWarning(y, resCol, multiplier)) {
                    warningCount++;
                }
            }
        }

        var detailsRecord = {
            displayName: resCol.DisplayName,
            deviceName: deviceNames[k],
            currentValue: series.yData[series.yData.length - 1],
            maxValue: Number(max.toFixed(2)),
            minValue: Number(min.toFixed(2)),
            avgValue: Number(avg.toFixed(2)),
            warningState: resCol.UseThreshold ? Number((warningCount * 100.0 / series.yData.length).toFixed(2)) : null,
            criticalState: resCol.UseThreshold ? Number((criticalCount * 100.0 / series.yData.length).toFixed(2)) : null,
            columnSettings: resCol,
            color: series.color,
            seriesId: series.options.id
        };
        k++;
        if (toolInterfaceMonitor) {
            if (resCol.PropertyName.indexOf("Bandwidth") >= 0) {
                switch (resCol.PropertyName) {
                    case 'BandwidthRx':
                        detailsRecord.totalBytesTransfered = getTotalBytesReceived(series.options.id, "BandwidthRx", "TotalBytesTransferredRx");
                        break;
                    case 'BandwidthRxTx':
                        detailsRecord.totalBytesTransfered = getTotalBytesReceived(series.options.id, "BandwidthRxTx", "TotalBytesTransferredRxTx");
                        break;
                    case 'BandwidthTx':
                        detailsRecord.totalBytesTransfered = getTotalBytesReceived(series.options.id, "BandwidthTx", "TotalBytesTransferredTx");
                        break;
                }
            }
        }
        detailsData.push(detailsRecord);
    }
    return detailsData;
}

function getTotalBytesReceived(seriesId, parameterIn, parameterOut) {
    for (var i = 0; i < lastResultsReceived.length; i++) {
        if (seriesId == getSeriesId(lastResultsReceived[i].HostAddress, lastResultsReceived[i].InterfaceIndex, parameterIn)) {
            return lastResultsReceived[i][parameterOut];
        }
    }
    return 0;
}

function applyDataRetention(serverTime) {
    if (!ChartDataLimitByTimeMin && !ChartDataLimitByPoints && !ChartDataLimitType) {
        return;
    }

    var needsRedraw = false;

    //removes first point from a series, using Point.remove() if available
    var removeFirstPoint = function(series) {
        if (series.data[0]) {
            series.data[0].remove(false);
        } else {
            series.options.data.splice(0, 1);
            series.xData.splice(0, 1);
            series.yData.splice(0, 1);
        }
        series.isDirty = true;
        series.isDataDirty = true;
        needsRedraw = true;
    }

    //gets the series with the leftmost point in the chart
    var getOldestSeries = function() {
        var oldest = null;

        for (var l = 2; l < resultsChart.series.length; l++) {
            if (resultsChart.series[l].xData.length > 0) {
                if (oldest) {
                    if (oldest.xData[0] > resultsChart.series[l].xData[0]) {
                        oldest = resultsChart.series[l];
                    }
                } else {
                    oldest = resultsChart.series[l];
                }
            }
        }
        return oldest;
    };

    //no need to redraw for every single removed point
    Ext42.suspendLayouts();

    if (ChartDataLimitType == 0) {
        //limit by time 
        var minDate = new Date(serverTime - ChartDataLimitByTimeMin * 60000).getTime();
        for (var i = 2; i < resultsChart.series.length; i++) {
            while (resultsChart.series[i].xData.length > 0 && resultsChart.series[i].xData[0] < minDate) {
                removeFirstPoint(resultsChart.series[i]);
            }
        }

        while (resultsChart.series[0].xData.length > 0 && resultsChart.series[0].xData[0] < minDate) {
            removeFirstPoint(resultsChart.series[0]);
        }

    } else {
        //limit by series points
        var count = 0;
        for (var j = resultsChart.series.length - 1; j >= 2; j--) {
            count += resultsChart.series[j].xData.length;
        }

        count -= ChartDataLimitByPoints;
        if (count > 0) {
            var oldestSeries = getOldestSeries();
            for (var k = 0; k < count; k++) {
                if (oldestSeries) {
                    removeFirstPoint(oldestSeries);
                    oldestSeries = getOldestSeries();
                    if (oldestSeries == null || resultsChart.series[0].xData[0] < oldestSeries.xData[0]) {
                        removeFirstPoint(resultsChart.series[0]);
                    }
                }
            }
        }
    }

    Ext42.resumeLayouts();
    if (needsRedraw) {
        SW.Toolset.Visibility.CallWhenVisible('chart', function () { resultsChart.redraw(); });
    }
}

var currentViewType;

function getCurrentViewType() {
    if (!currentViewType) {
        currentViewType = $("#viewTypeSelect").val();
    }
    return currentViewType;
}

function isTableView() {
    return (getCurrentViewType() === "table");
}

function isChartView() {
    return (getCurrentViewType() === "chart");
}

function setTableView() {
    currentViewType = "table";
}

function setChartView() {
    currentViewType = "chart";
    if (turnOnFirstMetric) {
        var rec = metrictree.getStore().getRootNode().childNodes[0].childNodes[0];
        rec.set('checked', true);
        performMetricsCheck(rec);
        turnOnFirstMetric = false;
    }
}
