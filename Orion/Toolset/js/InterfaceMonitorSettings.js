﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
var saveChanges;
var cancelChanges;
var selectorModelForAll;
var selectorModelForSelected;
var allDataStore;
var selectedDataStore;
var gridAll;
var gridSelected;
var tree;

var GridRecord = Ext.data.Record.create(['Name', 'DisplayName', 'Header','SubHeader', 'ShortName']);
addSelectedColumns = function() {
    var selections = tree.getChecked();
    for (var i = 0; i < selections.length; i++) {
        if (!selections[i].hidden) {
            if (selections[i].isLeaf()) {
                var record = new GridRecord({
                    Name: selections[i].attributes['name'],
                    DisplayName: selections[i].attributes['displayName'],
                    Header: selections[i].attributes['header'],
                    SubHeader: selections[i].attributes['subHeader'],
                    ShortName: selections[i].attributes['shortName']
                });
                gridSelected.getStore().add(record);
            }

            selections[i].ui.toggleCheck(false);
            selections[i].ui.hide();

            selections[i].parentNode.attributes.childsNumber -= 1;
            var count = selections[i].parentNode.attributes.childsNumber;
            if (selections[i].parentNode.ui.textNode)
                selections[i].parentNode.ui.textNode.innerHTML = selections[i].parentNode.attributes.displayName + ' (' + count + ')';
        }
    }
};
function removeSelectedColumns() {
    var selections = gridSelected.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        var header = selections[i].data['Header'];
        var subHeader = selections[i].data['SubHeader'];
        var name = selections[i].data['Name'];
        //searching header
        var node = tree.root.findChild('displayName',header);
        node.ui.show();
        node.childNodes.forEach(function(n){
            if(n.attributes.name === name){
                //Add childsNumber
                node.ui.textNode.innerHTML = node.attributes.displayName +' (' + ++node.attributes.childsNumber + ')';
                n.ui.show();
            }
        });
        //searching subHeader    
        if (subHeader) {
            node.ui.toggleCheck();
            node.ui.toggleCheck();
            var subNode = node.findChild('displayName', subHeader);
            if(subNode.hidden)
                node.ui.textNode.innerHTML = node.attributes.displayName +' (' + ++node.attributes.childsNumber + ')';
            subNode.ui.show();
            subNode.ui.toggleCheck();   
            subNode.ui.toggleCheck();       
            subNode.childNodes.forEach(function(n) {
                if (n.attributes.name === name) {
                    subNode.ui.textNode.innerHTML = subNode.attributes.displayName +' (' + ++subNode.attributes.childsNumber + ')'; 
                    n.ui.show();
                }
            });    
        }
        
        gridSelected.getStore().remove(selections[i]);
    }
}
function saveSettings(obj) {
    ORION.callWebService("/Orion/Toolset/Services/InterfaceMonitorSettings.asmx", "SaveSettings", {
            table: Ext.encode(Ext.pluck(selectedDataStore.data.items, 'data'))
        }, function(result) {
            if (!result) {
                $("#successMsg").css("display", "block");
                setTimeout(function () {
                    $("#successMsg").css("display", "none");
                }, 3000);
                $("#failedMsg").css("display", "none");
                if (obj !== "dummy") window.location.replace(obj);
            } else {
                $("#successMsg").css("display", "none");
                $("#failedMsg").css("display", "block");
                $("#errorMsg").html("An Error occured during saving. " + result);
            }
        });
}

function getURLParameter(name) {
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
        );
    };

ToolsetSettingsManager = function() {

    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    function renderColumn(value, meta, record) {
        return encodeHTML(value);
    };  

    function selectTreeColumns(all) {
       tree.getRootNode().cascade(function(n) {
            var ui = n.getUI();
            ui.toggleCheck(all);
        });
        if(!all)
        tree.collapseAll();
    }
    
    function selectColumns(all, selectionModel) {
        if (all) {
            selectionModel.selectAll();
        } else
            selectionModel.clearSelections();
    }
        
    function moveSelectedRow(grid, direction) {
        var record = grid.getSelectionModel().getSelected();
        if (!record) {
            return;
        }
        var index = grid.getStore().indexOf(record);
        if (direction < 0) {
            index--;
            if (index < 0) {
                return;
            }
        } else {
            index++;
            if (index >= grid.getStore().getCount()) {
                return;
            }
        }
        grid.getStore().remove(record);
        grid.getStore().insert(index, record);
        grid.getSelectionModel().selectRow(index, true);
    }

    return {
        reload: function() { //gridAll.getStore().reload(); 
        },
        init: function() {          
            // define grid panel
            function GetResultColumns(serviceUrl, method, passData) {
        var resultColumns;
        $.ajax({
            type: method,
            url: serviceUrl,
            data: passData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                resultColumns = data.d;
            },
            error: function (data, textStatus, xhr) {
                alert(xhr);
            }
        });
        return resultColumns;
    }

    var currentTreeData = GetResultColumns("/Orion/Toolset/Services/InterfaceMonitorSettings.asmx/GetTreeGridCollumn", "POST", {});

    tree = new Ext.tree.TreePanel({
        renderTo: 'Grid',
        collapsible: true,
        style: 'padding-bottom: 5px',
        loader: new Ext.tree.TreeLoader(),
        rootVisible: false,
        //useArrows:true, //Doesn't supporn sizing
        autoScroll: true,
        lines: false,        
        root: new Ext.tree.TreeNode({
            children: currentTreeData,
        }),
        hideHeaders: true,
                width: 300,
                height: 400,
                frame: true,
                bodyStyle: 'background-color:#fff;',
                title: 'Available Metrics',
                tbar: [
                    {
                        id: 'selectAllButtonAva',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectTreeColumns(true); }
                    }, '-',
                    {
                        id: 'selectNoneButtonAva',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectTreeColumns(false); }
                    },
                    {
                        xtype: 'checkbox',
                        name: 'SelectAll',
                        boxLabel: 'Select/Deselect All',
                        iconCls: '',
                        id: 'cbSelectAll',
                        listeners: { check: function() { var checked = Ext.getDom('cbSelectAll').checked;  selectTreeColumns(checked); } },
                    }
                ],
                bbar: [
                    {
                        id: 'addSelected',
                        text: 'Add Selected Metrics',
                        handler: function() { addSelectedColumns(); }
                    }
                ]
        });
            
    function checkChildNodes(node, checked) {
        node.expand(checked);
        node.eachChild(function (n) {
            n.getUI().toggleCheck(checked);
            if (n.hasChildNodes) {
                checkChildNodes(n, checked);
            }
        });
        if(!checked)
            node.collapse();
    }
    tree.on('checkchange', function (node, checked) { 
        checkChildNodes(node, checked);
    });

    tree.getLoader().load(tree.root);
    selectorModelForSelected = new Ext.grid.CheckboxSelectionModel();

    selectedDataStore = new ORION.WebServiceStore(
        "/Orion/Toolset/Services/InterfaceMonitorSettings.asmx/GetSelectedColumns",
        [
            { name: 'Name', mapping: 0 },
            { name: 'DisplayName', mapping: 1 },
            { name: 'Header', mapping: 2},
            { name: 'SubHeader', mapping: 3},
            { name: 'ShortName', mapping: 4}
        ],
        "");

        // define grid panel
        gridSelected = new Ext.grid.GridPanel({
            store: selectedDataStore,
            columns: [
                selectorModelForSelected,
                { header: 'Name', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'Name' },
                { header: 'DisplayName', dataIndex: 'DisplayName', width: 798, sortable: false, menuDisabled: true, render: renderColumn },
                { header: 'Header', dataIndex: 'Header', hideable: false, hidden: true},
                { header: 'ShortName', dataIndex: 'ShortName', hideable:false, hidden: true}
            ],
            frame: true,
            bodyStyle: 'background-color:#fff;',
            hideHeaders: true,
            sm: selectorModelForSelected,
            viewConfig: {
                forceFit: true,
                scrollOffset: 0,
                hideSortIcons: true,
                stripeRows: false
            },
            autoScroll: true,
            width: 800,
            height: 400,
            title: 'Selected Metrics',
            tbar: [
                {
                    id: 'SelectAll',
                    text: 'Select All',
                    iconCls: 'selectall',
                    handler: function() { selectColumns(true, selectorModelForSelected); }
                }, '-',
                {
                    id: 'SelectNone',
                    text: 'Select None',
                    iconCls: 'selectnone',
                    handler: function() { selectColumns(false, selectorModelForSelected); }
                }, '-',
                {
                    id: 'MoveUpButton',
                    text: 'Move Up',
                    iconCls: 'moveup',
                    handler: function() { moveSelectedRow(gridSelected, -1); }
                },'-',
                {
                    id: 'MoveDownButton',
                    text: 'Move Down',
                    iconCls: 'movedown',
                    handler: function() { moveSelectedRow(gridSelected, 1);}
                },
                {
                    id: 'RemoveSelectedButton',
                    text: 'Remove Selected',
                    iconCls: '',
                    handler: function() { removeSelectedColumns(); }
                },
                {
                    xtype: 'checkbox',
                    name: 'SelectAll',
                    boxLabel: 'Select/Deselect All',
                    iconCls: '',
                    id: 'cbSelectAllSG',
                    listeners: { check: function() { var checked = Ext.getDom('cbSelectAllSG').checked;  selectColumns(checked, selectorModelForSelected); } },
                }
            ],
                
        });

            var cbSelectAllTG = Ext.getCmp('cbSelectAll');
            var btnAddSell = Ext.getCmp('addSelected');
            var btnSell = Ext.getCmp('SelectAll');
            var btnDeSell = Ext.getCmp('SelectNone');
            var btnRemoveSlct = Ext.getCmp('RemoveSelectedButton');
            var cbSelectAllSG = Ext.getCmp('cbSelectAllSG');
            //var btnAddSellAlign = Ext.getCmp('x-toolbar-left');
            
            if (getURLParameter("IsMobileView") === 'null') {
                cbSelectAllSG.hide();
                cbSelectAllTG.container.setStyle('display', 'none');
                btnRemoveSlct.hide();
                btnAddSell.hide();
            } else {
                cbSelectAllTG.container.setStyle('display', 'block');
                btnSell.hide();
                btnDeSell.hide();
                btnRemoveSlct.show();
                cbSelectAllSG.show();
                btnAddSell.show();
                //btnAddSellAlign.set({align :  'center'});
            }

        gridSelected.render('GridSelected');
        selectorModelForSelected.fireEvent('selectionchange');
        gridSelected.store.proxy.conn.jsonData = {};
        gridSelected.store.load();
        }
    };
}();

function SaveChanges() {
    saveChanges();
}

function CancelChanges() {
    cancelChanges();
}

Ext.onReady(ToolsetSettingsManager.init, ToolsetSettingsManager);
