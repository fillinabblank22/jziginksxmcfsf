﻿var hub = $.connection.ToolsetHub;
var resultReceived;
var settingsChanged;
var lastKeepAliveAcknowledgementRequest = new Date();
var debugResultsCnt = 3;
var debugLastResults = [];
var resultCounter = 0;
var isNetworkProblem = false;

$.connection.hub.url = "@{R=Toolset.Startup;K=SignalrPath;E=js}"; // see: SolarWinds.Toolset.Web.Startup

hub.client.ShowAlert = function (taskId) {
    ts.Log.debug('Showing termination alert for task {0} and stopping SignalR connection', taskId);
    if (taskId) {
        if ($('#taskIdTextbox').val() != taskId)
           return;
    }
    debugLastResults = [];
    $.connection.hub.stop();
    ShowDisconnectionAlert();
};

hub.client.StopConnection = function () {
    ts.Log.debug('Stopping SignalR connection');
    $.connection.hub.stop();
};

hub.client.onResultReceived = function (newResult, taskId) {
    ++resultCounter;
    if (SW.Toolset.debugging) {
        pushNewResultToDebugCache(newResult);
        ShowDataReceivedInformation();
        ts.Log.debug('Result #' + resultCounter + ' with Id ' + newResult.ResultId + ' arrived from SignalR');
    }
    if (resultReceived) {
        newResult.CompletionInfo.Events.push({ Text: 'Result received from SignalR', Time: new Date().toJSON() });
        resultReceived(newResult, taskId);
    } else {
        SW.Toolset.Log.error('Result arrived but signalR receiving has not been initialized.');
    }
};

hub.client.onKeepAliveAcknowledgementRequest = function(taskId) {
    var timeStamp = new Date();
    var requestTimeDiff = ts.Log.diffDates(lastKeepAliveAcknowledgementRequest, timeStamp);
    ts.Log.debug("KeepAliveAcknowledgementRequest diff = " + requestTimeDiff);
    lastKeepAliveAcknowledgementRequest = timeStamp;
    ReactToAcknowledgementRequest(taskId, requestTimeDiff, resultCounter);
};

hub.client.onSettingsChanged = function () {
    if (settingsChanged) {
        settingsChanged();
    }
};

function connect(onSuccessCallback, param1, param2, param3, param4) {
    // Do not initialize SignalR if tool limit is reached
    if (SW.Toolset.ToolLimitReached) {
        return;
    }

    $.connection.hub.stateChanged(function (state) {
        if (state.oldState === $.signalR.connectionState.connecting && state.newState === $.signalR.connectionState.connected) {
            if (onSuccessCallback != DoRegisterUserSession) {
                onSuccessCallback(param1, param2, param3, param4);
            }
        }
    });

    $.connection.hub.reconnecting(function () {
        ts.Log.warn('SignalR reconnect');
        isNetworkProblem = true;
    });

    $.connection.hub.reconnected(function () {
        $.connection.hub.stop();
    });

    $.connection.hub.disconnected(function () {
        ts.Log.debug('SignalR disconnected');
        if (isNetworkProblem) {
            ShowLostConnectionAlert();
        }
    });

    $.connection.hub.start().done(function () {
        if ($.connection.hub.state === $.signalR.connectionState.connected) {
            onSuccessCallback(param1, param2, param3, param4);
        }
        isNetworkProblem = false;
    }).fail(function () {
        alert("Connection failed");
    });
}

function IsSignalRConnected() {
    return ($.connection.hub.state === $.signalR.connectionState.connected);
}

function InitResultNotification(taskId, onResultReceived, onSettingsChanged) {
    if (IsSignalRConnected()) {
        DoInitResultNotification(taskId, onResultReceived, onSettingsChanged);
    } else {
        connect(DoInitResultNotification, taskId, onResultReceived, onSettingsChanged);
    }
}

function DoInitResultNotification(taskId, onResultReceived, onSettingsChanged) {
    resultReceived = onResultReceived;
    settingsChanged = onSettingsChanged;
    ts.Log.debug('Initating result notification for task {0}', taskId);
    hub.server.registerTask(taskId);
}

function StopResultNotification(taskId){
    resultReceived = null;
    ts.Log.debug('Stopping result notification for task {0}', taskId);
    if (IsSignalRConnected()) {
        hub.server.unregisterTask(taskId);
    }
}

function ReactToAcknowledgementRequest(taskId, timeDiff, resultCounter) {
    if (!taskId)
        return;
    if (IsSignalRConnected()) {
        hub.server.keepAliveAcknowledgement(taskId, timeDiff, resultCounter);
    }
}

function ShowDataReceivedInformation() {
    if (!SW || !SW.Toolset || !SW.Toolset.debugging)
        return;
    
    setTimeout(function () {
        var timeStamp = 'Last result arrived  #' + resultCounter + ' - ' + new Date().toJSON();
        var taskIdInfo = 'Task ID = ' + $('#taskIdTextbox').val() + '   SessionID = ' + $("#sessionId").val();
        var div = $('#dbgInfoPanel');
        $('#dbgDataReceivedInformation')[0].innerHTML = timeStamp;
        $('#dbgTaskId')[0].innerHTML = taskIdInfo;
        div[0].style.display = "inline";
        div[0].style.visibility = 'visible';
    }, 0);
}

function ShowDisconnectionAlert() {
    ShowStopAlert('@{R=Toolset.Strings;K=ToolsetGUICODE_SL_01;E=js}',
        'Polling has stopped because your session has been terminated. Please close the tool.',
        'Discovery has stopped because your session has been terminated. Please close the tool.',
        'Tracing has stopped because your session has been terminated. Please close the tool.');
};

function ShowLostConnectionAlert() {
    ShowStopAlert('@{R=Toolset.Strings;K=ToolsetGUICODE_PS_01;E=js}',
        'Polling has stopped because of a network issue. Please restart the tool.',
        'Discovery has stopped because of a network issue. Please restart the tool.',
        'Tracing has stopped because of a network issue. Please restart the tool.');
};

function ShowStopAlert(alertMsg, pollingMsg, discoveryMsg, tracingMsg) {
    alert(alertMsg);
    $('#isSessionTerminated').val('true');
    if ($('#spinnerDiv') != null) {
        $('#spinnerDiv').hide();
    }
    if ($('#stopSpinnerDiv') != null) {
        $('#stopSpinnerDiv span:first-child').text(pollingMsg);
        $('#stopSpinnerDiv span:first-child').css('color', 'red');
        $('#stopSpinnerDiv').show();
    }
    if ($('#loading-cancel-div') != null) {
        $('#loading-cancel-div').hide();
    }
    if ($('#restart-div') != null) {
        $('#restart-div #discStopMsg').text(discoveryMsg);
        $('#restart-div #discStopMsg').css('color', 'red');
        $('#restart-div #traceStopMsg').text(tracingMsg);
        $('#restart-div #traceStopMsg').css('color', 'red');
        $('#restart-div').show();
    }
};

function pushNewResultToDebugCache(result) {
    if (debugLastResults.length < debugResultsCnt) {
        debugLastResults.push(result);
    } else {
        debugLastResults[resultCounter % debugResultsCnt] = result;
    }
};

function debugDoWithDrawSupression(action) {
    var redrawOrig = resultsChart.redraw;
    resultsChart.redraw = function() {};

    Ext42.suspendLayouts();
    action();
    Ext42.resumeLayouts();

    resultsChart.redraw = redrawOrig;
    resultsChart.redraw();
}

function debugCopyResults(numberTimes, timeShiftMinutes) {
    if (debugLastResults.length < debugResultsCnt) {
        ts.Log.warn("!Has not received {0} results, can't to copy", debugResultsCnt);
        if (!SW.Toolset.debugging) {
            ts.Log.info("!SW.Toolset.debugging switched on, wait for {0} results .... result#{1}", debugResultsCnt, resultCounter + debugResultsCnt);
            SW.Toolset.debugging = 1;
        }
        return;
    }
    var origLogLevel = ts.LogLevel;
    ts.Log.info('debugCopyResults - start - {0} times, time shift: {1} minutes', numberTimes, timeShiftMinutes);

    //disable log too often - bug in Chrome/Opera https://code.google.com/p/chromium/issues/detail?id=352557
    ts.LogLevel = ts.LogLevelEnum.error;    
    debugDoWithDrawSupression(function() {
        var startDate = new Date().getTime() + 60000 * timeShiftMinutes;
        var shiftRatio = 60000 * timeShiftMinutes / numberTimes;
        var progress = 0;
        for (var i = 1; i < numberTimes; ++i) {
            var p = Math.floor(i / numberTimes * 100) % 10 * 10;
            if (p > progress) {
                progress = p;
                ts.Log.info('!Done ' + p + ' %.');
            }
            var result = debugLastResults[i % debugResultsCnt];
            result.CompletionInfo.CompletionTimeMsSinceEpoch = startDate + i * shiftRatio;
            resultReceived(result);
        }
        ts.Log.info('!Done 100 %.');
    });    
    ts.LogLevel = origLogLevel;
    ts.Log.info('debugCopyResults - end');
}