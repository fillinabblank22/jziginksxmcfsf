﻿function InitResultNotificationClient(taskId, resultSink, settingsChanged) {
    InitResultNotification(taskId, resultSink, settingsChanged);
}

function StopResultNotificationClient(taskId) {
    StopResultNotification(taskId);
};

function IsValidResultsReceived(results) {
    if (results.length > 0) {
        if (!results[0].DeviceName && !results[0].HostAddress) {
            return false;
        }
        return true;
    }
    return false;
}