(function(){

  var ns = SW.Core.namespace('SW.Toolset.Visibility'), 
      isHidden = 0,
      callbacks = [], 
      callCache = {},
      mask = {},
      skip = {},
      pos = 0,
      hiddenProp = "hidden",
      start = new Date().getTime();

  var refresh = function(){ $(callbacks).each(function(i,obj){ obj(isHidden); }); };
  var unhide = function(){ if(isHidden!=1) return; isHidden = 0; refresh(); };
  var hide = function(){ if(isHidden!=0) return; isHidden = 1; refresh(); };
  var log = SW.Toolset.Log.info;
  var debugLog = SW.Toolset.Log.debug;
  var traceLog = SW.Toolset.Log.trace;

  ns.IsHidden = function(){ return isHidden; };
  ns.Hide = hide;
  ns.Show = unhide;
  ns.Init = function(cb){ if(typeof(cb) == "function" ) callbacks.push(cb); };
  ns.Mask = ns._Mask = function(id,enable) { if(!id) mask = {}; else mask[id||''] = !enable; };
  ns.Skip = function(id,enable) { if(!id) skip = {}; else skip[id||''] = enable;};
  ns.Force = function(id){ id=id||''; var obj = callCache[id]; if( !obj ) return 0; log('queue-force['+obj.tag+']'); try{ obj.fn.apply( this, obj.args ); } catch(err){} delete callCache[id]; return 1; };

  var resume = function(isHidden){
    if(isHidden) return;

    var ordered = [];
    var that = this;
    for( k in callCache ) { var o = callCache[k]; if( !o || !o.fn ) return; ordered.push(o); } // v.fn is needed in case somebody extends object (bad!).
    callCache = {};
    pos = 0;
    ordered.sort(function(a,b){ return a.pos-b.pos; });
    $(ordered).each(function(i,obj){ try { 

      if( mask[obj.id] ) {
          traceLog('queue-delay[' + obj.tag + ']');
        obj.pos = ++pos;
        callCache[obj.tag] = obj;
        return;
      }

      traceLog('queue-resume[' + obj.tag + ']');
      obj.fn.apply( that, obj.args ); 
    } catch(err) {} });
  };

  ns.CallWhenVisible = function(tag,fn){
   var args = Array.prototype.slice.call(arguments, 2);

   if(skip[tag]) {
       traceLog('queue-skip[' + tag + ']');
    return 0;
   }

   if( !isHidden && !mask[tag] ) {

       traceLog('queue-immediate[' + tag + ']');
     fn.apply( this, args );

     return 1;
   }

   traceLog('queue-delay[' + tag + ']');
   callCache[tag] = { pos: ++pos, tag: tag, fn: fn, args: args };
   return 0;
  };

  var detectchange = function(){
    var state = !!(document[hiddenProp]);
    if( state == isHidden ) return;
    isHidden = state;
    debugLog( 'visibility-change {0}', state ? 'hide' : 'show');
    if( isHidden ) hide();
    else unhide();
  }

  if (hiddenProp in document) document.addEventListener("visibilitychange", detectchange); // <-- standards detection
  else if ((hiddenProp = "mozHidden") in document) document.addEventListener("mozvisibilitychange", detectchange);
  else if ((hiddenProp = "webkitHidden") in document) document.addEventListener("webkitvisibilitychange", detectchange);
  else if ((hiddenProp = "msHidden") in document) document.addEventListener("msvisibilitychange", detectchange);

  callbacks.push(resume);

})();
