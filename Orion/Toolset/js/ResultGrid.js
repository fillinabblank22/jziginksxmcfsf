﻿var toolResponseMonitor;
var toolTraceRoute;
var columnWidth;
function GetJSONData(serviceUrl, method, passData) {
    var result;
    $.ajax({
        type: method,
        url: serviceUrl,
        data: passData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            result = data.d;
        },
        error: function (data, textStatus, xhr) {
            alert(xhr);
        }
    });
    return result;
}
function getRenderer(columnSetting) {
    if (columnSetting.UseThreshold == true) {
        switch (columnSetting.ColumnType) {
            case 0: //format value
                switch (columnSetting.ValueType) {
                    case 0: //none
                        return GetThresholdString;
                    case 1: //percent
                        return GetPercentThresholdString;
                    case 2: //byte
                        return GetByteThresholdString;
                    case 3: //bit
                        return GetBitThresholdString;
                    case 4: //Packets
                        return GetPacketsThresholdString;
                    case 5: //Packets per second
                        return GetPacketsPerSecThresholdString;
                    case 8: // compare with megabytes
                        return GetMegaByteThresholdString;
                    case 9: // miliseconds
                        return GetMiliSecondThresholdString;
                    default:
                }
            default:
                return null;
        }
    } else {
        switch (columnSetting.ColumnType) {
            case 0: //format value
                switch (columnSetting.ValueType) {
                    case 0: //none
                        return null;
                    case 1: //percent
                        return GetPercentString;
                    case 2: //byte
                        return GetByteString;
                    case 3: //bit
                        return GetBitString;
                    case 4: //Packets
                        return GetPacketsString;
                    case 5: //Packets per second
                        return GetPacketsPerSecString;
                    case 8:
                        return GetByteString;
                    case 9:
                        return GetMiliSecondString;
                    default:
                }
            default:
                return null;
        }
    }
}

function IsValueCritical(value, columnSetting, multiplier) {
    multiplier = multiplier ? multiplier : 1;
    switch (columnSetting.CriticalThresholdOperator) {
        case 0:
            return value > columnSetting.CriticalThresholdValue * multiplier;
        case 1:
            return value >= columnSetting.CriticalThresholdValue * multiplier;
        case 2:
            return value < columnSetting.CriticalThresholdValue * multiplier;
        case 3:
            return value <= columnSetting.CriticalThresholdValue * multiplier;
        default:
            return false;
    }
}

function IsValueWarning(value, columnSetting, multiplier) {
    multiplier = multiplier ? multiplier : 1;
    switch (columnSetting.WarningThresholdOperator) {
        case 0:
            return value > columnSetting.WarningThresholdValue * multiplier;
        case 1:
            return value >= columnSetting.WarningThresholdValue * multiplier;
        case 2:
            return value < columnSetting.WarningThresholdValue * multiplier;
        case 3:
            return value <= columnSetting.WarningThresholdValue * multiplier;
        default:
            return false;
    }
}

function GetPacketsString(value, metaData, record, row, col, store, gridView) {
    return (value != null) ? stringFormat('@{R=Toolset.Strings;K=ToolsetGUIDATA_MD0_1;E=js}', value) : "";
}

function GetThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + value + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + value + '</span>';
    }
    return value;
}

function GetPacketsThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetPacketsString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetPacketsString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetPacketsString(value, metaData, record, row, col, store, gridView);
}

function GetPacketsPerSecString(value, metaData, record, row, col, store, gridView) {
    return (value != null) ? stringFormat('@{R=Toolset.Strings;K=ToolsetGUIDATA_MD0_2;E=js}', value) : "";
}

function GetPacketsPerSecThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetPacketsPerSecString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetPacketsPerSecString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetPacketsPerSecString(value, metaData, record, row, col, store, gridView);
}

function GetMiliSecondString(value, metaData, record, row, col, store, gridView) {
    if (isNaN(value)) {
        return "";
    }
    if ((toolTraceRoute || toolResponseMonitor) && value == -1) {
        return "Response timeout";
    }
    return (value != null) ? stringFormat('@{R=Toolset.Strings;K=ToolsetGUICODE_MD0_46;E=js}', value) : "";
}

function GetMiliSecondThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if (!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetMiliSecondString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetMiliSecondString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetMiliSecondString(value, metaData, record, row, col, store, gridView);
}

function GetPercentThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetPercentString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetPercentString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetPercentString(value, metaData, record, row, col, store, gridView);
}

function GetMegaByteThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if (!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings, 1048576)) {
        return '<span class="resGridErrorValue">' + GetByteString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings, 1048576)) {
        return '<span class="resGridWarningValue">' + GetByteString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetByteString(value, metaData, record, row, col, store, gridView);
}

function GetByteThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetByteString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetByteString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetByteString(value, metaData, record, row, col, store, gridView);
}

function GetPercentString(value, metaData, record, row, col, store, gridView) {
    return (value != null) ? stringFormat('@{R=Toolset.Strings;K=ToolsetGUIDATA_MD0_0;E=js}', value) : "";
}

function GetByteString(value, metaData, record, row, col, store, gridView) {
    if (value == null && value != 0 || value === '') return "";
    if (toolMemoryMonitor && value == -1) {
        return "Response timeout";
    }
    var decimalPlaces = 3;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesBytes;E=js}', value);
    }

    value /= 1024.0;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesKbytes;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1024.0;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesMbytes;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1024.0;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesGbytes;E=js}', value.toFixed(decimalPlaces));
    }
    value /= 1024.0;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesTbytes;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1024.0;
    if (value < 1024) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToStorageBytesPbytes;E=js}', value.toFixed(decimalPlaces));
    }

    return value;
}

function GetBitString(value, metaData, record, row, col, store, gridView) {
    if (value == null) return "";
    var decimalPlaces = 3;
    if (value < 1000.0) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBits;E=js}', value);
    }

    value /= 1000.0;
    if (value < 1000) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBitsK;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1000.0;
    if (value < 1000) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBitsM;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1000.0;
    if (value < 1000) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBitsG;E=js}', value.toFixed(decimalPlaces));
    }
    value /= 1000.0;
    if (value < 1000) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBitsT;E=js}', value.toFixed(decimalPlaces));
    }

    value /= 1000.0;
    if (value < 1000) {
        return stringFormat('@{R=Toolset.Strings;K=DisplayHelperToNetworkBitsP;E=js}', value.toFixed(decimalPlaces));
    }

    return value;
}

function GetBitThresholdString(value, metaData, record, row, col, store, gridView, columnSettings) {
    if (value == null) return "";
    if(!columnSettings) {
        columnSettings = metaData.column.columnSettings;
    }
    if (IsValueCritical(value, columnSettings)) {
        return '<span class="resGridErrorValue">' + GetBitString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    if (IsValueWarning(value, columnSettings)) {
        return '<span class="resGridWarningValue">' + GetBitString(value, metaData, record, row, col, store, gridView) + '</span>';
    }
    return GetBitString(value, metaData, record, row, col, store, gridView);
}

var resultsGridObj = {
    grid: null,
    gridColWidth: null,
    draggable: true,
    sortable: true,
    menuDisabled: false
};

function storeColWidth(gridObj) {
    gridObj.gridColWidth = [];
    for (var j = 0; j < gridObj.grid.columns.length; j++) {
        gridObj.gridColWidth.push(gridObj.grid.columns[j].width);
    }
}

//replaces old data in a grid with new data while with suspended events
function PushNewDataToGrid(grid, newResults) {
    var dataStore = grid.getStore();
    dataStore.suspendEvents();
    dataStore.clearData();
    dataStore.add(newResults);
    dataStore.resumeEvents();
    grid.getView().refresh();
}
function GetColumnWidth()
{
    columnWidth = GetJSONData("/Orion/Toolset/Services/Toolset.asmx/GetColumnWidth", "GET", "");
    setTimeout(GetColumnWidth, 600000)
}

//creates result grid with configuration
//column settings come from data cached in tool's javascript
function CreateResultGrid(columnSettings, results, renderTo, tableHeight, tableWidth, customRenderer, gridObj) {
    removeEmptyResults(results);
    var res = results;
    gridObj = gridObj ? gridObj : resultsGridObj;
    if(!columnWidth){
        GetColumnWidth();
    }
    var createGrid = function() {
        var tableFields = [];
        var tableColumns = [];
        if (gridObj.gridColWidth) {
            if (gridObj.gridColWidth.length != columnSettings.length) {
                gridObj.gridColWidth = null;
            }
        }
        tableFields.push("uiID");
        for (var i = 0; i < columnSettings.length; i++) {
            tableFields.push(columnSettings[i].PropertyName);
            var tblcolumn = {
                columnSettings: columnSettings[i],
                text: columnSettings[i].DisplayName,
                dataIndex: columnSettings[i].PropertyName,
                sortable: gridObj.sortable,
                menuDisabled: gridObj.menuDisabled,
                width: columnWidth,
                autoSizeColumn: false,
                draggable: gridObj.draggable,
                renderer: customRenderer ? customRenderer : getRenderer(columnSettings[i])
            };

            tableColumns.push(tblcolumn);
        }
        var gridWidth = columnWidth * tableColumns.length + 10; //added 10 because the whole grid width contains frame/border as well
        if (gridObj.grid) {
            PushNewDataToGrid(gridObj.grid, res);
            storeColWidth(gridObj);
        } else {
            $('#' + renderTo).html('');
            gridObj.grid = new Ext42.grid.GridPanel({
                autoHeight: true,
                width: gridWidth,
                enableTextSelection: true,
                scrollOffset: 0,
                forceFit: false,
                autoscroll: true,
                renderTo: renderTo,
                frame: true,
                multiSelect: true,
                store: new Ext42.data.JsonStore({
                    fields: tableFields,
                    autoLoad: true,
                    data: res
                }),
                stateful: false,
                selModel: { allowDeselect: true },
                viewConfig: {
                    stripeRows: true,
                    listeners: {
                        refresh: function(dataview) {
                            //ToDo: FB322935 - temporarily disabled
                            /*var intGridWidth = 0;
                            Ext42.each(dataview.panel.columns, function(column) {
                                if (column.autoSizeColumn === true) {
                                    column.autoSize();
                                    column.width += 15;
                                    intGridWidth += column.width;
                                }
                            });
                            gridObj.grid.setWidth(intGridWidth + 13);*/
                            $('#' + renderTo).css('visibility', 'visible');
                        }
                    }
                },
                columns: {
                    items: tableColumns,
                    defaults: {
                        flex: 1
                    }
                },
                defaults: {
                    flex: 1
                }
            });

            Ext42.EventManager.onWindowResize(gridObj.grid.doLayout, gridObj.grid);

            gridObj.grid.getView().preserveScrollOnRefresh = true;
            gridObj.grid.getView().autoScroll = false;
            storeColWidth(gridObj);

            $("#" + renderTo + " .x42-grid-view").removeClass('x42-unselectable');
            $("#" + renderTo + " .x42-grid-cell").removeClass('x42-unselectable');
        }
    };

    //modify data in column setting asynchronously
    var nodesPromise = getRenderedNodesAsync(columnSettings, res);
    var interfacesPromise = getRenderedInterfacesAsync(columnSettings, res);

    //when done, create grid
    var sequence = $.when(nodesPromise, interfacesPromise).then(createGrid);

    if( CreateResultGrid.fnContinue ) { // look for continuance delegate
       sequence = sequence.then(CreateResultGrid.fnContinue);
       CreateResultGrid.fnContinue = null;
    }
}

// shim pattern: continuance delegate

CreateResultGrid.Next = function(fn){
  CreateResultGrid.fnContinue = fn;
}


//updates column settings with some UI captions asynchronously
function getRenderedNodesAsync(columnSettings, results) {
    var colSett = columnSettings;
    var res = results;

    var renderedNodes = [];

    var prepareArguments = function () {
        for (var i = 0; i < colSett.length; i++) {
            if (colSett[i].ColumnType == 4) {
                for (var j = 0; j < res.length; j++) {
                    renderedNodes.push(res[j][colSett[i].PropertyName]);
                }
            }
            if (colSett[i].ColumnType == 5) {
                return true;
            }
        }
        return false;
    };

     var processResult = function (result) {	 
       

	  //This code Avilable for For Other pages ( NON Trace root page)  Trace root page gives result in Text , other 	
	  var k = 0;       
	   for (var colItem = 0; colItem < colSett.length; colItem++) {
            if (colSett[colItem].ColumnType == 4) {
                for (var resItem = 0; resItem < result.length; resItem++) 
				{					
					var currentItem =k++;										
					if(result[currentItem].Tool ==1 && result[currentItem].HTML)
					{		
						//debugger
						var textData =result[currentItem].HTML;
						if(textData) 
						{
							var arrayData = textData.split("£");
								for( var arrayItem =0; arrayItem < arrayData.length; arrayItem++) 
								{									
									var keyValue = arrayData[arrayItem].split("¥");								
									res[resItem][keyValue[0]] = keyValue[1];																																
								}
						}
					}
					else if(result[currentItem].HTML)
					{
						res[resItem][colSett[colItem].PropertyName] = result[currentItem].HTML;
					}
                }
            }
        }	
	
    };

    var hasInterfaces = prepareArguments();
    if (hasInterfaces == true) {
        return $.Deferred().resolve();
    }
    //no data - no need to call a web service
    if (renderedNodes.length == 0) {
        return $.Deferred().resolve();
    }

    //call web service asynchronously
    var nodeResultPromise = getNodeNames(renderedNodes);
    if (!IsEnabledOrionObjectsCrossLinks())
    nodeResultPromise = removeLinkAndIconFromName(nodeResultPromise);

    //when the data is received, process it
    return $.when(nodeResultPromise).pipe(processResult);
}

var removeLinkAndIconFromName = function (names) {
    $(names).each(function(entry) {
        if (entry.HTML != null) {
            entry.HTML = entry.HTML.replace(/<(?:.|\n)*?>/gm, '');
        }
    });
    return names;
};

//updates column settings with some UI captions asynchronously
function getRenderedInterfacesAsync(columnSettings, results) {
    var colSett = columnSettings;
    var res = results;

    var addresses = [];
    var interfaces = [];

    var prepareArguments = function () {
        var addressColumnIndex;
        for (var i = 0; i < colSett.length; i++) {
            if (colSett[i].ColumnType == 4) {
                addressColumnIndex = i;
                break;
            }
        }

        for (i = 0; i < colSett.length; i++) {
            if (colSett[i].ColumnType == 5) {
                for (var j = 0; j < res.length; j++) {
                    interfaces.push(res[j][colSett[i].PropertyName]);
                    addresses.push(res[j][colSett[addressColumnIndex].PropertyName]);
                }
            }
        }
    };

    var processResult = function (resultData, nodeResultData) {
        var k = 0;
        for (var i = 0; i < colSett.length; i++) {
            if (colSett[i].ColumnType == 5) {
                for (var j = 0; j < resultData.length; j++) {
                    res[j][colSett[i].PropertyName] = '<div style="display:inline-block">' + resultData[k].HTML + " on " + nodeResultData[k++].HTML.replace("div>", "span>") + '</div>';
                }
            }
        }
    };

    prepareArguments();

    //no data - no need to call a web service
    if (addresses.length == 0) {
        return $.Deferred().resolve();
    }

    //call web services asynchronously
    var interfaceResultPromise = getInterfaceNames(addresses, interfaces);
    var nodeResultPromise = getNodeNames(addresses);

    //tmp solution for FB336387(Status icons and links are leading to memory leak in FF)
    if (!IsEnabledOrionObjectsCrossLinks())
    nodeResultPromise = removeLinkAndIconFromName(nodeResultPromise);
    
    //when their data is received, process it
    return $.when(interfaceResultPromise, nodeResultPromise).pipe(processResult);
}

var cachedRenderedInterfaces = null;
var cachedRenderedInterfacesCleaner = null;
//returns Interface names with statuses as HTML fragments
//requires a list of IP addresses and interface indexes
//calls GetInterfaceNetObjectIds web service if the requested data is not already cached
function getInterfaceNames(addressesList, interfacesList) {
    if (cachedRenderedInterfacesCleaner == null) {
        cachedRenderedInterfacesCleaner = setInterval(function () {
            cachedRenderedInterfaces = null;
        }, GetCacheInvalidationInterval());
    }

    var result = [];

    var fillErrorResult = function (addresses, interfaces) {
        var newData = [];
        for (var i = 0; i < addresses.length; i++) {
            newData.push({ IP: addresses[i], IFC: interfaces[i], HTML: interfaces[i] });
        }
        return newData;
    };

    var callWebService = function (addresses, interfaces) {
        var newData = [];
        $.ajax({
            type: "POST",
            url: "/Orion/Toolset/Services/Toolset.asmx/GetInterfaceNetObjectIds",
            data: JSON.stringify({ interfaces: interfaces, ipaddress: addresses }),
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                for (var i = 0; i < addresses.length; i++) {
                    newData.push({ IP: addresses[i], IFC: interfaces[i], HTML: msg.d[i] });
                }
            },
            error: function (msg) {
                newData = fillErrorResult(addresses, interfaces);
            }
        });
        return newData;
    };

    var getDataFromCache = function (ipList, ifcList, cache, resultList, newIpList, newIfcList) {
        for (var i = 0; i < ipList.length; i++) {
            var found = false;
            for (var j = 0; j < cache.length; j++) {
                if (ipList[i] == cache[j].IP && ifcList[i] == cache[j].IFC) {
                    resultList.push(cache[j]);
                    found = true;
                    break;
                }
            }
            if (found == false) {
                newIpList.push(ipList[i]);
                newIfcList.push(ifcList[i]);
            }
        }
        return newIpList.length == 0;
    };

    if (addressesList.length != 0 && cachedRenderedInterfaces == null) {
        cachedRenderedInterfaces = callWebService(addressesList, interfacesList);
        result = cachedRenderedInterfaces;
    }
    else if (addressesList.length != 0 && cachedRenderedInterfaces != null) {
        var newItemsIp = [];
        var newItemsIfc = [];

        var success = getDataFromCache(addressesList, interfacesList, cachedRenderedInterfaces, result, newItemsIp, newItemsIfc);
        if (!success) {
            var newCache = callWebService(newItemsIp, newItemsIfc);
            cachedRenderedInterfaces = newCache.concat(cachedRenderedInterfaces);
            newItemsIp = [];
            newItemsIfc = [];
            result = [];
            success = getDataFromCache(addressesList, interfacesList, cachedRenderedInterfaces, result, newItemsIp, newItemsIfc);
            if (!success) {
                result = fillErrorResult(addressesList, interfacesList);
            }
        }
    }

    return result;
}

function stringFormat() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

function GetResultColumns(serviceUrl) {
    var resultColumns;
    $.ajax({
        type: "GET",
        url: serviceUrl,
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            resultColumns = data.d;
        },
        error: function (data, textStatus, xhr) {
            return [];
        }
    });
    return resultColumns;
}

//creates mobile result grid with configuration
//column settings come from data cached in tool's javascript
function CreateResultGridMobile(columnSettings, results, renderTo, tableHeight, tableWidth, customRenderer, gridObj, refresh) {
    removeEmptyResults(results);
    var res = results;
    gridObj = gridObj ? gridObj : resultsGridObj;

    var createGrid = function() {
        var newResults = [];
        for (var i = 0; i < res.length; i++) {
            for (var j = 0; j < columnSettings.length; j++) {
                newResults.push({
                    title: columnSettings[j].DisplayName,
                    data: res[i][columnSettings[j].PropertyName]
                });
            }
        }

        tableHeight = tableHeight ? tableHeight : 400;
        tableWidth = tableWidth ? tableWidth : 600;
        if (gridObj.grid) {
            PushNewDataToGrid(gridObj.grid, newResults);
        } else {
            gridObj.grid = new Ext42.grid.GridPanel({
                width: tableWidth,
                autoHeight: true,
                scrollOffset: 0,
                forceFit: false,
                autoscroll: false,
                layout: 'fit',
                renderTo: renderTo,
                frame: true,
                store: new Ext42.data.JsonStore({
                    fields: ['title', 'data'],
                    autoLoad: true,
                    settingsPerNode: columnSettings.length,
                    data: newResults
                }),
                stateful: false,
                multiSelect: true,
                viewConfig: {
                    stripeRows: false,
                    enableTextSelection: true,
                    getRowClass: function(record, rowIndex, rowParams, store) {
                        if ((rowIndex - rowIndex % record.store.settingsPerNode) / record.store.settingsPerNode % 2 == 0) {
                            return 'lightResultsRow';
                        } else {
                            return 'darkResultsRow';
                        }
                    },
                    listeners: {
                        refresh: function(dataview) {
                            //ToDo: FB322935 - temporarily disabled
                            /*Ext42.each(dataview.panel.columns, function(column) {
                                if (column.autoSizeColumn === true)
                                    column.autoSize();
                            });*/
                        }
                    }
                },
                columns: [
                    { text: "title", dataIndex: 'title', flex: 1, sortable: false },
                    { text: "data", dataIndex: 'data', flex: 2, sortable: false, columnSettings: columnSettings, renderer: customRenderer ? customRenderer : getMobileDataRenderer }
                ],
                defaults: {
                    flex: 1
                },
                hideHeaders: true
            });
            gridObj.grid.getView().preserveScrollOnRefresh = true;
            gridObj.grid.getView().autoScroll = false;
        }
    };

    //modify data in column setting asynchronously
    var nodesPromise = getRenderedNodesAsync(columnSettings, res);
    var interfacesPromise = getRenderedInterfacesAsync(columnSettings, res);

    //when done, create grid
    var sequence = $.when(nodesPromise, interfacesPromise).then(createGrid);

    if( CreateResultGrid.fnContinue ) { // look for continuance delegate
       sequence = sequence.then(CreateResultGrid.fnContinue);
       CreateResultGrid.fnContinue = null;
    }
}

function getMobileDataRenderer(value, metaData, record, row, col, store, gridView, columnSettingInput) {
    if (!value && value != 0) return "";
    if (!columnSettingInput && (!metaData || !metaData.column)) return value;
    var columnSetting = columnSettingInput == null ? metaData.column.columnSettings[row % store.settingsPerNode] : columnSettingInput;
   
   if (columnSetting.UseThreshold == true) {
        switch (columnSetting.ColumnType) {
            case 0: //format value
                switch (columnSetting.ValueType) {
                    case 0: //none
                        return GetThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 1: //percent
                        return GetPercentThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 2: //byte
                        return GetByteThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 3: //bit
                        return GetBitThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 4: //Packets
                        return GetPacketsThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 5: //Packets per second
                        return GetPacketsPerSecThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 8:
                        return GetMegaByteThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    case 9:
                        return GetMiliSecondThresholdString(value, metaData, record, row, col, store, gridView, columnSetting);
                    default:
                        return value;
                }
            default:
                return value;
        }
    } else {
        switch (columnSetting.ColumnType) {
            case 0: //format value
                switch (columnSetting.ValueType) {
                    case 0: //none
                        return value;
                    case 1: //percent
                        return GetPercentString(value, metaData, record, row, col, store, gridView);
                    case 2: //byte
                        return GetByteString(value, metaData, record, row, col, store, gridView);
                    case 3: //bit
                        return GetBitString(value, metaData, record, row, col, store, gridView);
                    case 4: //Packets
                        return GetPacketsString(value, metaData, record, row, col, store, gridView);
                    case 5: //Packets per second
                        return GetPacketsPerSecString(value, metaData, record, row, col, store, gridView);
                    case 8: //byte
                        return GetByteString(value, metaData, record, row, col, store, gridView);
                    case 9: //byte
                        return GetMiliSecondString(value, metaData, record, row, col, store, gridView);
                    default:
                        return value;
                }
            default:
                return value;
        }
    }
}

function removeEmptyResults(results) {
    for (var i = results.length - 1; i >= 0; i--) {
        if (results[i] == null) {
            results.splice(i, 1);
        }
    }
}
