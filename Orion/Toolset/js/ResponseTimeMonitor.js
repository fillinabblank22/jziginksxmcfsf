﻿var isInApplyModeValue = false;
var currentTaskID = null;
var cachedResultColumnsPromise = null;
var cacheCleaner = null;

$(document).ready(function(){
    Ext42.require([
    'Ext42.data.*',
    'Ext42.grid.*',
    'Ext42.tree.*'
    ]);
});

(function(){

  var getTitle = function(){ return "Response Time Monitor results for " + $("#itemsCount").html(); };

    var currentdate = new Date(); 
    var fileName = "ResponseTimeMonitorResults " 
                + currentdate.getFullYear() + "-" 
                +(currentdate.getMonth()+1) + "-" 
                + currentdate.getDate() + " " 
                + currentdate.getHours() + "."  
                + currentdate.getMinutes() + "." 
                + currentdate.getSeconds();                
  SW.Toolset.Export.Init({ 
    txt: { domid: 'ExportToTxtId', grid: function(){ return resultsGridObj.grid; }, file: fileName+ '.txt', title: getTitle },
    csv: { domid: 'ExportToCSVId', grid: function(){ return resultsGridObj.grid; }, file: fileName+ '.csv', title: getTitle },
    pdf: { domid: 'ExportToPdf', grid: function(){ return resultsGridObj.grid; }, file: fileName+ '.pdf', title: "Response Time Monitor results", subtitle: getTitle, mode: function(){ return isTableView() ? "table" : "chart"; } }
  });

})();


function InvalidateResultColumnsCache() {
    cachedResultColumnsPromise = null;
}

function InitResultHub(taskId) {
    if (taskId) {
        InitResultNotificationClient(taskId, ReceiveResponseTimeMonitorResults, InvalidateResultColumnsCache);
        //delete cache once a 10 minutes
        cacheCleaner = setInterval(InvalidateResultColumnsCache, 600000);
        SetUpTaskId(taskId);
        $("#stopSpinnerDiv").hide();
        $("#export-button-div").hide();
        $("#spinnerDiv").show();
    }
}

//asynchronously prepares current/cached result columns
function getResultColumnsAsync() {
    if (cachedResultColumnsPromise == null) {
        cachedResultColumnsPromise = CallServiceAsync("/Orion/Toolset/Services/ResponseTimeMonitorSettings.asmx/GetResultColumns", "GET", {});
    }
    return cachedResultColumnsPromise;
}

//processes data from SignalR
function ReceiveResponseTimeMonitorResults(newResult) {
    if (!IsValidResultsReceived(newResult.ResponseTimeDetails)) {
        return;
    }
    $("#lastUpdate").val(new Date().getTime());
    //get result columns, render result grid and update chart asynchronously
    var resultColumnsPromise = getResultColumnsAsync();
    var addResultToChartPromise = addResultToChartAsync(newResult.ResponseTimeDetails, newResult.CompletionInfo.CompletionTimeMsSinceEpoch, resultColumnsPromise);

    //when done, add toolset integration
    $.when(addResultToChartPromise)
        //rendering result grid destroys data used by chart - chart must be rendered first
        .pipe(updateResultGridAsync(newResult, resultColumnsPromise))
        .then(function () {
        $("#export-button-div").show();
    });
}

//asynchronously renders result grid
function updateResultGridAsync(newResult, resultColumnsPromise) {
    return resultColumnsPromise.pipe(function (data) {
        var resultcolumns = data.d;

        for (var i = 0; i < newResult.ResponseTimeDetails.length; i++) {
            newResult.ResponseTimeDetails[i].uiID = newResult.ResponseTimeDetails[i].HostAddress;
        }
        if (IsMobile()) {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGridMobile(resultcolumns, newResult.ResponseTimeDetails, 'signalRTableDiv', 400, 300);
});
        } else {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGrid(resultcolumns, newResult.ResponseTimeDetails, 'signalRTableDiv', 400, 600);
});
        }
    });
}

function backToMainPage(taskId, items, itemsToPoll) {
    $("#CWHolder").hide();
    $("#responseTimeMonitorResults").show();
    if (taskId != "" || (taskId == "" && items == 0)) {
        var itemsExist = items > 0;
        if (itemsExist) {
            $('#itemsCount').html(items + ' selected node(s)');
            if (currentTaskID) {
                StopResultNotificationClient(currentTaskID);
                CallServiceAsync("/Orion/Toolset/Services/ConfigurationWizard.asmx/StopTask", "POST", JSON.stringify({ taskId: currentTaskID }));
                if (resultsGridObj.grid) {
                    var dataStore = resultsGridObj.grid.getStore();
                    var indexes = new Array();
                    dataStore.each(function (record) {
                        var exId = record.get('uiID');
                        var exists = false;
                        for (var i = 0; i < itemsToPoll.length; i++) {
                            if (itemsToPoll[i].IP == exId) {
                                exists = true;
                                continue;
                            }
                        }
                        if (!exists) {
                            var index = dataStore.indexOf(record);
                            indexes.push(index);
                        }
                    }, this);
                    indexes = indexes.sort();
                    for (var j = indexes.length - 1; j >= 0; j--) {
                        dataStore.removeAt(indexes[j]);
                    }
                }
            }
            InitResultHub(taskId);
            currentTaskID = taskId;
            $("#resultsTable").show();
            $("#noNodesMessage").hide();

            if ($("#viewTypeSelect").val() == "table") {
                switchToTableView();
            } else {
                switchToPlotView();
            }
        } else {
            $("#resultsTable").hide();
            $("#noNodesMessage").show();
        }
    }

}

function isInApplyMode() {
    return isInApplyModeValue;
}

function backToConfigWizard() {
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("form").css("background", "#F5F5F5");    
    $("#CWHolder").show();
    $("#responseTimeMonitorResults").hide();
    isInApplyModeValue = true;
}

function StopTask() {
    Ext42.MessageBox.confirm({
        title: 'Confirmation',
        width: 300,
        msg: 'Are you sure you want to stop the current real-time procedure? If you try to restart the procedure later, all data collected up to this point will be lost.',
        buttons: Ext42.MessageBox.YESNO,
        fn: function (btn) {
            if (btn == 'yes') {
                CancelResponseTimeMonitorPolling();
                if (resultsChart) {
                    resultsChart.render();
                }
            }
        },
        icon: Ext42.MessageBox.WARNING
    });
}


function CancelResponseTimeMonitorPolling() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        CancelTask(taskId);
        $("#taskIdTextbox").val("");
    }

    $("#spinnerDiv").hide();
    $("#stopSpinnerDiv").show();
}

function StopResponseTimeMonitorResultNotification() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        StopResultNotificationClient(taskId);
        clearInterval(cacheCleaner);
    }
}

function switchToTableView() {
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 1);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnActive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnInactive');
    $("#chartcontainer").hide();
    $("#signalRTableDiv").show();
    $("#viewTypeSelect").val("table");
    $("form").css("background", "#F5F5F5");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    if (resultsGridObj.grid) resultsGridObj.grid.getView().refresh();
    setTableView();
}

function switchToPlotView() {
    SW.Toolset.Visibility.Mask('chart', 1);
    SW.Toolset.Visibility.Mask('chart-grid', 1);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnInactive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnActive');
    $("#chartcontainer").show();
    $("#signalRTableDiv").hide();
    $("form").css("background", "white");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    fixMetricsPanel();
    $("#viewTypeSelect").val("chart");
    if (chartGridObj.grid) chartGridObj.grid.getView().refresh();
    if (resultsChart) resultsChart.render();
    setChartView();
}
