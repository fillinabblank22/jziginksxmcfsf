﻿/*
* jQuery dropdown: A simple dropdown plugin
*
* Copyright 2013 Cory LaViska for A Beautiful Site, LLC. (http://abeautifulsite.net/)
*
* Licensed under the MIT license: http://opensource.org/licenses/MIT
*
*/
if (jQuery) (function ($) {

    $.extend($.fn, {
        dropdown: function (method, data) {

            switch (method) {
                case 'show':
                    show(null, $(this));
                    return $(this);
                case 'hide':
                    hide();
                    return $(this);
                case 'attach':
                    return $(this).attr('data-dropdown', data);
                case 'detach':
                    hide();
                    return $(this).removeAttr('data-dropdown');
                case 'disable':
                    return $(this).addClass('dropdown-disabled');
                case 'enable':
                    hide();
                    return $(this).removeClass('dropdown-disabled');
            }

        }
    });

    function show(event, object) {

        var trigger = event ? $(this) : object,
			dropdown = $(trigger.attr('data-dropdown')),
			isOpen = trigger.hasClass('dropdown-open');

        // In some cases we don't want to show it
        if (event) {
            if ($(event.target).hasClass('dropdown-ignore')) return;

            event.preventDefault();
            event.stopPropagation();
        } else {
            if (trigger !== object.target && $(object.target).hasClass('dropdown-ignore')) return;
        }
        hide();

        if (isOpen || trigger.hasClass('dropdown-disabled')) return;

        // Show it
        trigger.addClass('dropdown-open');
        dropdown
			.data('dropdown-trigger', trigger)
			.show();

        // Trigger the show callback
        dropdown
			.trigger('show', {
			    dropdown: dropdown,
			    trigger: trigger
			});
		$(document).find('.dropdown-menu').each(function () {
            var dropd = $(this);
            dropd
				.show()
				.data('dropdown-trigger', trigger)
				.trigger('show', {
			    dropdown: dropdown,
			    trigger: trigger
			});
        });

    }

    function hide(event) {
        // Hide any dropdown that may be showing
        $(document).find('.toolset-dropdown:visible').each(function () {
            var dropdown = $(this);
            dropdown
				.hide()
				.removeData('dropdown-trigger')
				.trigger('hide', { dropdown: dropdown });
        });
		$(document).find('.dropdown-menu').each(function () {
            var dropdown = $(this);
            dropdown
				.hide()
				.removeData('dropdown-trigger')
				.trigger('hide', { dropdown: dropdown });
        });

        // Remove all dropdown-open classes
        $(document).find('.dropdown-open').removeClass('dropdown-open');}
		
    $(document).on('click.dropdown', '[data-dropdown]', show);
    $(document).on('click.dropdown', hide);

})(jQuery);