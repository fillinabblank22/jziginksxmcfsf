﻿Ext42.require([
'Ext.grid.*',
'Ext.data.*',
'Ext.panel.*',
'Ext.util.*',

]);

var CW_GUID = 270;
var CW_TIME = 170;

var JobList = new (function () {
    var self = this;
    var jobFields = ['Id', 'LastExecution', 'NextExecution', 'Timestamp', 'JobType', 'JobNamespace', 'Tool', 'State', 'Progress', 'CreationTime', 'LastStateUpdateTime', 'CompletionStatus', 'CompletionTime', 'FaultInfo', 'IsFinal', 'LastClientAcknowledgement', 'AcknowledgementRequestTime', 'LastResulNotify'];
    var jobColumns = [
            {
                dataIndex: 'Id',
                text: 'Job Id',
                width: CW_GUID
            },
            {
                dataIndex: 'JobType',
                text: 'Job Type',
                filterable: true,
                width: 85
            },
            {
                dataIndex: 'JobNamespace',
                text: 'Job Namespace',
                filterable: true,
                width: 50
            },
            //{
            //    dataIndex: 'State',
                //    text: 'State',
                //    filterable: true,
                //    width: 100
                //},
            {
                dataIndex: 'LastStateUpdateTime',
                text: 'LastStateUpdateTime',
                filterable: true,
                sortable: true,
                width: CW_TIME
            }, {
                dataIndex: 'AcknowledgementRequestTime',
                text: 'AcknowledgementRequestTime',
                filterable: true,
                sortable: true,
                width: CW_TIME
            }, {
                dataIndex: 'LastClientAcknowledgement',
                text: 'LastClientAcknowledgement',
                filterable: true,
                sortable: true,
                width: CW_TIME
            }, {
                dataIndex: 'LastResulNotify',
                text: 'LastResulNotify',
                filterable: true,
                sortable: true,
                width: CW_TIME
            },
            //{
            //    dataIndex: 'Progress',
                //    text: 'Progress',
                //    filterable: true,
                //    width: 50
                //},
            {
                dataIndex: 'Tool',
                text: 'Tool',
                filterable: true,
                width: 100
            },
            {
                dataIndex: 'CreationTime',
                text: 'Creation Time',
                filterable: true,
                //format: 'Y-m-d H:i:s',
                width: CW_TIME
            },
            //{
            //        dataIndex: 'CompletionStatus',
        //        text: 'CompletionStatus',
        //        filterable: true,
        //        width: 60
        //    }
        //    , {
        //        dataIndex: 'CompletionTime',
        //        text: 'CompletionTime',
        //        filterable: true,
        //        width: CW_TIME
        //    }, {
        //        dataIndex: 'FaultInfo',
        //        text: 'FaultInfo',
        //        filterable: true,
        //        width: 45
        //    }
            {
                dataIndex: 'IsFinal',
                text: 'IsFinal',
                filterable: true,
                width: 100
            },
    ];

    var NotImplementedMessage = function () {
        alert("not implemented ..");
    };

    self.killFunction = NotImplementedMessage;
    var grid;
    
    var selectionColumn = {
        xtype: 'checkcolumn',
        header: 'Selected',
        dataIndex: 'selected',
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 55
    };

    var jobFieldsExtended = jobFields;
    jobFieldsExtended.push('DeletionTime');

    
    var jobColumnsExtended = jobColumns;
    jobColumnsExtended.push({
        dataIndex: 'DeletionTime',
        text: 'DeletionTime',
        filterable: true,
        width: CW_TIME
    });

    function ShowGrid(serviceUrl, fieldList, columnList) {
        HideAllControlButtons();
        var newData = CallWebService(serviceUrl, "{}");
        var arrayLength = newData.length;
        for (var i = 0; i < arrayLength; i++) {
            newData[i]['selected'] = false;
            //Do something
        }
        var store = new Ext42.data.JsonStore({
            // Load data at once
            autoLoad: true,
            data: newData,
            // Root variable
            root: 'd',
            // Record identifier
            id: fieldList[0],
            // Fields declaration
            fields: fieldList
        });

        $("#grid-example").empty();
        var foundColumn = $.grep(columnList, function (e) { return e.dataIndex == 'selected'; });
        if (foundColumn.length == 0) {
            columnList.push({
                xtype: 'checkcolumn',
                header: 'Selected',
                dataIndex: 'selected',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 55
            });
        }
        new Ext42.grid.GridPanel({
            id: 'jobListGrid',
            height: 500,
            enableTextSelection: true,
            verticalScrollerType: 'paginggridscroller',
            forceFit: false,
            autoscroll: true,
            layout: 'fit',
            renderTo: 'grid-example',
            frame: true,
            //features: [filters],
            multiSelect: true,
            store: store,
            stateful: false,
            selModel: { allowDeselect: true },
            columns: columnList,
            viewConfig: { enableTextSelection: true }
        });
    }

    function CallWebService(serviceUrl, data) {
        var resultColumns;
        $.ajax({
            type: "POST",
            processData: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            url: serviceUrl,
            data: data,
            success: function (data) {
                resultColumns = data.d;
            },
            error: function (data, textStatus, xhr) {
                return [];
            }
        });
        return resultColumns;
    }

    function HideAllControlButtons() {
        $("#taskControlButtons").hide();
        $("#jobControlButtons").hide();
    }

    self.KillJob = function () {
        var ids = GetSelection('Id');
        if (ids.length < 1)
            return;
        CallWebService('../../Services/TaskWebService.asmx/CancelJobs', JSON.stringify({ "jobs": ids }));
        ShowJobs();
    }

    ;

    self.ShowJobs = function () {
        self.killFunction = function () {
            KillJob();
            ShowJobs();
        };
        ShowGrid('../../Services/TaskWebService.asmx/GetScheduledJobs', jobFields, jobColumns);
        $("#jobControlButtons").show();
    }

    self.ShowAllJobs = function () {
        killFunction = function () {
            KillJob();
            ShowAllJobs();
        };
        ShowGrid('../../Services/TaskWebService.asmx/GetAllJobs', jobFieldsExtended, jobColumnsExtended);
        $("#jobControlButtons").show();
    }

    function GetSelection(idField) {
        var store = GetStore();
        var ids = [];
        store.suspendEvents();
        store.each(function (record, idx) {
            var val = record.get('selected');
            if (val == true) {
                ids.push(record.get(idField));
            }
        });
        store.resumeEvents();
        return ids;
    }

    self.ShowTasks = function () {
        self.killFunction = function () {
            var ids = GetSelection('TaskId');
            if (ids.length < 1)
                return;
            CallWebService('../../Services/TaskWebService.asmx/StopTasks', JSON.stringify({ "tasks": ids }));
            ShowTasks();
        };
        ShowGrid('../../Services/TaskWebService.asmx/GetTaskInfos',
            ['TaskId', 'SessionId', 'AccountId', 'PollingEngineName', 'Frequency', 'JobId', 'Tool', 'IsPollingRequest', 'JobState'],
            [
                {
                    dataIndex: 'TaskId',
                    text: 'Task Id',
                    width: CW_GUID
                },
                {
                    dataIndex: 'Tool',
                    text: 'Tool',
                    width: 200
                },
                {
                    dataIndex: 'SessionId',
                    text: 'Session Id',
                    filterable: true,
                    width: 200
                },
                {
                    dataIndex: 'AccountId',
                    text: 'Account Id',
                    filterable: true,
                    width: 200
                },
                {
                    filterable: true,
                    text: 'Polling Engine Name',
                    dataIndex: 'PollingEngineName'
                },
                {
                    text: 'IsPollingRequest',
                    dataIndex: 'IsPollingRequest'
                },
                {
                    text: 'JobState',
                    dataIndex: 'JobState'
                },
                {
                    text: 'Job Id',
                    filterable: true,
                    dataIndex: 'JobId',
                    width: CW_GUID
                },
            ]);
        $("#taskControlButtons").show();
    }

    self.ShowSessions = function () {
        self.killFunction = NotImplementedMessage;
        ShowGrid('../../Services/TaskWebService.asmx/GetSessions',
            ['SessionId', 'AccountId', 'ToolDetailCounter'],
            [
                {
                    dataIndex: 'SessionId',
                    text: 'Session Id',
                    width: CW_GUID
                },
                
                {
                    dataIndex: 'AccountId',
                    text: 'Account Id',
                    width: CW_GUID
                },
                {
                    
                    text: 'Tool Detail #',
                    dataIndex: 'ToolDetailCounter'
                }
            ]);
        

    }
    function GetStore() {
        grid = Ext42.getCmp('jobListGrid');
        var store = grid.getStore();
        return store;
    }

    self.SelectAllRows = function () {
        var store = GetStore();
        store.suspendEvents();
        store.each(function (record, idx) {
            record.set('selected', true);
            record.commit();
        });
        store.resumeEvents();
        grid.getView().refresh();
    }

    function GetAllSelected() {
        var grid = Ext42.getCmp('jobListGrid');
        var selected = [];
        var store = grid.getStore();
        store.suspendEvents();
        store.each(function (record, idx) {
            var val = record.get('selected');
            if (val == true) {
                selected.push(record.get('TaskId'));
            }
        });
        store.resumeEvents();
        return selected;
    }

    self.CloneSelected = function () {
        var taskList = GetAllSelected();
        var numberOfCopies = document.getElementById('txbNumberOfCopies').value;
        var jsonData = JSON.stringify({ taskIds: taskList, copies: numberOfCopies });
        var error = CallWebService("/Orion/Toolset/Services/TaskWebService.asmx/CloneTasks", jsonData);
        if (error != null) {
            alert(error);
        }
    }
});


$(document).ready(function () {
    $("#ButtonListJobs").bind("click", function () {
        JobList.ShowJobs();
        return false;
    }
    );
    $("#ButtonListAllJobs").bind("click", function () {
        JobList.ShowAllJobs();
        return false;
    }
    );

    $("#ButtonListTasks").bind("click", function () {
        JobList.ShowTasks();
        return false;
    }
    );
    $("#ButtonListSessions").bind("click", function () {
        JobList.ShowSessions();
        return false;
    }
    );

    $("#ButonSelectAll").bind("click", function () {
        JobList.SelectAllRows();
        return false;
    });

    $("#ButonStopSelected").bind("click", function () {
        JobList.killFunction();
        return false;
    });

    $("#ButtonCloneSelected").bind("click", function () {
        JobList.CloneSelected();
        JobList.ShowTasks();
        return false;
    });

    JobList.ShowJobs();
}
    );