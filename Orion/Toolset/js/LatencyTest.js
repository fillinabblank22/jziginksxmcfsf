﻿SW.Core.namespace('SW.Toolset.LatencyTest');

var cachedResultColumnsPromise = null;
var cacheCleaner = null;

$(document).ready(function () {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        InitResultNotificationClient(taskId, ReceiveLatencyTestResults, InvalidateResultColumnsCache);
        //delete cache once a 10 minutes
        cacheCleaner = setInterval(InvalidateResultColumnsCache, 600000);
    }

    $("#RunButton").bind("click", function () {
        latencyTestRunClick();
    });
    $("#RunButtonMobile").bind("click", function () {
        latencyTestRunClick();
    });

    $("#cancelButton").bind("click", function (e) {
        e.preventDefault();
        latencyTestCancelClick();
    });
    $("#restartButton").bind("click", function (e) {
        e.preventDefault();
        latencyTestRestartClick();
        return false;
    });

    $('#loading-cancel-div').css("display", "none");
    $('#statusExportDiv').css("display", "none");

    $("[id$='msetlink']").attr('href', 'LatencyTestSettings.aspx');
    $(".indent.setting").click(function () {
        window.location = "LatencyTestSettings.aspx";
    });
    $(".link").click(function () {
        window.location = "LatencyTest.aspx";
    });
    $("#ExportToCSVId").click(function () {
        if (resultsGridObj.grid) {
            ExportToCSV(resultsGridObj.grid, 'LatencyTestResults.csv');
        }
    });
    $("#ExportToTxtId").click(function () {
        if (resultsGridObj.grid) {
            ExportToTxt(resultsGridObj.grid, 'LatencyTestResults.txt');
        }
    });
    $("#ExportToPdf").click(function () {
        if (resultsGridObj.grid) {
            ExportToPdfServer(resultsGridObj.grid, "LatencyTest.pdf", "LatencyTest results");
        }
    });

    RenderEnginesComboAsync();
});

Ext42.require([
    'Ext42.grid.*',
    'Ext42.data.*',
    'Ext42.panel.*'
]);


function InvalidateResultColumnsCache() {
    cachedResultColumnsPromise = null;
}

function latencyTestRestartClick() {

    var mobile = getURLParameter("IsMobileView");
    if ('null' == mobile) {
        $("#RunButton").click();
    } else {
     $("#RunButtonMobile").click();
    }
}

var latencyTestCancelled = false;

function latencyTestCancelClick() {
    latencyTestCancelled = true;
    results = null;
    var taskId = $("#taskIdTextbox").val();
    if (taskId)
        CallServiceAsync("/Orion/Toolset/Services/LatencyTestService.asmx/StopTest", "Post", JSON.stringify({ taskId: taskId }));
    $('#completeStatus').html('LatencyTest Cancelled');
    $('#statistics').css("display", "none");
    $('#exportButton').css("display", "inline");
    var mobile = getURLParameter("IsMobileView");
    if ('null' == mobile) {
        $('.status-line').css("display", "inline");
        document.getElementById("restart-div").style.display = "inline";
    } else {
        $('.status-line').css("display", "block");
        document.getElementById("restart-div").style.display = "block";
    }
    $('#exportButton').css("display", "none");
    if (lastResults != null) {
        if (lastResults.length > 0) {
            $('#exportButton').css("display", "inline");
        }
    }
    $('#loading-cancel-div').css("display", "none");
    document.getElementById("completeStatus").style.display = "block";
    return false;
}

function latencyTestCompleted() {
    latencyTestCancelled = false;
    $('#completeStatus').html('');
    $('#statistics').css("display", "block");
    $('#exportButton').css("display", "inline");
    document.getElementById("restart-div").style.display = "none";
    var mobile = getURLParameter("IsMobileView");
    if ('null' == mobile) {
        $('.status-line').css("display", "inline");
    } else {
        $('.status-line').css("display", "block");
    }
    //display export button if results were received
    $('#exportButton').css("display", "none");
    if (lastResults != null) {
        if (lastResults.length > 0) {
            $('#exportButton').css("display", "inline");
        }
    }

    $('#loading-cancel-div').css("display", "none");
    document.getElementById("restart-div").style.display = "none";
    $('#statusExportDiv').css("display", "inline");
    document.getElementById("completeStatus").style.display = "block";
}

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

function GetSelectedEngine() {
    return $("#pollingEnginesSelect option:selected").text();
}

//fills a list of available engines to a combobox asynchronously
function RenderEnginesComboAsync() {
    var fillEnginesCombo = function (data) {
        $.each(data.d, function (index, item) { $('#pollingEnginesSelect').append(new Option(item.Name, item.ID)); });
    };
    var enginesPromise = CallServiceAsync("/Orion/Toolset/Services/ConfigurationWizard.asmx/GetAvailableEngines", "GET", {});
    return $.when(enginesPromise).pipe(fillEnginesCombo);
}

//starts/restarts the tool
function latencyTestRunClick() {
    if (resultsGridObj.grid) {
        var widths = [];
        for (var i = 0; i < resultsGridObj.grid.columns.length; i++) {
            widths.push(resultsGridObj.grid.columns[i].width);
        }
        $("#txtResTblWidths").val(widths.join("|"));
    }
    var id = $("#taskIdTextbox").val();
    results = null;
    if (id) {
        StopResultNotificationClient(id);
        clearInterval(cacheCleaner);
        if (resultsGridObj.grid) {
            var dataStore = resultsGridObj.grid.getStore();
            dataStore.removeAll();
        }
        latencyTestCancelClick();
    }
    $('#completeStatus').html('');

    $("[id$='loadDiv']").css("display", "inline");
    document.getElementById("restart-div").style.display = "none";
    $("#divSpinner").css("display", "block");
    $('#exportButton').css("display", "none");
    var mobile = getURLParameter("IsMobileView");
    if ('null' == mobile) {
        $('.status-line').css("display", "inline");
        $('#loading-cancel-div').css("display", "inline");
    } else {
        $('#loading-cancel-div').css("display", "block");
        $('.status-line').css("display", "block");
    }
    $('#statusExportDiv').css("display", "inline");
    var selectedEngine = GetSelectedEngine();
    latencyTestCancelled = false;

    //start the tool and register it in SignalR
    var jsonData = JSON.stringify({ pollingEngine: selectedEngine });
    var runPromise = CallServiceAsync("/Orion/Toolset/Services/LatencyTestService.asmx/RunTest", "POST", jsonData);

    var initTaskId = function (data) {
        var taskId = data.d;
        if (taskId != "" && taskId) {
            SetUpTaskId(taskId);
            InitResultNotificationClient(taskId, ReceiveLatencyTestResults);
        }
    };

    runPromise.done(initTaskId);
}

var lastResults = null;
var results;

//asynchronously prepares current/cached result columns
function getResultColumnsAsync() {
    if (cachedResultColumnsPromise == null) {
        cachedResultColumnsPromise = CallServiceAsync("/Orion/Toolset/Services/LatencyTestSettings.asmx/GetResultColumns", "GET", {});
    }
    return cachedResultColumnsPromise;
}

//processes data from SignalR
function ReceiveLatencyTestResults(newResult) {
    if (latencyTestCancelled)
        return;

    var lastResultTime = (lastResults != null) ? SW.Toolset.LatencyTest.searchEventsLast(lastResults, 'Result received from SignalR') : null;

    results = newResult.CompletionInfo.Events;
    if (document.getElementById('cbKeepHistory').checked && lastResults != null) {
        lastResults = lastResults.concat(results);
    } else {
        lastResults = results;
    }
    var currentresults = lastResults;

    if (newResult.CompletionInfo.CompletionStatus == 4) {
        latencyTestCompleted();
        $('#completeStatus').html('LatencyTest Failed with message: ' + newResult.CompletionInfo.FaultInfo);
        $('#statistics').css("display", "none");
        return;
    }

    latencyTestCompleted();

    var tableRenderingTime = new Date().getTime();
    var widthVal = [];
    if ($("#txtResTblWidths").val()) {
        widthVal = $("#txtResTblWidths").val().split('|');
    }

    resultsGridObj.gridColWidth = [];
    for (var i = 0; i < widthVal.length; i++) {
        resultsGridObj.gridColWidth.push(parseInt(widthVal[i]));
    }
    resultsGridObj.sortable = false;

    //get result columns and update chart asynchronously
    var resultColumnsPromise = getResultColumnsAsync();
    var updateResultGridPromise = updateResultGridAsync(currentresults, resultColumnsPromise);

    //when done, calculate some statistics and add toolset integration
    $.when(updateResultGridPromise).then(function () {
        tableRenderingTime = new Date().getTime() - tableRenderingTime;

        updateStatistics(currentresults, lastResultTime, tableRenderingTime);
    });
}

//asynchronously renders result grid
function updateResultGridAsync(currentresults, resultColumnsPromise) {
    return resultColumnsPromise.pipe(function (data) {
        var resultcolumns = data.d;

        if ($('#jsonData').css("display") == "none") {
            CreateResultGridMobile(resultcolumns, currentresults, 'jsonDataMobile', 400, 400);
        } else {
            CreateResultGrid(resultcolumns, currentresults, 'jsonData', 600, 700);
        }
    });
}

SW.Toolset.LatencyTest.signalRDelaySum = 0;
SW.Toolset.LatencyTest.signalRDelayCount = 0;
SW.Toolset.LatencyTest.resultToWebPageSum = 0;
SW.Toolset.LatencyTest.resultToWebPageCount = 0;
SW.Toolset.LatencyTest.webServiceStarted = null;
SW.Toolset.LatencyTest.launcherStarted = null;
SW.Toolset.LatencyTest.firstResultReceivedFromSignalR = null;

function updateStatistics(results, lastResultTime, tableRenderingTime) {
    $('#statistics').css("display", "block");

    if (!SW.Toolset.LatencyTest.webServiceStarted) {
        SW.Toolset.LatencyTest.webServiceStarted = SW.Toolset.LatencyTest.searchEvents(results, 'WebService started');
    }

    if (!SW.Toolset.LatencyTest.launcherStarted) {
        SW.Toolset.LatencyTest.launcherStarted = SW.Toolset.LatencyTest.searchEvents(results, 'Launcher started');
    }

    if (!SW.Toolset.LatencyTest.firstResultReceivedFromSignalR) {
        SW.Toolset.LatencyTest.firstResultReceivedFromSignalR = SW.Toolset.LatencyTest.searchEvents(results, 'Result received from SignalR');
    }

    var progressReported = SW.Toolset.LatencyTest.searchEventsLast(results, 'Progress reported');
    var sendingResultToSignalR = SW.Toolset.LatencyTest.searchEventsLast(results, 'Sending result to SignalR');
    var resultReceivedFromSignalR = SW.Toolset.LatencyTest.searchEventsLast(results, 'Result received from SignalR');

    var signalRDelay = Math.max(resultReceivedFromSignalR - sendingResultToSignalR, 0);
    SW.Toolset.LatencyTest.signalRDelaySum += signalRDelay;
    if (SW.Toolset.LatencyTest.signalRDelaySum == Number.MAX_VALUE) {
        SW.Toolset.LatencyTest.signalRDelaySum = 0;
        SW.Toolset.LatencyTest.signalRDelayCount = 0;
    }
    SW.Toolset.LatencyTest.signalRDelayCount++;

    var resultToWebPage = Math.max(resultReceivedFromSignalR - progressReported, 0);
    SW.Toolset.LatencyTest.resultToWebPageSum += resultToWebPage;
    if (SW.Toolset.LatencyTest.resultToWebPageSum == Number.MAX_VALUE) {
        SW.Toolset.LatencyTest.resultToWebPageSum = 0;
        SW.Toolset.LatencyTest.resultToWebPageCount = 0;
    }
    SW.Toolset.LatencyTest.resultToWebPageCount++;

    if (SW.Toolset.LatencyTest.webServiceStarted != null && SW.Toolset.LatencyTest.launcherStarted != null) {
        $('#clickToJobStart').html('Click -> Job start: ' + (SW.Toolset.LatencyTest.launcherStarted - SW.Toolset.LatencyTest.webServiceStarted) + " ms");
    }
    if (SW.Toolset.LatencyTest.webServiceStarted != null && SW.Toolset.LatencyTest.firstResultReceivedFromSignalR != null) {
        $('#clickToFirstResultOnWebPage').html('Click -> First response on WebPage: ' + (SW.Toolset.LatencyTest.firstResultReceivedFromSignalR - SW.Toolset.LatencyTest.webServiceStarted) + " ms");
    }
    if (sendingResultToSignalR != null && resultReceivedFromSignalR != null) {
        $('#signalRDelay').html('SignalR delay: ' + signalRDelay + ' ms (average: ' + (SW.Toolset.LatencyTest.signalRDelaySum / SW.Toolset.LatencyTest.signalRDelayCount).toFixed(0) + ')');
    }
    if (progressReported != null && resultReceivedFromSignalR != null) {
        $('#resultToWebPage').html('Result generated -> Result on WebPage: ' + resultToWebPage + ' ms (average: ' + (SW.Toolset.LatencyTest.resultToWebPageSum / SW.Toolset.LatencyTest.resultToWebPageCount).toFixed(0) + ')');
    }
    $('#tableRendering').html('Rendering result table: ' + tableRenderingTime + " ms");
    if (lastResultTime != null && resultReceivedFromSignalR != null) {
        $('#delayBetweenResults').html('Delay between last two results: ' + ((resultReceivedFromSignalR - lastResultTime) / 1000) + " s");
    }
}

SW.Toolset.LatencyTest.searchEvents = function(array, o) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].Text == o) {
            return Date.parse(array[i].Time);
        }
    }
    return null;
}
SW.Toolset.LatencyTest.searchEventsLast = function(array, o) {
    for (var i = array.length - 1; i >= 0; i--) {
        if (array[i].Text == o) {
            return Date.parse(array[i].Time);
        }
    }
    return null;
}

//
// Functions copied from ConfigWizard.js, because it is not included
//

//runs a specified web service asynchronously and returns its Promise
function CallServiceAsync(serviceUrl, method, passData, successCallback, errorCallback) {
    return $.ajax({
        type: method,
        url: serviceUrl,
        data: passData,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (typeof (successCallback) === 'function') {
                successCallback(data.d);
            }
        },
        error: function (data, textStatus, xhr) {
            if (typeof (errorCallback) === 'function') {
                errorCallback(data, textStatus, xhr);
            }
        }
    });
}

//returns Node names with statuses as HTML fragments
//this function is here just because result grid calls it
//since this tool does not work with actual nodes, we just return the simplest usable result
function getNodeNames(list) {
    var result = [];
    for (var i = 0; i < list.length; i++) {
        result.push({ IP: list[i], HTML: list[i] });
    }
    return result;
}
