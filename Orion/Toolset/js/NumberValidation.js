﻿function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function validateRange(input, min, max) {
    var value = parseInt(input.value, 10);

    if (value < min) {
        input.value = min;
    }
    if (value > max) {
        input.value = max;
    }
}