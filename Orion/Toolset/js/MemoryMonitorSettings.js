﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
var saveChanges;
var cancelChanges;
var selectorModelForAll;
var selectorModelForSelected;
var allDataStore;
var selectedDataStore;
var gridAll;
var gridSelected;

function selectColumns() {
    var selections = gridAll.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        gridSelected.getStore().add(selections[i]);
        gridAll.getStore().remove(selections[i]);
    }
}
function deselectColumns() {
    var selections = gridSelected.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        gridAll.getStore().add(selections[i]);
        gridSelected.getStore().remove(selections[i]);
    }
}

function saveSettings(obj) {
    ORION.callWebService("/Orion/Toolset/Services/MemoryMonitorSettings.asmx", "SaveSettings", {
            table: Ext.encode(Ext.pluck(selectedDataStore.data.items, 'data'))
        }, function(result) {
            if (!result) {
                $("#successMsg").css("display", "block");
                setTimeout(function () {
                    $("#successMsg").css("display", "none");
                }, 3000);
                $("#failedMsg").css("display", "none");
                if (obj !== "dummy") window.location.replace(obj);
            } else {
                $("#successMsg").css("display", "none");
                $("#failedMsg").css("display", "block");
                $("#errorMsg").html("An Error occured during saving. " + result);
            }
        });
}

ToolsetSettingsManager = function() {

    function encodeHTML(htmlText) {

        return $('<div/>').text(htmlText).html();
    };

    function renderColumn(value, meta, record) {
        return encodeHTML(value);
    };


    function selectColumns(all, selectionModel) {
        if (all) {
            selectionModel.selectAll();
        } else
            selectionModel.clearSelections();
    }
        
    function moveSelectedRow(grid, direction) {
        var record = grid.getSelectionModel().getSelected();
        if (!record) {
            return;
        }
        var index = grid.getStore().indexOf(record);
        if (direction < 0) {
            index--;
            if (index < 0) {
                return;
            }
        } else {
            index++;
            if (index >= grid.getStore().getCount()) {
                return;
            }
        }
        grid.getStore().remove(record);
        grid.getStore().insert(index, record);
        grid.getSelectionModel().selectRow(index, true);
    }

    return {
        reload: function() { gridAll.getStore().reload(); },
        init: function() {

            selectorModelForAll = new Ext.grid.CheckboxSelectionModel();

            allDataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/MemoryMonitorSettings.asmx/GetAllColumns",
                [
                    { name: 'Name', mapping: 0 },
                    { name: 'DisplayName', mapping: 1 }, 
                    { name: 'Header', mapping: 2 }, 
                    { name: 'Type', mapping: 3}
                ],
                "");
            
            // define grid panel
            gridAll = new Ext.grid.GridPanel({
                store: allDataStore,
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                },
                autoScroll: true,
                columns: [
                    selectorModelForAll,
                    { header: 'Name', fixed:true, hideable: false, hidden:true, sortable: false, dataIndex: 'Name' },
                    { header: 'DisplayName', dataIndex: 'DisplayName', width:298, resizable: true, sortable: false, menuDisabled: true },
                    { header: 'Header', fixed:true, hideable: false, hidden:true, sortable: false, dataIndex: 'Header' },
                    { header: 'Type', fixed:true, hideable: false, hidden:true, sortable: false, dataIndex: 'Type' }
                ],
                selModel: selectorModelForAll,
                hideHeaders: true,
                width: 300,
                height: 400,
                frame: true,
                bodyStyle: 'background-color:#fff;',
                title: 'Available Metrics',
                tbar: [
                    {
                        id: 'selectAllButtonAva',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectColumns(true, selectorModelForAll); }
                    }, '-',
                    {
                        id: 'selectNoneButtonAva',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectColumns(false, selectorModelForAll); }
                    }, '-'
                   ],
            });

            gridAll.render('Grid');
            gridAll.getView().fitColumns();
            selectorModelForAll.fireEvent('selectionchange');
            gridAll.store.proxy.conn.jsonData = {};
            gridAll.store.load();
            
            selectorModelForSelected = new Ext.grid.CheckboxSelectionModel();

            selectedDataStore = new ORION.WebServiceStore(
                "/Orion/Toolset/Services/MemoryMonitorSettings.asmx/GetSelectedColumns",
                [
                    { name: 'Name', mapping: 0 },
                    { name: 'DisplayName', mapping: 1 }
                ],
                "");

            // define grid panel
            gridSelected = new Ext.grid.GridPanel({
                store: selectedDataStore,
                columns: [
                    selectorModelForSelected,
                    { header: 'Name', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'Name' },
                    { header: 'DisplayName', dataIndex: 'DisplayName', width: 798, sortable: false, menuDisabled: true, render: renderColumn }
                ],
                frame: true,
                bodyStyle: 'background-color:#fff;',
                hideHeaders: true,
                sm: selectorModelForSelected,
                viewConfig: {
                    forceFit: true,
                    scrollOffset: 0,
                    hideSortIcons: true,
                    stripeRows: false
                },
                autoScroll: true,
                width: 800,
                height: 400,
                title: 'Selected Metrics',
                tbar: [
                    {
                        id: 'SelectAll',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectColumns(true, selectorModelForSelected); }
                    }, '-',
                    {
                        id: 'SelectNone',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectColumns(false, selectorModelForSelected); }
                    }, '-',
                    {
                        id: 'MoveUpButton',
                        text: 'Move Up',
                        iconCls: 'moveup',
                        handler: function() { moveSelectedRow(gridSelected, -1); }
                    },'-',
                 {
                        id: 'MoveDownButton',
                        text: 'Move Down',
                        iconCls: 'movedown',
                        handler: function() { moveSelectedRow(gridSelected, 1);}
                    }
                ],
                
            });

            gridSelected.render('GridSelected');
            selectorModelForSelected.fireEvent('selectionchange');
            gridSelected.store.proxy.conn.jsonData = {};
            gridSelected.store.load();
        }
    };
}();

function SaveChanges() {
    saveChanges();
}

function CancelChanges() {
    cancelChanges();
}

Ext.onReady(ToolsetSettingsManager.init, ToolsetSettingsManager);