$(function () {
    var groupingGridValue = "@{R=Core.Strings;K=WEBJS_JP0_1; E=js}", groupingCellValue = "";
    var groupingSelectedRow = 0;
    var selDepObject = '';
    var selectorModel;
    var dataStore;
    var grid;
    var gridType = "Orion.Nodes";
    var parentIdRef = $("#isParentIdRef")[0];
    if (parentIdRef == null)
        return;
    var parentIdRefRoot = $("#" + parentIdRef.value)[0];
    if (parentIdRefRoot == null)
        return;
    var isParentDependency = parentIdRefRoot.value;
    var dependencyPrefix = "dep";
    $("#searchImg").click(function () {
        $("#divGrid")[0].innerHTML = '';
        $("#divGrid")[0].innerText = '';
        var val = $("#txtSerch")[0].value;
        SetSearchHref(val, false);

        SW.Orion.SelectNode.init();
    });
    function getURLParameter(name) {
        return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
    }
    $("#txtSerch").keydown(function (event) {
        if (window.event && window.event.keyCode == 13) {
            $("#divGrid")[0].innerHTML = '';
            SetSearchHref($("#txtSerch")[0].value, true);
            SW.Orion.SelectNode.init();
            event.returnValue = false;
            event.cancelBubble = true;
            return false;
        }
        else if (event && event.which == 13) {
            $("#divGrid")[0].innerHTML = '';
            SetSearchHref($("#txtSerch")[0].value, true);
            SW.Orion.SelectNode.init();
            event.cancelBubble = true;
            event.stopPropagation();
            return false;
        }
    });

    function SetSearchHref(val, isEnter) {
        if (val != null && val.length > 0 && val != encodedSearchString) {
            var href = $("#searchImg")[0].src;
            if (href.indexOf("/Orion/images/search_button.gif") == -1 && !isEnter) {
                $("#txtSerch")[0].value = encodedSearchString;
                $("#txtSerch")[0].style.color = '#555';
                $("#txtSerch")[0].style.fontStyle = 'italic';
                $("#txtSerch")[0].style.fontSize = '9pt';
                $("#searchImg")[0].src = "/Orion/images/search_button.gif";
            }
            else {
                $("#searchImg")[0].src = "/Orion/images/clear_button.gif";
            }
        }
        else {
            $("#searchImg")[0].src = "/Orion/images/search_button.gif";
        }
    }

    function SetGridType() {
        var selObject = $("#" + $("#selObjectIdRef")[0].value)[0].value;
        var gridVal = $("#" + $("#groupingGridValueIdRef")[0].value)[0].value;
        var cellVal = $("#" + $("#groupingCellValueIdRef")[0].value)[0].value;
        var selectedRow = $("#" + $("#groupingSelectedRowIdRef")[0].value)[0].value;

        if (selObject.length > 0) {
            groupingGridValue = "";
            groupingSelectedRow = -1;
            var uriParts = selObject.split("/");
            if (uriParts.length > 0) {
                var type = uriParts[uriParts.length - 2];
                if (type.length > 0) {
                    switch (type) {
                        case 'Interfaces':
                            gridType = "Orion.NPM.Interfaces";
                            break;
                        case 'Applications':
                            gridType = "Orion.APM.Application";
                            break;

                        default:
                            gridType = type;
                            break;
                    }
                }
                var typeParts = gridType.split('.');
                if (typeParts.length > 1) {
                    selDepObject = GetSelectedValue(typeParts[typeParts.length - 1]);
                }
            }
        }

        if (gridVal.length > 0 && cellVal.length > 0) {
            groupingGridValue = gridVal; groupingCellValue = cellVal; groupingSelectedRow = selectedRow;
        }
    }

    function GetSelectedValue(type) {
        switch (type) {
            case 'Interfaces':
                return '@{R=Core.Strings;K=Entity_interfaces; E=js}';
            case 'Nodes':
                return '@{R=Core.Strings;K=WEBJS_VB0_77; E=js}';
            case 'Groups':
                return '@{R=Core.Strings;K=XMLDATA_TM0_161; E=js}';
            default:
                return type;
        }
    }

    SetGridType();

    Ext.namespace('SW');
    Ext.namespace('SW.Orion');
    Ext.QuickTips.init();

    SW.Orion.SelectNode = function () {
        var reMsAjax = /^\/Date\((d|-|.*)\)\/$/;
        var comboArray;
        var pageSize = 20;
        var search = "";
        var showOnlyCombo;
        var expandedItems = new Array();

        function renderStatus(status, meta, record) {
            return GetObjectStatusText($.trim(status.toString()));
        }

        var ObjectGrDoQuery = function (query, succeeded) {
            Information.Query(query, function (result) {
                var table = [];
                for (var y = 0; y < result.Rows.length; ++y) {
                    var row = result.Rows[y];
                    var tableRow = {};
                    for (var x = 0; x < result.Columns.length; ++x) {
                        tableRow[result.Columns[x]] = row[x];
                    }
                    table.push(tableRow);
                }
                succeeded({ Rows: table });
            }, function (error) {
                if (error.get_message() == "@{R=Core.Strings;K=WEBJS_VB0_67; E=js}") {
                    alert('@{R=Core.Strings;K=WEBJS_AK0_26; E=js}');
                    window.location.reload();
                }
            });
        };

        function addVendorIcons() {
            ObjectGrDoQuery("SELECT DISTINCT N.Vendor, N.VendorIcon FROM Orion.Nodes N", function (result) {
                $(result.Rows).each(function () {
                    if (this.Vendor.length > 0 && $("a[value='" + this.Vendor + "'][class='NodeGroupName']") != null &&
            $("a[value='" + this.Vendor + "'][class='NodeGroupName']")[0].firstChild != null &&
            $("a[value='" + this.Vendor + "'][class='NodeGroupName']")[0].firstChild.nodeName != "IMG")
                        $('<img src="/NetPerfMon/images/Vendors/' + this.VendorIcon + '" />').error(function () {
                            this.src = "/NetPerfMon/images/Vendors/Unknown.gif";
                            return true;
                        }).prependTo("a[value='" + this.Vendor + "'][class='NodeGroupName']").after(" ");
                });
            });
        };

        GetLoadingRecord = function (level) {
            var blankRecord = Ext.data.Record.create(dataStore.fields);
            var loadingRecord = new blankRecord({
                ID: -1,
                Level: level + 1,
                Name: '@{R=Core.Strings;K=WEBJS_TM0_3; E=js}'
            });
            return loadingRecord;
        };

        ToggleRow = function (expander, recordId) {
            var item = dataStore.getById(recordId);
            var itemID = item.data.Path;

            if (expandedItems[recordId]) {
                CollapseRow(expander, recordId);
                ORION.callWebService("/Orion/Services/Containers.asmx", "ChangeTree", { path: itemID, expand: false }, function (results) {
                });
            } else {
                var items = [];
                items.push(itemID);
                ExpandRow(expander, recordId);
                ORION.callWebService("/Orion/Services/Containers.asmx", "ChangeTree", { path: itemID, expand: false }, function (results) {
                });
            }
        };

        ExpandRow = function (expander, recordId, callback, items) {
            var rowRecord = dataStore.getById(recordId);
            // when changing this, change DataGridRecord accordingly in Default.aspx.cs
            var store = new ORION.WebServiceStore(
                            "/Orion/Services/Containers.asmx/GetContainersChildren",
                            [
                                { name: 'ID', mapping: 0 },
                                { name: 'Name', mapping: 1 },
                                { name: 'ObjectType', mapping: 2 },
                                { name: 'Description', mapping: 3 },
                                { name: 'RollupType', mapping: 4 },
                                { name: 'IsExpandable', mapping: 5 },
                                { name: 'IsGroup', mapping: 6 },
                                { name: 'IsQuery', mapping: 7 },
                                { name: 'Entity', mapping: 8 },
                                { name: 'Status', mapping: 9 },
                                { name: 'Definition', mapping: 10 },
                                { name: 'PlainName', mapping: 11 },
                                { name: 'MaxGroupDefID', mapping: 12 },
                                { name: 'Uri', mapping: 13 },
                                { name: 'Level', mapping: 19 },
                                { name: 'Path', mapping: 20 },
                                { name: 'ParentPath', mapping: 21 }
                            ],
                            "Name");

            store.addListener("exception", function (dataProxy, type, action, options, response, arg) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: "@{R=Core.Strings;K=WEBJS_VB0_69; E=js}",
                    msg: error.Message,
                    icon: Ext.Msg.ERROR, buttons: Ext.Msg.OK
                });
            });

            store.proxy.conn.jsonData = { itemId: rowRecord.data.ID, level: parseInt(rowRecord.data.Level) + 1, parentPath: rowRecord.data.Path };

            var insertIndex = dataStore.indexOfId(recordId) + 1;
            dataStore.insert(insertIndex, GetLoadingRecord(parseInt(rowRecord.data.Level)));

            store.addListener("load", function (store) {
                expandedItems[recordId] = true;
                expander.className = 'tree-grid-expander-collapse';

                var toAdd = new Array();

                store.each(function (record) {
                    toAdd.unshift(record);
                });

                dataStore.removeAt(insertIndex); // remove "loading" record
                if (toAdd.length > 0) {
                    dataStore.insert(insertIndex, toAdd);
                }

                //UpdateExpandStates();
            });
            store.load();
        };

        CollapseRow = function (expander, recordId) {
            var rowRecord = dataStore.getById(recordId);
            var index = dataStore.indexOfId(recordId);
            var limit = dataStore.getCount();
            var toRemove = new Array();

            // remove all records in a branch under current record, use Level to check this
            for (var i = index + 1; i < limit; i++) {
                var record = dataStore.getAt(i);
                if (record.data.Level > rowRecord.data.Level) {
                    toRemove.push(record);
                } else {
                    break;
                }
            }

            dataStore.remove(toRemove);
            delete expandedItems[recordId];
            expander.className = 'tree-grid-expander-expand';
        };

        function renderName(value, meta, record) {
            return String.format('<a href="{3}" class="NodeName" value=""><img src="/Orion/images/StatusIcons/Small-{1}" style="vertical-align: middle; padding-top:1px;"/>&nbsp;<span style="vertical-align: middle;">{2}</span></a>', record.data.ID, GetObjectStatusText($.trim(record.data.Status.toString())) + ".gif", Ext.util.Format.htmlEncode(value), record.data.DetailsUrl)
        }


        function renderNodeName(value, meta, record) {
            return String.format('<a href="{3}" class="NodeName" value=""><img src="/Orion/StatusIcon.ashx?entity=Orion.Nodes&status={4}&id={0}" style="vertical-align: middle; padding-top:1px;"/>&nbsp;<span style="vertical-align: middle;">{2}</span></a>', record.data.ID, record.data.GroupStatus, Ext.util.Format.htmlEncode(value), record.data.DetailsUrl, record.data.Status);
        }

        function renderInterfaceName(value, meta, record) {
            var href = record.data.DetailsUrl;
            if (href == null || href.length == 0) {
                //Temporary fix for Intrfaces due to DetailsUrl is empty for it
                href = String.format('/Orion/NPM/InterfaceDetails.aspx?NetObject=I:{0}&view=InterfaceDetails', record.data.ID);
            }
            return String.format('<a href="{3}" class="NodeName" value=""><img src="/Orion/images/StatusIcons/Small-{1}" style="vertical-align: middle; padding-top:1px;" />&nbsp;<span style="vertical-align: middle;">{2}</span></a>', record.data.ID, record.data.StatusLED, Ext.util.Format.htmlEncode(value), href);
        }

        function renderGroupName(value, meta, record) {
            var id = record.data.ID;
            var level = 0;
            var entity = record.data.Entity;
            var status = record.data.Status;
            var maxId = parseInt(record.data.MaxGroupDefID);

            if (record.data.Level != null && record.data.Level != "undefined")
                level = record.data.Level;

            var result = '';

            for (var i = 0; i < level; i++) {
                result += '<span class="tree-grid-indent-block">';
            }

            // if Orion.Groups members count > 0
            if (maxId > 0) {
                result += String.format('<span><img src="/Orion/images/Pixel.gif" style="vertical-align: middle;" class="tree-grid-expander-expand" onclick="SW.Orion.SelectNode.toggleRow(this, \'{0}\');">', record.id);
            } else {
                result += '<span class="tree-grid-expander-dummy">';
            }

            if (id === -2) {// Max childrens cut off record
                result = String.format('{0} <span style="margin-left:15px; font-size:12px;">{1}</span>', result, Ext.util.Format.htmlEncode(value)); ;
            }
            else if (id === -1) { // "loading" record
                result = String.format('{0} <span class="entityIconBox"><img src="/Orion/images/AJAX-Loader.gif" /></span> {1}', result, Ext.util.Format.htmlEncode(value));
            } else {
                var resVal = String.format('<a href="/Orion/View.aspx?NetObject=C:{2}" class="NodeName" value=""><img src="{0}" style="vertical-align: middle;"/>&nbsp;<span style="vertical-align: middle;">{1}</span></a>',
                            String.format("/Orion/StatusIcon.ashx?entity={0}&amp;status={1}&amp;size=small", entity, status),
                Ext.util.Format.htmlEncode(value), id);

                result = String.format('{0} {1}', result, resVal);
            }

            result += '</span>';
            for (var i = 0; i < level; i++) {
                result += '</span>';
            }
            return result;
        }

        function renderString(value, meta, record) {
            return String.format(Ext.util.Format.htmlEncode(value));
        }

        function renderIP(value, meta, record) {
            if (search != null && search.length > 0) {
                value = value.replace(search, "<span class='highlight'>" + search + "</span>");
            }
            return String.format(value);
        }

        function renderGroup(value, meta, record) {
            var disp;
            a = reMsAjax.exec(value);
            if (a) {
                var b = a[1].split(/[-,.]/);
                var val = new Date(+b[0]);
                disp = val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) + " (" + record.data.Cnt + ")";
                value = val;
            }
            else {
                disp = ($.trim(String((Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : value))
                    || '@{R=Core.Strings;K=WEBDATA_PS0_0;E=js}') + " (" + record.data.Cnt + ")";

                if (comboArray.getValue() == "@{R=Core.Strings;K=WEBJS_AK0_6; E=js}") {
                    disp = value + " (" + record.data.Cnt + ")";
                }
                if (value == null) {
                    value = "null";
                }
            }

            return String.format('<a href="javascript:void(0)" class="NodeGroupName" value="{0}">{1}</a>', value, Ext.util.Format.htmlEncode(disp));
        }

        function highlightSearchString(search) {
            if (search != null && search.length > 0)
                $('a.NodeName').highlight(search);
        }

        function getEntityRowNumber(entityType, uri, search, succeeded) {
            var orderBy = "DisplayName";

            if (entityType == "Orion.NPM.Interfaces")
                orderBy = "Interfaces.Node.DisplayName";

            var where = (search.length > 0 && search != encodedSearchString) ? String.format(" Where DisplayName Like '%{0}%'", search) : "";
            var query = String.format("SELECT Uri, DisplayName FROM {0} {2} ORDER BY {1} ASC ", entityType, orderBy, where);
            ObjectGrDoQuery(query, function (result) {
                for (var i = 0; i < result.Rows.length; i++) {
                    if (result.Rows[i].Uri == uri) {
                        if (succeeded != null) {
                            succeeded({ ind: i });
                            return;
                        }
                    }
                }
                if (succeeded != null)
                    succeeded({ ind: -1 });
            },
            function () {
                if (succeeded != null)
                    succeeded({ ind: -1 });
            });
        }

        function refreshObjects(elem, property, type, value, search, callback) {
            elem.store.removeAll();
            elem.store.proxy.conn.jsonData = { property: property, type: type, value: value || "", search: search, prefix: dependencyPrefix };

            currentSearchTerm = search;
            elem.store.load({ callback: callback });
        };

        function refreshGridObjects(elem, isParent, callback) {
            elem.store.removeAll();
            elem.store.proxy.conn.jsonData = { isParent: isParent };
            elem.store.load({ callback: callback });
        };

        //Error handler
        function ErrorHandler(result) {
            if (result != null && result.Error) {
                Ext.Msg.show({
                    title: result.Source,
                    msg: result.Msg,
                    minWidth: 500,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        }

        //Ext grid
        ORION.prefix = "Orion_SelectNode_";
        return {
            toggleRow: ToggleRow,
            init: function () {
                var gridFields, storePath, groupingPath, gridColumns, groupingStorePath, gridFieldName;
                var searchText = $("#txtSerch")[0].value;
                if (searchText == encodedSearchString) searchText = "";
                search = searchText;

                if (searchText.length != 0) {
                    searchText = "*" + searchText + "*";
                }

                selectorModel = new Ext.sw.grid.RadioSelectionModel();

                switch (gridType) {
                    case 'Orion.Groups':
                        InitGroupsGrid();
                        break;
                    case 'Orion.Nodes':
                        InitNodesGrid();
                        break;
                    case 'Orion.NPM.Interfaces':
                        InitInterfacesGrid();
                        break;
                    default:
                        InitGrid();
                        break;
                }

                $("#Grid").click(function (e) {
                    if ($(e.target).hasClass('NodeName'))
                        return false;
                });

                var setActiveNode = function () {
                    var objID;
                    $("#" + $('#entityNameRef')[0].value)[0].value = gridType;
                    var selObject = $("#" + $("#selObjectIdRef")[0].value)[0].value;
                    if (selObject.length > 0) {
                        var uriParts = selObject.split("/");
                        if (uriParts.length > 0) {
                            var idParts = uriParts[uriParts.length - 1].split("=");
                            if (idParts.length == 2) {
                                objID = idParts[1];

                                var ind = grid.store.find('ID', objID);
                                if (ind > -1) {
                                    grid.getView().focusRow(ind);
                                    grid.getSelectionModel().selectRow(ind);
                                }
                            }
                        }
                    }
                };

                function InitGrid() {
                    gridFields =
						[
                            { name: 'ID', mapping: 0 },
                            { name: 'Uri', mapping: 4 },
                            { name: 'DisplayName', mapping: 1 },
                            { name: 'Status', mapping: 2 },
                            { name: 'StatusLED', mapping: 3 },
                            { name: 'DetailsUrl', mapping: 4 }
						];

                    gridColumns = [selectorModel,
	                        { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 80, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
							{ header: '@{R=Core.Strings;K=WEBJS_AK0_9; E=js}', width: 350, sortable: true, dataIndex: 'DisplayName', renderer: renderName },
							{ header: '@{R=Core.Strings;K=WEBJS_AK0_6; E=js}', width: 90, sortable: true, dataIndex: 'Status', renderer: renderStatus }
						];
                }

                function InitNodesGrid() {
                    gridFields =
                    [
                        { name: 'ID', mapping: 0 },
                        { name: 'DisplayName', mapping: 2 },
                        { name: 'IP_Address', mapping: 1 },
                        { name: 'Status', mapping: 3 },
                        { name: 'GroupStatus', mapping: 7 },
                        { name: 'Uri', mapping: 5 },
                        { name: 'DetailsUrl', mapping: 6 }
                    ];

                    gridColumns = [selectorModel,
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 120, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_9; E=js}', width: 240, sortable: true, dataIndex: 'DisplayName', renderer: renderNodeName },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_30; E=js}', width: 150, sortable: true, dataIndex: 'IP_Address', renderer: renderIP },
                        { header: '@{R=Core.Strings;K=WEBJS_AK0_6; E=js}', width: 150, sortable: true, dataIndex: 'Status', renderer: renderStatus }
                    ];
                }

                function InitGroupsGrid() {
                    gridFields =
                    [
                        { name: 'ID', mapping: 0 },
                        { name: 'Name', mapping: 1 },
                        { name: 'ObjectType', mapping: 2 },
                        { name: 'Description', mapping: 3 },
                        { name: 'RollupType', mapping: 4 },
                        { name: 'IsExpandable', mapping: 5 },
                        { name: 'IsGroup', mapping: 6 },
                        { name: 'IsQuery', mapping: 7 },
                        { name: 'Entity', mapping: 8 },
                        { name: 'Status', mapping: 9 },
                        { name: 'Definition', mapping: 10 },
                        { name: 'PlainName', mapping: 11 },
                        { name: 'MaxGroupDefID', mapping: 12 },
                        { name: 'Uri', mapping: 13 },
                        { name: 'Level', mapping: 19 },
                        { name: 'Path', mapping: 20 },
                        { name: 'ParentPath', mapping: 21 }
                     ];

                    gridColumns = [
                        selectorModel,
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}', width: 40, hidden: true, hideable: false, sortable: true, dataIndex: 'ID' },
                        { header: '@{R=Core.Strings;K=WEBJS_VB0_73; E=js}', width: 200, sortable: true, dataIndex: 'Name', renderer: renderGroupName },
		                { header: '@{R=Core.Strings;K=WEBJS_VB0_74; E=js}', width: 140, sortable: true, dataIndex: 'ObjectType' },
		                { id: 'Description', header: '@{R=Core.Strings;K=WEBJS_VB0_75; E=js}', width: 345, sortable: true, dataIndex: 'Description' }
                    ];
                }

                function InitInterfacesGrid() {
                    gridFields =
		                    [
		                        { name: 'ID', mapping: 0 },
		                        { name: 'Name', mapping: 1 },
		                        { name: 'DisplayName', mapping: 2 },
		                        { name: 'Type', mapping: 3 },
		                        { name: 'StatusLED', mapping: 6 },
		                        { name: 'Uri', mapping: 4 },
		                        { name: 'DetailsUrl', mapping: 5 }
		                    ];

                    gridColumns = [selectorModel,
		                        {
		                            header: '@{R=Core.Strings;K=WEBJS_VB0_21; E=js}',
		                            width: 120,
		                            hidden: true,
		                            hideable: false,
		                            sortable: true,
		                            dataIndex: 'ID'
		                        },
		                        {
		                            header: '@{R=Core.Strings;K=WEBJS_AK0_9; E=js}',
		                            width: 280,
		                            sortable: true,
		                            dataIndex: 'Name',
		                            renderer: renderInterfaceName
		                        }, {
		                            header: '@{R=Core.Strings;K=WEBJS_AK0_44; E=js}',
		                            width: 200,
		                            sortable: true,
		                            dataIndex: 'DisplayName',
		                            renderer: renderString
		                        }, {
		                            header: '@{R=Core.Strings;K=WEBJS_AK0_7; E=js}',
		                            width: 185,
		                            sortable: true,
		                            dataIndex: 'Type',
		                            renderer: renderString
		                        }
		                    ];
                }

                if (gridType == 'Orion.Groups')
                    dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/Containers.asmx/GetPagedContainersForGrid",
                                gridFields,
                                "Name");
                else
                    dataStore = new ORION.WebServiceStore(
                                "/Orion/Services/WebAdmin.asmx/GetGridObjects",
                                gridFields,
                                "DisplayName");

                var groupingStore = new ORION.WebServiceStore(
                                "/Orion/Services/WebAdmin.asmx/GetObjectPropertyValues",
                                [
                                    { name: 'Value', mapping: 0 },
                                    { name: 'Cnt', mapping: 1 }
                                ],
                                "Value", "");

                var groupingDataStore = new ORION.WebServiceStore(
                                "/Orion/Services/WebAdmin.asmx/GetFilteredObjectProperties",
                                [
                                    { name: 'Name', mapping: 1 },
                                    { name: 'Value', mapping: 0 },
                                    { name: 'Type', mapping: 2 }
                                ]);

                var availableObjectsStore = new ORION.WebServiceStore(
                                "/Orion/Services/WebAdmin.asmx/GetDependencyObjects",
                                [
                                    { name: 'Key', mapping: 0 },
                                    { name: 'Value', mapping: 1 }
                                ]);

                comboArray = new Ext.form.ComboBox({
                    fieldLabel: 'Name',
                    hiddenName: 'Value',
                    store: groupingDataStore,
                    displayField: 'Name',
                    triggerAction: 'all',
                    value: '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}',
                    typeAhead: true,
                    mode: 'local',
                    forceSelection: true,
                    selectOnFocus: false
                });

                showOnlyCombo = new Ext.form.ComboBox({
                    fieldLabel: 'Value',
                    hiddenName: 'Key',
                    store: availableObjectsStore,
                    displayField: 'Value',
                    triggerAction: 'all',
                    value: (selDepObject == null || selDepObject.length == 0) ? '@{R=Core.Strings;K=WEBJS_VB0_77; E=js}' : selDepObject,
                    typeAhead: true,
                    mode: 'local',
                    forceSelection: true,
                    selectOnFocus: false,
                    hidden: true
                });

                var pagingToolbar = new Ext.PagingToolbar({
                    store: dataStore,
                    pageSize: pageSize,
                    displayInfo: true,
                    displayMsg: '@{R=Core.Strings;K=WEBJS_VB0_66; E=js}',
                    emptyMsg: "@{R=Core.Strings;K=WEBJS_VB0_65; E=js}",
                    beforePageText: "@{R=Core.Strings;K=WEBJS_VB0_39; E=js}",
                    afterPageText: "@{R=Core.Strings;K=WEBJS_AK0_12; E=js}",
                    firstText: "@{R=Core.Strings;K=WEBJS_VB0_40; E=js}",
                    prevText: "@{R=Core.Strings;K=WEBJS_VB0_41; E=js}",
                    nextText: "@{R=Core.Strings;K=WEBJS_VB0_42; E=js}",
                    lastText: "@{R=Core.Strings;K=WEBJS_VB0_43; E=js}",
                    refreshText: "@{R=Core.Strings;K=CommonButtonType_Refresh; E=js}",
                    doLoad: function (start) {
                        var o = {}, pn = this.getParams();
                        o[pn.start] = start;
                        o[pn.limit] = this.pageSize;
                        if (this.fireEvent('beforechange', this, o) !== false) {
                            this.store.load({ params: o, callback: function () {
                                highlightSearchString(search);
                                setActiveNode();
                            }
                            });
                        }
                    },
                    onClick: function (which) {
                        var store = this.store;
                        switch (which) {
                            case "first":
                                this.doLoad(0);
                                break;
                            case "prev":
                                this.doLoad(Math.max(0, this.cursor - this.pageSize));
                                break;
                            case "next":
                                this.doLoad(this.cursor + this.pageSize);
                                break;
                            case "last":
                                var total = store.getTotalCount();
                                var extra = total % this.pageSize;
                                var lastStart = extra ? (total - extra) : total - this.pageSize;
                                this.doLoad(lastStart);
                                break;
                            case "refresh":
                                var value = "";
                                var obj = this;
                                if (groupingGridValue != '') {
                                    comboArray.setValue(groupingGridValue);
                                    for (i = 0; i < comboArray.store.data.items.length; i++) {
                                        if (comboArray.store.data.items[i].data.Name == groupingGridValue) {
                                            value = comboArray.store.data.items[i].data.Value;
                                            break;
                                        }
                                    }
                                }
                                if (value == "")
                                    obj.doLoad(obj.cursor);
                                else
                                    refreshObjects(groupGrid, value, gridType, "", searchText, function () {
                                        obj.doLoad(obj.cursor);
                                    });
                                break;
                        }
                    }
                });

                grid = new Ext.grid.GridPanel({
                    region: 'center',
                    store: dataStore,
                    columns: gridColumns,
                    sm: selectorModel,
                    layout: 'fit',
                    autoScroll: 'true',
                    loadMask: true,
                    width: 400,
                    height: 380,
                    stripeRows: true,
                    viewReady: true,

                    bbar: pagingToolbar,
                    listeners: {
                        viewready: function () {
                            highlightSearchString(search);
                            setActiveNode();
                        },
                        rowclick: function (grid, rowIndex, e) {
                            Ext.each(grid.getSelectionModel().getSelections(), function (item) {
                                if (groupingGridValue.length > 0 && groupingCellValue.length > 0) {
                                    $("#" + $("#groupingGridValueIdRef")[0].value)[0].value = /*"groupingGridValue=" + */groupingGridValue;
                                    $("#" + $("#groupingCellValueIdRef")[0].value)[0].value = /*"groupingCellValue=" + */groupingCellValue;
                                    $("#" + $("#groupingSelectedRowIdRef")[0].value)[0].value = /*"groupingSelectedRow=" + */groupingSelectedRow;
                                }

                                $("#" + $("#selObjectIdRef")[0].value)[0].value = item.data.Uri;
                                $("#traceRouteHiddenField").val(item.data.IP_Address);
                                $("#" + $('#entityNameRef')[0].value)[0].value = gridType;
                                __doPostBack($("#selObjectIdRef")[0].value, "N:" + item.data.ID);
                            });
                        }
                    }
                });

                var groupGrid = new Ext.grid.GridPanel({
                    id: 'groupingGrid',
                    cls: 'hide-header',
                    store: groupingStore,
                    autoHeight: true,
                    columns: [{
                        width: 190,
                        editable: false,
                        sortable: false,
                        dataIndex: 'Value',
                        renderer: renderGroup
                    }],
                    selModel: new Ext.grid.RowSelectionModel(),
                    layout: 'fit',
                    autoScroll: 'true',
                    loadMask: true,
                    listeners: {
                        cellclick: function (mygrid, row, cell, e) {
                            var val = mygrid.store.data.items[row].data[mygrid.store.data.items[row].fields.keys[cell]];

                            groupingCellValue = val;
                            groupingSelectedRow = row;
                            val = (val == '@{R=Core.Strings;K=WEBDATA_PS0_0;E=js}') ? '' : val;

                            var groupVal = '';
                            for (i = 0; i < comboArray.store.data.items.length; i++) {
                                if (comboArray.store.data.items[i].data.Name == groupingGridValue) {
                                    groupVal = comboArray.store.data.items[i].data.Value;
                                    break;
                                }
                            }

                            refreshObjects(grid, groupVal, gridType, val, searchText, function () {
                                highlightSearchString(search);
                                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                    this.tooltip = 'processed';
                                    $.swtooltip(this);
                                });
                            });
                        }
                    },

                    anchor: '0 0', viewConfig: { forceFit: true }, split: true, autoExpandColumn: 'Value'
                });

                var groupBy = new Ext.Panel({
                    region: 'center',
                    cls: 'panel-bg-gradient panel-no-border',
                    split: false,
                    autoHeight: true,
                    viewConfig: { forceFit: true },
                    collapsible: false,
                    items: [showOnlyCombo, new Ext.form.Label({ text: "@{R=Core.Strings;K=WEBJS_AK0_76; E=js}" }), comboArray]
                });
                var nav = new Ext.Panel({ region: 'west', animCollapse: false, split: true, width: 205, viewConfig: { forceFit: true }, items: [groupBy, groupGrid] });
                var mobile = getURLParameter("IsMobileView");
                if ('true' == mobile) {
                    grid.autoHeight = true,
                    nav = new Ext.Panel({ region: 'north', title: 'FILTERING', collapsible: true, autoHeight:true, collapseMode: 'header', titleCollapse:true, split: true, width: 205, items: [groupBy, groupGrid] });
                }

                var mainGridPanel = new Ext.Panel({ id: 'mainGrid', region: 'center', split: true, width: 778, height: 380, layout: 'border', collapsible: false, items: [nav, grid], cls: 'no-border' }); // , hidden:true});

                nav.on('bodyresize', function () {
                    groupGrid.setSize(nav.getSize().width, 492);
                });

                showOnlyCombo.on('select', function () {
                    selDepObject = this.getValue();
                    gridType = this.store.data.items[this.selectedIndex].data.Key;

                    $("#divGrid")[0].innerHTML = '';
                    groupingGridValue = ""; groupingCellValue = ""; groupingSelectedRow = -1;
                    SW.Orion.SelectNode.init();
                });

                comboArray.on('select', function () {
                    var val = this.store.data.items[comboArray.selectedIndex].data.Value;
                    groupingGridValue = this.getValue();
                    refreshObjects(groupGrid, val, gridType, "", searchText, function () {
                        if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_JP0_1; E=js}')
                            addVendorIcons();
                        if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_VB0_76; E=js}') {
                            groupingGridValue = "";
                            refreshObjects(grid, "", gridType, "", searchText, function () {
                                highlightSearchString(search);
                                $("a[tooltip!='processed'][href*='NetObject=']:not(.NoTip)").livequery(function () {
                                    this.tooltip = 'processed';
                                    $.swtooltip(this);
                                });
                            });
                        }
                    });
                    groupingCellValue = ""; groupingSelectedRow = -1;
                });

                refreshGridObjects(showOnlyCombo, isParentDependency, function () {
                    refreshObjects(comboArray, "", gridType, "", searchText, function () {
                        var value = "";
                        if (groupingGridValue != '') {
                            comboArray.setValue(groupingGridValue);
                            for (i = 0; i < comboArray.store.data.items.length; i++) {
                                if (comboArray.store.data.items[i].data.Name == groupingGridValue) {
                                    value = comboArray.store.data.items[i].data.Value;
                                    break;
                                }
                            }
                        }
                        refreshObjects(groupGrid, value, gridType, "", searchText, function () {
                            if (comboArray.getValue() == '@{R=Core.Strings;K=WEBJS_JP0_1; E=js}')
                                addVendorIcons();

                            groupGrid.getView().focusRow(groupingSelectedRow);
                            groupGrid.getSelectionModel().selectRow(groupingSelectedRow);
                            var store = groupGrid.getStore();
                            var grVal = (groupingSelectedRow == -1) || (store.data.items.length == 0) ? "" : store.getAt(groupingSelectedRow).get("Value");
                            var uri = $("#" + $("#selObjectIdRef")[0].value)[0].value;
                            getEntityRowNumber(
                                    gridType,
                                        uri,
                                        search,
                                        function (res) {
                                            var pageNumFl = res.ind / 20;
                                            var index = parseInt(pageNumFl.toString());

                                            grid.store.removeAll();
                                            grid.store.proxy.conn.jsonData = {
                                                property: value,
                                                type: gridType,
                                                value: grVal == '@{R=Core.Strings;K=WEBDATA_PS0_0;E=js}' ? "" : grVal,
                                                search: search,
                                                prefix: dependencyPrefix
                                            };

                                            if (index < 0) index = 0;

                                            grid.store.load({ params: { start: (index * 20), limit: 20 }, callback: function () {
                                                var ind = res.ind % 20;
                                                if (ind > -1) {
                                                    grid.getView().focusRow(ind);
                                                    grid.getSelectionModel().selectRow(ind);
                                                }
                                                highlightSearchString(search);
                                            }
                                            });
                                        });
                        });
                    });
                });

                mainGridPanel.render('divGrid');
            }
        };
    } ();

    // Ext.onReady(SW.Orion.SelectNode.init, SW.Orion.NetObjects);
    //  $("#divGrid").css('visibility', 'collapse');
    //  $("#divGrid").css("height", "0px");
});

