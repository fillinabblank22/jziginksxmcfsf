﻿Ext.namespace('SW');
Ext.namespace('SW.Core');
var saveChanges;
var cancelChanges;
var selectorModelForAll;
var selectorModelForSelected;
var allDataStore;
var selectedDataStore;
var gridAll;
var gridSelected;
var tree;

//Moving items to selected grid
var GridRecord = Ext.data.Record.create(['Name', 'DisplayName', 'Header','SubHeader', 'ShortName']);
addSelectedColumns = function() {
    var selections = tree.getChecked();
    for (var i = 0; i < selections.length; i++) {
        if (!selections[i].hidden) {
            if (selections[i].isLeaf()) {
                var record = new GridRecord({
                    Name: selections[i].attributes['name'],
                    DisplayName: selections[i].attributes['displayName'],
                    Header: selections[i].attributes['header'],
                    SubHeader: selections[i].attributes['subHeader'],
                    ShortName: selections[i].attributes['shortName']
                });
                gridSelected.getStore().add(record);
            }

            selections[i].ui.toggleCheck(false);
            selections[i].ui.hide();
            selections[i].parentNode.attributes.childsNumber -= 1;
            var count = selections[i].parentNode.attributes.childsNumber;
            if (selections[i].parentNode.ui.textNode)
                selections[i].parentNode.ui.textNode.innerHTML = selections[i].parentNode.attributes.displayName + ' (' + count + ')';
        }
    }
};

//movinf items from selected grid
function removeSelectedColumns() {
    var selections = gridSelected.getSelectionModel().getSelections();
    for (var i = 0; i < selections.length; i++) {
        var header = selections[i].data['Header'];
        var subHeader = selections[i].data['SubHeader'];
        var name = selections[i].data['Name'];
        //searching header
        var node = tree.root.findChild('displayName',header);
        node.ui.show();  
        node.ui.toggleCheck(false);
        for (var j = 0; j < node.childNodes.length; j++) {
                if (node.childNodes[j].attributes.name === name) {
                node.ui.textNode.innerHTML = node.attributes.displayName +' (' + ++node.attributes.childsNumber + ')'; 
                node.childNodes[j].ui.toggleCheck(false);
                node.childNodes[j].ui.show();
            }
        }
        //searching subHeader    
        if (subHeader) {
            var subNode = node.findChild('displayName', subHeader);
            if(subNode.hidden)
                node.ui.textNode.innerHTML = node.attributes.displayName +' (' + ++node.attributes.childsNumber + ')';
            subNode.ui.toggleCheck(false);
            subNode.ui.show();
            
            for (var k = 0; k < subNode.childNodes.length; k++) {
                 if (subNode.childNodes[k].attributes.name === name) {
                    subNode.ui.textNode.innerHTML = subNode.attributes.displayName +' (' + ++subNode.attributes.childsNumber + ')'; 
                    subNode.childNodes[k].ui.toggleCheck(false);
                    subNode.childNodes[k].ui.show();
                }
            }
        }
        gridSelected.getStore().remove(selections[i]);
    }
}

function saveChartLimits() {
    var settingsJson;
    var limitType;
    var limitByTime= $("[id$='rdLimitByTime']").prop("checked");
    if (limitByTime)
        limitType = 'LimitByTime';
    else
        limitType = 'LimitByPoints';

    var timeValue = $("#txtLimitByTime").val();
    var timeType = $("#drpValueType").val();
    if (timeType === "Hours")
        timeValue *= 60;
    var pointsValue =  $("#txtLimitByPoints").val();
    var pollingInterval =  $("#txtPollingInterval").val();
    
    settingsJson = {LimitType: limitType, TimeValue: timeValue, PointsValue: pointsValue, PollingInterval: pollingInterval};
    return settingsJson;
}

function saveThresholds() {
    var tresholds = Ext.query('.ThresholdBlock');
    var count = 0;
    var settingsJson = [];
    Ext.each(tresholds, function(item, index) {
        var treshold = Ext.get(item);
        var name = Ext.get(treshold.query('#ThresholdName')).elements[0].value;
        var useThresholds = Ext.get(treshold.query('.trigger input[type="checkbox"]')).elements[0].checked;
        var warnLvl = Ext.get(treshold.query("[id$='drpWarningLevel']")).elements[0].value;
        var warnVal = Ext.get(treshold.query("[id$='txtWarningValue']")).elements[0].value;
        var critVal = Ext.get(treshold.query("[id$='txtCriticalValue']")).elements[0].value;

        settingsJson.push({Name: name, UseThresholds: useThresholds, WarningLevel: warnLvl, WarningValue: warnVal, CriticalValue: critVal});
        count++;
    });
    if (count > 0) {
        return settingsJson;
    }
}

function saveGrid() {
    var data = Ext.encode(Ext.pluck(selectedDataStore.data.items, 'data'));
    return data;
}

//Save changes and show result message
function saveSettings(obj) {
    try {
       $.ajaxSetup({async:false});
       var thresholds = saveThresholds();
       var metrics = saveGrid();
       var charts = saveChartLimits();
       
        var settings = JSON.stringify({charts: charts, table: metrics, thresholds:thresholds });
        var result = GetJSONDataNoAlertWhenFails(saveLink, "POST", settings);
        
        if (obj !== 'dummy') {
            window.location.replace(obj);
        }
        if (!result) {
            $("#successMsg").css("display", "none");
            $("#failedMsg").css("display", "block");
        } else {
            $("#successMsg").css("display", "block");
            $("#failedMsg").css("display", "none");
        }
        $.ajaxSetup({ async: true });
    }
    catch(e) {
        $("#successMsg").css("display", "none");
        $("#failedMsg").css("display", "block");
    }
}

//Get parametrs from URL, for getting IsMobileView parametr
function getURLParameter(name) {
        return decodeURI(
            (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
        );
    };

ToolsetSettingsManager = function() {
    function encodeHTML(htmlText) {
        return $('<div/>').text(htmlText).html();
    };

    function renderColumn(value, meta, record) {
        return encodeHTML(value);
    };  
    
    //Select/deselect tree columns
    function selectTreeColumns(all) {
       tree.getRootNode().cascade(function(n) {
            var ui = n.getUI();
            ui.toggleCheck(all);
        });
        if(!all)
        tree.collapseAll();
    }
    
    //Select/deselect grid columns
    function selectColumns(all, selectionModel) {
        if (all) {
            selectionModel.selectAll();
        } else
            selectionModel.clearSelections();
    }
        
  function moveSelectedRow(grid, direction) {
        var records = grid.getSelectionModel().getSelections();
        records = records.sort(function(a,b){return grid.getStore().indexOf(a) - grid.getStore().indexOf(b)});
        var isEnd = false;

        if (direction > 0) {
            for (var j = 0; j < grid.getStore().getCount(); j++) {
                if (records.length - 1 - j < 0) {
                    break;
                }
                if (grid.getStore().getCount() - 1 - j != grid.getStore().indexOf(records[records.length - 1 - j])) {
                    isEnd = false;
                    continue;
                }
                isEnd = true;
            }
        }
        if (isEnd) {
            return;
        }
        if (direction < 0) {
            for (var k = 0; k < records.length; k++) {
                if (k != grid.getStore().indexOf(records[k])) {
                    isEnd = false;
                    continue;
                }
                isEnd = true;
            }
        }

        if (isEnd) {
            return;
        }

        for (var i = 0; i < records.length; i++) {
            var record;

            if (direction < 0) {
                record = records[i];
            } else {
                record = records[records.length - 1 - i];
            }
            if (!record) {
                continue;
            }
            var index = grid.getStore().indexOf(record);
            if (direction < 0) {
                index--;
                if (index < 0) {
                    continue;
                }
            } else {
                index++;
                if (index >= grid.getStore().getCount()) {
                    continue;
                }
            }
            grid.getStore().remove(record);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().selectRow(index, true);
        }
    }

    return {
        reload: function() { //gridAll.getStore().reload(); 
        },
        init: function() { 
            function GetResultColumns(serviceUrl, method, passData) {
        var resultColumns;
        $.ajax({
            type: method,
            url: serviceUrl,
            data: passData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                resultColumns = data.d;
            },
            error: function (data, textStatus, xhr) {
                alert(xhr);
            }
        });
        return resultColumns;
    }

    //Define tree panel
    var currentTreeData = GetResultColumns(allSettingsLink, "POST", {});
            tree = new Ext.tree.TreePanel({
                renderTo: 'Grid',
                id: 'gridAll',
                collapsible: true,
                style: 'padding-bottom: 5px',
                loader: new Ext.tree.TreeLoader(),
                rootVisible: false,
                useArrows:true,
                autoScroll: true,
                lines: false,
                root: new Ext.tree.TreeNode({
                    children: currentTreeData,
                }),
                hideHeaders: true,
                width: 300,
                height: 400,
                frame: true,
                bodyStyle: 'background-color:#fff;',
                title: 'Available Metrics',
                tbar: [
                    {
                        id: 'selectAllButtonAva',
                        text: 'Select All',
                        iconCls: 'selectall',
                        handler: function() { selectTreeColumns(true); }
                    }, '-',
                    {
                        id: 'selectNoneButtonAva',
                        text: 'Select None',
                        iconCls: 'selectnone',
                        handler: function() { selectTreeColumns(false); }
                    },
                    {
                        xtype: 'checkbox',
                        name: 'SelectAll',
                        boxLabel: 'Select/Deselect All',
                        iconCls: '',
                        id: 'cbSelectAll',
                        listeners: {
                            check: function() {
                                var checked = Ext.getDom('cbSelectAll').checked;
                                selectTreeColumns(checked);
                            }
                        },
                    }
                ],
                buttonAlign: 'center',
                bbar: [
                { 
                    id: 'addSelected',
                    text: 'Add Selected Metrics',
              //      html: [ '<button id="aff2" class="affff" type="button" ">Add Selected Metrics</button>'],
                    handler: function() { addSelectedColumns(); }
                    }
                ],
                listeners: {
                    click : function(node) {
                        if (node) {
                            node.getUI().toggleCheck();
                            node.cascade(function(n) {
                                if (n != node) {
                                    var ui = n.getUI();
                                    ui.toggleCheck(node.getUI().checkbox.checked);
                                }
                            });
                        }
                    }
                }
            });
    //Checking if child nodes are checked        
    function checkChildNodes(node, checked) {
        node.eachChild(function (n) {
            n.getUI().toggleCheck(checked);
            if (n.hasChildNodes) {
                checkChildNodes(n, checked);
            }
        });
        if(!checked)
            node.collapse();
    }
    tree.on('checkchange', function (node, checked) { 
        checkChildNodes(node, checked);
    });
    
    tree.getLoader().load(tree.root);  
    //Child nodes initialized only after expanding. TODO find out another solution.
    tree.expandAll();
    tree.collapseAll();
            
    selectorModelForSelected = new Ext.grid.CheckboxSelectionModel({
                singleSelect: false,
                checkOnly: true
            });
    selectedDataStore = new ORION.WebServiceStore(
        selectedSettingsLink,
        [
            { name: 'Name', mapping: 0 },
            { name: 'DisplayName', mapping: 1 },
            { name: 'Header', mapping: 2},
            { name: 'SubHeader', mapping: 3},
            { name: 'ShortName', mapping: 4}
        ],
        "");
        // define grid panel
        gridSelected = new Ext.grid.GridPanel({
            store: selectedDataStore,
            columns: [
                selectorModelForSelected,
                { header: 'Name', width: 0, hidden: true, hideable: false, sortable: false, dataIndex: 'Name' },
                { header: 'DisplayName', dataIndex: 'DisplayName', width: 798, sortable: false, menuDisabled: true, render: renderColumn },
                { header: 'Header', dataIndex: 'Header', hideable: false, hidden: true},
                { header: 'ShortName', dataIndex: 'ShortName', hideable:false, hidden: true}
            ],
            frame: true,
            id: 'gridSel',
            bodyStyle: 'background-color:#fff;',
            hideHeaders: true,
            sm: selectorModelForSelected,
            viewConfig: {
                forceFit: true,
                scrollOffset: 0,
                hideSortIcons: true,
                stripeRows: false
            },
            autoScroll: false,
            layout: 'fit',
            height: 400,
            title: 'Selected Metrics',
            tbar: [
                {
                    id: 'SelectAll',
                    text: 'Select All',
                    iconCls: 'selectall',
                    handler: function() { selectColumns(true, selectorModelForSelected); }
                }, '-',
                {
                    id: 'SelectNone',
                    text: 'Select None',
                    iconCls: 'selectnone',
                    handler: function() { selectColumns(false, selectorModelForSelected); }
                }, '-',
                {
                    id: 'MoveUpButton',
                    text: 'Move Up',
                    iconCls: 'moveup',
                    handler: function() { moveSelectedRow(gridSelected, -1); }
                },'-',
                {
                    id: 'MoveDownButton',
                    text: 'Move Down',
                    iconCls: 'movedown',
                    handler: function() { moveSelectedRow(gridSelected, 1);}
                },
                {
                    id: 'RemoveSelectedButton',
                    text: 'Remove Selected',
                    iconCls: 'delete',
                    handler: function() { removeSelectedColumns(); }
                },
                {
                    xtype: 'checkbox',
                    name: 'SelectAll',
                    boxLabel: 'Select/Deselect All',
                    iconCls: '',
                    id: 'cbSelectAllSG',
                    listeners: {
                        check: function() {
                            var checked = Ext.getDom('cbSelectAllSG').checked;
                            selectColumns(checked, selectorModelForSelected);
                        }
                    },
                }
            ],
                listeners: {
                    cellclick: function(grid, rowIndex, columnIndex, e) {
                        if (columnIndex == 0)
                            return;

                        if (grid.getSelectionModel().isSelected(rowIndex)) {
                            grid.getSelectionModel().deselectRow(rowIndex);
                        } else {
                            grid.getSelectionModel().selectRow(rowIndex, true);
                        }
                    }
                }
        });
            
        //Hide/show elements according to mode(Mobile or desktop)
        var cbSelectAllTG = Ext.getCmp('cbSelectAll');
        var btnAddSell = Ext.getCmp('addSelected');
        var btnSell = Ext.getCmp('SelectAll');
        var btnDeSell = Ext.getCmp('SelectNone');
        var btnRemoveSlct = Ext.getCmp('RemoveSelectedButton');
        var cbSelectAllSG = Ext.getCmp('cbSelectAllSG');
        if (getURLParameter("IsMobileView") === 'null') {
            cbSelectAllSG.hide();
            cbSelectAllTG.container.setStyle('display', 'none');
            btnRemoveSlct.hide();
            btnAddSell.hide();
        } else {
            cbSelectAllTG.container.setStyle('display', 'block');
            btnSell.hide();
            btnDeSell.hide();
            btnRemoveSlct.show();
            cbSelectAllSG.show();
            btnAddSell.show();
            //Moving Units td below previous td
            $('.Units').each(function(){
                var self = $(this);
                self.parent().after('<tr>').next().append(self);
                self.before('<td></td>').next().append('<td></td>');
            });
            gridSelected.autoHeight = true;
        }

        gridSelected.render('GridSelected');
        selectorModelForSelected.fireEvent('selectionchange');
        gridSelected.store.proxy.conn.jsonData = {};
        gridSelected.store.load();

        preLoadImages();
        }
    };
}();

//Temporary workaround to pre-load imgs, that aren`t displaying in mobile safari.
function preLoadImages  () {
    var expandErrorIcon = new Image();
    
    expandErrorIcon.src = "../../../js/extjs/4.2.2/resources/images/tree/arrows.gif";
}

function SaveChanges() {
    saveChanges();
}

function CancelChanges() {
    cancelChanges();
}
