﻿var isInApplyModeValue = false;
var currentTaskID = null;
var cachedResultColumnsPromise = null;
var cacheCleaner = null;


$(document).ready(function(){
    Ext42.require([
    'Ext42.data.*',
    'Ext42.grid.*',
    'Ext42.tree.*'
    ]);
});

(function(){

  var getTitle = function(){ return "Memory Monitor results for " + $("#itemsCount").html(); };

    var currentdate = new Date(); 
    var fileName = "MemoryMonitorResults " 
                + currentdate.getFullYear() + "-" 
                +(currentdate.getMonth()+1) + "-" 
                + currentdate.getDate() + " " 
                + currentdate.getHours() + "."  
                + currentdate.getMinutes() + "." 
                + currentdate.getSeconds();               
  SW.Toolset.Export.Init({ 
    txt: { domid: 'ExportToTxtId', grid: function(){ return resultsGridObj.grid; }, file: fileName+ '.txt', title: getTitle },
    csv: { domid: 'ExportToCSVId', grid: function(){ return resultsGridObj.grid; }, file:  fileName+ '.csv', title: getTitle },
    pdf: { domid: 'ExportToPdf', grid: function(){ return resultsGridObj.grid; }, file:  fileName+ '.pdf', title: 'Memory Monitor results', subtitle: getTitle, mode: function(){ return isTableView() ? "table" : "chart"; } }
  });

})();


function InvalidateResultColumnsCache() {
    cachedResultColumnsPromise = null;
}

function InitResultHub(taskId) {
    if (taskId) {
        InitResultNotificationClient(taskId, ReceiveMemoryMonitorResults, InvalidateResultColumnsCache);
        //delete cache once a 10 minutes
        cacheCleaner = setInterval(InvalidateResultColumnsCache, 600000);
        SetUpTaskId(taskId);
        $("#stopSpinnerDiv").hide();
        $("#export-button-div").hide();
        $("#spinnerDiv").show();
    }
}

//asynchronously prepares current/cached result columns
function getResultColumnsAsync() {
    if (cachedResultColumnsPromise == null) {
        cachedResultColumnsPromise = CallServiceAsync("/Orion/Toolset/Services/MemoryMonitorSettings.asmx/GetResultColumns", "GET", {});
    }
    return cachedResultColumnsPromise;
}

//processes data from SignalR
function ReceiveMemoryMonitorResults(newResult) {
    if (!IsValidResultsReceived(newResult.MemoryDetails)) {
        return;
    }

    $("#lastUpdate").val(new Date().getTime());
    //get result columns, render result grid and update chart asynchronously
    var resultColumnsPromise = getResultColumnsAsync();
    var addResultToChartPromise = addResultToChartAsync(newResult.MemoryDetails, newResult.CompletionInfo.CompletionTimeMsSinceEpoch, resultColumnsPromise);

    //when done, add toolset integration
    $.when(addResultToChartPromise)
        //rendering result grid destroys data used by chart - chart must be rendered first
        .pipe(updateResultGridAsync(newResult, resultColumnsPromise))
        .then(function () {
        $("#export-button-div").show();
    });
}

//asynchronously renders result grid
function updateResultGridAsync(newResult, resultColumnsPromise) {
    return resultColumnsPromise.pipe(function (data) {
        var resultcolumns = data.d;

        for (var i = 0; i < newResult.MemoryDetails.length; i++) {
            newResult.MemoryDetails[i].uiID = newResult.MemoryDetails[i].DisplayName + newResult.MemoryDetails[i].HostAddress;
        }
        if (IsMobile()) {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGridMobile(resultcolumns, newResult.MemoryDetails, 'signalRTableDiv', 400, 300);
});
        } else {
SW.Toolset.Visibility.CallWhenVisible( 'signalrtablediv', function(){
            CreateResultGrid(resultcolumns, newResult.MemoryDetails, 'signalRTableDiv', 400, 700);
});
        }
    });
}

function backToMainPage(taskId, items, itemsToPoll) {
    $("#CWHolder").hide();
    $("#memoryMonitorResults").show();
    if (taskId != "" || (taskId == "" && items == 0)) {
        var itemsExist = items > 0;
        if (itemsExist) {
            $('#itemsCount').html(items + ' selected memory item(s)');
            if (currentTaskID) {
                StopResultNotificationClient(currentTaskID);
                CallServiceAsync("/Orion/Toolset/Services/ConfigurationWizard.asmx/StopTask", "POST", JSON.stringify({ taskId: currentTaskID }));
                if (resultsGridObj.grid) {
                    var dataStore = resultsGridObj.grid.getStore();
                    var indexes = new Array();
                    dataStore.each(function (record) {
                        var exId = record.get('uiID');
                        var exists = false;
                        for (var i = 0; i < itemsToPoll.length; i++) {
                            for (var j = 0; j < itemsToPoll[i].Items.length; j++) {
                                if (itemsToPoll[i].Items[j] + itemsToPoll[i].IP == exId) {
                                    exists = true;
                                    continue;
                                }
                            }
                        }
                        if (!exists) {
                            var index = dataStore.indexOf(record);
                            indexes.push(index);
                        }
                    }, this);
                    indexes = indexes.sort();
                    for (var j = indexes.length - 1; j >= 0; j--) {
                        dataStore.removeAt(indexes[j]);
                    }
                }
            }
            InitResultHub(taskId);
            currentTaskID = taskId;
            $("#resultsTable").show();
            $("#noInterfacesMessage").hide();

            if ($("#viewTypeSelect").val() == "table") {
                switchToTableView();
            } else {
                switchToPlotView();
            }
        } else {
            $("#switchView").hide();
            $("#resultsTable").hide();
            $("#noInterfacesMessage").show();
        }
    }

}

function switchToTableView() {
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 1);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnActive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnInactive');
    $("#chartcontainer").hide();
    $("#signalRTableDiv").show();
    $("form").css("background", "#F5F5F5");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("#viewTypeSelect").val("table");
    if (resultsGridObj.grid) resultsGridObj.grid.getView().refresh();
    setTableView();
}

function switchToPlotView() {
    SW.Toolset.Visibility.Mask('chart', 1);
    SW.Toolset.Visibility.Mask('chart-grid', 1);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#tableViewDiv").removeClass();
    $("#tableViewDiv").addClass('tableViewBtnInactive');
    $("#plotViewDiv").removeClass();
    $("#plotViewDiv").addClass('plotViewBtnActive');
    $("#chartcontainer").show();
    $("#signalRTableDiv").hide();
    $("form").css("background", "white");
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("#viewTypeSelect").val("chart");
    fixMetricsPanel();
    if (chartGridObj.grid) chartGridObj.grid.getView().refresh();
    if (resultsChart) resultsChart.render();
    setChartView();
}

function isInApplyMode() {
    return isInApplyModeValue;
}

function backToConfigWizard() {
    SW.Toolset.Visibility.Mask('chart', 0);
    SW.Toolset.Visibility.Mask('chart-grid', 0);
    SW.Toolset.Visibility.Mask('signalrtablediv', 0);
    $("#resultsTableHeader").css("background", "#F5F5F5");
    $("form").css("background", "#F5F5F5");
    $("#CWHolder").show();
    $("#memoryMonitorResults").hide();
    isInApplyModeValue = true;
}

function StopTask() {
    Ext42.MessageBox.confirm({
        title: 'Confirmation',
        width: 300,
        msg: 'Are you sure you want to stop the current real-time procedure? If you try to restart the procedure later, all data collected up to this point will be lost.',
        buttons: Ext42.MessageBox.YESNO,
        fn: function (btn) {
            if (btn == 'yes') {
                CancelMemoryMonitorPolling();
                if (resultsChart) {
                    resultsChart.render();
                }
            }
        },
        icon: Ext42.MessageBox.WARNING
    });
}

function CancelMemoryMonitorPolling() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        CancelTask(taskId);
        $("#taskIdTextbox").val("");
    }

    $("#spinnerDiv").hide();
    $("#stopSpinnerDiv").show();
}

function StopMemoryMonitorResultNotification() {
    var taskId = $("#taskIdTextbox").val();
    if (taskId) {
        StopResultNotificationClient(taskId);
        clearInterval(cacheCleaner);
    }
}
