
// SW.Toolset.Export.Init : see traceroute.js for example.
// 
//  SW.Toolset.Export.Init({ 
//    txt: { domid: 'domid', grid: function(){ return resultsGridObj.grid; }, file: 'filename', title: 'title' },
//    ...
//  });


(function(){

  var settings = {};

  function getExportCfg(key){
    var opts = $.extend( {}, settings[key] || {} );
    if( typeof(opts.grid) == "function" ) opts.grid = opts.grid();
    if( typeof(opts.title) == "function" ) opts.title = opts.title();
    if( typeof(opts.subtitle) == "function" ) opts.subtitle = opts.subtitle();
    if( typeof(opts.file) == "function" ) opts.file = opts.file();
    if( typeof(opts.mode) == "function" ) opts.mode = opts.mode();
    return opts;
  };

  function doCsv() {
    var opts = getExportCfg('csv');
    if( opts.grid ) ExportToCSV(opts.grid, opts.file, opts.title);
  };

  function doTxt() { 
    var opts = getExportCfg('txt');
    if(opts.grid) ExportToTxt(opts.grid, opts.file, opts.title);
  };

  function doPdf() { 
    var opts = getExportCfg('pdf');
    if(!opts.grid) return;

    if(opts.mode == "table") {
       ExportToPdfServer( opts.grid, opts.file, opts.title, opts.subtitle);
    }
    else {
      var body = $('body'), bgi = body.css('background-image');
      body.css('background-image','none');
      ExportToPDF("chartcontainer",opts.file, "landscape", true);
      body.css('background-image', bgi);
    }
  };

  function doExport(exportFn){
      CreateResultGrid.Next(exportFn); // provide 'continue delegate' to the grid.
      if( SW.Toolset.Visibility.Force('signalrtablediv') ) return; // kicked off w/ delegate
      CreateResultGrid.Next(null); // clear it as we have the table already rendered
      exportFn(); // running it now.
  };

  //

  SW.Core.namespace('SW.Toolset.Export').Init = function(config){
    var opts = config;

    $(document).ready(function(){
      if( settings['csv'] = opts.csv ) $('#'+opts.csv.domid).click(function(){ doExport(doCsv); });
      if( settings['txt'] = opts.txt ) $('#'+opts.txt.domid).click(function(){ doExport(doTxt); });
      if( settings['pdf'] = opts.pdf ) $('#'+opts.pdf.domid).click(function(){ doExport(doPdf); });
    });
  };

})();
