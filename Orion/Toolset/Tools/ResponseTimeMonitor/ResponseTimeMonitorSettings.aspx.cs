﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Windows.Forms.VisualStyles;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Toolset.Common.Extensions;
using SolarWinds.Toolset.Common.ResultColumnsSettings;
using SolarWinds.Toolset.Common.Tools;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using Label = System.Web.UI.WebControls.Label;
using SolarWinds.Toolset.Tools.ResponseTimeMonitor;

public partial class Orion_Toolset_Tools_ResponseTimeMonitor_ResponseTimeMonitorSettings : System.Web.UI.Page
{
    public int PollingFrequency { get; set; }

    protected int ToolId
    {
        get
        {
            return SupportedTools.ResponseTimeMonitorToolId;
        }
    }

    public string ToolURL
    {
        get
        {
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (ResponseTimeMonitorTool)toolFactory.CreateTool();
            return UrlHelper.GetToolDetailAddress(tool.DetailPageUrl);
        }
    }

    private string _accountId;
    private ToolsDAL _toolsDal = new ToolsDAL();
    private SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        _accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        this.Master.Master.ImgLogoPath = "../images/AppIcons/response_time_monitor_logo.png";
        this.Master.Master.ToolId = ToolId;
        var items = Thresholds.Items;
        var settings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, User.Identity.Name);
        PollingFrequency = settings.ResponsePollInterval / 1000;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadDataFromDataSource();
        }
        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/MonitorsSettingsStyles.responsive.css";
        }
    }

    private void LoadDataFromDataSource()
    {
        var ds = CreateDataSource();
        Thresholds.DataSource = ds;
        Thresholds.DataBind();
        LoadChartLimits();
        LoadPollingInterval();
    }

    private void LoadPollingInterval()
    {
        var settings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);
        txtPollingInterval.Text = (settings.ResponsePollInterval / 1000).ToString();
    }

    private enum TimeLimitValueType
    {
        Minutes,
        Hours
    }

    private void LoadChartLimits()
    {
        var settings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);

        if (settings.ChartDataLimitType == ChartsRetentionSettings.LimitByTime)
        {
            rdLimitByTime.Checked = true;
            txtLimitByPoints.Enabled = false;
        }
        else
        {
            txtLimitByTime.Enabled = false;
            drpValueType.Enabled = false;
            rdLimitByPoints.Checked = true;
        }

        drpValueType.Items.Add(new ListItem("Minutes", TimeLimitValueType.Minutes.ToString()));
        drpValueType.Items.Add(new ListItem("Hours", TimeLimitValueType.Hours.ToString()));

        var tmp = Convert.ToInt32(settings.ChartDataLimitByTimeMin.ToString());
        if (tmp > 120)
        {
            tmp = tmp / 60;
            drpValueType.SelectedValue = TimeLimitValueType.Hours.ToString();
        }
        txtLimitByTime.Text = tmp.ToString();
        txtLimitByPoints.Text = settings.ChartDataLimitByPoints.ToString();
    }

    private DataSet CreateDataSource()
    {
        _accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        DataTable dt = new DataTable();
        dt.Columns.Add("DisplayName", typeof(string));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("UseThreshold", typeof(bool));
        dt.Columns.Add("CriticalLevel", typeof(string));
        dt.Columns.Add("CriticalValue", typeof(string));
        dt.Columns.Add("WarningLevel", typeof(string));
        dt.Columns.Add("WarningValue", typeof(string));
        dt.Columns.Add("Units", typeof(string));

        var settings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);
        var objects = settings.GetAllColumns();

        foreach (var setting in objects)
        {
            if (setting.ValueType == ResultColumnValueType.None)
                continue;
            var row = dt.NewRow();
            row["DisplayName"] = setting.DisplayName;
            row["Name"] = setting.PropertyName;
            row["UseThreshold"] = setting.UseThreshold;
            row["CriticalLevel"] = setting.CriticalThresholdOperator.ToDescription();
            row["CriticalValue"] = setting.CriticalThresholdValue;
            row["WarningLevel"] = setting.WarningThresholdOperator;
            row["WarningValue"] = setting.WarningThresholdValue;
            if (setting.ValueType != ResultColumnValueType.None)
                row["Units"] = setting.Unit;

            dt.Rows.Add(row);
        }

        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        GoBack();
    }

    public void GoBack()
    {
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
        var tool = (ResponseTimeMonitorTool)toolFactory.CreateTool();
        Response.Redirect(UrlHelper.GetToolDetailAddress(tool.DetailPageUrl));
    }

    protected void ItemCreated(object sender, ListViewItemEventArgs e)
    {
        {
            LoadDropDownList(e);
        }
    }

    private static void AddExtraClassToThresholdsValues(ListViewItemEventArgs row)
    {
        var elementsList = new List<string>()
        {
            "txtWarningValue",
            "txtCriticalValue"
        };
        var className = "ThresholdsValues";
        AddExtraClass(elementsList, className, row);
    }

    private static void AddExtraClass(List<string> elementsList, string className, ListViewItemEventArgs row)
    {
        foreach (var element in elementsList)
        {
            var textBox = row.Item.FindControl(element) as System.Web.UI.WebControls.TextBox;
            if (textBox != null)
                textBox.CssClass = string.Format("{0}{1} {2}", className, row.Item.ID, textBox.CssClass);
        }
    }

    private static void LoadDropDownList(ListViewItemEventArgs e)
    {
        var warningLevel = (DropDownList)e.Item.FindControl("drpWarningLevel");
        foreach (ThresholdOperator foo in Enum.GetValues(typeof(ThresholdOperator)))
        {
            warningLevel.Items.Add(new ListItem(foo.ToDescription(), foo.ToString()));
        }
        AddExtraClassToThresholdsValues(e);
    }
}