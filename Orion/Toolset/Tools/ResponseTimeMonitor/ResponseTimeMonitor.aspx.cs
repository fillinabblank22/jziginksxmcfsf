﻿using System;
using System.Web.UI;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.ResponseTimeMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Tools_ResponseTime : Page
{
    public int ChartDataLimitByTimeMin;
    public int ChartDataLimitByPoints;
    public int ChartDataLimitType;

    public int PollingFrequency { get; set; }

    protected int ToolId
    {
        get
        {
            return SupportedTools.ResponseTimeMonitorToolId;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/ResponseTimeMonitorStyles.responsive.css";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        string newUrl = UrlHelper.SwapNetobjectWithToolParams();

        if (newUrl != null)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Redirect(newUrl);
        }

        this.Master.Master.ImgLogoPath = "../images/AppIcons/response_time_monitor_logo.png";
        this.Master.Master.ToolId = ToolId;

        var settings = (ResponseTimeMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, User.Identity.Name);
        PollingFrequency = settings.ResponsePollInterval / 1000;
        ChartDataLimitByPoints = settings.ChartDataLimitByPoints;
        ChartDataLimitByTimeMin = settings.ChartDataLimitByTimeMin;
        ChartDataLimitType = (int)settings.ChartDataLimitType;
    }
}
