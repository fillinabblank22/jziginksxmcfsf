﻿<%@ Page Title="Response Time Monitor" AutoEventWireup="true" CodeFile="ResponseTimeMonitor.aspx.cs"
    Inherits="Orion_Toolset_Tools_ResponseTime" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>
<%@ Register TagPrefix="uc" TagName="ConfigurationWizard" Src="~/Orion/Toolset/Controls/ConfigurationWizard.ascx" %>
<%@ Register TagPrefix="uc" TagName="ResultsChart" Src="~/Orion/Toolset/Controls/ResultsChart.ascx" %>
<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ToolDetails" runat="server">
    <link rel="stylesheet" type="text/css" href="../../../js/extjs/4.2.2/resources/ext-theme-gray-sandbox-42-all.css" />
    <orion:Include ID="Include2" runat="server" File="commonviewstyles.css"/>
    <script type="text/javascript" src="../../js/ResponseTimeMonitor.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Styles/MonitorGrid.css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/ResponseTimeMonitorStyles.css" />
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
   <%-- <script type="text/javascript" src="../../../js/OrionMasterPage.js/WebToolsetMenu.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="../../Styles/DropDownButton.css" />
    <script type="text/javascript" src="../../js/DropDownButtonTools.js"></script>
    <%if (!this.Master.IsMobileMode)
       {%>
    <link rel="stylesheet" type="text/css" href="../../Styles/ExportButtonStyles.css" />
    <% } %>

    <script type="text/javascript">
        var settingsPageName = "ResponseTimeMonitorSettings.aspx";
        var ChartDataLimitByTimeMin = <%= this.ChartDataLimitByTimeMin %>;
        var ChartDataLimitByPoints =  <%= this.ChartDataLimitByPoints %>;
        var ChartDataLimitType = <%= this.ChartDataLimitType %>;
        
        $(document).ready(function () {
            $("[id$='msetlink']").attr('href', 'ResponseTimeMonitorSettings.aspx');
            $(function () {
                $(".indent.setting").click(function () {
                    window.location = "ResponseTimeMonitorSettings.aspx";
                });

                  <%if (this.Master.IsMobileMode){%>
                    $("#switchView").insertAfter($("#resultsTable"));
                  <%}%>
            });
        });
    </script>
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true" />
    <div id="responseMonitor">
        <div id="CWHolder" style="padding:30px 30px 45px 30px;">
            <div style="clear: both">
                <span >Configure Response Time Monitor</span>
            </div>
            <%if (this.Master.IsMobileMode)
              {%>
            <uc:configurationwizard id="ConfigurationWizardMobile" toolid="5" runat="server"
                ismobileview="True" />
            <%}
              else
              {%>
           <div style="margin: 15px 0; clear: both">
                <span style="font-size: 12px">Configure your current task, select from your managed nodes in Orion or add non-Orion device.
                <br/> For permanent settings (metrics, thresholds and preferences) go to the <a href="ResponseTimeMonitorSettings.aspx" class="link" style="margin-left: 0px;"> Settings page.</a></span>
            </div>
            <uc:configurationwizard id="Configurationwizard" toolid="5" runat="server" />
            <%}%>
        </div>
         <div id="responseTimeMonitorResults" style="float: left; width: 100%; display: none;padding-bottom: 45px;">
            <div id="noNodesMessage">
                <div align="center">
                    <img style="vertical-align: middle" src="../../images/icon_bulb.png" />
                    <span style="vertical-align: middle; padding-left: 5px;">Select a node to monitor</span>
                </div>
                <div align="center">
                    <input type="button" id="LocalizableButton1" value="SELECT NODES" onclick="backToConfigWizard()" />
                </div>
            </div>
            <div id="resultsTable" style="display: none">
                <div id="resultsTableHeader">
                <div style="float: left" class="stats-line">
                    <b>Statistics for </b><b style="margin-left: 2px; margin-right: 2px;"><a onclick="backToConfigWizard()" class="link" style="margin-left: 0px;cursor: pointer"><span id="itemsCount"></span></a></b>
                </div>
                <div class="mobile_break" style="display: none">
                     <%if (!this.Master.IsMobileMode){%><br />  <%}%>
                    <br />
                    <hr color="#D6D6D6">
                </div>
                <div id="spinnerDiv" clientidmode="Static" style="float: left; margin-left: 20px;
                    height: 16px" class="spinnerDiv">
                    <img src="../../images/animated_loading_16x16.gif" style="display: inline-block;"></img><span
                        style="display: inline-block; vertical-align: top; padding-left: 5px">Monitoring in progress... <%if (this.Master.IsMobileMode){%><br /><%}else{%> &nbsp; <%} %>  Polling frequency: <% =this.PollingFrequency%> sec</span> <span class="moblinks">
                            <asp:LinkButton ID="LinkButton1" class="link" Style="margin-left: 20px; vertical-align: top;"
                                runat="server" OnClientClick="StopTask(); return false;" Text="Stop" />
                        </span>
                </div>
                <div id="stopSpinnerDiv" visible="False" style="float: left; margin-left: 20px;
                    height: 16px" class="stopSpinnerDiv">
                    <span>Monitoring has been stopped</span>
                    <span class="moblinks">
                          <asp:LinkButton ID="LinkButton2" class="link" Style="margin-left: 20px; vertical-align: top;"
                              runat="server" OnClientClick="MonitorAll(); return false;"
                              Text="Restart" />
                  </span>
                </div>
                <%-- </div>--%>
                <div class="mobile_break" style="display: none;">
                    <br />
                    <br />
                    <br />
                    <hr color="#D6D6D6">
                </div>
                <span class="MobNav">
                     <div id="switchView">
                        View: <div id="tableViewDiv" class="tableViewBtnActive" onclick="switchToTableView();">&nbsp;</div>
                        <div id="plotViewDiv" class="plotViewBtnInactive" onclick="switchToPlotView();">&nbsp;</div>
                    </div>
                    <div id="export-button-div" style="float: right" class="button-wrapper">
                        <button class="flat-button" id="exportButton" data-dropdown="#export-div" style="color: #336699;
                            font-weight: bold; font-size: 12px; font-family: Arial, Helvetica, Sans-Serif;">
                            <img class="flat-button-img" src="../../images/export-icon.png" />
                            Export
                            <img class="flat-button-imgDown" src="../../images/arrow-down.png" />
                        </button>
						<div id="export-div" class="dropdown dropdown-tip">
							<ul class="dropdown-menu">
								<li><a id="ExportToCSVId" href="#1">CSV</a></li>
								<li><a id="ExportToTxtId" href="#2">Text</a></li>
								<li><a id="ExportToPdf" href="#3">PDF</a></li>
							</ul>
						</div>
                    </div>
                    <span class="moblink">
                        <div style="margin-top: 10px; float: right">
                            <img style="margin-left: 20px" src="../../images/select-icon.png" />
                            <a class="link" style="margin-left: 5px; vertical-align: top; cursor: pointer" onclick="backToConfigWizard()">
                                Configure Monitor</a>
                        </div>
                    </span>
                </span>
                </div>
                
                 <%if (this.Master.IsMobileMode){%>
                    <uc:ResultsChart ID="resultsChart" ClientIDMode="Static" ChartTitle="CPU chart" runat="server" IsMobileView="True"></uc:ResultsChart>
                <%}else{%>
                    <uc:ResultsChart ID="resultsChart1" ClientIDMode="Static" ChartTitle="CPU chart" runat="server" IsMobileView="False"></uc:ResultsChart>
                <%}%>
                
                <div id="signalRTableDiv" style="width: 100%; clear: both;overflow: auto;" />
            </div>
        </div>
        <%--<input type="hidden" id="taskIdTextbox" />--%>
    </div>
    <div style="clear:both;"></div>
</asp:Content>
