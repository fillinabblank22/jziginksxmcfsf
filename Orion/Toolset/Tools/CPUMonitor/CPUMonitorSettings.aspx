﻿<%@ Page Title="CPU Monitor Settings" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master"
    AutoEventWireup="true" CodeFile="CPUMonitorSettings.aspx.cs" Inherits="Orion_Toolset_Tools_CPUMonitor_CPUMonitorSettings" %>

<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ToolDetails" runat="Server">
    <orion:Include ID="Include2" runat="server" ClientIDMode="Static" File="/Toolset/js/MonitorsSettings.js" />
    <link href="../../Styles/MonitorsSettingsStyles.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/TabsStyle.css" rel="stylesheet" type="text/css" />
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var selectedSettingsLink = '/Orion/Toolset/Services/CPUMonitorSettings.asmx/GetSelectedColumns';
        var allSettingsLink = '/Orion/Toolset/Services/CPUMonitorSettings.asmx/GetTreeGridCollumn';
        var saveLink = '/Orion/Toolset/Services/CPUMonitorSettings.asmx/SaveAllSettings';

        $(document).ready(function () {
            $("[id$='msetlink']").attr('href', 'CPUMonitorSettings.aspx');
            $(".mlink").attr('href', 'CPUMonitor.aspx');
            $(function () {
                $("#tabs").tabs();
                $(".indent.setting").click(function () {
                    window.location = "CPUMonitorSettings.aspx";
                });
                $(".link").click(function () {
                    window.location = "CPUMonitor.aspx";
                });
                $("#saveandcontinueButton").click(function () {
                    if (<%= this.Master.IsDemo %>)
                    return;
                    saveSettings("dummy");
                });
                $("#saveSettingsButton").click(function () {
                    if (<%= this.Master.IsDemo %>)
                    return;
                    saveSettings("<%= ToolURL%>");
                });
                $("#saveSettingsMobile").click(function () {
                    if (<%= this.Master.IsDemo %>)
                    return;
                    saveSettings("<%= ToolURL %>");
                });
                $("#saveAndContinueMobile").click(function () {
                    if (<%= this.Master.IsDemo %>)
                    return;
                    saveSettings("dummy");
                });
                $("#cancelMobile").click(function () {
                    window.location.replace("<%=ToolURL%>");
                });
                
                if (<%= this.Master.IsDemo %>) {
                    demoMode();
                }
            });
        });
    </script>
    <script src="../../js/MonitorsSettingsRendering.js" type="text/javascript"></script>
    <div id="content-div" style="padding-bottom: 30px;">
    <div>
        <div id="successMsg" class="success-msg msg" runat="server" clientidmode="Static"
            viewstatemode="Enabled">
            <img src="../../images/ok_16x16.gif" /><span>Settings were successfully saved.</span></div>
        <div id="failedMsg" class="failed-msg msg" runat="server" clientidmode="Static" viewstatemode="Enabled">
            <img src="../../images/failed_16x16.gif" /><span id="errorMsg">An Error occured during
                saving. Please double check your network connection or try it later.</span></div>
        <div id="tabs">
            <ul>                    
                <li><a href="#tabs-1">Preferences</a></li>        
                <li><a href="#tabs-2">Metrics</a></li>
                <li><a href="#tabs-3">Thresholds</a></li>
            </ul>
            <div id="tabs-1">
                <asp:Panel ID="Panel2" runat="server" GroupingText="Cpu Statistics Polling Interval" class="Agenda">
                    <br />
                    <div id="PollingInterval">
                        <asp:Label ID="Label7" CssClass="LimitsValues MargLeftLess" runat="server" Text="Polling Interval:"></asp:Label>
                        <asp:TextBox ID="txtPollingInterval" CssClass="LimitsValues" ClientIDMode="Static" runat="server" onkeypress="return isNumber(event)" onchange="validateMaxMinValues(this, 5, 120)"></asp:TextBox>
                        <asp:Label ID="Label8" CssClass="ChartsLimits"  runat="server" Text="seconds. Collect Cpu stats every X seconds"></asp:Label>
                    </div>
                    <br />
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel1" runat="server" GroupingText="Chart Data Retention" class="Agenda">   
                <p class="Descript">When the data retention limit is reached, the oldest data point is discarded as a new data point arrives. Choose a setting to define the data retention limit.</p>  <br />       
                    <div id="TimeLimit">
                        <label for="rdLimitByTime">
                            <input id="rdLimitByTime" name="ChartsLimits"  ClientIDMode="Static" class="Point" type="radio" runat="server" onchange="disableElements(['txtLimitByPoints'],['txtLimitByTime','drpValueType'])"/>Limit the chart data by time
                        </label><br />
                        <asp:Label ID="Label4" runat="server" CssClass="LimitsValues MargLeft" Text="Limit by Time:"></asp:Label>
                        <asp:TextBox ID="txtLimitByTime" runat="server" CssClass="LimitsValues" ClientIDMode="Static" onkeypress="return isNumber(event)" onchange="validateTimeValue(this,5,120,1,96,'drpValueType')"></asp:TextBox>
                        <asp:DropDownList ID="drpValueType" CssClass="LimitsValues"  ClientIDMode="Static" runat="server" onchange="$('#txtLimitByTime').change();">
                        </asp:DropDownList>
                    </div>
                    <br />
                    <div id="PointsLimit">
                        <label for="rdLimitByPoints">
                            <input id="rdLimitByPoints" name="ChartsLimits"  ClientIDMode="Static" class="Point" type="radio" runat="server" onchange="disableElements(['txtLimitByTime','drpValueType'],['txtLimitByPoints'])"/>Limit the chart to a specific number of data points
                        </label><br />
                        <asp:Label ID="Label5" CssClass="LimitsValues MargLeftLess" runat="server" Text="Limit of Data Points:"></asp:Label>
                        <asp:TextBox ID="txtLimitByPoints" CssClass="LimitsValues" ClientIDMode="Static" runat="server" onkeypress="return isNumber(event)" onchange="validateMaxMinValues(this,100,10000)"></asp:TextBox>
                        <asp:Label ID="Label6" CssClass="ChartsLimits"  runat="server" Text="Max value 10000"></asp:Label>
                        <br />
                    </div>
                </asp:Panel>
            </div>
            <div id="tabs-2">
                <h4 class="Descript">
                    Select the metrics you want to see in the chart or table.</h4>
                <div id="Grid" >
                </div>
               
                <div id="Buttons">
                    <a href="#" class="next nav-btn" onclick="addSelectedColumns()"></a><a href="#" class="prev nav-btn"
                        onclick="removeSelectedColumns()"></a>
                </div>
                <div id="GridSelected">
                </div>
            </div>
            <div id="tabs-3">
                <div class="Descript">
                    Modify the thresholds for specific metrics to display Warning or Critical conditions.</div>
                      <hr/>
                <asp:ListView runat="server" ID="Thresholds" EnableViewState="True" OnItemCreated="ItemCreated">
                    <ItemTemplate>
                        <div class="ThresholdBlock">
                        <div class="RowHeaders">
                             <div class="ThresholdTitle">
                                <asp:Label ID="Label1" CssClass="displayName" runat="server" Text='<%#Eval("DisplayName")%>'></asp:Label>
                            </div>
                            <div class="trigger">
                                <asp:CheckBox ID="chbUseThresholds" runat="server" Text=" Use threshold" Checked='<%#Bind("UseThreshold")%>' />
                            </div>
                        </div>
                        <div class="panel">
                            <asp:Label Visible="True" runat="server" ClientIDMode="Static" ID="lblName" CssClass="HiddenName" Text='<%#Eval("Name")%>'></asp:Label>
                            <asp:HiddenField ID="ThresholdName" ClientIDMode="Static" Value='<%#Eval("Name")%>' runat="server" />
                            <div class="ThresholdsDetails">
                                <table>
                                    <tr>
                                        <td>
                                            <img src="../../images/Warning_icon16x16.png" /><span class="LevelText"> Warning level:
                                            </span>
                                        </td>
                                        <td>
                                            <asp:DropDownList EnableViewState="True" ID="drpWarningLevel" runat="server" onchange='<%#"validateValue(this,\"" + Eval("Units" ) + "\" );" %>' SelectedValue='<%#(Eval("WarningLevel"))%>'
                                                Width="100%" AppendDataBoundItems="True" CssClass="WarningLevel" />
                                        </td>
                                        <td class="Units">
                                            <div style="overflow: auto">
                                                <asp:TextBox runat="server" onkeypress="return isNumber(event)" onblur='<%#"validateValue(this,\"" + Eval("Units" ) + "\" );" %>' ID="txtWarningValue"
                                                    Text='<%#Eval("WarningValue")%>' CssClass="WarningValue NumericField" />
                                                <asp:Label ID="Label2" runat="server" Text=' <%#Eval("Units")%>' style="font-size: 12px;"></asp:Label>
                                            </div>
                                            <asp:HiddenField ID="Hidden" runat="server" EnableViewState="True" Value='<%#Eval("WarningValue")%>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../../images/Critical_icon16x16.png" /><span class="WarningLevel"> Critical
                                                level: </span>
                                        </td>
                                        <td>
                                            <asp:TextBox ReadOnly="True" EnableViewState="True" runat="server" ID="txtCriticalLevel"
                                                BorderWidth="0px" CausesValidation="False" Text='<%#Eval("CriticalLevel")%>'
                                                Width="100%" CssClass="CriticalLevel" />
                                        </td>
                                        <td class="Units">
                                            <div>
                                                <asp:TextBox runat="server" onkeypress="return isNumber(event)" onblur='<%#"validateValue(this,\"" + Eval("Units" ) + "\" );" %>' EnableViewState="True"
                                                    ID="txtCriticalValue" Text='<%#Eval("CriticalValue")%>' CssClass="CriticalValue NumericField" />
                                                <asp:Label ID="Label3" runat="server" Text=' <%#Eval("Units")%>' style="font-size: 12px;"></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        </div>
                        <hr />
                    </ItemTemplate>
                </asp:ListView>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="Result" Value="false" />
            </div>            
        </div>
    </div>
    <div id="Tab2" class="BottomBurrons">
        <orion:LocalizableButton ID="saveSettingsButton" ClientIDMode="Static" OnClientClick="return false;" LocalizedText="Submit" DisplayType="Primary"  runat="server" />
        <orion:LocalizableButton ID="saveandcontinueButton" ClientIDMode="Static" LocalizedText="CustomText" Text="Save and Continue Working" DisplayType="Secondary"
            OnClientClick="return false;" runat="server" />
        <orion:LocalizableButton ID="cancelButtonThresholds" ClientIDMode="Static" LocalizedText="Cancel"
            DisplayType="Secondary" runat="server" OnClick="cancelButton_OnClick" /><br />
    </div>
    <div id="Tab1Mobile" class="globalButtonsMobile">
        <asp:Button type="button" Text="Submit" id="saveSettingsMobile" runat="server" ClientIDMode="Static" OnClientClick="return false;"/>
        <asp:Button type="button" Text="Save and Continue Working" id="saveAndContinueMobile" ClientIDMode="Static" runat="server" OnClientClick="return false;"/>
        <asp:Button type="button" Text="Cancel" id="cancelMobile" runat="server" ClientIDMode="Static" OnClientClick="return false;"/>
    </div>
    </div>
</asp:Content>
