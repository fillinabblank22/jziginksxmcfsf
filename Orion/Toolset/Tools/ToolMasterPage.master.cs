﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Common.Tools;
using SolarWinds.Toolset.Common.UserSessions;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Web;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Web.Services;

public partial class Orion_Toolset_Tools_ToolMasterPage : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        OrionInclude.ModuleFile("Toolset", "Tools.css");
        //TODO: Remove this once we got working Orion for Android 4+
        if (MobileHelper.ApplyMobileView())
        {
            string url = Request.Url.GetLeftPart(UriPartial.Path);
            if (!Request.QueryString.ToString().ToLower().Contains("ismobileview"))
            {
                url += (Request.QueryString.ToString() == "") ? "?IsMobileView=true" : "?" + Request.QueryString.ToString() + "&IsMobileView=true";
                Response.Redirect(url);
            }
        }
        base.OnInit(e);
    }

    public string AccountId
    {
        get
        {
            return OrionAccountHelper.GetCurrentLoggedUserName().ToLower();
        }
    }

    public string SessionId
    {
        get { return GetSessionId(); }
    }


    private bool? _ismobile;

    public bool IsMobileMode
    {
        get
        {
            return _ismobile.HasValue ? _ismobile.Value :
                (_ismobile = CommonWebHelper.IsMobileView(Request)).Value;
        }
    }

    public bool IsLimitReached { get; set; }

    public string ImgLogoPath
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                deskLogo.Src = value;
                mobLogo.Src = value;
            }
        }
    }

    public AccessLimitationReturnCode AccessLimitationResult { get; set; }

    private int _toolId;
    public int ToolId
    {
        set
        {
            _toolId = value;
        }
        get
        {
            return _toolId;
        }
    }

    public int ToolLimitReachedReloadInterval { get; set; }

    public string Title { get; set; }
    public string ImageSrc { get; set; }
    public string Description { get; set; }
    public string ToolDetailedDescription { get; set; }
    public string GuideHref { get; set; }
    public string MainPageHref { get; set; }

    protected void InitializeLearnMore(ITool tool)
    {
        if (tool == null)
            return;
        Title = tool.Name;
        ImageSrc = string.Format("../../images/AppsScreenshots/{0}", tool.IconFileName);
        Description = tool.Description;
        GuideHref = HelpHelper.GetHelpUrl(tool.HelpLinkFragment);
        MainPageHref = string.Format("{0}?IsMobileView=True", tool.DetailPageUrl.Remove(0, tool.DetailPageUrl.IndexOf("/", System.StringComparison.Ordinal) + 1));

        var points = tool.DetailedDescription.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        foreach (var point in points)
            ToolDetailedDescription += string.Format("<li>{0}</li>", point);

        if (IsMobileMode)
            LearnMoreCssLink.Attributes["href"] = "../../Styles/LearnMorePopUp.responsive.css";
        else
            LearnMoreCssLink.Attributes["href"] = "../../Styles/LearnMorePopUp.css";
    }

    private ITool Tool { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        IsLimitReached = false;
        var limitationService = new AccessLimitationService();
        bool isDetailFlag = !Request.Url.ToString().Contains("Settings.aspx");
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(_toolId);
        Tool = toolFactory.CreateTool();

        AccessLimitationResult = limitationService.CheckAccessLimitation(AccountId, Tool.ToolKey, SessionId, IsMobileMode, isDetailFlag);
        //TODO:Move SSH validation logic to AccessLimitationService
        switch (AccessLimitationResult)
        {
            case AccessLimitationReturnCode.LicenseExpired:
                ErrorMsgTitle.Text = Resources.ToolsetWebContent.WebData_MD0_51;
                ErrorMsgBody.Text = Resources.ToolsetWebContent.WebData_MD0_52;
                setlink.Target = "#";
                break;
            case AccessLimitationReturnCode.WebToolsetRightDisabled:
                ErrorMsgTitle.Text = Resources.ToolsetWebContent.WebData_MD0_45;
                ErrorMsgBody.Text = Resources.ToolsetWebContent.WebData_MD0_46;
                setlink.Target = "#";
                break;
            case AccessLimitationReturnCode.ToolIsNotEnabled:
                if (Tool.IsInternal && Profile.AllowAdmin)
                {
                    AccessLimitationResult = AccessLimitationReturnCode.Allowed;
                }
                else
                {
                    ErrorMsgTitle.Text = Resources.ToolsetWebContent.WebData_MD0_47;
                    ErrorMsgBody.Text = Resources.ToolsetWebContent.WebData_MD0_48;
                    setlink.Target = "#";
                }
                break;
            case AccessLimitationReturnCode.AnotherSessionForTheSameUserExists:
                ErrorMsgTitle.Text = string.Format(Resources.ToolsetWebContent.WebData_MD0_43, AccountId);
                ErrorMsgBody.Text = Resources.ToolsetWebContent.WebData_MD0_44;
                setlink.Target = "#";
                break;
            case AccessLimitationReturnCode.ToolDetailLimitReached:
                    // This is for internal 1sec refresh.
                    OrionInclude.InlineJs("SW.Core.namespace('SW.Toolset').ToolLimitReached=1;");
                    ToolLimitReachedReloadInterval = 1000; // TODO: Change this hard-coded value!
                    ErrorMsgTitle.Text = Resources.ToolsetWebContent.WEBDATA_SL_15;
                    var toolSettingsDal = new ToolsetSettingsDAL();
                    string toolCount = IsMobileMode
                                           ? toolSettingsDal.GetGlobalSettingsValue<string>(
                                               GlobalSettingsKeys.MaximumRunningToolsPerAccountMobileKey,
                                               DefaultGlobalSettings.DefGlobalSettings[
                                                   GlobalSettingsKeys.MaximumRunningToolsPerAccountMobileKey])
                                           : toolSettingsDal.GetGlobalSettingsValue<string>(
                                               GlobalSettingsKeys.MaximumRunningToolsPerAccountDesktopKey,
                                               DefaultGlobalSettings.DefGlobalSettings[
                                                   GlobalSettingsKeys.MaximumRunningToolsPerAccountDesktopKey]);
                    ErrorMsgBody.Text = string.Format(Resources.ToolsetWebContent.WEBDATA_SL_16, toolCount, IsMobileMode ? Resources.ToolsetWebContent.WEBDATA_SL_17 : Resources.ToolsetWebContent.WEBDATA_SL_18);
                    setlink.Target = "#";
                    IsLimitReached = true;
               
                break;
            case AccessLimitationReturnCode.AllowedNumberOfUsersReached: //new session initiation even though there are no seats left - before the bub call and formal registration of the session from JS code
                
                var userSessions = UserSessionHelper.GetSessions();

                bool isSessionExist = userSessions.Any(userSession => userSession.SessionId == SessionId);

                if (!isSessionExist)
                {
                    ErrorMsgTitle.Text = Resources.ToolsetWebContent.WebData_MD0_49;
                    ErrorMsgBody.Text = Resources.ToolsetWebContent.WebData_MD0_50;
                    setlink.Target = "#";
                }
                else
                {
                    AccessLimitationResult = AccessLimitationReturnCode.Allowed;
                }
                break;
        }
        if (!IsPostBack && !IsLimitReached)
        {
            
            Page.ClientScript.RegisterStartupScript(GetType(), "initSessionManagement", "InitSessionManagement();", true);
            
        }

        InitializeLearnMore(Tool);
    }

    protected string GetSessionId()
    {
        // Get or create cookie
        Cookies cookiemanager = new Cookies();
        string strToolsetIdCookie = cookiemanager.ToolsetIdCookieName;
        // Get or create cookie
        var cookie = Request.Cookies[strToolsetIdCookie];
        if (cookie == null)
        {
            cookie = new HttpCookie(strToolsetIdCookie)
            {
                Expires = DateTime.Now.AddYears(1),
                Value = Guid.NewGuid().ToString()
            };
            Response.Cookies.Add(cookie);
        }

        // Todo: Move this to appropirate place, since the method does extra work other than fetching session id and logging
        if (IsGuid(cookie.Value))
        {
            return cookie.Value;
        }
        else
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.ToolsetWebContent.WEBDATA_SL_20), Title = Resources.ToolsetWebContent.WEBDATA_SL_19 });
            return "";
        }
    }

    // Method to check whether the string is valid guid
    private bool IsGuid(string guid)
    {
        Guid temp;
        if (Guid.TryParse(guid, out temp))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
