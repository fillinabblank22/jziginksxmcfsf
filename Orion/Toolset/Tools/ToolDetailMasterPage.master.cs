﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Toolset.Web.Controls;

public partial class Orion_Toolset_Tools_ToolDetailMasterPage : MasterPage
{
    private bool? _ismobile;

    public bool IsOrionObjectsCrossLinks { get; private set; }

    public bool IsMobileMode
    {
        get
        {
            return _ismobile.HasValue ? _ismobile.Value :
                (_ismobile = CommonWebHelper.IsMobileView(Request)).Value;
        }
    }
    public string IsDemo
    {
        get
        {
            return OrionConfiguration.IsDemoServer.ToString().ToLower();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var master = this.Master as Orion_Toolset_Tools_ToolMasterPage;
        if (master == null)
            throw new InvalidOperationException("There is no master!");

        GetObjectCrossLinksSettingValue();
        DataBind();
    }

    private void GetObjectCrossLinksSettingValue()
    {
        Dictionary<string, string> settigns = (new SolarWinds.Toolset.Web.DAL.ToolsetSettingsDAL()).GetGlobalSettings();

        IsOrionObjectsCrossLinks =
            Convert.ToBoolean(settigns.ContainsKey(SolarWinds.Toolset.Common.Settings.GlobalSettingsKeys.OrionObjectsCrossLinks)
                    ? settigns[SolarWinds.Toolset.Common.Settings.GlobalSettingsKeys.OrionObjectsCrossLinks]
                    : SolarWinds.Toolset.Common.Settings.DefaultGlobalSettings.DefGlobalSettings[
                        SolarWinds.Toolset.Common.Settings.GlobalSettingsKeys.OrionObjectsCrossLinks]);
    }
}
