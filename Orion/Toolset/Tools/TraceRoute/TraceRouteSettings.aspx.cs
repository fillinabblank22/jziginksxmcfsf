﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Common;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.TraceRoute;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Tools_SwitchPortMapper_TraceRouteSettings : System.Web.UI.Page
{
    protected int ToolId
    {
        get
        {
            return SupportedTools.TraceRouteToolId;
        }
    }

    public string ToolURL
    {
        get
        {
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (TraceRouteTool)toolFactory.CreateTool();
            return UrlHelper.GetToolDetailAddress(tool.DetailPageUrl);
        }
    }
    public string IsDemo
    {
        get
        {
            return OrionConfiguration.IsDemoServer.ToString().ToLower();
        }
    }

    private string accountId;

    protected void Page_Init(object sender, EventArgs e)
    {
        this.Master.Master.ImgLogoPath = "../images/AppIcons/TraceRoutelogo.png";
        this.Master.Master.ToolId = ToolId;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        saveSettingsProgress.Visible = false;
        this.accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        if (!Page.IsPostBack)
        {
            // Load permanent settings
            var settings = (TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this.accountId);
            LoadSettingsToForm(settings);
        }
    }

    private void LoadSettingsToForm(TraceRoutePermanentSettings settings)
    {
        if (settings == null)
            throw new ArgumentNullException("settings");
        cbxEnableDNS.Checked = settings.IsDNSResolvingEnabled;
        txbPollingInterval.Value = (settings.PollingInterval / 1000).ToString();
        cbxUseToolsetSettings.Checked = settings.UseToolsetSettings;
        txbICMPTimeout.Value = settings.ICMPTimeout.ToString();
        txbMaxHops.Value = settings.MaxHops.ToString();
        txbSNMPRetries.Value = settings.SNMPRetries.ToString();
        txbSNMPTimeout.Value = settings.SNMPTimeout.ToString();
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        GoBack();
    }

    public void GoBack()
    {
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
        var tool = (TraceRouteTool)toolFactory.CreateTool();
        Response.Redirect(UrlHelper.GetToolDetailAddress(tool.DetailPageUrl));
    }
}