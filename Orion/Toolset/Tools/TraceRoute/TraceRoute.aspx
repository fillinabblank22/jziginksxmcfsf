﻿<%@ Page Title="Trace Route" AutoEventWireup="true" CodeFile="TraceRoute.aspx.cs"
    Inherits="Orion_Toolset_Tools_TraceRoute" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>

<%@ Register TagPrefix="uc" TagName="AutoComplete" Src="~/Orion/Toolset/Controls/AutoCompleteTextbox.ascx" %>
<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Services/TraceRouteService.asmx" %>
<%@ Register Src="~/Orion/Toolset/Controls/SelectNode.ascx" TagPrefix="orion" TagName="SelectNode" %>
<asp:Content ContentPlaceHolderID="ToolDetails" runat="Server">
    <orion:Include ID="IncludeExtJs" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <link rel="stylesheet" type="text/css" href="../../Styles/TraceRouteStyles.css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/MonitorGrid.css" />
    <link rel="stylesheet" type="text/css" href="../../../js/extjs/4.2.2/resources/ext-theme-gray-sandbox-42-all.css" />
    <script type="text/javascript" src="../../js/TraceRoute.js"></script>
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="../../../js/OrionMasterPage.js/WebToolsetMenu.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="../../Styles/DropDownButton.css" />
    <script type="text/javascript" src="../../js/DropDownButtonTools.js"></script>
    <%if (!this.Master.IsMobileMode)
       {%>
    <link rel="stylesheet" type="text/css" href="../../Styles/ExportButtonStyles.css" />
    <% } %>

    <script type="text/javascript">
        
        function isDemoMode() {
            return <%= this.Master.IsDemo %>;
        }

        function getIPsHistory() {
            return [<%=this.GetIPsHistory() %>];
        }

        function isMobileView() {
            return <%= this.Master.IsMobileMode.ToString().ToLowerInvariant() %>;
        }

        function GetCacheInvalidationInterval() {
            return <%= this.CacheInvalidationInterval %>;
        }

        $(document).ready(function() {
            //disable the run button by default
            $("#RunButton").attr("disabled","disabled");
            $('#RunButtonMobile').attr('disabled', 'disabled');
            
            function fixedFromCharCode(codePt) {
                if (codePt > 0xFFFF) {
                    codePt -= 0x10000;
                    return String.fromCharCode(0xD800 + (codePt >> 10), 0xDC00 + (codePt & 0x3FF));
                } else {
                    return String.fromCharCode(codePt);
                }
            }

            function getCaretPosition(control) {
                var position = {};
                if (control.selectionStart && control.selectionEnd) {
                    position.start = control.selectionStart;
                    position.end = control.selectionEnd;
                } 
                position.length = position.end - position.start;
                return position;
            }
            
            var values = [<%=this.GetIPsHistory() %>];
            if (<%= this.Master.IsDemo %>) {
                document.getElementById('cbDNSResolve').disabled = true;
                document.getElementById('selectImg').disabled = true;
                if ($('#selectDevice').hasClass('selectDevice')) {
                    $('#selectDevice').removeClass('selectDevice').addClass('selectDeviceDisabled');
                }
                $('#selectImg').addClass('selectImgDisabled');
                $("[id$='AutoCompleteTextbox']").keypress(function(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    var currentValue = fixedFromCharCode(charCode);
                    var val = $("[id$='AutoCompleteTextbox']").attr('value');
                    var selection = getCaretPosition(this);
                    var expValue = val + currentValue;
                    if (evt.keyCode == 46)
                        return false;
                    if ((evt.keyCode == 8) && selection.start == val.length) {
                        return true;
                    }

                    if (!expValue)
                        return false;
                    for (var i = 0; i < values.length; i++) {
                        if (values[i].substr(0, expValue.length).toLowerCase() == expValue.toLowerCase()) {
                            return true;
                        }
                    }
                    return false;
                });
            } else {
                $("[id$='AutoCompleteTextbox']").keypress(function(evt) {
                    evt = (evt) ? evt : window.event;
                    if (evt.keyCode == 13) {
                        $("#RunButton").click();
                        return false;
                    }
                    return true;
                });                
            }
        });
    </script>
   
    <div id="dialog-message" title="Empty Address" style="display: none;">
        <p>
            Please, fill Hostname or IP Address to Trace
        </p>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true" />
    <div id="select-dialog" title="Select a Network Object">
        <p>
            <orion:SelectNode ID="Grid" runat="server" IsParentDependency="true" />
            <div class="sw-btn-bar-wizard">
                <orion:LocalizableButton Text="Select Object" ClientIDMode="Static" DisplayType="Primary"
                    OnClientClick="return false; " runat="server" ID="imgbSelect" />
                &nbsp;
                <orion:LocalizableButton LocalizedText="Cancel" DisplayType="Secondary" runat="server"
                    ID="imgbCancel" OnClientClick="$('#select-dialog').dialog('close'); return false;" />
            </div>
        </p>
    </div>
    <div id="dialog-message-invalid" title="Empty Address" style="display: none;">
        <p>
            Please, use correct IP or Hostname
        </p>
    </div>
    <div id="traceRoute">
        <div class="trace-route-content-wrapper">
            <div class="enter-hostname-wrapper">
                <label>
                    Enter Hostname or IP Address:</label>
                <div class="enter-hostname-or-ip-div">
                    <div id="input-button">
                        <input type="text" clientidmode="Static" id="txbInputAddressMobile" />
                        <uc:AutoComplete Width="300" ClientIDMode="Static" ID="txbInputAddress" runat="server"
                            ControlId="TraceRouteHost" ControlType="Host" CssClass="AutoComplete" EnableViewState="false" />
                        <orion:LocalizableButton ID="RunButton" ClientIDMode="Static" OnClientClick="return false;"
                LocalizedText="CustomText" DisplayType="Secondary" runat="server" Text="Trace" style="height:15px"/>
                        <input type="button" id="RunButtonMobile" value="Trace" />
                    </div>
                    <br>
                    <div id="checkbox-selector">
                        <div id="select-device-div">
                            <img id="selectImg" src="../../images/select-icon.png" />
                            <label for="selectImg" id="selectDevice" class="selectDevice">
                                Select from Orion</label>
                        </div>
                        <div id="checkbox-div">
                            <input runat="server" clientidmode="Static" type="checkbox" id="cbDNSResolve" value="value" />
                            <label for="cbDNSResolve" style="font-size: 12px;">
                                Resolve DNS</label>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <br>
                    <div id="PollingEngines">
                        <table>
                            <tr class="x42-form-item-input-row">
                                <td width="105" class="x42-field-label-cell">
                                    <label style="width: 100px; margin-right: 5px;">
                                        Polling Engines<span role="separator">:</span></label>
                                </td>
                                <td style="width: 150px;">
                                    <select id="pollingEnginesSelect" style="width: 10em;">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </br>
            </div>

              <div class="content-preview-wrapper">
                <div id="results-wrapper">
                    <div id="statusExportDiv">
                        <div class="div-spinner" id="divSpinner">
                            <div id="loadDiv" class="title-div">
                                <div class="status-line">
                                    <span><b>TraceRoute</b> to </span><b>
                                        <label id="labelAddrr">
                                        </label>
                                    </b>
                                  <div id="loading-cancel-div">
                                    <img id="imgUpdateProgress" src="../../images/animated_loading_16x16.gif" />
                                    
                                    <span id="discoverMsg"> Discovery of route is in  progress...</span>
                                    <span id="tracingMsg" style="display:none;"> Tracing in progress... Polling frequency: <% =this.PollingFrequency%> sec</span>
                                    
                                    <span class="moblinks"><button id="cancelButton" class="CancelLink" title="Stop">
                                        Stop</button></span>
                                </div>
                                </div>
                                <div id="export-button-div" style="float: right" class="button-wrapper">
                                    <button class="flat-button" id="exportButton" data-dropdown="#export-div" style="color: #336699;
                                        font-weight: bold; font-size: 12px; font-family: Arial, Helvetica, Sans-Serif;">
                                        <img class="flat-button-img" src="../../images/export-icon.png" />
                                        Export
                                        <img class="flat-button-imgDown" src="../../images/arrow-down.png" />
                                    </button>
									<div id="export-div" class="dropdown dropdown-tip">
										<ul class="dropdown-menu">
											<li><a id="ExportToCSVId" href="#1">CSV</a></li>
											<li><a id="ExportToTxtId" href="#2">Text</a></li>
											<li><a id="ExportToPdf" href="#3">PDF</a></li>
										</ul>
									</div>
                                 </div>
                                <br class="statusBreak" />
                                <span id="restart-div">
                                    <span id="discStopMsg" style="font-size: small"> Discovery has been stopped</span>
                                    <span id="traceStopMsg" style="font-size: small; display:none;"> Tracing has been stopped</span>
                                    <span class="moblinks"><button id="restartButton" class="CancelLink" title="Restart">
                                        Restart</button></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="hintDiv" class="default-message">
                        <img src="../../images/icon_bulb.png" />
                        <p>
                            Use the text field to search in your recent traces.<a href="#" id="hideTooltip" class="hide-info">I've
                                got it, hide this message</a>
                    </div>
                    <div id="jsonData" style="clear: both; visibility: hidden; overflow: auto; width: 100%;">
                    </div>
                    <div id="jsonDataMobile">
                    </div>
                </div>
                <div id="completeStatus">
                </div>                
                <asp:HiddenField runat="server" ID="txtResTblWidths" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="ipAddress" />
                <input type="hidden" id="traceRouteHiddenField" />
            </div>
        </div>
        </div>
    </div>
</asp:Content>
