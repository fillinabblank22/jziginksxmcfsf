﻿<%@ Page Title="Trace Route Settings" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master"
    AutoEventWireup="true" CodeFile="TraceRouteSettings.aspx.cs" Inherits="Orion_Toolset_Tools_SwitchPortMapper_TraceRouteSettings" %>

<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ToolDetails" runat="Server">
    <script src="../../js/TraceRoute.js" type="text/javascript"></script>
    <script src="../../js/NumberValidation.js" type="text/javascript"></script>
    <link href="../../Styles/TraceRouteSettingsStyles.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/TabsStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/SettingsLists.js"></script>
    <%if (this.Master.IsMobileMode)
    {%>
        <link rel="stylesheet" type="text/css" href="../../Styles/TraceRouteSettingsStyles.responsive.css" />
    <% } %>
    <script type="text/javascript">
        function isDemoMode() {
            return <%= this.Master.IsDemo %>;
        }
        
        $(document).ready(function () {
            $("[id$='msetlink']").attr('href', 'TraceRouteSettings.aspx');
            $(".mlink").attr('href', 'TraceRoute.aspx');
            $(function () {
                $("#tabs").tabs();
            });
            function checkState() {
                if ($('#cbxUseToolsetSettings').is(':checked')) {
                    $(".txbSNMPRetries").attr('readonly', 'true');
                    $(".txbSNMPTimeout").attr('readonly', 'true');
                    $(".txbSNMPTimeout").css("background-color", "#F2F2F2");
                    $(".txbSNMPRetries").css("background-color", "#F2F2F2");

                } else {
                    $(".txbSNMPRetries").removeAttr('readonly');
                    $(".txbSNMPTimeout").removeAttr('readonly');
                    $(".txbSNMPTimeout").css("background-color", "#FFFFFF");
                    $(".txbSNMPRetries").css("background-color", "#FFFFFF");
                }
            };
            $(".cbToolsetSettings").click(function () {
                checkState();
            });
            checkState();
            $("#saveandcontinueButton").click(function () {
                if (<%= IsDemo %>) {
                    return;
                }
                saveSettings("dummy");
            });
            $("#saveSettingsButton").click(function () {
                if (<%= IsDemo %>) {
                    return;
                }
                saveSettings("<%= ToolURL %>");
            });
            $("#saveandcontinueButtonMobile").click(function () {
                if (<%= IsDemo %>) {
                    return;
                }
                saveSettings("dummy");
            });
            $("#saveSettingsButtonMobile").click(function () {
                if (<%= IsDemo %>) {
                    return;
                }
                saveSettings("<%= ToolURL %>");
            });
            $("#cancelButtonMobile").click(function () {
                window.location.replace("<%= ToolURL %>");
            });
            if (<%= IsDemo %>) {
                $("#content-div :input").attr("disabled", true);
                document.getElementById("saveSettingsButton").disabled = true;
                document.getElementById("saveandcontinueButton").disabled = true;
                $("#saveSettingsButton").addClass('sw-btn-disabled');                
                $("#saveandcontinueButton").addClass('sw-btn-disabled');
            }
        });
        Ext.onReady(function () {
            ToolsetSettingsManager.init(ToolsetSettingsManager);
            if (<%= IsDemo %>) {
                Ext.getCmp('gridAll').setDisabled(true);
                Ext.getCmp('gridSelected').setDisabled(true);
                Ext.getCmp('gridMobile').setDisabled(true);
            }
        });
    </script>
    <div id="content-div" style="padding-bottom: 45px;">
        <div>
            <div id="successMsg" style="display: none;" class="success-msg msg">
                <img src="../../images/ok_16x16.gif" /><span>Settings were successfully saved.</span></div>
            <div id="failedMsg" style="display: none;" class="failed-msg msg">
                <img src="../../images/failed_16x16.gif" /><span id="errorMsg">An Error occured during
                    saving. Please double check your network connection or try it later.</span></div>
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Preferences</a></li>
                    <li><a href="#tabs-2">Metrics</a></li>
                </ul>
                <div id="tabs-1">
                    <asp:Panel runat="server" ID="PanelICMP" CssClass="settingsPanelLevel2" GroupingText="ICMP">
                        <div class="panelDiv">
                            <div>
                                <label class="label-1">
                                    ICMP Timeout:</label>
                                <input type="text" class="txb" onkeypress="return isNumber(event)" onblur="validateRange(this, 500, 8000)"
                                    clientidmode="Static" runat="server" id="txbICMPTimeout" />
                                <br class="settingsBreaks" />
                                <div class="bottomValue">
                                    <label class="label-2">
                                        milliseconds, max. value is 8000</label></div>
                            </div>
                            <div class="lastdiv">
                                <label class="label-1">
                                    Polling interval:</label>
                                <input type="text" class="txb" onkeypress="return isNumber(event)" onblur="validateRange(this, 4, 120)"
                                    clientidmode="Static" runat="server" id="txbPollingInterval" />
                                <%-- <div id="updown" style="display: inline-block">
                                <a href="#" class="upIcon"></a>
                                <a href="#" class="downIcon"></a>
                            </div>--%>
                                <br class="settingsBreaks" />
                                <div class="bottomValue">
                                    <label class="label-2">
                                        seconds</label></div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelTrace" CssClass="settingsPanelLevel2" GroupingText="Trace">
                        <div class="panelDiv">
                            <div>
                                <label class="label-1">
                                    Maximum Number of Hops:</label>
                                <input type="text" class="txb" onkeypress="return isNumber(event)" onblur="validateRange(this, 4, 100)"
                                    clientidmode="Static" runat="server" id="txbMaxHops" />
                                <br class="settingsBreaks" />
                                <div class="bottomValue">
                                    <label class="label-2">
                                        hops</label></div>
                                <%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txbMaxHops"
                            ErrorMessage="Maximum hops can't be empty" ForeColor="Red">
                        </asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="checkbox-div lastdiv">
                                <asp:CheckBox runat="server" ClientIDMode="Static" ID="cbxEnableDNS" />
                                <asp:Label ID="Label5" ClientIDMode="Static" runat="server" AssociatedControlID="cbxEnableDNS"
                                    CssClass="toolSettingsPropertyLabel">Resolve DNS</asp:Label>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanelSNMP" CssClass="settingsPanelLevel3" Visible="False"
                        GroupingText="SNMP">
                        <div class="panelDiv">
                            <div class="wrapper-snmp">
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                                <div class="checkbox-div">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox runat="server" ClientIDMode="Static" ID="cbxUseToolsetSettings" CssClass="cbToolsetSettings" />
                                            <asp:Label ClientIDMode="Static" ID="Label3" runat="server" AssociatedControlID="cbxUseToolsetSettings"
                                                CssClass="toolSettingsPropertyLabel">Use Toolset user settings</asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div>
                                    <label class="label-1">
                                        SNMP Timeout:</label>
                                    <input type="text" class="txb" onkeypress="return isNumber(event)" runat="server"
                                        clientidmode="Static" id="txbSNMPTimeout" />
                                    <br class="settingsBreaks" />
                                    <div class="bottomValue">
                                        <label class="label-2">
                                            milliseconds</label></div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ClientIDMode="Static"
                                        ControlToValidate="txbSNMPTimeout" ErrorMessage="SNMP Timeout can't be empty"
                                        ForeColor="Red">
                                    </asp:RequiredFieldValidator>
                                </div>
                                <div class="lastdiv">
                                    <label class="label-1">
                                        SNMP Retries:</label>
                                    <input type="text" class="txb" onkeypress="return isNumber(event)" runat="server"
                                        clientidmode="Static" id="txbSNMPRetries" />
                                    <br class="settingsBreaks" />
                                    <div class="bottomValue">
                                        <label class="label-2">
                                            attempts</label></div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txbSNMPRetries"
                                        ErrorMessage="Retries count can't be empty" ForeColor="Red">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="hindmsg lastdiv msg">
                                <img src="../../images/Icon.Lightbulb.gif" /><span>If you want to set individual SNMP
                                    settings for all tools, visit <a href="#">&#187; Toolset Personal Settings</a></span></div>
                        </div>
                    </asp:Panel>
                </div>
                <div id="tabs-2">
                    <h4>
                        Select the metrics you want to see in the results table.</h4>
                    <div id="Grid">
                    </div>
                    <div id="MobileGrid">
                    </div>
                    <div id="Buttons">
                        <a href="#" class="next nav-btn" onclick="selectColumns()"></a><a href="#" class="prev nav-btn"
                            onclick="deselectColumns()"></a>
                    </div>
                    <div id="GridSelected">
                    </div>
                </div>
            </div>
        </div>
        <div id="globalButtons">
            <orion:LocalizableButton ID="saveSettingsButton" ClientIDMode="Static" OnClientClick="return false;"
                LocalizedText="Submit" DisplayType="Primary" runat="server" />
            <orion:LocalizableButton ID="saveandcontinueButton" ClientIDMode="Static" LocalizedText="CustomText"
                Text="Save and Continue Working" DisplayType="Secondary" runat="server" OnClientClick="return false;" />
            <orion:LocalizableButton ID="cancelSettingsButton" ClientIDMode="Static" LocalizedText="Cancel"
                DisplayType="Secondary" runat="server" OnClick="cancelButton_OnClick" />
            <br />
            <asp:Image ID="saveSettingsProgress" runat="server" ImageUrl="../../images/animated_loading_16x16.gif"
                AlternateText="Saving ..." ToolTip="Saving ..." />
        </div>
        <div id="globalButtonsMobile">
            <input type="button" value="Submit" id="saveSettingsButtonMobile" />
            <br id="buttonsBreak" />
            <input type="button" value="Save and Continue Working" id="saveandcontinueButtonMobile" />
            <input type="button" value="Cancel" id="cancelButtonMobile" />
            <br />
        </div>
    </div>
</asp:Content>

