﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;

using SolarWinds.Orion.Common;
using SolarWinds.Toolset.Common.Settings;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.TraceRoute;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using SolarWinds.Toolset.Common.Demo;

public partial class Orion_Toolset_Tools_TraceRoute : Page
{
    public int PollingFrequency { get; set; }
    public int CacheInvalidationInterval { get; set; }
    
    protected int ToolId
    {
        get
        {
            return SupportedTools.TraceRouteToolId;
        }
    }

    private string _accountId;

    private TraceRoutePermanentSettings settings;

    protected void Page_Init(object sender, EventArgs e)
    {
        string newUrl = UrlHelper.SwapNetobjectWithToolParams();

        if (newUrl != null)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Redirect(newUrl);
        }

        this.Master.Master.ImgLogoPath = "../images/AppIcons/TraceRoutelogo.png";
        this.Master.Master.ToolId = ToolId;
        var settings = (TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, User.Identity.Name);
        PollingFrequency = settings.PollingInterval / 1000;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        if (Page.IsPostBack)
        {
            return;
        }
        this.settings = (TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(this.ToolId, _accountId);
        this.cbDNSResolve.Checked = this.settings.IsDNSResolvingEnabled;
        if (this.settings.ShowTooltip)
            ScriptManager.RegisterStartupScript(
                this,
                typeof(string),
                "uniqueKey",
                @"(function() {
                    var hint = document.getElementById('hintDiv');
                    if (hint != null)
                        hint.style.display='inline';
                })();",
                true);

        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/TraceRouteStyles.responsive.css";
        }

        this.CacheInvalidationInterval =
            (new ToolsetSettingsDAL()).GetGlobalSettingsValue<int>(
                GlobalSettingsKeys.CacheInvalidationIntervalKey,
                DefaultGlobalSettings.DefGlobalSettings[GlobalSettingsKeys.CacheInvalidationIntervalKey])
            *1000;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.ClientScript.RegisterStartupScript(typeof(string), "cs_autocomplete",
                                                @"  <script type=""text/javascript"">
		            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function() {
			            $('.AutoComplete').unautocomplete();
                        $('.AutoComplete').autocomplete([" + this.GetIPsHistory() +
                                                @"], {max : 1000, minChars : 0});
                        if(typeof(AutoCompleteChangedEvent) != 'undefined') {
                            $('.AutoComplete').result(AutoCompleteChangedEvent);
                        }
                    });
                </script>
            ");
    }

    protected string GetIPsHistory()
    {
        List<string> temp = null;
        temp = OrionConfiguration.IsDemoServer ? DemoPredefinedInfo.GetAllHosts().Select(it => it.DisplayName).ToList() : new AutocompleteDAL().GetAutocompleteList(this.txbInputAddress.ControlType, OrionAccountHelper.GetCurrentLoggedUserName());
       
        return txbInputAddress.FormatString(temp);
    }

    [WebMethod]
    public static void HideTooltip()
    {
        var setting = (TraceRoutePermanentSettings)new ToolsDAL().GetToolPermanentSettings(SupportedTools.TraceRouteToolId, OrionAccountHelper.GetCurrentLoggedUserName());
        setting.ShowTooltip = false;
        new ToolsDAL().SaveToolPermanentSettings(SupportedTools.TraceRouteToolId, OrionAccountHelper.GetCurrentLoggedUserName(), setting);
    }
}
