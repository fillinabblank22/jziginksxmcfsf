﻿using SolarWinds.Toolset.Tools;
using System;
using System.Web.UI;

public partial class Orion_Toolset_Tools_LatencyTest : Page
{
    protected int ToolId
    {
        get { return SupportedTools.LatencyTestToolId; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.Master.Master.ImgLogoPath = "../../images/Logo.Navbar.gif";
        this.Master.Master.ToolId = ToolId;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            return;
        }

        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/LatencyTestStyles.responsive.css";
        }
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        //use empty image in description
        this.Master.Master.ImageSrc = @"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D";
        base.OnLoadComplete(e);
    }
}
