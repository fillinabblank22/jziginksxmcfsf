﻿<%@ Page Title="Latency Test Settings" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master"
    AutoEventWireup="true" CodeFile="LatencyTestSettings.aspx.cs" Inherits="Orion_Toolset_Tools_LatencyTestSettings" %>

<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolSettingsMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ToolDetails" runat="Server">
    <script src="../../js/LatencyTest.js" type="text/javascript"></script>
    <script src="../../js/NumberValidation.js" type="text/javascript"></script>
    <link href="../../Styles/LatencyTestSettingsStyles.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/TabsStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/LatencyTestSettings.js"></script>
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id$='msetlink']").attr('href', 'LatencyTestSettings.aspx');
            $(".mlink").attr('href', 'LatencyTest.aspx');
            $(function () {
                $("#tabs").tabs();
            });
            $("#saveandcontinueButton").click(function () {
                saveSettings("dummy");
            });
            $("#saveSettingsButton").click(function () {
                saveSettings("<%= ToolUrl %>");
            });
            $("#saveandcontinueButtonMobile").click(function () {
                saveSettings("dummy");
            });
            $("#saveSettingsButtonMobile").click(function () {
                saveSettings("<%= ToolUrl %>");
            });
            $("#cancelButtonMobile").click(function () {
                window.location.replace("<%= ToolUrl %>");
            });
        });
        Ext.onReady(function () {
            ToolsetSettingsManager.init(ToolsetSettingsManager);
            });
    </script>
    <div id="content-div" style="padding-bottom: 45px;">
        <div>
            <div id="successMsg" style="display: none;" class="success-msg msg">
                <img src="../../images/ok_16x16.gif" /><span>Settings were successfully saved.</span></div>
            <div id="failedMsg" style="display: none;" class="failed-msg msg">
                <img src="../../images/failed_16x16.gif" /><span id="errorMsg">An Error occured during
                    saving. Please double check your network connection or try it later.</span></div>
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Preferences</a></li>
                    <li><a href="#tabs-2">Metrics</a></li>
                </ul>
                <div id="tabs-1">
                    <asp:Panel runat="server" ID="PanelGeneral" CssClass="settingsPanelLevel2" GroupingText="General">
                        <div class="panelDiv">
                            <div>
                                <label class="label-1">
                                    Payload size:</label>
                                <input type="text" class="txb" onkeypress="return isNumber(event)" onblur="validateRange(this, 0, 2147483648)"
                                    clientidmode="Static" runat="server" id="txbPayloadSize" />
                                <br class="settingsBreaks" />
                                <div class="bottomValue">
                                    <label class="label-2">
                                        bytes, max. value is 2147483648</label></div>
                            </div>
                            <div class="lastdiv">
                                <label class="label-1">
                                    Polling interval:</label>
                                <input type="text" class="txb" onkeypress="return isNumber(event)" onblur="validateRange(this, 4, 120)"
                                    clientidmode="Static" runat="server" id="txbPollingInterval" />
                                <br class="settingsBreaks" />
                                <div class="bottomValue">
                                    <label class="label-2">
                                        seconds</label></div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div id="tabs-2">
                    <h4>
                        Select the metrics you want to see in the results table.</h4>
                    <div id="Grid">
                    </div>
                    <div id="MobileGrid">
                    </div>
                    <div id="Buttons">
                        <a href="#" class="next nav-btn" onclick="selectColumns()"></a><a href="#" class="prev nav-btn"
                            onclick="deselectColumns()"></a>
                    </div>
                    <div id="GridSelected">
                    </div>
                </div>
            </div>
        </div>
        <div id="globalButtons">
            <orion:LocalizableButton ID="saveSettingsButton" ClientIDMode="Static" OnClientClick="return false;"
                LocalizedText="Submit" DisplayType="Primary" runat="server" />
            <orion:LocalizableButton ID="saveandcontinueButton" ClientIDMode="Static" LocalizedText="CustomText"
                Text="Save and Continue Working" DisplayType="Secondary" runat="server" OnClientClick="return false;" />
            <orion:LocalizableButton ID="cancelSettingsButton" ClientIDMode="Static" LocalizedText="Cancel"
                DisplayType="Secondary" runat="server" OnClick="cancelButton_OnClick" />
            <br />
            <asp:Image ID="saveSettingsProgress" runat="server" ImageUrl="../../images/animated_loading_16x16.gif"
                AlternateText="Saving ..." ToolTip="Saving ..." />
        </div>
        <div id="globalButtonsMobile">
            <input type="button" value="Submit" id="saveSettingsButtonMobile" />
            <br id="buttonsBreak" />
            <input type="button" value="Save and Continue Working" id="saveandcontinueButtonMobile" />
            <input type="button" value="Cancel" id="cancelButtonMobile" />
            <br />
        </div>
    </div>
</asp:Content>
