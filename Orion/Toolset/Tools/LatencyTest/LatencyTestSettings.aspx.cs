﻿using System;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.LatencyTest;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Tools_LatencyTestSettings : Page
{
    protected int ToolId
    {
        get { return SupportedTools.LatencyTestToolId; }
    }

    public string ToolUrl
    {
        get
        {
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (LatencyTestTool)toolFactory.CreateTool();
            return UrlHelper.GetToolDetailAddress(tool.DetailPageUrl);
        }
    }

    private string accountId;

    protected void Page_Init(object sender, EventArgs e)
    {
        //this.Master.Master.ImgLogoPath = "../images/AppIcons/TraceRoutelogo.png";
        this.Master.Master.ToolId = ToolId;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        saveSettingsProgress.Visible = false;
        this.accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        if (!Page.IsPostBack)
        {
            // Load permanent settings
            var settings = (LatencyTestPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this.accountId);
            LoadSettingsToForm(settings);
        }
        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/LatencyTestSettingsStyles.responsive.css";
        }
    }

    private void LoadSettingsToForm(LatencyTestPermanentSettings settings)
    {
        if (settings == null)
            throw new ArgumentNullException("settings");
        txbPollingInterval.Value = (settings.PollingInterval / 1000).ToString(CultureInfo.CurrentCulture);
        txbPayloadSize.Value = (settings.PayloadSize).ToString(CultureInfo.CurrentCulture);
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        GoBack();
    }

    public void GoBack()
    {
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
        var tool = (LatencyTestTool)toolFactory.CreateTool();
        Response.Redirect(UrlHelper.GetToolDetailAddress(tool.DetailPageUrl));
    }
}