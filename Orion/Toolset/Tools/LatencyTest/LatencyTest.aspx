﻿<%@ Page Title="Latency Test" AutoEventWireup="true" CodeFile="LatencyTest.aspx.cs"
    Inherits="Orion_Toolset_Tools_LatencyTest" Language="C#" MasterPageFile="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>

<%@ MasterType VirtualPath="~/Orion/Toolset/Tools/ToolDetailMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Tools/ToolMasterPage.master" %>
<%@ Reference VirtualPath="~/Orion/Toolset/Services/LatencyTestService.asmx" %>
<asp:Content ContentPlaceHolderID="ToolDetails" runat="Server">
    <orion:Include ID="IncludeExtJs" runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include ID="Include2" runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
    <link rel="stylesheet" type="text/css" href="../../Styles/LatencyTestStyles.css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/MonitorGrid.css" />
    <link rel="stylesheet" type="text/css" href="../../../js/extjs/4.2.2/resources/ext-theme-gray-sandbox-42-all.css" />
    <script type="text/javascript" src="../../js/LatencyTest.js"></script>
    <link runat="server" id="link" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../js/OrionMasterPage.js/WebToolsetMenu.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Styles/DropDownButton.css" />
    <script type="text/javascript" src="../../js/DropDownButtonTools.js"></script>
    <%if (!this.Master.IsMobileMode)
       {%>
    <link rel="stylesheet" type="text/css" href="../../Styles/ExportButtonStyles.css" />
    <% } %>

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true" />
    <div id="latencyTest">
        <div class="latency-test-content-wrapper">
            <div class="enter-hostname-wrapper">
                <div class="enter-hostname-or-ip-div">
                    <div id="input-button">
                        <orion:LocalizableButton ID="RunButton" ClientIDMode="Static" OnClientClick="return false;"
                LocalizedText="CustomText" DisplayType="Primary" runat="server" Text="Run" />
                        <input type="button" id="RunButtonMobile" value="Run" />
                    </div>
                    <div id="checkbox-div">
                        <input runat="server" clientidmode="Static" type="checkbox" id="cbKeepHistory" value="value" />
                        <label for="cbKeepHistory">Keep previous results</label>
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <div id="PollingEngines">
                    <table>
                        <tr class="x42-form-item-input-row">
                            <td style="width: 105px;" class="x42-field-label-cell">
                                <label style="width: 100px; margin-right: 5px;">
                                    Polling Engines<span role="separator">:</span></label>
                            </td>
                            <td style="width: 150px;">
                                <select id="pollingEnginesSelect" style="width: 10em;">
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="content-preview-wrapper">
                <div id="results-wrapper">
                    <div id="statusExportDiv">
                        <div class="div-spinner" id="divSpinner">
                            <div id="loadDiv" class="title-div">
                                <div class="status-line">
                                    <span><b>Latency Test</b></span>
                                </div>
                                <div class="button-wrapper" id="exportButton" style="display: none;">
                                    <button class="flat-button" data-dropdown="#export-div">
                                        <img class="flat-button-img" src="../../images/export-icon.png" />
                                        Export
                                        <img class="flat-button-imgDown" src="../../images/arrow-down.png" />
                                    </button>
									<div id="export-div" class="dropdown dropdown-tip">
										<ul class="dropdown-menu">
											<li><a id="ExportToCSVId" href="#1">CSV</a></li>
											<li><a id="ExportToTxtId" href="#2">Text</a></li>
											<li><a id="ExportToPdf" href="#3">PDF</a></li>
										</ul>
									</div>
                                </div>
                                <br class="statusBreak" />
                                <div id="loading-cancel-div">
                                    <img id="imgUpdateProgress" src="../../images/animated_loading_16x16.gif" />
                                    <span>Initialization in progress...</span>
                                    <span class="moblinks"><button id="cancelButton" class="CancelLink" title="Stop">
                                        Stop</button></span>
                                </div>
                                <span id="restart-div">
                                    <span style="font-size: small"> &nbsp; Testing has been stopped</span>
                                    <span class="moblinks"><button id="restartButton" class="CancelLink" title="Restart">
                                        Restart</button></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="statistics">
                        <span><b>Statistics:</b></span>
                        <div id="clickToJobStart"></div>
                        <div id="clickToFirstResultOnWebPage"></div>
                        <div id="signalRDelay"></div>
                        <div id="resultToWebPage"></div>
                        <br/>
                        <div id="tableRendering"></div>
                        <div id="delayBetweenResults"></div>
                        <br/>
                    </div>
                    <div id="jsonData" style="clear: both; visibility: hidden; overflow: auto; width: 100%;">
                    </div>
                    <div id="jsonDataMobile">
                    </div>
                    <div id="completeStatus"></div>
                </div>                
                <asp:HiddenField runat="server" ID="txtResTblWidths" ClientIDMode="Static" />
                <input type="hidden" id="latencyTestHiddenField" />
            </div>
        </div>
    </div>
</asp:Content>
