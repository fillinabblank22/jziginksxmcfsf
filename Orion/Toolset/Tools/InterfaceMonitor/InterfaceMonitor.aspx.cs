﻿using System;
using System.Web.UI;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.InterfaceMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Tools_InterfaceMonitor : Page
{
    private bool stopRequested = false;
    public int ChartDataLimitByTimeMin;
    public int ChartDataLimitByPoints;
    public int ChartDataLimitType;

    public int PollingFrequency { get; set; }

    protected int ToolId
    {
        get
        {
            return SupportedTools.InterfaceMonitorId;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        string newUrl = UrlHelper.SwapNetobjectWithToolParams();

        if (newUrl != null)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Redirect(newUrl);
        }

        this.Master.Master.ImgLogoPath = "../images/AppIcons/InterfaceMonitorlogo.png";
        this.Master.Master.ToolId = ToolId;
        var settings = (InterfaceMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, User.Identity.Name);
        PollingFrequency = settings.InterfacePollInterval / 1000;
        ChartDataLimitByPoints = settings.ChartDataLimitByPoints;
        ChartDataLimitByTimeMin = settings.ChartDataLimitByTimeMin;
        ChartDataLimitType = (int)settings.ChartDataLimitType;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/InterfaceMonitorStyles.responsive.css";
        }
    }
}
