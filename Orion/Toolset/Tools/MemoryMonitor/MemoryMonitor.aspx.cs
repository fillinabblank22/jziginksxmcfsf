﻿using System;
using System.Web.UI;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.MemoryMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;

public partial class Orion_Toolset_Tools_MemoryMonitor_MemoryMonitor : Page
{
    public int PollingFrequency { get; set; }

    protected int ToolId
    {
        get
        {
            return SupportedTools.MemoryMonitorToolId;
        }
    }

    public int ChartDataLimitByTimeMin;
    public int ChartDataLimitByPoints;
    public int ChartDataLimitType;

    protected void Page_Init(object sender, EventArgs e)
    {
        string newUrl = UrlHelper.SwapNetobjectWithToolParams();

        if (newUrl != null)
        {
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Redirect(newUrl);
        }

        this.Master.Master.ImgLogoPath = "../images/AppIcons/memory_monitor_logo.png";
        this.Master.Master.ToolId = ToolId;
        var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, User.Identity.Name);
        PollingFrequency = settings.MemoryPollInterval / 1000;
        ChartDataLimitByPoints = settings.ChartDataLimitByPoints;
        ChartDataLimitByTimeMin = settings.ChartDataLimitByTimeMin;
        ChartDataLimitType = (int)settings.ChartDataLimitType;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/MemoryMonitor.responsive.css";
        }
    }
}