﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Windows.Forms.VisualStyles;
using Microsoft.Practices.ObjectBuilder2;
using SolarWinds.Toolset.Common.Extensions;
using SolarWinds.Toolset.Common.ResultColumnsSettings;
using SolarWinds.Toolset.Common.Tools;
using SolarWinds.Toolset.Tools;
using SolarWinds.Toolset.Tools.MemoryMonitor;
using SolarWinds.Toolset.Web.DAL;
using SolarWinds.Toolset.Web.Helpers;
using Label = System.Web.UI.WebControls.Label;

public partial class Orion_Toolset_Tools_MemoryMonitor_MemoryMonitorSettings : System.Web.UI.Page
{
    protected int ToolId
    {
        get
        {
            return SupportedTools.MemoryMonitorToolId;
        }
    }

    public string ToolURL
    {
        get
        {
            var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
            var tool = (MemoryMonitorTool)toolFactory.CreateTool();
            return UrlHelper.GetToolDetailAddress(tool.DetailPageUrl);
        }
    }

    private string _accountId;
    private ToolsDAL _toolsDal = new ToolsDAL();
    private SolarWinds.Logging.Log _myLog = new SolarWinds.Logging.Log();

    protected void Page_Init(object sender, EventArgs e)
    {
        _accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        this.Master.Master.ImgLogoPath = "../images/AppIcons/memory_monitor_logo.png";
        this.Master.Master.ToolId = ToolId;
        var items = Thresholds.Items;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadDataFromDataSource();
        }
        if (this.Master.IsMobileMode)
        {
            link.Attributes["href"] = "../../Styles/MonitorsSettingsStyles.responsive.css";
        }
    }

    private void LoadDataFromDataSource()
    {
        var ds = CreateDataSource();
        Thresholds.DataSource = ds;
        Thresholds.DataBind();
        LoadChartLimits();
        LoadPollingInterval();
    }

    private void LoadPollingInterval()
    {
        var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);
        txtPollingInterval.Text = (settings.MemoryPollInterval / 1000).ToString();
    }


    private enum TimeLimitValueType
    {
        Minutes,
        Hours
    }

    private void LoadChartLimits()
    {
        var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);

        if (settings.ChartDataLimitType == ChartsRetentionSettings.LimitByTime)
        {
            rdLimitByTime.Checked = true;
            txtLimitByPoints.Enabled = false;
        }
        else
        {
            txtLimitByTime.Enabled = false;
            drpValueType.Enabled = false;
            rdLimitByPoints.Checked = true;
        }

        drpValueType.Items.Add(new ListItem("Minutes", TimeLimitValueType.Minutes.ToString()));
        drpValueType.Items.Add(new ListItem("Hours", TimeLimitValueType.Hours.ToString()));

        var tmp = Convert.ToInt32(settings.ChartDataLimitByTimeMin.ToString());
        if (tmp > 120)
        {
            tmp = tmp / 60;
            drpValueType.SelectedValue = TimeLimitValueType.Hours.ToString();
        }
        txtLimitByTime.Text = tmp.ToString();
        txtLimitByPoints.Text = settings.ChartDataLimitByPoints.ToString();
    }

    private DataSet CreateDataSource()
    {
        _accountId = OrionAccountHelper.GetCurrentLoggedUserName();
        DataTable dt = new DataTable();
        dt.Columns.Add("DisplayName", typeof(string));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("UseThreshold", typeof(bool));
        dt.Columns.Add("CriticalLevel", typeof(string));
        dt.Columns.Add("CriticalValue", typeof(string));
        dt.Columns.Add("WarningLevel", typeof(string));
        dt.Columns.Add("WarningValue", typeof(string));
        dt.Columns.Add("Units", typeof(string));

        var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, this._accountId);
        var objects = settings.GetAllColumns();

        foreach (var setting in objects)
        {
            if (setting.ValueType == ResultColumnValueType.None)
                continue;
            var row = dt.NewRow();
            row["DisplayName"] = setting.DisplayName;
            row["Name"] = setting.PropertyName;
            row["UseThreshold"] = setting.UseThreshold;
            row["CriticalLevel"] = setting.CriticalThresholdOperator.ToDescription();
            row["CriticalValue"] = setting.CriticalThresholdValue;
            row["WarningLevel"] = setting.WarningThresholdOperator;
            row["WarningValue"] = setting.WarningThresholdValue;
            if (setting.ValueType != ResultColumnValueType.None)
                row["Units"] = setting.Unit;

            dt.Rows.Add(row);
        }

        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds;
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        GoBack();
    }

    public void GoBack()
    {
        var toolFactory = ToolFactoryProvider.Instance.GetToolFactory(this.ToolId);
        var tool = (MemoryMonitorTool)toolFactory.CreateTool();
        Response.Redirect(UrlHelper.GetToolDetailAddress(tool.DetailPageUrl));
    }

    //protected void SaveChanges()
    //{
    //    var settings = (MemoryMonitorPermanentSettings)new ToolsDAL().GetToolPermanentSettings(ToolId, _accountId);
    //    var elements = settings.GetAllColumns();

    //    GetFieldsData(elements);
    //    _toolsDal.SaveToolPermanentSettings(ToolId, _accountId, settings);
    //    LoadDataFromDataSource();
    //    Result.Value = "True";
    //}

    //private void GetFieldsData(List<ResultColumnSettings> elements)
    //{
    //    foreach (ListViewDataItem item in this.Thresholds.Items)
    //    {
    //        var label = item.FindControl("lblName") as Label;
    //        if (label == null)
    //            continue;
    //        var name = label.Text;
    //        var checkBox = item.FindControl("chbUseThresholds") as CheckBox;
    //        if (checkBox == null)
    //            continue;
    //        var useThresholds = checkBox.Checked;
    //        var dropDown = item.FindControl("drpWarningLevel") as DropDownList;
    //        if (dropDown == null)
    //            continue;
    //        var tmp = dropDown.SelectedValue;
    //        ThresholdOperator warningLevel;
    //        var result = MemoryMonitorPermanentSettings.TryParseThresholdOperator(tmp, out warningLevel);
    //        var warnTextBox = item.FindControl("txtWarningValue") as System.Web.UI.WebControls.TextBox;
    //        if (warnTextBox == null)
    //            continue;
    //        var warningValue = warnTextBox.Text;
    //        var critTextBox = item.FindControl("txtCriticalValue") as System.Web.UI.WebControls.TextBox;
    //        if (critTextBox == null)
    //            continue;
    //        var criticalValue = critTextBox.Text;
    //        var element = elements.Single(c => c.PropertyName.Equals(name, StringComparison.CurrentCultureIgnoreCase));
    //        element.UseThreshold = useThresholds;
    //        element.WarningThresholdOperator = warningLevel;
    //        element.WarningThresholdValue = Convert.ToDouble(warningValue);
    //        element.CriticalThresholdOperator = warningLevel;
    //        element.CriticalThresholdValue = Convert.ToDouble(criticalValue);
    //    }
    //}

    //protected void SaveThresholds(object sender, EventArgs e)
    //{
    //    SaveChanges();
    //}

    //protected void SaveThresholdsAndGo(object sender, EventArgs e)
    //{
    //    SaveThresholds(sender, e);
    //    Response.Redirect(ToolURL);
    //}

    //protected void SaveTHIS(object sender, EventArgs e)
    //{
    //    SaveChanges();
    //}

    //protected void OnItemCommand_(object source, RepeaterCommandEventArgs e)
    //{
    //    SaveChanges();
    //}

    protected void ItemCreated(object sender, ListViewItemEventArgs e)
    {
        {
            LoadDropDownList(e);
        }
    }

    private static void AddExtraClassToThresholdsValues(ListViewItemEventArgs row)
    {
        var elementsList = new List<string>()
        {
            "txtWarningValue",
            "txtCriticalValue"
        };
        var className = "ThresholdsValues";
        AddExtraClass(elementsList, className, row);
    }

    private static void AddExtraClass(List<string> elementsList, string className, ListViewItemEventArgs row)
    {
        foreach (var element in elementsList)
        {
            var textBox = row.Item.FindControl(element) as System.Web.UI.WebControls.TextBox;
            if (textBox != null)
                textBox.CssClass = string.Format("{0}{1} {2}", className, row.Item.ID, textBox.CssClass);
        }
    }

    private static void LoadDropDownList(ListViewItemEventArgs e)
    {
        var warningLevel = (DropDownList)e.Item.FindControl("drpWarningLevel");
        foreach (ThresholdOperator foo in Enum.GetValues(typeof(ThresholdOperator)))
        {
            warningLevel.Items.Add(new ListItem(foo.ToDescription(), foo.ToString()));
        }
        AddExtraClassToThresholdsValues(e);
    }
}