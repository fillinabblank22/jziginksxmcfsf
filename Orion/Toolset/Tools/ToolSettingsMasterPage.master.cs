﻿using System;
using SolarWinds.Orion.Common;

public partial class Orion_Toolset_Tools_ToolSettingsMasterPage : System.Web.UI.MasterPage
{
    private bool? _ismobile;

    public bool IsMobileMode
    {
        get
        {
            return _ismobile.HasValue ? _ismobile.Value :
                (_ismobile = CommonWebHelper.IsMobileView(Request)).Value;
        }
    }

    public string IsDemo
    {
        get
        {
            return OrionConfiguration.IsDemoServer.ToString().ToLower();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind();

    }
}
