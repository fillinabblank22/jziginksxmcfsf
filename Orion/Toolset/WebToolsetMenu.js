// SW.Toolset.WebMenu

var userHasTools = false;
var userTools;
var interfaceBasedTools = [];
var isIE8 = (window.navigator.userAgent.indexOf("MSIE 8") > 0);

$(function () {

    // -- watch for new content

    function InitWatcher() {

        // locate net object links. verify
        $("a[tsextend!='processed']").livequery(function () {
            this.tsextend = 'processed';

            var me = this, isnode = 1, href = $(me).attr('href') || '', host = $(me).attr('nodehostname') || $(me).attr('ip');

            if (!host) return;

            // apparently, only these are links worthy of instrumenting.

            if (/NodeDetails\.aspx\?NetObject=N/i.test(href)) {

                if (me.parentNode instanceof HTMLTableCellElement) {
                    me.parentNode.appendChild(createToolsetButtonInTable());
                }
                else if ((me.parentNode instanceof HTMLDivElement) && !$(me.nextSibling).hasClass("toolsetButtonInTree")) {
                    // apparently, we are in a tree?
                    me.parentNode.insertBefore(createToolsetButtonInTree(), me.nextSibling);
                }
            }
            else if ($(me).attr('ifname')) {

                if (interfaceBasedTools.length < 1) return;

                if (!$(me.parentNode.lastChild).hasClass("toolsetButtonInTable") && (me.parentNode instanceof HTMLHeadingElement)) {
                    me.parentNode.appendChild(createIfToolsetButtonInTitle());
                }
                else if (!$(me.parentNode.lastChild).hasClass("toolsetButtonInTable")) {
                    me.parentNode.appendChild(createIfToolsetButtonInTable());
                }    
            }
        });
    };

    // -- common methods to find the existance of js - Starts

    var geturl = function (url) {
        var rx = /^(?:https?:\/\/[^\/]+)?(\/.*?)(?:\.i18n\.ashx|$|[?])/i;
        m = url.match(rx);
        return (m && m[1] || url).toLowerCase();
    };

    function IsFileAlreadyExist(checkurl) {
        var found = null;

        $('script[src*=".js"]').each(function () {
            if (checkurl == geturl(this.src)) {
                found = this;
                return false;
            }
            return true;
        });
        return found ? true : false;
    }

    // -- common methods to find the existance of js - Ends

    // -- load menu resources

    function InitMenu() {

        // load missing scripts and css if not already loaded.

        if (!IsFileAlreadyExist(('/Orion/Toolset/js/DropDownButton.js').toLowerCase())) {
            var head = document.getElementsByTagName('head')[0];
            var elem = document.createElement('script');
            elem.type = 'text/javascript';
            elem.src = '/Orion/Toolset/js/DropDownButton.js.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}';
            head.appendChild(elem);

            elem = document.createElement('link');
            elem.rel = 'stylesheet';
            elem.type = 'text/css';
            elem.href = '/Orion/Toolset/Styles/DropDownButton.css.i18n.ashx?l=@{R=Core.LiveCss;K=Locale}&v=@{R=Core.LiveCss;K=WebVersion}&csd=@{R=Core.LiveCss;K=CssData}';
            head.appendChild(elem);
        }

        InitMenuMarkup();

        InitWatcher();
    };

    // -- ts enabled?

    function VerifyEnablement() {
        if (SW.Toolset.DisableWebToolMenu) return;
        var fnFetchToolPermissions = function () {
            if (!userHasTools) return;

            $.ajax({
                type: "GET",
                url: "/Orion/Toolset/Services/Toolset.asmx/GetUserTools",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: function (data) {
                    userTools = data.d || [];
                    $(userTools).each(function (i, obj) { if (obj.IsInterfaceBased) interfaceBasedTools.push(obj); });
                    if (interfaceBasedTools.length < 0) userHasTools = false;
                    if (userHasTools) InitMenu();
                },
                error: function (data, textStatus, xhr) { if (window.console && window.console.log) window.console.log(xhr); }
            });
        };

        // check if user has toolset enabled    
        $.ajax({
            type: "GET",
            url: "/Orion/Toolset/Services/Toolset.asmx/HasToolsEnabled",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data) {
                userHasTools = data.d;
                if (userHasTools) fnFetchToolPermissions();
            },
            error: function (data, textStatus, xhr) { if (window.console && window.console.log) window.console.log(xhr); }
        });
    };

    // -- markup

    function createToolsetButton(className) {
        var div = document.createElement('div');
        div.className = className;
        div.innerHTML = '<img title="View Toolset" width=16 height=16 class="dropDownButton sw-ts-dd" data-dropdown="#dropdown-1" onclick="SW.Toolset.WebMenu.onToolButtonClick(this);" src="/Orion/Toolset/images/blank.gif" />';
        return div;
    }

    function createToolsetButtonInTable() { return createToolsetButton('toolsetButton toolsetButtonInTable'); }

    function createToolsetButtonInTree() { return createToolsetButton('toolsetButton toolsetButtonInTree'); }

    function InitMenuMarkup() {
        if (!userHasTools) return;

        //create menu for nodes
        var body = document.getElementsByTagName('body')[0];
        var div = document.createElement('div');
        var innerHtml = ' <ul class="dropdown-panel dropdown-menu has-icon nodeDropdown">'
        + '<li class="suggestedTools">Toolset</li>';
        for (var i = 0; i < userTools.length; i++) {
            innerHtml += '<li class="toolItemLI"><div class="toolItem"><a href="#1" id="' + userTools[i].Key + '" onclick="SW.Toolset.WebMenu.onToolClick(this);" style="background-image: url(\/Orion\/toolset\/images\/AppIcons\/' + userTools[i].IconFileName + ');">' + userTools[i].Name + '</a></div></li>';
        }
        innerHtml += '</li><li><div class="allToolsItem"><a href="/Orion/Toolset/AllTools.aspx">All Tools</a></div></li></ul>';
        div.innerHTML = innerHtml;
        div.className = 'dropdown dropdown-tip nodeDropdown';
        div.id = 'dropdown-1';
        body.appendChild(div);

        $(div).css('display', 'none'); // hide the menu!

        if (interfaceBasedTools.length > 0) {
            //create tool menu for interfaces
            div = document.createElement('div');
            innerHtml = ' <ul class="dropdown-panel dropdown-menu has-icon nodeDropdown">'
            + '<li class="suggestedTools">Toolset</li>';
            for (i = 0; i < interfaceBasedTools.length; i++) {
                innerHtml += '<li class="toolItemLI"><div class="toolItem"><a href="#1" id="' + interfaceBasedTools[i].Key
                + '" onclick="SW.Toolset.WebMenu.onToolClick(this);" style="background-image: url(\/Orion\/toolset\/images\/AppIcons\/' + interfaceBasedTools[i].IconFileName + ');">' + interfaceBasedTools[i].Name + '</a></div></li>';
            }
            innerHtml += '<li><div class="allToolsItem"><a href="/Orion/Toolset/AllTools.aspx">All Tools</a></li></div></ul>';
            div.innerHTML = innerHtml;

            div.className = 'dropdown dropdown-tip nodeDropdown';
            div.id = 'dropdown-2';
            body.appendChild(div);
            $(div).css('display', 'none');
        }
    }

    function createIfToolsetButtonInTable() {
        var div = document.createElement('div');
        div.className = "toolsetButton toolsetButtonInTable";
        div.innerHTML = '<img title="View Toolset" width=16 height=16 class="dropDownButton sw-ts-dd" data-dropdown="#dropdown-2" onclick="SW.Toolset.WebMenu.onToolButtonClick(this);" src="/Orion/Toolset/images/blank.gif"/>';
        return div;
    }

    function createIfToolsetButtonInTitle() {
        var div = document.createElement('div');
        div.className = "toolsetButton toolsetButtonInTitle";
        div.innerHTML = '<img title="View Toolset" width=16 height=16 class="dropDownButton sw-ts-dd" data-dropdown="#dropdown-2" onclick="SW.Toolset.WebMenu.onToolButtonClick(this);" src="/Orion/Toolset/images/blank.gif"/>';
        return div;
    }

    var lastClickedNode;

    function getToolUrl(toolid) {
        var elm = $(lastClickedNode), path = '/Orion/Toolset/Tools/' + toolid + '/' + toolid + '.aspx',
            ip = elm.attr('ip'),
            nodehostname = elm.attr('nodehostname');

        if (!ip && !nodehostname) {
            var m = (elm.attr('href') || '').match(/[?&]NetObject=([IN]):([0-9]+)/i);
            if (m) return path + '?NetObject=' + m[1] + ':' + m[2];
        }

        var community = elm.attr('community'),
            ifindex = elm.attr('ifindex'),
            ifname = elm.attr('ifname');

        return path + '?ip=' + ip + '&nodehostname=' + nodehostname + '&community=' + community + '&ifindex=' + ifindex + '&ifname=' + ifname;
    }

    function runTool(toolid) {
        var url = getToolUrl(toolid);
        window.open(url, "", "");
    }

    // -- events & public fns

    var ns = SW.Core.namespace('SW.Toolset.WebMenu');

    ns.onToolClick = function (sender) {
        var id = sender.getAttribute("id");
        runTool(id);
    };

    ns.onToolButtonClick = function (sender) {
        if (sender.parentNode.previousSibling instanceof HTMLAnchorElement) {
            lastClickedNode = sender.parentNode.previousSibling;
        } else {
            lastClickedNode = sender.parentNode.previousSibling.previousSibling;
        }
    };

    // -- todo: call init from markup after toolset can inject markup in all pages
    ns.Init = function () { VerifyEnablement(); };

    ns.Init();
});
