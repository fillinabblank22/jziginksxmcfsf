﻿using System;
using Resources;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.SamAppOptics.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
[ResourceMetadata(StandardMetadataPropertyName.Type, "AppOptics")]
[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Owner, "Applications")]
public partial class Orion_SamAppoptics_Resources_GettingStarted : BaseResourceControl
{
    private readonly IResourceVisibility _resourceVisibility;
    protected override string DefaultTitle => SamAppOpticsWebContent.GettingStarted_Title;

    public Orion_SamAppoptics_Resources_GettingStarted() : this(new GettingStartedResourceVisibility())
    {
    }

    public Orion_SamAppoptics_Resources_GettingStarted(IResourceVisibility resourceVisibility)
    {
        _resourceVisibility = resourceVisibility;
    }

    protected override void OnInit(EventArgs e)
    {
        var netObjectProvider = GetInterfaceInstance<INetObjectProvider>();
        var netObject = (netObjectProvider != null) ? netObjectProvider.NetObject : null;

        Wrapper.Visible = _resourceVisibility.IsVisible(netObject);

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Profile.AllowAdmin)
        {
            getStartedButton.Attributes["onclick"] = "SW.SamAppOptics.PermissionsRequiredDialog(); return false;";
        }
        if (IsDetached(Request.Url.ToString()) || !Profile.AllowAdmin)
        {
            RemoveResourceLink.Visible = false;
        }
    }

    protected void OnRemoveResourceButtonClicked(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }

    private static bool IsDetached(string absoluteUri)
    {
        return absoluteUri.Contains("DetachResource.aspx");
    }

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Static;
}
