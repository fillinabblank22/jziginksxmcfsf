﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GettingStarted.ascx.cs" Inherits="Orion_SamAppoptics_Resources_GettingStarted" %>
<%@ Import Namespace="Resources" %>

<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="core" TagName="HelpLink" %>

<orion:Include runat="server" File="SamAppOptics/styles/Resources.css" />
<orion:Include runat="server" File="SamAppOptics/js/samAppOptics.PermissionsRequiredDialog.js" Section="Bottom" />

<orion:resourceWrapper runat="server" ID="Wrapper" ShowHeaderBar="false" CssClass="sam-appoptics-getting-started">
    <Content>
        <div class="getting-started-container">

            <div class="getting-started-header">
                <div class="getting-started-icon"></div>
                <span class="header-title"><%= SamAppOpticsWebContent.GettingStarted_Title_Part1 %></span>
                <span class="header-subtitle"><%= SamAppOpticsWebContent.GettingStarted_Title_Part2 %></span>
            </div>

            <div class="getting-started-separator"></div>

            <div class="getting-started-content">
                <h5><%= SamAppOpticsWebContent.GettingStarted_SubTitle %></h5>
                <p><%= SamAppOpticsWebContent.GettingStarted_Description %></p>
                <div>
                    <core:HelpLink ID="samAppOpticsHelpLink" runat="server" HelpUrlFragment="OrionSAMAppOpticsIntegration" LocalizedText="CustomText" HelpDescription="<%$ Resources: SamAppOpticsWebContent, GettingStarted_LearnMoreLink %>" />
                </div>
            </div>
            <div class="getting-started-separator"></div>
            <div class="getting-started-footer">
                <asp:LinkButton ID="RemoveResourceLink" CssClass="getting-started-remove-link" OnClick="OnRemoveResourceButtonClicked" runat="server" Text="<%$ Resources: SamAppOpticsWebContent, GettingStarted_Remove_Resource_Link %>"></asp:LinkButton>
                <orion:LocalizableButtonLink runat="server" ID="getStartedButton" NavigateUrl="/ui/samappoptics/integration" DisplayType="Primary" LocalizedText="CustomText" Text="<%$ Resources: SamAppOpticsWebContent, GettingStarted_Button_Label %>" />
            </div>
        </div>
    </Content>
</orion:resourceWrapper>
