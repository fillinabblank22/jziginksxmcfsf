using SolarWinds.Orion.AccountManagement.Models;
using SolarWinds.Orion.AccountManagement.Saml;
using SolarWinds.Orion.AccountManagement.Web;
using System;
using Resx = Resources.AccountManagementWebContent;

namespace SolarWinds.Orion.AccountManagement.LegacyWebSite
{
    public partial class Orion_SamlInit : SamlPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageInit();

            try
            {
                // check if is SAML authentication enabled
                if (!samlManager.IsSamlEnabled())
                    HandleLoginException(Resx.SamlDisabledTitle, Resx.SamlDisabledMessage);

                // handle test mode    
                if (relayState.IsTestMode)
                {
                    samlTestResult.SamlInitLoaded = DateTime.Now;
                    SaveTestResult();
                }

                // configure SAML library
                samlManager.ConfigureSaml(Request);

                // send SAML request
                samlManager.InitiateSSO(Response, relayState.ToQueryString());
            }
            catch (Exception ex)
            {
                log.Error(ex);

                if (relayState.IsTestMode)
                {
                    WriteExceptionToTestResult(ex);
                    Response.Redirect(SamlWebHelper.GetTestResultUrl(relayState), true);
                }
                else
                {
                    HandleLoginException(Resx.SamlAuthErrorTitle, Resx.SamlAuthErrorMessage);
                }
            }
        }
    }
}