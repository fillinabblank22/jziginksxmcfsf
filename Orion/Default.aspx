﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/OrionMinReqs.master" Title="New Orion Web Thingy" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        // sync changes to login.aspx.cs::DoRedirect
        Response.Redirect(string.Format("View.aspx{0}", 
            (Request.QueryString["IsMobileView"] != null) ? string.Format("?IsMobileView={0}", Request.QueryString["IsMobileView"]) : string.Empty));
        base.OnLoad(e);
    }
</script>