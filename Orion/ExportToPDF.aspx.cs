﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Text;
using System.Threading;
using System.IO;
using EO.Pdf;
using HtmlAgilityPack;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using HtmlNode = HtmlAgilityPack.HtmlNode;
using System.Text.RegularExpressions;

public partial class Orion_ExportToPDF : System.Web.UI.Page
{
    private const string OUT_FILE = "EXPORTTOPDF_OUTFILE";
    private const string EXPORT_COMPLETE = "EXPORTTOPDF_EXPORTCOMPLETE";    // set when the export is complete, unset when the file is deleted.
    private const string EXPORT_ERROR = "EXPORTTOPDF_ERROR";                // set to empty string if no errors, otherwise set to the error message
    private const string EXPORT_PENDING = "EXPORTTOPDF_EXPORTPENDING";      // set when the export is started, reset when the export is complete
    private const string FILE_EXISTS = "EXPORTTOPDF_FILESTILLEXISTS";       // set when the file is created, then reset when the file is deleted
    private const string DOCUMENT_TITLE = "EXPORTTOPDF_DOCUMENTTITLE";      // this helps us build the filename when retrieved from the server
    private const string HTTPS_SERVER_SUBJECT = "HTTPS_SERVER_SUBJECT";       // returns the subject field of the server certificate

    private string _certIssuedTo;
    private readonly Log log = new Log();
    bool _serverSettingsAvailable = false;
    int _serverPort;
    string _serverName;

    Dictionary<string, string> imageCache = new Dictionary<string, string>();

    private string GetCertIssuedTo(NameValueCollection requestParams)
    {
        if (!string.IsNullOrEmpty(_certIssuedTo))
        {
            //In case setting exists in config - use it
            return _certIssuedTo;
        }
	
        if (requestParams == null)
        {
            return null;
        }

        var serverSubject = requestParams[HTTPS_SERVER_SUBJECT];
        if (string.IsNullOrEmpty(serverSubject))
        {
            return null;
        }

        var serverSubjectData = serverSubject.Split(new[] { "\n", "\r\n", "," }, StringSplitOptions.RemoveEmptyEntries);
        return
            (
                from row in serverSubjectData
                select row.Split('=') into subject
                where subject.Count() == 2 && subject[0].Trim().Equals("CN", StringComparison.OrdinalIgnoreCase) && !subject[1].Trim().StartsWith("*.")  //skip formats like “*.domain.com” 
                select subject[1].Trim()
            ).FirstOrDefault();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int port;
        bool natFixEnabled;
        if (!bool.TryParse(ConfigurationManager.AppSettings["ExportToPDF.EnableNATFix"], out natFixEnabled))
            natFixEnabled = true;

        // When Orion server is running behind NAT we need to replace server name and port to correct values within the page url
        if (natFixEnabled && Int32.TryParse(Request.ServerVariables["SERVER_PORT"], out port)
            && !string.IsNullOrEmpty(Request.ServerVariables["LOCAL_ADDR"]))
        {
            _serverPort = port;
            IPAddress ip;

            // FB75818. In case "LOCAL_ADDR" is IPv6 we can wrap this address with square brackets
            // but this will work only for browsers without proxy settings or configured proxy server must have an IPv6 address assigned 
            // and the proxy server must able to proxy IPv6 addresses (see http://msdn.microsoft.com/en-us/library/ms740593(v=vs.85).aspx).
            if (IPAddress.TryParse(Request.ServerVariables["LOCAL_ADDR"], out ip)
                && ip.AddressFamily == AddressFamily.InterNetwork)
            {
                _serverName = Request.ServerVariables["LOCAL_ADDR"];
                _serverSettingsAvailable = true;
            }
            else if (!string.IsNullOrEmpty(Server.MachineName))
            {
                _serverName = Server.MachineName;
                _serverSettingsAvailable = true;
            }
        }

        // get setting only for ssl
        if (Request.Params["SERVER_PORT_SECURE"] != "0")
        {
            _certIssuedTo = ConfigurationManager.AppSettings["ExportToPDF.HttpsServerName"];
        }

        ProcessRequest();
    }

    /// <summary>
    ///  Determines if this is the initial request, or an are we done check
    /// </summary>
    internal void ProcessRequest()
    {
        bool pageDisabled = WebSettingsDAL.GetValue("ExportToPDFPageDisabled", true);
        if (pageDisabled)
        {
            log.Error("ExportToPDf: Access to the page ExportToPDf is denied.");
            Response.Write("Access denied.");
            Response.StatusCode = 403;
            Response.End();
            return;
        }

        if (EnginesDAL.GetCachedFIPSModeOnPrimaryEngine())
        {
            log.Error("ExportToPDf: Export to PDF is disabled due to incompatibility with FIPS.");
            Response.Write("ERROR Export to PDF is disabled due to incompatibility with FIPS.");
            Response.End();
            return;
        }

        // we allow only POST requests to this page
        if ("POST".Equals(Request.HttpMethod, StringComparison.OrdinalIgnoreCase) == false)
        {
            Response.StatusCode = 405;
            Response.End();
            return;
        }

        if (!AntiXsrfProtectionHelper.ValidateAntiXsrfProtection(Request))
        {
            log.Error("ExportToPDf: Validation of Anti-XSRF token failed.");
            Response.Write("ERROR Validation of Anti-XSRF token failed.");
            Response.End();
            return;
        }

        string queryString = Request.Params["QUERY_STRING"];
        log.Info("queryString: " + queryString);

        bool isProgressCheck = ExtractProgressCheck(queryString);
        bool isFileRequest = ExtractGimmeTheFile(queryString);
        bool isStartRequest = !isProgressCheck && !isFileRequest;
        bool isDeleteRequest = ExtractDeleteRequest(queryString);

        string exportID = ExtractExportID();

        if (isStartRequest)
        {
            #region DebugRequestParams
            //string strOut = string.Empty;
            //foreach (string str in Request.Params)
            //    strOut += ("\t" + str + "=" + Request.Params[str] + "</p>\r\n");
            //log.Info("Request Params: \r\n" + strOut);
            #endregion

            log.Info("ExportToPDf: ProcessRequest() starting export...:");
            log.Info("QueryString: " + queryString);

            //Response.Write("<script type='text/javascript' >exportID = " + exportID + ";</script>");

            Session[EXPORT_COMPLETE + exportID] = "0";
            Session[EXPORT_PENDING + exportID] = "1";
            if ((string)Session[FILE_EXISTS + exportID] == "1")
            {
                string outFile = Session[OUT_FILE + exportID].ToString();
                if (File.Exists(outFile))
                    File.Delete(outFile);
                Session[OUT_FILE + exportID] = "";
                Session[FILE_EXISTS + exportID] = "";
            }

            BeginExportPage(exportID);

        }
        else if (isProgressCheck)
        {
            // this is a progress check, are we done yet.
            //log.Info("ExportToPDf: ProcessRequest() checking export status - ID:" + exportID);
            //log.Info("Session[EXPORT_COMPLETE + exportID] = " + Session[EXPORT_COMPLETE + exportID]);
            if ((string)Session[EXPORT_COMPLETE + exportID] == "1")
            {
                //log.Info("Session[OUT_FILE + exportID]:" + Session[OUT_FILE + exportID]);
                string filename = (string)Session[OUT_FILE + exportID];

                if (File.Exists(filename) && new FileInfo(filename).Length > 0)
                {
                    log.Info("ExportToPDf: ProcessRequest() export is complete.");
                    Response.Write("DONE");
                }
                else
                {
                    log.Info("ExportToPDf: ProcessRequest() export is complete - but file was not created.");
                    Response.Write("ERROR" + Session[EXPORT_ERROR + exportID]);
                }
                Response.End();
            }
            else if ((string)Session[EXPORT_PENDING + exportID] == "1")
            {
                log.Info("ExportToPDf: ProcessRequest() export not complete.");
                Response.Write("NOTYET");
                Response.End();
            }
            else
            {
                log.Info("ExportToPDf: ProcessRequest() export not started.");
                Response.Write("NOOPERATIONS");
                Response.End();
            }
        }
        else if (isFileRequest)
        {
            log.Info("ExportToPDf: ProcessRequest() Sending file back");
            SendFile(exportID);
            Response.End();
        }
        else if (isDeleteRequest)
        {
            log.Info("ExportToPDf: ProcessRequest() Deleting file");
            string file = Session[OUT_FILE + exportID].ToString();
            if ((string)Session[FILE_EXISTS + exportID] == "1" && File.Exists(file))
                File.Delete(file);

            Session[FILE_EXISTS + exportID] = "";
        }
    }

    /// <summary>
    /// Reads the outfile session var, then loads the file in a byte array and sends it as an attachment.
    /// </summary>
    internal void SendFile(string exportID)
    {
        string outFile = Session[OUT_FILE + exportID].ToString();
        string errorMessage = Session[EXPORT_ERROR + exportID].ToString();
        string downloadFile = "OrionReport.PDF";

        try
        {
            //If there are no errors.
            if (string.IsNullOrEmpty(errorMessage))
            {
                //Make sure that the output file exists.
                if (!string.IsNullOrEmpty(outFile) && File.Exists(outFile))
                {
                    string docTitle = (string)Session[DOCUMENT_TITLE + exportID];
                    if (!string.IsNullOrEmpty(docTitle))
                        downloadFile = ExportFileHelper.MakeValidFileName(docTitle) + ".pdf";

                    //Send the PDF file as a response.
                    byte[] downloadBytes = File.ReadAllBytes(outFile);

                    if (downloadBytes.Length > 0)
                    {
                        //// send it back to the client.
                        // System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                        HttpResponse response = Response;

                        response.AddHeader("Content-Type", "binary/octet-stream");
                        response.AddHeader("Content-Disposition",
                            "attachment; filename=\"" + downloadFile + "\"; size=" + downloadBytes.Length.ToString(CultureInfo.InvariantCulture));
                        response.Flush();
                        response.BinaryWrite(downloadBytes);
                        response.Flush();
                    }
                }
                else
                {
                    log.ErrorFormat("Outfile {0} does not exist.", string.IsNullOrEmpty(outFile) ? "" : outFile);
                }
            }
            else
            {
                log.ErrorFormat("ExportPDF Worker thread reported error: {0}", errorMessage);
            }
        }
        catch (Exception ex)
        {
            log.Error("ExportToPdf::sendFile - Error: " + ex.ToString());

        }

    }
    
    private string GetUrlPrefix(NameValueCollection requestParams)
    {
        bool useSSL = requestParams["SERVER_PORT_SECURE"] != "0";
        string certIssuedTo = (useSSL) ? GetCertIssuedTo(requestParams) : null;
        string server = (useSSL) ? (string.IsNullOrEmpty(certIssuedTo)) ? requestParams["SERVER_NAME"] : certIssuedTo : _serverName;
        string port = (useSSL) ? requestParams["SERVER_PORT"] : _serverPort.ToString();

        string prefix = useSSL ? "https://" : "http://";
        prefix += server;
        if (port != "80" && port != "443")
            prefix += ":" + port;

        return prefix;
    }

    internal string ExtractURL(NameValueCollection requestParams)
    {
        if (!String.IsNullOrEmpty(requestParams["page"]))
        {
            bool useSSL = requestParams["SERVER_PORT_SECURE"] != "0";
            string temp = HttpUtility.UrlDecode(requestParams["page"]);

            // Fix for FB38876: "Export to PDF doesn't work behind firewall / NAT"
            // We need to replace hostname and port with real values in case Orion server is running behind firewall / NAT
            string certIssuedTo = (useSSL) ? GetCertIssuedTo(requestParams) : null;
            if (useSSL && string.IsNullOrEmpty(certIssuedTo))
            {
                // couldn't get cert issued value
                // use previous scenario for ssl
                return temp;
            }

            if (_serverSettingsAvailable /*&& !useSSL*/)
            {
                UriBuilder ub = new UriBuilder(temp);
                ub.Host = (useSSL) ? certIssuedTo : _serverName;
                ub.Port = _serverPort;

                return ub.ToString();
            }
            // Fix for FB38876

            return temp;
        }
        return requestParams["HTTP_REFERER"];  // this could be empty due to referer blocking
    }
    
    internal string ExtractExportID()
    {
        return Request["ExportID"];
    }
    
    internal bool ExtractProgressCheck(string queryString)
    {
        return CheckForParam(queryString, "progresscheck=");
    }
    internal bool ExtractGimmeTheFile(string queryString)
    {
        return CheckForParam(queryString, "gimmethefile=");
    }

    internal bool ExtractSendAuthSetting(string queryString)
    {
        return CheckForParam(queryString, "exporttopdfsendauth=true");
    }

    internal bool ExtractDeleteRequest(string queryString)
    {
        return CheckForParam(queryString, "deletethefile=");
    }

    internal bool CheckForParam(string queryString, string param)
    {
        return queryString.ToLower().Contains(param);
    }

    private class ThreadData
    {
        public HttpCookieCollection RequestCookies { get; set; }
        public Uri RequestUrl { get; set; }
        public NameValueCollection RequestParams { get; set; }
        public string ExportID { get; set; }
        public string CurrentUICultureName { get; set; }
    }

    internal void BeginExportPage(string exportID)
    {
        log.Info("BegineExportPage()");

        ThreadData threadData = new ThreadData();
        threadData.RequestCookies = Request.Cookies;
        threadData.RequestUrl = Request.Url;
        threadData.RequestParams = Request.Params;
        threadData.ExportID = exportID;
        threadData.CurrentUICultureName = Thread.CurrentThread.CurrentUICulture.ToString();

        Thread t = new Thread(new ParameterizedThreadStart(ExportPageInternal));
        t.IsBackground = true;
        t.Start(threadData);
    }
    
    private void SaveWebResponse(HttpWebResponse httpWebResponse, string tempImageFile)
    {
        if (httpWebResponse.ContentType.ToLower() == "image/gif")
        {
            //Check to see if this is an animated gif.
            var gif = System.Drawing.Image.FromStream(httpWebResponse.GetResponseStream());

            var fd = new FrameDimension(gif.FrameDimensionsList[0]);
            int frameCount = gif.GetFrameCount(fd);

            //A frame count greater than 1 indicates an animated gif.
            if (frameCount > 1)
            {
                log.Debug("Animated gif detected, saving first frame as " + tempImageFile);

                //select first GIF frame based on FrameDimension and frameIndex
                gif.SelectActiveFrame(fd, 0);

                var firstGifFrame = new Bitmap(gif);

                //If you try to save it as a GIF, a default ColorPalette will be saved and hence you'll loose the transparent color.
                firstGifFrame.Save(tempImageFile, ImageFormat.Png);

                return;
            }

            //Since the stream is already loaded to check if it was an animated GIF, save the image to the file.
            gif.Save(tempImageFile);
            return;
        }

        using (BinaryReader binaryReader = new BinaryReader(httpWebResponse.GetResponseStream()))
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(new FileStream(tempImageFile, FileMode.Create)))
            {
                int bufferSize = 16384; //16k
                while (true)
                {
                    byte[] buffer = binaryReader.ReadBytes(bufferSize);

                    binaryWriter.Write(buffer);

                    if (buffer.Length < bufferSize)
                        break;
                }

            }
        }
    }

    private void FillCookieContainerWithCurrentRequestCookies(ThreadData threadData, HttpWebRequest httpWebRequest)
    {
        HttpCookieCollection httpCookieCollection = threadData.RequestCookies;
        for (int index = 0; index < httpCookieCollection.Count; index++)
        {
            HttpCookie httpCookie = httpCookieCollection.Get(index);

            Cookie cookie = new Cookie();

            // Convert between the System.Net.Cookie to a System.Web.HttpCookie... 
            cookie.Domain = httpWebRequest.RequestUri.Host;
            cookie.Expires = httpCookie.Expires;
            cookie.Name = httpCookie.Name;
            cookie.Path = httpCookie.Path;
            cookie.Secure = httpCookie.Secure;
            cookie.Value = httpCookie.Value;

            try
            {
                httpWebRequest.CookieContainer.Add(cookie);
                log.InfoFormat("Added cookie with name {0} and path {1}", httpCookie.Name, httpCookie.Path);
            }
            catch (Exception exception)
            {
                log.DebugFormat("Unable to add cookie with name {0} and path {1}.  Exception: {2}", httpCookie.Name, httpCookie.Path, exception.Message);
            }
        }
    }

    /// <summary>
    /// Reads in the page URI, then strips off all the parameters and pagename
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private string GetApplicationRoot(string url)
    {
        log.Info("GetApplicationRoot: starting URL: " + url);
        UriBuilder ub = new UriBuilder(url);
        var path = ub.Scheme + "://" + ub.Host + ":" + ub.Port + ub.Path;
        int found = path.LastIndexOf("/");
        var ret = path.Substring(0, found + 1);
        log.Info("GetApplicationRoot result: " + ret);
        return ret;
    }

    internal void ExportPageInternal(object context)
    {
        var threadData = (ThreadData)context;

        Stopwatch entireExportStopWatch = new Stopwatch();
        entireExportStopWatch.Start();

        string outFile = Path.GetTempFileName();
        Session[OUT_FILE + threadData.ExportID] = outFile;

        try
        {
            log.Info("Convert from URL to PDF with Server.Execute() & temp images");

            // get the html string for the report
            string pageToDL = ExtractURL(threadData.RequestParams);
            string applicationRoot = GetApplicationRoot(pageToDL);

            // set the doc title for later retrieval.
            Session[DOCUMENT_TITLE + threadData.ExportID] = threadData.RequestParams["title"];
            
            string token = (pageToDL.Contains("?")) ? "&" : "?";
            if (!pageToDL.Contains("InlineRendering=TRUE"))
                pageToDL += token + "InlineRendering=TRUE";
            
            log.Info("ExportToPDF url: " + pageToDL);

            //Request the page and write it to the HTML writer
            //string htmlCodeToConvert = ServerExecuteForHtmlText(threadData, pageToDL);
            // Get the HTML sent up from the page:
            // string htmlCodeToConvert = (string)Session["pageHTML" + threadData.ExportID];
            string htmlCodeToConvert = threadData.RequestParams["pageHTML"];

            //For debugging, write to a temp file.
            //File.AppendAllText("c:\\windows\\temp\\BeforeTempImages.html", htmlCodeToConvert);

            if (string.IsNullOrEmpty(htmlCodeToConvert))
            {
                Session[EXPORT_ERROR + threadData.ExportID] = "Export to PDF failed because HTML was empty (no post)";
                log.Error("Setting FileExist == False - ID:" + threadData.ExportID);
                Session[FILE_EXISTS + threadData.ExportID] = "";
                log.Error("ExportToPDF failed because there was no posted HTML Data...");
                return;
            }

            //Keep a list of all the temp files that need to get cleaned up.
            var tempFiles = new List<string>();

            try
            {
                htmlCodeToConvert = InjectTempImagesIntoHtml(threadData, htmlCodeToConvert, tempFiles, applicationRoot);

                htmlCodeToConvert = InjectSvgImagesIntoHtml(threadData, htmlCodeToConvert, tempFiles, applicationRoot);

                htmlCodeToConvert = InjectDynamicCssIntoHtml(threadData, htmlCodeToConvert, tempFiles, applicationRoot);

                htmlCodeToConvert = InjectChartImagesIntoHtml(threadData, htmlCodeToConvert, tempFiles, applicationRoot);

                htmlCodeToConvert = RemoveScriptTagsFromHtml(threadData, htmlCodeToConvert, tempFiles, applicationRoot);

                if (applicationRoot.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
                {
                    htmlCodeToConvert = MakeFullOrionLink(htmlCodeToConvert, applicationRoot, "//a[@href]", "href");
                    htmlCodeToConvert = MakeFullOrionLink(htmlCodeToConvert, applicationRoot, "//img[@src]", "src");
                    var uri = new Uri(applicationRoot);
                    htmlCodeToConvert = MakeFullLinkBackgroundImage(htmlCodeToConvert, applicationRoot,
                        new Regex(@"url\(.*/Orion", RegexOptions.IgnoreCase),
                        new Regex("/Orion", RegexOptions.IgnoreCase),
                        string.Format("{0}://{1}:{2}/Orion", uri.Scheme, uri.Host, uri.Port));
                    htmlCodeToConvert = MakeFullLinkBackgroundImage(htmlCodeToConvert, applicationRoot,
                        new Regex(@"url\(.*/netperfmon", RegexOptions.IgnoreCase),
                        new Regex("/netperfmon", RegexOptions.IgnoreCase),
                        string.Format("{0}://{1}:{2}/netperfmon", uri.Scheme, uri.Host, uri.Port));
                }

                //For debugging, write to a temp file.
                //File.WriteAllText(@"C:\ProgramData\Solarwinds\Logs\Orion\AfterTempImages.html", htmlCodeToConvert);
                //log.Info("Extracted new modified html to c:\\windows\\temp\\AfterTempImages.html");
                log.Info("Extracted new modified html");

                ConvertHtmlIntoPdfFile(threadData, outFile, htmlCodeToConvert, applicationRoot);
            }
            finally
            {
                log.Info("Cleaning up the temp files.");

                Stopwatch cleanUpTempFilesStopWatch = new Stopwatch();
                cleanUpTempFilesStopWatch.Start();

                //Clean up the temp images.
                tempFiles.ForEach(File.Delete);

                cleanUpTempFilesStopWatch.Stop();
                log.InfoFormat("Cleaning up temp files took {0} seconds to complete.", (cleanUpTempFilesStopWatch.ElapsedMilliseconds / (double)1000).ToString("N3"));
            }

            //Indicate success to the main thread.
            Session[EXPORT_ERROR + threadData.ExportID] = "";
            Session[FILE_EXISTS + threadData.ExportID] = "1";


            log.Info("Successfully converted page into a PDF");
        }
        catch (Exception exception)
        {
            log.Error("Export to PDF failed.", exception);

            if (Session[EXPORT_ERROR + threadData.ExportID] == null)
                Session[EXPORT_ERROR + threadData.ExportID] = "Export to PDF failed. Err: " + exception.Message;
        }
        finally
        {
            entireExportStopWatch.Stop();
            log.InfoFormat("Worker thread for PDF export took {0} seconds to complete.", (entireExportStopWatch.ElapsedMilliseconds / (double)1000).ToString("N3"));

            Session[EXPORT_COMPLETE + threadData.ExportID] = "1";
        }
    }

    private string MakeFullLinkBackgroundImage(string htmlCodeToConvert, string applicationRoot, Regex regexUrl,
        Regex regexHost, string host)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);
       
        //Find all the <img> tags with sources that are aspx pages.
        var divs = doc.DocumentNode.SelectNodes("//div[(contains(@style, 'background-image'))]");
        string attributeName = "style";

        if (divs != null)
        {
            foreach (HtmlNode div in divs)
            {
                HtmlAttribute attr = div.Attributes[attributeName];
                if (attr != null)
                {
                    string foundString = regexUrl.Match(attr.Value).Value;
                    if (!string.IsNullOrEmpty(foundString))
                    {
                        attr.Value = attr.Value.Replace(foundString, regexHost.Replace(foundString, host));
                    }
                }
            }
        }

        htmlCodeToConvert = DocToHtml(doc);
        log.Info("MakeFullLinkBackgroundImage - returning");

        return htmlCodeToConvert;
    }

    private string DocToHtml(HtmlDocument doc)
    {
        string htmlCodeToConvert;
        //Save the new modified html that contains the temp images instead of aspx links
        using (StringWriter htmlStringWriter = new StringWriter())
        {
            doc.Save(htmlStringWriter);

            htmlCodeToConvert = htmlStringWriter.GetStringBuilder().ToString();
            htmlStringWriter.Close();
        }
        return htmlCodeToConvert;
    }

    private string MakeFullOrionLink(string htmlCodeToConvert, string applicationRoot, string xpath, string attributeName)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);
        var uri = new Uri(applicationRoot);
        var host = string.Format("{0}://{1}:{2}", uri.Scheme, uri.Host, uri.Port);

        var links = doc.DocumentNode.SelectNodes(xpath);
        if (links != null)
        {
            foreach (HtmlNode link in links)
            {
                HtmlAttribute attr = link.Attributes[attributeName];
                if (attr != null)
                {
                    if (attr.Value.StartsWith("/Orion", StringComparison.InvariantCultureIgnoreCase))
                    {
                        attr.Value = host + attr.Value;
                    }
                    else if (attr.Value.StartsWith("#"))
                    {
                        attr.Value = host + "/" + attr.Value;
                    }
                }
            }

            log.InfoFormat("MakeFullOrionLink - returning; xpath: {0}; attributeName: {1}", xpath, attributeName);
        }

        htmlCodeToConvert = DocToHtml(doc);
        return htmlCodeToConvert;
    }
    
    private void ConvertHtmlIntoPdfFile(ThreadData threadData, string outFile, string htmlCodeToConvert, string applicationRoot)
    {
        try
        {
            // get the base url for string conversion which is the url from where the html code was retrieved
            // the base url is a hint for the converter to find the external CSS and images referenced by relative URLs
            var baseUrl = ResolveUrl(threadData.RequestParams, null, applicationRoot);
            log.Info("baseUrl: " + baseUrl);

            Runtime.AddLicense(
                "pPOxD+Ct3MGz465vprbB3Lhbl/cGDcSx5+0DEPJ668Gz465vprbB3a9bl/r2" +
                "HfKi5vLOzbBymaTA6YxDl6Sxy7us4Ov/DPOu6enP7+WS39b62veCua/dHcVv" +
                "6NoL1vKFuMHO566s4Ov/DPOu6enPuIl1pvD6DuSn6unPuIl14+30EO2s3MKe" +
                "tZ9Zl6TNF+ic3PIEEMidtbjC4LBrrr3E5Lhxs7P9FOKe5ff29ON3hI6xy59Z" +
                "s/D6DuSn6un26bto4+30EO2s3OnPuIlZl6Sx5+Cl4/MI6YxDl6Sxy59Zl6TN" +
                "DOOdl/gKG+R2mcng2c+d3aaxIeSr6u0AGbxbqL2/26FZpsKetZ9Zl6TN2uCl" +
                "4/MI6YxDl6Sxy7uo6ej2Hcin3Q==");

            Stopwatch convertPdfStopWatch = new Stopwatch();
            convertPdfStopWatch.Start();
            
            HtmlToPdf.Options.BaseUrl = applicationRoot.StartsWith("https", StringComparison.InvariantCultureIgnoreCase) ? string.Empty : baseUrl;
            HtmlToPdf.Options.MaxLoadWaitTime = (int)TimeSpan.FromSeconds(180).TotalMilliseconds;
            HtmlToPdf.Options.AdditionalHeaders = new string[] { "Cache-Control: no-cache, no-store" };
            HtmlToPdf.Options.PageSize = PdfPageSizes.A4;
            HtmlToPdf.Options.OutputArea = new RectangleF(new PointF(0, 0), HtmlToPdf.Options.PageSize);
            HtmlToPdf.ConvertHtml(htmlCodeToConvert, outFile);

            convertPdfStopWatch.Stop();
            log.InfoFormat("Converting to PDF took {0} seconds to complete.", (convertPdfStopWatch.ElapsedMilliseconds / (double)1000).ToString("N3"));
        }
        catch (Exception exception)
        {
            log.Error("Unable to convert html into PDF file.", exception);
            Session[EXPORT_ERROR + threadData.ExportID] = "Unable to convert html into PDF file. Err: " + exception.Message;
        }
    }

    private string InjectDynamicCssIntoHtml(ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {

        //Need to resolve OrionCSS.ashx:
        //<link rel="stylesheet" type="text/css" href="/Orion/styles/OrionCSS.ashx?path=/Orion/styles/Resources.css" />
        //To temp css file:
        //<link rel="stylesheet" type="text/css" href="C:/windows/temp/tempxxx.css" />

        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);

        Dictionary<string, string> cache = new Dictionary<string, string>();

        //Find all the <img> tags with sources that are aspx pages.
        var links = doc.DocumentNode.SelectNodes("//link[@href]");
        
        if (links != null)
        {
            foreach (HtmlNode link in links)
            {
                HtmlAttribute att = link.Attributes["href"];

                //Just the Dynamic CSS stuff (.ashx)
                if (att.Value.Contains(".ashx?"))
                {

                    string urlToCss = ResolveUrl(threadData.RequestParams, att.Value, applicationRoot);
                    log.Info("Resolve URL smashing  " + att.Value + " ApplicationRoot: " + applicationRoot);
                    log.Info("Resolving  " + urlToCss);

                    //Execute the page to get the CSS.
                    //We need to give this sessions cookies to the web request, so that its authenticated.
                    HttpWebRequest httpWebRequest = GetHttpWebRequestWithServerContext(urlToCss, threadData);

                    string tempCssFile;
                    bool inCache = cache.ContainsKey(urlToCss.ToLower());
                    if (inCache)
                    {
                        log.Info(urlToCss + " was in the cache.");
                        tempCssFile = cache[urlToCss.ToLower()];
                    }
                    else
                    {
                        //Store the CSS locally, so we need a place to put it.
                        log.Info(urlToCss + " was NOT in the cache.");
                        tempCssFile = Path.GetTempFileName();
                        tempFiles.Add(tempCssFile);
                    }

                    if (!inCache)
                    {
                        log.Info("Temp CSS file: " + tempCssFile);
                        try
                        {
                            using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                            {

                                log.Info("GetResponse() returned.");


                                if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                                {
                                    //So, the CSS not found, but don't fail the whole conversion  (i.e. continue on)
                                    log.ErrorFormat("The web request to {0} returned with a response StatusCode = {1}",
                                                    urlToCss,
                                                    httpWebResponse.StatusCode);
                                    continue;
                                }

                                log.Info("Response ContentType: " + httpWebResponse.ContentType);

                                //Store the result into the file.
                                SaveWebResponse(httpWebResponse, tempCssFile);
                                string newApplicationRoot = applicationRoot;
                                int index = urlToCss.LastIndexOf("/");
                                if (index > 0)
                                {
                                    newApplicationRoot = urlToCss.Substring(0, index + 1);
                                }

                                InjectTempImagesIntoCss(threadData, tempCssFile, tempFiles, applicationRoot, newApplicationRoot);
                                cache.Add(urlToCss.ToLower(), tempCssFile);
                            }
                        }
                        catch (Exception ex)
                        {
                            //So, the CSS not found, but don't fail the whole conversion  (i.e. continue on)
                            log.ErrorFormat("The web request to {0} returned with an exception {1}", urlToCss,
                                            ex.Message);
                            continue;
                        }
                    }

                    att.Value = tempCssFile;
                }
            }
        }

        log.Debug("Finishing resolving all dynamic css");
        htmlCodeToConvert = DocToHtml(doc);
        log.Info("InjectDynamicCssIntoHtml - returning");

        return htmlCodeToConvert;
    }

    private void InjectTempImagesIntoCss(ThreadData threadData, string tempCssFile, List<string> tempFiles, string applicationRoot, string newApplicationRoot)
    {
        var cssData = string.Empty;
        bool isChanges = false;
        Dictionary<string, string> cache = new Dictionary<string, string>();
        Regex imageUrlPattern =
            new Regex(
                @"\*(?>[^*]|\*(?!/))*(?>\*/|$)|(?<=background(?>-image\s*:|\s*:(?>\s*[^;{}u\s]+\s)?)\s*url\s*\(\s*([']?))(?<Url>[^""')\s]*)(?=\1\s*\))",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

        using (StreamReader sr = new StreamReader(tempCssFile))
        {
            cssData = sr.ReadToEnd();
        }
       
        foreach (Match m in imageUrlPattern.Matches(cssData))
        {
            var url = m.Groups["Url"].Value;
            if (string.IsNullOrEmpty(url) || url.StartsWith("data:", StringComparison.OrdinalIgnoreCase)) continue;
            isChanges = true;
            bool usedApplicationRoot = true;
            string urlToGif = string.Empty;
            if (url.Contains(".."))
            {
                UriBuilder ub = new UriBuilder(newApplicationRoot);
                string tmpPath = ub.Path;
                if (!string.IsNullOrEmpty(tmpPath))
                {
                    string[] paths = tmpPath.Split('/');
                    if (paths.Length > 0)
                    {
                        if (paths.Length > 1)
                        {
                            paths = paths.Take(paths.Length - 2).ToArray();
                            tmpPath = string.Join("/", paths);
                            tmpPath += "/";
                        }
                    }
                }

                url = url.Replace("../", tmpPath);
                urlToGif = ResolveUrl(threadData.RequestParams, url, newApplicationRoot);
                usedApplicationRoot = false;
            }
            else
            {
                urlToGif = ResolveUrl(threadData.RequestParams, url, applicationRoot);
            }
            
            string tempImageFile;
            bool inCache = cache.ContainsKey(urlToGif.ToLower());
            if (inCache)
            {
                log.Info(urlToGif + " was in the cache.");
                tempImageFile = cache[urlToGif.ToLower()];
            }
            else
            {
                //Store the image locally, so we need a place to put it.
                log.Info(urlToGif + " was NOT in the cache.");
                tempImageFile = Path.GetTempFileName();
                tempFiles.Add(tempImageFile);
            }

            Func<bool> downloadFile = () =>
            {
                HttpWebRequest httpWebRequest = GetHttpWebRequestWithServerContext(urlToGif, threadData);
                using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {

                    log.Info("GetResponse() returned.");


                    if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        //So, the chart, gauge or Network Atlas map map not show on the PDF, but don't fail the whole conversion  (i.e. continue on)
                        log.ErrorFormat("The web request to {0} returned with a response StatusCode = {1}",
                                        urlToGif,
                                        httpWebResponse.StatusCode);
                        return false;
                    }

                    log.Info("Response ContentType: " + httpWebResponse.ContentType);

                    //Store the result into the file.
                    SaveWebResponse(httpWebResponse, tempImageFile);
                    cache.Add(urlToGif.ToLower(), tempImageFile);
                }

                return true;
            };

            if (!inCache)
            {
                log.Info("Temp image file: " + tempImageFile);
                try
                {
                    if (!downloadFile())
                        continue;
                }
                catch (WebException ex)
                {
                    if (!usedApplicationRoot)
                    {
                        log.DebugFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                    ex.Message);

                        if (string.Compare(applicationRoot, newApplicationRoot, StringComparison.OrdinalIgnoreCase) != 0)
                        {
                            urlToGif = ResolveUrl(threadData.RequestParams, url, applicationRoot);
                            try
                            {
                                if (!downloadFile())
                                    continue;
                            }
                            catch (Exception ex2)
                            {
                                log.DebugFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                    ex2.Message);
                            }
                        }
                    }
                    else
                    {
                        log.DebugFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                    ex.Message);

                        if (string.Compare(applicationRoot, newApplicationRoot, StringComparison.OrdinalIgnoreCase) != 0)
                        {
                            if (url.Contains(".."))
                            {
                                UriBuilder ub = new UriBuilder(applicationRoot);
                                url = url.Replace("../", ub.Path);
                            }

                            urlToGif = ResolveUrl(threadData.RequestParams, url, newApplicationRoot);
                            try
                            {
                                if (!downloadFile())
                                    continue;
                            }
                            catch (Exception ex2)
                            {
                                log.DebugFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                    ex2.Message);
                            }
                        }
                    }

                    continue;                    
                }
                catch (Exception ex)
                {
                    //So, the chart, gauge or Network Atlas map map not show on the PDF, but don't fail the whole conversion  (i.e. continue on)
                    if (!urlToGif.Contains("/data:image/"))
                        log.ErrorFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                    ex.Message);
                    continue;
                }
            }
            cssData = cssData.Replace(m.Groups["Url"].Value, tempImageFile.Replace(@"\", @"\\"));
        }
        if (isChanges)
        {
            using (StreamWriter writer = new StreamWriter(tempCssFile, false))
            {
                writer.Write(cssData);
            }
        }
    }

    private string InjectTempImagesIntoHtml(ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {
        return InjectImagesIntoHtml("//img[@src]", "src", threadData, htmlCodeToConvert, tempFiles, applicationRoot);
    }

    private string InjectSvgImagesIntoHtml(ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {
        return InjectImagesIntoHtml("//*[local-name()='image']", "xlink:href", threadData, htmlCodeToConvert, tempFiles, applicationRoot);
    }

    private string InjectImagesIntoHtml(string selector, string attributeName, ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);

        //Find all the <img> tags with sources that are aspx pages.
        var links = doc.DocumentNode.SelectNodes(selector);

        if (links != null)
        {
            foreach (HtmlNode link in links)
            {
                HtmlAttribute att = link.Attributes[attributeName];
                if (att != null)
                {
                    ResolveImageSource(att, threadData, tempFiles, applicationRoot);
                }
            }
        }

        log.Debug("Finishing resolving all images");
        htmlCodeToConvert = DocToHtml(doc);
        log.Info("InjectImagesIntoHtml - returning");

        return htmlCodeToConvert;
    }

    private void ResolveImageSource(HtmlAttribute attribute, ThreadData threadData, List<string> tempFiles, string applicationRoot)
    {
        string urlToGif = ResolveUrl(threadData.RequestParams, attribute.Value, applicationRoot);
        log.Info("Resolve URL smashing  " + attribute.Value + " ApplicationRoot: " + applicationRoot);
        log.Info("Resolving  " + urlToGif);

        if (urlToGif.EndsWith("SolarWinds.Logo.Footer.svg", StringComparison.OrdinalIgnoreCase))
            urlToGif = urlToGif.Replace("SolarWinds.Logo.Footer.svg", "SolarWinds.Logo.FooterPdf.png");

        //Execute the page to get the image.
        //We need to give this sessions cookies to the web request, so that its authenticated.
        HttpWebRequest httpWebRequest = GetHttpWebRequestWithServerContext(urlToGif, threadData);

        string tempImageFile = string.Empty;
        bool inCache = imageCache.ContainsKey(urlToGif.ToLower());
        if (inCache)
        {
            log.Info(urlToGif + " was in the cache.");
            tempImageFile = imageCache[urlToGif.ToLower()];
            attribute.Value = tempImageFile;
        }
        else
        {
            //Store the image locally, so we need a place to put it.
            log.Info(urlToGif + " was NOT in the cache.");
            tempImageFile = Path.GetTempFileName();
            if (urlToGif.EndsWith(".svg", StringComparison.OrdinalIgnoreCase))
                tempImageFile += ".svg";

            tempFiles.Add(tempImageFile);
        }
        
        if (!inCache)
        {
            log.Info("Temp image file: " + tempImageFile);
            try
            {
                using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    log.Info("GetResponse() returned.");
                    
                    if (httpWebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        //So, the chart, gauge or Network Atlas map map not show on the PDF, but don't fail the whole conversion  (i.e. continue on)
                        log.ErrorFormat("The web request to {0} returned with a response StatusCode = {1}",
                                        urlToGif,
                                        httpWebResponse.StatusCode);
                        return;
                    }

                    log.Info("Response ContentType: " + httpWebResponse.ContentType);

                    //Store the result into the file.
                    SaveWebResponse(httpWebResponse, tempImageFile);
                    imageCache.Add(urlToGif.ToLower(), tempImageFile);
                    attribute.Value = tempImageFile;
                }
            }
            catch (Exception ex)
            {
                //So, the chart, gauge or Network Atlas map map not show on the PDF, but don't fail the whole conversion  (i.e. continue on)
                log.ErrorFormat("The web request to {0} returned with an exception {1}", urlToGif,
                                ex.Message);
                attribute.Value = urlToGif;
            }
        }
    }

    private string InjectChartImagesIntoHtml(ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);

        //Find all the <div> tags that contain charts.
        var chartSvgDivs = doc.DocumentNode.SelectNodes("//div[@class='hasChart']");

        if (chartSvgDivs != null)
        {
            foreach (HtmlNode div in chartSvgDivs)
            {
                var divChartContainer = div.SelectSingleNode("div[contains(@class,'highcharts-container')]");
                if (divChartContainer != null)
                {
                    divChartContainer.Remove();
                }

                var divSvgContainer = div.SelectSingleNode("div[contains(@class,'chartSvg')]");
                if (divSvgContainer != null)
                {
                    // make sure the div is not hidden.
                    divSvgContainer.Attributes["style"].Value = "";

                    var scriptNode = divSvgContainer.Element("script");
                    var svg = (scriptNode != null) ? scriptNode.WriteContentTo() : String.Empty;

                    // According to HighCharts there is a bug in Chrome that causes it to treat a stroke width
                    // of 0 as a single pixel wide line.  HighCharts uses 0.000001 as stroke width instead of 
                    // 0 to handle this.  The SVG could have been generated by a browser other than Chrome so 
                    // it wouldn't have this fix.  However, we will be generating the image using Chrome so we
                    // need to "reapply" this fix.
                    svg = svg.Replace("stroke-width=\"0\"", "stroke-width=\"0.000001\"");
                    
                    divSvgContainer.InnerHtml = svg;
                }
            }
        }

        log.Debug("Finishing resolving all charts to svg");
        htmlCodeToConvert = DocToHtml(doc);
        log.Info("InjectChartImagesIntoHtml - returning");

        return htmlCodeToConvert;
    }

    private string RemoveScriptTagsFromHtml(ThreadData threadData, string htmlCodeToConvert, List<string> tempFiles, string applicationRoot)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlCodeToConvert);

        //Find all the <script> tags and remove them
        var scriptTags = doc.DocumentNode.SelectNodes("//script");

        if (scriptTags != null)
        {
            foreach (HtmlNode tag in scriptTags)
            {
                tag.Remove();
            }
        }

        log.Debug("Finishing removing all script tags");
        htmlCodeToConvert = DocToHtml(doc);
        log.Info("RemoveScriptTagsFromHtml - returning");

        return htmlCodeToConvert;
    }
        
    private string ResolveUrl(NameValueCollection requestParams, string url, string applicationRoot)
    {
        const string pathSeparator = "/";
        string urlPrefix = GetUrlPrefix(requestParams);

        if (string.IsNullOrEmpty(url))
            return applicationRoot;

        //First determine if this is already an absolute path.
        if (url.StartsWith("https://") || url.StartsWith("http://"))
            return url;

        // starts with a /, meaning go to the root.
        if (url.StartsWith(pathSeparator))
            return urlPrefix + url;

        // otherwise, return the application root.
        return applicationRoot + url;
    }

    private string ServerExecuteForHtmlText(ThreadData threadData, string httpAddress)
    {
        HttpWebRequest httpWebRequest = GetHttpWebRequestWithServerContext(httpAddress, threadData);

        using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
        {
            log.Info("Main GetResponse() returned.");


            if (httpWebResponse.StatusCode != HttpStatusCode.OK)
            {
                log.ErrorFormat("The web request to {0} returned with a response StatusCode = {1}", httpAddress,
                                httpWebResponse.StatusCode);
                throw new HttpException((int)httpWebResponse.StatusCode,
                                        "Status code returned in web request for " + httpAddress);
            }

            using (Stream stream = httpWebResponse.GetResponseStream())
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }

    private HttpWebRequest GetHttpWebRequestWithServerContext(string httpAddress, ThreadData threadData)
    {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(httpAddress.Replace("&amp;", "&"));

        // setting correct header for running locale
        if (!string.IsNullOrEmpty(threadData.CurrentUICultureName))
        {
            httpWebRequest.Headers["Accept-Language"] = threadData.CurrentUICultureName;
        }
        httpWebRequest.CookieContainer = new CookieContainer();
        httpWebRequest.Timeout = 50000000;
        httpWebRequest.ReadWriteTimeout = 50000000;
        FillCookieContainerWithCurrentRequestCookies(threadData, httpWebRequest);

        log.Info("All cookies for this request have been added: " + httpAddress);
        return httpWebRequest;
    }
}

