﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopologyPolling.ascx.cs" Inherits="Orion_Topology_Controls_TopologyPolling" %>

<div class="contentBlock" runat="server" id="TopologyPollingContent">
    <div runat="server" id="TopologyPollingHeader">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="contentBlockHeader">
                    <asp:Literal runat="server" Text="<%$ Resources: TopologyWebContent,WEBDATA_YK0_01 %>" />
                </td>
            </tr>
        </table>
    </div>
    <table class="blueBox">
        <tr runat="server" id="TopologyLocation">
            <td class="leftLabelColumn" style="vertical-align: text-top;">
                <asp:CheckBox runat="server" ID="ApplyTopologyPollInterval" AutoPostBack="true" OnCheckedChanged="ApplyTopologyPollInterval_CheckedChanged" />

                <asp:Literal runat="server" ID="LabelTopologyPollInterval" Text="<%$ Resources: TopologyWebContent,WEBDATA_YK0_02 %>"/>
            </td>
            <td class="rightInputColumn">
                <asp:CheckBox runat="server" ID="OverrideTopologyPollInterval" AutoPostBack="true" OnCheckedChanged="OverrideTopologyPollInterval_CheckedChanged"/>

                <asp:Literal runat="server" ID="Literal1" Text="<%$ Resources: TopologyWebContent,WEBDATA_YK0_03 %>"/>
                
                <div runat="server" ID="SettingsTopologyPollInterval">
                    <asp:TextBox runat="server" ID="TopologyPollInterval"/>

                    <asp:RequiredFieldValidator runat="server" SetFocusOnError="True"
                        ID="RequiredValidatorTopologyPollInterval"
                        ControlToValidate="TopologyPollInterval"
                        ErrorMessage="*"/>
                    <asp:RegularExpressionValidator runat="server" SetFocusOnError="True"
                        ID="RegexValidatorTopologyPollInterval"
                        ControlToValidate="TopologyPollInterval"
                        ErrorMessage="*"
                        ValidationExpression="^\d+$"/>
                    <asp:RangeValidator runat="server" Display="Dynamic" SetFocusOnError="True" Type="Integer"
                        ID="RangeValidatorTopologyPollInterval"
                        ControlToValidate="TopologyPollInterval"/>

		            <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_MZ0_3 %>" />
                </div>
            </td>
        </tr>
    </table>
</div>
