﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Topology.Common;

public partial class Orion_Topology_Controls_TopologyPolling : UserControl, INodePropertyPlugin
{
    protected class SimplePropertyControl
    {
        public HtmlContainerControl ContainerControl { get; set; }
        public string SettingId { get; set; }
        public string NodeSettingId { get; set; }
        public CheckBox ApplyToAllNodesControl { get; set; }
        public CheckBox OverrideValueControl { get; set; }
        public TextBox InputControl { get; set; }
        public RangeValidator RangeValidator { get; set; }
        public List<int> SupportedNodeIds { get; set; }

        public bool IsSupported
        {
            get { return SupportedNodeIds.Any(); }
        }

        public SimplePropertyControl()
        {
            SupportedNodeIds = new List<int>();
        }
    }

    private IList<Node> editedNodes;
    private SimplePropertyControl propertyControl;

    protected SimplePropertyControl PropertyControl
    {
        get { return propertyControl ?? (propertyControl = GetPropertyControl(editedNodes)); }
    }

    public bool IsMultiselected
    {
        get { return editedNodes != null && editedNodes.Count > 1; }
    }

    protected bool IsSupported
    {
        get { return PropertyControl.IsSupported; }
    }

    protected SimplePropertyControl GetPropertyControl(IList<Node> editedNodes)
    {
        return new SimplePropertyControl
        {
            SettingId = Strings.GlobalPollingSettingName,
            NodeSettingId = Strings.NodePollingSettingName,
            ContainerControl = TopologyLocation,
            ApplyToAllNodesControl = ApplyTopologyPollInterval,
            OverrideValueControl = OverrideTopologyPollInterval,
            InputControl = TopologyPollInterval,
            RangeValidator = RangeValidatorTopologyPollInterval,
            SupportedNodeIds = editedNodes.Where(n => n.NodeSubType == NodeSubType.SNMP).Select(n => n.Id).ToList()
        };
    }

    private IDictionary<string, Tuple<int, int>> GetRangesForProperties(SimplePropertyControl propertyControl)
    {
        var result = new Dictionary<string, Tuple<int, int>>();
        var settingId = propertyControl.SettingId;

        if (string.IsNullOrEmpty(settingId))
        {
            return result;
        }

        using (var proxy = InformationServiceProxy.CreateV3())
        {
            var query = string.Format("SELECT SettingID, Minimum, Maximum FROM Orion.Settings WHERE SettingID = '{0}'", settingId);
            var queryResult = proxy.Query(query);

            if (queryResult != null && queryResult.Rows.Count > 0)
            {
                result = queryResult.Rows.Cast<DataRow>().ToDictionary(k => k[0].ToString(), k =>
                        {
                            var min = Convert.ToInt32(k[1]);
                            var max = Convert.ToInt32(k[2]);
                            return new Tuple<int, int>(min, max);
                        }
                    );
            }

            return result;
        }
    }

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        editedNodes = nodes;
        PropertyControl.SupportedNodeIds = editedNodes.Where(n => n.NodeSubType == NodeSubType.SNMP).Select(n => n.Id).ToList();
        TopologyPollingContent.Visible = IsSupported;

        if (TopologyPollingContent.Visible && IsPostBack && string.IsNullOrEmpty(PropertyControl.InputControl.Text))
        {
            //Polling method is changed
            //load controls
            LoadControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        TopologyPollingContent.Visible = IsSupported;
        if (!TopologyPollingContent.Visible || IsPostBack)
        {
            return;
        }

        LoadControls();
    }

    public bool Update()
    {
        if (!IsSupported)
        {
            return true;
        }

        if (!IsMultiselected || PropertyControl.ApplyToAllNodesControl.Checked)
        {
            foreach (var nodeId in PropertyControl.SupportedNodeIds)
            {
                if (PropertyControl.OverrideValueControl.Checked)
                {
                    NodeSettingsDAL.SafeInsertNodeSetting(nodeId, PropertyControl.NodeSettingId, PropertyControl.InputControl.Text);
                }
                else
                {
                    NodeSettingsDAL.DeleteSpecificSettingForNode(nodeId, PropertyControl.NodeSettingId);
                }
            }
        }

        return true;
    }

    public bool Validate()
    {
        return true;
    }

    protected void ApplyTopologyPollInterval_CheckedChanged(object sender, EventArgs e)
    {
        if (!IsMultiselected)
            return;

        ICheckBoxControl applyPropertyCheckBox = sender as ICheckBoxControl;
        if (applyPropertyCheckBox == null)
            return;

        if (PropertyControl.ApplyToAllNodesControl.Equals(applyPropertyCheckBox))
        {
            PropertyControl.OverrideValueControl.Enabled = applyPropertyCheckBox.Checked;
            PropertyControl.InputControl.Enabled = applyPropertyCheckBox.Checked && PropertyControl.OverrideValueControl.Checked;
        }
    }

    protected void OverrideTopologyPollInterval_CheckedChanged(object sender, EventArgs e)
    {
        ICheckBoxControl overridePropertyCheckBox = sender as ICheckBoxControl;
        if (overridePropertyCheckBox == null)
            return;

        if (PropertyControl.OverrideValueControl.Equals(overridePropertyCheckBox))
        {
            PropertyControl.InputControl.Enabled = overridePropertyCheckBox.Checked;
            PropertyControl.InputControl.Parent.Visible = overridePropertyCheckBox.Checked;
        }
    }

    private void LoadControls()
    {
        var propertyRanges = GetRangesForProperties(PropertyControl);

        var firstNodeId = PropertyControl.SupportedNodeIds.First();
        var overridenValue = NodeSettingsDAL.GetLastNodeSettings(firstNodeId, PropertyControl.NodeSettingId);

        PropertyControl.ApplyToAllNodesControl.Visible = IsMultiselected;
        PropertyControl.OverrideValueControl.Enabled = !IsMultiselected;
        PropertyControl.OverrideValueControl.Checked = !IsMultiselected && !string.IsNullOrEmpty(overridenValue);
        PropertyControl.InputControl.Parent.Visible = PropertyControl.OverrideValueControl.Checked;
        PropertyControl.InputControl.Enabled = PropertyControl.OverrideValueControl.Checked;
        if (IsMultiselected || string.IsNullOrEmpty(overridenValue))
        {
            PropertyControl.InputControl.Text = SettingsDAL.GetCurrentInt(PropertyControl.SettingId, 0).ToString();
        }
        else
        {
            PropertyControl.InputControl.Text = overridenValue;
        }

        if (PropertyControl.RangeValidator != null)
        {
            var min = propertyRanges[PropertyControl.SettingId].Item1;
            var max = propertyRanges[PropertyControl.SettingId].Item2;
            PropertyControl.RangeValidator.MinimumValue = min.ToString();
            PropertyControl.RangeValidator.MaximumValue = max.ToString();
            PropertyControl.RangeValidator.ErrorMessage = string.Format("({0}-{1})", min, max);
        }
    }
}