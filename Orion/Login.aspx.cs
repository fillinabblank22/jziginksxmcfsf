using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Services;
using SolarWinds.Orion.Web.UI;
using WebSettingsDAL = SolarWinds.Orion.Web.DAL.WebSettingsDAL;
using System.Security.Cryptography;
using System.Text;
using SolarWinds.Orion.Web.Model;
using SolarWinds.Orion.Web.Settings;
using SolarWinds.Orion.Web.Session;

public partial class Login : System.Web.UI.Page
{
    private static readonly SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected const string TestCookie_Name = "TestCookieSupport";
    protected const string XsrfCookie_Name = "XSRF-TOKEN";

    protected bool autoLogin;
    protected bool isSessionTimedOut;
	bool? isSolarWindsLogo;

    public bool FistTimeLogin
    {
        get
        {
            try
            {
                return (Membership.GetUser("Admin") != null
                        && WebSettingsDAL.GetValue("AdminPasswordEnforcement", false));
            }
            catch (Exception ex)
            {
                log.Error("Error when trying to check Fist Time Login: ", ex);
                return false;
            }
        }
    }

    public bool ShowErrorMessage
    {
        get
        {
            return Session["LoginErrorMessage"] is LoginErrorMessage;
        }
    }

    public bool IsSolarWindsLogo
    {
        get
        {
            if (isSolarWindsLogo.HasValue == false)
            {
                string logo = WebSettingsDAL.SiteLogo;
                isSolarWindsLogo = new[] { "logo.navbar.", "solarwinds.logo.", "navbar.logo." }.Any(x => logo.IndexOf(x, StringComparison.OrdinalIgnoreCase) >= 0);
            }

            return isSolarWindsLogo.Value;
        }
    }

    protected string SiteLogoUri
    {
        get
        {
            if (IsSolarWindsLogo)
                return "/orion/images/Login/SolarWinds-Logo.svg";

            string logo;
            if (WebSettingsDAL.TryGetLogo(out logo))
            {
                return "/Orion/LogoImageHandler.ashx?f=logo&id=SitelogoImage&time=" + DateTime.Now.Ticks;
            }
            return logo;
        }
    }

    protected bool IsEvalRedirectionNeeded => EvalRegistrationHelper.IsRedirectNeeded();

    protected string EvalRedirectionUrl => EvalRegistrationHelper.EvalRegistrationFormUrl;

    protected override void OnLoad(EventArgs e)
    {
        // username and password from the user?
        if (IsPostBack)
        {
            this.Page.Validate();
            if (FistTimeLogin)
            {
                if (this.Page.IsValid
                        && Username.Text == "Admin"
                        && ChangePassword(Username.Text, "", NewPassword.Text)
                        && UnlockUser(Username.Text))
                {
                    WebSettingsDAL.SetValue("AdminPasswordEnforcement", false);
                    DoLoginAction(Username.Text, NewPassword.Text);
                }
            }
            if (this.Page.IsValid)
            {
                InitiateLogin();
                return;
            }
        }

        DataBind();

        Response.Cookies.Add(new HttpCookie(TestCookie_Name, "Supported"));

        if (Page.Session != null && Page.Session["ActiveTabId"] == null && Page.Request.Browser.Cookies && !string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
        {
            Page.Response.Cookies["Orion_IsSessionExp"].Value = "TRUE";
            Page.Response.Cookies["Orion_IsSessionExp"].Expires = DateTime.Now.AddHours(2);
        }

        string successfulLogout = this.Request.QueryString["SuccessfulLogout"];
        if (!string.IsNullOrEmpty(successfulLogout))
        {
            LogoutMessage.Visible = successfulLogout.Equals("yes", StringComparison.InvariantCultureIgnoreCase);
        }

        if (IsPostBack)
        {
            LogoutMessage.Visible = false;
        }

        //Check if user is presenting with AccountID (querystring or embedded in returnURL)
        //If they are, then bypass the WindowsLogin bounce

        string accId = this.Request["AccountID"];
        string retUrl = ReturnUrl;
        const string accountIdMarker = "?AccountID=";
        int pos = retUrl.IndexOf(accountIdMarker, StringComparison.InvariantCultureIgnoreCase);
        if (pos >= 0) accId = retUrl.Substring(pos + accountIdMarker.Length);

        //Check if LoginNonce is presenting in querystring/cookies or is it embedded in returnURL
        //If yes, then do Login with it
        var requestNonce = Request["LoginNonce"] ?? HttpUtility.ParseQueryString(retUrl).Get("LoginNonce");
        if (string.IsNullOrEmpty(requestNonce))
        {
            if (accId == null)
            {
                //Check Window Auto-login setting, if disabled bypass autologin
                bool autoLoginSetting = (WebSettingsDAL.Get("WindowsAccountLogin", "True") == "True");

                //Additional check for "autologin=no" querystring value (indicates user has come from logout page)
                // - Do not autlogin.
                string disableAutologinFromLogOut = this.Request.QueryString["autologin"];

                //Check for sessionTimeout in Querystring

                isSessionTimedOut = false;
                if (this.Request.QueryString["sessionTimeout"] != null)
                {

                    isSessionTimedOut = (this.Request.QueryString["sessionTimeout"].Equals("yes", StringComparison.InvariantCultureIgnoreCase)) ? true : false;

                    if (isSessionTimedOut)
                    {
                        // Clear also session cookie
                        CookieHelper.ClearCookie("ASP.NET_SessionId", true);
                        // Empty Profile.UserName means that session is already closed
                        if (!string.IsNullOrEmpty(HttpContext.Current.Profile.UserName))
                        {
                            // Log out if session still exists
                            LoggedInUser.LogOut();
                        }
                    }

                    SessionExpired.Visible = isSessionTimedOut;
                }

                //AutoLogin - Y/N?
                autoLogin = ((autoLoginSetting) && (String.IsNullOrEmpty(disableAutologinFromLogOut)) && !isSessionTimedOut);

                if (autoLogin)
                {
                    //Bounce off WindowsLogin to get windows creds

                    //Initialize the WindowsAuthBounce session variable
                    if (HttpContext.Current.Session["WindowsAuthBounce"] == null)
                    {
                        HttpContext.Current.Session["WindowsAuthBounce"] = false;
                    }
                    //Check the current WindowsAuthBounce value (Have we sent 401?)
                    var windowsAuthBounceVar = HttpContext.Current.Session["WindowsAuthBounce"];
                    if (windowsAuthBounceVar != null)
                    {
                        bool bounced = Convert.ToBoolean(windowsAuthBounceVar);
                        //Transfer control to WindowsLogin.aspx; to Send 401
                        if (!bounced)
                        {
                            //Bounce to WindowsLogin (preserve ReturnUrl)
                            //Server.Transfer to avoid additional requests from client
                            retUrl = "?returnUrl=" + ReturnUrl;
                            Server.Transfer("/Orion/WindowsLogin.aspx" + retUrl);
                        }
                        else
                        {
                            //Already been through WindowsLogin.aspx, 401 has been sent, so WindowsIdentity should now be available.
                            //Assign Session Identity for Authentication persons only.
                            if (Request.LogonUserIdentity.IsAuthenticated)
                            {
                                HttpContext.Current.Session["CurrentWindowsIdentity"] = Request.LogonUserIdentity;
                            }
                        }
                    }
                }
            }
        }

        var requestUserName = this.Request["AccountID"];
        var requestPassword = this.Request["Password"];
        if (!string.IsNullOrEmpty(requestNonce))
        {
            var dal = new LoginNonceDAL();

            string accountName;
            bool result = dal.TryNonceAuthentication(new Guid(requestNonce), out accountName);
            dal.CleanExpiredNonces();

            if (result && OrionMembershipProvider.ValidateUserWithoutPassword(accountName))
            {
                DoLoginAction(accountName, string.Empty);
            }
        }
        else if (!String.IsNullOrEmpty(requestUserName))
        {
            this.Username.Text = requestUserName;
            this.Password.Text = requestPassword;

            if (this.Username.Text.Length == 0)
            {
                // if AccountID was the first query parameter, it won't show up in Request.QueryString.
                // In that case it will be buried in the ReturnUrl parameter, so we need to look for it there.
                string url = ReturnUrl;
                pos = url.IndexOf(accountIdMarker, StringComparison.InvariantCultureIgnoreCase);
                if (pos > 0)
                {
                    string accountID = url.Substring(pos + accountIdMarker.Length);
                    int ampersand = accountID.IndexOf('&');
                    if (ampersand > 0)
                        accountID = accountID.Substring(0, ampersand);
                    this.Username.Text = accountID;
                }
            }
        }

        // only attempt the auto-login if the return url exists,
        //  otherwise, we'd never be able to NOT auto-login

        //Autologin if provided AccountId and Password
        //If ReturnUrl == null
        //redirect to "/Orion/Default.aspx";
        if (!string.IsNullOrEmpty(ReturnUrlFromQueryString()) || (this.Request["AccountID"] != null && this.Request["Password"] != null))
            InitiateLogin();

        if (!IsPostBack)
        {
            if (!WebSettingsDAL.UseBrowserAutoComplete)
            {
                Username.Attributes.Add("autocomplete", "off");
            }

            // SAML login button
            var samlInfo = IdentityProviderDAL.GetSamlInfo();
            if (samlInfo != null)
            {
                var returnUrl = UrlHelper.GetReturnUrl(Request.QueryString);
                samlLogin.Visible = samlInfo.Enabled;
                samlLoginLink.NavigateUrl = UrlHelper.AppendParameter("/Orion/SamlInit.aspx", "ReturnUrl", returnUrl);
                samlLoginLink.Text = HttpUtility.HtmlEncode(String.Format(Resources.CoreWebContent.WEBDATA_KG0_1, samlInfo.Name));
            }

            // Login page error message
            if (ShowErrorMessage)
            {
                var loginErrorMsg = (LoginErrorMessage)Session["LoginErrorMessage"];
                loginErrorMessagePanel.Visible = true;
                loginErrorTitle.Text = loginErrorMsg.Title;
                loginErrorText.Text = loginErrorMsg.Message;
                loginErrorLink.Text = "<b>Learn&nbsp;more</b>";
                loginErrorLink.NavigateUrl = loginErrorMsg.HelpLink;
                loginErrorLink.Attributes.Add("rel", "noopener noreferrer");
                Session.Remove("LoginErrorMessage");
            }
        }

        if (FistTimeLogin)
        {
            Username.Text = "Admin";
            LoginButton.Text = Resources.CoreWebContent.LoginPage_LoginButton;
            ShowChangePassword();
        }

        base.OnLoad(e);

        // disable the always-present vertical scrollbar in ie
        var ctrl = Master.FindControl("Body");
        if (ctrl is System.Web.UI.HtmlControls.HtmlGenericControl)
            (ctrl as System.Web.UI.HtmlControls.HtmlGenericControl).Attributes["scroll"] = "auto";
    }

    private static readonly string[] urlsForwardingToView = new[] { "/Orion/Default.aspx", "/Default.aspx", "/", "/Orion/", "/Orion/Logout.aspx" };

    private void DoRedirect(string url, string passThroughUrl = null)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        var cookie = new HttpCookie(TestCookie_Name);
        cookie.Expires = DateTime.Now.AddDays(-1D);
        Response.Cookies.Add(cookie);

        // bypass one redirect
        if (urlsForwardingToView.Any(x => x.Equals(url, StringComparison.OrdinalIgnoreCase)))
        {
            url = "/Orion/View.aspx" + (OrionMinReqsMaster.IsMobileView ? "?IsMobileView=true" : string.Empty);
        }

        if (!string.IsNullOrWhiteSpace(passThroughUrl))
            url = UrlHelper.UrlAppendUpdateParameter(passThroughUrl, "ReturnUrl", url);

        Response.Redirect(url);
    }

    protected static Regex proxyFinder = new Regex(@"/Orion/Proxy\.aspx\?(PageTitle=[^&]*&)?Path=(?<path>.*)",
        RegexOptions.IgnoreCase | RegexOptions.Compiled);

    protected static Regex credentialsRemover = new Regex(@"[&?]AccountID=[^&]*&Password=[^&]*",
        RegexOptions.IgnoreCase | RegexOptions.Compiled);

    protected static Regex directLinkUrlValidator = new Regex(@"(^.*/((Default|Login)\.aspx?)?(\?.*)?$)|(^/Orion/Hubble.ashx\?Id=.*$)",
        RegexOptions.IgnoreCase | RegexOptions.Compiled);

    //Extends standard ReturnUrl fetch to include workaround for FF+AD
    protected string ReturnUrlFromQueryString()
    {
        string url = string.Empty;
        string[] urls = Request.QueryString.GetValues("ReturnUrl");
        if (urls != null && urls.Length > 0)
        {
            url = urls[0];
        }

        //Workaround to obtain DirectLink from Referer, (FF+AD) - FB#6152
        if (String.IsNullOrEmpty(url))
        {
            if (Request.UrlReferrer != null)
            {
                if (Request.UrlReferrer.ToString().StartsWith(Request.Url.ToString()))
                {
                    //Get ReturnUrl from referer
                    url = ReturnUrlFromRequest(Request.UrlReferrer);
                }
            }
        }
        return url;
    }

    private void ShowChangePassword()
    {
        Username.Enabled = false;
        PasswordBox.Visible = false;
        ChangePasswordBox.Visible = true;
    }

    // Helper method to get ReturnUrl from Request Uri
    private string ReturnUrlFromRequest(Uri RequestUrl)
    {
        string query = RequestUrl.Query;
        NameValueCollection items = HttpUtility.ParseQueryString(query);
        if (items != null && items["ReturnUrl"] != null)
        {
            return (string)items["ReturnUrl"];
        }
        else
        {
            return String.Empty;
        }
    }

    protected string ReturnUrl
    {
        get
        {
            //Wrapper method to get ReturnUrl
            string url = ReturnUrlFromQueryString();

            url = Server.UrlDecode(url);

            Match proxyMatch = proxyFinder.Match(url);
            if (proxyMatch.Success)
                url = proxyMatch.Groups["path"].Value;

            url = credentialsRemover.Replace(url, string.Empty);

            if (url.Contains("&") && !url.Contains("?"))
            {
                var location = url.IndexOf('&');
                url = url.Substring(0, location) + "?" + url.Substring(location + 1);
            }

            if (string.IsNullOrEmpty(url))
                url = "/Orion/Default.aspx";

            if (url.IndexOf("IsMobileView", StringComparison.OrdinalIgnoreCase) == -1
                && CommonWebHelper.IsMobileView(Request))
                url += (url.Contains("?")) ? "&IsMobileView=true" : "?IsMobileView=true";

            return url;
        }
    }

    private void SetLoginErrorMessage(string text)
    {
        var html = string.Format("{0}<br />{1}", Resources.CoreWebContent.WEBCODE_PCC_6, text);
        this.phMessage.Controls.Clear();
        this.phMessage.Controls.Add(new LiteralControl(html));
        this.phMessage.Visible = true;
    }

    protected void InitiateLogin()
    {
        string username = Username.Text;
        string password = Password.Text;

        string loginFailureMessage = String.Empty;

        if (!string.IsNullOrEmpty(username))
        {
            //forms login
            if (username.Contains("\\") || username.Contains("@")) //domain login (*forms)
            {
                IIdentity logonIdentity = null;
                try
                {
                    var ldapEnabled = LDAPAuthHelper.IsLDAPAuthenticationEnabled();
                    if (ldapEnabled)
                    {
                        //Get LDAP identity
                        logonIdentity = OrionMixedModeAuth.GetLdapIdentity(username, password, LDAPAuthHelper.GetLDAPAuthSettings());
                    }
                    else
                    {
                        //Get Windows identity (from username/password combo)
                        logonIdentity = OrionMixedModeAuth.GetIdentity(username, password);
                    }
                    //Check the Identity with AuthorizationManager
                    if (!AuthorizationManager.AuthorizeUser(logonIdentity))
                    {
                        //Windows auth denied
                        SetLoginErrorMessage(Resources.CoreWebContent.WEBCODE_PCC_7);
                    }
                    else
                    {
                        //Windows auth ok - remember current identity for rest of the session
                        HttpContext.Current.Session["CurrentWindowsIdentity"] = logonIdentity;
                        DoLoginAction(logonIdentity.Name, string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    if (ex is System.ComponentModel.Win32Exception)
                    {
                        //Login failure
                        this.phMessage.Controls.Clear();
                        this.phMessage.Controls.Add(new LiteralControl(ex.Message));
                        this.phMessage.Visible = true;
                        loginFailureMessage = ex.Message;
                    }
                    else
                    {
                        //Domain account, has failed logon for some other general reason
                        //Windows auth denied
                        SetLoginErrorMessage(Resources.CoreWebContent.WEBCODE_PCC_8);
                    }
                }
            }
            else //normal forms login
            {
                if (Membership.ValidateUser(username, password))
                {
                    try
                    {
                        DoLoginAction(username, password);
                    }
                    catch (AuthenticationException ex)
                    {
                        //Login failure
                        this.phMessage.Controls.Clear();
                        this.phMessage.Controls.Add(new LiteralControl(ex.Message));
                        this.phMessage.Visible = true;
                        loginFailureMessage = ex.Message;
                    }
                }
            }
        }
        else if ((this.Request.LogonUserIdentity != null) && autoLogin)
        {
            //windows auto login

            IIdentity logonIdentity = null;
            var ldapEnabled = LDAPAuthHelper.IsLDAPAuthenticationEnabled();
            if (ldapEnabled)
            {
                //Get LDAP identity using logged user (WindowsIdentity)
                var currentSid = this.Request.LogonUserIdentity.User != null ? this.Request.LogonUserIdentity.User.ToString() : string.Empty;
                logonIdentity = OrionMixedModeAuth.GetLdapIdentity(this.Request.LogonUserIdentity.Name, password,
                    LDAPAuthHelper.GetLDAPAuthSettings(), true, currentSid);
            }
            else
            {
                logonIdentity = this.Request.LogonUserIdentity;
            }

            //Already have LogonUserIdentity (WindowsIdentity)
            //Check the Identity with AuthorizationManager
            if (!AuthorizationManager.AuthorizeUser(logonIdentity))
            {
                //Windows auth denied - do not show an authorization failure message
                this.phMessage.Controls.Clear();
                //this.phMessage.Controls.Add(new LiteralControl("Login failure. Windows Account not authorized."));
            }
            else
            {
                //windows login (either WindowsAccount or VirtualAccount (Group Membership Authorized))
                DoLoginAction(this.Request.LogonUserIdentity.Name, string.Empty);
            }
        }

        //DirectLink
        if (string.IsNullOrEmpty(username) && PageOkForDirectLink(ReturnUrl))
        {
            MembershipUser directLink = Membership.GetUser("DirectLink");
            if (null != directLink && !directLink.IsLockedOut)
            {
                DoLoginAction("DirectLink", string.Empty);
            }
        }

        if (!string.IsNullOrEmpty(this.Username.Text))
        {
            this.phMessage.Controls.Clear();
            if (Context.Items["UserExpired"] is bool && (bool)Context.Items["UserExpired"])
            {
                SetLoginErrorMessage(Resources.CoreWebContent.WEBCODE_PCC_9);
            }
            else if (!String.IsNullOrEmpty(loginFailureMessage))
            {
                this.phMessage.Controls.Add(new LiteralControl(loginFailureMessage));
            }
            else
            {
                SetLoginErrorMessage(Resources.CoreWebContent.WEBCODE_PCC_10);
            }
        }
    }

    private bool PageOkForDirectLink(string url)
    {
        Match match = directLinkUrlValidator.Match(url);
        return !match.Success;
    }

    private bool ChangePassword(string userName, string oldPassword, string newPassword)
    {
        return new OrionMembershipProvider().ChangePassword(Username.Text, oldPassword, newPassword);
    }

    private bool UnlockUser(string userName)
    {
        return new OrionMembershipProvider().UnlockUser(userName);
    }

    private void DoLoginAction(string username, string password)
    {
        // Invalidate user settings for all users as we might have changed cached values by user cloning
        UserProfileCache.Invalidate();

        bool disableSessionTimeout = OrionMembershipProvider.GetDisableSessionTimeout(username);

        // ProfilerProxy needs access to the username in a context where, bizarrely, the session is available but
        // the profile is not. I would like to find a better way to handle this, but this will do for now. -TD
        Session["-hack-username"] = username;
        Session["-UserHostAddress"] = Request.UserHostAddress;

        string xsrfToken;
		HttpCookie[] authCookies = AuthorizationManager.GetAuthenticationAndXsrfCookies(username, out xsrfToken);
	
		foreach (HttpCookie cookie in authCookies)
		{
			Response.Cookies.Add(cookie);
		}

		OrionMembershipProvider.UpdateUserLastLoginTime(username, Request.UserHostAddress);
        SessionManager.Login(username);

        ResourcePickerWebServices.PreBuildCache(xsrfToken);

        string returnUrl = ReturnUrl;
		if (OrionMembershipProvider.IsNewApiUser(username))
		{
			string passThrough = ResolveUrl("~/Orion/LoginApi.aspx");
			log.InfoFormat("Account ID '{0}' needs additional processing on first login. Redirecting to pass-through url '{1}', original url '{2}'. ", username, passThrough, returnUrl);

			DoRedirect(returnUrl, passThrough);
		}
		else
		{
			log.InfoFormat("Account ID '{0}' doesn't need additional processing on first login. Redirecting to original url '{1}'. ", username, returnUrl);

			DoRedirect(returnUrl);
		}
	}
}