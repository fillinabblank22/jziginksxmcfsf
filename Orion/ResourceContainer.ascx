﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResourceContainer.ascx.cs" Inherits="Orion_ResourceContainer" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DAL" %>
<%@ Register Src="~/Orion/ViewGroupSubNav.ascx" TagPrefix="orion" TagName="ViewGroupSubNav" %>
<%@ Reference Control="~/Orion/NetPerfMon/Resources/EmptyResource.ascx" %>
<Orion:Include runat="server" File="resourceContainer.js"/>

<style type="text/css">
.sw-orion .sw-app-region {
    margin-left: 40px;
}

form.sw-app-region.sw-hidden-left-nav {
    margin-left: 0px;
}

.sw-dashboard__subnav-panel {
    position: fixed;
    left: 0;
    top: 40px;
    bottom: 0;
    width: 40px;
    padding-top: 1px;
    background: #ffffff;
    transition: width cubic-bezier(0.230, 1.000, 0.320, 1.000) .5s;
    transition-delay: .3s;
    z-index: 1;
    box-shadow: rgba(0, 0, 0, 0.3) 0 1px 5px 0;
    overflow: hidden;
    z-index: 2;
}
.sw-dashboard__subnav-panel:hover {
    width: 250px;
}
.sw-eval-mode .sw-dashboard__subnav-panel {
    top: 64px;
}

.sw-orion .subNavContainer .btnCollapse {
    display: none;
}

.sw-static-subview {
    width: 100%;
}

.sw-static-subview .sw-frame-wrapper {
    display: flex;
    position: absolute;
    top: 0;
    left: 10px;
    width: calc(100vw - 30px);   /* horizontal padding for absolute positioning */
    height: calc(100vh - 130px); /* extracting header and footer height */
}

.sw-static-subview iframe {
    flex: 1;
}

</style>
<script type="text/javascript">
    $(function() {
        SW.Core.ResourceContainer.Init({
            viewsColumnInfo: <%=ViewsColumnInfo %>,
            isDemoMode: <%=(IsDemoMode) ? "true" : "false"%>,
            isNocView: <%=(IsNOCView) ? "true" : "false"%>,
            allowCustomize: <%= (Profile.AllowCustomize) ? "true" : "false"%>,
            viewsRotationInterval: <%=ViewsCount > 1 ? DataSource.NOCViewRotationInterval * 1000 : WebSettingsDAL.AutoRefreshSeconds * 1000 %>,
            isReadOnlyMode: <%= IsReadOnlyMode ? "true" : "false" %>
        });
    });
</script>

<div class="slideshow" sw-view-info="<%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(this.SerializedViewInfo)) %>" ng-init="readOnlyDashboard = <%= IsReadOnlyMode ? "true" : "false" %>"
        sw-dashboard-functions>
    <asp:Repeater runat="server" ID="rptContainers" OnItemDataBound="rptContainers_ItemDataBound">
        <ItemTemplate>
            <table class="ResourceContainer sw-dashboard__widget-container" <%= IsFullScreenMode ? "style='width:99%'" : "" %> >
                <tr>
                    <td class="subNavWrapperMain" runat="server" Visible="<%$Code: !OrionMinReqsMaster.IsApolloEnabled && !IsNOCView%>">
                        <orion:ViewGroupSubNav runat="server" id="LeftSubNav" ApplyConditionalHiding="True"></orion:ViewGroupSubNav>
                    </td>
                    <td runat="server" id="tdStaticSubview" class="sw-static-subview" Visible="false">
                        <div class="sw-frame-wrapper">
                            <iframe runat="server" id="iframeStaticSubview" frameborder="0">
                            </iframe>
                        </div>
                    </td>
                    <td runat="server" id="tdColumn1" class="ResourceColumn" ColumnID="1" ViewID='<%# Eval("ViewID") %>' xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn1">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph1" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumn2" class="ResourceColumn" ColumnID="2" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn2">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph2" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumn3" class="ResourceColumn" ColumnID="3" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn3">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph3" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumn4" class="ResourceColumn" ColumnID="4" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn4">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph4" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumn5" class="ResourceColumn" ColumnID="5" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn5">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph5" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumn6" class="ResourceColumn" ColumnID="6" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <asp:Repeater runat="server" ID="rptColumn6">
                            <ItemTemplate>
                                <asp:PlaceHolder runat="server" ID="ph6" OnDataBinding="ResourcePlaceHolder_DataBind" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="ResourceColumn_drag-spacer"></div>
                    </td>
                    <td runat="server" id="tdColumnNew" class="ResourceColumn ResourceColumn-New" ColumnID="7" ViewID='<%# Eval("ViewID") %>' Visible="<%$Code: !IsMobileView%>" xui-droppable sw-widget-drop-zone>
                        <div class="ResourceColumn-Center">
                            <div class="ResourceColumn-Circle ResourceColumn-Circle--drop-over">
                                <xui-icon icon="drop-active" icon-size="large"></xui-icon>
                            </div>

                            <div class="ResourceColumn-Circle">
                                <xui-icon icon="drop" icon-size="large"></xui-icon>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr runat="server" Visible="<%$HtmlEncodedCode: IsMobileView%>">
                    <td colspan="2"><div id="ShowMoreDiv" runat="server" class='mobileMoreBg'><a href="<%= DefaultSanitizer.SanitizeHtml(ShowMoreResourcesUrl) %>"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_14) %></a></div></td>
                </tr>
                <tr runat="server" Visible="<%$HtmlEncodedCode: IsMobileView%>">
                    <td colspan="2"><div id="Div1" runat="server" class='mobileMoreBg'><a href="/Orion/NetPerfMon/Alerts.aspx?IsMobileView=true"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_PS0_15) %> &raquo;</a></div></td>
                </tr>
            </table>
            
            <% if (GetShowLeftNav())
               { %>
            <div class="sw-dashboard__subnav-panel">
                <div class="sw-dashboard__subnav-content">
                    <% if (Profile.AllowCustomize)
                        { %>
                    <div class="xui ng-cloak">
                        <sw-dashboard-menu>
                            <sw-dashboard-menu-item ng-show="!editingDashboard">
                                <a href ng-click="editDashboard()">
                                    <xui-icon icon="edit"></xui-icon>
                                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_241) %></span>
                                </a>
                            </sw-dashboard-menu-item>
                            <sw-dashboard-menu-item ng-if="editingDashboard">
                                <a href ng-click="legacyEditDashboard('<%= DefaultSanitizer.SanitizeHtml(Server.HtmlEncode(GetCustomizationUrl())) %>')">
                                    <xui-icon icon="gear"></xui-icon>
                                    <span><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_SO0_71) %></span>
                                </a>
                            </sw-dashboard-menu-item>
                        </sw-dashboard-menu>
                    </div>
                      <% } %>
                
                    <orion:ViewGroupSubNav runat="server" id="LeftSubNav2" ApplyConditionalHiding="True"></orion:ViewGroupSubNav>
                </div>
            </div>
            
            <div class="xui sw-dashboard__widget-drawer {{ drawerOpen ? 'open' : '' }}">
                <sw-widget-drawer is-open="drawerOpen"></sw-widget-drawer>
            </div>

            <% } %>
        </ItemTemplate>
    </asp:Repeater>

    <div style="clear: both"></div>
</div>
