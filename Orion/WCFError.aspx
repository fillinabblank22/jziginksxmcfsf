<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMinReqs.master" Title="Orion Website Error" Inherits="SolarWinds.Orion.Web.UI.OrionErrorPageBase" ValidateRequest="false" %>
<%@ Register TagPrefix="orion" TagName="PageHeader" Src="~/Orion/Controls/PageHeader.ascx" %>
<%@ Register TagPrefix="orion" TagName="PageFooter" Src="~/Orion/Controls/PageFooter.ascx" %>

<asp:Content ContentPlaceHolderID="BodyContent" Runat="Server">
<div id="container">
    <div id="content">
<orion:PageHeader Minimalistic="true" runat="server" />

    <h1>Orion Website Error</h1>

    <p>There was an error communicating with the Orion server.</p>
    
    <h2>Additional Information</h2>
    <%= HttpUtility.HtmlEncode( Details.Error.Message ) %>
   
    <h2>Troubleshooting Steps</h2>
    <ol>
		<li>Confirm that the SolarWinds Orion Module Engine service is running, as shown in the following steps:
			<ol>
				<li>Click <b>Start &gt; Administrative Tools &gt; Services</b>.</li>
				<li>Right-click <b>SolarWinds Orion Module Engine</b>, and then click <b>Start</b>.</li>
			</ol>
		</li>
		<li>If the SolarWinds Orion Module Engine service is already running, contact <a href="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.SolarWindsSupportUrl) %>" target="_blank">SolarWinds Support</a>.</li>
		<li>Refresh the Orion Web Console browser.</li>
		<li>If the Orion Web Console does not display, contact <a href="<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.SolarWindsSupportUrl) %>" target="_blank">SolarWinds Support</a>.</li>
    </ol>

    </div>
    <div id="container-after">&nbsp;</div>
</div>
<orion:PageFooter runat="server" />
</asp:Content>
