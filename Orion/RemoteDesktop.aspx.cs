﻿using System;

public partial class Orion_RemoteDesktop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       if ( !IsPostBack)
       {
           if ( !String.IsNullOrEmpty(Request.UserAgent) && !Request.UserAgent.ToUpperInvariant().Contains("MSIE"))
           {
               WarningPlaceHolder.Visible = true;
           }

       }
    }

    public string RdpHeight
    {
        get { return GetRequestedDimension("Height",200,1200,600); }
    }

    public string RdpWidth
    {
        get { return GetRequestedDimension("Width", 200, 1600, 800); ; }
    }

    string GetRequestedDimension(string name, int minValue, int maxValue, int defaultValue)
    {
        string value = Request[name];

        if (String.IsNullOrEmpty(value))
            return defaultValue.ToString();

        int result = 0;
        if (int.TryParse(value, out result))
        {
            if (result >= minValue && result <= maxValue)
                return result.ToString();
        }

        return defaultValue.ToString();
    }
}
