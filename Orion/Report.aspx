<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Orion_Report" 
Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="orion" TagName="SchedulePicker" Src="~/Orion/Reports/Controls/SchedulePicker.ascx" %>
<%@ Register TagPrefix="xui" TagName="Include" Src="~/Orion/Controls/XuiInclude.ascx" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" runat=server>
<xui:Include runat="server" />
<orion:Include runat="server" File="AsyncView.js" />
<orion:Include runat="server" File="visibilityObserver.js" />
<script type="text/javascript">
    function getReportID() { return '<%= this.ReportID %>'; }
    function getReportName() { return '<%= this.ReportName %>'; }
    function GetHeaderTitleFn() { return '<%= string.Format(Resources.CoreWebContent.WEBDATA_IB0_176, this.ReportName) %>'; }
</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
  <style type="text/css">
      h1 {
          font-size: 30px !important;
          padding: 15px 0 10px 15px;
      }
  </style>
<% if (!Printable)
   { %>
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" /> 
<orion:Include runat="server" File="OrionCore.js" />
 <% if (Profile.AllowReportManagement) { %> 
    <orion:Include runat="server" File="Reports/js/ReportScheduleMenu.js" />  
<%} %>
    <div class="sheduleMessage">
        <span style="color: red;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_RV0_3) %> </span>
        <a style="padding:0;margin:0;" href="<%= DefaultSanitizer.SanitizeHtml(SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("orioncorereportmigrationtool")) %>" target="_blank" rel="noopener noreferrer"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_8) %></a>
    </div>
    <% if (Profile.AllowReportManagement) { %>
    <div id="sheduleMenu" class="sw-suggestion-icon" >  
    </div>
    <%} %>
    <div class="printableAndPdfLink">
    <orion:IconLinkExportToPDF runat="server" ID="ExportToPDFLink"/>
    <asp:HyperLink ID="PrintableLink" runat="server" CssClass="printablePageLink" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_246 %>" /> 
    </div>
 <% } %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<orion:Refresher ID="Refresher1" runat="server" />
    <h1><%= DefaultSanitizer.SanitizeHtml(Page.Title) %></h1>
    <h2 runat="server" id="subtitleText" style="font-size:small;" />
<div style="margin: 8px;">
	<%= DefaultSanitizer.SanitizeHtml(GetReportHtml()) %>
</div>
<%--Fix for FB45147--%>
<%if (!string.IsNullOrEmpty(Request.QueryString["MinPageWidth"]))
  {%>
<div style="width:<%= DefaultSanitizer.SanitizeHtml(Request.QueryString["MinPageWidth"]) %>px;">&nbsp;</div>
<%} %>
<%--Fix for FB45147--%>
   <orion:SchedulePicker ID="SchedulePicker1" runat="server" 
        Title="<%$ HtmlEncodedResources:CoreWebContent,WEBDATA_YP0_2 %>" 
        GetHeaderTitleFn="GetHeaderTitleFn" 
        OnSchedulesSelected="SchedulesSelected" BlockIfDemo="True"/>
</asp:Content>

