﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_Help : System.Web.UI.Page
{
    const string defaultHelpUrl = @"/NPMV6/help";

    protected void Page_Load(object sender, EventArgs e)
    {
        string url = String.Concat(WebSettingsDAL.HelpServer, defaultHelpUrl);
        Response.Redirect(url);
    }
}
