﻿<%@ WebHandler Language="C#" Class="Hubble" %>

using System;
using System.Web;
using SolarWinds.Orion.Web.Hubble;

public class Hubble : IHttpHandler
{
    private const string _emptyHubbleData = "{}";
    
    public void ProcessRequest (HttpContext context)
    {
        Guid hubbleId = ParseGuid(context.Request.Params["id"]);
        string hubbleData = LoadFromCache(hubbleId);

        context.Response.ContentType = "application/json";
        context.Response.Write(hubbleData);
    }

    private static string LoadFromCache(Guid id)
    {
        var cachedItem = HubbleCachedItem.LoadFromCache(id);
        return (cachedItem != null) ? cachedItem.JsonData : _emptyHubbleData;
    }

    private static Guid ParseGuid(string param)
    {
        try
        {
            return new Guid(param);
        }
        catch (Exception)
        {
            return Guid.Empty;
        }
    }

    public bool IsReusable 
    {
        get { return false; }
    }

}