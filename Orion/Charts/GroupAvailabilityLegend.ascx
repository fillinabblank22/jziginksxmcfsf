<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupAvailabilityLegend.ascx.cs" Inherits="Orion_Charts_GroupAvailabilityLegend" %>
<%@ Import Namespace="SolarWinds.Shared" %>

<script type="text/javascript">

        SW.Core.Charts.Legend.core_standardLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
            chart.series[0].legendItems = [{name:'<%= StatusInfo.GetStatus( StatusInfo.UpStatusId).ShortDescription %>', color:'#77bd2d'},{name:'<%= StatusInfo.GetStatus( StatusInfo.WarningStatusId).ShortDescription %>', color:'#FCD928'},{name:'<%= StatusInfo.GetStatus( StatusInfo.CriticalStatusId).ShortDescription %>', color:'#F99D1C'}, {name:'<%=StatusInfo.GetStatus( StatusInfo.DownStatusId).ShortDescription  %>', color:'red'}, {name:'<%=StatusInfo.GetStatus( StatusInfo.UnreachableStatusId).ShortDescription  %>', color:'black'}, {name:'<%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_OJ0_1) %>', color:'#8080FF'}];
            $('#<%= legend.ClientID %>').empty();
            SW.Core.Charts.Legend.createMultiColorLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
            SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '<%= legend.ClientID %>'); //use the standard method for 95th percentile, Sum, trend
        };
        
</script>

<table runat="server" id="legend" class="chartLegend"></table>
<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
