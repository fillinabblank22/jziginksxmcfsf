﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StandardLegend.ascx.cs" Inherits="Orion_Charts_StandardLegend" %>

<script type="text/javascript">
    SW.Core.Charts.Legend.core_standardLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        $('#<%= legend.ClientID %>').empty();
        SW.Core.Charts.Legend.createStandardLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
    };
        
        
</script>

<table runat="server" id="legend" class="chartLegend"></table>
<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
