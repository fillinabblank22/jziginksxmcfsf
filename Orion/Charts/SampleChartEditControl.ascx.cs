﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_Charts_SampleChartEditControl : UserControl, IChartEditorSettings
{
    private static readonly Log _log = new Log();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        string lineColor = resourceInfo.Properties["LineColor"] ?? String.Empty;
        LineColor.SelectedValue = lineColor;
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        _log.ErrorFormat("Adding Line Color: \"{0}\"", LineColor.SelectedValue);
        properties["LineColor"] = LineColor.SelectedValue;
    }
}