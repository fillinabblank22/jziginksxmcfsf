﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForecastingLegend.ascx.cs" Inherits="Orion_Charts_ForecastingLegend" %>
<orion:Include ID="Include1" runat="server" File="Formatters.js" />
<orion:Include ID="Include2" runat="server" File="Forecasting.css" />

<script type="text/javascript">
    SW.Core.Charts.Legend.core_standardLegendInitializer__<%= legend.ClientID %> = function (chart, dataUrlParameters) {
        $('#<%= legend.ClientID %>').empty();
        SW.Core.Charts.Legend.createForecastingLegend(chart, dataUrlParameters, '<%= legend.ClientID %>');
    };
</script>

<style type="text/css">
table.forecastingTableLegent {border-spacing: 0;}
table.forecastingTableLegent tr td { border-bottom: 1px solid #DBDBDB !important; }
</style>

<table runat="server" id="legend" class="chartLegend forecastingTableLegent NeedsZebraStripes"></table>
<div style="text-align:right;">
    <img class="chartLegendLogo" src="/orion/images/SolarWinds.Logo.Footer.png"/>
</div>
