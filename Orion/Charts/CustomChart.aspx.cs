﻿using System;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using System.Web.Script.Serialization;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Charts_CustomChart : System.Web.UI.Page
{
    private readonly string defaultResourcePathForChartNameQuery =
         "SELECT [DefaultResourcePath] FROM [dbo].[ChartSettings] WHERE [ChartName]=@chartName";

    private readonly Log log = new Log();

    private string netObjectId = string.Empty;
    private NetObject netObject = null;
    private ResourceInfo resource = null;
    private StandardChartResource customChart = null;

    /// <summary>
    /// Gets the NetObjectId.
    /// </summary>
    protected string NetObjectId
    {
        get
        {
            if (string.IsNullOrEmpty(netObjectId))
            {
                netObjectId = Request["NetObject"];
            }

            return netObjectId;
        }
    }

    /// <summary>
    /// Gets the NetObject.
    /// </summary>
    public NetObject NetObject
    {
        get { return netObject; }
    }

    /// <summary>
    /// Gets the Resource.
    /// </summary>
    public ResourceInfo Resource
    {
        get { return resource; }
    }

    /// <summary>
    /// Gets a value indicating whether [without persistent resource].
    /// </summary>
    /// <value>
    /// 	<c>true</c> if [without persistent resource]; otherwise, <c>false</c>.
    /// </value>
    public bool WithoutPersistentResource { get; private set; }

    /// <summary>
    /// Gets the width of the chart.
    /// </summary>
    protected int Width
    {
        get
        {
            string value = string.Empty;
            if (!IsPostBack)
            {
                value = Request["Width"];
            }
            else
            {
                value = chartWidth.Text;
            }

            int width = 0;
            if (Int32.TryParse(value, out width) && width > 0)
            {
                return width;
            }
            else
            {
                return 640;
            }
        }
    }

    /// <summary>
    /// Gets the height of the chart
    /// </summary>
    protected int Height
    {
        get
        {
            string value = string.Empty;
            if (!IsPostBack)
            {
                value = Request["Height"];
            }
            else
            {
                value = chartHeight.Text;
            }

            int height = 0;
            if (Int32.TryParse(value, out height) && height > 0)
            {
                return height;
            }
            else
            {
                return 0;
            }
        }
    }

    protected bool IsChartInDynamicLoadingMode
    {
        get { return customChart != null && customChart.ChartResourceSettings != null && customChart.ChartResourceSettings.ChartSettings.LoadingMode == LoadingModeEnum.DynamicLoading; }
    }

    /// <summary>
    /// Values can be e.g. Orion.Node, Orion.Interfaces etc.
    /// </summary>
    public string ObjectSWISType
    {
        get
        {
            return SolarWinds.Orion.Web.Dependencies.DependencyUIItem.GetSwisEntityTypeByEntityNameOrObjectFilter(Resource.Properties["EntityName"], Resource.Properties["ObjectFilter"]);
        }
    }

    protected bool IsCustomTimePeriodSelected()
    {
        return ChartResourceSettings.IsChartDateSpanCustom(ChartTimeSpan.SelectedValue);
    }

    /// <summary>
    /// Calls BuildUrl and adds current request QueryString
    /// </summary>
    /// <param name="basePage"></param>
    /// <returns></returns>
    private string BuildUrlWithRequestQueryString(string basePage)
    {
        var url = BuildUrl(basePage);
        var queryStrings = Page.Request.QueryString.ToEnumerable().Merge(UrlHelper.ParseQueryString(url));
        return UrlHelper.ReplaceQueryString(url, queryStrings);
    }

    /// <summary>
    /// Builds the URL which lead to CustomChart page from parameters entered in this page by user.
    /// </summary>
    /// <param name="basePage">The base page.</param>
    /// <returns></returns>
    private string BuildUrl(string basePage)
    {
        string url = basePage;
        SortedList parameters = new SortedList();

        parameters["NetObject"] = this.NetObjectId;
        if (!WithoutPersistentResource)
            parameters["ResourceID"] = this.Resource.ID.ToString(CultureInfo.InvariantCulture);

        parameters["ChartName"] = customChart.ChartResourceSettings.ChartName;

        if (WithoutPersistentResource && !String.IsNullOrEmpty(Request.QueryString["ChartResourceFile"]))
        {
            parameters["ChartResourceFile"] = Request.QueryString["ChartResourceFile"];
        }

        if (!String.IsNullOrEmpty(Request.QueryString["ReturnTo"]))
        {
            parameters["ReturnTo"] = Request.QueryString["ReturnTo"];
        }

        if (WithoutPersistentResource && !String.IsNullOrEmpty(Request.QueryString["ViewID"]))
        {
            parameters["ViewID"] = Request.QueryString["ViewID"];
        }

        if (!String.IsNullOrEmpty(Request.Params["ResourceTitle"]))
        {
            parameters["ResourceTitle"] = WebSecurityHelper.SanitizeHtmlV2(Request.Params["ResourceTitle"]);
        }

        if (!String.IsNullOrEmpty(txtTitle.Text))
            parameters["ChartTitle"] = WebSecurityHelper.SanitizeHtmlV2(txtTitle.Text);

        if (!String.IsNullOrEmpty(txtSubtitle.Text))
            parameters["ChartSubTitle"] = WebSecurityHelper.SanitizeHtmlV2(txtSubtitle.Text);

        parameters["ChartDateSpan"] = ChartTimeSpan.SelectedValue;
        if (IsCustomTimePeriodSelected())
        {
            parameters["DateFromUtc"] = DatabaseFunctions.ToISO860DateTimeString(dtBeginDateTime.Value);
            parameters["DateToUtc"] = DatabaseFunctions.ToISO860DateTimeString(dtEndDateTime.Value);
        }

        if (!String.IsNullOrEmpty(SampleSizeList.SelectedValue))
            parameters["SampleSize"] = SampleSizeList.SelectedValue;

        if (!String.IsNullOrEmpty(ChartZoom.SelectedValue))
            parameters["ChartInitialZoom"] = ChartZoom.SelectedValue;
        if (chbCalculateTrendLine.Visible)
            parameters["CalculateTrendLine"] = chbCalculateTrendLine.Checked;
        if (chbCalculateSumLine.Visible)
            parameters["CalculateSumLine"] = chbCalculateSumLine.Checked;
        if (chbCalculate95thPercentileLine.Visible)
            parameters["Calculate95thPercentile"] = chbCalculate95thPercentileLine.Checked;

        // resource properties parameters
        foreach (var key in Request.QueryString.AllKeys)
        {
            if (key.StartsWith("rp"))
            {
                parameters[key] = Request.QueryString[key];
            }
        }

        parameters["Width"] = Width.ToString();
        parameters["Height"] = Height.ToString();

        if (parameters.Count > 0)
        {
            // see if we need to add the ?
            if (url.IndexOf("?") == -1)
                url += "?";
            else
                url += "&";

            bool firstOne = true;
            foreach (DictionaryEntry o in parameters)
            {
                if (!firstOne)
                    url += "&";
                else
                    firstOne = false;
                string value = (o.Value != null) ? o.Value.ToString() : string.Empty;
                url += string.Concat(
                     HttpContext.Current.Server.UrlEncode(o.Key.ToString()),
                     "=",
                     HttpContext.Current.Server.UrlEncode(value));
            }
        }

        return url;
    }

    /// <summary>
    /// Method return javascript initialize for TimePerid ranges (Time Period and Sample Interval).
    /// </summary>
    /// <returns></returns>
    private string GetInitializeScript()
    {
        if (ViewState["TimePeriodInitialScript"] == null)
        {
            StringBuilder initializescript = new StringBuilder();
            string ids = string.Empty;
            string values = string.Empty;
            GetTimePeriods(ref ids, ref values);
            initializescript.Append(ids);
            initializescript.Append(values);
            initializescript.Append(GetTimeRelations());
            initializescript.Append("$(function(){");
            initializescript.AppendFormat("window.timePeriodControl = document.getElementById('{0}');", ChartTimeSpan.ClientID);
            initializescript.AppendFormat("window.sampleTimePeriodControl = document.getElementById('{0}');", SampleSizeList.ClientID);
            initializescript.Append("$(document).ready(function() { if(typeof(CheckTimePeriods) != 'undefined') $(CheckTimePeriods); }); });");
            ViewState["TimePeriodInitialScript"] = initializescript.ToString();
        }

        return ViewState["TimePeriodInitialScript"].ToString();
    }

    /// <summary>
    /// Gets javascript array for time periods. Usefull for tie Time Period control and Sample Interval.
    /// </summary>
    /// <param name="IDs">The I ds.</param>
    /// <param name="Values">The values.</param>
    private void GetTimePeriods(ref string IDs, ref string Values)
    {
        StringBuilder _ids = new StringBuilder("var sampleTimeIDS = [");
        StringBuilder _values = new StringBuilder("var sampleTimeValue = [");
        for (int i = 0; i < SampleSizeList.Items.Count; i++)
        {
            _ids.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", SampleSizeList.Items[i].Value));
            _values.Append(string.Format("{0}'{1}'", (i == 0) ? "" : ",", SampleSizeList.Items[i].Text));
        }

        _ids.Append("];");
        _values.Append("];");
        IDs = _ids.ToString();
        Values = _values.ToString();
    }

    /// <summary>
    /// Gets javascript definition for tie Time Period control and Sample Interval.
    /// </summary>
    /// <returns></returns>
    private string GetTimeRelations()
    {
        StringBuilder result = new StringBuilder("var sampleTimeRelations = [");
        for (int i = 0; i < ChartTimeSpan.Items.Count; i++)
        {
            StringBuilder samplesizeassigned = new StringBuilder("[");
            string timePeriod = ChartTimeSpan.Items[i].Value;

            string separator = string.Empty;
            foreach (ListItem sli in SampleSizeList.Items)
            {
                if (ChartResourceSettings.IsChartDateSpanCustom(timePeriod) || CheckTimePeriodValue(timePeriod, sli.Value.ToUpper()))
                {
                    samplesizeassigned.Append(string.Format("{0}'{1}'", separator, sli.Value));
                    separator = ",";
                }
            }

            samplesizeassigned.Append("]");
            result.Append(string.Format("{0}{1}", ((i == 0) ? "" : ","), samplesizeassigned.ToString()));
        }

        result.Append("];");
        return result.ToString();
    }

    private void InitialzeCustomDateControlsRequestOrResourceProperties()
    {
        var beginDateTime = DateTime.MinValue;
        if (DateTime.TryParse(Request.QueryString["DateFromUtc"], out beginDateTime))
        {
            dtBeginDateTime.Value = beginDateTime.ToLocalTime();
            customChart.DateFrom = beginDateTime;
        }
        else if (DateTime.TryParse(Resource.Properties["DateFromUtc"], out beginDateTime))
        {
            dtBeginDateTime.Value = beginDateTime.ToLocalTime();
        }

        var endDateTime = DateTime.MinValue;
        if (DateTime.TryParse(Request.QueryString["DateToUtc"], out endDateTime))
        {
            dtEndDateTime.Value = endDateTime.ToLocalTime();
            customChart.DateTo = endDateTime;
        }
        else if (DateTime.TryParse(Resource.Properties["DateToUtc"], out endDateTime))
        {
            dtEndDateTime.Value = endDateTime.ToLocalTime();
        }
    }

    private double ParseDoubleString(string value, double defaultValue)
    {
        double result;
        if (double.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out result))
            return result;
        else
            return defaultValue;
    }

    /// <summary>
    /// Method initialize all customize controls from values obtained from QueryString or Resource.Properties.
    /// </summary>
    private void InitializeControlsFromRequestOrResourceProperties()
    {
        var _settingsDal = new ChartSettingsDAL();
        var _chartResourceSettings = ChartResourceSettings.FromResource(Resource, _settingsDal);

        if (!string.IsNullOrEmpty(Request.QueryString["ChartTitle"]))
        {
            txtTitle.Text = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["ChartTitle"]);
            customChart.ChartTitle = txtTitle.Text;
        }
        else
        {
            txtTitle.Text = Resource.Properties["ChartTitle"];
        }

        if (!string.IsNullOrEmpty(Request.QueryString["ChartSubTitle"]))
        {
            txtSubtitle.Text = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["ChartSubTitle"]);
            customChart.ChartSubTitle = txtSubtitle.Text;
        }
        else
        {
            txtSubtitle.Text = Resource.Properties["ChartSubtitle"];
        }


        if (!string.IsNullOrEmpty(Request.QueryString["ChartDateSpan"]))
        {
            //set page control
            ChartTimeSpan.SelectedValue = Request.QueryString["ChartDateSpan"];
            customChart.TimeSpanInDays = ParseDoubleString(ChartTimeSpan.SelectedValue, ChartResourceSettings.DefaultChartDateSpan);
        }
        else
        {
            ChartTimeSpan.SelectedValue = Resource.Properties["ChartDateSpan"];
        }

        if (IsCustomTimePeriodSelected())
            InitialzeCustomDateControlsRequestOrResourceProperties();



        if (!string.IsNullOrEmpty(Request.QueryString["SampleSize"]))
        {
            SampleSizeList.SelectedValue = Request.QueryString["SampleSize"];
            int sampleSize;

            if (int.TryParse(SampleSizeList.SelectedValue, NumberStyles.Number, CultureInfo.InvariantCulture, out sampleSize))
            {
                customChart.SampleSize = sampleSize;
            }
        }
        else
        {
            if (!Resource.Properties.ContainsKey("SampleSize"))
                Resource.Properties.Add("SampleSize", _chartResourceSettings.SampleSize.ToString());

            SampleSizeList.SelectedValue = Resource.Properties["SampleSize"];
        }

        if (!string.IsNullOrEmpty(Request.QueryString["ChartInitialZoom"]))
        {
            ChartZoom.SelectedValue = Request.QueryString["ChartInitialZoom"];
            customChart.InitialZoom = ChartZoom.SelectedValue;
        }
        else
        {
            ChartZoom.SelectedValue = Resource.Properties["ChartInitialZoom"];
            customChart.InitialZoom = ChartZoom.SelectedValue;
        }

        // Solve Width of the chart by what user entered
        switch (Resource.Column)
        {
            case 1:
                Resource.View.Column1Width = Width;
                break;
            case 2:
                Resource.View.Column2Width = Width;
                break;
            default:
                Resource.View.Column3Width = Width;
                break;
        }

        if (Width != 0)
        {
            customChart.Width = Width;
        }

        if (Height != 0)
        {
            customChart.Height = Height;
        }

        bool btmpCalculateSerie = false;
        if (Request.QueryString["CalculateTrendLine"] != null)
        {
            if (bool.TryParse(Request.QueryString["CalculateTrendLine"], out btmpCalculateSerie))
            {
                chbCalculateTrendLine.Checked = btmpCalculateSerie;
            }
        }
        else
        {
            if (Resource.Properties["CalculateTrendLine"] == "1")
            {
                chbCalculateTrendLine.Checked = true;
            }
        }

        if (Request.QueryString["CalculateSumLine"] != null)
        {
            if (bool.TryParse(Request.QueryString["CalculateSumLine"], out btmpCalculateSerie))
            {
                chbCalculateSumLine.Checked = btmpCalculateSerie;
            }
        }
        else
        {
            if (Resource.Properties["CalculateSum"] == "1")
            {
                chbCalculateSumLine.Checked = true;
            }
        }

        if (Request.QueryString["Calculate95thPercentile"] != null)
        {
            if (bool.TryParse(Request.QueryString["Calculate95thPercentile"], out btmpCalculateSerie))
            {
                chbCalculate95thPercentileLine.Checked = btmpCalculateSerie;
            }
        }
        else
        {
            if (Resource.Properties["Calculate95thPercentile"] == "1")
            {
                chbCalculate95thPercentileLine.Checked = true;
            }
        }

        if (!string.IsNullOrEmpty(Request.QueryString["Printable"]))
        {
            bool bPrintable = false;
            if (bool.TryParse(Request.QueryString["Printable"], out bPrintable))
            {
                ChartSettingsTable.Visible = !bPrintable;
            }
        }
    }

    private bool CheckTimePeriodValue(string ChartTimeSpanValue, string SampleSizeValue)
    {

        int sampleSizeInMinutes = Int32.Parse(SampleSizeValue, CultureInfo.InvariantCulture);
        int daysToLoad = Int32.Parse(ChartTimeSpanValue, CultureInfo.InvariantCulture);

        const int minutesPerDay = 60 * 24;
        const int maxNumberOfPointsAllowed = 3000;

        int pointsToChart = (daysToLoad * minutesPerDay) / sampleSizeInMinutes;

        // For chart dynamic (lazy loading) mode we show all sample intervals.
        return ((pointsToChart <= maxNumberOfPointsAllowed) || IsChartInDynamicLoadingMode) && (pointsToChart >= 2);
    }

    /// <summary>
    /// Method fills the resource properties from URL params. 
    /// Takes from url params every parameter which starts with rp prefix and into resource.Properties collection give it without rp prefix.
    /// </summary>
    private void FillResourcePropertiesFromUrlParams()
    {
        foreach (var key in Request.QueryString.AllKeys)
        {
            if (key.StartsWith("rp"))
            {
                resource.Properties[key.Substring(2, key.Length - 2)] = Request.QueryString[key];
            }
        }
    }

    /// <summary>
    /// Initializes resource in the case, that we don't have record about resource in DB.
    /// </summary>
    private void InitializeNonPersistentResourceFromQueryStringOrDB()
    {
        WithoutPersistentResource = true;
        resource = new ResourceInfo();
        resource.Column = 1;

        if (String.IsNullOrEmpty(Request.QueryString["ChartName"]))
            throw new InvalidOperationException("ChartName missing from query string.");

        resource.Properties.Add("ChartName", Request.QueryString["ChartName"]);

        if (!String.IsNullOrEmpty(Request.QueryString["ViewID"]))
        {
            // if ViewID will be pass via url parameter we will prefer this one
            try
            {
                resource.View = ViewManager.GetViewById(Convert.ToInt32(Request.QueryString["ViewID"]));
            }
            catch (Exception ex)
            {
                log.Error("Parameter ViewID in QueryString is not numeric value.");
                if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                    throw new InvalidOperationException("NetObject is missing from the query string.");

                var viewType = NetObjectFactory.GetViewTypeForNetObject(Request.QueryString["NetObject"]);
                resource.View = ViewManager.GetViewsByType(viewType).FirstOrDefault();
            }
        }
        else
        {
            if (String.IsNullOrEmpty(Request.QueryString["NetObject"]))
                throw new InvalidOperationException("NetObject is missing from the query string.");

            var viewType = NetObjectFactory.GetViewTypeForNetObject(Request.QueryString["NetObject"]);
            resource.View = ViewManager.GetViewsByType(viewType).FirstOrDefault();
        }

        if (!String.IsNullOrEmpty(Request.QueryString["ChartResourceFile"]))
        {
            resource.File = Request.QueryString["ChartResourceFile"];
        }
        else
        {
            using (SqlCommand command =
                SolarWinds.Orion.Common.SqlHelper.GetTextCommand(
                    defaultResourcePathForChartNameQuery))
            {
                command.Parameters.AddWithValue("@chartName", Request.QueryString["ChartName"]);
                object objResourcePath = SolarWinds.Orion.Common.SqlHelper.ExecuteScalar(command);
                resource.File = objResourcePath.ToString();
            }
        }
    }

    private void ConstructAndAssignExportButtonsUrl()
    {
        string url = "/Orion/Charts/CustomChartData.ashx";

        string strDisplayDetails = new JavaScriptSerializer().Serialize(customChart.DisplayDetailsDictionary);
        strDisplayDetails = Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(strDisplayDetails));

        // =================== Beginning Raw data Export ========================
        string rawDataUrl = BuildUrl(url) + "&DataFormat=Excel";
        var strElementIds = string.Join(",", customChart.ElementIdsForChart);
        rawDataUrl += "&NetObjectIds=" + HttpContext.Current.Server.UrlEncode(strElementIds);

        string strJavaScriptRawData = "DownloadChartExportFile ('" + rawDataUrl + "','" + strDisplayDetails + "')";
        locButtonExcel.NavigateUrl = "#";
        locButtonExcel.Attributes["onclick"] = strJavaScriptRawData;

        // =================== Beginning HTML Export data ==============================
        string chartDataUrl = BuildUrl(url) + "&DataFormat=ChartData";
        chartDataUrl += "&NetObjectIds=" + HttpContext.Current.Server.UrlEncode(strElementIds);
        locButtonChart.NavigateUrl = "#";

        string strJavaScript = "OpenNewWindowWithExportChartData('" + chartDataUrl + "',(new Sys.StringBuilder('" + strDisplayDetails + "')).toString())";

        locButtonChart.Attributes["onclick"] = strJavaScript;
    }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Page.PreInit"/> event at the beginning of page initialization.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnPreInit(EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(Request.QueryString["NetObject"]))
        {
            try
            {
                netObject = NetObjectFactory.Create(Request.QueryString["NetObject"]);
            }
            catch (AccountLimitationException)
            {
                this.Page.Server.Transfer(
                    string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", Request.QueryString["NetObject"])
                    );
            }
        }

        if (String.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        { // initialization page when resource identification wasn't passes special case
            InitializeNonPersistentResourceFromQueryStringOrDB();
        }
        else
        {
            resource = ResourceManager.GetResourceByID(Convert.ToInt32(Request.QueryString["ResourceID"]));
            this.Context.Items[typeof(ViewInfo).Name] = this.Resource.View;
        }

        FillResourcePropertiesFromUrlParams();

        base.OnPreInit(e);
    }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
        if (Profile.AllowCustomize && !string.IsNullOrEmpty(Request.Params["ResourceID"]))
        {
            btnSave.Visible = true;
        }
        else
        {
            btnSave.Visible = false;
        }

        if (Resource.IsAspNetControl)
        {
            var path = Resource.File;
            BaseResourceControl resourceControl = (BaseResourceControl)LoadControl(path);

            ResourceHostControl host = null;
            if (resourceControl is StandardChartResource)
            {
                customChart = resourceControl as StandardChartResource;
            }
            else
            {
                // case when chart is embeded into Custom Object Resource
                var embeddedPath = Resource.Properties["EmbeddedObjectResource"];
                if (!string.IsNullOrEmpty(embeddedPath))
                {
                    var embeddedData = embeddedPath.Split(';');

                    Control control = LoadControl(embeddedData[0]);
                    control.ID = "ObjectResourceControl";
                    customChart = control as StandardChartResource;
                }

                if ((netObject != null) && !string.IsNullOrEmpty(ObjectSWISType))
                {
                    host = ResourceHostManager.GetResourceHostControl(ObjectSWISType, netObject);
                }
            }

            customChart.Resource = Resource;
            if (host == null)
            {
                string netObjectViewType;
                try
                {
                    netObjectViewType = NetObjectFactory.GetViewTypeForNetObject(NetObjectId);
                }
                catch (Exception ex)
                {
                    log.Debug(string.Format("Unable to get view type for net object '{0}', resource view type will be used '{1}'", NetObjectId, Resource.View.ViewType), ex);
                    netObjectViewType = Resource.View.ViewType;
                }
                host = ResourceHostManager.GetSupportingControl(customChart, netObjectViewType);
            }

            this.chartPlaceHolder.Controls.Add(host);
            host.LoadFromRequest();
            host.Controls.Add(customChart);

            chbCalculateTrendLine.Visible = customChart.ChartResourceSettings.ChartSettings.AllowTrendLine;
            chbCalculateSumLine.Visible = customChart.ChartResourceSettings.ChartSettings.AllowSumLine;
            chbCalculate95thPercentileLine.Visible = customChart.ChartResourceSettings.ChartSettings.AllowPercentileLine;
            var calculatedSeriesSectionVisible = chbCalculateTrendLine.Visible || chbCalculateSumLine.Visible ||
                                                 chbCalculate95thPercentileLine.Visible;
            calculatedSeriesDivider.Visible = calculatedSeriesSectionVisible;
            calculatedSeriesHeader.Visible = calculatedSeriesSectionVisible;

            // we will hide header bar of chart resource
            System.Reflection.FieldInfo fInfo = customChart.GetType().GetField("Wrapper", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (fInfo != null)
            {
                dynamic wrapper = fInfo.GetValue(customChart);
                if (wrapper != null)
                {
                    wrapper.ShowHeaderBar = false;
                    if (String.IsNullOrEmpty(Request.Params["ResourceTitle"]))
                    {
                        System.Reflection.PropertyInfo pTitleInfo = wrapper.GetType().GetProperty("Title", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                        if (pTitleInfo != null)
                        {
                            string strTitle = string.Format(Resources.CoreWebContent.WEBCODE_TM0_15, pTitleInfo.GetValue(wrapper, null));
                            lblChartTitle.Text = strTitle;
                            Page.Title = strTitle;
                        }
                    }
                    else
                    {
                        lblChartTitle.Text = string.Format(Resources.CoreWebContent.WEBCODE_TM0_15, WebSecurityHelper.SanitizeHtmlV2(Request.Params["ResourceTitle"]));
                        Page.Title = lblChartTitle.Text;
                    }
                }
            }
        }

        InitializeControlsFromRequestOrResourceProperties();

        if (customChart.ChartResourceSettings.ChartSettings.AllowTrendLine)
        {
            customChart.CalculateTrendLine = chbCalculateTrendLine.Checked;
        }

        if (customChart.ChartResourceSettings.ChartSettings.AllowSumLine)
        {
            customChart.CalculateSumLine = chbCalculateSumLine.Checked;
        }

        if (customChart.ChartResourceSettings.ChartSettings.AllowPercentileLine)
        {
            customChart.Calculate95thPercentileLine = chbCalculate95thPercentileLine.Checked;
        }


        base.OnInit(e);
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        chartWidth.Text = Width.ToString();
        chartHeight.Text = Height.ToString();

        this.PreRender += new EventHandler(Orion_Charts_CustomChart_PreRender);
    }

    /// <summary>
    /// Handles the PreRender event of the Orion_Charts_CustomChart control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Orion_Charts_CustomChart_PreRender(object sender, EventArgs e)
    {
        ConstructAndAssignExportButtonsUrl();
        if (SampleSizeList != null)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "TimePeriodInitialScript", GetInitializeScript(), true);
            ChartTimeSpan.Attributes.Add("onChange", "CheckTimePeriods()");
            SolarWinds.Orion.Web.OrionInclude.CoreFile("TimePeriodScript.js");
        }
    }

    /// <summary>
    /// Refreshes the click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void RefreshClick(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;
        Response.Redirect(BuildUrlWithRequestQueryString(Request.Url.AbsolutePath));
    }

    /// <summary>
    /// Submits the click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;

        Resource.Properties.Remove("SampleSize");
        Resource.Properties.Add("SampleSize", SampleSizeList.SelectedValue);
        Resource.Properties.Remove("ChartInitialZoom");
        Resource.Properties.Add("ChartInitialZoom", ChartZoom.SelectedValue);
        Resource.Properties.Remove("ChartTitle");
        Resource.Properties.Add("ChartTitle", txtTitle.Text);
        Resource.Properties.Remove("ChartSubtitle");
        Resource.Properties.Add("ChartSubtitle", txtSubtitle.Text);
        Resource.Properties.Remove("ShowTitle");
        Resource.Properties.Add("ShowTitle", (!string.IsNullOrEmpty(txtTitle.Text) || !string.IsNullOrEmpty(txtSubtitle.Text)) ? "1" : "0");

        if (IsCustomTimePeriodSelected())
        {
            Resource.Properties.Remove("DateFromUtc");
            Resource.Properties.Add("DateFromUtc", DatabaseFunctions.ToISO860DateTimeString(dtBeginDateTime.Value));

            Resource.Properties.Remove("DateToUtc");
            Resource.Properties.Add("DateToUtc", DatabaseFunctions.ToISO860DateTimeString(dtEndDateTime.Value));
            Resource.Properties.Remove("ChartDateSpan");
            Resource.Properties.Add("ChartDateSpan", ChartResourceSettings.CustomChartDateSpan);
        }
        else
        {
            Resource.Properties.Remove("DateFromUtc");
            Resource.Properties.Remove("DateToUtc");
            Resource.Properties.Remove("ChartDateSpan");
            Resource.Properties.Add("ChartDateSpan", ChartTimeSpan.SelectedValue);
        }

        if (chbCalculateTrendLine.Visible)
        {
            Resource.Properties.Remove("CalculateTrendLine");
            Resource.Properties.Add("CalculateTrendLine", chbCalculateTrendLine.Checked ? "1" : "0");
        }

        if (chbCalculateSumLine.Visible)
        {
            Resource.Properties.Remove("CalculateSum");
            Resource.Properties.Add("CalculateSum", chbCalculateSumLine.Checked ? "1" : "0");
        }

        if (chbCalculate95thPercentileLine.Visible)
        {
            Resource.Properties.Remove("Calculate95thPercentile");
            Resource.Properties.Add("Calculate95thPercentile", chbCalculate95thPercentileLine.Checked ? "1" : "0");
        }

        // start of code which ensure, that we will get back to proper view page
        string url = string.Format("/Orion/View.aspx?ViewID={0}", resource.View.ViewID);

        if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
        {
            string netObjectViewType = NetObjectFactory.GetViewTypeForNetObject(Request.QueryString["NetObject"]);

            if (netObjectViewType.Equals(resource.View.ViewType, StringComparison.InvariantCultureIgnoreCase))
                url = string.Format("{0}&NetObject={1}", url, Request.QueryString["NetObject"]);
        }

        ReferrerRedirectorBase.Return(
            ReferrerRedirectorBase.GetDecodedReferrerUrlOrDefault(url));
    }

    /// <summary>
    /// Handles the Click event of the Printable control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Printable_Click(object sender, EventArgs e)
    {
        string strUrl = BuildUrlWithRequestQueryString(Request.Url.AbsolutePath);
        strUrl = strUrl + "&Printable=true";
        Response.Redirect(strUrl);
    }

}
