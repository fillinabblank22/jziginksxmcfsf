﻿(function () {
    Highcharts.setOptions({
        lang: {
            // These values are defined by HighCharts.  The descriptions can be found here: http://www.highcharts.com/stock/ref/#lang
            months: ["@{R=Core.Strings;K=Charts_YK0_1;E=js}", "@{R=Core.Strings;K=Charts_YK0_2;E=js}", "@{R=Core.Strings;K=Charts_YK0_3;E=js}", "@{R=Core.Strings;K=Charts_YK0_4;E=js}", "@{R=Core.Strings;K=Charts_YK0_5;E=js}", "@{R=Core.Strings;K=Charts_YK0_6;E=js}", "@{R=Core.Strings;K=Charts_YK0_7;E=js}", "@{R=Core.Strings;K=Charts_YK0_8;E=js}", "@{R=Core.Strings;K=Charts_YK0_9;E=js}", "@{R=Core.Strings;K=Charts_YK0_10;E=js}", "@{R=Core.Strings;K=Charts_YK0_11;E=js}", "@{R=Core.Strings;K=Charts_YK0_12;E=js}"],
            shortMonths: ["@{R=Core.Strings;K=Charts_YK0_13;E=js}", "@{R=Core.Strings;K=Charts_YK0_14;E=js}", "@{R=Core.Strings;K=Charts_YK0_15;E=js}", "@{R=Core.Strings;K=Charts_YK0_16;E=js}", "@{R=Core.Strings;K=Charts_YK0_17;E=js}", "@{R=Core.Strings;K=Charts_YK0_18;E=js}", "@{R=Core.Strings;K=Charts_YK0_19;E=js}", "@{R=Core.Strings;K=Charts_YK0_20;E=js}", "@{R=Core.Strings;K=Charts_YK0_21;E=js}", "@{R=Core.Strings;K=Charts_YK0_22;E=js}", "@{R=Core.Strings;K=Charts_YK0_23;E=js}", "@{R=Core.Strings;K=Charts_YK0_24;E=js}"],
            weekdays: ["@{R=Core.Strings;K=Charts_DOW_YK0_1;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_2;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_3;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_4;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_5;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_6;E=js}", "@{R=Core.Strings;K=Charts_DOW_YK0_7;E=js}"],
            decimalPoint: "@{R=Core.Strings;K=Charts_decimalPoint;E=js}",
            thousandsSep: "@{R=Core.Strings;K=Charts_thousandsSep;E=js}",
            rangeSelectorZoom: "@{R=Core.Strings;K=Charts_YK0_25;E=js}",
            rangeSelectorFrom: "@{R=Core.Strings;K=Charts_YK0_26;E=js}",
            rangeSelectorTo: "@{R=Core.Strings;K=Charts_YK0_27;E=js}",
            loading: "@{R=Core.Strings;K=Charts_YK0_28;E=js}",
            resetZoom: "@{R=Core.Strings;K=Charts_YK0_29;E=js}",
            resetZoomTitle: "@{R=Core.Strings;K=Charts_YK0_30;E=js}",
            downloadPNG: "@{R=Core.Strings;K=Charts_YK0_31;E=js}",
            downloadJPEG: "@{R=Core.Strings;K=Charts_YK0_32;E=js}",
            downloadPDF: "@{R=Core.Strings;K=Charts_YK0_33;E=js}",
            downloadSVG: "@{R=Core.Strings;K=Charts_YK0_34;E=js}",
            exportButtonTitle: "@{R=Core.Strings;K=Charts_YK0_35;E=js}",
            printButtonTitle: "@{R=Core.Strings;K=Charts_YK0_36;E=js}",
            errorHeader: "@{R=Core.Strings;K=Charts_VT0_40;E=js}",


            // The following symbols are used to format dates and times. See http://www.highcharts.com/stock/ref/#highcharts-object for more details
            //
            //     %a: Short weekday, like 'Mon'.
            //     %A: Long weekday, like 'Monday'.
            //     %d: Two digit day of the month, 01 to 31.
            //     %e: Day of the month, 1 through 31.
            //     %b: Short month, like 'Jan'.
            //     %B: Long month, like 'January'.
            //     %m: Two digit month number, 01 through 12.
            //     %y: Two digits year, like 09 for 2009.
            //     %Y: Four digits year, like 2009.
            //     %H: Two digits hours in 24h format, 00 through 23.
            //     %I: Two digits hours in 12h format, 00 through 11.
            //     %l (Lower case L): Hours in 12h format, 1 through 11.
            //     %M: Two digits minutes, 00 through 59.
            //     %p: Upper case AM or PM.
            //     %P: Lower case AM or PM.

            // Date formats for each time range.  The charts will pick the correct granularity based on the time range shown
            dateTimeLabelFormats: {
                second: '%l:%M:%S %p',
                minute: '%l:%M %p',
                hour: '%l:%M %p',
                day: '@{R=Core.Strings;K=Charts_dateTimeLabelFormats_day;E=js}',
                week: '@{R=Core.Strings;K=Charts_dateTimeLabelFormats_week;E=js}',
                month: '@{R=Core.Strings;K=Charts_dateTimeLabelFormats_month;E=js}',
                year: '@{R=Core.Strings;K=Charts_dateTimeLabelFormats_year;E=js}'
            },
			
            //Mapping from .Net formats (HH:mm:ss) to match the expected Highcharts format (%l:%M %p)
            dateTimeFormatMapping: {
                "HH" : "%H",
                "H" : "%H",
                "mm" : "%M",
                "ss" : "%S",
                "tt" : "%p",
                "h": "%l",
                "hh": "%l"  // zh-Hant (Chinese) is using hh in local time format
            },

            // The date format shown in the Date Range textboxes when the textbox has focus
            inputEditDateFormat: '@{R=Core.Strings;K=Charts_inputEditDateFormat;E=js}',

            // The date format shown in the Date Range textboxes when the textbox does not have focus
            inputDateFormat: '@{R=Core.Strings;K=Charts_inputDateFormat;E=js}',

            // The date format shown in tooltips.  If the data is grouped, this value might not be used.
            tooltipDateFormat: '@{R=Core.Strings;K=Charts_tooltipDateFormat;E=js}',

            // The width of the range selector edit box
            rangeSelectorWidth: '@{R=Core.Strings;K=Charts_rangeSelectorWidth;E=js}',

            // The label to show for the 1 day, 1 week, 1 month quick zoom buttons
            zoomOneHour: '@{R=Core.Strings;K=Charts_YK0_52;E=js}',
            zoomTwelveHours: '@{R=Core.Strings;K=Charts_YK0_38;E=js}',
            zoomTwentyfourHours: '@{R=Core.Strings;K=Charts_YK0_56;E=js}',

            // The width of the quick zoom buttons.  Some languages may need wider buttons.
            zoomButtonWidth: +'@{R=Core.Strings;K=Charts_TM1_ZoomButtonWidth;E=js}',
            
            // The date time format of ${ZoomRange} macro in chart subtitle.
            zoomRangeDateTimeFormat: '@{R=Core.Strings;K=Charts_TM1_ZoomRangeDateTimeFormat;E=js}',
            
            
             

            // The series label for the Sum line (seen in the legend and on tooltips)
            sumSeriesLabel: "@{R=Core.Strings;K=Charts_YK0_40;E=js}",

            // The series label for the Right Sum line (seen in the legend and on tooltips)
            rightSumSeriesLabel: "@{R=Core.Strings;K=Charts_TP0_1;E=js}",

            // The text to display when there is no data available.
            noData: '@{R=Core.Strings;K=Charts_YK0_41;E=js}',

            // Any array of units for labeling a number of bytes.  Each entry is 
            // 1000 larger than the previous.  
            byteUnitSizes: ['@{R=Core.Strings;K=Charts_YK0_42;E=js}', '@{R=Core.Strings;K=Charts_YK0_43;E=js}', '@{R=Core.Strings;K=Charts_YK0_44;E=js}', '@{R=Core.Strings;K=Charts_YK0_45;E=js}', '@{R=Core.Strings;K=Charts_YK0_46;E=js}', '@{R=Core.Strings;K=Charts_YK0_47;E=js}', '@{R=Core.Strings;K=Charts_YK0_48;E=js}', '@{R=Core.Strings;K=Charts_YK0_49;E=js}', '@{R=Core.Strings;K=Charts_YK0_50;E=js}'],
            bpsUnitSizes: ['@{R=Core.Strings;K=Charts_VT0_1;E=js}', '@{R=Core.Strings;K=Charts_VT0_2;E=js}', '@{R=Core.Strings;K=Charts_VT0_3;E=js}', '@{R=Core.Strings;K=Charts_VT0_4;E=js}', '@{R=Core.Strings;K=Charts_VT0_5;E=js}', '@{R=Core.Strings;K=Charts_VT0_6;E=js}', '@{R=Core.Strings;K=Charts_VT0_7;E=js}', '@{R=Core.Strings;K=Charts_VT0_8;E=js}', '@{R=Core.Strings;K=Charts_VT0_9;E=js}'],
			BpsUnitSizes: ['@{R=Core.Strings;K=Charts_LF0_1;E=js}', '@{R=Core.Strings;K=Charts_LF0_2;E=js}', '@{R=Core.Strings;K=Charts_LF0_3;E=js}', '@{R=Core.Strings;K=Charts_LF0_4;E=js}', '@{R=Core.Strings;K=Charts_LF0_5;E=js}', '@{R=Core.Strings;K=Charts_LF0_6;E=js}', '@{R=Core.Strings;K=Charts_LF0_7;E=js}', '@{R=Core.Strings;K=Charts_LF0_8;E=js}', '@{R=Core.Strings;K=Charts_LF0_9;E=js}'],
            ppsUnitSizes: ['@{R=Core.Strings;K=Charts_VT0_11;E=js}', '@{R=Core.Strings;K=Charts_VT0_12;E=js}', '@{R=Core.Strings;K=Charts_VT0_13;E=js}', '@{R=Core.Strings;K=Charts_VT0_14;E=js}', '@{R=Core.Strings;K=Charts_VT0_15;E=js}', '@{R=Core.Strings;K=Charts_VT0_16;E=js}', '@{R=Core.Strings;K=Charts_VT0_17;E=js}', '@{R=Core.Strings;K=Charts_VT0_18;E=js}', '@{R=Core.Strings;K=Charts_VT0_19;E=js}'],
            packetsUnitSizes: ['@{R=Core.Strings;K=Charts_VT0_21;E=js}', '@{R=Core.Strings;K=Charts_VT0_22;E=js}', '@{R=Core.Strings;K=Charts_VT0_23;E=js}', '@{R=Core.Strings;K=Charts_VT0_24;E=js}', '@{R=Core.Strings;K=Charts_VT0_25;E=js}', '@{R=Core.Strings;K=Charts_VT0_26;E=js}', '@{R=Core.Strings;K=Charts_VT0_27;E=js}', '@{R=Core.Strings;K=Charts_VT0_28;E=js}', '@{R=Core.Strings;K=Charts_VT0_29;E=js}'],
            emptyUnitSizes: ['@{R=Core.Strings;K=Charts_VT0_31;E=js}', '@{R=Core.Strings;K=Charts_VT0_32;E=js}', '@{R=Core.Strings;K=Charts_VT0_33;E=js}', '@{R=Core.Strings;K=Charts_VT0_34;E=js}', '@{R=Core.Strings;K=Charts_VT0_35;E=js}', '@{R=Core.Strings;K=Charts_VT0_36;E=js}', '@{R=Core.Strings;K=Charts_VT0_37;E=js}', '@{R=Core.Strings;K=Charts_VT0_38;E=js}', '@{R=Core.Strings;K=Charts_VT0_39;E=js}'],



            // Text to show for zoom levels.  This is used for the initial zoom level (on the edit page). 
            // The short version is used for the Quick Zoom feature under the chart (not implemented yet).
            zoomLastHour: '@{R=Core.Strings;K=Charts_YK0_51;E=js}',
            zoomLastHourShort: '@{R=Core.Strings;K=Charts_YK0_52;E=js}',
            zoomLastTwoHours: '@{R=Core.Strings;K=Charts_YK0_53;E=js}',
            zoomLastTwoHoursShort: '@{R=Core.Strings;K=Charts_YK0_54;E=js}',
            zoomLast24Hours: '@{R=Core.Strings;K=Charts_YK0_55;E=js}',
            zoomLast24HoursShort: '@{R=Core.Strings;K=Charts_YK0_56;E=js}',
            zoomToday: '@{R=Core.Strings;K=Charts_YK0_57;E=js}',
            zoomTodayShort: '@{R=Core.Strings;K=Charts_YK0_58;E=js}',
            zoomYesterday: '@{R=Core.Strings;K=Charts_YK0_59;E=js}',
            zoomYesterdayShort: '@{R=Core.Strings;K=Charts_YK0_60;E=js}',
            zoomLast7Days: '@{R=Core.Strings;K=Charts_YK0_61;E=js}',
            zoomLast7DaysShort: '@{R=Core.Strings;K=Charts_YK0_62;E=js}',
            zoomThisMonth: '@{R=Core.Strings;K=Charts_YK0_63;E=js}',
            zoomThisMonthShort: '@{R=Core.Strings;K=Charts_YK0_64;E=js}',
            zoomLastMonth: '@{R=Core.Strings;K=Charts_YK0_65;E=js}',
            zoomLastMonthShort: '@{R=Core.Strings;K=Charts_YK0_66;E=js}',
            zoomLast30Days: '@{R=Core.Strings;K=Charts_YK0_67;E=js}',
            zoomLast30DaysShort: '@{R=Core.Strings;K=Charts_YK0_68;E=js}',
            zoomLast3Months: '@{R=Core.Strings;K=Charts_YK0_69;E=js}',
            zoomLast3MonthsShort: '@{R=Core.Strings;K=Charts_YK0_70;E=js}',
            zoomLast6Months: '@{R=Core.Strings;K=Charts_IB0_1;E=js}',
            zoomLast6MonthsShort: '@{R=Core.Strings;K=Charts_IB0_2;E=js}',
            zoomThisYear: '@{R=Core.Strings;K=Charts_YK0_71;E=js}',
            zoomThisYearShort: '@{R=Core.Strings;K=Charts_YK0_72;E=js}',
            zoomLast12Months: '@{R=Core.Strings;K=Charts_YK0_73;E=js}',
            zoomLast12MonthsShort: '@{R=Core.Strings;K=Charts_YK0_74;E=js}'


        }
    });
})();