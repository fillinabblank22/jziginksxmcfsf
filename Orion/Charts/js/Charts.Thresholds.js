﻿var thresholds = SW.Core.namespace("SW.Core.Charts.Thresholds");


// Enums is defined SolarWinds.Orion.Core.Common.Models.Thresholds.ThresholdOperatorEnum and needs to be sync.
thresholds.ThresholdOperatorEnum = {
  Greater : 0,
  GreaterOrEqual : 1,
  Equal : 2,
  LessOrEqual : 3,
  Less : 4,
  NotEqual : 5
};


thresholds.ThresholdPresenter = function(chartInstance, chartSettings, chartOptions) {
    var self = this;

    self.ShowLabel = true;
    self.chart = chartInstance;
    self.chartSettings = chartSettings;
    self.chartOptions = chartOptions;

    var addBandToChart = function (chart, band, axis) {
        if (chart && band) {
            if (axis === 'x') {
                chart.xAxis[0].addPlotBand(band);
            } else {
                chart.yAxis[0].addPlotBand(band);
            }
        }
    };
    
    var addLineToChart = function (chart, line, axis) {
        
        if (chart && line) {
            line.width = 2;
            if (axis === 'x') {
                chart.xAxis[0].addPlotLine(line);
            } else {
                chart.yAxis[0].addPlotLine(line);
            }
        }
    };

    var getWarningThresholdBand = function(from, to) {
        return {
            from: from,
            to: to,
            color: "rgba(252,255,21,0.2)",
            id: 'plot-band-1',
            label : getThresholdLabel('@{R=Core.Strings;K=WEBJS_ZT0_33;E=js}')
        };
    };
    
    
    var getCriticalThresholdBand = function(from, to) {
        return {
            from: from,
            to: to,
            color: "rgba(255,0,21,0.2)",
            id: 'plot-band-2',
            label : getThresholdLabel('@{R=Core.Strings;K=WEBJS_ZT0_34;E=js}')
        };
    };

    var getThresholdLabel = function(label) {
        if (self.showLabel) {
            return {
                text: label,
                align: 'left',
                verticalAlign: 'top',
                y: 12
            };
        } else {
            return null;
        }
    };


    var isValueInChartScale = function(value, chartMin, chartMax) {
        return value != null && value >= chartMin && value <= chartMax;
    };

    var clearThresholds = function(chart) {
        chart.xAxis[0].removePlotBand('plot-band-1');
        chart.yAxis[0].removePlotBand('plot-band-1');
        chart.xAxis[0].removePlotBand('plot-band-2');
        chart.yAxis[0].removePlotBand('plot-band-2');
        chart.xAxis[0].removePlotLine('plot-band-1');
        chart.yAxis[0].removePlotLine('plot-band-1');
        chart.xAxis[0].removePlotLine('plot-band-2');
        chart.yAxis[0].removePlotLine('plot-band-2');
    };
    
    var showThresholdOnChart = function(chart, thresholdValues, axis) {
        var warnThresholdBand;
        var criticalTresholdBand;

        if (thresholdValues.WarningValue == null && thresholdValues.CriticalValue == null)
            return;

        if (thresholdValues.ShowLabel != null)
            self.ShowLabel = thresholdValues.ShowLabel;

        // Remove all bands & lines
        clearThresholds(chart);

        var chartMax;
        var chartMin;

        if (axis === 'x') {
            chartMax = chart.xAxis[0].max;
            chartMin = chart.xAxis[0].min;
        } else {
            chartMax = chart.yAxis[0].max;
            chartMin = chart.yAxis[0].min;
        }

        // Threshold >= value
        if (thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.Greater || thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.GreaterOrEqual) {

            if (thresholdValues.WarningValue < chartMin) {
                thresholdValues.WarningValue = chartMin;
            }

            if (thresholdValues.CriticalValue < chartMin) {
                thresholdValues.CriticalValue = chartMin;
            }

            // Warning
            
            // If warning is higher than critical => incorrect state, we show only critical. 
            if (isValueInChartScale(thresholdValues.WarningValue, chartMin, chartMax) && thresholdValues.WarningValue < thresholdValues.CriticalValue) {

                var toUp = isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax) ? thresholdValues.CriticalValue : chartMax;

                warnThresholdBand = getWarningThresholdBand(thresholdValues.WarningValue, toUp);

                addBandToChart(chart, warnThresholdBand,axis);
            }

            // Critical
            if (isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax)) {

                criticalTresholdBand = getCriticalThresholdBand(thresholdValues.CriticalValue, chartMax);

                addBandToChart(chart, criticalTresholdBand,axis);
            }
        }
        
        // Threshold <= value
        else if (thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.Less || thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.LessOrEqual) {

            if (thresholdValues.WarningValue > chartMax) {
                thresholdValues.WarningValue = chartMax;
            }

            if (thresholdValues.CriticalValue > chartMax) {
                thresholdValues.CriticalValue = chartMax;
            }

            // Warning
            
             // If warning is smaller than critical => incorrect state, we show only critical. 
            if (isValueInChartScale(thresholdValues.WarningValue, chartMin, chartMax) && thresholdValues.WarningValue > thresholdValues.CriticalValue) {

                var toDown = isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax) ? thresholdValues.CriticalValue : chartMin;

                warnThresholdBand = getWarningThresholdBand(thresholdValues.WarningValue, toDown);

                addBandToChart(chart, warnThresholdBand, axis);
            }

            // Critical
            if (isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax)) {

                criticalTresholdBand = getCriticalThresholdBand(chartMin, thresholdValues.CriticalValue);

                addBandToChart(chart, criticalTresholdBand, axis);
            }
        }
        
        // Threshold == value
        else if (thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.Equal) {
            
            if (isValueInChartScale(thresholdValues.WarningValue, chartMin, chartMax)) {
                
                warnThresholdBand = getWarningThresholdBand(thresholdValues.WarningValue, thresholdValues.WarningValue);
                
                warnThresholdBand.value = thresholdValues.WarningValue; // change band to line.

                addLineToChart(chart, warnThresholdBand, axis);
            }

            if (isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax)) {
                
                criticalTresholdBand = getCriticalThresholdBand(thresholdValues.CriticalValue, thresholdValues.CriticalValue);

                criticalTresholdBand.value = thresholdValues.CriticalValue; // change band to line.
                
                addLineToChart(chart, criticalTresholdBand, axis);
            }          
        }
        
        // Threshold != value
        else if (thresholdValues.ThresholdOperator === thresholds.ThresholdOperatorEnum.NotEqual) {
            
            // Critical has greater priority than warning 
            if (isValueInChartScale(thresholdValues.CriticalValue, chartMin, chartMax)) {
                
                criticalTresholdBand = getCriticalThresholdBand(chartMin, chartMax);                

                addBandToChart(chart, criticalTresholdBand);

                addLineToChart(chart, {
                    value: thresholdValues.CriticalValue,
                    color: "white",
                    id: 'plot-line-1'
                }, axis);
            }
      
            else if (isValueInChartScale(thresholdValues.WarningValue, chartMin, chartMax)) {
                
                warnThresholdBand = getWarningThresholdBand(chartMin, chartMax);   
                
                addBandToChart(chart, warnThresholdBand,axis);

                addLineToChart(chart, {
                    value: thresholdValues.WarningValue,
                    color: "white",
                    id: 'plot-line-1'
                },axis);
            }
        } 
    };

    var onLoadSuccess = function(response) {
        showThresholdOnChart(self.chart, response,'y');
    };

    var onLoadError = function() {
    };


    this.loadThreshold = function() {
        // Threshold has to be enabled on chart and only one netobject can be on chart.
        
        if(self.chartSettings.showThreshold && self.chartOptions.threshold && self.chartSettings.netObjectIds.length === 1) {

		    var param = {
		        ThresholdName : self.chartOptions.threshold.name,
		        InstanceId : self.chartSettings.netObjectIds[0],
		        EntityType : self.chartOptions.threshold.entityType
		    };

            SW.Core.Services.callController("/api/Thresholds/GetChartThresholdValues", param, onLoadSuccess, onLoadError);
        }

    };

    this.showThresholds = function(baselineValues, axis) {
        showThresholdOnChart(self.chart, baselineValues,axis);
    };
};