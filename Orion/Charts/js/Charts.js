﻿SW.Core.Charts = SW.Core.Charts || {};
(function(charts) {

    charts.LoadingModeEnum = { "StandardLoading": 0, "DynamicLoading": 1, "DynamicLoadingFirst": 2 };

    Highcharts.setOptions({
        global: {
            useUTC: true
        }
    });
        
    var lang = Highcharts.getOptions().lang;
    var isPieChart = false;

    var adjustRangeBasedOnDataRange = function(range, dataRange) {
        if (dataRange) {
            if ((dataRange.dataMin) && (dataRange.dataMin > range.min)) {
                range.min = dataRange.dataMin;
            }
            if ((dataRange.dataMax) && (dataRange.dataMax < range.max)) {
                range.max = dataRange.dataMax;
            }
        }
    };

    var runXAxisSetExtremesCallbacks = function(e, chartAxis) {
        var axis = chartAxis || this;
        var chart = axis.chart;

        adjustRangeBasedOnDataRange(e, chart.chartDataRange);

        $.each(charts.xAxisSetExtremesCallbacks, function(index, callback) {
                callback(axis, e);
            });
    };
    
    var runXAxisAfterSetExtremesCallbacks = function (e, chartAxis) {
         var axis = chartAxis || this;
        var chart = axis.chart;

        adjustRangeBasedOnDataRange(e, chart.chartDataRange);

        $.each(charts.xAxisAfterSetExtremesCallbacks, function(index, callback) {
                callback(axis, e);
            });
    };

    var isPlaceholderString = function (input) {
        return typeof input == "string" && input.match(/^\$[{[].*?[\]}]$/) && input != "$[ZoomRange]";
    };

    charts.RunXAxisSetExtremesCallbacks = function (e, chartAxis) {
        runXAxisSetExtremesCallbacks(e, chartAxis);
    }

    charts.RunXAxisAfterSetExtremesCallbacks = function (e, chartAxis) {
        runXAxisAfterSetExtremesCallbacks(e, chartAxis);
    }
	
	var tooltipWrapperHeader = '<div id="highchartTooltip">';
	var tooltipWrapperFooter = '</div>';

    var basicConfig = {
        chart: {
            alignTicks: false,
            animation: true,
            zoomType: 'x'
        },

        title: {
            text: '',
            style: { //initializing css for further usage
            },
            margin: 15
        },

        subtitle: {
            style: {
                margin2: '20px'
            }
        },

        navigator: {
            enabled: true,
            margin: 10,
            baseSeries: 'navigator',
            xAxis: {
                dateTimeLabelFormats: lang.dateTimeLabelFormats
            }
        },

        scrollbar: {
            enabled: true
        },

        rangeSelector: {
            enabled: true,
            buttonTheme: {
                width: lang.zoomButtonWidth
            },
            buttons: [{
                type: 'minute',
                count: 60,
                text: lang.zoomOneHour
            }, {
                type: 'minute',
                count: 720,
                text: lang.zoomTwelveHours
            }, {
                type: 'day',
                count: 1,
                text: lang.zoomTwentyfourHours
            }],
            inputEnabled: false,
            inputEditDateFormat: lang.inputEditDateFormat,
            inputDateFormat: lang.inputDateFormat,
            inputStyle: {
                width: lang.rangeSelectorWidth
            }
        },

        credits: {
            enabled: false
        },

        loading: {
            labelStyle: {
                background: 'url("/Orion/images/loading_gen_small.gif") left center no-repeat',
                paddingLeft: '21px'
            }
        },

        legend: {
            enabled: false,
            verticalAlign: 'bottom',
            x: -200, // Should this be based on the width?
            borderWidth: 0
        },

        plotOptions: {
            series: {
                connectNulls: false,
                marker: {
                    enabled: true,
                    radius: 1
                }
            },
            column: {
                dataGrouping: {
                    approximation: 'average'
                },
                minPointLength: 2
            },
            line: {
                lineWidth: 1.5,
                states: {
                    hover: {
                        lineWidth: 3
                    }
                }
            },
            candlestick: {
                lineWidth: 1,
                lineColor: 'black',
                tooltip: {
                    pointFormat: '<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding-top: 4px;">{series.name}: </td><td style="border: 0px; font-size: 12px; padding-top: 4px;"><b>{point.low} - {point.high}</b></td></td></tr>'
                },
                dataGrouping: {
                    approximation: function (open, high, low, close) {
                        var min = low.length ? _.min(low) : (low.hasNulls ? null : undefined);
                        var max = high.length ? _.max(high) : (high.hasNulls ? null : undefined);
                        if (jQuery.isNumeric(min) || jQuery.isNumeric(max)) {
                            
                            // [open,high,low,close]
                            return [min, max, min, max];
                        }
                    }
                }
            }
        },

        tooltip: {
            positioner: SW.Core.Charts.Tooltip.positioner,
            positioningEnabled: false,
            yDecimals: 2,
            xDateFormat: lang.tooltipDateFormat,
            useHTML: true,
            backgroundColor: 'rgba(0,0,0,0)',
            valueDecimals: 2,
            borderWidth: 0,
            shadow: false,     
            wrapperHeader: tooltipWrapperHeader,
			wrapperFooter: tooltipWrapperFooter,
            footerFormat: '</table>' + tooltipWrapperFooter,
            headerFormat: tooltipWrapperHeader + '<table><tr><td columnspan="2" style="border: 0px;font-size: 11px;">{point.key}</td></tr>',
            pointFormat: '<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding-top: 4px;">{series.name}:&nbsp;</td><td style="border: 0px; font-size: 12px; padding-top: 4px;"><b>{point.y}</b></td></td></tr>' 
        },

        exporting: {
            enabled: false
        },
        
        chartLoadingOptions: {
            lazyLoadingDynamicSampling: false
        },

        xAxis: {
            ordinal: false,
            dateTimeLabelFormats: lang.dateTimeLabelFormats,
            events: {
                setExtremes: runXAxisSetExtremesCallbacks,
                afterSetExtremes: runXAxisAfterSetExtremesCallbacks
            }
        },
        
        yAxis: [{
            title: {
                margin: 1
            },
            labels: {
                align: 'right',
                x: -8
            }
        }],
        series: [],
        chartInitializers: [],
        chartPreInitializers: [],

        setTitleOverrides: function(showTitles, title, titleWidth, subtitle, subTitleWidth) {
            // if chart title is set to empty string, do not keep the default value if it is an untranslated ${placeholder}
            var titleEmpty = title.length === 0 && isPlaceholderString(this.title.text);
            if (showTitles === false || titleEmpty) {
                this.title.text = null;
                this.subtitle.text = null;
                return;
            }

            if ($('html').hasClass("sw-is-mozilla") || $('html').hasClass("sw-is-msie-10")) {
                titleWidth += 2;
            }

            if (title.length > 0) {
                this.title.text = title;
                this.title.style.width = titleWidth + 'px';
            }

            this.subtitle.text = isPlaceholderString(subtitle) ? null : subtitle;
            this.subtitle.style.width = subTitleWidth + 'px';
            var tmpEl = $('<span style="visibility: hidden;font-family: Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align:center; text-align: center; font-weight: bold;width: ' + subTitleWidth + 'px;">' + this.title.text + "</span>");
            $('#' + this.chart.renderTo).append(tmpEl);
            this.subtitle.y = tmpEl.outerHeight() + this.title.margin;
            if (!this.subtitle.text || (this.subtitle.text.trim() == "")) {
                this.subtitle.y = this.subtitle.y - 20; // substract half buttons height
            }
                
            tmpEl.remove();
        }
    };

   

    var basicYAxisConfig = {
        lineWidth: 1,
        labels: { },
        title: {
            margin: 40
        }
    };

    var basicSeriesConfig = {
        seriesOrder: 100
    };

    var getColorPaletteFromTheme = function(colorTheme, series) {
        var seriesCount = $.grep(series, function(x) { return !x.isCalculated; }).length;
        var colorPalette = colorTheme.colors[colorTheme.colors.length - 1];

        $.each(colorTheme.colors, function(index, palette) {
            if (palette.length >= seriesCount) {
                colorPalette = palette;
                return false;
            }
        });

        return colorPalette;
    };

    var handleSpecialSeries = function(dataResults, chartConfig, series, colorTheme) {
        switch (dataResults.TagName) {
        case "Trend":
            series.name = dataResults.Label;
            series.color = colorTheme.trendLine;
            series.dashStyle = colorTheme.treadLineStyle;
            series.isCalculated = true;
            break;
        case "Percentile":
            series.name = dataResults.Label;
            series.color = colorTheme.percentileLine;
            series.dashStyle = colorTheme.percentileLineStyle;
            series.isCalculated = true;
            break;
        case "Sum":
            series.name = lang.sumSeriesLabel;
            series.color = colorTheme.sumLine;
            series.dashStyle = colorTheme.sumLineStyle;
            series.isCalculated = true;
            break;
        case "RightSum":
            series.name = lang.rightSumSeriesLabel;
            series.color = colorTheme.rightSumLine;
            series.dashStyle = colorTheme.rightSumLineStyle;
            series.isCalculated = true;
            series.yAxis = 1;   // second y axis 
            break;
        }

        if (series.isCalculated) {
             if (dataResults.ComputedFrom) {
                 var computedTemplate = chartConfig.seriesTemplates[dataResults.ComputedFrom];
                 if (computedTemplate) {
                     series.name = computedTemplate.name + " " + dataResults.Label;
                 }
                 else {
                     series.name = dataResults.ComputedFrom + " " + dataResults.Label;
                 }
             }
        }
    };

    /// Highcharts expects a datapoint with a null value to have a data and then a null.  
    /// When the DataUrl returns the series it can't add the null so we'll add it here.
    /// I'm not sure which way performs better.  We add 4 nulls to handle the MinMax
    /// points (which are actually open/high/low/close points).
    var fixNullDataPoints = function(series) {
        var len = series.length;
        for (var i = 0; i < len; i++) {
            var point = series[i];
            if (point.length === 1) {
                point.push(null, null, null, null);
            }
        }
    };


    // Let modules define their custom formatters before this script. Sometimes it may 
    // be difficult to put module script after this script.
    charts.dataFormatters = charts.dataFormatters || { };
    charts.dataFormatters.unknown = function(value, axis, decimalPlaces) {
        if (typeof(decimalPlaces) !== 'undefined') {
            value = Highcharts.numberFormat(value, decimalPlaces);
        }
        return value + ' ' + (axis.unit || '');
    };
    //Math.log10 is not supported by IE11
	var log10 = function(x) {
        return Math.log(x) * Math.LOG10E;
    };
	// Param "scaleFactor" - value from which we need to calculate number of digits
	// Param "tickInterval" - difference between two labels in real value
	// Calculate number of digits needed for rounding based on scaleFactor and tickInterval
	charts.dataFormatters._getNumberOfDigits = function(scaleFactor, tickInterval) {
		// Difference between Math.log10(scaleFactor) and Math.log10(tickInterval).
		// Math.log10(X) returns to what power 10 must be raised in order to yield X (Math.log10(X)=Y, Math.pow(10, Y)= X).
		return parseInt(Math.floor(log10(scaleFactor) - Math.floor(log10(tickInterval))));
	};
    
    charts.dataFormatters._fromSizes = function(value, baseValue, sizes, floatDigits, tickInterval)
    {
        if (value === 0) return value + ' ' + sizes[0];

        var i = parseInt(Math.floor(Math.log( Math.abs(value)) / Math.log(baseValue)));
        i = Math.max(i, 0);
        i = Math.min(i, sizes.length - 1);

        var digits = 0;
        
        if (typeof floatDigits === 'undefined') {
            digits = (i === 0) ? 0 : 1;
        } else {
            if (value % baseValue !== 0) {
                digits = (i === 0) ? floatDigits : 1;
            }            
        }
		
		// OADP-716: We are calculating number of digits for rounding to avoid duplications within the yAxix labels.
		if (typeof tickInterval !== 'undefined') {
			digits = Math.max(this._getNumberOfDigits(Math.pow(baseValue, i), tickInterval), digits);
		}

        return String((value / Math.pow(baseValue, i)).toFixed(digits)).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) + ' ' + sizes[i];
    }

    var removeDoubleError = function(x)
    {
        return Math.round(x * 10000) / 10000; // there are problems with rounding with transfer between c#<->js.
        // we remove first but 5 numbers after decimal for computing the trendline, this should be enough.
    }
                            
    var getVisibleDataPoints = function (seriesOptions, event) {
        var newRangePoints = [], min = event.min, max = event.max;
        $.each(seriesOptions.data, function (dataIndex, srcPoint) {
            var px = srcPoint[0] || srcPoint['x'];
            (px >= min) && (px <= max) && newRangePoints.push(srcPoint);
        });

        return newRangePoints;
    }
    
    var removeEmptyDataPoints = function (dataPoints, event, addBorderPoints) {
        var nDataPoints = [];
        $.each(dataPoints, function () {
            var px = this[0] || this['x'],
                py = this[1] || this['y'];
            if (py * 1.0 === py) // skip empty ones
                nDataPoints.push([px, py]);
        });

        if (addBorderPoints) {
            nDataPoints.unshift([event.min, 0]);
            nDataPoints.push([event.max, 0]);
        }
        return nDataPoints;
    }

    // Method should compute percentile value from passed data in format data[index][pointValue]
    var calculatePercentile = function(data, percentage) {
        if (data.length == 0) {
            throw new Error("Source Data Series can not be empty");
        }
        
        if ((percentage > 100) || (percentage < 0)) {
            throw new Error("Percentage value must be between 0 and 100. Actual value: " + percentage);
        }
        
        var indexToReturn = (Math.ceil(data.length * (percentage / 100.0)));
        var dataValues = [];
        for (var i = 0; i < data.length; i++) {
            dataValues.push(data[i][1]);
        }
        
        dataValues = dataValues.sort(function(a, b) { return a - b; });
        indexToReturn = indexToReturn - 1;
        if ((indexToReturn <= dataValues.length) && (indexToReturn >= 0)) {
            return dataValues[indexToReturn];
        }
        
        return dataValues[0];
    };
    
   
    var generatePercentileFunction = function(xAxis, event) {
        var chart = null;
            
        $.each(xAxis.series, function(index, series) {
            var seriesOptions = series.options;
            if (seriesOptions.showPercentileLine) {
                chart = chart || series.chart; 
                    
                // ============== Beginning of the code for calculate percentile ============== 
                var newRangePoints = getVisibleDataPoints(seriesOptions, event);          
                var cPercentilePoints = removeEmptyDataPoints(newRangePoints, event, false);
                            
                var percentileUniqueId = 'Percentile' + seriesOptions.uniqueName;
                var percentileSeries = $.grep(xAxis.series, function (x) { return x.options.uniqueId === percentileUniqueId; });
                if (percentileSeries.length == 1) {
                    percentileSeries = percentileSeries[0];
                    var calculatedPercentileValue = 0;
                    try {
                        calculatedPercentileValue = calculatePercentile(cPercentilePoints, percentileSeries.options.percentage);
                    }
                    catch(Exception) {
                        return; // we will not continue with computation of percentile
                    }
                       
                    var percentileData = [];
                    for (var i = 0; i < cPercentilePoints.length; i++) {
                        percentileData.push([removeDoubleError(cPercentilePoints[i][0]), calculatedPercentileValue]);
                    }
                       
                    percentileSeries.setData(percentileData, false); //Redraw
                }

                // ============= End code for calculate percentile =================
            }
        }); 
    };
    var generateTrendFunction = function(xAxis, event) {
        var chart = null;
        var sentWebServiceCall = false;
        $.each(xAxis.series, function(index, series) {
            var seriesOptions = series.options;
            if (seriesOptions.showTrendLine) {
                chart = chart || series.chart;

                var newRangePoints = getVisibleDataPoints(seriesOptions, event);

                // calculate the trend data for the new range
                var trendPoints = [];

                var cTrendPoints = removeEmptyDataPoints(newRangePoints, event, true);
                var trendUniqueId = 'Trend' + seriesOptions.uniqueName;
                var trendSeriesR = $.grep(xAxis.series, function (x) { return x.options.uniqueId === trendUniqueId; });

                if (!trendSeriesR[0]) {
                    return;
                }

                var paramsWebService = { request: cTrendPoints, sampleSizeInMinutes: trendSeriesR[0].options.SampleSizeInMinutes };
                sentWebServiceCall = true;
                charts.callWebService("/Orion/NetPerfMon/NodeChartData.asmx/CalculateTrendLine", paramsWebService, function(results) {
                    var data = results;

                    trendPoints = data; // should have only [x1,y1],[x2,y2], others will be interpolated (because of rounding errors, the lines otherwise doesn't match)

                    var interpolateTrendPoints = function(newPoints, range) {
                        newPoints[0][1] = removeDoubleError(newPoints[0][1]);
                        newPoints[1][1] = removeDoubleError(newPoints[1][1]);
                        newPoints[1][0] = removeDoubleError(newPoints[1][0]);
                        newPoints[0][0] = removeDoubleError(newPoints[0][0]);

                        var a = ((newPoints[0][1] - newPoints[1][1]) * 1.0) / (newPoints[0][0] - newPoints[1][0]); // parametrical line
                        var b = newPoints[0][1] - a * newPoints[0][0];

                        var rv = [];
                        for (var i = 0; i < newRangePoints.length; ++i) {
                            var x = newRangePoints[i][0];
                            var y = a * x + b;
                            if (y >= 0) { // FB108110 we don't want negative values for trend line
                                rv.push([x, y]);
                            } else {
                                rv.push([x, null]);
                            }
                        }
                        return rv;
                    };
                    var trendUniqueId = 'Trend' + seriesOptions.uniqueName;

                    if (typeof(xAxis.series) != "undefined") {
                        var trendSeries = $.grep(xAxis.series, function (x) { return x.options.uniqueId === trendUniqueId; }); // this find trend series
                        if (trendSeries.length == 1) {
                            trendSeries = trendSeries[0];
                            trendSeries.setData(interpolateTrendPoints(trendPoints, newRangePoints), true); //Third param causes redraw when webservice comes back. 
                        }
                    }
                });
            }
        });


        if (sentWebServiceCall == false) {
            //If the webservice isnt going to redraw then we need to.
            xAxis.redraw();
        }
    };

    charts.AdjustYMaxValueForNullOrZeroDataPointSeries = function(chart, event) {
       if (isPieChart)
          return;
        
        // Extreme was set in config, so we don't need it adjust YmaxValue for Zero datapoints.
        if (chart.chartOptionsWithOverride && chart.chartOptionsWithOverride.yAxis[0].max) {
            return;
        }

        var zeroDataPointSeries = true;
        var newSeries = [];
        
        $.each(chart.series, function(index, series) {
             var seriesOptions = series.options;
              
             var newRangePoints = getVisibleDataPoints(seriesOptions, event); 
             var pointWithoutEmpty = removeEmptyDataPoints(newRangePoints, event, false);
     
             newSeries.push({data: pointWithoutEmpty});
         });

         if (newSeries.length > 0) {
             zeroDataPointSeries = containsAllSeriesOnlyZeroValuesOrNullDataPoints(newSeries);
             if (zeroDataPointSeries) {
                 chart.yAxis[0].setExtremes(0, 1);
             } else {
                  chart.yAxis[0].setExtremes(null, null, true, false);
             }
         }
    };

    charts.generateCalculatedSeries = function(xAxis, event) {
        //Ordering of these functions is important.  generatePercentileFunction relys on generateTrendFunction to redraw when both are finished.
        generatePercentileFunction(xAxis, event);
        generateTrendFunction(xAxis, event);   
    };
    
    

    var processSubTitleMacro = function(xAxis, event) {
        var zoomRange = Highcharts.dateFormat(lang.zoomRangeDateTimeFormat, event.min) + ' - ' + Highcharts.dateFormat(lang.zoomRangeDateTimeFormat, event.max);
        var processedSubtitle = xAxis.options.originalSubtitle.replace('$[ZoomRange]', zoomRange);
        xAxis.chart.setTitle(null, { text: processedSubtitle });
    };

    var loadDataDynamically = function(xAxis, event) {
        charts.DynamicLoader.loadData(xAxis.chart, event);
    };

    // We only get a single event handler for the X Axis' setExtremes event.  Since 
    // multiple modules might want to catch this event, we'll just call all the callbacks
    // defined here.  A module just need to add a callback to this array.  The callback
    // is called with the X Axis object and the event object passed to the event.
    charts.xAxisSetExtremesCallbacks = charts.xAxisSetExtremesCallbacks || [];
    charts.xAxisSetExtremesCallbacks.push(processSubTitleMacro);    

    // calbacks which will be fired after the final min and max values are compured and corrected for minRange
    charts.xAxisAfterSetExtremesCallbacks = charts.xAxisAfterSetExtremesCallbacks || [];
    charts.xAxisAfterSetExtremesCallbacks.push(loadDataDynamically); 

    charts.dataFormatters.bytes = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1024;
        var sizes = lang.byteUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };
    
    charts.dataFormatters.bps = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1024;
        var sizes = lang.bpsUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };

    charts.dataFormatters.bps1000 = function (value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1000;
        var sizes = lang.bpsUnitSizes;
        
        return charts.dataFormatters._fromSizes(value,
            baseValue,
            sizes,
            axis.floatDigits,
            (typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined')
            ? this.axis.tickInterval
            : undefined);
    };

	charts.dataFormatters.Bps = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1024;
        var sizes = lang.BpsUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };

    charts.dataFormatters.Bps1000 = function (value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1000;
        var sizes = lang.BpsUnitSizes;

        return charts.dataFormatters._fromSizes(value,
            baseValue,
            sizes,
            axis.floatDigits,
            (typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined')
            ? this.axis.tickInterval
            : undefined);
    };

    charts.dataFormatters.pps = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1024;
        var sizes = lang.ppsUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };
    
    charts.dataFormatters.packets = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1000;
        var sizes = lang.packetsUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };

    charts.dataFormatters.empty = function(value, axis) {
        var baseValue = typeof axis.baseConversionValue !== "undefined" ? axis.baseConversionValue : 1000;
        var sizes = lang.emptyUnitSizes;
        
        return charts.dataFormatters._fromSizes(value, 
			baseValue, 
			sizes, 
			axis.floatDigits, 
			( typeof this.axis !== 'undefined' && typeof this.axis.tickInterval !== 'undefined' ) 
				? this.axis.tickInterval
				: undefined);
    };

    var findFormatterForUnit = function(unit) {
        var dataFormatter = charts.dataFormatters[unit];

        if (typeof(dataFormatter) !== 'function') {
            dataFormatter = charts.dataFormatters['unknown'];
        }

        return dataFormatter;
    };

    var getTooltipFormatterForUnit = function(unit, tooltipOptions) {
        var formatter = findFormatterForUnit(unit);
        if (formatter == charts.dataFormatters['unknown']) {
            return formatter;
        }

        var overrideSettings = {};
        if (typeof tooltipOptions.floatDigits !== "undefined") {
            overrideSettings.floatDigits = tooltipOptions.floatDigits;
        }
        if (typeof tooltipOptions.baseConversionValue !== "undefined") {
            overrideSettings.baseConversionValue = tooltipOptions.baseConversionValue;
        }

        return function (value, axis) {
            axis = $.extend(axis, overrideSettings);
            return typeof value === "undefined" ? "" : formatter(value, axis);
        };
    };

    // check each axis to see if any data is being clipped because a max value was set on the axis
    // If we are clipping and yAxis.extendExtremesForData is not false, then extend the axis range
    // to include all the data.
    var updateYAxisExtremes = function(chart) {
        $.each(chart.yAxis, function(index, axis) {
            var extendExtremesForData = true;
            if (typeof(axis.options.extendExtremesForData) !== 'undefined') {
                extendExtremesForData = axis.options.extendExtremesForData;
            }

            if (extendExtremesForData) {
                var extremes = axis.getExtremes();

                var newMin = extremes.min;
                var newMax = extremes.max;
                var resetExtremes = false;

                if (extremes.dataMin !== null && extremes.dataMin < extremes.min) {
                    newMin = extremes.dataMin - Math.abs(extremes.dataMin) * axis.options.maxPadding;
                    resetExtremes = true;
                }

                if (extremes.dataMax !== null && extremes.dataMax > extremes.max) {
                    newMax = extremes.dataMax + Math.abs(extremes.dataMax) * axis.options.maxPadding;
                    resetExtremes = true;
                }

                // handle special case when original min was 0 and all data are negative - if that's the case set the max to 0, so that the behavior is
                // the same as for positive values only chart
                if (resetExtremes && extremes.min === 0 && extremes.dataMax < 0) {
                    newMax = 0;
                }

                if (resetExtremes) {
                    axis.setExtremes(newMin, newMax, true, false);
                }
            }

        });
    };

    var matchNavigatorAxisToMainAxis = function(chartConfig) {
        if (chartConfig.series.length === 0)
            return;

        // find the naviagator series.
        var navigatorSeries = $.grep(chartConfig.series, function(x) { return x.id === 'navigator'; });
        if (navigatorSeries.length > 0)
            navigatorSeries = navigatorSeries[0];
        else {
            navigatorSeries = chartConfig.series[chartConfig.navigator.baseSeries];
        }

        if (navigatorSeries === null) return;

        var axisIndex = navigatorSeries.yAxis || 0;
        var yAxis = chartConfig.yAxis[axisIndex];

        chartConfig.navigator.yAxis = chartConfig.navigator.yAxis || { };
        chartConfig.navigator.yAxis.min = yAxis.min;
        chartConfig.navigator.yAxis.max = yAxis.max;
        
        if (navigatorSeries.type === 'column') {
            // column charts will always go all the way down to zero.  If the main chart will show a column series, we need to
            // force the navigator to also use a minimum value of zero so that the scaling of the navigator chart matches
            // the scaling of the main chart.
            chartConfig.navigator.yAxis.min = yAxis.min || 0;
        }
    };


    // This is mainly for debuging...
    charts.getBasicChartConfig = function() {
        return basicConfig;
    };

    charts.getChartTimeFormat = function (tickPos) {
        var chartFormat = '';
        var timeSep = '';
        var timePattern = Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern;
	
        if (tickPos === 'second') {
            timePattern = Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern;
        }
	
        var timeParts = timePattern.split(Sys.CultureInfo.CurrentCulture.dateTimeFormat.TimeSeparator);

        $.each(timeParts, function(index, value) { 
            var mappedVal = lang.dateTimeFormatMapping[value];
            if (mappedVal)
            {
                chartFormat += timeSep + mappedVal;
            }
            else
            {
                //get format with PM/AM identifier
                var values = value.split(' ');
                if (values.length === 2)
                {
                    var mappedVal1 = lang.dateTimeFormatMapping[values[0]];
                    var mappedVal2 = lang.dateTimeFormatMapping[values[1]];
				
                    if (mappedVal1 && mappedVal2)
                       chartFormat += timeSep + mappedVal1 + ' ' + mappedVal2;
                }
            }
            timeSep = Sys.CultureInfo.CurrentCulture.dateTimeFormat.TimeSeparator;
        });

        return chartFormat;
    };

    charts.basicChartConfig = function(config, renderTo, chartOptionsOverride) {
        var configOverride = typeof(chartOptionsOverride) === "undefined" ? null : chartOptionsOverride;
        // Handle charts using custom formatter for their content.
		// If chart uses only formatter, is most probably formats just content, not border.
		// In this case, add border to tooltip. If chart needs to provide complete formatting of tooltip,
		// it should set "formatWholeTooltip" to "true".
		if (config.tooltip && config.tooltip.formatter && !config.tooltip.formatterWrapped && !config.tooltip.formatWholeTooltip)
		{
			var originalFormatter = config.tooltip.formatter;
			config.tooltip.formatter = function() {
			    var point = this.point || this.points[0],
				series = point.series;

				var result = '';
				var formattedData = originalFormatter.apply(this);
				var usesDefaultWrapper = (formattedData.indexOf(basicConfig.tooltip.wrapperHeader) == 0);
				if (usesDefaultWrapper)
					return formattedData;
				
				// formatted data are not wrapped in default wrapper 
				// -> pre-Galaga chart 
				// -> wrap in default wrapper to get correct layout
				result += basicConfig.tooltip.wrapperHeader;
				result += formattedData;
				result += basicConfig.tooltip.wrapperFooter;
				
			    return result;
			}
			config.tooltip.formatterWrapped = true;
		}

		var newConfig = $.extend(true, {}, basicConfig, config, configOverride);



        //get time format based on browser locale
        $.each(['second', 'minute', 'hour'], function(index, value) { 
            var chartFormat = charts.getChartTimeFormat(value);
            newConfig.xAxis.dateTimeLabelFormats[value] = chartFormat;
            newConfig.navigator.xAxis.dateTimeLabelFormats[value] = chartFormat;
        });

        var newYAxis = [];
        $.each(newConfig.yAxis, function (i, axis) {

            if (typeof config !== 'undefined' && typeof config.isReporting !== 'undefined' && config.isReporting) {
                axis.floatDigits = 1;
            }
            
            var x = $.extend(true, { }, basicYAxisConfig, axis);

            var dataFormatter = findFormatterForUnit(x.unitFormatter || x.unit);

            x.labels.formatter = function() { return dataFormatter.call(this, this.value, x); };
            newYAxis.push(x);
        });
        newConfig.yAxis = newYAxis;
        newConfig.chart.renderTo = renderTo;

        return newConfig;
    };

    charts.callWebService = function(url, params, onSuccess, onError) {
        var paramString = JSON.stringify(params);

        if (paramString.length === 0) {
            paramString = "{}";
        }
              
        $.ajax({
            type: "POST",
            url: url,
            data: paramString,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(msg) {
                onSuccess(msg.d);
            },
            error: onError
        });
    };

    charts.turnOffChartAnimations = function(config) {
        config.chart.animation = false;
        $.each(config.series, function(index, series) {
            series.animation = false;
        });
    };

    charts.createChart = function(config, webServiceParams, onCreate) {

        // Allow modules to run custom code right before chart is created.
        // This may be used for example to dynamically size chart based on loaded data.
        $.each(config.chartPreInitializers, function(index, callback) { callback(config); });

        // Allow modules use Highcharts.Chart instead of Highcharts.StockChart
        var constructor = (config.highchartsChartType === 'chart') ? 'Chart' : 'StockChart';

        var newChart = new Highcharts[constructor](config, function(chart) {
            // Store the chart object in the DOM's data so we can grab it later. 
            $('#' + config.chart.renderTo).addClass("hasChart").data(chart);

            updateYAxisExtremes(chart);

            $.each(config.chartInitializers, function(index, callback) { callback(chart, webServiceParams); });

            if (typeof(onCreate) === "function") {
                onCreate(chart);
            }
            
            // Stores updated version of chart into DOM's data. ChartInitilaizers and onCreate event could change chart, so we need restore it.
            $('#' + config.chart.renderTo).addClass("hasChart").data(chart);
        });

        return newChart;
    };

    var createEmptyLoadingChart = function(chartSettings, chartOptions) {

        if ($('html').hasClass('sw-is-msie-7') || $('html').hasClass('sw-is-msie-8')) {
            // IE7 and 8 renders to slowly to let the loading chart even show.  Let's just show loading without the chart
            var loadingText = Highcharts.getOptions().lang.loading;
            var loadingDiv = $('<div class="chartLoadingMessage">');
            var loading = $('<span/>').text(loadingText).appendTo(loadingDiv);
            $.extend(loading[0].style, basicConfig.loading.labelStyle);

            $('#' + chartSettings.renderTo).append(loadingDiv);
            return;
        }

        var endDate = new Date();
        var startDate = new Date();
        startDate.setDate(startDate.getDate() - 1);

        var chartConfig = charts.basicChartConfig(chartOptions, chartSettings.renderTo);
       
        chartConfig.navigator.enabled = false;
        chartConfig.scrollbar.enabled = false;
        chartConfig.rangeSelector.inputStyle.color = Highcharts.theme.rangeSelector.inputStyle.backgroundColor;
        chartConfig.series.push({
            data: [[startDate.getTime(), null], [endDate.getTime(), null]]
        });
        chartConfig.setTitleOverrides(chartSettings.showTitle, chartSettings.title, chartSettings.titleWidth,  
            chartSettings.subtitle, chartSettings.subTitleWidth);

        charts.createChart(chartConfig, null, function(c) {
            c.showLoading();
        });
    };

    charts.initializeStandardChart = function(chartSettings, chartOptions, onFinish) {
        if (typeof(chartSettings.dataUrl) !== "string") {
            return;
        }
        chartOptions = $.extend(chartOptions, { ChartEditUrl: chartSettings.ChartEditUrl });
        // Create empty chart immediatelly so that "loading..." is visible also before web request is made 	
        createEmptyLoadingChart(chartSettings, chartOptions);
        doChartLoad( chartSettings, chartOptions, onFinish);
    };

    var doChartLoad = function(chartSettings, chartOptions, onFinish) {
        try {
            var webServiceParams = createWebServiceParams(chartSettings, chartOptions);
            initialChartLoad(chartSettings, chartOptions, webServiceParams, onFinish);
        } catch(exception) {
            //We want to be sure onFinish() is loaded to continue the loading chain in case there was a problem with one chart.
            if( onFinish )
                onFinish();
            showChartErrorMessage(exception, chartSettings.renderTo);
        }
    };

    var createWebServiceParams = function(chartSettings, chartOptions) {
        var params = {
                request: {
                    NetObjectIds: chartSettings.netObjectIds,
                    SampleSizeInMinutes: chartSettings.sampleSizeInMinutes,
                    DaysOfDataToLoad: chartSettings.timespanInDays,
                    CalculateTrendLine: chartSettings.CalculateTrendLine,
                    CalculateSum: chartSettings.CalculateSum,
                    Calculate95thPercentile: chartSettings.Calculate95thPercentile,
                    StartTime: chartSettings.DateFromUtc,
                    EndTime: chartSettings.DateToUtc,
                    AllSettings: chartSettings,
                    SeriesTypes: getSeriesTypes(chartOptions)
                }
        };

        params = charts.DynamicLoader.updateWebServiceParams(params, chartSettings);

        if (jQuery.isFunction(chartSettings.onWebServiceParamsCreated)) {
            chartSettings.onWebServiceParamsCreated(params);
        }

        return params;
    };

    var beginLoadingOnCharts = function(remaining) {
        
        if ($('html').hasClass('sw-is-msie-7') || $('html').hasClass('sw-is-msie-8')) {
            var msToWaitBetweenCharts = 100;
            if (remaining.length == 0) {
                return;
            } else {
                var head = remaining[0];
                var tail = remaining.slice(1, remaining.length);
               
                doChartLoad(head[0], head[1], function() {
                    setTimeout(function() {
                        beginLoadingOnCharts(tail);
                    }, msToWaitBetweenCharts);
                });
            }
        } else {
            //For all other browsers load all charts without waiting for them to finish.
            $.each(remaining, function() {
                doChartLoad(this[0], this[1]);
            });
        }
    };

    charts.runChartLoading = function(chartList) {
        beginLoadingOnCharts(chartList);
    };    


    charts.initializeEmptyChart = function(chartSettings, chartOptions) {
        chartOptions = $.extend(chartOptions, { ChartEditUrl: chartSettings.ChartEditUrl });
        // Create empty chart immediatelly so that "loading..." is visible also before web request is made
        createEmptyLoadingChart(chartSettings, chartOptions);
    };

    var ClearComputedFromForSingleSeries = function(dataSeries) {
        var nTrend = 0;
            var nPercentile = 0;
            for (var i = 0; i < dataSeries.length; i++) {
                switch(dataSeries[i].TagName) {
                   case "Trend":
                      nTrend++;
                      break;
                   case "Percentile":
                      nPercentile++;
                      break; 
                }
            }
            
            if ((nTrend == 1) || (nPercentile == 1))
            {
                 for (var i = 0; i < dataSeries.length; i++)
                 {
                    switch(dataSeries[i].TagName) {
                        case "Trend":
                           if (nTrend == 1) {
                               dataSeries[i].ComputedFrom = null;
                           }
                           
                           break;
                        case "Percentile":
                            if (nPercentile == 1) {
                               dataSeries[i].ComputedFrom = null;
                            }
                    }
                 }
            }
    };
    var showChartErrorMessage = function(message, chartId) {
        var maxExceptionLength = 40;
        if (message.length > maxExceptionLength)
            message = message.substring(0, maxExceptionLength) + '...';

        var errmsg = '<div class="sw-suggestion sw-suggestion-fail">';
        errmsg += '<span class="sw-suggestion-icon"></span>';
        errmsg += '@{R=Core.Strings;K=WEBJS_OJ0_1;E=js}';
        errmsg += '<p>' + message + '<p>';
        errmsg += '</div>';
        $('#' + chartId).html(errmsg).height('auto');
    };

    var findClosestResourceWrapper = function($actElement) {
        if ($actElement.hasClass("ResourceWrapper")) {
	         return $actElement;
        }

        if ($actElement.parents().length > 0)
           return findClosestResourceWrapper($actElement.parent());

        return $actElement;
    };
    
    // method return true in the case, that minimally one seriesTemplates defines pie chart type
    var isPieChartType = function(chartConfig) {
        for(key in chartConfig.seriesTemplates) {
            if (chartConfig.seriesTemplates.hasOwnProperty(key)) {
                if (chartConfig.seriesTemplates[key].type == "pie")
                {
                    return true;
                }
            }
        }
        
        return false;
    };

    var getSeriesTypes = function(chartOptions) {
        var types = [];

        for (key in chartOptions.seriesTemplates) {
            if (chartOptions.seriesTemplates.hasOwnProperty(key)) {
                types.push(chartOptions.seriesTemplates[key].type);
            }
        }
        return $.unique(types);
    };
    
    var tooltipPositioner = function (labelWidth, labelHeight, myPoint) {
          return { 
                x: (myPoint.plotX-labelWidth/2) > 0 ? (myPoint.plotX-labelWidth/2) : 0,
                y: (myPoint.plotY-labelHeight/2) > 0 ? (myPoint.plotY-labelHeight/2): 0 
          };
    };
    
    // <summary>Method responsible for computing maximal possible width of serie label in tooltip </summary>
    // <param name="chartEl">Element wrapped as jQuery object into which chart is rendered</param>
    // <param name="seriesConfig">Current configuration of given data serie for which I compute maxWidth</param>
    // <param name="maxSerieWidth">Return value of this method can't be lesss than maxSerieWidth value</param>
    // <param name="isPieChart">True in the case that I compute it for pie chart type</param>
    // <returns>maxSerieWidth</returns>
    var getMaxSerieWidth = function($chartEl, seriesConfig, maxSerieWidth, isPieChart) {
          var tmpEl = $('<span style="postion:absolute;white-space: nowrap; font-size: 15px; width: auto;">' + seriesConfig.name + "</span>");                           
          $chartEl.append(tmpEl);
          var tmpWidth = tmpEl.outerWidth();
          tmpEl.remove();
          if (tmpWidth > maxSerieWidth)
              maxSerieWidth = tmpWidth;
                    
          if (isPieChart) {
              if (seriesConfig.data)  {
                  for (var i =0; i < seriesConfig.data.length; i++) {
                      if(seriesConfig.data[i].name) {
                             tmpEl = $('<span style="postion:absolute;white-space: nowrap; font-size: 15px; width: auto;">' + seriesConfig.data[i].name + "</span>");
                             $chartEl.append(tmpEl);
                             var tmpWidth = tmpEl.outerWidth();
                             tmpEl.remove();
                             if (tmpWidth > maxSerieWidth)
                                  maxSerieWidth = tmpWidth;
                      }
                  }
              }
          } 
          
          return maxSerieWidth; 
    };

    var AddAnnotationLines = function(chartSettings, colorTheme) {
         var plotLinesA = [];
         if (chartSettings.CustomPropertyKeys)
         {
            for (var i=0; i < chartSettings.CustomPropertyKeys.length; i++) 
            {
                plotLinesA.push({
                            value : chartSettings.CustomPropertyKeys[i].Marker,
                            color: chartSettings.AnnotationLineColor ? chartSettings.AnnotationLineColor : colorTheme.annotationLine,
                            dashStyle: colorTheme.annotationLineStyle,
                            width: 1,
                            zIndex: 5,
                            label: { 
                                   text: chartSettings.CustomPropertyKeys[i].Annotation, 
                                   align: 'right', 
                                   y: -2, 
                                   x: -2,
                                   style: {
                                        color: chartSettings.AnnotationLineColor ? chartSettings.AnnotationLineColor : colorTheme.annotationLine,
                                        fontWeight: 'bold',
                                        fontSize: '75%'
                                   } 
                            }
                 });
            } 
         } 
        
         return plotLinesA;
    };

    var getMinMaxFromSeriesData = function(dataSeries) {
        //Method expects ordered dataSeries.
        var minDate = null;
        var maxDate = null;
        for (var i = 0; i < dataSeries.length; i++) {
            var currentSeries = dataSeries[i];
            if (currentSeries.Data.length != 0) {
                var currentMinDate = currentSeries.Data[0].x || currentSeries.Data[0][0];
                var currentMaxDate = currentSeries.Data[currentSeries.Data.length - 1].x || currentSeries.Data[currentSeries.Data.length - 1][0];

                if (minDate == null || currentMinDate < minDate) {
                    minDate = currentMinDate;
                }
                if (maxDate == null || currentMaxDate > maxDate) {
                    maxDate = currentMaxDate;
                }
            }
        }
        return { dataMin: minDate, dataMax: maxDate };
    };

    var containsAllSeriesOnlyZeroValuesOrNullDataPoints = function (series) {
        var serieWithPoint = _.find(series || [], function(serie){
            var point = _.find(serie.data || [], function(point){
                if (!$.isArray(point) && !$.isPlainObject(point))
                    return false;

                var py = point[1] || point['y'];
                return (py * 1.0 === py) && (py != 0);
            });
            return (point !== undefined);
        });

        return (serieWithPoint === undefined);
    };

    var initialChartLoad = function(chartSettings, chartOptions, webServiceParams, onFinish) {
        var chartOnError = function(err) {
            var chartConfig = charts.basicChartConfig(chartOptions, chartSettings.renderTo, { });

            try {
                var errorMessage;
                if (err.status === 500) {
                    errorMessage = err.statusText;
                } else {
                    var response = JSON.parse(err.responseText);
                    errorMessage = response.Message; //.Net Error Message. We dont need to localize.
                }
                showChartErrorMessage(errorMessage, chartConfig.chart.renderTo);
            } catch(ex) {
                // compilation failure
            }

            if(onFinish)
                onFinish();
        };
        
        // <summary>Method performs html encoding.</summary>
        // <param name="value">Input which we want to encode.</param>
        // <result>value with encoded html formating elements</result>
        charts.htmlEncode = function(value) {
            value = value || '';
            value = value.replace(/\n/g, "%%NL%%");
            value = value.replace(/\t/g, "%%TAB%%");
            value = value.replace(/&/g, "%%AMP%%");
            var res = $('<div/>').text(value).html();
            return res.replace(/%%AMP%%/g, "&"); // we don't want to encode & character
        };
        
        // <summary>Method performs html decoding. Replace e.g. &lt; by < and so on.</summary>
        // <param name="value">Input which we want to decode.</param>
        // <result></result>
        charts.htmlDecode = function(value) {
            value = value || '';
            value = $('<div/>').html(value).text();
            value = value.replace(/%%NL%%/g, "\n");
            return  value.replace(/%%TAB%%/g, "\t");
        };

        
        charts.callWebService(chartSettings.dataUrl, webServiceParams, function(results) {
              try {
                var dataSeries = results.DataSeries;
                var percentile = results.Percentage;

                var chartConfig = charts.basicChartConfig(chartOptions, chartSettings.renderTo, results.ChartOptionsOverride);
                chartConfig.setTitleOverrides(chartSettings.showTitle, charts.htmlEncode(chartSettings.title), chartSettings.titleWidth,
                    chartSettings.subtitle, chartSettings.subTitleWidth);

                if (chartSettings.IsPrintable || chartSettings.inlineRendering) {
                    var plotOptions = {
                        plotOptions: {
                            series: {
                                enableMouseTracking: false,
                                animation: false,
                                stickyTracking: false
                            }
                        }
                    };

                   chartConfig = $.extend(true, chartConfig, plotOptions);
                }
                
                isPieChart = isPieChartType(chartConfig);    

                var colorThemeName = chartSettings.colorTheme || 'standard';
                var colorTheme = charts.ColorThemes[colorThemeName] || charts.ColorThemes['standard'];

                var annotationLines = AddAnnotationLines(chartSettings, colorTheme);    

                var legendInitializer = charts.Legend[chartSettings.legendInitializer];
                if (typeof(legendInitializer) === "function")
                    chartConfig.chartInitializers.push(legendInitializer);
                  
                var foundNavigatorSeries = false;

                ClearComputedFromForSingleSeries(dataSeries);

                var maxSerieWidth = '';
                var chartEl = $('#' + chartConfig.chart.renderTo);
                var closestResourceWrapper = findClosestResourceWrapper(chartEl);
                
                if (!closestResourceWrapper.parent().hasClass("ResourceWrapper")) { // isn't resource in Custom object resource?
                    // some resource such as Hardware health overview using floating and if there is not clear element it causing issues
                    // and also solarwinds logo which use floating sometimes isn't good placed
                    var lastDiv = closestResourceWrapper.find("div:last");
                    var lastDivCssClear = lastDiv.css("clear");
                    if (lastDivCssClear != "both") {
                        closestResourceWrapper.append('<div style="clear: both; height:1px"></div>');
                    }

                    closestResourceWrapper.css("overflow-x", "visible");
                }

                var bEmptyDataSeries = true;

                var fDisplayNoDataMessage = function (requestResult) {
                    var helpUrl = (requestResult.NoDataUrl) ? requestResult.NoDataUrl : SW.Core.KnowledgebaseHelper.getKBUrlWithLang('4114');
                    var noDataHtml = (requestResult.NoDataHtml) ? requestResult.NoDataHtml.replace("\\\'", "\\\\\\'") : '<center>@{R=Core.Strings;K=WEBJS_PS0_20;E=js}</center><div><center><a href="' + helpUrl + '" target="_blank" rel="noopener noreferrer">@{R=Core.Strings;K=WEBJS_PS0_19;E=js}</a></center></div>';
                    var tmpDiv = '<div class="chartDataNotAvailableArea"><center><div id="chartDataNotAvailable"><table style="height: 100%"><tr><td style="vertical-align: middle">' + noDataHtml + '</td></tr></table></div></center></div>';
                    var tmpDiv2 = $(tmpDiv);
                    chartEl.parent().parent().css("overflow-y", "hidden");
                    $('#' + chartConfig.chart.renderTo).append(tmpDiv2);
                    displaySwisfPartialErrorMessageIfThrown(requestResult, tmpDiv2);
                };

                
                for (var i = 0; i < dataSeries.length; i++) {
                    var currentSeries = dataSeries[i];
                    var template = chartConfig.seriesTemplates[currentSeries.TagName] || { };
                    var seriesConfig = $.extend({ }, basicSeriesConfig, template);
                    
                    seriesConfig.data = currentSeries.Data;
                    seriesConfig.showTrendLine = currentSeries.ShowTrendLine;
                    seriesConfig.customProperties = currentSeries.CustomProperties;
                    seriesConfig.showPercentileLine = currentSeries.ShowPercentileLine;

                    seriesConfig.uniqueId = currentSeries.TagName + currentSeries.NetObjectId;
                    
                    // UniqueName is like uniqueId, but is extended of series name and label, needed for pair series with calculated lines (trendline, percentile, ...)
                    seriesConfig.uniqueName = seriesConfig.uniqueId;

                    if (currentSeries.Label && currentSeries.Label.length > 0) {
                        if (seriesConfig.name) {
                            seriesConfig.name = seriesConfig.name + ' ' + charts.htmlEncode(currentSeries.Label);
                        } else {
                            seriesConfig.name = charts.htmlEncode(currentSeries.Label);
                        }
                        seriesConfig.uniqueName += seriesConfig.name;
                    }
                                       
                    handleSpecialSeries(currentSeries, chartConfig, seriesConfig, colorTheme);
                    fixNullDataPoints(seriesConfig.data);

                    if (seriesConfig.id === 'navigator')
                        foundNavigatorSeries = true;

                    // adding series trend lines (concequent will be also added legend)
                    if (seriesConfig.data.length >= 1) {
                        chartConfig.series.push(seriesConfig); // showing chart data
                        if (seriesConfig.showTrendLine) {

                            // Add a series for the trendline
                            var trendConfig = $.extend({ }, seriesConfig);
                            trendConfig.uniqueId = trendConfig.uniqueName = 'Trend' + seriesConfig.uniqueName;
                            trendConfig.showTrendLine = false;
                            trendConfig.type = 'line';

                            if (currentSeries.ComputedFrom != null) {
                                trendConfig.name = String.format("@{R=Core.Strings;K=Charts_YK0_77; E=js}", seriesConfig.name); // localize this
                            } else {
                                trendConfig.name = "@{R=Core.Strings;K=WEBJS_PS0_18; E=js}";
                            }

                            trendConfig["SampleSizeInMinutes"] = webServiceParams.request.SampleSizeInMinutes;
                            trendConfig.color = colorTheme.trendLine; //'#f00';// -- colorTheme.trendLine; // line color now it is red
                            trendConfig.dashStyle = colorTheme.treadLineStyle;
                            trendConfig.isCalculated = true;
                            trendConfig.data = [];

                            chartConfig.series.push(trendConfig);
                        }

                        // And also Percentile line
                        if (seriesConfig.showPercentileLine) {
                            // Add a serie for the percentileline
                            var percentConfig = $.extend({ }, seriesConfig);
                            percentConfig.uniqueId = percentConfig.uniqueName = 'Percentile' + seriesConfig.uniqueName;
                            percentConfig.showPercentileLine = false;
                            percentConfig.type = 'line';

                            if (currentSeries.ComputedFrom != null) {
                                percentConfig.name = String.format("@{R=Core.Strings; K=WEBJS_PS0_21; E=js}", seriesConfig.name, percentile);
                            } else {
                                percentConfig.name = String.format("@{R=Core.Strings; K=WEBJS_PS0_22; E=js}", percentile);
                            }

                            percentConfig["SampleSizeInMinutes"] = webServiceParams.request.SampleSizeInMinutes;
                            percentConfig.color = colorTheme.percentileLine;
                            percentConfig.dashStyle = colorTheme.percentileLineStyle;
                            percentConfig.isCalculated = true;
                            percentConfig.percentage = percentile;
                            percentConfig.data = [];

                            chartConfig.series.push(percentConfig);
                        }
                    }
                                                            
                    maxSerieWidth = getMaxSerieWidth(chartEl, seriesConfig, maxSerieWidth, isPieChart);
                }

                chartConfig.colors = getColorPaletteFromTheme(colorTheme, chartConfig.series);

                if (!foundNavigatorSeries)
                    chartConfig.navigator.baseSeries = 0;

                matchNavigatorAxisToMainAxis(chartConfig);

                // Sort the series array so that the colors will come out as expected.
                $.each(chartConfig.series, function(index, item) {
                    item["preserveReturnedOrder"] = index;
                });

                chartConfig.series.sort(function(a, b) {
                    return (a.seriesOrder - b.seriesOrder) == 0 ?
                        a.preserveReturnedOrder - b.preserveReturnedOrder :
                        a.seriesOrder - b.seriesOrder;
                });

                var index = 0;
                $.each(chartConfig.series, function (name, item) {

                    delete item.preserveReturnedOrder;

                    if (item.type === 'candlestick') {
                        // upcolor is set to same color as selected color for serie
                        item.upColor = item.color || chartConfig.colors[index];
                    }

                    if (!item.isCalculated) {
                        ++index;
                    }
                });

                if (chartConfig.series.length === 0) {
                    // No series returned.  Do we just have no data or should we wait and ask again.
                    if (!results.MoreDataAvailable) {
                        // TODO:  We should do something better/prettier than this.
                        bEmptyDataSeries = true;
                        chartConfig.navigator.enabled = false; // navigator will crash this
                    } else // not doing the else will cause infinite loop
                    {
                        webServiceParams.request.PartialDataCookie = results.PartialDataCookie;

                        setTimeout(function() {
                            initialChartLoad(chartSettings, chartOptions, webServiceParams, onFinish);
                        }, results.DelayForMoreDataInMs);

                        return;
                    }
                } 
                else if (results.MoreDataAvailable) {
                    bEmptyDataSeries = false;
                }
                else {
                    for (var serIndex = 0; serIndex < chartConfig.series.length; serIndex++) {
                        var curSerie = chartConfig.series[serIndex];
                        for (var j = 0; j < curSerie.data.length; j++) {
                            if (!(curSerie.data[j] instanceof Array))  // it is not array object
                            {
                                bEmptyDataSeries = false;
                                break;
                            } else if ((curSerie.data[j] != null) && (curSerie.data[j].length > 1) && (curSerie.data[j][1] != null)) {
                                bEmptyDataSeries = false;
                                break;
                            }
                        }

                        if (!bEmptyDataSeries) {
                            break;
                        }
                    }
                }

                if (bEmptyDataSeries) {
                    var chart = $('#' + chartConfig.chart.renderTo).data();
                    if (chart != null) {
                        chart.series = [];
                    }


                    // fixes memory leak occuring in some subversions of IE10 and IE11, FB351808
                    var IsIE = $.browser.msie;
                    if (!IsIE) {
                        $('#' + chartConfig.chart.renderTo).html('').height('auto');
                    }
                    else {
                        $('#' + chartConfig.chart.renderTo).height('auto');
                    }
                    
                    var $parentNotHasChart = $('#' + chartConfig.chart.renderTo).parent().find("div:not(.hasChart)");
                    if (typeof $parentNotHasChart.attr("data-css") == "undefined") {
                        $parentNotHasChart.attr("data-css", JSON.stringify({ display: $parentNotHasChart.css("display") }));
                    }

                    $parentNotHasChart.css({"display": "none"});
                    $chartRenderTo = $('#' + chartConfig.chart.renderTo);
                    if (typeof $chartRenderTo.attr("data-css") == "undefined") { 
                        $chartRenderTo.attr("data-css", JSON.stringify({ float: $chartRenderTo.css("float"), maxWidth: $chartRenderTo.css("max-width") }));
                    }

                    $chartRenderTo.css({'float' : 'none', 'max-width' : ''});
                    fDisplayNoDataMessage(results);
                    
                    if(onFinish)
                        onFinish();
                    return;
                } else {
                    var $parentNotHasChart = $('#' + chartConfig.chart.renderTo).parent().find("div:not(.hasChart)");
                    for (var i = 0; i < $parentNotHasChart.length; i++) {
                        var $parentNotHasChartItem = $($parentNotHasChart.get(i));
                        if (typeof $parentNotHasChartItem.attr("data-css") != "undefined") {
                            $parentNotHasChartItem.css({ display: JSON.parse($parentNotHasChartItem.attr("data-css")).display });
                        }
                    }

                    var $chartRenderTo = $('#' + chartConfig.chart.renderTo);
                    for (var i = 0; i < $chartRenderTo.length; i++) {
                        var $chartRenderToItem = $($chartRenderTo.get(i));
                        if (typeof $chartRenderToItem.attr("data-css") != "undefined") {
                            var cssData = JSON.parse($chartRenderToItem.attr("data-css"));
                            $chartRenderToItem.css({ 'float': cssData.float, 'max-width': cssData.maxWidth });
                        }
                    }
                }

                var tmpWidth = chartEl.width() * 0.85;
                var maxSerieWidth2 = maxSerieWidth * 1.1;
                  var ie7TooltipWidthFix = '';
                if (tmpWidth <= maxSerieWidth) {
                    maxSerieWidth = tmpWidth;
                    maxSerieWidth2 = tmpWidth;
                    maxSerieWidth = 'width:' + maxSerieWidth * 0.85 + 'px';
                    maxSerieWidth += ';max-width:' + tmpWidth * 0.85 + 'px';
                } else {
                    maxSerieWidth = '';
                }

                if ($('html').hasClass('sw-is-msie-7')) {
                    if (maxSerieWidth2 < 270) { // 270 is experimentally find out min value
                        maxSerieWidth2 = 270;
                    }
                    
                    if (maxSerieWidth2 > tmpWidth) {
                       maxSerieWidth2 = tmpWidth;
                    }
                    
                    ie7TooltipWidthFix = 'style="width: ' + maxSerieWidth2 + 'px"';
                }

                var isPathUndefined = function (o, path) {
                    if (typeof o === 'undefined') {
                        return true;
                    }
                    path = path.split('.');
                    while (path.length) {
                        o = o[path.shift()];
                        if (typeof o === 'undefined') {
                            return true;
                        }
                    }
                    return false;
                }
                 
                if ((maxSerieWidth != '') || (ie7TooltipWidthFix != '')) { // override only when it is needed
                    if (isPathUndefined(chartConfig, 'tooltip.footerFormat')) {
                        chartConfig.tooltip.footerFormat = "</table>" + tooltipWrapperFooter;
                    }
                    if (isPathUndefined(chartConfig, 'tooltip.headerFormat')) {
                        chartConfig.tooltip.headerFormat = tooltipWrapperHeader + '<table ' + ie7TooltipWidthFix + '><tr><td columnspan="2" style="border: 0px;font-size: 11px;">{point.key}</td></tr>';
                    }
                    if (isPathUndefined(chartConfig, 'tooltip.pointFormat')) {
                        chartConfig.tooltip.pointFormat = '<tr style="line-height: 100%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding-top: 4px;"><div class="pointFormatSerie" style="font-size: 12px;' + maxSerieWidth + '">{series.name} </div></td><td style="border: 0px; font-size: 12px; padding-top: 4px;"><span style="color: {series.color}">:&nbsp;</span><b>{point.y}</b></td></tr>';
                    }
                    if (isPathUndefined(chartConfig, 'plotOptions.candlestick.tooltip.pointFormat')) {
                        chartConfig.plotOptions.candlestick.tooltip.pointFormat = '<tr style="line-height: 90%; font-weight: bold;"><td style="border: 0; font-size: 12px; color: {series.color}; padding-top: 4px;"><div class="pointFormatSerie" style="' + maxSerieWidth + '">{series.name} </div></td><td style="border: 0px; font-size: 12px; padding-top: 4px;"><span style="color: {series.color}">:&nbsp;</span><b>{point.low} - {point.high}</b></td></td></tr>';
                    }
                }  
                
                if (isPieChart && !chartConfig.tooltip.positioner) {
                    chartConfig.tooltip.positioner = tooltipPositioner;
                }

                chartConfig.xAxis.originalSubtitle = chartSettings.subtitle;

                // following code is hack which solve issue with overlapping yAxis title when in column chart are values greater 1000
                chartConfig.title = chartConfig.title || {};
                if(!chartConfig.title.text || chartConfig.title.text == "") {
                    if (chartConfig.yAxis && chartConfig.yAxis.length > 0 && chartConfig.yAxis[0].title && chartConfig.yAxis[0].title.text && chartConfig.yAxis[0].title.text != "") {
                        chartConfig.title.text = " ";
                        chartConfig.subtitle = chartConfig.subtitle || {};
                        if (!chartConfig.subtitle.text || chartConfig.subtitle.text == "")
                            chartConfig.subtitle.text = " ";
                     }
                }

                if (!isPieChart) {
                    if (containsAllSeriesOnlyZeroValuesOrNullDataPoints(chartConfig.series)) {
                        if (!results.MoreDataAvailable) {
                            chartConfig.yAxis[0].max = chartConfig.yAxis[0].max || 1;
                        }
                    }
                }

                  var dataRange = getMinMaxFromSeriesData(dataSeries);
                chartConfig = charts.DynamicLoader.updateChartConfig(chartConfig, chartSettings);
                
                if (chartSettings.inlineRendering===true)
                   charts.turnOffChartAnimations(chartConfig);
                  
                charts.createChart(chartConfig, webServiceParams, function (chart) {
                    chart.chartSettings = chartSettings;
                    chart.chartOptions = chartOptions;
                    chart.chartOptionsWithOverride = chartConfig;
                    chart.chartDataRange = dataRange;
                    chart.webServiceParams = webServiceParams;

                    charts.DynamicLoader.chartCreated(chart, results);
                    
                    chart.Thresholds = new charts.Thresholds.ThresholdPresenter(chart, chartSettings, chartConfig);
                    chart.Thresholds.loadThreshold();
 
                    charts.setZoom(chart, chartSettings.initialZoom);//This triggers redraw/setExtremes/AfterSetExtremes
                   

                    loadMoreDataIfNeeded(chartSettings.dataUrl, webServiceParams, results, chart);

                    var yAxisIndex = chartConfig.yAxis.length - 1;
                    if (annotationLines && chart.yAxis && chart.yAxis.length > 0) {
                        for (var i = 0; i < annotationLines.length; i++) {
                            chart.yAxis[0].addPlotLine(annotationLines[i]);
                        }
                    }

                    if (onFinish)
                        onFinish();
                });
                 
                if (results.AppliedLimitation === true) {
                    var chartDiv = $('#' + chartConfig.chart.renderTo);
                    displayAppliedLimitationMessage(chartDiv);
                }

                displaySwisfPartialErrorMessageIfThrown(results, $('#' + chartConfig.chart.renderTo));
            }
            catch (jsException) {
                //We need to make sure finished handler is called to keep all other charts loading in case there is an issue with only one chart.   
                //At this point chart may be usable even with js errror.  
                if (onFinish)
                    onFinish();
            }
        }, chartOnError);
    };

    var displaySwisfPartialErrorMessageIfThrown = function (requestResult, appendErrorMessageAfter) {
        if (requestResult.SwisfErrorControlHtml && requestResult.SwisfErrorControlHtml.length > 0) {
           appendErrorMessageAfter.after(requestResult.SwisfErrorControlHtml);
        }
    }

    var loadMoreDataIfNeeded = function(dataUrl, webServiceParams, previousResults, chart) {
        if (!previousResults.MoreDataAvailable) {
            chart.hideLoading();
            return;
        }
        chart.showLoading();
        webServiceParams.request.PartialDataCookie = previousResults.PartialDataCookie;

        // There's more data so we need to ask the web service for it.  We'll wait however 
        // long the previous call told us to wait so we don't flood the server with requests.
        setTimeout(function() {
            charts.callWebService(dataUrl, webServiceParams, function(partialResults) {
                // add the new data to the chart
                addNewDataToChart(chart, partialResults);

                // call again if needed.
                loadMoreDataIfNeeded(dataUrl, webServiceParams, partialResults, chart);
            });
        }, previousResults.DelayForMoreDataInMs);
    };

    var replaceRange = function(original, updated) {
        var updatedStart = updated[0][0];
        var updatedEnd = updated[updated.length - 1][0];

        var result = [];
        var originalCount = original.length;
        var index = 0;
        
        // add the original values older (smaller) than the update values
        while ((index < originalCount) && (original[index][0] < updatedStart)) {
            result.push(original[index]);
            index++;
        }
        
        // add the update values
        result = result.concat(updated);

        // move over the original values replaced by the update values
        while ((index < originalCount) && (original[index][0] <= updatedEnd)) {
            index++;
        }
        
        // add the orignial values older (larger) than the update values
        result = result.concat(original.slice(index));
        
        return result;
    };

    var addNewDataToChart = function(chart, partialResults, replaceAllData) {
    
        var updated = false;
        for (var i = 0; i < partialResults.DataSeries.length; i++) {
            var newSeries = partialResults.DataSeries[i];

            var uniqueId = newSeries.TagName + newSeries.NetObjectId;
            var existingSeries = $.grep(chart.series, function (series) { return series.options.uniqueId == uniqueId; });

            if (existingSeries.length == 0)
                return;

            existingSeries = existingSeries[0];

            var newData;
            if (replaceAllData === true) {
                fixNullDataPoints(newSeries.Data);
                newData = newSeries.Data;
            } else {
                newData = replaceRange(existingSeries.options.data, newSeries.Data);
            }
            existingSeries.setData(newData, false); // don't do redraw
            //if(replaceAllData === false) {
                //I think this will be needed to support NTA progressive loading but dont have a good way to test now.
                //var dataRange = getMinMaxFromSeriesData(existingSeries);
                //chart.chartDataRange = dataRange;
            //}
            updated = true;
        }
        if (updated)
            chart.redraw();
    };

    charts.addNewDataToChart = addNewDataToChart;

    charts.calculateMaxYValue = function(dataSeries) {
        var maxY = 0;
        for (var i = 0, imax = dataSeries.length; i < imax; i++) {
            
            // If serie has no data or is CustomDataSeries (used for non-visible data) we skip calculation.
            if (dataSeries[i].IsCustomDataSerie || !dataSeries[i].Data) continue;

            var newSeriesData = dataSeries[i].Data;
            for (var j = 0, jmax = newSeriesData.length; j < jmax; j++) {
                var curY = newSeriesData[j][1];
                if (!curY) continue;

                maxY = (curY > maxY) ? curY : maxY;
            }
        }

        return maxY;
    };

    // We want to allow tooltips to use the formatting functions defined for an axis.  This lets us show
    // 10.1 MB rather than 10,100,000 bytes.  We'll just insert our function before the point formatter
    // and replace the {point.y} variable with our function's result.
    var pointTooltipFormatter = Highcharts.Point.prototype.tooltipFormatter;

    Highcharts.Point.prototype.tooltipFormatter = function(pointFormat) {
        var point = this;
        var series = point.series;
        if (series.type != "pie" && typeof (series.yAxis) != "undefined") {
            var yAxisOptions = series.yAxis.options;
            var tooltipOptions = series.chart.options.tooltip;
            var dataFormatter = getTooltipFormatterForUnit(yAxisOptions.unitFormatter || yAxisOptions.unit, tooltipOptions);
            var decimalPlaces = series.tooltipOptions.changeDecimals || 2;

            pointFormat = pointFormat.replace('{point.y}', dataFormatter(point.y, yAxisOptions, decimalPlaces));
            pointFormat = pointFormat.replace('{point.low}', dataFormatter(point.low, yAxisOptions, decimalPlaces));
            pointFormat = pointFormat.replace('{point.high}', dataFormatter(point.high, yAxisOptions, decimalPlaces));
        }

        return pointTooltipFormatter.apply(this, [pointFormat]);
    };

    // The Min/Max series is actually implemented as a candlestick chart.  Highcharts sets a tooltip for candlestick charts
    // to handle showing the open, high, low, close points.  Unfortunatly the way they do this is not overrideable.  It turns
    // out that the "standard" tooltip formatter used by most of the series types (defined for the Point class) can handle
    // candlestick data (the OHLC point) as long as the pointFormat values is set.  We've already set candlestick series 
    // tooltip.pointFormat value in the basic config above.  Now we just need to revert candlestick series' tooltip formatter
    // to Point's.
    var candleStickPointPrototype = Highcharts.seriesTypes.candlestick.prototype.pointClass.prototype;
    var pointPrototype = Highcharts.Point.prototype;
    candleStickPointPrototype.tooltipFormatter = pointPrototype.tooltipFormatter;


    var displayAppliedLimitationMessage = function (chartDiv) {
        var editUrl = '/Orion/Admin/Settings.aspx';
        var tmpDiv = '<div id="appliedLimitation" style="margin: auto;"><table cellspacing="0" style="border:solid 1px; border-color: #cecfce; color: #000; background-color: white;"><tr><td><img src="/NetPerfMon/images/Event-602.gif" alt="Event Type" /></td><td><span>@{R=Core.Strings;K=WEBJS_TP0_6;E=js}</span></td><td><a class="sw-link" target="_blank" href="' + editUrl + '">@{R=Core.Strings;K=CommonButtonType_Edit;E=js}</a></td></tr></table></div>';
        var tmpDiv2 = $(tmpDiv);
        var position = chartDiv.offset();
        var positionInside, insideDiv, topPosition, width;
        if ($('html').hasClass('sw-is-msie-7') || $('html').hasClass('sw-is-msie-8')) {
            var jQchartDiv = $(chartDiv);
            var title = jQchartDiv.find('.highcharts-title');
            var subtitle = jQchartDiv.find('.highcharts-subtitle');
            var offset = 0;

            if ((typeof title != 'undefined' && title.length == 0) && (typeof subtitle != 'undefined' && subtitle.length == 0)) {
                offset = 40;
            }
            
            insideDiv = $(chartDiv.data().container.firstChild).children().eq(17);
            positionInside = insideDiv.offset();
            topPosition = -chartDiv.height() + (positionInside.top - position.top) + offset;
            width = chartDiv.width() / 2;
			chartDiv.parent().parent().css("overflow-y", "hidden");
        } else {
            insideDiv = $(chartDiv.data().container.firstChild).children().eq(2);
            positionInside = insideDiv.offset();
            topPosition = -chartDiv.height() + (positionInside.top - position.top);
            width = insideDiv.attr('width') - insideDiv.attr('width') / 4;
        }        

        tmpDiv2.css({ "position": "relative", "width": width, "top": topPosition });
        chartDiv.append(tmpDiv2);
    };

    $(function() {
        $('.chartLegendLogo').click(function(e) {
            var resourceWrapper = $(this).parents('.ResourceWrapper').first();
            var chartDiv = resourceWrapper.find('.hasChart').first();
            var chart = chartDiv.data();

            if (resourceWrapper.hasClass('chartFullScreen')) {
                // Restore the chart to its orignal size/location
                resourceWrapper.removeClass('chartFullScreen');
                resourceWrapper.children().removeClass('fullscreenChild');
                $('body').css('overflow-x', 'auto');

                chartDiv.height(chartDiv.attr('data-originalHeight'));
                chart.setSize(chartDiv.width(), chartDiv.height(), false);
                chart.redraw();
            } else {
                // We only want to support this if the user hodls down shift when they click.
                if (!e.shiftKey)
                    return;

                // blow the chart up to full screen.
                chartDiv.attr('data-originalHeight', chartDiv.height());

                resourceWrapper.addClass('chartFullScreen');
                resourceWrapper.children().addClass('fullscreenChild');
                $('body').css('overflow-x', 'hidden');
                window.scroll(0, 0);

                chartDiv.height(chartDiv.height() * 2);
                chart.setSize(chartDiv.width(), chartDiv.height(), false);
            }

        });
        
        // If the user CTRL + ALT + Clicks on the legend logo, create a text area with the complete dump
        // of the chart config.
        $('.chartLegendLogo').click(function(e) {
            if (!e.ctrlKey || !e.altKey)
                return;

            var resourceWrapper = $(this).parents('.ResourceWrapper').first();
            var chartDiv = resourceWrapper.find('.hasChart').first();
            var chart = chartDiv.data();
            $('<textarea></textarea>').text( JSON.stringify(chart.options, null, 4)).appendTo(resourceWrapper);
        });
    });

})(SW.Core.Charts);
