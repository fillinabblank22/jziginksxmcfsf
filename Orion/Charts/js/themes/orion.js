﻿Highcharts.theme = {
    colors: ["#006ca9", "#49D2F2", "#A87000", "#D8C679", "#515151", "#B7B7B7", "#310091", 
             "#9673EA", "#93008B", "#F149E7", "#828C38", "#C5DE00", "#874340", "#DBB0A0"],

    chart: {
        backgroundColor: {
            linearGradient: [0, 0, 250, 500],
            stops: [
                [0, 'rgb(255, 255, 255)'],
                [1, 'rgb(255, 255, 255)']
            ]
        },
        borderColor: '#FFFFFF',
        borderWidth: 2,
        className: 'dark-container',
        plotBackgroundColor: '#FFFFFF',
        plotBorderColor: '#CCCCCC',
        plotBorderWidth: 1
    },
    title: {
        style: {
            color: '#000000',
            font: 'bold 14px Arial, Helvetica, Sans-Serif'
        }
    },
    subtitle: {
        style: {
            color: '#000000',
            font: '11px Arial, Helvetica, Sans-Serif'
        }
    },
    xAxis: {
        gridLineColor: '#d7d7d7',
        gridLineWidth: 0,
        labels: {
            style: {
                color: '#646464'
            }
        },
        lineColor: '#FFFFFF',
        tickColor: 'd7d7d7',
        title: {
            style: {
                color: '#646464',
                textTransform: 'uppercase',
                fontSize: '10px',
                fontFamily: 'Arial, Helvetica, Sans-Serif'

            }
        }
    },
    yAxis: {
        gridLineColor: '#d7d7d7',
        labels: {
            style: {
                color: '#646464'
            }
        },
        lineColor: '#d7d7d7',
        minorTickInterval: null,
        tickColor: '#d7d7d7',
        tickWidth: 1,
        title: {
            style: {
                color: '#646464',
                textTransform: 'uppercase',
                fontSize: '10px',
                fontFamily: 'Arial, Helvetica, Sans-Serif',
                fontWeight: 'normal'
            }
        }
    },
    tooltip: {
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        style: {
            color: '#000000'
        }
    },
    toolbar: {
        itemStyle: {
            color: 'silver'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                color: '#CCC'
            },
            marker: {
                lineColor: '#333'
            }
        },
        spline: {
            marker: {
                lineColor: '#333'
            }
        },
        scatter: {
            marker: {
                lineColor: '#333'
            }
        },
        candlestick: {
            lineColor: 'white'
        }
    },
    legend: {
        itemStyle: {
            font: '11pt Arial, Helvetica, Sans-Serif',
            color: '#A0A0A0'
        },
        itemHoverStyle: {
            color: '#FFF'
        },
        itemHiddenStyle: {
            color: '#444'
        }
    },
    credits: {
        style: {
            color: '#666'
        }
    },
    labels: {
        style: {
            color: '#666'
        }
    },

    navigation: {
        buttonOptions: {
            backgroundColor: {
                linearGradient: [255, 255, 255, 20],
                stops: [
                    [0.4, '#bbb'],
                    [0.6, '#888']
                ]
            },
            borderColor: '#000000',
            symbolStroke: '#646464',
            hoverSymbolStroke: '#FFFFFF'
        }
    },

    exporting: {
        buttons: {
            exportButton: {
                symbolFill: '#55BE3B'
            },
            printButton: {
                symbolFill: '#7797BE'
            }
        }
    },


    // scroll charts
    rangeSelector: {
        buttonTheme: {
            width: 58,
            fill: {
                linearGradient: [0, 0, 0, 20],
                stops: [
                    [0.4, '#ffffff'],
                    [0.6, '#e6e6e6']
                ]
            },
            stroke: '#cccccc',
            style: {
                color: '#4d4d4d',
                fontWeight: 'normal'
            },
            states: {
                hover: {
                    fill: {
                        linearGradient: [0, 0, 0, 20],
                        stops: [
                            [0.4, '#fff'],
                            [0.6, '#e6e6e6']
                        ]
                    },
                    stroke: '#9b9b9b',
                    style: {
                        color: '#4d4d4d',
                        fontWeight: 'normal'
                    }
                },
                select: {
                    fill: {
                        linearGradient: [0, 0, 0, 20],
                        stops: [
                            [0.1, '#ccc'],
                            [0.3, '#e6e6e6']
                        ]
                    },
                    stroke: '#cccccc',
                    style: {
                        color: '#4d4d4d',
                        fontWeight: 'bold'
                    }
                }
            }
        },
        
        inputStyle: {
            backgroundColor: '#eee'
        }
        
    },   

    navigator: {
        maskFill: 'rgba(250, 250, 250, 0.85)',
        outlineColor: '#ccc',
        series: {
            color: '#076CBA',
            lineColor: '#076CBA'
        }
    },

    // special colors for some of the
    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    legendBackgroundColorSolid: 'rgb(35, 35, 70)',
    dataLabelsColor: '#444',
    textColor: '#C0C0C0',
    maskColor: 'rgba(255,255,255,0.3)'
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);

