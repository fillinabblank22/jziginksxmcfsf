SW.Core.Charts.Legend = SW.Core.Charts.Legend || {}; 
(function (legend) {

    legend.toggleSeries = function(series) {
        series.visible ? series.hide() : series.show();
    };

    legend.addCheckbox = function(series, container) {
        
        var check = $('<input type="checkbox"/>')
                    .click(function() { legend.toggleSeries(series); });
        
        if (series.visible)
            check.attr("checked", 'checked');

        check.appendTo(container);
    };

    legend.addTitle = function(series, container, nameOverride) {
        var label = $('<span/>');
        var title = nameOverride || SW.Core.Charts.htmlDecode(series.name);
        label.text(title);
        label.appendTo(container);
    };

    legend.addLegendSymbol = function(series, container, colorOverride) {
        var symbolWidth = 16;
        var symbolHeight = 12;
        var label = $('<span class="legendSymbol" />');
        var renderer = new Highcharts.Renderer(label[0], symbolWidth, symbolHeight + 3);
        var symbolColor = colorOverride || series.color;

        renderer.rect(0, 3, symbolWidth, symbolHeight, 2)
                .attr({
                    stroke: symbolColor,
                    fill: symbolColor
                })
                .add();

        label.appendTo(container);
    };

    legend.createStandardLegend = function(chart, dataUrlParameters, legendContainerId, interactiveMode) {
        var table = $('#' + legendContainerId);
        
        if (typeof(interactiveMode) == 'undefined') {
            interactiveMode = true;
        }

        $.each(chart.series, function(index, series) {
            if (!series.options.showInLegend)
                return;
            
            var row = $('<tr/>');
            
            if (interactiveMode) {
                var td = $('<td style="width:15px;"/>').appendTo(row);
                legend.addCheckbox(series, td);
            }
                    
            td = $('<td style="width:20px;"/>').appendTo(row);
            legend.addLegendSymbol(series, td);
            
            td = $('<td/>').appendTo(row);
            legend.addTitle(series, td);
            
            row.appendTo(table);
           
        });

    };

    legend.createForecastingLegend = function(chart, dataUrlParameters, legendContainerId, interactiveMode) {
        var table = $('#' + legendContainerId);
        
        if (typeof(interactiveMode) == 'undefined') {
            interactiveMode = true;
        }

        var hrow = $('<tr style="background-color:#DEDEDE;" />');
        var headerColumns = "";
        headerColumns += '<td colspan="2" class="ReportHeader">@{R=Core.Strings;K=WEBDATA_LF_Resource;E=js}</td>';
        headerColumns += '<td class="ReportHeader">@{R=Core.Strings;K=WEBDATA_LF_TrendSlope;E=js}</td>';
        headerColumns += '<td class="ReportHeader sw-forecast-threshold-header">@{R=Core.Strings;K=WEBDATA_LF_Warning;E=js}</td>';
        headerColumns += '<td class="ReportHeader sw-forecast-threshold-header">@{R=Core.Strings;K=WEBDATA_LF_Critical;E=js}</td>';
        headerColumns += '<td class="ReportHeader sw-forecast-threshold-header">@{R=Core.Strings;K=WEBDATA_LF_Capacity;E=js}</td>';

        hrow.html(headerColumns);
        hrow.appendTo(table);
        var cpuLoadDraw = 0;
        var memoryLoadDraw = 0;

        $.each(chart.series, function(index, series) {
            if (!series.options.showInLegend)
                return;
           
            if (series.options.nameOverrideFC)
            /*{   
                    var row = $('<tr />');
            
                    if (interactiveMode) {
                        var td = $('<td style="width:15px;"/>').appendTo(row);
                        legend.addCheckbox(series, td);                     
                    }
                    
                    td = $('<td colspan="7"/>').appendTo(row);            
                    legend.addLegendSymbol(series, td);                                

                    var label = $('<span/>');
                    var title = '&nbsp;' + SW.Core.Charts.htmlDecode(series.name);
                    label.html(title);
                    label.appendTo(td); 

                    row.appendTo(table);
            }
            else*/
            {
                var row = $('<tr />');
            
            /*    if (interactiveMode) {
                    
                    $('<td style="width:15px;"/>').appendTo(row);
                    var td = $('<td style="width:15px;"/>').appendTo(row);
                    legend.addCheckbox(series, td);
                }
              */      
                td = $('<td />').appendTo(row);            
                legend.addLegendSymbol(series, td);            
                
                var label = $('<span/>');
                var title = '&nbsp;' + series.options.nameOverrideFC;
                label.html(title);
                label.appendTo(td); 

                td = $('<td/>').appendTo(row);                        
                var down = '';
                if (series.options.customProperties.DayPercent < 0)
                    down = 'down';
                td = $('<td style="width:100px;"><img src="/Orion/images/ForecastingIcons/trend'+down+'.png" />&nbsp;' + String.format('@{R=Core.Strings;K=WEBDATA_LF_PercPerDay;E=js}', series.options.customProperties.DayPercent) +'</td>').appendTo(row);            
                td = $(SW.Core.Formatters.capacityForecastDateFormatTable(series.options.customProperties.Warn, series.options.customProperties.WarningThreshold, 'sw-forecast-warning')).appendTo(row);            
                td = $(SW.Core.Formatters.capacityForecastDateFormatTable(series.options.customProperties.Critical, series.options.customProperties.CriticalThreshold, 'sw-forecast-critical')).appendTo(row);            
                td = $(SW.Core.Formatters.capacityForecastDateFormatTable(series.options.customProperties.Capacity, series.options.customProperties.CapacityThreshold, 'sw-forecast-capacity')).appendTo(row);
                row.appendTo(table);
               }
        });

    };
    
    legend.createMultiColorLegend = function(chart, dataUrlParameters, legendContainerId) {
        //This method expects series to have added property of legendItems:{name:"test",color:"#fff"}
        var table = $('#' + legendContainerId);
        $.each(chart.series, function(index, series) {
            if( series.legendItems) {
                $.each(series.legendItems, function(index, legendItem) {
                    var row = $('<tr/>');

                    var td = $('<td style="width:15px;"/>').appendTo(row);

                    td = $('<td style="width:20px;"/>').appendTo(row);
                    legend.addLegendSymbol(series, td, legendItem.color);

                    td = $('<td/>').appendTo(row);
                    legend.addTitle(series, td, legendItem.name);

                    row.appendTo(table);
                });
            }
        });
    };
    
    
} (SW.Core.Charts.Legend));