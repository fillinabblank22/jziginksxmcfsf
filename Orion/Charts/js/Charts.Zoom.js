SW.Core.Charts = SW.Core.Charts || {}; 
(function (charts) {
    
    var lang = Highcharts.getOptions().lang;

    var constants = {
        msPerMinute: 1000 * 60,
        msPerHour: 1000 * 60 * 60,
        msPerDay: 1000 * 60 * 60 * 24
    };

    charts.zoomLevels = [{
        key: '1h',
        display: lang.zoomLastHour,
        displayShort: lang.zoomLastHourShort,
        calculate: function(now, today) {
            return {
                min: now - constants.msPerHour,
                max: now
            };
        }
    }, {
        key: '2h',
        display: lang.zoomLastTwoHours,
        displayShort: lang.zoomLastTwoHoursShort,
        calculate: function(now, today) {
            return {
                min: now - constants.msPerHour * 2,
                max: now
            };
        }
    }, {
        key: '24h',
        display: lang.zoomLast24Hours,
        displayShort: lang.zoomLast24HoursShort,
        calculate: function(now, today) {
            return {
                min: now - constants.msPerHour * 24,
                max: now
            };
        }
    }, {
        key: 'today',
        display: lang.zoomToday,
        displayShort: lang.zoomTodayShort,
        calculate: function(now, today) {
            return {
                min: today,
                max: now
            };
        }
    }, {
        key: 'yesterday',
        display: lang.zoomYesterday,
        displayShort: lang.zoomYesterdayShort,
        calculate: function(now, today) {
            return {
                min: today - constants.msPerDay,
                max: today - 1
            };
        }
    }, {
        key: '7d',
        display: lang.zoomLast7Days,
        displayShort: lang.zoomLast7DaysShort,
        calculate: function(now, today) {
            return {
                min: today - (7 * constants.msPerDay),
                max: now
            };
        }
    }, {
        key: 'thisMonth',
        display: lang.zoomThisMonth,
        displayShort: lang.zoomThisMonthShort,
        calculate: function(now, today) {
            return {
                min: new Date(now.getFullYear(), now.getMonth(), 1),
                max: now
            };
        }
    }, {
        key: 'lastMonth',
        display: lang.zoomLastMonth,
        displayShort: lang.zoomLastMonthShort,
        calculate: function(now, today) {
            return {
                min: new Date(now.getFullYear(), now.getMonth() - 1, 1),
                max: new Date(now.getFullYear(), now.getMonth(), 1) - 1
            };
        }
    }, {
        key: '30d',
        display: lang.zoomLast30Days,
        displayShort: lang.zoomLast30DaysShort,
        calculate: function(now, today) {
            return {
                min: today - (30 * constants.msPerDay),
                max: now
            };
        }
    }, {
        key: '3m',
        display: lang.zoomLast3Months,
        displayShort: lang.zoomLast3MonthsShort,
        calculate: function(now, today) {
            return {
                min: today - (90 * constants.msPerDay),
                max: now
            };
        }
    }, {
        key: '6m',
        display: lang.zoomLast6Months,
        displayShort: lang.zoomLast6MonthsShort,
        calculate: function (now, today) {
            return {
                min: today - (180 * constants.msPerDay),
                max: now
            };
        }
    }, {
        key: 'thisYear',
        display: lang.zoomThisYear,
        displayShort: lang.zoomThisYearShort,
        calculate: function(now, today) {
            return {
                min: new Date(now.getFullYear(), 0, 1),
                max: now
            };
        }
    }, {
        key: '12m',
        display: lang.zoomLast12Months,
        displayShort: lang.zoomLast12MonthsShort,
        calculate: function(now, today) {
            return {
                min: today - (365 * constants.msPerDay),
                max: now
            };
        }
    }];


    charts.calculateDateWithTimeZoneOffset = function (date, useTimePart) {
        var lastDateTime = new Date(date);
        var localOffset = (new Date().getTimezoneOffset() * 60 * 1000);

        if (useTimePart)
            return new Date(new Date(lastDateTime.getUTCFullYear(), lastDateTime.getUTCMonth(), lastDateTime.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()).getTime() - localOffset);
        else
            return new Date(new Date(lastDateTime.getUTCFullYear(), lastDateTime.getUTCMonth(), lastDateTime.getUTCDate()).getTime() - localOffset);
    };


    charts.calculateZoom = function (range, zoomLevelKey) {
        if (!zoomLevelKey) return range;
        
        var lastDateTime = new Date(range.dataMax);
        var lastDay = charts.calculateDateWithTimeZoneOffset(lastDateTime, false);

        zoomLevelKey = zoomLevelKey.toUpperCase();
        var zoomLevel = $.grep(charts.zoomLevels, function (x) { return x.key.toUpperCase() === zoomLevelKey });
        if (zoomLevel.length === 0) return null;

        zoomLevel = zoomLevel[0];
        var dateRange = zoomLevel.calculate(lastDateTime, lastDay);

        dateRange.min = Math.max(dateRange.min, range.dataMin);
        dateRange.max = Math.min(dateRange.max, range.dataMax);
        return dateRange;
    };
    
    charts.setZoom = function(chart, zoomLevelKey) {
        var xAxis = chart.xAxis[0];
        if (xAxis === null) return;

        var range = xAxis.getExtremes();

        var dateRange = charts.calculateZoom(range, zoomLevelKey);
        if( dateRange == null)
            return;
        xAxis.setExtremes(dateRange.min, dateRange.max);
    };

})(SW.Core.Charts);
