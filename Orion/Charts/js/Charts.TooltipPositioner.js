﻿SW.Core.Charts = SW.Core.Charts || {};
SW.Core.Charts.Tooltip = SW.Core.Charts.Tooltip || {};
(function (tooltip) {

    tooltip.positioner = function(boxWidth, boxHeight, point) {
        //if the positioning is not enabled in tooltip's config, let's behave as it was so far.
        //only charts that has this enabled will use computed position for their tooltips
        if (this.chart.tooltip.options.positioningEnabled != true)
            return this.chart.tooltip.getPosition(boxWidth, boxHeight, point);    

        var $tooltipWrapper = $("#TooltipWrapper_highchartTooltip");
        if ($tooltipWrapper.length == 0) {
            //create a tooltip wrapper in the body DOM - to be visible upper than everything what is in the page
            $(document.body).append('<div id="TooltipWrapper_highchartTooltip" />');
            $tooltipWrapper = $("#TooltipWrapper_highchartTooltip");             
            $tooltipWrapper.css('position', 'absolute');
            $tooltipWrapper.css('white-space', 'nowrap');
            $tooltipWrapper.css('z-index', '99999');
        }
        //clear tooltip wrapper element to be ready for the new data
        $tooltipWrapper.empty();

        //checking whether we are displaying in IE7 and whether we have chart siblings that are upon the chart
        //if so, we need to put the tooltip out of the chart since, IE7 is not able to show the tooltip over the chart's siblings
        //once IE7 is not supported, we can safely remove this part
        //-----------------------------------------------------------------
        if (this.chart.tooltip.maxSize == undefined && IE.Version() <= 7) {
            var showOutOfChartArea = false;
            var $chartContainer = $(this.chart.container);
            $.each($chartContainer.parent().children(), function() {
                if ($(this).attr('id') != $chartContainer.attr('id')) {
                    var top = $(this).css('top');
                    if (top && top.replace('px', '') < 0) {
                        showOutOfChartArea = true;
                        return false;
                    }
                } 
            });
            if (showOutOfChartArea == true)
                this.chart.tooltip.showOutOfChartArea = true;
        }
        //-----------------------------------------------------------------

        if (this.chart.tooltip.maxSize == undefined)
            this.chart.tooltip.maxSize = { width: boxWidth, height: boxHeight };

        if (this.chart.tooltip.maxSize.width < boxWidth)
            this.chart.tooltip.maxSize.width = boxWidth;

        if (this.chart.tooltip.maxSize.height < boxHeight)
            this.chart.tooltip.maxSize.height = boxHeight;

        var plotSize = { width: this.chart.plotWidth, height: this.chart.plotHeight };
        var plotPosition = { left: this.chart.plotLeft, top: this.chart.plotTop };

        if (this.chart.tooltip.showOutOfChartArea != true 
         && this.chart.tooltip.maxSize.width < plotSize.width 
         && this.chart.tooltip.maxSize.height < plotSize.height) 
        {
            $(this.chart.container).css('z-index', '');
            
            //get default tooltip location
            var location = this.chart.tooltip.getPosition(boxWidth, boxHeight, point);

            //get selected point range to avoid tooltip dispalying through hovered points
            var minY = 99999;
            var maxY = -99999;
            $.each(this.chart.hoverPoints, function() {
                if (Math.min(this.plotOpen || this.plotY, this.plotClose || this.plotY) < minY)
                    minY = Math.min(this.plotOpen || this.plotY, this.plotClose || this.plotY);
                if (Math.max(this.plotOpen || this.plotY, this.plotClose || this.plotY) > maxY)
                    maxY = Math.max(this.plotOpen || this.plotY, this.plotClose || this.plotY);
            });

            //compute position to put the tooltip below all hovered points
            location.y = maxY + plotPosition.top;
            //if there is not enough space to display tooltip below or there is plenty space above, move the tooltip above the hovered points
            if (location.y + boxHeight > plotPosition.top + this.chart.chartHeight || maxY + boxHeight > plotSize.height && minY - boxHeight > 0)
                location.y = minY - boxHeight + plotPosition.top;
            //if there is enough space above as well, let's display tooltip. Otherways continue with tooltip position computing
            if (location.y >= 0 && location.x > 0 && location.x < this.chart.chartWidth)
                return location;
        }

        //if the tooltip is big, let's put it out of highchart's tooltip wrapper 
        var $tooltip = $("#highchartTooltip");
        $tooltip.remove();

        //if there is present more than one tooltip in highchart, drop them all 
        //and wait for next mouse motion in chart area to display the correct tooltip
        if ($("#highchartTooltip").length > 0) {
            while ($("#highchartTooltip").length > 0)
                $("#highchartTooltip").remove();
            return false;
        }

        //put the tooltip to the main tooltip wrapper in body DOM to be upper that every other element 
        $tooltipWrapper.css('top', '0px');
        $tooltipWrapper.css('left', '0px');
        $tooltipWrapper.append($tooltip);
                            
        //compute location of tooltip
        var chartOffset = $(this.chart.renderTo).offset();

        var positionYtop  = plotPosition.top - $tooltipWrapper.outerHeight();
        var positionYbottom  = (plotPosition.top + plotSize.height) + 15 + 2;
        var positionXleft = ((plotPosition.left + plotSize.width) - $tooltipWrapper.outerWidth());
        var positionXright = plotPosition.left;

        var tooltipLocation = { x: positionXright, y: positionYtop };
                            
        var $wnd = $(window);
        //Left possition correction if there is not enough space on the right side, align it with the right plot border
        if ($wnd.width() < tooltipLocation.x + chartOffset.left + $tooltipWrapper.outerWidth() && tooltipLocation.x > positionXleft)
            tooltipLocation.x = positionXleft;
                            
        //Top possition correction if there is more space on the bottom than on the top of screen, place it to the bottom
        if ((plotPosition.top + chartOffset.top) - $wnd.scrollTop() < $wnd.scrollTop() + $wnd.height() - (positionYbottom + chartOffset.top))
            tooltipLocation.y = positionYbottom;

        //setting possition of the main tooltip wrapper to display tooltip in the correct position
        $tooltipWrapper.css('top', (tooltipLocation.y + chartOffset.top) + 'px');
        $tooltipWrapper.css('left', (tooltipLocation.x  + chartOffset.left - 7) + 'px');
        $tooltipWrapper.show();

        this.chart.tooltip.showOutOfChartArea = true;

        if (this.chart.tooltip.hideDefault == undefined) {
            //backup the tooltip.hide method because we are going to override it
            this.chart.tooltip.hideDefault = this.chart.tooltip.hide;

            //overriding the tooltip.hide method to be able to hide main tooltip wrapper in the body DOM
            this.chart.tooltip.hide = function() {
                //hide the our tooltip wrapper and clear its content
                var $tooltipWrapper = $("#TooltipWrapper_highchartTooltip");
                $tooltipWrapper.hide();
                $tooltipWrapper.empty();

                //call the backup-ed (original) tooltip.hide method after all
                if (this.hideDefault != undefined)
                    this.hideDefault();
            };
        }

        return false;
    };

    var IE = {
        Version: function() {
            if (navigator.appVersion.indexOf("MSIE") != -1) {
                // bah, IE again, lets downgrade version number
                return parseFloat(navigator.appVersion.split("MSIE")[1]);
            }
            return undefined;
        }
    };
})(SW.Core.Charts.Tooltip);
