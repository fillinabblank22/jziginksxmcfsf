(function () {
    var events = window.ExportToPDFEvents;
    var removedElements = [];
    if (events === undefined)
        return;

    function SetIELengendElementStyle(newCss) {
        // fix legend on IE8 and IE7
        if ($('html').hasClass('sw-is-msie-7') || $('html').hasClass('sw-is-msie-8')) {
            $("table.chartLegend tbody tr span.legendSybmol").each(function () {
                var elem = $(this).find("shape")[0];
                if (elem) {
                    var innerDiv = $(this).find("div");
                    var bgColor = elem.fillColor.value;
                    innerDiv.css({ "visibility": newCss.visibility, "background-color": newCss.backgroundColor ? newCss.backgroundColor : bgColor });
                    innerDiv.attr("style", innerDiv.attr("style") + ";border-radius: 2px");
                }
            });
        }
    }

    function GetChartSVG(chart) {
        var tempInitFunction = null;
        if (chart.options.chart.events) {
            tempInitFunction = chart.options.chart.events.init;
            chart.options.chart.events.init = function () { };
        }

        var res = chart.getSVG();

        if (chart.options.chart.events) {
            chart.options.chart.events.init = tempInitFunction;
        }

        return res;
    }

    events.beforeExport.push(function () {
        $("span.legendSymbol div").each(function () {
            var bgColor = $(this).find("shape")[0].fillcolor.value;
            $(this).css({ "visibility": "visible", "background-color": bgColor });
            $(this).height($(this).height() - 4);
            $(this).attr("style", $(this).attr("style") + ";border-radius: 2px");
        });

        SetIELengendElementStyle({ visibility: "visible" });

        $('.hasChart').each(function () {
            var chartDiv = $(this);
            var chart = chartDiv.data();
            if (chart.series.length > 0) {
                chartDiv.parent().parent().find("input[type='checkbox']").each(function () {
                    $(this).attr("checked", $(this)[0].checked);
                });

                chartDiv.find('.chartSvg').remove();
                if ($('html').hasClass('sw-is-msie-8')) {
                    var elToRemove = chartDiv.find('.highcharts-container');
                    removedElements.push({ parent: elToRemove.parent(), removed: elToRemove });
                    elToRemove.remove();
                }

                var scriptTag = "script";

                var html = String.format('<div class="chartSvg" style="display:none;" svgHeight="{0}" svgWidth="{1}">' +
                    '<{2} type="text/xml">{3}</{4}></div>',
                    chartDiv.height(), chartDiv.width(),
                    scriptTag, GetChartSVG(chart), scriptTag);

                //Hack for IE10 rendering svg in scientific notation FB141518
                html = html.replace(/="1e-006"/g, '="0.000001"');

                chartDiv.append($(html));
            }
        });

        $("input.highcharts-range-selector").each(
            function () {
                $(this).attr("value", $(this).val());
            });
    });

    events.afterExport.push(function () {
        $("span.legendSymbol div").each(function () {
            $(this).css({ "visibility": "hidden", "background-color": "transparent", "padding-top": "0px", "padding-bottom": "0px" });
            $(this).height($(this).height() + 4);
        });

        SetIELengendElementStyle({ visibility: "hidden", backgroundColor: "transparent" });

        $('.hasChart').each(function () {
            var chartDiv = $(this);

            chartDiv.find('.chartSvg').remove();
        });

        if ($('html').hasClass('sw-is-msie-8')) {
            for (var i = 0; i < removedElements.length; i++) {
                removedElements[i].parent.append(removedElements[i].removed);
            }

            removedElements = [];
        }
    });

})();