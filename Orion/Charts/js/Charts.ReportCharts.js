﻿var reportChartsNS = SW.Core.namespace("SW.Core.Charts.ReportCharts");

(function (reportChartsNs) {

    // REQUIRES: 
    var charts = SW.Core.Charts;
    if (!charts) throw new Error("ReportChart requires Charts.js library.");;

    /*
     * Class represents report chart.
     */
    reportChartsNs.Chart = (function () {

        // PRIVATE STATIC METHODS:

        function updateWebServiceParams(originalParams, chartSettings) {
            var request = originalParams.request;

            request.DataSource = chartSettings.dataSource;
            request.ChartConfiguration = chartSettings.chartConfiguration;
            request.TimeFrame = chartSettings.timeFrame;
            request.ViewLimitationId = chartSettings.viewLimitationId;
        }

        // Overriding original constructor, it has access to private fields.
        var ChartConstructor = function (chartSettings, chartOptions) {
            var self = this;

            self.chartSettings = chartSettings;
            self.chartOptions = chartOptions;
        };

        ChartConstructor.prototype.initChart = function () {
            var settings = this.chartSettings;

            this.chartSettings.onWebServiceParamsCreated = function (originalParams) {
                updateWebServiceParams(originalParams, settings);
            };

            // Sets chart option according to rendering mode.
            var renderModeConfig = new reportChartsNs.RenderingModeConfigurator();
            this.chartOptions = renderModeConfig.update(this.chartSettings, this.chartOptions);

            
            var chartTypeConfig = new reportChartsNs.ChartTypeConfigurator();
            this.chartOptions = chartTypeConfig.update(this.chartSettings, this.chartOptions);            
            this.chartOptions.isReporting = true;
            
            charts.initializeStandardChart(this.chartSettings, this.chartOptions);
        };

        return ChartConstructor;

    })(); // execute immediately

    
    reportChartsNs.ChartTypeConfigurator = (function() {
        var typeChartOptions = [];

        typeChartOptions["Bar"] = {
            highchartsChartType : 'chart',
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            }
        };
        
        function containsBarDataSeries(chartOptions) {
            var hasBar = _.reduce(chartOptions.seriesTemplates, function(hasBar, template) {
                
                var isBar = template.type === 'bar';
                return hasBar || isBar;

            }, false);

            return hasBar;
        }


         // Overriding original constructor, it can access to private static fields.
        var ConfiguratorConstructor = function () {};


        ConfiguratorConstructor.prototype.update = function (chartSettings, chartOptions) {
            var isBarChart = containsBarDataSeries(chartOptions);
            if (isBarChart) {
                var barChartOptions = typeChartOptions["Bar"] || {};
                chartOptions = jQuery.extend(true, chartOptions, barChartOptions);
            }
        

            return chartOptions;
        };

        return ConfiguratorConstructor;

    }());

    /*
     * Class responsible for setup chart options according to rendering mode. 
     */
    reportChartsNs.RenderingModeConfigurator = (function () {

        // PRIVATE STATIC FIELDS:

        var RENDERING_MODE_ENUM = {
            READ_ONLY: 0,
            INTERACTIVE: 1
        };

        var modeChartOptions = [];

        // Config for ReadOnly mode:
        modeChartOptions[RENDERING_MODE_ENUM.READ_ONLY] = {
            chart : {
              zoomType: ''  
            },
            navigator: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            rangeSelector: {
                enabled: false
            },
            plotOptions: {
                series: {
                    enableMouseTracking: false,
                    animation : false,
                    stickyTracking: false
                }
            },
            scrollbar: {
                enabled: false
            }
        };


        // Config for Interactive mode:
        modeChartOptions[RENDERING_MODE_ENUM.INTERACTIVE] = {
            // Nothing special. It is default in charting framework.              
        };

        // Overriding original constructor it can access to private static fields.
        var ConfiguratorConstructor = function () {

        };


        ConfiguratorConstructor.prototype.update = function (chartSettings, chartOptions) {
            var modeOptions = modeChartOptions[chartSettings.renderingMode] || {};

            chartOptions = jQuery.extend(true, chartOptions, modeOptions);

            return chartOptions;
        };

        return ConfiguratorConstructor;
    }()); // execute immediately


}(reportChartsNS));
