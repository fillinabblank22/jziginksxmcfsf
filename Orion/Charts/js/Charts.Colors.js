SW.Core.Charts = SW.Core.Charts || {};
SW.Core.Charts.ColorThemes = SW.Core.Charts.ColorThemes || {};
(function (themes) {

    themes.standard = {
        name: 'Blue Steel',
        colors: [
            // 2 color palette
            ["#0D477D", "#8EBBDD"],
            
            // 14 color palette
            ["#006ca9", "#49D2F2", "#A87000", "#D8C679", "#515151", "#B7B7B7", "#310091", 
             "#9673EA", "#93008B", "#F149E7", "#828C38", "#C5DE00", "#874340", "#DBB0A0"]
        ],
        
        trendLine: "#666666",
        treadLineStyle: 'DashDot',
        
        sumLine: "#000000",
        sumLineStyle: 'Dot',
        
        rightSumLine: "#878787",
        rightSumLineStyle: 'Dot',

        percentileLine: "#999999",
        percentileLineStyle: 'ShortDash',

        annotationLine: '#FF9D1C',
        annotationLineStyle: 'Solid'
    };

} (SW.Core.Charts.ColorThemes));