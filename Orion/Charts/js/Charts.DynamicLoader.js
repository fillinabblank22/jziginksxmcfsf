﻿/*
 * Depends on Charts.js
 */
SW.Core.Charts.DynamicLoader = SW.Core.Charts.DynamicLoader || {};

(function (dynamicLoader, charts) {

    var isDynamicLoadingModeEnabled = function (chartSettings) {
        return ((chartSettings !== undefined) &&
            (chartSettings.loadingMode !== undefined) &&
            ((chartSettings.loadingMode === charts.LoadingModeEnum.DynamicLoading) || (chartSettings.loadingMode === charts.LoadingModeEnum.DynamicLoadingFirst))
        );
    };

    // ISO UTC format '%Y-%m-%dT%H:%M:%SZ'
    var formatDateToIso = function (date) {
        return date.getUTCFullYear() + '-' + formatToTwoDigits(date.getUTCMonth() + 1) + '-' + formatToTwoDigits(date.getUTCDate()) + 'T' +
            formatToTwoDigits(date.getUTCHours()) + ':' + formatToTwoDigits(date.getUTCMinutes()) + ':' + formatToTwoDigits(date.getUTCSeconds()) + 'Z';
    };

    var formatToTwoDigits = function (val) {
        if (val < 10)
            return '0' + val;
        else {
            return val;
        }
    };

    dynamicLoader.loadData = function (chart, event) {

        if (isDynamicLoadingModeEnabled(chart.chartSettings)) {
            var minDate = new Date(event.min),
                maxDate = new Date(event.max);

            var chartWebServiceRequest = chart.webServiceParams.request;
            
            chartWebServiceRequest.StartTime = formatDateToIso(minDate);
            chartWebServiceRequest.EndTime = formatDateToIso(maxDate);
            chartWebServiceRequest.Mode = charts.LoadingModeEnum.DynamicLoading;

            chart.showLoading();
            charts.callWebService(chart.chartSettings.dataUrl, chart.webServiceParams, function (results) {
                setMaxYValue(chart, results.DataSeries);
                charts.addNewDataToChart(chart, results, true);
                charts.AdjustYMaxValueForNullOrZeroDataPointSeries(chart, event);
                charts.generateCalculatedSeries(chart, event);

                chart.hideLoading();
            });
        } else {
          // If chart is pivoted (X-axis vertical) then this was called on X-axis and broke Y-axis range
          if (event && (!event.target || !event.target.isXAxis)) {
            //for non dynamic load charts just generate calculated series
            charts.AdjustYMaxValueForNullOrZeroDataPointSeries(chart, event);
          }
          charts.generateCalculatedSeries(chart, event);
        }
    };

    dynamicLoader.updateWebServiceParams = function (originalParams, chartSettings) {

        if (isDynamicLoadingModeEnabled(chartSettings)) {
            var chartContainer = $('#' + chartSettings.renderTo);

            originalParams.request.Mode = charts.LoadingModeEnum.DynamicLoadingFirst;
            originalParams.request.ChartWidth = chartContainer.width();
            originalParams.request.ChartHeight = chartContainer.height();
        }
        return originalParams;
    };

    dynamicLoader.updateChartConfig = function (chartConfig, chartSettings) {
        // We need to set navigator properly before the first dynamic request to server, 
        // because when the request failed a navigator is not set up corectly and chart is cycle in requesting a server.    
        if (isDynamicLoadingModeEnabled(chartSettings)) {
            chartConfig.navigator.adaptToUpdatedData = false;
        }
        
        return chartConfig;
    };

    dynamicLoader.chartCreated = function(chart, requestResult) {

        if (isDynamicLoadingModeEnabled(chart.chartSettings)) {

            //Fix the yAxis on the first chart load. This prevents the axis from changing when zooming/panning.
            setMaxYValue(chart, requestResult.DataSeries);
        }
    };

    var setMaxYValue = function (chart, dataSeries) {
        var actualMaxY = chart.yAxis[0].userMax || 0;
        var maxYfromNewData = charts.calculateMaxYValue(dataSeries);
        if (maxYfromNewData > actualMaxY) {
            chart.yAxis[0].userMax = maxYfromNewData;
        }
    };

}(SW.Core.Charts.DynamicLoader, SW.Core.Charts));
