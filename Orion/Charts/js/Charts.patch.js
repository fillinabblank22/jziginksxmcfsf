/*
  Following patch code solve issue with memory leaks in highcharts mostly in IE9 when page is reloaded or user went from page.
*/

(function(charts) {
    // because that CW doesn't delete files we need to keep this file minimally empty, because upgrade
    var dummyDestroy = Highcharts.Series.prototype.destroy;
    Highcharts.Series.prototype.destroy = function () {
        try {
            dummyDestroy();
        } catch (ex) {
        }
    };
})(SW.Core.Charts);

// FB398923 - Chart Framework incorrectly counted positions of vertical bars.
(function () {
    var UNDEFINED,
        HIDDEN = 'hidden',
        VISIBLE = 'visible',
        SVG_NS = 'http://www.w3.org/2000/svg',
        hasSVG = !!document.createElementNS && !!document.createElementNS(SVG_NS, 'svg').createSVGRect,
        hasTouch = document.documentElement.ontouchstart !== UNDEFINED,
        TRACKER_FILL = 'rgba(192,192,192,' + (hasSVG ? 0.0001 : 0.002) + ')';

    function defined(obj) {
        return obj !== UNDEFINED && obj !== null;
    }

    function attr(elem, prop, value) {
        var key, ret;
        if (typeof prop === 'string') {
            if (defined(value)) {
                elem.setAttribute(prop, value);
            } else if (elem && elem.getAttribute) {
                ret = elem.getAttribute(prop);
            }
        } else if (defined(prop) && isObject(prop)) {
            for (key in prop) {
                elem.setAttribute(key, prop[key]);
            }
        }
        return ret;
    }

    Highcharts.seriesTypes.column.prototype.drawTracker = function () {
		var series = this,
		chart = series.chart,
		renderer = chart.renderer,
		shapeArgs,
		tracker,
		trackerLabel = +new Date(),
		options = series.options,
		cursor = options.cursor,
		css = cursor && { cursor: cursor },
		trackerGroup = series.isCartesian && series.plotGroup('trackerGroup', null, VISIBLE, options.zIndex || 1, chart.trackerGroup),
		rel,
		plotY,
		validPlotY,
		points = series.points,
		point,
		i = points.length,
		onMouseOver = function (event) {
			rel = event.relatedTarget || event.fromElement;
			if (chart.hoverSeries !== series && attr(rel, 'isTracker') !== trackerLabel) {
				series.onMouseOver();
			}
			points[event.target._i].onMouseOver();
		},
		onMouseOut = function (event) {
			if (!options.stickyTracking) {
				rel = event.relatedTarget || event.toElement;
				if (attr(rel, 'isTracker') !== trackerLabel) {
					series.onMouseOut();
				}
			}
		};

		while (i--) {
			point = points[i];
			tracker = point.tracker;
			shapeArgs = point.trackerArgs || point.shapeArgs;
			plotY = point.plotY;
			validPlotY = !series.isCartesian || (plotY !== UNDEFINED && !isNaN(plotY));
			delete shapeArgs.strokeWidth;
			if (point.y !== null && validPlotY) {
				if (tracker) {// update
					tracker.attr(shapeArgs);

				} else {
					point.tracker = tracker =
						renderer[point.shapeType](shapeArgs)
						.attr({
							isTracker: trackerLabel,
							fill: TRACKER_FILL,
							visibility: series.visible ? VISIBLE : HIDDEN
						})
						.on('mouseover', onMouseOver)
						.on('mouseout', onMouseOut)
						.css(css)
						.add(point.group || trackerGroup); // pies have point group - see issue #118

					if (hasTouch) {
						tracker.on('touchstart', onMouseOver);
					}
				}
				// Fix - we need to do this only once.
				if (!tracker._isCreated) {
					tracker._isCreated = true;
					tracker.element._i = i;
				}
				else {
					tracker.element._pt = point;
				}
			}
		}
	};
}());