﻿<%@ WebHandler Language="C#" Class="CustomChartData" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Charting.v2.Enums;
using SolarWinds.Orion.Web.Helpers;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;

public class CustomChartData : IHttpHandler {

    private static readonly Log log = new SolarWinds.Logging.Log();
    private readonly string defaultResourcePathForChartNameQuery =
        "SELECT [DefaultResourcePath] FROM [dbo].[ChartSettings] WHERE [ChartName]=@chartName";

    private const string XsrfCookie = "XSRF-TOKEN";
    private const string XsrfHeader = "X-XSRF-TOKEN";
    
    public void ProcessRequest (HttpContext context)
    {
        string requestCookieValue = context.Request.Headers["Cookie"];

        var xsrfcookie = context.Request.Cookies[XsrfCookie];
        
    	var netObjectId = string.IsNullOrEmpty(context.Request.QueryString["NetObject"])
    	                  	? ""
    	                  	: context.Request.QueryString["NetObject"].Trim();

        if (String.IsNullOrEmpty(context.Request.Form["DisplayDetails"]))
        { // if DisplayDetails wasn't passed we will not export anything
            GenerateEmptyDocument(context);
            return;
        }
        
        UriBuilder builder = new UriBuilder(context.Request.Url);
        
        ChartSettingsDAL chartSettingsDal = new ChartSettingsDAL();

        ChartSettings chartsettings = chartSettingsDal.GetChartSettings(context.Request.Params["ChartName"]);
        
        string strDataUrl = chartsettings.DataUrl;

        dynamic dChartOptions = null;
        try
        {
            dChartOptions = new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(chartsettings.ChartOptions);
        }
        catch (Exception) { }

        List<string> parameters = new List<string>();
        string strNetObjectIds = context.Request.Params["NetObjectIds"];
        strNetObjectIds = GetParsedNetObjectIds(strNetObjectIds);

        parameters.Add(strNetObjectIds);
        parameters.Add(context.Request.Params["SampleSize"]);
        parameters.Add(context.Request["ChartDateSpan"]);
        if (ChartResourceSettings.IsChartDateSpanCustom(context.Request["ChartDateSpan"]) )
        {
            parameters.Add(context.Request["DateFromUtc"]);
            parameters.Add(context.Request["DateToUtc"]);
        }
            
        string strChartTitle = !string.IsNullOrEmpty(context.Request["ChartTitle"]) ? context.Request["ChartTitle"] : string.Empty;
        string strChartSubTitle = !string.IsNullOrEmpty(context.Request["ChartSubTitle"]) ? context.Request["ChartSubTitle"] : string.Empty;
        string strSampleSize = context.Request["SampleSize"];

        NetObject netObject = null;
        try
        {
            if (!string.IsNullOrEmpty(netObjectId))
                netObject = NetObjectFactory.Create(netObjectId);
        }
        catch (AccountLimitationException)
        {
            context.Server.Transfer(
                string.Format("/Orion/AccountLimitationError.aspx?NetObject={0}", context.Request.QueryString["NetObject"])
                );
        }

        if (!string.IsNullOrEmpty(strChartSubTitle) || string.IsNullOrEmpty(strChartSubTitle))
        {
            if (string.IsNullOrEmpty(strChartTitle))
            {
                strChartTitle = (netObject != null && !string.IsNullOrEmpty(netObject.Name)) ? netObject.Name : string.Empty;
            }

            if (string.IsNullOrEmpty(strChartSubTitle))
            {
                if ((dChartOptions != null) && dChartOptions.ContainsKey("subtitle"))
                {
                    strChartSubTitle = !string.IsNullOrEmpty(dChartOptions["subtitle"]["text"]) ? dChartOptions["subtitle"]["text"] : string.Empty;
                }
            }
        }

        // Parse macros in the title/subtitle (if they are there)
        if (strChartTitle.Contains("${") || strChartSubTitle.Contains("${"))
        {
        	Dictionary<string, object> netObjectcontext;
        	if (netObject != null)
        	{
        		netObject = NetObjectFactory.FindNetObjectByNetObjectPrefix(netObject, chartsettings.NetObjectPrefix);
        		netObjectcontext = netObject.ObjectProperties;
        	}
            else
        	{
        		netObjectcontext = new Dictionary<string, object>();
        	}
            
        	strChartTitle = Macros.ParseDataMacros(strChartTitle, netObjectcontext, true, false, true);
            strChartSubTitle = Macros.ParseDataMacros(strChartSubTitle, netObjectcontext, true, false, true);
            strChartSubTitle = strChartSubTitle.Replace(Macros.ChartsClientMacro, string.Empty);
        }

        // own retrieving data which we will consequently export
        strDataUrl = string.Format("{0}://{1}:{2}", builder.Scheme, builder.Host, builder.Port) + strDataUrl;

        Dictionary<string, object> resourceProperties = null;
        if (!string.IsNullOrEmpty(context.Request.Params["ResourceID"]))
        {
            int resourceId = 0;
            if (Int32.TryParse(context.Request.Params["ResourceID"], out resourceId))
            {
                resourceProperties = GetResourcePropertiesByResourceId(Convert.ToInt32(context.Request.Params["ResourceID"]));
            }
        }

        var resourcePropertiesQueryString = GetResourcePropertiesFromQueryStringParams(context.Request);

        if (resourceProperties == null)
        {
            resourceProperties = new Dictionary<string, object>();
        }

        foreach (var key in resourcePropertiesQueryString.Keys)
        {
            resourceProperties[key] = resourcePropertiesQueryString[key];
        }

        Dictionary<string, object> displayDetails = GetDisplayDetails(chartsettings.ChartName, context);
        
        var res = GetDataSeriesFromWebService(strDataUrl, requestCookieValue, resourceProperties, displayDetails, xsrfcookie == null ? "" : xsrfcookie.Value, parameters.ToArray());
        
        // Starting exporting table
        HttpResponse response = context.Response;
        string strDataFormat = context.Request.Params["DataFormat"];
        if (strDataFormat == "Excel")
        {
            context.Response.AddHeader("Content-Disposition", "attachment; filename=ChartData.xls");
            context.Response.ContentType = "application/vnd.ms-excel";
        }

        var chartExportValueFormatter = CreateChartExportValueFormatter(chartsettings.ChartExportValueFormatter);
        RenderingExportedDocument(dChartOptions, res.Where(item => !item.IsCustomDataSerie).ToList(), strChartTitle, strChartSubTitle, ReturnChartDateSpanFromRequest(context.Request), chartExportValueFormatter, response);
    }

    private IChartExportValueFormatter CreateChartExportValueFormatter(string chartExportValueFormatter)
    {
        if (string.IsNullOrEmpty(chartExportValueFormatter))
            return null;
        try
        {
            Type formatterType = Type.GetType(chartExportValueFormatter);
            var formatter = Activator.CreateInstance(formatterType) as IChartExportValueFormatter;
            return formatter;
        }
        catch (Exception ex)
        {
            log.Warn("Unable to load chart export value formatter from class " + chartExportValueFormatter, ex);
            return null;
        }
    }
    
    /// <summary>
    /// Method returns information about DisplayDetails for given chart by chartName which pick up from sended Post data.
    /// </summary>
    /// <param name="chartName">Name of chart</param>
    /// <param name="context"></param>
    /// <returns>Display details for given chart.</returns>
    private Dictionary<string, object> GetDisplayDetails(string chartName, HttpContext context)
    {
        Dictionary<string, object> displayDetails = null;
        if (!String.IsNullOrEmpty(context.Request.Form["DisplayDetails"]))
        {
            try
            {
                string strDisplayDetails = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(context.Request.Form["DisplayDetails"])); 
                displayDetails = new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(strDisplayDetails) as Dictionary<string, object>;
            }
            catch (Exception)
            {
                log.Error("Couldn't parse DisplayDetails chart data");
            }
        }

        return displayDetails;
    }

    /// <summary>
    /// Method produces empty HTML document
    /// </summary>
    /// <param name="context"></param>
    private static void GenerateEmptyDocument(HttpContext context)
    {
        context.Response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        context.Response.Write(Environment.NewLine);
        context.Response.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        context.Response.Write(Environment.NewLine);
        context.Response.Write("<body>");
        context.Response.Write(Environment.NewLine);
        context.Response.Write("</body>");
        context.Response.Write("</html>");
    }
    
    /// <summary>
    /// Method will from string in format val1, val2, ..., valn will return "val1", "val2", ..., "valn"
    /// </summary>
    /// <param name="netObjectIds">Input string format</param>
    /// <returns>Resulted string format</returns>
    private static string GetParsedNetObjectIds(string netObjectIds)
    {
        if (!String.IsNullOrEmpty(netObjectIds))
        {
            string[] strNetObjectIdsA = netObjectIds.Split(',');
            netObjectIds = string.Empty;
            foreach (string strNetObjectId in strNetObjectIdsA)
            {
                if (!string.IsNullOrEmpty(netObjectIds))
                {
                    netObjectIds += ",";
                }

                netObjectIds += "\"" + strNetObjectId + "\"";
            }
        }
        else
        {
            netObjectIds = "\"\"";
        }

        return netObjectIds;
    }
    
    private static string ReturnChartDateSpanFromRequest(HttpRequest request)
    {
        int dateSpan = 0;
        if (!string.IsNullOrEmpty(request["ChartDateSpan"]))
        {
            if (int.TryParse(request["ChartDateSpan"], out dateSpan))
            {
                switch (dateSpan)
                {
                    case (int)ChartTimePeriod.custom:
                        DateTime startDate;
                        if (!DateTime.TryParse(request["DateFromUtc"], out startDate))
                        {
                            startDate = DateTime.MinValue;
                        }

                        DateTime endDate;
                        if (!DateTime.TryParse(request["DateToUtc"], out endDate))
                        {
                            endDate = DateTime.MaxValue;
                        }

                        return startDate.ToString() + "~" + endDate.ToString();
                    case (int)ChartTimePeriod.lastDay:
                        return Resources.CoreWebContent.WEBDATA_IB0_107;
                    case (int)ChartTimePeriod.last7Days:
                        return Resources.CoreWebContent.WEBDATA_AK0_187;
                    case (int)ChartTimePeriod.lastMonth:
                        return Resources.CoreWebContent.WEBDATA_IB0_103;
                    case (int)ChartTimePeriod.lastThreeMonths:
                        return string.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3);
                    case (int)ChartTimePeriod.lastYear:
                        return Resources.CoreWebContent.WEBDATA_IB0_106;
                    default:
                        return Resources.CoreWebContent.WEBDATA_IB0_107;
                }
            }
        }

        return Resources.CoreWebContent.WEBDATA_IB0_107;
    }
    
    private static string FormatData(string pointSeriesTag, double data, IChartExportValueFormatter chartExportValueFormatter)
    {
        //New charts have MINMAX series as opposed to seperate series for min and max.  Lets Use Max Formatting.
        pointSeriesTag = pointSeriesTag.ToUpperInvariant().Replace("MINMAX", "MAX");
        if (chartExportValueFormatter == null)
        {
            var formattedData = Macros.DataMacro(pointSeriesTag, data.ToString(), true, true);
            return formattedData;
        }
        
        return chartExportValueFormatter.GetExportedValueWithUnitsFormatted(pointSeriesTag, data.ToString());
    }
    
    /// <summary>
    /// Method produces exported output from chart data.
    /// </summary>
    /// <param name="dChartOptions">Chart's parameters</param>
    /// <param name="lstDataSeries">List of chart's data series</param>
    /// <param name="response">Object via which we produce output</param>
    private static void RenderingExportedDocument(dynamic dChartOptions, List<DataSeries> lstDataSeries, string chartTitle, string chartSubtitle, string strPeriod, IChartExportValueFormatter chartExportValueFormatter, HttpResponse response)
    {
        Func<int> getColumnCount = () =>
        {
            int columnCount = 0;
            for (int i = 0; i < lstDataSeries.Count; i++)
            {
                DataSeries dataSeries = lstDataSeries[i];
                bool bIsMinMaxSerie = dataSeries.Data.Any(item => item.IsMinMaxPoint);
                columnCount++;
                if (bIsMinMaxSerie)
                {
                    columnCount++;
                }
            }

            return columnCount;
        };
        
        response.Write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        response.Write(Environment.NewLine);
        response.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        response.Write(Environment.NewLine);
        response.Write("<body>");
        response.Write(Environment.NewLine);

        response.Write("<table border=\"0\" cellspacing=\"0\" cellpadding=\"4\">");

        int tableColumnCount = 0;
        if (!string.IsNullOrEmpty(chartTitle) || !string.IsNullOrEmpty(chartSubtitle))
        {
            tableColumnCount = getColumnCount();

            // Rendering chartTitle
            response.Write("<tr>" + Environment.NewLine);
            response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + (tableColumnCount + 2) + "\"><font size=\"3\"><b>" + WebSecurityHelper.SanitizeHtmlV2(chartTitle) + "</b></font></td>" + Environment.NewLine);
            response.Write("</tr>" + Environment.NewLine);

            // Sub-Title
            response.Write("<tr>" + Environment.NewLine);
            response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + (tableColumnCount + 2) + "\"><b>" + WebSecurityHelper.SanitizeHtmlV2(chartSubtitle) + "</b></td>" + Environment.NewLine);
            response.Write("</tr>" + Environment.NewLine);
            
            // Time period
            response.Write("<tr>" + Environment.NewLine);
            response.Write("<td class=\"ViewHeader\" align=\"center\" colspan=\"" + (tableColumnCount + 2) + "\"><b>" + strPeriod + "</b></td>" + Environment.NewLine);
            response.Write("</tr>" + Environment.NewLine);
        }
        
        // ======================= start of rendering table header ==========================
        response.Write(Environment.NewLine);
        response.Write("<tr>");
        response.Write(Environment.NewLine);
        response.Write("<td class=\"ReportHeader\" colspan=\"2\">DATE / TIME</td>");
        response.Write(Environment.NewLine);

        Dictionary<int, bool> isMinMaxSerieByIndex = new Dictionary<int, bool>();
        for (int i = 0; i < lstDataSeries.Count; i++)
        {
            DataSeries dataSeries = lstDataSeries[i];
            bool bIsMinMaxSerie = dataSeries.Data.Any(item => item.IsMinMaxPoint);
            isMinMaxSerieByIndex.Add(i, bIsMinMaxSerie);
            string strColspan = bIsMinMaxSerie ? "colspan=\"2\"" : string.Empty;

            string seriesName = GetSeriesName(dataSeries, dChartOptions);

            response.Write(string.Format("<td class=\"ReportHeader\" {0}>{1}</td>{2}", strColspan, seriesName, Environment.NewLine));
        }

        response.Write("</tr>");
        response.Write(Environment.NewLine);
        // ================ end rendering of table header ==================================

        // ======== we need to rearange DataPoints in order to better work with them in exporting =================
        DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        Dictionary<double, List<DataPoint>> dataToExport = new Dictionary<double, List<DataPoint>>();
        
        if (lstDataSeries.Count > 0)
        {
            for (int i = 0; i < lstDataSeries[0].Data.Count; i++)
            {
                var tmpPoint = lstDataSeries[0].Data[i];
                if (!dataToExport.ContainsKey(tmpPoint.UnixTime))
                {
                    dataToExport.Add(tmpPoint.UnixTime, new List<DataPoint> { tmpPoint });
                }
            }

            // Deduplication Data in lstDataSeries
            List<List<DataPoint>> nlstDataSeries = new List<List<DataPoint>>();
            foreach (DataSeries tmpDataSeries in lstDataSeries.Skip(1))
            {
                List<DataPoint> newSerie = new List<DataPoint>();
                foreach (DataPoint tmpPoint in tmpDataSeries.Data)
                {
                    if (!newSerie.Any(item => item.UnixTime == tmpPoint.UnixTime))
                    {
                        newSerie.Add(tmpPoint);
                    }
                }
                
                nlstDataSeries.Add(newSerie);
            }
            
            // Deduplication is done and we can continue with exporting
            foreach (var tmpDataSeries in nlstDataSeries)
            {
                foreach (DataPoint tmpPoint in tmpDataSeries)
                {
                    if (dataToExport.ContainsKey(tmpPoint.UnixTime))
                    {
                        dataToExport[tmpPoint.UnixTime].Add(tmpPoint);
                    }
                    else
                    {
                        dataToExport.Add(tmpPoint.UnixTime, new List<DataPoint> { tmpPoint });
                    }
                }
            }
        }

        var dates = dataToExport.Keys.OrderBy(item => item).Select(item => item);

        // ============== On previous line end rearanging DataPoints  =========================================

        // ================================= start of rendering own chart data ========================
        for (int i = 0; i < dates.Count(); i++)
        {
            var tmpUnixDate = dates.ElementAt(i);
            response.Write("<tr>");
            response.Write(Environment.NewLine);
            DateTime dateOfPoint = _epoch.AddMilliseconds(tmpUnixDate);
            response.Write("<td class=\"Property\">" + dateOfPoint.ToString("dd-MMM-yyyy") + "</td>");
            response.Write(Environment.NewLine);
            response.Write("<td class=\"Property\">" + dateOfPoint.ToShortTimeString() + "</td>");
            response.Write(Environment.NewLine);

            List<DataPoint> lstDataPoint = dataToExport[tmpUnixDate];
            for (int j = 0; j < lstDataPoint.Count; j++)
            {
                DataPoint tmpPoint = lstDataPoint[j];
                var pointSeriesTag = lstDataSeries[j].TagName;
                bool bIsMinMaxSerie = isMinMaxSerieByIndex[j];
                if (!tmpPoint.IsNullPoint)
                {
                    if (tmpPoint.IsMinMaxPoint)
                    {
                        response.Write("<td class=\"Property\">" + FormatData(pointSeriesTag, tmpPoint.Min, chartExportValueFormatter) + "</td>");
                        response.Write(Environment.NewLine);
                        response.Write("<td class=\"Property\">" + FormatData(pointSeriesTag, tmpPoint.Max, chartExportValueFormatter) + "</td>");
                    }
                    else
                    {

                        response.Write("<td class=\"Property\">" + FormatData(pointSeriesTag, tmpPoint.Value, chartExportValueFormatter) + "</td>");
                    }
                }
                else
                {
                    if (bIsMinMaxSerie)
                    {
                        response.Write("<td class=\"Property\"></td>");
                        response.Write(Environment.NewLine);
                    }

                    response.Write("<td class=\"Property\"></td>");
                }

                response.Write(Environment.NewLine);
            }

            response.Write("</tr>");
            response.Write(Environment.NewLine);
        }

        // ======================= end rendering own chart data ===============================

        response.Write(Environment.NewLine);
        response.Write("</table>");
        response.Write(Environment.NewLine);
        response.Write("</body>");
        response.Write("</html>");
    }

    private static string GetSeriesName(DataSeries dataSeries, dynamic chartOptions)
    {
        string seriesName = dataSeries.TagName;

        // Try read name from ChartOptions.
        if ((chartOptions != null) && (chartOptions["seriesTemplates"] != null))
        {
            if (chartOptions["seriesTemplates"].ContainsKey(dataSeries.TagName))
            {
                var dataSeriesTemplate = chartOptions["seriesTemplates"][dataSeries.TagName];
                if (dataSeriesTemplate != null && !string.IsNullOrWhiteSpace(dataSeriesTemplate["name"]))
                {
                    seriesName = dataSeriesTemplate["name"];
                }
            }
            else
            {
                log.Warn(string.Format("Chart options does not contain TagName: {0}", dataSeries.TagName));
            }
        }

        seriesName = string.IsNullOrEmpty(dataSeries.Label) ? seriesName : seriesName + " " + dataSeries.Label;
        
        return HttpUtility.HtmlEncode(seriesName);
    }

    /// <summary>
    /// Method load resource properties from database and return them as dictionary collection where key is name of propertie.
    /// </summary>
    /// <param name="resourceId">Uniqueidentifier of resource.</param>
    /// <returns>Resource properties</returns>
    private Dictionary<string, object> GetResourcePropertiesByResourceId(int resourceId)
    {
        Dictionary<string, object> res = new Dictionary<string, object>();
        using (SqlCommand command = SqlHelper.GetTextCommand("SELECT * FROM dbo.ResourceProperties WHERE ResourceID=@resourceId"))
        {
            command.Parameters.AddWithValue("@resourceId", resourceId);
            DataTable tblResourceProperties = SqlHelper.ExecuteDataTable(command);
            foreach (DataRow row in tblResourceProperties.Rows)
            {
                if (row["PropertyName"] != DBNull.Value)
                {
                    string strPropertyValue = (row["PropertyValue"] != DBNull.Value) ? row["PropertyValue"].ToString() : string.Empty;
                    res.Add(row["PropertyName"].ToString(), strPropertyValue);
                }
            }
        }

        return res;
    }
           
    /// <summary>
    /// Method returns ditionary collection filled by the resource properties from URL params. 
    /// Takes from url params every parameter which starts with rp prefix and into resource.Properties collection give it without rp prefix.
    /// </summary>
    private Dictionary<string, object> GetResourcePropertiesFromQueryStringParams(HttpRequest request)
    {
        Dictionary<string, object> res = new Dictionary<string, object>();
        foreach (var key in request.Params.AllKeys)
        {
            if (key.StartsWith("rp"))
            {
                res[key.Substring(2, key.Length - 2)] = request.Params[key];
            }
        }
        
        return res;
    } 
    
    /// <summary>
    /// Method will contact web service which runs on url passed in parameter url and will try to get data which uses charts for displaying.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="requestCookie"></param>
    /// <param name="resourceProperties">Collection of resource properties filed by url params starts with rp prefix</param>
    /// <param name="postData">parameters needed for passing to calling web service</param>
    /// <returns>DataSeries which serve for displaying charts.</returns>
    private List<DataSeries> GetDataSeriesFromWebService(string url, string requestCookie, Dictionary<string, object> resourceProperties, Dictionary<string, object> displayDetails, string xsrfHeaderValue, params string[] postData)
    {
        List<DataSeries> res = new List<DataSeries>();

        string webServiceParams = @"'NetObjectIds': [{0}],
                                        'SampleSizeInMinutes': {1},
                                        'DaysOfDataToLoad': {2},
                                        'CalculateTrendLine': {3},
                                        'CalculateSum': {4},
                                        'Calculate95thPercentile': {5},
                                        'AllSettings': {6}";
        if (postData[2] == "-1")
            webServiceParams += string.Format( @",'StartTime':'{0}',
                                  'EndTime':'{1}'", postData[3], postData[4]);
        
        if (string.IsNullOrEmpty(postData[0]))
        {
            postData[0] = "''"; // Needed because some web services don't like empty array NetObjectIds and if it is empty they fails with error.
        }

        // generating AllSettings
        Dictionary<string, object> allSettings = new Dictionary<string, object>();
        allSettings["ResourceProperties"] = resourceProperties;

        if (displayDetails != null)
        { // we need this part of code because e.g. APM module has override method GenerateDisplayDetails of StandardChartResource class
            foreach (var key in displayDetails.Keys)
            {
                allSettings[key] = displayDetails[key];
            }
        }
        
        
        string strAllSettings = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(allSettings);
        
        webServiceParams = string.Format(webServiceParams, postData[0], postData[1], postData[2],
            "false", "false", "false", strAllSettings);
        webServiceParams = "{'request': { " + webServiceParams + " }  }";
        
        webServiceParams = webServiceParams.Replace("'", "\"");

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "POST";
        request.ContentType = "application/json";
        request.ServicePoint.Expect100Continue = false;
        request.Timeout = 30000;
        request.Headers.Add("Cookie", requestCookie);

        if (!string.IsNullOrEmpty(xsrfHeaderValue))
        {
            request.Headers.Add(XsrfHeader, xsrfHeaderValue);
        }

        byte[] param = System.Text.ASCIIEncoding.ASCII.GetBytes(webServiceParams);
        // request.ContentLength = param.Length;
        using (Stream stream = request.GetRequestStream())
        {
            stream.Write(param, 0, param.Length);
        }

        using (WebResponse response = request.GetResponse())
        {
            using (StreamReader rd = new StreamReader(response.GetResponseStream()))
            {
                string soapResult = rd.ReadToEnd();
                object chartData = new System.Web.Script.Serialization.JavaScriptSerializer().DeserializeObject(soapResult);
                dynamic dChartData = chartData;
                if ((dChartData != null) && (dChartData["d"] != null) && (dChartData["d"]["DataSeries"] != null))
                {
                    object[] dSeries = (object[])dChartData["d"]["DataSeries"];
                    if ((dSeries != null) && (dSeries.Length > 0))
                    {
                        // begin own export to csv
                        for (int i = 0; i < dSeries.Length; i++)
                        {
                            DataSeries nDataSeries = new DataSeries();
                            Dictionary<string, object> dictionarydSeries = (dSeries[i] as Dictionary<string, object>);
                            nDataSeries.ComputedFrom = (dictionarydSeries.ContainsKey("ComputedFrom") && dictionarydSeries["ComputedFrom"] != null) ? dictionarydSeries["ComputedFrom"].ToString() : string.Empty;
                            nDataSeries.IsCustomDataSerie = (dictionarydSeries.ContainsKey("IsCustomDataSerie") && dictionarydSeries["IsCustomDataSerie"] != null) ? Convert.ToBoolean(dictionarydSeries["IsCustomDataSerie"]) : false;
                            nDataSeries.Label = (dictionarydSeries["Label"] != null) ? dictionarydSeries["Label"].ToString() : string.Empty;
                            nDataSeries.NetObjectId = (dictionarydSeries["NetObjectId"] != null) ? dictionarydSeries["NetObjectId"].ToString() : string.Empty;
                            nDataSeries.ShowTrendLine = (dictionarydSeries.ContainsKey("ShowTrendLine") && dictionarydSeries["ShowTrendLine"] != null) ? Convert.ToBoolean(dictionarydSeries["ShowTrendLine"]) : false;
                            nDataSeries.TagName = (dictionarydSeries["TagName"] != null) ? dictionarydSeries["TagName"].ToString() : string.Empty;
                            object[] dataFromSerie = (object[])dictionarydSeries["Data"];
                            foreach (var point in dataFromSerie)
                            {
                                DataPoint dataPoint = null;
                                if (point is Dictionary<string, object>)
                                {
                                    dataPoint = DataPointWithOptions.CreatePointWithOptions((Dictionary<string, object>)point);
                                }
                                else
                                {
                                    object[] point2 = (object[])point;
                                    if (point2.Length == 1)
                                    {
                                        dataPoint = DataPoint.CreateNullPoint(Convert.ToDouble(point2[0]));
                                    }
                                    else if (point2.Length == 2)
                                    {
                                        dataPoint = DataPoint.CreatePoint(Convert.ToDouble(point2[0]), Convert.ToDouble(point2[1]));
                                    }
                                    else if (point2.Length == 5)
                                    {
                                        dataPoint = DataPoint.CreateMinMaxPoint(Convert.ToDouble(point2[0]), Convert.ToDouble(point2[3]), Convert.ToDouble(point2[1]));
                                    }
                                }

                                if (dataPoint != null)
                                {
                                    nDataSeries.AddPoint(dataPoint);
                                }
                            }
                            
                            res.Add(nDataSeries);
                        }
                    }
                }
            }
        }
        
        return res;
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
