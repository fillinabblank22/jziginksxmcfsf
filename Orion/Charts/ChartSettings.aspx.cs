﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web.UI;

public partial class Orion_Charts_ChartSettings : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (!Profile.AllowAdmin)
        {
            OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new LocalizedExceptionBase(() => Resources.CoreWebContent.WEBCODE_VB0_323), Title = Resources.CoreWebContent.WEBDATA_VB0_567 });
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadChartNameDropDown();
            OnChartNameChanged(this, EventArgs.Empty);
        }

    }

    private void LoadChartNameDropDown()
    {
        const string sql = @"SELECT ChartName, NetObjectPrefix, DisplayName
                             FROM ChartSettings                             
                             ORDER BY NetObjectPrefix, DisplayName";

        using (var cmd = SqlHelper.GetTextCommand(sql))
        using (var reader = SqlHelper.ExecuteReader(cmd))
        {
            while(reader.Read())
            {
                var chartName = DatabaseFunctions.GetString(reader, 0);
                var prefix = DatabaseFunctions.GetString(reader, 1);
                var display = DatabaseFunctions.GetString(reader, 2);

                if (String.IsNullOrEmpty(prefix))
                    prefix = "Summary";

                var toShow = String.Format("{0} - {1}", prefix, display);

                ChartNames.Items.Add(new ListItem(toShow, chartName));
            }
        }
    }

    protected void OnChartNameChanged(object sender, EventArgs e)
    {
        var chartName = ChartNames.SelectedValue;

        const string sql = @"SELECT ChartOptions, ChartEditOptions 
                             FROM ChartSettings
                             WHERE ChartName = @ChartName";

        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddWithValue("@ChartName", chartName);

            using (var reader = SqlHelper.ExecuteReader(cmd))
            {
                while (reader.Read())
                {
                    var chartOptions = DatabaseFunctions.GetString(reader, 0);
                    var editorOptions = DatabaseFunctions.GetString(reader, 1);

                    ChartOptions.Text = chartOptions;
                    EditorOptions.Text = editorOptions;
                }
            }
        }
    }

    protected void OnSave(object sender, EventArgs e)
    {
        var chartName = ChartNames.SelectedValue;
        var options = ChartOptions.Text;
        var editorOptions = EditorOptions.Text;

        const string sql = @"UPDATE ChartSettings SET 
                               ChartOptions = @ChartOptions, 
                               ChartEditOptions = @ChartEditOptions
                           WHERE ChartName = @ChartName";

        using (var cmd = SqlHelper.GetTextCommand(sql))
        {
            cmd.Parameters.AddWithValue("@ChartOptions", options);
            cmd.Parameters.AddWithValue("@ChartEditOptions", editorOptions);
            cmd.Parameters.AddWithValue("@ChartName", chartName);

            SqlHelper.ExecuteNonQuery(cmd);
        }
    }
}