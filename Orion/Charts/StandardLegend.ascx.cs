﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_Charts_StandardLegend : UserControl, IChartLegendControl
{
    public string LegendInitializer { get { return "core_standardLegendInitializer__" + legend.ClientID; } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

}