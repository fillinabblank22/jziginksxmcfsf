<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="CustomChart.aspx.cs" Inherits="Orion_Charts_CustomChart" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Charting.v2" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/Controls/DateTimePicker.ascx" TagPrefix="orion" TagName="DateTimePicker" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
	<orion:Include ID="Include1" runat="server" File="visibilityObserver.js" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menuBarPlaceholder" Runat="Server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <orion:IconLinkExportToPDF ID="IconLinkExportToPDF1" runat="server" />
    <asp:LinkButton ID="btnPrintable" runat="server" CssClass="printablePageLink" OnClick="Printable_Click" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB0_246 %>" />
    <orion:IconHelpButton HelpUrlFragment="OrionPHViewCustomChart" runat="server" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptManagerPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <!-- Here should be placed my code -->
   <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true" />

   <script type="text/javascript">
       // This function will initiate web request which start download file process with data for given chart
       function DownloadChartExportFile (url, data, method) {
           if (url && data) {
               var inputs = '';               
               inputs = '<input type="hidden" name="DisplayDetails" value="' + data + '" />';
               jQuery('<form action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
           }
       }

       function OpenNewWindowWithExportChartData(url, displayDetails) {
           WinId = window.open(url, 'chartDataWindow', 'location=1,status=1,scrollbars=1,menubar=1,resizable=1,status=0');
           $.post(url, { DisplayDetails:  displayDetails  }, function (result) {
               if (typeof jQuery.browser.msie != 'undefined') {
                   WinId.document.open();
                   WinId.document.write(result);
                   WinId.document.close();
               } else {
                   setTimeout(function () { WinId.document.documentElement.innerHTML = result; }, 600);
               }
           });
       }
   </script>
   
   <table cellpadding="8" border="0">
      <tr valign="top">
        <td nowrap class="PageHeader">
            <asp:Literal ID="lblChartTitle" runat="server" />
        </td>
      </tr>
    </table>
   <div style="padding: 10px; background: white;">
      <div ID="chartPlaceHolder" runat="server" />
      <table class="formTable" ID="ChartSettingsTable" runat="server" width="840" cellspacing="0" cellpadding="0">
            <tr class="PageHeader">
              <td style="width: 30%;"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_73) %></td>
              <td style="width: 40%;">
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="Img1" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
                </div>
            </td>
              <td style="width: 30%"></td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
              <td class="formGroupHeader" colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_71) %></td>
            </tr>
            <tr>
                <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBCODE_VB0_102) %></td>
                <td class="formRightInput"><asp:TextBox ID="txtTitle" runat="server" /></td>
                <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr>
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_VB0_163) %></td>
               <td class="formRightInput"><asp:TextBox ID="txtSubtitle" runat="server" /></td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
               <td class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_98) %></td>
               <td class="formRightInput"><asp:DropDownList runat="server" ID="ChartZoom">
                    <asp:ListItem Value="1h" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_99%>"></asp:ListItem>
                    <asp:ListItem Value="2h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 2)%>"></asp:ListItem>
                    <asp:ListItem Value="24h" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_100, 24)%>"></asp:ListItem>
                    <asp:ListItem Value="today" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBCODE_VB0_133%>"></asp:ListItem>
                    <asp:ListItem Value="yesterday" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_101%>"></asp:ListItem>
                    <asp:ListItem Value="7d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
                    <asp:ListItem Value="thisMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_102%>"></asp:ListItem>
                    <asp:ListItem Value="lastMonth" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
                    <asp:ListItem Value="30d" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_188%>"></asp:ListItem>
                    <asp:ListItem Value="3m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
                    <asp:ListItem Value="6m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 6)%>"></asp:ListItem>   
                    <asp:ListItem Value="thisYear" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_105%>"></asp:ListItem>
                    <asp:ListItem Value="12m" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 12)%>"></asp:ListItem>    
                </asp:DropDownList></td>
               <td class="formHelpfulText"></td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
               <td class="formGroupHeader" colspan="3"></td>
            </tr>
            <tr>
              <td class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_311) %></td>
              <td class="formRightInput">
                  <asp:DropDownList runat="server" ID="ChartTimeSpan">
                    <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_107%>"></asp:ListItem>
                    <asp:ListItem Value="7" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_AK0_187%>"></asp:ListItem>
                    <asp:ListItem Value="30" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_103%>"></asp:ListItem>
                    <asp:ListItem Value="90" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 3)%>"></asp:ListItem>
                    <asp:ListItem Value="180" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_104, 6)%>"></asp:ListItem>
                    <asp:ListItem Value="365" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_106%>"></asp:ListItem>
                    <asp:ListItem Value="<%$ HtmlEncodedCode: ChartResourceSettings.CustomChartDateSpan%>" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_OJ0_1%>"></asp:ListItem>
                </asp:DropDownList>
              </td>
              <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr class="CustomDate">
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_367) %></td>
               <td class="formRightInput">&nbsp;</td>
               <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr class="CustomDate">
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_369) %></td>
               <td class="formRightInput"><orion:DateTimePicker ID="dtBeginDateTime" runat="server" /></td>

               <td class="formHelpfulTxt" rowspan="3">&nbsp;</td>
            </tr>
            <tr class="CustomDate">
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_370) %></td>
               <td class="formRightInput"><orion:DateTimePicker ID="dtEndDateTime" runat="server" /></td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
               <td class="formGroupHeader"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_AK0_290) %></td>
               <td class="formRightInput"> <asp:DropDownList runat="server" ID="SampleSizeList">
                                            <asp:ListItem Value="1" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_110 %>"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 5)%>"></asp:ListItem>
                                            <asp:ListItem Value="10" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 10)%>"></asp:ListItem>
                                            <asp:ListItem Value="15" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 15)%>"></asp:ListItem>
                                            <asp:ListItem Value="30" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_111, 30)%>"></asp:ListItem>
                                            <asp:ListItem Value="60" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_112 %>"></asp:ListItem>
                                            <asp:ListItem Value="120" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 2)%>"></asp:ListItem>
                                            <asp:ListItem Value="360" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 6)%>"></asp:ListItem>
                                            <asp:ListItem Value="720" Text="<%$ HtmlEncodedCode: String.Format(Resources.CoreWebContent.WEBDATA_IB0_113, 12)%>"></asp:ListItem>
                                            <asp:ListItem Value="1440" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_114 %>"></asp:ListItem>
                                            <asp:ListItem Value="10080" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_115 %>"></asp:ListItem>
                                        </asp:DropDownList></td>
               <td class="formHelpfulText"></td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
              <td class="formGroupHeader" colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_70) %></td>
            </tr>
            <tr>
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_69) %></td>
               <td class="formRightInput"><asp:TextBox ID="chartWidth" Width="80px" runat="server" /></td>
               <td class="formHelpfulTxt" rowspan="2"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_72) %></td>
            </tr>
            <tr>
               <td class="formLeftTitle"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_68) %></td>
               <td class="formRightInput"><asp:TextBox ID="chartHeight" Width="80px" runat="server" /></td>
            </tr>
            <tr runat="server" id="calculatedSeriesDivider">
                <td colspan="3">
                <hr class="formDivider" />
                </td>
            </tr>
            <tr runat="server" id="calculatedSeriesHeader">
                <td class="formGroupHeader" colspan="3"><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_93) %></td>
            </tr>
            <tr>
                <td class="formLeftTitle"></td>
                <td class="formRightInput"><asp:CheckBox ID="chbCalculateTrendLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_94 %>" runat="server" /></td>
                <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr>
                <td class="formLeftTitle"></td>
                <td class="formRightInput"><asp:CheckBox ID="chbCalculateSumLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_95 %>" runat="server" /></td>
                <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr>
                <td class="formLeftTitle"></td>
                <td class="formRightInput"><asp:CheckBox ID="chbCalculate95thPercentileLine" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_96 %>" runat="server" /></td>
                <td class="formHelpfulText">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3">
                <hr class="formDivider" />
              </td>
            </tr>
            <tr>
		      <td>&nbsp;</td>
		      <td>
                 <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="btnRefresh" OnClick="RefreshClick" LocalizedText="Refresh" DisplayType="Primary" />
                    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />
                </div>
		      </td>
		      <td>
			   &nbsp;
		     </td>
	       </tr>
           <tr>
		      <td colspan="3"><hr class="formDivider"></td>
	       </tr>
	       <tr>
		        <td><b><%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_TM0_74) %></b></td>
		        <td colspan="2">
                    <div class="sw-btn-bar">
                        <orion:LocalizableButtonLink ID="locButtonExcel" DisplayType="Small" 
                            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_33 %>" runat="server" />
                        <orion:LocalizableButtonLink ID="locButtonChart" DisplayType="Small"
                            LocalizedText="CustomText" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_PS0_34 %>" runat="server" />
                    </div>
		        </td>
	        </tr>
      </table>
   </div>
   
   <script type="text/javascript">
       function SetCustomDateVisibility() {
           if( $('#<%=ChartTimeSpan.ClientID %>').val() == '<%= ChartResourceSettings.CustomChartDateSpan%>' )
               $(".CustomDate").show();
           else {
               $(".CustomDate").hide();
           }

       }
       $('#<%=ChartTimeSpan.ClientID %>').change(SetCustomDateVisibility);
       SetCustomDateVisibility();
   </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="outsideFormPlaceHolder" Runat="Server">
</asp:Content>