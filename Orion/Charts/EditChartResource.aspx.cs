﻿using System;
using System.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Helpers;

// NOTE:  This class is basically the same as the /Orion/NetPerfMon/Resources/EditResource.aspx page.  That 
//        page is used for resouces that just provide a edit control.  Charts would like to do this, but that
//        means that our edit control would get used on the Edit Custom Object Resource page.  This page does
//        not support edit controls that make use of view state. (The page is horrible becuase of many different
//        reasons, but the lack of view state is the excuse I'm using today).  This page is designed just for 
//        editing charts but we'll keep the main part of the page in a user control so that it will be easier
//        to move back to that once the COR edit page is fixed.
public partial class Orion_Charts_EditChartResource : Page
{
    private BaseResourceEditControl _editControl;

    public ResourceInfo Resource { get; set; }
    public string NetObjectId { get; set; }
    public int ViewId { get; set; }

    public void SubmitClick(object sender, EventArgs e)
    {
        if (Resource == null)
            return;

        Page.Validate();
        if (!Page.IsValid)
            return;

        string resTitle = TitleEditControl.ResourceTitle;
        string resSubTitle = TitleEditControl.ResourceSubTitle;

        if (_editControl != null)
        {
            foreach (var property in _editControl.Properties)
            {
                if (Resource.Properties.ContainsKey(property.Key))
                    Resource.Properties[property.Key] = property.Value.ToString();
                else
                    Resource.Properties.Add(property.Key, property.Value.ToString());
            }

            if (string.IsNullOrEmpty(resTitle) && !string.IsNullOrEmpty(_editControl.DefaultResourceTitle))
                resTitle = _editControl.DefaultResourceTitle;
            if (string.IsNullOrEmpty(resSubTitle) && !string.IsNullOrEmpty(_editControl.DefaultResourceSubTitle))
                resSubTitle = _editControl.DefaultResourceSubTitle;
            
            //Right now edit page does not support custom date ranges
            Resource.Properties.Remove("DateFromUtc");
            Resource.Properties.Remove("DateToUtc");
        }

        if (String.IsNullOrEmpty(resTitle))
        {
            //The original localized title was destroyed. Let's fetch the control's default.
            using (var resourceControl = (BaseResourceControl)LoadControl(Resource.File))
                Resource.Title = resourceControl.Title;
        }
        else
            Resource.Title = resTitle;

        Resource.SubTitle = resSubTitle;

        ResourcesDAL.Update(Resource);

        Response.Redirect(
            BaseResourceControl.GetEditPageReturnUrl(
                Request.QueryString,
                ((ViewId > 0) ? ViewId : Resource.View.ViewID).ToString()));
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        int resourceId;
        Int32.TryParse(Request.QueryString["ResourceID"], out resourceId);
        Resource = ResourceManager.GetResourceByID(resourceId);

        int viewId;
        Int32.TryParse(Request.QueryString["ViewID"], out viewId);
        ViewId = viewId;

        NetObjectId = WebSecurityHelper.SanitizeHtmlV2(Request.QueryString["NetObject"]);

        LoadResourceEditControl();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Resource != null)
        {
            if (!IsPostBack)
            {
                TitleEditControl.ResourceTitle = Resource.Title;
                TitleEditControl.ResourceSubTitle = Resource.SubTitle;
                ResourceName.Text = WebSecurityHelper.HtmlEncode(Resource.Title);
            }
            Title = String.Format(Resources.CoreWebContent.WEBCODE_AK0_88, Resource.Title);
        }
    }

    private void LoadResourceEditControl()
    {
        if (Resource == null)
            return;
        
        const string editControlLocation = "/Orion/NetPerfMon/Controls/EditResourceControls/EditChart.ascx";

        _editControl = LoadControl(editControlLocation) as BaseResourceEditControl;

        if (_editControl != null)
        {
            TitleEditControl.ShowSubTitle = _editControl.ShowSubTitle;
            TitleEditControl.ShowSubTitleHintMessage = _editControl.ShowSubTitleHintMessage;
            TitleEditControl.SubTitleHintMessage = _editControl.SubTitleHintMessage;
            _editControl.Resource = Resource;
            _editControl.NetObjectID = NetObjectId;
            phResourceEditControl.Controls.Add(_editControl.TemplateControl);
        }
        
    }
}