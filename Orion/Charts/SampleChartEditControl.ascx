<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SampleChartEditControl.ascx.cs" Inherits="Orion_Charts_SampleChartEditControl" %>

<div class="sw-res-editor-row">
    <%= DefaultSanitizer.SanitizeHtml(Resources.CoreWebContent.WEBDATA_IB0_121) %>
    <asp:DropDownList runat="server" ID="LineColor">
        <asp:ListItem Value="" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_122 %>"></asp:ListItem>
        <asp:ListItem Value="red" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_123 %>"></asp:ListItem>
        <asp:ListItem Value="green" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_124 %>"></asp:ListItem>
        <asp:ListItem Value="black" Text="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_IB0_125 %>"></asp:ListItem>
    </asp:DropDownList>
</div>

