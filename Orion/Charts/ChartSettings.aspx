﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="ChartSettings.aspx.cs" Inherits="Orion_Charts_ChartSettings" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.Charting.v2" Assembly="OrionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ResourcesStyleSheetPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadPlaceHolder" Runat="Server">
    <orion:IncludeCharts runat="server" />


    <script type="text/javascript">
        $(function () {
            var config = SW.Core.Charts.getBasicChartConfig();
            $('#baseChartOptions').text(JSON.stringify(config, null, 4));
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptManagerPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="outsideFormPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1>Edit Chart Settings</h1>

<div style="padding:0px 20px;">

    <p><i>This page is an unsupported testing page only.</i></p>

    Chart Name: <asp:DropDownList runat="server" ID="ChartNames" AutoPostBack="true" OnSelectedIndexChanged="OnChartNameChanged"/>

    <span style="padding-left: 50px;">
        <orion:LocalizableButton runat="server" ID="SaveButton" LocalizedText="Save" DisplayType="Primary" OnClick="OnSave"></orion:LocalizableButton>
    </span>

    <hr/>
    
    <table>
        <tr style="vertical-align: top;">
            <td>
                Chart Options:<br/>
                <asp:TextBox runat="server" ID="ChartOptions" TextMode="MultiLine" Columns="60" Rows="40" ></asp:TextBox>
            </td>
            <td style="padding-left: 50px;">
                Base Chart Options:<br/>
                <textarea id="baseChartOptions" cols="60" rows="40" readonly="readonly"></textarea>
                
            </td>
        </tr>
        <tr>
            <td>
                Chart Editor Options:<br/>
                <asp:TextBox runat="server" ID="EditorOptions" TextMode="MultiLine" Columns="60" Rows="10" ></asp:TextBox>
            </td>
        </tr>
    </table>

    
</div>

</asp:Content>


