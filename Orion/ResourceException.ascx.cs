using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.UI;

public partial class ResourceException : BaseResourceExceptionControl
{
	protected override string DefaultTitle
	{
		get { return ""; }
	}

    private bool _showHeaderBar = true;
    private bool _showEditButton = true;
    //private bool _showDrDownMenu = false;
    private bool _showManageButton = false;
    private string _manageButtonImage;
    private string _manageButtonTarget;

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowHeaderBar
    {
        get { return _showHeaderBar; }
        set { _showHeaderBar = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowEditButton
    {
        get { return _showEditButton; }
        set { _showEditButton = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ShowManageButton
    {
        get { return _showManageButton; }
        set { _showManageButton = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonImage
    {
        get { return _manageButtonImage; }
        set { _manageButtonImage = value; }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string ManageButtonTarget
    {
        get { return _manageButtonTarget; }
        set { _manageButtonTarget = value; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return ResourcePlaceHolder; }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder HeaderButtons
    {
        get { return HeaderButtonsPlaceHolder; }
    }

    protected new string Title
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.DisplayTitle;
            else
                return "Orion Web Resource";
        }

    }

    protected new string SubTitle
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.SubTitle;
            else
                return string.Empty;
        }
    }

    protected string HelpUrlFragment
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.HelpLinkFragment;
            else
                return string.Empty;
        }
    }

    protected new string DetachURL
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.DetachURL;
            else
                return string.Empty;
        }
    }

    protected int Width
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.Resource.Width;
            else
                return 0;
        }
    }

    protected string EditLink
    {
        get
        {
            BaseResourceControl parent = GetParentResource();
            if (parent != null)
                return parent.EditURL;
            else
                return string.Empty;
        }
    }

    protected bool IsPresentInDB()
    {
        BaseResourceControl parent = GetParentResource();
        if (parent != null && parent.Resource != null)
        {
            return parent.Resource.ID != 0;
        }
        else
        {
            return false;
        }
    }

    protected BaseResourceControl GetParentResource()
    {
        return GetParentResource(this);
    }

    protected BaseResourceControl GetParentResource(Control control)
    {
        if (control == null)
            return null;
        else if (control is BaseResourceControl && ((BaseResourceControl)control).Resource != null)
            return (BaseResourceControl)control;
        else
            return GetParentResource(control.Parent);
    }

    protected override void OnLoad(EventArgs e)
    {
        this.HelpButton.HelpUrlFragment = this.HelpUrlFragment;

        base.OnLoad(e);

        headerBar.Visible = ShowHeaderBar;
        phEditButton.Visible = IsPresentInDB() && ShowEditButton && Profile.AllowCustomize && !string.IsNullOrEmpty(EditLink);

        ManageButton.NavigateUrl = ManageButtonTarget;
        ManageButton.ImageUrl = ManageButtonImage;
        ManageButton.Visible = true;
    }
}
