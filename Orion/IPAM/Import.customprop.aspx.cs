using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects.Enumerations;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportGroupPickcolumns : CommonPageServices
	{
        private PageImportCustomProp mgr;
        public ImportGroupPickcolumns() { mgr = new PageImportCustomProp(this); }

	    protected void Page_Load(object sender, EventArgs e)
		{
            if (hfAddAll.Value.Equals("true"))
            {
                hfAddAll.Value = "";
                mgr.AddAllToCustom();
            }

            if (hfAddSingle.Value.Equals("true"))
            {
                hfAddSingle.Value = "";
                mgr.AddSingleCustomProperty(ColumnNameCP.Value, DescriptionCP.Value);
            }

            if (Page.Request.QueryString["SaveCmb"] != null)
                mgr.Save();

            if (Request.QueryString["GroupID"] != null)
                this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));
		}

        protected void Back_Click(object src, EventArgs args)
        {
            string pageName = "Import.struct.pickcolumns.aspx";

            if (mgr.ImportData.OpenedFrom == (int)ImportOpenFromTypes.ImportPickColumns 
                || (mgr.ImportData.OpenedFrom == (int)ImportOpenFromTypes.ImportCustomProp && mgr.ImportData.IsZipImport))
            {
                pageName = "Import.pickcolumns.aspx";
            }

            Response.Redirect(
                string.Format("{0}?ObjectID={1}&Type={2}{3}&ImportType={4}&OpenedFrom=4&IsZipImport={5}{6}",
                pageName,
                mgr.ImportData.Filename,
                mgr.ImportData.Type,
                mgr.ImportData.GroupId >= 0 ? "&GroupID=" + mgr.ImportData.GroupId : string.Empty,
                mgr.ImportData.ImportType,
                mgr.ImportData.IsZipImport,
                mgr.ImportData.CreateType > 0 ? "&CreateType=" + mgr.ImportData.CreateType : string.Empty
            ), false);
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
    }
}
