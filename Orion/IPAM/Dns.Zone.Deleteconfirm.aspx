﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
    CodeFile="Dns.Zone.Deleteconfirm.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DnsZoneDeleteConfirm" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_378 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <IPAM:AccessCheck ID="AccessCheck1" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener ID="MsgListener" Name="dialog" runat="server">
    <Msgs>
        <IPAMmaster:WindowMsg ID="WindowMsg1" Name="Delete" OnMsg="MsgDelete" runat="server" />
    </Msgs>
</IPAMmaster:WindowMsgListener>
<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-dns-manage.js" Orientation="jsInit"
    runat="server">
    var dat = $SW.Env && $SW.Env['deleteopt'];
    if( dat )
    {
        $(window).load( function(){ $SW.msgq.UPSTREAM('confirmed',$SW.Env['deleteopt']); });
    }
    else
    {
        $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
        $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
        $(document).ready( function(){ Ext.QuickTips.init(); });
    }
</IPAMmaster:JsBlock>
<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
.grid { max-height: 180px; overflow: auto; padding-top: 10px; margin-bottom: 15px; }
* html .grid { height: 180px; }
.gridColumn { background-color: #E5F1F7;  padding: 5px 15px 5px 0; vertical-align:middle;}
.gridColumnAlternate { background-color: #FFFFFF; padding: 5px 0 5px 0; vertical-align:middle; }

div.sw-form-heading { font-size: small; margin-top: 16px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: small; margin: 0; }
</IPAMmaster:CssBlock>
<div id="formbox">
<div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_218 %></div>

    <div style="background-color: #FFFFFF; padding: 10px;">
    <asp:PlaceHolder ID="DhcpServerPlaceHolder" runat="server" Visible="false">
        <div class="sw-form-heading">
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_379 %>"></asp:Label>
        </div>
    <div class="grid">

            <asp:Repeater ID="ServerRepeater" runat="server">
            <HeaderTemplate><ul class="status-bg"></HeaderTemplate>
             <ItemTemplate>
                    <li class="gridColumn <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%>"> <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                    </li>
                </ItemTemplate>
                <AlternatingItemTemplate>
                <li class="gridColumnAlternate <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%>"> <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                    </li>
                </AlternatingItemTemplate>
            <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
    </div>  

    </asp:PlaceHolder>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:PlaceHolder ID="DnsZonePlaceHolder" runat="server">

    <div class="sw-form-heading">

    <asp:Label ID="DnsZoneWarningLabel" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_380 %>"></asp:Label>
    <asp:Label ID="ZoneWarningMsgLabel" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_381 %>" Visible="false"></asp:Label>

    </div>

    <div class="grid">
            <asp:Repeater ID="ZoneRepeater" runat="server">
                <HeaderTemplate><ul class="status-bg"> <b><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_1 %></b> </HeaderTemplate>
                <ItemTemplate>
                    <li class="gridColumn <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%>"> 
                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>  <%# HttpUtility.HtmlEncode(Convert.ToString(DataBinder.Eval(Container.DataItem,"ViewName"))!=string.Empty? string.Format(" - {0}", DataBinder.Eval(Container.DataItem,"ViewName").ToString()) : string.Empty)%>
                    </li>
                </ItemTemplate>
                <AlternatingItemTemplate>
                <li class="gridColumnAlternate <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%>"> 
                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%> <%# HttpUtility.HtmlEncode(Convert.ToString(DataBinder.Eval(Container.DataItem,"ViewName"))!=string.Empty? string.Format(" - {0}", DataBinder.Eval(Container.DataItem,"ViewName").ToString()) : string.Empty)%>
                    </li>
                </AlternatingItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
    </div>
    </asp:PlaceHolder>

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:PlaceHolder ID="RemoveZonePlaceHolder" runat="server">
        <div style="margin-bottom:10px">
          <asp:CheckBox ID="ChkRemoveZoneFromDnsServer" runat="server" AutoPostBack="true" Checked="false" />&nbsp;<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_382 %>
        </div>
        </asp:PlaceHolder>
      
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
</div>
</asp:Content>
