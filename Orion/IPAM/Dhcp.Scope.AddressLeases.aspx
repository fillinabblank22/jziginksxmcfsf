<%-- TODO: IPAM-3272 remove when doing code cleanup for 2020.2 release --%>

<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
    Inherits="SolarWinds.IPAM.WebSite.DhcpScopeAddressLeases" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_623%>"
    CodeFile="Dhcp.Scope.AddressLeases.aspx.cs" ValidateRequest="false" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DhcpServerScanSettings.ascx" TagName="DhcpServerScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NewDhcpScopeSettings.ascx" TagName="NewDhcpScopeSettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
 
    <IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-helpserver,sw-subnet-manage.js,sw-dhcp-manage.js,sw-ux-wait.js" Orientation="jsPostInit" runat="server">
            $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
            $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
    </IPAMmaster:JsBlock>
    <IPAMui:ValidationIcons runat="server" />
    <div id="formbox">
        <div id="ChromeFormHeader" runat="server" class="sw-form-header"> <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_624 %></div>

        <IPAMlayout:ViewPort ID="viewport" runat="server">
            <IPAMlayout:BorderLayout ID="mainlayout" runat="server" borderVisible="false">
                <Center>
                    <IPAMlayout:GridBox runat="server" ID="AddressLeases" UseLoadMask="True"
                        autoHeight="false" emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_625 %>" borderVisible="true"
                        deferEmptyText="false" RowSelection="true"  listeners="{ statesave: $SW.IPAM.DhcpAddressLeasesStateSave }">
                        <PageBar pageSize="100" displayInfo="true" />
                        <Columns>                                       
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_626 %>" width="80" isSortable="true" dataIndex="ClientIpAddress"  runat="server" />
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_627 %>" width="150" isSortable="true" dataIndex="ClientName" runat="server"  />
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_277 %>" width="100" isSortable="true" dataIndex="ClientLeaseExpires" runat="server" renderer="$SW.IPAM.DhcpIpLeaseExpiresRenderer($SW.ExtLocale.DateFull)" />
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="60" isSortable="true" dataIndex="LeaseType" runat="server" renderer="$SW.IPAM.DhcpLeaseTypeRenderer()" />
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_628 %>" width="60" isSortable="true" dataIndex="ClientStatus" runat="server" renderer="$SW.IPAM.DhcpLeaseStatusRenderer()"/>
                            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_629 %>" width="100" isSortable="true" dataIndex="ClientMAC" runat="server" />                             
                        </Columns>
                        <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx"
                            proxyEntity="IPAM.DhcpLease" AmbientSort="ClientIpAddress"
                            listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                            <Fields> 
                                <IPAMlayout:GridStoreField  dataIndex="ClientIpAddress" isKey="True" runat="server" />
                                <IPAMlayout:GridStoreField  dataIndex="ClientName" runat="server" />
                                <IPAMlayout:GridStoreField  dataIndex="ClientLeaseExpires" runat="server" dataType="date" dateFormat="Y-m-d H:i:s"/>
                                <IPAMlayout:GridStoreField  dataIndex="ClientStatus" runat="server" />
                                <IPAMlayout:GridStoreField  dataIndex="LeaseType" runat="server" />
                                <IPAMlayout:GridStoreField  dataIndex="ClientMAC" runat="server" />                                
                            </Fields>
                        </Store>
                    </IPAMlayout:GridBox> 
                </Center>
            </IPAMlayout:BorderLayout>
       </IPAMlayout:ViewPort>

    </div>
    <table>
        <tr>
            <td>
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" NavigateUrl="/api2/ipam/ui/scopes"
                        runat="server" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
