using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class GroupEdit : CommonPageServices
    {
        private PageSubnetEditor editor;
        private readonly VrfGroupExistsHelper vrfHelper;

        public GroupEdit()
        {
            editor = new PageSubnetEditor(this, false);
            vrfHelper = new VrfGroupExistsHelper();
            editor.SaveCustomProperties = SaveCustomProperties;
            editor.GetCustomProperties = GetCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties group)
        {
            CustomPropertyRepeater.GetChanges(group);
        }

        public void GetCustomProperties(ICustomProperties group, IList<int> ids)
        {
            CustomPropertyRepeater.RetrieveCustomProperties(group, ids);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void OnVrfGroupNameExists(object source, ServerValidateEventArgs args)
        {
            if (editor.GroupType == GroupNodeType.Root)
            {
                args.IsValid = vrfHelper.CheckVrfGroupNameNotExists(this.txtDisplayName.Text, args.Value, false, editor.GroupId);
            }
        }
    }
}
