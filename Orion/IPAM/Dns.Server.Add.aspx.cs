﻿using System;
using System.Data;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.Orion.Web.Helpers;
using System.Text;
using System.IO;
using SolarWinds.Orion.Core.SharedCredentials.Credentials;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsServerAdd : CommonPageServices
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            if (this.Page.IsPostBack == false)
            {
                var redirector = new DhcpDnsServerAddRedirector(this);
                redirector.HandleAddOrionNodePage(true);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            SwisConnector.RunSwisOps(new SwisOpCtx[]{ 
                new SwisOpCtx("Retrieve DNS Server List", OpGetDnsServers )
                 });
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                btnSave.OnClientClick = "javascript:demoAction('IPAM_Add_DNS_Server',null); return false;";
            }
            else
            {
                bool isFreeTool = false;
                bool isOtherModuleInstalled = false;
                isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);
                if (isFreeTool && !isOtherModuleInstalled)
                    btnSave.OnClientClick = string.Format("alert('{0}'); return false;", Resources.IPAMWebContent.IPAMWEBDATA_DF1_45);
            }

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);


            btnSave.Click += new EventHandler(OnSave);
            if (!this.IsPostBack)
            {
                UseSWIS("Fill default values", FillDefaultValues);
                SelectOrionNodes();
            }

        }

        void FillDefaultValues(IpamClientProxy proxy)
        {           
            Setting scanInterval = proxy.AppProxy.Setting.GetGlobal(SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANINTERVAL);
            if (scanInterval != null)
            {
                TimeSpan? span;
                if (scanInterval.TryGetValue(out span) && span.HasValue)
                    ServerScanInterval.Value = span.Value;
            }
        }

        void SelectOrionNodes()
        {
            // get NetObjectId to pre-select added orion node
            string netObjectId = AddNodeReturnPageHelper.NetObjectId;

            // and cleanup session value
            AddNodeReturnPageHelper.NetObjectId = null;

            if ((this.PickHost != null) && !string.IsNullOrEmpty(netObjectId))
            {
                this.PickHost.Selection = netObjectId;
            }
        }

        private object OpGetDnsServers(IpamClientProxy proxy, object param)
        {
            Dictionary<string, int> ids = new Dictionary<string, int>();

            using (DataTable dt = proxy.SwisProxy.Query("SELECT G.NodeId from IPAM.DnsServer G"))
            {
                foreach (DataRow dr in dt.Rows)
                    ids[((int)dr[0]).ToString()] = 1;
            }

            AddJsEnv("ExistingDnsServers", ids);
            return null;
        }

        protected void OnSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            Page.Validate();
            Page.Validate("NewCred");

            if (Page.IsValid && (SelectedNodeId != null))
            {
                BusinessObjects.DnsServer Objdns = new BusinessObjects.DnsServer
                {
                    //Dns Properties
                    NodeId = int.Parse(SelectedNodeId.Value),                   
                    ServerType = CredSelector.DnsServerType,
                    CredentialId = StoreCredentials(),
                    IncrementalZoneTransfer = cbIncrementalZoneTransfer.Checked,

                    //Group Property
                    //GroupId = 0,
                    ParentId = 2,
                    GroupType = BusinessObjects.GroupNodeType.DnsServer,
                };

                if (cbEnableScanning.Checked)
                {
                    TimeSpan span = ServerScanInterval.Value;
                    Objdns.ScanInterval = Convert.ToInt32(Math.Round(span.TotalMinutes));                    
                    Objdns.DisableAutoScanning = false;
                }
                else
                {                   
                    Objdns.DisableAutoScanning = true;
                }

                if (Objdns.ServerType != DnsServerType.Windows)
                    Objdns.IncrementalZoneTransfer = false;
                
                int serverid = -1;
                SwisOpCtx addDnsServerSwisOpCtx = new SwisOpCtx(
                    "Add new DNS Server",
                    delegate(IpamClientProxy proxy, object param)
                    {
                        return serverid = proxy.AppProxy.DnsServer.Add(Objdns);
                    }
                );
                SwisConnector.RunSwisOps(new SwisOpCtx[] { addDnsServerSwisOpCtx });

                string refUrl = "~/api2/ipam/ui/dns";

                DiscoveryCentralHelper.Return(refUrl);
            }
        }

        private Nullable<int> StoreCredentials()
        {
            Nullable<int> credentialId = CredSelector.CredId;
            if (credentialId != null)
                return credentialId;

            ICredentials credentials = CredSelector.GetCredentials();

            // windows credentials are WMI credentials from Core, therefore need to handle them separately.
            if (credentials is WindowsCredential)
            {
                // for inherited credentials there is nothing to do.
                if (credentials.ID == -1)
                    return -1;

                return CreateWMICredentials((WindowsCredential)credentials);
            }

            // handle all other types of credentials as usual.
            SwisOpCtx createNewCredentialSwisOpCtx = new SwisOpCtx(
                "Create New Credential",
                delegate(IpamClientProxy proxy, object param)
                {
                    credentialId = proxy.AppProxy.Credentials.Create(credentials);
                    return null;
                }
            );
            SwisConnector.RunSwisOps(new SwisOpCtx[] { createNewCredentialSwisOpCtx });
            return credentialId;
        }

        /// <summary>
        /// Creates WMI credentail and returns credential id.
        /// </summary>
        /// <returns>credential id</returns>
        private Nullable<int> CreateWMICredentials(WindowsCredential credential)
        {
            UsernamePasswordCredential rawCredential = new UsernamePasswordCredential()
            {
                Name = credential.Name,
                Username = credential.UserName,
                Password = credential.Password,
            };

            using (ICoreBusinessLayer proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(null))
                return proxy.InsertWmiCredential(rawCredential, CoreConstants.CoreCredentialOwner).Value;
        }

        protected void OnAddOrionNodeClick(object src, EventArgs args)
        {
            var redirector = new DhcpDnsServerAddRedirector(this);
            redirector.RedirectByOrionNodeCount(0);
        }

        protected string AddNodeLinkBeginTag()
        {
            addNodeLink.Visible = true;
            var textBuilder = new StringBuilder();

            using (var sw = new StringWriter(textBuilder))
            using (var textWriter = new HtmlTextWriter(sw))
                addNodeLink.RenderBeginTag(textWriter);

            addNodeLink.Visible = false;

            return textBuilder.ToString();

        }
    }
}
