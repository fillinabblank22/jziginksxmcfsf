<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="True" CodeFile="Import.pickcolumns.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportPickcolumns"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_350 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewSpreadsheetColumnsImport" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
   
   <IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
   
   <IPAMmaster:JsBlock Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server>
    $SW['PossibleValues'] = [];
    $SW['ColumnUsage'] = {};
    $SW['ColumnBoxes'] = [];
    $SW.isIpSelected = false;
    var _inProgress = true;

    $SW.StartValidation = function(){
      if( !$SW.ValidateRequiredColumns() ) {
          return false;
      }
      // Sending CmbBox values in Query
      var s='';
      Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( c ) 
           s += c.value+'|';
        });

      $.ajax({
        type: "POST",
        url: location.href+'&SaveCmb='+s,
        async: false,
        data: '',
        success: function() {
          $SW.ShowValidationProgressBar();
        },
        error: function(jqXHR, textStatus, errorThrown) { 
            err(jqXHR.statusText + '<br/>' + jqXHR.responseText);
            return false;
        }
      });

      return false;
    };


    $SW.ValidateRequiredColumns = function(){
      if( !$SW.isIpSelected ) {
          $SW.Alert( "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_74;E=js}", "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_75;E=js}", { buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING } );
          return false;
      }
      return true;
    };

    Ext.onReady(function()
     {
        var boxes = $SW['ColumnBoxes'];
        var transform = function(id){
            var item = $('#'+id)[0];
            var converted = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                transform: id,
                forceSelection: true,
                disabled: item.disabled || false
            });

            return converted;
        };

        $("select").each(function(i,o){
            var cb = transform(o.id);
            cb.store.sortInfo = { field: "value", direction: "ASC" };
            cb.on('change',function(that,newVal,oldVal){
              $SW.ComboUsageInUse(newVal,that.id);
              $SW.ComboUsageNotInUse(oldVal,that.id);
            });
            boxes.push(cb.id);
        });

        $SW.InitPossibleValue();
     });

     $SW.ComboUsageInUse = function(val,by)
     {
        if(val == "-1") return; // do not import okay for all.

        var r = $SW.ColumnUsage[val];
        if( !r ) return;
        r.inuse = by;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c || c.id == by ) return;

         var exist = c.store.getById(val);
         if( exist )
           c.store.remove(exist);
        });

        $SW.UpdateNextButton();
     };

     $SW.ComboUsageNotInUse = function(val)
     {
        var r = $SW.ColumnUsage[val];
        if( !r ) return;
        r.inuse = null;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c ) return;

         var exist = c.store.getById(val);
         if( !exist )
            c.store.addSorted(new Ext.data.Record({ value: val, text: r.text }, val));
        });

        $SW.UpdateNextButton();
     };

     $SW.UpdateNextButton = function()
     {
        $SW.isIpSelected = Ext.getCmp($SW['ColumnBoxes'][0]).value != -1;
        if( $SW.isIpSelected ) $SW.ns$($nsId,'Preview').removeClass('sw-form-dim');
        else $SW.ns$($nsId,'Preview').addClass('sw-form-dim');
     };

     $SW.InitPossibleValue = function()
     {
        var seen = {};
        var vals = [];

        Ext.each( $SW['ColumnBoxes'], function(o){
          var c = Ext.getCmp(o), v;
          if( !c ) return;
           v = c.getValue();

           c.store.each(function(r){
            if( !seen[r.data.value] )
            {
              var n = Ext.apply({ inuse: null }, r.data);
              seen[r.data.value] = n;
              vals.push(n);
            }
            return true;
           });

           if( v == "-1" )
             return;

           seen[v].inuse = c.id;
        });

        Ext.each($SW['ColumnBoxes'],function(id){
          var c = Ext.getCmp(id), v;
          if( !c ) return;
          Ext.each(vals,function(o){
            if( o.inuse && o.inuse != id )
            {
              var r = c.store.getById(o.value);
              if( r ) c.store.remove(r);
            }
          });
        });

        $SW['ColumnUsage'] = seen;
        $SW.UpdateNextButton();
     }
   
    Ext.getUrlParam = function(param)
    {
       var params = Ext.urlDecode(location.search.substring(1));
       return param ? params[param] : params;
    };
    
    $SW.ConfirmBack = function()
    {
		$SW.GetUrlParamForImport();
		
		var commonParams = 'ObjectID=' + $SW['ObjectID'] +'&Type=' + $SW['Type'] + '&ImportType=2';
		if($SW['GroupID'] != null)
			commonParams += "&GroupID=" + $SW['GroupID'];

		if ($SW['CreateType'] != null)
			commonParams += "&CreateType=" + $SW['CreateType'];
		
		if ($SW['IsZipImport'] != null)
			commonParams += "&IsZipImport=" + $SW['IsZipImport'];

		if ($SW['OpenedFrom'] != null)
			commonParams += "&OpenedFrom=" + $SW['OpenedFrom'];

		if ($SW['IsZipImport'] != null && $SW['IsZipImport'].toLowerCase() === 'true') {
			window.location = 'Import.struct.pickcolumns.aspx?'+commonParams;
		} else {
			Ext.MessageBox.confirm(
				'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_76;E=js}',
				'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_77;E=js}',
				function(btnid){
				if( btnid == 'yes' )
				{
					var groupId = Ext.getUrlParam('GroupID');
					if(groupId) {
						window.location = 'import.upload.aspx?GroupID=' +groupId;
					}
					else {
						window.location = 'import.upload.aspx';
					}    
				}
			});
		}
    };

    //progress indicator

	Ext.getUrlParam = function(param) {
		var params = Ext.urlDecode(location.search.substring(1));
		return param ? params[param] : params;
	};
		
	$SW.GetUrlParamForImport = function() {
		$SW['ObjectID'] = Ext.getUrlParam('ObjectID');
		$SW['Type'] = Ext.getUrlParam('Type');
		$SW['GroupID'] = Ext.getUrlParam('GroupID');
		$SW['ImportType'] = Ext.getUrlParam('ImportType');
		$SW['CreateType'] = Ext.getUrlParam('CreateType');
		$SW['IsZipImport'] = Ext.getUrlParam('IsZipImport');
		$SW['OpenedFrom'] = Ext.getUrlParam('OpenedFrom');
	};

	function err(msg)
	{
		msg = msg || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}';

		Ext.Msg.show({
			title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
			msg: msg,
			buttons: Ext.Msg.OK,
			animEl: this,
			icon: Ext.MessageBox.ERROR
		});        
	}
		
	function StartTask()
	{
		$SW.GetUrlParamForImport();
		
		var dataparams =  'requesttype=validateimportip&verb=CreateTask&filetype='+$SW['Type']+'&objectid='+$SW['ObjectID'];

		if($SW['GroupID'] != null)
		dataparams += "&GroupID=" + $SW['GroupID'];

		if ($SW['CreateType'] != null)
		dataparams += "&CreateType=" + $SW['CreateType'];
		
		if($SW['IsZipImport'] != null)
		dataparams += "&IsZipImport=" + $SW['IsZipImport'];
			
		$.ajax({
			type: "POST",
			url: 'ImportExportProvider.ashx',
			async: false,
			data: dataparams,
			error: err
		});
	}

	function RecvTaskStatus(o)
	{
		if( o.status === 'Completed' ) {
			_inProgress = false;
			ProgressEnd('ok',o);
		} else {
			if (o.status === 'Failed')
			ProgressEnd('failed',o.error);
			else
			{
				var val = typeof(o.pos) == "string" ? eval(o.pos) : o.pos;
				val /= 100;
                window.localStorage.setItem("lastAction", Date.now());
				Ext.MessageBox.updateProgress(val , o.msg, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}');
			}
		}
	}

	function GetTaskStatus()
	{
		Ext.Ajax.request({
			url: 'ImportExportProvider.ashx',
			method: 'POST',
			success: function(result, request)
			{
					var o = Ext.util.JSON.decode(result.responseText);
					RecvTaskStatus(o);
			},
			failure: function(result, request) {
						if(runner!=null)
							runner.stopAll();
						},
			params:
			{
				requesttype: 'validateimportip',
				verb: 'GetStatusValidateTask',
				objectid:$SW['ObjectID']
			}
		});
	}

	function ProgressEnd(btnid,value)
	{
		Ext.MessageBox.hide();
		if(runner!=null)
			runner.stopAll();
			
		if( btnid === 'ok' )
		{
			var commonParams = 'ObjectID=' + $SW['ObjectID'] +'&Type=' + $SW['Type'] + '&ImportType=' + $SW['ImportType'] + '&OpenedFrom=1';
			if($SW['GroupID'] != null)
				commonParams += "&GroupID=" + $SW['GroupID'];

			if ($SW['CreateType'] != null)
				commonParams += "&CreateType=" + $SW['CreateType'];
			
			if ($SW['IsZipImport'] != null)
				commonParams += "&IsZipImport=" + $SW['IsZipImport'];
			
			if (value.validationSuccess == false)
			{
				var link = $SW.appRoot() + 'Orion/IPAM/Import.preview.aspx?' + commonParams;
				
				window.location.href = link;
			}
			else
			{
				var link = $SW.appRoot();
				if ($SW['IsZipImport'] != null && $SW['IsZipImport'].toLowerCase() === 'true') {
				link += 'Orion/IPAM/Import.customprop.aspx?' + commonParams;
				} else {
				link += 'Orion/IPAM/Import.struct.pickcolumns.aspx?' + commonParams;
				}

				window.location.href = link;
			}
		}
		
		if( btnid === 'failed' && _inProgress === true)
			err(value);
	}

	var runner = new Ext.util.TaskRunner();
	var StatusTask = {
		run: function(){
			GetTaskStatus();
		},
		interval: 500
	}

	$SW.ShowValidationProgressBar = function(el)
	{
		_inProgress = true;
		StartTask();

		Ext.MessageBox.show({
			title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_79;E=js}',
			msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}',
			progressText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_62;E=js}',
			width:300,
			buttons:false,
			progress:true,
			closable:false
		});
	
		runner.start(StatusTask);
	};
   </IPAMmaster:JsBlock>

<div style="margin-left: 10px;">

     <table width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_350 %></h1>
        </td>
        </tr>
      </table>

    <div id="formbox">

        <div style="padding-bottom: 10px;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_351 %> <br />
        </div>

        <div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_352 %></div>

        <div><!-- fix layout --></div>

        <div style="padding-left:20px;">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-table">
                    <td class="sw-form-col-label-large"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                    <td><div>
                        <asp:Label ID="IpName" runat="server"></asp:Label>
                        <asp:Label ID="IpEntity" runat="server" Visible="false"></asp:Label>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:DropDownList ID="IpAttributes" runat="server"></asp:DropDownList>
                    </div></td>
                    <td />
                </tr>
            </table>
        </div>

        <div><!-- fix layout --></div>

        <div class="sw-form-subheader" style="padding-top: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_353 %></div>

        <div><!-- fix layout --></div>
        
        <asp:Repeater ID="ColumnsRepeater" runat="server" >
            <HeaderTemplate>
                <div style="padding-left:20px;">
                    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                        <tr class="sw-form-cols-table">
                            <td class="sw-form-col-label-large"></td>
                            <td class="sw-form-col-control"></td>
                            <td class="sw-form-col-comment"></td>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td><div>
                                <asp:Label ID="columnName" runat="server"></asp:Label>
                                <asp:Label ID="columnEntity" runat="server" Visible="false"></asp:Label>
                            </div></td>
                            <td><div class="sw-form-item">
                                <asp:DropDownList ID="ddlAttributes" runat="server"></asp:DropDownList></div>
                            </div></td>
                            <td />
                        </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>
        </asp:Repeater>
    </div>

    <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
         <orion:LocalizableButton runat="server" ID="Back" OnClientClick="$SW.ConfirmBack(); return false;" LocalizedText="Back" DisplayType="Secondary" />
         <orion:LocalizableButton runat="server" ID="Preview" cssClass="sw-form-dim" OnClientClick="$SW.StartValidation(); return false;" LocalizedText="Next" DisplayType="Primary" />
         <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
   </div>

</div>
</asp:Content>
