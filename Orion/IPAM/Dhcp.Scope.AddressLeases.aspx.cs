using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common.UISetting;
using AjaxControlToolkit;

namespace SolarWinds.IPAM.WebSite
{
    //TODO: IPAM-3272 remove when doing code cleanup for 2020.2 release

    public partial class DhcpScopeAddressLeases : CommonPageServices
    {


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            int scopeId = base.GetParam("ObjectId", -1);
            if (scopeId >= 0)
            {
                this.AddressLeases.Store.WhereClause = string.Format("{{0}}.{0} = '{1}'", DhcpLease.EIM_SCOPEID, scopeId);
            }
            
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PerformPersonalization();
        }

        private void PerformPersonalization()
        {
            SettingsCache cache = SettingsCache.GetInstance();             

            List<MgtColumn> columns = null;

            if (cache.TryParseJsonSetting(UISetting.UIS_DHCPADDRESSLEASECOLUMNS, out columns))
                SubnetMgrHelper.PersonalizeExtGrid(AddressLeases, columns);

             
        }
       
    }
}
