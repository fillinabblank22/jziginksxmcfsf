using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;

public partial class Orion_IPAM_Admin_IPAMSummaryView : OrionView
{
    public string ViewTitle
    {
        get
        {
            string viewTitle = Page.Title;
            
            if ((this.ViewInfo != null) &&
                !string.IsNullOrEmpty(this.ViewInfo.ViewTitle))
            {
                viewTitle = this.ViewInfo.ViewTitle;
            }

            return viewTitle;
        }
    }

	protected override void OnInit(EventArgs e)
	{
		this.resContainer.DataSource = this.ViewInfo;
		this.resContainer.DataBind();

		base.OnInit(e);
	}

	public override string ViewType
	{
		get { return "IPAM Summary"; }
	}
}
