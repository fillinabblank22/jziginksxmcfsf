﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Solarwinds.IPAM.Common.CommonExt;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Common;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class IPv6PrefixEdit : CommonPageServices
{
    #region Constructors

    private PageIPv6PrefixEditor Editor { get; set; }
    public IPv6PrefixEdit()
    {
        this.Editor = new PageIPv6PrefixEditor(this, false, GroupNodeType.PrefixAggregate);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.PrefixId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            PrefixAggregate prefix = this.Editor.RetrievePrefixObject();
            InitControls(prefix);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(PrefixAggregate prefix)
    {
        int parentId = prefix.ParentId;
        List<PrefixAggregate> parents =
            PageIPv6EditorHelper.GetPrefixParentsList(parentId);

        bool hasChilds = this.Editor.PrefixObjectChildsCount(prefix) > 0;

        if (this.txtName != null)
        {
            this.txtName.Text = prefix.FriendlyName;
        }
        if (this.txtDescription != null)
        {
            this.txtDescription.Text = prefix.Comments;
        }

        // init global prefix address
        string fnAddressListener = string.Empty;
        if (this.PrefixAddress != null)
        {
            this.PrefixAddress.ReadOnly = hasChilds;
            this.PrefixAddress.Init(
                prefix.FullPrefix,
                prefix.GroupType,
                prefix.FullSize,
                parents, parentId, prefix.GroupId,
                Editor.ClusterId);
            fnAddressListener = this.PrefixAddress.Listener;
        }

        // init prefix size control
        if (this.PrefixSize != null)
        {
            this.PrefixSize.ReadOnly = hasChilds;
            this.PrefixSize.Init(
                PageIPv6EditorHelper.MinPrefixSize,
                PageIPv6EditorHelper.CountMaxPrefixSize(parents),
                prefix.Size,
                fnAddressListener);
        }

        CustomPropertyRepeater.RetrieveCustomProperties(prefix, new List<int>(){ prefix.GroupId });

        if (this.AccountRolesBox != null)
        {
            this.AccountRolesBox.GroupId = prefix.GroupId;
        }
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        PrefixAggregate prefix = this.Editor.RetrievePrefixObject();
        if (prefix != null)
        {
            PrefixAggregate parentPrefix =
                PageIPv6EditorHelper.GetParentPrefix(prefix.ParentId);

            PrefixAggregate updatedPrefix = UpdateObject(prefix, parentPrefix);
            this.Editor.Store(updatedPrefix);
        }
    }
    private PrefixAggregate UpdateObject(PrefixAggregate localPrefix, PrefixAggregate parentPrefix)
    {
        localPrefix.GroupType = GroupNodeType.PrefixAggregate;

        if (this.txtName != null)
        {
            localPrefix.FriendlyName = this.txtName.Text;
        }
        if (this.txtDescription != null)
        {
            localPrefix.Comments = this.txtDescription.Text;
        }

        // NOTE: For local prefix case, the value and size
        // are specialy calculated.
        if (this.PrefixSize != null)
        {
            int size = this.PrefixSize.PrefixSizeValue;
            localPrefix.FullSize = parentPrefix.FullSize + size;
            localPrefix.Size = size;
        }
        if (this.PrefixAddress != null)
        {
            string prefix = this.PrefixAddress.IPv6Value;
            if (prefix.IndexOf("::") < 0) prefix += "::";
            localPrefix.FullPrefix = prefix;

            AddressPrefix fullPrefix = new AddressPrefix(prefix, localPrefix.FullSize);
            AddressPrefix prefixValue = fullPrefix.ExtractAggregation(localPrefix.Size);
            localPrefix.Value = prefixValue.Address.ToStringIPv6();
        }

        CustomPropertyRepeater.GetChanges(localPrefix);

        return localPrefix;
    }

    #endregion // Methods
}
