using System;
using System.Web.UI;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPEdit : CommonPageServices
    {
        PageIPEditor editor;

        private string IPNodeStatus
        {
            get { return ddlStatus.SelectedValue; }
        }

        public IPEdit()
        {
            editor = new PageIPEditor(this);
            editor.SaveCustomProperties = SaveCustomProperties;
            editor.GetCustomProperties = GetCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties node)
        {
            CustomPropertyRepeater.GetChanges(node);
        }

        public void GetCustomProperties(ICustomProperties node, IList<int> ids)
        {
            CustomPropertyRepeater.RetrieveCustomProperties(node, ids);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            SaveForm();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            SaveForm();
        }
        
        protected void ClickCancel(object sender, EventArgs e)
        {
            editor.RedirectToRefUrl(null);
        }

        private void SaveForm()
        {
            CustomPropertyRepeater.DisableValidatorsByStatus(IPNodeStatus);

            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            editor.Save(true);
        }
    }
}
