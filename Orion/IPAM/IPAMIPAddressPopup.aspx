﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IPAMIPAddressPopup.aspx.cs" Inherits="Orion_IPAM_IPAddressPopup" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/IPAddressLink.ascx" TagName="IPAddressLink" %>

<%--This is intended to be displayed as the body of a cluetip tooltip/popup.--%>

    <%if (GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_IPNODEID) != ""  ){ %>
<h3 class="StatusUp">
    <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_846, GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_IPADDRESS))%>
    <% } else {%>
 <h3 class="StatusUnmanaged">
    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_339 %>
    <%} %>
</h3>


<div class="NetObjectTipBody">
    <%if (GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_IPNODEID) != ""  ){ %>
  <p class="StatusDescription"></p>
  <table cellpadding="0" cellspacing="0">
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_STATUS)%>:
	    </th>
		<td width="100%">
		    <IPAM:IPAddressLink ID="ipAddressLink" ValueTemplate="{2}" runat="server" />
		</td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_ALLOCPOLICY)%>:
	    </th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_ALLOCPOLICY)%>
		</td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_ALIAS)%>:</th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_ALIAS)%>
		</td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_COMMENTS)%>:
	    </th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_COMMENTS)%>
	    </td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_DNSBACKWARD)%>:
	    </th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_DNSBACKWARD)%>
		</td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_DHCPCLIENTNAME)%>:
	    </th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_DHCPCLIENTNAME)%>
		</td>
    </tr>
    <tr>
	    <th style="white-space: nowrap;">
	        <%=GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.IPNode.EIM_MAC)%>:
	    </th>
		<td>
		    <%=GetPropertyTextValue(this.IPNode, SolarWinds.IPAM.BusinessObjects.IPNode.EIM_MAC)%>
		</td>
    </tr>
  </table>
<% } else {%>
<p class="StatusDescription">
    <table cellpadding="0" cellspacing="0">
    <tr>
	    <th style="white-space: nowrap;">	       
	    </th>
		<td width="100%">
		     <%= Resources.IPAMWebContent.IPAMWEBDATA_MR1_38 %></p> 
		</td>
    </tr>  
  </table>
  
 <%} %>

</div>

