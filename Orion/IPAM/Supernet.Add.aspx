<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.SupernetAdd"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_409%>" CodeFile="Supernet.Add.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<style type="text/css">
    .sw-pairednum
    {
        left: 80px !important;
    }
    
    .sw-form-item .x-form-text
    {
        width: 301px;
    }
    
    .sw-form-item .sw-withlblnum
    {
        width: 148px;
    }
    .sw-form-wrapper div.sw-form-invalid-icon {
        left: 333px !important;
        top: 0px !important;
    }
</style>

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation=true OnMsg="MsgSave" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
    .sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation=jsPostInit runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){
    $SW.SubnetEditorInit($nsId);
    $('.expander').expander();
});
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMui:ValidationIcons runat=server />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_410 %></div>

    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_411 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="DisplayNameRequire" runat="server"
                  ControlToValidate="txtDisplayName"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_412 %>">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_413 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtNetworkAddress" CssClass="x-form-text x-form-field sw-withlblnum" runat="server" />
                <span class=sw-pairedlbl><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_414 %></span>
                <asp:TextBox ID="txtCidr" CssClass="x-form-text x-form-field sw-pairednum" Text="16" runat="server"/>
            </div></td>
            <td><IPAMmaster:JsBlock Orientation=inline runat=server>document.getElementById($SW.nsGet($nsId,'txtNetworkAddress')).focus();
            </IPAMmaster:JsBlock><asp:RequiredFieldValidator ID="NetworkAddressRequire" runat="server"
                  ControlToValidate="txtNetworkAddress"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent,IPAMWEBDATA_VB1_415 %>" />
                <asp:CustomValidator ID="NetworkAddressIPv4" runat="server"
                  ControlToValidate="txtNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_416 %>"
              /><asp:CustomValidator
                  ID="NetworkAddressSubnet"
                  runat="server"
                  ControlToValidate="txtNetworkAddress"
                  OtherControl="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnet"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_417 %>"
              /><asp:CustomValidator
                  ID="SubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  OtherControl="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_418 %>"
              /><asp:RequiredFieldValidator ID="CidrRequire" runat="server"
                  ControlToValidate="txtCidr"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_50 %>"
             /><asp:CompareValidator ID="CidrAbove" runat="server"
                  ControlToValidate="txtCidr"
                  Operator="GreaterThan"
                  ValueToCompare="0"
                  Type="Integer"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_51 %>"
              /><asp:CompareValidator ID="CidrBelow" runat="server"
                  ControlToValidate="txtCidr"
                  Operator="LessThanEqual"
                  ValueToCompare="30"
                  Type="Integer"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_419 %>"
              /><asp:CustomValidator ID="CidrIsv4" runat="server"
                  ControlToValidate="txtCidr"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4cidr"
                  OtherControl="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_420 %>"
              /></td>         
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_421 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtNetworkMask" CssClass="x-form-text x-form-field" ReadOnly=true Text="255.255.0.0" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="NetworkMaskRequire" runat="server"
                  ControlToValidate="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_422 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_423 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4Mask" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4mask"
                  OtherValidator="NetworkAddressSubnet"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_424 %>"
              /></td>
        </tr>
        <tr>
            <td></td>
            <td colspan=2><div id="ipcount" class="sw-form-item sw-form-clue"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_425 %></div></td>
        </tr>
        <tr id="parentifo" runat=server> 
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_385%>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
             <asp:TextBox ID="txtParentAddress" Enabled=false CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147%>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan=2><div class="sw-form-item sw-form-clue">
              <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_426 %>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
        <div class="group-box white-bg" style="margin-bottom: 0px">
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
        </div>
    </div>

    <div><!-- ie6 --></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 120px;">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl="/Orion/IPAM/subnets.aspx"
                        LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
