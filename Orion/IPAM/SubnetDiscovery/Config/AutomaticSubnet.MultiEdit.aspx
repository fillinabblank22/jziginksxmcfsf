﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" 
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_291 %>" CodeFile="AutomaticSubnet.MultiEdit.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.AutomaticSubnetMultiEdit" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.IPv6AddressEditor.ascx" TagName="IPv6AddressEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NeighborScanSettings.ascx" TagName="NeighborScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TransientPeriodSettings.ascx" TagName="TransientPeriodSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DnsZoneSelector.ascx" TagName="DnsZoneSelector" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<ipam:accesscheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<script type="text/javascript">
    $(document).ready(function() {
        $SW.SubnetsRange = { from: '<%=this.FromRange%>', into: '<%=this.IntoRange%>' };
        $SW.ShowParentChangeConfirmDlg = <%=this.ShowParentChangeConfirmDlg %>;
    });
</script>
<IPAMmaster:JsBlock ID="JS" Orientation="jsPostInit" runat="server"
    Requires="ext,sw-ux-tree.js,ext-quicktips,sw-expander.js,sw-subnet-edit.js,sw-dialog.js,sw-treecombo.js">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready(function(){
    /* parent selector */
    var chbParent = $SW.ns$($nsId, 'chbParent');
    var hfParent = $SW.ns$($nsId, 'hfParent');
    var SetParentHiddenValue = function(el, node){
        if(el && hfParent){ hfParent.val(el.value); }
    };
    var ClearParentValue = function(el, node){
        if(el && hfParent){ el.clearValue(); hfParent.val(''); }
    };
    var treeLoader = $SW.MultiEdit.GetParentTreeLoader();
    treeLoader.query = $SW.MultiEdit.SubnetsQuery($SW.SubnetsRange);

    var parentTree = new Ext.ux.form.TreeCombo( {
        loader: treeLoader,
        expandTree: false,
        listeners: {
            beforeselect: function(el, node){
                return $SW.MultiEdit.ChangeParentPermission(node.attributes.Role);
            },
            select: $SW.MultiEdit.ConfirmParentChange(SetParentHiddenValue, ClearParentValue)
        }
    });
    parentTree.render('parentSelector');
    $SW.MultiEdit.CheckBoxParentInit(chbParent[0], parentTree);
    
    /* dns zone */
    $SW.chbDnsZone = $SW.ns$($nsId, 'chbDnsZone')[0];
    $SW.MultiEdit.CheckBoxScanningInit($SW.chbDnsZone, $('#divDnsZoneSelector'));

    /* scanning settings */
    $SW.ns$($nsId,'cbDisableAutoScanning').click( function(){ $SW.SubnetSyncScanDisable($nsId); } );
    $SW.SubnetSyncScanDisable($nsId,true);

    $SW.chbAutoScanning = $SW.ns$($nsId, 'chbAutoScanning')[0];
    $SW.chbNeighborScanning = $SW.ns$($nsId, 'chbNeighborScanning')[0];
    $SW.chbTransientPeriod = $SW.ns$($nsId, 'chbTransientPeriod')[0];

    $SW.OnNeighborScanSkipValidation = function(){
        var chb = $SW.chbNeighborScanning; return (chb && !chb.checked);
    };
    $SW.OnTransientPeriodSkipValidation = function(){
        var chb = $SW.chbTransientPeriod; return (chb && !chb.checked);
    };

    $SW.MultiEdit.CheckBoxScanningInit($SW.chbAutoScanning,    $('#divAutoScanning')    );
    $SW.MultiEdit.CheckBoxScanningInit($SW.chbNeighborScanning,$('#divNeighborScanning'));
    $SW.MultiEdit.CheckBoxScanningInit($SW.chbTransientPeriod, $('#divTransientPeriod') );
    
    var opt = {header: $SW.ns$($nsId, 'txtGroupsCountLabel') };
    $('#selectedGroupsList').expander(opt);
    $('.expander').expander();
});
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
.sw-form-disabled { color: #777; }
.sw-form-listheader { font-weight: bold; margin-bottom: 4px; }
#formbox .group-box { margin-bottom: 20px; padding: 8px; 6px; max-width: 876px; }
#formbox .white-bg { background: none repeat scroll 0 0 transparent; }
#formbox .blue-bg { background: none repeat scroll 0 0 #E4F1F8; }
#separator hr { height: 1px; border: none; color: #B5B8C8; background-color: #B5B8C8;}
#selectedGroupsList li { height: 16px; padding-left: 60px; margin-bottom: 2px; background-position: 36px center; background-repeat: no-repeat;}
.sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>

<ipamui:validationicons runat="server" />

    <div id="formbox">
        <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {%>
        <input type="hidden" id="isDemoMode" />
        <%}%>
        <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_292 %></div>

        <asp:validationsummary id="valSummary" runat="server"
            showsummary="true"
            showmessagebox="true"
            displaymode="BulletList" />

        <div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_304 %></div>

        <asp:label id="txtGroupsCountLabel" style="display: none;" runat="server"></asp:label>
        <div id="selectedGroupsList" collapsed="true" style="display: none;">
            <asp:bulletedlist id="GroupsList" runat="server" />
        </div>
        <div id="separator">
            <hr align="center" />
        </div>

        <div class="group-box white-bg">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">
                    <td class="sw-form-col-checkbox"></td>
                    <td class="sw-form-col-label"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                    <td class="sw-form-col-checkbox">
                        <asp:checkbox runat="server" id="chbParent" />
                    </td>
                    <td>
                        <div class="sw-field-label">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_408 %>
                        </div>
                    </td>
                    <td>
                        <div class="sw-form-item" id="parentSelector">
                            <asp:hiddenfield id="hfParent" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="sw-form-col-checkbox">
                        <asp:checkbox runat="server" id="chbComments" />
                    </td>
                    <td>
                        <div class="sw-field-label">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147 %>
                        </div>
                    </td>
                    <td>
                        <div class="sw-form-item">
                            <asp:textbox id="txtComments" cssclass="x-form-text x-form-field" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <!-- ie6 -->
        </div>

        <div>
            <!-- ie6 -->
        </div>

        <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_270 %>" style="display: none;">
            <div class="group-box blue-bg">
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-normal">
                        <td class="sw-form-col-checkbox"></td>
                        <td class="sw-form-col-label"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                    <tr>
                        <td class="sw-form-col-checkbox">
                            <asp:checkbox runat="server" id="chbVLAN" />
                        </td>
                        <td>
                            <div class="sw-field-label">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_185 %>
                            </div>
                        </td>
                        <td>
                            <div class="sw-form-item">
                                <asp:textbox id="txtVLAN" cssclass="x-form-text x-form-field" runat="server" />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="sw-form-col-checkbox">
                            <asp:checkbox runat="server" id="chbLocation" />
                        </td>
                        <td>
                            <div class="sw-field-label">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_186 %>
                            </div>
                        </td>
                        <td>
                            <div class="sw-form-item">
                                <asp:textbox id="txtLocation" cssclass="x-form-text x-form-field" runat="server" />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>

        <div>
            <!-- ie6 -->
        </div>

        <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_294 %>" style="display: none;">

            <div style="padding: 8px;">
                <span>
                    <asp:checkbox runat="server" id="chbAutoScanning" />
                </span>
                <span style="font-weight: bold; padding-left: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_33 %></span>
            </div>

            <div>
                <!-- ie6 -->
            </div>

            <div id="divAutoScanning" style="display: none;">

                <div id="IcmpScanInfo" class="hintbox" style="margin-bottom: 3px;" runat="server">
                    <div class="inner info">
                        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_TM0_70, 
                                    "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>") %>
                    </div>
                </div>

                <div>
                    <!-- ie6 -->
                </div>

                <div class="group-box blue-bg" style="padding-left: 30px;">
                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:checkbox id="cbDisableAutoScanning" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_34 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:checkbox id="cbRetainUserData" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_35 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div id="ScanSettings">
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_36 %>
                                    </div>
                                </td>
                                <td>
                                    <ipam:timespaneditor Width="168" id="ScanInterval"
                                        ValidIf="cbDisableAutoScanning" ActiveWhenChecked="False"
                                        TextName="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_290 %>" runat="server"
                                        MinValue="00:10:00" MaxValue="7.00:00:00" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>

            <div>
                <!-- ie6 -->
            </div>

            <div style="padding: 8px;">
                <span>
                    <asp:checkbox runat="server" id="chbNeighborScanning" />
                </span>
                <span style="font-weight: bold; padding-left: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_295 %></span>
            </div>

            <div>
                <!-- ie6 -->
            </div>

            <div id="divNeighborScanning" style="display: none;">

                <div id="NeighborScanInfo" class="hintbox" style="margin-bottom: 3px;" runat="server">
                    <div class="inner info">
                        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_TM0_74, 
                                    "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>") %>
                    </div>
                </div>

                <div>
                    <!-- ie6 -->
                </div>

                <div class="group-box blue-bg" style="padding-left: 30px;">
                    <ipam:neighborscansettings id="NeighborScan" runat="server"
                        ClientSkipValidation="return $SW.OnNeighborScanSkipValidation();"
                        OnServerSkipValidation="OnNeighborScanSkipValidation"
                        FnSupplyServerAddress="function(fnContinue){ fnContinue('Subnet.MultiEdit: SNMP'); }" />
                </div>

            </div>

            <div>
                <!-- ie6 -->
            </div>

            <div style="padding: 8px;">
                <span>
                    <asp:checkbox runat="server" id="chbTransientPeriod" />
                </span>
                <span style="font-weight: bold; padding-left: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_10 %></span>
            </div>

            <div>
                <!-- ie6 -->
            </div>

            <div id="divTransientPeriod" style="display: none;">

                <div class="group-box blue-bg" style="padding-left: 30px;">
                    <ipam:transientperiodsettings id="TransientPeriod" runat="server"
                        ClientSkipValidation="return $SW.OnTransientPeriodSkipValidation();"
                        OnServerSkipValidation="OnTransientPeriodSkipValidation" />
                </div>

            </div>

            <div>
                <!-- ie6 -->
            </div>
            
            <div class="expander" header="<asp:Literal runat='server' Text='<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_187 %>' />">
                <div class="group-box blue-bg">
                    <ipam:CustomPropertyEdit ID="CustomPropertyEdit" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
                </div>
            </div>
            <div>
                <!-- ie6 -->
            </div>

            <asp:customvalidator id="SomethingSelectedValidator" runat="server"
                errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_322 %>" display="None"
                onservervalidate="OnCheckBoxesServerValidate" />

        </div>

        <div id="ChromeButtonBar" runat="server" class="sw-btn-bar" style="padding-left: 150px">
            <orion:localizablebutton runat="server" id="btnSave" onclick="ClickSave" localizedtext="Save" displaytype="Primary" causesvalidation="true" />
            <orion:localizablebutton runat="server" id="btnCancel" onclick="ClickCancel" localizedtext="Cancel" displaytype="Secondary" causesvalidation="false" />
        </div>

    </div>

</asp:Content>
