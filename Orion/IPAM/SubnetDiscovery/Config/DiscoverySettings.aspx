﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryMasterPage.master" AutoEventWireup="true" CodeFile="DiscoverySettings.aspx.cs" 
    Title="<%$ Resources: CoreWebContent,WEBDATA_AK0_134 %>" Inherits="SolarWinds.IPAM.WebSite.DiscoverySettings" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="orion" TagName="Slider" Src="~/Orion/Controls/Slider.ascx" %>
<%@ Register Src="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryProgressIndicator.ascx" TagPrefix="ipam" TagName="Progress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="discoveryWizardContentHolder" runat="server">
    
        <IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server" />
        <orion:Include runat="server" File="Discovery.css" />
        <IPAMmaster:JsBlock Requires="sw-expander.js,sw-asd-progressbar.js" Orientation="jsInit" runat="server" >
            $(document).ready( function(){
                $('.expander').expander();
                
                <%--//Orion SliderControl Textbox allows character and special characters in slider textbox. So, need to allow only numbers (0-9)--%>
                RegisterSliderBoxKeyEvent('SNMPRetriesSlider_st');
                RegisterSliderBoxKeyEvent('SNMPTimeoutSlider_st');
                RegisterSliderBoxKeyEvent('HopCountSlider_st');
                RegisterSliderBoxKeyEvent('JobTimeoutSlider_st');
            });
            function RegisterSliderBoxKeyEvent(id){
                var tb = $SW.ns$($nsId,id)
                  $(tb).keypress(function (e) {
                     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                      return false;
                    }
                });
            }
        </IPAMmaster:JsBlock>

        <style type="text/css">
          .x-window.x-window-plain.x-window-dlg {
              z-index:20001!important;
            }
          .progressLabel {
                white-space: nowrap !important;
                width: auto !important;
            }
        </style>

        <script type="text/javascript">
            $SW.TaskId = '<%=TaskId %>';
	    	var isCancel = false;
            if (!$SW.IPAM.DiscoveryProgressBar) $SW.IPAM.DiscoveryProgressBar = {};
            //global text variables | Internationalization team
            
            var IntTM = {
                txt1: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_17)%>', //Unable to start discovery.\n\n
                txt2: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_18)%>', //An error has occured.\nSee discovery log for more details.\n
                txt3: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_19)%>', //Network discovery complete. Processing results...
                txt4: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_20)%>', //An error while processing results has occured.\nSee discovery log for more details.\n
                txt5: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_21)%>', //An error has occured.\nSee discovery log for more details.
                txt6: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_22)%>', //Are you sure you want to cancel this discovery?
                txt7: '<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEBDATA_AK0_23)%>', //An error while cancelling discovery has occured.\nSee discovery log for more details.\n
                txt8: '<%= ControlHelper.EncodeJsString(Resources.IPAMWebContent.IPAMWEBDATA_SE_42)%>' //Discovering Subnets...
            };

            var baseUrl = '/Orion/IPAM/AddSubnetIPAddresses.aspx';
            var nextStepUrl = '<%= this.ProgressIndicator1.NextStep %>';

            var CreateBarImage = function () {
                return $("<img />").attr("src", "/Orion/Discovery/images/progBar_on.gif").height(20);
            };
            
            var SubnetDiscoveryErrMsgTpl = new Ext.XTemplate(
                '<tpl if="!this.hasErrs(results)">{message:this.toHtml}</tpl>',
                '<tpl if="this.hasErrs(results)">',
                        '<tpl for="results">',
                            '<br>■ &ensp; {.:this.translate}',
                        '</tpl>',
                '</tpl>',
                {
                    toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
                    hasErrs: function (rs) { return (rs && rs.length > 0); },
                    translate: function (err) {
                        return err.ErrorMessage;
                    }
            });

            var SetProgressBar = function (id, value, maxValue) {
                if (value > maxValue) value = maxValue;
                if (value == 0) {
                    $("#progressBar" + id).empty();
                }
                else {
                    if ($("#progressBar" + id + " img").length == 0) {
                        $("#progressBar" + id).append(CreateBarImage());
                    }
                    $("#progressBar" + id + " img").width((300 / maxValue) * value);
                }
            };

            var that = {
                SuccessRedirect: function () {
                    $("#progressDialog").dialog("close");
                    <%= this.ClientScript.GetPostBackEventReference(RedirectLinkButton, "redirect") %>;
                },
                ShowPollingStatus: function (result) {
	            isCancel = false;
                    //Update Progess Status
                    that.UpdateProgress(result);

                    //if (result.success) {
                    //setTimeout("that.SuccessRedirect()", 500);
                    that.SuccessRedirect();
                    //return;
                    //}

                    //Ext.MessageBox.show({
                    //    title: IntTM.txt8,
                    //    msg: SubnetDiscoveryErrMsgTpl.apply(result),
                    //    buttons: Ext.MessageBox.OK,
                    //    icon: Ext.MessageBox.ERROR,
                    //    fn: function() {
                    //        that.CloseProgress();
                    //    }
                    //});
                },
                CloseProgress: function () {
                    $("#progressDialog").dialog("close");
                    window.location = baseUrl;
                },
                StartFailed: function () {
                    Ext.MessageBox.show({
                        title: IntTM.txt8,
                        msg: IntTM.txt1,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        fn: function () {
                            that.CloseProgress();
                        }
                    });
                },
                StartDiscovery: function () {
                    this.ShowProgressDialog();
                    $("#statusText").text("<%= ControlHelper.EncodeJsString(Resources.CoreWebContent.WEB_JS_CODE_TM0_9) %>");
                },
                ShowProgressDialog: function () {
                    var encodedTitle = $("<div/>").text(IntTM.txt8).html();
                    $("#progressDialog").show().dialog({
                        width: '550px', height: 'auto', modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: encodedTitle
                    });
                    $(".ui-dialog-titlebar-close").unbind('click');
                    $(".ui-dialog-titlebar-close").bind('click', function () { $SW.IPAM.DiscoveryProgressBar.CancelDiscoveryJob(); });
                },
                CancelDiscovery: function () {
		  			if (isCancel == true) return false;
                    if (confirm(IntTM.txt6)) {
                        $("#statusText").text("<%= ControlHelper.EncodeJsString(Resources.IPAMWebContent.IPAMWEBDATA_SE_43) %>");
						isCancel = true;
                        return true;
                    }
                    return false;
                },
                FailedToReciveResponse: function (error) {
                    $("#progressDialog").dialog("close");
                    alert(IntTM.txt2);
                    window.location = baseUrl;
                },
                UpdateProgress: function (result) {

                    if (!result || result == 'undefined')
                        return;
                    
                    SetProgressBar(1, result.processedPrimaryCount, result.totalPrimayCount);
                    SetProgressBar(2, result.processedElementCount, result.totalElementCount);

		 		   if (isCancel == false) {
                        $("#statusText").text(result.message);
                    }
                    else {
                        if (result.processedPrimaryCount == 5)
                            $("#statusText").text("<%= ControlHelper.EncodeJsString(Resources.IPAMWebContent.IPAMWEBDATA_SE_44) %>");

                        return;
                    }

                    
                    if (result.state == 0) { // Not Started
                        //Do Nothing
                    }
                    else if (result.state == 2) { // In progress
                        $("#subnets").text(result.subnetsCount);
                        $("#routers").text(result.routerCount);
                    }
                    else if (result.state == 3) { // finished
                        $("#subnets").text(result.subnetsCount);
                        $("#routers").text(result.routerCount);
                    }
                    else if (result.state == 4) { // Cancelled
                        $("#statusText").text(result.message);
                    }
                    else if (result.state == 5) { // Error
                        //Do Nothing
                    }
                    else if (result.state == 6) { //Process job result
                        $("#subnets").text(result.subnetsCount);
                        $("#routers").text(result.routerCount);
                    }
                    else {
                        alert(IntTM.txt5);
                        window.location = baseUrl;
                    }
                }
            };

            $SW.IPAM.DiscoveryProgressBar.fnHelper = that;
            
            function CheckDemoMode() {
                if ($('#isDemoMode').length > 0) {
                    demoAction('IPAM_Automatic_Subnet_Discovery_ButtonClick', null);
                    return false;
                }
                return true;
            }
            
            $(document).ready(function () {
	  			if ($('#isDemoMode').length <= 0) {
                if ($SW.TaskId && $SW.TaskId != '') {
                    $SW.IPAM.DiscoveryProgressBar.Attach($SW.TaskId);
                }
	     }
            });

        </script>
    
        <asp:LinkButton ID="RedirectLinkButton" runat="server" Visible="false" OnClick="RedirectLinkButton_Click"></asp:LinkButton>

        <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		    <div id="isDemoMode" style="display:none;"></div>
	    <%}%>
    <table>
         <tr>      
         <td><h1 class="sw-hdr-title"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_33 %></h1><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_35 %> </td>
        </tr><tr><td><br />
        <div style="width: 900px;">
            
            <asp:HiddenField ID="hdWebId" runat="server" />

            <ipam:Progress ID="ProgressIndicator1" runat="server" />
	        <div class="GroupBox" id="NormalText">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <div class="ActionName"><%= Resources.CoreWebContent.WEBDATA_AK0_134 %></div>
                            <div class="TextWithSpaceAfter"><%= Resources.IPAMWebContent.IPAMWEBDATA_VR_2 %></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div>  <b>  <%= Resources.IPAMWebContent.IPAMWEBDATA_SE_39 %></b>
                                <div class="group-box blue-bg"  style="width: 80%">
                                    <asp:Panel ID="ConfigurationControls" runat="server">
                                        <table class="ConfigurationControls" style="border-collapse: separate; border-spacing: 5px;width:80% ">
                                            <tr>
                                                <orion:Slider runat="server" ID="HopCountSlider" TableCellCount="3" Label="<%$ Resources: CoreWebContent,WEBDATA_AK0_126 %>"
                                                    RangeFrom="0" RangeTo="15" ValidateRange="true" Unit="<%$ Resources: CoreWebContent, DiscoverySettings_Hop_Unit %>" ErrorMessage="<%$ Resources: IPAMWebContent,IPAMWEBDATA_VN0_38 %>" />
                                            </tr>
                                            <tr>
                                                <orion:Slider runat="server" ID="SNMPTimeoutSlider" TableCellCount="3" Label="<%$ Resources: CoreWebContent,WEBDATA_AK0_132 %>"
                                                    RangeFrom="100" RangeTo="5000" Unit="<%$ Resources: CoreWebContent, WEBDATA_VB0_151 %>" ValidateRange="true" Step="10" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_131 %>" />
                                            </tr>
                                            <tr>
                                                <orion:Slider runat="server" ID="SNMPRetriesSlider" TableCellCount="3" Label="<%$ Resources: CoreWebContent,WEBDATA_AK0_128 %>"
                                                    RangeFrom="0" RangeTo="10" ValidateRange="true" Unit="<%$ Resources: CoreWebContent, DiscoverySettings_Retry_Unit %>" ErrorMessage="<%$ Resources: CoreWebContent,WEBDATA_AK0_127 %>" />
                                            </tr>
                                            <tr>
                                                <orion:Slider runat="server" ID="JobTimeoutSlider" TableCellCount="3" Label="<%$ Resources: CoreWebContent,WEBDATA_AK0_124 %>" Unit="<%$ Resources: CoreWebContent, WEBDATA_VB0_152 %>"
                                                    RangeFrom="10" ValidateRange="true" />
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="PollingLabel" runat="server" Text="<%$ Resources: CoreWebContent,WEBDATA_IB0_26 %>" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="EnginesList" runat="server" Width="200" />
                                                    <br/>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:panel>
                                </div>
                            </div>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <table class="NavigationButtons"><tr><td>
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="ImgbBack_Click" CausesValidation="false"/>
                                    <orion:LocalizableButton id="imgbDiscover" runat="server" DisplayType="Primary" LocalizedText="CustomText" OnClientClick="return CheckDemoMode()" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SV1_2 %>" OnClick="ImgbDiscover_Click"></orion:LocalizableButton>
                                    <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="ImgbCancel_Click" CausesValidation="false"/>
                                </div>
                            </td></tr></table>
                        </td>
                    </tr>
            </table>
            </div>
        </div>
    </td></tr>
    </table>
        <div id="progressDialog">
            <div id="progressContent">
                <div id="statusText"><%= Resources.CoreWebContent.WEBDATA_AK0_32 %></div>
                <table id="progressTable" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="progressLabel"><%= Resources.CoreWebContent.WEBDATA_TM0_244 %></td>
                        <td colspan="2"><div id="progressBar1" class="progressBar" /></td>
                    </tr>
                    <tr><td colspan="3" class="progressSeparator">&nbsp;</td></tr>
                    <tr>
                        <td class="progressLabel"><%= Resources.CoreWebContent.WEBDATA_TM0_245 %></td>
                        <td colspan="2"><div id="progressBar2" class="progressBar" /></td>
                    </tr>
                    <tr><td colspan="3" class="progressSeparator">&nbsp;</td></tr>
                    <tr><td colspan="3" class="progressSeparator">&nbsp;</td></tr>
                    <tr>
                        <td class="progressLabel"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_40 %></td>
                        <td class="progressData progressAltRow" id="routers">0</td>
                        <td style="width: 149px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="progressLabel"><%= Resources.CoreWebContent.WEBDATA_AK0_40 %></td>
                        <td class="progressData" id="subnets">0</td>
                        <td style="width: 149px;">&nbsp;</td>
                    </tr>
                </table>
                <div style="text-align: right">
                    <orion:LocalizableButton ID="DiscoveryCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" CausesValidation="false" OnClientClick="if ($(this)[0].href != undefined) $(this)[0].href='#';  $SW.IPAM.DiscoveryProgressBar.CancelDiscoveryJob(); return false;" />
                </div>
            </div>
        </div>
    </asp:Content>
