﻿using System;
using System.Collections;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DiscoverySubnetEdit : CommonPageServices
    {
        private const string QUERY_SUBNETID_PARAM = "ObjectID";
        private const string QUERY_PARENTID_PARAM = "ParentID";

        private PageAutomaticSubnetEditor editor;

        public string discoveredSubnetId
        {
            get
            {
                int subnetId = -1;
                string objectid = Page.Request.QueryString[QUERY_SUBNETID_PARAM];
                int.TryParse(objectid, out subnetId);
                return subnetId.ToString();
            }
        }

        public DiscoverySubnetEdit()
        {
            editor = new PageAutomaticSubnetEditor(this, false, GroupNodeType.Subnet);
            editor.SaveCustomProperties = SaveCustomProperties;
            editor.GetCustomProperties = GetCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties group)
        {
            CustomPropertyRepeater.GetChanges(group);
        }

        public void GetCustomProperties(ICustomProperties group, IList<int> ids)
        {
            CustomPropertyRepeater.RetrieveCustomProperties(group, ids);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }
    }
}
