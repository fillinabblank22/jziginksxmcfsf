﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryMasterPage.master" AutoEventWireup="true" CodeFile="SubnetResult.aspx.cs"
     Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_24%>"  Inherits="SolarWinds.IPAM.WebSite.SubnetResult" %>
<%@ Import Namespace="SolarWinds.IPAM.Strings" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="discoveryWizardContentHolder" runat="server">
        <IPAMmaster:JsBlock ID="JsBlock3" Requires="ext,ext-quicktips,sw-helpserver,sw-ux-wait.js,sw-ux-ext.js,ext-ux,sw-subnet-manage.js" Orientation="jsInit" runat="server" />
<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-helpserver,sw-ux-ext.js,sw-ux-wait.js" Orientation="jsInit" runat=server />
      
    <style type="text/css">
    .import{
		padding:8px;
		border:1px solid blue;
	}
    span.SearchText { background: #FDE9A0;}
</style>
     <IPAMmaster:JsBlock ID="JsBlock2" Requires="ext, sw-dns-manage.js" Orientation="jsPostInit" runat="server">
        $(document).ready(function(){ 
         
        $SW.discoversubnet.DoDiscoverySubnetImport = function(data) {

        $SW.Tasks.DoProgress('ImportSubnets', data, {
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_52;E=js}',
                buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}' }
            },
          function(d) {
                if (d.success)
                    $SW.discoversubnet.RedirectManageSubnet();
                else {
                    if (d.state == 4) {
                        $SW.discoversubnet.RedirectManageSubnet();
                    }
                    else {
                        Ext.MessageBox.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_52;E=js}',
                            msg: (d.message && d.message.length > 0) ? Ext.util.Format.htmlEncode(d.message).replace(/\[br\]/g, "<br />") : '@{R=IPAM.Strings;K=IPAMWEBJS_SE_53;E=js}',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () { $SW.discoversubnet.RedirectManageSubnet(); }
                        });
                    }
                }
            },
            function () {
                $SW.discoversubnet.RedirectManageSubnet();
            }
        );
    };

   $SW.discoversubnet.UpdateSkipImportManyById = function (el, value, webId,successFn) {
        var grid = Ext.getCmp('grid');
        var sel = grid.getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            var message = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VN0_28;E=js}', (value == false) ? '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_29;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_30;E=js}');
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: message,
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        }
        var items = sel.getSelections();
        var discoveredSubnetIds = [];
        
        Ext.each(items, function(o) {
            if (!o.id) return true;
            discoveredSubnetIds.push(o.id);
        });
        
        var fail = $SW.ext.AjaxMakeFailDelegate('Delete IP addresses');
        

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText);
                if (res.success == true && res.license && res.license.app == "IPAM") {
                    $SW.LicenseListeners.recv(res.license);
                    var gridStore = grid.getStore();
                    gridStore.reload();
                    $SW.ClearGridSelection(grid);
                    $SW.discoversubnet.GetSkipImportCount(webId);
                }
            },
            failure: fail,
            timeout: fail,

            params:
       {
           entity: 'IPAM.DiscoveredSubnets',
           verb: 'UpdateSkipImportManyById',
           ids: discoveredSubnetIds,
           Value: value
       }
        });
    };

   $SW.discoversubnet.RedirectManageSubnet = function() {
        window.location.href = "/Orion/IPAM/Subnets.aspx";
    };
         $SW.discoversubnet.ClearGridSelection = function(grid) {
        try {
            if (grid != null) {
                var sm = grid.getSelectionModel();
                if (sm.grid) sm.clearSelections();
                Ext.get(grid.getView().getHeaderCell(0)).select("div.x-grid3-hd-inner").removeClass('x-grid3-hd-checker-on');
            }
        } catch (e) { }
    };
    

 $SW.discoversubnet.GetSkipImportCount = function (webId) {
       
        var fail = $SW.ext.AjaxMakeFailDelegate('Get Skip Count');      

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText);
                if (res.success == true && res.license && res.license.app == "IPAM") {
                    $SW.LicenseListeners.recv(res.license);
                    $('#importCount').html(String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VN0_27;E=js}', res.data["importCount"], res.data["totalCount"]));
                    $('#importCountFooter').html(String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VN0_27;E=js}', res.data["importCount"], res.data["totalCount"]));
                    if (res.data["importCount"] <= 0) {
                        ImportDisabled($nsId, false);
                    }
                    else
                        ImportDisabled($nsId, true);

                }
            },
            failure: fail,
            timeout: fail,

            params:
       {
           entity: 'IPAM.DiscoveredSubnets',
           verb: 'GetSkipImportCount',
           Value: webId
       }
        });
    };

       function ImportDisabled(nsId, isEnable) {
        var rowCount = $SW.nsGet(nsId, 'ImportButton');

        if (isEnable)
            $('#' + rowCount).show();
        else
            $('#' + rowCount).hide();

    };
  
         var startSearch = function () {
            var webParam = getWebParam();
            var searchColoumns =getGridColoumns();
            var grid = Ext.getCmp('grid');
            var gridStore = grid.getStore();
            var searchString =  $.trim($('#searchBox').val());
            gridStore.baseParams["whereClause"] = "";
            if (searchString && searchString.length > 0) 
            {
                searchColoumns = searchColoumns.replace(/%searchString%/g, "'%"+searchString+"%'");
                gridStore.baseParams["whereClause"] = "WebID = '" + webParam + "' AND ("+searchColoumns+") ";
            }
            else 
            {
                gridStore.baseParams["whereClause"] = "WebID = '" + webParam + "'";
            }
            gridStore.load();
        };
        
         Ext.onReady(function () {
            var gridRowCount = Ext.getCmp('grid').getStore().getTotalCount();
            var rowCount = $SW.nsGet($nsId, 'ImportButton');
            
            if(gridRowCount <= 0)
                $('#'+rowCount).hide();
            else 
                $('#'+rowCount).show();
         
            $("#searchBox").keypress(function (e) {
                if (e.which == 13)
                    startSearch();
            });
            $(".searchButton").click(startSearch);
	         $SW.subnet.AssignNsId($nsId);
            $SW.discoversubnet.GetSkipImportCount(getWebParam(), false);
        });
          });
        if (!$SW.discoversubnet) $SW.discoversubnet = {};
        $SW.discoversubnet.ImportImgRenderer = function (that) {
            return function (value, meta, r) {
                if (value == 'True') {
                    meta.style += "background-image:url(../../../../Orion/IPAM/res/images/sw/Skipped.png  ) !important; background-repeat:no-repeat;background-position:center;";
                } else {
                    meta.style += "background-image:url(../../../../Orion/IPAM/res/images/sw/ok_enabled.png) !important; background-repeat:no-repeat;background-position:center;";
                }
                return "&emsp;";
            };
        };
        $SW.discoversubnet.ObjectsCreatedRenderer = function (that) {
            return function (value, meta, r) {
                var displayText = "";
                if (r.data["SkipImport"] != "True") {
                    if (value == "True") {
                        if (parseInt(r.data["CIDR"]) < 20) {
                            meta.attr = 'ext:qtip="' + "@{R=IPAM.Strings;K=IPAMWEBJS_VN0_26;E=js}" + '"';
                            meta.style += "background-image:url(../../../../Orion/IPAM/res/images/sw/icon.warning.gif ) !important; background-repeat:no-repeat;";
                            displayText = "&emsp;";
                        }
                        displayText = displayText + '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_31;E=js}';
                    } else {
                        displayText = '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_32;E=js}';
                    }
                }
                return displayText;
            };
        };
        $SW.discoversubnet.OverwriteStatusRenderer = function (that) {
            return function (value, meta, r) {
              if(value) {  
                    return (value == "True") ? "Yes" : "No";
                }
             else
                {
                    return "N/A";
                }
            };
        };
     </IPAMmaster:JsBlock>

    <script type ="text/javascript" >
       
        $SW.search = {};

        $SW.search.RegExWildcard = function (str) {
            var specials = new RegExp("[.*+?|()\\[\\]{}\\\\]", "g"); // .*+?|()[]{}\
            // Escape the pattern to make it regex-safe. Wildcards use only '*' and '?'.
            // Then convert '\*' and '\?' to their respective regex equivalents, '.*' and '..'
            return str.replace(specials, "\\$&").replace("\\*", ".*").replace("\\?", ".");
        };
        
        $SW.search.Renderer = function (that) {
            return function (value, meta, r) {
                
                var searchString = $.trim($('#searchBox').val());
                if (searchString) {
                    if (typeof value == "string") {
                        if (value.length > 0) {
                            var q = $SW.search.RegExWildcard(searchString);
                            return value.replace(new RegExp(q, "ig"), '<span class="SearchText">$&</span>');
                        } else {
                            return "&#160;";
                        }
                    }
                }
                return value;
            };
        };
        function CheckDemoMode() {
            if ($('#isDemoMode').length > 0) {
              demoAction('IPAM_Automatic_Subnet_Discovery_ResultPage', null);
                return true;
            }
            return false;
        }

        function DoProgress() {
            //Check current mode demo or not
            if ($('#isDemoMode').length > 0) {
                demoAction('IPAM_Automatic_Subnet_Discovery_ResultPage', null);
                window.location.href = "/Orion/IPAM/Subnets.aspx";
                return false;
            }
            else {
                var data = { WebId: '<%=WebParamId %>', ClusterId: '<%=ClusterId%>', EngineId: '<%=EngineId%>' };
                $SW.discoversubnet.DoDiscoverySubnetImport(data);
                return false;
            }
        };

        function getWebParam() {
            return '<%=WebParamId %>';
        };

        function getGridColoumns() {
            return '<%=GridCloumns %>';
        }
        var typemap = {
            1: 'Group', 2: 'Group', 4: 'Supernet', 8: 'Edit Subnet Properties',
            128: 'IPv6 Global Prefix', 256: 'IPv6 Site', 512: 'IPv6 Subnet'
        };
        var MASK_SUBNET = 8;
        function ShowSubnetDialog(src, id, header, hlpSuffix, isAddMode) {
            $SW.subnet.edit('BtnEditCurrent', true);
        };
        function RefreshGrid() {
            var store = Ext.getCmp('grid').getStore();
            store.baseParams.webID = '<%=WebParamId %>';
            store.reload();
        };
    </script>
     <IPAMlayout:DialogWindow ID="DialogWindow2" jsID="multiEditWindow"  Name="multiEditWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_217 %>" height="600" width="550" runat="server">
        <Buttons>
            <IPAMui:ToolStripButton id="ToolStripButton3" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
                <handler>function(){ 
                    if (CheckDemoMode()) { return false; } 
                    $SW.msgq.DOWNSTREAM( $SW['multiEditWindow'], 'dialog', 'save' ); }</handler>
            </IPAMui:ToolStripButton>
            <IPAMui:ToolStripButton ID="ToolStripButton4" text="<%$ Resources :IPAMWebContent, IPAMWEBDATA_VB1_142  %>" runat="server">
                <handler>function(){ $SW['multiEditWindow'].hide(); }</handler>
            </IPAMui:ToolStripButton>
        </Buttons>
        <Msgs>
            <IPAMmaster:WindowMsg ID="WindowMsg4" Name="ready" runat=server>
                <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['multiEditWindow'] ); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg ID="WindowMsg5" Name="unload" runat=server>
                <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['multiEditWindow'], true ); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg ID="WindowMsg6" Name="close" runat=server>
                <handler>function(n,o,info){ $SW['multiEditWindow'].hide(); $SW.subnet.EditManyComplete(info); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg ID="WindowMsg7" Name="license" runat=server>
                <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg ID="WindowMsg8" Name="refresh" runat=server>
                <handler>function(n,o,info){ window.location = 'subnets.aspx?opento=' + info.id; }</handler>
            </IPAMmaster:WindowMsg>
        </Msgs>
        </IPAMlayout:DialogWindow>
     <IPAMlayout:DialogWindow ID="DialogWindow1" jsID="mainWindow" Name="mainWindow" helpID="edithelp"
		    Url="~/Orion/IPAM/res/html/loading.htm" title="Edit Subnet" height="450" width="520" runat="server">
		    <Buttons>
			    <IPAMui:ToolStripButton ID="ToolStripButton1" text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
				    <handler>function(){ if (CheckDemoMode()) { return false; } 
							 $SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
			    </IPAMui:ToolStripButton>
			    <IPAMui:ToolStripButton ID="ToolStripButton2" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
				    <handler>function(){ $SW['mainWindow'].hide(); }</handler>
			    </IPAMui:ToolStripButton>
		    </Buttons>
		    <Msgs>
			    <IPAMmaster:WindowMsg ID="WindowMsg1" Name="ready" runat="server">
				    <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
			    </IPAMmaster:WindowMsg>
			    <IPAMmaster:WindowMsg ID="WindowMsg2" Name="unload" runat="server">
				    <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'], true ); }</handler>
			    </IPAMmaster:WindowMsg>
			    <IPAMmaster:WindowMsg ID="WindowMsg3" Name="close" runat="server">
				    <handler>function(n,o){ $SW['mainWindow'].hide();RefreshGrid(); }</handler>
			    </IPAMmaster:WindowMsg>
		    </Msgs>
	    </IPAMlayout:DialogWindow>
    
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
     <table>
         <tr>      
         <td><h1 class="sw-hdr-title"> Subnet Discovery Results</h1>  </td>
        </tr><tr><td><br />
    <div style="width: 980px;">

        <asp:HiddenField ID="hdWebId" runat="server" />
         

        <div class="GroupBox" id="NormalText">
            <table style="width: 100%">
                <tr>
                        <td colspan ="2">
                            <span class="ActionName"><%= IPAMWebContent.IPAMWEBDATA_VN0_25 %></span>
                            <br />
                            <div style="width: 100%;">
                                <%= IPAMWebContent.IPAMWEBDATA_VN0_26 %><br />
                            </div>
                            <br />
                        </td>
                    </tr>
                <tr>
                    <td>
                    <div class="sw-suggestion sw-suggestion-noicon" id ="importCount"></div>
                        </td>
                    <td align ="right">
                        <input type="text" size="16" autocomplete="off" class="x-form-text x-form-field searchBoxCls x-form-empty-field" style="width: 160px;" id="searchBox">
                        <orion:LocalizableButtonLink runat="server" CssClass="searchButton" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBCODE_VB0_131 %>" DisplayType="Secondary" Style="vertical-align: baseline;"/>
                    </td>
                </tr>
               
                <tr>
                    <td colspan ="2">
                        <IPAMlayout:GridBox runat="server"
                            width="950"
                            maxBodyHeight="500"
                            minBodyHeight="70"
                            autoHeight="false"
                            autoFill="false"
                            autoScroll="false"
                            ID="grid"
                            emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_34 %>"
                            borderVisible="true"
                            RowSelection="false"
                            clientEl="gridframe"
                            deferEmptyText="false"
                            StripeRows="true"
                            UseLoadMask="True"
                            >
                            <TopMenu borderVisible="false">
                                 <IPAMui:ToolStripMenuItem id="BtnEditCurrent" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-edit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_28 %>" runat="server"  disabled="false">
                                    <handler>function(that){ ShowSubnetDialog(that,-1, '@{R=IPAM.Strings;K=IPAMWEBJS_SE_49;E=js}', 'OrionIPAMPHViewAddSNMPCredentials', 'true'); }</handler>
                                </IPAMui:ToolStripMenuItem> 
                                <IPAMui:ToolStripSeparator runat="server" />
                                <IPAMui:ToolStripMenuItem id="ToolStripMenuItem2" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-doImportSubnets" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_35 %>" runat="server" disabled="false">
                                    <handler>function(el){if (CheckDemoMode()) { return false; }
							 return $SW.discoversubnet.UpdateSkipImportManyById(el,false,getWebParam(),false); }</handler>
                                </IPAMui:ToolStripMenuItem> 
                                <IPAMui:ToolStripSeparator runat="server" />
                                <IPAMui:ToolStripMenuItem id="ToolStripMenuItem3" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-doNotImportSubnets" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_36 %>" runat="server" disabled="false">
                                    <handler>function(el){ if (CheckDemoMode()) { return false; }
							 return $SW.discoversubnet.UpdateSkipImportManyById(el,true,getWebParam(),false); }</handler>
                                </IPAMui:ToolStripMenuItem> 
                            </TopMenu>
                            <PageBar pageSize="50" displayInfo="true" />
                            <Columns>
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_30 %>" width="120" dataIndex="SubnetAddress" isSortable="true" isResizable ="false"  runat="server" renderer="$SW.search.Renderer()"/>
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_31 %>"  width="50" dataIndex="CIDR" isSortable="true" isResizable ="false" runat="server" renderer="$SW.search.Renderer()"/>
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_32 %>"  width="115" dataIndex="Mask" isSortable="true" isResizable ="false" runat="server" renderer="$SW.search.Renderer()"/>
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DF1_43 %>"  width="150" dataIndex="RouterAddress" isSortable="false" runat="server" renderer="$SW.search.Renderer()"/>
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_35 %>"  width="50" dataIndex="SkipImport" isSortable="false" isResizable ="false" runat="server"   renderer="$SW.discoversubnet.ImportImgRenderer()" />
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_33 %>"  width="200" dataIndex="SkipIPImport" isSortable="true" isResizable ="false" runat="server"  renderer="$SW.discoversubnet.ObjectsCreatedRenderer()" />
                                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_40 %>"  width="210" dataIndex="OverwriteExistingSubnet" isSortable="true" isResizable ="false" runat="server" renderer="$SW.discoversubnet.OverwriteStatusRenderer()"/>
                            </Columns>
                            <Store ID="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../../extdataprovider.ashx" proxyEntity="IPAM.DiscoveredSubnets" AmbientSort="SubnetAddressN" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                                <Fields>
                                    <IPAMlayout:GridStoreField dataIndex="DiscoveredSubnetID" isKey="true" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="SubnetName" runat="server" />
									<IPAMlayout:GridStoreField dataIndex="WebID" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="SubnetAddress" runat="server" />
									<IPAMlayout:GridStoreField dataIndex="SubnetAddressN" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="CIDR" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="RouterAddress" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="Mask" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="SkipImport" runat="server" />
                                    <IPAMlayout:GridStoreField dataIndex="SkipIPImport" runat="server" />
								    <IPAMlayout:GridStoreField dataIndex="OverwriteExistingSubnet" runat="server" />
                                </Fields>
                            </Store>
                        </IPAMlayout:GridBox>
                        <ipam:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />

                        <div id="gridframe" style="margin: 1px 0px 0px 0px;">
                            <!-- -->
                        </div>
   
                    </td>
                </tr>
                <tr>
                    <td colspan ="2">
                        <br />
                        <table class="NavigationButtons">
                            <tr>
                                <td>
                                    <div class="sw-suggestion sw-suggestion-noicon" id ="importCountFooter"></div>
                                    <div class="sw-btn-bar-wizard">
                                         <orion:LocalizableButton id="ImportButton" class='enableOnInput' runat="server" DisplayType="Primary" LocalizedText="Import" OnClientClick ="DoProgress(); return false;" />
                                        <orion:LocalizableButton ID="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="CustomText"  Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SV1_13 %>"  OnClick="ImgbCancel_Click" CausesValidation="false" />

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        </div>
             </td></tr>
    </table>
</asp:Content>

