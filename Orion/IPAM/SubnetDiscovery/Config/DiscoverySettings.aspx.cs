﻿using System;
using System.Linq;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.IPAM.BusinessObjects;
using System.Collections.Generic;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DiscoverySettings : CommonPageServices
    {
        private const string PARAM_TASKID = "taskid";
        private const string DEMO_WEBID = "4B312B43-352A-4564-9374-FC71706AA192";
        #region Declarations

        private PageDiscoveryWizardEditor Editor { get; set; }

        private IEnumerable<IpamEngine> Engines { get; set; }

        public string WebParamId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_WEBID, null);
            }
        }

        public string ClusterId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_CLUSTERID, null);
            }
        }

        public string TaskId
        {
            get
            {
                if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                    return DEMO_WEBID;
                else
                    return base.GetParam(PARAM_TASKID, string.Empty);
            }
        }

        public string EngineId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_ENGINEID, null);
            }
        }

        #endregion // Declarations

        #region Constructors

        public DiscoverySettings()
        {
            this.Editor = new PageDiscoveryWizardEditor(this);
        }

        #endregion // Constructors

        #region Page Event Handler
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            int jobTimeout = WebSettingsDAL.DiscoveryMaximumTimeout;
            this.JobTimeoutSlider.RangeTo = jobTimeout;
            this.JobTimeoutSlider.ErrorMessage = String.Format(Resources.CoreWebContent.WEBCODE_AK0_35, jobTimeout);

            if (!IsPostBack)
                Editor.AttachCancelButtonEvent(this.imgbCancel);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ProgressIndicator1.CurrentStep = IPAMDiscoveryStep.DiscoverySettings;

            if (!IsPostBack)
            {
                InitControls();
            }
        }

        private bool InitControls()
        {
            Engines = this.Editor.Engines;
            this.hdWebId.Value = this.WebParamId;
            Editor.ActivateProgressStep(null, PageDiscoveryWizardEditor.IPAM_DISCOVERY_DISCOVER_SETTINGS);
            int retryCount;
            TimeSpan timeOut;
            TimeSpan discoveryTimeOut;
            int hopCount;
            Editor.GetDiscoverySettings(this.WebParamId, out retryCount, out timeOut, out hopCount, out discoveryTimeOut, this.ClusterId);

            SNMPTimeoutSlider.Value = (int)timeOut.TotalMilliseconds;
            SNMPRetriesSlider.Value = retryCount;
            HopCountSlider.Value = hopCount;
            JobTimeoutSlider.Value = (int)discoveryTimeOut.TotalMinutes;

            foreach (var item in Engines)
            {
                item.ServerName = $"{item.ServerName} ({item.ServerType})";
            }
            EnginesList.DataSource = Engines;
            EnginesList.DataTextField = "ServerName";
            EnginesList.DataValueField = "EngineId";
            EnginesList.DataBind();

            EnginesList.SelectedValue = this.EngineId;

            return true;
        }

        #endregion

        # region ButtonClick Handler

        protected void RedirectLinkButton_Click(object sender, EventArgs e)
        {
            ResultPageRedirection(this.TaskId, this.ClusterId, this.EnginesList.SelectedValue);
        }

        /// <summary> </summary>
        protected void ImgbDiscover_Click(object sender, EventArgs e)
        {
            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                if (!Save(true))
                {
                    return;
                }
            }
        }

        protected void ImgbBack_Click(object sender, EventArgs e)
        {
            if (!Save(false))
                return;

            string url = Editor.AddParamsToUrl(ProgressIndicator1.PrevStep, null, this.ClusterId);
            Response.Redirect(url, true);
        }

        /// <summary> </summary>
        protected void ImgbCancel_Click(object sender, EventArgs e)
        {
            Editor.CancelWizard(null, this.ClusterId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion

        #region Wizzard

        private bool ValidateUserInput()
        {
            this.Validate();

            return this.IsValid;
        }

        private bool Save(bool startDiscovery)
        {
            bool status = false;
            if (ValidateUserInput())
            {
                status = Editor.SetDiscoverySettings(this.hdWebId.Value, SNMPRetriesSlider.Value,
                    TimeSpan.FromMilliseconds(this.SNMPTimeoutSlider.Value), HopCountSlider.Value,
                    TimeSpan.FromMinutes(this.JobTimeoutSlider.Value), this.ClusterId);

                if (startDiscovery)
                {
                    var selectedEngine = Editor.Engines.FirstOrDefault(x => x.EngineId.ToString() == this.EnginesList.SelectedValue);

                    if (selectedEngine != null)
                    {
                        Session["EngineName"] = selectedEngine.IP;
                        Session["BusinessLayerPort"] = selectedEngine.BusinessLayerPort;
                    }

                    status = Editor.StartDiscoveryJob(this.hdWebId.Value, this.ClusterId, this.EnginesList.SelectedValue);
                }
            }

            return status;
        }

        private void ResultPageRedirection(string taskId, string clusterId, string engineId = null)
        {
            Editor.Session_Clear(WebParamId, clusterId);
            string url = Editor.AddParamsToUrl("SubnetResult.aspx", taskId, clusterId, engineId);
            Response.Redirect(url, true);
        }

        #endregion
    }
}
