<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MonitoredSNMPnodes.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.MonitoredSNMPNodes" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock Requires="sw-subnet-discover.js" Orientation="jsInit" runat="server" />

<IPAMmaster:CssBlock Requires="~/orion/styles/Discovery.css,~/orion/styles/NodeMNG.css" runat="server">
#NodeTree2 th {
    background-color: #e2e1d4;
    cursor: pointer;
    font-style: normal;
}
</IPAMmaster:CssBlock>

<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include runat="server" File="js/jquery/timePicker.css" />
<orion:Include runat="server" File="UnmanageDialog.css" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />
<orion:Include runat="server" File="Nodes/js/ChangeEngineDialog.js" />
<orion:Include runat="server" File="jquery/jquery.bgiframe.min.js" Section="Bottom" />

<style type="text/css">
    TABLE {
        border-color: -moz-use-text-color;
        border-style: none;
        border-width: 1px;
    }
    #NodeTree2 td:nth-child(1),#NodeTree2 th:nth-child(1) {
        text-align: center; /* center checkbox horizontally */
        vertical-align: middle; /* center checkbox vertically */
    }
    #NodeTree2 { 
            border:0px solid #bbb;
            border-collapse:collapse; 
        }
        #NodeTree2 td,#NodeTree2 th { 
            border:0px solid #ccc;
            border-collapse:collapse;
            overflow: hidden;
            padding: 4px 3px 3px 5px;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
</style>
<div>
    <table class="NodeManagement">
        <tr valign="top" >
            <td><span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VR_3 %></span></td>
            <td style ="text-align:right">
                <input id="search" style="width: 300px" type="text" />
                <orion:LocalizableButtonLink runat="server" CssClass="searchButton" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBCODE_VB0_131 %>" DisplayType="Small" Style="vertical-align: baseline;" />
            </td>
        </tr>
        <tr valign="top" align="left">
            <td style="border-right: #e1e1e0 1px solid; border-left: 0; border-top: 0; border-bottom: 0; background-color: white;">
                <div class="NodeGrouping">
                    <div class="GroupSection">
                        <div style="width: 180px"><%= Resources.CoreWebContent.WEBDATA_VB0_5 %></div>
                        <select id="groupByProperty" style="width: 100%">
                            <option value=""><%= Resources.CoreWebContent.WEBCODE_AK0_70 %></option>
                            <option value="Vendor"><%= Resources.CoreWebContent.WEBCODE_VB0_124 %></option>
                            <option value="MachineType"><%= Resources.CoreWebContent.WEBDATA_VB0_16 %></option>
                            <option value="EngineID"><%= Resources.CoreWebContent.WEBDATA_VB0_134 %></option>
                            <option value="ObjectSubType"><%= Resources.CoreWebContent.WEBDATA_SO0_1 %></option>
                            <option value="SNMPVersion"><%= Resources.CoreWebContent.WEBCODE_VB0_340 %></option>
                            <option value="Status"><%= Resources.CoreWebContent.WEBDATA_VB0_14 %></option>
                            <option value="Location"><%= Resources.CoreWebContent.WEBDATA_VB0_128 %></option>
                            <option value="Contact"><%= Resources.CoreWebContent.WEBDATA_VB0_129 %></option>
                            <option value="Community"><%= Resources.CoreWebContent.WEBCODE_VB0_190 %></option>
                            <option value="RWCommunity"><%= Resources.CoreWebContent.WEBCODE_VB0_339 %></option>
                            <option value="IPAddressType"><%= Resources.CoreWebContent.WEBDATA_AK0_352 %></option>
                            <option value="Outage"><%= Resources.CoreWebContent.WEBDATA_VS1_3 %></option>
                        </select>
                    </div>
                    <ul class="NodeGroupItems"></ul>
                </div>

            </td>
            <td>
                <div id="selectAllPan"></div>
                <%--toolbar based on ext-js 2.2 styles--%>
                <div class="x-toolbar x-small-editor" id="extPagerTop" style="height: 24px;">
                    <table cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="firstPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-first" id="firstPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="previousPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-prev" id="previousPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span5"></span>
                                </td>
                                <td style="width: 30px;">
                                    <span class="ytb-text" id="Span6"><%= Resources.CoreWebContent.WEBDATA_VB0_9 %></span>
                                </td>
                                <td style="width: 30px;">
                                    <input type="text" class="sw-tbar-page-number x-tbar-page-number" value="1" size="3" id="pageNumberTop" />
                                </td>
                                <td>
                                    <span class="ytb-text" id="totalPageCountTop"><%= Resources.CoreWebContent.WEBDATA_TM0_264 %> </span>
                                </td>
                                <td>
                                    <span class="ytb-sep"></span>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="nextPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-next" id="nextPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="lastPageTableTop" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-last" id="lastPageTop">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep"></span>
                                </td>
                                <td style="width: 55px;">
                                    <span class="ytb-text" id="paegSizeTitleTop"><%= Resources.CoreWebContent.WEBDATA_VB0_10 %></span>
                                </td>
                                <td>
                                    <input type="text" class="x-tbar-page-number sw-tbar-page-number" value="1" size="3" id="pageSizeTop" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="x-paging-info" id="displayInfoTop" style="padding: 2px 2px 0px 2px; color: Black;">
                    </div>
                </div>
                <table id="NodeTree2" cellpadding="0" cellspacing="0" style="width: 450px">
                    <thead>
                        <tr>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <%-- bottom pager based on extJs v 2.2 --%>
                <div class="x-toolbar x-small-editor" id="extPager" style="height: 24px;">
                    <table cellspacing="0">
                        <tbody>
                            <tr>
                                <td id="Td1">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="firstPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-first" id="firstPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td2">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon x-item-disabled"
                                        id="previousPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-prev" id="previousPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span1"></span>
                                </td>
                                <td style="width: 30px;">
                                    <span class="ytb-text" id="Span2"><%= Resources.CoreWebContent.WEBDATA_VB0_9 %></span>
                                </td>
                                <td style="width: 30px;">
                                    <input type="text" class="sw-tbar-page-number x-tbar-page-number" value="1" size="3"
                                        id="pageNumber" />
                                </td>
                                <td>
                                    <span class="ytb-text" id="totalPageCount"><%= Resources.CoreWebContent.WEBDATA_TM0_264 %> </span>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span3"></span>
                                </td>
                                <td id="Td3">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="nextPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-next" id="nextPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td id="Td4">
                                    <table cellspacing="0" cellpadding="0" border="0" class="x-btn-wrap x-btn x-btn-icon "
                                        id="lastPageTable" style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td class="x-btn-left">
                                                    <i>&nbsp;</i>
                                                </td>
                                                <td class="x-btn-center">
                                                    <em unselectable="on">
                                                        <button type="button" class="x-btn-text x-tbar-page-last" id="lastPage">
                                                            &nbsp;</button></em>
                                                </td>
                                                <td class="x-btn-right">
                                                    <i>&nbsp;</i>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td>
                                    <span class="ytb-sep" id="Span4"></span>
                                </td>
                                <td style="width: 55px;">
                                    <span class="ytb-text" id="paegSizeTitle"><%= Resources.CoreWebContent.WEBDATA_VB0_10 %></span>
                                </td>
                                <td>
                                    <input type="text" class="x-tbar-page-number sw-tbar-page-number" value="1" size="3"
                                        id="pageSize" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="x-paging-info" id="displayInfo" style="padding: 2px 2px 0px 2px; color: Black;">
                    </div>
                </div>

            </td>
        </tr>
    </table>
</div>

