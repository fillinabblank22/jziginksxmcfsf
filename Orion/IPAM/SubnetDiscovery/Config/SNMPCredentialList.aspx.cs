﻿using System;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;

namespace SolarWinds.IPAM.WebSite
{
    public partial class SNMPCredentialList : CommonPageServices
    {
        #region Declarations

        public string WebParamId
        {
            get
            {
                return WebSecurityHelper.SanitizeHtml(GetParam(DiscoveryWorkflowHelper.PARAM_WEBID, null));
            }
        }

        public string ClusterId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_CLUSTERID, null);
            }
        }

        private PageDiscoveryWizardEditor Editor { get; set; }

        #endregion // Declarations

        #region Constructors

        public SNMPCredentialList()
        {
            this.Editor = new PageDiscoveryWizardEditor(this);
        }

        #endregion // Constructors

        #region Page Event Handler
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
                Editor.AttachCancelButtonEvent(this.imgbCancel);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = IPAMDiscoveryStep.SNMP;

            if (!IsPostBack)
            {
                InitControls();
            }
        }

        private bool InitControls()
        {
            this.hdWebId.Value = this.WebParamId;
            Editor.ActivateProgressStep(null, PageDiscoveryWizardEditor.IPAM_DISCOVERY_SNMP);
            return true;
        }

        #endregion

        # region ButtonClick Handler

        /// <summary> </summary>
        protected void ImgbNext_Click(object sender, EventArgs e)
        {
            if (!Save())
                return;

            string url = Editor.AddParamsToUrl(ProgressIndicator1.NextStep, null, this.ClusterId);
            Response.Redirect(url, true);
        }

        protected void ImgbBack_Click(object sender, EventArgs e)
        {
            //Editor.UpdateOrigionalCredentialList(this.hdWebId.Value);
            string url = Editor.AddParamsToUrl(ProgressIndicator1.PrevStep, null, this.ClusterId);
            Response.Redirect(url, true);
        }

        /// <summary> </summary>
        protected void ImgbCancel_Click(object sender, EventArgs e)
        {
            Editor.CancelWizard(null, this.ClusterId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion

        #region Validate & Save

        private bool ValidateUserInput()
        {
            return true;
        }

        private bool Save()
        {
            if (!ValidateUserInput())
                return false;

            Editor.UpdateTempCredentialList(this.hdWebId.Value);

            return true;
        }

        #endregion
    }
}