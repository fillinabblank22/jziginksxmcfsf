using System;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Web.Discovery;
using System.Text;
using SolarWinds.Orion.Core.Models.Discovery;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Web.Common;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;
using System.Linq;

namespace SolarWinds.IPAM.WebSite
{
    /// <summary> UI component for bulk uload of IP addresses and host names </summary>
    public partial class AddNodeByIPAddress : UserControl
    {
        private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
        public string WebId;
        private PageDiscoveryWizardEditor Editor { get; set; }
        public List<string> ipAddresses = new List<string>();
       
        private static void AddBulkLine(List<string> bulkList, string line)
        {
            StringBuilder lineBuilder = new StringBuilder(line.Trim());
            if (lineBuilder.Length == 0) return;
            if (lineBuilder[lineBuilder.Length - 1] == '\r')
            {
                lineBuilder.Remove(line.Length - 1, 1);
            }

            string normalizedLine = lineBuilder.ToString();

            if (!bulkList.Exists(l => l.Equals(normalizedLine, StringComparison.InvariantCultureIgnoreCase)))
            {
                bulkList.Add(normalizedLine);
            }
        }
        public List<string> GetNodeByAddresses()
        {
            string[] lines = HostsTextBox.Text.Split('\n');
            string[] removeRlines = lines.Select(x => x.Replace("\r", "")).ToArray();
            var ipAddresses = new List<string>();
            foreach (string s in removeRlines)
            {
                if (s != "")
                    ipAddresses.Add(s);
            }
            //var ipAddresses = new List<string>(orglines);
            return ipAddresses;
        }
        public void SetNodeByAddresses(List<string> ipAddresses)
        {
            StringBuilder sb = new StringBuilder();
            sb.Replace('\n', ' ');
            sb.Replace('\r', ' ');
            List<string> bulkList = new List<string>();
            foreach (string line in ipAddresses)
            {
                AddBulkLine(bulkList, line);
            }

            foreach (string line in bulkList)
            {
                sb.AppendLine(line);
            }
            this.HostsTextBox.Text = sb.ToString();
            hdIpAddress.Value = string.Join(",", bulkList);
        }
    }
}
