<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryMasterPage.master" AutoEventWireup="true" CodeFile="AddNodesToDiscover.aspx.cs"
    Inherits="SolarWinds.IPAM.WebSite.AddNodesToDiscover" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_36%>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register Src="~/Orion/Controls/PageMenu.ascx" TagPrefix="orion" TagName="PageMenu" %>
<%@ Register Src="~/Orion/IPAM/SubnetDiscovery/Config/AddNodeByIPAddress.ascx" TagPrefix="ipam" TagName="AddNodeByIPAddress" %>
<%@ Register Src="~/Orion/IPAM/SubnetDiscovery/Config/MonitoredSNMPnodes.ascx" TagPrefix="ipam" TagName="MonitoredSNMPNodes" %>
<%@ Register Src="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryProgressIndicator.ascx" TagPrefix="ipam" TagName="Progress" %>


<asp:Content ID="Content1" ContentPlaceHolderID="discoveryWizardContentHolder" runat="server">
    
    <IPAMmaster:CssBlock ID="CssBlock1" Requires="sw-ipv6-manage.css,~/orion/styles/Discovery.css" runat="server">
        .sw-form-col-label {width: 160px !important; }
        .sw-field-label { word-wrap: break-word; }
        .sw-form-disabled { color: #777; }
    </IPAMmaster:CssBlock>

    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-expander.js,sw-ipv6-manage.js,sw-subnet-discover.js" Orientation="jsPostInit" runat="server">
        $(document).ready( function(){ $('.exp-box').expander({delay: 0}); });
    </IPAMmaster:JsBlock>

    <script type="text/javascript" language="javascript">
        var txtAdd = "";
        var gateWay = "";
        var hdnMulitiNodes = "";
        window.$SW = window.$SW || {}; $SW.Env = $SW.Env || {}; var env = $SW.Env;
        function Initialize() {
            var hdnMulitiNodes = $('#<%=hdMultipleNodes.ClientID%>');
            if (hdnMulitiNodes.val() != "undefined") {
                var splitVal = hdnMulitiNodes.val().trim();
                if (splitVal.length > 0) {
                    var splitValue = splitVal.split(",");
                    var multiNodes = [];
                    for (var i = 0; i < splitValue.length; i++) {
                        multiNodes.push(splitValue[i]);
                    }
                    $SW.Env["PageLoadBehav"] = multiNodes;
                }
            }
        }
        function pageLoad(sender, e) {
            getDefaultGateway();             
            $('.expander').expander();
            var objectType = MNG.Prefs.load("ObjectType", "Orion.Nodes");
            $("#groupByProperty").val(MNG.Prefs.load("GroupBy", "ObjectSubType")).change(MNG.LoadGroupByValues);
            $("#showObjectType").val(objectType).change(function () { MNG.showObjectType($(this).val()); });
            MNG.showObjectType(objectType, true);

            $("#groupByProperty").change(MNG.LoadGroupByValues);
            $("#groupByProperty").val(MNG.Prefs.load("GroupBy", "ObjectSubType"));
            $("#search").keypress(function (e) {
                if (e.which == 13)
                    MNG.startSearch();
            });
            var watermark = new SW.Core.Widgets.WatermarkTextbox($("#search"), MNG.WatermarkTextbox);
            $(".searchButton").click(MNG.startSearch);
            
            $("#pageNumber").keypress(Pager.PageNumberChanging);
            $("#pageNumberTop").keypress(Pager.PageNumberChanging);
            $("#pageSize").keypress(Pager.PageSizeChanging);
            $("#pageSizeTop").keypress(Pager.PageSizeChanging);
            $(window).unload(function () { MNG.Prefs.save("ActualPage", Pager.actualPage); });
            $(window).load(MNG.SetMenuWidth);
        };
         function getDefaultGateway() {
            var defaultGatewayId = document.getElementById("<%=DefaultGateWayAddress.ClientID%>");
            $.ajax({
                type: "POST",
                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result);
                    gateWay = res.data["defaultGateway"];
                    $(defaultGatewayId).val(gateWay);
                    document.getElementById("<%=defaultgetway.ClientID%>").innerHTML = '<%=Resources.IPAMWebContent.IPAMWEBDATA_SV1_3 %> - ' + gateWay;
                },
                failure: function () {
                    gateWay = "";
                    $(defaultGatewayId).val(gateWay);
                    document.getElementById("<%=defaultgetway.ClientID%>").innerHTML = '<%=Resources.IPAMWebContent.IPAMWEBDATA_SV1_3 %> - ' + gateWay;

                },
                async: false,
                data:
                {
                    entity: 'IPAM.DiscoveredSubnets',
                    verb: 'GetDefaultGateway'
                }
            });
           
        };
        function AssignMultiNodes() {
            hdnMulitiNodes = $('#<%=hdMultipleNodes.ClientID%>');
            hdnMulitiNodes.val($SW.Env["PageLoadBehav"]);

            var multiTextValue = document.getElementById("ctl00_ctl00_ctl00_ctl00_BodyContent_ContentPlaceHolder1_adminContentPlaceholder_discoveryWizardContentHolder_nodeByAddress_HostsTextBox");
            if (multiTextValue != null)
                multiTextValue = multiTextValue.value.trim();
            else
                multiTextValue = "";

            var isChecked = $('#<%=cbDisableAutoScanning.ClientID%>').is(":checked");

            if (multiTextValue != "") {
                if (StartValidation() == false)
                    return false;
            }
            if (hdnMulitiNodes.val() == "" &&  multiTextValue == "" && isChecked ==false) {
                Ext.MessageBox.show({
                    title: "<%=Page.Title%>",
                   msg: "<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_41%>",
                   buttons: Ext.MessageBox.OK,
                   icon: Ext.MessageBox.ERROR,
                   width: 350,
                   fn: function () { }
                });
                return false;
            } 
            return true;
        };

    
        function StartValidation() {
            $('.ValidationDiv .TransparentBoxID').show();
            var txtAdd = document.getElementById("ctl00_ctl00_ctl00_ctl00_BodyContent_ContentPlaceHolder1_adminContentPlaceholder_discoveryWizardContentHolder_nodeByAddress_HostsTextBox");
            var realValue = txtAdd.value.trim();
            var errorBox = "";

            if (realValue == "" || realValue == null) {
                errorBox = "No ip addresses are specified.";
                $('.ValidationDiv .TransparentBoxID').hide();
                $(".HostsTextBoxValidator").append(errorBox);
                $(".HostsTextBoxValidator").text("No ip addresses are specified.").css({ color: 'red', visibility: 'visible', display: 'block' });
                
                return false;
            }

            var splitValue = realValue.split("\n");
            for (var i = 0; i < splitValue.length; i++) {
                if ((splitValue[i] != "") && !$SW.IP.v4_isok($SW.IP.v4($.trim(splitValue[i])))) {
                    errorBox += "Unable to resolve :" + splitValue[i] + "\n\r";
                }
            }
            if (errorBox == "") {
                $('.ValidationDiv .TransparentBoxID').hide();
                $(".HostsTextBoxValidator").text("Validation Completed Successfully").css({ color: 'green', visibility: 'visible', display: 'block' });
                return true;
            }
            else {
                $('.ValidationDiv .TransparentBoxID').hide();
                $(".HostsTextBoxValidator").append(errorBox);
                $(".HostsTextBoxValidator").text(errorBox).css({ color: 'red', visibility: 'visible', display: 'block' });
                return false;
            }
            return true;
        }
        $(document).ready(function () {

            Initialize();
            
        });

       </script> 
    <style type="text/css">
        .group-box {
            margin-bottom: 20px;
            max-width: 876px;
            padding: 8px 6px;
            background: none repeat scroll 0 0 #E4F1F8;
        }

        .PageMenu {
            width: 250px !important;
        }
    </style>

    <asp:ScriptManagerProxy ID="ScriptManager" runat="server">
        <Services>
            <asp:ServiceReference Path="../../../Services/Information.asmx" />
            <asp:ServiceReference Path="../../../Services/NodeManagement.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
    
    <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    <table>
         <tr>      
         <td><h1 class="sw-hdr-title"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_33 %></h1><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_35 %> </td>
        </tr><tr><td><br />
    <div style="width: 100%;">
        <ipam:Progress ID="ProgressIndicator1" runat="server" />
        <div class="GroupBox" id="NormalText">
            <asp:HiddenField ID="hdWebId" runat="server" />
            <asp:HiddenField ID="hdMultipleNodes" runat="server" />
            <asp:HiddenField ID="DefaultGateWayAddress" runat="server" />
            
            <table>
                <tr>
                    <td>
                        <div><span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_33 %></span></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_DF1_34,"<br />") %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="contentBlock">
                            <table>
                                <tr>
                                    <td>
                                        <div><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_35 %></div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="group-box" style="width:950px">
                            <asp:CheckBox ID="cbDisableAutoScanning"   runat="server"  /> 
                                <span id="defaultgetway"  runat="server" class="ActionName"> <%= Resources.IPAMWebContent.IPAMWEBDATA_SV1_3 %></span>
                                <div class="sw-form-item">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_37 %>
                                </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        
                                <div  class="exp-box"  header="<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_36 %>" collapsed="true">
                                    <asp:UpdatePanel ID="SubnetUpdatePanel" runat="server">
                            <ContentTemplate>
                                    <div class="contentBlock">
                                        <table>
                                            <tr>
                                                <td class="SelectionMethod">
                                                    <orion:PageMenu ID="NetworkPageMenu" runat="server" OnClicked="NetworkPageMenu_Clicked" />
                                                </td>
                                                <td class="NodeByAddressTd" valign="top" id="nodeByAddressTd" runat="server">
                                                    <ipam:AddNodeByIPAddress runat="server" ID="nodeByAddress" />
                                                </td>
                                                <td id="MonitoredSNMPNodesTd" runat="server" valign="top">
                                                    <ipam:MonitoredSNMPNodes runat="server" ID="monitoredSNMPNode" />
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                 </ContentTemplate>
                        </asp:UpdatePanel>
                                </div>
                           
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <table class="NavigationButtons">
                            <tr>
                                <td>
                                    <div class="sw-btn-bar-wizard">
                                        <orion:LocalizableButton ID="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next" OnClientClick=" return AssignMultiNodes()" OnClick="ImgbNext_Click"/>
                                        <orion:LocalizableButton ID="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="ImgbCancel_Click" CausesValidation="false" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div>
        </div>
    </div>
        </td></tr>
    </table>

</asp:Content>



