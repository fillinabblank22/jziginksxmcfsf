﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DiscoverySNMPCredAdd : CommonPageServices
    {
        PageDicoveryCredEditor editor;
        public DiscoverySNMPCredAdd()
        {
            editor = new PageDicoveryCredEditor(this, true);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            byte version = byte.Parse(ddlSNMPVersion.SelectedValue);

            if (version == 3)
                CommunityStringRequiredValidator.Enabled = false;

            editor.Save(true);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void OnFirstAddressValidate(object sender, ServerValidateEventArgs args)
        {
            editor.OnNameExistsValidate(sender,args);
        }
    }
}