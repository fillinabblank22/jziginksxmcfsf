﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryMasterPage.master" AutoEventWireup="true" CodeFile="SNMPCredentialList.aspx.cs" 
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_39%>" Inherits="SolarWinds.IPAM.WebSite.SNMPCredentialList" %>

<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="orion" TagName="HelpHint" %>
<%@ Register Src="~/Orion/IPAM/SubnetDiscovery/Config/DiscoveryProgressIndicator.ascx" TagPrefix="ipam" TagName="Progress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="discoveryWizardContentHolder" runat="server">
    
        <IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-helpserver,sw-ux-wait.js,sw-ux-ext.js,ext-ux,sw-asd-snmpcredential.js,sw-expander.js" Orientation="jsInit" runat="server" />

        <script type="text/javascript">
            if (!$SW.IPAM.DiscoverySnmpCredential) $SW.IPAM.DiscoverySnmpCredential = {};
            $SW.IPAM.DiscoverySnmpCredential.GridId = 'SNMPCredentialGrid';
            $SW.IPAM.DiscoverySnmpCredential.WebID = '<%=this.WebParamId %>';
            
            $SW.IPAM.DiscoverySnmpCredential.SetWebId = function () {
                $SW.store.baseParams.webID = '<%=this.WebParamId %>';
            };
            
            function CheckCredentialListCount() {
                if (!$SW.IPAM.DiscoverySnmpCredential.TotalRecordCount()) {
                    Ext.MessageBox.show({
                        title: "<%=Page.Title%>",
                        msg: "<%= Resources.IPAMWebContent.IPAMWEBDATA_SE_41%>",
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 350,
                        fn: function() {
                        }
                    });
                    return false;
                }
                return true;
            }
            
        </script>

        <IPAMlayout:DialogWindow jsID="snmpCredentialWindow" helpID="deletehelp" Name="snmpCredentialWindow"
		    Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_218 %>" height="450" width="520" runat="server">
		    <Buttons>
			    <IPAMui:ToolStripButton ID="ToolStripButton1" text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
				    <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['snmpCredentialWindow'], 'dialog', 'save' ); }</handler>
			    </IPAMui:ToolStripButton>
			    <IPAMui:ToolStripButton ID="ToolStripButton2" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
				    <handler>function(){ $SW['snmpCredentialWindow'].hide(); }</handler>
			    </IPAMui:ToolStripButton>
		    </Buttons>
		    <Msgs>
			    <IPAMmaster:WindowMsg Name="ready" runat="server">
				    <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['snmpCredentialWindow'] ); }</handler>
			    </IPAMmaster:WindowMsg>
			    <IPAMmaster:WindowMsg Name="unload" runat="server">
				    <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['snmpCredentialWindow'], true ); }</handler>
			    </IPAMmaster:WindowMsg>
			    <IPAMmaster:WindowMsg Name="close" runat="server">
				    <handler>function(n,o){ $SW['snmpCredentialWindow'].hide(); $SW.IPAM.DiscoverySnmpCredential.RefreshGrid(); }</handler>
			    </IPAMmaster:WindowMsg>
		    </Msgs>
	    </IPAMlayout:DialogWindow>
    <table>
         <tr>      
         <td><h1 class="sw-hdr-title"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_33 %></h1><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_35 %> </td>
        </tr><tr><td><br />
        <div style="width: 950px;">
            
            <asp:HiddenField ID="hdWebId" runat="server" />

            <ipam:Progress ID="ProgressIndicator1" runat="server" />
	        <div class="GroupBox" id="NormalText">
                <table>
                    <tr>
                        <td>
                            <span class="ActionName"><%= Resources.CoreWebContent.WEBDATA_AK0_68 %></span>
                            <br />
                            <div style="width: 100%;">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VR_1 %><br />
                                <a target="_blank" href="<%=HelpHelper.GetHelpUrl("OrionPHAboutSNMP") %>"><%= Resources.CoreWebContent.WEBDATA_AK0_70 %></a>
                            </div>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <orion:HelpHint runat="server">
                                <HelpContent>
                                     <b><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_31 %></b> <%= Resources.IPAMWebContent.IPAMWEBDATA_SV1_12 %> 
                                </HelpContent>
                            </orion:HelpHint>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <IPAMlayout:GridBox runat="server" ID="SNMPCredentialGrid" title="" UseLoadMask="True" height="230" width="900" autoHeight="false" autoFill="false" autoScroll="false"
                                emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_67 %>" borderVisible="true" deferEmptyText="false" StripeRows="true" EnableDragAndDrop="false" RowSelection="True"
                                SelectorName="selector" listeners="{cellclick: $SW.IPAM.DiscoverySnmpCredential.grid_cellclick,render: $SW.IPAM.DiscoverySnmpCredential.SetWebId  }">
                                <TopMenu>
                                    <IPAMui:ToolStripItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_32 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_69 %>" iconCls="mnu-add" runat=server>
                                        <handler>function(el){ $SW.IPAM.DiscoverySnmpCredential.Add(el,-1); }</handler>
                                    </IPAMui:ToolStripItem>
                                </TopMenu>
                                <Columns>
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_81 %>" width="100" dataIndex="DisplayOrder" isSortable="true" runat="server" />
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_78 %>" width="380" dataIndex="DisplayName" isSortable="true" runat="server" />
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_79 %>" width="200" dataIndex="SNMPVersionText" isSortable="true" runat="server" />
                                    <IPAMlayout:GridColumn title="Actions" width="150" isFixed="true" isHideable="false" dataIndex="ID" runat="server" renderer="$SW.IPAM.DiscoverySnmpCredential.action_buttons()" />
                                </Columns>

                                <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="/Orion/IPAM/sessiondataprovider.ashx" proxyEntity="IPAM.DiscoveryCredential" AmbientSort="ID" 
                                    listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                                    <Fields>
                                        <IPAMlayout:GridStoreField dataIndex="ListIndex" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="DisplayName" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="SNMPVersion" dataType="int" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="SNMPVersionText" dataType="string" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="DisplayOrder" dataType="int" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="CredID" dataType="int" runat="server" />
                                    </Fields>
                                </Store>

                            </IPAMlayout:GridBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <table class="NavigationButtons"><tr><td>
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton id="imgBack" runat="server" DisplayType="Secondary" LocalizedText="Back"  OnClick="ImgbBack_Click" CausesValidation="false"/>
                                    <orion:LocalizableButton id="imgbNext" runat="server" DisplayType="Primary" LocalizedText="Next" OnClientClick="return CheckCredentialListCount();"  OnClick="ImgbNext_Click"/>
                                    <orion:LocalizableButton id="imgbCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="ImgbCancel_Click" CausesValidation="false"/>
                                </div>
                            </td></tr></table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>  </td></tr>
    </table>
</asp:Content>
