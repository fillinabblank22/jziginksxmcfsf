﻿using System;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;
using System.Collections.Generic;
using System.Linq;

namespace SolarWinds.IPAM.WebSite
{
    public partial class AddNodesToDiscover : CommonPageServices, IPostBackEventHandler
    {
        #region Declarations

        //private string WebId { get; set; }
        private PageDiscoveryWizardEditor Editor { get; set; }
        private IPAMDiscoverySelectionMethod ipamDiscoverySelectionMethod { get; set; }
        public string WebParamId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_WEBID, null);
            }
        }

        public string ClusterId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_CLUSTERID, null);
            }
        }

        #endregion // Declarations

        #region Constructors

        public AddNodesToDiscover()
        {
            this.Editor = new PageDiscoveryWizardEditor(this);
           
        }

        #endregion // Constructors

        private void CreatePageMenu()
        {
            NetworkPageMenu.Title.TextLabel.Text = Resources.CoreWebContent.WEBCODE_AK0_55;
            NetworkPageMenu.Title.CSSClass = "SelMeth";
            NetworkPageMenu.Title.TextBackImageUrl = "/Orion/Discovery/images/bkgd_selectionMethod.gif";

            NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem(Resources.IPAMWebContent.IPAMWEBDATA_DF1_38, IPAMDiscoverySelectionMethod.MonitoredSNMPNodes.ToString()));
            NetworkPageMenu.Items.Add(new System.Web.UI.WebControls.MenuItem(Resources.IPAMWebContent.IPAMWEBDATA_DF1_39, IPAMDiscoverySelectionMethod.AddNodesByIPAddress.ToString()));

            NetworkPageMenu.Items[0].Selected = true;
        }
        private void GetSetNodeByIpAddresses()
        {
             var ipAddresses = new List<string>();
             Editor.GetNodeByIPAddress(WebParamId, out ipAddresses);
             this.nodeByAddress.SetNodeByAddresses(ipAddresses);
             Editor.SetNodeByIPAddress(this.hdWebId.Value, ipAddresses);
             this.nodeByAddress.WebId = WebParamId;
             this.nodeByAddress.ipAddresses = ipAddresses;
        }
        protected void NetworkPageMenu_Clicked(object sender, MenuEventArgs e)
        {
            var ipAddresses = this.nodeByAddress.GetNodeByAddresses();
            this.nodeByAddress.SetNodeByAddresses(ipAddresses);
            Editor.SetNodeByIPAddress(this.hdWebId.Value, ipAddresses);
            //SetMultipleNodes();
            if (e.Item.Value == this.lastMenuItem.Value)
            {
                return;
            }

            this.Validate();
            if (!this.IsValid)
            {
                this.lastMenuItem.Selected = true;
                return;
            }

            this.SelectionMethod = (IPAMDiscoverySelectionMethod)Enum.Parse(typeof(IPAMDiscoverySelectionMethod), e.Item.Value);
            this.ShowSelectionMethod();
        }
        protected IPAMDiscoverySelectionMethod SelectionMethod
        {
            get
            {
                return ipamDiscoverySelectionMethod;
            }
            set
            {
                ipamDiscoverySelectionMethod = value;
            }
        }
        public void RaisePostBackEvent(string argument)
        {
            foreach (System.Web.UI.WebControls.MenuItem item in this.NetworkPageMenu.Items)
            {
                if (item.Value == argument)
                {
                    item.Selected = true;
                    this.SelectionMethod = (IPAMDiscoverySelectionMethod)Enum.Parse(typeof(IPAMDiscoverySelectionMethod), argument);
                    this.ShowSelectionMethod();
                    break;
                }
            }
        }
        protected void ShowSelectionMethod()
        {
           
            IPAMDiscoverySelectionMethod method = this.SelectionMethod;
            if ((method == IPAMDiscoverySelectionMethod.AddNodesByIPAddress))
            {
                nodeByAddressTd.Attributes.Add("style", "display:block;");
            }
            else
            {
                nodeByAddressTd.Attributes.Add("style", "display:none;");
            }

            if ((method == IPAMDiscoverySelectionMethod.MonitoredSNMPNodes))
            {
                MonitoredSNMPNodesTd.Attributes.Add("style", "display:block;");
            }
            else
            {
                MonitoredSNMPNodesTd.Attributes.Add("style", "display:none;");
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
            {
                Editor.AttachCancelButtonEvent(this.imgbCancel);
                List<int> multipleNodes = new List<int>();
                this.Editor.GetMultipleNodes(WebParamId, out multipleNodes);
                var multiNodeConstruction = string.Join(",", multipleNodes);
                hdMultipleNodes.Value = multiNodeConstruction;
                var isDefaultGateway = false;
                Editor.GetDefaultGateway(WebParamId, out isDefaultGateway);
                cbDisableAutoScanning.Checked = isDefaultGateway;
            }
        }
        private System.Web.UI.WebControls.MenuItem lastMenuItem;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = IPAMDiscoveryStep.AddNodes;

            if (string.IsNullOrEmpty(this.hdWebId.Value) && string.IsNullOrEmpty(WebParamId))
            {
                this.hdWebId.Value = Editor.Session_GetNewWebId();
                Editor.CreateEmptyDicoveryJobSettings(this.hdWebId.Value, this.ClusterId);
                cbDisableAutoScanning.Checked = true;
            }

            if (!IsPostBack)
            {
                GetSetNodeByIpAddresses();
                InitControls();
                CreatePageMenu();

                //sets visibility according to session (IPRange is default)
                this.ShowSelectionMethod();
                //sets appropriate menu item
                foreach (System.Web.UI.WebControls.MenuItem item in this.NetworkPageMenu.Items)
                {
                    if (item.Value == this.SelectionMethod.ToString())
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            this.lastMenuItem = this.NetworkPageMenu.SelectedItem;
            
        }

        # region ButtonClick Handler

        /// <summary> </summary>
        protected void ImgbNext_Click(object sender, EventArgs e)
        {
            if (!Save())
                return;

            string url = Editor.AddParamsToUrl(ProgressIndicator1.NextStep, this.hdWebId.Value, this.ClusterId);
            Response.Redirect(url, true);
        }

        /// <summary> </summary>
        protected void ImgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.CancelWizard(webId, this.ClusterId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion

        #region Wizzard

        private void InitControls()
        {
            List<string> ipAddresses =null;
            Editor.GetNodeByIPAddress(this.WebParamId, out ipAddresses);
            Editor.ActivateProgressStep(this.hdWebId.Value, PageDiscoveryWizardEditor.IPAM_DISCOVERY_ADDNODES);
        }

        private bool ValidateUserInput()
        {
            return true;
        }

        private bool Save()
        {
            if (!ValidateUserInput())
                return false;
            var ipAddresses = this.nodeByAddress.GetNodeByAddresses();
            Editor.SetNodeByIPAddress(this.hdWebId.Value, ipAddresses);
            SetMultipleNodes();
            Editor.SetDefaultGateway(this.hdWebId.Value, cbDisableAutoScanning.Checked);

            if (cbDisableAutoScanning.Checked && !string.IsNullOrEmpty(this.DefaultGateWayAddress.Value))
                Editor.SetDefaultGateWay(this.hdWebId.Value, this.DefaultGateWayAddress.Value);

            return true;
        }

        #endregion

        private void SetMultipleNodes()
        {
            List<int> multiNodes = new List<int>();
            if (hdMultipleNodes.Value.Trim().Length > 0)
                multiNodes = hdMultipleNodes.Value.Split(',').Select(int.Parse).ToList();
            Editor.SetMultipleNodes(this.hdWebId.Value, multiNodes, this.ClusterId);
        }
    }

    public enum IPAMDiscoverySelectionMethod
    {
        MonitoredSNMPNodes = 0,
        AddNodesByIPAddress = 1,
        RangeForCapableNodes = 2,
    }
}