<%@ Import Namespace="SolarWinds.Orion.Core.Web.Discovery"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddNodeByIPAddress.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.AddNodeByIPAddress" %>
<%@ Register Src="~/Orion/Controls/TransparentBox.ascx" TagPrefix="orion" TagName="TBox" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<div class="ValidationDiv">
    <orion:TBox ID="ValidationDiv" runat="server" Message="<%$ Resources: CoreWebContent,WEBDATA_AK0_92 %>" MessageVisible="true" />
</div>  
      

<div class="BulkUpload">
    <p class="SectionTitle"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_40 %></p>
    <div>
        <asp:HiddenField ID="hdIpAddress" runat="server" />
        <asp:TextBox ID="HostsTextBox" runat="server" TextMode="MultiLine" CssClass="HostTextBox"/>
        <br />
        <orion:LocalizableButton id="ValidateButton" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="Validate" OnClientClick="StartValidation(); return false; " />
        <div class="HostsTextBoxValidator" style ="white-space: pre-line;"/>
    </div>
    
    
</div>
    
