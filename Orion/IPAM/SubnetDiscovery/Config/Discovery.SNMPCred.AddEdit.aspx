﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" CodeFile="Discovery.SNMPCred.AddEdit.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DiscoverySNMPCredAdd"
    Title="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_40%>" ValidateRequest="false" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ContentPlaceHolderID="main" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <!-- parent window will be sending down events, which will cause a postback and run a delegate -->
    <IPAMmaster:WindowMsgListener ID="MsgListener" Name="dialog" runat="server">
        <Msgs>
            <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
        </Msgs>
    </IPAMmaster:WindowMsgListener>
    
    <IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server"/>

    <IPAMmaster:JsBlock Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server>
        Ext.onReady(function(){ $SW.AdminSnmpCredEditorInit($nsId); });
    </IPAMmaster:JsBlock>
    
    <IPAMui:ValidationIcons runat=server />
    <IPAMmaster:JsBlock Requires="ext,sw-ux-wait.js,sw-dnsrecord-manage.js,sw-dialog.js,ext-ux,sw-expander.js" Orientation="jsPostInit" runat="server">
        $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
        $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
    </IPAMmaster:JsBlock>

  <div id=formbox>
    
    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_40 %></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_41 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_42 %>" runat="server" />
            </div></td>
            <td>
            <asp:RequiredFieldValidator
                ID="DisplayNameRequiredValidator"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtDisplayName"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_44 %>"></asp:RequiredFieldValidator>
            <asp:CustomValidator 
                ID="NameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate ="txtDisplayName" OnServerValidate="OnFirstAddressValidate" 
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_45 %>"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_46 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlSNMPVersion" runat="server">
                    <asp:ListItem Selected=true Value="2" Text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_47 %>"/>
                </asp:DropDownList>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div id=layoutv2only><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0" style="display: none">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbVersion2Only" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_48 %>"  runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0" style="display: none">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr id="SNMPPort">
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_49 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPPort" CssClass="x-form-text x-form-field" runat="server" Text="161" />
            </div>
            </td>
            <td>
                <asp:CompareValidator ID="SNMPPortTypeValidator" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Operator="GreaterThan"
                    ValueToCompare="0"
                    Type="Integer"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_50 %>"></asp:CompareValidator>
                <asp:CompareValidator ID="SNMPPortTypeValidator2" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Operator="LessThanEqual"
                    ValueToCompare="65535"
                    Type="Integer"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_52 %>" ></asp:CompareValidator>
                 <asp:RequiredFieldValidator ID="SNMPPortRequiredValidator" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_51  %>"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>
   
    <div id="layoutSNMPv1_2">
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_63%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtCommunityString" CssClass="x-form-text x-form-field" runat="server" Text="" />
            </div></td>
            <td>
                <asp:RequiredFieldValidator ID="CommunityStringRequiredValidator" runat="server"
                    ControlToValidate="txtCommunityString"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_53 %>"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table></div>
    </div>

    <div><!-- ie6 --></div>

    <div id="layoutSNMPv3">
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_54 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPv3Username" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_55%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPv3Context" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan=3><div class=sw-form-subheader>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_56%>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_57 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlAuthMethod" runat="server">
                    <asp:ListItem Selected=true Value="None" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_58 %>"/>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_59%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="tbPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item id="divAuthPasswordIsKey" runat="server">
                <asp:CheckBox ID="chbAuthPasswordIsKey" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_34 %>"  runat="server"/>
            </div></td>
        </tr>
        <tr>
            <td colspan=3><div class=sw-form-subheader>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_60 %>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_61%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlSecurityMethod" runat="server">
                    <asp:ListItem Selected=true Value="None" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_58 %>"/>
                </asp:DropDownList> 
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_62%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSecurityPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item id="divSecurityPasswordIsKey" runat="server">
                <asp:CheckBox ID="chbSecurityPasswordIsKey" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_34 %>"  runat="server"/>
            </div></td>
        </tr>
    </table></div>
  </div>

    <asp:ValidationSummary id="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
      <table>
          <tr>
              <td style="width: 117px">&nbsp;</td>
              <td>
                  <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                      <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="btnSave_Click" LocalizedText="Save" DisplayType="Primary" />
                      <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" />
                  </div>
              </td>
      </table>
</div>
</asp:Content>
