﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.DiscoverySubnetEdit"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_285 %>" CodeFile="DiscoverySubnetEdit.aspx.cs" validateRequest="false" %>

<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NeighborScanSettings.ascx" TagName="NeighborScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TransientPeriodSettings.ascx" TagName="TransientPeriodSettings" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<script type="text/javascript">
    function GetSubnetId() {
        return <%= this.discoveredSubnetId %>;
    }
</script>
<IPAMmaster:JsBlock Requires="ext,sw-ux-wait.js,sw-ux-tree.js,ext-quicktips,sw-expander.js,sw-subnet-edit.js,sw-dialog.js,sw-treecombo.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){
    
    /* parent selector */
    var hfParent = $SW.ns$($nsId, 'hfParent');
    var SetParentHiddenValue = function(el, node){
        if(el && hfParent){ hfParent.val(el.value); }
    };
    var ClearParentValue = function(el, node){
        if(el && hfParent){ el.clearValue(); hfParent.val(''); }
    };
    var treeLoader = $SW.MultiEdit.GetParentTreeLoader();
    treeLoader.query = $SW.MultiEdit.SubnetsQuery($SW.SubnetsRange);

    var parentTree = new Ext.ux.form.TreeCombo( {
        loader: treeLoader,
        expandTree: false,
        listeners: {
            beforeselect: function(el, node){
                return $SW.MultiEdit.ChangeParentPermission(node.attributes.Role);
            },
            select: $SW.MultiEdit.ConfirmParentChange(SetParentHiddenValue, ClearParentValue)
        }
    });
    parentTree.render('parentSelector');
    
    var subnetId = GetSubnetId();

    if(subnetId && subnetId != '' && subnetId != 'undefined' && subnetId != '-1')
        GetParentFriendlyName(subnetId,parentTree);

    $SW.SubnetEditorInit($nsId);
    var cidrbox = $SW.ns$($nsId,'txtCidr');
    var cb = $SW.ns$($nsId,'cbEditIPAddresses');
    
    var cbOverwrite = $SW.ns$($nsId,'cbOverwriteSubnet');
    var warningDiv = $('#warnMessage');
    if (!(cbOverwrite[0])) warningDiv.hide();
    
    if(cidrbox[0].value < 20){
            cb.bind( 'click', DisableCheckBox);
            $(cb[0].parentNode).addClass('sw-form-disabled');
            $('#subnettoolarge').removeClass('sw-form-disabled').css("display","");
    }
     function DisableCheckBox(e)
    {
      cb[0].checked = false;
    }
    $('.expander').expander();
});
$SW.SubnetEditCidrAccepted = -1;
$SW.SubnetEditCidrChange = function(minsize)
{
    var sz = minsize;
    return function(src,args)
    {
      var v = parseInt(args.Value);
      if( v <= sz || $SW.SubnetEditCidrAccepted == v)
      {
        args.IsValid = true;
        return;
      }

      var n = confirm( "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_70;E=js}" + "\n\n" + "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_71;E=js}" + "\n" );
      if( n )
      {
        args.IsValid = true;
        $SW.SubnetEditCidrAccepted = v;
      }
      else
      {
        args.IsValid = false;
        $SW.SubnetEditCidrAccepted = -1;
      }

      return args.IsValid;
    };
};
    
    function GetParentFriendlyName(subnetId,treeObject) {
        $SW.DoQuery(String.format('SELECT Z.FriendlyName,Z.GroupId FROM IPAM.GroupNode Z JOIN IPAM.DiscoveredSubnets D ON Z.GroupId = D.ParentId  WHERE Z.GroupId IS NOT NULL AND D.DiscoveredSubnetID = {0}',subnetId), function (values) {
            if (values && values != null && values.length > 0) {
                var hfParent = $SW.ns$($nsId, 'hfParent');
                if(hfParent)
                    hfParent.val(values[0].GroupId);

                treeObject.LoadInitialValue(values[0].GroupId,values[0].FriendlyName);
            }
        });
    }
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css, ~/solarwinds.css, ~/orion/styles/mainlayout.css, ~/webengine/resources/common/globalStyles.css" runat="server">
    #subnettoolarge {
    padding: 2px 2px 2px 0;
}
.sw-form-disabled {
    color: #777;
}
.ArrowedLink a { color : #336699 !important; }
.ArrowedLink a:hover { color : orange !important; }
.sw-form-cols-normal td.sw-form-col-label { width: 180px; }
.sw-form-cols-normal td.sw-form-col-control { width: 300px; }
.sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>

<IPAMui:ValidationIcons runat="server" />

<div id=formbox>
     <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
    <input type="hidden" id="isDemoMode" />
	<%}%>
    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_286 %></div>

    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_408 %>
            </div></td>
            <td>
                <div class="sw-form-item" id="parentSelector">
                    <asp:HiddenField ID="hfParent" runat="server" />
                </div>
            </td>
            <td></td>
        </tr> 
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_41 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="DisplayNameRequire" runat="server"
                  ControlToValidate="txtDisplayName"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_42 %>">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_43 %>
            </div></td>
            <td><div class="sw-form-item">
                <span class="x-item-disabled"><asp:TextBox ID="txtNetworkAddress" ReadOnly=true Width="150px" CssClass="x-form-text x-form-field sw-withlblnum sw-item-disabled-iefix" runat="server" /></span>
                <span class=sw-pairedlbl><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_44 %></span>
                <span class="x-item-disabled" style="padding-left: 30px"><asp:TextBox ID="txtCidr" Width="30px" Enabled ="false" ReadOnly="true" CssClass="x-form-text x-form-field sw-pairednum sw-withlblnum sw-item-disabled-iefix" runat="server" /></span>
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_53 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtNetworkMask" Enabled=false CssClass="x-form-text x-form-field" ReadOnly=true runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="NetworkMaskRequire" runat="server"
                  ControlToValidate="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_54 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_55 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4Mask" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4mask"
                  OtherValidator="NetworkAddressSubnet"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_56 %>"
              /></td>
        </tr>
        <tr>
            <td></td>
            <td colspan=2><div id="ipcount" class="sw-form-item sw-form-clue"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_288 %></div></td>
        </tr>
       <tr>
            <td></td>
            <td colspan="2"><div class="sw-form-item">
                <asp:CheckBox ID="cbEditIPAddresses" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_62 %>"  runat="server"/>
                <div class="sw-form-clue" id='subnettoolarge' style="display: none;"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_59 %></div>
                <div class="sw-form-clue" id='licensenoroom' style="display: none;"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_60 %></div>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_183 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan=2><div class="sw-form-item sw-form-clue">
              <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_184 %>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_185 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtVLAN" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_186 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtLocation" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label></div></td>
            <td><div class=sw-form-item>
            <asp:CheckBox ID="cbOverwriteSubnet" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VN0_41 %>"  runat="server"/>
            </div>
                <div id ="warnMessage" class="sw-suggestion sw-suggestion-warn"><span class="sw-suggestion-icon"></span><%= Resources.IPAMWebContent.IPAMWEBDATA_VN0_42 %></div>
            </td>
        </tr>
        </table></div>
        
    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
    <div class="group-box white-bg" style="margin-bottom: 0px">
        <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
    </div>
</div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_33 %>">

    <div id="IcmpScanInfo" class="hintbox" runat="server">
      <div class="inner info">
        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_TM0_70, 
                                    "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>") %>
      </div>
    </div>

    <div><!-- ie6 --></div>

  <div class="group-box blue-bg">

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>        
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbDisableAutoScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_34 %>" runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div id=ScanSettings><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbRetainUserData" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_35 %>" runat="server"/>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
              <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_36 %>
            </div></td>
            <td>
                <ipam:TimeSpanEditor Width="168" id="ScanInterval" ValidIf="cbDisableAutoScanning" ActiveWhenChecked="False" TextName="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_290 %>" runat="server" MinValue="00:10:00" MaxValue="7.00:00:00" />
            </td>
        </tr>
    </table></div>

  </div>

</div>

    <div><!-- ie6 --></div>

 <div class="expander" header="<%= String.Format("{0} &lt;a class=&#34;sw-form-helpTip&#34; href=&#34;#&#34; onclick=&#34;window.open('{1}', '_blank');&#34;&gt;{2}&lt;/a&gt;",
                                                 Resources.IPAMWebContent.IPAMWEBDATA_TM0_76, SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPAMPHNeighborScanning"), Resources.IPAMWebContent.IPAMWEBDATA_VB1_29) %>">

    <div id="NeighborScanInfo" class="hintbox" runat="server">
      <div class="inner info">
        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_TM0_74, 
                                    "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>") %>
      </div>
    </div>

    <div><!-- ie6 --></div>

    <div class="group-box blue-bg">
      <IPAM:NeighborScanSettings id="NeighborScan" runat="server"
          FnSupplyServerAddress="function(fnContinue){ fnContinue('Subnet.Edit: SNMP'); }" />
    </div>

</div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_10 %>">
    <div class="group-box blue-bg">
        <IPAM:TransientPeriodSettings id="TransientPeriod" runat="server" />
    </div>
</div>

    <div><!-- ie6 --></div>
</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

<div id="ChromeButtonBar" runat="server" class="sw-btn-bar" style="padding-left: 150px">
    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary" CausesValidation="true" />
    <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary" />
</div>

</asp:Content>

