﻿using System;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;

namespace SolarWinds.IPAM.WebSite
{
    public partial class SubnetResult : CommonPageServices
    {
        private const string PARAM_TASKID = "taskid";
        public GridBox Grid
        {
            get { return grid; }
        }
        public string GridCloumns
        {
            get
            {
                string returnString = string.Empty;
                for (int i = 0; i < grid.Columns.Count;i++ )
                {
                    if (grid.Columns[i].Visible)
                    {
                        returnString += grid.Columns[i].dataIndex.ToUpper() + " Like %searchString% or ";
                    }
                }

                //Remove the last or
               returnString = returnString.Substring(0, returnString.LastIndexOf("or"));
               return returnString;
            }
        }
        public string TaskId
        {
            get
            {
                return base.GetParam(PARAM_TASKID, string.Empty);
            }
        }
        #region Declarations

        private PageDiscoveryWizardEditor Editor { get; set; }

        public string WebParamId
        {
            get
            {
                return WebSecurityHelper.SanitizeHtml(GetParam(DiscoveryWorkflowHelper.PARAM_WEBID, null));
            }
        }

        public string ClusterId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_CLUSTERID, null);
            }
        }

        public string EngineId
        {
            get
            {
                return GetParam(DiscoveryWorkflowHelper.PARAM_ENGINEID, null);
            }
        }

        #endregion // Declarations

        #region Constructors

        public SubnetResult()
        {
            this.Editor = new PageDiscoveryWizardEditor(this);
        }

        #endregion // Constructors

        #region Page Event Handler
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack)
                Editor.AttachCancelButtonEvent(this.imgbCancel);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
           
            if (!IsPostBack)
            {
                InitControls();
            }
            this.grid.Store.WhereClause = " WebID = '" + this.WebParamId + "'";
            GridPreloader1.WhereClause = " WebID = '" + this.WebParamId + "'";
           
        }

        private bool InitControls()
        {
            this.hdWebId.Value = this.WebParamId;            
            return true;
        }

        #endregion

        # region ButtonClick Handler
     

        /// <summary> </summary>
       
        protected void ImgbCancel_Click(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer == false)
            {
                SolarWinds.IPAM.Web.Helpers.ImportHelper.DeleteDiscoveryNodes(this.WebParamId);
            }

            Editor.CancelWizard(null, this.ClusterId);
            Response.Redirect("/Orion/IPAM/SubnetDiscovery/Config/AddNodesToDiscover.aspx");
        }
        #endregion

        #region Wizard

        private bool ValidateUserInput()
        {
            this.Validate();

            return this.IsValid;
        }

        private bool Save()
        {
            if (!ValidateUserInput())
                return false;

            return true;
        }

        #endregion
    }
}
