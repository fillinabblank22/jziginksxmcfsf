﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.DiscoveryHelper;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;

public enum DiscoveryWizardMode
{
    Standard
};

public enum IPAMDiscoveryStep
{
    AddNodes,
    SNMP,
    DiscoverySettings,
    SubnetResults
};

public struct Steps
{
    public IPAMDiscoveryStep StepName;
    public string Pageurl;
    public string ImageName;
    public string HighlightImage;
    public string Title;
}

public partial class DiscoveryProgressIndicator : System.Web.UI.UserControl
{
    private readonly string indicatorImageFolderPath = "~/Orion/IPAM/res/images/progress_indicator/";
    List<Steps> discoveryWizardSteps = new List<Steps>();

    public string WebId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        discoveryWizardSteps.Add(new Steps() { StepName = IPAMDiscoveryStep.AddNodes, Pageurl = "AddNodesToDiscover.aspx", ImageName = "PI_definingscope_off.png", HighlightImage = "PI_definingscope_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_SE_36 });
        discoveryWizardSteps.Add(new Steps() { StepName = IPAMDiscoveryStep.SNMP, Pageurl = "SNMPCredentialList.aspx", ImageName = "PI_review_off.png", HighlightImage = "PI_review_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_SE_37 });
        discoveryWizardSteps.Add(new Steps() { StepName = IPAMDiscoveryStep.DiscoverySettings, Pageurl = "DiscoverySettings.aspx", ImageName = "PI_review_off.png", HighlightImage = "PI_review_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_SE_38 });

        LoadProgressImages();
    }

    #region public properties

    [Browsable(true)]
    [Category("Appearance")]
    public IPAMDiscoveryStep CurrentStep
    {
        set;
        get;
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string NextStep
    {
        get
        {
            int index = discoveryWizardSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index + 1 < discoveryWizardSteps.Count) ? discoveryWizardSteps[index + 1].Pageurl : discoveryWizardSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string PrevStep
    {
        get
        {
            int index = discoveryWizardSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index - 1 >= 0) ? discoveryWizardSteps[index - 1].Pageurl : discoveryWizardSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string HomePage
    {
        get
        {
            return "~/Orion/IPAM/AddSubnetIPAddresses.aspx";
        }
    }
    #endregion

    #region public methods

    public void LoadProgressImages()
    {
        RenderProgressImages();
    }

    #endregion

    #region private methods

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();
        int count = discoveryWizardSteps.Count;

        for (int i = 0; i < count; i++)
        {
            bool nextActive;

            Image imgSeparator = new Image();
            imgSeparator.ID = string.Format("imgSep{0}", i + 1);
            bool isCurrentStep = false;
            string url = "";

            nextActive = (i < count - 1) ? (this.CurrentStep == discoveryWizardSteps[i + 1].StepName) : false;

            if (IsStepActive(this.discoveryWizardSteps[i].StepName.ToString()))
            {
                url = PageDiscoveryWizardEditor.AddParamsToUrl(this.Page,discoveryWizardSteps[i].Pageurl, this.WebId);
            }

            if (this.CurrentStep == discoveryWizardSteps[i].StepName)
            {
                isCurrentStep = true;
                imgSeparator.ImageUrl = string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off");
            }
            else
            {
                imgSeparator.ImageUrl = (nextActive) ? string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on") : string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off");

            }

            HyperLink tb = new HyperLink();
            tb.Text = discoveryWizardSteps[i].Title;
            tb.NavigateUrl = url;
            tb.CssClass = string.Format("PI_{0}", isCurrentStep ? "on" : "off");
            tb.Attributes["automation"] = tb.Text;
            // Text of first step must be move to the right a little 
            if (i == 0)
            {
                tb.Style.Add("padding-left", "10px;");
            }

            phPluginImages.Controls.Add(tb);
            phPluginImages.Controls.Add(imgSeparator);
        }
    }


    private bool IsStepActive(string stepName)
    {
        using (var storage = new DhcpSessionStorage(this.Page, this.WebId))
        {
            return storage[stepName] != null ? Convert.ToBoolean(storage[stepName]) : false;
        }
    }

    #endregion
}