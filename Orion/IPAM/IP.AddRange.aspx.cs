using System;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPAddRange : CommonPageServices
    {
        PageIpRangeEditor editor;
        public IPAddRange() { editor = new PageIpRangeEditor(this); }

        protected void MsgSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            editor.Save(true);
        }
        protected void ClickSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            editor.Save(true);
        }
		       
    }
}
