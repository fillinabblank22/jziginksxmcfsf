﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" 
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_277 %>" CodeFile="IPv6.IPAddress.MultiEdit.aspx.cs" Inherits="IPv6AddressMultiEdit" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.IPv6AddressEditor.ascx" TagName="IPv6AddressEditor" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.Operator"  ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<script type="text/javascript">
    $SW.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
</script>

<IPAMmaster:JsBlock ID="JS" Requires="ext,ext-quicktips,sw-expander.js,sw-expander.js,sw-ipv6-manage.js,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready(function(){
    var stat = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlStatus' ), 'ddlStatus' );
    var type = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlType' ), 'ddlType' );
    var scan = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlScanning' ), 'ddlScanning' );

    $SW.IPv6.CheckBoxInitExt( $SW.nsGet($nsId, 'chbStatus' ), stat);
    $SW.IPv6.CheckBoxInitExt( $SW.nsGet($nsId, 'chbType' ),   type);
    $SW.IPv6.CheckBoxInitExt( $SW.nsGet($nsId, 'chbScanning' ),   scan);

    $SW.IPv6.ddlStatus = stat.getValue();

    var fnToggleArea = function(s) {
        var isAvailable = (s == 'Available');
        var chb = $SW.ns$($nsId, 'chbStatus')[0];
        var div = $('#detailArea');
        if (!chb || !div) return;

        if((chb.checked || !chb.checked) && !isAvailable) 
            div.show(250);
        else
            div.hide(250);
    };

    stat.on( 'select', function(c){ $SW.IPv6.StatusChangedMultiEdit(c,fnToggleArea); });
    $SW.IPv6.StatusChangedMultiEdit(stat,fnToggleArea);

    var opt = {header: $SW.ns$($nsId, 'txtAddressCountLabel') };
    $('#selectedIpList').expander(opt);
    $('.expander').expander();
});

</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
.sw-field-label { word-wrap: break-word; }
.sw-form-disabled { color: #777; }
.sw-form-listheader { font-weight: bold; margin-bottom: 4px; }
#formbox .group-box { margin-bottom: 20px; padding: 8px; 6px; max-width: 876px; }
#formbox .white-bg { background: none repeat scroll 0 0 transparent; }
#formbox .blue-bg { background: none repeat scroll 0 0 #E4F1F8; }
#separator hr { height: 1px; border: none; color: #B5B8C8; background-color: #B5B8C8;}
#selectedIpList li { height: 16px; padding-left: 60px; margin-bottom: 2px; background-position: 36px center; background-repeat: no-repeat;}
</IPAMmaster:CssBlock>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_278 %></div>
    
    
    <asp:ValidationSummary id="valSummary" runat="server"
                           ShowSummary="true"
                           ShowMessageBox="true"
                           DisplayMode="BulletList" />

    <div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_304 %></div>
    
    <asp:Label ID="txtAddressCountLabel" style="display: none;" runat="server" />
    <div id="selectedIpList" collapsed="true" style="display: none;">
        <asp:BulletedList ID="IPv6AddressList" runat="server" />
    </div>
    <div id="separator">
        <hr align="center" />
    </div>
    
    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-checkbox></td>
            <td class=sw-form-col-label style="width: 160px"></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbStatus" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_230 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlStatus" runat="server" />
            </div></td>
        </tr>
        <tr class="sw-form-clue">
            <td colspan="2"></td>
            <td style="text-decoration: underline">
                <asp:HyperLink ID="StatusHelpLink" Target="_blank" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_307 %>" ForeColor="blue" runat="server" />
            </td>
            <td></td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbType" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_308 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlType" runat="server" />
            </div></td>
        </tr>
        <tr class="sw-form-clue">
            <td colspan="2"></td>
            <td style="text-decoration: underline">
                <asp:HyperLink ID="TypeHelpLink" Target="_blank" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_309 %>" ForeColor="blue" runat="server" />
            </td>
            <td></td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbScanning" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_310 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlScanning" runat="server" Width="100%" >
                    <asp:ListItem Value="0" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_311 %>"></asp:ListItem>
                    <asp:ListItem Value="1" Selected="True" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_312 %>"></asp:ListItem>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbComment" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_314 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>
    </table></div>
        
    <div><!-- ie6 --></div>

<div id="detailArea">

  <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_315 %>" style="display: none;">    
    <div><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_316 %></div>
    <div class="group-box blue-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-checkbox></td>
            <td class=sw-form-col-label style="width: 160px; white-space: normal"></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbMachineType" />
            </td>        
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_317 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtMachineType" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbVendor" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_318 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtVendor" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbSystemName" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_355 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSystemName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbSystemDescription" />
            </td>
            <td><div class=sw-field-label style="white-space: normal">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_319 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSystemDescription" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbSystemContact" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_320 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSystemContact" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>        
        <tr>
            <td class=sw-form-col-checkbox>
                <asp:CheckBox runat="server" ID="chbSystemLocation" />
            </td>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_321 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSystemLocation" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td></td>
        </tr>        
    </table></div>
  </div>
        
    <div><!-- ie6 --></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_142 %>" style="display: none;">
        <div class="group-box blue-bg">
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_NodeAttrData" />
        </div>
    </div>
  
    <div><!-- ie6 --></div>
    
</div>

    <asp:CustomValidator ID="SomethingSelectedValidator" runat="server"
        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_322 %>"  Display="None"
        OnServerValidate="OnCheckBoxesServerValidate" />
        
</div>

<div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
    <orion:LocalizableButton runat="server" ID="btnSave" LocalizedText="Save" OnClientClick="if($SW.IsDemoServer) {demoAction('IPAM_Ipv6Subnet_MultiEditIP', null); return false; }" OnClick="ClickSave" DisplayType="Primary" CausesValidation="true" />
    <orion:LocalizableButtonLink runat="server" ID="btnCancel" LocalizedText="Cancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
</div>

</asp:Content>
