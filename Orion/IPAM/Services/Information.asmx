<%@ WebService Language="C#" Class="Information" %>

using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.Web.Script.Services;
using SolarWinds.IPAM.Client;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Master;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Information : System.Web.Services.WebService
{
	private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

	[WebMethod(EnableSession=true)]
	public DataTable Query(string query)
	{
        try
        {
            int retries = 1;
            while (true)
            {
                --retries;
                try
                {
                    DataTable ret = DoQuery(query);
                    return ret;
                }
                catch (FaultException)
                {
                    // FaultException is a subclass of CommunicationException. We want to retry on 
                    // CommunicationException but not FaultException, so we need to catch and rethrow here.
                    throw;
                }
                catch (CommunicationException ex)
                {
                    if (retries >= 0)
                        log.Warn("CommunicationException caught. Retrying...", ex);
                    else
                    {
                        throw;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Error occurred when trying to execute SWQL statement: {0}", query), ex);
            throw;
        }
	}

    [WebMethod(EnableSession = true)]
    public DataTable LoadSubnetTreeData(string query,string unionQuery)
    {
        try
        {
            DataTable ret = this.Query(query);
            if (ret != null && !string.IsNullOrEmpty(unionQuery))
            {
                DataTable unionResult = this.Query(unionQuery);

                if (unionResult != null && unionResult.Rows.Count > 0)
                {
                    int groupid = (int) unionResult.Rows[0]["GroupId"];

                    DataRow[] rows = ret.Select("GroupId = " + groupid);

                    if (rows != null && rows.Length == 0)
                        ret.Merge(unionResult);
                }
            }

            return ret;
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Error occurred when trying to execute SWQL statement: {0}", query), ex);
            throw;
        }
    }

	public DataTable DoQuery(string query)
	{
        using (IpamClientProxy proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.GetProxy())
        {
            return proxy.SwisProxy.Query(query);
        }
	}

    [WebMethod(EnableSession = true)]
    public void GetCount()
    {
        HttpContext obj = this.Context;
        string query = obj.Request.Params["query"];

        if (string.IsNullOrEmpty(query))
            SendFailure("Input SWQL query must not be blank.");
        
        using (IpamClientProxy proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.GetProxy())
        {
            var dt = proxy.SwisProxy.Query(query);
            var iCount = (dt == null) ? 0 : dt.Rows.Count;
            SendSuccess(obj,"GetCount Successful",iCount.ToString());
        }
    }

    [WebMethod(EnableSession = true)]
    public void GetAssociateRecordForAandPtr()
    {
        HttpContext obj = this.Context;
        string recordType = obj.Request.Params["recordType"];
        string dnsRecordId = obj.Request.Params["DnsRecordId"];
        int currentRecordType = 0;
        int currentDnsId = 0;
        
        if (string.IsNullOrEmpty(recordType) || string.IsNullOrEmpty(dnsRecordId))
            SendFailure("Input parameter is blank.");

        if (!int.TryParse(recordType, out currentRecordType) || !int.TryParse(dnsRecordId, out currentDnsId))
            SendFailure("Input parameter is incorrect format.");

        string tempQuery = "SELECT IsNull({0}.DnsRecordId,-1) AS Id from IPAM.DnsRecord as A " +
                           "JOIN IPAM.DnsRecord ptr on A.Name = ptr.Data " +
                           "JOIN IPAM.DnsZone z on (A.DnsZoneId = z.DnsZoneId) " +
                           "JOIN IPAM.DnsZone z1 on (ptr.DnsZoneId = z1.DnsZoneId) " +
                           "WHERE z.NodeId = z1.NodeId and A.[Type] = 1 and ptr.[Type] = 12 and {1}.DnsRecordId = " +
                           currentDnsId.ToString();

        string query = (currentRecordType == 1)
                           ? String.Format(tempQuery, "ptr", "A")
                           : String.Format(tempQuery, "A", "ptr");

        using (IpamClientProxy proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.GetProxy())
        {
            var dt = proxy.SwisProxy.Query(query);
            var iCount = (dt != null && dt.Rows.Count > 0) ? Convert.ToInt32(dt.Rows[0]["Id"]) : -1;
            SendSuccess(obj, "Associated DNS RecordId", iCount.ToString());
        }
    }

    private static void SendFailure(string message)
    {
        var response = HttpContext.Current.Response;
        response.ContentType = "text/plain";

        var sb = new StringBuilder();
        sb.Append("{success:false,message:");
        JsonHelper.Encode(sb, message);
        sb.Append("}");

        response.Write(sb.ToString());
    }

    private static void SendSuccess(string message)
    {
        SendSuccess(HttpContext.Current, message, string.Empty);
    }

    private static void SendSuccess(HttpContext context, string message, string jsondata)
    {
        context.Response.ContentType = "text/plain";

        StringBuilder sb = new StringBuilder();
        sb.Append("{success:true,message:");
        JsonHelper.Encode(sb, message);

        if (string.IsNullOrEmpty(jsondata) == false)
        {
            sb.AppendFormat(",data:{0}", jsondata);
        }

        sb.Append("}");
        context.Response.Write(sb.ToString());
    }
}
