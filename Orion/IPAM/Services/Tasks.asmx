<%@ WebService Language="C#" Class="Tasks" %>
using System;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Web.Common.UITask;
using SolarWinds.IPAM.Web.Common.Utility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Tasks : System.Web.Services.WebService
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    [WebMethod(EnableSession=true)]
    public object Start(string taskname, object options )
    {
        try
        {
            CheckAccess();

            UITaskBase task = UITaskFactory.GetInstance().Create(taskname);
            task.Configure(options);
            UITaskRegistry.GetInstance().AddTask(task);
            task.Start();
            return task.GetStatus();
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Error occurred when trying to start task: {0}", taskname), ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public object Cancel(Guid taskid)
    {
        try
        {
            UITaskRegistry reg = UITaskRegistry.GetInstance();
            UITaskBase task = reg.GetTask(taskid);

            if (task == null)
                return null;

            task.SyncCancel();
            UITaskStatus ret = task.GetStatus();

            return ret;
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Error occurred when trying to cancel task - taskId: {0}", taskid), ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public object Status(Guid taskid, bool? autokill )
    {
        try
        {
            UITaskRegistry reg = UITaskRegistry.GetInstance();
            UITaskBase task = reg.GetTask(taskid);

            if (task == null)
                return null;

            UITaskStatus ret = task.GetStatus();

            if (autokill == null || autokill.Value)
            {
                if (ret.state == UITaskState.Completed ||
                    ret.state == UITaskState.Canceled)
                    reg.KillTask(taskid, true);
            }

            return ret;
        }
        catch (Exception ex)
        {
            log.Error(String.Format("Error occurred when trying to get task status - TaskId: {0}", taskid), ex);
            throw;
        }
    }

    private void CheckAccess()
    {
        if (!AuthorizationHelper.IsUserInRole(AccountRole.Operator))
        {
            throw new UnauthorizedAccessException();
        }
    }
}
