﻿<%@ WebService Language="C#" CodeBehind="~/App_Code/IPv4Provider.cs" Class="IPv4Provider" %>
using System.Linq;
using System;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Helpers;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class IPv4Provider : System.Web.Services.WebService
{
    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public IPv4ValidationResponse ValidateAddress(string addr, string cidr, string subnet, string clusterId)
    {
        var proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.OpenCacheConnection();
        return SubnetDuplicityValidatorV4.ValidateAddress(addr, cidr, subnet, clusterId, proxy );
    }
}