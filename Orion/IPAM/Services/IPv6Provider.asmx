﻿<%@ WebService Language="C#" CodeBehind="~/App_Code/IPv6Provider.cs" Class="IPv6Provider" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Common;
using Solarwinds.IPAM.Common.CommonExt;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Helpers;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class IPv6Provider : System.Web.Services.WebService
{
    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public IPv6ValidationResponse ValidateIPv6Address(string ipv6, string parent, int size, int pid)
    {
        var proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.OpenCacheConnection();
        return SubnetDuplicityValidatorV6.ValidateIPv6Address(ipv6, parent, size, pid, proxy);
    }

    [WebMethod, ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public IPv6ValidationResponse ValidateIPv6Prefix(string ipv6, string parent, GroupNodeType type, int size, int pid, int oid, int clusterId)
    {
        var proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.OpenCacheConnection();
        return SubnetDuplicityValidatorV6.ValidateIPv6Prefix(ipv6, parent, type, size, pid, oid, clusterId, proxy);
    }
}
