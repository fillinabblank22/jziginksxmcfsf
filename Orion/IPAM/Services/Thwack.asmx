<%@ WebService Language="C#" Class="IpamThwack" %>

// NOTE:  This is a copy of Core\Src\Web\Orion\Services\Thwack.asmx.  That version doesn't support
//        other Forums or Content Zones so I had to make my own copy here.  Once that version has
//        that support, this code should be removed and we should use the new code.

using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class IpamThwack : System.Web.Services.WebService
{
    [WebMethod]
    public string GetRecentPosts(int postsCount)
    {
        // The numitems parameter is not currently supported but the thwack team is working on its implementation. 
        // PostItemList posts = GetPostItems(string.Format("http://thwackfeeds.solarwinds.com/products/community/IPAM.aspx?numitems={0}", postsCount));
        PostItemList posts = GetPostItems("http://thwackfeeds.solarwinds.com/products/community/IPAM.aspx");
        if (posts != null && posts.ItemList != null)
        {
            return FormatRecentPostsTable(posts.ItemList, postsCount);
        }

        return GetErrorMessage();
    }

    [WebMethod]
    public string GetRecentlyPostedContent(string contentFolder, int postsCount, DateTime oldestAllowed, string sortBy)
    {
        string url = String.Format("http://thwack.com/productfeeds/xmlfeed.aspx?showfolder={0}", Uri.EscapeUriString(contentFolder));

        PostItemList posts = GetPostItems(url);
        if (posts != null && posts.ItemList != null)
        {
            // filter older items than allowed
            posts.ItemList.RemoveAll(delegate(PostItem item) { return item.PubDate < oldestAllowed; });
            // sort items
            if (String.Equals(sortBy, "Title", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByTitle);
            }
            else if (String.Equals(sortBy, "Views", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByViews);
            }
            else if (String.Equals(sortBy, "Downloads", StringComparison.OrdinalIgnoreCase))
            {
                posts.ItemList.Sort(SortByDownloads);
            }
            else
            {
                posts.ItemList.Sort(SortByDateTime);
            }
            // format the result table
            return FormatRecentlyPostedContentTable(posts.ItemList, postsCount);
        }
        return GetErrorMessage();
    }    
    
    #region Query methods
    private static PostItemList GetPostItems(string url)
    {
        ConfigurePostsManager();
        try
        {
            return PostsManager.GetPostItemsList(url);
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    private static void ConfigurePostsManager()
    {
        try
        {
            if (ConfigurationManager.AppSettings["proxyAvailable"].ToLowerInvariant().Equals("true", StringComparison.InvariantCulture))
            {
                PostsManager.ProxyAvailable = true;
                PostsManager.SetProxyInfo(ConfigurationManager.AppSettings["proxyAddress"], Convert.ToInt32(ConfigurationManager.AppSettings["proxyPort"]),
                    ConfigurationManager.AppSettings["userName"], ConfigurationManager.AppSettings["password"]);
                return;
            }
        }
        catch { }
        // no need to handle this one, just set proxy to false
        PostsManager.ProxyAvailable = false;
    } 
    #endregion

    #region Formatting methods
    private static string GetErrorMessage()
    {
        return String.Format("<div class=\"ThwackConnectError\">{0}</div>", Resources.IPAMWebContent.IPAMWEBDATA_VB1_762);
    }

    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Class,"NeedsZebraStripes");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);
        
        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

           
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

        
            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);
            
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }

    private static string FormatRecentlyPostedContentTable(List<PostItem> posts, int count)
    {
        StringWriter content = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        writer.Write("NAME&nbsp;");
        writer.RenderEndTag();
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write("DATE&nbsp;");
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write("DOWNLOADS&nbsp;");
        writer.RenderEndTag();

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "7pt");
        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.Write("VIEWS&nbsp;");
        writer.RenderEndTag();

        writer.RenderEndTag();   

                
        int realCount = Math.Min(count, posts.Count);
        for (int i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "#e4f1f8");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(!String.IsNullOrEmpty(posts[i].Title) ? posts[i].Title : "-");
            writer.RenderEndTag();
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.WriteEncodedText(posts[i].PubDate.ToShortDateString());
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "right");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(posts[i].DownloadCount);
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, "right");
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "#5C6972");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(posts[i].ViewCount);
            writer.Write("&nbsp;");
            writer.RenderEndTag();

            writer.RenderEndTag();
        }
        writer.RenderEndTag();
        return content.ToString();
    }
    #endregion   
    
    #region Posts sorting methods
    private static int SortByDateTime(PostItem x, PostItem y)
    {
        return y.PubDate.CompareTo(x.PubDate);
    }

    private static int SortByTitle(PostItem x, PostItem y)
    {
        return x.Title.CompareTo(y.Title);
    }

    private static int SortByViews(PostItem x, PostItem y)
    {
        return y.ViewCount.CompareTo(x.ViewCount);
    }

    private static int SortByDownloads(PostItem x, PostItem y)
    {
        return y.DownloadCount.CompareTo(x.DownloadCount);
    } 
    #endregion
 
}
