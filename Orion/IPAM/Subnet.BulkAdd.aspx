<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Subnet.BulkAdd.aspx.cs" Inherits="SubnetBulkAdd"
 Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_8 %>" %>
 
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHWindowSubnetBulkAdd" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<script type="text/javascript">
function show(id)
{
	var item = document.getElementById(id);
	item.style.display = "";
}

function hide(id)
{
	var item;
	if (id.style)
		item = id;
	else
		item = document.getElementById(id);
	item.style.display = "none";
}

function IsPostback()
{
    return <%= IsPostBack.ToString().ToLower() %>;
};
</script>

<IPAMmaster:CssBlock runat="server">
.formbox { margin: 10px 10px 0px 0px; padding: 1em; border: 1px solid #dfdfde; background: #fff; }
#selectedsubnetlist ul { padding: 0px 0px 10px 20px; }
#selectedsubnetlist li { list-style-type: disc; line-height: 150%; }
.sw-form-cols-normal td.sw-form-col-label { width: 140px; }
.sw-form-cols-normal td.sw-form-col-control { width: 400px; }

</IPAMmaster:CssBlock>

<div style="margin-left: 10px; margin-right: 10px;">

    <table id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb">
        <div style="margin: 15px 0px 11px 0px;"><a href="/Orion/IPAM/Subnets.aspx"><%= IPAMWebContent.IPAMWEBDATA_AK1_4%></a></div>
	    <h1><%= Page.Title %></h1>
    </td></tr>
    </table>

   
    <asp:Wizard ID="AddSubnetsWizard" runat="server" DisplaySideBar="false">
        <HeaderTemplate>
            <IPAMui:ValidationIcons runat="server" />
        </HeaderTemplate>
        <WizardSteps>
            <asp:WizardStep ID="CreateSubnets" Title="Select Subnets to Create">

                <IPAMmaster:JsBlock Requires="ext, sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
                  $SW.subnetwizard={};
                  
                  $SW.subnetwizard.IsSubnetlistempty = function()
                  {
                      var sel = Ext.getCmp('grid').getSelectionModel();
                      var c = sel.getCount();
                      return c < 1;
                  };
                  
                  $SW.subnetwizard.createsubnetlist = function()
                  {
                            
                   var ok = Page_ClientValidate();
                   if(ok)
                   {
                         if( !$SW.subnetwizard.IsSubnetlistempty())
                         {
                                        
                           var sel = Ext.getCmp('grid').getSelectionModel();
                           var items = sel.getSelections();
                           var listsubnet = [];
                                            
                           Ext.each(items,function(o){
                           listsubnet.push(o.data.IPCIDR);
                           });
                                            
                           var hfSubnetListobj = $SW.ns$($nsId,'hfSubnetList')[0];
                           hfSubnetListobj.value = listsubnet.join('|');
                                        
                         }else
                         {
                           Ext.Msg.show({
                           title:' @{R=IPAM.Strings;K=IPAMWEBJS_TM0_1;E=js}',
                           msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_2;E=js}',
                           buttons: Ext.Msg.OK,
                           icon: Ext.MessageBox.ERROR
                           }); 
                           return false;  
                         }
                                
                       return true;
                   }
                    return false;
                  };
                </IPAMmaster:JsBlock>
                <asp:HiddenField ID="hfSubnetList" runat="server" />

                <div id="CreateSubnetsformbox" class="formbox" style="width: 640px;">
                
                    <div visible="true" runat="server">
                        <%= IPAMWebContent.IPAMWEBDATA_TM0_10 %>
                    </div>
                
                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_11 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <asp:TextBox ID="txtSource" TextMode="MultiLine"  Wrap="false" Width="500px"  Height="300px" runat="server"></asp:TextBox>
                                    </div>
                                </td>
                                <td>
                                     <asp:RequiredFieldValidator ID="SubnteSource" runat="server"
                                     ControlToValidate="txtSource"
                                     Display="Dynamic"
                                     ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_12 %>"/>
                                </td>
                            </tr>
                            </table>
                            
                            <table class="" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 250px" >
                                    <div class="sw-form-item">
                                        <orion:LocalizableButton runat="server" ID="btnrefreshsubnetlist" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_15 %>" OnClick="Parse_Click" DisplayType="Secondary" />
                                    </div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbIgnoreExistSubnet" Checked="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_13 %>" runat="server" />
                                        <asp:Label ID="lblOverlappedSubnetsInfo" class="sw-field-label" runat="server"><%= IPAMWebContent.IPAMWEBDATA_TM0_14 %></asp:Label>                                        
                                    </div>
                                </td>
                            </tr>
                        </table>
                        
                        <div>
                        <asp:Label runat="server" ID="ParsedCountLabel"></asp:Label>
                        </div>
                     
                        
                        <div id="gridframe" style="margin: 16px 0px 0px 0px;">
                            <!-- -->
                        </div>
                        <IPAMlayout:GridBox runat="server"
                        width="640"
                        EnableHeaderMenu="false"
                        ID="grid"
                        maxBodyHeight="500" 
                        autoFill="false"
                        autoScroll="true"
                        emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_16 %>"
                        borderVisible="true" 
                        isFramed="false" 
                        RowSelection="false"
                        clientEl="gridframe" 
                        deferEmptyText="false"
                        StripeRows="true"
                        listeners="{ beforerender: $SW.ext.gridSnap }">
                        <Columns>
                            <IPAMlayout:GridColumn id="GridColumn1" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_17 %>" dataIndex="SubnetStart" width="150" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn2" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_18 %>" dataIndex="SubnetStartEnd" width="150" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn5" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_19 %>" dataIndex="CIDR" width="80" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn3" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_20 %>" dataIndex="SubnetMask" width="150" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn4" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_21 %>" dataIndex="IPCIDR" width="150" isFixed="true" isHidden="true" runat="server" />
                        </Columns>
                        <Store ID="store"  dataId="PRELOAD"  autoLoad="true" runat="server">
                            <Fields>
                               <IPAMlayout:GridStoreField ID="GridStoreField1" dataIndex="SubnetStart" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField2" dataIndex="SubnetStartEnd" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField5" dataIndex="CIDR" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField3" dataIndex="SubnetMask" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField4" isKey="true" dataIndex="IPCIDR" runat="server" />
                            </Fields>
                        </Store>
                        </IPAMlayout:GridBox>
                        <IPAM:GenericGridPreloader ID="PreLoader" SourceID="Grid" runat="server" />
                    </div>
                    
                </div>
                
                <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
            </asp:WizardStep>
            <asp:WizardStep ID="SubnetsProperties" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_28 %>">
                <IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
                $SW.backroundSubnetsCreationInfoMsg = function(obj)
                {
                    if($('#isDemoMode').length > 0) {demoAction('IPAM_Subnet_BulkAdd_Wizard', null); return false; }
                    alert('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_3;E=js}')
                    return true;
                };
                </IPAMmaster:JsBlock>
                
                <div id="SubnetsPropertiesformbox" class="formbox" style="width:510px">

                <div id="hints2" visible="false" runat="server"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                  <tr class="sw-form-cols-normal">
                   <td class="sw-form-col-label"></td>
                   <td class="sw-form-col-control"></td>
                   <td class="sw-form-col-comment"></td>
                  </tr>
                  <tr>
                    <td colspan="2"><div class="sw-form-subheader">
                        <%= IPAMWebContent.IPAMWEBDATA_TM0_22 %>
                    </div></td>
                  </tr>
                  <tr>
                    <td colspan="3">
                  <ul style="padding: 10px 0px 10px 15px;">
                    <li style="list-style-type: disc; line-height: 150%;"><%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_23, blSubnets.Items.Count) %></li>
                    <li style="list-style-type: disc; line-height: 150%;"><%= IPAMWebContent.IPAMWEBDATA_TM0_24 %></li>
                  </ul>
                    </td>
                  </tr>
                </table></div>

                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_25 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <div class="sw-form-subheader">
                                            <img id="open" alt="<%= IPAMWebContent.IPAMWEBDATA_TM0_38 %>" src="/Orion/images/Button.Expand.gif" onclick="hide(this);show('close');show('selectedsubnetlist')" />
                                            <img id="close" alt="<%= IPAMWebContent.IPAMWEBDATA_TM0_39 %>" style="display: none" src="/Orion/images/Button.Collapse.gif" onclick="hide(this);show('open');hide('selectedsubnetlist')" />
                                            <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_26, blSubnets.Items.Count) %>
                                        </div>
                                        <div id="selectedsubnetlist" style="display: none">
                                            <asp:BulletedList ID="blSubnets" runat="server"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbMoveIntoExistSupernets" Checked="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_27 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader">
                                        <%= IPAMWebContent.IPAMWEBDATA_TM0_28 %>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_AK1_147%> </div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                    <asp:TextBox ID="txtComment" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item sw-form-clue"><%= IPAMWebContent.IPAMWEBDATA_VB1_184%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_VB1_185%></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                     <asp:TextBox ID="txtVLAN" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_VB1_186 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:TextBox ID="txtLocation" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_33 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbDisableAutoScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_34 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbRetainUserData" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_35 %>" runat="server"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="ScanSettings">
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label">
                                        <%= IPAMWebContent.IPAMWEBDATA_TM0_36 %>
                                    </div>
                                </td>
                                <td colspan="2">
                                    <div class="sw-form-item" id='scanSlider'>
                                        <asp:TextBox ID="txtScanInterval" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" Text="240" runat="server" />
                                        <span id="lblScanInterval"><%= IPAMWebContent.IPAMWEBDATA_AK1_20%></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <!-- ie6 -->
                </div>
            </asp:WizardStep>
        </WizardSteps>
        <StartNavigationTemplate>
            <div id="buttonbarStart" runat="server" class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="StartNext" LocalizedText="Next" CommandName="MoveNext" OnClientClick="return $SW.subnetwizard.createsubnetlist();" CausesValidation="true" DisplayType="Primary" />
                <orion:LocalizableButtonLink runat="server" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary" />
            </div>
        </StartNavigationTemplate>
        <FinishNavigationTemplate>
            <div id="buttonbarFinish" runat="server" class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="FinishBack" LocalizedText="Back"  DisplayType="Secondary" AlternateText="Back"  CommandName="MovePrevious"/>
                <orion:LocalizableButton runat="server" ID="Done" LocalizedText="Done" DisplayType="Primary" CommandName="MoveComplete" OnClientClick="return $SW.backroundSubnetsCreationInfoMsg();" OnClick="Done_Click" />
                <orion:LocalizableButtonLink runat="server" LocalizedText="Cancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
            </div>
        </FinishNavigationTemplate>
    </asp:Wizard>
   </div>
 </div>
</asp:Content>


