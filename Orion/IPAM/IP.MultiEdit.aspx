<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True" Inherits="SolarWinds.IPAM.WebSite.IPMultiEdit"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_302%>" CodeFile="IP.MultiEdit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="orion" TagName="ThreeStateCheckBox" Src="~/Orion/Controls/ThreeStateCheckBox.ascx" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.Operator" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation=true OnMsg="MsgSave" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock ID="CssBlock1" Requires="sw-ipv6-manage.css" runat="server">
    .helpTip { text-decoration:underline; font-style: italic}
    .helpTip tr { padding: 20px }
    .paddedBottom { padding-bottom: 15px; }

    .editItems { background: #e4f1f8; max-width: 876px; padding: 8px 6px; margin-bottom: 20px; }  
    .editItemsWhite { max-width: 876px; padding: 8px 6px; margin-bottom: 20px; }  

    .edit-combo { width: 340px; }

    #selectedIpList ul { padding: 0px 0px 10px 20px; }
    #selectedIpList li { list-style-type: disc; line-height: 150%; }
    .sw-field-label { word-wrap: break-word; }
    .sw-form-disabled { color: #777; }
    .sw-form-listheader { font-weight: bold; margin-bottom: 4px; }
    #formbox .group-box { margin-bottom: 20px; padding: 8px; 6px; max-width: 876px; }
    #formbox .white-bg { background: none repeat scroll 0 0 transparent; }
    #formbox .blue-bg { background: none repeat scroll 0 0 #E4F1F8; }
    #separator hr { height: 1px; border: none; color: #B5B8C8; background-color: #B5B8C8;}
    #selectedIpList li { height: 16px; padding-left: 60px; margin-bottom: 2px; background-position: 36px center; background-repeat: no-repeat;}
</IPAMmaster:CssBlock>
    
<script type="text/javascript">
    
    $SW.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
    
    function show(id)
    {
        var item = document.getElementById(id);
        item.style.display = "";
    }

    function hide(id)
    {
        var item;
        if (id.style)
            item = id;
        else
            item = document.getElementById(id);
        item.style.display = "none";
    }

    function IsVisible(obj) {

        if (obj.style.display == 'none')
            return false;

        obj = obj.parentElement;
        while (obj) {
            if (obj.style && obj.style.display == 'none')
                return false;

            obj = obj.parentElement;
        }

        return true;
    }

</script>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js,sw-expander.js," Orientation=jsPostInit runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });

    function transform(id,newid){
        var item = $('#'+id)[0];
        var ops = {
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        };

        if( newid ) ops.id = newid;
        var converted = new Ext.form.ComboBox(ops);
        
        converted.on( "expand", function(me){
            var tr = me.trigger.getBox(),
                bo = me.list.getBorderWidth("lr"),
                b1 = Ext.fly(me.container.dom).getBox(),
                w1 = tr.x + tr.width - b1.x;

            me.list.setStyle('width', ''+(w1-bo)+'px');
            me.innerList.setStyle('width','auto');
        });
        
        return converted;
    };
    
    $ToggleAreaShouldBeHidden = false;
    
    var ToggleArea = function(c,r){
    
        var combo = $SW.ns$($nsId, 'ddlStatus')[0];
        var checkBox = $SW.ns$($nsId, 'chbEditStatus')[0];
        
        var selectedAvailable = combo.value == 'Available';
        var checked = checkBox.checked;
        
        var showUserDetails = checked && !selectedAvailable
            || !checked && !$ToggleAreaShouldBeHidden;
    
        if( !showUserDetails ) 
            $('#detailArea').hide(250);
        else 
            $('#detailArea').show(250);
    };
    
    $(document).ready(function(){
    
        var ddlStatus = $SW.ns$($nsId, 'ddlStatus')[0];
    
        if (ddlStatus.value == 'Available'){
            $SW.CustomPropertyFormDisable(true, true, 'cpEdit');
        }

        var statusCheckBox = $('#' + $SW.nsGet($nsId, 'chbEditStatus' ))[0];
        $(statusCheckBox).bind('click', ToggleArea);

        ToggleArea();
    
        $SW.previousStatus = ddlStatus.value;
   
        $(ddlStatus).change(function(event){
        
            var c = $SW.ns$($nsId, 'ddlStatus')[0];

            if( c.value == 'Available' )
            {
                if( confirm( "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_167;E=js}" ) )
                {
                    $SW.CustomPropertyFormDisable(true, true, 'cpEdit');
                }
                else
                {
                    if ( $SW.previousStatus )
                    {
                        c.value = $SW.previousStatus;
                    }
                
                    return false;
                }
            }
            else
            {
                $SW.CustomPropertyFormDisable(false, false, 'cpEdit');
            }

            $SW.previousStatus = c.value;
            ToggleArea();
            return true;
        });
    
        $('.expander').expander();
    
    });

</IPAMmaster:JsBlock>

<IPAMmaster:JsBlock ID="jsBlock" Orientation="jsPostInit" runat="server"></IPAMmaster:JsBlock>

<IPAMui:ValidationIcons runat=server />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_303  %></div>

    <div class="sw-form-heading"><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_304 %></h2></div>

    <div class="sw-form-item">
        <div class="sw-form-subheader">
            <img id="open" alt="open" src="/Orion/images/Button.Expand.gif" onclick="hide(this);show('close');show('selectedIpList')" />
            <img id="close" alt="close" style="display: none" src="/Orion/images/Button.Collapse.gif" onclick="hide(this);show('open');hide('selectedIpList')" />
            <asp:Label ID="AddressCountLabel" runat="server"></asp:Label><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_305, "") %>
        </div>
        <div id="selectedIpList" style="display:none"> 
            <asp:BulletedList ID="blAddresses" runat="server"/>
        </div>
    </div>

    <div style="height:1px; border:none;" >
    <hr style="color:#B5B8C8; background-color:#B5B8C8; height:1px; " align="center" />
    </div>

    <div>
     <div class="editItemsWhite     ">
    <table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-checkbox></td>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
       
        <tr><td colspan=2>&nbsp;</td></tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditStatus" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_306 %></div></td>
            <td><div class="sw-form-item x-form-field-wrap edit-combo"><asp:DropDownList ID="ddlStatus" runat="server" Width="100%" /></div></td>
        </tr>
        <tr class="helpTip">
            <td class="paddedBottom" colspan="2"></td>
            <td class="paddedBottom helpTip"><asp:HyperLink ID="StatusHelpLink" Target="_blank" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_307 %>" runat="server"></asp:HyperLink></td>
            <td></td>
        </tr>
        <tr>
            <td class="sw-form-col-checkbox"><asp:CheckBox runat="server" ID="chbEditType" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_308 %></div></td>
            <td><div class="sw-form-item x-form-field-wrap edit-combo"><asp:DropDownList ID="ddlType" runat="server" Width="100%" /></div></td>
        </tr>
         <tr style="padding-bottom:20px;" >
            <td class="paddedBottom" colspan="2"></td>
            <td class="paddedBottom helpTip" style="padding-bottom:20px;"><asp:HyperLink ID="TypeHelpLink" Target="_blank" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_309 %>" runat="server"></asp:HyperLink></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditScanning" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_310 %></div></td>
            <td><div class="sw-form-item x-form-field-wrap edit-combo">
                <asp:DropDownList ID="ddlScanning" runat="server" Width="100%" >
                    <asp:ListItem Value="0" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_311 %>"></asp:ListItem>
                    <asp:ListItem Value="1" Selected="True" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_312 %>"></asp:ListItem>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr><td colspan=4>&nbsp;</td></tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditComment" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_314 %></div></td>
            <td><div class="sw-form-item x-form-field-wrap edit-combo"><asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" Width="98%"/></div></td>
        </tr>
        </table>
        </div>
        
<div id="detailArea">

  <div><!-- ie6 --></div>
  
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_315 %>" style="display: none;">    
    <div><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_316 %></div>
        <div class="editItems"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-checkbox></td>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditMachineType" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_317 %></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtMachineType" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditVendor" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_318 %></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtVendor" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditSysName" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_355 %></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtSysName" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditSysDescription" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_319 %></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtSysDescr" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditSysContact" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_320 %></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtSysContact" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        <tr>
            <td class=sw-form-col-checkbox><asp:CheckBox runat="server" ID="chbEditSysLocation" /></td>
            <td><div class=sw-field-label><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_321%></div></td>
            <td><div class=sw-form-item><asp:TextBox ID="txtSysLocation" CssClass="x-form-text x-form-field" runat="server" /></div></td>
            <td></td>
        </tr>
        </table>
        </div>
    
    </div>
        
    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_142 %>" style="display: none;">
        <div class="editItems">
            <div id="cpEdit">
                <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_NodeAttrData" />
            </div>
        </div>
    </div>

    <div><!-- ie6 --></div>
</div>

    <asp:CustomValidator ID="SomethingSelectedValidator" runat="server" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_322 %>" 
    Display="None" ClientValidationFunction="$SW.ValidateNotAvailableStatus" OnServerValidate="SomethingSelectedValidator_OnServerValidate"/>

</div>
    
<asp:ValidationSummary id="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 100px;"></td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClientClick="if($SW.IsDemoServer) {demoAction('IPAM_Ipv4Subnet_MultiEditIP', null); return false; }" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl="/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>


</asp:Content>
