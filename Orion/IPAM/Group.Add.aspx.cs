using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class GroupAdd : CommonPageServices
    {
        private readonly PageSubnetEditor editor;
        private readonly VrfGroupExistsHelper vrfHelper;

        public GroupAdd()
        {
            editor = new PageSubnetEditor(this, true);
            vrfHelper = new VrfGroupExistsHelper();
            editor.SaveCustomProperties = SaveCustomProperties;
            editor.GetCustomProperties = GetCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties group)
        {
            CustomPropertyRepeater.GetChanges(group);
        }

        public void GetCustomProperties(ICustomProperties group, IList<int> ids)
        {
            CustomPropertyRepeater.RetrieveCustomProperties(group, ids);
        }

        protected void OnNewVrfGroupNameExists(object source, ServerValidateEventArgs args)
        {
            if (editor.GroupType == GroupNodeType.Root)
            {
                args.IsValid = vrfHelper.CheckVrfGroupNameNotExists(this.txtDisplayName.Text, args.Value, false);
            }
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }
    }
}
