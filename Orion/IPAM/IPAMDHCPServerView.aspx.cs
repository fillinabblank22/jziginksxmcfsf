﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.Orion.Web.UI;

public partial class Orion_IPAM_IPAMDHCPServerView : OrionView, IProvidePrincipal
{
    private const string NavigateUrlFormat = "{0}/{1}";

    #region Properties

    protected DhcpServer DhcpServer { get; set; }

    public override string ViewType
    {
        get { return "IPAM DHCP Server View"; }
    }

    #endregion // Properties

    #region Events

    protected override void OnPreLoad(EventArgs e)
    {
        // handle given NetObject
        IpamDhcpServer netObject = this.NetObject as IpamDhcpServer;
        if ((netObject != null) && (netObject.DhcpServer != null))
        {
            this.DhcpServer = netObject.DhcpServer;
        }

        // set page controls
        this.Title = this.ViewInfo.ViewTitle;
        this.DhcpServerName.Text = this.DhcpServer.FriendlyName;
        this.DhcpServerLinkURL.NavigateUrl = IpamSiteMapRenderer.GetNavigateDhcpServerUrl(this.DhcpServer);

        // set help link
        ((Orion_View)this.Master).HelpFragment = "OrionIPAMDhcpServer";

        // set resource container
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnPreLoad(e);
    }

    protected void OnInitDhcpServerSiteMapPath(object sender, EventArgs e)
    {
        if (false == CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new IpamSiteMapRenderer();

            KeyValuePair<string, string>[] data = new KeyValuePair<string, string>[4];

            // dhcp server net object
            data[0] = new KeyValuePair<string, string>(
                IpamSiteMapRenderer.NetObjectKey,
                Request.Form[IpamSiteMapRenderer.NetObjectKey] ?? string.Empty);

            // subnet data
            IpamDhcpServer server = this.DhcpServerResHost.DhcpServer;
            data[1] = new KeyValuePair<string, string>(
                IpamSiteMapRenderer.DhcpServerKey, server.Name);

            renderer.SetUpData(data);

            this.DhcpServerSiteMapPath.SetUpRenderer(renderer);
        }
    }

    public IPrincipal GetPrincipal()
    {
        return AuthorizationHelper.GetPrincipal();
    }

    #endregion // Events
}