﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class IPv6GlobalPrefixEdit : CommonPageServices
{
    #region Constructors

    private PageIPv6PrefixEditor Editor { get; set; }
    public IPv6GlobalPrefixEdit()
    {
        this.Editor = new PageIPv6PrefixEditor(this, false, GroupNodeType.GlobalPrefix);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.PrefixId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            PrefixAggregate prefix = this.Editor.RetrievePrefixObject();
            InitControls(prefix);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(PrefixAggregate prefix)
    {
        bool hasChilds = this.Editor.PrefixObjectChildsCount(prefix) > 0;

        if (this.txtName != null)
        {
            this.txtName.Text = prefix.FriendlyName;
        }
        if (this.txtDescription != null)
        {
            this.txtDescription.Text = prefix.Comments;
        }

        // init global prefix address
        string fnAddressListener = string.Empty;
        if (this.PrefixAddress != null)
        {
            this.PrefixAddress.ReadOnly = hasChilds;
            this.PrefixAddress.Init(
                prefix.FullPrefix,
                prefix.GroupType,
                prefix.FullSize,
                null, prefix.ParentId, prefix.GroupId,
                Editor.ClusterId);
            fnAddressListener = this.PrefixAddress.Listener;
        }

        // init prefix size control
        if (this.PrefixSize != null)
        {
            this.PrefixSize.ReadOnly = hasChilds;
            this.PrefixSize.Init(
                PageIPv6EditorHelper.MinPrefixSize,
                PageIPv6EditorHelper.MaxPrefixSize,
                prefix.Size,
                fnAddressListener);
        }

        CustomPropertyRepeater.RetrieveCustomProperties(prefix, new List<int>(){ prefix.GroupId });
        
        if (this.AccountRolesBox != null)
        {
            this.AccountRolesBox.GroupId = prefix.GroupId;
        }
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        PrefixAggregate prefix = this.Editor.RetrievePrefixObject();
        if (prefix != null)
        {
            PrefixAggregate updatedPrefix = UpdateObject(prefix);
            this.Editor.Store(updatedPrefix);
        }
    }
    private PrefixAggregate UpdateObject(PrefixAggregate globalPrefix)
    {
        globalPrefix.GroupType = GroupNodeType.GlobalPrefix;

        if (this.txtName != null)
        {
            globalPrefix.FriendlyName = this.txtName.Text;
        }
        if (this.txtDescription != null)
        {
            globalPrefix.Comments = this.txtDescription.Text;
        }

        // NOTE: For global prefix case, the value and size
        // are identical as FullPrefix and FullSize.
        if (this.PrefixAddress != null)
        {
            string prefix = this.PrefixAddress.IPv6Value;
            if (prefix.IndexOf("::") < 0) prefix += "::";

            globalPrefix.FullPrefix = prefix;
            globalPrefix.Value = prefix;
        }
        if (this.PrefixSize != null)
        {
            int size = this.PrefixSize.PrefixSizeValue;
            globalPrefix.FullSize = size;
            globalPrefix.Size = size;
        }

        CustomPropertyRepeater.GetChanges(globalPrefix);

        return globalPrefix;
    }

    #endregion // Methods
}
