using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Master;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DefaultMaster : MasterPage
    {
		public bool AutoSyncCopyright
        {
            get
            {
                if (LicenseStatus == null)
                    return false;
                return LicenseStatus.Visible;
            }

            set
            {
                if (LicenseStatus != null)
                    LicenseStatus.Visible = value;
            }
        }
    }
}
