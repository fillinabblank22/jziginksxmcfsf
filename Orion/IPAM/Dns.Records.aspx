﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" CodeFile="Dns.Records.aspx.cs"
    Inherits="SolarWinds.IPAM.WebSite.DNSRecords" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_4 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDNSZoneRecords" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    
<ipam:AccessCheck RoleAclOneOf="IPAM.Any" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"  runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
.sw-grid-buttons { text-align: right; }
.sw-grid-cancel { margin-right: 6px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
.warningbox a { text-decoration: underline; }
.x-grid3-hd-checker {display:none;}  
</IPAMmaster:CssBlock>

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock Orientation="jsPostInit" runat="server"
    Requires="ext,ext-quicktips,sw-helpserver,sw-ux-wait.js,sw-dns-constants.js,sw-dns-manage.js,sw-dnsrecord-manage.js,sw-ux-ext.js,ext-ux,sw-dhcp-manage.js"/>

<script type="text/javascript">
    $SW.IPAM.DisableDNSServerAutoScanning = "<%=(this.DnsServer.DisableAutoScanning == true)%>";
</script>

<IPAMlayout:DialogWindow ID="DnsAddEditWindow" jsID="dnsRecordsWindow" 
        Name="dnsRecordsWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_825 %>"
        height="350" width="650" runat="server">
    <Buttons>
        <IPAMui:ToolStripButton ID="dnsRecordSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
            <handler>function(){ var val =  $SW.msgq.DOWNSTREAM( $SW['dnsRecordsWindow'], 'dialog', 'save' );  }</handler> 
        </IPAMui:ToolStripButton>
        <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_301 %>"  cls="sw-enableonload" runat="server">
            <handler>function(){ $SW['dnsRecordsWindow'].hide(); }</handler>
        </IPAMui:ToolStripButton>
    </Buttons>
    <Msgs>
        <IPAMmaster:WindowMsg ID="editCredReady" Name="ready" runat="server">
            <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['dnsRecordsWindow'] ,info); }</handler>
        </IPAMmaster:WindowMsg>    
        <IPAMmaster:WindowMsg ID="editCredClose" Name="close" runat="server">
            <handler>function(n,o){ $SW['dnsRecordsWindow'].hide(); $SW.IPAM.DnsRecordGridRefresh(); }</handler>
        </IPAMmaster:WindowMsg>   
    </Msgs>
</IPAMlayout:DialogWindow>

    <div class="titleTable">
        <div style="margin-left: 10px;">
        <br /><span>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <ul class="breadcrumb">
        <li class="bc_item"><a href="/api2/ipam/ui/zones" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %>" 
        class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a><img 
        src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"/><ul 
            id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %></ul></li><% 
if (this.IsZoneSet) {
%><li class="bc_item"><a href="/api2/ipam/ui/dns/node/<%=this.DnsServer.NodeId %>" 
    title="<%=this.DnsServer.FriendlyName %>" class="bc_link"><%=this.DnsServer.FriendlyName %></a><img 
    src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"><ul 
        id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;"><li class="bc_submenuitem"><a
        href="/api2/ipam/ui/dns/node/<%=this.DnsServer.NodeId %>" class="bc_submenulink"><%=this.DnsServer.FriendlyName %></a></li>
</ul></li><li class="bc_item"><a href="/api2/ipam/ui/zone/<%=this.DnsZone.GroupId %>" 
    title="<%=this.DnsZone.FriendlyName %>" class="bc_link"><%=this.DnsZone.FriendlyName%></a><img 
    src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"><ul 
        id="Ul1" class="bc_submenu" style="overflow: auto; height: auto;"><li class="bc_submenuitem"><a
        href="/api2/ipam/ui/zone/<%=this.DnsZone.GroupId %>" class="bc_submenulink"><%=this.DnsZone.FriendlyName%></a></li>
</ul></li><% 
 }
%></ul><%
   }
%>
        </span></div>
        <h1><%=Page.Title%></h1>
    </div>

    <div style="margin: 10px;">
        <asp:HiddenField runat="server" ID="hZoneId" Value="-1" />
        <asp:HiddenField runat="server" ID="hZoneName" Value="" />
        <div id="gridframe" style="margin: 0px 0px 0px 0px;"/>
    </div>

    <IPAMlayout:GridBox runat="server" 
        width="1200" 
        height = "600"
        autoHeight="false" 
        autoFill="false"
        autoScroll="true"             
        ID="DnsRecordsGrid" 
        emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_298 %>"
        borderVisible="true" 
        RowSelection="false" 
        clientEl="gridframe" 
        deferEmptyText="false"
        StripeRows="true" 
        UseLoadMask="True" 
        listeners="{ statesave: $SW.IPAM.DnsRecordsStateSave }"  
        SelectorName="selector">
        <TopMenu>
            <IPAMui:ToolStripMenu id="gridAddTab" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_85 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_161 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" runat="server">
                <IPAMui:ToolStripMenuItem id="btnAddDnsServer" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_826 %>" runat="server">                           
                        <handler>function(that) { var id =$("[id$=_hZoneId]").val(); var name =$("[id$=_hZoneName]").val(); $SW.IPAM.DnsRecordAdd(that,id,name); }</handler>                             
                </IPAMui:ToolStripMenuItem>
            </IPAMui:ToolStripMenu>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripItem id="gridEditTab" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_163 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_828 %>" iconCls="mnu-edit" RoleAclOneOf="IPAM.PowerUser"  runat="server">
                <handler>$SW.IPAM.DnsRecordEdit</handler>
            </IPAMui:ToolStripItem>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripItem id="gridDeleteTab" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_169 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_829 %>" iconCls="mnu-delete" RoleAclOneOf="IPAM.PowerUser"  runat="server">                   
                    <handler>$SW.IPAM.DeleteRecord</handler>                    
            </IPAMui:ToolStripItem>
        </TopMenu>
        <PageBar pageSize="100" displayInfo="true"   />
        <Columns>
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_94 %>" width="200" isSortable="true" dataIndex="Name" runat="server" />
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_830 %>" width="200" isSortable="true" dataIndex="Type" runat="server" renderer="$SW.IPAM.DnsRecordTypeRenderer()" />
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_371 %>" width="250" isSortable="true" dataIndex="Data" runat="server" renderer="$SW.IPAM.DnsRecordDataRenderer()" />
            <IPAMlayout:GridColumn id="ZoneNameColumn" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>" width="200" isSortable="true" dataIndex="ZoneName" runat="server" />
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_831 %>" width="200" isSortable="true" dataIndex="ServerName" runat="server" renderer="$SW.IPAM.DnsRecordServerRenderer()"/>
        </Columns>
        <Store ID="store"  dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx"
            proxyEntity="IPAM.DnsRecordReport" AmbientSort="Type" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
            <Fields>
                <IPAMlayout:GridStoreField dataIndex="DnsRecordId" isKey="True" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="DnsZoneId" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Name" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Type" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Data" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ZoneName" clause="Zone.Name AS ZoneName" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="NodeId" clause="server.NodeId AS NodeId" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ServerName" clause="G.FriendlyName AS ServerName" runat="server" />
            </Fields>
        </Store>
    </IPAMlayout:GridBox>
</asp:Content>
