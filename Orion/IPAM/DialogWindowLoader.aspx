<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ChromelessMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.DialogWindowLoader"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_279 %>" CodeFile="DialogWindowLoader.aspx.cs" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAMmaster:JsBlock Requires="ext" Orientation="jsPostInit" runat="server">

 $(document).ready(function() {
    Ext.getUrlParam = function(param)
    {
        var params = Ext.urlDecode(location.search.substring(1));
        return param ? params[param] : params;
    };

    var settingsVariableName = Ext.getUrlParam('DataContainer');

    var settings = window.parent.ipam[settingsVariableName];

    var url = settings.url;
    var data = settings.data;
    var paramName = settings.parameterName;

    Ext.DomHelper.append(document.body, {
        id: 'post-forward',
        tag: 'form',
        method: 'POST',
        action: url,
        children: [{ tag: 'input', type: 'hidden', value: data, name: paramName}]
    }).submit();
});

</IPAMmaster:JsBlock>

<div id="formbox">

</div>

</asp:Content>
