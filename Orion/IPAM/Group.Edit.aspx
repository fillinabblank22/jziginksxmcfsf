<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.GroupEdit"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_391%>" CodeFile="Group.Edit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/AccountRolesBox.ascx" TagName="AccountRolesBox" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/Orion/IPAM/DialogWindowDone.aspx?sendlicense=true&forcerefresh=0" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation=true OnMsg="MsgSave" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server" >
    .sw-form-cols-normal td.sw-form-col-label { width: 180px; }
.sw-form-cols-normal td.sw-form-col-control { width: 300px; }
    .sw-form-item, .sw-field-label{ word-wrap: break-word;  }
</IPAMmaster:CssBlock>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation=jsPostInit runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){
    $SW.SubnetEditorInit($nsId);
    $('.expander').expander();
});
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMui:ValidationIcons ID="ValidationIcons1" runat=server />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_392 %></div>

    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class="sw-field-label" style="white-space: nowrap">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_389 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="DisplayNameRequire" runat="server"
                  ControlToValidate="txtDisplayName"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources :  IPAMWebContent, IPAMWEBDATA_VB1_390 %>">
                </asp:RequiredFieldValidator>
                <asp:CustomValidator ID="VrfGroupNameExists" runat="server"
                  ControlToValidate="txtDisplayName"
                  Display=Dynamic
                  OnServerValidate="OnVrfGroupNameExists"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_11 %>">
               </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_183%>
            </div></td>
            <td ><div class=sw-form-item>
              <asp:TextBox ID="txtComment" style="width: 270px; height: 64px;" TextMode=MultiLine CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            
        </tr>
     </table></div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
        <div class="group-box white-bg" style="margin-bottom: 0px">
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
        </div>
    </div>

    <div><!-- ie6 --></div>

    <IPAMcrtl:AccountRolesBox id="AccountRolesBox" runat="server" />

    <div><!-- ie6 --></div>

  </div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

<div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary"/>
    <orion:LocalizableButtonLink runat="server" NavigateUrl="/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary"/>
</div>

</asp:Content>
