﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.SplitScope.SplitPercentage.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpSplitScope_SplitPercentage"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_720 %>" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register Src="~/Orion/IPAM/DhcpSplitScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGSplitScopes" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />


<IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
.GroupBorder-Scope1 {
    border: solid 1px #dbdcd7; 
    border-color: rgb(153,102,0);
    border-width: 3px 3px 3px 30px; 
    border-radius: 2px;
    padding: 10px 10px 0px 10px;
    width : 440px;
}
.GroupBorder-Scope2 {
    border: solid;
    border-color: rgb(0,51,102);
    border-width: 3px 3px 3px 30px;
    border-radius: 2px;
    padding: 10px 10px 0px 10px;
    width : 440px;    
}
.group-header { height: 34px; }
.Slider-bar{            margin : 20px 150px 70px 215px; }
.sw-form-invalid-icon{  left : 246px !important; }
.sw-form-item .x-form-text { text-align: right; }
.tdPadding{             padding-left : 7px; }
            
#splitScopeSlider {
    margin: 0px, auto, 0px, auto;
    width: 500px; height: 30px;
    font-size: 10px; line-height: 1.5; font-family: Arial;
}

#splitScopeSlider .x-slider-thumb, #splitScopeSlider .x-slider-thumb-over {
    background-image:url(/Orion/IPAM/res/images/Slider/slider-thumb.png);
    background-position: -1px -1px;
    background-repeat: repeat-x;
    width: 17px; height:28px;
    margin: 0px; z-index: 1000;
    border: 1px solid transparent;
}
#splitScopeSlider .x-slider-thumb-over { border: 1px solid #707070;  border-radius: 3px; }

#splitScopeSlider .x-slider-end, #splitScopeSlider .x-slider-inner {
    background-image:url(/Orion/IPAM/res/images/Slider/slider-right.png);
    background-position: 0px 3px;
    background-repeat: repeat-x;
}
#splitScopeSlider .x-slider-inner { height:30px; }

#splitScopeSlider .ux-slider-left {
    background-image:url(/Orion/IPAM/res/images/Slider/slider-left.png);
    background-repeat: repeat-x;
    background-position: 0px 3px;
    z-index: 10000;
}

#splitScopeSlider .ux-slider-bottomgraphlegend div.tick {
    float:left !important; z-index: 99;
    background-image:url('/Orion/IPAM/res/images/Slider/tick.png');
    width: 1px; height: 8px;
}
#splitScopeSlider .ux-slider-bottomgraphlegend div.percent {
    float:left !important; z-index: 100;
    background: #FFFFFF; font-size: 8pt;
}
.sw-form-wrapper { table-layout: auto; }
.x-window.x-window-plain.x-window-dlg{ min-width: 300px !important; }
    a img {margin-right: 5px;}
</IPAMmaster:CssBlock>

<orion:Include File="breadcrumb.js" runat="server" />

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-ux-wait.js,sw-subnet-manage.js,sw-dialog.js,sw-dhcp-scopewizard.js,sw-helpserver,sw-dhcp-splitscopewizzard.js,ninja-tooltip.js" Orientation="jsPostInit" runat="server">
$(document).ready(function(){ $SW.IPAM.SplitScopeSlider(
    $nsId,
    'splitScopeSlider',
    $SW.ScopeSubnet.address,
    $SW.ScopeSubnet.mask,
    $SW.ScopeSubnet.start,
    $SW.ScopeSubnet.end,
    $SW.ScopeSubnet.slider
); });

$(document).ready(function(){ if($SW.TaskId && $SW.TaskId != ''){
    $SW.IPAM.SplitScopeTask($SW.TaskId);
} 
     $('#toolTip').html(GetTooltipHtml());
    });
</IPAMmaster:JsBlock>
    
<script type="text/javascript" language="javascript">
$SW.ScopeSubnet = {
    address: '<%=this.DhcpScope.Address %>',
    mask: '<%=this.DhcpScope.AddressMask %>',
    start: '<%=this.DhcpRange[0].StartAddress %>',
    end: '<%=this.DhcpRange[0].EndAddress %>',
    slider: '<%=this.SplitScopeSlider %>'
};
$SW.TaskId = '<%=TaskId %>';
</script>

    <IPAMui:ValidationIcons runat="server" />

<div style="margin: 10px 0px 0px 10px;">
  <div><table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr>
      <td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled)
   { %>
    <div style="margin-top: 10px;"><ul class="breadcrumb">
        <li class="bc_item">
            <a href="/api2/ipam/ui/scopes" title="DHCP &amp; DNS Management" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
  { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6%></a></li>
<% } %>
        </ul></li>
    </ul></div>
<% } %>
        <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
      </td>
    </tr>
  </table></div>
  <div><!-- ie6 --></div>

  <div style="width: 990px;">
    <asp:HiddenField ID="hdWebId" runat="server" />
    <asp:HiddenField ID="hdnSplitAddress" runat="server" />
    <orion:Progress ID="ProgressIndicator1" runat="server" /> 

    <div class="GroupBox" id="NormalText" >
    <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_732 %></span>
    <br />
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_733 %>
    <br />
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_734%>
    <br /><br />
        
    <div class="contentBlock">
        <table cellspacing="10" cellpadding="0" class="sw-form-wrapper">
            <tr><td colspan="2" style="width:850px;"><div id="splitScopeSlider" class="Slider-bar"></div></td></tr>
			<tr>
                <td colspan ="2" class ="TooltipLinkPlace">
                    <a id ="PercentageSplitScopeHelpLink" runat ="server" target ="_blank"><span id="toolTip"></span></a>                                
                </td>
            </tr>
            <tr>
                <td class="GroupBorder-Scope1"><div>
                    <div class="ActionName group-header"><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_735, this.SrcScopeName, this.SrcServerName)%></div>
                    <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                        <tr class="sw-form-cols-normal" style = "height :15px">
                            <td class="sw-form-col-label" style = "width:135px"/>
                            <td class="sw-form-col-control" />
                            <td class="sw-form-col-comment" />
                        </tr>
                        <tr style="height: 48px">
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_736 %> </div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtPercentOfAddress1" runat="server" CssClass="x-form-text x-form-field"
                                    Style="width: 40px; margin-left: 50px;" /> &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_742 %>
                            </div></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_737%> </div></td>
                            <td><div class="sw-form-item"> 
                                    <asp:TextBox ID="txtIncludeIPFrom" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                                    &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_738%> &nbsp; 
                                    <asp:TextBox ID="txtIncludeIPTo" runat="server" CssClass="x-form-text x-form-field" Width="90px" /> &nbsp;
                            </div></td>
                            <td><asp:RequiredFieldValidator ID="ReqIncludeAddress1" runat="server" ControlToValidate="txtIncludeIPTo"
                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_739 %>" 
                            /><asp:CustomValidator ID="IncludeAddress1IPv4" runat="server" ControlToValidate="txtIncludeIPTo" ClientValidationFunction="$SW.Valid.Fns.ipv4" 
                                    Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_740 %>"/></td>
                        </tr>
                        <tr style="height: 45px">
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_741 %> </div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtExcludeIPFrom" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                                &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_738%> &nbsp;
                                <asp:TextBox ID="txtExcludeIPTo" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                            </div></td>
                            <td></td>
                        </tr>
                        <tr><td colspan="3"><div id="srcScopeOfferDelay" runat="server">
                            <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                                <tr class="sw-form-cols-normal">
                                    <td class="sw-form-col-label" style="width: 135px" />
                                    <td class="sw-form-col-control" />
                                    <td class="sw-form-col-comment" />
                                </tr>
                                <tr>
                                    <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_745%> </div></td>
                                    <td><div class="sw-form-item">
                                        <asp:TextBox ID="txtOfferDelay" runat="server" CssClass="x-form-text x-form-field"
                                            Width="90px"></asp:TextBox> &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_561%>
                                    </div></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div></td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                    </table>
                </div></td>
                <td class="GroupBorder-Scope2"><div>
                    <div class="ActionName group-header"><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_735, this.DstScopeName, this.DstServerName)%></div>
                    <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                        <tr class="sw-form-cols-normal" style = "height :15px">
                            <td class="sw-form-col-label" style = "width:135px"/>
                            <td class="sw-form-col-control" />
                            <td class="sw-form-col-comment" />
                        </tr>
                        <tr style="height: 48px">
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_736%> </div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtPercentOfAddress2" runat="server" CssClass="x-form-text x-form-field"
                                    Style="width: 40px; margin-left: 50px;" /> &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_742%></div></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_737%> </div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtIncludeIPFrom2" runat="server" CssClass="x-form-text x-form-field" Width="90px"></asp:TextBox>
                                &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_738%> &nbsp;
                                <asp:TextBox ID="txtIncludeIPTo2" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                            </div></td>
                            <td><asp:RequiredFieldValidator ID="ReqIncludeAddress2" runat="server" ControlToValidate="txtIncludeIPFrom2"
                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_743 %>"
                                /><asp:CustomValidator ID="CustIncludeAddress2IPv4" runat="server" ControlToValidate="txtIncludeIPFrom2" ClientValidationFunction="$SW.Valid.Fns.ipv4" 
                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_744 %>"/></td>
                        </tr>
                        <tr style="height: 45px">
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_741%> </div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtExcludeIPFrom2" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                                &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_738%> &nbsp;
                                <asp:TextBox ID="txtExcludeIPTo2" runat="server" ReadOnly="true" CssClass="x-form-text x-form-field" style="width: 90px; color: gray;" Enabled="false" />
                            </div></td>
                            <td></td>
                        </tr>
                        <tr><td colspan="3"><div id="dstScopeOfferDelay" runat="server">
                            <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                                <tr class="sw-form-cols-normal">
                                    <td class="sw-form-col-label" style="width: 135px" />
                                    <td class="sw-form-col-control" />
                                    <td class="sw-form-col-comment" />
                                </tr>
                                <tr>
                                    <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_745%> </div></td>
                                    <td><div class="sw-form-item">
                                        <asp:TextBox ID="txtOfferDelay2" runat="server" CssClass="x-form-text x-form-field"
                                            Width="90px" /> &nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_561%>
                                    </div></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div></td></tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                    </table>
                </div></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div><table cellspacing="5" class="sw-form-wrapper">
        <tr class="sw-form-cols-normal" style = "height : 20px">
            <td class="sw-form-col-label"  style="width: 300px"/>
            <td class="sw-form-col-control"  style="width: 370px"/>
            <td class="sw-form-col-comment" />
        </tr>
        <tr>
            <td class="leftLabelColumn" align = "right">&nbsp;</td>
            <td colspan = "2"><div class="sw-btn-bar-wizard">
                <orion:LocalizableButton ID="imgbPrev" runat="server" LocalizedText="Back" DisplayType="Secondary" OnClick="imgbPrev_Click" CausesValidation="false" />
                <orion:LocalizableButton ID="imgbNext" runat="server" LocalizedText="Finish" DisplayType="Primary" OnClick="imgbFinish_Click" />
                <orion:LocalizableButton ID="imgbCancel" runat="server" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" OnClick="imgbCancel_Click" />
            </div></td>
        </tr>
    </table></div>

        </div>

  </div>    
</div>
<asp:ValidationSummary ID="valSummary" ShowSummary="false" ShowMessageBox="true" runat="server" DisplayMode="BulletList" />
</asp:Content>
