<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.SplitScope.Wizard.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpSplitScopeWizard"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_720 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register Src="~/Orion/IPAM/DhcpSplitScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGSplitScopes" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css" >
        .GroupBorder-Scope1 {
            border: solid 1px #dbdcd7; 
            border-color: rgb(153,102,0);
            border-width: 3px 3px 3px 30px; 
            border-radius: 2px;
            padding: 10px 10px 0px 10px;   
            height: 128px;   
            width : 372px;
        }

        .GroupBorder-Scope2 {
            border: solid; 
            border-color: rgb(0,51,102);
            border-width: 3px 3px 3px 30px; 
            border-radius: 2px;
            padding: 10px 10px 0px 10px;
            height: 128px;
            width : 372px;      
        }
        .sw-form-invalid-icon{
                left : 210px !important;
            }
        .sw-form-wrapper .x-form-field-wrap div.sw-form-invalid-icon {
                margin-left: -18px !important;
            }
        .sw-form-item .x-form-field-wrap .x-form-text {
                width: 168px !important;
            }
        .sw-form-item .x-form-text {
                width: 175px !important;
            }
        .CiscoHighAvailability {
            margin: 0 60px 0 10px;
            padding: 5px 0 10px 10px;
            background-color:#FFF7CD;
        }
        .sw-form-item .x-form-text{width: 185px !important;}
    </IPAMmaster:CssBlock>

    <IPAMmaster:JsBlock Requires="ext, sw-dns-manage.js" Orientation="jsInit" runat="server">
    
        $(document).ready(function(){
        var serverId = $SW.nsGet($nsId, 'drpDhcpServer');
        var serverCb = $SW.TransformDropDown(serverId, serverId, { hiddenId: serverId } );
    });

    </IPAMmaster:JsBlock>

    <orion:Include File="breadcrumb.js" runat="server" />

    <IPAMui:ValidationIcons runat="server" />

     <div style="margin: 10px 0px 0px 10px;">

        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
                    <div style="margin-top: 10px;"><ul class="breadcrumb">
                        <li class="bc_item">
                            <a href="/api2/ipam/ui/scopes" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
                            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                    <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                    <% } %>
                        </ul></li>
                    </ul></div>
                    <% } %>
                    <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        
        <div><!-- ie6 --></div>

        <div style="width: 950px;">
            <asp:HiddenField ID="hdWebId" runat="server" />
            <orion:Progress ID="ProgressIndicator1" runat="server" /> 
            
            <div class="GroupBox" id="NormalText">
            <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_721 %></span>
            <br />
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_722 %>
            <div class="contentBlock">
                <div  runat="server" class="CiscoHighAvailability">
                    <table>
                        <tr>
                            <td ><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif"  /></td>
                            <td>
                                <%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_723, "<b>", "</b>")%>
                                
                                 <div id="highAvailability" runat="server" >
                                     <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_724, "<b>", "</b>", "<br/>")%>
                                        <span>�</span>
                                        <a style="text-decoration:none;" href="http://www.solarwinds.com/documentation/kbLoader.aspx?kb=4268&lang=en" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_725 %></a>
                                  </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <table cellspacing="10" cellpadding="0" class="sw-form-wrapper">
                    <tr>
                        <td style = "width : 425Px">
                            <div class = "GroupBorder-Scope1">
                                <span class="ActionName"><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_726, this.ScopeName)%></span>
                                <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                                    <tr class="sw-form-cols-normal">
                                        <td class="sw-form-col-label" style = "width :150px"/>
                                        <td class="sw-form-col-control" />
                                        <td class="sw-form-col-comment" />
                                    </tr>
                                    <tr style = "height : 50px">
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_727 %> </div></td>
                                        <td><div class="sw-form-item"> <asp:Label ID = "lblCurrentScopeName" runat = "server"></asp:Label></div></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_578 %> </div></td>
                                        <td><div class="sw-form-item"> <asp:Label ID = "lblDhcpServer" runat = "server"></asp:Label></div></td>
                                        <td></td>
                                    </tr>
                                    <tr style = "height : 40px">
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147%> </div></td>
                                        <td><div class="sw-form-item"> <asp:Label ID = "lblDescription" runat = "server"></asp:Label></div></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td style = "width : 425Px">
                            <div class = "GroupBorder-Scope2">
                                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_728%></span>
                                <table cellspacing="0" cellpadding="0" class="sw-form-wrapper">
                                    <tr class="sw-form-cols-normal">
                                        <td class="sw-form-col-label" style = "width :150px"/>
                                        <td class="sw-form-col-control" />
                                        <td class="sw-form-col-comment" />
                                    </tr>
                                    <tr style = "height : 48px">
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_729%> </div></td>
                                        <td><div class="sw-form-item">
                                            <asp:TextBox ID = "txtNewScopeName" runat = "server" CssClass="x-form-text x-form-field"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqNewScopeName" runat="server" ControlToValidate="txtNewScopeName" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_730 %>" />
                                        </div></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_578 %> </div></td>
                                        <td>
                                            <div class="sw-form-item">
                                                <asp:DropDownList ID = "drpDhcpServer" runat = "server">
                                                    <asp:ListItem Text = "<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_763 %>" Value = "0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="ReqSecondServer" runat="server" ControlToValidate="drpDhcpServer" Display="Dynamic" 
                                                InitialValue = "0" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_731 %>" />
                                            </div>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr style = "height : 39px">
                                        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147%> </div></td>
                                        <td><div class="sw-form-item"><asp:TextBox ID = "txtDescription" runat = "server" CssClass="x-form-text x-form-field" Width = "175px"></asp:TextBox></div></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>

            <div>
                <table cellspacing="5" class="sw-form-wrapper"  >
                    <tr class="sw-form-cols-normal" style = "height : 100px">
                        <td class="sw-form-col-label"  style="width: 380px"/>
                        <td class="sw-form-col-control"  style="width: 380px"/>
                        <td class="sw-form-col-comment" />
                    </tr>
                    <tr>
                        <td class="leftLabelColumn" align = "right">&nbsp;</td>
                        <td colspan = "2">
                            <div class="sw-btn-bar-wizard"  >
                                <orion:LocalizableButton ID = "imgbNext" runat = "server" LocalizedText = "Next" DisplayType = "Primary" OnClick = "imgbNext_Click"/>
                                <orion:LocalizableButton ID = "imgbCancel" runat = "server" LocalizedText = "Cancel" DisplayType = "Secondary" OnClick = "imgbCancel_Click" CausesValidation = "false"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
               
        </div>        

    </div>
    <asp:ValidationSummary ID="valSummary" ShowSummary="false" ShowMessageBox="true" runat="server" DisplayMode="BulletList" />
</asp:Content>
