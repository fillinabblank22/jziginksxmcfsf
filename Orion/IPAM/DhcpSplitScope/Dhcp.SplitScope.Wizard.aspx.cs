using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Common;


namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpSplitScopeWizard : CommonPageServices
    {
        #region Declarations

        private const string QueryScopeParam = "GroupId";
        private const string QueryWebIdParam = "wId";

        public string ScopeName { get; set; }
        private PageDhcpSplitScopeEditor Editor { get; set; }
        private string WebId { get; set; }
        private int GroupId { get; set; }
        private string WId { get; set; }

        #endregion

        #region Constructors

        public DhcpSplitScopeWizard()
        {
            this.Editor = new PageDhcpSplitScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DhcpSplitScopeWizardStep.Define;

            this.WebId = GetParam(DhcpSessionStorage.PARAM_WEBID, null);

            // [oh] Dhcp Scope from scope's GroupId
            this.GroupId = GetParam(QueryScopeParam, -1);

            this.WId = GetParam(QueryWebIdParam, null);
            if (!string.IsNullOrEmpty(this.WId) && this.GroupId < 0)
            {
                DhcpScope scope;
                List<DhcpRange> range;

                Editor.Session_RetrieveScopeInfo(this.WId, out scope, out range, null);
                this.GroupId = Editor.GetScopeGroupID(scope.FoundAddress, scope.CIDR);
            }

            if (string.IsNullOrEmpty(this.WebId) && this.GroupId >= 0)
            {
                this.WebId = Editor.Session_SetScopeInfo(this.GroupId);
            }            

            if (!IsPostBack)
            {
                InitControls();
            }
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.Session_Clear(webId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            this.hdWebId.Value = this.WebId;

            DhcpScope srcScope, dstScope;
            List<DhcpRange> srcRange, dstRange;
            
            Editor.Session_RetrieveScopeInfo(this.WebId, out srcScope, out srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Session_RetrieveScopeInfo(this.WebId, out dstScope, out dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);
            srcScope.DhcpServerName = Editor.Session_RetrieveServerName(this.WebId);

            ScopeName = srcScope.FriendlyName;
            lblCurrentScopeName.Text = srcScope.FriendlyName;
            lblDhcpServer.Text = srcScope.DhcpServerName;
            lblDescription.Text = srcScope.Comments;

            txtNewScopeName.Text = dstScope.FriendlyName;
            txtDescription.Text = dstScope.Comments;

            var srcDhcpServer = Editor.GetDhcpServer(srcScope.ParentId);

            highAvailability.Visible = srcDhcpServer.ServerType == DhcpServerType.CISCO;

            drpDhcpServer.AppendDataBoundItems = true;
            drpDhcpServer.DataSource = Editor.GetDhcpServersList(srcDhcpServer);
            drpDhcpServer.DataTextField = "FriendlyName";
            drpDhcpServer.DataValueField = "GroupId";
            drpDhcpServer.DataBind();

            if (dstScope.ParentId > 0)
                drpDhcpServer.SelectedValue = dstScope.ParentId.ToString();
            else
                drpDhcpServer.SelectedValue = "0";

            this.ProgressIndicator1.WebId = this.WebId;
            Editor.ActivateProgressStep(this.WebId, PageDhcpSplitScopeEditor.IPAMDHCPSCOPEDEFAULT);
        }

        private void Next()
        {
            string webId = this.hdWebId.Value;

            DhcpScope srcScope, dstScope;
            List<DhcpRange> srcRange, dstRange;
            Editor.Session_RetrieveScopeInfo(webId, out srcScope, out srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Session_RetrieveScopeInfo(webId, out dstScope, out dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            dstScope.FriendlyName = txtNewScopeName.Text;
            dstScope.Comments = txtDescription.Text;
            if (drpDhcpServer.SelectedValue != "0")
            {
                var dstDhcpServerGroupId = Convert.ToInt32(drpDhcpServer.SelectedValue);
                var dstDhcpServer = Editor.GetDhcpServer(dstDhcpServerGroupId);
                dstScope.NodeId = dstDhcpServer.NodeId;
                dstScope.ParentId = Convert.ToInt32(drpDhcpServer.SelectedValue);
            }

            srcScope.DhcpServerName = Editor.Session_RetrieveServerName(this.WebId);
            dstScope.DhcpServerName = drpDhcpServer.SelectedItem.Text;

            Editor.Sesssion_UpdateScopeInfo(webId, srcScope, srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Sesssion_UpdateScopeInfo(webId, dstScope, dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, webId);
            Response.Redirect(url, true);
        }

        #endregion
    }
}
