﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Solarwinds.IPAM.Common.CommonExt;
using SolarWinds.Common.Net;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using SolarWinds.IPAM.Common;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Web.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpSplitScope_SplitPercentage : CommonPageServices
    {
        #region Declarations

        private const string PARAM_TASKID = "taskid";

        public string TaskId
        {
            get { return base.GetParam(PARAM_TASKID, string.Empty); }
        }

        private PageDhcpSplitScopeEditor Editor { get; set; }

        private DhcpScope srcScope, dstScope;
        protected DhcpScope DhcpScope { get { return srcScope; } }

        private List<DhcpRange> srcRange, dstRange;
        protected List<DhcpRange> DhcpRange { get { return srcRange; } }
		
		public string SrcScopeName { get; set; }
        public string DstScopeName { get; set; }
        public string SrcServerName { get; set; }
        public string DstServerName { get; set; }

        protected string SplitScopeSlider
        {
            get
            {
                return Editor.Session_GetSplitScopeSlider(null);
            }
            set
            {
                Editor.Session_SetSplitScopeSlider(null, value);
            }
        }

        #endregion // Declarations

        #region Constructors

        public DhcpSplitScope_SplitPercentage()
        {
            this.Editor = new PageDhcpSplitScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DhcpSplitScopeWizardStep.Percentage;

            if (!IsPostBack)
            {
                InitControls();
            }
        }

        protected void imgbFinish_Click(object sender, EventArgs e)
        {
            Split();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        private void InitControls()
        {
            Editor.Session_RetrieveScopeInfo(null, out srcScope, out srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Session_RetrieveScopeInfo(null, out dstScope, out dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            // To display the Scope and Server Names on UI:
            SrcScopeName = srcScope.FriendlyName;
            SrcServerName = srcScope.DhcpServerName;
            DstScopeName = dstScope.FriendlyName;
            DstServerName = dstScope.DhcpServerName;

            var address = IPAddress.Parse(srcScope.Address);

            if (srcScope.CIDR > 30)
                throw new ArgumentException("Splitting of scope is supported only for scopes with CIDR <= 30");

            var subnetStart = IPFunction.SubnetFirstIP(address, srcScope.CIDR);
            var subnetEnd = IPFunction.SubnetLastIP(address, srcScope.CIDR);
            var subnetRange = new IPAddressRange(
                IPHelper.GetNextIPAddress(subnetStart),
                IPHelperEx.GetPreviousIPAddress(subnetEnd),
                false
            );

            var scopeRange = new IPAddressRange(
                 IPAddress.Parse(srcRange[0].StartAddress),
                 IPAddress.Parse(srcRange[0].EndAddress),
                 false
            );
            var range = IPAddressRangeEx.Intersect(scopeRange, subnetRange);

            txtIncludeIPFrom.Text = txtExcludeIPFrom2.Text = range.From.ToString();
            txtExcludeIPTo.Text = txtIncludeIPTo2.Text = range.To.ToString();
            txtExcludeIPFrom.Text = txtIncludeIPFrom2.Text;
            txtExcludeIPTo2.Text = txtIncludeIPTo.Text;

            InitOfferDelay(srcScopeOfferDelay, txtOfferDelay, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            InitOfferDelay(dstScopeOfferDelay, txtOfferDelay2, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            PercentageSplitScopeHelpLink.HRef = HelpHelper.GetHelpUrl("OrionIPAMAGSplitScopes");
            

            if (Orion.Common.OrionConfiguration.IsDemoServer)
            {
                imgbNext.OnClientClick = String.Format("Ext.MessageBox.show({{msg:'{0}',buttons: Ext.MessageBox.OK}}); return false;", Resources.IPAMWebContent.IPAMWEBDATA_ED1_3);
            }

            Editor.ActivateProgressStep(null, PageDhcpSplitScopeEditor.IPAMDHCPSPLITSCOPEPERCENTAGE);
        }

        private void InitOfferDelay(HtmlGenericControl container, TextBox control, string postfix)
        {
            // [oh] get proper DHCP manager to check capability
            var dhcpServerType = Editor.Session_RetrieveServerType(null, postfix);
            var isCapableOfOfferDelay = DhcpFactory.Instance.CheckedDhcpManagerCapability(
                dhcpServerType, typeof(IOfferDelayManagement));

            if (isCapableOfOfferDelay)
            {
                int offerDelay;
                Editor.Session_RetrieveScopeProperties(null, out offerDelay, postfix);
                control.Text = offerDelay.ToString();
            }
            else
            {
                control.Text = "0";
                control.ReadOnly = true;
                control.CssClass += " x-item-disabled disabled-textbox";

                container.Style.Add("display", "none");
            }
        }

        private void Split()
        {
            if (!ValidateUserInput())
                return;

            Editor.Session_RetrieveScopeInfo(null, out srcScope, out srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Session_RetrieveScopeInfo(null, out dstScope, out dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            if (!string.IsNullOrEmpty(txtOfferDelay.Text))
                srcScope.OfferDelay = Convert.ToInt32(txtOfferDelay.Text);

            if (!string.IsNullOrEmpty(txtOfferDelay2.Text))
                dstScope.OfferDelay = Convert.ToInt32(txtOfferDelay2.Text);

            Editor.Sesssion_UpdateScopeOfferDelay(null, srcScope.OfferDelay, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Sesssion_UpdateScopeOfferDelay(null, dstScope.OfferDelay, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            txtExcludeIPFrom.Text = txtIncludeIPFrom2.Text;
            txtExcludeIPTo2.Text = txtIncludeIPTo.Text;

            dstRange[0].StartAddress = txtExcludeIPFrom2.Text;
            srcRange[0].EndAddress =txtExcludeIPTo.Text;

            SplitScopeSlider = hdnSplitAddress.Value;

            Editor.Sesssion_UpdateScopeInfo(null, srcScope, srcRange, PageDhcpSplitScopeEditor.IPAMDhcpSrcScope);
            Editor.Sesssion_UpdateScopeInfo(null, dstScope, dstRange, PageDhcpSplitScopeEditor.IPAMDhcpDstScope);

            var srcExclusion = new IpRange(txtExcludeIPFrom.Text, txtExcludeIPTo.Text);
            var dstExclusion = new IpRange(txtExcludeIPFrom2.Text, txtExcludeIPTo2.Text);

            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Editor.Split(null, srcExclusion, dstExclusion);

                string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
                Response.Redirect(url, true);
            }
        }

        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }
    }
}
