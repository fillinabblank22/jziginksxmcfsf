<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs"
    Inherits="DhcpSplitScope_ProgressIndicator" %>
    <%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css" >
        .image-tab-indicator-on { padding-right: 0 !important; }
        .image-tab-indicator-off { padding-right: 0 !important;
                 }
        .image-tab-indicator-on span.sw-btn-c {
            background-repeat: repeat;
            background-image: url('/Orion/IPAM/res/images/progress_indicator/PI_on.png') !important;
            cursor: auto;            
        }
        .image-tab-indicator-off span.sw-btn-c {
            background-repeat: repeat;
            background-position: 0 center !important;
            background-image: url('/Orion/IPAM/res/images/progress_indicator/pi_bg_gradient.gif') !important;
            cursor: auto;
        }
        .image-tab-indicator-on span {
            color: #FFFFFF !important;         
        }
        .image-tab-indicator-off span {
            color: black !important;         
        }
        .dhcpsplitscope-progress > a {
            margin-bottom: 0;
            margin-left: 0;
        }
        .dhcpsplitscope-progress > a .sw-btn-c {
            float: left;
        }
        .dhcpsplitscope-progress > a .sw-btn-t {
            line-height: 17px;
            padding-left: 4px;
        }
        
    </IPAMmaster:CssBlock>

    <div style="width:700x; background: url('<%=Page.ResolveUrl("~/Orion/IPAM/res/images/progress_indicator/pi_bg_gradient.gif") %>'); background-repeat: repeat-x;">
</div>

<div class="dhcpsplitscope-progress" style="padding-bottom:0px;margin-bottom:0px; height:17px;width:700x; background: url('<%=Page.ResolveUrl("~/Orion/IPAM/res/images/progress_indicator/pi_bg_gradient.gif") %>'); background-repeat: repeat-x; background-position:top;"><asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder></div>

 