using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Web.UI;

public enum DhcpSplitScopeWizardStep
{
    Define,
    Percentage
};

public struct Steps
{
    public DhcpSplitScopeWizardStep StepName;
    public string Pageurl;
    public string ImageName;
    public string HighlightImage;
    public string DisplayText;
}

public partial class DhcpSplitScope_ProgressIndicator : System.Web.UI.UserControl
{
    private readonly string indicatorImageFolderPath = "~/Orion/IPAM/res/images/progress_indicator/";
    List<Steps> scopeSteps = new List<Steps>();

    public string WebId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        scopeSteps.Add(new Steps() { StepName = DhcpSplitScopeWizardStep.Define, Pageurl = "Dhcp.SplitScope.Wizard.aspx", ImageName = "PI_definingscope_off.png", HighlightImage = "PI_definingscope_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBDATA_VB1_746 });
        scopeSteps.Add(new Steps() { StepName = DhcpSplitScopeWizardStep.Percentage, Pageurl = "Dhcp.SplitScope.SplitPercentage.aspx", ImageName = "PI_percentagesplit_off.png", HighlightImage = "PI_percentagesplit_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBDATA_VB1_747 });
        
        LoadProgressImages();       

    }

    #region public properties

    [Browsable(true)]
    [Category("Appearance")]
    public DhcpSplitScopeWizardStep CurrentStep
    {
        set;
        get;
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string NextStep
    {
        get
        {
            int index = scopeSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index + 1 < scopeSteps.Count) ? scopeSteps[index + 1].Pageurl : scopeSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string PrevStep
    {
        get
        {
            int index = scopeSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index - 1 >= 0) ? scopeSteps[index - 1].Pageurl : scopeSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string HomePage
    {
        get
        {
            return "~/api2/ipam/ui/scopes";
        }
    }
    #endregion

    #region public methods

    public void LoadProgressImages()
    {
        RenderProgressImages();
    }

    #endregion

    #region private methods

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();
        int count = scopeSteps.Count;

        for (int i = 0; i < count; i++)
        {
            bool nextActive;
            //ImageButton imgStep = new ImageButton();      
            LocalizableButton imgStep = new LocalizableButton();
            imgStep.DisplayType = SolarWinds.Orion.Web.UI.ButtonType.Resource;
            imgStep.ID = string.Format("imgStep{0}", i + 1);

            ImageButton imgSeparator = new ImageButton();
            imgSeparator.ID = string.Format("imgSep{0}", i + 1);

            nextActive = (i < count - 1) ? (this.CurrentStep == scopeSteps[i + 1].StepName) : false;

            imgStep.Enabled = IsStepActive(this.scopeSteps[i].StepName.ToString());
            if (imgStep.Enabled)
            {
                string url = PageDhcpScopeEditor.AddWebIdParam(this.Page,
                    scopeSteps[i].Pageurl, this.WebId);
                imgStep.PostBackUrl = url;               
            }
            imgStep.CausesValidation = false;
            imgStep.Text = scopeSteps[i].DisplayText;
            if (this.CurrentStep == scopeSteps[i].StepName)
            {
                imgStep.CssClass = "image-tab-indicator-on";
                imgSeparator.ImageUrl = string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off");
            }
            else
            {
                imgStep.CssClass = "image-tab-indicator-off";
                imgSeparator.ImageUrl = (nextActive) ? string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on") : string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off");

            }
            phPluginImages.Controls.Add(imgStep);
            phPluginImages.Controls.Add(imgSeparator);
        }
    }


    private bool IsStepActive(string stepName)
    {
        using (var storage = new DhcpSessionStorage(this.Page, this.WebId))
        {
            return storage[stepName] != null ? 
                Convert.ToBoolean(storage[stepName]) : false;
        }
    }

    #endregion
}
