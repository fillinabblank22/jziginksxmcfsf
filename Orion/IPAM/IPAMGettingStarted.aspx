﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master"
    Inherits="SolarWinds.IPAM.WebSite.IPAMGettingStarted" CodeFile="IPAMGettingStarted.aspx.cs"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_96 %>" %>

<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <IPAMmaster:CssBlock ID="CssBlock1" Requires="~/Orion/styles/admin.css" runat="server">
    .ContentBucket {
        padding: 17px !important;
    }
    h2 {
        font-family: Arial,Helvetica,Sans-Serif;
        font-weight: bold;
        font-size: 14px;
    }
    h3 {
        font-family: Arial,Helvetica,Sans-Serif;
        font-weight: normal;
        font-size: 12px;
    }
    #sw-item-list {
        width: 100%;
        table-layout: fixed;
        margin-top: 10px !important;        
    }
    #sw-item-list td {
        padding-top: 12px;
        padding-bottom: 12px;
        padding-left: 0.8%;
        padding-right: 8px;
    }
    #sw-item-list .even {
        background-color: rgb(228, 241, 248);
    }
    #sw-item-list .odd {
        background-color: rgb(255, 255, 255);
    }
    #sw-item-list .icon {
        text-align: left;
        width: 42px;
    }
    .advanced {
        width: 100%;
        margin-left:0.8%;
        margin-top:8px;
    }
    #sw-item-list td.oddheading {
        padding : 25px 0 4px;
    }
    
    </IPAMmaster:CssBlock>
    <IPAMmaster:JsBlock Orientation="jsInit" runat="server">
    $(document).ready( function(){
        $(':radio').click( function(){
            $(':radio:checked').attr('checked', '');
            $(this).attr('checked', 'checked');
        });
    });
    
    $SW.IPAMGettingStarted = function(that){
        var rb = $(':radio:checked');
        if(rb && rb.length==1){ return true;}

        Ext.Msg.show({
               title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_8;E=js}',
               msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_9;E=js}',
               buttons: Ext.Msg.OK,
               width: 400,
               icon: Ext.MessageBox.INFO,
               fn: function(btn){}
        });
        return false;
    };
 
    </IPAMmaster:JsBlock>
     <script type="text/javascript">
         var navigateButtons = [];
         navigateButtons.push($('#' + '<%= btnNext.ClientID %>'));
         $.each(navigateButtons, function(idx, btn) {
             $(btn).on("click", function (event) {
                 if (($('#' + '<%= rbDnsServer.ClientID %>').is(':checked') || $('#' + '<%= rbDhcpServer.ClientID %>').is(':checked')) && '<%= this.isFreeTool %>' == "True" && '<%= this.isOtherModuleInstalled %>' == "False") {
                     alert('<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_45 %>');
                     return false;
                 }
             });
         });
         </script>
    <div style="margin-left: 10px;">
        <table id="sw-navhdr" style="width: 100%; table-layout: fixed;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding-top: 14px;" rowspan="2">
                    <div style="padding-left: 10px; font-size: 18px">
                        <%=Page.Title%></div>
                </td>
            </tr>
        </table>
        <div class="ContentBucket">
            <table id="sw-item-list" cellpadding="0" cellspacing="0">
                <tr class="even">
                    <td class="icon">
                        <asp:RadioButton ID="rbImport" GroupName="choice" Checked="true" runat="server" />
                    </td>
                  
                <td class="icon">
                    <img src="/Orion/IPAM/res/images/sw-resources/ip-icon.png" alt="" />
                </td>
                <td style="left: 20px">
                     <h2>
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_19%></h2>
                      <h3>
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_20%>
                    </h3>
                </td>
                </tr>
                <tr class="odd">
                    <td class="icon">
                        <asp:RadioButton ID="rbDnsServer" GroupName="choice" runat="server" />
                    </td>
                    <td class="icon">
                        <img src="res/images/sw/Add_DNS_Server.png" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_134 %>" />
                    </td>
                    <td>
                        <h2>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_2%></h2>
                        <h3>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_3%></h3>
                    </td>
                </tr>
                <tr class="even">
                    <td class="icon">
                        <asp:RadioButton ID="rbDhcpServer" GroupName="choice" runat="server" />
                    </td>
                    <td class="icon">
                        <img src="res/images/sw/Add_DHCP_Server.png" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_134 %>" />
                    </td>
                    <td>
                        <h2>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_125 %></h2>
                        <h3>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_126 %></h3>
                    </td>
                </tr>
            </table>
        </div>
        <div class="sw-btn-bar" style="margin-top: 20px; margin-left: 20px">
            <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnNext" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK0_4 %>"
                DisplayType="Primary" OnClick="OnNextClick" />
            <orion:LocalizableButtonLink runat="server" CausesValidation="false" ID="btnCancel"
                LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="IPAMSummaryView.aspx" />
        </div>
    </div>
</asp:Content>
