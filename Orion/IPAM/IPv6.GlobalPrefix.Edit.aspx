﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" 
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_442%>" CodeFile="IPv6.GlobalPrefix.Edit.aspx.cs" Inherits="IPv6GlobalPrefixEdit" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.PrefixSize.ascx" TagName="IPv6PrefixSize" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.CompoundAddress.ascx" TagName="IPv6CompoundAddress" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/AccountRolesBox.ascx" TagName="AccountRolesBox" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-expander.js,sw-ipv6-manage.js,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){ $('.expander').expander(); });
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
.sw-field-label { word-wrap: break-word; }
.sw-form-disabled { color: #777; }
#formbox .align-top { vertical-align: text-top; padding-top: 3px; }
#formbox .align-bottom { vertical-align: bottom; padding-bottom: 3px; }
.sw-form-cols-normal td.sw-form-col-label { width: 210px; }
.sw-form-cols-normal td.sw-form-col-control { width: 300px; }
.sw-form-item .x-form-text {width: 339px;}
</IPAMmaster:CssBlock>

<IPAMui:ValidationIcons ID="ValidationIcons1" runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_443%></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_434%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="NameRequireValidator" runat="server"
                  ControlToValidate="txtName"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_435 %>">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="align-top"><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147%>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="x-form-text x-form-field x-form-desc" runat="server" />
            </div></td>
            <td></td>
        </tr>      
    </table></div>

    <div><!-- ie6 --></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_240 %>">
      <table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td colspan="3" style="padding-bottom: 12px;">
                <IPAMcrtl:IPv6CompoundAddress ID="PrefixAddress" ReadOnly="true" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="align-top"><div class=sw-field-label style="white-space: normal">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_436%>
            </div></td>
            <td><IPAMcrtl:IPv6PrefixSize ID="PrefixSize" ReadOnly="true" runat="server" /></td>
            <td></td>
        </tr>      
      </table>
    </div>
        
    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
        <div class="group-box white-bg" style="margin-bottom: 0px">
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
        </div>
    </div>

    <div><!-- ie6 --></div>

    <IPAMcrtl:AccountRolesBox id="AccountRolesBox" runat="server" />

    <div><!-- ie6 --></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 180px;">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl="/Orion/IPAM/subnets.aspx"
                        LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
