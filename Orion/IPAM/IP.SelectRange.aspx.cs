using System;
using System.Web.UI;
using SolarWinds.IPAM.Common;
using SolarWinds.IPAM.Web.Common;
using System.Collections.Generic;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Data;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPSelectRange : CommonPageServices
    {
        PageIpRangeEditor editor;
        const string RemoveActionName = "remove";
        const string EditActionName = "edit";

        public IPSelectRange() 
        {
            editor = new PageIpRangeEditor(this); 
        }

        protected void DoAction(string actionName)
        {
            int availableIpCount, usedIpCount;
            LoadIpCounts(out availableIpCount, out usedIpCount);

            SetSelectedAction(actionName, availableIpCount, usedIpCount);
            editor.Save(true);
        }

        protected void MsgEdit(object sender, EventArgs e) 
        {
            DoAction(EditActionName);
        }

        protected void MsgRemove(object sender, EventArgs e) 
        {
            DoAction(RemoveActionName);
        }

        void SetSelectedAction(string actionName, int availableIpCount, int usedIpCount)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data["actionName"] = actionName;
            data["availableIpCount"] = availableIpCount;
            data["usedIpCount"] = usedIpCount;
            data["totalIpCount"] = usedIpCount + availableIpCount;

            this.AddJsEnv("ipRangeAction", data);
        }

        protected void ClickSave(object sender, EventArgs e) 
        { 
            editor.Save(true); 
        }

        void LoadIpCounts(out int availableIpCount, out int usedIpCount)
        {
            int subnetId = this.editor.SubnetId;
            Guid start = IPStorageHelper.IntoSqlGuid(txtStartingNetworkAddress.Text.Trim()).Value;
            Guid end = IPStorageHelper.IntoSqlGuid(txtEndingNetworkAddress.Text.Trim()).Value;

            string cmdString = String.Format(@"SELECT n.{0}, Count(n.{0}) AS StatusCount 
                FROM 
                    {1} n
                WHERE
                    n.{2} = '{3}'
                    AND n.{4} >= '{5}' 
                    AND n.{4} <= '{6}'
                GROUP BY
                    n.{0}
                ", IPNode.EIM_STATUS, IPNode.EIM_ENTITYNAME, IPNode.EIM_SUBNETID, subnetId, IPNode.EIM_IPADDRESSN, start, end);


            int available = 0;
            int used = 0;

            SwisDelegate worker = delegate(IpamClientProxy proxy)
            {
                DataTable table = proxy.SwisProxy.Query(cmdString);
                foreach (DataRow row in table.Rows)
                {
                    if (row[IPNode.EIM_STATUS] == DBNull.Value || row[IPNode.EIM_STATUS] == null)
                        continue;

                    IPNodeStatus status = (IPNodeStatus)Convert.ToInt32(row[IPNode.EIM_STATUS]);
                    int count = Convert.ToInt32(row["StatusCount"]);

                    if (status == IPNodeStatus.Available)
                        available += count;
                    else
                        used += count;
                }

            };

            UseSWIS("Loading IP counts", worker);

            availableIpCount = available;
            usedIpCount = used;

        }
		       
    }
}
