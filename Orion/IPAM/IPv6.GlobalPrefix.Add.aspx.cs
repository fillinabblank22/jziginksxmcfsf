﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class IPv6GlobalPrefixAdd : CommonPageServices
{
    #region Constructors

    private PageIPv6PrefixEditor Editor { get; set; }
    public IPv6GlobalPrefixAdd()
    {
        this.Editor = new PageIPv6PrefixEditor(this, true, GroupNodeType.GlobalPrefix);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.ParentId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // init prefix object
        PrefixAggregate prefix = this.Editor.RetrievePrefixObject();

        // postbacks will not update controls values
        if (this.Page.IsPostBack ==false)
        {
            InitControls(prefix);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(PrefixAggregate prefix)
    {
        // init global prefix address
        string fnAddressListener = string.Empty;
        if (this.PrefixAddress != null)
        {
            this.PrefixAddress.Init(
                "0000:0000:0000:0000::",
                GroupNodeType.GlobalPrefix,
                PageIPv6EditorHelper.InitGlobalPrefixSize,
                null, null, null, Editor.ClusterId);
            fnAddressListener = this.PrefixAddress.Listener;
        }

        // init prefix size control
        if (this.PrefixSize != null)
        {
            this.PrefixSize.Init(
                PageIPv6EditorHelper.MinPrefixSize,
                PageIPv6EditorHelper.MaxPrefixSize,
                PageIPv6EditorHelper.InitGlobalPrefixSize,
                fnAddressListener);
        }

        CustomPropertyRepeater.RetrieveCustomProperties(prefix, new List<int>(){ prefix.GroupId });
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        PrefixAggregate prefix = this.Editor.RetrievePrefixObject();
        if (prefix != null)
        {
            PrefixAggregate updatedPrefix = UpdateObject(prefix);
            this.Editor.Store(updatedPrefix);
        }
    }
    private PrefixAggregate UpdateObject(PrefixAggregate globalPrefix)
    {
        globalPrefix.GroupType = GroupNodeType.GlobalPrefix;

        if (this.txtName != null)
        {
            globalPrefix.FriendlyName = this.txtName.Text;
        }
        if (this.txtDescription != null)
        {
            globalPrefix.Comments = this.txtDescription.Text;
        }

        // NOTE: For global prefix case, the value and size
        // are identical as FullPrefix and FullSize.
        if (this.PrefixAddress != null)
        {
            string prefix = this.PrefixAddress.IPv6Value;
            if (prefix.IndexOf("::") < 0) prefix += "::";

            globalPrefix.FullPrefix = prefix;
            globalPrefix.Value = prefix;
        }
        if (this.PrefixSize != null)
        {
            int size = this.PrefixSize.PrefixSizeValue;
            globalPrefix.FullSize = size;
            globalPrefix.Size = size;
        }

        CustomPropertyRepeater.GetChanges(globalPrefix);

        return globalPrefix;
    }

    #endregion // Methods
}
