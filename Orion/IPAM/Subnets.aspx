﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Subnets.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.Subnets"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_3%>" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMcommon" Namespace="SolarWinds.IPAM.Web.Common" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" TagName="NavBar" Src="~/Orion/IPAM/Controls/NavigationTabBar.ascx" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<script type="text/javascript">function setFooter(){};</script>
<IPAMmaster:JsBlock requires="ext,sw-helpserver,sw-ux-wait.js,sw-dhcp-manage.js" Orientation=jsPostInit runat="server" />
<orion:Include runat="server" File="OrionCore.js" />

<script type="text/javascript">
$SW.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
$SW.SubnetScanningEnabled = <%= SubnetScanningEnabled.ToString().ToLower() %>;
$SW.LicenseListeners.push(function(lic){ $SW.subnet.recvlic(lic); });
$SW.ShowParentChangeConfirmDlg = <%= ShowParentChangeConfirmDlg.ToString().ToLower() %>;
</script>

<IPAMmaster:CssBlock runat="server">
body #content { padding-bottom: 0px; }
body #footer { position: static; border: none; }
.warningbox a { text-decoration: underline; }
h2 { font-size: large; font-family: Arial,Helvetica,Sans-Serif; font-weight: normal; }

#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
.rptlegend .rptcount { width: 64px; text-align: right; padding: 4px 0px 4px 0px; }
.rptlegend .rptlabel { padding: 4px 0px 4px 28px; background-position: left; }
.rptlegend .rptcount span, .rptlegend .rptlabel div { padding-right: 4px; }
.rptlegend .rptcount span { padding-left: 6px; }
.rptlegend .rptsum .rptcount { padding-top: 7px; }
.rptlegend .rptsum .rptlabel { padding-left: 0px; }
.rptlegend .rptsum span, .rptlegend .rptsum div { border-top: 3px double black; }

#ipcount { font-size: 11px; position: absolute; right: 0px; bottom: 2px; width: 250px; padding: 2px 4px 2px 2px; z-index: 150001; text-align: right; overflow: vislble; white-space: nowrap; }
#ipcount .ipcountlabel { font-weight: bold; }
#ipcount.warn .ipcountcurrent { color: #ffae00; font-weight: bold; }
#ipcount.critical .ipcountcurrent { color: #e75d5a; x-blah: #ffae00; font-weight: bold; }

.ext-indent {padding: 0px 10px !important; background:none repeat scroll 0 0 #FAF9F5 !important; }

/* IEv6 and IEv7 SearchBox fix .. in this order */
html > body .searchBoxCls { top: 0spx !important; }
* html .searchBoxCls { top: 0px !important; }
.x-grid3-cell-inner a, .x-grid3-cell-inner a:active, .x-grid3-cell-inner a:visited {color: #000000 !important; }

.x-tree-node-collapsed .x-tree-node-icon, .x-tree-node-expanded .x-tree-node-icon, .x-tree-node-leaf .x-tree-node-icon {width: 27px;}
.x-menu-item-icon {width: 27px; background-repeat: no-repeat;}
.x-menu-item-icon + span.x-menu-item-text {margin-left: 5px;}
</IPAMmaster:CssBlock>


<IPAMmaster:JsBlock requires="ext-quicktips,sw-ipv6-manage.js,sw-subnet-manage.js" Orientation=jsPostInit runat="server">
Ext.onReady(function(){ $('#container').hide(); });

/* called when we insert an eval banner */

$SW.ReflowPage = function(){
var hdr = Ext.getCmp('headerlayout');
hdr.setHeight( Ext.get('content').getHeight() );
hdr.syncSize();
Ext.getCmp('mainlayout').doLayout();
};
</IPAMmaster:JsBlock>
    <div id="tab2" style="padding: 8px;" class='x-hide-display'>
    <h2 id='reportheading'>&nbsp;</h2>
    <div id="reportsummary">

        <div id='reportloading' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
            <img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/default/tree/loading.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_207%>
        </div>

        <div id='reportfailure' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
            <img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/sw/icon.warning.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_208 %>
        </div>

        <div id='Div1' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
            <img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/sw/icon.warning.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_209%>
        </div>

        <div id='reportcontent' style="padding-left: 300px;" class="rptlegend">
            <div style="position: relative;"><img id='reportimage' style="position: absolute; margin-left: -300px;" xW="300" xH="280" src="/orion/ipam/res/images/default/s.gif" border=0></div>
            <div>
                <table style="height: 280px; width: 100%;"><tr><td valign="middle" id='reportlegend'></td></tr></table>
            </div>
        </div>
    </div>
  </div>

  <div id="warningbox" class="warningbox" runat="server">
    <div class=inner>
        <b><span id="warningItems" runat="server">#</span></b><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_211, "", "<b>", "</b>")%><a href="Import.SubnetManagement.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_210 %></a>
    </div>
  </div>

<table width="100%" style="table-layout: fixed; height: 55px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2">
	 <div style="padding-left: 10px;"><h1><%=Page.Title%></h1></div>
    </td>
    <td class="links" width="auto" style="padding-right: 10px; padding-top: 10px;">
       <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
         { %>
         <a class="HelpText" href="/Orion/IPAM/Admin/Admin.Overview.aspx" style="background: transparent url(/Orion/IPAM/res/images/sw/Page.Icon.MdlSettings.gif) scroll no-repeat left center; padding: 2px 0px 2px 20px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a>
       <%}%>       
       <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHViewManageSubnetsIPAddressesNetwork" />
    </td>
  </tr>
  <tr>
    <td align="right" style="padding-right: 10px; padding-bottom: 2px">
      <IPAMui:SearchBox cssClass="searchBoxCls" id="searchbox" emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_212 %>" runat="server" listeners="{beforerender:function(that){$SW.SearchInit(that,$SW.SearchInfo)}}">
        <handler>$SW.SearchGo</handler>
        <Menu ID="Menu1" runat="server"  />
      </IPAMui:SearchBox>
      <IPAMcommon:SearchState ID="SearchState1" runat="server" />
    </td>
  </tr>
</table>

<IPAMlayout:DialogWindow jsID="mainWindow" helpID="subnetedit" Name="mainWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_213 %>" height="400" width="550" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="mainWindowSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ 
            if ($SW.IsDemoServer) { demoAction('IPAM_Subnet_Manage', null); return false; }
            $SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['mainWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="close" runat=server>
        <handler>function(n,o,info){ $SW['mainWindow'].hide(); $SW.subnet.UiTaskComplete(n,o,info, function(){ $SW.subnet.RefreshGridPage(); }); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="license" runat=server>
        <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="refresh" runat=server>
        <handler>function(n,o,info){
         window.location = 'subnets.aspx?opento=' + info.id; 
         }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMMaster:WindowMsg Name="addips" runat="server">
        <handler>function(n,o,info){ $SW.subnet.addiprangecomplete(info); }</handler>
    </IPAMMaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:DialogWindow jsID="selectRangeWindow" helpID="subnetedit" Name="selectRangeWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_214 %>" height="250" width="550" runat="server">
<Buttons>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_215 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['selectRangeWindow'], 'dialog', 'edit' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_216 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['selectRangeWindow'], 'dialog', 'remove' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['selectRangeWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['selectRangeWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['selectRangeWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="close" runat=server>
        <handler>function(n,o){ $SW['selectRangeWindow'].hide();  $SW.subnet.RefreshTab(true); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="license" runat=server>
        <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="refresh" runat=server>
        <handler>function(n,o,info){ window.location = 'subnets.aspx?opento=' + info.id; }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMMaster:WindowMsg Name="rangeSelected" runat="server">
        <handler>function(n,o,info){ $SW.subnet.selectIpRangeForEditRemoveComplete(info); }</handler>
    </IPAMMaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:DialogWindow jsID="multiEditWindow" helpID="subnetedit" Name="multiEditWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_217 %>" height="600" width="620" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="ToolStripButton1" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ 
            if ($SW.IsDemoServer) { demoAction('IPAM_Subnet_MultiEdit', null); return false; }
            $SW.msgq.DOWNSTREAM( $SW['multiEditWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="ToolStripButton2" text="<%$ Resources :IPAMWebContent, IPAMWEBDATA_VB1_142  %>" runat="server">
        <handler>function(){ $SW['multiEditWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg ID="WindowMsg1" Name="ready" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['multiEditWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg2" Name="unload" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['multiEditWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg3" Name="close" runat=server>
        <handler>function(n,o,info){ $SW['multiEditWindow'].hide(); $SW.subnet.EditManyComplete(info); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg4" Name="license" runat=server>
        <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg5" Name="refresh" runat=server>
        <handler>function(n,o,info){ window.location = 'subnets.aspx?opento=' + info.id; }</handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:DialogWindow jsID="deleteConfirm" helpID="deletehelp" Name="deleteConfirm" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_218 %>" height="400" width="480" runat="server">
<Buttons>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_219 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['deleteConfirm'], 'dialog', 'delete' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['deleteConfirm'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['deleteConfirm'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['deleteConfirm'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="license" runat=server>
        <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="close" runat=server>
        <handler>function(n,o){ $SW['deleteConfirm'].hide(); $SW.subnet.delcurrentdone(); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="confirmed" runat=server>
        <handler>function(n,o,info){ $SW.subnet.deletecomplete(info); }</handler>
    </IPAMmaster:WindowMsg></Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:ViewPort id="viewport" runat="server">
    <IPAMlayout:BorderLayout id="mainlayout" runat="server">
    <North>
        <IPAMlayout:BoxComponent id="headerlayout" layout="fit" runat="server" borderVisible="false" clientEl="content" />
    </North>
    <Center>
        <IPAMlayout:BorderLayout cssClass="ext-indent" runat="server" borderVisible="false">
            <West>
            <IPAMlayout:TreeGrid id="treeSubnet" runat="server"
                title="&amp;nbsp;" 
                MultipleSelection="true"
                borderVisible="true"
                autoScroll="false"
                EnableDragAndDrop="true"
                EnableSort="false"
                cssClass="test"
                split="true"
                collapsible="true"
                width="310"
                Plugins="new Ext.ux.plugins.TreeGridState(),
                         new Ext.ux.plugins.TreeGridMultiSelectDragDrop()"
                listeners="$SW.subnet.treelisteners()">

            <Store ID="TestBoxStore" runat="server"
                proxyEntity="IPAM.ManageSubnetsAndIps"
                limit="150"
                proxyUrl="/Orion/IPAM/Services/Information.asmx/LoadSubnetTreeData"
                >
            <fnRemap>
            function(node,rows){
                var temp = null;
                var fountAtIdex = -1;
                var opento = -1;
                var sel = $SW['treegridInitialselection'];
                var regex = new RegExp("\/Date.([0-9]+)([+\-]\\d{4})?.+\/");
                var renderDateTime = Ext.util.Format.dateRenderer($SW.ExtLocale.DateFull);
                var nodeHasCustomProperty = node.getOwnerTree().columns.filter(function (item) {
                    return item.dataIndex !== 'text' && item.dataIndex !== 'Comments';
                }).length;

                if (sel && sel.groupId)
                    opento = sel.groupId;

                 jQuery.each( rows, function(idx,row){
                    if (nodeHasCustomProperty) {
                        for (var key in row) {
                            if (regex.test(row[key])){
                                var value = new Date(parseInt(row[key].substr(6)));
                                row[key] = renderDateTime(value);
                            }
                        }
                    }
					if(row.text)
                        row.text = Ext.util.Format.htmlEncode(row.text);
                    if(row.Comments)
                        row.Comments = Ext.util.Format.htmlEncode(row.Comments);
                    row.id = ''+row.GroupId;
                    row.leaf = (row.t == 8) || (row.t == 512);
                    row.uiProvider = 'col';
                    if( row.t != 1 && row.t != 2  )
                        row.iconCls = row.GroupIconPrefix+'-'+row.StatusIconPostfix;
                    if (row.Role == 'NoAccess') row.cls = 'sw-treenode-noaccess';
                        row.draggable = $SW.subnet.canChangeParent(row.Role);
                
                    if (opento == row.GroupId && temp == null)
                    {
                        temp = row;
                        fountAtIdex = idx;
                    }
                 });

                var q = node.getOwnerTree().getLoader().query;
                if( q && q.limit && (q.limit < rows.length) )
                {
                    rows.length = q.limit;
                    if(fountAtIdex >= q.limit && temp)
                        rows.push(temp);

                    rows.push({ leaf: true, text: "&nbsp;" + q.limit + " items reached.", iconCls: "tree-limit", t: -1, qtip: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_33;E=js}" });
                    };

                return rows;
            }
            </fnRemap>
            <fnQueryBuilder>
            function(node,loader){
                var w_root = "{0}.GroupType = 1";
                var w_normal = "{0}.Distance = 1 AND {0}.AncestorId = " + node.attributes.id;
                loader.query.where = node.isRoot ? w_root : w_normal;

                var sel = $SW['treegridInitialselection'];
                if (sel && sel.parentId == node.attributes.id)
                    loader.query.union = " AND {0}.GroupId = " + sel.groupId;
                else
                    loader.query.union = null;
                
                var query = node.isRoot ? $SW.SwqlJoinBuild( node, loader ) : $SW.SWQLbuild( node, loader );
                if(!query.unionQuery)
                    $.extend(query, { unionQuery: '' });

                return query;
            }
            </fnQueryBuilder>
            <Fields>
                <IPAMlayout:TreeStoreField dataIndex="GroupId" isKey="true" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="t" clause="{0}.GroupType" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="text" clause="{0}.FriendlyName" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="cidr" clause="{0}.CIDR" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="Comments" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ClusterId" clause="{0}.ClusterId" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="GroupIconPrefix" runat="server" />  
                <IPAMlayout:TreeStoreField dataIndex="StatusIconPostfix" runat="server" />  
                <IPAMlayout:TreeStoreField dataIndex="Role" runat="server" />
            </Fields>
            </Store>
            <Columns>
              <IPAMlayout:TreeColumn width="300" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_220 %>" DataIndex="text" Visible="true" runat="server" />
              <IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_221 %>" DataIndex="Comments" HideInGrid="false" Visible="true" runat="server" />
            </Columns>
            <TopMenu>
            <IPAMui:ToolStripMenu id="BtnAddMenu" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_68 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_222 %>" iconCls="mnu-add" ExceptDemoMode="true" disabled="true" runat="server">
                <IPAMui:ToolStripMenuItem id="btnAddVrfGroup" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_13 %>" iconCls="mnu-group" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.add(that,1); }</handler>
                </IPAMui:ToolStripMenuItem>
                <IPAMui:ToolStripMenuItem id="btnAddGroup" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_223 %>" iconCls="mnu-group" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.add(that,2); }</handler>
                </IPAMui:ToolStripMenuItem>
                <IPAMui:ToolStripMenuItem id="btnAddSupernet" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_224 %>" iconCls="mnu-supernet" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.add(that,4); }</handler>
                </IPAMui:ToolStripMenuItem>
                <IPAMui:ToolStripMenuItem id="btnAddSubnet" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_225 %>" iconCls="mnu-subnet" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.add(that,8); }</handler>
                </IPAMui:ToolStripMenuItem>

                <IPAMui:ToolStripSeparator runat="server" />

                <IPAMui:ToolStripMenuItem id="btnAddSubnetWizard" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_226 %>" iconCls="mnu-wizard" ExceptDemoMode="true" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.wizard(); }</handler>
                </IPAMui:ToolStripMenuItem>

                <IPAMui:ToolStripSeparator runat="server" />

                <IPAMui:ToolStripMenuItem id="btnAddGlobalPrefix" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_227 %>" iconCls="mnu-gprefix6" ExceptDemoMode="true" disabled="true" runat="server">
                    <handler>function(that){ $SW.IPv6Handlers.GlobalPrefixAdd(that); }</handler>
                </IPAMui:ToolStripMenuItem>
                <IPAMui:ToolStripMenuItem id="btnAddPrefix" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_228  %>" iconCls="mnu-prefix6" ExceptDemoMode="true" disabled="true" runat="server">
                    <handler>function(that){ $SW.IPv6Handlers.PrefixAdd(that); }</handler>
                </IPAMui:ToolStripMenuItem>
                <IPAMui:ToolStripMenuItem id="btnAddSubnetIPv6" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_229 %>" iconCls="mnu-subnet6" ExceptDemoMode="true" disabled="true" runat="server">
                    <handler>function(that){ $SW.IPv6Handlers.SubnetAdd(that); }</handler>
                </IPAMui:ToolStripMenuItem>

            </IPAMui:ToolStripMenu>

            <IPAMui:ToolStripSeparator runat="server" />

            <IPAMui:ToolStripButton id="BtnEditCurrent" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"  toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_230 %>" iconCls="mnu-edit" disabled="true" runat="server">
                <handler>function(){ $SW.subnet.properties('BtnEditCurrent'); }</handler>
            </IPAMui:ToolStripButton>

            <IPAMui:ToolStripSeparator runat="server" />

            <IPAMui:ToolStripButton id="BtnDeleteCurrent" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" RoleAclOneOf="IPAM.PowerUser" toolTip="<%$  Resources : IPAMWebContent, IPAMWEBDATA_VB1_231 %>" iconCls="mnu-delete" disabled="true" runat="server">
                <handler>$SW.subnet.delcurrent</handler>
            </IPAMui:ToolStripButton>

            </TopMenu>
            </IPAMlayout:TreeGrid>
               
            </West>
            <Center>
                <IPAMlayout:TabLayout id="ipam_tabs" runat="server" cssClass="sw-tabpanel-top" tabPosition="top" activeTab="0" borderVisible="false" listeners="$SW.subnet.tablisteners()">

                <IPAMlayout:GridBox
                    runat="server"
                    id="tabGroup"
                    autoHeight="false"
                    title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_299 %>"
                    UseLoadMask="True"
                    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_232 %>"
                    borderVisible="true"
                    deferEmptyText="false"
                    listeners="{ cellclick: $SW.ext.gridClickToDrilldown, drilldown: $SW.subnet.groupdbleclick, statesave: $SW.subnet.GridColumnChangedHandler }">
                <ConfigOptions runat="server">
                    <Options>
                        <IPAMlayout:ConfigOption Name="plugins" Value="[
new Ext.ux.plugins.DhcpScopesTTip({}),
new $SW.subnet.tabGridPlugin() ]" IsRaw="true" runat="server" />
                    </Options>
                </ConfigOptions>

                <TopMenu borderVisible="false">

                <IPAMui:ToolStripButton id="grpEdit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_233 %>"  ExceptDemoMode="true"  iconCls="mnu-edit" disabled="true" runat="server">
                    <handler>function(that){ $SW.subnet.edit(that); }</handler>
                </IPAMui:ToolStripButton>
                    
                <IPAMui:ToolStripSeparator runat="server" />

                <IPAMui:ToolStripButton ID="btnNeighbourDiscovery" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MK0_2 %>" iconCls="mnu-scan-ndp" runat="server" disabled="true" ExceptDemoMode="true">
                    <handler>
                        $SW.subnet.ndpdiscovery
                    </handler>
                </IPAMui:ToolStripButton>
                <IPAMui:ToolStripButton id="grpDiscoverSubnet" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SV1_2  %>" iconCls="mnu-scan-ndp" disabled="true" runat="server" ExceptDemoMode="true">
                    <handler>$SW.subnet.discoverSubnet</handler>
                </IPAMui:ToolStripButton>
                <IPAMui:ToolStripSeparator runat="server" />
                
                <IPAMui:ToolStripMenu id="grpImport" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_234  %>" iconCls="mnu-import" disabled="true" runat="server" ExceptDemoMode="true">
                    <IPAMui:ToolStripMenuItem id="grpImportSheet" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_235 %>" iconCls="mnu-importIps" disabled="true" runat="server">
                        <handler>$SW.subnet.importSpreadsheet</handler>
                    </IPAMui:ToolStripMenuItem>
                    <IPAMui:ToolStripMenuItem id="grpBulkImport" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_236  %>" iconCls="mnu-importSubnets" disabled="true" runat="server">
                        <handler>$SW.subnet.bulkAdd</handler>
                    </IPAMui:ToolStripMenuItem>
                    
                </IPAMui:ToolStripMenu>

                <IPAMui:ToolStripMenu id="grpExport" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_237  %>" iconCls="mnu-export" disabled="true" runat="server" ExceptDemoMode="true">
                    <IPAMui:ToolStripMenuItem id="grpExportAll" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_912 %>" iconCls="mnu-export" disabled="true" runat="server">
                        <handler>$SW.subnet.exporterStructureStep</handler>
                    </IPAMui:ToolStripMenuItem>
                    <IPAMui:ToolStripMenuItem id="grpExportStructureOnly" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_915  %>" iconCls="mnu-export" disabled="true" runat="server">
                        <handler>$SW.subnet.exportStructureOnly</handler>
                    </IPAMui:ToolStripMenuItem>
                </IPAMui:ToolStripMenu>

                <IPAMui:ToolStripSeparator runat="server" />

                <IPAMui:ToolStripButton id="grpDelete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_238 %>" iconCls="mnu-delete" disabled="true" runat="server">
                    <handler>$SW.subnet.del</handler>
                </IPAMui:ToolStripButton>

                </TopMenu>
                <PageBar pageSize="100" displayInfo="true" />
                <Columns>
                    <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" dataIndex="GroupType" runat="server" renderer="$SW.subnet.grpStatusImgRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_239 %>" width="120" isSortable="true" dataIndex="FriendlyName" renderer="$SW.ext.gridRendererDrilldown()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="90" isSortable="true" dataIndex="Address" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_241 %>" width="40" isSortable="true" dataIndex="CIDR" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_242 %>" width="90" isSortable="true" dataIndex="AddressMask" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_243 %>" width="120" isSortable="true" dataIndex="LastDiscovery" renderer="$SW.ext.DateRenderer($SW.ExtLocale.DateShort)" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_244 %>" width="80" isSortable="true" dataIndex="Location" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_245 %>" width="80" isSortable="true" dataIndex="VLAN" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_246 %>" width="80" isSortable="true" dataIndex="Comments" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_247%>"  width="80" isSortable="true" dataIndex="PercentUsed" renderer="$SW.subnet.ipUsageRenderer(2)" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_248 %>" width="80" isSortable="true" dataIndex="TotalCount" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_249 %>" width="60" isSortable="true" dataIndex="UsedCount" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_250 %>" width="80" isSortable="true" dataIndex="AvailableCount" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_251 %>" width="80" isSortable="true" dataIndex="ReservedCount" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_252 %>" width="80" isSortable="true" dataIndex="TransientCount" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_6 %>"   width="80" isSortable="false" dataIndex="DhcpScopes" renderer="$SW.subnet.DhcpScopesRenderer()" runat="server"/>
                </Columns>
                  <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="GroupType, AddressN, FriendlyName" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server"
                      >
                    <Fields>
                        <IPAMlayout:GridStoreField dataIndex="GroupId" isKey="true" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="GroupType" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="FriendlyName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Address" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="AddressMask" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="LastDiscovery" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="CIDR" dataType="int" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="PercentUsed" dataType="float" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="UsedCount" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="AvailableCount" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ReservedCount" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="TransientCount" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="TotalCount" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="VLAN" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusIconPostfix" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusShortDescription" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="GroupTypeText" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="GroupIconPrefix" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Role" runat="server" />  
                        <IPAMlayout:GridStoreField dataIndex="DhcpScopes" runat="server" />
                    </Fields>
                  </Store>
                </IPAMlayout:GridBox>

                <IPAMlayout:GridBox runat="server"
                    id="tabSubnet"
                    title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_300 %>"
                    UseLoadMask="True"
                    autoHeight="false"
                    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_253 %>"
                    borderVisible="true"
                    deferEmptyText="false"
                    listeners="{ statesave: $SW.subnet.GridColumnChangedHandler }">
                <ConfigOptions ID="ConfigOptions1" runat="server">
                    <Options>
                        <IPAMlayout:ConfigOption Name="plugins" Value="[ new $SW.subnet.tabGridPlugin() ]" IsRaw="true" runat="server" />
                    </Options>
                </ConfigOptions>
                <TopMenu borderVisible="false">

                    <IPAMui:ToolStripMenuItem id="ipv4AddRange" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_254 %>" ExceptDemoMode="true" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_255 %>" iconCls="mnu-add" disabled="true" runat="server">
                        <handler>$SW.subnet.addiprange</handler>
                    </IPAMui:ToolStripMenuItem>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripButton id="ipv4Edit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" ExceptDemoMode="true" iconCls="mnu-edit" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_256 %>" disabled="true" runat="server">
                        <handler>function(that){ $SW.subnet.editip(that); }</handler>
                    </IPAMui:ToolStripButton>
                    
                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_257 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_258 %>" iconCls="mnu-details" runat="server">
                        <handler>function(that){ $SW.subnet.ipdetails(that); }</handler>
                    </IPAMui:ToolStripButton>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripText text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_259 %>" runat="server" />

                    <IPAMui:ToolStripMenu id="futureFilter" iconCls="mnu-all-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" runat="server">
                        <IPAMui:ToolStripMenuItem id="ffAll" iconCls="mnu-all-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffAvail" iconCls="mnu-avail-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffTransient" iconCls="mnu-transient-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_262 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffUsed" iconCls="mnu-used-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_263 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffRsvd" iconCls="mnu-usedrsvd-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffBlocked" iconCls="mnu-blocked-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_847 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                          <IPAMui:ToolStripMenuItem id="ffConflicts" iconCls="mnu-conflict-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DF1_44 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffDynamic" iconCls="mnu-dhcpscope-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_265 %>" group="filter" runat="server">
                            <handler>$SW.subnet.SetGridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripSeparator runat="server" />
                    
                    <IPAMui:ToolStripMenuItem id="ipv4SelectRange" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_214 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_266 %>" iconCls="mnu-check" disabled="true" runat="server">
                        <handler>$SW.subnet.selectIpRangeForEditRemove</handler>
                    </IPAMui:ToolStripMenuItem>
                    
                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripSplitButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_267 %>" iconCls="mnu-scan" runat="server">
                        <handler>function(e){
var s = Ext.getCmp('ipv4ScanNow'), v = Ext.getCmp('ScanViewStatus');
return (s.disabled == false) ? s.handler(e) : v.handler(e); } </handler>
                    <Menu runat="server">
                        <IPAMui:ToolStripMenuItem id="ipv4ScanNow" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_268 %>" iconCls="mnu-scan" disabled="true" runat="server">
                            <handler>$SW.subnet.scanme</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ScanViewStatus" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_269 %>" RoleAclOneOf="IPAM.Any" iconCls="mnu-scan-list" runat="server">
                            <handler>function(e){ window.location = "subnet.scanstatus.aspx"; }</handler>
                        </IPAMui:ToolStripMenuItem>
                    </Menu>
                    </IPAMui:ToolStripSplitButton>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripMenu ID="ipv4SetStatus" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_270 %>" iconCls="mnu-setstatus" disabled="true" runat="server">
                        <IPAMui:ToolStripMenuItem id="ipv4Available" iconCls="ip-avail" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'available',false); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv4Used" iconCls="ip-used" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_263 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'used',false); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv4Transient" iconCls="ip-transient" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_262 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'transient',false); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv4Reserved" iconCls="ip-usedrsvd" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'reserved',false); }</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripMenu id="ipv4Import" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_234 %>" iconCls="mnu-import" disabled="true" runat="server">
                        <IPAMui:ToolStripMenuItem id="ipv4IPImport" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_5 %>" iconCls="mnu-importIps" disabled="true" runat="server">
                            <handler>$SW.subnet.importWithRestriction</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv4BulkImport" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_236 %>" iconCls="mnu-importSubnets" disabled="true" runat="server">
                            <handler>$SW.subnet.bulkAdd</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripMenu id="ipv4Export" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_237  %>" iconCls="mnu-export" disabled="true" runat="server" ExceptDemoMode="true">
                        <IPAMui:ToolStripMenuItem id="ipv4ExportAll" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_912 %>" iconCls="mnu-export" disabled="true" runat="server">
                            <handler>$SW.subnet.exporterStructureStep</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv4ExportStructureOnly" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_915  %>" iconCls="mnu-export" disabled="true" runat="server">
                            <handler>$SW.subnet.exportStructureOnly</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripMenuItem id="ipv4Delete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_271 %>" iconCls="mnu-delete" disabled="true" runat="server">
                        <handler>$SW.subnet.unmanageip</handler>
                    </IPAMui:ToolStripMenuItem>

                </TopMenu>
                <PageBar pageSize="100" displayInfo="true" prependButtons="true" listeners="$SW.subnet.pgtoolbarlisteners()">
                    <IPAMui:ToolStripText hidden=true text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_272 %>" runat="server" />
                    <IPAMui:ToolStripButton id='currentFilter' hidden=true iconCls="ip-avail" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" runat="server">
                    <handler>function(){
                        Ext.MessageBox.confirm(
                            'Remove Filter',
                            'Are you sure you want to remove the filter?',
                                function(btn){ if( btn == 'yes' ) $SW.subnet.SetGridFilter(Ext.getCmp('ffAll')); }
                        );}
                    </handler>
                    </IPAMui:ToolStripButton>
                    <IPAMui:ToolStripSeparator hidden=true runat="server" />
                </PageBar>
                <Columns>
                    <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" dataIndex="Status" runat="server" renderer="$SW.subnet.ipStatusImgRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="120" isSortable="true" dataIndex="IPAddress" renderer="$SW.ext.ipAddressToolsetIntegration" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_158 %>" width="66" isSortable="true" dataIndex="Status"  renderer="$SW.subnet.ipStatusRenderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_273 %>" width="120" isSortable="true" isHidden="false" dataIndex="IPMapped" renderer="$SW.subnet.ipMapped6Renderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_274 %>" width="80" isSortable="true" isHidden="false" dataIndex="MAC" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_155 %>" width="100" isSortable="true" isHidden="false" dataIndex="Vendor" runat="server" renderer="$SW.subnet.iconImgRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_275 %>" width="130" isSortable="false" isHidden="false" runat="server" dataIndex="AllocPolicy" renderer="$SW.subnet.ipScopeNameRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_276 %>" width="151" isSortable="true" isHidden="true" dataIndex="LeaseExpires" runat="server" renderer="$SW.subnet.ipDaysRenderer(0)" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_277 %>" width="123" isSortable="true" isHidden="true" dataIndex="LeaseExpires" runat="server" renderer="$SW.subnet.ipLeaseExpires($SW.ExtLocale.DateFull)" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_278 %>" width="120" isSortable="true" isHidden="true" dataIndex="Alias" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_93%>" width="86" isSortable="true" isHidden="true" dataIndex="AllocPolicy" runat="server" renderer="$SW.subnet.ipTypeRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_279 %>" width="86" isSortable="true" isHidden="true" dataIndex="SkipScan" runat="server" renderer="$SW.subnet.ipScanRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_280 %>" width="120" isSortable="true" dataIndex="DnsBackward" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_281 %>" width="130" isSortable="true" dataIndex="DhcpClientName" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_282 %>" width="120" isSortable="true" dataIndex="DhcpLease" runat="server" renderer="$SW.subnet.ipDhcpLeaseRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_283 %>" width="120" isSortable="true" dataIndex="LastSync" renderer="$SW.subnet.ipLastResponse($SW.ExtLocale.DateShort)" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_284 %>" width="120" isSortable="true" isHidden="true" dataIndex="MachineType" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_285%>" width="120" isSortable="true" isHidden="true" dataIndex="sysName" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_286 %>" width="120" isSortable="true" dataIndex="Description" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_287 %>" width="120" isSortable="true" dataIndex="Location" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_288 %>" width="120" isSortable="true" dataIndex="Contact" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_289 %>" width="120" isSortable="true" dataIndex="ResponseTime" renderer="$SW.subnet.ipResponseTimeRenderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_290 %>" width="120" isSortable="true" dataIndex="Comments" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_291 %>" width="120" isSortable="true" dataIndex="DnsRecords" runat="server" renderer="$SW.subnet.ipDnsRecordsRenderer()" />
                </Columns>
                  <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.IPNodeGrid" AmbientSort="IPAddressN" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                    <Fields>
                        <IPAMlayout:GridStoreField dataIndex="IPOrdinal" isKey="true" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IpNodeId" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddress" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Alias" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="MAC" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="MachineType" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Vendor" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="VendorIcon" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="sysName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPMapped" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Status" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="AllocPolicy" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="SkipScan" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DnsBackward" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DhcpClientName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DhcpLease" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="LastSync" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Description" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Contact" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ResponseTime" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="LeaseExpires" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="SubnetId6" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddress6" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ScopeId" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ScopeName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DnsRecords" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IsConflict" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ConflictTypeIcon" runat="server" />
                    </Fields>
                  </Store>
                </IPAMlayout:GridBox>

                <IPAMlayout:GridBox runat="server"
                    id="tabIPv6Subnet"
                    title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_301 %>"
                    UseLoadMask="True"
                    autoHeight="false"
                    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_292 %>"
                    borderVisible="true"
                    deferEmptyText="false"
                    listeners="{ statesave: $SW.subnet.GridColumnChangedHandler }">
                <ConfigOptions ID="ConfigOptions2" runat="server">
                    <Options>
                        <IPAMlayout:ConfigOption Name="plugins" Value="[ new $SW.subnet.tabGridPlugin() ]" IsRaw="true" runat="server" />
                    </Options>
                </ConfigOptions>
                <TopMenu borderVisible="false">
                    <IPAMui:ToolStripButton id="ipv6AddIP" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_293 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_294 %>" iconCls="mnu-add" disabled="true" runat="server">
                        <handler>function(that){ $SW.IPv6Handlers.IPAddressAdd(that); }</handler>
                    </IPAMui:ToolStripButton>
                    <IPAMui:ToolStripSeparator runat="server" />
                    <IPAMui:ToolStripButton id="ipv6Edit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_295 %>" iconCls="mnu-edit" disabled="true" runat="server">
                        <handler>function(that){ $SW.IPv6Handlers.IPAddressEdit(that); }</handler>
                    </IPAMui:ToolStripButton>
                    <IPAMui:ToolStripSeparator runat="server" />
                    <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_257 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_258 %>" iconCls="mnu-details" runat="server">
                        <handler>function(that){ $SW.IPv6Handlers.IPAddressDetails(that); }</handler>
                    </IPAMui:ToolStripButton>
                    
                    <IPAMui:ToolStripSeparator ID="ToolStripSeparator1" runat="server" />
                    
                    <IPAMui:ToolStripText text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_259 %>" runat="server" />
                    
                    <IPAMui:ToolStripMenu id="ipv6Filter" iconCls="mnu-all-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" runat="server">
                        <IPAMui:ToolStripMenuItem id="ffIPv6All" iconCls="mnu-all-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" group="filter" runat="server">
                            <handler>$SW.IPv6.SetIPv6GridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffIPv6Avail" iconCls="mnu-avail-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>" group="filter" runat="server">
                            <handler>$SW.IPv6.SetIPv6GridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffIPv6Transient" iconCls="mnu-transient-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_262 %>" group="filter" runat="server">
                            <handler>$SW.IPv6.SetIPv6GridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffIPv6Used" iconCls="mnu-used-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_263 %>" group="filter" runat="server">
                            <handler>$SW.IPv6.SetIPv6GridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ffIPv6Rsvd" iconCls="mnu-usedrsvd-filter" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>" group="filter" runat="server">
                            <handler>$SW.IPv6.SetIPv6GridFilter</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripSplitButton ID="ToolStripSplitButton1" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_267 %>" iconCls="mnu-scan" runat="server">
                        <handler>function(e){
                            var s = Ext.getCmp('ipv6ScanNow'), v = Ext.getCmp('ipv6ScanViewStatus');
                            return (s.disabled == false) ? s.handler(e) : v.handler(e); } 
                        </handler>
                        <Menu ID="ipv6ScanMenu" runat="server">
                            <IPAMui:ToolStripMenuItem id="ipv6ScanNow" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_268 %>" iconCls="mnu-scan" disabled="true" runat="server">
                                <handler>$SW.subnet.scanme</handler>
                            </IPAMui:ToolStripMenuItem>
                            
                            <IPAMui:ToolStripMenuItem id="ipv6ScanViewStatus" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_269 %>" RoleAclOneOf="IPAM.Any" iconCls="mnu-scan-list" runat="server">
                                <handler>function(e){ window.location = "subnet.scanstatus.aspx"; }</handler>
                            </IPAMui:ToolStripMenuItem>
                        </Menu>
                    </IPAMui:ToolStripSplitButton>

                    <IPAMui:ToolStripButton ID="ipv6NeighbourDiscovery" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MK0_2 %>" iconCls="mnu-scan-ndp" runat="server" disabled="true" ExceptDemoMode="true">
                    <handler>
                        $SW.subnet.ndpdiscovery
                    </handler>
                    </IPAMui:ToolStripButton>

                    <IPAMui:ToolStripSeparator runat="server" />
                    
                    <IPAMui:ToolStripMenu ID="ipv6SetStatus" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_270 %>" iconCls="mnu-setstatus" disabled="true" runat="server">
                        <IPAMui:ToolStripMenuItem id="ipv6Available" iconCls="ip-avail" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'available',true); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv6Used" iconCls="ip-used" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_263 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'used',true); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv6Transient" iconCls="ip-transient" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_262 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'transient',true); }</handler>
                        </IPAMui:ToolStripMenuItem>
                        <IPAMui:ToolStripMenuItem id="ipv6Reserved" iconCls="ip-usedrsvd" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>" disabled="true" runat="server">
                            <handler>function(el){ return $SW.subnet.setstatus(el,'reserved',true); }</handler>
                        </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>

                    <IPAMui:ToolStripSeparator runat="server" />

                    <IPAMui:ToolStripButton  id="ipv6Import" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_234 %>" toolTip="<%$  Resources : IPAMWebContent, IPAMWEBDATA_TM0_5 %>" iconCls="mnu-import" disabled="true" runat="server">
                        <handler>function(that){ $SW.IPv6Handlers.IPsImport(that); }</handler>
                    </IPAMui:ToolStripButton>
                    <IPAMui:ToolStripMenu id="Ipv6Export" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_237  %>" iconCls="mnu-export" disabled="true" runat="server" ExceptDemoMode="true">
                            <IPAMui:ToolStripMenuItem id="Ipv6ExportAll" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_912 %>" iconCls="mnu-export" disabled="true" runat="server">
                                <handler>$SW.subnet.exporterStructureStep</handler>
                            </IPAMui:ToolStripMenuItem>
                            <IPAMui:ToolStripMenuItem id="Ipv6ExportStructureOnly" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_915  %>" iconCls="mnu-export" disabled="true" runat="server">
                                <handler>$SW.subnet.exportStructureOnly</handler>
                            </IPAMui:ToolStripMenuItem>
                    </IPAMui:ToolStripMenu>
                    <IPAMui:ToolStripSeparator runat="server" />
                    <IPAMui:ToolStripButton id="ipv6Delete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_296 %>" iconCls="mnu-delete" disabled="true" runat="server">
                        <handler>function(that){ $SW.IPv6Handlers.IPAddressRemove(that); }</handler>
                    </IPAMui:ToolStripButton>
                </TopMenu>
                <PageBar pageSize="100" displayInfo="true" prependButtons="true" listeners="$SW.IPv6.ToolbarListeners()">
                    <IPAMui:ToolStripText hidden=true text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_272 %>" runat="server" />
                    <IPAMui:ToolStripButton id='ipv6CurrentFilter' hidden=true iconCls="ip-avail" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_260 %>" runat="server">
                    <handler>function(){
                        Ext.MessageBox.confirm(
                            'Remove Filter',
                            'Are you sure you want to remove the filter?',
                                function(btn){ if( btn == 'yes' ) $SW.IPv6.SetIPv6GridFilter(Ext.getCmp('ffIPv6All')); }
                        );}
                    </handler>
                    </IPAMui:ToolStripButton>
                    <IPAMui:ToolStripSeparator hidden=true runat="server" />
                </PageBar>
                <Columns>
                    <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" dataIndex="Status" runat="server" renderer="$SW.subnet.ipStatusImgRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="120" isSortable="true" dataIndex="IPAddress" renderer="$SW.subnet.ip6Renderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_297 %>" width="120" isSortable="true" isHidden="false" dataIndex="IPAddress4" renderer="$SW.subnet.ipMapped4Renderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_158 %>" width="66" isSortable="true" dataIndex="Status"  renderer="$SW.subnet.ipStatusRenderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_274 %>" width="80" isSortable="true" isHidden="false" dataIndex="MAC" runat="server" />
                    <%--<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_275 %>" width="130" isSortable="false" isHidden="false" runat="server" dataIndex="AllocPolicy" renderer="$SW.subnet.ipScopeNameRenderer()" />--%>
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_276 %>" width="151" isSortable="true" isHidden="true" dataIndex="LeaseExpires" runat="server" renderer="$SW.subnet.ipDaysRenderer(0)" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_277 %>" width="123" isSortable="true" isHidden="true" dataIndex="LeaseExpires" runat="server" renderer="$SW.subnet.ipLeaseExpires($SW.ExtLocale.DateFull)" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_278 %>" width="120" isSortable="true" isHidden="true" dataIndex="Alias" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="86" isSortable="true" isHidden="true" dataIndex="AllocPolicy" runat="server" renderer="$SW.subnet.ipTypeRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_279 %>" width="86" isSortable="true" isHidden="true" dataIndex="SkipScan" runat="server" renderer="$SW.subnet.ipScanRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_280 %>" width="120" isSortable="true" dataIndex="DnsBackward" runat="server" />
                     <%--<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_281 %>" width="130" isSortable="true" dataIndex="DhcpClientName" runat="server" />--%>
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_283 %>" width="120" isSortable="true" dataIndex="LastSync" renderer="$SW.subnet.ipLastResponse($SW.ExtLocale.DateShort)" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_284 %>" width="120" isSortable="true" isHidden="true" dataIndex="MachineType" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_155 %>" width="80" isSortable="true" isHidden="true" dataIndex="Vendor" runat="server" renderer="$SW.subnet.iconImgRenderer()" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_285 %>" width="120" isSortable="true" isHidden="true" dataIndex="sysName" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_286 %>" width="120" isSortable="true" dataIndex="Description" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_287 %>" width="120" isSortable="true" dataIndex="Location" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_288 %>" width="120" isSortable="true" dataIndex="Contact" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_289 %>" width="120" isSortable="true" dataIndex="ResponseTime" renderer="$SW.subnet.ipResponseTimeRenderer()" runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_290 %>" width="120" isSortable="true" dataIndex="Comments" runat="server" />
                </Columns>
                  <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.IPNodeGrid" AmbientSort="IPAddressN" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                    <Fields>
                        <IPAMlayout:GridStoreField dataIndex="IpNodeId" isKey="true" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddress" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Alias" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="MAC" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="MachineType" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Vendor" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="VendorIcon" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="sysName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Status" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="AllocPolicy" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="SkipScan" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DnsBackward" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DhcpClientName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="LastSync" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Description" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Contact" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="ResponseTime" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="LeaseExpires" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="SubnetId4" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="IPAddress4" runat="server" />                        
                    </Fields>
                  </Store>
                </IPAMlayout:GridBox>

               <IPAMlayout:BoxComponent id="tabReport" runat="server" layout="fit" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_298 %>" autoScroll="true" clientEl="tab2" borderVisible="true" />
                    
                </IPAMlayout:TabLayout>
            </Center>
        </IPAMlayout:BorderLayout>
    </Center>
    <South>
        <IPAMlayout:BoxComponent ID="ipamFooter" borderVisible="false" clientEl="footer" layout="fit" runat="server" />
    </South>
    </IPAMlayout:BorderLayout>

</IPAMlayout:ViewPort>

</asp:Content>
