<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True" Inherits="SolarWinds.IPAM.WebSite.IPSelectRange"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_373%>" CodeFile="IP.SelectRange.aspx.cs" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.Operator" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Edit" CausesValidation="true" OnMsg="MsgEdit" runat=server />
    <IPAMmaster:WindowMsg Name="Remove" CausesValidation="true" OnMsg="MsgRemove" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation=jsPostInit runat="server">
var d = $SW.Env && $SW.Env['rangedata'] && $SW.Env['ipRangeAction'] && $SW.Env['ipRangeAction']['totalIpCount'];
if( d )
{
    var result = {rangeData: $SW.Env['rangedata'], actionData: $SW.Env['ipRangeAction'] };

    $(window).load( function(){ $SW.msgq.UPSTREAM('rangeSelected',result); });
}
else
{
    //are there some IPs within range?
     if ( $SW.Env && $SW.Env['ipRangeAction'] && !$SW.Env['ipRangeAction']['totalIpCount'] )
     {
        $SW.Alert('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_70;E=js}','@{R=IPAM.Strings;K=IPAMWEBJS_VB1_200;E=js}',{ buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.WARNING });
    }
}

 $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
 $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });

</IPAMmaster:JsBlock>

<IPAMui:ValidationIcons runat=server />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_214 %></div>
    
    <div style="padding-bottom:20px">
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_374 %>
    </div>
    
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_375 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtStartingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="StartingNetworkAddressRequire" runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_376 %>" />
                <asp:CustomValidator ID="StartingNetworkAddressIPv4" runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_377 %>"
              /><asp:CustomValidator
                  ID="StartingSubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_378 %>"
              /></td>
        </tr>
        <tr runat=server> 
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_379 %>
            </div></td>
            <td><div class="sw-form-item sw-form-half">
             <asp:TextBox ID="txtEndingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="EndingNetworkAddressRequire" runat="server"
                  ControlToValidate="txtEndingNetworkAddress"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_380 %>" />
                <asp:CustomValidator ID="EndingNetworkAddressIPv4" runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_381 %>"
              /><asp:CustomValidator
                  ID="EndingSubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtEndingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_382 %>"
              /><asp:CustomValidator
                  ID="NetworkAddressComparator"
                  runat="server"
                  OtherControl="txtEndingNetworkAddress"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4ipcomparator"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_383 %>"
              /><asp:CustomValidator
                  ID="CustomValidator1"
                  runat="server"
                  OtherControl="txtEndingNetworkAddress"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4ipnumberinrange"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_384 %>"
              /></td>
        </tr>
        <tr style="display:none">
            <td><div class=sw-field-label >
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_306%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlStatus" runat="server">
                    <asp:ListItem Selected=true Value="2" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>"/>
                    <asp:ListItem Value="4" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>"/>
                    <asp:ListItem Value="1" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_263 %>"/>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr id="parentifo" runat=server> 
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_385 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled ">
             <asp:TextBox ID="txtParentAddress" Enabled=false CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr style="display:none">
            <td><div class=sw-field-label visible="false">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_314%>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>      
    </table></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 100px">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl="/Orion/IPAM/subnets.aspx"
                        CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
