<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.upload.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportUpload"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_313 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewImportIPAddressSpreadsheet" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:JsBlock Requires="ext,ext-fileupload" Orientation="jsPostInit" runat="server">
$SW['ipButton'] = '';
$SW['notes'] = '';

Ext.onReady(function()
{ 
    var transform = function(id){

        var item = $('#'+id)[0];

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };
    
    $SW['hfFileType'] = $SW.nsGet($nsId, 'hfFileType' );

    $SW['hfHeadersWarn'] = $SW.nsGet($nsId, 'hfHeadersWarn' );
    
    $SW['hfIgnoreHeadersWarn'] = $SW.nsGet($nsId, 'hfIgnoreHeadersWarn' );

    if($('#'+$SW['hfHeadersWarn']).val() != "")
    {
    Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_63;E=js}',
                    msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_64;E=js}",
                    
                    buttons: { yes:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}', no:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_65;E=js}' },
                    animEl: id,
                    prompt: false,
                    icon: Ext.MessageBox.WARNING,
                    closable: false,
                    fn: function (btn, text) {
                        if (btn == 'no') { 
                            $('#'+$SW['hfIgnoreHeadersWarn']).val('true');
                            $('form').submit();
                        } else {
                            $('#'+$SW['hfIgnoreHeadersWarn']).val('false');
                        }
                    }
                });
    }

     var fupl = new Ext.ux.form.FileUploadField({
        id: 'UploadFileCtrl',
        renderTo: 'fu',
        width: 350,
        emptyText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_66;E=js}'
    });
    
    fupl.on('fileselected',function(el){
        $('#'+$SW['hfIgnoreHeadersWarn']).val('false');
        var val = el.getValue();

        if( /[.]csv$/i.test( val ) ) {
          $('#'+$SW['hfFileType']).val("csv");
        }
        else if( /[.]xls$/i.test( val ) ) {
          $('#'+$SW['hfFileType']).val("xls");
        }
        else if( /[.]xlsx$/i.test( val ) ) {
          $('#'+$SW['hfFileType']).val("xlsx");
        }
        else if( /[.]zip$/i.test( val ) ) {
          $('#'+$SW['hfFileType']).val("zip");
        }
        else {
          Ext.Msg.show( {
            width:320,
            title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_67;E=js}', 
            msg:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_68;E=js}',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.WARNING
            });
          $('#'+$SW['hfFileType']).val("");
          return;
        }
        var uplbtn = $SW.ns$($nsId,'Upload');
        uplbtn.removeClass('sw-form-dim');
        uplbtn[0].disabled=false;
        uplbtn[0].setAttribute("class", "sw-btn-primary sw-btn sw-btn-enabled");
        uplbtn[0].setAttribute("onclick", "");

    });

    $SW['ipButton'] = $SW.nsGet($nsId, 'rbIpAddresses');
    $SW['notes'] = $SW.nsGet($nsId, 'restrictionSubnetNotes');
});

$SW.checkRadio = function()
{
    var ipButton = $('#'+$SW['ipButton'])[0];

    var res = $('#subnetRestriction')[0];
    var notes = $('#'+$SW['notes'])[0];

    if (ipButton.checked)
        notes.style.display = res.style.display = "inline";
    else
        notes.style.display = res.style.display = "none";
}

</IPAMmaster:JsBlock>

<div style="margin-left: 10px">

     <table width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><%= Page.Title %></h1>
        </td>
        </tr>
      </table>

    <div style="margin: 0px 0px 10px 10px;">
      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_124 %>
    </div>

        <div id="formbox">
        <asp:HiddenField ID="hfHeadersWarn" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hfFileType" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hfIgnoreHeadersWarn" runat="server"></asp:HiddenField>
        <asp:HiddenField ID="hfObjectID" runat="server"></asp:HiddenField>

            <div>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_314 %>
            </div>
            
            <div style="padding-top:8px;padding-bottom:8px">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_315 %> 
            </div> 
            
            <div>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-normal">
                        <td class="sw-form-col-label" style="width: 190px;"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                      <tr>
                        <td colspan=3><div class="sw-form-subheader">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_316 %>
                        </div></td>
                      </tr>
                    <tr>
                        <td><div class="sw-field-label">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_317 %>
                        </div></td>
                        <td><div class="sw-form-item">
                            <div id="fu" />
                        </div></td>
                        <td />
                    </tr>
                    <tr style="height:15px"></tr>
                    <tr ><td><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_318 %></td>
                        <td>
                            <asp:RadioButton ID="rbIpAddresses" runat="server" Text="<%$ Resources: IPAMWebContent, SELECT_FILE_IP_ADDRESSES %>" Checked="true"  GroupName="cgImportType" /><br/>
                            <asp:RadioButton ID="rbStructure" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_319 %>" Checked="false" GroupName="cgImportType"/><br/>
                            <asp:RadioButton ID="rbAll" runat="server" Text="<%$ Resources: IPAMWebContent, SELECT_FILE_STRUCTURE_AND_IP_ADDRESSES %>" Checked="false" GroupName="cgImportType"/>
                        </td>
                        <td />
                    </tr>
                    <tr>
                        <td><div class="sw-field-label"></div></td>
                        <td>
                            <div class="sw-form-item">
                                <div id="subnetRestriction">
                                    <asp:CheckBox ID="cbSubnetRestriction"  runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_320 %>" />
                                </div>
                            </div>
                        </td>
                        <td />
                    </tr>
                    <tr>
                        <td><div class="sw-field-label"></div></td>
                        <td>
                            <div runat="server" id="restrictionSubnetNotes" class="sw-form-item sw-form-clue">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_321 %>
                            </div>
                        </td>
                        <td />
                    </tr>
                </table>
            </div>

            <asp:CustomValidator ID="Validator" runat="server"  
            ErrorMessage=""
            OnServerValidate="ValidateUploadFile"
            Display="Dynamic" ></asp:CustomValidator>

            <asp:PlaceHolder id="ErrorAdvice" runat="server" Visible="false">

                <div class=sw-form-subheader style="color: Red;"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_322 %></div>
                        
                <asp:PlaceHolder id="InvalidCsvFile" runat="server">
                    <div style="margin-top: 10px;">
                      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_323 %>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder id="InvalidXlsFile" runat="server">
                    <div style="margin-top: 10px;">
                      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_324 %>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder id="InvalidXlsxFile" runat="server">
                    <div style="margin-top: 10px;">
                      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_325 %>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder id="FileParseError" runat="server">
                    <div style="margin-top: 10px;">
                      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_326 %>
                    </div>
                </asp:PlaceHolder>

                <div style="margin-top: 10px;">
                  <a href="#" id='showinternalerr' style="text-decoration: underline;" onclick="$(this).hide(); $('#internalerr').show(); return false;"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_327 %></a>
                  <pre id='internalerr' style="display: none;"><asp:Literal id="InternalMessage" runat="server" /></pre>
                </div>

            </asp:PlaceHolder>

            <asp:PlaceHolder id="CannotUseZipFileErrorPlaceHolder" runat="server" Visible="false">
                <div class="sw-form-subheader" style="color: Red; margin-top: 10px;">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_ERRORMSG_CANNOT_USE_ZIP_FILE_FOR_THIS_TYPE_OF_UPLOAD %>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder id="ZipFileRequiredErrorPlaceHolder" runat="server" Visible="false">
                <div class="sw-form-subheader" style="color: Red; margin-top: 10px;">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_ERRORMSG_ZIP_FILE_REQUIRED_FOR_THIS_TYPE_OF_UPLOAD %>
                </div>
            </asp:PlaceHolder>

            <asp:PlaceHolder id="ZipFileMissingData" runat="server" Visible="false">
                <div class="sw-form-subheader" style="color: Red; margin-top: 10px;">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_ERRORMSG_ZIP_FILE_MISSING_DATA %>
                </div>
            </asp:PlaceHolder>
            
        </div>
        
        <div class="sw-btn-bar-wizard" style="padding-right: 20px">
            <orion:LocalizableButtonLink runat="server" ID="btnIntroLink" LocalizedText="Back" DisplayType="Secondary" />
            <orion:LocalizableButton runat="server" ID="Upload" OnClick="Upload_Click" OnClientClick="return false;" LocalizedText="Next" DisplayType="Primary" CausesValidation="true" CssClass="sw-form-dim" />
            <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>

</div>
</asp:Content>
