﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/View.master" CodeFile="IPAMAddressDetailsView.aspx.cs" Inherits="Orion_IPAM_IPAMAddressDetailsView" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/IPAddressLink.ascx" TagName="IPAddressLink" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/TimePeriodSelector.ascx" TagName="TimePeriodSelector" %>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">

<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
  { %>
<style type="text/css">
a.IpamSettingsLink {
    margin-left: 5px; padding: 2px 0 2px 20px;
    background: url("/Orion/IPAM/res/images/sw/Page.Icon.MdlSettings.gif") no-repeat left center;
}
</style>
<script type='text/javascript' src='/Orion/IPAM/res/js/injectDateAreaLinks.js'>
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function() { $('div.dateArea').IpamSettingsLinkInjector({ pos: 2, before: false }); });
</script>
<%}%>

<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <br />
    <div style="margin-left: 10px;" >
        <orion:DropDownMapPath ID="IPNodeSiteMapPath" runat="server" OnInit="OnInitIPNodeSiteMapPath" >
            <RootNodeTemplate>
                <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
            </RootNodeTemplate>
            <CurrentNodeTemplate>
	            <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
	        </CurrentNodeTemplate>
        </orion:DropDownMapPath>
	</div>
<% } %>

	<h1><%=Page.Title %><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_637, "")%><asp:HyperLink ID="ipAddressLinkURL" runat="server"><IPAM:IPAddressLink ID="ipAddressLink" ValueTemplate="{0}" runat="server" /></asp:HyperLink></h1>

    <div style="font-size: 12pt; font-weight: bold; padding: 0 0 0 10px;">
        <span style="vertical-align:middle;"><asp:Label ID="TimePeriodValue" Text="{TimePeriod}" runat="server" /></span>
        <a onclick="$('#time-period').toggle();">
            <img src="/Orion/IPAM/res/images/sw-navigation/pick-btn.gif" style="vertical-align:middle;" alt="select" />
        </a>
    </div>
    <div id="time-period" style="display: none; position:absolute; z-index: 1999; ">
        <IPAM:TimePeriodSelector ID="TimePeriodSelector" runat="server"
            OnClientSave="$('#time-period').hide();"
            OnClientCancel="$('#time-period').hide();"
        />
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

	<IPAM:IPAMNodeResourceHost ID="addressResHost" runat="server">
		<orion:ResourceContainer ID="resContainer" runat="server"/>
	</IPAM:IPAMNodeResourceHost>
    <IPAM:LicenseStatus runat="server" />
</asp:Content>





