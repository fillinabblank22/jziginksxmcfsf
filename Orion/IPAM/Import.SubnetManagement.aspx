<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.SubnetManagement.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportSubnetManagement"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_369 %>" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewAssignSubnetsOrphanedIPs" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.Operator" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="ux-all-plugins.css" runat="server">
#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,ext-ux,ext-quicktips,sw-helpserver,sw-subnet-manage.js" Orientation="jsPostInit" runat="server">

$SW.LicenseListeners.push(function(obj){ var o = obj; $(document).ready(function(){

  var cmax = parseInt(o.countmax), cpos = parseInt(o.countcurrent);

  if( cmax && cmax <= cpos )
  {
    var btn;
    if(btn = Ext.getCmp('AssignSubnet')) btn.disable();

    Ext.MessageBox.show({
     title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_86;E=js}',
     msg: String.format( "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_87;E=js}", cmax ),
     buttons: Ext.MessageBox.OK,
     closable: false
    });
  }
});});


$SW.subnet.SetGridFilterToInternal = function(){
var baseParams = $SW['store'].baseParams;
if( !baseParams ) $SW['store'].baseParams = {};
baseParams["filter[0][data][comparison]"] = "eq";
baseParams["filter[0][data][type]"] = "numeric";
baseParams["filter[0][data][value]"] = "1";
baseParams["filter[0][field]"] = "SubnetId";

$SW['store'].on('load',function(that,rs,opts){
  if( that.totalLength == 0 )
  {
    Ext.MessageBox.show({
     title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_34;E=js}',
     msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_88;E=js}",
     buttons: Ext.MessageBox.OK,
     closable: false,
     fn:function(btn){
       window.location = 'subnets.aspx';
     }
    });
  }
});

};

$SW.moveorphans = function(el){

    var ajaxok = function(result)
    {
        if(!result)
            return;

        var msg = (result.data && result.data.rejected) ? 
            '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_89;E=js}' :
            '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_90;E=js}';

        // if(result.data && result.data.rejected && result.data.rejected == true)
        Ext.Msg.show({
          title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_91;E=js}',
          msg: msg,
          fn: function(btn){ window.location = "import.subnetmanagement.aspx?_cb=" + (new Date()).getTime(); },
          buttons: Ext.Msg.OK,
          icon: Ext.MessageBox.INFO });
    };

    var fail = $SW.ext.AjaxMakeFailDelegate( 'Move Orphan IPs' );
    var pass = $SW.ext.AjaxMakePassDelegate( 'Move Orphan IPs', ajaxok );

    Ext.Ajax.request({
       url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
       success: pass,
       failure: fail,
       timeout: fail,
       params:
       {
        entity: 'IPAM.IPNode',
        verb: 'MoveOrphans',
        _dc: (new Date()).getTime()
       }
    });

    return true;
};

</IPAMmaster:JsBlock>

    <IPAMlayout:DialogWindow jsID="mainWindow" helpID="subnetedit" Name="mainWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_370 %>" height="400" width="480" runat="server">
        <buttons>
            <IPAMui:ToolStripButton id="mainWindowSave" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
                <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
            </IPAMui:ToolStripButton>
            <IPAMui:ToolStripButton ID="ToolStripButton3" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
                <handler>function(){ $SW['mainWindow'].hide(); }</handler>
            </IPAMui:ToolStripButton>
        </buttons>
        <msgs>
            <IPAMmaster:WindowMsg Name="ready" runat="server">
                <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg Name="unload" runat="server">
                <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'], true ); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg Name="close" runat="server">
                <handler>function(n,o){ $SW['mainWindow'].hide(); $SW['store'].load(); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg Name="refresh" runat="server">
                <handler>function(n,o,info){ $SW['mainWindow'].hide(); $SW['store'].load(); }</handler>
            </IPAMmaster:WindowMsg>
            <IPAMmaster:WindowMsg Name="license" runat="server">
                <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
            </IPAMmaster:WindowMsg>
       </msgs>
    </IPAMlayout:DialogWindow>
    
    <div style="margin-left: 10px;">
	
    <table width="auto" style="table-layout: fixed; margin-top: 20px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb">
      <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	  <h1><%= Page.Title %></h1>
    </td>
    </tr>
    </table>

        <div style="margin: 10px 0px 10px 0px;">
          <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_371 %>
        </div>

        <div style="margin: 10px 0px 10px 0px;">
             <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_372 %>
        </div>
    
        <div id="gridframe" style="margin: 16px 10px 0px 0px;"> <!-- --> </div>

        <IPAMlayout:GridBox
            width="640"
            height="600"
            autoScroll="true"
            autoHeight="false"
            ID="hiddenSubnet"
            title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_72 %>"
            emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_312 %>"
            borderVisible="true"
            isFramed="false"
            UseLoadMask="True"
            deferEmptyText="true" 
            RowSelection="false"
            clientEl="gridframe"
            runat="server"
            StripeRows="true"
            listeners="{ beforerender: $SW.ext.gridSnap,statesave: $SW.subnet.GridColumnChangedHandler }"
            SelectorName="selector">
            <ConfigOptions runat="server">
                <Options>
                    <IPAMlayout:ConfigOption runat="server" Name="plugins" Value="[ new Ext.ux.plugins.GridSelectAll() ]" IsRaw="true" />
                </Options>
            </ConfigOptions>
            <topmenu>
                <IPAMui:ToolStripButton id="AssignSubnet" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_373 %>" toolTip="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_374 %>" iconCls="mnu-add" runat="server">
                    <handler>$SW.subnet.addforip</handler>
                </IPAMui:ToolStripButton>
                <IPAMui:ToolStripButton id="MoveOrphanedIPs" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_375 %>" disabled="true" toolTip="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_376 %>" iconCls="mnu-add" runat="server">
                    <handler>$SW.moveorphans</handler>
                </IPAMui:ToolStripButton>
                <IPAMui:ToolStripButton text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_377 %>" iconCls="mnu-delete" runat="server">
                    <handler>$SW.subnet.delipfromorphaned</handler>
                </IPAMui:ToolStripButton>
            </topmenu>
            <PageBar pageSize="100" displayInfo="true" listeners="{ beforerender: $SW.subnet.SetGridFilterToInternal }" />
            <columns>
                <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" dataIndex="Status" runat="server" renderer="$SW.subnet.ipStatusImgRenderer()" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_269 %>" width="120" isSortable="true" dataIndex="IPAddress" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_158 %>" width="66" isSortable="true" dataIndex="Status"  renderer="$SW.subnet.ipStatusRenderer()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_273 %>" width="120" isSortable="true" isHidden="true" dataIndex="IPMapped" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_274 %>" width="80" isSortable="true" isHidden="true" dataIndex="MAC" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_278 %>" width="120" isSortable="true" isHidden="true" dataIndex="Alias" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="86" isSortable="true" isHidden="true" dataIndex="AllocPolicy" runat="server" renderer="$SW.subnet.ipTypeRenderer()" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_280 %>" width="120" isSortable="true" dataIndex="DnsBackward" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_283 %>" width="120" isSortable="true" dataIndex="LastSync" renderer="$SW.subnet.ipLastResponse($SW.ExtLocale.DateShort)" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_155 %>" width="80" isSortable="true" isHidden="true" dataIndex="Vendor" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_285 %>" width="120" isSortable="true" isHidden="true" dataIndex="sysName" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_286 %>" width="120" isSortable="true" dataIndex="Description" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_287 %>" width="120" isSortable="true" dataIndex="Location" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_288 %>" width="120" isSortable="true" dataIndex="Contact" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_289 %>" width="120" isSortable="true" dataIndex="ResponseTime" renderer="$SW.subnet.ipResponseTimeRenderer()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_290 %>" width="120" isSortable="true" dataIndex="Comments" runat="server" />
            </columns>
            <store id="store" dataroot="rows" autoload="true" proxyurl="extdataprovider.ashx" AmbientSort="IPAddressN" proxyentity="IPAM.IPNode" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
            <Fields>
                <IPAMlayout:GridStoreField dataIndex="IPNodeId" isKey="true" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="IPOrdinal" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="IPAddress" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="IPMapped" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Alias" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="MAC" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Vendor" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="sysName" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Status" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="AllocPolicy" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="DnsBackward" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="LastSync" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Description" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Contact" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ResponseTime" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
            </Fields>
            </store>
            </IPAMlayout:GridBox>

            <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />

        <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
            <orion:LocalizableButtonLink runat="server" href="#" onclick="history.back(); return false;" LocalizedText="Back" DisplayType="Secondary" />
            <orion:LocalizableButton runat="server" ID="btnDone" OnClick="OnDoneClick" LocalizedText="Done" DisplayType="Primary" />
        </div>

</div>
</asp:Content>
