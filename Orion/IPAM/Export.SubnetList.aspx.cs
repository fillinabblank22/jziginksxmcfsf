using System;
using System.Data;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ExportSubnetList : CommonPageServices
	{
        PageExportSubnetList mgr;
        public ExportSubnetList() { mgr = new PageExportSubnetList(this); }

        public GridBox Grid
        {
            get { return grid; }
        }

		//#region Controls
		//protected global::SolarWinds.IPAM.WebSite.GridPreloader GridPreloader1;
		//protected global::SolarWinds.IPAM.Web.Layout.GridBox grid;
		//#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
            GridPreloader1.WhereClause = mgr.WhereClause;
        }
	}
}
