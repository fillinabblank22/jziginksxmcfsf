<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True" Inherits="SolarWinds.IPAM.WebSite.IPAddRange"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_218 %>" CodeFile="IP.AddRange.aspx.cs" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.Operator" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation=true OnMsg="MsgSave" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation=jsPostInit runat="server">
var d = $SW.Env && $SW.Env['rangedata'];
if( d )
{
 $(window).load( function(){ $SW.msgq.UPSTREAM('addips',$SW.Env['rangedata']); });
}
else
{
 $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
 $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
}
</IPAMmaster:JsBlock>

<IPAMui:ValidationIcons runat=server />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_219 %></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_220 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtStartingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="StartingNetworkAddressRequire" runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_221 %>" />
                <asp:CustomValidator ID="StartingNetworkAddressIPv4" runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_222 %>"
              /><asp:CustomValidator
                  ID="StartingSubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_223 %>"
              /></td>
        </tr>
        <tr runat=server> 
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_224 %>
            </div></td>
            <td><div class="sw-form-item sw-form-half">
             <asp:TextBox ID="txtEndingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="EndingNetworkAddressRequire" runat="server"
                  ControlToValidate="txtEndingNetworkAddress"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_225 %>" />
                <asp:CustomValidator ID="EndingNetworkAddressIPv4" runat="server"
                  ControlToValidate="txtEndingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_226 %>"
              /><asp:CustomValidator
                  ID="EndingSubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtEndingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_227 %>"
              /><asp:CustomValidator
                  ID="NetworkAddressComparator"
                  runat="server"
                  OtherControl="txtEndingNetworkAddress"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4ipcomparator"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_228 %>"
              /><asp:CustomValidator
                  ID="CustomValidator1"
                  runat="server"
                  OtherControl="txtEndingNetworkAddress"
                  ControlToValidate="txtStartingNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4ipnumberinrange"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_229 %>"
              /></td>
        </tr>
        <tr style="display:none">
            <td><div class=sw-field-label >
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_230 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlStatus" runat="server">
                    <asp:ListItem Selected=true Value="2" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_231 %>"></asp:ListItem>
                    <asp:ListItem Value="4" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_232 %>"></asp:ListItem>
                    <asp:ListItem Value="1" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_233 %>"></asp:ListItem>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr id="parentifo" runat=server> 
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_234 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled ">
             <asp:TextBox ID="txtParentAddress" Enabled=false CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr style="display:none">
            <td><div class=sw-field-label visible="false">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_235 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <br/><br/>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" CausesValidation="true" DisplayType="Primary" />
                    <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary"/>
                </div>
            </td>
        </tr>      
    </table></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

</asp:Content>
