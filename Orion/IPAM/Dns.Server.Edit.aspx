﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" CodeFile="Dns.Server.Edit.aspx.cs" 
Inherits="SolarWinds.IPAM.WebSite.DnsServerEdit" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_191 %>" validateRequest="false"%>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DNSCredentialsSelector.ascx" TagName="DNSCredentialsSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" TagName="CustomPropertyEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){ 
    $('.expander').expander();
    $SW.DnsServerScanEnable = function($nsId,initialRun)
    {
        var cbEnable = $SW.ns$( $nsId, 'cbEnableScanning' )[0],
        div = $('#DnsScanSettings'),
        delay = initialRun ? 0 : 200;
	
        if( !cbEnable ) return;
        if(!cbEnable.checked) div.hide(delay);
        else div.show(delay);
    };
    $SW.ns$($nsId,'cbEnableScanning').click( function(){ $SW.DnsServerScanEnable($nsId); } );
    $SW.DnsServerScanEnable($nsId,true);
});
$SW.IPAM.IncrementalZoneTransfer = function(show){
    var el = $('.incremental-zone-transfer');
    if(show) el.show(250); else el.hide(250);
}
</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
div.sw-form-heading { font-size: small; margin-top: 16px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: small; margin: 0; }
.sw-heading { width: 130px; }
.warningbox-inside .inner a { text-decoration: underline; }
.warningbox-inside { border: 1px solid #d1d0c9; max-width: 870px; background: #f7f596 url(/orion/ipam/res/images/sw/bg.warning.float.gif) repeat-x scroll left bottom; }
.warningbox-inside .inner { background:transparent url(/orion/ipam/res/images/sw/icon.notification.32.gif) no-repeat scroll left top; }
.sw-form-cols-normal td.sw-form-col-label { width: 200px; }
.sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>
    
<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class="sw-form-header">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_192 %>
    </div>
    
    <div class="group-box white-bg">
        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
            <tr class="sw-form-cols-normal">
                <td class="sw-form-col-label"></td>
                <td class="sw-form-col-control"></td>
                <td class="sw-form-col-comment"></td>
            </tr>
            <tr>
                <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_193 %></div></td>
                <td>
                    <div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtServerType" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_194 %></div></td>
                <td>
                    <div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtIpAddress" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_195 %></div></td>
                <td>
                    <div class="sw-form-item">
                        <asp:TextBox ID="txtDescription" CssClass="x-form-text x-form-field" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_196 %></div></td>
                <td>
                    <div class="sw-form-item">
                        <asp:TextBox ID="txtVLAN" CssClass="x-form-text x-form-field" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_197 %></div></td>
                <td>
                    <div class="sw-form-item">
                        <asp:TextBox ID="txtLocation" CssClass="x-form-text x-form-field" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    <div><!-- ie6 --></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_142 %>">    
        <div class="group-box white-bg" style="margin-bottom: 0px">
            <ipam:CustomPropertyEdit ID="CustomPropertyEdit" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
        </div>
    </div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_199 %>">
        <div class="warningbox sw-credential-warning" style="display: none;">
            <div class="inner">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_200 %>
            </div>
        </div>  
        <div><!-- ie6 --></div>
        <div class="group-box blue-bg">
           <ipam:DNSCredentialsSelector ValidationGroup="NewCred" ID="CredSelector" runat="server" />           
        </div>
    </div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_201 %>">

        <div><!-- ie6 --></div>

        <div class="group-box blue-bg">
            <div>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-normal">
                        <td class="sw-form-col-label"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>        
                <tr>
                    <td colspan="3"><div class="sw-form-item">
                    <asp:CheckBox ID="cbEnableScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_202 %>" runat="server"/>
                    </div></td>
                </tr>
                </table>
            </div>

            <div><!-- ie6 --></div>

            <div id="DnsScanSettings">
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-normal">
                        <td class="sw-form-col-label"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><div class="sw-field-label" style="padding-left: 19px">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_203 %> 
                        </div></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 150px;">
                            <ipam:TimeSpanEditor Width="168" id="DnsScanInterval" ActiveWhenChecked="False" TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_273 %>" runat="server" MinValue="00:10:00" MaxValue="7.00:00:00" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><div class="sw-form-item incremental-zone-tansfer" id="areaIncrementalZoneTransfer" runat="server">
                        <asp:CheckBox ID="cbIncrementalZoneTransfer" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_204 %>" runat="server"/>
                        </div></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br/><br/>
    <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" CausesValidation="true" DisplayType="Primary" 
            OnClientClick="if(CheckDemoMode()) {demoAction('IPAM_DNS_ServerEdit', null); return false; }" />
        <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/Orion/IPAM/Dhcp.Management.aspx" LocalizedText="Cancel" DisplayType="Secondary"/>
    </div>
</div>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />

</asp:Content>
