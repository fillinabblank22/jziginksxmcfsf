using System;
using System.IO;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.Orion.Web.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpServerAdd : CommonPageServices
    {
        private const string newDhcpManagementUrl = "~/api2/ipam/ui/dhcp";

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            if (this.Page.IsPostBack == false)
            {
                var redirector = new DhcpDnsServerAddRedirector(this);
                redirector.HandleAddOrionNodePage(true);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
			
			CredSelector.ServerTypeChanged+=ServerChanged;

            SwisConnector.RunSwisOps(new SwisOpCtx[]{
                new SwisOpCtx("Retrieve DHCP Server List", OpGetDhcpServers )
            });
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                btnSave.OnClientClick = "javascript:demoAction('IPAM_Add_DHCP_Server',null); return false;";
            }
            else
            {
                bool isFreeTool = false;
                bool isOtherModuleInstalled = false;
                isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);
                if (isFreeTool && !isOtherModuleInstalled)
                    btnSave.OnClientClick = string.Format("alert('{0}'); return false;", Resources.IPAMWebContent.IPAMWEBDATA_DF1_45);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            btnSave.Click += new EventHandler(OnSave);

            if (!this.IsPostBack)
            {
                UseSWIS("Fill default values", FillDefaultValues);
                SelectOrionNodes();
            }
        }

        void FillDefaultValues(IpamClientProxy proxy)
        {
            Setting scanEnable = proxy.AppProxy.Setting.GetGlobal(SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANENABLE);
            Setting scanInterval = proxy.AppProxy.Setting.GetGlobal(SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANINTERVAL);

            if (scanEnable != null)
            {
                bool? enable;
                if (scanEnable.TryGetValue(out enable) && enable.HasValue)
                    cbEnableSubnetScanning.Checked = enable.Value;
            }

            if (scanInterval != null)
            {
                TimeSpan? span;
                if (scanInterval.TryGetValue(out span) && span.HasValue)
                    NewSubnetInterval.Value = span.Value;
            }
        }

        void SelectOrionNodes()
        {
            // get NetObjectId to pre-select added orion node
            string netObjectId = AddNodeReturnPageHelper.NetObjectId;

            // and cleanup session value
            AddNodeReturnPageHelper.NetObjectId = null;

            if ((this.PickHost != null) && !string.IsNullOrEmpty(netObjectId))
            {
                this.PickHost.Selection = netObjectId;
            }
        }

        private object OpGetDhcpServers(IpamClientProxy proxy, object param)
        {
            Dictionary<string, int> ids = new Dictionary<string, int>();

            using (DataTable dt = proxy.SwisProxy.Query("SELECT G.NodeId from IPAM.DhcpServer G"))
            {
                foreach (DataRow dr in dt.Rows)
                    ids[((int)dr[0]).ToString()] = 1;
            }

            AddJsEnv("ExistingDhcpServers", ids);
            return null;
        }

        protected void OnSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            Page.Validate();
            Page.Validate("NewCred");

            if (Page.IsValid && (SelectedNodeId != null))
            {
                Nullable<int> credentialId = CredSelector.CredId;
                if (credentialId == null)
                {
                    ICredentials credentials = CredSelector.GetCredentials();
                    SwisOpCtx createNewCredentialSwisOpCtx = new SwisOpCtx(
                        "Create New Credential",
                        delegate (IpamClientProxy proxy, object param)
                        {
                            credentialId = proxy.AppProxy.Credentials.Create(credentials);
                            return null;
                        }
                    );
                    SwisConnector.RunSwisOps(new SwisOpCtx[] { createNewCredentialSwisOpCtx });
                }

                int maxscopeid = 0;
                this.UseSWIS("Retrieve Lastest ScopeId", delegate (IpamClientProxy proxy)
                {
                    using (DataTable dt = proxy.SwisProxy.Query("SELECT ISNULL(MAX(S.ScopeId),0) AS MaxItem FROM IPAM.DhcpScope S"))
                    {
                        maxscopeid = (int)dt.Rows[0].ItemArray[0];
                    }
                });

                //ddlVrfGroupNameint
                var clusterId = 0;

                if (CredSelector.DhcpServerType != DhcpServerType.Infoblox)
				{
                    var vrfGroupNameItem = vrfGroupName.FindControl("ddlVrfGroupName") as DropDownList;
					var newVrfGroupName = vrfGroupName.FindControl("newVrfGroupName") as TextBox;

                    if (!string.IsNullOrWhiteSpace(newVrfGroupName?.Text) && string.IsNullOrWhiteSpace(vrfGroupNameItem.SelectedItem.Value))
					{
						GroupNode group = new GroupNode();
						group.FriendlyName = newVrfGroupName.Text;
						group.GroupType = GroupNodeType.Root;
						group.Status = 1;
						group.DisableNeighborScanning = null;

						SwisOpCtx addNewClusterSwisOpCtx = new SwisOpCtx(
							"Add new Cluster",
							delegate (IpamClientProxy proxy, object param)
							{
								clusterId = proxy.AppProxy.GroupNode.AddCluster(group, true);
								return clusterId;
							}
						);
						SwisConnector.RunSwisOps(new SwisOpCtx[] { addNewClusterSwisOpCtx });

						GroupNode discoveredGroup = new GroupNode
						{
							FriendlyName = "Discovered Subnets",
							GroupType = GroupNodeType.Group,
							Status = 1,
							ParentId = clusterId,
							ClusterId = clusterId
						};

						SwisOpCtx addNewDiscoveredGroupSwisOpCtx = new SwisOpCtx(
							"Add new Discovered Subnets to created Cluster",
							delegate (IpamClientProxy proxy, object param)
							{
								return proxy.AppProxy.GroupNode.AddCluster(discoveredGroup, true);
							}
						);
						SwisConnector.RunSwisOps(new SwisOpCtx[] { addNewDiscoveredGroupSwisOpCtx });
					}
					else
					{
						clusterId = Convert.ToInt32(vrfGroupNameItem.SelectedItem.Value);
					}
                }

                DhcpServer server = new DhcpServer
                {
                    AddNewScopes = cbAutoAddScopes.Checked,
                    NodeId = int.Parse(SelectedNodeId.Value),
                    ParentId = 2,
                    CredentialId = credentialId,
                    GroupType = GroupNodeType.DhcpServer,
                    PercentUsed = 0,
                    Status = 1,
                    ReservedCount = 0,
                    TotalCount = 0,
                    TransientCount = 0,
                    UsedCount = 0,
                    ScanInterval = Convert.ToInt32(Math.Round(ServerScanInterval.Value.TotalMinutes)),
                    ServerType = CredSelector.DhcpServerType,
                    ClusterId = clusterId
                };

                if (cbEnableSubnetScanning.Checked)
                {
                    TimeSpan span = NewSubnetInterval.Value;
                    server.NewScopesScanInterval = Convert.ToInt32(Math.Round(span.TotalMinutes));
                    server.ScanNewScopesByDefault = true;
                }
                else
                {
                    server.ScanNewScopesByDefault = false;
                }

                SwisOpCtx addDhcpServerSwisOpCtx = new SwisOpCtx(
                    "Add new DHCP Server",
                    delegate (IpamClientProxy proxy, object param)
                    {
                        return proxy.AppProxy.DhcpServer.Add(server);
                    }
                );
                SwisConnector.RunSwisOps(new SwisOpCtx[] { addDhcpServerSwisOpCtx });

                //TODO: add handling for navigating to last scope/server: IPAM-3288 also IPAM-3276, IPAM-3255
                //string refUrl = string.Format(
                //    "Dhcp.Management.aspx?tab=scopes&added=true&lastscope={0}&nodeid={1}",
                //    maxscopeid, int.Parse(SelectedNodeId.Value).ToString());
                var refUrl = newDhcpManagementUrl;
                SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
            }
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = newDhcpManagementUrl;
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }

        protected void OnAddOrionNodeClick(object src, EventArgs args)
        {
            var redirector = new DhcpDnsServerAddRedirector(this);
            redirector.RedirectByOrionNodeCount(0);
        }

        protected string AddNodeLinkBeginTag()
        {
            addNodeLink.Visible = true;
            var textBuilder = new StringBuilder();

            using (var sw = new StringWriter(textBuilder))
            using (var textWriter = new HtmlTextWriter(sw))
                addNodeLink.RenderBeginTag(textWriter);

            addNodeLink.Visible = false;

            return textBuilder.ToString();

        }
		
		protected void ServerChanged(DhcpServerType type)
		{
			if(type == DhcpServerType.Infoblox)
			{
				vrfGroupName.Enabled = false;
			}
			else
			{
				vrfGroupName.Enabled = true;
			}
		}
    }
}
