﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Utility;

public partial class Orion_IPAM_IPAddressPopup : System.Web.UI.Page
{
    #region Properties

    public IPNode IPNode { get; set; }

    #endregion // Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        IpamIPNode ipamIPNode = NetObjectFactory.Create(Request.Form["NetObject"]) as IpamIPNode;
        if (ipamIPNode != null)
        {
            this.IPNode = ipamIPNode.IPAddress;
            this.ipAddressLink.IPNode = ipamIPNode.IPAddress;
        }

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    }

    #endregion // Events

    #region Methods

    protected string GetPropertyTextName(string property)
    {
        return LocalizationHelper.GetEntityDisplayString(
            IPNode.EIM_ENTITYNAME.Replace('.', '_'), property);
    }

    protected string GetPropertyTextValue<T>(T obj, string property)
    {
        System.Reflection.PropertyInfo propertyInfo = typeof(T).GetProperty(property);
        if (propertyInfo == null)
        {
            throw new ArgumentException(string.Format(
                "Property '{0}' does not exist for type IPNode.", property));
        }
        if (obj == null)
            return "";
        object propertyValue = propertyInfo.GetValue(obj, null);
        return (propertyValue != null) ? GetLocalizedProperty("IPType", propertyValue.ToString()) : "&nbsp;";
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    #endregion // Methods
}
