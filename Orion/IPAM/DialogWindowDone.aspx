<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ChromelessMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.DialogWindowDone"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_279 %>" CodeFile="DialogWindowDone.aspx.cs" %>
<%@ Register Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" TagPrefix="IPAMmaster" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<!-- we will be sending up our 'complete' message -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server" />

<div id=formbox><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_280 %></div>
  
</asp:Content>
