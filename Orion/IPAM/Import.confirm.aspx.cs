using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportConfirm : CommonPageServices
	{
        PageImportConfirm mgr;
        public ImportConfirm() { mgr = new PageImportConfirm(this); }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["GroupID"] != null)
                this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));

            btnBack.NavigateUrl = mgr.GetBackLinkUrl();
        }
    }
}
