<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeEdit"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_396 %>" CodeFile="Dhcp.Scope.Edit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DhcpServerScanSettings.ascx" TagName="DhcpServerScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NewDhcpScopeSettings.ascx" TagName="NewDhcpScopeSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" TagName="CustomPropertyEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
div.sw-form-heading { font-size: small; margin-top: 16px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: small; margin: 0; }
.sw-heading { width: 130px; }
.sw-form-cols-normal td.sw-form-col-label { width: 140px; }
</IPAMmaster:CssBlock>


<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

<script type="text/javascript">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
</script>

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_180 %></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_579 %>
        </div></td>
        <td><div class="sw-form-item x-item-disabled">
            <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" ReadOnly="true" runat="server" />
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_397 %>
        </div></td>
        <td><div class="sw-form-item x-item-disabled">
            <asp:TextBox ID="txtDhcpServer" CssClass="x-form-text x-form-field" ReadOnly="true" runat="server" />
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_398 %>
        </div></td>
        <td><div class="sw-form-item">
            <span class="x-item-disabled"><asp:TextBox ID="txtNetworkAddress" ReadOnly="true" CssClass="x-form-text x-form-field sw-withlblnum sw-item-disabled-iefix" runat="server" Width="140" /></span>
            <span class=sw-pairedlbl><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_44 %></span>
            <span class=" x-item-disabled" style="padding-left: 30px">
            <asp:TextBox ID="txtCidr" CssClass="x-form-text x-form-field sw-pairednum" ReadOnly="true" runat="server" Width="40" />
            </span>
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_53 %>
        </div></td>
        <td><div class="sw-form-item x-item-disabled">
            <asp:TextBox ID="txtNetworkMask" CssClass="x-form-text x-form-field" ReadOnly="true" runat="server" />
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_183 %>
        </div></td>
        <td><div class="sw-form-item x-item-disabled">
          <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" ReadOnly="true" runat="server" />
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_185 %>
        </div></td>
        <td><div class=sw-form-item>
          <asp:TextBox ID="txtVLAN" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_186 %>
        </div></td>
        <td><div class=sw-form-item>
          <asp:TextBox ID="txtLocation" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
    </tr>
    </table></div>
       
    <asp:PlaceHolder runat="server" ID="CustomFieldsPlaceHolder">
        <div><!-- ie6 --></div>
        
        <div class="sw-form-heading">
        <h2>
        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %></h2>
        </div>
        <div>
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
        </div>
    </asp:PlaceHolder>

</div>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList" />

<div id="ChromeButtonBar" runat="server" class="sw-btn-bar" style="padding-left: 163px">
    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary" CausesValidation="true"/>
    <orion:LocalizableButtonLink runat="server" NavigateUrl="~/api2/ipam/ui/scopes" LocalizedText="Cancel" DisplayType="Secondary"/>
</div>

</asp:Content>
