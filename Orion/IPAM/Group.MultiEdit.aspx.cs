﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class GroupMultiEdit : CommonPageServices
{
    #region Constants

    private const string AttributeCheckBoxId = "chbAttribute";
    private const string PropertyTextId = "PropertyText";

    #endregion // Constants
    #region Properties

    public int ClusterId { get; private set; }
    public string GroupsExclusion { get; private set; }
    public string ShowParentChangeConfirmDlg
    {
        get { return this.Editor.ShowParentChangeConfirmDlg.ToString().ToLower(); }
    }

    #endregion // Properties

    #region Constructors

    private PageGroupMultiEditor Editor { get; set; }
    public GroupMultiEdit()
    {
        this.GroupsExclusion = string.Empty;
        this.Editor = new PageGroupMultiEditor(this);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.Editor.LoadPage();

        if (this.AccessCheck != null)
            this.AccessCheck.GroupIds.AddRange(this.Editor.GroupIds);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            List<GroupNode> groups = this.Editor.RetrieveGroupObjects();
            InitControls(groups);
        }
    }

    protected void OnCheckBoxesServerValidate(object source, ServerValidateEventArgs e)
    {
        List<CheckBox> checkBoxes = new List<CheckBox>();

        // Group Fields
        checkBoxes.Add(this.chbParent);
        checkBoxes.Add(this.chbComments);

        e.IsValid = checkBoxes.Exists(o => o.Checked) || CustomPropertyEdit.CheckIfCpChanged();
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(List<GroupNode> groups)
    {
        List<GroupNode> groupNodes = (groups ?? new List<GroupNode>());
        this.ClusterId = groupNodes.FirstOrDefault()?.ClusterId ?? 0;
        bool hasIPv6Groups = false;
        groupNodes.ForEach(g => hasIPv6Groups |=
            (g.GroupType == GroupNodeType.PrefixAggregate) ||
            (g.GroupType == GroupNodeType.IPv6Subnet)
        );

        var exclusions = groupNodes.
            Where(g => g.GroupType == GroupNodeType.Group).
            Select(q => q.GroupId.ToString()).ToArray();
        if (exclusions != null && exclusions.Length > 0)
        {
            this.GroupsExclusion = "(" + string.Join(",", exclusions) + ")";
        }

        #region Groups List

        if (this.GroupsList != null)
        {
            foreach (GroupNode groupNode in groupNodes)
            {
                ListItem li = new ListItem();
                li.Text = PageGroupMultiEditor.GroupNodeName(groupNode);
                li.Attributes.Add("class", PageGroupMultiEditor.ListItemCssClass(groupNode));
                this.GroupsList.Items.Add(li);
            }

            if (this.txtGroupsCountLabel != null)
            {
                this.txtGroupsCountLabel.Text = string.Format(
                    Resources.IPAMWebContent.IPAMWEBCODE_VB1_1, this.GroupsList.Items.Count);
            }
        }

        #endregion // Groups List

        #region Group Fields

        if (this.chbParent != null)
        {
            this.chbParent.Enabled = !hasIPv6Groups;
        }
        SetJSCheckBoxControl(this.chbComments, this.txtComments);

        #endregion // Group Fields

        CustomPropertyEdit.RetrieveCustomProperties(new GroupNode(), groups.Select(g => g.GroupId).ToList());
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickCancel(object sender, EventArgs e)
    {
        Response.Redirect("~/Orion/IPAM/Subnets.aspx");
    }

    private void Save()
    {
        Dictionary<string, object> changedProperties = CreatePropertyChanges();
        Dictionary<string, string> changedCustomProperties = CustomPropertyEdit.GetMultiEditChanges();
        this.Editor.Store(true, changedProperties, changedCustomProperties);
    }

    private Dictionary<string, object> CreatePropertyChanges()
    {
        Dictionary<string, object> changes = new Dictionary<string, object>();

        if ((this.hfParent != null) &&
            (this.chbParent != null) && this.chbParent.Checked)
        {
            int parentId = -1;
            if (int.TryParse(this.hfParent.Value, out parentId))
            {
                changes.Add("ParentId", parentId);
            }
        }

        if ((this.txtComments != null) &&
            (this.chbComments != null) && this.chbComments.Checked)
        {
            changes.Add("Comments", this.txtComments.Text);
        }

        return changes;
    }

    private void SetJSCheckBoxControl(Control checkBox, Control control)
    {
        if ((checkBox == null) || (control == null))
        {
            return;
        }

        this.JS.AddScript(
            string.Format("$SW.MultiEdit.CheckBoxInit('{0}', '{1}');{2}",
            checkBox.ClientID, control.ClientID, Environment.NewLine)
        );
    }
    #endregion // Methods
}
