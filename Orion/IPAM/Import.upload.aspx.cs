using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Import;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class ImportUpload : CommonPageServices
    {
        PageImportUpload mgr;
        public ImportUpload() { mgr = new PageImportUpload(this); }

        protected void Page_Load(object sender, EventArgs e)
        {
            var groupId = Convert.ToInt32(Request.QueryString["GroupID"]);
            if (Request.QueryString["GroupID"] != null)
            {
                this.AccessCheck.GroupIds.Add(groupId);
            }

            if (mgr.IsSubnetOrSubnetIpv6(groupId))
            {
                mgr.HideImportStructureAndAllRadioButtons();
            }

            this.btnIntroLink.NavigateUrl = mgr.GetIntroLinkUrl();
            
            // Invoked by form.submit after confirmed submit of first sheet, which was already uploaded
            if (hfIgnoreHeadersWarn.Value == "true" && !string.IsNullOrEmpty(hfObjectID.Value))
            {
                // we need to fill validation state to check page for validity later
                Page.Validate();
                mgr.CompleteUpload();
            }
        }

        protected void Upload_Click(object src, EventArgs args)
        {
            mgr.CompleteUpload();
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }

        protected void ResetErrorControls()
        {
            ErrorAdvice.Visible = false;
            InvalidCsvFile.Visible = false;
            InvalidXlsFile.Visible = false;
            FileParseError.Visible = false;
            InternalMessage.Text = "";
            hfHeadersWarn.Value = null;
            CannotUseZipFileErrorPlaceHolder.Visible = false;
            ZipFileRequiredErrorPlaceHolder.Visible = false;
            ZipFileMissingData.Visible = false;
        }

        protected void ValidateUploadFile(object src, ServerValidateEventArgs args)
        {
            string errorMessage = string.Empty;
            bool fileLoaded = false;

            ResetErrorControls();
            
            if (mgr.IsValidUpload(out errorMessage, out fileLoaded) == false)
            {
                if (errorMessage == ImportHelperErrorMsg.ERRORMSG_WORKSHEETHEADERS_DOESNTMATCH)
                {
                    hfHeadersWarn.Value = ImportHelperErrorMsg.ERRORMSG_WORKSHEETHEADERS_DOESNTMATCH;
                } 
                else if (errorMessage == ImportHelperErrorMsg.ERRORMSG_ZIP_FILE_REQUIRED_FOR_THIS_TYPE_OF_UPLOAD)
                {
                    ZipFileRequiredErrorPlaceHolder.Visible = true;
                }
                else if (errorMessage == ImportHelperErrorMsg.ERRORMSG_CANNOT_USE_ZIP_FILE_FOR_THIS_TYPE_OF_UPLOAD)
                {
                    CannotUseZipFileErrorPlaceHolder.Visible = true;
                }
                else if (errorMessage == ImportHelperErrorMsg.ERRORMSG_ZIP_FILE_MISSING_DATA)
                {
                    ZipFileMissingData.Visible = true;
                }
                else
                {
                    ErrorAdvice.Visible = true;

                    if (fileLoaded)
                        FileParseError.Visible = true;
                    else if (hfFileType.Value.Equals("xls"))
                        InvalidXlsFile.Visible = true;
                    else
                        InvalidCsvFile.Visible = true;

                    InternalMessage.Text = errorMessage;
                }
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }
}
