<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="True" CodeFile="Import.struct.pickcolumns.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportStructPickcolumns"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_354 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewSpreadsheetColumnsImport" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
   
   <IPAM:AccessCheck ID ="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
   
   <IPAMmaster:JsBlock Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server>
    $SW['PossibleValues'] = [];
    $SW['ColumnUsage'] = {};
    $SW['ColumnBoxes'] = [];
    $SW['CreateTypeRadio'] = '';
    $SW.isGroupTypeSelected = false;
    var _inProgress = true;

    $SW.StartValidation = function(){
      if(!$SW.ValidateRequiredColumns()) {
          return false;
      }
      // Sending CmbBox values in Query
      var s='';
      Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( c ) 
           s += c.value+'|';
        });

      $.ajax({
        type: "POST",
        url: location.href+'&SaveCmb='+s,
        async: false,
        data: '',
        success: function() {
          $SW.ShowValidationProgressBar();
        },
        error: function(jqXHR, textStatus, errorThrown) { 
            err(jqXHR.statusText + '<br/>' + jqXHR.responseText);
        }
      });

      return false;
    };
    
    $SW.ValidateRequiredColumns = function(){
        var importType = Ext.getUrlParam('ImportType');
        if (importType == 2) {
            if(!$SW.isGroupTypeSelected ) {
                $SW.Alert( "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_74;E=js}", "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_80;E=js}", { buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING } );
                return false;
            }
        }
        return true;
    };

    Ext.onReady(function()
     {
        var boxes = $SW['ColumnBoxes'];
        var transform = function(id){
            var item = $('#'+id)[0];
            var converted = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                transform: id,
                forceSelection: true,
                disabled: item.disabled || false
            });

            return converted;
        };

        $("select").each(function(i,o){
            var cb = transform(o.id);
            cb.store.sortInfo = { field: "value", direction: "ASC" };
            cb.on('change',function(that,newVal,oldVal){
              $SW.ComboUsageInUse(newVal,that.id);
              $SW.ComboUsageNotInUse(oldVal,that.id);
            });
            boxes.push(cb.id);
        });

        $SW.InitPossibleValue();

        $SW['CreateTypeRadio'] = $SW.nsGet($nsId, 'CreateSubnetsButtons_0');
     });

     $SW.UpdateCreateSubnet = function()
     {
        var visible = false;

        for (var i in $SW['ColumnUsage'])
        {
            var item = $SW['ColumnUsage'][i];

            if (item.inuse)
                visible = true;
        }

        if (!visible)
            $("#createSubnets").css("display", "none");
        else
            $("#createSubnets").css("display", "block");
     };

     $SW.ComboUsageInUse = function(val,by)
     {
        if(val == "-1") return; // do not import okay for all.

        var r = $SW.ColumnUsage[val];
        if(!r) return;
        r.inuse = by;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c || c.id == by ) return;

         var exist = c.store.getById(val);
         if( exist )
           c.store.remove(exist);
        });

        $SW.UpdateCreateSubnet();
        $SW.UpdateNextButton();
     };

     $SW.ComboUsageNotInUse = function(val)
     {
        var r = $SW.ColumnUsage[val];
        if( !r ) return;
        r.inuse = null;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c ) return;

         var exist = c.store.getById(val);
         if( !exist )
            c.store.addSorted(new Ext.data.Record({ value: val, text: r.text }, val));
        });

        $SW.UpdateCreateSubnet();
        $SW.UpdateNextButton();
     };
     
     $SW.UpdateNextButton = function()
     {
        var importType = Ext.getUrlParam('ImportType');
        if (importType == 2) {
            $SW.isGroupTypeSelected = Ext.getCmp($SW['ColumnBoxes'][0]).value != -1;
            if($SW.isGroupTypeSelected) $SW.ns$($nsId,'Next').removeClass('sw-form-dim');
            else $SW.ns$($nsId,'Next').addClass('sw-form-dim');
        }
     };

     $SW.InitPossibleValue = function()
     {
        var seen = {};
        var vals = [];

        Ext.each( $SW['ColumnBoxes'], function(o){
          var c = Ext.getCmp(o), v;
          if( !c ) return;
           v = c.getValue();

           c.store.each(function(r){
            if( !seen[r.data.value] )
            {
              var n = Ext.apply({ inuse: null }, r.data);
              seen[r.data.value] = n;
              vals.push(n);
            }
            return true;
           });

           if( v == "-1" )
             return;

           seen[v].inuse = c.id;
        });

        Ext.each($SW['ColumnBoxes'],function(id){
          var c = Ext.getCmp(id), v;
          if( !c ) return;
          Ext.each(vals,function(o){
            if( o.inuse && o.inuse != id )
            {
              var r = c.store.getById(o.value);
              if( r ) c.store.remove(r);
            }
          });
        });

        $SW['ColumnUsage'] = seen;
        $SW.UpdateCreateSubnet();
        $SW.UpdateNextButton();
     }
   
    Ext.getUrlParam = function(param)
    {
       var params = Ext.urlDecode(location.search.substring(1));
       return param ? params[param] : params;
    };
    
    $SW.ConfirmBack = function()
    {
        var groupId = Ext.getUrlParam('GroupID');
        var importType = Ext.getUrlParam('ImportType');

        if (importType == 1)
        {
            var objectId = Ext.getUrlParam('ObjectID');
            var type = Ext.getUrlParam('Type');
            
            var link = 'import.pickcolumns.aspx?ObjectID=' + objectId + '&Type=' + type + '&ImportType=' + importType + '&OpenedFrom=2';
            if (groupId)
                link +=  '&GroupID=' + groupId;

            window.location = link;
        }
        else
        {
            Ext.MessageBox.confirm(
             '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_76;E=js}',
             '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_77;E=js}',
             function(btnid){
              if( btnid == 'yes' )
              {
                  if(groupId)
                    window.location = 'import.upload.aspx?GroupID=' + groupId;
                  else
                    window.location = 'import.upload.aspx';
              }
             });
         }
    };

Ext.getUrlParam = function(param)
    {
       var params = Ext.urlDecode(location.search.substring(1));
       return param ? params[param] : params;
 };
    
function err(msg)
{
   msg = msg || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}';

   Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
       msg: msg,
       buttons: Ext.Msg.OK,
       animEl: this,
       icon: Ext.MessageBox.ERROR
   });

   return false;
}

    
function StartTask()
{
    $SW.GetUrlParamForImport();

    var dataparams = '';
    if ($SW['ImportType'] == 1)
        dataparams =  'requesttype=validateimportip';
    else
        dataparams =  'requesttype=validateimportstruct';
    
    dataparams += '&verb=CreateTask&filetype='+$SW['Type']+'&objectid='+$SW['ObjectID'];
    
    if($SW['GroupID'] != null)
        dataparams += "&GroupID=" + $SW['GroupID'];

    if($SW['CreateType'] != null)
        dataparams += "&createtype=" + $SW['CreateType'];
        
    if($SW['IsZipImport'] != null)
        dataparams += "&IsZipImport=" + $SW['IsZipImport'];

    $.ajax({
        type: "POST",
        url: 'ImportExportProvider.ashx',
        async: false,
        data: dataparams,
        error: err
      });
      
}

function RecvTaskStatus(o)
{
  if( o.status == 'Completed' ) {
      _inProgress = false;
      ProgressEnd('ok',o);
  } else
    if (o.status == 'Failed')
       ProgressEnd('failed',o.error);
    else
    {
        var val = typeof(o.pos) == "string" ? eval(o.pos) : o.pos;
        val /= 100;
        window.localStorage.setItem("lastAction", Date.now());
        Ext.MessageBox.updateProgress(val , o.msg, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}');
    }
}

function GetTaskStatus()
{
  var requestType = $SW['ImportType'] == 1 ? 'validateimportip' : 'validateimportstruct';
  
  Ext.Ajax.request({
       url: 'ImportExportProvider.ashx',
       method: 'POST',
       success: function(result, request)
       {
            var o = Ext.util.JSON.decode(result.responseText);
            RecvTaskStatus(o);
       },
       failure: function(result, request) {
                  if(runner!=null)
                     runner.stopAll();
                  },
       params:
       {
        requesttype: requestType,
        verb: 'GetStatusValidateTask',
        objectid:$SW['ObjectID']
       }
       });
}

function ProgressEnd(btnid,value)
{
    $SW.GetUrlParamForImport();

    Ext.MessageBox.hide();
    if(runner!=null)
        runner.stopAll();
        
    if( btnid === 'ok' )
    {
        var commonParams = $SW['ObjectID'] +'&Type=' + $SW['Type'] + '&ImportType=' + $SW['ImportType'] + '&OpenedFrom=2';
        if($SW['GroupID'] != null)
            commonParams += "&GroupID=" + $SW['GroupID'];

        if ($SW['CreateType'] != null)
            commonParams += "&CreateType=" + $SW['CreateType'];
        
        if ($SW['IsZipImport'] != null)
            commonParams += "&IsZipImport=" + $SW['IsZipImport'];
        
        if (value.validationSuccess == false)
        {
            var link = $SW.appRoot() + 'Orion/IPAM/Import.preview.aspx?ObjectID=' + commonParams;            
            window.location.href = link;
        }
        else
        {
            var link = $SW.appRoot() + 'Orion/IPAM/Import.customprop.aspx?ObjectID=' + commonParams;
            window.location.href = link;
        }
    }
    
    if( btnid == 'failed' && _inProgress === true)
        err(value);
}

var runner = new Ext.util.TaskRunner();

var StatusTask = {
    run: function(){
        GetTaskStatus();
    },
    interval: 500 //1 second
}

$SW.ShowValidationProgressBar = function(el)
{
     _inProgress = true;
     StartTask();

     Ext.MessageBox.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_79;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}',
        progressText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_62;E=js}',
        width:300,
        progress:true,
        closable:true,
        animEl: el ? el.id : null
     });
   
   runner.start(StatusTask);
};

$SW.GetUrlParamForImport = function() {
    $SW['ObjectID'] = Ext.getUrlParam('ObjectID');
    $SW['Type'] = Ext.getUrlParam('Type');
    $SW['GroupID'] = Ext.getUrlParam('GroupID');
    $SW['ImportType'] = Ext.getUrlParam('ImportType');
    $SW['IsZipImport'] = Ext.getUrlParam('IsZipImport');

    var radioButton = $('#'+$SW['CreateTypeRadio'])[0];
    $SW['CreateType'] = radioButton.checked ? 1 : 2;
};
   </IPAMmaster:JsBlock>

<div style="margin-left: 10px;">

     <table width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_354 %></h1>
        </td>
        </tr>
      </table>

    <div id="formbox">

        <div style="padding-bottom: 10px;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_351 %> <br />
        </div>

        <div class="sw-form-subheader"><asp:Label runat="server" ID="HeaderText" /></div>
        
        <div><!-- fix layout --></div>

        <div style="padding-left: 20px">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-table">
                    <td class="sw-form-col-label-large" style="width: 400px"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr id="groupImport" runat="server" Visible="false">
                    <td><div>
                        <asp:Label ID="GroupType" runat="server"></asp:Label>
                        <asp:Label ID="GroupTypeEntity" runat="server" Visible="false"></asp:Label>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:DropDownList ID="GroupTypeAttributes" runat="server"></asp:DropDownList>
                    </div></td>
                    <td />
                </tr>
                <tr id="ipImport" runat="server" Visible="false">
                    <td colspan="3"><div>
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_355 %>
                    </div></td>
                </tr>
            </table>
        </div>

        <div><!-- fix layout --></div>

        <div class="sw-form-subheader" style="padding-top: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_353 %></div>

        <div><!-- fix layout --></div>

        <asp:Repeater ID="ColumnsRepeater" runat="server">
            <HeaderTemplate>
                <div style="padding-left: 20px">
                    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                        <tr class="sw-form-cols-table">
                            <td class="sw-form-col-label-large" style="width: 400px;"></td>
                            <td class="sw-form-col-control"></td>
                            <td class="sw-form-col-comment"></td>
                        </tr>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr>
                            <td><div>
                                <asp:Label ID="columnName" runat="server"></asp:Label>
                                <asp:Label ID="columnEntity" runat="server" Visible="false"></asp:Label>
                            </div></td>
                            <td><div class="sw-form-item">
                                <asp:DropDownList ID="ddlAttributes" runat="server"></asp:DropDownList></div>
                            </div></td>
                            <td />
                        </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </table>
                </div>
            </FooterTemplate>
        </asp:Repeater>

        <div id="createSubnets" class="sw-div-create-subnets" style="width: auto">
            <asp:RadioButtonList ID="CreateSubnetsButtons" runat="server">
                <asp:ListItem Selected="True" Value="1" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_356 %>" />
                <asp:ListItem Value="2" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_357 %>" />
            </asp:RadioButtonList>
        </div>
    </div>

    <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
         <orion:LocalizableButton runat="server" ID="Back" OnClientClick="$SW.ConfirmBack(); return false;" LocalizedText="Back" DisplayType="Secondary" />
         <orion:LocalizableButton runat="server" ID="Next" OnClientClick="$SW.StartValidation(); return false;" LocalizedText="Next" DisplayType="Primary" />
         <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
   </div>

</div>
</asp:Content>
