<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.confirm.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportConfirm"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_337 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
    
<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewImportSummary" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck ID ="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock runat="server">
#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
</IPAMmaster:CssBlock>
<IPAMmaster:JsBlock ID="JsBlock1" Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server>

var _inProgress = true;

Ext.getUrlParam = function(param)
{
    var params = Ext.urlDecode(location.search.substring(1));
    return param ? params[param] : params;
 };
    
function err(msg)
{
   msg = msg || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}';

   Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
       msg: msg,
       buttons: Ext.Msg.OK,
       animEl: this,
       icon: Ext.MessageBox.ERROR
   });
}

function StartTask()
{
    $SW['ObjectID'] = Ext.getUrlParam('ObjectID');
    $SW['Type'] = Ext.getUrlParam('Type');
    $SW['GroupID'] = Ext.getUrlParam('GroupID');
    $SW['ImportType'] = Ext.getUrlParam('ImportType');
    $SW['CreateType'] = Ext.getUrlParam('CreateType');
    $SW['IsZipImport'] = Ext.getUrlParam('IsZipImport');
    
    var dataparams = '';
    if ($SW['ImportType'] == 1)
        dataparams =  'requesttype=importip';
    else if ($SW['ImportType'] == 2)
        dataparams =  'requesttype=importstruct';
    else
        dataparams =  'requesttype=importall';

    dataparams += '&verb=CreateTask&filetype='+$SW['Type']+'&objectid='+$SW['ObjectID'];
    
    if($SW['GroupID'] != null)
        dataparams += "&GroupID=" + $SW['GroupID'];

    if ($SW['CreateType'] != null)
        dataparams += "&createtype=" + $SW['CreateType'];
        
    if ($SW['IsZipImport'] != null)
        dataparams += "&IsZipImport=" + $SW['IsZipImport'];

    $.ajax({
        type: "POST",
        url: 'ImportExportProvider.ashx',
        async: false,
        data: dataparams,
        error: err
      });
      
}

function RecvTaskStatus(o)
{
  if( o.status == 'Completed' ) {
      _inProgress = false;
      ProgressEnd('ok',o);
  } else
    if (o.status == 'Failed')
       ProgressEnd('failed',o.error);
    else
    {
        var val = typeof(o.pos) == "string" ? eval(o.pos) : o.pos;
        val /= 100;
        window.localStorage.setItem("lastAction", Date.now());
        Ext.MessageBox.updateProgress(val , o.msg, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_72;E=js}');
    }
}

function GetTaskStatus()
{
  var requestType = $SW['ImportType'] == 1 ? 'importip' : 'importstruct';

  Ext.Ajax.request({
       url: 'ImportExportProvider.ashx',
       method: 'POST',
       success: function(result, request)
       {
            var o = Ext.util.JSON.decode(result.responseText);
            RecvTaskStatus(o);
       },
       failure: function(result, request) {
                  if(runner!=null)
                     runner.stopAll();
                  },
       params:
       {
        requesttype: requestType,
        verb: 'GetStatusTask',
        objectid:$SW['ObjectID']
       }
       });
}

function ProgressEnd(btnid,value)
{
   Ext.MessageBox.hide();
   if(runner!=null)
     runner.stopAll();
    
   if( btnid == 'ok' )
   {
        var link = $SW.appRoot() + 'Orion/IPAM/Import.summary.aspx?ObjectID=' + $SW['ObjectID'] +'&Type=' + $SW['Type'] + '&ImportType=' + $SW['ImportType']+ '&OpenedFrom=1';

        if($SW['GroupID'] != null)
            link += "&GroupID=" + $SW['GroupID'];

        if ($SW['CreateType'] != null)
            link += "&CreateType=" + $SW['CreateType'];

        window.location.href = link;
   }
   
   if( btnid == 'failed' && _inProgress === true)
       err(value);
}

var runner = new Ext.util.TaskRunner();

var StatusTask = {
    run: function(){
        GetTaskStatus();
    },
    interval: 500 //1 second
}

$SW.ShowValidationProgressBar = function(el)
{
    _inProgress = true;
     StartTask();

     Ext.MessageBox.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_73;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_72;E=js}',
        progressText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_62;E=js}',
        width:300,
        progress:true,
        closable:true,
        animEl: el ? el.id : null
     });
   
   runner.start(StatusTask);
};
</IPAMmaster:JsBlock>

<div style="margin-left: 10px;">

    <table width="auto" style="table-layout: fixed;  margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb">
      <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	  <h1><%= Page.Title %></h1>
    </td>
    </tr>
    </table>

    <div id="formbox">
        <div id="formheader" runat="server" class="sw-form-label"> 
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_338 %>
        </div>

        <asp:Repeater ID="IPAddressRepeater" runat="server" >
            <HeaderTemplate>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-table">
                        <td class="sw-form-col-label" style="width: 300px;"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                    <tr>
                        <td><div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_339 %></div></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><div class="sw-field-label" style="padding-left:20px;">
                        <asp:Label ID="columnName" runat="server" Font-Bold="true"></asp:Label>
                    </div></td>
                    <td><div class="sw-field-label">
                        <asp:Label ID="spreadsheetColumn" runat="server"></asp:Label>
                    </div></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater ID="SubnetRepeater" runat="server" >
            <HeaderTemplate>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-table">
                        <td class="sw-form-col-label" style="width: 300px;"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                    <tr>
                        <td><div class="sw-form-subheader"><asp:Label runat="server" id ="lblSubnetInfo"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_340 %></asp:Label></div></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><div class="sw-field-label" style="padding-left:20px;">
                        <asp:Label ID="columnName" runat="server" Font-Bold="true"></asp:Label>
                    </div></td>
                    <td><div class="sw-field-label">
                        <asp:Label ID="spreadsheetColumn" runat="server"></asp:Label>
                    </div></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:Repeater ID="CustomRepeater" runat="server" >
            <HeaderTemplate>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-table">
                        <td class="sw-form-col-label" style="width: 300px;"></td>
                        <td class="sw-form-col-control"></td>
                        <td class="sw-form-col-comment"></td>
                    </tr>
                    <tr>
                        <td><div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_341 %></div></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><div class="sw-field-label" style="padding-left:20px;">
                        <asp:Label ID="columnName" runat="server" Font-Bold="true"></asp:Label>
                    </div></td>
                    <td><div class="sw-field-label">
                        <asp:Label ID="spreadsheetColumn" runat="server"></asp:Label>
                    </div></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
   </div>
        
    <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
         <orion:LocalizableButtonLink runat="server" ID="btnBack" LocalizedText="Back" DisplayType="Secondary" />
         <orion:LocalizableButton runat="server" ID="btnImport" OnClientClick="$SW.ShowValidationProgressBar(this); return false;" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_234 %>" DisplayType="Primary" />
         <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
    
    </div>

</div>
</asp:Content>
