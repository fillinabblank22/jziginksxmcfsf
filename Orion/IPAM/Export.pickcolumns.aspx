<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="True" CodeFile="Export.pickcolumns.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ExportPickcolumns"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_299 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewExportSelectedSubnets" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
<IPAMmaster:JsBlock ID="JsBlock1" requires="ext,sw-helpserver" Orientation=jsInit runat="server" />

<IPAMmaster:CssBlock runat="server">
#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
.sw-form-cols-normal td.sw-form-col-label { width: 140px; }
.sw-form-item .x-form-field-wrap .x-form-text { width: 350px; }
.sw-form-item .x-form-text { width: 368px; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock ID="WarnOnZeroItems" Requires="ext" visible=false Orientation="jsInit"  runat="server">
Ext.onReady(function(){
    Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_56;E=js}',
       msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_57;E=js}",
       buttons: { ok: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_58;E=js}' },
       closable: false,
       modal: true,
       fn: function(){ window.location = $SW.appRoot() + 'Orion/IPAM/subnets.aspx'; },
       icon: Ext.MessageBox.WARNING
    });
});
</IPAMmaster:JsBlock>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-manage.js" Orientation="jsPostInit"  runat="server">
$SW.subnet.ShowExportList = function(el)
{
   var did = 'subnetsToExport', dlg = $SW[did];
   $SW.ext.dialogWindowOpen(dlg, el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_59;E=js}', '', 'OrionIPAMPHViewExportSelectedSubnets.htm' );
   
   var iframeName = dlg.items.map[did + '-iframe'].el.dom.name;

   var loadGrid = function(target){
        Ext.DomHelper.append(dlg.body, {
           tag: 'form', method: 'POST', target: target,
           action: 'Export.SubnetList.aspx?NoChrome=True',
           children: [{ tag: 'input', type: 'hidden', value: $SW['SubnetsId'], name: 'ObjectId'}]
        }).submit();
   };

   setTimeout((function(){ var t = iframeName; return function(){ loadGrid(t); } })(), 100);
};

Ext.onReady(function()
{ 
    var transform = function(id){
         var item = $('#'+id)[0];
        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };
     
    $SW['ExportFileTypeId'] = transform( $SW.nsGet($nsId, 'ddlExportFileType' ) ).id;
     
});

//Progress window

function err()
{
   err('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}');
}

function err(msg)
{
   Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
       msg: msg,
       buttons: Ext.Msg.OK,
       animEl: this,
       icon: Ext.MessageBox.ERROR
   });        
}

function GetSelectedColumns()
{
  var selcolumns = [];
  
  $("INPUT[type='checkbox']").each(function(){
     if( this.checked )
       selcolumns.push( this.parentNode.attributes['ColumnMap'].value );
  });

  return selcolumns;
}

function GetExportType()
{
   var value  = Ext.getCmp($SW['ExportFileTypeId']).getValue();
   return value;
}

var runner = new Ext.util.TaskRunner();
var nextrun = null;

function GetTaskStatus(taskid)
{
  var now = new Date().getTime();

  if( nextrun != null && now < nextrun )
    return;

  // this will make sure our task doesn't pile up multiple asyncronous commands
  nextrun = now + 86400000;

  Ext.Ajax.request({
    url: 'ImportExportProvider.ashx',
    method: 'POST',
    success: function(result, request)
    {
        nextrun = new Date().getTime() + 1500; // run in 1.5 seconds
        var o = Ext.util.JSON.decode(result.responseText);
        RecvTaskStatus(o);
    },
    failure: function(result, request) { StatusTaskCancel(); err(); },
    params:
    {
      requesttype: 'export',
      verb: 'GetStatusTask',
      objectid: taskid || $SW['TaskId']
    }
  });
}

function StatusTaskInit()
{
  StatusTaskCancel();
  var task = { run: GetTaskStatus, interval: 500, swkilled: false };
  $SW['StatusTask'] = task;
  return task;
}

function StatusTaskCancel()
{
  nextrun = null;
  var task = $SW['StatusTask'] || null;
  if( task ) { $SW['StatusTask'] = null; task.swkilled = true; runner.stop(task); }
  Ext.MessageBox.hide();
}

function StatusTaskStart(task,taskid)
{
  nextrun = null;
  if( !task ) return false;
  task.taskid = taskid;
  task.args = [taskid];
  runner.start(task);
  return true;
}

function ProgressEnd(btnid,value)
{
   if( btnid == 'cancel' )
   {
     Cancel();
     return;
   }

   StatusTaskCancel();

   if( btnid == 'ok' )
   {
       if(value != null)
       {
           window.location.href = $SW.appRoot() + value;
       }
       $SW.subnet.ExportFinished();
    }
    
   if( btnid == 'failed')
       err(value);
}

function RecvTaskStatus(o)
{
  if( o.status == 'Completed' ) 
      ProgressEnd('ok',o.url);
  else
    if (o.status == 'Failed')
       ProgressEnd('failed',o.msg + ': ' + o.error);
    else
    {
       window.localStorage.setItem("lastAction", Date.now());
       Ext.MessageBox.updateProgress( typeof(o.pos) == "string" ? parseFloat(o.pos) : o.pos, o.msg, o.title );
    }
}

$SW['GoBack'] = function(el)
{
    Ext.DomHelper.append(document.body, {
        id: 'post-forward',
        tag: 'form',
        method: 'POST',
        action: 'ExportStructure.PickColumns.aspx',
        children: [{ tag: 'input', type: 'hidden', value: $SW['AllGroupNodesIds'], name: 'GroupNodeIds' }, 
        { tag: 'input', type: 'hidden', value: $SW['SelectedSubnetIds'], name: 'SubnetID' },
        { tag: 'input', type: 'hidden', value: $SW['SelectedGroupIds'], name: 'GroupID' },
        { tag: 'input', type: 'hidden', value: 'all', name: 'ExportType' },
        { tag: 'input', type: 'hidden', value: $SW['SelectedStructureColumns'], name: 'SelectedStructureColumns' }]
    }).submit();
};

$SW['ExportStart'] = function(el)
{
    var task = StatusTaskInit();
    var selectedcolumns = GetSelectedColumns();
    var type = GetExportType();

    $SW.ShowExportProgressBar();

    $.ajax({
    type: "POST",
    url: 'ImportExportProvider.ashx',
    async: false,
    data: "requesttype=export&verb=CreateTaskExportAll&filetype=" + type + "&objectid=" + $SW['AllGroupNodesIds'] + "&structureColumns=" 
    + $SW['SelectedStructureColumns'] + "&subnetsIds=" + $SW['SubnetsId'] + "&columns=" + selectedcolumns.join('|'),
    error: err,
    success: function(msg){
        try
        {
            if( task.swkilled )
                return;

            var o = Ext.util.JSON.decode(msg);
            if( o && o.taskid )
            {
                StatusTaskStart(task,o.taskid);
                return;
            }

            err();
        }
        catch(e)
        {
            err();
        }
    }
    });
}

function Cancel()
{
  var task = $SW['StatusTask'] || null;
  StatusTaskCancel();

  if( task && task.taskid )
  Ext.Ajax.request({
       url: 'ImportExportProvider.ashx',
       method: 'POST',
       success: function(result, request){},
       params:
       {
        requesttype: 'export',
        verb: 'Cancel',
        objectid: task.taskid
       }
   });
}

$SW.ShowExportProgressBar = function(el)
{
     Ext.MessageBox.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_60;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_61;E=js}',
        progressText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_62;E=js}',
        width:300,
        buttons: Ext.MessageBox.CANCEL,
        progress:true,
        closable:true,
        animEl: el || null,
        fn: ProgressEnd
     });
};

</IPAMmaster:JsBlock>

<IPAMlayout:DialogWindow jsID="subnetsToExport" helpID="exporthelp" Name="subnetsToExport" Url="~/Orion/IPAM/res/html/loading.aspx" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_300 %>" height="400" width="480" runat="server">
<Buttons>
    <IPAMui:ToolStripButton ID="ToolStripButton2" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_301 %>" runat="server">
        <handler>function(){ $SW['subnetsToExport'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg ID="WindowMsg1" Name="ready" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['subnetsToExport'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg2" Name="unload" runat=server>
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['subnetsToExport'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="WindowMsg3" Name="close" runat=server>
        <handler>function(n,o){ $SW['subnetsToExport'].hide(); $SW.subnet.RefreshTab(true); }</handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<div style="margin-left: 10px;">
    
     <table  width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
             <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
                <h1><%= Page.Title %></h1>
            </td>
        </tr>
        </table>
        
    <div style="margin: 0px 0px 10px 10px;">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_302 %>
    </div>

    <div id="formbox">
            <div id="formheader" runat="server" class="sw-form-header">
                <asp:Label ID="lblHeader" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_303 %>"/>
            </div>

            <div style="margin: 10px 0px 10px 0px;">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_302 %> <a onclick="$SW.subnet.ShowExportList(this); return false;" href="#" style="color: #369"><asp:Label ID="linkTitle" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_304 %>" /></a>
            </div>

        <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
            <tr class="sw-form-cols-normal">
               <td class="sw-form-col-label"></td>
               <td class="sw-form-col-control"></td>
               <td class="sw-form-col-comment"></td>
            </tr>
            <tr>
                <td colspan=2><div class=sw-form-subheader>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_305 %>
                </div></td>
            </tr>
            <tr>
                <td colspan=2><div class=sw-form-item>
                    <asp:CheckBoxList ID="cblIPAddrcolumns"  runat="server" />
                </div></td>
            </tr>
            <tr>
                <td colspan=2><div class=sw-form-subheader>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_306 %>
                </div></td>
            </tr>
            <tr>
                <td><div class=sw-form-label style="width: 150px">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_307 %>
                </div></td>
                <td><div class=sw-form-item style="padding-left: 20px"> 
                    <asp:DropDownList ID="ddlExportFileType" runat="server" />
                </div></td>
            </tr>
            <tr>
                <td><div class=sw-form-label style="width: 150px">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_308 %>
                </div></td>
                <td><div class="sw-form-item x-item-disabled" style="padding-left: 20px">
                    <input type=text value="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_309 %>" readonly=readonly class="x-form-text x-form-field" />
                </div></td>
            </tr>
        </table></div>
    </div>
    
     <div class="sw-btn-bar" style="padding-left: 183px">
        <orion:LocalizableButton runat="server" ID="btnBack" LocalizedText="Back" OnClientClick="$SW['GoBack'](this); return false;" DisplayType="Secondary" />
        <orion:LocalizableButton runat="server" ID="btnExport" OnClientClick="$SW['ExportStart'](this); return false;" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_237 %>" DisplayType="Primary" />
        <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary" />
     </div>

</div>
</asp:Content>
