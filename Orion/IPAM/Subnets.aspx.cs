using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Common.Security;

namespace SolarWinds.IPAM.WebSite
{
    public partial class Subnets : CommonPageServices
    {
        PageSubnetManager manager;

        public Subnets()
        {
            manager = new PageSubnetManager(this);
        }

		public bool SubnetScanningEnabled
    	{
			get
			{
				return manager.SubnetScanningEnabled;
			}
    	}

        public bool ShowParentChangeConfirmDlg
        {
            get
            {
                return manager.ShowParentChangeConfirmDlg;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            MoveToolsetControl();
        }

        private void MoveToolsetControl()
        {
            // without moving the toolset control, IE8 crashes on Win2k8

            Control ToolsetObj = null;
            ToolsetObj = Locator.FindControlRecursive<Control>(Page.Master.Master, "toolsetObject", ToolsetObj);

            if (ToolsetObj == null || ToolsetObj.Visible == false)
                return;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter tw = new System.IO.StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            ToolsetObj.RenderControl(hw);
            ToolsetObj.Visible = false;
            LiteralControl ctx = new LiteralControl(sb.ToString());

	    try { Page.Form.Parent.Controls.Add(ctx); return; } catch( Exception ) {}
	    try { Page.Controls.Add(ctx); return; } catch( Exception ) {}
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (CommonWebHelper.IsMobileBrowser() || CommonWebHelper.IsMobileView(this.Request))
            {
                if (this.ipamFooter != null)
                    this.ipamFooter.clientEl = "mobileFooter";
            }

            // default is 150
            if( treeSubnet.Store.limit < manager.TreeMaxItemsPerGroup )
                treeSubnet.Store.limit = manager.TreeMaxItemsPerGroup;

            // default is 100
            if (tabSubnet.PageBar.pageSize < manager.IPGridPageSize)
                tabSubnet.PageBar.pageSize = manager.IPGridPageSize;

            // default is 100
            if (tabIPv6Subnet.PageBar.pageSize < manager.IPGridPageSize)
                tabIPv6Subnet.PageBar.pageSize = manager.IPGridPageSize;

            // default is 100
            if (tabGroup.PageBar.pageSize < manager.GroupGridPageSize)
                tabGroup.PageBar.pageSize = manager.GroupGridPageSize;

            // default is by address
            if (manager.TreeSortByAddr)
                treeSubnet.Store.orderByClause = "'{0}.GroupType ASC, {0}.AddressN ASC, {0}.FriendlyName ASC, {0}.CIDR ASC'";
            else
                treeSubnet.Store.orderByClause = "'{0}.GroupType ASC, {0}.FriendlyName ASC, {0}.AddressN ASC, {0}.CIDR ASC'";

            // default is 200
            if (treeSubnet.Columns[0].width < manager.TreeFirstColWidth)
                treeSubnet.Columns[0].width = manager.TreeFirstColWidth;
        }
    }
}