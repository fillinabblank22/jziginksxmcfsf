﻿using System;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Collections.Generic;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DNSRecords : CommonPageServices
    {
        #region Properties

        private const string QueryGroupId = "ObjectId";

        public bool IsZoneSet { get { return this.DnsZone.GroupId >= 0; } }
        public DnsZone DnsZone { get; set; }
        public DnsServer DnsServer { get; set; }
        
        #endregion // Properties

        #region Events

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InitDnsObjects();
            LoadDnsObjects( base.GetParam(QueryGroupId, -1));

            if (this.IsZoneSet == true)
            {
                Page.Title = string.Format(Resources.IPAMWebContent.IPAMWEBDATA_OH1_5, this.DnsZone.FriendlyName);
            }

            if (this.DnsZone.ZoneType == DnsZoneType.Secondary || 
                this.DnsZone.ZoneType == DnsZoneType.Stub || 
                this.DnsServer.ServerType == DnsServerType.Infoblox)
            {
                DisableAllGridTab();
            }
            
            InitControls();
        }

        #endregion // Events

        private void InitDnsObjects()
        {
            // unknown zone
            this.DnsZone = new DnsZone()
            {
                NodeId = -1,
                GroupId = -1,
                FriendlyName = string.Empty
            };

            // unknown server
            this.DnsServer = new DnsServer()
            {
                NodeId = -1,
                FriendlyName = string.Empty
            };
        }

        private void LoadDnsObjects(int zoneGroupId)
        {
            if (zoneGroupId < 0)
            {
                return;
            }

            SwisConnector.RunSwisOps(new SwisOpCtx[]{ 
                new SwisOpCtx("Retrieve DNS Zone and Server objects", delegate(IpamClientProxy proxy, object param){
                    // get DNS zone data
                    this.DnsZone = proxy.AppProxy.DnsZone.GetByGroupId(zoneGroupId);

                    // retrieve DNS server data
                    this.DnsServer = proxy.AppProxy.DnsServer.Get(this.DnsZone.ParentId, false);

                    return null;
                })
            });
        }

        private void InitControls()
        {
            if (this.DnsZone == null)
            {
                return;
            }

            DnsRecordsGrid.Store.JoinClause = "LEFT JOIN " + DnsZone.EIM_ENTITYNAME + " as zone ON {0}." + DnsZone.EIM_DNSZONEID + " = zone." + DnsZone.EIM_DNSZONEID +
                                    " LEFT JOIN " + DnsServer.EIM_ENTITYNAME + " as server on server." + DnsServer.EIM_NODEID + " =Zone." + DnsZone.EIM_NODEID +
                                    " LEFT JOIN " + GroupNode.EIM_ENTITYNAME + " as G on  G." + GroupNode.EIM_GROUPID + " =server." + GroupNode.EIM_GROUPID;

            if (this.DnsZone.DnsZoneId.HasValue)
            {
                DnsRecordsGrid.Store.WhereClause = string.Format("A.{0} = '{1}'", DnsRecord.EIM_DNSZONEID, this.DnsZone.DnsZoneId);
                hZoneId.Value = this.DnsZone.DnsZoneId.ToString();
                hZoneName.Value = this.DnsZone.FriendlyName;
            }

            
            var isZoneSet = this.DnsZone.GroupId >= 0;
            ZoneNameColumn.isHidden = isZoneSet;
            ZoneNameColumn.isHideable = !isZoneSet;
        }

        private void DisableAllGridTab()
        {
            gridAddTab.disabled = true;
            gridEditTab.disabled = true;
            gridDeleteTab.disabled = true;
        }
    }
}