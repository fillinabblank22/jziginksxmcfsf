using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class ImportUploadIntro : CommonPageServices
    {
        PageImportIntro mgr;
        public ImportUploadIntro() { mgr = new PageImportIntro(this); }

        protected void Page_Load(object sender, EventArgs e)
        {
            // If request contains this parameter, don't redirect automatically
            bool showIntro = Request.QueryString.AllKeys.Length > 0 && Request.QueryString.ToString().Contains("showIntro");

            if (mgr.GetDontShowIntroPageSetting() && !showIntro)
            {
                RedirectToNextPage();
            }

           if(Request.QueryString["GroupID"] != null)
                this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Upload.OnClientClick = "Ext.MessageBox.show({msg:'" + Resources.IPAMWebContent.IPAMWEBCODE_AK1_5 + "',buttons: Ext.MessageBox.OK}); return false;";
            }
        }
        protected void ImportFileUploadPage_Click(object src, EventArgs args)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return; 
            mgr.Save();
            RedirectToNextPage();
        }

        private void RedirectToNextPage()
        {
            string newUrl = "~/Orion/IPAM/Import.upload.aspx";
            var groupID = Request.QueryString.Get("GroupID");
            if (groupID != null)
                newUrl += "?GroupID=" + groupID;
            HttpContext.Current.Response.Redirect(newUrl, false);
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }

    }
}
