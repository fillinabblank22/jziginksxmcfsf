﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class IPv6SubnetEdit : CommonPageServices
{
    #region Constructors

    private PageIPv6SubnetEditor Editor { get; set; }
    public IPv6SubnetEdit()
    {
        this.Editor = new PageIPv6SubnetEditor(this, false);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.SubnetId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            GroupNode subnet = this.Editor.RetrieveSubnetObject();
            InitControls(subnet);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(GroupNode subnet)
    {
        int parentId = subnet.ParentId;
        List<PrefixAggregate> parents =
            PageIPv6EditorHelper.GetPrefixParentsList(parentId);

        if (this.txtName != null)
        {
            this.txtName.Text = subnet.FriendlyName;
        }
        if (this.txtDescription != null)
        {
            this.txtDescription.Text = subnet.Comments;
        }
        if (this.txtVLAN != null)
        {
            this.txtVLAN.Text = subnet.VLAN;
        }
        if (this.txtLocation != null)
        {
            this.txtLocation.Text = subnet.Location;
        }

        if (this.cbRetainUserData != null)
        {
            this.cbRetainUserData.Checked = subnet.RetainUserData ?? false; ;
        }

        if (this.TransientPeriod != null)
        {
            this.TransientPeriod.Value = subnet.TransientPeriod;
        }

        if (this.cbDisableAutoScanning != null)
        {
            if (subnet.DisableAutoScanning != null)
                this.cbDisableAutoScanning.Checked = subnet.DisableAutoScanning.Value;
        }

        if (this.ScanInterval != null)
        {
            if (subnet.ScanInterval != null)
                this.ScanInterval.Value = TimeSpan.FromMinutes(subnet.ScanInterval.Value);
        }

        if (PollingEngine != null)
        {
            PollingEngine.GroupId = subnet.GroupId;
            PollingEngine.ManagingDHCPServersIds = subnet.DhcpServers;
            PollingEngine.PollingEngineId = subnet.EngineId ?? 0;
        }

        //init subnet address
        int initSubnetSize = subnet.CIDR;
        string fnAddressListener = string.Empty;
        if (this.SubnetAddress != null)
        {
            this.SubnetAddress.Init(
                subnet.Address,
                GroupNodeType.IPv6Subnet,
                initSubnetSize,
                parents, parentId, subnet.GroupId,
                Editor.ClusterId);
            fnAddressListener = this.SubnetAddress.Listener;
        }

        // init subnet size control
        if (this.SubnetSize != null)
        {
            this.SubnetSize.Init(
                initSubnetSize, 128,
                initSubnetSize,
                fnAddressListener);
        }

        CustomPropertyRepeater.RetrieveCustomProperties(subnet, new List<int>(){ subnet.GroupId });

        if (this.AccountRolesBox != null)
        {
            this.AccountRolesBox.GroupId = subnet.GroupId;
        }

        ReInitScanSettings();
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        GroupNode subnet = this.Editor.RetrieveSubnetObject();
        if (subnet != null)
        {
            PrefixAggregate parentPrefix =
                PageIPv6EditorHelper.GetParentPrefix(this.Editor.ParentId);

            GroupNode updatedSubnet = UpdateObject(subnet, parentPrefix);
			this.Editor.Store(updatedSubnet);
        }
    }
    private GroupNode UpdateObject(GroupNode subnet, PrefixAggregate parentPrefix)
    {
        subnet.GroupType = GroupNodeType.IPv6Subnet;

        if (this.txtName != null)
        {
            subnet.FriendlyName = this.txtName.Text;
        }
        if (this.txtDescription != null)
        {
            subnet.Comments = this.txtDescription.Text;
        }
        if (this.txtVLAN != null)
        {
            subnet.VLAN = this.txtVLAN.Text;
        }
        if (this.txtLocation != null)
        {
            subnet.Location = this.txtLocation.Text;
        }

        if (this.SubnetAddress != null)
        {
            string ip = this.SubnetAddress.IPv6Value;
            subnet.Address = ip;
        }
        if (this.SubnetSize != null)
        {
            subnet.CIDR = this.SubnetSize.PrefixSizeValue;
            subnet.AddressMask = GroupNode.MaskFromCIDR(subnet.CIDR, System.Net.Sockets.AddressFamily.InterNetworkV6);
        }

        if (this.TransientPeriod != null)
        {
            subnet.TransientPeriod = this.TransientPeriod.Value;
        }

        if (this.cbRetainUserData != null)
        {
            subnet.RetainUserData = this.cbRetainUserData.Checked;
        }

        if (this.cbDisableAutoScanning != null)
            subnet.DisableAutoScanning = this.cbDisableAutoScanning.Checked;

        if (this.ScanInterval != null)
            subnet.ScanInterval = Convert.ToInt32(Math.Round(this.ScanInterval.Value.TotalMinutes));

        int? newEngineId = PollingEngine?.GetSelectedPollingEngine();
        if (newEngineId.HasValue)
        {
            subnet.EngineId = newEngineId.Value;
        }

        CustomPropertyRepeater.GetChanges(subnet);

        return subnet;
    }

    private void ReInitScanSettings()
    {
        bool isIcmpEnabled = false;
        bool isSnmpEnabled = false;
        bool isNeighborEnabled = false;

        // Read ScanSettings
        this.Editor.RetrieveScanSettings(out isIcmpEnabled, out isSnmpEnabled, out isNeighborEnabled);

        // set controls visibility
        if (this.IcmpScanInfo != null)
        {
            this.IcmpScanInfo.Visible = !isIcmpEnabled;
        }
    }

    #endregion // Methods
}
