using System;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Helpers;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportPreview : CommonPageServices
	{
		PageImportPreview mgr;
        public ImportPreview() { mgr = new PageImportPreview(this); }

	    private static int TOP_N_COMMON_ERRORS = 10;

        public GridBox Grid
        {
            get { return grid; }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                if (Request.QueryString["GroupID"] != null)
                    this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));

                if (PreLoader != null)
                {
                	int totalrows;
                    Dictionary<string, int> errorCounts;
                    string errorFilelink;
                    DataTable dt = mgr.GetDataTable(out totalrows, out errorCounts, out errorFilelink);
					if (dt.Rows.Count > 0)
					{
						PreLoader.getDetails = delegate(
							DataColumn column,
							int ColumnOrd,
							out bool Visible,
							out string name,
							out int width,
							out bool sortable,
							out string renderer)
						                       	{
						                       		Visible = true;
						                       		name = null;
						                       		width = 80;
						                       		sortable = false;
						                       		renderer = "$SW.ValidationCellRenderer";
						                       		return true;
						                       	};

						PreLoader.DataSource = dt;
						PreLoader.DataBind();

					    FillErrorLabels(dt.Rows.Count, totalrows, errorCounts);
                        hrErrorsExport.NavigateUrl = errorFilelink;
					}
                    else
                    {
                        string nextUrl = mgr.GetNextLinkUrl();
                        Page.Response.Redirect(nextUrl);
                    }
                }
            }

		    TitleValidation.Text = mgr.GetValidationTitleText();

		    LinkBack.NavigateUrl = mgr.GetBackLinkUrl();

		    Next.NavigateUrl = mgr.GetNextLinkUrl();
		}

        private void FillErrorLabels(int totalErrors, int totalRows, Dictionary<string, int> errorCounts)
        {
            subnetPersmissionsLabel.Text = Resources.IPAMWebContent.IPAMWEBCODE_AK1_6;
            licenseExceedingLabel.Text = String.Format(Resources.IPAMWebContent.IPAMWEBCODE_AK1_7, totalRows - totalErrors, "<a href='#' style='color: blue; text-decoration: underline;'>", "</a>");

            if (totalErrors > 500)
                errorsDetectedLabel.Text = Resources.IPAMWebContent.IPAMWEBCODE_AK1_8;
            else
                errorsDetectedLabel.Text =
                    String.Format(
                        Resources.IPAMWebContent.IPAMWEBCODE_AK1_9, totalErrors);

            var commonErrors = GetCommonErrors(errorCounts);
            var sortedCommonErrors = (from entry in commonErrors orderby entry.Value descending select entry).ToDictionary(pair => pair.Key, pair => pair.Value);
            
            commonErrorsLabel.Text = "<ul style='list-style: disc inside; padding-left: 20px;'>";

            var length = sortedCommonErrors.Count > TOP_N_COMMON_ERRORS ? TOP_N_COMMON_ERRORS : sortedCommonErrors.Count;

            for (var i = 0; i < length; i++ )
                commonErrorsLabel.Text += "<li>" + sortedCommonErrors.ElementAt(i).Key + "</li>";
            
            commonErrorsLabel.Text += "</ul>";

            importPreviewHeaderLabel.Text = String.Format(Resources.IPAMWebContent.IPAMWEBCODE_AK1_10, 
                                                          "<ul style='list-style: disc inside;'><li>", 
                                                          "</li><li>", 
                                                          "</li></ul>");

            if (sortedCommonErrors.Count > 0)
                commonErrorsLabel.Visible = true;

            if (errorCounts.ContainsKey(ImportHelperErrorMsg.ERRORMSG_ROLE_UNABLE_TO_IMPORT_IP) || errorCounts.ContainsKey(ImportHelperErrorMsg.ERRORMSG_ROLE_UNABLE_TO_CREATE_SUBNET))
                subnetPersmissionsLabel.Visible = true;

            if (errorCounts.ContainsKey(ImportHelperErrorMsg.ERRORMSG_NOLICENSE_FOR_IP))
                licenseExceedingLabel.Visible = true;
        }

        private Dictionary<string, int> GetCommonErrors(Dictionary<string, int> errorCounts)
        {
            var commonErrors = new Dictionary<string, int>();

            var valuesToKeys = new Dictionary<string, string>();
            foreach (var entry in ImportHelperErrorMsg.CommonErrorAssociations)
            {
                foreach(var value in entry.Value)
                    valuesToKeys.Add(value, entry.Key);
            }

            foreach (var error in errorCounts)
            {
                string commonError;
                
                if (!valuesToKeys.TryGetValue(error.Key, out commonError)) continue;
                
                if (commonErrors.ContainsKey(commonError))
                    commonErrors[commonError] += error.Value;
                else
                    commonErrors.Add(commonError, error.Value);
            }
            return commonErrors;
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
    }
}
