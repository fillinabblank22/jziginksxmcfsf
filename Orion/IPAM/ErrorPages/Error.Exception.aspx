<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="IPAM Website Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>

  <div class=topmost>
    <h3>IPAM Website Error</h3>
    An unexpected error occurred while generating your web page.
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Error Description</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

  <div>
    <h3>Additional Information</h3>
    <asp:Label id=errMessage runat=server>There is no error text available.</asp:Label>
  </div>

  <div>
    <h3>Helpful Hints</h3>
    <ul>
      <li>The information in this page may provide valuable insight into the root cause</li>
      <li>Clicking "Show", under Stack Trace, may lead to more useful troubleshooting information</li>
      <li>Text and Event Logs may contain additional information, which can be best utilized by SolarWinds proffessionals</li>
    </ul>
  </div>

  <div id=errTypeDiv runat=server>
    <h3>Error Type</h3>
    <asp:Label id=errType runat=server>There is no error type information available.</asp:Label>
  </div>

  <div>
    <h3>Stack Trace</h3>
    <div id=stack class=trace><pre id=errTraceLocal runat=server>This is no stack trace available.</pre></div>
  </div>
  
  <div class=buttonbar>
    <a href="#" onclick="history.back(); return false;"><img runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
  </div>
</div>

</asp:Content>
