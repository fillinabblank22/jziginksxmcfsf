<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Title="Invaild Credential" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Invaild Credential</h3>
    You have not provided a windows credential that authenticates with IIS on this remote server. Please try again.
  </div>

<div>
  <h3>Helpful Hints</h3>
  <ul>
    <li>Verify that your caps lock is not on</li>
    <li>Click Login, then retype your password</li>
    <li>Have your network administrator verify that you have the following Windows right on the IPAM Website Computer:<br />
        <i>Access this computer from the network</i></li>
  </ul>
</div>

<div class=buttonbar>
  <a href="~/WindowsLogin.aspx" runat=server><img runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.login.gif" /></a>
</div>

</div>
</asp:Content>
