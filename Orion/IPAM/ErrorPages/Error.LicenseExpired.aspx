<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="IPAM License Expired" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Your IP Address Manager License is currently unavailable</h3>
    The IP Address Manager license has either expired or is unavailable for technical reasons.
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Information</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

  <div>
    <h3>Helpful Hints</h3>
    <ul>
      <li>To resolve, click Start &gt; All Programs &gt; SolarWinds Orion &gt; Orion IP Address Manager &gt; Orion IP Address Manager Licensing</li>
    </ul>
  </div>

   <div class=buttonbar>
    <a href="#" onclick="history.back(); return false;"><img id="Img1" runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
    <a href="#" onclick="window.location.reload(true); return false;"><img id="Img2" runat=server alt=refresh src="~/Orion/images/button.refresh.primary.gif" /></a>
  </div>
</div>

</asp:Content>