<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Title="Unsupported Web Browser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Unsupported Web Browser</h3>
    Your browser's capabilities are not expressly supported by the IPAM Website.
  </div>

<div>
  <h3>Supported Web Browsers</h3>
    <ul>
      <li>Internet Explorer 6+</li>
      <li>FireFox 1.5+ (PC)</li>
      <li>Safari 3+ (PC)</li>
      <li>Opera 9+ (PC)</li>
    </ul>
</div>

<div>
  <h3>Helpful Hints</h3>
  <ul>
    <li>Download and install one of the supported web browsers</li>
    <li>Continue and login anyway, knowing that you may experience defects</li>
  </ul>
</div>

<div class=buttonbar>
  <a href="~/WindowsLogin.aspx" runat=server><img runat=server alt=Login src="~/Orion/IPAM/res/images/sw/button.Login.gif" /></a>
</div>
  
</div>
</asp:Content>
