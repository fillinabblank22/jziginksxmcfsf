<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Title="No Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>No Access</h3>
    You have entered a valid windows credential, but your account has expired.
  </div>

<div>
  <h3>Helpful Hints</h3>
  <ul>
    <li>Ask Administrator to enable your account</li>
    <li>Login within another account</li>
  </ul>
</div>

<div class=buttonbar>
  <a href="~/default.aspx" runat=server><img id="Img1" runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
</div>
  
</div>
</asp:Content>
