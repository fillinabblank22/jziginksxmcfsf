<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Title="No Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>No Access</h3>
    You have entered a valid windows credential, but this account is not registered for IPAM access.
  </div>

<div>
  <h3>Helpful Hints</h3>
  <ul>
    <li>Ask Administrator to create an account for you</li>
    <li>Next time you login, try using a different windows account</li>
  </ul>
</div>

<div class=buttonbar>
  <a href="~/default.aspx" runat=server><img runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
</div>
  
</div>
</asp:Content>
