<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="Database Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Database Error</h3>
    The IPAM Information Service utilizes a SQL Server to provide both authentication and application data. A SQL specific error has occurred.
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Error Description</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

  <div>
    <h3>Helpful Hints</h3>
    <ul>
      <li>Start a remote desktop connection to the IPAM Website Computer</li>
      <li>From the Start Menu, run the IPAM Configuration Wizard</li>
      <li>Allow the Wizard to reconfigure your database<br />
          <b>Note:</b> ensure that you select the existing database used by IPAM</li>
    </ul>
  </div>

  <div>
    <h3>Additional Information</h3>
    <asp:Label id=errMessage runat=server>There is no error text available.</asp:Label>
  </div>

  <div id=errTypeDiv runat=server>
    <h3>Error Type</h3>
    <asp:Label id=errType runat=server>There is no error type information available.</asp:Label>
  </div>

  <div>
    <h3>Stack Trace</h3>
    <div id=stack class=trace><pre id=errTraceLocal runat=server>This is no stack trace available.</pre></div>
  </div>
</div>

</asp:Content>