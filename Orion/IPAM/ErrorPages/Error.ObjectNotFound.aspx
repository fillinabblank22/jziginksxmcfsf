<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="Object Not Found" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Object Not Found</h3>
    This page you tried to load requires a specific object that no longer exists.
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Error Description</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

  <div>
    <h3>Helpful Hints</h3>
    <ul>
      <li>Cut and Paste errors can cause this error to become visible</li>
      <li>Bookmarks to Objects that have previously been deleted</li>
      <li>If the previous page was a bookmark, try the IPAM Website login page.</li>
    </ul>
  </div>

  <div>
    <h3>Additional Information</h3>
    <asp:Label id=errMessage runat=server>There is no error text available.</asp:Label>
  </div>

  <div id=errTypeDiv runat=server>
    <h3>Error Type</h3>
    <asp:Label id=errType runat=server>There is no error type information available.</asp:Label>
  </div>

  <div>
    <h3>Stack Trace</h3>
    <div id=stack class=trace><pre id=errTraceLocal runat=server>This is no stack trace available.</pre></div>
  </div>

  <div class=buttonbar>
    <a href="#" onclick="history.back(); return false;"><img id="Img1" runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
  </div>
</div>

</asp:Content>