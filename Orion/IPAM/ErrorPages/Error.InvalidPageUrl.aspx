<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="Invalid Page URL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Invalid Page URL</h3>
    This page expects a parameter in the URL. Frequently, this is "ObjectID" which must be present and references an existing application object.
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Error Description</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

  <div>
    <h3>Helpful Hints</h3>
    <ul>
      <li>Cut and Paste errors can cause this error to become visible</li>
      <li>Click back to return to the previous page.</li>
      <li>If the previous page was a bookmark, try the IPAM Website login page.</li>
    </ul>
  </div>

  <div>
    <h3>Additional Information</h3>
    <asp:Label id=errMessage runat=server>There is no error text available.</asp:Label>
  </div>

  <div id=errTypeDiv runat=server>
    <h3>Error Type</h3>
    <asp:Label id=errType runat=server>There is no error type information available.</asp:Label>
  </div>

  <div>
    <h3>Stack Trace</h3>
    <div id=stack class=trace><pre id=errTraceLocal runat=server>This is no stack trace available.</pre></div>
  </div>

  <div class=buttonbar>
    <a href="#" onclick="history.back(); return false;"><img id="Img1" runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
  </div>
</div>

</asp:Content>