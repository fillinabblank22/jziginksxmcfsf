<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="Access Denied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
<div class=topmost>
  <h3>Access Denied</h3>
  Your account has been found to have insufficient permissions to carry out the desired action.
</div>

<div>
  <h3>Helpful Hints</h3>
  <ul>
    <li>Click Back</li>
    <li>Ask Administrator for more permissions</li>
    <li>Login within another account</li>
  </ul>
</div>

<div class=buttonbar>
  <a href="#" onclick="history.back(); return false;"><img runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
  <a href="/Orion/logout.aspx"><img runat=server alt=login src="~/Orion/IPAM/res/images/sw/Button.Login.gif" /></a>
  
</div>
</div>
</asp:Content>
