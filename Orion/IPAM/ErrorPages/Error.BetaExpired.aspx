<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/ErrorMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.ExceptionPage" Title="Beta expired" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
<div id=errbox>
  <div class=topmost>
    <h3>Beta license has expired</h3>
    
  </div>

  <div id=errDescDiv runat=server visible=false>
    <h3 id=errTitle runat=server>Information</h3>
    <asp:Label id=errDesc runat=server>There is no description available.</asp:Label>
  </div>

   <div class=buttonbar>
    <a href="#" onclick="history.back(); return false;"><img id="Img1" runat=server alt=back src="~/Orion/IPAM/res/images/sw/button.back.gif" /></a>
  </div>
</div>

</asp:Content>