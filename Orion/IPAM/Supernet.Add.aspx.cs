using System;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;

namespace SolarWinds.IPAM.WebSite
{
    public partial class SupernetAdd : CommonPageServices
    {
        private PageSubnetEditor editor;

        public SupernetAdd()
        {
            editor = new PageSubnetEditor(this, true, GroupNodeType.Supernet);
            editor.SaveCustomProperties = SaveCustomProperties;
            editor.GetCustomProperties = GetCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties group)
        {
            CustomPropertyRepeater.GetChanges(group);
        }

        public void GetCustomProperties(ICustomProperties group, IList<int> ids)
        {
            CustomPropertyRepeater.RetrieveCustomProperties(group, ids);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            editor.Save(true);
        }
    }
}
