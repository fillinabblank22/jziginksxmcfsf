﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
    Inherits="SolarWinds.IPAM.WebSite.DnsZoneRecords" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_296 %>"
    CodeFile="DNS.Zone.Records.aspx.cs" ValidateRequest="false" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
 
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,ext-quicktips,sw-helpserver,sw-dhcp-manage.js,sw-ux-wait.js,sw-dns-constants.js,sw-dns-manage.js" Orientation="jsPostInit" runat="server">
            $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
            $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
    </IPAMmaster:JsBlock>
     <div id="formbox">
        <div id="ChromeFormHeader" runat="server" class="sw-form-header"> <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_297 %></div>

        <IPAMlayout:ViewPort ID="viewport" runat="server">
            <IPAMlayout:BorderLayout ID="mainlayout" runat="server" borderVisible="false">
                <Center>
                    <IPAMlayout:GridBox runat="server" ID="ZoneRecords" UseLoadMask="True"
                        autoHeight="false" emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_298 %>" borderVisible="true"
                        deferEmptyText="false" RowSelection="true"  listeners="{ statesave: $SW.IPAM.DnsRecordsStateSave }">
                        <PageBar pageSize="100" displayInfo="true" />
                        <Columns>                                       
                            <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_94 %>" width="200" isSortable="true" dataIndex="Name"  runat="server" />
                            <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="200" isSortable="true" dataIndex="Type" runat="server" renderer="$SW.IPAM.DnsRecordTypeRenderer()" /> 
                            <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_371 %>" width="250" isSortable="true" dataIndex="Data" runat="server" renderer="$SW.IPAM.DnsRecordDataRenderer()" />                                                      
                        </Columns>
                        <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx"
                            proxyEntity="IPAM.DnsRecordReport" AmbientSort="DnsRecordId"
                            listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                            <Fields>                                 
                                <IPAMlayout:GridStoreField dataIndex="DnsRecordId" isKey="True" runat="server" />
                                <IPAMlayout:GridStoreField dataIndex="DnsZoneId" runat="server" />
                                <IPAMlayout:GridStoreField dataIndex="Name" runat="server"/>
                                <IPAMlayout:GridStoreField dataIndex="Type" runat="server" />
                                <IPAMlayout:GridStoreField dataIndex="Data" runat="server" />
                                <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
                            </Fields>
                        </Store>
                    </IPAMlayout:GridBox> 
                </Center>
            </IPAMlayout:BorderLayout>
       </IPAMlayout:ViewPort>

    </div>
     
    <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">         
        <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/api2/ipam/ui/zones" LocalizedText="Cancel" DisplayType="Secondary" />
    </div>

</asp:Content>
