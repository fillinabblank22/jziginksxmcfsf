﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddSubnetIPAddresses.aspx.cs" Inherits="AddSubnetIPAddresses"
    MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" Title="<%$ Resources:IPAMWebContent,IPAMWEBDATA_DF1_19 %>"%>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">
    <IPAMmaster:CssBlock Requires="orionminreqs.css,sw-base.css,ext-all.css,sw-ext-tabs.css,ext-theme-gray.css,~/Orion/styles/MainLayout.css" runat="server">

    </IPAMmaster:CssBlock>
     <script language="javascript" type="text/javascript">
         var radioImportID = '<%=radioImport.ClientID%>';
         var radioAutoID = '<%=radioAuto.ClientID%>';
         var radioManualID = '<%=radioManual.ClientID%>';
        

         function selectOption(id) {
             var el = document.getElementById(id);
             el.checked = true;
         }
    </script>
    <style type="text/css">

.Inline {
display: inline;
line-height: 0%;
}
.GroupBoxAccounts {
   background-color: #FFFFFF;
    border: 1px solid #DBDCD7;

    width:60%;
}
.Align {

    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    margin-left: 10px;
    padding: 0;
    
}
</style>
   <table>
	    <tr><td></td></tr>
		<tr>
			<td><h1>
				<%=Page.Title%></h1>
			</td>
		</tr>
       <tr >
            <td>
               <div class ="Align"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_22 %></div> 
            </td>
        </tr>
	</table>
    <br/><div id ="groupbox" class ="GroupBoxAccounts Align" >
     <div id="InerSelectTypeDiv" style="padding: 10px 10px 5px 10px;">
                <h2 style="padding-left: 10px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_27 %></h2>
                
                <table width="98%" cellpadding="0" cellspacing="0" style="margin-left:10px; margin-right:10px;" >
                     <tr style="background-color:#E4F1F8; height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%"><input type="radio" id="radioAuto" name="radioButtons" value="auto" style="margin-top:10px; cursor:pointer;" runat="server"  /></td>
                        <td width="96%" style="padding-top:10px; font-weight:bolder"><div style="cursor:pointer;" onclick="selectOption(radioAutoID);"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_23%></div></td>
                    </tr>
                    <tr  style="background-color:#E5F1F7; height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%">&nbsp;</td>                        
                        <td width="96%" style="font-weight:lighter; font-size:small"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_24 %></td>
                    </tr> 
                      <tr style="height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%"><input type="radio" id="radioImport" name="radioButtons" value="import" style="margin-top:10px; cursor:pointer;" runat="server" /></td>                        
                        <td width="96%" style="padding-top:10px; font-weight:bolder;"><div style="cursor:pointer;" onclick="selectOption(radioImportID);"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_21 %></div></td>
                    </tr>
                    <tr style="height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%">&nbsp;</td>                        
                        <td width="96%" style="font-weight:lighter; font-size:small"><%= Resources.IPAMWebContent.IPAMWEBDATA_VN0_19 %></td>
                    </tr> 
                                
                  <tr style="background-color:#E4F1F8; height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%"><input type="radio" id="radioManual" name="radioButtons" value="manual" style="margin-top:10px; cursor:pointer;" runat="server"/></td>
                        <td width="96%" style="padding-top:10px; font-weight:bolder"><div style="cursor:pointer;" onclick="selectOption(radioManualID);"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_25 %></div></td>
                    </tr>
                    <tr style="background-color:#E5F1F7; height:20px;">
                        <td width="1%">&nbsp;</td>
                        <td width="1%">&nbsp;</td>                        
                        <td width="96%" style="font-weight:lighter; font-size:small"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_26 %></td>
                    </tr>
                        
                   

                   <tr style="height:20px; padding-top:20px;">
                        <td class="leftLabelColumn">
                            &nbsp;
                        </td>
                         <td>&nbsp;</td>
                        <td style="text-align:right">
                            <div class="sw-btn-bar-wizard">
                                <orion:LocalizableButton id="btnNext" runat="server" DisplayType="Primary" LocalizedText="Next"  OnClick="btnNext_Click"/>
                                <orion:LocalizableButton id="btnCancel" runat="server" DisplayType="Secondary" LocalizedText="Cancel" OnClick="btnCancel_Click" CausesValidation="false"/>
                            </div>
                        </td>
                    </tr>
                </table>             
         </div>
        </div>
</asp:Content>


