﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNewIPAM.ascx.cs" Inherits="Orion_IPAM_Resources_WhatsNew_WhatsNewIPAM" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
           <script type="text/javascript">
               
               var dialog;

               function CloseRegisterationDialog() {
                   $("#SalesRegisterForm").dialog("close");
               }

               function intializeDialog(message) {

                   dialog = $("#alertDialog").dialog({
                       autoOpen: false,
                       position: 'center',
                       draggable: true,
                       width: 500,
                       height: 120,
                       modal: true,
                       overlay: { "background-color": "black", opacity: 0.4 },
                       resizable: false,
                       close: function() {
                           window.location.reload();
                       },
                       open: function () {
                           //Workaround JQuery dialog issue - according to the jQuery UI ticket this will be fixed in version 1.9 by using the focusSelector option
                           $(this).find('input').attr("tabindex", "1");

                           var page = "http://www.solarwinds.com/IPAT-eval-activation";

                           var ifr = $('<iframe/>', {
                               id: 'MainPopupIframe',
                               src: page,
                               style: 'display:none;border:0px;width:100%;height:100%',
                               load: function () {
                                   $('#LoadingProgress').html('');
                                   $('#RegisterationContent').show();
                                   $("#SalesRegisterForm").dialog("option", "height", 680);
                                   $("#SalesRegisterForm").dialog("option", "width", 950);
                                   $("#SalesRegisterForm").dialog("option", "draggable", true);
                                   $("#SalesRegisterForm").dialog("option", "position", 'center');
                                   $("#SalesRegisterForm").dialog("option", "dialogClass", '');
                                   $(this).show();
                               }
                           });

                           var $dialog = $('<div id=\'SalesRegisterForm\'><div id=\'LoadingProgress\' /><div id=\'RegisterationContent\' style=\"width:100%;height:100%\" /></div>')
                                    .dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 200,
                                    width: 600,
                                    resizable: false,
                                    draggable: false,
                                    position: 'center',
                                    dialogClass: "no-close",
                                    title: "<%= Resources.IPAMWebContent.IPAMWEBDATA_VR_9 %>",
                                    open: function () {
                                        $('#LoadingProgress').empty().html('<img style="padding-right: 7px;" src="/Orion/images/animated_loading_sm3_whbg.gif" />Loading...');
                                        $('#RegisterationContent').html('').hide();
                                        $('#RegisterationContent').html(ifr);
                                    }
                                });

                           $('#SalesRegisterForm').bind('dialogclose', function (event) {
                               $('#SalesRegisterForm').html('');
                           });

                            $dialog.dialog('open');
                       },
                       title: "<%= Resources.IPAMWebContent.IPAMWEBCODE_VR_19 %>"
                   });

                   dialog.css('overflow', 'hidden');

                   $('#alertDialog').bind('dialogclose', function (event) {
                       //Clear Value
                   });
                   dialog.off("dialogclose");

                   $("#<%= Ok.ClientID %>").unbind("click");

                   $("#<%= Ok.ClientID %>").click(function () {
                       $("#alertDialog").dialog("close");
                    });

                   $('#alertText').text(message);
                   dialog.dialog("open");
               }

               function updatestatus(flagType,result) {
                   //flagType == 1: Switch to Eval
                   //flagType == 2: Switch to FreeTool
                   var isSuccess = false, message = null;
                   try {
                       var res = eval('(' + result + ')');
                       if (res && res.success == true) {
                           isSuccess = true;
                           message = res.message;
                       }
                       if (res && res.success == false) {
                           message = res.message;
                       }
                   }
                   catch (e) { }

                   if (!isSuccess) {
                       if (message)
                           
                           alert(message);
                       return;
                   }

                   if (flagType && flagType == '1') {
                       intializeDialog(message);
                   }
                   else if (flagType && flagType == '2') {
                       if (message) alert(message);
                       window.location.reload();
                   }
               }

               function SwitchLicense(switchType, flagType) {
                   $.ajax({
                       type: "POST",
                       url: '/Orion/IPAM/ExtCmdProvider.ashx',
                       async: false,
                       data: { entity: 'IPAM.SwitchLicense', verb: switchType },
                       success: function (result, request) {
                           updatestatus(flagType, result);
                       },
                       failure: function (result, request) {
                           updatestatus(flagType, result);
                       }
                   });
               }
        </script>

        <style type="text/css">
            .whatsNewcls h1 {
                font-size: 11px;
                font-weight: bold;
                font-family: Verdana;
                color: Gray;
                padding: 5px;
            }

           .whatsNewcls  a {
                color: #336699;
            }

           .whatsNewcls  td {
                border-bottom: 0px!important;
                }

           .whatsNewcls  td a img {
                vertical-align: baseline;
               }

           .whatsNewcls ul {
                padding-left: 22px;
                margin-bottom: -3px;
                margin-top: 3px;
                list-style: square;
            }

            .li-circle {
                list-style-type: circle;
            }

            .ActivationText {
                position: relative;
                padding-right:10px;
            }

            .no-close .ui-dialog-titlebar-close {
                display: none;
            }

           .whatsNewcls tr { line-height: 14px; }

           .whatsNewcls div {height:40px;}

        </style>
         <div id="alertDialog" style="display:none;">
	        <div id="alertText" style=" margin-left:3px;margin-top: 5px;"></div>
	        <div>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButtonLink runat="server" Text="Ok" DisplayType="Primary" ID="Ok" />
		            </div>
	        </div>
        </div>
        <div id="whatsnew" runat="server" >
            <table class="whatsNewcls">
                <colgroup>
                    <col width="auto" />
                    <col width="100%" />
                </colgroup>
                <tr>
                    <td valign="top">
                        <img id="WhatNewIcon" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("IPAM", "res/images/WhatsNew/icon_new.gif") %>" />
                    </td>
                    <td>
                        <h1><%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_21%></h1>
                        <ul>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_22%>
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_23%>
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_24%>
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_25%>
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_DM1_26%>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                <td colspan="2" align="right" style="height: 50px" >
                        <orion:LocalizableButtonLink runat="server" Target="_blank" NavigateUrl="http://www.solarwinds.com/documentation/kbloader.aspx?kb=3633&lang=en" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VR_8 %>" DisplayType="Primary" />
                            &nbsp;
                        <orion:LocalizableButton runat="server" ID="btnRemoveResource" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_431 %>" DisplayType="Secondary" />
                </td>
                </tr>
            </table>
        </div>
        <div id="text" runat="server">
            </div>
            
        <div id="IPAT" runat="server">
         <table class="whatsNewcls">
            <colgroup>
                <col width="auto" />
                <col width="100%" />
            </colgroup>
                    
            <tr >
                <td valign="top">
                    <img id="WhatNewIpatIcon" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("IPAM", "res/images/WhatsNew/icon_new.gif") %>" />
                </td>
                <td>
                       <h1><%= Resources.IPAMWebContent.IPAMWEBCODE_VR_1%></h1>
                        <ul>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_2%><br />
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_3%><br />
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_4%><br />
                            </li>
                             <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_5%><br />
                            </li>
                             <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_6%><br />
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_7%><br />
                            </li>
                        </ul>
                      </tr>
                    <tr>
                    <td>                       
                    </td>
                        <td>
                        <h1><%= Resources.IPAMWebContent.IPAMWEBCODE_VR_8%></h1>
                        <ul>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_9%><br />
                            </li>
                            <li>
                                <%= Resources.IPAMWebContent.IPAMWEBCODE_VR_10%><br />
                            </li>
                        </ul>
                        </td>
                    </tr>
                    <tr>
                    <td>
                    </td>
                        <td>
                        <h1><%= Resources.IPAMWebContent.IPAMWEBCODE_VR_11%></h1>                       
                        </td>
                    </tr>
                   <tr>
                   <td>
                 <tr>
            </table>
        </div>
    </Content>
</orion:resourceWrapper>
