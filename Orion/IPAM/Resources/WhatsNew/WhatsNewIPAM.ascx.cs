﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.Logging;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_IPAM_Resources_WhatsNew_WhatsNewIPAM : BaseResourceControl
{
    private const string IPAM_VERSION = "";
    private static readonly Log log = new Log();
 	private string resourceTitle = Resources.IPAMWebContent.IPAMWEBCODE_AK1_17;
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    public override string HelpLinkFragment
    {
        get
        {
            return string.Empty;
        }
    }

    protected override string DefaultTitle
    {
        get { return resourceTitle; }
    }

    public sealed override string DisplayTitle
    {
        get { return String.Format(this.Title, IPAM_VERSION); }
    }

    protected void OnRemoveResourceClick(object sender, EventArgs e)
    {
        ResourceManager.DeleteById(this.Resource.ID);

        // reorder resources after delete
        var targetColumn = Resource.Column;
        var resources = ResourceManager.GetResourcesForView(Resource.View.ViewID);
        var pos = 1;

        foreach (var resource in resources)
        {
            if (resource.Column == targetColumn)
            {
                ResourceManager.SetPositionById(pos++, resource.ID);
            }
        }

        // after delete resource is still in Page.Controls, because it's a postback
        // resource. so we have to reload page
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Wrapper.ShowEditButton = false;
        IPAT.Visible = false;
        whatsnew.Visible = true;
        btnRemoveResource.Click += OnRemoveResourceClick;
        btnRemoveResource.Text = Resources.IPAMWebContent.IPAMWEBDATA_AK1_431;
        var licenseInfo = GetIpamLicenseInfo();
        log.Debug("IPAM What's New Resource-License Information retrieved");
        string licenseTypeVal = string.Empty;

        if (licenseInfo != null && licenseInfo.ContainsKey(InternalStatusKey.LicensingMode.ToString()))
        {
            licenseTypeVal = licenseInfo[InternalStatusKey.LicensingMode.ToString()];

            if (string.IsNullOrEmpty(licenseTypeVal))
            {
                using (
                    ICoreBusinessLayer proxy =
                        CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(ex => _log.Error(ex)))
                {
                    List<ModuleLicenseInfo> allModules = proxy
                        .GetModuleLicenseInformation()
                        .Where(m => string.Equals("IPAM", m.ModuleName, StringComparison.OrdinalIgnoreCase))
                        .ToList();
                    foreach (ModuleLicenseInfo license in allModules)
                    {
                        var type = license.GetType();
                        var properties = type.GetProperties();

                        foreach (var property in properties)
                        {
                            _log.DebugFormat("Name: " + property.Name + ", Value: " + property.GetValue(license, null));
                        }
                    }

                    var evalModules = allModules.Where(x => x.IsExpired == true).ToList();

                    if (evalModules != null && evalModules.Any())
                    {
                        log.Debug("IPAM What's New Resource-Evaluation expired");
                        btnRemoveResource.Click -= OnRemoveResourceClick;
                        btnRemoveResource.Text = Resources.IPAMWebContent.IPAMWEBDATA_VR_10;
                        btnRemoveResource.OnClientClick =
                            string.Format("javascript:SwitchLicense('{0}','2'); return false;",
                                "FreeTool");
                    }
                }
            }
            else
            {
                try
                {
                    log.DebugFormat("IPAM What's New Resource-licenceType:{0}", licenseTypeVal);

                    if ("FreeTool".Equals(licenseTypeVal.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                            whatsnew.Visible = false;
                            IPAT.Visible = true;
                            this.Resource.Title = Resources.IPAMWebContent.IPAMWEBCODE_VR_19;
                            btnRemoveResource.Click -= OnRemoveResourceClick;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("IPAM What's New Resource-Exception During License Processing", ex);
                    return;
                }
            }
        }
        else
        {
            return;
        }
    }

    private Dictionary<string, string> GetIpamLicenseInfo()
    {
        try
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                proxy.AppProxy.InternalStatus.EnsureCache(true);

                return proxy.AppProxy.InternalStatus.GetAll();
            }
        }
        catch (Exception ex)
        {
            log.Error("IPAM What's New Resource-Exception",ex);
            return null;
        }
    }
}
