﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Web.Common.Controls;
using System.Text;

public partial class Orion_IPAM_Resources_EditIPAddressHistory : AbstractResourceEditPage
{
    #region Constants

    private const string ParamMaxItems = "MaxItems";
    private const string ParamAutoHide = "AutoHide";

    #endregion // Constants

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((this.Resource != null) && (this.IsPostBack == false))
        {
            // resource title
            this.TitleEditControl.ResourceTitle = Resource.Title;
            this.TitleEditControl.ResourceSubTitle = Resource.SubTitle;
            this.ResourceName.Text = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, Resource.Name);

            // max items
            int maxItems = TryParse<int>(
                this.Resource.Properties[ParamMaxItems], ex => 5);
            this.MaxItems.Text = maxItems.ToString();

            // auto hide
            bool autoHide = TryParse<bool>(
                this.Resource.Properties[ParamAutoHide], ex => false);
            this.AutoHide.Checked = autoHide;
        }
    }

    protected override void OnSubmitClick(object sender, EventArgs args)
    {
        if (this.TitleEditControl != null)
        {
            string title = this.TitleEditControl.ResourceTitle;
            string subtitle = this.TitleEditControl.ResourceSubTitle;
            if ((this.Resource.Title != title) || (this.Resource.SubTitle != subtitle))
            {
                this.Resource.Title = title;
                this.Resource.SubTitle = subtitle;
                SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
            }
        }

        if (this.MaxItems != null)
        {
            int maxItems = TryParse<int>(this.MaxItems.Text, ex => 5);
            this.Resource.Properties[ParamMaxItems] = maxItems.ToString();
        }

        if (this.AutoHide != null)
        {
            string autoHide = this.AutoHide.Checked.ToString();
            this.Resource.Properties[ParamAutoHide] = autoHide;
        }

        StringBuilder url = new StringBuilder();
        url.AppendFormat("/Orion/View.aspx?ViewID={0}", this.Resource.View.ViewID);
        if (string.IsNullOrEmpty(this.NetObjectId) == false)
        {
            url.AppendFormat("&{0}={1}", NetObjectIdQueryParam, this.NetObjectId);
        }
        Response.Redirect(url.ToString());
    }

    #endregion // Events
}
