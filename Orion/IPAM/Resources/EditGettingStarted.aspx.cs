﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;

public partial class Orion_IPAM_Resources_EditGettingStarted : System.Web.UI.Page
{
    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format("Edit \"{0}\"", this.Resource.Title);
        }
        
        // properties initialization add here ..
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        // change of resource properties add here ..

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        Response.Redirect(url);
    }
}
