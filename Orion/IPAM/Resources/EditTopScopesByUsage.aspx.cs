using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_IPAM_Resources_EditTopScopesByUsage : System.Web.UI.Page
{
	private ResourceInfo _resource;
	protected ResourceInfo Resource
	{
		get { return _resource; }
	}
	
	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, this.Resource.Title);
			this.resourceTitleEditor.ResourceTitle = this.Resource.Title;
			

			string howMany = this.Resource.Properties["HowManyScopesByIpSpaceUsage"];
			if (string.IsNullOrEmpty(howMany))
				howMany = "10";

			HowMany.Text = howMany;
			SQLFilter.FilterTextBox.Text =  WebSecurityHelper.SanitizeHtml(this.Resource.Properties["Filter"]);
		}
	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
		{
			Resource.Title = resourceTitleEditor.ResourceTitle;
			SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
		}

        Resource.Properties["HowManyScopesByIpSpaceUsage"] = HowMany.Text;
		Resource.Properties["Filter"] = SqlFilterChecker.CleanFilter(SQLFilter.FilterTextBox.Text);

		string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
		Response.Redirect(url);
	}
}
