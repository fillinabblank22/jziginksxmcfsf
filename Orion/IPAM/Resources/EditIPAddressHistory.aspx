﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
         CodeFile="EditIPAddressHistory.aspx.cs" Inherits="Orion_IPAM_Resources_EditIPAddressHistory" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ContentPlaceHolderID="MainContent" Runat="Server">
<style type="text/css">
    .editResourceTitle span { font-size: 9pt; color: gray; margin: 4px; }
</style>

  <div class="res-item">
    <h1><asp:Label ID="ResourceName" runat="server" Text="-" ForeColor="Black" /></h1>
    <asp:ValidationSummary runat="server" />
    
    <div class="editResourceTitle">
    <orion:EditResourceTitle runat="server" ID="TitleEditControl"
        ShowSubTitle="true" ShowSubTitleHintMessage="true"
        SubTitleHintMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_671 %>" />
        </div>

    <p>
        <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_672 %></b>
        <br />
        <asp:TextBox ID="MaxItems" Columns="30" runat="server" />
        <asp:CompareValidator runat="server" CssClass="validator" Display="Dynamic"
            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_673 %>" 
            Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1"
            ControlToValidate="MaxItems">
            <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
        </asp:CompareValidator>
        <asp:RequiredFieldValidator runat="server" CssClass="validator" Display="Dynamic"
            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_674 %>"
            ControlToValidate="MaxItems">
            <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
        </asp:RequiredFieldValidator>
    </p>

    <p>
        <asp:CheckBox ID="AutoHide" runat="server" />&nbsp;<b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_675 %></b>
        <br />
        <span class="hint" style="font-size: 9pt; color: gray; margin: 4px; ">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_676 %>
        </span>
    </p>

    <br />
      <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" OnClick="OnSubmitClick" LocalizedText="Submit" DisplayType="Primary"/>
  </div>
</asp:Content>

