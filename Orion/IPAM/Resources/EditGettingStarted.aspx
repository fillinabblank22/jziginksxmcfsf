﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/ResourceEdit.master" CodeFile="EditGettingStarted.aspx.cs" Inherits="Orion_IPAM_Resources_EditGettingStarted" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format("Edit <font color=\"Blue\">\"{0}\"</font>", Resource.Title) %></h1>
    <div style="padding-left: 16px;">
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
    <br />
    <b>Maximum Number of Posts to Display</b>
    <br/>
        <asp:TextBox ID="PostsCount" runat="server" />

        <asp:CompareValidator ID="CompareValidator1" runat="server" 
          ErrorMessage="Number of Post must be greater than zero" 
          Operator="GreaterThanEqual" 
          Type="Integer" 
          ValueToCompare="1" 
          ControlToValidate="PostsCount">*</asp:CompareValidator>
      
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
          ControlToValidate="PostsCount" 
          ErrorMessage="Number of Post must not be blank.">*</asp:RequiredFieldValidator>

    <br />
    <br />
    <asp:ImageButton runat="server" ID="btnSubmit" ImageUrl="~/Orion/images/Button.Submit.gif" OnClick="SubmitClick" />                    
    
    </div>
</asp:Content>
