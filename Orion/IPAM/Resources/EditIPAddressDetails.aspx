﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
         CodeFile="EditIPAddressDetails.aspx.cs" Inherits="Orion_IPAM_Resources_EditIPAddressDetails" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><asp:Label ID="ResourceName" runat="server" Text="-" ForeColor="Black" /></h1>
 
 <asp:ValidationSummary runat="server" />
 
 <orion:EditResourceTitle runat="server" ID="TitleEditControl" ShowSubTitle="true" />
 
 <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" OnClick="OnSubmitClick"  LocalizedText="Submit" DisplayType="Primary"/>
</asp:Content>

