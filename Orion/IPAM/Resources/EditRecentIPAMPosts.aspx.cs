using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;

public partial class Orion_IPAM_Resources_EditRecentIPAMPosts : System.Web.UI.Page
{
	private ResourceInfo _resource;
	protected ResourceInfo Resource
	{
		get { return _resource; }
	}

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, this.Resource.Title);
		}

		if (!string.IsNullOrEmpty(Resource.Properties["NumberOfPosts"]))
			PostsCount.Text = Resource.Properties["NumberOfPosts"];
		else
			PostsCount.Text = "15";


	}

	protected void SubmitClick(object sender, EventArgs e)
	{
		Resource.Properties["NumberOfPosts"] = PostsCount.Text;

		string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
		Response.Redirect(url);
	}
}
