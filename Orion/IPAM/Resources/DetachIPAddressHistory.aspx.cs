﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Common.Resources;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_IPAM_Resources_DetachIPAddressHistory : CommonPageServices
{
    private enum HistoryType
    {
        unknown,
        IP,
        MAC,
        DNS
    }

    #region Constants

    private const int MAX_HISTORY_COUNT = 1000;
    private const string QUERY_PARAM_RESOURCEID = "ResourceID";
    private const string QUERY_PARAM_NETOBJECT = "NetObject";
    private const string QUERY_PARAM_MAC = "MAC";
    private const string QUERY_PARAM_DNS = "DNS";
    private const string QUERY_INVALID_VALUE = "INVALID_VALUE";
    private const string QUERY_PARAM_TIMEPERIOD = "TimePeriod";

    #endregion // Constants

    #region Properties

    protected ResourceInfo Resource { get; set; }

    protected IpamIPNode IpamIPAddress { get; set; }
    protected string Mac { get; set; }
    protected string Dns { get; set; }

    protected TimePeriodValue TimePeriod { get; set; }

    public string SearchQuery
    {
        get
        {
            if (this.hfSearchQuery == null)
            {
                return string.Empty;
            }
            return this.hfSearchQuery.Value;
        }
        set
        {
            if (this.hfSearchQuery != null)
            {
                this.hfSearchQuery.Value = value;
            }
        }
    }

    protected bool HistoryCountOverflow { get; set; }

    #endregion // Properties

    #region Events

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        LoadFromRequest();

        // [oh] init access check contol with subnetId for custom user delegation.
        if (AccessCheck != null &&
            this.IpamIPAddress != null &&
            this.IpamIPAddress.IPAddress != null &&
            this.IpamIPAddress.IPAddress.IpNodeId != IpamIPNode.PreviewNetObjectId)
            AccessCheck.GroupIds.Add(this.IpamIPAddress.IPAddress.SubnetId);

        this.TimePeriod = TimePeriodFromRequest(this.TimePeriod);

        ResetHistoryControls();
        SetHistoryControls(this.TimePeriod);

        HistoryType type = GetHistoryType();
        SetEditButtonFromRequest(type);
        SetHelpButtonFromRequest(type);
        string title = GetDecoratedTitle(type);
        string subtitle = GetDecoratedSubTitle(this.TimePeriod);

        this.Title = title;
        this.TitleText.Text = title;
        this.ResourceTitleText.Text = title;
        this.ResourceSubTitleText.Text = subtitle;
        this.TimePeriodText.Text = this.TimePeriod.ToDisplayString();
        this.TimePeriodSelector.SetValue(this.TimePeriod);
    }

    protected void OnSearchClick(object sender, EventArgs e)
    {
        if (this.txtSearch != null)
        {
            this.SearchQuery = this.txtSearch.Text;
        }

        SetHistoryControls(this.TimePeriod);
    }

    #endregion // Events

    #region Methods

    private void LoadFromRequest()
    {
        int resourceId = GetParam(QUERY_PARAM_RESOURCEID, -1);
        if (resourceId >= 0)
        {
            this.Resource = ResourceManager.GetResourceByID(resourceId);
        }

        this.Mac = WebSecurityHelper.SanitizeHtml(GetParam(QUERY_PARAM_MAC, QUERY_INVALID_VALUE));
        this.Dns = WebSecurityHelper.SanitizeHtml(GetParam(QUERY_PARAM_DNS, QUERY_INVALID_VALUE));

        // load NetObject from request
        string netObjectID = GetParam(QUERY_PARAM_NETOBJECT, string.Empty);
        if (string.IsNullOrEmpty(netObjectID) == false)
        {
            this.IpamIPAddress = NetObjectFactory.Create(netObjectID) as IpamIPNode;
        }
    }

    private HistoryType GetHistoryType()
    {
        // Decide the type of history to show
        if (QUERY_INVALID_VALUE.Equals(this.Mac) == false)
        {
            return HistoryType.MAC;
        }
        else if (QUERY_INVALID_VALUE.Equals(this.Dns) == false)
        {
            return HistoryType.DNS;
        }
        else if (this.IpamIPAddress != null)
        {
            return HistoryType.IP;
        }
        else
        {
            return HistoryType.unknown;
        }
    }

    private void SetEditButtonFromRequest(HistoryType type)
    {
        if (this.ResourceEditButton == null)
        {
            return;
        }

        bool isVisible = (this.Resource != null) && (this.IpamIPAddress != null);
        this.ResourceEditButton.Visible = isVisible;
        if (isVisible)
        {
            this.ResourceEditButton.NavigateUrl = string.Format(
                BaseAssignedResourceControl.EditPageTemplate,
                this.Resource.ID, this.IpamIPAddress.NetObjectID);
        }
    }

    private void SetHelpButtonFromRequest(HistoryType type)
    {
        if (this.ResourceHelpButton == null)
        {
            return;
        }

        string fragment = string.Empty;
        switch(type)
        {
            case HistoryType.IP:
                fragment = BaseAssignedResourceControl.HelpPageIPAddressAsssignedHistory;
                break;
            case HistoryType.MAC:
                fragment = BaseAssignedResourceControl.HelpPageMACAsssignedHistory;
                break;
            case HistoryType.DNS:
                fragment = BaseAssignedResourceControl.HelpPageDNSAsssignedHistory;
                break;
            default:
                break;
        }

        this.ResourceHelpButton.NavigateUrl = GetHelpUrl(fragment);
    }

    private TimePeriodValue TimePeriodFromRequest(TimePeriodValue timeperiod)
    {
        // from post/get request
        string request = GetParam(QUERY_PARAM_TIMEPERIOD, string.Empty);
        if (!string.IsNullOrEmpty(request))
        {
            request = System.Web.HttpUtility.UrlDecode(request);
            timeperiod = TimePeriodValue.DeSerialize(request);
        }

        // set new value, whether the read value does not fail
        if ((this.Page.IsPostBack == true) &&
            (this.TimePeriodSelector != null))
        {
            TimePeriodValue value = this.TimePeriodSelector.ReadValue();
            if (value != null)
            {
                timeperiod = value;
            }
        }

        if (timeperiod == null)
        {
            timeperiod = TimePeriodValue.DetaultTimePeriod;
        }

        return timeperiod;
    }

    private void ResetHistoryControls()
    {
        if (this.IPAssignedHistoryList != null)
        {
            this.IPAssignedHistoryList.Visible = false;
        }
        if (this.AssignedHistoryList != null)
        {
            this.AssignedHistoryList.Visible = false;
        }

        if (this.MsgNoItems != null)
        {
            this.MsgNoItems.Visible = false;
        }
        if (this.MsgMaxItems != null)
        {
            this.MsgMaxItems.Visible = false;
        }
    }

    private void SetHistoryControls(TimePeriodValue timeperiod)
    {
        List<AssignedHistory> history = null;

        HistoryType type = GetHistoryType();
        switch (type)
        {
            case HistoryType.IP:
                history = LoadIPAddressHistoryData(this.IpamIPAddress, timeperiod, this.SearchQuery);
                ShowAssignedHistoryData(this.IPAssignedHistoryList, history);
                break;
            case HistoryType.MAC:
                history = LoadMACAssignedHistoryData(this.Mac, timeperiod, this.SearchQuery);
                ShowAssignedHistoryData(this.AssignedHistoryList, history);
                break;
            case HistoryType.DNS:
                history = LoadDNSAssignedHistoryData(this.Dns, timeperiod, this.SearchQuery);
                ShowAssignedHistoryData(this.AssignedHistoryList, history);
                break;
            default:
                break;
        }
    }

    private string GetDecoratedTitle(HistoryType type)
    {
        string titleBase = string.Empty;
        string decorator = string.Empty;
        switch (type)
        {
            case HistoryType.IP:
                titleBase = BaseAssignedResourceControl.TitleTemplateIPAddressAsssignedHistory;
                decorator = this.IpamIPAddress.Name;
                break;
            case HistoryType.MAC:
                titleBase = BaseAssignedResourceControl.TitleTemplateMACAsssignedHistory;
                decorator = this.Mac;
                break;
            case HistoryType.DNS:
                titleBase = BaseAssignedResourceControl.TitleTemplateDNSAsssignedHistory;
                decorator = this.Dns;
                break;
            default:
                break;
        }

        if ((this.Resource != null) && !string.IsNullOrEmpty(this.Resource.Title))
        {
            titleBase = this.Resource.Title;
        }
        return titleBase + " - " + decorator;
    }

    private string GetDecoratedSubTitle(TimePeriodValue timeperiod)
    {
        if ((this.Resource != null) &&
            !string.IsNullOrEmpty(this.Resource.SubTitle))
        {
            return this.Resource.SubTitle;
        }
        else if (timeperiod != null)
        {
            return timeperiod.ToDisplayString();
        }
        else
        {
            return "&nbsp;";
        }
    }

    private void ShowAssignedHistoryData(Repeater historyList, List<AssignedHistory> history)
    {
        if ((historyList == null) || (history == null))
        {
            return;
        }

        // resource content
        historyList.Visible = true;
        historyList.DataSource = history;
        historyList.DataBind();

        if (this.MsgNoItems != null)
        {
            this.MsgNoItems.Visible = (history.Count <= 0);
        }

        if (this.MsgMaxItems != null)
        {
            this.MsgMaxItems.Visible = this.HistoryCountOverflow;
        }
    }

    private List<AssignedHistory> LoadIPAddressHistoryData(IpamIPNode ipamIPAddress,
        TimePeriodValue timeperiod, string filter)
    {
        if ((ipamIPAddress == null) ||
            (ipamIPAddress.IPAddress == null) ||
            (ipamIPAddress.IPAddress.IpNodeId == IpamIPNode.PreviewNetObjectId))
        {
            // handle incorrect or 'preview' netobject
            return new List<AssignedHistory>();;
        }

        if (timeperiod == null)
        {
            timeperiod = TimePeriodValue.DetaultTimePeriod;
        }

        // .. real and process whole assigned history
        int ip = ipamIPAddress.IPAddress.IpNodeId.Value;
        List<AssignedHistory> history = AssignedHistoryManager.LoadIPAssignedHistory(
            ip, filter,
            timeperiod.BeginDate.ToUniversalTime(),
            timeperiod.EndDate.ToUniversalTime(),
            false);

        return TrimMaxHistoryCount(history);
    }

    private List<AssignedHistory> LoadMACAssignedHistoryData(string mac,
        TimePeriodValue timeperiod, string filter)
    {
        if (string.IsNullOrEmpty(mac))
        {
            return new List<AssignedHistory>();
        }

        if (timeperiod == null)
        {
            timeperiod = TimePeriodValue.DetaultTimePeriod;
        }

        // real and process whole assigned history
        List<AssignedHistory> history = AssignedHistoryManager.LoadMACAssignedHistory(
            mac, filter,
            this.TimePeriod.BeginDate.ToUniversalTime(),
            this.TimePeriod.EndDate.ToUniversalTime(),
            false);

        return TrimMaxHistoryCount(history);
    }

    private List<AssignedHistory> LoadDNSAssignedHistoryData(string dns,
        TimePeriodValue timeperiod, string filter)
    {
        if (string.IsNullOrEmpty(dns))
        {
            return new List<AssignedHistory>();
        }

        if (timeperiod == null)
        {
            timeperiod = TimePeriodValue.DetaultTimePeriod;
        }

        // real and process whole assigned history
        List<AssignedHistory> history = AssignedHistoryManager.LoadDNSAssignedHistory(
            dns, filter,
            this.TimePeriod.BeginDate.ToUniversalTime(),
            this.TimePeriod.EndDate.ToUniversalTime(),
            false);

        return TrimMaxHistoryCount(history);
    }

    private List<AssignedHistory> TrimMaxHistoryCount(List<AssignedHistory> history)
    {
        if (history.Count <= MAX_HISTORY_COUNT)
        {
            return history;
        }

        this.HistoryCountOverflow = true;

        AssignedHistory[] trim = new AssignedHistory[MAX_HISTORY_COUNT];
        history.CopyTo(0, trim, 0, MAX_HISTORY_COUNT);

        return new List<AssignedHistory>(trim);
    }

    protected IPNode IPNodeFromAssignedHistory(object arg)
    {
        return AssignedHistoryManager.IPNodeFromAssignedHistory(arg);
    }

    protected string HandleDateTimeValue(string property)
    {
        object value = Eval(property);
        return BaseAssignedResourceControl.HandleDateTimeValue(value);
    }

    protected string HandleEmptyValue(object value)
    {
        return BaseAssignedResourceControl.HandleEmptyValue(value);
    }

    #endregion // Methods
}
