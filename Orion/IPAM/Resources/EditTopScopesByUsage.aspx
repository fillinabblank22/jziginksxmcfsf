<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditTopScopesByUsage.aspx.cs" Inherits="Orion_IPAM_Resources_EditTopScopesByUsage" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_648%>" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="ipam" TagName="FilterScopesSql" Src="~/Orion/IPAM/Controls/FilterScopesSql.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <span><b><big><%= Page.Title %></big></b></span>
 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
 
 <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
 
 <b><% = Resources.IPAMWebContent.IPAMWEBDATA_VB1_649 %></b>
 <br /> 
 <asp:TextBox runat="server" ID="HowMany" />
 <asp:CompareValidator ID="CompareValidator1" runat="server" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_650 %>" 
      Operator="GreaterThanEqual" 
      Type="Integer" 
      ValueToCompare="1" 
      ControlToValidate="HowMany">*</asp:CompareValidator>
      
 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
      ControlToValidate="HowMany" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_651 %>">*</asp:RequiredFieldValidator>   
 <br />
 <br />
 
 <ipam:FilterScopesSql runat="server" ID="SQLFilter" />
    <br />
    <br />
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary"/> 
</asp:Content>

