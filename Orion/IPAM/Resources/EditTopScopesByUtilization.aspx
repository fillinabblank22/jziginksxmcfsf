<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="~/Orion/IPAM/Resources/EditTopScopesByUtilization.aspx.cs" Inherits="Orion_IPAM_Resources_EditTopScopesByUtilization" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_648%>" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register TagPrefix="ipam" TagName="FilterSubnetsSql" Src="~/Orion/IPAM/Controls/FilterScopesUtilization.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <span><b><big><%= Page.Title %></big></b></span>
 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
 
 <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
 
 <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_649%></b>
 <br /> 
 <asp:TextBox runat="server" ID="HowMany" />
 <asp:CompareValidator ID="CompareValidator1" runat="server" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_658 %>" 
      Operator="GreaterThanEqual" 
      Type="Integer" 
      ValueToCompare="1" 
      ControlToValidate="HowMany">*</asp:CompareValidator>
      
 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
      ControlToValidate="HowMany" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_659 %>">*</asp:RequiredFieldValidator>   
 <br />
 <br />
 
 <ipam:FilterSubnetsSql runat="server" ID="SQLFilter" />
  <br />
 <br />
   <b><%= Resources.IPAMWebContent.IPAMWEBDATA_MR1_44 %></b><br />
    <asp:RadioButtonList ID="rdb_SortBY" runat="server">
        <asp:ListItem 
            Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_41 %>"
            Value="0"
            ></asp:ListItem>
        <asp:ListItem 
            Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_42 %>"
            Value="1" 
            ></asp:ListItem>
        <asp:ListItem 
            Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_43 %>"
            Value="2"
            ></asp:ListItem>
    </asp:RadioButtonList>
    <orion:LocalizableButton runat="server" ID="btnSubmit"  OnClick="SubmitClick" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" LocalizedText="Submit" DisplayType="Primary"/>
</asp:Content>

