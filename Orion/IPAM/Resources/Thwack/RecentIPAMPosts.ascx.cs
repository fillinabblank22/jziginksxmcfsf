using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_IPAM_Resources_RecentThwackPosts : BaseResourceControl
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");
        HelpButton.NavigateUrl =
            string.Format(
                "http://www.solarwinds.com/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={0}.htm",
                this.HelpLinkFragment);
    }

	protected override string DefaultTitle
	{
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_12; }
	}

	[PersistenceMode(PersistenceMode.Attribute)]
	public int Count
	{
		get
		{
			return GetIntProperty("NumberOfPosts", 15);
		}
	}

    public int ForumId
    {
        get { return 127; }
    }

	public override string HelpLinkFragment
	{
		get { return "OrionIPAMPHResourceThwackRecentPosts"; }
	}

    public override string EditURL
    {
		get { return GetEditUrl(this.Resource); }
    }

    public override string SubTitle
    {
        get { return string.Empty; }
    }

	private string GetEditUrl(ResourceInfo resource)
	{
		return string.Format("/Orion/IPAM/Resources/EditRecentIPAMPosts.aspx?ResourceID={0}", resource.ID);
	}
}
