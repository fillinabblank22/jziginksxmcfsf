<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentIPAMPosts.ascx.cs" Inherits="Orion_IPAM_Resources_RecentThwackPosts" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
			<asp:ServiceReference path="/Orion/IPAM/Services/Thwack.asmx" />
    </Services>
</asp:ScriptManagerProxy>
<link rel="stylesheet" type="text/css" href="/Orion/IPAM/res/css/sw-resources.css" />
<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
		<div class="IPAMThwackContentBorder">
        <table class="IPAMThwackHeader IPAMThwackTableMargin">
            <tr>
				<td class="NoBorder">				
					<table class="IPAMThwackTableMargin">
						<tr>
							<td class="IPAMThwackCaptionColumn NoBorder" colspan = "2">
                                <div>
								    <img class="IPAMThwackHeaderImage" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("IPAM", "res/images/sw-thwack/ThwackLogo.png") %>" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_682 %>" />
                                    <span class="IPAMThwackTextHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_4%></span>
                                </div>
                            </td>
                            <td class="NoBorder">
                                <asp:PlaceHolder ID="phHelpButton" runat="server">
                                    <orion:LocalizableButtonLink ID="HelpButton" runat="server" LocalizedText="CustomText"
                                                                 CssClass="EditResourceButton" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_37 %>" DisplayType="Secondary"
                                                                 Target="_blank"/>
                                </asp:PlaceHolder>&nbsp;
                                <asp:PlaceHolder ID="phEditButton" runat="server">
                                    <orion:LocalizableButtonLink ID="LocalizableButton1" runat="server" LocalizedText="Edit"
                                                                 CssClass="HelpButton" DisplayType="Secondary" NavigateUrl="<%# this.EditURL %>"/>
                                </asp:PlaceHolder>
                            </td>
                            <td style="border: none;height:34px;width:15px;">&nbsp;</td>
						</tr>
					</table>
				</td>				
            </tr>
        </table>
        <div runat="server" id="ThwackRecentPostsList" class="IPAMThwackContent"/>
		<table cellpadding="0" cellspacing="0" style="height: 22px;">
            <tr>
                <td style="border: none; text-align: right; padding-right: 10px;">
                    <a href="http://thwack.com/forums/<%= this.ForumId %>.aspx" class="IPAMThwack_buttom"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_680 %></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://thwack.com/user/CreateUser.aspx" class="IPAMThwack_buttom"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_681%></a>
                </td>
            </tr>
        </table>
		</div>
        <script type="text/javascript">
//<![CDATA[
            Sys.Application.add_init(function(){
                IpamThwack.GetRecentPosts(<%= this.Count %>, 
                    function(result) {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = result;
                    },
                    function() {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_277;E=js}';
                    }); });
//]]>
        </script>
     </Content>
</orion:resourceWrapper>
