﻿using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_DNSMismatch_DNSMismatch :BaseResourceControl
{
   
    protected ResourceSearchControl SearchControl { get; private set; }

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);

        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;
        // Set search SWQL query that is used for searching
        CustomTable.SearchSWQL = SearchSWQL;
        
    }


    private string SWQL
    {
        get
        {
            return @"SELECT DNSServer,
                 '/api2/ipam/ui/dns/node/' + TOSTRING(NodeId) AS [_LinkFor_DNSServer],
                 DNSZone,
                 '/Orion/IPAM/Dns.Records.aspx?ObjectId=' + TOSTRING(DnsZoneGroupId) AS [_LinkFor_DNSZone],
                 ClientHostName, 
                 '/Orion/IPAM/res/images/sw/icon.DNS.ClientHName.png' AS [_IconFor_ClientHostName],
                 ForwardZoneIPAddressN AS ""IPInFwdZone"",          
                 ReverseZoneIPAddressN AS ""IPInRevZone"",
                  ForwardZoneIPAddress AS [_FWDZoneIP],
                  ReverseZoneIPAddress AS [_ReverseZoneIP]  
                  FROM IPAM.DNSMismatch";
        }
   
    }
    

    private string SearchSWQL
    {
        get
        {
            return @"SELECT DNSServer,
                 '/api2/ipam/ui/dns/node/' + TOSTRING(NodeId) AS [_LinkFor_DNSServer],
                 DNSZone,
                 '/Orion/IPAM/Dns.Records.aspx?ObjectId=' + TOSTRING(DnsZoneGroupId) AS [_LinkFor_DNSZone],
                 ClientHostName, 
                 '/Orion/IPAM/res/images/sw/icon.DNS.ClientHName.png' AS [_IconFor_ClientHostName],
                 ForwardZoneIPAddressN AS ""IPInFwdZone"",        
                 ReverseZoneIPAddressN AS ""IPInBwdZone"",
                  ForwardZoneIPAddress AS [_FWDZoneIP],
                  ReverseZoneIPAddress AS [_ReverseZoneIP]  
                  FROM IPAM.DNSMismatch WHERE DNSServer LIKE '%${SEARCH_STRING}%' or DNSZone LIKE '%${SEARCH_STRING}%' 
                  or ClientHostName LIKE '%${SEARCH_STRING}%' or [_FWDZoneIP] LIKE '%${SEARCH_STRING}%' 
                  or [_ReverseZoneIP] LIKE '%${SEARCH_STRING}%'";
        }
    }
    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBDATA_MR1_37; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMDNSMismatch"; }
    }

   
}