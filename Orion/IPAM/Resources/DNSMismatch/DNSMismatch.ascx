﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DNSMismatch.ascx.cs" Inherits="Orion_IPAM_Resources_DNSMismatch_DNSMismatch" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx"  %>
<!-- This is needed if you want a search feature -->
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
 

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
 
        <script type="text/javascript">
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= ScriptFriendlyResourceID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        // How many rows there can be on one page. If there are more rows, paging toolbar is displayed.
                        rowsPerPage: <%= Resource.Properties["RowsPerPage"] ?? "5" %>,
                        // If you want a search feature, this must be set to client ID of search textbox
                        searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                        // If you want a search feature, this must be set to client ID of search button
                        searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                        // True to allow sorting of rows by clicking on column headers
                        allowSort: true,
                        allowSearch: true,
                        columnSettings: {
                            // Use column name (or alias if it's defined) from SWQL query to define which column this setting applies to. Column name is case sensitive.
                            "DNSServer": {
                                header: 'DNS Server'
                            },
                            "DNSZone": {
                                header: 'DNS Zone'
                            },
                            "ClientHostName": {
                                header: 'Client Host Name'
                            },
                            "IPInFwdZone": {
                                header: 'IP In Fwd Zone',
                                formatter: function (value, row, cellinfo) { 
                                    var forwardIP =row[8];
                                    var reverseIP =row[9];
                                    var len = Math.max(forwardIP.length, reverseIP.length);
                                    for(var i = 0; i < len; i++) {
                                        if(forwardIP.charAt(i) !== reverseIP.charAt(i)) {
                                            var preAppchar="<font color='red'>";
                                           // return forwardIP;
                                            return forwardIP.substr(0, i) + preAppchar + forwardIP.substr(i) +"</font>";                                        
                                        }
                                    };
                                    return forwardIP;
                                },
                                isHtml: true  
                            },
                            "IPInRevZone": {
                                header: 'IP In REV Zone',
                                formatter: function (value, row, cellinfo) { 
                                    var forwardIP =row[8];
                                    var reverseIP =row[9];

                                    var len = Math.max(forwardIP.length, reverseIP.length);
                                    for(var i = 0; i < len; i++) {
                                        if(forwardIP.charAt(i) !== reverseIP.charAt(i)) {
                                            var preAppchar="<font color='red'>";
                                          //  return reverseIP;
                                           return reverseIP.substr(0, i) + preAppchar + reverseIP.substr(i) +"</font>";                                        
                                        }
                                    };
                                    return reverseIP;
                                },
                                isHtml: true  
                            }  
                        }
                    });
 
                // This can be used to put placeholder text to search textbox
                $('#' + '<%= SearchControl.SearchBoxClientID %>').attr("placeholder", "Search"); 
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });    
        </script>
    </Content>
</orion:resourceWrapper>
