<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true" CodeFile="EditSearchIPAddress.aspx.cs" Inherits="Orion_IPAM_Resources_EditSearchIPAddress" Title="Untitled Page" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>
<%@ Register Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" TagPrefix="IPAMmaster"%>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <IPAMmaster:jsCollector ID="jscollector" runat="server" />
    <span><b><big><%= Page.Title %></big></b></span>
    <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
    
    <asp:Repeater ID="SearchFieldsRepeater" runat="server" >
    <HeaderTemplate>
        <table border="0" cellspacing="3px" cellpadding="0">
                        <tr>
                            <td ></td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td ><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_646 %></b></td>
                            <td ><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_647 %></b></td>
                        </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
            <td><asp:Label ID="columnName" runat="server"></asp:Label></td>
            <td align="center"><asp:CheckBox ID="isCheckedByDefault" runat="server" /></td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
    </table>
    </FooterTemplate>
    </asp:Repeater>
    <br/>
    <br/>
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary"/>
</asp:Content>

