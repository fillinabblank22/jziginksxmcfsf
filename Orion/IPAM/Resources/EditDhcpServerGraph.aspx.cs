﻿using System;
using SolarWinds.IPAM.Web.Common.Controls;

public partial class Orion_IPAM_Resources_EditDhcpServerGraph : AbstractResourceEditPage
{
    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((this.Resource != null) && (this.IsPostBack == false))
        {
            this.TitleEditControl.ResourceTitle = Resource.Title;
            this.TitleEditControl.ResourceSubTitle = Resource.SubTitle;
            this.ResourceName.Text = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, Resource.Name);

            string width = this.Resource.Properties["width"];
            string height = this.Resource.Properties["height"];
            if (!string.IsNullOrEmpty(width)) this.txtWidth.Text = width;
            if (!string.IsNullOrEmpty(height)) this.txtHeight.Text = height;
        }
    }

    protected override void OnSubmitClick(object sender, EventArgs e)
    {
        if (this.TitleEditControl == null)
        {
            return;
        }

        string title = this.TitleEditControl.ResourceTitle;
        string subtitle = this.TitleEditControl.ResourceSubTitle;
        if ((Resource.Title != title) || (Resource.SubTitle != subtitle))
        {
            Resource.Title = title;
            Resource.SubTitle = subtitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        Resource.Properties["width"] = this.txtWidth.Text;
        Resource.Properties["height"] = this.txtHeight.Text;

        string url = string.Format(
            "/Orion/IPAM/IPAMDHCPServerView.aspx?ViewID={0}", this.Resource.View.ViewID);
        if (string.IsNullOrEmpty(this.NetObjectId) == false)
        {
            url += string.Format("&{0}={1}", NetObjectIdQueryParam, this.NetObjectId);
        }
        Response.Redirect(url);
    }

    #endregion // Events
}
