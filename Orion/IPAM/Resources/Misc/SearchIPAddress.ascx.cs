using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Text;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_IPAM_Resources_Summary_SearchIPAddress : BaseResourceControl
{
    #region Constants

    private const string PROPERTY_SEARCHFIELDS_VISIBLE = "SearchFields_Visible";
    private const string PROPERTY_SEARCHFIELDS_CHECKED = "SearchFields_Checked";

    const string PATH_SEARCHPAGE = "~/Orion/IPAM/search.aspx"; // should be implemented

    #endregion // Constants

    #region Resource Properties

    private bool? noAccessRole = null;
    protected bool NoAccessRole
    {
        get
        {
            if (this.noAccessRole == null)
            {
                this.noAccessRole = AuthorizationHelper.AccessCheck(
                    this.Page, AccountRole.NoAccess, new int[] { });
            }
            return this.noAccessRole.Value;
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_11; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceSearchIPAddress"; }
    }

    public override string EditURL
    {
        get { return GetEditUrl(this.Resource); }
    }

    #endregion // Resource Properties

    #region Events

    protected override void OnLoad(EventArgs e)
    {
        if (this.NoAccessRole == false)
        {
            base.AddStylesheet("~/Orion/IPAM/res/css/sw-search-box.css");

            // add all necessary JavaScripts
            TryAddJavaScript("~/Orion/IPAM/res/js/sw-search-box.js");
            TryAddJavaScript("~/Orion/IPAM/res/js/sw-search-resource.js");
        }
        else
        {
            this.Visible = false;
        }

        base.OnLoad(e);
    }

    protected override void OnInit(EventArgs e)
    {
        if (this.NoAccessRole == false)
        {
            InitSearchResource();
        }
        else
        {
            this.Visible = false;
        }

        base.OnInit(e);
    }

    #endregion // Events

    #region Methods

    private void InitSearchResource()
    {
        Exception ex = null;
        string taskName;

        List<object> values = SwisConnector.RunSwisOps(new SwisOpCtx[]{
            new SwisOpCtx("Retrieve list of custom properties", OpGetCustomPropertyDefs,null)
        }, out ex, out taskName);

        if (ex != null)
        {
            this.ResourceWrapper1.Content.Controls.Clear();
            this.ResourceWrapper1.Content.Controls.Add(new VisualizeException(ex));
        }
        else
        {
            string resourceId = string.Format("Res{0}", Resource.ID);

            StringBuilder renderer = new StringBuilder();
            renderer.AppendLine("function (el) {");
            renderer.AppendLine("  return jQuery.jmenu({");
            renderer.AppendFormat("   items: $SW.SearchInfo{0}.columns,", resourceId); renderer.AppendLine();
            renderer.AppendLine("   onClick: $SW.CheckBoxItemClick,");
            renderer.AppendLine("   elem: el,");
            renderer.AppendFormat("   config: $SW.SearchInfo{0}", resourceId); renderer.AppendLine();
            renderer.AppendLine("  });");
            renderer.AppendLine("}");

            this.searchResourceState.ResourceId = resourceId;
            this.searchResourceBox.ResourceId = resourceId;
            this.searchResourceBox.ContentRenderer = renderer.ToString();
            this.searchResourceBox.ControlReady = "$SW.FillSearchFilter";
            this.btnSearch.OnClientClick = string.Format("return $SW.SearchResultGo('#{0}','{1}');",
                this.txtSearchString.ClientID, resourceId);

            InitSearchField();
        }
    }

    private string GetEditUrl(ResourceInfo resource)
    {
        string url = string.Format("/Orion/IPAM/Resources/EditSearchIPAddress.aspx?ResourceID={0}", resource.ID);
        return url;
    }   

    private void TryAddJavaScript(string path)
    {
        if (string.IsNullOrEmpty(path))
        {
            throw new ArgumentNullException("path must not be null or empty");
        }

        ScriptReference found = this.ScriptRegistrar.Scripts.FirstOrDefault(
            js => path.Equals(js.Path, StringComparison.OrdinalIgnoreCase));

        if (found == null)
        {
            this.ScriptRegistrar.Scripts.Add(new ScriptReference(path));
        }
    }

    private void InitSearchField()
    {
        // get (user defined) checked columns
        string checkedColumns = Resource.Properties[PROPERTY_SEARCHFIELDS_CHECKED];
        // parse resource
        Dictionary<string, bool> fields = new Dictionary<string, bool>();
        if (!string.IsNullOrEmpty(checkedColumns))
        {
            foreach (string column in checkedColumns.Split(','))
            {
                fields.Add(column, true);
            }
        }
        // set search state to show correct checked columns
        this.searchResourceState.Fields = fields;
    }

    private object OpGetCustomPropertyDefs(IpamClientProxy proxy, object ignore)
    {
        return proxy.AppProxy.CustomProperty.GetAllFromIPNode();
    }

    #endregion // Methods
}
