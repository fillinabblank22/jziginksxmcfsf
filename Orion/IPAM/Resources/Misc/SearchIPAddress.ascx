<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchIPAddress.ascx.cs" Inherits="Orion_IPAM_Resources_Summary_SearchIPAddress" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcommon" Namespace="SolarWinds.IPAM.Web.Common" Assembly="SolarWinds.IPAM.Web.Common" %>

<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<asp:ScriptManagerProxy ID="ScriptRegistrar" runat="server" />

<style type="text/css"> 
input.search-field{
    width:95%;
}
.ext-ie input.search-field{
    margin-top:2px;
}
.jcombo-content {
    width: auto !important;
    min-width: 200px !important;
}
</style>

<orion:ResourceWrapper ID="ResourceWrapper1" runat="server">
    <Content>
        <table class="DefaultShading LayoutTable">
            <tr>
                <td style="width: auto;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_677%></td>
                <td style="width: 200px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_678 %></td>
                <td style="width: 77px;"></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtSearchString" Rows="1"
                                 CssClass="jcombo-input" Height="18px" Width="98%"/>
                </td>
                <td>
                    <IPAMui:SearchResourceBox id="searchResourceBox" emptyText="" runat="server" style="width: auto" />
                    <IPAMcommon:SearchResourceState ID="searchResourceState" StoreColumns="false" runat="server" />
                </td>
                <td>
                    <div style="padding-top: 3px">
                        <orion:LocalizableButton runat="server"  ID="btnSearch" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_679 %>"/>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
        </table>
    </Content>
</orion:ResourceWrapper>