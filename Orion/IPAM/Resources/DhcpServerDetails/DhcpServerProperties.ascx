﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpServerProperties.ascx.cs"
            Inherits="Orion_IPAM_Resources_DhcpServerProperties" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <asp:Repeater runat="server" ID="DhcpServerProperties">
        <HeaderTemplate>

    <div style="height: 5px;">&nbsp;</div>
        <table border="0" cellPadding="2" cellSpacing="0" width="100%">
	        <tr>
		        <td class="Property" width="10px">&nbsp;</td>
		        <td class="PropertyHeader" valign="center" width="150px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_683%></td>
		        <td class="Property" width="22px">&nbsp;</td>
		        <td class="Property NodeManagementIcons">
<% if (this.IsPowerUser) { %>
			        <a href="<%#this.EditDhcpServerURL%>"  runat="server" ID="EditServerUrlPowerUser">
                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_686 %>" />
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_686 %>
                    </a>
<% } else { %>
                    <a href="#" runat="server" ID="EditServerUrlNormalUser" style="color: #A0A0A0;"  onclick="alert('<%# Resources.IPAMWebContent.IPAMWEBDATA_VB1_687 %>');">
                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_686 %>" />
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_686 %>
                    </a>
<% } %>
			            
		        </td>
	        </tr>
	                
        </HeaderTemplate>
        
        <ItemTemplate>
	        <tr id="PropertyContainer" runat="server">
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="center"><asp:Label ID="PropertyName" runat="server" /></td>
              <td class="Property">&nbsp;</td>
              <td class="Property">
                 <asp:Label ID="PropertyValue" Visible="false" runat="server" />
              </td>
	        </tr>
        </ItemTemplate>
        
        <FooterTemplate>
        
        </table>
    </div>
            
        </FooterTemplate>
    </asp:Repeater>
  </Content>
</orion:resourceWrapper>
