﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_DhcpServerProperties : BaseResourceControl
{
    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_17; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMDhcpServerProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceDhcpProperties"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region DhcpServer Properties

    protected IpamDhcpServer IpamDhcpServer { get; set; }
    protected DhcpServer DhcpServer { get; set; }
    protected string EditDhcpServerURL { get; set; }

    private bool? isPowerUser = null;
    protected bool IsPowerUser
    {
        get
        {
            if (this.isPowerUser == null)
            {
                this.isPowerUser = AuthorizationHelper.IsUserInRole(AccountRole.PowerUser);
            }
            return this.isPowerUser.Value;
        }
    }

    #endregion // DhcpServer Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");

        IIPAMDhcpServerProvider ipamDhcpServerProvider = GetInterfaceInstance<IIPAMDhcpServerProvider>();
        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
        if ((ipamDhcpServerProvider != null) && (ipamDhcpServerProvider.DhcpServer != null))
        {
            this.IpamDhcpServer = ipamDhcpServerProvider.DhcpServer;
            this.DhcpServer = this.IpamDhcpServer.DhcpServer;

            InitResourceLinks();
            InitResourceData();
            if (resInfo.IsInReport)
            {
                this.Resource.Title = string.Format("{0} - {1}", this.Resource.Title, this.IpamDhcpServer.Name);
                Control headerTemplate = DhcpServerProperties.Controls[0].Controls[0];
                HtmlAnchor anchorPowerUser = headerTemplate.FindControl("EditServerUrlPowerUser") as HtmlAnchor;
                HtmlAnchor anchorNormalUser = headerTemplate.FindControl("EditServerUrlNormalUser") as HtmlAnchor;
                anchorPowerUser.Visible = false;
                anchorNormalUser.Visible = false;
            }
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditDhcpServerDetails.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // edit dhcp server button
        this.EditDhcpServerURL = string.Format(
            "/Orion/IPAM/Dhcp.Server.Edit.aspx?ObjectId={0}",
            this.DhcpServer.GroupId);
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // resource content
        if (this.DhcpServerProperties != null)
        {
            List<CustomPropertyBindInfo> customProperties =
                PageIPv6EditorHelper.RetreiveCustomProperties(this.DhcpServer, CustomPropertyObject.Group);
            this.DhcpServerProperties.DataSource = customProperties;
            this.DhcpServerProperties.ItemDataBound +=
                new RepeaterItemEventHandler(OnDataBinding);
            this.DhcpServerProperties.DataBind();
        }
    }

    private void OnDataBinding(object src, RepeaterItemEventArgs args)
    {
        if (this.DhcpServer == null)
        {
            return;
        }

        RepeaterItem item = args.Item;
        if ((item.ItemType != ListItemType.Item) &&
            (item.ItemType != ListItemType.AlternatingItem))
        {
            return;
        }

        CustomPropertyBindInfo bindInfo = item.DataItem as CustomPropertyBindInfo;
        string propertyValue = WebSecurityHelper.SanitizeHtmlV2(bindInfo.ExistingValue);

        // correct visualise the blank values
        if (string.IsNullOrEmpty(propertyValue)) propertyValue = "&nbsp;";

        Label name = item.FindControl("PropertyName") as Label;
        if (name != null)
        {
            name.Text = bindInfo.PropertyName;
        }

        Label value = item.FindControl("PropertyValue") as Label;
        value.Visible = true;
        value.Text = propertyValue;
        value.Attributes.Add("ColumnName", bindInfo.PropertyName);
    }

    #endregion // Methods
}