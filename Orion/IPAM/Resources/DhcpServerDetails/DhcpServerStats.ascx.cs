﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_DhcpServerStats : BaseResourceControl
{
    #region Constants

    /// <summary>
    /// This list define the DhcpServer statistic properties and they order.
    /// </summary>
    public static string[] DisplayProperties = new string[]
	{
        DhcpServer.EIM_STATSTARTTIME,
		DhcpServer.EIM_STATDISCOVERS,
        DhcpServer.EIM_STATOFFERS,
        DhcpServer.EIM_STATREQUESTS,
        DhcpServer.EIM_STATACKS,
        DhcpServer.EIM_STATNAKS,
        DhcpServer.EIM_STATDECLINES,
        DhcpServer.EIM_STATRELEASES,
        DhcpServer.EIM_TOTALCOUNT,
        DhcpServer.EIM_USEDCOUNT,
        DhcpServer.EIM_AVAILABLECOUNT
	};

    #endregion // Constants

    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_18; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMDhcpServerProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceDhcpStats"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region DhcpServer Properties

    protected IpamDhcpServer IpamDhcpServer { get; set; }
    protected DhcpServer DhcpServer { get; set; }

    #endregion // DhcpServer Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");

        IIPAMDhcpServerProvider ipamDhcpServerProvider = GetInterfaceInstance<IIPAMDhcpServerProvider>();
        if ((ipamDhcpServerProvider != null) && (ipamDhcpServerProvider.DhcpServer != null))
        {
            this.IpamDhcpServer = ipamDhcpServerProvider.DhcpServer;
            this.DhcpServer = this.IpamDhcpServer.DhcpServer;

            InitResourceLinks();
            InitResourceData();
        }

        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
        if (resInfo.IsInReport)
        {
            this.Resource.Title = string.Format("{0} - {1}",this.Resource.Title, this.IpamDhcpServer.Name);
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditDhcpServerDetails.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // resource content
        if (this.DhcpServerStats != null)
        {
            this.DhcpServerStats.DataSource = DisplayProperties;
            this.DhcpServerStats.ItemDataBound += new RepeaterItemEventHandler(OnDataBinding);
            this.DhcpServerStats.DataBind();
        }
    }

    private void OnDataBinding(object src, RepeaterItemEventArgs args)
    {
        if (this.DhcpServer == null)
        {
            return;
        }

        RepeaterItem item = args.Item;
        if ((item.ItemType != ListItemType.Item) &&
            (item.ItemType != ListItemType.AlternatingItem))
        {
            return;
        }

        string property = item.DataItem as string;
        string propertyName = GetPropertyTextName(property);
        string propertyValue = GetPropertyTextValue(this.DhcpServer, property);

        // Convert the Start date to user selected time zone value.
        if (property == DhcpServer.EIM_STATSTARTTIME && !string.IsNullOrEmpty(propertyValue))
            propertyValue = Convert.ToDateTime(propertyValue).ConvertToDisplayDate().ToString();

        // correct visualise the blank values
        if (string.IsNullOrEmpty(propertyValue)) propertyValue = "&nbsp;";        

        Label name = item.FindControl("PropertyName") as Label;
        if (name != null)
        {
            name.Text = propertyName;
        }

        Label value = item.FindControl("PropertyValue") as Label;
        if (value != null)
        {
            value.Text = propertyValue;
        }
    }

    protected string GetPropertyTextName(string property)
    {
        return LocalizationHelper.GetEntityDisplayString(
            DhcpServer.EIM_ENTITYNAME.Replace('.', '_'), property);
    }

    protected string GetPropertyTextValue<T>(T obj, string property)
    {
        System.Reflection.PropertyInfo propertyInfo = typeof(T).GetProperty(property);
        if (propertyInfo == null)
        {
            throw new ArgumentException(string.Format(
                "Property '{0}' does not exist for type DhcpServer.", property));
        }

        object propertyValue = propertyInfo.GetValue(obj, null);
        return (propertyValue != null) ? propertyValue.ToString() : string.Empty;
    }

    #endregion // Methods
}