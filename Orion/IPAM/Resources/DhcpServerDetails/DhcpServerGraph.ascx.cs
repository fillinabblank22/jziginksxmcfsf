﻿using System;
using System.Linq;
using System.Collections.Generic;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.PieCharts)]
[ResourceMetadata(StandardMetadataPropertyName.SearchTags, CoreMetadataSearchTagsPieChartsValues.Charts)]
public partial class Orion_IPAM_Resources_DhcpServerGraph : BaseResourceControl
{
    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_14; }
    }

    public override string DisplayTitle
    {
        get
        {
            if ((this.DhcpServer != null) &&
               !string.IsNullOrEmpty(this.DhcpServer.FriendlyName))
            {
                return string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_690, base.DisplayTitle, this.DhcpServer.FriendlyName);
            }
            return base.DisplayTitle;
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMDhcpServerProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceDhcpChart"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region DhcpServer Properties

    protected IpamDhcpServer IpamDhcpServer { get; set; }
    protected DhcpServer DhcpServer { get; set; }
    protected string ChartDhcpServerURL { get; set; }

    #endregion // DhcpServer Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");

        IIPAMDhcpServerProvider ipamDhcpServerProvider = GetInterfaceInstance<IIPAMDhcpServerProvider>();
        if ((ipamDhcpServerProvider != null) && (ipamDhcpServerProvider.DhcpServer != null))
        {
            this.IpamDhcpServer = ipamDhcpServerProvider.DhcpServer;
            this.DhcpServer = this.IpamDhcpServer.DhcpServer;

            InitResourceLinks();
            InitResourceData();
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditDhcpServerGraph.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // edit dhcp server button
        this.ChartDhcpServerURL = string.Format(
            "/Orion/IPAM/ImgChartProvider.ashx?w={0}&h={1}{2}",
            GetIntProperty("width", 300),
            GetIntProperty("height", 200),
            ChartCounts()
        );
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        if (this.DhcpServer.TotalCount.HasValue)
        {
            long total = this.DhcpServer.TotalCount.Value;
            this.TotalCount.Text = total.ToString();

            if (this.DhcpServer.UsedCount.HasValue)
            {
                long count = this.DhcpServer.UsedCount.Value;
                double percent = (total > 0) ? ((double)count / (double)total) : 0;
                this.UsedCount.Text = string.Format("{0}", count);
                this.UsedPercent.Text = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_15, percent * 100);
            }
            if (this.DhcpServer.AvailableCount.HasValue)
            {
                long count = this.DhcpServer.AvailableCount.Value;
                double percent = (total > 0) ? ((double)count / (double)total) : 0;
                this.AvailableCount.Text = string.Format("{0}", count);
                this.AvailablePercent.Text = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_16, percent * 100);
            }
        }
    }

    private string ChartCounts()
    {
        SortedList<long, string> items = new SortedList<long, string>();

        if (this.DhcpServer.TotalCount.HasValue && this.DhcpServer.TotalCount > 0)
        {
            if (this.DhcpServer.UsedCount.HasValue && this.DhcpServer.UsedCount > 0)
            {
                items.Add(this.DhcpServer.UsedCount.Value, "F8D677");
            }
            if (this.DhcpServer.AvailableCount.HasValue && this.DhcpServer.AvailableCount > 0)
            {
                items.Add(this.DhcpServer.AvailableCount.Value, "7CC364");
            }
        }
        else
        {
            items.Add(1, "FEFEFE");
        }
        
        return string.Format("&d={0}&c={1}",
            string.Join("|", items.Keys.Select(l => string.Format("{0}", l)).ToArray()),
            string.Join("|", items.Values.ToArray())
        );
    }

    #endregion // Methods
}