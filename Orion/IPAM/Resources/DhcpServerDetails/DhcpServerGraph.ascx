﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpServerGraph.ascx.cs"
            Inherits="Orion_IPAM_Resources_DhcpServerGraph" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <div style="vertical-align: middle; text-align: center;">
        <img border="0" src="<%=this.ChartDhcpServerURL%>" alt="<%#this.DisplayTitle%>" />
        <table cellspacing="0" cellpadding="0" border="0" style="width: auto; margin:auto; text-align: left;">
          <tr>
            <td class="ip-count" style="width:25px;"><asp:Label Text="0" ID="UsedCount" runat="server" /></td>
            <td class="ip-state ip-used" style="background-position: left center; padding: 2px 22px;">
                <asp:Label Text="0.00" ID="UsedPercent" runat="server" /></td>
          </tr>
          <tr>
            <td class="ip-count"><asp:Label Text="0" ID="AvailableCount" runat="server" /></td>
            <td class="ip-state ip-avail" style="background-position: left center; padding: 2px 22px;">
                <asp:Label Text="0.00" ID="AvailablePercent" runat="server" /></td>
          </tr>
          <tr >
            <td style="border-top: 2px solid gray;" class="ip-count"><asp:Label Text="0" ID="TotalCount" runat="server" /></td>
            <td style="border-top: 2px solid gray;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_685 %></td>
          </tr>
        </table>
    </div>
  </Content>
</orion:resourceWrapper>