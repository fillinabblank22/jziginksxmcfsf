﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpServerStats.ascx.cs"
            Inherits="Orion_IPAM_Resources_DhcpServerStats" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <table border="0" cellPadding="2" cellSpacing="0" width="100%">
	    <tr>
            <td class="ReportHeader">&nbsp;</td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_688 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_689 %></td>
            <td class="ReportHeader">&nbsp;</td>
        </tr>

    <asp:Repeater runat="server" ID="DhcpServerStats">
      <HeaderTemplate />
      <ItemTemplate>
	    <tr>
            <td class="Property" width="10">&nbsp;</td>
            <td class="PropertyHeader"><asp:Label ID="PropertyName" runat="server" /></td>
            <td class="Property"><asp:Label ID="PropertyValue" runat="server" /></td>
            <td class="Property" width="10">&nbsp;</td>
	    </tr>
      </ItemTemplate>
      <FooterTemplate />
    </asp:Repeater>

    </table>
  </Content>
</orion:resourceWrapper>