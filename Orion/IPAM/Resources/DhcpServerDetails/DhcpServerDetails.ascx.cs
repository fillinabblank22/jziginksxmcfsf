﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_DhcpServerDetails : BaseResourceControl
{
    #region Constants

    /// <summary>
    /// This list define the DhcpServer properties and they order.
    /// </summary>
    public static string[] DisplayProperties = new string[]
	{
        //DhcpServer.EIM_STATUS,
        DhcpServer.EIM_SERVERTYPE,
		DhcpServer.EIM_ADDRESS,
        DhcpServer.EIM_COMMENTS,
        DhcpServer.EIM_VLAN,
        DhcpServer.EIM_LOCATION,
        DhcpServer.EIM_ADDNEWSCOPES
	};

    #endregion // Constants

    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_13; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMDhcpServerProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceDhcpDetails"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region DhcpServer Properties

    protected IpamDhcpServer IpamDhcpServer { get; set; }
    protected DhcpServer DhcpServer { get; set; }
    protected string EditDhcpServerURL { get; set; }

    private bool? isPowerUser = null;
    protected bool IsPowerUser
    {
        get
        {
            if (this.isPowerUser == null)
            {
                this.isPowerUser = AuthorizationHelper.IsUserInRole(AccountRole.PowerUser);
            }
            return this.isPowerUser.Value;
        }
    }

    #endregion // DhcpServer Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");

        IIPAMDhcpServerProvider ipamDhcpServerProvider = GetInterfaceInstance<IIPAMDhcpServerProvider>();
        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
       
        if ((ipamDhcpServerProvider != null) && (ipamDhcpServerProvider.DhcpServer != null))
        {
            this.IpamDhcpServer = ipamDhcpServerProvider.DhcpServer;
            this.DhcpServer = this.IpamDhcpServer.DhcpServer;

            InitResourceLinks();
            InitResourceData();
            if (resInfo.IsInReport)
            {
                Control headerTemplate = DhcpServerProperties.Controls[0].Controls[0];
                HtmlAnchor anchorPowerUser = headerTemplate.FindControl("EditServerUrlPowerUser") as HtmlAnchor;
                HtmlAnchor anchorNormalUser = headerTemplate.FindControl("EditServerUrlNormalUser") as HtmlAnchor;
                anchorPowerUser.Visible = false;
                anchorNormalUser.Visible = false;
            }
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditDhcpServerDetails.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamDhcpServer.NetObjectID);

        // edit dhcp server button
        this.EditDhcpServerURL = string.Format(
            "/Orion/IPAM/Dhcp.Server.Edit.aspx?ObjectId={0}",
            this.DhcpServer.GroupId);
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.DhcpServer == null))
        {
            return;
        }

        // resource content
        if (this.DhcpServerProperties != null)
        {
            this.DhcpServerProperties.DataSource = DisplayProperties;
            this.DhcpServerProperties.ItemDataBound += new RepeaterItemEventHandler(OnDataBinding);
            this.DhcpServerProperties.DataBind();          
        }
    }

    private void OnDataBinding(object src, RepeaterItemEventArgs args)
    {
        if (this.DhcpServer == null)
        {
            return;
        }

        RepeaterItem item = args.Item;
        if ((item.ItemType != ListItemType.Item) &&
            (item.ItemType != ListItemType.AlternatingItem))
        {
            return;
        }

        string property = item.DataItem as string;
        string propertyName = GetPropertyTextName(property);
        string propertyValue = WebSecurityHelper.SanitizeHtmlV2(GetPropertyTextValue(this.DhcpServer, property));


        // correct visualise the blank values
        if (string.IsNullOrEmpty(propertyValue)) propertyValue = "&nbsp;";

        Label name = item.FindControl("PropertyName") as Label;
        if (name != null)
        {
            name.Text = propertyName;
        }

        Label value = item.FindControl("PropertyValue") as Label;
        if (value != null)
        {
            value.Text = propertyValue;
        }
    }

    protected string GetPropertyTextName(string property)
    {
        return LocalizationHelper.GetEntityDisplayString(
            DhcpServer.EIM_ENTITYNAME.Replace('.', '_'), property);
    }

    protected string GetPropertyTextValue<T>(T obj, string property)
    {
        System.Reflection.PropertyInfo propertyInfo = typeof(T).GetProperty(property);
        if (propertyInfo == null)
        {
            throw new ArgumentException(string.Format(
                "Property '{0}' does not exist for type DhcpServer.", property));
        }

        object propertyValue = propertyInfo.GetValue(obj, null);

        if(propertyValue != null)
        {
            switch (property)
            {
                case "StatusName":
                    return GetLocalizedProperty("StatusDesc", propertyValue.ToString());
                case "AddNewScopes":
                    return GetLocalizedProperty("AddNewScopes", propertyValue.ToString());
                default:
                    return propertyValue.ToString();
            }
        }
        else
            return string.Empty;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    #endregion // Methods
}