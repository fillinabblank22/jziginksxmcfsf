﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpServerDetails.ascx.cs"
            Inherits="Orion_IPAM_Resources_DhcpServerDetails" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <asp:Repeater runat="server" ID="DhcpServerProperties">
        <HeaderTemplate>

    <div style="height: 5px;">&nbsp;</div>
        <table border="0" cellPadding="2" cellSpacing="0" width="100%">
	        <tr>
		        <td class="Property" width="10px">&nbsp;</td>
		        <td class="PropertyHeader" valign="center" width="150px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_683 %></td>
		        <td class="Property" width="22px">&nbsp;</td>
		        <td class="Property NodeManagementIcons">
<% if (this.IsPowerUser) { %>
			        <a href="<%#this.EditDhcpServerURL%>" runat="server" ID="EditServerUrlPowerUser">
			            <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="Edit DHCP Server" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_684 %>
                    </a>

<% } else { %>
                    <a href="#" style="color: #A0A0A0;" onclick="alert('Your account is not authorized to edit this DHCP server.');" runat="server" ID="EditServerUrlNormalUser">
                        <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="Edit DHCP Server" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_684 %>
                    </a>
<% } %>
                        
		        </td>
	        </tr>
	        <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="center"><%#GetPropertyTextName(SolarWinds.IPAM.BusinessObjects.DhcpServer.EIM_STATUS)%></td>
              <td class="Property" align="right"><div class="dhcpserver-img dhcpserver-<%#GetPropertyTextValue(this.DhcpServer, SolarWinds.IPAM.BusinessObjects.DhcpServer.EIM_STATUSICONPOSTFIX)%>"/></td>
              <td class="Property"><%#GetPropertyTextValue(this.DhcpServer, SolarWinds.IPAM.BusinessObjects.DhcpServer.EIM_STATUSNAME)%></td>
	        </tr>
        </HeaderTemplate>
        
        <ItemTemplate>
	        <tr>
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="center"><asp:Label ID="PropertyName" runat="server" /></td>
              <td class="Property">&nbsp;</td>
              <td class="Property"><asp:Label ID="PropertyValue" runat="server" /></td>
	        </tr>
        </ItemTemplate>
        
        <FooterTemplate>
        
        </table>
    </div>
            
        </FooterTemplate>
    </asp:Repeater>
  </Content>
</orion:resourceWrapper>