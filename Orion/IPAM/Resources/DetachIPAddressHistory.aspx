﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" validateRequest="false"
    Inherits="Orion_IPAM_Resources_DetachIPAddressHistory"
    CodeFile="DetachIPAddressHistory.aspx.cs"%>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/IPAddressLink.ascx" TagName="IPAddressLink" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/TimePeriodSelector.ascx" TagName="TimePeriodSelector" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id=MsgListener Name="dialog" runat="server">
<Msgs />
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock Requires="sw-resources.css" runat="server">
.headerbox { padding-left: 10px; margin: 0px !important; }
.titlebox { padding-top: 10px; }
.ReportHeader { padding-right: 5px; }
.Property { padding-right: 5px; }
#formbox { display: table; position: relative; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
</IPAMmaster:JsBlock>


<div id="ChromeFormHeader" runat="server" class="sw-form-header headerbox">
  <div class="titlebox" style="font-size: large; font-weight: normal;">
     <asp:Label ID="TitleText" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_383 %>" runat="server" />
  </div>

  <div class="titlebox">
    <span style="vertical-align:middle;"><asp:Label ID="TimePeriodText" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_384 %>" runat="server" /></span>
    <a onclick="$('#time-period').toggle();">
        <img src="/Orion/IPAM/res/images/sw-navigation/pick-btn.gif" style="vertical-align:middle;" />
    </a>
  </div>

  <div id="time-period" style="display: none; position:absolute; z-index: 1999; ">
    <IPAM:TimePeriodSelector ID="TimePeriodSelector" runat="server"
        OnClientSave="$('#time-period').hide();"
        OnClientCancel="$('#time-period').hide();"
    />
  </div>
</div>

<div id="formbox">

    <div class="HeaderBar">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div style="width: auto">
                        <h1><asp:Label ID="ResourceTitleText" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_383 %>" runat="server" /></h1>
                        <h2><asp:Label ID="ResourceSubTitleText" Text="&nbsp;" runat="server" /></h2>
                    </div>
                </td>
                <td style="text-align: right">
                    <div style="width: auto">
                        <orion:LocalizableButtonLink runat="server" ID="ResourceHelpButton" Target="_blank" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_37 %>" DisplayType="Resource" />
                        <orion:LocalizableButtonLink runat="server" ID="ResourceEditButton" LocalizedText="Edit" DisplayType="Resource" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
        
    <table cellspacing="0" cellpadding="2" border="0"><tbody>

      <asp:Repeater ID="IPAssignedHistoryList" runat="server">
        <HeaderTemplate>
          <tr>
            <td width="5px" class="ReportHeader">&nbsp;</td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_387 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_388 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_389 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_390 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_391 %></td>
          </tr>        
        </HeaderTemplate>
        <ItemTemplate>
          <tr>
            <td class="Property">&nbsp;</td>
            <td class="Property" style="white-space: nowrap; text-align: center;">
                <%#HandleDateTimeValue("BeginDate")%><br />
                <%#HandleDateTimeValue("EndDate")%>
            </td>
            <td class="Property" style="white-space: nowrap;">
                <IPAM:IPAddressLink ValueTemplate="{2}" runat="server"
                    IPNode="<%#IPNodeFromAssignedHistory(Container.DataItem)%>" />
            </td>
            <td class="Property"><%#HandleEmptyValue(Eval("DNS"))%></td>
            <td class="Property"><%#HandleEmptyValue(Eval("MAC"))%></td>
            <td class="Property"><%#HandleEmptyValue(Eval("Source"))%></td>
          </tr>
        </ItemTemplate>
        <FooterTemplate />
      </asp:Repeater>

      <asp:Repeater ID="AssignedHistoryList" runat="server">
        <HeaderTemplate>
          <tr>
            <td colspan="2" width="5" class="ReportHeader">&nbsp;</td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_387 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_392 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_393 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_391 %></td>
          </tr>        
        </HeaderTemplate>
        <ItemTemplate>
          <tr>
            <td colspan="2" class="Property">&nbsp;</td>
            <td class="Property" style="white-space: nowrap; text-align: center;">
                <%#HandleDateTimeValue("BeginDate")%><br />
                <%#HandleDateTimeValue("EndDate")%>
            </td>
            <td class="Property"><%#HandleEmptyValue(Eval("IPAddress"))%></td>
            <td class="Property"><%#HandleEmptyValue(Eval("Subnet"))%></td>
            <td class="Property"><%#HandleEmptyValue(Eval("Source"))%></td>
          </tr>
        </ItemTemplate>
        <FooterTemplate />
      </asp:Repeater>

      <tr id="MsgMaxItems" runat="server" visible="false">
        <td class="Property">&nbsp;</td>
        <td class="Property" colspan="5" style="padding: 5px; color: Red;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_394 %>
        </td>
      </tr>

      <tr id="MsgNoItems" runat="server" visible="false">
        <td class="Property">&nbsp;</td>
        <td class="Property" colspan="5" style="height: 50px;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_395 %>
        </td>
      </tr>

      <tr>
        <td class="ReportHeader" colspan="6">
          <asp:Panel DefaultButton="btnSearch" runat="server">
       
            <table class="PageSearchContent" cellspacing="0" cellpadding="0"><tbody><tr>
                <td width="100%">&nbsp;</td>
                <td><img src="/Orion/images/form_prompt_divider.gif" height="22px" width="2px" /></td>
                <td>&nbsp;</td>
                <td><asp:TextBox ID="txtSearch" style="padding:2px;" runat="server"/></td>
                <td><asp:ImageButton runat="server" ID="btnSearch" OnClick="OnSearchClick" ImageUrl="/Orion/IPAM/res/images/sw-navigation/search-icon.gif" /></td>
            </tr></tbody></table>
            
          </asp:Panel>
        </td>
      </tr>
    </tbody></table>
    
    <asp:HiddenField runat="server" ID="hfSearchQuery" />
</div>

</asp:Content>