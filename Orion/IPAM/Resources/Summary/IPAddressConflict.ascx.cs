﻿using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressConflict_IPAddressConflict : BaseResourceControl
{
   
    protected ResourceSearchControl SearchControl { get; private set; }


    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/styles/MainLayout.css");
        this.AddStylesheet("/Orion/IPAM/res/css/sw-base.css");
        Wrapper.ShowEditButton = false;
        SearchControl = (ResourceSearchControl)LoadControl("~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx");
        Wrapper.HeaderButtons.Controls.Add(SearchControl);
        // Set unique ID CustomQueryTable instance that you put to .ascx file
        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display
        CustomTable.SWQL = SWQL;
        // Set search SWQL query that is used for searching
        CustomTable.SearchSWQL = SearchSWQL;
    }
   

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBDATA_MR1_45; }
    }
    public string SWQL
    {
        get
        {
            string swql = @"SELECT DISTINCT
                                IPAddressN, _IPAddress, _LinkFor_IPAddressN, ConflictTypeText, SubnetAddressN, _SubnetStatus, _SubnetAddressN1, ConflictTimeUTC,
                                AssignedMac, ConflictingMac, _IPStatus, _ConflictId, _ConflictTypeIcon, _AssignMACVendIcon, _ConflictMACVendIcon, _SubnetAddress, _SubnetId,
                                _AssignedRawMac, _ConflictingRawMac
                            FROM
                                (SELECT
                                    IPAddressN, c.IPAddress as [_IPAddress], '/Orion/NetPerfMon/NodeDetails.aspx?NetObject=IPAMN:'+ TOSTRING(IPNodeID) AS [_LinkFor_IPAddressN],
                                    ConflictTypeText, SubnetAddressN, SubnetStatus as [_SubnetStatus], '/Orion/IPAM/Subnets.aspx?opento='+ TOSTRING(SubnetId) AS [_SubnetAddressN1],
                                    ToLocal(ConflictTimeUTC) AS ConflictTimeUTC, AssignedMac, ConflictingMac, IPStatus as [_IPStatus], ConflictId as [_ConflictId],
                                    ConflictTypeIcon as [_ConflictTypeIcon], AssignMACVendIcon as [_AssignMACVendIcon], ConflictMACVendIcon as [_ConflictMACVendIcon],
                                    SubnetAddress as [_SubnetAddress], SubnetId as [_SubnetId], AssignedRawMac as [_AssignedRawMac], ConflictingRawMac as [_ConflictingRawMac]";
            if (UDTInstalled)
            {
                swql = @"SELECT
	                        IPAddressN, _IPAddress, _LinkFor_IPAddressN, ConflictTypeText, SubnetAddressN, _SubnetStatus, _SubnetAddressN1, ConflictTimeUTC, AssignedMac, ConflictingMac, _IPStatus, _ConflictId, _ConflictTypeIcon, 	_AssignMACVendIcon, _ConflictMACVendIcon, _SubnetAddress, _SubnetId, _AssignedRawMac, _ConflictingRawMac, 
	                        MAX(_AssMacNodeStatusIcon) as _AssMacNodeStatusIcon, MAX(_AssMacNodeAccessPoint) AS _AssMacNodeAccessPoint, MAX(_AssMacPortIcon) AS _AssMacPortIcon, MAX([_AssMacPortName]) AS _AssMacPortName, 
	                        MAX([_NodeStatusIcon]) AS _NodeStatusIcon , MAX([_NodeAccessPoint]) AS _NodeAccessPoint, MAX([_PortIcon]) AS _PortIcon, MAX([_PortName]) AS _PortName, MAX([_AssignedMacURL]) AS [_AssignedMacURL], 
	                        MAX([_ConflictingMacURL]) AS [_ConflictingMacURL], MAX([_AssMacPortURL]) AS [_AssMacPortURL], MAX([_ConfPortName]) AS [_ConfPortName], MAX([_AssMacNodeURL]) AS [_AssMacNodeURL], 
	                        MAX([_ConfMACNodeURL]) AS [_ConfMACNodeURL]
                        FROM
	                        (SELECT
		                        IPAddressN,c.IPAddress as [_IPAddress], '/Orion/NetPerfMon/NodeDetails.aspx?NetObject=IPAMN:'+ TOSTRING(IPNodeID) AS [_LinkFor_IPAddressN], ConflictTypeText,SubnetAddressN,
		                        SubnetStatus as [_SubnetStatus], '/Orion/IPAM/Subnets.aspx?opento='+ TOSTRING(SubnetId) AS [_SubnetAddressN1],ToLocal(ConflictTimeUTC) AS ConflictTimeUTC,AssignedMac,ConflictingMac,
		                        IPStatus as [_IPStatus], ConflictId as [_ConflictId],ConflictTypeIcon as [_ConflictTypeIcon],AssignMACVendIcon as [_AssignMACVendIcon], ConflictMACVendIcon as [_ConflictMACVendIcon],
		                        SubnetAddress as [_SubnetAddress],SubnetId as [_SubnetId],AssignedRawMac as [_AssignedRawMac], ConflictingRawMac as [_ConflictingRawMac] ,
		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[NodeStatusIcon] ELSE NULL END AS [_AssMacNodeStatusIcon],
		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[NodeAccessPoint] ELSE NULL END AS [_AssMacNodeAccessPoint], 
		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[PortIcon] ELSE NULL END AS [_AssMacPortIcon],
		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[PortName] ELSE NULL END AS [_AssMacPortName], 
		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[NodeStatusIcon] ELSE NULL END AS [_NodeStatusIcon], 
		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[NodeAccessPoint] ELSE NULL END AS [_NodeAccessPoint], 
		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[PortIcon] ELSE NULL END AS [_PortIcon], 
		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[PortName] ELSE NULL END AS [_PortName],

		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[MacUrl] ELSE NULL END AS [_AssignedMacURL],
		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[MacUrl] ELSE NULL END AS[_ConflictingMacURL], 

		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN [am].[PortUrl] ELSE NULL END AS [_AssMacPortURL], 

		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN [am].[PortUrl] ELSE NULL END AS [_ConfPortName],
                
		                        CASE WHEN [am].[rawMAC] = [c].[AssignedRawMac] THEN (CASE WHEN ([am].[IsWireless] = 1) THEN ('/Orion/UDT/AccessPointDetails.aspx?NetObject=UW-AP:' + TOSTRING([am].[NodeID])) 
			                        ELSE ('/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + TOSTRING([am].[NodeID])) END)
			                        ELSE NULL END AS [_AssMacNodeURL], 

		                        CASE WHEN [am].[rawMAC] = [c].[ConflictingRawMac] THEN 
			                        (CASE WHEN ([am].[IsWireless] = 1) THEN ('/Orion/UDT/AccessPointDetails.aspx?NetObject=UW-AP:' + TOSTRING([am].[NodeID])) 
                                        ELSE ('/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:' + TOSTRING([am].[NodeID])) END) 
			                        ELSE NULL END AS [_ConfMACNodeURL] 

                        FROM 
	                        IPAM.Conflict c 
                        LEFT JOIN 
	                        Orion.UDT.Endpoint AS E1 ON ([E1].[MacAddress] IN (c.[AssignedRawMac], c.[ConflictingRawMac]) )
                        LEFT JOIN 
	                        Orion.UDT.MACCurrentInformation AS am ON ([E1].[EndPointId] = am.[EndPointId])
                        WHERE 
	                        ConflictStatus = 1 )
                        GROUP BY 
	                        IPAddressN, _IPAddress, _LinkFor_IPAddressN, ConflictTypeText, SubnetAddressN, _SubnetStatus, _SubnetAddressN1, ConflictTimeUTC, AssignedMac, ConflictingMac, _IPStatus, _ConflictId, _ConflictTypeIcon, 
	                        _AssignMACVendIcon, _ConflictMACVendIcon, _SubnetAddress, _SubnetId, _AssignedRawMac, _ConflictingRawMac
                        ";
            }
            else
            {
                swql = swql + " FROM IPAM.Conflict c where ConflictStatus = 1)";
            }
         

            return swql;
        }
    }
    public bool UDTInstalled
    {
        get
        {
            return OrionModuleManager.IsInstalled("UDT") && OrionModuleManager.IsModuleVersionAtLeast("UDT", "3.2");           
        }
    }
    private string SearchSWQL
    {
        get
        {
            string swql = @"SELECT DISTINCT
                                IPAddressN, _IPAddress, _LinkFor_IPAddressN, ConflictTypeText, SubnetAddressN, _SubnetStatus, _SubnetAddressN1, ConflictTimeUTC,
                                AssignedMac, ConflictingMac, _IPStatus, _ConflictId, _ConflictTypeIcon, _AssignMACVendIcon, _ConflictMACVendIcon, _SubnetAddress, _SubnetId,
                                _AssignedRawMac, _ConflictingRawMac
                            FROM
                                (SELECT
                                    IPAddressN,c.IPAddress as [_IPAddress], '/Orion/NetPerfMon/NodeDetails.aspx?NetObject=IPAMN:'+ TOSTRING(IPNodeID) AS [_LinkFor_IPAddressN],
                                    ConflictTypeText, SubnetAddressN, SubnetStatus as [_SubnetStatus], '/Orion/IPAM/Subnets.aspx?opento='+ TOSTRING(SubnetId) AS [_SubnetAddressN1],
                                    ToLocal(ConflictTimeUTC) AS ConflictTimeUTC, AssignedMac, ConflictingMac, IPStatus as [_IPStatus], ConflictId as [_ConflictId],
                                    ConflictTypeIcon as [_ConflictTypeIcon], AssignMACVendIcon as [_AssignMACVendIcon], ConflictMACVendIcon as [_ConflictMACVendIcon],
                                    SubnetAddress as [_SubnetAddress],SubnetId as [_SubnetId], AssignedRawMac as [_AssignedRawMac], ConflictingRawMac as [_ConflictingRawMac]";
            if (UDTInstalled)
            {
                swql = @"SELECT
                            IPAddressN,c.IPAddress as [_IPAddress], '/Orion/NetPerfMon/NodeDetails.aspx?NetObject=IPAMN:'+ TOSTRING(IPNodeID) AS [_LinkFor_IPAddressN],
                            ConflictTypeText, SubnetAddressN, SubnetStatus as [_SubnetStatus], '/Orion/IPAM/Subnets.aspx?opento='+ TOSTRING(SubnetId) AS [_SubnetAddressN1],
                            ToLocal(ConflictTimeUTC) AS ConflictTimeUTC, AssignedMac, ConflictingMac, IPStatus as [_IPStatus], ConflictId as [_ConflictId],
                            ConflictTypeIcon as [_ConflictTypeIcon], AssignMACVendIcon as [_AssignMACVendIcon], ConflictMACVendIcon as [_ConflictMACVendIcon],
                            SubnetAddress as [_SubnetAddress],SubnetId as [_SubnetId], AssignedRawMac as [_AssignedRawMac], ConflictingRawMac as [_ConflictingRawMac],
                            am.NodeStatusIcon as [_AssMacNodeStatusIcon], am.NodeAccessPoint as [_AssMacNodeAccessPoint] , am.PortIcon as [_AssMacPortIcon],
                            am.PortName as [_AssMacPortName], cm.NodeStatusIcon as [_NodeStatusIcon], cm.NodeAccessPoint as [_NodeAccessPoint], cm.PortIcon as [_PortIcon],
                            cm.PortName as [_PortName], am.macurl AS[_AssignedMacURL], cm.macurl as [_ConflictingMacURL], am.porturl as [_AssMacPortURL],
                            cm.porturl as [_ConfPortName], 
                            CASE
                                WHEN am.IsWireless = 1
                                THEN am.APUrl
                                ELSE am.Node.DetailsUrl
                                END
                            AS [_AssMacNodeURL],
                            CASE 
                                WHEN cm.IsWireless = 1
                                THEN cm.APUrl
                                ELSE cm.Node.DetailsUrl
                                END
                            AS [_ConfMACNodeURL]
                        FROM 
                            IPAM.Conflict c
                        LEFT JOIN
                            Orion.UDT.MACCurrentInformation am
                        ON (c.AssignedRawMac = am.rawMAC)
                        LEFT JOIN Orion.UDT.MACCurrentInformation cm
                        ON (c.ConflictingRawMac = cm.rawMAC)
                        WHERE
                            (c.IPAddress LIKE '%${SEARCH_STRING}%'
                            OR SubnetAddress LIKE '%${SEARCH_STRING}%'
                            OR ConflictTypeText LIKE '%${SEARCH_STRING}%'
                            OR AssignedMac LIKE '%${SEARCH_STRING}%'
                            OR ConflictingMac LIKE '%${SEARCH_STRING}%'
                            OR ConflictTimeUTC LIKE '%${SEARCH_STRING}%'
                            OR AssignedRawMac LIKE '%${SEARCH_STRING}%'
                            OR ConflictingRawMac LIKE '%${SEARCH_STRING}%'
                            OR am.NodeAccessPoint LIKE '%${SEARCH_STRING}%'
                            OR am.PortName LIKE '%${SEARCH_STRING}%'
                            OR cm.NodeAccessPoint LIKE '%${SEARCH_STRING}%'
                            OR cm.PortName LIKE '%${SEARCH_STRING}%')
                            AND ConflictStatus = 1";
            }
            else
            {
                swql = swql + @"FROM
                                    IPAM.Conflict c
                                WHERE
                                    (IPAddress LIKE '%${SEARCH_STRING}%'
                                    OR SubnetAddress LIKE '%${SEARCH_STRING}%'
                                    OR ConflictTypeText LIKE '%${SEARCH_STRING}%'
                                    OR AssignedMac LIKE '%${SEARCH_STRING}%'
                                    OR ConflictingMac LIKE '%${SEARCH_STRING}%'
                                    OR ConflictTimeUTC LIKE '%${SEARCH_STRING}%'
                                    OR AssignedRawMac LIKE '%${SEARCH_STRING}%'
                                    OR ConflictingRawMac LIKE '%${SEARCH_STRING}%')
                                    AND ConflictStatus = 1)";
            }

            return swql;
        }
    }
   
    public int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }
    public override string HelpLinkFragment
    {
        get { return "OrionIPAMConflictSummary"; }
    }
   
    
}