﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressConflict.ascx.cs" Inherits="Orion_IPAM_Resources_IPAddressConflict_IPAddressConflict" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx"  %>
<%@ Reference Control="~/Orion/NetPerfMon/Controls/ResourceSearchControl.ascx" %>
<orion:resourceWrapper runat="server" ID="Wrapper">
<Content>
<orion:CustomQueryTable runat="server" ID="CustomTable"/>
    <style type="text/css">
            .timeConflict {
                width:65px;
            }
        </style>
<script type="text/javascript">
    
   
    var UDTInfoAvailable = false;
    var AssMacNodeName = "";    
    var AssMacPortName = "";
    var ConfMacNodeName ="";
    var ConfMacPortName ="";
    var previousIPAddressN="";   
    var statusIcoMap = { '0': '', '1': 'ip-used', '2': 'ip-avail', '4': 'ip-availrsvd', '5': 'ip-usedrsvd', '8': 'ip-transient', 'history': 'ip-history' };
    var CreateToolTip1 =function(currselector) {
        $(currselector).tooltip({
            delay: 50,
            showURL: false,
            extraClass: "Tooltip1", 
            top: -15, 
            left: 5, 
            bodyHandler: function() { 
                var localval =$(currselector).attr('ConflictTypeText');
                var toolTipBody ="<div class='custom-x-tip-bwrap Tooltip1Body'><b>Conflict Type:</b> "+localval+"</div>";   
                return  toolTipBody ;
            }
        });
    };
    var UDTInstalled = '<%=UDTInstalled %>';  
  
    $(document).ready(function () {

        var monthTxtMap = { '0': 'Jan', '1': 'Feb', '2': 'Mar', '3': 'Apr', '4': 'May', '5': 'Jun', '6': 'Jul', '7': 'Aug', '8': 'Sep' ,'9': 'Oct','10': 'Nov','11': 'Dec' };

        SW.Core.Resources.CustomQuery.initialize(
            {
                // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                uniqueId: <%= ScriptFriendlyResourceID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        // How many rows there can be on one page. If there are more rows, paging toolbar is displayed.
                        rowsPerPage: 5,                        
                // If you want a search feature, this must be set to client ID of search textbox
                searchTextBoxId: '<%= SearchControl.SearchBoxClientID %>',
                // If you want a search feature, this must be set to client ID of search button
                searchButtonId: '<%= SearchControl.SearchButtonClientID %>',
                // True to allow sorting of rows by clicking on column headers
                allowSort: true,
                allowSearch: true,
                        columnSettings: {                           
                            "IPAddressN": {
                                header: 'IP Address',
                                allowSort:true,
                                formatter: function (value, row, cellinfo) {                               
                                    var IPAddressIcon = "<img class='"+statusIcoMap[row[10]]+" ConflictImg' src='/Orion/IPAM/res/images/default/s.gif'/>";                                                                          
                                    var renderInfo =IPAddressIcon + "<span class='ImageCell'>" + row[1] + "</span>";                               
                                    if(UDTInstalled == 'True')
                                    {
                                        AssMacNodeName = row[20];
                                        AssMacPortName = row[22];
                                        ConfMacNodeName = row[24];
                                        ConfMacPortName = row[26];                                        
                                        if((AssMacNodeName != null && AssMacPortName !=null) ||( ConfMacNodeName !=null && ConfMacPortName !=null))
                                        {
                                            var UDTInfo = "<br/> <br/><div> </div> <br/> <br/><div></div>";
                                            renderInfo =renderInfo + UDTInfo;
                                        }

                                    }                                    
                                    return renderInfo;
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol';
                                },
                                isHtml: true  
                            },
                            "ConflictTypeText":{
                                header: 'Type',                                
                                isHtml: true,
                                formatter: function (value, row, cellinfo) {
                                    var subnetIcon = "<img style='padding-left:5px' class='ConflictImg' id="+row[0]+" ConflictTypeText ='"+value+"' onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\"  src='/Orion/IPAM/res/images/sw/"+row[12]+"'/>";
                                    var  selector='img[id='+row[0]+']';
                                    
                                    CreateToolTip1(previousIPAddressN);
                                    
                                    previousIPAddressN = selector;
                                 
                                    
                                    var renderInfo = subnetIcon;
                                    if(UDTInstalled == 'True')
                                    {
                                        var UDTInfo = "<br/> <br/><div> </div> <br/> <br/><div></div>";
                                        renderInfo = renderInfo + UDTInfo;
                                    }
                                    return renderInfo;
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol';
                                },
                                allowSort:true
                            },
                            "SubnetAddressN":{
                                header: 'Subnet',
                                isHtml: true,
                                formatter: function (value, row, cellinfo) { 
                                    var subnetIcon = "<img class='subnet-"+row[5]+" ConflictImg' src='/Orion/IPAM/res/images/default/s.gif'/>";
                                    var subnetAddress = "<a href='/Orion/IPAM/Subnets.aspx?opento="+row[16]+"'>"+row[15]+"</a>";
                                    var renderInfo = subnetIcon + "<span class='ImageCell ConflictMACCol'>" + subnetAddress + "</span>";
                                    if(UDTInstalled == 'True')
                                    {
                                        var UDTInfo = "<div class= 'UDTCol'>UDT Node / Access Point:</div> <div class= 'UDTCol'>UDT Node Port / SSID :</div>";
                                        renderInfo = renderInfo + UDTInfo;
                                    }                                    
                                    return renderInfo;
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol';
                                },
                                allowSort:true
                            },
                            "ConflictTimeUTC":{
                                header: 'Time of <br/> Conflict',
                                formatter: function (value, row, cellinfo) {
                                    var dt = new Date(value);                                 
                                    if (dt instanceof Date && isNaN(dt.valueOf())) {
                                        dt = row[7];
                                    }
                                    var dateFormat = dt.toLocaleDateString();
                                    var ConfTime = dateFormat + "<br/>" + dt.toLocaleTimeString();

                                    var renderInfo = "<span class='ConflictMACCol'>"+ ConfTime+ "</span>";
                                    if(UDTInstalled == 'True')
                                    {                                      
                                        var UDTInfo = "<div class= 'UDTCol'></div> <div class= 'UDTCol'></div>";
                                        renderInfo = renderInfo +UDTInfo;
                                    }
                                    return renderInfo;
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol timeConflict';
                                },
                                allowSort:true,
                                isHtml: true
                            },
                            "AssignedMac":{
                                header: 'Assigned MAC',  
                                formatter: function (value, row, cellinfo) {                                                             
                                    var MACIcon = "<img style=\"vertical-align: middle;\" onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\" src='/NetPerfMon/images/Vendors/"+row[13]+"'/>";
                                    var AssMac =value;
                                    if(row[27] != null)
                                    {
                                        AssMac ="<a href='"+row[27]+"'>"+value+"</a>";
                                    }                                   
                                    if(row[29] !=null)
                                    {
                                        AssMacPortName = "<a href='"+row[29]+"'>"+AssMacPortName+"</a>";
                                    }
                                    if(row[31] !=null)
                                    {
                                        AssMacNodeName = "<a href='"+row[31]+"'>"+AssMacNodeName+"</a>";
                                    }

                                    var renderInfo = "<span class='ConflictMACCol'>" + MACIcon + "<span class='ImageCell'>" + AssMac + "</span> </span>";
                                   
                                    if(UDTInstalled == 'True')
                                    {
                                        var NodeIcon = "<img class='ConflictImg' onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\"   src='"+row[19]+"'/> ";
                                        var PortIcon = "<img class='ConflictImg' onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\"   src='"+row[21]+"'/> ";
                                        var UDTInfo ="";
                                        if (AssMacNodeName == null) {
                                            AssMacNodeName = "N/A";
                                            UDTInfo  = "<div class= 'UDTCol'>N/A</div> <div class= 'UDTCol'>N/A</div>";
                                        }
                                        else
                                        {   
                                            UDTInfo = "<div class= 'UDTCol'>"+NodeIcon + AssMacNodeName+"</div> <div class= 'UDTCol'>"+PortIcon + AssMacPortName+ "</div>";
                                        }
                                        
                                        renderInfo = renderInfo + UDTInfo;
                                    }
                                    return renderInfo;                                    
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol';
                                },
                                isHtml: true,
                                allowSort:true
                            },
                            "ConflictingMac":{
                                header: 'Conflicting MAC',
                                formatter: function (value, row, cellinfo) {                                    
                                    var MACIcon = "<img style=\"vertical-align: middle;\" onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\" src='/NetPerfMon/images/Vendors/"+row[14]+"'/>";
                                    var ConfMac =value;
                                    if(row[28] != null)
                                    {
                                        ConfMac ="<a href='"+row[28]+"'>"+value+"</a>";
                                    }
                                    if(row[30] !=null)
                                    {
                                        ConfMacPortName = "<a href='"+row[30]+"'>"+ConfMacPortName+"</a>";
                                    }
                                    if(row[32] !=null)
                                    {
                                        ConfMacNodeName = "<a href='"+row[32]+"'>"+ConfMacNodeName+"</a>";
                                    }
                                    
                                        
                                    var renderInfo = "<span class='ConflictMACCol'>" + MACIcon + "<span class='ImageCell'>" + ConfMac + "</span> </span>";                            
                                    if(UDTInstalled == 'True')
                                    {
                                       
                                        var NodeIcon = "<img class='ConflictImg' onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\"  src='"+row[23]+"'/> ";
                                        var PortIcon = "<img class='ConflictImg' onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\"  src='"+row[25]+"'/> ";
                                        var UDTInfo ="";
                                        if (ConfMacNodeName == null) {
                                            ConfMacNodeName = "N/A";
                                            UDTInfo  = "<div class= 'UDTCol'>N/A</div> <div class= 'UDTCol'>N/A</div>";
                                        }
                                        else
                                        {
                                            UDTInfo = "<div class= 'UDTCol'>"+NodeIcon + ConfMacNodeName+"</div> <div class= 'UDTCol'>"+PortIcon + ConfMacPortName+ "</div>";
                                        }
                                        renderInfo = renderInfo + UDTInfo;
                                    }
                                    return renderInfo;
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'ConflictCol';
                                },
                                isHtml: true,
                                allowSort:true
                            }
                        }
                    });

                $('#OrderBy-' + "<%= ScriptFriendlyResourceID %>").val('[ConflictTimeUTC] DESC');
        
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
    });

    //Initialize Tooltip for last row in resource.      

    var grid = $('#Grid-' + '<%= ScriptFriendlyResourceID %>');
    $(grid).live( "mouseover",function( event ) {
        CreateToolTip1(previousIPAddressN);
    });
</script>
    </Content>
</orion:resourceWrapper>