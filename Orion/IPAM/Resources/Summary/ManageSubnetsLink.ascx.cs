using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Logging;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_IPAM_Resources_Summary_ManageSubnetsLink : BaseResourceControl
{
     private static readonly Log log = new Log();
    public bool isFreeTool = false;
    public bool isOtherModuleInstalled = false;
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);
    }
    protected override string DefaultTitle
	{
		get { return Resources.IPAMWebContent.IPAMWEBCODE_AK1_15; }
	}

	public override string HelpLinkFragment
	{
        get
        {
            return string.Empty;
        }
	}

    public override string EditURL
    {
        get { return string.Empty; }
    }

    public override string SubTitle
    {
        get { return string.Empty; }
    }

    protected void OnRemoveResourceButtonClicked(Object sender, EventArgs e)
    {
        ResourceManager.DeleteById(Resource.ID);
        Response.Redirect(Request.Url.ToString());
    }
    
}
