<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManageSubnetsLink.ascx.cs"Inherits="Orion_IPAM_Resources_Summary_ManageSubnetsLink" %>
<orion:resourcewrapper runat="server" id="Wrapper" showeditbutton="False" showmanagebutton="True" cssclass="ResourceWrapper">
    <HeaderButtons>
        <orion:LocalizableButton ID="btnRemoveMe" CssClass="EditResourceButton" runat="server"
            OnClick="OnRemoveResourceButtonClicked" DisplayType="Resource" 
            Text='REMOVE RESOURCE' />
    </HeaderButtons>
    <Content>
         <%--Commenting out IPAT  Changes--%>
      <%--   <script type="text/javascript">
             var navigateButtons = [];
             navigateButtons.push($('#' + '<%= dhcpServerUrlId.ClientID %>'));
             navigateButtons.push($('#' + '<%= dnsServerUrlId.ClientID %>'));
             $.each(navigateButtons,function(idx, btn) {
                 $(btn).on("click", function (event) {
                     if ('<%= this.isFreeTool %>' == "True" && '<%= this.isOtherModuleInstalled %>' == "False") {
                         alert('<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_45 %>');
                          return false;
                      }
                  });
             })
         </script>--%>

        <style type="text/css">

.Ipam-gettingstarted-innertable {
    background-color: #FFFFFF;
}

.Ipam-gettingstarted-innertable td {
    padding: 15px 5px 10px 10px;
}

.Ipam-gettingstarted-innertable td.icon {
    width: 40px;
    padding-right: 0px;
    padding-left: 15px;
}
.Ipam-gettingstarted-innertable td.button a {
    width: 85%;
	text-align: center;
}
.Ipam-gettingstarted-innertable .title {
    font-weight: bold;
    font-size: 14px;
}
.Ipam-text-helpful {
    font-size: 12px;
    color: #646464;
}

.Ipam-gettingstarted-innertable span.sw-btn-c {
    min-width: 100px;
    text-align: center;
}
</style>
        <asp:HiddenField ID="modules" runat="server" />
        <table class="Ipam-gettingstarted-innertable" cellpadding="0" cellspacing="0">
     <tr>
                <td class="icon">
                    <img src="/Orion/IPAM/res/images/sw-resources/ip-icon.png" alt="" />
                </td>
                <td>
                    <div class="title">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_19%></div>
                    <div class="Ipam-text-helpful">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_20%>
                    </div>
                </td>
         
                <td class="button">
                    <orion:LocalizableButtonLink ID="addSubnetUrlId" runat="server"  NavigateUrl="~/Orion/IPAM/AddSubnetIPAddresses.aspx"   Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_DF1_28 %>" DisplayType="Primary" />
                </td>
        
            </tr>
             <tr>
                <td class="icon">
                    <img src="/Orion/IPAM/res/images/sw-resources/server-icon.png" alt="" />
                </td>
                <td>
                    <div class="title">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_18%></div>
                    <div class="Ipam-text-helpful">
                         <%= Resources.IPAMWebContent.IPAMWEBDATA_VN0_20%>
                    </div>
                </td>
                
                <td class="button">
                    <orion:LocalizableButtonLink ID="dhcpServerUrlId" NavigateUrl="~/Orion/IPAM/Dhcp.Server.Add.aspx"  runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_DF1_29 %>" DisplayType="Primary" />
                </td>
            </tr>
            <tr>
                <td class="icon">
                    <img src="/Orion/IPAM/res/images/sw-resources/server-icon.png" alt="" />
                </td>
                <td>
                    <div class="title">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_17%></div>
                    <div class="Ipam-text-helpful">
                         <%= Resources.IPAMWebContent.IPAMWEBDATA_VN0_4%>
                    </div>
                </td>
                <td class="button">
                    <orion:LocalizableButtonLink ID="dnsServerUrlId" runat="server"   NavigateUrl="~/Orion/IPAM/Dns.Server.Add.aspx"   Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_DF1_30 %>" DisplayType="Primary" />
                </td>
            </tr>
         </table>
     
    </Content>
</orion:resourcewrapper>
