<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastIPAMEvents.ascx.cs" Inherits="Orion_IPAM_Resources_TopXX_LastIPAMEvents" %>
<%@ Register Src="~/Orion/IPAM/Controls/IPAMEventList.ascx" TagName="EventList" TagPrefix="ipam" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	<ipam:EventList runat="server" ID="IPAMEventList" >
	</ipam:EventList>
	</Content>
</orion:resourceWrapper>

