using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_IPAM_Resources_TopXXSubnetsByUsage : BaseResourceControl
{
	private static readonly Log _log = new Log();
	private const string identifierPrefix = "Subnet";

    protected override void OnLoad(EventArgs e)
    {
		base.OnLoad(e);
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");
		
		if (string.IsNullOrEmpty(Resource.Title))
			Resource.Title = DefaultTitle;
		
		Resource.Title = Resource.Title.Replace("XX", HowManySubnets.ToString());

    	List<GroupNode> src = GetTopXXSubnets();
		if (src != null)
		{
			SubnetsGrid.DataSource = src;
			SubnetsGrid.DataBind();
		}
    }

	protected override string DefaultTitle
	{
		get { return Resources.IPAMWebContent.IPAMWEBCODE_AK1_12; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionIPAMPHResourceTopSubnetsbyIPAddressSpaceUsage"; }
	}

	public override string EditURL
	{
		get { return GetEditUrl(this.Resource); }
	}

    public override string SubTitle
    {
        get { return string.Empty; }
	}

	public int HowManySubnets
	{
		get
		{
			return GetIntProperty("HowManySubnetsByIpSpaceUsage", 10);
		}
	}

	public string SQLFilter
	{
		get
		{
			return GetStringValue("Filter", string.Empty);
		}
	}

	#region private members

    private List<GroupNode> GetTopXXSubnets()
    {
        string whereclause = identifierPrefix + "." + GroupNode.EIM_GROUPTYPE + " = " +
                             (int)GroupNodeType.Subnet + "{0}" +
                             " AND " + identifierPrefix + ".Distance = 0" +
                             " ORDER BY " + identifierPrefix + "." + GroupNode.EIM_PERCENTUSED + " DESC";

        string filter = SQLFilterHelper.SQLFilterTransformer(SQLFilter);

        whereclause = string.Format(whereclause,
                                    string.IsNullOrEmpty(filter) ? string.Empty : " AND " +  WebSecurityHelper.SanitizeHtml(filter));

        try
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                return proxy.AppProxy.GroupNode.Get(HowManySubnets, whereclause, identifierPrefix);
            }
        }
        catch (Exception ex)
        {
            this.ResourceWrapper1.Content.Controls.Clear();
            string msg;
            if (!ExceptionHandlerSWIS.NormalizeException(ex, out msg) && !string.IsNullOrEmpty(filter))
                msg = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_26,  WebSecurityHelper.SanitizeHtml(SQLFilter));

            this.ResourceWrapper1.Content.Controls.Add(new VisualizeException(msg));
            return null;
        }
    }

	private string GetEditUrl(ResourceInfo resource)
	{
		return string.Format("/Orion/IPAM/Resources/EditTopSubnetsByUsage.aspx?ResourceID={0}", resource.ID);
	}

	#endregion

}
