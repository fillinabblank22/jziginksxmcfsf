<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopScopesByUsage.ascx.cs" Inherits="Orion_IPAM_Resources_TopXXScopesByUsage" %>
<%@ Register TagPrefix="ipam" TagName="IpUsedBar" Src="~/Orion/IPAM/Controls/IpUsedBar.ascx" %>

<orion:ResourceWrapper ID="ResourceWrapper1" runat="server">
<Content>
    <asp:Repeater runat="server" ID="ScopesGrid">
        <HeaderTemplate>
            <table cellspacing="0" cellpadding="3px" class="NeedsZebraStripes">
                <thead>
                    <tr>
                        <td><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_403 %></td>
                        <td width="140" colspan="2"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_400 %></td>
                        <td width="60"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_401 %></td>
                        <td width="60"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_402 %></td>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td>
                    <a href="/api2/ipam/ui/scope?name=<%# HttpUtility.UrlEncode(Convert.ToString(Eval("FriendlyName"))) %>">
                        <img title="<%# Convert.ToString(Eval("StatusShortDescription")) %>" alt="<%# Convert.ToString(Eval("StatusShortDescription")) %>" src="/Orion/IPAM/res/images/sw/icon.<%# Convert.ToString(Eval("GroupIconPrefix")) %>.<%# Convert.ToString(Eval("StatusIconPostFix")) %>.gif" />
                        <%# HttpUtility.HtmlEncode( Convert.ToString( Eval("FriendlyName") ) ) %></a>
                    </td>
                    <td width="90"><ipam:IpUsedBar ClassName='<%# Convert.ToString(Eval("StatusIconPostfix")) %>' Value='<%#Convert.ToDouble(Eval("PercentUsed"))%>' runat="server" /></td>
                    <td width="50"><%# Convert.ToDouble(Eval("PercentUsed") ?? 0).ToString("N2") %>%</td>
                    <td width="60"><%# Eval("AvailableCount")??0 %></td>
                    <td width="60"><%# Eval("UsedCount")??0 %></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</Content>
</orion:ResourceWrapper>
