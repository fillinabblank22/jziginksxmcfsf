﻿using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Helpers;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;
using System;
using System.Net;
using System.Web;
using System.Web.Services;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_IPAM_Resources_TopScopesByUtilization : BaseResourceControl
{
    string _swql = @"SELECT FriendlyName,PercentUsed, ScopeUtil, SubnetInfo,AvailableCount AS [_AvailableCount], UsedCount AS [_UsedCount],SplitCount AS [_SplitCount],Address AS [_Address],ServerName as [_ServerName],ScopeIPRanges,AllocSize as [_AllocSize],ServerType as [_ServerType],ScopeStatus as[_ScopeStatus],ServerStatus as [_ServerStatus],ScopeRanges as [_ScopeRanges],CIDR as[_CIDR],OfferDelay as[_OfferDelay],TotalCount as [_TotalCount],SplitScopeCount as [_SplitScopeCount] ,ViewType as [_ViewType],Groupid as [_Groupid],TotalPercentUsed as [_TotalPercentUsed],AvgPercentUsed as [_AvgPercentUsed],TotalAvailableIPs as [_TotalAvailableIPs],ReservedCount AS [_ReservedCount],TransientCount AS [_TransientCount] FROM (SELECT TOP {0} FriendlyName,PercentUsed, ScopeUtil, SubnetInfo,AvailableCount, UsedCount ,SplitCount ,Address ,ServerName ,ScopeIPRanges,AllocSize ,ServerType ,ScopeStatus ,ServerStatus ,ScopeRanges ,CIDR ,OfferDelay ,TotalCount ,SplitScopeCount ,ViewType,Groupid,TotalPercentUsed,AvgPercentUsed,TotalAvailableIPs,ReservedCount,TransientCount FROM IPAM.TopUtilDHCPScopes scope WHERE ViewType = 1 ORDER BY AvgPercentUsed desc) s ORDER BY AvgPercentUsed desc";
   
    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");
        this.AddStylesheet("/Orion/IPAM/res/css/sw-base.css");
        // Set unique ID CustomQueryTable instance that you put to .ascx file

        CustomTable.UniqueClientID = ScriptFriendlyResourceID;
        // Set SWQL query that loads data to display

        var filter = "";
        try
        {
            filter = SQLFilterHelper.SQLFilterTransformer(SQLFilter);
            SWQL = string.Format(_swql, HowManyScopes.ToString());
            _swql = _swql.Replace("ViewType = 1", "ViewType = 1 {0}");
            _swql = _swql.Replace("AvgPercentUsed desc", SortBy);
            SWQL = string.Format(_swql,string.IsNullOrEmpty(filter) ? string.Empty : " AND " + WebSecurityHelper.SanitizeHtml(filter));
            CustomTable.SWQL = SWQL;
            Resource.Title = Resource.Title.Replace("XX", HowManyScopes.ToString());
        }
         catch (Exception ex)
        {
            this.Wrapper.Content.Controls.Clear();
            string msg;
            if (!ExceptionHandlerSWIS.NormalizeException(ex, out msg) && !string.IsNullOrEmpty(filter))
                msg = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_26,  WebSecurityHelper.SanitizeHtml(SQLFilter));

            this.Wrapper.Content.Controls.Add(new VisualizeException(msg));           
        }


    } 

    protected string SWQL
    {
        get
        {
            return _swql;
        }
        set
        {
            _swql = value;
        }
    }
    public override string SubTitle
    {
        get { return  SortByTitle; }
    }
   
    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBDATA_MR1_39; }
    }
    public string SQLFilter
    {
        get
        {
            return GetStringValue("UtilizationFilter", string.Empty);
        }
    }
    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    public override string HelpLinkFragment
    {
        get { return "orionipamtopscopespercentused"; }
    }
    public override string EditURL
    {
        get { return GetEditUrl(this.Resource); }
    }
    public int HowManyScopes
    {
        get
        {
            return GetIntProperty("TotalScopes", 10);
        }
    }
    private string SortBy
    {
        get {
            int value = 0;
            if(Resource.Properties["SortBy"] == null)
            {
                value = 1;
            }
            else{
             int.TryParse(Resource.Properties["SortBy"] ,out value);
            }
            switch (value)
            {
                case 0:
                    return "PercentUsed desc";
                case 1:
                    return "AvgPercentUsed desc";
                case 2:
                    return "TotalAvailableIPs asc";
                default:
                    return "AvgPercentUsed desc";
            }
        }
    }
    private string SortByTitle
    {
        get
        {
            int value = 0;
            if (Resource.Properties["SortBy"] == null)
            {
                value = 1;
            }
            else
            {
                int.TryParse(Resource.Properties["SortBy"], out value);
            }
            switch (value)
            {
                case 0:
                    return Resources.IPAMWebContent.IPAMWEBDATA_MR1_41;
                case 1:
                    return Resources.IPAMWebContent.IPAMWEBDATA_MR1_42;
                case 2:
                    return Resources.IPAMWebContent.IPAMWEBDATA_MR1_43;
                default:
                    return Resources.IPAMWebContent.IPAMWEBDATA_MR1_42;
            }
        }
    }
    private string GetEditUrl(ResourceInfo resource)
    {
        return string.Format("/Orion/IPAM/Resources/EditTopScopesByUtilization.aspx?ResourceID={0}", resource.ID);
    }

   
}