using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Web.Helpers;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_IPAM_Resources_TopXX_LastIPAMEvents : BaseResourceControl
{

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);

		if (string.IsNullOrEmpty(Resource.Title))
			Resource.Title = DefaultTitle;

		Resource.Title = Resource.Title.Replace("XX", HowManyEvents.ToString());

        DataTable events = GetLastXXIPAMEvents();
        DataColumn colEventTime = events.Columns["EventTime"];
        DataColumn colMsg = events.Columns["Message"];

        foreach (DataRow row in events.Rows)
        {
            // To Convert the Event date to user selected time zone value.
            row[colEventTime] = row.Field<DateTime>(colEventTime).ConvertToDisplayDate();

            //To prevent Stored-XSS attack IPAM-806 
            row[colMsg] = WebSecurityHelper.SanitizeHtml((string)row.Field<string>(colMsg));
        }

        IPAMEventList.DataSource = events;
		IPAMEventList.DataBind();
	}

	protected override string DefaultTitle
	{
		get { return Resources.IPAMWebContent.IPAMWEBCODE_AK1_14; }
	}

	public override string HelpLinkFragment
	{
		get { return "OrionIPAMPHResourceLastXXEvents"; }
	}

	public override string EditURL
	{
		get { return GetEditUrl(this.Resource); }
	}

	public override string SubTitle
	{
		get { return string.Empty; }
	}

	public int HowManyEvents
	{
		get
		{
			return GetIntProperty("HowManyIPAMEvents", 25);
		}
	}

	#region private members

	private DataTable GetLastXXIPAMEvents()
	{
		return OrionEventHelper.GetLastXXIPAMEvents(HowManyEvents);
	}

	private string GetEditUrl(ResourceInfo resource)
	{
		return string.Format("/Orion/IPAM/Resources/EditLastIPAMEvents.aspx?ResourceID={0}", resource.ID);
	}

	#endregion
}
