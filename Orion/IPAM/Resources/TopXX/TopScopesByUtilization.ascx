﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopScopesByUtilization.ascx.cs" Inherits="Orion_IPAM_Resources_TopScopesByUtilization" %>
<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx"  %>
<%@ Register TagPrefix="ipam" TagName="IpUsedBar" Src="~/Orion/IPAM/Controls/IpUsedBar.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    
    <Content>
        
        <!-- Include CustomQueryTable control in your resource -->
        <orion:CustomQueryTable runat="server" ID="CustomTable"/>            
  <script type="text/css">
     .tooltip{
	margin:8px;
	padding:8px;
	border:1px solid blue;
	background-color:yellow;
	position: absolute;
	z-index: 2;
}
     
  </script>
        <script type="text/javascript">
            
           
          var norm = function (n, size, min, max, calcType) {

                if (n > 0 && calcType == 'L')
                    n = n - 1;

                min = min || 0;
                max = max || 100;
                var v = (n * 100) / size;
                var n = (v > min) ? ((v < max) ? v : max) : min;
                return Math.round(n);
            };
            var ipUsedBar =function(row)
            {
                var percentUsed =String(row[1]);
                percentUsed =percentUsed.replace(",",".");
                return '<div class="InlineBar '+row[12]+'"><h3><div style="width:'+percentUsed+'%; ; height: 10px; font-size: 7px;">&nbsp;</div></h3></div>';
            };
            var CreateSubnetUtilizationBar =function(row){
                var chart = []; 
                if(row[9] == null)
                    return;
                    var values =row[9].replace(/s/g,"\"s\"").replace(/e/g,"\"e\"").replace(/t/g,"\"t\"").replace(/p/g,"\"p\"");
                    var allocSize = row[10];
                    allocSize = (!allocSize || allocSize < 1) ? 1 : allocSize;                    
                    var data = jQuery.parseJSON(values);
                    if ($.isArray(data))
                    {
                        $.each(data, function( i, d ) {
                            chart.push({
                                left: norm(d.s, allocSize, 0, 100, 'L'),
                                width: norm(d.e - d.s, allocSize, 1, 100, 'W'),
                                type: d.t
                            });
                        });
                    }                    
                    if (typeof (row[11]) != "undefined") {
                        if (row[11] == 4) {
                            noRange = "<span class = \"r-no-ranges-hint\">@{R=IPAM.Strings;K=IPAMWEBJS_MK1_1;E=js}</span>";
                        }
                    }
                    else{
                        noRange = "<span class = \"r-no-ranges-hint\">@{R=IPAM.Strings;K=IPAMWEBJS_MK1_12;E=js}</span>";
                    }
                    
                    var tr = $('<ul/>');
                    tr.attr('class', 'wrapper');
                    tr.attr('style', 'list-style: none');                   
                    $.each(chart, function(i, c) { 
                        var li =$('<li/>');
                        if(c.type == 1)
                        {
                            li.attr('class', 'exclusion');                
                        }
                        else if(c.type == 2)
                        {  
                            li.attr('class', 'range');                         
                        }
                        li.attr('style', 'left:'+c.left+'%;width:'+c.width+'%');
                        var img =$('<ul/>');
                        img.attr('src', '/Orion/IPAM/res/images/default/s.gif');
                        img.appendTo(li);
                        li.appendTo(tr);
                    });
               
                    return tr;
            };
            var createSubnetInfo =function(row)
            {
                var subnetInfo = jQuery.parseJSON(row[3]);  
                var TotalCount =row[17];
                var SplitCount =row[18];
                var splitSize =0;
                if(TotalCount !=0)
                {
                    splitSize =(SplitCount *100) /TotalCount;
                    splitSize =Math.round(splitSize);
                }
                splitSize = String(splitSize)+"%";
                if(subnetInfo != null)
                {
                    var hiddenDiv ="<div style='display:none' AvailableCount="+subnetInfo.Available+" UsedCount="+subnetInfo.Used+" Total="+subnetInfo.Total+" ReservedCount="+subnetInfo.Reserved+" TransientCount="+subnetInfo.Transient+" Status="+subnetInfo.Status+" SubnetAddress="+subnetInfo.SubnetAddress+" SubnetRange='"+row[14]+"' ScopeName='"+row[0]+"' ServerName='"+row[8]+"' CIDR="+row[15]+" OfferDelay="+row[16]+" ScopeStatus = "+row[12]+" splitSize ="+splitSize+" ServerStatus ="+row[13]+" ></div>"; 
                    var subnetUtil = subnetInfo.Used +" / " + subnetInfo.Available;
                    return subnetUtil + hiddenDiv;
                }
                else
                {
                    return "";
                }
                
            };

            var CreateToolTip =function(selector,locrow,uID,isParent,gid) {
                $(selector).tooltip({
                    delay: 0,
                    showURL: false,
                    extraClass: "Tooltip1", 
                    top: -15, 
                    left: 5, 
                    bodyHandler: function() {   
                        var locrow11=locrow;                
                        var select1="";
                        if(isParent == 0)
                        {
                            select1 = $('td[class="column9 dhcp-scope-chart1 '+locrow11+' '+uID+' '+gid+'"]').prev().find('div');
                        }
                        else 
                        {
                            select1 = $('td[class="dhcp-scope-chart1 '+locrow11+' '+uID+' '+gid+'"]').prev().find('div');
                        }
                        var toolTipBody ="<div class='custom-x-tip-bwrap Tooltip1Body'>";                                            
                        toolTipBody =toolTipBody + "<b>Subnet:</b><img class='subnet-"+select1.attr('Status')+" Tooltip1Images' src='/Orion/IPAM/res/images/default/s.gif'/>"+select1.attr('SubnetAddress')+"/"+ select1.attr('CIDR')+"<br/>";
                        toolTipBody = toolTipBody +"<b>IPs:Available:</b>"+select1.attr('AvailableCount')+" <b>Used:</b>"+select1.attr('usedcount')+"<br/>";
                        toolTipBody = toolTipBody +"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Reserved:</b>"+select1.attr('ReservedCount')+" <b>Transient:</b>"+select1.attr('Transientcount')+"<br/><br/>";
                        toolTipBody = toolTipBody +"<b>Scope:</b> <img class='scope-"+select1.attr('ScopeStatus') +" Tooltip1Images' src='/Orion/IPAM/res/images/default/s.gif'/>" + select1.attr('ScopeName') +" on " ;
                        toolTipBody = toolTipBody +"<img align='top' class='dhcpserver-"+select1.attr('ServerStatus')+" Tooltip1Images' src='/Orion/IPAM/res/images/default/s.gif' />"+ select1.attr('ServerName')+"<br/>";    
                        toolTipBody = toolTipBody +"<b>Split size:</b>"+select1.attr('splitSize')+" <br/>";
                        toolTipBody = toolTipBody +"<b>Scope Address Range:</b> <br/>";
                        var ScopeIpRanges = jQuery.parseJSON(select1.attr('SubnetRange'));
                        if(ScopeIpRanges !=null)
                        {
                            $.each(ScopeIpRanges, function( i, s ) {
                                if(s.Type == 2)
                                {
                                    toolTipBody = toolTipBody +s.StartIP+" - "+s.EndIP+"<br/>";
                                }
                            }); 
                            toolTipBody = toolTipBody +"<b>Excluded Address:</b> <br/>";    
                            
                            $.each(ScopeIpRanges, function( i, s ) {                                
                                if(s.Type == 1)
                                {
                                    toolTipBody = toolTipBody +s.StartIP+" - "+s.EndIP+"<br/>";                               
                                }
                            }); 
                            
                            var OfferDelay = select1.attr('OfferDelay');
                            if(OfferDelay == "null")
                            {
                                OfferDelay = 0;
                            }
                            toolTipBody = toolTipBody +"<b>Delay set on DHCP: </b>"+OfferDelay+" ms <br/></div>";                                                
                        }
                        else 
                        {
                            return "";
                        }
                        return toolTipBody;                                             
                    }
                });

            }
            var ExpandUtilzScopes  = function(that){
                
                if($(that).attr('src') == '/Orion/Images/Button.Collapse.gif')
                {
                    $(that).parent().closest('tr').attr("class",'');
                    $(that).attr('src', '/Orion/Images/Button.Expand.gif');
                    $( "tr[id='Generated "+that.id+"']" ).attr("style","display:none");
                }
                else
                {
                    $(that).parent().closest('tr').attr("class",'hideBtmBorder');
                    $(that).attr('src', '/Orion/Images/Button.Collapse.gif');
                    if(($( "tr[id='"+that.id+"']" ).length) <= 0)
                    {
                        var swql = "<%= SWQL %>";
                        var ipaddress = String($(that).attr('ipaddress'));                     
                        var CIDR = $(that).attr('CIDR');
                        var ServerType =$(that).attr('ServerType');
                        swql = swql.replace("ViewType = 1", "ViewType > 1 and Address ='" +ipaddress+"' and CIDR="+CIDR+" and ServerType="+ServerType+""); 
                        swql = swql.replace("PercentUsed desc","PercentUsed asc");
                        swql = swql + " WITH ROWS 1 TO 100001 WITH TOTALROWS"                     
                        Information.Query(swql, function(result) {                            
                            if(result.TotalRows == null)
                            {
                                $(that).attr('src', '/Orion/IPAM/res/images/default/s.gif');                                
                            }
                            else
                            {
                                $.each(result.Rows, function(index, row) {
                                    var tr = $('<tr/>');
                                    tr.attr('id', 'Generated '+ that.id);
                                    tr.attr('style', "display:compact");
                                    $.each(row, function(colIndex, cell) { 
                                        if(colIndex <= 4)
                                        {                                       
                                            var td = $('<td/>');                                      
                                            td.attr('class', 'column' +colIndex);
                                            if(colIndex == 0)
                                            {   
                                                td.attr('style', 'padding-left : 30px;');
                                                var serverImage ="<img align='top' class='dhcpserver-"+row[13]+" Tooltip1Images' src='/Orion/IPAM/res/images/default/s.gif' />";
                                                var Scope = "<a href='/api2/ipam/ui/scope?name=" + encodeURIComponent(row[0]) + "'>" + row[0] + "</a>";
                                                var ServerName ="<a href='/api2/ipam/ui/dhcp'>"+String(row[8])+"</a>";
                                                td.html(Scope +" on " + serverImage + ServerName);
                                            }
                                            else if(colIndex == 1)
                                            {
                                                td.html(ipUsedBar(row));
                                            }
                                            else if(colIndex == 3)
                                            { 
                                                td.html(createSubnetInfo(row));
                                            }
                                            else if(colIndex == 4)
                                            {
                                                var uId = index +1;
                                                td.attr('class', 'dhcp-scope-chart1 '+row[0]+' ' + uId+' ' +row[20]);
                                                td.html(CreateSubnetUtilizationBar(row));
                                            }
                                            else
                                            {
                                                td.attr('style', 'padding-left : 0px;');
                                                td.html(cell);
                                            }
                                            td.appendTo(tr);
                                            if(colIndex == 4)
                                            {
                                                var loccell =row[0];
                                                var uId = index +1;
                                                CreateToolTip(td,loccell,uId,1,row[20]);
                                            }
                                        }
                                    });                                
                               
                                    $(that).parent().closest('tr').after(tr);
                                });
                            }
                        }, function(error) {
                            //errorMsg.text(error.get_message()).show();                           
                        });
                    }
                    else
                    {
                        $( "tr[id='Generated "+that.id+"']" ).attr("style","display:compact");
                    }
                }
                return true;
            };

            var previous = "";
            var prevGid=0;
            
            $(document).ready(function () {
                
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: <%= ScriptFriendlyResourceID %>,
                        // Which page should resource display when loaded
                        initialPage: 0,
                        // How many rows there can be on one page. If there are more rows, paging toolbar is displayed.
                        rowsPerPage: <%=HowManyScopes%>,                        
                        // True to allow sorting of rows by clicking on column headers
                        allowSort: false,                       
                        columnSettings: {                           
                            "FriendlyName": {
                                header: 'Scope >> related scope',
                                formatter: function (value, row, cellinfo) {
                                    var SplitCount = row[6];
                                    var serverImage ="<img align='top' class='dhcpserver-"+row[13]+" Tooltip1Images' src='/Orion/IPAM/res/images/default/s.gif' />";
                                    var Scope = "<a href='/api2/ipam/ui/scope?name=" + encodeURIComponent(row[0])+"'>"+row[0]+"</a>";
                                    var ServerName ="<a href='/api2/ipam/ui/dhcp'>"+String(row[8])+"</a>";
                                    if(SplitCount == 1 )
                                    {   
                                        return  Scope +" on " + serverImage + ServerName;
                                    }
                                    else
                                    {  
                                        var image = "<img style='padding-right:10px' CIDR ="+row[15]+" ServerType="+row[11]+" ipaddress="+row[7]+" id="+ row[20] +" src='/Orion/Images/Button.Expand.gif' onclick='return ExpandUtilzScopes(this);'>";                                            
                                        return image + Scope +" on " + serverImage +  ServerName;
                                    }
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    var SplitCount = row[6];
                                    
                                    if(SplitCount == 1 )
                                    {
                                        return 'Sibilingcolumn';
                                    }
                                    else
                                    {
                                        return 'parentCol';
                                    }
                                },
                                isHtml: true  
                            },
                            "PercentUsed":{
                                header: 'Percent IPs used',
                                formatter: function (value, row, cellinfo) {
                                    return ipUsedBar(row);
                                },                                
                                isHtml: true
                            },
                            "ScopeUtil":{
                                header: 'Scope IPs <br/> Used / Available',
                                isHtml: true
                            },
                            "SubnetInfo":{
                                header: 'Subnet IPs <br/> Used / Available',
                                formatter: function (value, row, cellinfo) {
                                    return createSubnetInfo(row);
                                },
                                isHtml: true
                            },
                            "ScopeIPRanges":{
                                header: 'Scope in Subnet',
                                formatter: function (value, row, cellinfo) {
                                    var locrow = previous;  
                                    var locgid =prevGid;
                                    var  selector='td[class="column9 dhcp-scope-chart1 '+locrow+' 0 '+locgid+'"]';
                                    
                                    CreateToolTip(selector,locrow,'0',0,locgid);
                                    previous = row[0];
                                    prevGid =row[20];
                                    return CreateSubnetUtilizationBar(row);
                                },
                                cellCssClassProvider: function(value, row, cellInfo) {
                                    return 'dhcp-scope-chart1 ' + row[0] +' 0 ' + row[20];
                                },
                                isHtml: true
                            }
                        }
                    }); 
               
        
                // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };
                // This registers resource in view refresh routine
                SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');
                // And finally loads initial data
                refresh();
            });
            
            // + ' tr:last td:eq(4)'
            var grid = $('#Grid-' + '<%= ScriptFriendlyResourceID %>');
            $(grid).live( "mouseover",function( event ) {
                var  selector='td[class="column9 dhcp-scope-chart1 ' + previous + ' 0 ' + prevGid + '"]';
                CreateToolTip(selector, previous, '0', 0, prevGid);
            });

            //Initialize Tooltip for last row in resource.
            $(window).load(function() {
                var  selector='td[class="column9 dhcp-scope-chart1 ' + previous + ' 0 ' + prevGid + '"]';
                CreateToolTip(selector, previous, '0', 0, prevGid);
            });
           
        </script>

    </Content>
</orion:resourceWrapper>
