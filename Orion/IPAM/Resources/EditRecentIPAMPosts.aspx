<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/ResourceEdit.master" CodeFile="EditRecentIPAMPosts.aspx.cs" Inherits="Orion_IPAM_Resources_EditRecentIPAMPosts" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><%= string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, Resource.Title)%></h1>
    <div style="padding-left: 16px;">
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
    <br />
    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_642 %></b>
    <br/>
        <asp:TextBox ID="PostsCount" runat="server" />

        <asp:CompareValidator ID="CompareValidator1" runat="server" 
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_643 %>" 
          Operator="GreaterThanEqual" 
          Type="Integer" 
          ValueToCompare="1" 
          ControlToValidate="PostsCount">*</asp:CompareValidator>
      
        <asp:CompareValidator ID="CompareValidator2" runat="server" 
          ErrorMessage="Number of Post must be less than 100" 
          Operator="LessThanEqual" 
          Type="Integer" 
          ValueToCompare="100" 
          ControlToValidate="PostsCount">*</asp:CompareValidator>
      
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
          ControlToValidate="PostsCount" 
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_644 %>">*</asp:RequiredFieldValidator>

    <br />
    <br />
        <orion:LocalizableButton runat="server" ID="btnSubmit" OnClick="SubmitClick" LocalizedText="Submit" DisplayType="Primary" />                 
    
    </div>
</asp:Content>
