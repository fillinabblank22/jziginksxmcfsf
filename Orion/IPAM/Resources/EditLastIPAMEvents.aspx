<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/ResourceEdit.master" CodeFile="EditLastIPAMEvents.aspx.cs" Inherits="Orion_IPAM_Resources_EditLastIPAMEvents" %>
<%@ Register TagPrefix = "orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <span><b><big><%= Page.Title %></big></b></span>
 
 <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
 
 <b><orion:EditResourceTitle runat="server" ID="resourceTitleEditor" ShowSubTitle="false" /></b>
 
 <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_639 %></b>
 <br /> 
 <asp:TextBox runat="server" ID="HowMany" />
 <asp:CompareValidator ID="CompareValidator1" runat="server" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_640 %>" 
      Operator="GreaterThanEqual" 
      Type="Integer" 
      ValueToCompare="1" 
      ControlToValidate="HowMany">*</asp:CompareValidator>
      
 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
      ControlToValidate="HowMany" 
      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_641 %>">*</asp:RequiredFieldValidator>   
 <br />
 <br /> 
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>" OnClick="SubmitClick" DisplayType="Primary" LocalizedText="Submit"/>   
</asp:Content>