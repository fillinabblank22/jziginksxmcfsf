using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Core.Common.Models;

public partial class Orion_IPAM_Resources_EditSearchIPAddress : System.Web.UI.Page
{
    private const string PROPERTY_SEARCHFIELDS_CHECKED = "SearchFields_Checked";

    private List<string> checkedcolumns = new List<string>();
    private Dictionary<string, string> customProperties = null;

    private ResourceInfo _resource;
    protected ResourceInfo Resource
    {
        get { return _resource; }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
        {
            int resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
            this._resource = ResourceManager.GetResourceByID(resourceID);
            Page.Title = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_VB1_10, this.Resource.Title);
            this.resourceTitleEditor.ResourceTitle = this.Resource.Title;

        }

        List<object> values = SwisConnector.RunSwisOps(new SwisOpCtx[]{
                new SwisOpCtx( "Retrieve list of custom fields", OpGetCustomPropertyDefs, null )
            });

        List<CustomProperty> allCustomProperties = values[0] as List<CustomProperty>;
        customProperties = GetIPCustomProperties(allCustomProperties);

        SearchFieldsRepeater.ItemDataBound += new RepeaterItemEventHandler(SearchFieldsRepeater_ItemDataBound);
        InitSearchFields();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitJavaScript();

            List<string> source = GetSearchFields();
            source.AddRange(customProperties.Keys);

            SearchFieldsRepeater.DataSource = source;
            SearchFieldsRepeater.DataBind();
        }
    }

    private List<string> GetSearchFields()
    {
        List<string> result = new List<string>();

        foreach (string column in SearchStateInfo.SEARCH_FIELDS)
        {
            // skip separators
            if (column == SearchStateInfo.FIELD_SEPARATOR)
            {
                continue;
            }
            result.Add(column);
        }

        return result;
    }

    protected void SearchFieldsRepeater_ItemDataBound(object src, RepeaterItemEventArgs args)
    {
        RepeaterItem item = args.Item;
        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        {
            Label label = (Label)item.FindControl("columnName");
            CheckBox isVisibleInResource = (CheckBox)item.FindControl("isVisibleInResource");
            CheckBox isCheckedByDefault = (CheckBox)item.FindControl("isCheckedByDefault");

            string field = (string)item.DataItem;
            string displaytext = field;

            if (field.StartsWith("CustomProperties."))
            {
                if (customProperties.ContainsKey(field))
                {
                    displaytext = customProperties[field];
                }
            }
            else
            {
                displaytext = LocalizationHelper.GetEntityDisplayString(IPNode.EIM_ENTITYNAME.Replace('.', '_'), field);
            }

            label.Text = GetLocalizedProperty("SearchField", displaytext);
            label.Attributes["Value"] = field;
            isCheckedByDefault.Checked = checkedcolumns.Contains(field);

            string js = string.Format(
                "$SWRES.AddCB('{0}', '{1}');",
                field, isCheckedByDefault.ClientID);

            WriteJavaScript(js, true);
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (!resourceTitleEditor.ResourceTitle.Equals(Resource.Title))
        {
            Resource.Title = resourceTitleEditor.ResourceTitle;
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(Resource);
        }

        SaveResourceSettings();

        string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
        Response.Redirect(url);
    }

    private void InitSearchFields()
    {
        if (Resource != null)
        {
            string strCheckedColumns = Resource.Properties[PROPERTY_SEARCHFIELDS_CHECKED];
            if (strCheckedColumns != null)
            {
                checkedcolumns.AddRange(strCheckedColumns.Split(','));
                return;
            }
        }

        // default will check all (non-custom) columns
        List<string> columns = GetSearchFields();
        checkedcolumns.AddRange(columns);
    }

    private void SaveResourceSettings()
    {
        List<string> checkedFields = new List<string>();
        foreach (RepeaterItem item in SearchFieldsRepeater.Items)
        {
            Label clname = (Label)item.FindControl("columnName");
            string field = clname.Attributes["Value"];
            CheckBox isCheckedByDefault = (CheckBox)item.FindControl("isCheckedByDefault");

            if (isCheckedByDefault.Checked)
            {
                checkedFields.Add(field);
            }
        }

        if (Resource != null)
        {
            string resourceData = string.Join(",", checkedFields.ToArray());
            Resource.Properties[PROPERTY_SEARCHFIELDS_CHECKED] = resourceData;
        }
    }

    private object OpGetCustomPropertyDefs(IpamClientProxy proxy, object ignore)
    {
        return proxy.AppProxy.CustomProperty.GetAllFromIPNode();
    }

    private Dictionary<string, string> GetIPCustomProperties(IEnumerable<CustomProperty> customProperties)
    {
        Dictionary<string, string> ipNodeCustomProperties = new Dictionary<string, string>();

        if (customProperties != null)
        {
            foreach (var properties in customProperties)
            {
                ipNodeCustomProperties.Add("CustomProperties." + properties.PropertyName, properties.PropertyName);
            }
        }

        return ipNodeCustomProperties;
    }

    private void InitJavaScript()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("if(!window.$SWRES) window.$SWRES=new (function(){ this._cb=[];");
        sb.AppendLine(" this.AddCB=function(n,v){this._cb.push([n,v])};");
        sb.AppendLine(" this.GetCB=function(n){var r='';$.each(this._cb,function(k,v){if(n==v[0]){r='#'+v[1];return false;}});return r;};");
        sb.AppendLine(" this.EachCBs=function(fn){$.each(this._cb,fn);};");
        sb.AppendLine("})();");
        sb.AppendLine("$(document).ready(function(){");
        sb.AppendLine(@"
    var allCB = $($SWRES.GetCB('All Fields'));
    // AllFields check change
    allCB.click( function(){
        var s=this.checked;
        $SWRES.EachCBs( function(k,cb){
            if(k=='All Fields') return true;
            $('#'+cb[1])[0].checked=s;
        });
    });
    // other fields check change
    $SWRES.EachCBs( function(k,cb){
        if(k=='All Fields') return true;
        $('#'+cb[1]).click( function(){
            if(this.checked) return;
            allCB[0].checked=false;
        });
    });
");
        sb.AppendLine("});");

        WriteJavaScript(sb.ToString(), true);
    }

    private void WriteJavaScript(string javaScript, bool once)
    {
        this.jscollector.SW_TryAddInit(javaScript, once);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }
}