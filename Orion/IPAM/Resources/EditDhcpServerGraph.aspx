﻿<%@ Page Language="C#" MasterPageFile="~/Orion/ResourceEdit.master" AutoEventWireup="true"
         CodeFile="EditDhcpServerGraph.aspx.cs" Inherits="Orion_IPAM_Resources_EditDhcpServerGraph" %>
<%@ Register TagPrefix="orion" TagName="EditResourceTitle" Src="~/Orion/Controls/EditResourceTitle.ascx" %>

<asp:Content ContentPlaceHolderID="MainContent" Runat="Server">
    <h1><asp:Label ID="ResourceName" runat="server" Text="-" ForeColor="Black" /></h1>
 
 <asp:ValidationSummary runat="server" />
 
 <orion:EditResourceTitle runat="server" ID="TitleEditControl" ShowSubTitle="true" />
 
    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_665 %></b>
    <br />
    <asp:TextBox runat="server" ID="txtWidth" Text="300" Columns="30" />
    <asp:CompareValidator runat="server" Display="Dynamic" ControlToValidate="txtWidth"
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_666 %>" 
          Operator="GreaterThanEqual" Type="Integer" ValueToCompare="100">*</asp:CompareValidator>
    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="txtWidth" 
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_667 %>">*</asp:RequiredFieldValidator>   

    <br />

    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_668 %></b>
    <br />
    <asp:TextBox runat="server" ID="txtHeight" Text="200" Columns="30" />
    <asp:CompareValidator runat="server" Display="Dynamic" ControlToValidate="txtHeight"
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_669 %>"
          Operator="GreaterThanEqual" Type="Integer" ValueToCompare="100">*</asp:CompareValidator>
    <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="txtHeight"
          ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_670 %>">*</asp:RequiredFieldValidator>   

    <br />
    <br />
    
    <orion:LocalizableButton runat="server" ID="btnSubmit" ToolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_645 %>"  OnClick="OnSubmitClick" LocalizedText="Submit" DisplayType="Primary"/>
</asp:Content>


