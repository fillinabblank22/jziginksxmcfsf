﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;

public partial class AddSubnetIPAddresses : System.Web.UI.Page
{
    
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            radioAuto.Checked = true;
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        
        if (radioImport.Checked)
        {
            Page.Response.Redirect("~/Orion/IPAM/Import.upload.intro.aspx?GroupID=0");
        }

        if (radioAuto.Checked)
        {
            Page.Response.Redirect("~/Orion/Discovery/Default.aspx?origUrl=Admin");
        }

        if (radioManual.Checked)
        {
            Page.Response.Redirect("~/Orion/IPAM/Subnet.BulkAdd.aspx?ObjectID=0");
        }
        if (radioSingle.Checked)
        {
            Page.Response.Redirect("~/Orion/IPAM/Subnet.Add.aspx?ObjectId=0&ParentId=0");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Orion/IPAM/Admin/Admin.Overview.aspx");
    }
}
