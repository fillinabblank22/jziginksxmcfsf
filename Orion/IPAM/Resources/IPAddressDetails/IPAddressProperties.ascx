﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressProperties.ascx.cs"
            Inherits="Orion_IPAM_Resources_IPAddressProperties" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/IPAddressLink.ascx" TagName="IPAddressLink" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <asp:Repeater runat="server" ID="IPAddressProperties">
        <HeaderTemplate>

    <div style="height: 5px;">&nbsp;</div>
        <table border="0" cellPadding="2" cellSpacing="0" width="100%" class="NeedsZebraStripes">
	        <tr>
		        <td class="Property" width="10px">&nbsp;</td>
		        <td class="PropertyHeader" valign="center" width="150px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_683%></td>
		        <td class="Property" width="22px">&nbsp;</td>
		        <td class="Property NodeManagementIcons">
<% if (this.IsOperator) { %>
			        <a href="<%#this.EditIPAddressURL%>"  runat="server" ID="EditServerUrlPowerUser">
                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_702 %>" style="white-space: normal"/>
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_702 %>
                    </a>
<% } else { %>
                    <a href="#" style="color: #A0A0A0;"  runat="server" ID="EditServerUrlNormalUser"  onclick="alert('<%# Resources.IPAMWebContent.IPAMWEBDATA_VB1_701 %>');">
                    <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_702 %>" style="white-space: normal"/>
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_702 %>
                    </a>
<% } %>
			            
		        </td>
	        </tr>
	                
        </HeaderTemplate>
        
        <ItemTemplate>
	        <tr id="PropertyContainer" runat="server">
              <td class="Property" width="10">&nbsp;</td>
              <td class="PropertyHeader" valign="center"><asp:Label ID="PropertyName" runat="server" /></td>
              <td class="Property">&nbsp;</td>
              <td class="Property">
                 <asp:Label ID="PropertyValue" Visible="false" runat="server" />
              </td>
	        </tr>
        </ItemTemplate>
        
        <FooterTemplate>
        
        </table>
    </div>
            
        </FooterTemplate>
    </asp:Repeater>
  </Content>
</orion:resourceWrapper>
