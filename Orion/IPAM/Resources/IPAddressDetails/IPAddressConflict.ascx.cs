using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using System.Linq;
using System.Collections.Specialized;
using System.Web;
using SolarWinds.InformationService.Contract2;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressConflict : BaseResourceControl
{
    #region Resource Properties

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMNodeProvider) }; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_SE_01; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMConflictDetails"; }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get { return ResourceLoadingMode.Ajax; }
    }

    private string SWQL
    {
        get
        {
            return string.Format("SELECT TOP 10 {0} FROM Orion.UDT.MACAddressInfo {1}", string.Join(",", udtEventsColumnList), string.IsNullOrEmpty(UdtWhereClause) ? string.Empty : UdtWhereClause); 
        }
    }
    
    private string UdtWhereClause = string.Empty;

    public int HowManyUdtEvents
    {
        get
        {
            return 10;
        }
    }

    // This is here to make resource working in reports. Resources in reports get negative ID assigned.
    // If we use negative ID in CustomQueryTable control, it won't work because it's used
    // on some places where JS does not allow it.
    protected int ScriptFriendlyResourceID
    {
        get { return Math.Abs(Resource.ID); }
    }

    #endregion // Resource Properties

    #region IPNode Properties

    private string[] conflictColumnList = new string[]
        {
            "C.IPNodeID","C.SubnetId","C.IPAddress","C.IPAddressN","C.ConflictId","C.ConflictType","C.ConflictTypeIcon","C.ConflictTypeText", "C.ConflictTimeUTC AS ConflictTimeUTC","C.ConflictStatus",
            "C.AssignedMac","C.AssignedRawMac","C.AssignedSourceType","C.AssignedSourceText", "C.ConflictingMac","C.ConflictingRawMac","C.ConflictingSourceType","C.ConflictingSourceText",
            "C.AssignMACVendIcon","C.AssignedScopeName","C.AssignedScopeIcon","C.AssignedDhcpServerName","C.AssignedDhcpServerIcon","C.ConflictMACVendIcon","C.ConflictScopeName","C.ConflictScopeIcon",
            "C.ConflictDhcpServerName","C.ConflictDhcpServerIcon"
        };

    private string[] udtEventsColumnList = new string[]
        {
            "ToLocal(LastUpdate) AS EventTime","MACAddress","VendorIcon AS [_VendorIcon]","MACUrl AS [_MACUrl]","PortName","PortIcon AS [_PortIcon]","PortUrl AS [_PortUrl]",
            "NodeName","NodeStatusIcon AS [_NodeStatusIcon]","CASE WHEN IsWireless = 1 THEN APUrl ELSE MACAddressInfo.Node.DetailsUrl END AS [_NodeUrl]","UserName AS [_UserName]",
            "UserUrl AS [_Userurl]","ClientSSID AS [_ssid]","NodeID as [_NodeID]","PortDescription AS [_PortDescription]","rawMAC AS [_rawMAC]","IPAddress AS [_IPAddress]",
            "PortId AS [_PortId]", "EndpointID AS [_EndpointID]" , "HostName AS [_HostName]", "HostNameUrl AS [_HostNameUrl]"
        };

    private string[] udtMacCurrentInfo_ColumnList = new string[]
        {
            "{0}.AdministrativeStatus AS {0}_AdministrativeStatus", "{0}.APUrl AS {0}_APUrl", "{0}.ClientSSID AS {0}_ClientSSID", "{0}.Description AS {0}_Description", "{0}.DisplayName AS {0}_DisplayName", 
            "{0}.EndpointID AS {0}_EndpointID", "{0}.HostName AS {0}_HostName","{0}.HostNameUrl AS {0}_HostNameUrl","{0}.IPAddress AS {0}_IPAddress", "{0}.IsRWCommunityDefined AS {0}_IsRWCommunityDefined", "{0}.IsWireless AS {0}_IsWireless", 
            "{0}.LastSeen AS {0}_LastSeen", "{0}.MACAddress AS {0}_MACAddress","{0}.NodeAccessPoint AS {0}_NodeAccessPoint", "{0}.NodeID AS {0}_NodeID", "{0}.NodeStatus AS {0}_NodeStatus", 
            "{0}.OperationalStatus AS {0}_OperationalStatus", "{0}.PortDescription AS {0}_PortDescription", "{0}.PortIcon AS {0}_PortIcon", "{0}.PortID AS {0}_PortID", "{0}.PortName AS {0}_PortName", 
            "{0}.PortType AS {0}_PortType", "{0}.PortUrl AS {0}_PortUrl", "{0}.rawMAC AS {0}_rawMAC", "{0}.ShutdownIcon AS {0}_ShutdownIcon"
        };

    protected IpamIPNode IpamIPAddress { get; set; }
    protected IPNode IPAddress { get; set; }

    private bool? isOperator = null;
    protected bool IsOperator
    {
        get
        {
            if (!this.isOperator.HasValue)
            {
                this.isOperator = AuthorizationHelper.AccessCheck(this.Page, AccountRole.Operator, new int[] { this.IPAddress.SubnetId });
            }

            return this.isOperator.Value;
        }
    }

    public bool isConflictFound { get; set; }

    public bool isUdtInstalled
    {
        get { return isUDTInstalled(); }
    }

    public int AssignedSourceType { get; set; }
    public int ConflictSourceType { get; set; }
    public bool isUdtCurrentInfoFound { get; set; }

    #endregion // IPNode Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        IIPAMNodeProvider ipamNodeProvider = GetInterfaceInstance<IIPAMNodeProvider>();
        if ((ipamNodeProvider != null) && (ipamNodeProvider.IPAddress != null))
        {
            this.AddStylesheet("/Orion/IPAM/res/css/sw-resources.css");
            this.AddStylesheet("/Orion/IPAM/res/css/sw-base.css");

            this.IpamIPAddress = ipamNodeProvider.IPAddress;
            this.IPAddress = this.IpamIPAddress.IPAddress;

            if (this.IPAddress != null && this.IPAddress.IsIPv6)
                this.Visible = false;

            InitResourceData();

            // Set unique ID CustomQueryTable instance that you put to .ascx file
            CustomTable.UniqueClientID = ScriptFriendlyResourceID;

            // Set SWQL query that loads data to display
            CustomTable.SWQL = SWQL;
        }
        else
        {
            this.Visible = false;
        }
    }

    #endregion // Events

    #region Methods

    private DataTable getConflictData(string query, PropertyBag param)
    {
        // load subnet (use SWIS to load GroupNode)
        DataTable dt = null;
        using (IpamClientProxy proxy = SwisConnector.GetProxy())
        {
            if (param.Count > 0)
            {
                dt = proxy.SwisProxy.Query(query, param);
            }
            else
                dt = proxy.SwisProxy.Query(query);
        }
        return dt;
    }

    private void LoadConflictData()
    {
        var columnLists = conflictColumnList.ToList();
        string udtJoinClause = string.Empty;
        string nodeIdProjection = string.Empty;
        var param = new PropertyBag();
        //Frame SWIS query column list & where condition to get data from Orion.UDT.MACCurrentInfo Entity
        if (isUDTInstalled())
        {
            var macsReturnQuery = string.Format("SELECT AssignedRawMac, ConflictingRawMac FROM IPAM.ConflictDetail AS C " +
                                "WHERE C.IPNodeID = {0} AND C.IPAddressN = '{1}' " +
              "AND C.ConflictStatus IN (1,2) ",
               this.IPAddress.IpNodeId, this.IPAddress.IPAddressN);
            var macsData = getConflictData(string.Format(macsReturnQuery, this.IPAddress.IpNodeId, this.IPAddress.IPAddressN), new PropertyBag());
            param = new PropertyBag()
                {
                    {"AssignedMac" ,string.Empty},
                    {"ConflictMac" ,string.Empty}
                };
            if (macsData != null && macsData.Rows.Count > 0)
            {
                param["AssignedMac"] = macsData.Rows[0]["AssignedRawMac"].ToString();
                param["ConflictMac"] = macsData.Rows[0]["ConflictingRawMac"].ToString();
            }

            for (int i = 0; i <= 1; i++)
            {
                string alias = (i == 0) ? "Assigned" : "conflict";
                string NodeAlias = (i == 0) ? "AssignedNode" : "ConflictNode";

                columnLists.AddRange(udtMacCurrentInfo_ColumnList.Select(r => string.Format(r, alias)).ToList());
                udtJoinClause += string.Format(" LEFT JOIN Orion.UDT.MACCurrentInfo AS {0} ON {0}.rawMAC = {1} ", alias, (i == 0) ? "C.AssignedRawMac" : "C.ConflictingRawMac");
                udtJoinClause += string.Format(" LEFT JOIN Orion.Nodes AS {0} ON {0}.NodeID = {1}.NodeID  ", NodeAlias, alias);
            }
            nodeIdProjection = ", AssignedNode.NodeID AS Assigned_OrionNode, ConflictNode.NodeID AS Conflict_OrionNode";
        }

        string query =
            string.Format(
                "SELECT {0}{4} FROM IPAM.ConflictDetail AS C {3} WHERE C.IPNodeID = {1} AND C.IPAddressN = '{2}' " +
                "AND C.ConflictStatus IN (1,2) ORDER BY C.ConflictTimeUTC DESC", string.Join(",", columnLists),
                this.IPAddress.IpNodeId, this.IPAddress.IPAddressN, udtJoinClause, nodeIdProjection);

        string assignedMac = string.Empty;
        string conflictingMac = string.Empty;
        string conflictId = string.Empty;
        string statusFilterConditionVal = "2";

        var conflictData = getConflictData(string.Format(query, this.IPAddress.IpNodeId, this.IPAddress.IPAddressN),param);

        var isConflictExits = (from DataRow dr in conflictData.Rows
                               where SafeConvert<string>(dr["ConflictStatus"], "-1") == statusFilterConditionVal
                               select dr).Count();

        isConflictFound = conflictData != null && conflictData.Rows.Count > 0;

        if (!isConflictFound)
        {
            this.noConflictFoundMsg.Style.Remove("display");
        }
        else
        {
            assignedMac = SafeConvert<string>(conflictData.Rows[0]["AssignedRawMac"], string.Empty);
            conflictingMac = SafeConvert<string>(conflictData.Rows[0]["ConflictingRawMac"], string.Empty);
            conflictId = SafeConvert<string>(conflictData.Rows[0]["ConflictId"],string.Empty);

            AssignedSourceType = SafeConvert<int>(conflictData.Rows[0]["AssignedSourceType"], 0);
            ConflictSourceType = SafeConvert<int>(conflictData.Rows[0]["ConflictingSourceType"], 0);

            if (isUDTInstalled())
                isUdtCurrentInfoFound = (SafeConvert<int>(conflictData.Rows[0]["Assigned_EndpointID"], 0) > 0 || SafeConvert<int>(conflictData.Rows[0]["Conflict_EndpointID"], 0) > 0);

            if (isConflictExits > 0)
            {
                this.resourceData.Style.Add("display","none");
                this.enableConflict.Style.Remove("display");
            }
            else
            {
                statusFilterConditionVal = "1";
                this.resourceData.Style.Remove("display");
            }

            //Here filtering top first conflict row based on ConflictStatus
            conflictData = (from DataRow dr in conflictData.Rows
                            where SafeConvert<string>(dr["ConflictStatus"], "-1") == statusFilterConditionVal
                            select dr).Take(1).CopyToDataTable();

            List<string> sb = new List<string>();
            if (!string.IsNullOrEmpty(assignedMac))
                sb.Add(assignedMac);

            if (!string.IsNullOrEmpty(conflictingMac))
                sb.Add(conflictingMac);

            if (sb.Count > 0)
                UdtWhereClause = string.Format(" WHERE EndpointID <> -2 AND (rawMAC IN ({0}) OR IPAddress = '{1}')", string.Join(",", sb.Select(r => string.Format("'{0}'", r))), this.IPAddress.IPAddress);
        }

        //TimeZoneInfo.ClearCachedData();
        var utcTimeFormat = (conflictData != null && conflictData.Rows.Count > 0) ? SafeConvert<DateTime>(conflictData.Rows[0]["ConflictTimeUTC"], DateTime.Now) : DateTime.Now.ToUniversalTime();
        utcTimeFormat = DateTime.SpecifyKind(utcTimeFormat, DateTimeKind.Utc);

        //TimeZoneInfo localZone = TimeZoneInfo.Local;
        //TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(localZone.Id);
        //utcTimeFormat = TimeZoneInfo.ConvertTimeFromUtc(utcTimeFormat, tz);

        var startUpScript = string.Format("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"IP_Address_Conflict_Resource_{0}\">\n",this.ScriptFriendlyResourceID) +
                                "if (!window.$SW) window.$SW = {};\n"+
                                "if(!$SW.IPAM) $SW.IPAM = {};\n" +
                                string.Format("if(!$SW.IPAM.IPConflictResource_{0}) $SW.IPAM.IPConflictResource_{0} = {1};\n",this.ScriptFriendlyResourceID,"{}") +
                                string.Format("$SW.IPAM.IPConflictResource_{1}.isConflictFound = '{0}';\n ", isConflictFound ? "True" : "False",this.ScriptFriendlyResourceID) +
                                string.Format("$SW.IPAM.IPConflictResource_{1}.ConflictId = '{0}';\n ", Convert.ToString(conflictId),this.ScriptFriendlyResourceID) +
                                string.Format("$SW.IPAM.IPConflictResource_{1}.isUdtInstalled = '{0}';\n ", isUdtInstalled ? "True" : "False",this.ScriptFriendlyResourceID) +
                                string.Format("$SW.IPAM.IPConflictResource_{1}.ConflictAt = '{0}';\n ", utcTimeFormat.ToString("o"), this.ScriptFriendlyResourceID) +
                                string.Format("$SW.IPAM.IPConflictResource_{1}.FormattedConflictTime = '{0}';\n ", String.Format("{0:d MMM yyyy hh:mm:ss tt}", utcTimeFormat), this.ScriptFriendlyResourceID) +
                            "</script>\n";
        
        ScriptManager.RegisterStartupScript(this.Page, GetType(), string.Format("IP_Address_Conflict_Resource_{0}",this.ScriptFriendlyResourceID), startUpScript, false);

        this.IPAddressConflict.DataSource = conflictData;
        this.IPAddressConflict.ItemDataBound += new RepeaterItemEventHandler(OnIpConflict_DataBinding);
        this.IPAddressConflict.DataBind();
    }

    public void InitResourceData()
    {
        if ((this.Resource == null) || (this.IPAddress == null))
            return;

        // resource content
        if (this.IPAddressConflict != null)
        {
            // load ConflictData (use SWIS)
            LoadConflictData();
        }
    }

    private bool isUDTInstalled()
    {
        return OrionModuleManager.IsInstalled("UDT") && OrionModuleManager.IsModuleVersionAtLeast("UDT", "3.2");    
    }

    public string RenderDhcpInfo(object container,int renderType,string prefix)
    {
        string dhcpScopeRendertag = @"<a href='/api2/ipam/ui/scope?name={0}'>
                                                <img title='' alt='' src='/orion/ipam/res/images/default/s.gif' class='{1} Tooltip1Images' />
                                                {2}
                                        </a>";

        string dhcpServerRendertag = @"{0}<a href='/api2/ipam/ui/dhcp'>
                                            <img title='' alt='' src='/orion/ipam/res/images/default/s.gif' class='dhcpserver-{1} Tooltip1Images' />
                                            {2}
                                        </a>";

        switch(renderType)
            {
            case 1:
                    if (string.IsNullOrEmpty(SafeConvert<string>(DataBinder.Eval(container, prefix + "ScopeName"),string.Empty)))
                        return Resources.IPAMWebContent.IPAMWEBCODE_SE_37;

                    return string.Format(dhcpScopeRendertag
                                            , HttpUtility.UrlEncode(SafeConvert<string>(DataBinder.Eval(container, prefix + "ScopeName"),string.Empty))
                                            , SafeConvert<string>(DataBinder.Eval(container, prefix + "ScopeIcon"), string.Empty)
                                            , SafeConvert<string>(DataBinder.Eval(container, prefix + "ScopeName"), string.Empty));
                    break;
            case 2:

                    if (string.IsNullOrEmpty(SafeConvert<string>(DataBinder.Eval(container, prefix + "DhcpServerName"), string.Empty)))
                        return string.Empty;

                    return string.Format(dhcpServerRendertag
                                            , Resources.IPAMWebContent.IPAMWEBCODE_SE_35
                                            , SafeConvert<string>(DataBinder.Eval(container, prefix + "DhcpServerIcon"), string.Empty)
                                            , SafeConvert<string>(DataBinder.Eval(container, prefix + "DhcpServerName"), string.Empty));
                    break;
            default:
                    return string.Empty;
                    break;

            }
    }

    public string FormatCurrentUdtData(RepeaterItem container,string colName,string defValue)
    {
        if (!isUdtInstalled)
            return defValue;

        return SafeConvert<string>(DataBinder.Eval(container.DataItem, colName), defValue);
    }

    public string GetRecommendedAction(object container)
    {
        int assignedType = SafeConvert<int>(DataBinder.Eval(container, "AssignedSourceType"), 0);
        int conflictType = SafeConvert<int>(DataBinder.Eval(container, "ConflictingSourceType"), 0);

        if (assignedType == 8 && conflictType == 8)
            return Resources.IPAMWebContent.IPAMWEBCODE_SE_40;
        else if (assignedType != 8 && conflictType != 8)
            return Resources.IPAMWebContent.IPAMWEBCODE_SE_34;
        else
            return Resources.IPAMWebContent.IPAMWEBCODE_SE_39;
    }

    protected void OnIpConflict_DataBinding(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Header:
                if (!isConflictFound)
                {
                    HtmlGenericControl lblHeader = (HtmlGenericControl)e.Item.FindControl("msgContainer");
                    if (lblHeader != null)
                        lblHeader.Visible = false;
                }
                else
                {
                    HtmlGenericControl conflictIpMsg = e.Item.FindControl("ConflictIpMsg") as HtmlGenericControl;
                    if (conflictIpMsg != null)
                        conflictIpMsg.InnerText = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_SE_02, this.IPAddress.IPAddress);
                }
                break;
            case ListItemType.Item:
                ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
                HtmlAnchor ignoreConflict = e.Item.FindControl("IgnoreConflict") as HtmlAnchor;
                HtmlImage conflictTypeIcon = e.Item.FindControl("conflictTypeIcon") as HtmlImage;
                HtmlAnchor assignedShutdownLink = e.Item.FindControl("assignedShutdownLink") as HtmlAnchor;
                HtmlAnchor conflictShutdownLink = e.Item.FindControl("conflictShutdownLink") as HtmlAnchor;

                StringBuilder config = new StringBuilder();
                config.Append("{0} ");
                config.Append("conflictId: {1}, ");
                config.Append("updateValue: '{2}', ");
                config.Append("statusType: '{3}', ");
                config.Append("macAddress: '{4}' ");
                config.Append(" {5}");

                int conflictId = SafeConvert<int>(DataBinder.Eval(e.Item.DataItem, "ConflictId"), -1);

                if (isConflictFound && ignoreConflict != null)
                    ignoreConflict.Attributes.Add("onclick", string.Format("javascript:FireAlert_{1}({0},null,this); return false;", string.Format(config.ToString(), "{", conflictId.ToString(), "2", "1", "", "}"), this.ScriptFriendlyResourceID));

                if (conflictTypeIcon != null)
                {
                    conflictTypeIcon.Attributes.Add("onmouseover", string.Format("javascript:showtip_{1}({2},'{0}');"
                                                                                , String.Format(Resources.IPAMWebContent.IPAMWEBCODE_SE_36, SafeConvert<string>(DataBinder.Eval(e.Item.DataItem, "ConflictTypeText"), string.Empty))
                                                                                , this.ScriptFriendlyResourceID
                                                                                , conflictTypeIcon.ClientID));

                    conflictTypeIcon.Attributes.Add("onmouseout", string.Format("javascript:hidetip_{0}(this);"
                                                                                ,this.ScriptFriendlyResourceID));
                }

                if (isUDTInstalled() && isConflictFound)
                {
                    Initialize_ShutdownButton(assignedShutdownLink, "Assigned", e.Item.DataItem, config.ToString(),conflictId, resInfo.IsInReport);
                    Initialize_ShutdownButton(conflictShutdownLink, "Conflict", e.Item.DataItem, config.ToString(),conflictId, resInfo.IsInReport);
                }

                if (resInfo.IsInReport || SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
                {
                    if (ignoreConflict != null)
                        ignoreConflict.Visible = false;
                }
                break;
            case ListItemType.Footer:
                break;
            default:
                break;
        }
    }

    private void Initialize_ShutdownButton(HtmlAnchor link, string prefix, object dataitem, string config, int conflictId, bool isInReport)
    {
        bool enableShutdown = true;
        string toolTipText = string.Empty;
        bool isWireless = SafeConvert<int>(DataBinder.Eval(dataitem, prefix + "_IsWireless"), 0) == 1;
        int portId = SafeConvert<int>(DataBinder.Eval(dataitem, prefix + "_PortID"), -1);
        bool isShutdownDisable = SafeConvert<int>(DataBinder.Eval(dataitem, prefix + "_OrionNode"), -1) == -1;
        

        string scripCallbackFn = "function() { " + string.Format("DoPortShutdown_{0}('{1}','{2}',{3},'3','3');", this.ScriptFriendlyResourceID, SafeConvert<string>(DataBinder.Eval(dataitem, prefix + "_NodeID"), "-1"),
                                            portId.ToString(), conflictId.ToString()) + " }";

        if (link != null)
        {
            if (isInReport || portId < 0 || isWireless)
            {
                link.Style.Add("display", "none");
                return;
            }

            if (!Profile.AllowNodeManagement || isShutdownDisable)
            {
                toolTipText = Resources.IPAMWebContent.IPAMWEBDATA_VN0_43;
                enableShutdown = false;
            }
            else if (SafeConvert<int>(DataBinder.Eval(dataitem, prefix + "_AdministrativeStatus"), 0) != 1)
            {
                enableShutdown = false;
            }
            else if (SafeConvert<int>(DataBinder.Eval(dataitem, prefix + "_IsRWCommunityDefined"), 0) == 0)
            {
                toolTipText = Resources.IPAMWebContent.IPAMWEBDATA_VN0_44;
                enableShutdown = false;
            }
            else
            {
               // Do Nothing
            }

            if (!enableShutdown)
            {
                link.Style.Add("cursor", "default");
                link.Style.Add("color", "#A09D9D");
                link.Style.Add("opacity", "0.6");

                if (!string.IsNullOrEmpty(toolTipText))
                {
                    link.Attributes.Add("onmouseover",string.Format("javascript:showtip_{1}({2},'{0}');"
                        , toolTipText,this.ScriptFriendlyResourceID
                        , link.ClientID));

                    link.Attributes.Add("onmouseout",string.Format("javascript:hidetip_{0}(this);"
                        , this.ScriptFriendlyResourceID));
                }
            }
            else
                link.Attributes.Add("onclick", string.Format("javascript:FireAlert_{0}({1},{2},this); return false;" 
                    ,this.ScriptFriendlyResourceID
                    , string.Format(config, "{", conflictId.ToString(), "3", "3", SafeConvert<string>(DataBinder.Eval(dataitem, prefix + "_MACAddress"), ""), "}")
                    , scripCallbackFn));
        }
    }

    protected T SafeConvert<T>(object value, T defaultValue)
    {
        if (value is DBNull) return defaultValue;

        try
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
        catch
        {
            return defaultValue;
        }
    }

    #endregion //Methods
}
