﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressDNSRecords.ascx.cs" Inherits="Orion_IPAM_Resources_IPAddressDetails_IPAddressDNSRecords" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DnsRecordsGrid.ascx" TagName="DnsRecordsGrid" %>

<orion:Include ID="Include1" runat="server" File="IPAM/res/js/sw-dns-constants.js" />
<orion:Include ID="Include2" runat="server" File="IPAM/resources/IPAddressDetails/CustomQuery.js" />

<orion:ResourceWrapper ID="ResourceWrapper" runat="server" ShowEditButton="true">
    <Content>
        <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" >
            <Services>
                <asp:ServiceReference path="/Orion/Services/Information.asmx" />
            </Services>
        </asp:ScriptManagerProxy>
    
        <div id="ErrorMsg-<%= Resource.ID %>"></div>

        <table id="Grid-<%= Resource.ID %>" cellpadding="2" cellspacing="0" width="100%">
            <tr class="HeaderRow"></tr>
        </table>

        <div id="Pager-<%= Resource.ID %>" class="ReportHeader hidden" style="line-height: 15px; text-align: center"></div>

        <textarea id="SWQL-<%= Resource.ID %>" style="display:none;">
            <%= HttpUtility.HtmlEncode(GetSwql()) %>
        </textarea>

        <script type="text/javascript">
            $(function () {
                $SW.IPAM.Resources.CustomQuery.createTableFromQuery(<%= Resource.ID %>, 0, <%= Resource.Properties["RowsPerPage"] ?? "5" %>);
            });	    
        </script>
    </Content>
</orion:ResourceWrapper>