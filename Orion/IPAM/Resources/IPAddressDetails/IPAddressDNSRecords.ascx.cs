﻿using System;
using System.Collections.Generic;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressDetails_IPAddressDNSRecords : BaseResourceControl
{
    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMNodeProvider) }; }
    }

    public string GetSwql() 
    {
        IIPAMNodeProvider ipamNodeProvider = GetInterfaceInstance<IIPAMNodeProvider>();
        IPNode ipNode = ipamNodeProvider.IPAddress.IPAddress;       
        return string.Format("SELECT {0}, {1} as _DnsRecordType_Type, {1}, {2} FROM {3} WHERE {4} = '{5}'", 
            DnsRecord.EIM_NAME,
            DnsRecord.EIM_TYPE,
            DnsRecord.EIM_DATA,
            DnsRecord.EIM_ENTITYNAME,
            DnsRecord.EIM_IPADDRESSN,
            ipNode != null ? ipNode.IPAddressN :
            System.Data.SqlTypes.SqlGuid.Parse("00000000-0000-0000-0000-000000000000"));
    }

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_21; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionIPAMAGDNSRecords";
        }
    }
}