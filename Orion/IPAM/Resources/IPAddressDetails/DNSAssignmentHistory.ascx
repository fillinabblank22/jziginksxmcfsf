﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DNSAssignmentHistory.ascx.cs"
            Inherits="Orion_IPAM_Resources_DNSAssignmentHistory" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<orion:resourceWrapper runat="server" ID="resourceWrapper">
  <Content>
    <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
      <ContentTemplate> 
        <table cellspacing="0" cellpadding="2" border="0" width="100%"><tbody>
          <tr>
            <td width="5" class="ReportHeader">&nbsp;</td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_691 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_692%></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_693 %></td>
            <td class="ReportHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_694 %></td>
          </tr>
             
         <asp:Repeater ID="HistoryList" runat="server">
            <HeaderTemplate />
            <ItemTemplate>
              <tr>
                <td class="Property">&nbsp;</td>
	            <td class="Property" style="white-space: nowrap; text-align: center; width: 1%;">
	                <%#HandleDateTimeValue("BeginDate")%><br />
	                <%#HandleDateTimeValue("EndDate")%>
	            </td>
	            <td class="Property"><%#HandleEmptyValue(Eval("IPAddress"))%></td>
	            <td class="Property"><%#HandleEmptyValue(Eval("Subnet"))%></td>
	            <td class="Property"><%#HandleEmptyValue(Eval("Source"))%></td>
              </tr>
            </ItemTemplate>
            <FooterTemplate />
          </asp:Repeater>

          <tr id="MsgMaxItems" runat="server" visible="false">
            <td class="Property">&nbsp;</td>
            <td class="Property" colspan="5" style="color: Red;">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_695 %>
            </td>
          </tr>

          <tr id="MsgNoItems" runat="server" visible="false">
            <td class="Property">&nbsp;</td>
            <td class="Property" colspan="5" style="height: 50px;">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_696 %>
            </td>
          </tr>

          <tr>
            <td class="ReportHeader" colspan="6">

<table class="PageButtons" cellspacing="0" cellpadding="0" width="100%"><tbody>
    <tr>
        <td>
            <asp:LinkButton CssClass="enabled" CommandName="Page" OnCommand="OnIndexChanging" CausesValidation="false" CommandArgument="Prev" ID="cmdPrev" runat="server">
                <img class="spacer prev" src="/Orion/IPAM/res/images/sw-navigation/arrow-spacer.gif" />&nbsp;<%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_697, ItemsCount)%>
            </asp:LinkButton>
        </td>
        <td style="padding: 0px;">
          <div style="width: 15px; height:15px;">
            <asp:UpdateProgress AssociatedUpdatePanelID="updatePanel" runat="server">
                <ProgressTemplate>
                  <img src="/Orion/IPAM/res/images/sw-navigation/loading.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_144 %>" />
                </ProgressTemplate>
            </asp:UpdateProgress>
          </div>
        </td>
        <td>
            <asp:LinkButton CssClass="enabled" CommandName="Page" OnCommand="OnIndexChanging" CausesValidation="false" CommandArgument="Next" ID="cmdNext" runat="server">
                  <%=string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_698, ItemsCount)%>&nbsp;<img class="spacer next" src="/Orion/IPAM/res/images/sw-navigation/arrow-spacer.gif" />
            </asp:LinkButton>
        </td>
        <td>
            <img src="/Orion/images/form_prompt_divider.gif" height="22px" width="2px" />
        </td>
        <td>
            <asp:HyperLink CssClass="enabled" ID="linkAll" Target="_blank" CausesValidation="false" runat="server">
                 <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_699 %>&nbsp;<img class="spacer all" src="/Orion/IPAM/res/images/sw-navigation/arrow-spacer.gif" />
            </asp:HyperLink>        
        </td>
        <td>
            <img src="/Orion/images/form_prompt_divider.gif" height="22px" width="2px" />
        </td>
        <td style="width: 100%; text-align: right;" >
            <asp:Panel CssClass="PageSearchContent" DefaultButton="btnSearch" runat="server">
                <asp:TextBox ID="txtSearch" runat="server"/>
                <asp:ImageButton runat="server" ID="btnSearch" OnClick="OnSearchClick" ImageUrl="/Orion/IPAM/res/images/sw-navigation/search-icon.gif" />
            </asp:Panel>
        </td>
    </tr>
</tbody></table>
            
            </td>
          </tr>
        </tbody></table>
        <asp:HiddenField runat="server" ID="hfPageIndex" />
        <asp:HiddenField runat="server" ID="hfSearchQuery" />
      </ContentTemplate>
      
      <Triggers>
        <asp:AsyncPostBackTrigger ControlID="txtSearch" />
        <asp:AsyncPostBackTrigger ControlID="btnSearch" />
        <asp:AsyncPostBackTrigger ControlID="cmdPrev" />
        <asp:AsyncPostBackTrigger ControlID="cmdNext" />
      </Triggers>      
    </asp:UpdatePanel>
  </Content>
</orion:resourceWrapper>