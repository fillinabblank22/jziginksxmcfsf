﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Resources;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressHistory : BaseAssignedResourceControl
{
    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_20; }
    }

    public override string HelpLinkFragment
    {
        get { return BaseAssignedResourceControl.HelpPageIPAddressAsssignedHistory; }
    }

    #endregion // Resource Properties

    #region History Properties

    public override int PageIndex
    {
        get
        {
            if (this.hfPageIndex == null)
            {
                return 0;
            }

            return AbstractResourceEditPage.TryParse<int>(
                this.hfPageIndex.Value, ex => 0);
        }
        set
        {
            if (this.hfPageIndex != null)
            {
                this.hfPageIndex.Value = value.ToString();
            }
        }
    }

    public override string SearchQuery
    {
        get
        {
            if (this.hfSearchQuery == null)
            {
                return string.Empty;
            }
            return this.hfSearchQuery.Value;
        }
        set
        {
            if (this.hfSearchQuery != null)
            {
                this.hfSearchQuery.Value = value;
            }
        }
    }

    #endregion // History Properties

    #region Events

    protected void OnIndexChanging(object sender, CommandEventArgs e)
    {
        bool showAll = false;
        switch (e.CommandArgument.ToString().ToLowerInvariant())
        {
            case "next":
                this.PageIndex++;
                break;
            case "prev":
                this.PageIndex--;
                break;
            default:
                break;
        }

        LoadHistoryData(this.SearchQuery);
        ShowHistoryData(showAll);
    }

    protected void OnSearchClick(object sender, EventArgs e)
    {
        this.PageIndex = 0;

        if (this.txtSearch != null)
        {
            this.SearchQuery = this.txtSearch.Text;
        }

        //SetSearchString();
        LoadHistoryData(this.SearchQuery);
        ShowHistoryData(false);
    }

    #endregion // Events

    #region Methods

    protected override void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.IPAddress == null) || (this.History == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            BaseAssignedResourceControl.EditPageTemplate,
            this.Resource.ID, this.IpamIPAddress.NetObjectID);

        // detach resource link
        string timeperiod = this.TimePeriod.Serialize();
        this._DetachURL = string.Format(
            BaseAssignedResourceControl.DetachPageTemplate, this.Resource.ID,
            System.Web.HttpUtility.UrlEncode(this.IpamIPAddress.NetObjectID),
            System.Web.HttpUtility.UrlEncode(timeperiod),
            string.Empty
        );
        if (this.linkAll != null)
        {
            this.linkAll.NavigateUrl = this._DetachURL;
        }
    }

    protected override void InitResourceData()
    {
        if ((this.Resource == null) || (this.IPAddress == null) || (this.History == null))
        {
            return;
        }

        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
        if (resInfo.IsInReport)
        {
            this.Resource.Title = string.Format("{0} - {1}", this.Resource.Title, this.IpamIPAddress.Name);
        }

        this.PageIndex = 0;
        this.PageCount = 0;
        LoadHistoryData(string.Empty);

        // hide the resource if there is no history data
        if (this.AutoHide && (this.History.Count <= 0))
        {
            this.Visible = false;
            return;
        }

        ShowHistoryData(false);
    }

    protected void LoadHistoryData(string filter)
    {
        // set filter (search query)
        this.SearchQuery = filter;

        // handle preview resources page
        if (this.IPAddress.IpNodeId == IpamIPNode.PreviewNetObjectId)
        {
            this.PageIndex = 0;
            this.PageCount = 0;
            this.AutoHide = false;
            this.History = new List<AssignedHistory>();
            return;
        }

        // real and process whole assigned history
        int ip = this.IPAddress.IpNodeId.Value;
        List<AssignedHistory> history = AssignedHistoryManager.LoadIPAssignedHistory(
            ip, filter,
            this.TimePeriod.BeginDate.ToUniversalTime(),
            this.TimePeriod.EndDate.ToUniversalTime(),
            false);

        ProcessHistoryDate(history);
    }

    protected void ShowHistoryData(bool showAll)
    {
        if ((this.HistoryList == null) || (this.History == null))
        {
            return;
        }

        // resource content
        this.HistoryList.DataSource = this.History;
        this.HistoryList.DataBind();

        if ((this.History.Count <= 0) && (this.MsgNoItems != null))
        {
            this.MsgNoItems.Visible = true;
        }

        if (this.cmdPrev != null)
        {
            bool isFistPage = showAll || (this.PageIndex <= 0);
            this.cmdPrev.Enabled = !isFistPage;
            this.cmdPrev.CssClass = isFistPage ? "disabled" : "enabled";
        }
        if (this.cmdNext != null)
        {
            bool isLastPage = showAll || (this.PageIndex >= (this.PageCount - 1));
            this.cmdNext.Enabled = !isLastPage;
            this.cmdNext.CssClass = isLastPage ? "disabled" : "enabled";
        }
        if (this.MsgMaxItems != null)
        {
            this.MsgMaxItems.Visible = showAll && (this.PageCount > 1);
        }
    }

    #endregion // Methods
}
