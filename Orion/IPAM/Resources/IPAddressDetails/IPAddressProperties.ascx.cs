﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web;
using System.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressProperties : BaseResourceControl
{
    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_25; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMNodeProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceIPProperties"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region IPNode Properties

    protected IpamIPNode IpamIPAddress { get; set; }
    protected IPNode IPAddress { get; set; }
    protected string EditIPAddressURL { get; set; }

    private bool? isOperator = null;
    protected bool IsOperator
    {
        get
        {
            if (this.isOperator == null)
            {
                this.isOperator = AuthorizationHelper.AccessCheck(
                    this.Page, AccountRole.Operator,
                    new int[] { this.IPAddress.SubnetId });
            }
            return this.isOperator.Value;
        }
    }

    #endregion // IPNode Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);
        IIPAMNodeProvider ipamNodeProvider = GetInterfaceInstance<IIPAMNodeProvider>();
        if ((ipamNodeProvider != null) && (ipamNodeProvider.IPAddress != null))
        {
            this.IpamIPAddress = ipamNodeProvider.IPAddress;
            this.IPAddress = this.IpamIPAddress.IPAddress;
            InitResourceLinks();
            InitResourceData();
            if (resInfo.IsInReport)
            {
                this.Resource.Title = string.Format("{0} - {1}", this.Resource.Title, this.IpamIPAddress.Name);
                Control headerTemplate = IPAddressProperties.Controls[0].Controls[0];
                HtmlAnchor anchorPowerUser = headerTemplate.FindControl("EditServerUrlPowerUser") as HtmlAnchor;
                HtmlAnchor anchorNormalUser = headerTemplate.FindControl("EditServerUrlNormalUser") as HtmlAnchor;
                anchorPowerUser.Visible = false;
                anchorNormalUser.Visible = false;
            }
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.IPAddress == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditIPAddressDetails.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamIPAddress.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamIPAddress.NetObjectID);

        // edit ip address button
        if (this.IPAddress.IsIPv6 == false)
        {
            this.EditIPAddressURL = string.Format(
                "/Orion/IPAM/IP.Edit.aspx?SubnetId={0}&ipOrdinal={1}&{2}&nodeId={3}",
                this.IPAddress.SubnetId, this.IPAddress.IPOrdinal,
                PageIPEditor.IPAddressDetailParam, this.IPAddress.IpNodeId);
        }
        else
        {
            this.EditIPAddressURL = string.Format(
                "/Orion/IPAM/IPv6.IPAddress.Edit.aspx?ParentId={0}&ObjectId={1}&{2}",
                this.IPAddress.SubnetId, this.IPAddress.IpNodeId,
                PageIPv6AddressEditor.IPAddressDetailParam);
        }
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.IPAddress == null))
        {
            return;
        }

        // resource content
        if (this.IPAddressProperties != null)
        {
            List<CustomPropertyBindInfo> customProperties =
                PageIPv6EditorHelper.RetreiveCustomProperties(this.IPAddress);
            this.IPAddressProperties.DataSource = customProperties;
            this.IPAddressProperties.ItemDataBound +=
                new RepeaterItemEventHandler(OnIPAddressPropertiesDataBinding);
            this.IPAddressProperties.DataBind();
        }
    }

    private void OnIPAddressPropertiesDataBinding(object src, RepeaterItemEventArgs args)
    {
        if (this.IPAddress == null)
        {
            return;
        }

        RepeaterItem item = args.Item;
        if ((item.ItemType != ListItemType.Item) &&
            (item.ItemType != ListItemType.AlternatingItem))
        {
            return;
        }

        CustomPropertyBindInfo bindInfo = item.DataItem as CustomPropertyBindInfo;
        string propertyValue = bindInfo.ExistingValue;

        // don't show a row if no value exists
        HtmlTableRow container = item.FindControl("PropertyContainer") as HtmlTableRow;
        if ((container != null) && string.IsNullOrEmpty(propertyValue))
        {
            container.Visible = false;
            return;
        }

        Label name = item.FindControl("PropertyName") as Label;
        if (name != null)
        {
            name.Text = bindInfo.PropertyName;
        }

        Label value = item.FindControl("PropertyValue") as Label;
        value.Visible = true;
        value.Text = HttpUtility.HtmlEncode(propertyValue);
        value.Attributes.Add("ColumnName", bindInfo.PropertyName);
    }

    #endregion // Methods
}
