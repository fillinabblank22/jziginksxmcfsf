﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressConflict.ascx.cs" Inherits="Orion_IPAM_Resources_IPAddressConflict" %>

<%@ Register TagPrefix="orion" TagName="CustomQueryTable" Src="~/Orion/NetPerfMon/Controls/CustomQueryTable.ascx"  %>

<orion:resourceWrapper runat="server" ID="Wrapper" ShowEditButton="False">
    <Content>
        
        <style type="text/css">
            .MoveLeft {
                border-left: 1px dashed #cccccc;padding-left: 7px;
            }
            <%= "#Grid-" +  this.ScriptFriendlyResourceID %> img {
                padding-right:5px;
                vertical-align: bottom;
            }
            <%= "#Grid-" +  this.ScriptFriendlyResourceID %> a {
                vertical-align: bottom;
            }
            <%= "#helpBodyDiv-" + this.ScriptFriendlyResourceID %> .IPAM_HoverHelpTooltip {
                position: fixed;
                z-index: 90;
            }
            <%= "#helpText-" + this.ScriptFriendlyResourceID %> .IPAM_HoverHelpBody {
                font-style: normal;
                max-width: 250px;
                white-space: normal;
                z-index: 91;
            }
            .DisableShutdown {
                cursor: default;
                color: #A09D9D;
                opacity: 0.6;
                -moz-border-bottom-colors: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                border-image: none;
                border-style: none solid none none;
                border-width: medium 1px 0 0;
            }
            ul hr {
                height: 1px;
                border: none;
                border-top: dashed 1px #cccccc;
            }
        </style>

        <div ID="noConflictFoundMsg" runat="server" style="display: none">
            <table>
                <tr align="center">
                    <td style="border-bottom: 0; font-size: small;font-family: Arial,Verdana,Helvetica,sans-serif;"><img src="/Orion/IPAM/res/images/sw/ok_enabled.png" alt="" />&nbsp;<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_29 %></td>
                </tr>
            </table>
        </div>
        
        <div ID="enableConflict" runat="server" style="display: none">
            <table>
                <tr>
                    <td style="border-bottom: 0; font-size: small;font-family: Arial,Verdana,Helvetica,sans-serif;"><%= string.Format(Resources.IPAMWebContent.IPAMWEBCODE_SE_22,this.ScriptFriendlyResourceID) %></td>
                </tr>
            </table>
        </div>
        
        <div runat="server" id="resourceData">
            <table border="0" cellPadding="3" cellSpacing="0" width="97%" id="IpConflictData">

                <asp:Repeater runat="server" ID="IPAddressConflict">
                    <HeaderTemplate>
                        <tr>
                            <td style="width: 30%;border-bottom: 0;"></td>
                            <td style="width: 20%;border-bottom: 0;"></td>
                            <td style="width: 25%;border-bottom: 0;"></td>
                            <td style="width: 25%;border-bottom: 0;"></td>
                        </tr>
	                    <tr>
		                    <td style="border-bottom: 0;" colspan="4">
		                        <div class="sw-suggestion sw-suggestion-warn" id ="msgContainer" runat="server" style="width: 95%;">
		                            <span class="sw-suggestion-icon"></span><span id ="ConflictIpMsg" runat="server" style="font-weight: bold; font-size: small;"></span>
		                        </div>
		                    </td>
	                    </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="border-bottom: 0;" colspan="4"><div style="height: 5px;">&nbsp;</div></td>
                        </tr>
                        <tr id="Tr1" runat="server">
                            <td class="PropertyHeader" colspan="1"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_03 %></td>
                            <td class="Property" colspan="3">
                                <span style="color: red; font-weight: bold; font-size: small;"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_10 %></span>
                                <a href="javascript:void(0);" style="color: #336699;font-weight: bold; font-size: small;" runat="server" ID="IgnoreConflict"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_11 %></a>
                            </td>
	                    </tr>
                        <tr id="Tr2" runat="server">
                            <td class="PropertyHeader" colspan="1"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_04 %></td>
                            <td class="Property" colspan="3" >
                                <img style="vertical-align: bottom;" runat="server" id="conflictTypeIcon" src='<%# "/orion/ipam/res/images/sw/" +  Eval("ConflictTypeIcon") %>' />&nbsp;<%# DataBinder.Eval(Container.DataItem, "ConflictTypeText") %>
                            </td>
	                    </tr>
                        <tr id="Tr3" runat="server">
                            <td class="PropertyHeader" colspan="1"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_05 %></td>
                            <td class="Property" colspan="3"><span id="conflictAt"></span></td>
	                    </tr>
                        <%--Recommeded Action--%>
                        <tr id="Tr4" runat="server">
                            <td class="PropertyHeader" style="vertical-align: top;" colspan="1"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_06 %></td>
                            <td class="Property" colspan="3"><%# GetRecommendedAction(Container.DataItem) %></td>
	                    </tr>
                        <tr runat="server"><td colspan="4" style="border-bottom: 0;">
                            <table border="0" cellPadding="3" cellSpacing="0" width="100%" id="IpConflictDeviceData" >
                                <tr>
                                    <td style="width: 30%;border-bottom: 0;"></td>
                                    <td style="width: 35%;border-bottom: 0;"></td>
                                    <td style="width: 35%;border-bottom: 0;"></td>
                                </tr>
                                <tr id="Tr9" runat="server" style="border-bottom-style: solid" >
                                    <td style="border-bottom: 3px solid #cccccc;">&nbsp;</td>
                                    <td class="PropertyHeader MoveLeft" style="background-color: #E8E8E8;border-bottom: 3px solid #cccccc;"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_07 %></td>
                                    <td class="PropertyHeader MoveLeft" style="background-color: #E8E8E8;border-bottom: 3px solid #cccccc;"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_08 %></td>
	                            </tr>
                                <tr id="Tr5"  runat="server">
                                    <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_23 %></td>
                                    <td class="MoveLeft Property"><%# DataBinder.Eval(Container.DataItem, "AssignedSourceText") %></td>
                                    <td class="MoveLeft Property"><%# DataBinder.Eval(Container.DataItem, "ConflictingSourceText") %></td>
	                            </tr>
                                <tr id="Tr6"  runat="server">
                                    <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_24 %></td>
                                    <td class="MoveLeft Property">
                                        <img style="vertical-align: bottom;" onerror="this.src='/orion/ipam/res/images/default/s.gif'; return true;" runat="server" src='<%# "/NetPerfMon/images/Vendors/" + Eval("AssignMACVendIcon") %>' />&nbsp;<%# DataBinder.Eval(Container.DataItem, "AssignedMac") %>
                                    </td>
                                    <td class="MoveLeft Property">
                                        <img style="vertical-align: bottom;" onerror="this.src='/orion/ipam/res/images/default/s.gif'; return true;" runat="server" src='<%# "/NetPerfMon/images/Vendors/" + Eval("ConflictMACVendIcon") %>' />&nbsp;<%# DataBinder.Eval(Container.DataItem, "ConflictingMac") %>
                                    </td>
	                            </tr>

                                 <% if (this.AssignedSourceType == 8 || this.ConflictSourceType == 8) { %>
                                    <tr id="Tr12"  runat="server" >
                                        <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_25 %></td>
                                        <td class="MoveLeft Property">
                                           <%# RenderDhcpInfo(Container.DataItem,1,"Assigned") %> 
                                        </td>
                                        <td class="MoveLeft Property">
                                            <%# RenderDhcpInfo(Container.DataItem,1,"Conflict") %> 
                                        </td>
	                                </tr>
                                    <tr id="Tr13"  runat="server" >
                                        <td class="PropertyHeader">&nbsp;</td>
                                         <td class="MoveLeft Property">
                                            <%# RenderDhcpInfo(Container.DataItem,2,"Assigned") %>
                                        </td>
                                        <td class="MoveLeft Property">
                                            <%# RenderDhcpInfo(Container.DataItem,2,"Conflict") %>
                                        </td>
	                                </tr>
                                <% } %>

                                <% if (this.isConflictFound && this.isUdtInstalled) { %>
                                
                                     <% if (!isUdtCurrentInfoFound) { %>
                                        <tr id="Tr14"  runat="server" style="background-color: #E4F1F8;" >
                                            <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_26 %></td>
                                            <td class="MoveLeft Property" colspan="2" rowspan="4" style="text-align: center;"><b><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_38 %></b></td>
	                                    </tr>
                                        <tr id="Tr16"  runat="server" style="background-color: #E4F1F8;">
                                            <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_28 %></td>
	                                    </tr>
                                    <% } %>
                                    <% else { %>
                                        <tr id="Tr7"  runat="server" style="background-color: #E4F1F8;" >
                                            <td class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_26 %></td>
                                            <td class="MoveLeft Property" >
                                                <a href='<%# FormatCurrentUdtData(Container, "Assigned_HostNameUrl",string.Empty) %>' ><%# FormatCurrentUdtData(Container,"Assigned_HostName",string.Empty) %></a>
                                            </td>
                                            <td class="MoveLeft Property" >
                                                <a href='<%# FormatCurrentUdtData(Container, "Conflict_HostNameUrl",string.Empty) %>' ><%# FormatCurrentUdtData(Container,"Conflict_HostName",string.Empty) %></a>
                                            </td>
	                                    </tr>
                                        <tr id="Tr10"  runat="server" style="background-color: #E4F1F8;">
                                            <td rowspan="2" class="PropertyHeader"><%= Resources.IPAMWebContent.IPAMWEBCODE_SE_28 %></td>
                                            <td class="MoveLeft Property" style="border-bottom: 0;">
                                                <img id="Img2" style="vertical-align: bottom;" runat="server" src='<%# FormatCurrentUdtData(Container,"Assigned_PortIcon","/orion/ipam/res/images/default/s.gif") %>' />&nbsp;
                                                <a href='<%# FormatCurrentUdtData(Container, "Assigned_PortUrl",string.Empty) %>'><%# FormatCurrentUdtData(Container, "Assigned_PortName",string.Empty) %></a>
                                            </td>
                                            <td class="MoveLeft Property" style="border-bottom: 0;">
                                                <img id="Img1" style="vertical-align: bottom;" runat="server" src='<%# FormatCurrentUdtData(Container,"Conflict_PortIcon","/orion/ipam/res/images/default/s.gif") %>' />&nbsp;
                                                <a href='<%# FormatCurrentUdtData(Container, "Conflict_PortUrl",string.Empty) %>' ><%# FormatCurrentUdtData(Container,"Conflict_PortName",string.Empty) %></a>
                                            </td>
	                                    </tr>
                                        <tr id="Tr11" runat="server" style="background-color: #E4F1F8;">
                                            <td class="MoveLeft Property">
                                                <a href="javascript:void(0);" id="assignedShutdownLink" runat="server">
                                                    <img id="Img4" style="vertical-align: bottom;" runat="server" src='<%# FormatCurrentUdtData(Container,"Assigned_ShutdownIcon","/orion/ipam/res/images/default/s.gif") %>' />&nbsp;
                                                    <%= Resources.IPAMWebContent.IPAMWEBCODE_SE_21 %>
                                                </a>
                                                <span id="assignloading-<%= this.ScriptFriendlyResourceID %>"></span>
                                            </td>
                                            <td class="MoveLeft Property">
                                                <a href="javascript:void(0);" id="conflictShutdownLink" runat="server">
                                                    <img id="Img3" style="vertical-align: bottom;" runat="server" src='<%# FormatCurrentUdtData(Container,"Conflict_ShutdownIcon","/orion/ipam/res/images/default/s.gif") %>' />&nbsp;
                                                    <%= Resources.IPAMWebContent.IPAMWEBCODE_SE_21 %>
                                                </a>
                                                <span id="conflictloading-<%= this.ScriptFriendlyResourceID %>"></span>
                                            </td>
	                                    </tr>
                                    <% } %>
                                <% } %>
                            </table>
                        </td></tr>
                    </ItemTemplate>
                </asp:Repeater>
            
                <% if (this.isConflictFound && this.isUdtInstalled) { %>
                    <tr><td colspan="4" style="border-bottom: 0; padding-top: 20px;" >
                        <span style="font-weight: bold; font-size: small;"><%= string.Format(Resources.IPAMWebContent.IPAMWEBCODE_SE_13,this.HowManyUdtEvents) %></span>

                        <!-- Include CustomQueryTable control in your resource -->
                        <orion:CustomQueryTable runat="server" ID="CustomTable"/>
                    </td></tr>
                <% } %>

            </table>
        </div>

        <div id="alertDialog-<%= this.ScriptFriendlyResourceID %>" style="display:none;">
	        <div id="alertText-<%= this.ScriptFriendlyResourceID %>" style=" margin-left:3px;margin-top: 5px;"></div>
	        <div>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButtonLink runat="server" Text="YES" DisplayType="Primary" ID="Ok" />
		            <orion:LocalizableButtonLink runat="server" Text="NO" DisplayType="Secondary" ID="Cancel" />
                </div>
	        </div>
        </div>
        
         <div id="helpBodyDiv-<%= this.ScriptFriendlyResourceID %>" class="IPAM_HoverHelpTooltip" style="display: none; position: absolute;">
            <div class="IPAM_HoverHelpBody sw-suggestion" id="helpText-<%= this.ScriptFriendlyResourceID %>" style="padding: 5px 6px 6px 10px;"></div>
        </div>
        
        <script type="text/javascript">

            var showAlertDialog_<%= this.ScriptFriendlyResourceID %>, dialog_<%= this.ScriptFriendlyResourceID %>;
            var monthTxtMapping = { '0': 'Jan', '1': 'Feb', '2': 'Mar', '3': 'Apr', '4': 'May', '5': 'Jun', '6': 'Jul', '7': 'Aug', '8': 'Sep', '9': 'Oct', '10': 'Nov', '11': 'Dec' };

            function intializeDialog_<%= this.ScriptFriendlyResourceID %>() {
                
                dialog_<%= this.ScriptFriendlyResourceID %> = $("#alertDialog-<%= this.ScriptFriendlyResourceID %>").dialog({
                    autoOpen: false,
                    position: 'center',
                    draggable: true,
                    width: 500,
                    modal: true,
                    overlay: { "background-color": "black", opacity: 0.4 }, 
                    resizable: false,
                    open: function () {
                        //Workaround JQuery dialog issue - according to the jQuery UI ticket this will be fixed in version 1.9 by using the focusSelector option
                        $(this).find('input').attr("tabindex", "1");
                    },
                    title: "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_30 %>"
                });
                
                dialog_<%= this.ScriptFriendlyResourceID %>.css('overflow', 'hidden');

                $("#<%= Cancel.ClientID %>").click(function () {
                    $("#alertDialog-<%= this.ScriptFriendlyResourceID %>").dialog("close");
                });

                $('#alertDialog-<%= this.ScriptFriendlyResourceID %>').bind('dialogclose', function (event) {
                    //Clear Value
                });
            }

            //statusType = 1 - Ignore
            //statusType = 2 - Show
            //statusType = 3 - ShutDown
            function FireAlert_<%= this.ScriptFriendlyResourceID %>(config, callbackFn,that) {
                
                //Once Shutdown clicked then not allow it again untill finish
                if ($(that).hasClass("DisableShutdown") && (config.statusType && config.statusType == '3'))
                    return false;
                
                if (CheckDemoMode())
                    return false;

                intializeDialog_<%= this.ScriptFriendlyResourceID %>();
                dialog_<%= this.ScriptFriendlyResourceID %>.off("dialogclose");

                $("#<%= Ok.ClientID %>").unbind( "click" );
                
                $("#<%= Ok.ClientID %>").click(function () {
                    $("#alertDialog-<%= this.ScriptFriendlyResourceID %>").dialog("close");
                    
                    if (callbackFn != null) {
                        var ret = false;
                        
                        $(that).addClass("DisableShutdown");
                        var progress = $(that).next();
                        if (progress)
                            progress.empty().html('<img style="padding-left: 10px;padding-right: 7px;" src="/Orion/images/loading_gen_small.gif" />In Progress...');
                        
                        if (callbackFn() == false)
                            ret = false;
                        else
                            ret = true;
                        
                        if (progress)
                            progress.html('');
                        
                        $(that).removeClass("DisableShutdown");
                        return ret;
                    }
                    UpdateConflictStatus_<%= this.ScriptFriendlyResourceID %>(config.conflictId, config.updateValue, config.statusType);
                });
                
                dialog_<%= this.ScriptFriendlyResourceID %>.dialog('option', 'title', (config.statusType == '1' || config.statusType == '2') ? "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_30 %>" : "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_32 %>");
                $('#alertText-<%= this.ScriptFriendlyResourceID %>').text((config.statusType == '1' || config.statusType == '2') ? "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_31 %>" : String.format("<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_33 %>",config.macAddress));
                dialog_<%= this.ScriptFriendlyResourceID %>.dialog("open");
            }

            function UpdateConflictStatus_<%= this.ScriptFriendlyResourceID %>(conflictId, conflictStatus, statusType) {

                if (CheckDemoMode())
                    return false;
                
                var isSuccess = false;
                $.ajax({
                    type: "POST",
                    url: "/Orion/IPAM/ExtCmdProvider.ashx",
                    success: function (result, request) {
                        var res = eval("(" + result + ")");
                        if (res.success == true) {
                            isSuccess = true;
                        } 
                        else if (res.success == false) {
                            alert(res.message);
                        }
                    },
                    error: function (error) {
                        isSuccess = false;
                    },
                    async: false,
                    data:
                    {
                        entity: 'IPAM.IpConflict',
                        verb: 'UpdateIpConflictStatus',
                        ids: conflictId,
                        status: conflictStatus
                    }
                });

                UpdateConflictResourceData_<%= this.ScriptFriendlyResourceID %>(isSuccess, statusType);
                return isSuccess;
            }
            
            function UpdateConflictDetectTime() {
                if ($('#conflictAt')) {
                        var dt = new Date();
                        var diff = Math.abs(dt - Date.parse($SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.ConflictAt));
                    
                        var eventTimeDiff = "0 second";
                        if (Math.round(diff / 86400000)) {
                            eventTimeDiff = Math.round(diff / 86400000) + " day(s)";
                        } else if (Math.floor(diff / 3600000)) {
                            eventTimeDiff = Math.round(diff / 3600000) + " hour(s)";
                        } else if (Math.floor(diff / 60000)) {
                            eventTimeDiff = Math.round(diff / 60000) + " minute(s)";
                        } else if (Math.floor(diff / 1000)) {
                            eventTimeDiff = Math.round(diff / 1000) + " second(s)";
                        } else {
                            //Do Nothing
                        }
                        $('#conflictAt').text(String.format("<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_12 %>", eventTimeDiff, $SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.FormattedConflictTime));
                    }
                }
            
            function UpdateConflictResourceData_<%= this.ScriptFriendlyResourceID %>(isSuccess, statusType) {
                //statusType = 1 - Ignore
                //statusType = 2 - Show
                //statusType = 3 - ShutDown
                if (isSuccess == true) {
                    UpdateConflictDetectTime();
                    
                    //Hide: Recommended Action detailed Information
                    if ($('.detailedInfo'))
                        $('.detailedInfo').hide();
                    
                    //Show: Link for see more actions
                    if ($('#showMoreInfo'))
                        $('#showMoreInfo').show();
                    
                    if (statusType == '1') {
                        $("#<%= resourceData.ClientID %>").hide(0);
                        $("#<%= enableConflict.ClientID %>").show(0);
                    }
                    else if (statusType == '2') {
                        $("#<%= resourceData.ClientID %>").show(0);
                        $("#<%= enableConflict.ClientID %>").css({"display":"none"});
                        $("#<%= enableConflict.ClientID %>").hide(0);

                        RefreshUdtEvents_<%= this.ScriptFriendlyResourceID %>();
                    }
                    else if (statusType == '3') {
                        $("#<%= resourceData.ClientID %>").hide(0);
                        $("#<%= enableConflict.ClientID %>").css({"display":"none"});
                        $("#<%= enableConflict.ClientID %>").hide(0);
                        $("#<%= noConflictFoundMsg.ClientID %>").show(0);
                    }
                }
            }
            
            function CheckDemoMode() {
                var isDemo = "<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>";

                if (isDemo && isDemo == "True") {
                    demoAction('IPAM_IP_Conflict_Resource', null);
                    return true;
                }

                return false;
            }
            
            function DoPortShutdown_<%= this.ScriptFriendlyResourceID %>(nodeId, portId, conflictId, conflictStatus, statusType) 
            {
                if (CheckDemoMode())
                    return false;

                var isSuccess = false;
                $.ajax({
                    type: "POST",
                    url: "/Orion/IPAM/ExtCmdProvider.ashx",
                    success: function (result, request) {
                        var res = eval("(" + result + ")");
                        if (res.success == true) {
                            isSuccess = true;
                        }
                        else if (res.success == false) {
                            alert(res.message);
                        }
                    },
                    error: function (error) {
                        isSuccess = false;
                    },
                    async: false,
                    data:
                    {
                        entity: 'IPAM.IpConflict',
                        verb: 'PortShutdown',
                        ids: conflictId,
                        status: conflictStatus,
                        nodeid: nodeId,
                        portid: portId
                    }
                });

                UpdateConflictResourceData_<%= this.ScriptFriendlyResourceID %>(isSuccess, statusType);
                return isSuccess;
            }
            
            $(function () {
                SW.Core.Resources.CustomQuery.initialize(
                    {
                        // This must be unique ID for the whole page. It should be the same as CustomTable uses.
                        uniqueId: "<%= this.ScriptFriendlyResourceID %>",
                        // Which page should resource display when loaded
                        initialPage: 0,
                        allowSort: true,
                        allowSearch: false,
                        rowsPerPage: "<%= this.HowManyUdtEvents %>",
                        columnSettings: {
                            // Use column name (or alias if it's defined) from SWQL query to define which column this setting applies to. Column name is case sensitive.
                            "EventTime": {
                                header: "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_14 %>"
                            },
                            "MACAddress": {
                                header: "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_15 %>",
                                formatter: function (value, row, cellinfo) {
                                    var macIcon = "<img onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\" src='" + row[2] + "'/>";
                                    if (parseInt(row[18]) > 0)
                                        return (isValueExists(row[15])) ? (macIcon + "<a href='" + row[3] + "'>" + row[1] + "</a>") : '';
                                    else if (parseInt(row[18]) == -1) {
                                        var userUrl = "<a href='" + row[11] + "'>" + row[10] + "</a>";
                                        return (isValueExists(row[10])) ? String.format("<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_41 %>", userUrl) : '';
                                    }
                                    else if (parseInt(row[18]) == -2) {
                                        var hostUrl = "<a href='" + row[20] + "'>" + row[19] + "</a>";
                                        return (isValueExists(row[18])) ? hostUrl : '';
                                    }

                                },
                                isHtml: true
                            },
                            "PortName": {
                                header: "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_16 %>",
                                formatter: function (value, row, cellinfo) {
                                    var portIcon = "<img onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\" src='" + row[5] + "'/>";
                                    return isValueExists(row[4]) ? (portIcon + "<a href='" + row[6] + "'>" + row[4] + "</a>") : '';
                                },
                                isHtml: true
                            },
                            "NodeName": {
                                header: "<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_17 %>",
                                formatter: function (value, row, cellinfo) {
                                    var nodeIcon = "<img onerror=\"this.src='/orion/ipam/res/images/default/s.gif'; return true;\" src='" + row[8] + "'/>";
                                    return (isValueExists(row[13]) && parseInt(row[13]) >= 0) ? (nodeIcon + ((row[9] != null) ? "<a href='" + row[9] + "'>" + row[7] + "</a>" : row[7])) : '';
                                },
                                isHtml: true
                            }
                        }
                    });
                
                    $('#OrderBy-' + "<%= ScriptFriendlyResourceID %>").val('[EventTime] DESC');
            });
            
            function isValueExists(val) {
                if (!val || val == '' || typeof val == 'undefined')
                    return false;
                else
                    return true;
            }
            function RefreshUdtEvents_<%= this.ScriptFriendlyResourceID %>() {
                if ($SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.isConflictFound == 'True' && $SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.isUdtInstalled == 'True') {
                    // This reloads data for this resource (see the same ID as we used for uniqueId in initialize method)
                    var refresh = function() { SW.Core.Resources.CustomQuery.refresh(<%= ScriptFriendlyResourceID %>); };

                    // This registers resource in view refresh routine
                    SW.Core.View.AddOnRefresh(refresh, '<%= CustomTable.ClientID %>');

                    // And finally loads initial data
                    refresh();
                }
            }
            
            function showtip_<%= this.ScriptFriendlyResourceID %>(that, text) {
                $('#helpBodyDiv-<%= this.ScriptFriendlyResourceID %>').css('left', $(that).position().left + 15);
                $('#helpBodyDiv-<%= this.ScriptFriendlyResourceID %>').css('top', $(that).position().top + 20);
                $('#helpText-<%= this.ScriptFriendlyResourceID %>').html(text);
                $('#helpBodyDiv-<%= this.ScriptFriendlyResourceID %>').fadeIn("fast");
            }
            
            function hidetip_<%= this.ScriptFriendlyResourceID %>(that) {
                $('#helpBodyDiv-<%= this.ScriptFriendlyResourceID %>').fadeOut("fast");
            }
           
            $( document ).ready(function() {

                var dateObj = new Date();
                var dt = new Date($SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.ConflictAt);
                var confTime = dt.toLocaleString();

                $SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.ConflictAt = dt;
                $SW.IPAM.IPConflictResource_<%= this.ScriptFriendlyResourceID %>.FormattedConflictTime = confTime;

                UpdateConflictDetectTime();
                
                if ($('.detailedInfo'))
                    $('.detailedInfo').hide();
                
                $("#showMoreInfo").on('click',function(e){
                    $('#showMoreInfo').hide();
                    $('.detailedInfo').show();
                    return false;
                });
                
                RefreshUdtEvents_<%= this.ScriptFriendlyResourceID %>();
                
                //Put N/A if href & text is Empty
                $('#IpConflictDeviceData a').each(function () {
                    if ($.trim($(this).text()) == '' && $(this).attr('href') == '')
                        $(this).parent().html('<%= Resources.IPAMWebContent.IPAMWEBCODE_SE_37 %>');
                    else if ($(this).attr('href') == '') {
                        $(this).attr('href', 'javascript:void(0)');
                    }
                });
                
            });

        </script>

    </Content>
</orion:resourceWrapper>
