﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.UI;
using SolarWinds.IPAM.Web.Master;
using System.Security.Principal;
using SolarWinds.IPAM.Common.Security;
using SolarWinds.IPAM.Client;
using System.Data;
using System.Text;
using System.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Extensions;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "true")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Details)]
public partial class Orion_IPAM_Resources_IPAddressDetails : BaseResourceControl
{
    #region Constants

    private const string EIM_DHCPLEASE = "DhcpLease";

    /// <summary>
    /// This list define the IPNode properties and they order.
    /// </summary>
    public static string[] DisplayProperties = new string[]
	{
        //IPNode.EIM_STATUS,
		IPNode.EIM_ALLOCPOLICY,
        IPNode.EIM_IPADDRESS,
        IPNode.EIM_IPMAPPED,
        IPNode.EIM_SKIPSCAN,
        IPNode.EIM_DNSBACKWARD,
        IPNode.EIM_DHCPCLIENTNAME,
        EIM_DHCPLEASE,
        IPNode.EIM_MAC,
		IPNode.EIM_ALIAS,
        IPNode.EIM_COMMENTS,
        IPNode.EIM_MACHINETYPE,
        IPNode.EIM_VENDOR,
        IPNode.EIM_SYSNAME,
        IPNode.EIM_DESCRIPTION,
        IPNode.EIM_CONTACT,
        IPNode.EIM_LOCATION,
        IPNode.EIM_LASTSYNC,
        IPNode.EIM_RESPONSETIME,
        //IPNode.EIM_LASTCREDENTIAL,
		IPNode.EIM_LEASEEXPIRES
	};

    #endregion // Constants

    #region Resource Properties

    protected override string DefaultTitle
    {
        get { return Resources.IPAMWebContent.IPAMWEBCODE_VB1_23; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(IIPAMNodeProvider) }; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionIPAMPHResourceIPDetails"; }
    }

    private string _EditURL;
    public override string EditURL
    {
        get { return this._EditURL; }
    }

    private string _DetachURL;
    public override string DetachURL
    {
        get { return this._DetachURL; }
    }

    #endregion // Resource Properties

    #region IPNode Properties

    protected IpamIPNode IpamIPAddress { get; set; }
    protected IPNode IPAddress { get; set; }
    protected string EditIPAddressURL { get; set; }

    private bool? isOperator = null;
    protected bool IsOperator
    {
        get
        {
            if (!this.isOperator.HasValue)
            {
                this.isOperator = AuthorizationHelper.AccessCheck(
                    this.Page, AccountRole.Operator,
                    new int[] { this.IPAddress.SubnetId });
            }
            return this.isOperator.Value;
        }
    }

    protected bool? _hasDhcpReservation = null;
    protected bool HasDhcpReservation
    {
        get
        {
            if (!this._hasDhcpReservation.HasValue)
            {
                this._hasDhcpReservation = CheckDhcpreservation(this.IPAddress);
            }
            return this._hasDhcpReservation.Value;
        }
    }

    #endregion // IPNode Properties

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        IIPAMNodeProvider ipamNodeProvider = GetInterfaceInstance<IIPAMNodeProvider>();

        ResourceInfo resInfo = ResourceManager.GetResourceByID(this.Resource.ID);

        if ((ipamNodeProvider != null) && (ipamNodeProvider.IPAddress != null))
        {
            this.IpamIPAddress = ipamNodeProvider.IPAddress;
            this.IPAddress = this.IpamIPAddress.IPAddress;

            InitResourceLinks();
            InitResourceData();
            if (resInfo.IsInReport)
            {
                Control headerTemplate = IPAddressProperties.Controls[0].Controls[0];
                HtmlAnchor anchorPowerUser = headerTemplate.FindControl("EditServerUrlPowerUser") as HtmlAnchor;
                HtmlAnchor anchorNormalUser = headerTemplate.FindControl("EditServerUrlNormalUser") as HtmlAnchor;
                anchorPowerUser.Visible = false;
                anchorNormalUser.Visible = false;
            }
        }
    }

    #endregion // Events

    #region Methods

    private void InitResourceLinks()
    {
        if ((this.Resource == null) || (this.IPAddress == null))
        {
            return;
        }

        // edit resource button
        this._EditURL = string.Format(
            "/Orion/IPAM/Resources/EditIPAddressDetails.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamIPAddress.NetObjectID);

        // resource title link
        this._DetachURL = string.Format(
            "/Orion/DetachResource.aspx?ResourceID={0}&NetObject={1}",
            this.Resource.ID, this.IpamIPAddress.NetObjectID);

        // edit ip address button
        if (this.IPAddress.IsIPv6 == false)
        {
            this.EditIPAddressURL = string.Format(
                "/Orion/IPAM/IP.Edit.aspx?SubnetId={0}&ipOrdinal={1}&{2}&nodeId={3}",
                this.IPAddress.SubnetId, this.IPAddress.IPOrdinal,
                PageIPEditor.IPAddressDetailParam,this.IPAddress.IpNodeId);
        }
        else
        {
            this.EditIPAddressURL = string.Format(
                "/Orion/IPAM/IPv6.IPAddress.Edit.aspx?ParentId={0}&ObjectId={1}&{2}",
                this.IPAddress.SubnetId, this.IPAddress.IpNodeId,
                PageIPv6AddressEditor.IPAddressDetailParam);
        }
    }

    private void InitResourceData()
    {
        if ((this.Resource == null) || (this.IPAddress == null))
        {
            return;
        }

        // resource content
        if (this.IPAddressProperties != null)
        {
            this.IPAddressProperties.DataSource = DisplayProperties;
            this.IPAddressProperties.ItemDataBound += new RepeaterItemEventHandler(OnIPAddressPropertiesDataBinding);
            this.IPAddressProperties.DataBind();
        }
    }

    private void OnIPAddressPropertiesDataBinding(object src, RepeaterItemEventArgs args)
    {
        if (this.IPAddress == null)
        {
            return;
        }

        RepeaterItem item = args.Item;
        if ((item.ItemType != ListItemType.Item) &&
            (item.ItemType != ListItemType.AlternatingItem))
        {
            return;
        }

        string property = item.DataItem as string;
        string propertyName = GetPropertyTextName(property);
        string propertyValue = GetPropertyTextValue(this.IPAddress, property);

        // correct visualise some of the values
        switch (property)
        {
            case IPNode.EIM_SKIPSCAN:
                propertyName = Resources.IPAMWebContent.IPAMWEBCODE_VB1_24;
                propertyValue = (this.IPAddress.SkipScan) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_311: Resources.IPAMWebContent.IPAMWEBDATA_VB1_312;
                break;
            case IPNode.EIM_LASTSYNC:
                if (string.IsNullOrEmpty(propertyValue))
                    propertyValue = Resources.IPAMWebContent.IPAMWEBDATA_VB1_346;
                else
                    // Convert the Start date to user selected time zone value.
                    propertyValue = Convert.ToDateTime(propertyValue).ConvertToDisplayDate().ToString();
                break;
            case IPNode.EIM_RESPONSETIME:
                if (string.IsNullOrEmpty(propertyValue)) propertyValue = Resources.IPAMWebContent.IPAMWEBDATA_VB1_348;
                break;
            case IPNode.EIM_LASTCREDENTIAL:
                if (string.IsNullOrEmpty(propertyValue)) propertyValue = Resources.IPAMWebContent.IPAMWEBDATA_VB1_58;
                break;
            case IPNode.EIM_LEASEEXPIRES:
                if (string.IsNullOrEmpty(propertyValue))
                    propertyValue = Resources.IPAMWebContent.IPAMWEBDATA_VB1_351;
                else
                    // Convert the Start date to user selected time zone value.
                    propertyValue = Convert.ToDateTime(propertyValue).ConvertToDisplayDate().ToString();
                break;
            case EIM_DHCPLEASE:
                propertyValue = this.HasDhcpReservation ? Resources.IPAMWebContent.IPAMWEBDATA_AK1_281: Resources.IPAMWebContent.IPAMWEBDATA_AK1_282;
                break;
            case IPNode.EIM_VENDOR:
                string vendorIconPath = GetPropertyTextValue(this.IPAddress, IPNode.EIM_VENDORICON);
                Image value1 = item.FindControl("Icon") as Image;
                if (value1 != null && !string.IsNullOrWhiteSpace(vendorIconPath))
                {
                    string relativePath = string.Concat("/NetPerfmon/images/Vendors/", vendorIconPath);
                    if (File.Exists(Server.MapPath(relativePath)))
                    {
                        value1.ImageUrl = relativePath;
                        value1.Visible = true;
                    }
                }

                break;
            case IPNode.EIM_IPMAPPED:
                propertyName = this.IPAddress.IPAddress.Contains(":") ? Resources.IPAMWebContent.IPAMWEBDATA_SE_28 : propertyName;
                if (this.IPAddress.IPAddress.Contains(":"))
                    propertyValue = GetIPv4DualStackAddress(this.IPAddress);
                break;
            default:
                if (string.IsNullOrEmpty(propertyValue)) propertyValue = " ";
                break;
        }

        Label name = item.FindControl("PropertyName") as Label;
        if (name != null)
        {
            name.Text = propertyName;
        }

        Label value = item.FindControl("PropertyValue") as Label;
        if (value != null)
        {
            value.Text = HttpUtility.HtmlEncode(propertyValue);
        }
    }

    protected string GetPropertyTextName(string property)
    {
        return LocalizationHelper.GetEntityDisplayString(
            IPNode.EIM_ENTITYNAME.Replace('.', '_'), property);
    }

    protected string GetPropertyTextValue<T>(T obj, string property)
    {
        System.Reflection.PropertyInfo propertyInfo = typeof(T).GetProperty(property);
        if (propertyInfo == null)
        {
            //throw new ArgumentException(string.Format(
            //    "Property '{0}' does not exist for type IPNode.", property));
            return null;
        }

        object propertyValue = propertyInfo.GetValue(obj, null);
        if (propertyValue != null)
        {
            switch (property)
            {
                case "Status":
                    return GetLocalizedProperty("IPStatus", propertyValue.ToString());
                case "AllocPolicy":
                    return GetLocalizedProperty("IPType", propertyValue.ToString());
                default:
                    return propertyValue.ToString();
            }
        }
        else
            return string.Empty;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
    }

    private bool CheckDhcpreservation(IPNode ipNode)
    {
        bool result = false;
        StringBuilder swqlQuery = new StringBuilder();
        swqlQuery.AppendFormat("SELECT L.{0} FROM {1} AS L",
            DhcpLease.EIM_CLIENTSTATUS, DhcpLease.EIM_ENTITYNAME);
        swqlQuery.AppendFormat(" JOIN {0} AS S ON S.{1} = L.{2}",
            DhcpScope.EIM_ENTITYNAME, DhcpScope.EIM_SCOPEID, DhcpLease.EIM_SCOPEID);
        swqlQuery.AppendFormat(" WHERE S.{0} = '{1}' AND L.{2} = '{3}'",
            DhcpScope.EIM_SUBNETID, ipNode.SubnetId,
            DhcpLease.EIM_CLIENTIPADDRESSN, ipNode.IPAddressN);

        try
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                DataTable dt = proxy.SwisProxy.Query(swqlQuery.ToString());
                if (dt != null && dt.Rows.Count > 0)
                {
                    object clientstatus = dt.Rows[0].ItemArray.GetValue(0);
                    result = ((System.Byte)clientstatus == 2);
                }
            }
        }
        catch (Exception ex)
        {
            SolarWinds.IPAM.Web.Common.Utility.ExceptionHandlerSWIS.HandlePageException(ex, null);
        }

        return result;
    }

    private string GetIPv4DualStackAddress(IPNode ipNode)
    {
        string result = string.Empty;

        string swqlQuery = "SELECT TOP 1 N.IPAddress FROM IPAM.IPNode AS N WHERE N.SubnetId IS NOT NULL AND N.SubnetId <> 1 AND N.IPNodeId IS NOT NULL AND N.IPMappedN = '{0}'";

        try
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                DataTable dt = proxy.SwisProxy.Query(string.Format(swqlQuery,ipNode.IPAddressN));
                if (dt != null && dt.Rows.Count > 0)
                {
                    object clientstatus = dt.Rows[0].ItemArray.GetValue(0);
                    result = Convert.ToString(clientstatus);
                }
            }
        }
        catch (Exception ex)
        {
            SolarWinds.IPAM.Web.Common.Utility.ExceptionHandlerSWIS.HandlePageException(ex, null);
        }

        return result;
    }

    #endregion // Methods
}
