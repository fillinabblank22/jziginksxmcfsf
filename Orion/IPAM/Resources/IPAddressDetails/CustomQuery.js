if (!window.$SW) window.$SW = {};
if (!$SW.IPAM) $SW.IPAM = {};
if (!$SW.IPAM.Resources) $SW.IPAM.Resources = {};
if (!$SW.IPAM.Resources.CustomQuery) $SW.IPAM.Resources.CustomQuery = {};

(function(cq, lut) {

    var PageManager = function(pageIndex, pageSize) {
        this.currentPageIndex = pageIndex;
        this.rowsPerPage = pageSize;

        this.startItem = function() {
            return (this.rowsPerPage * this.currentPageIndex) + 1;
        };

        this.lastItem = function() {
            // Ask for rowsPerPage + 1 rows so we can detect if this is the last page.
            return (this.rowsPerPage * (this.currentPageIndex + 1)) + 1;
        };

        this.withRowsClause = function() {
            return " WITH ROWS " + this.startItem() + " TO " + this.lastItem();
        };

        this.isLastPage = function(rowCount) {
            // We asked for rowsPerPage + 1 rows.  If more than rowsPerPage rows 
            // got returned, we know this isn't the last page.
            return rowCount <= this.rowsPerPage;
        };
    };

    var generateColumnInfo = function(columns) {
        var columnInfo = [];
        var indexLookup = {};

        // Map column text to its index.
        $.each(columns, function(index, name) {
            indexLookup[name] = index;
            columnInfo[index] = { };
        });
        
        // Lookup each column's formatting information.
        $.each(columns, function(index, name) {
            columnInfo[index].header = ((name != null) && (name.substring(0, 1) !== '_')) ? name : null;

            var otherColumnIndex = getReferencedColumnIndex(name, '_IconFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].iconColumn = index;
            }
            
            otherColumnIndex = getReferencedColumnIndex(name, '_LinkFor_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].linkColumn = index;
            }
            
            otherColumnIndex = getReferencedColumnIndex(name, '_DnsRecordType_', indexLookup);
            if (otherColumnIndex != null) {
                columnInfo[otherColumnIndex].dnsRecordType = index;
            }
        });
        
        return columnInfo;
    };

    var getReferencedColumnIndex = function(name, prefix, columnIndexLookup) {
        var re = new RegExp('^' + prefix + '(.+)$', 'i');
        var match = name.match(re);
        return match ? columnIndexLookup[match[1]] : null;
    };

    var renderCell = function(cellValue, rowArray, columnInfo) {
        var cell = $('<td/>');
        if (cellValue == null)
        {
           cellValue = "";
        }
        else if (columnInfo.dnsRecordType) {
           cellValue = lut[cellValue];
		}
        else if (Date.isInstanceOfType(cellValue)) {
            cellValue = cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + cellValue.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern); 
        }
         
        if (columnInfo.iconColumn) {
            $('<img/>').attr('src', rowArray[columnInfo.iconColumn]).appendTo(cell);
        }
        
        if (columnInfo.linkColumn) {
            $('<a/>').attr('href', rowArray[columnInfo.linkColumn])
                     .text(cellValue)
                     .appendTo(cell);
        } else {
            $('<span/>').text(cellValue).appendTo(cell);
        }

        return cell;
        
    };

    var updatePagerControls = function(resourceId, pageManager, rowCount) {
        var pager = $('#Pager-' + resourceId);
        var pageIndex = pageManager.currentPageIndex;
        var html = [];

        var previousText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_PREVIOUS;E=js}'; //Previous
        var nextText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_NEXT;E=js}'; //Next
        var showAllText = '@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_SHOWALL;E=js}'; //Show all
        
        var style = 'style="vertical-align:middle"';
        var previousImgRoot = '/Orion/images/Arrows/button_paging_previous';
        var nextImgRoot = '/Orion/images/Arrows/button_paging_next';

        var showAll = showAllText + ' <img src="/Orion/images/Arrows/show_all.gif" style="vertical-align:middle"/>';
        var haveLinks = false;

        var startHtml;
        var endHtml;
        var contents;
        
        if (pageIndex > 0) {
            startHtml = '<a href="#" class="previousPage">';
            contents = String.format('<img src="{0}.gif" {1}/> {2}', previousImgRoot, style, previousText);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = String.format('<img src="{0}_disabled.gif" {1}/> {2}', previousImgRoot, style, previousText);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push(' | ');
        
        if (!pageManager.isLastPage(rowCount)) {
            startHtml = '<a href="#" class="nextPage">';
            contents = String.format('{2} <img src="{0}.gif" {1}/>', nextImgRoot, style, nextText);
            endHtml = '</a>';
            haveLinks = true;
        } else {
            startHtml = '<span style="color:#646464;">';
            contents = String.format('{2} <img src="{0}_disabled.gif" {1}/>', nextImgRoot, style, nextText);
            endHtml = '</span>';
        }

        html.push(startHtml + contents + endHtml);
        html.push('<a href="#" class="showAll">' + showAll + '</a>');

        pager.empty().append(html.join(' '));
        var method = haveLinks ? 'show' : 'hide';
        pager[method]();
       
        pager.find('.previousPage').click(function() {
            cq.createTableFromQuery(resourceId, pageIndex - 1, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.nextPage').click(function() {
            cq.createTableFromQuery(resourceId, pageIndex + 1, pageManager.rowsPerPage);
            return false;
        });

        pager.find('.showAll').click(function() {
            // We don't have a good way to show all.  We'll show 1 million and 
            // accept that there's an issue if there are more than that :)
            cq.createTableFromQuery(resourceId, 0, 1000000);
            return false;
        });
    };
    
    cq.createTableFromQuery = function(resourceId, pageIndex, pageSize) {
        var swql = $('#SWQL-' + resourceId).val();
        var errorMsg = $('#ErrorMsg-' + resourceId);
        var grid = $('#Grid-' + resourceId);
        
        if (swql.trim().length === 0)
        {
            errorMsg.text('@{R=Core.Strings;K=WEBJS_TM1_CUSTOMQUERY_NOQUERY;E=js}').show();            
            return;
        }

        errorMsg.hide();
       
        var pageManager = new PageManager(pageIndex, pageSize);
        var swqlUpperCase = swql.toUpperCase();
        if (swqlUpperCase.indexOf("ORDER BY") === -1) {
            var commaIndex = swqlUpperCase.indexOf(",");
            var fromIndex = swqlUpperCase.indexOf("FROM");
            var selectIndex = swqlUpperCase.indexOf("SELECT");
            var endIndex = commaIndex;
            if (endIndex == -1)
            {
                endIndex = fromIndex;
            }

            var startIndex = selectIndex + 6;
            if (startIndex < endIndex) {
              var orderColumn = swql.substring(startIndex, endIndex).trim();
              var orderColumnBackspaceIndex =  orderColumn.indexOf(" ");
              if (orderColumnBackspaceIndex > -1)
              {
                 orderColumn = orderColumn.substring(0, orderColumnBackspaceIndex);
              }
              
              swql += "ORDER BY " + orderColumn;
            }
            else 
            {
                swql += " ORDER BY 1 ";
            }
        }
        
        swql += pageManager.withRowsClause();
        Information.Query(swql, function(result) {
            var headers = $('tr.HeaderRow', grid);

            headers.empty();
            grid.find('tr:not(.HeaderRow)').remove();
            
            var columnInfo = generateColumnInfo(result.Columns);
            $.each(columnInfo, function(colIndex, column) {
                if (column.header !== null) {
                    $('<td/>')
                        .addClass('ReportHeader')
                        .css('text-transform', 'uppercase')
                        .text(GetLocalizedColumnName(column.header))
                        .appendTo(headers);
                }
            });

            function GetLocalizedColumnName(value) {
                switch (value) {
                case 'Data':
                    return '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_279;E=js}';
                case 'Name':
                    return '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_278;E=js}';
                case 'Type':
                    return '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_280;E=js}';
                default:
                    return value;
                }
            }
            
            var rowsToOutput = result.Rows.slice(0, pageManager.rowsPerPage);
            
            $.each(rowsToOutput, function(rowIndex, row) {
                var tr = $('<tr/>');

                $.each(row, function(cellIndex, cell) {
                    var info = columnInfo[cellIndex];
                    if (info.header !== null) 
                        renderCell(cell, row, info).appendTo(tr);
                });
                grid.append(tr);
            });

            updatePagerControls(resourceId, pageManager, result.Rows.length);

        }, function(error) {
            errorMsg.text(error.get_message()).show();
        });
    };

})($SW.IPAM.Resources.CustomQuery, $SW.IPAM.dnsRecordTypes);