<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.Summary.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportSummary"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_342 %>" %> 
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
    
<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewImportSummary" />
</asp:Content>
    
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck ID ="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock runat="server">
#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
</IPAMmaster:CssBlock>

<div style="margin-left: 10px;">

    <table width="auto" style="table-layout: fixed;  margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb">
      <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	  <h1><%= Page.Title %></h1>
    </td>
    </tr>
    </table>

    <div id="formbox">

        <div style="margin: 0px 0px 20px 0px;">
            <asp:Literal ID="okheader" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_343 %>" />
            <asp:Literal ID="notokheader" visible="false" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_344 %>" />
        </div>

        <div id="formheader" runat="server" class="sw-form-label">
            <ul style="list-style: disc inside;">
                <li><asp:Label ID="importedNodes" runat="server" /></li>
            </ul>
        </div>

        <div id="ignoredRowsDiv" runat="server" class="sw-form-label" visible="false">
            <ul style="list-style: disc inside;">
                <li><asp:Label ID="ignoredRows" runat="server" />&nbsp;&nbsp;&nbsp;<orion:LocalizableButtonLink runat="server" ID="exportErrors" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_347 %>" DisplayType="Small" /></li>
            </ul>
        </div>

        <div id="worksheetCountDiv" runat="server" class="sw-form-label" visible="false">
            <ul style="list-style: disc inside;">
                <li><asp:Label ID="worksheetCount" runat="server" /></li>
            </ul>
        </div>

        <div id="createTypeDiv" runat="server" class="sw-form-label" visible="false">
            <ul style="list-style: disc inside;">
                <li><asp:Label ID="createType" runat="server"/></li>
            </ul>
        </div>

        <div id="FurtherAction" style="margin: 20px 0px 10px 0px;" visible="false" runat="server">
            <asp:Label ID="hiddenNodes" runat="server" />
        </div>
    </div>
        
    <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
        <orion:LocalizableButtonLink runat="server" ID="A4" NavigateUrl="~/Orion/IPAM/import.upload.aspx" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_349 %>" DisplayType="Secondary" />
        <orion:LocalizableButtonLink runat="server" ID="A2" NavigateUrl="~/Orion/IPAM/Import.SubnetManagement.aspx" LocalizedText="Next" DisplayType="Primary" visible="false" />
        <orion:LocalizableButtonLink runat="server" ID="A1" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Done" DisplayType="Primary" />
    </div>
</div>
</asp:Content>
