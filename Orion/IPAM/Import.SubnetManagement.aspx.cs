using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportSubnetManagement : CommonPageServices
	{
		PageOrphanedIPs manager;

        public GridBox Grid
        {
            get { return hiddenSubnet; }
        }

        public int MoveableIPAddresses
        {
            get { return manager.MoveableIPs; }
        }

		public ImportSubnetManagement()
        {
			manager = new PageOrphanedIPs(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (MoveableIPAddresses != 0)
            {
                MoveOrphanedIPs.disabled = false;
                MoveOrphanedIPs.text = string.Format(Resources.IPAMWebContent.IPAMWEBCODE_AK1_11, MoveableIPAddresses);
            }
        }

        protected void OnDoneClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
	}
}
