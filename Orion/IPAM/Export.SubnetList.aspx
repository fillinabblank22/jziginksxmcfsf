<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" CodeFile="Export.SubnetList.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ExportSubnetList" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_310 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:JsBlock Requires="ext,sw-subnet-manage.js" Orientation="jsInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
Ext.onReady(function() {Ext.QuickTips.init();});
</IPAMmaster:JsBlock>

<div id="formbox">

<div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_311 %></div>

<IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />

<IPAMlayout:ViewPort id="viewport" runat="server">
    <IPAMlayout:BorderLayout ID="BorderLayout1" runat="server" borderVisible=false>
    <North>
       <IPAMlayout:BoxComponent ID="BoxComponent3" runat="server" borderVisible="false" visible=false serverEl="WarningMsg" /> 
    </North>
    <Center>
        <IPAMlayout:BorderLayout ID="BorderLayout2" runat="server" borderVisible="false">
            <North>
                <IPAMlayout:BoxComponent ID="BoxComponent2" runat="server" borderVisible="false" visible=false serverEl="breadcrumb" />
            </North>
            <Center>
              <IPAMlayout:GridBox id="grid" forceFit="True" borderVisible="false" UseLoadMask="True" emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_312 %>" deferEmptyText=false RowSelection=true runat="server" SelectorName="selector">

              <Columns>
                    <IPAMlayout:GridColumn id="GridColumn1" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_220 %>" width="120" isSortable="true" dataIndex="FriendlyName" runat="server" />
                    <IPAMlayout:GridColumn id="GridColumn2" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="90" isSortable="true" dataIndex="Address" runat="server" />
                    <IPAMlayout:GridColumn id="GridColumn3" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_241 %>" width="40" isSortable="true" dataIndex="CIDR" runat="server" />
                    <IPAMlayout:GridColumn id="GridColumn4" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_242 %>" width="90" dataIndex="AddressMask" runat="server" />
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                <Fields>
                    <IPAMlayout:GridStoreField ID="GridStoreField5" dataIndex="GroupId" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField ID="GridStoreField1" dataIndex="FriendlyName" runat="server" />
                    <IPAMlayout:GridStoreField ID="GridStoreField2" dataIndex="Address" runat="server" />
                    <IPAMlayout:GridStoreField ID="GridStoreField3" dataIndex="CIDR" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField ID="GridStoreField4" dataIndex="AddressMask" runat="server" />
                </Fields>
              </Store>
              </IPAMlayout:GridBox>
            </Center>
             <South>
                 
            </South>
         </IPAMlayout:BorderLayout>
      </Center>
    </IPAMlayout:BorderLayout>
    
</IPAMlayout:ViewPort>
      
</div>

</asp:Content>
