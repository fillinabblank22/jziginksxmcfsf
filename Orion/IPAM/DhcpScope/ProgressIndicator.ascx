<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs"
    Inherits="DhcpScope_ProgressIndicator" %>
    
    <orion:Include ID="Include1" runat="server" File="ProgressIndicator.css" />
    <style type="text/css">
        .PI_on:link, .PI_on:visited, .PI_on:hover, .PI_on:active { color: white; }
        .PI_off:link, .PI_off:visited, .PI_off:hover, .PI_off:active { color: black; }
    </style>

<div class="ProgressIndicator">
    <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
</div>
    

 