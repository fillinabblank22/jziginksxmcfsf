﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dhcp_ScopeIpAddressList : System.Web.UI.UserControl
{  
    private string _scopeIpRangeSupplyFn;
    public string WebId { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
       
        this.ServersGrid.jsID = this.ServersGrid.ClientID;
        this.ServersGrid.listeners = string.Format(@"{{ cellclick: $SW.IPAM.RouterIPs['{0}'].grid_cellclick,  render: function(){{ return $SW.IPAM.RouterIPs['{0}'].SetWebId(); }} }}", this.ID);
        this.btnAdd.handler = string.Format(@"function(el){{ return $SW.IPAM.RouterIPs['{0}'].AddServerBtnPressed(el); }}", this.ID);
        this.closeMsg.handler = this.closeMsg.handler.Replace("%ID%", this.ID);

    }
  
}