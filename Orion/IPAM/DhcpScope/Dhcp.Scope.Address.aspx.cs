﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Common;
using System.Net;
using SolarWinds.IPAM.Web.Common.Utility;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeAddress : CommonPageServices
    {
        #region Declarations

        private PageDhcpScopeEditor Editor { get; set; }

        #endregion // Declarations

        public bool IsMultipleScopeMgt { get; set; }
        public string SubnetAddress { get; set; }

        public string WebParamId
        {
            get
            {
                return GetParam(DhcpSessionStorage.PARAM_WEBID, null);
            }
        }
        #region Constructors

        public DhcpScopeAddress()
        {
            this.Editor = new PageDhcpScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DhcpScopeWizardStep.Address;
            IsMultipleScopeMgt = false;

            if (!IsPostBack)
            {
                InitControls();
            }

            var webId = GetParam(DhcpSessionStorage.PARAM_WEBID, null);
            Exclusions.WebId = webId;
            Title = string.Format(Title, Editor.Session_IsAddMode(null) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_68 : Resources.IPAMWebContent.IPAMWEBDATA_VB1_70);
            IPRangeGrid.HideMgtProcess = false;
            IPRangeGrid.WebId = webId;
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            // [oh] get proper DHCP manager to check capability
            var dhcpServerType = Editor.Session_RetrieveServerType(null);

            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            var isCapableOfExclusions = typeof(IExclusionManagement).IsAssignableFrom(managerType);
            var isCapableOfScopeRange = typeof(IScopeRangeManagement).IsAssignableFrom(managerType);
            var hasGlobalExclusions = typeof(IGlobalExclusions).IsAssignableFrom(managerType);
            var hasScopeMultipleRangeManagement = typeof (IScopeMultipleRangeManagement).IsAssignableFrom((managerType));
            hdnMultiRangeManagement.Value = hasScopeMultipleRangeManagement.ToString();
            IsMultipleScopeMgt = hasScopeMultipleRangeManagement;

            DhcpScope scope;
             var ranges = new List<DhcpRange>();
            Editor.Session_RetrieveScopeInfo(null, out scope, out ranges);
            ScopeRange.Visible = isCapableOfScopeRange;
            ScopeAddress.Visible = !isCapableOfScopeRange;
            divJustScopeExclusions.Visible = hasGlobalExclusions;
            PoolandIpAddressRange.Visible = hasScopeMultipleRangeManagement;
            if (hasScopeMultipleRangeManagement){ ScopeAddress.Visible = isCapableOfScopeRange;}
            // By default we filter just scope related for Global Exclusions
            cbJustScopeExclusions.Checked = hasGlobalExclusions;

            txtSubnetAddress.Text = scope.Address;
            if (!hasScopeMultipleRangeManagement)
            {
                txtStartAddress.Text = ranges[0].StartAddress;
                txtEndAddress.Text = ranges[0].EndAddress;
            }
            else
            {
                SubnetAddress = "for " + scope.Address + "/" + scope.CIDR;
            }

            bool addMode = Editor.Session_IsAddMode(null);

            txtCidr.Text = scope.CIDR.ToString(CultureInfo.InvariantCulture);
            if (!addMode)
            {
                txtSubnetAddress.ReadOnly = true;
                txtSubnetAddress.CssClass += " x-item-disabled disabled-textbox";

                txtCidr.ReadOnly = true;
                txtCidr.CssClass += " x-item-disabled disabled-textbox";
            }

            txtSubnetMask.Value = scope.AddressMask;
            if (!hasScopeMultipleRangeManagement)
            {
                cbAddIPAddresses.Visible = addMode;
                if (addMode)
                    cbAddIPAddresses.Checked = !Editor.Session_RetrieveEmptySubnet(null);
            }
            divExclusions.Visible = isCapableOfExclusions;

            Editor.ActivateProgressStep(null, PageDhcpScopeEditor.IPAM_DHCPSCOPE_ADDRESS);

            List<DhcpOptionsWebMeta> webMeta = new List<DhcpOptionsWebMeta>();
            Editor.Session_RetrieveWebMetaSessionData(null, out webMeta, dhcpServerType);
   }

        private void Next()
        {
            if (!ValidateUserInput())
                return;
            var dhcpServerType = Editor.Session_RetrieveServerType(null);

            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            var hasScopeMultipleRangeManagement = typeof(IScopeMultipleRangeManagement).IsAssignableFrom((managerType));
            if (!hasScopeMultipleRangeManagement)
            {
                DhcpScope scope;
                var ranges = new List<DhcpRange>();
                Editor.Session_RetrieveScopeInfo(null, out scope, out ranges);
                int cidr;
                if (!int.TryParse(txtCidr.Text, out cidr))
                    throw new ApplicationException("Error in CIDR validator. It's not a number!");

                IpRange ipRange = GetSubnetRange(cidr);

                ranges[0].StartAddress = ipRange.StartIp.ToString();
                ranges[0].EndAddress = ipRange.EndIp.ToString();
                scope.CIDR = cidr;

                // [oh] don't need to set it's automatically calculated from CIDR
                //scope.AddressMask = txtSubnetMask.Value;

                Editor.Sesssion_UpdateScopeInfo(null, scope, ranges);
                Editor.Sesssion_UpdateEmptySubnet(null, !this.cbAddIPAddresses.Checked);
            }
            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
            Response.Redirect(url, true);
        }

        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }

        protected void OnFirstAddressValidate(object sender, ServerValidateEventArgs args)
        {
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = noRangeMode || Editor.ValidateFirstAddress(txtStartAddress.Text, txtCidr.Text);
        }

        protected void OnLastAddressValidate(object sender, ServerValidateEventArgs args)
        {
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = noRangeMode || Editor.ValidateLastAddress(txtStartAddress.Text, txtEndAddress.Text, txtCidr.Text);
        }

        protected void OnCidrRangeValidate(object sender, ServerValidateEventArgs args)
        {
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = noRangeMode || Editor.ValidateCidrRange(txtStartAddress.Text, txtEndAddress.Text, txtCidr.Text);
        }

        protected void OnStartAddressValidate(object sender, ServerValidateEventArgs args)
        {
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = noRangeMode || Editor.ValidateStartAddress(txtStartAddress.Text, txtEndAddress.Text);
        }

        protected void OnExclusionsRangeValidate(object sender, ServerValidateEventArgs args)
        {
            int cidr;
            if (!int.TryParse(txtCidr.Text, out cidr))
                throw new ApplicationException("Error in CIDR validator. It's not a number!");
            IpRange ipRange = GetSubnetRange(cidr);

            string error = string.Empty;
            if (ipRange == null)
            {
                args.IsValid = false;
                error = "Subnet range is not valid.";
            }

            if (args.IsValid)
            {
                args.IsValid = Editor.ValidateExclusionsRage(
                    ipRange.StartIp.ToString(), ipRange.EndIp.ToString(), out error);
            }

            var customValidator = sender as CustomValidator;
            if (customValidator != null)
                (customValidator).ErrorMessage = error;
        }

        protected void OnStartAddressToScopeValidate(object sender, ServerValidateEventArgs args)
        {
            var addMode = Editor.Session_IsAddMode(null);
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = addMode || noRangeMode || Editor.ValidateAddressToScope(txtStartAddress.Text);
        }

        protected void OnEndAddressToScopeValidate(object sender, ServerValidateEventArgs args)
        {
            var addMode = Editor.Session_IsAddMode(null);
            var noRangeMode = (ScopeRange.Visible == false);
            args.IsValid = addMode || noRangeMode || Editor.ValidateAddressToScope(txtEndAddress.Text);
        }

        protected void OnSubnetAddressValidate(object sender, ServerValidateEventArgs args)
        {
            try
            {
                var subnetAddress = txtSubnetAddress.Text;

                if (String.IsNullOrEmpty(subnetAddress))
                    throw new ArgumentNullException("subnetAddress");

                IPAddress address;
                if (!IPAddress.TryParse(subnetAddress, out address))
                    throw new ArgumentException("subnetAddress is not IP address");

                args.IsValid = true;
            }
            catch (Exception exception)
            {
                args.IsValid = false;
                var customValidator = sender as CustomValidator;
                if (customValidator != null)
                    (customValidator).ErrorMessage = exception.Message;
            }
        }

        private IpRange GetSubnetRange(int cidr)
        {
            IpRange range = new IpRange();
            if (ScopeRange.Visible == true)
            {
                // Validators will take care of correct message
                IPAddress tryAddress;
                if (string.IsNullOrEmpty(txtStartAddress.Text) || string.IsNullOrEmpty(txtEndAddress.Text))
                    return null;
                
                if (!IPAddress.TryParse(txtStartAddress.Text.Trim(), out tryAddress) || !IPAddress.TryParse(txtEndAddress.Text.Trim(), out tryAddress))
                    return null;

                range.StartIp = IPAddress.Parse(txtStartAddress.Text.Trim());
                range.EndIp = IPAddress.Parse(txtEndAddress.Text.Trim());
            }
            else if (ScopeAddress.Visible == true)
            {
                IPAddress tryAddress;
                if (string.IsNullOrEmpty(txtSubnetAddress.Text))
                    return null;
                if (!IPAddress.TryParse(txtSubnetAddress.Text.Trim(), out tryAddress))
                    return null;

                IPAddress address = IPAddress.Parse(txtSubnetAddress.Text.Trim());
                // Cisco and ASA devices which are specified by ScopeAddress/Cidr have actual IPRanges starting from .1 to .254
                range.StartIp = IPFunction.UInt32ToIPv4(IPFunction.IPv4ToUInt32(IPFunction.SubnetFirstIP(address, cidr)) + 1);
                range.EndIp = IPFunction.UInt32ToIPv4(IPFunction.IPv4ToUInt32(IPFunction.SubnetLastIP(address, cidr)) - 1);
            }
            return range;
        }

        #endregion // Methods
    }
}