﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dhcp.Scope.IPRange.ascx.cs" Inherits="DhcpScope_IPRange" %>

<orion:Include ID="IncludeExtJs" runat="server" debug="false" Framework="Ext" FrameworkVersion="4.0" />
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,ext-quicktips,sw-dialog.js,ext-ux,sw-ux-wait.js,sw-scope-iprange.js,sw-iprangepool-manage.js" Orientation="jsPostInit" runat="server" />

<script type="text/javascript">
    
    if (!$SW.IPAM) $SW.IPAM = {};
    
    Ext.onReady(function () {
        $SW.IPAM.IPRangeTreeGrid('<%=this.WebId %>', '<%=this.HideMgtProcess %>');
    });
    
    jQuery(document).ready(function () {
        $SW.IPAM.LoadIpRangeDialogLayout("<%= OptionLayout.ClientID %>", "<%= this.WebId %>");
    });
    
    function SaveIpRange() {
        $SW.IPAM.GlobalSaveRecord();
        return false;
    }
    
    function CloseDialog() {
        $SW.IPAM.ShowIpRangeLayoutDialog.HideDialog();
        return false;
    }
    
    $SW.IPAM.SetWebId = function () {
        $SW.store.baseParams.w = '<%=this.WebId %>';
    };

</script>

<style>
 .pool-cell { 
    background-image: url("/Orion/IPAM/res/images/sw-resources/chart-pool.png") !important;
    background-repeat: no-repeat;
}
 .range-cell { 
    background-image: url("/orion/ipam/res/images/sw-resources/IPrange_icon16x16.png") !important;
    background-repeat: no-repeat;
}
 .EmptyRow {
        height: 10px;
    }

 .ui-dialog .ui-dialog-titlebar 
{
    height: 20px;
}

 .x4-tree-arrows .x4-grid-tree-node-expanded .x4-tree-elbow-plus {
    background-position:-16px 0;
}
  
.x4-grid-row .x4-grid-cell {
    background-color: #FFFFFF !important;
    border-color: #FAFAFA #EDEDED #EDEDED !important;
    border-right: 0 solid #EDEDED !important;
    border-style: solid !important;
    border-width: 1px 0 !important;
    font: 11px tahoma,arial,verdana,sans-serif !important;
    vertical-align: middle  !important;
    height: 20px !important;
    overflow: auto !important;
    text-overflow: ellipsis !important;
    white-space: nowrap !important;
}
</style>

<div>
    <div id="scopeIpRangediv" style="width: 970px;" class="x-panel x-treegrid" ></div>

    <asp:Panel ID="OptionLayout" runat="server" BackColor= "white">
     
        <div>
        
            <table class="BoderCollapse">
            
                <tr class="EmptyRow"></tr>
            
                <tr>
                    <td>
                        <div>
                            <IPAMlayout:GridBox runat="server" ID="IpRangeGrid" title="" UseLoadMask="True" autoHeight="false" height="240" width="450"
                                    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_26 %>" borderVisible="true" deferEmptyText="false"
                                    SelectorName="selector" RowSelection="true" listeners="{cellclick: $SW.IPAM.IpRangeRecords.grid_cellclick,render: $SW.IPAM.SetWebId}">

                                    <ConfigOptions ID="ConfigOptions1" runat="server">
                                        <Options>
                                            <IPAMlayout:ConfigOption ID="ConfigOption1" Name="plugins" Value="sw-ipRangeRecord-editor" runat="server" />
                                        </Options>
                                    </ConfigOptions>
                                    <ViewOptions ID="ViewOptions1" runat="server" />
    
                                    <TopMenu borderVisible="false">
                                        <IPAMui:ToolStripMenuItem id="AddIpRange" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_27 %>" runat="server">
                                                <handler>function() { $SW.IPAM.IpRangeRecords.AddRecord( Ext.getCmp('IpRangeGrid')); }</handler>
                                        </IPAMui:ToolStripMenuItem>
                                    </TopMenu>

                                    <Columns>
                                        <IPAMlayout:GridColumn title="Start IP Address" width="190" isHideable="true" isSortable="true" runat="server" dataIndex="StartIPAddr" editor="{ xtype: 'textfield'}" />
                                        <IPAMlayout:GridColumn title="End IP Address" width="190" isHideable="true" isSortable="true" runat="server" dataIndex="EndIPAddr" editor="{ xtype: 'textfield'}" />
                                        <IPAMlayout:GridColumn title="Actions" width="65" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.IpRangeList_renderer_buttons()" />
                                    </Columns>

                                    <Store id="store" dataRoot="rows" dataCount="count" autoLoad="false" proxyUrl="../sessiondataprovider.ashx"
                                        proxyEntity="IPRange" AmbientSort="AutoId" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                                        <Fields>
                                            <IPAMlayout:GridStoreField dataIndex="ScopeId"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="PoolId"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="StartIPAddr"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="EndIPAddr"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="PoolType"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="RangeId"  runat="server" />
                                            <IPAMlayout:GridStoreField dataIndex="AutoId" runat="server" />
                                        </Fields>
                                    </Store>

                            </IPAMlayout:GridBox>
                        </div>
                    </td>
                </tr>
                
                <tr class="EmptyRow"></tr>
                
                <tr>
                    <td>
                        <div class="sw-btn-bar-wizard">
                            <orion:LocalizableButton runat="server" ID="imgbtnNext" CausesValidation="true" OnClientClick="SaveIpRange(); return false;" DisplayType="Primary" LocalizedText="Save" />
                            <orion:LocalizableButton runat="server" ID="imgbtnCancel" CausesValidation="false" OnClientClick="CloseDialog(); return false;" DisplayType="Secondary" LocalizedText="Cancel" />
                        </div>
                    </td>
                </tr>

             </table>

        </div>

    </asp:Panel>
</div>

