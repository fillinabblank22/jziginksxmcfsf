﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScope_Exclusions_IPRange : CommonPageServices
    {
        private readonly PageDhcpScopeExclusionsEditor _manager;

        public DhcpScope_Exclusions_IPRange()
        {
            _manager = new PageDhcpScopeExclusionsEditor(this);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            if (_manager.Save())
                _manager.Redirect();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            if (_manager.Save())
                _manager.Redirect();
        }

        protected void ClickCancel(object sender, ImageClickEventArgs e)
        {
            _manager.Redirect();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnExclusionRangeValidate(object sender, ServerValidateEventArgs args)
        {
            var errorMessage = _manager.ValidateExclusionRange();
            if (String.IsNullOrEmpty(errorMessage))
                args.IsValid = true;
            else
            {
                args.IsValid = false;
                if (sender is CustomValidator)
                    (sender as CustomValidator).Text = errorMessage;
            }
        }

        protected void OnStartAddressToEndValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = _manager.ValidateStartAddressToEnd();
        }

        protected void OnStartAddressToScopeValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = _manager.ValidateStartAddressToScope();
        }

        protected void OnEndAddressToScopeValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = _manager.ValidateEndAddressToScope();
        }
    }
}