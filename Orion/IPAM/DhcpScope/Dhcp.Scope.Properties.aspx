<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.Scope.Properties.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeProperties"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_511%>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGCreatingDHCPScopes" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
        .contentBlock p { margin-bottom: 8px; }
        .scopeIscProperty { margin: 5px 10px 5px 0px; padding: 5px 0 5px 10px; background-color:#FFF7CD; }
    </IPAMmaster:CssBlock>

    <orion:Include File="breadcrumb.js" runat="server" />
    
    <IPAMmaster:JsBlock Requires="ext,ext-quicktips" Orientation="jsPostInit" runat="server">
    $(document).ready(function() {
        var nsId = $nsId;
        var DefLimitedBtnClick = function(defUnlimitedbutton,maxUnlimitedbutton) {
            var defUnLtdchecked = defUnlimitedbutton.checked;

            if(defUnLtdchecked === true) {
                maxUnlimitedbutton.checked=true;
                $SW.ns$($nsId,'txtDaysDef')[0].value="";
                $SW.ns$($nsId,'txtHoursDef')[0].value="";
                $SW.ns$($nsId,'txtMinutesDef')[0].value="";
                $SW.ns$($nsId,'txtSecondsDef')[0].value="";
                $SW.ns$($nsId,'txtDaysMax')[0].value="";
                $SW.ns$($nsId,'txtHoursMax')[0].value="";
                $SW.ns$($nsId,'txtMinutesMax')[0].value="";
                $SW.ns$($nsId,'txtSecondsMax')[0].value="";
                
                $SW.ns$($nsId,'txtDaysDef')[0].disabled =true;
                $SW.ns$($nsId,'txtHoursDef')[0].disabled =true;
                $SW.ns$($nsId,'txtMinutesDef')[0].disabled =true;
                $SW.ns$($nsId,'txtSecondsDef')[0].disabled =true;
                $SW.ns$($nsId,'txtDaysMax')[0].disabled =true;
                $SW.ns$($nsId,'txtHoursMax')[0].disabled =true;
                $SW.ns$($nsId,'txtMinutesMax')[0].disabled =true;
                $SW.ns$($nsId,'txtSecondsMax')[0].disabled =true;
            } else {
                $SW.ns$($nsId,'txtDaysDef')[0].disabled =false;
                $SW.ns$($nsId,'txtHoursDef')[0].disabled =false;
                $SW.ns$($nsId,'txtMinutesDef')[0].disabled =false;
                $SW.ns$($nsId,'txtSecondsDef')[0].disabled =false;
            }
        };
    
        var MaxRadioBtnClick = function(rdbutton) {
            var checked = rdbutton.checked;
            if(checked === true) {
                $SW.ns$($nsId,'txtDaysMax')[0].disabled =false;
                $SW.ns$($nsId,'txtHoursMax')[0].disabled =false;
                $SW.ns$($nsId,'txtMinutesMax')[0].disabled =false;
                $SW.ns$($nsId,'txtSecondsMax')[0].disabled =false;
            } else {
                $SW.ns$($nsId,'txtDaysMax')[0].value="";
                $SW.ns$($nsId,'txtHoursMax')[0].value="";
                $SW.ns$($nsId,'txtMinutesMax')[0].value="";
                $SW.ns$($nsId,'txtSecondsMax')[0].value="";
                $SW.ns$($nsId,'txtDaysMax')[0].disabled =true;
                $SW.ns$($nsId,'txtHoursMax')[0].disabled =true;
                $SW.ns$($nsId,'txtMinutesMax')[0].disabled =true;
                $SW.ns$($nsId,'txtSecondsMax')[0].disabled =true;
            }
        };
    
         var MinRadioBtnClick = function(rdbutton) {
            var checked = rdbutton.checked;
            if(checked === true) {
                $SW.ns$($nsId,'txtDaysMin')[0].disabled =false;
                $SW.ns$($nsId,'txtHoursMin')[0].disabled =false;
                $SW.ns$($nsId,'txtMinutesMin')[0].disabled =false;
                $SW.ns$($nsId,'txtSecondsMin')[0].disabled =false;
            } else {
                $SW.ns$($nsId,'txtDaysMin')[0].value="";
                $SW.ns$($nsId,'txtHoursMin')[0].value="";
                $SW.ns$($nsId,'txtMinutesMin')[0].value="";
                $SW.ns$($nsId,'txtSecondsMin')[0].value="";
                $SW.ns$($nsId,'txtDaysMin')[0].disabled =true;
                $SW.ns$($nsId,'txtHoursMin')[0].disabled =true;
                $SW.ns$($nsId,'txtMinutesMin')[0].disabled =true;
                $SW.ns$($nsId,'txtSecondsMin')[0].disabled =true;
            }
        };

        var defUnlimitedbutton = $SW.ns$($nsId,'DefUnlimitedBtn')[0];
        var deflimitedbutton = $SW.ns$($nsId,'DefLimitedBtn')[0];
        var maxUnlimitedbutton = $SW.ns$($nsId,'MaxUnlimitedBtn')[0];
        var Maxlimitedbutton = $SW.ns$($nsId,'MaxLimitedBtn')[0];
        var Minlimitedbutton = $SW.ns$($nsId,'MinLimitedBtn')[0];
        $SW.ns$(nsId,'DefUnlimitedBtn').click( function(){ DefLimitedBtnClick(defUnlimitedbutton,maxUnlimitedbutton); } );
        $SW.ns$(nsId,'DefLimitedBtn').click( function(){ DefLimitedBtnClick(defUnlimitedbutton,maxUnlimitedbutton); } );
        $SW.ns$(nsId,'MaxUnlimitedBtn').click( function(){ MaxRadioBtnClick(Maxlimitedbutton); } );
        $SW.ns$(nsId,'MaxLimitedBtn').click( function(){ MaxRadioBtnClick(Maxlimitedbutton); } );
    
       $SW.ns$(nsId,'MinUnlimitedBtn').click( function(){ MinRadioBtnClick(Minlimitedbutton); } );
       $SW.ns$(nsId,'MinLimitedBtn').click( function(){ MinRadioBtnClick(Minlimitedbutton); } );
    });
    </IPAMmaster:JsBlock>

    <script type="text/javascript">
        function ValidateMaximumLeaseDays(source, daysTbox) {
            var hours = $('[id$=txtHoursMax]').val();
            var minutes = $('[id$=txtMinutesMax]').val();
            var seconds = $('[id$=txtSecondsMax]').val();
            var totSeconds = getTimeSeconds(daysTbox.Value, hours, minutes, seconds);
            if (totSeconds > 4294967295) {
                daysTbox.IsValid = false;
            } else {
                daysTbox.IsValid = true;
            }
        }

        function ValidateDefLeaseDays(source, daysTbox) {
            var hours = $('[id$=txtHoursDef]').val();
            var minutes = $('[id$=txtMinutesDef]').val();
            var seconds = $('[id$=txtSecondsDef]').val();
            var totSeconds = getTimeSeconds(daysTbox.Value, hours, minutes, seconds);
            if (totSeconds > 4294967295) {
                daysTbox.IsValid = false;
            } else {
                daysTbox.IsValid = true;
            }
        }

        function ValidateMinLeaseDays(source, daysTbox) {
            var hours = $('[id$=txtHoursMin]').val();
            var minutes = $('[id$=txtMinutesMin]').val();
            var seconds = $('[id$=txtSecondsMin]').val();
            var totSeconds = getTimeSeconds(daysTbox.Value, hours, minutes, seconds);
            if (totSeconds > 4294967295) {
                daysTbox.IsValid = false;
            } else {
                daysTbox.IsValid = true;
            }
        }
        
        function getTimeSeconds(days, hours, minutes, seconds) {
            hours = (isNaN(parseInt(hours)) ? 0 : parseInt(hours)) + (days * 24);
            minutes = (isNaN(parseInt(minutes)) ? 0 : parseInt(minutes))+ (hours * 60);
            seconds = (isNaN(parseInt(seconds)) ? 0 : parseInt(seconds)) + (minutes * 60); 
            return seconds;
        }
    </script>
    
    <IPAMui:ValidationIcons runat="server" />
    
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item">
                                    <a href="/api2/ipam/ui/scopes" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage" />
                                    <ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                                        <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                                        <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                        <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                        <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                        <% } %>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <% } %>
                        <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>

        <div><!-- ie6 --></div>
        <div>
            <table width="1000px" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <orion:Progress ID="ProgressIndicator1" runat="server" />
                        <div class="GroupBox" id="NormalText">
                            <div>
                                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_541 %><asp:Literal runat="server" ID="litNodeIP" /></span>
                            </div>
                            <div class="contentBlock">
                                <div id="divOfferDelay" runat="server">
                                    <p><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_559 %></b></p>
                                    <div class="blueBox" style="padding:5px 20px; margin-bottom: 8px; width: 95%;">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_560 %>
                                        <asp:TextBox runat="server" ID="txtDelay" Width="30" MaxLength="4" CssClass="x-form-text x-form-field" />
                                        &nbsp;<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_561 %>
                                        <asp:RequiredFieldValidator ID="OfferDelayRequire" runat="server" ControlToValidate="txtDelay" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_562 %>" />
                                        <asp:CompareValidator ID="DelayAbove" runat="server" ControlToValidate="txtDelay" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_563 %>" />
                                        <asp:CompareValidator ID="DelayBelow" runat="server" ControlToValidate="txtDelay" Operator="LessThanEqual" ValueToCompare="1000" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_564 %>" />
                                    </div>
                                    <div style="font-family:Arial;font-size:10px;color:#646464"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_565 %></div>
                                    <%-- Failover info. --%>
                                    <asp:Label ID="lbFailover" runat="server">
                                        <br/>
                                        <p><b><%= Resources.IPAMWebContent.IPAMWEBDATA_DM1_1 %></b></p>
                                        <div class="blueBox" style="padding:5px 20px; margin-bottom: 8px; width: 95%;">
                                            <asp:Literal ID="litFailoverInformation" runat="server"></asp:Literal>                        
                                        </div>
                                        <div style="font-family:Arial;font-size:10px;color:#646464"><%= Resources.IPAMWebContent.IPAMWEBDATA_DM1_3 %></div>
                                    </asp:Label>
                                </div>
                                <div class="contentBlock" runat="server" id="iscProperties" Visible="False">
                                    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_24 %> </b><br/>
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_25 %>
                                    <div style="padding-left: 20px; padding-top: 10px">
                                    <ul style="list-style-type: square">
                                        <li><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_543 %></li>
                                        <li><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_544 %></li>
                                    </ul>
                                </div>
                                <div id="Div2" runat="server" class="scopeIscProperty">
                                    <table>
                                        <tr>
                                            <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                            <td id ="OtherTd">
                                              <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_26 %>      
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="DefdivLeaseLast" runat="server">
                                    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_18%></b>
                                    <div style="font-family:Arial; font-size:10px; color:#646464; margin-bottom: 8px;">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_21%>
                                    </div>
                                    <div id="DeflimitedDiv">
                                        <asp:RadioButton runat="server" ID="DefLimitedBtn" GroupName="LeaseTime" Checked /> &nbsp;
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_546%> &nbsp;
                                        <asp:TextBox runat="server" ID="txtDaysDef" Width="35" MaxLength="5" CssClass="x-form-text x-form-field" />
                                        <asp:CompareValidator ID="DaysAbove" runat="server" ControlToValidate="txtDaysDef" Operator="GreaterThanEqual"
                                            ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_548 %>" />
                                        <asp:CompareValidator ID="DaysBelow" runat="server" ControlToValidate="txtDaysDef" Operator="LessThanEqual"
                                            ValueToCompare="49710" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_27 %>" />
                                        <asp:CustomValidator runat="server" ControlToValidate="txtDaysDef" 
                                                             ClientValidationFunction="ValidateDefLeaseDays" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_32 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_550 %></span>
                                        <asp:TextBox runat="server" ID="txtHoursDef" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                       <asp:CompareValidator ID="HoursAbove" runat="server" ControlToValidate="txtHoursDef"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_552 %>" />
                                        <asp:CompareValidator ID="HoursBelow" runat="server" ControlToValidate="txtHoursDef"
                                            Operator="LessThanEqual" ValueToCompare="23" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_556 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_553 %></span>
                                        <asp:TextBox runat="server" ID="txtMinutesDef" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                       <asp:CompareValidator ID="MinutesAbove" runat="server" ControlToValidate="txtMinutesDef"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_555 %>" />
                                        <asp:CompareValidator ID="MinutesBelow" runat="server" ControlToValidate="txtMinutesDef"
                                            Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_557 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_29 %></span>
                                        <asp:TextBox runat="server" ID="txtSecondsDef" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                       <asp:CompareValidator ID="SecondsDefAbove" runat="server" ControlToValidate="txtSecondsDef"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_31 %>" />
                                        <asp:CompareValidator ID="SecondsBelowDef" runat="server" ControlToValidate="txtSecondsDef"
                                            Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_30 %>" />
                                   
                                      &nbsp;&nbsp; or &nbsp;<asp:RadioButton runat="server" ID="DefUnlimitedBtn" GroupName="LeaseTime" />
                                            <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545_2%></span>
                                    </div>
                                    <br/>
                                </div>
                                <div id="MindivLeaseLast" runat="server">
                                    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_19%></b>
                                    <div style="font-family:Arial; font-size:10px; color:#646464; margin-bottom: 8px;">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_22 %>
                                    </div>
                                    <div id="Div1">
                                       <asp:RadioButton runat="server" ID="MinLimitedBtn" GroupName="MinLeaseTime" Checked /> &nbsp;

                                      <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_546%> &nbsp;
                                            <asp:TextBox runat="server" ID="txtDaysMin" Width="35" MaxLength="5" CssClass="x-form-text x-form-field" />
                                            <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtDaysMax" Operator="GreaterThanEqual"
                                                ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_548 %>" />
                                            <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="txtDaysMin" Operator="LessThanEqual"
                                                ValueToCompare="49710" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_27 %>" />
                                             <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtDaysMin" 
                                                                 ClientValidationFunction="ValidateMinLeaseDays" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_32 %>" />
                                            <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_550 %></span>
                                            <asp:TextBox runat="server" ID="txtHoursMin" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                           <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="txtHoursMin"
                                                Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_552 %>" />
                                            <asp:CompareValidator ID="CompareValidator10" runat="server" ControlToValidate="txtHoursMin"
                                                Operator="LessThanEqual" ValueToCompare="23" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_556 %>" />
                                            <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_553 %></span>
                                            <asp:TextBox runat="server" ID="txtMinutesMin" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                           <asp:CompareValidator ID="CompareValidator11" runat="server" ControlToValidate="txtMinutesMin"
                                                Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_555 %>" />
                                            <asp:CompareValidator ID="CompareValidator12" runat="server" ControlToValidate="txtMinutesMin"
                                                Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_557 %>" />
                                            <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_29 %></span>
                                            <asp:TextBox runat="server" ID="txtSecondsMin" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                           <asp:CompareValidator ID="CompareValidator13" runat="server" ControlToValidate="txtSecondsMin"
                                                Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_31 %>" />
                                            <asp:CompareValidator ID="CompareValidator14" runat="server" ControlToValidate="txtSecondsMin"
                                                Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_30 %>" />
                                             
                                              &nbsp;&nbsp; or &nbsp;<asp:RadioButton runat="server" ID="MinUnlimitedBtn" GroupName="MinLeaseTime" />
                                        <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545_2%></span>                 
                                    </div>
                                    <br/>
                                </div>
                                <div id="MaxdivLeaseLast" runat="server">
                                    <b><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_20%></b>
                                    <div style="font-family:Arial; font-size:10px; color:#646464; margin-bottom: 8px;">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_23 %>
                                    </div>
                                    <div id="MaxlimitedDiv">
                                        <asp:RadioButton runat="server" ID="MaxLimitedBtn" GroupName="MaxLeaseTime" Checked /> &nbsp;
                
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_546%> &nbsp;
                                        <asp:TextBox runat="server" ID="txtDaysMax" Width="35" MaxLength="5" CssClass="x-form-text x-form-field" />
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDaysMax" Operator="GreaterThanEqual"
                                            ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_548 %>" />
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtDaysMax" Operator="LessThanEqual"
                                            ValueToCompare="49710" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_27 %>" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtDaysMax" 
                                                             ClientValidationFunction="ValidateMaximumLeaseDays" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_32 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_550 %></span>
                                        <asp:TextBox runat="server" ID="txtHoursMax" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtHoursMax"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_552 %>" />
                                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtHoursMax"
                                            Operator="LessThanEqual" ValueToCompare="23" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_556 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_553 %></span>
                                        <asp:TextBox runat="server" ID="txtMinutesMax" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                        <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtMinutesMax"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_555 %>" />
                                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtMinutesMax"
                                            Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_557 %>" />
                                        <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_29 %></span>
                                        <asp:TextBox runat="server" ID="txtSecondsMax" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" />
                                        <asp:CompareValidator ID="CompareValidator15" runat="server" ControlToValidate="txtSecondsMax"
                                            Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None"
                                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_31 %>" />
                                        <asp:CompareValidator ID="CompareValidator16" runat="server" ControlToValidate="txtSecondsMax"
                                            Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_30 %>" />
                                        &nbsp;&nbsp; or &nbsp;<asp:RadioButton runat="server" ID="MaxUnlimitedBtn" GroupName="MaxLeaseTime" />
                                        <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545_2%></span>                 
                                    </div>
                                    <br/>
                                </div>
                            </div>
                            <div><!-- ie6 --></div>
                            <div>
                                <br />
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click" DisplayType="Secondary" LocalizedText="Back"/>
                                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="imgbNext" OnClick="imgbNext_Click" DisplayType="Primary" LocalizedText="Next"/>
                                    <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false" OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="Cancel"/>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
    </div>
</asp:Content>
