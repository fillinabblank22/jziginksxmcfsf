using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeWizard : CommonPageServices
    {
        #region Declarations

        private const string QueryServerParam = "ObjectID";
        private const string QueryScopeParam = "ScopeID";
        private const string QueryClusterIdParam = "ClusterID";

        private PageDhcpScopeEditor Editor { get; set; }
        private string WebId { get; set; }
        private int ServerId { get; set; }
        private int ScopeId { get; set; }
        protected bool IsAddMode { get; set; }
        protected string defaultToSelect { get; set; }
        protected string orgSharedNetName { get; set; }

        #endregion // Declarations

        #region Constructors

        public DhcpScopeWizard()
        {
            this.Editor = new PageDhcpScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DhcpScopeWizardStep.Define;

            this.WebId = GetParam(DhcpSessionStorage.PARAM_WEBID, null);

            // [oh] Dhcp Server from server's GroupId
            this.ServerId = GetParam(QueryServerParam, -1);
            if (string.IsNullOrEmpty(this.WebId) && this.ServerId >= 0)
            {
                this.WebId = Editor.Session_SetServerInfo(this.ServerId);
            }
            
            // [oh] Dhcp Scope from scope's GroupId
            this.ScopeId = GetParam(QueryScopeParam, -1);
            if (string.IsNullOrEmpty(this.WebId) && this.ScopeId >= 0)
            {
                this.WebId = Editor.Session_SetScopeInfo(this.ScopeId);
            }
            // [oh] if no zone and server is selected
            if (this.ServerId < 0 && this.ScopeId < 0 && Editor.Session_IsAddMode(WebId))
            {
                this.WebId = Editor.Session_GetNewWebId();
            }
            if (!IsPostBack)
            {
                this.IsAddMode = Editor.Session_IsAddMode(this.WebId);
                InitializeDhcpServersDropDown();
                InitControls();
            }
            Title = string.Format(Title, Editor.Session_IsAddMode(WebId) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_68 : Resources.IPAMWebContent.IPAMWEBDATA_VB1_70);

            if (AsyncAddressValidator != null)
            {
                int _clusterId;
                string clusterId = Page.Request.QueryString[QueryClusterIdParam];
                if (clusterId != null && int.TryParse(clusterId, out _clusterId))
                {
                    AsyncAddressValidator.Attributes["ClusterId"] = clusterId;
                }
            }
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.Session_Clear(webId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            this.hdWebId.Value = this.WebId;

            DhcpScope scope;
            var ranges = new List<DhcpRange>();
            Editor.Session_RetrieveScopeInfo(this.WebId, out scope, out ranges);
            txtScopeName.Text = scope.FriendlyName;
            txtScopeDesc.Text = scope.Comments;
            txtScopeVLAN.Text = scope.VLAN;
            txtScopeLocation.Text = scope.Location;
            
            txtNetworkAddress.Text = scope.Address;
            txtNetmask.Text = scope.AddressMask;
            txtCidr.Text = scope.CIDR.ToString();


            CustomPropertyRepeater.RetrieveCustomProperties(scope, new List<int>{ scope.GroupId });

            txtNetmask.ReadOnly = true;
            txtNetmask.CssClass += " x-item-disabled disabled-textbox";

            var serverName = Editor.Session_RetrieveServerName(this.WebId);
            var serverItem = ddlDhcpServers.Items.FindByText(serverName);
            ddlDhcpServers.SelectedIndex = ddlDhcpServers.Items.IndexOf(serverItem);

            defaultToSelect = string.Empty;
            bool addMode = Editor.Session_IsAddMode(this.WebId);
            int dhcpServerTypeId = Convert.ToInt32(ddlDhcpServers.SelectedItem.Value.Split('-')[1]);

            if (dhcpServerTypeId == Convert.ToInt32(DhcpServerType.ISC))
            {
                AsyncAddressValidator.Enabled = false;
                cbAddIPAddresses.Visible = addMode;
                if (addMode)
                    cbAddIPAddresses.Checked = !Editor.Session_RetrieveEmptySubnet(null);

                orgSharedNetName = Editor.Session_OrgSharedNetName(this.WebId, scope.SharedNetworkId.ToString());

                if (scope.SharedNetworkId >= 0)
                {
                    defaultToSelect = string.Format("{0}", scope.SharedNetworkId).Trim();
                }
                else
                {
                    var sharedNetName = string.Empty;
                    Editor.Session_RetrieveSharedNetName(this.WebId, out sharedNetName);
                    if (sharedNetName == Resources.IPAMWebContent.IPAMWEBDATA_SE_21)
                        defaultToSelect = string.Format("{0}", -2).Trim();
                }
            }

            if (!addMode)
            {
                ddlDhcpServers.Enabled = false;
                ddlDhcpServers.CssClass += " x-item-disabled disabled-textbox";

                txtNetworkAddress.Enabled = false;
                txtCidr.Enabled = false;

                txtNetworkAddress.CssClass += " x-item-disabled disabled-textbox";
                txtCidr.CssClass += " x-item-disabled disabled-textbox";

                // [oh] get proper DHCP manager to check capability
                var dhcpServerType = Editor.Session_RetrieveServerType(this.WebId);
                var isCapableScopeNameChange = DhcpFactory.Instance.CheckedDhcpManagerCapability(
                    dhcpServerType, typeof (IScopeNameChangeManagement));
                txtScopeName.Enabled = isCapableScopeNameChange;
                CheckPropertyTabIsHidden(WebId, dhcpServerType);
            }
            hdnServerType.Value = ddlDhcpServers.SelectedItem.Value.Split('-')[1];
            this.ProgressIndicator1.WebId = this.WebId;
            Editor.ActivateProgressStep(this.WebId, PageDhcpScopeEditor.IPAM_DHCPSCOPE_DEFAULT);
        }

        private void Next()
        {
            int dhcpServerId = Convert.ToInt32(ddlDhcpServers.SelectedItem.Value.Split('-')[0]);
            int dhcpServerTypeId = Convert.ToInt32(ddlDhcpServers.SelectedItem.Value.Split('-')[1]);
            
            if (dhcpServerTypeId == Convert.ToInt32(DhcpServerType.ISC))
            {
                scopeNameRequire.Enabled = false;
                NetworkAddressRequire.Enabled = true;
                NetworkAddressIPv4.Enabled = true;
                NetworkAddressSubnet.Enabled = true;
                CidrRequire.Enabled = true;
                CidrAbove.Enabled = true;
                CidrBelow.Enabled = true;
                CidrIsv4.Enabled = true;
                if (IsAddMode == false)
                {
                    AsyncAddressValidator.Enabled = false;
                }
                else
                {
                    AsyncAddressValidator.Enabled = true;
                }
                NetMaskRequire.Enabled = true;
                NetMaskRequireCustomValidator.Enabled = true;
            }
            else
            {
                scopeNameRequire.Enabled = true;
                NetworkAddressRequire.Enabled = false;
                NetworkAddressIPv4.Enabled = false;
                NetworkAddressSubnet.Enabled = false;
                CidrRequire.Enabled = false;
                CidrAbove.Enabled = false;
                CidrBelow.Enabled = false;
                CidrIsv4.Enabled = false;
                AsyncAddressValidator.Enabled = false;
                NetMaskRequire.Enabled = false;
                NetMaskRequireCustomValidator.Enabled = false;
            }
            if (!ValidateUserInput())
                return;

            string webId = this.hdWebId.Value;
            DhcpScope scope;

            List<DhcpRange> ranges = new List<DhcpRange>();
            Editor.Session_RetrieveScopeInfo(webId, out scope, out ranges);
            if (ddlDhcpServers.SelectedItem != null)
            {
                scope.ParentId = dhcpServerId;
            }

            scope.FriendlyName = txtScopeName.Text;
            scope.Comments = txtScopeDesc.Text;
            scope.VLAN = txtScopeVLAN.Text;
            scope.Location = txtScopeLocation.Text;
            if (dhcpServerTypeId == Convert.ToInt32(DhcpServerType.ISC))
            {
                Editor.Sesssion_UpdateEmptySubnet(webId, !this.cbAddIPAddresses.Checked);

                int nodeId = Convert.ToInt32(ddlDhcpServers.SelectedItem.Value.Split('-')[2]);
                scope.Address = txtNetworkAddress.Text;
                scope.AddressMask = txtNetmask.Text;
                scope.CIDR = Convert.ToInt32(txtCidr.Text);

                var selectedText = selectedSNetworkName.Value.Trim();
                var selectedValue = selectedSNetwork.Value.Trim();

                if (!string.IsNullOrEmpty(selectedText) && selectedText != Resources.IPAMWebContent.IPAMWEBDATA_SE_21)
                    scope.SharedNetworkId = (!string.IsNullOrEmpty(selectedValue)) ? Convert.ToInt32(selectedValue.Split(',')[0]) : -1;
                else
                    scope.SharedNetworkId = -1;

                string shareNetName = null;
                string dhcpGroupId = null;
                if (!string.IsNullOrEmpty(selectedText))
                {
                    shareNetName = (!string.IsNullOrEmpty(selectedValue)) ? selectedText.Trim() : null;
                    dhcpGroupId = (!string.IsNullOrEmpty(selectedValue) && selectedText != Resources.IPAMWebContent.IPAMWEBDATA_SE_21) ? Convert.ToString(selectedValue.Split(',')[1]) : null;
                }
                Editor.Session_SetSharedNetworkData(webId, shareNetName, dhcpGroupId);
                Editor.Sesssion_UpdateDuplicateSubnetsIPRanges(webId, nodeId, scope.Address, scope.CIDR, scope.ScopeId);
            }

            CustomPropertyRepeater.GetChanges(scope);

            Editor.Sesssion_UpdateServerInfo(webId, scope, ranges);
            bool addMode = Editor.Session_IsAddMode(this.WebId);

            if (addMode)
            {
                var dhcpServerType = Editor.Session_RetrieveServerType(webId);
                CheckPropertyTabIsHidden(webId, dhcpServerType);
            }

            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, webId);
            Response.Redirect(url, true);
        }

        protected void ValidateScopeName(object src, ServerValidateEventArgs args)
        {
            var name = txtScopeName.Text;
            if (String.IsNullOrEmpty(name))
                return;

            if (ddlDhcpServers.SelectedItem == null)
                return;

            var serverId = Convert.ToInt32(ddlDhcpServers.SelectedItem.Value.Split('-')[0]);
            if (serverId < 0)
                return;

            DhcpServer server;
            using (var proxy = SwisConnector.GetProxy())
            {
                server = proxy.AppProxy.DhcpServer.Get(serverId);
            }

            if (server == null)
                return;

            args.IsValid = !(server.ServerType == DhcpServerType.CISCO && name.Contains(" "));
        }

        private void CheckPropertyTabIsHidden(string webId, DhcpServerType dhcpServerType)
        {
            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            bool isCapableOfOfferDelay = typeof(IOfferDelayManagement).IsAssignableFrom(managerType);
            bool isCapableOfLeaseTimeProperty = typeof(IISCleaseTimePropertyManagement).IsAssignableFrom(managerType);
            bool hideScopePropertyTab;
            if (isCapableOfOfferDelay || isCapableOfLeaseTimeProperty)
            {
                hideScopePropertyTab = false;
            }
            else
            {
                hideScopePropertyTab = true;
            }
            Editor.Sesssion_UpdateScopePropertyIsHide(webId, hideScopePropertyTab);
        }

        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }

        private void InitializeDhcpServersDropDown()
        {
            var dhcpScopeManagementTypes = DhcpFactory.Instance.EnumDhcpManagersCapableOf(typeof(IScopeManagement)).ToList();

            ddlDhcpServers.Items.Clear();

            using (var proxy = SwisConnector.GetProxy())
            {
                var servers = proxy.AppProxy.DhcpServer.Get();
                foreach (var server in servers)
                {
                    // skip DHCP server which type is not capable of Scope management
                    if (!dhcpScopeManagementTypes.Contains(server.ServerType))
					{
                        continue;
					}

					if(server.ServerType == DhcpServerType.Infoblox)
					{
						continue;
					}

                    var item = new ListItem
                    {
                        Text = server.FriendlyName,
                        Value = server.GroupId.ToString(CultureInfo.InvariantCulture) + "-" +
                                Convert.ToInt32(server.ServerType) + "-" + Convert.ToInt32(server.NodeId)
                    };

                    ddlDhcpServers.Items.Add(item);
                }
               
            }

            if (ddlDhcpServers.Items.Count == 0)
            {
                ddlDhcpServers.Items.Add(new ListItem
                {
                    Text = Resources.IPAMWebContent.IPAMWEBDATA_VB1_465,
                    Value = "-1"
                });
                imgbNext.Enabled = false;
            }
        }

        #endregion // Methods
    }
}
