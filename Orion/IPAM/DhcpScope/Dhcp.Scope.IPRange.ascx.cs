﻿using System;
using System.Web.UI;

public partial class DhcpScope_IPRange : UserControl
{
    public string WebId { get; set; }
    public string ServerType { get; set; }
    public bool HideMgtProcess { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}