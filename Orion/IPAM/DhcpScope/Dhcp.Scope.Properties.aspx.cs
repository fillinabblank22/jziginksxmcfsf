using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeProperties : CommonPageServices
    {
        private const string HyperLinkTemplate = "<a target=\"_blank\" href=\"{0}\">{1}</a>";
        private const string ScopeUrl = "/api2/ipam/ui/scope?name={0}";
        private const string NodeUrl = "/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{0}";

        #region Declarations

        private PageDhcpScopeEditor Editor { get; set; }
        private DhcpSessionStorage Storage { get; set; }
        private GroupNode Group { get; set; }

        #endregion // Declarations

        #region Constructors

        public DhcpScopeProperties()
        {
            this.Editor = new PageDhcpScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ProgressIndicator1.CurrentStep = DhcpScopeWizardStep.Properties;
            Storage = new DhcpSessionStorage(this);

            if (!IsPostBack)
            {
                InitControls();
            }

            Title = string.Format(Title, Editor.Session_IsAddMode(null) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_68 : Resources.IPAMWebContent.IPAMWEBDATA_VB1_70);
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            PrepareFailoverInformation();

            // [oh] get proper DHCP manager to check capability
            var dhcpServerType = Editor.Session_RetrieveServerType(null);

            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            var isCapableOfOfferDelay = typeof(IOfferDelayManagement).IsAssignableFrom(managerType);
            int offerDelay;
            Editor.Session_RetrieveScopeProperties(null, out offerDelay);
            if (isCapableOfOfferDelay)
            {
                txtDelay.Text = offerDelay.ToString();
            }
            else
            {
                txtDelay.Text = "0";
                txtDelay.CssClass += " x-item-disabled disabled-textbox";
                divOfferDelay.Style.Add("display", "none");
            }

            txtDelay.ReadOnly = !isCapableOfOfferDelay;


            var isISCProperties = typeof(IISCleaseTimePropertyManagement).IsAssignableFrom(managerType);
            if (isISCProperties)
            {
                long? defaultLeastTime, minLeaseTime, maxLeaseTime;
                Editor.Session_RetrieveIscScopeProperties(null, out defaultLeastTime, out minLeaseTime, out maxLeaseTime);
                iscProperties.Visible = true;
                TimeSpan defLease, minLease, maxLease;
                if (defaultLeastTime != null)
                {
                    defLease = TimeSpan.FromSeconds(Convert.ToDouble(defaultLeastTime));
                    if (defaultLeastTime == -1)
                    {
                        DefUnlimitedBtn.Checked = true;
                        txtDaysDef.Enabled = false;
                        txtHoursDef.Enabled = false;
                        txtMinutesDef.Enabled = false;
                        txtSecondsDef.Enabled = false;
                    }
                    else
                    {
                        txtDaysDef.Text = defLease.Days.ToString();
                        txtHoursDef.Text = defLease.Hours.ToString();
                        txtMinutesDef.Text = defLease.Minutes.ToString();
                        txtSecondsDef.Text = defLease.Seconds.ToString();
                    }
                }

                if (minLeaseTime != null)
                {
                    minLease = TimeSpan.FromSeconds(Convert.ToDouble(minLeaseTime));
                    if (minLeaseTime == -1)
                    {
                        MinUnlimitedBtn.Checked = true;
                        txtDaysMin.Enabled = false;
                        txtHoursMin.Enabled = false;
                        txtMinutesMin.Enabled = false;
                        txtSecondsMin.Enabled = false;
                    }
                    else
                    {
                        txtDaysMin.Text = minLease.Days.ToString();
                        txtHoursMin.Text = minLease.Hours.ToString();
                        txtMinutesMin.Text = minLease.Minutes.ToString();
                        txtSecondsMin.Text = minLease.Seconds.ToString();
                    }
                }
                if (maxLeaseTime != null)
                {
                    maxLease = TimeSpan.FromSeconds(Convert.ToDouble(maxLeaseTime));
                    if (maxLeaseTime == -1)
                    {
                        MaxUnlimitedBtn.Checked = true;
                        txtDaysMax.Enabled = false;
                        txtHoursMax.Enabled = false;
                        txtMinutesMax.Enabled = false;
                        txtSecondsMax.Enabled = false;
                    }
                    else
                    {
                        txtDaysMax.Text = maxLease.Days.ToString();
                        txtHoursMax.Text = maxLease.Hours.ToString();
                        txtMinutesMax.Text = maxLease.Minutes.ToString();
                        txtSecondsMax.Text = maxLease.Seconds.ToString();
                    }
                }

            }
            Editor.ActivateProgressStep(null, PageDhcpScopeEditor.IPAM_DHCPSCOPE_PROPERTIES);
        }

        private void Next()
        {
            if (!ValidateUserInput())
                return;
            int offerDelay = int.Parse(txtDelay.Text);
            Editor.Session_UpdateScopeProperties(null, offerDelay);
            var dhcpServerType = Editor.Session_RetrieveServerType(null);

            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            var isISCProperties = typeof(IISCleaseTimePropertyManagement).IsAssignableFrom(managerType);

            if (isISCProperties)
            {
                SolarWinds.IPAM.BusinessObjects.DhcpScopeProperties iscScopeProperties = new SolarWinds.IPAM.BusinessObjects.DhcpScopeProperties();
                TimeSpan defLease, minLease, maxLease;
                if (DefLimitedBtn.Checked)
                {
                    if (txtDaysDef.Text != "" || txtHoursDef.Text != "" || txtMinutesDef.Text != "" || txtSecondsDef.Text != "")
                    {
                        defLease = new TimeSpan(
                           txtDaysDef.Text.Trim() != "" ? int.Parse(txtDaysDef.Text) : 0,
                           txtHoursDef.Text.Trim() != "" ? int.Parse(txtHoursDef.Text) : 0,
                           txtMinutesDef.Text.Trim() != "" ? int.Parse(txtMinutesDef.Text) : 0,
                           txtSecondsDef.Text.Trim() != "" ? int.Parse(txtSecondsDef.Text) : 0
                            );
                        iscScopeProperties.DefaultLeaseTime = Convert.ToInt64(defLease.TotalSeconds);
                    }
                    else
                    {
                        iscScopeProperties.DefaultLeaseTime = null;
                    }
                }
                else
                    iscScopeProperties.DefaultLeaseTime = -1;

                if (MinLimitedBtn.Checked)
                {
                    if (txtDaysMin.Text != "" || txtHoursMin.Text != "" || txtMinutesMin.Text != "" || txtSecondsMin.Text != "")
                    {
                        minLease = new TimeSpan(
                                          txtDaysMin.Text.Trim() != "" ? int.Parse(txtDaysMin.Text) : 0,
                                          txtHoursMin.Text.Trim() != "" ? int.Parse(txtHoursMin.Text) : 0,
                                          txtMinutesMin.Text.Trim() != "" ? int.Parse(txtMinutesMin.Text) : 0,
                                          txtSecondsMin.Text.Trim() != "" ? int.Parse(txtSecondsMin.Text) : 0
                                           );
                        iscScopeProperties.MinLeaseTime = Convert.ToInt64(minLease.TotalSeconds);
                    }
                    else
                    {
                        iscScopeProperties.MinLeaseTime = null;
                    }
                }
                else
                {
                    iscScopeProperties.MinLeaseTime = -1;
                }

                if (MaxLimitedBtn.Checked)
                {
                    if (txtDaysMax.Text != "" || txtHoursMax.Text != "" || txtMinutesMax.Text != "" || txtSecondsMax.Text != "")
                    {
                        maxLease = new TimeSpan(
                           txtDaysMax.Text.Trim() != "" ? int.Parse(txtDaysMax.Text) : 0,
                           txtHoursMax.Text.Trim() != "" ? int.Parse(txtHoursMax.Text) : 0,
                           txtMinutesMax.Text.Trim() != "" ? int.Parse(txtMinutesMax.Text) : 0,
                           txtSecondsMax.Text.Trim() != "" ? int.Parse(txtSecondsMax.Text) : 0
                            );
                        iscScopeProperties.MaxLeaseTime = Convert.ToInt64(maxLease.TotalSeconds);
                    }
                    else
                    {
                        iscScopeProperties.MaxLeaseTime = null;
                    }

                }
                else
                    iscScopeProperties.MaxLeaseTime = -1;

                Editor.Session_UpdateIscScopeProperties(null, iscScopeProperties);
            }
            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
            Response.Redirect(url, true);
        }

        private bool ValidateUserInput()
        {
            //function kept here to validate ISC device lease time related validation
            return true;
        }

        private void PrepareFailoverInformation()
        {
            int groupId = Convert.ToInt32(Storage["GroupId"]);
            if (groupId < 1)
            {
                lbFailover.Visible = false;
                return;
            }

            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                Group = proxy.AppProxy.GroupNode.GetWithFailover(groupId, true);
            }

            string relationshipName = Group.RelationshipName;
            if (string.IsNullOrEmpty(relationshipName))
            {
                lbFailover.Visible = false;
                return;
            }

            // Displaying failover information.
            List<GroupNode> groups;
            string secondaryServerName = Group.FailoverServerType == FailoverServer.PrimaryServer
                ? Group.SecondaryServer
                : Group.PrimaryServer;
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                groups = proxy.AppProxy.GroupNode.Get(string.Format("Address = '{0}'", secondaryServerName), "A");
            }

            litFailoverInformation.Text = WebSecurityHelper.SanitizeHtmlV2(GetFailoverInformation(groups, secondaryServerName));
        }

        private string GetFailoverInformation(List<GroupNode> groups, string secondaryServerName)
        {
            if (!groups.Any())
            {
                return string.Format(Resources.IPAMWebContent.IPAMWEBDATA_DM1_4,
                    GetFailoverMode(Group.Mode), secondaryServerName, Group.RelationshipName);
            }

            GroupNode group = groups.First();
            string serverName = group.FriendlyName;
            GroupNode entity;
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                entity = proxy.AppProxy.GroupNode.GetServerInfo(group.GroupId, Group.Address, true);
            }

            if (entity == null)
            {
                return string.Format(Resources.IPAMWebContent.IPAMWEBDATA_DM1_4,
                    GetFailoverMode(Group.Mode), secondaryServerName, Group.RelationshipName);
            }
            var dhcpScopeName = entity.FriendlyName;
            var nodeId = entity.NodeId;

            return
                FailoverInformationString(dhcpScopeName, nodeId, serverName, Group.RelationshipName, Group.Mode);
        }

        private string FailoverInformationString(string dhcpScopeName, int parentId, string parentName,
            string relationName, FailoverMode? failoverMode)
        {
            string scopeLink = string.Format(ScopeUrl, HttpUtility.UrlEncode(dhcpScopeName));
            string nodeLink = string.Format(NodeUrl, parentId);
            string scopeStr = string.Format(HyperLinkTemplate, scopeLink, dhcpScopeName);
            string nodeStr = string.Format(HyperLinkTemplate, nodeLink, parentName);

            return string.Format(Resources.IPAMWebContent.IPAMWEBDATA_DM1_2, GetFailoverMode(failoverMode),
                scopeStr, nodeStr, relationName);
        }

        private string GetFailoverMode(FailoverMode? mode)
        {
            return mode == FailoverMode.HotStandby ? "Hot Standby" : "Load Balance";
        }

        #endregion // Methods
    }
}
