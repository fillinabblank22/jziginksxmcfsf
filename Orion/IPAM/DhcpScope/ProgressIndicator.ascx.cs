using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;

public enum DhcpScopeWizardStep
{
    Define,
    Address,
    Options,
    Properties,
    Review
};

public struct Steps
{
    public DhcpScopeWizardStep StepName;
    public string Pageurl;
    public string ImageName;
    public string HighlightImage;
    public string Title;
}

public partial class DhcpScope_ProgressIndicator : System.Web.UI.UserControl
{
    private readonly string indicatorImageFolderPath = "~/Orion/IPAM/res/images/progress_indicator/";
    List<Steps> scopeSteps = new List<Steps>();

    public string WebId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        scopeSteps.Add(new Steps() { StepName = DhcpScopeWizardStep.Define, Pageurl = "Dhcp.Scope.Wizard.aspx", ImageName = "PI_definingscope_off.png", HighlightImage = "PI_definingscope_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_VB1_516 });
        scopeSteps.Add(new Steps() { StepName = DhcpScopeWizardStep.Address, Pageurl = "Dhcp.Scope.Address.aspx", ImageName = "PI_ipaddressrange_off.png", HighlightImage = "PI_ipaddressrange_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_VB1_517 });
        if (!IsScopePropertySupported())
        {
            scopeSteps.Add(new Steps()
            {
                StepName = DhcpScopeWizardStep.Properties,
                Pageurl = "Dhcp.Scope.Properties.aspx",
                ImageName = "PI_scopeproperties_off.png",
                HighlightImage = "PI_scopeproperties_on.png",
                Title = Resources.IPAMWebContent.IPAMWEBDATA_VB1_518
            });
        }
        scopeSteps.Add(new Steps() { StepName = DhcpScopeWizardStep.Options, Pageurl = "Dhcp.Scope.Options.aspx", ImageName = "PI_scopeoptions_off.png", HighlightImage = "PI_scopeoptions_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_VB1_519 });
        scopeSteps.Add(new Steps() { StepName = DhcpScopeWizardStep.Review, Pageurl = "Dhcp.Scope.Review.aspx", ImageName = "PI_review_off.png", HighlightImage = "PI_review_on.png", Title = Resources.IPAMWebContent.IPAMWEBDATA_VB1_520 });

        LoadProgressImages();  
    }

    #region public properties

    [Browsable(true)]
    [Category("Appearance")]
    public DhcpScopeWizardStep CurrentStep
    {
        set;
        get;
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string NextStep
    {
        get
        {
            int index = scopeSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index + 1 < scopeSteps.Count) ? scopeSteps[index + 1].Pageurl : scopeSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string PrevStep
    {
        get
        {
            int index = scopeSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index - 1 >= 0) ? scopeSteps[index - 1].Pageurl : scopeSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string HomePage
    {
        get
        {
            return "~/api2/ipam/ui/scopes";
        }
    }
    #endregion

    #region public methods

    public void LoadProgressImages()
    {
        RenderProgressImages();
    }

    #endregion

    #region private methods

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();
        int count = scopeSteps.Count;

        for (int i = 0; i < count; i++)
        {
            bool nextActive;

            Image imgSeparator = new Image();
            imgSeparator.ID = string.Format("imgSep{0}", i + 1);
            bool isCurrentStep = false;
            string url = "";

            nextActive = (i < count - 1) ? (this.CurrentStep == scopeSteps[i + 1].StepName) : false;

            if (IsStepActive(this.scopeSteps[i].StepName.ToString()))
            {
                url = PageDhcpScopeEditor.AddWebIdParam(this.Page,
                    scopeSteps[i].Pageurl, this.WebId);              
            }          

            if (this.CurrentStep == scopeSteps[i].StepName)
            {
                isCurrentStep = true;
                imgSeparator.ImageUrl = string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off");
            }
            else
            {
                imgSeparator.ImageUrl = (nextActive) ? string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on") : string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off");

            }

            HyperLink tb = new HyperLink();
            tb.Text = scopeSteps[i].Title;
            tb.NavigateUrl = url;
            tb.CssClass = string.Format("PI_{0}", isCurrentStep ? "on" : "off");
            tb.Attributes["automation"] = tb.Text;
            // Text of first step must be move to the right a little 
            if (i == 0)
            {
                tb.Style.Add("padding-left", "10px;");
            }

            phPluginImages.Controls.Add(tb);
            phPluginImages.Controls.Add(imgSeparator);
        }
    }


    private bool IsStepActive(string stepName)
    {
        using (var storage = new DhcpSessionStorage(this.Page, this.WebId))
        {
            return storage[stepName] != null ? 
                Convert.ToBoolean(storage[stepName]) : false;
        }
    }
    private bool IsScopePropertySupported()
    {
        using (var storage = new DhcpSessionStorage(this.Page, this.WebId))
        {
            return storage["IPAM_Dhcp_Scope_Property_Hide"] != null ?
                Convert.ToBoolean(storage["IPAM_Dhcp_Scope_Property_Hide"]) : false;
        }
    }

    #endregion
}
