<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.Scope.Review.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeProperties"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_511%>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/Dhcp.ScopeOptions.ascx" TagName="AllOptions"
    TagPrefix="orion" %>

<%@ Register Src="~/Orion/IPAM/DhcpScope/Dhcp.Scope.IPRange.ascx" TagName="IPRange"
    TagPrefix="orion" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/ProgressIndicator.ascx" TagName="Progress"
    TagPrefix="orion" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGCreatingDHCPScopes" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
.tablediv {display: table;}
.rowdiv  { width:850px;display: table-row;}
.summaryHeader {  font-size: small;font-weight: bold; width:250px;display: table-cell;float:left;padding-top: 10px;}
.summaryData{    font-size: small;font-weight: normal; width:600px;display: table-cell;float:left;padding-top: 10px;}
.dnsLabels{   display:none; list-style-type:circle; }
    </IPAMmaster:CssBlock>

    <orion:Include File="breadcrumb.js" runat="server" />
    <IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-ux-wait.js,sw-dhcp-scopewizard.js,sw-dhcp-manage.js" Orientation="jsPostInit" runat="server">
 $(document).ready(function(){ if($SW.TaskId && $SW.TaskId != ''){      
    var title = $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_253;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_254;E=js}';
    var urlTpl = "../DhcpSplitScope/Dhcp.SplitScope.Wizard.aspx?ScopeId={0}&wId={1}";

    $SW.Tasks.Attach($SW.TaskId,
    { title: $SW.AddMode ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_255;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_256;E=js}" },
    function(d) {
        if (d.results && d.results.LicenseInfo) {
            var licInfo = Ext.util.JSON.decode(d.results.LicenseInfo);
            $SW.LicenseListeners.recv(licInfo);
        }
        if (d.success){
            var warning = false;
            if (d.results) { $.each(d.results, function(i, result) { if (result.Success === false) warning = true; }); }

            var redirectFn = $SW.Split ? function(){ window.location = String.format(urlTpl, $SW.GroupId, $SW.WebId); } : function(){ window.location = "../../../api2/ipam/ui/scopes"; };
            if (warning) {
                d.message = $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_257;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_258;E=js}'
                Ext.Msg.show({
                    title: title, buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING,
                    msg: $SW.IPAM.DhcpScopeWarnMsgTpl.apply(d), fn: redirectFn
                });
            } else {
                Ext.Msg.show({
                    title: title, buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO ,
                    msg: $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_259;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_260;E=js}',
                    fn: redirectFn
                });
            }
        } else Ext.MessageBox.show({
            title: $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_261;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_262;E=js}',
            msg: $SW.IPAM.DhcpScopeErrMsgTpl.apply(d),
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR                 
        });
    });
}});
    </IPAMmaster:JsBlock>

      <script language="javascript" type="text/javascript">

          $SW.TaskId = '<%=TaskId %>';
          $SW.AddMode = '<%=AddMode %>' == 'True' ? true : false;
          $SW.Split = '<%=Split %>' == 'True' ? true : false;
          $SW.GroupId = '<%=GroupId %>';
          $SW.WebId = '<%=WebId %>';

          <%if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
          $(document).ready(function () {
              var type = '<%=(int)DhcpServerType %>';
              var server = '<%=DhcpServerGroupId %>';
              $('.split-scope-button').click(function (e) {
                  e.preventDefault();
                  var url = $(this).attr('href'), fn = function () { window.location.href = url; };
                  $SW.IPAM.DhcpDnsManage.SplitScopeWizardCheck(this, type, server, fn);
              });
          });
          <%}%>
      </script>

    <IPAMui:ValidationIcons runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                           { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/dhcp" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto;
                                            height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/scopes" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                              { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div>
            <table width="1000px" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <orion:Progress ID="ProgressIndicator1" runat="server" />
                        <div class="GroupBox" id="NormalText">
                            <div>
                                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_520%> </span>
                                <br />
                            </div>
                            <div class="contentBlock">
                                <div class="tablediv" style="margin-left: 10px;">
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_578%></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DHCPServerLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_579 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DHCPScopeLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_580 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DHCPScopeDescLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_196 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="VlanLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_197 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="LocationLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    
                                    <div id="scopeaddressrangediv" runat="server">
                                        <div class="rowdiv">
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_524%></span><span class="summaryData">
                                                <asp:Label runat="server" ID="StartAddressLabel"></asp:Label></span>
                                        </div>
                                        <div>
                                            <!-- ie6 -->
                                        </div>
                                        <div class="rowdiv">
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_530%></span> <span class="summaryData">
                                                <asp:Label runat="server" ID="EndAddressLabel"></asp:Label></span>
                                        </div>
                                        <div>
                                            <!-- ie6 -->
                                        </div>
                                    </div>

                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_44%></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="CIDRLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>

                                    <div class="rowdiv">
                                        <% if ((int)this.DhcpServerType != 4)
                                           {%>
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_538 %></span>
                                        <%} else {%>
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_14 %></span>
                                        <%}%>
                                        <span class="summaryData"><asp:Label runat="server" ID="SubnetMaskOrNetmaskLabel"></asp:Label></span>
                                    </div>
                                    
                                    <% if ((int)this.DhcpServerType == 4) {%>
                                        <div class="rowdiv">
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_20%>:</span> 
                                            <span class="summaryData"><asp:Label runat="server" ID="sharedNetLabel"></asp:Label></span>
                                        </div>
                                        <div><!-- ie6 --></div>
                                    <%}%>
                                </div>  
                                
                                <div id="exclusiondiv" runat="server">
                                    <div style="max-width: 1000px; padding: 0px 6px 10px 10px;">
                                        <div class="rowdiv">
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_581 %></span> <span class="summaryData">
                                                <asp:Repeater runat="server" ID="contentsRepeater">
                                                    <ItemTemplate>
                                                        <li style="list-style-type: none; *position: relative; *left: -18px;">
                                                            <%# DataBinder.Eval(Container.DataItem, "StartIp")%>
                                                            -
                                                            <%# DataBinder.Eval(Container.DataItem, "EndIp")%>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </span>
                                        </div>
                                    </div>
                                    <div>
                                            <!-- ie6 -->
                                    </div>
                                </div>

                                <div class="tablediv" style="margin-left: 10px;" id="LeaseTimediv" runat="server">
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_588 %></span>
                                        <span id="dhcpOfferDelay" class="summaryData" runat="server">
                                            <asp:Label runat="server" ID="OfferDelayLabel" Text="{0} ms"></asp:Label>
                                        </span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                </div>
                                <div class="tablediv" style="margin-left: 10px;" id="IscScopePropertydiv" runat="server">
                                    <div class="rowdiv" id="DefaultLeasediv" runat="server">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_15 %></span>
                                        <span id="Span1" class="summaryData" runat="server">
                                            <asp:Label runat="server" ID="DefaultLeaseLabel" Text="{0}"></asp:Label>
                                        </span>
                                    </div>
                                    <div class="rowdiv" id="MinLeasediv" runat="server">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_16 %></span>
                                        <span id="Span2" class="summaryData" runat="server">
                                            <asp:Label runat="server" ID="MinLeaseLabel" Text="{0}"></asp:Label>
                                        </span>
                                    </div>
                                    <div class="rowdiv" id="MaxLeasediv" runat="server">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_17 %></span>
                                        <span id="Span3" class="summaryData" runat="server">
                                            <asp:Label runat="server" ID="MaxLeaseLabel" Text="{0}"></asp:Label>
                                        </span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                </div>
                                <div class="tablediv" style="margin-left: 10px;" id="poolrangediv" runat="server">
                                    <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_13 %></span>
                                    <br/>
                                    <table cellspacing="0">
                                        <tr>
                                            <td style="width: 1px"></td>
                                            <td>
                                                <br />
                                                <orion:IPRange ID="IPRangeGrid" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                </div>
                                <div class="tablediv" style="margin-left: 10px;" id="divAllOptions">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_8 %></span>
                                        <br/>
                                    <table cellspacing="0">
                                        <tr>
                                            <td style="width: 1px">
                                            </td>
                                            <td>
                                                <orion:AllOptions ID="DhcpAllOptions" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table cellspacing="5">
                                    <tr>
                                        <td class="leftLabelColumn" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="leftLabelColumn">
                                            &nbsp;
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                                <div class="sw-btn-bar-wizard">
                                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Back"
                                        ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click" />
                                    <orion:LocalizableButton runat="server" DisplayType="Primary" LocalizedText="CustomText"
                                        Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_595 %>" CausesValidation="true" ID="imgbOk" OnClick="imgbOk_Click" />
                                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="CustomText" CssClass="split-scope-button"
                                        Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_596 %>" CausesValidation="true" ID="imgbSplit" OnClick="imgbSplit_Click" Visible="false" />
                                    <orion:LocalizableButton runat="server" DisplayType="Secondary" LocalizedText="Cancel"
                                        ID="imgbCancel" CausesValidation="false" OnClick="imgbCancel_Click" />
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
