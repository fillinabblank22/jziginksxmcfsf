﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dhcp.Scope.Exclusions.ascx.cs" Inherits="DhcpScope_Exclusions" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock requires="ext,ext-quicktips,sw-dhcp-scopewizard.js" Orientation="jsPostInit" runat="server"/>
<script type="text/javascript">
if(!$SW.IPAM.DHCPExclusions) $SW.IPAM.DHCPExclusions = {};
$SW.IPAM.DHCPExclusions.GridId = 'ExclusionsGrid';
$SW.IPAM.DHCPExclusions.WebId = '<%=this.WebId %>';
$SW.IPAM.DHCPExclusions.SetWebId = function(){
    $SW.store.baseParams.w = '<%=this.WebId %>';
};

Ext.onReady(function(){
    $SW.IPAM.DHCPExclusions.ScopeIPRangeSupplyFn = <%=ScopeIPRangeSupplyFn %>;
});
</script>

<IPAMlayout:DialogWindow jsID="exclusionsWindow" helpID="exclusionedit" Name="exclusionsWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_604 %>" height="200" width="550" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="exclusionsSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['exclusionsWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="exlusionsCancel" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['exclusionsWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['exclusionsWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['exclusionsWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="close" runat="server">
        <handler>function(n,o,info){ $SW['exclusionsWindow'].hide(); $SW.IPAM.DHCPExclusions.RefreshGrid(); }</handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:GridBox runat="server" ID="ExclusionsGrid" title="" UseLoadMask="True" autoHeight="false" Height="200"
    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_605 %>" borderVisible="true" deferEmptyText="false"
    SingleSelection="false" listeners="{ render: $SW.IPAM.DHCPExclusions.SetWebId }">
    <ConfigOptions runat="server">
        <Options>
            <IPAMlayout:ConfigOption Name="plugins" Value="[ new $SW.IPAM.DHCPExclusions.SelectionChangedPlugin({
            add: 'btnAdd',
            edit: 'btnEdit',
            remove: 'btnRemove'}) ]" IsRaw="true" runat="server" />
        </Options>
    </ConfigOptions>
    <TopMenu borderVisible="false">
        <IPAMui:ToolStripMenuItem id="btnAdd" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_606 %>" runat="server">
            <handler>$SW.IPAM.DHCPExclusions.AddExclusionBtnPressed()</handler>
        </IPAMui:ToolStripMenuItem>
        <IPAMui:ToolStripMenuItem id="btnEdit" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-edit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_607 %>" Disabled="true" runat="server">
            <handler>$SW.IPAM.DHCPExclusions.EditExclusionBtnPressed()</handler>
        </IPAMui:ToolStripMenuItem>
        <IPAMui:ToolStripMenuItem id="btnRemove" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-delete" text="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_608 %>" Disabled="true" runat="server">
            <handler>$SW.IPAM.DHCPExclusions.RemoveExclusionBtnPressed()</handler>
        </IPAMui:ToolStripMenuItem>
    </TopMenu>
    <Columns>
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_609 %>" width="300" isSortable="true" runat="server" dataIndex="AddressStart" />
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_610 %>" width="300" isSortable="true" runat="server" dataIndex="AddressEnd" />
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_611 %>" width="200" isSortable="true" runat="server" dataIndex="AddressCount" />
    </Columns>
    <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../sessiondataprovider.ashx"
        proxyEntity="IPAM.DhcpExclusions" AmbientSort="ID"
        listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
        <Fields>
            <IPAMlayout:GridStoreField dataIndex="ID" isKey="True" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="AddressStart" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="AddressEnd" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="AddressCount" runat="server" />
       </Fields>
    </Store>
</IPAMlayout:GridBox>