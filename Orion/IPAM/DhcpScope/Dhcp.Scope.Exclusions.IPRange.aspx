﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True" Inherits="SolarWinds.IPAM.WebSite.DhcpScope_Exclusions_IPRange"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_597%>" CodeFile="Dhcp.Scope.Exclusions.IPRange.aspx.cs" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
</IPAMmaster:JsBlock>

<script type="text/javascript" language="javascript">
    if (!$SW.IPAMValid) $SW.IPAMValid = {};
    $SW.IPAMValid.ValidateIPRange = function (sender) {
        var startValidator = $("#<%=StartExclusionRangeValidator.ClientID %>")[0];
        var endValidator = $("#<%=EndExclusionRangeValidator.ClientID %>")[0];

        if (!startValidator || !endValidator)
            return;

        if (startValidator === sender && !endValidator.isvalid) {
            endValidator.isvalid = true;
            ValidatorValidate(endValidator);
        }
        if (endValidator === sender && !startValidator.isvalid) {
            startValidator.isvalid = true;
            ValidatorValidate(startValidator);
        }
    };
</script>

<IPAMui:ValidationIcons ID="ValidationIcons1" runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%# Resources.IPAMWebContent.IPAMWEBDATA_VB1_598%></div>
    
    <div style="padding-bottom:20px">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_599 %>
    </div>
    
    <div>
        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
            <tr class="sw-form-cols-normal">
                <td class="sw-form-col-label"></td>
                <td class="sw-form-col-control"></td>
                <td class="sw-form-col-comment"></td>
            </tr>
            <tr>
                <td><div class="sw-field-label">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_600%>
                </div></td>
                <td><div class="sw-form-item">
                    <asp:TextBox ID="txtStartingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
                </div></td>
                <td><asp:RequiredFieldValidator ID="StartingNetworkAddressRequire" runat="server"
                      ControlToValidate="txtStartingNetworkAddress"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_221 %>" />
                    <asp:CustomValidator ID="StartingNetworkAddressIPv4" runat="server"
                      ControlToValidate="txtStartingNetworkAddress"
                      ClientValidationFunction="$SW.Valid.Fns.ipv4"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_222 %>"/>
                    <asp:CustomValidator ID="StartAddressToEnd" runat="server"
                      ControlToValidate="txtStartingNetworkAddress"
                      OnServerValidate="OnStartAddressToEndValidate"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources :IPAMWebContent, IPAMWEBDATA_VB1_601 %>"/>
                    <asp:CustomValidator ID="StartAddressToScope" runat="server"
                      ControlToValidate="txtStartingNetworkAddress"
                      OnServerValidate="OnStartAddressToScopeValidate"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources :IPAMWebContent, IPAMWEBDATA_VB1_602 %>"/>
                    <asp:CustomValidator ID="StartExclusionRangeValidator" runat="server"
                      OnServerValidate="OnExclusionRangeValidate"
                      ClientValidationFunction="$SW.IPAMValid.ValidateIPRange"
                      ControlToValidate="txtStartingNetworkAddress"
                      Display="Dynamic"/></td>
            </tr>
            <tr> 
                <td><div class="sw-field-label">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_224%>
                </div></td>
                <td><div class="sw-form-item sw-form-half">
                 <asp:TextBox ID="txtEndingNetworkAddress" CssClass="x-form-text x-form-field" runat="server" />
                </div></td>
                <td><asp:CustomValidator ID="EndingNetworkAddressIPv4" runat="server"
                      ControlToValidate="txtEndingNetworkAddress"
                      ClientValidationFunction="$SW.Valid.Fns.ipv4"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_226 %>"/>
                    <asp:CustomValidator ID="EndAddressToScope" runat="server"
                      ControlToValidate="txtEndingNetworkAddress"
                      OnServerValidate="OnEndAddressToScopeValidate"
                      Display="Dynamic"
                      ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_603 %>"/>
                    <asp:CustomValidator ID="EndExclusionRangeValidator" runat="server"
                      OnServerValidate="OnExclusionRangeValidate"
                      ClientValidationFunction="$SW.IPAMValid.ValidateIPRange"
                      ControlToValidate="txtEndingNetworkAddress"
                      Display="Dynamic"/></td>
            </tr>
                
        </table>
    </div>
</div>
<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    
    <table>
        <tr>
            <td style="width: 150px">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave" DisplayType="Primary" LocalizedText="Save"/>
                    <orion:LocalizableButtonLink runat="server" NavigateUrl="/Orion/IPAM/DhcpScope/Dhcp.Scope.Address.aspx" DisplayType="Secondary" LocalizedText="Cancel"/>
</div>
            </td>
        </tr>
    </table>


</asp:Content>