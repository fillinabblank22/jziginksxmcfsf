using System;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Helpers;
using System.Collections.Generic;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using System.Web;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeProperties : CommonPageServices
    {
        #region Declarations

        private PageDhcpScopeEditor Editor { get; set; }

        #endregion // Declarations

        #region Constructors

        public DhcpScopeProperties()
        {
            this.Editor = new PageDhcpScopeEditor(this);
        }

        #endregion // Constructors

        #region Properties

        private const string PARAM_TASKID = "taskid";
        private const string PARAM_SPLIT = "split";
        private const string QueryWebIdParam = "w";

        public string TaskId
        {
            get
            {
                return base.GetParam(PARAM_TASKID, string.Empty);
            }
        }

        public bool Split
        {
            get
            {
                return base.GetParam(PARAM_SPLIT, false);
            }
        }

        public bool AddMode { get; set; }
        public int? GroupId { get; set; }
        public string WebId { get; set; }

        public DhcpServerType DhcpServerType { get; private set; }
        public int DhcpServerGroupId { get; private set; }
        
        #endregion // Properties

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.AddMode = Editor.Session_IsAddMode(null);
            ProgressIndicator1.CurrentStep = DhcpScopeWizardStep.Review;

            if (!IsPostBack)
            {
                InitControls();
                this.WebId = GetParam(QueryWebIdParam, null);
            }
            DhcpAllOptions.ViewMode = true;
            DhcpAllOptions.WebId = WebId;

            IPRangeGrid.HideMgtProcess = true;
            IPRangeGrid.WebId = WebId;

            Title = string.Format(Title, this.AddMode ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_68 : Resources.IPAMWebContent.IPAMWEBDATA_VB1_70);
        }

        protected void imgbOk_Click(object sender, EventArgs e)
        {
            Next(false);
        }

        protected void imgbSplit_Click(object sender, EventArgs e)
        {
            Next(true);
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            // [oh] get proper DHCP manager to check capability
            var dhcpServerType = Editor.Session_RetrieveServerType(null);

            var factory = DhcpFactory.Instance.GetDeviceDhcpFactory(dhcpServerType);
            var managerType = factory.GetDhcpManagerType();
            var isCapableOfExclusions = typeof(IExclusionManagement).IsAssignableFrom(managerType);
            var isCapableOfOfferDelay = typeof(IOfferDelayManagement).IsAssignableFrom(managerType);
            var isCapableOfScopeRange = typeof(IScopeRangeManagement).IsAssignableFrom(managerType);
            var isCapableOfSplitScope = typeof(ISplitScopeManagement).IsAssignableFrom(managerType);
            var isCapableOfScopeMultipleRangeManagement = typeof(IScopeMultipleRangeManagement).IsAssignableFrom(managerType);
            var isCapableOfScopeProperties = typeof(IISCleaseTimePropertyManagement).IsAssignableFrom(managerType);

            DhcpScope scope;
            List<DhcpRange> ranges = new List<DhcpRange>();
            int offerDelay;
            List<IpRange> exclusions;
            
            long? defaultLeastTime, minLeaseTime, maxLeaseTime;
            var webId = GetParam(DhcpSessionStorage.PARAM_WEBID, null);
            DHCPServerLabel.Text = Editor.Session_RetrieveServerName(webId);

            Editor.Session_RetrieveScopeInfo(null, out scope, out ranges);
            DHCPScopeLabel.Text = HttpUtility.HtmlEncode(scope.FriendlyName);
            DHCPScopeDescLabel.Text = HttpUtility.HtmlEncode(scope.Comments);
            VlanLabel.Text = HttpUtility.HtmlEncode(scope.VLAN);
            LocationLabel.Text = HttpUtility.HtmlEncode(scope.Location);
            StartAddressLabel.Text = ranges[0].StartAddress;
            EndAddressLabel.Text = ranges[0].EndAddress;
            CIDRLabel.Text = scope.CIDR.ToString();
            SubnetMaskOrNetmaskLabel.Text = scope.AddressMask;

            if (isCapableOfOfferDelay)
            {
                Editor.Session_RetrieveScopeProperties(null, out offerDelay);
                OfferDelayLabel.Text = string.Format(OfferDelayLabel.Text, offerDelay.ToString());
            }

            if (isCapableOfExclusions)
            {
                Editor.Session_RetrieveExclusions(null, out exclusions);
                contentsRepeater.DataSource = exclusions;
                contentsRepeater.DataBind();
            }

            contentsRepeater.Visible = isCapableOfExclusions;
            exclusiondiv.Visible = isCapableOfExclusions;
            poolrangediv.Visible = isCapableOfScopeMultipleRangeManagement;
            LeaseTimediv.Visible = isCapableOfOfferDelay;
            scopeaddressrangediv.Visible = isCapableOfScopeRange;
            IscScopePropertydiv.Visible = isCapableOfScopeProperties;

            if (isCapableOfScopeProperties)
            {
                
                Editor.Session_RetrieveIscScopeProperties(null,out defaultLeastTime, out minLeaseTime, out maxLeaseTime);

                DefaultLeasediv.Visible = MinLeasediv.Visible = MaxLeasediv.Visible = true;

                DefaultLeaseLabel.Text = defaultLeastTime.HasValue ? (defaultLeastTime == -1 ? Resources.IPAMWebContent.IPAMWEBDATA_GK_28 : string.Format(DefaultLeaseLabel.Text,
                        defaultLeastTime.ToString()+" seconds")): string.Empty;
                MinLeaseLabel.Text = minLeaseTime.HasValue ? (minLeaseTime == -1 ? Resources.IPAMWebContent.IPAMWEBDATA_GK_28 : string.Format(MinLeaseLabel.Text,
                       minLeaseTime.ToString()+" seconds" )): string.Empty;
                MaxLeaseLabel.Text = maxLeaseTime.HasValue ? (maxLeaseTime == -1 ? Resources.IPAMWebContent.IPAMWEBDATA_GK_28 : string.Format(MaxLeaseLabel.Text, 
                    maxLeaseTime.ToString()+" seconds")) : string.Empty;
            }

            if (dhcpServerType == DhcpServerType.ISC)
            {
                var sharedNetName = string.Empty;
                Editor.Session_RetrieveSharedNetName(webId, out sharedNetName);
                sharedNetLabel.Text = (sharedNetName == Resources.IPAMWebContent.IPAMWEBDATA_SE_21) ? string.Empty : sharedNetName;
            }

            this.DhcpServerType = dhcpServerType;
            this.DhcpServerGroupId = scope.ParentId;

            if (!this.AddMode)
                imgbOk.Text = Resources.IPAMWebContent.IPAMWEBCODE_VB1_6;

            imgbSplit.Visible = this.AddMode && isCapableOfSplitScope;

            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                imgbOk.OnClientClick = "javascript:demoAction('IPAM_Create_DHCP_Scope',null); return false;";
                imgbSplit.OnClientClick = "javascript:demoAction('IPAM_Create_DHCP_Split_Scope',null); return false;";
            }
        }

        private void Next(bool splitMode)
        {
            if (!ValidateUserInput())
                return;

            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Editor.Save(null, splitMode);

                string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
                Response.Redirect(url, true);
            }
        }

        private bool ValidateUserInput()
        {
            return true;
        }

        #endregion // Methods
    }
}
