﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dhcp.ScopeOptions.ascx.cs"
    Inherits="Dhcp_ScopeOptions" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register Src="~/Orion/IPAM/Controls/GenericDhcpOptions/DhcpOptionLayoutBuilder.ascx"
    TagPrefix="orion" TagName="WhatIsNew" %>
<IPAMmaster:JsBlock ID="JsBlock2" Requires="ext,ext-quicktips,sw-alloptions-manage.js,ext-ux,sw-ux-wait.js,sw-option-layout.js,sw-ipaddressgrid-manage.js"
    Orientation="jsPostInit" runat="server" />
<orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
<orion:Include File="ResourcePicker.css" runat="server" />
<orion:Include ID="Include4" runat="server" File="RenderControl.js" />
<orion:Include File="breadcrumb.js" runat="server" />
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include File="ResourcePicker.css" runat="server" />
<script type="text/javascript">
    var serverType = '<%=this.ServerType %>';
    var hasLeaseTime = false;
    if (!$SW.IPAM.Options) $SW.IPAM.Options = [];
    $SW.IPAM.Options['<%=this.ID %>'] = $SW.IPAM.OptionsList({
        GridId: '<%=this.DHCPoptionsgrid.ClientID %>',
        WebId: '<%=this.WebId %>'
    });

    $SW.IPAM.Options['<%=this.ID %>'].grid_cellclick = function (grid, rowIndex, columnIndex, e) {
        $SW.ext.gridClickToDrilldown(grid, rowIndex, columnIndex, e);

        var be = e.browserEvent.target || e.browserEvent.srcElement;
        if (be.tagName.toUpperCase() == 'IMG')
            be = be.parentNode;

        if (be.tagName.toUpperCase() != 'A')
            return;

        var record = grid.getStore().getAt(rowIndex);

        if (/sw-grid-edit/.test(be.className))
            $SW.IPAM.Options['<%=this.ID %>'].Editoption(be, record);

        if (/sw-grid-delete/.test(be.className))
            $SW.IPAM.Options['<%=this.ID %>'].OptionRemove(be, record);
    };

</script>
<div style="display: none;">
    <orion:WhatIsNew ID="WhatIsNew" runat="server" />
</div>
<table>
    <tr style="background-color: #FFF7CD;">
        <td>
            <div>
                <div id="HelpTxtId" runat="server" class="OptionHelpDescription" style="margin: 5px 10px 5px 0px;
                    padding: 5px 0 5px 10px; height: 15px;">
                    <table>
                        <tr>
                            <td>
                                <img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" />
                            </td>
                            <td>
                                <div>
                                    <asp:Label ID="helpTextID" runat="server"> <%= Resources.IPAMWebContent.IPAMWEBDATA_GK_17%> </asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>

<div style="overflow: auto;width:950px;">
    
    <IPAMlayout:GridBox runat="server" ID="DHCPoptionsgrid" title="" UseLoadMask="True"
        autoHeight="true" width="950" emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_13 %>"
        borderVisible="true" deferEmptyText="false" SelectorName="selector" RowSelection="true"
        StripeRows="true" cssClass="extra-alt" >
        <TopMenu borderVisible="false">
            <IPAMui:ToolStripMenuItem id="btnAdd" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_GK_12 %>" runat="server">
            </IPAMui:ToolStripMenuItem>         
        </TopMenu>
        <Columns>
            <IPAMlayout:GridColumn title="Option" width="200" isSortable="true" runat="server" dataIndex="OptionDescription" />
            <IPAMlayout:GridColumn id="OptionValue" title="Value(s)" width="600" isSortable="true" runat="server" dataIndex="Values" renderer="$SW.IPAM.AllOptions_renderer_OptionValue()" />
            <IPAMlayout:GridColumn id="EditColumn" title="Edit" width="75" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.AllOptions_renderer_Editbuttons()" />
            <IPAMlayout:GridColumn id="DeleteColumn" title="Delete" width="75" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.AllOptions_renderer_Deletebuttons()" />
        </Columns>
        <Store ID="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../sessiondataprovider.ashx"
            proxyEntity="AllOptions" AmbientSort="OptionCode" listeners="{ loadexception: $SW.ext.HttpProxyException }"
            runat="server">
            <Fields>
                <IPAMlayout:GridStoreField dataIndex="OptionCode" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="OptionDescription" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Values" runat="server" />
            </Fields>
        </Store>
    </IPAMlayout:GridBox>

</div>

