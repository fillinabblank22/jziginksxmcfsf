using System;
using System.Collections.Generic;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Common.Helpers;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeOptions : CommonPageServices
    {
        #region Declarations

        private PageDhcpScopeEditor Editor { get; set; }

        #endregion // Declarations

        public string WebParamId
        {
            get
            {
                return GetParam(DhcpSessionStorage.PARAM_WEBID, null);
            }
        }
        #region Constructors

        public DhcpScopeOptions()
        {
            this.Editor = new PageDhcpScopeEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DhcpScopeWizardStep.Options;

            if (!IsPostBack)
            {
                InitControls();
            }
            var webId = GetParam(DhcpSessionStorage.PARAM_WEBID, null);
            DhcpAllOptions.WebId = webId;
            Title = string.Format(Title, Editor.Session_IsAddMode(null) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_68 : Resources.IPAMWebContent.IPAMWEBDATA_VB1_70);
        }

        
        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }
        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        #region Methods
        
        private void InitControls()
        {
            var dhcpServerType = Editor.Session_RetrieveServerType(null);
                   
            Editor.ActivateProgressStep(null, PageDhcpScopeEditor.IPAM_DHCPSCOPE_OPTIONS);

            DhcpAllOptions.ServerType = LocalizationHelper.GetEnumDisplayString(typeof (DhcpServerType), dhcpServerType).ToUpper();

            if (dhcpServerType == DhcpServerType.CISCO || dhcpServerType == DhcpServerType.Windows || dhcpServerType == DhcpServerType.ASA)
            {
                List<GenericDhcpOptions> options;
                Editor.Session_RetrieveScopeAllOptionsSessionData(null, out options);
                if (options == null)
                {
                    options = new List<GenericDhcpOptions>();
                }
                if (! options.Exists(op=> op.Code ==51))
                   {
                       options.Add(new GenericDhcpOptions(51, "691200"));
                       Editor.Session_UpdateScopeOptions(null, options);
                   }
            }
        }

        private void Next()
        {
            if (!ValidateUserInput())
                return;

            Editor.ClearScopeTempOptionsSessionData(null);

           string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
            Response.Redirect(url, true);
        }

        private bool ValidateUserInput()
        {
          return true;
        }

        #endregion // Methods
    }
}
