<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Dhcp.Scope.Options.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeOptions"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_511%>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>

<%@ Register Src="~/Orion/IPAM/DhcpScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/Dhcp.ScopeOptions.ascx" TagName="AllOptions" TagPrefix="orion" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionIPAMAGCreatingDHCPScopes" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

    <style type="text/css">
        .checkbox label
        {
            padding-left: 5px;
        }
    </style>
    
    <script type="text/javascript" language="javascript">
        if (!$SW.IPAMValid) $SW.IPAMValid = {};
        $SW.IPAMValid.WebId = '<%=this.WebParamId %>';
    </script>
    
     <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css" />

    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
   { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/scopes" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%>"
                                    class="bc_link">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto;
                                            height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
  { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div>
            <table width="1000px" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <orion:Progress ID="ProgressIndicator1" runat="server" />
                        <div class="GroupBox" id="NormalText">
                            <span class="ActionName">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_566 %>
                                <asp:Literal runat="server" ID="litNodeIP"></asp:Literal>
                            </span>
                            <br />
                            <div class="contentBlock">
                                <div id="divAllOptions">
                                    <table cellspacing="10">
                                        <tr>
                                            <td style="width: 1px">
                                            </td>
                                            <td>
                                                <orion:AllOptions ID="DhcpAllOptions" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <div class="sw-btn-bar-wizard">
                                        <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click"
                                            DisplayType="Secondary" LocalizedText="Back" />
                                        <orion:LocalizableButton runat="server" CausesValidation="true" ID="imgbNext" OnClick="imgbNext_Click"
                                            DisplayType="Primary" LocalizedText="Next" OnClientClick="return  $SW.IPAM.AllOptions_ValidateLeaseOption()" />
                                        <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                                            OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
