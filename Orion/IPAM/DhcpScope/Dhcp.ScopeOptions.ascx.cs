﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Layout;

public partial class Dhcp_ScopeOptions : System.Web.UI.UserControl
{
     public string WebId { get; set; }
     public string ServerType { get; set; }
    public bool ViewMode
    {
        get { return false; }
        set 
        {
            DHCPoptionsgrid.TopMenu.hidden = value;
            EditColumn.isHidden = value;
            DeleteColumn.isHidden = value;
            OptionValue.width = 600;
            HelpTxtId.Visible = !value;
        }
    }
     protected override void OnLoad(EventArgs e)
     {
        base.OnLoad(e);

        this.DHCPoptionsgrid.jsID = this.DHCPoptionsgrid.ClientID;
        this.DHCPoptionsgrid.listeners = string.Format(@"{{cellclick: $SW.IPAM.Options['{0}'].grid_cellclick,  render: function(){{ return $SW.IPAM.Options['{0}'].SetWebId(); }},columnresize: function(){{ return $SW.IPAM.Options['{0}'].ResizeColWidth(); }},statesave: function(){{ return $SW.IPAM.Options['{0}'].ResizeColWidth() }}}}", this.ID);
        this.btnAdd.handler = string.Format(@"function(el){{ return $SW.IPAM.Options['{0}'].AddOptionBtnPressed(el); }}", this.ID);
        
         WhatIsNew.WebId = WebId;
     }
}