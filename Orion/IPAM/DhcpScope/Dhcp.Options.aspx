﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Dhcp.Options.aspx.cs" 
Inherits="SolarWinds.IPAM.WebSite.DhcpScopeAddress" %>
 <%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
   
     <IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"
        ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit"
        runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
    </IPAMmaster:JsBlock>
    <script type="text/javascript" language="javascript">
        if (!$SW.IPAMValid) $SW.IPAMValid = {};

    </script>
    <IPAMui:ValidationIcons ID="ValidationIcons" runat="server" />
    <div id="formbox">
        <div style="padding-bottom: 20px">
            
        </div>
        <div>
            <table  class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">                     
                    <td  class="sw-form-col-label" style="width: 150px">
                        <div class="sw-field-label" style="width: 150px">
                                              
                        </div>
                    </td>
                    <td class="sw-form-col-control"> 
                        <div class="sw-form-item" >
                            <asp:Label runat="server" ID="lblconstruct"></asp:Label>
                        </div>
                    </td>
                    <td class="sw-form-col-comment">
                                            
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
</asp:Content>
