﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Master;

public partial class DhcpScope_Exclusions : UserControl
{
    #region Properties

    private string _scopeIpRangeSupplyFn;

    public string ScopeIPRangeSupplyFn
    {
        get
        {
            return string.IsNullOrEmpty(_scopeIpRangeSupplyFn) ? "function(){}" : _scopeIpRangeSupplyFn;
        }
        set
        {
            _scopeIpRangeSupplyFn = value;
        }
    }

    public string WebId { get; set; }

    #endregion
}