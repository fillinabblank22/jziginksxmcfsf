﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dhcp.ScopeIpAddressList.ascx.cs" Inherits="Dhcp_ScopeIpAddressList" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock ID="JsBlock1" requires="ext,ext-quicktips,sw-ipaddress-manage.js" Orientation="jsPostInit" runat="server" />

<script type="text/javascript">
    if (!$SW.IPAM.RouterIPs) $SW.IPAM.RouterIPs = [];
    $SW.IPAM.RouterIPs['<%=this.ID %>'] = $SW.IPAM.RouterList({
    GridId: '<%=this.ServersGrid.ClientID %>',
    WebId: '<%=this.WebId %>'
});


$SW.IPAM.RouterIPs['<%=this.ID %>'].grid_cellclick = function (grid, rowIndex, columnIndex, e) {
        $SW.ext.gridClickToDrilldown(grid, rowIndex, columnIndex, e);

        var be = e.browserEvent.target || e.browserEvent.srcElement;
        if (be.tagName.toUpperCase() == 'IMG')
            be = be.parentNode;

        if (be.tagName.toUpperCase() != 'A')
            return;

        var record = grid.getStore().getAt(rowIndex);


        if (/sw-grid-edit/.test(be.className))
            $SW.IPAM.RouterIPs['<%=this.ID %>'].EditServer(be, record);

    if (/sw-grid-up/.test(be.className))
        $SW.IPAM.RouterIPs['<%=this.ID %>'].ServerUp(be, record);

    if (/sw-grid-down/.test(be.className))
        $SW.IPAM.RouterIPs['<%=this.ID %>'].ServerDown(be, record);

    if (/sw-grid-delete/.test(be.className))
        $SW.IPAM.RouterIPs['<%=this.ID %>'].ServerRemove(be, record);
}
</script>

<IPAMlayout:DialogWindow jsID="masterServerWindow"  Name="masterServerWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="Master Servers" height="200" width="550" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="exclusionsSave" text="Save" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['masterServerWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="exlusionsCancel" text="Cancel" runat="server">
        <handler>function(){ $SW['masterServerWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['masterServerWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['masterServerWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="closeMsg" Name="close" runat="server">
        <handler>
        function(n,o,info){ $SW['masterServerWindow'].hide();            
            $SW.IPAM.RouterIPs['RoutersGrid'].ServerRefresh();
            }           
         </handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:GridBox runat="server" ID="ServersGrid" title="" UseLoadMask="True" autoHeight="true" width="525"
    emptyText="No Routers are added." borderVisible="true" deferEmptyText="false" 
    SelectorName="selector" RowSelection="true">
    
    <TopMenu borderVisible="false">
        <IPAMui:ToolStripMenuItem id="btnAdd" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="Add IP Address" runat="server">
        </IPAMui:ToolStripMenuItem>         
    </TopMenu>
    <Columns>
        <IPAMlayout:GridColumn title="Order" width="50" isSortable="true" runat="server" dataIndex="ID" />
        <IPAMlayout:GridColumn title="IP Address" width="300" isSortable="true" runat="server" dataIndex="serverAddress" />        
        <IPAMlayout:GridColumn title="Actions" width="150" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.RouterList_renderer_buttons()" />
        
    </Columns>
    <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../sessiondataprovider.ashx"
        proxyEntity="Routers" AmbientSort="ID"
        listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
        <Fields>
            <IPAMlayout:GridStoreField dataIndex="ID"    runat="server" />
            <IPAMlayout:GridStoreField dataIndex="serverAddress"  runat="server" />                      
       </Fields>
    </Store>
</IPAMlayout:GridBox>
