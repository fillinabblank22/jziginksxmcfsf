<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.Scope.Address.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeAddress"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_511%>" %>

<%@ Import Namespace="SolarWinds.IPAM.BusinessObjects" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/Dhcp.Scope.Exclusions.ascx" TagName="Exclusions" TagPrefix="orion" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<%@ Register Src="~/Orion/IPAM/DhcpScope/Dhcp.Scope.IPRange.ascx" TagName="IPRange"TagPrefix="orion" %>
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGCreatingDHCPScopes" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
        <script type="text/javascript" language="javascript">
            if (!$SW.IPAMValid) $SW.IPAMValid = {};
            $SW.IPAMValid.WebId = '<%=this.WebParamId %>';
            var webId = '<%=this.WebParamId %>';
            
    </script>
    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
    .disabled-textbox { 
        background-color:#fff;
        background-image: url(/Orion/IPAM/res/images/sw/textBox-bg.gif);
        border-color:#b5b8c8;
    }
    </IPAMmaster:CssBlock>
    <orion:Include File="breadcrumb.js" runat="server" />
    <IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-subnet-manage.js,sw-dialog.js,sw-dhcp-scopewizard.js,sw-helpserver,ext-ux,sw-ux-wait.js"
        Orientation="jsPostInit" runat="server">
$(document).ready( function(){ try {
     InitControl();
  $SW.IPAM.JustScopeRelated = $SW.ns$($nsId, 'cbJustScopeExclusions')[0];
  $SW.IPAM.InitMaskCalculation($nsId, 'txtCidr', 'txtSubnetMask', 'cbAddIPAddresses');
  $SW.IPAM.ScopeIPRangeSupplyFn = (function(){
    var cidr = '#'+$SW.ns$($nsId, 'txtCidr')[0].id;
    var mask = '#'+$SW.ns$($nsId, 'txtSubnetMask')[0].id;

    try {
      var addr = '#'+$SW.ns$($nsId, 'txtSubnetAddress')[0].id;
      return function(){
        var a = $SW.IP.v4($(addr).val()); if(!$SW.IP.v4_isok(a)) return '';
        var m = $SW.IP.v4($(mask).val()); if(!$SW.IP.v4_ismask(m)) return '';
        var c = $SW.IP.v4($(cidr).val()); if((Number(c)<1) || (Number(c)>31)) return '';


        if(!$SW.IP.v4_issubnet(a,m)) return '';

        var range = $SW.IP.v4_subnet(a, m);
/*
        // [oh] shift start end address (for cidr < 31)
        var s_n = $SW.IP.v4_toint(range.start), e_n = $SW.IP.v4_toint(range.end);
        if((e_n - s_n) >= 2){ s_n++; e_n--; }
        var s = $SW.IP.v4_fromint(s_n).join('.');
        var e = $SW.IP.v4_fromint(e_n).join('.');
*/
        var s = range.start.join('.'), e = range.end.join('.');
        return '&start='+s+'&end='+e+'&cidr='+$(cidr).val();
      };
    }catch(e) {
      var sIp = '#'+$SW.ns$($nsId, 'txtStartAddress')[0].id;
      var eIp = '#'+$SW.ns$($nsId, 'txtEndAddress')[0].id;    

      return function()
      { 
          var c = $SW.IP.v4($(cidr).val()); if((Number(c)<1) || (Number(c)>31)) return '';
          return '&start='+$(sIp).val()+'&end='+$(eIp).val()+'&cidr='+$(cidr).val();
      };
    }
  })();

  $SW.IPAM.FilterJustScopeRelated = function (item) {
    var addr = '#' + $SW.ns$($nsId, 'txtSubnetAddress')[0].id;
    var mask = '#' + $SW.ns$($nsId, 'txtSubnetMask')[0].id;

    var a = $SW.IP.v4($(addr).val()); if (!$SW.IP.v4_isok(a)) return '';
    var m = $SW.IP.v4($(mask).val()); if (!$SW.IP.v4_ismask(m)) return '';
    var range = $SW.IP.v4_subnet(a, m);

    var range_a_int = $SW.IP.v4_toint(range.start);
    var range_e_int = $SW.IP.v4_toint(range.end);
    var item_a_int =  $SW.IP.v4_toint($SW.IP.v4(item.data.AddressStart));
    var item_e_int = $SW.IP.v4_toint($SW.IP.v4(item.data.AddressEnd));

    
    var item_start_is_in_range = range_a_int <= item_a_int && range_e_int >= item_a_int;
    var item_end_is_in_range = range_a_int <= item_e_int && range_e_int >= item_e_int;
    return item_start_is_in_range || item_end_is_in_range;
  }

  $SW.IPAM.SwitchFilter = function () {
    if (!$SW.IPAM.JustScopeRelated) {
      return;
    }

    if ($SW.IPAM.JustScopeRelated.checked)
      $SW.IPAM.DHCPExclusions.FilterBy($SW.IPAM.FilterJustScopeRelated);
    else
      $SW.IPAM.DHCPExclusions.ClearFilter();
  }

  $SW.IPAM.DHCPExclusions.getGrid().getStore().on({
        'load': function() {
            $SW.IPAM.SwitchFilter();
            }
        });

  Ext.onReady(function(){
    
  });
  
}catch(err){}
});
    </IPAMmaster:JsBlock>
    
    <script type="text/javascript" language="javascript">
        if(!$SW.IPAMValid) $SW.IPAMValid = {};
        $SW.IPAMValid.ValidateIPRange = function (sender) {
            var startValidator = $("#<%=StartAddressExclusionRangeValidator.ClientID %>")[0];
            var endValidator = $("#<%=EndAddressExclusionRangeValidator.ClientID %>")[0];

            if (!startValidator || !endValidator)
                return;

            if (startValidator === sender && !endValidator.isvalid) {
                endValidator.isvalid = true;
                ValidatorValidate(endValidator);
            }
            if (endValidator === sender && !startValidator.isvalid) {
                startValidator.isvalid = true;
                ValidatorValidate(startValidator);
            }
        };
        
        function InitControl() {
            if (document.getElementById("<%=hdnMultiRangeManagement.ClientID%>").value == "True") {
                $("#iscActionName").show();
                $("#otherActionName").hide();
                
                $("#iscsubdefinition").show();
                $("#othersubdefinition").hide();

                $("#iscbulbglow").show();

                $("#poolrangeefinition").show();
                $("#dhcpserverdefinition").hide();
                $("#subnetsize").hide();
               
            } else {
               
                $("#iscActionName").hide();
                $("#otherActionName").show();

                $("#iscsubdefinition").hide();
                $("#othersubdefinition").show();

                $("#iscbulbglow").hide();

                $("#poolrangeefinition").hide();
                $("#dhcpserverdefinition").show();
                $("#subnetsize").show();
            }
        }

        function isSubnetExists() {
            var iCount = 0;
            $.ajax({
                type: "POST",
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        iCount = res.data;
                    }
                },
                failure: function () {
                    iCount = 0;
                },
                complete: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        iCount = res.data;
                    }
                },
                async: false,
                data:
                {
                    entity: 'IPRange',
                    verb: 'GetCount',
                    w: '<%=this.WebParamId %>'
                }
            });
            return iCount;
        }

        function GetAddedSubnetCount() {
            
            if ('<%=this.IsMultipleScopeMgt %>' == 'False')
                return true;

            var iCount = isSubnetExists();

            if (iCount <= 0 || iCount == undefined) {

                Ext.MessageBox.show({
                    title: "<%=Page.Title%>",
                    msg: "<%= Resources.IPAMWebContent.IPAMWEBDATA_SE_18%>",
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR,
                    width: 350,
                    fn: function () { }
                });
                return false;
            }
        }

    </script>
    
    <style>
 .pool-cell { 
    background-image: url("/Orion/IPAM/res/images/sw-resources/chart-pool.png") !important;
    background-repeat: no-repeat;
}
 .range-cell { 
    background-image: url("/orion/ipam/res/images/sw-resources/IPrange_icon16x16.png") !important;
    background-repeat: no-repeat;
}
.x4-column-header-checkbox {
    display: none;
}
.YellowBox {
    margin: 5px 10px 5px 0px;
    padding: 5px 0 5px 10px;
    background-color:#FFF7CD;
}
</style>

    <IPAMui:ValidationIcons runat="server" />

<div style="margin: 10px 0px 0px 10px;">
  <div><table width="100%" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr>
      <td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div style="margin-top: 10px;"><ul class="breadcrumb">
        <li class="bc_item">
            <a href="/api2/ipam/ui/scope" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li>
    </ul></div>
<% } %>
        <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
      </td>
    </tr>
  </table></div>
    </div>
    <input type="hidden" id="hdnMultiRangeManagement" runat="server" />
  <div><!-- ie6 --></div>
  <div id="othertest"><table width="1000px" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <orion:Progress ID="ProgressIndicator1" runat="server" />
        <div class="GroupBox" id="NormalText">
        <div id ="otherActionName"><span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_521 %></span></div>
        <div id ="iscActionName"> <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_11 %>  <%=this.SubnetAddress%></span></div> 
          <div id ="othersubdefinition" style="margin: 5px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_522 %></div>
         <div id ="iscsubdefinition" style="margin: 5px;"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_12 %></div>
            <div id="iscbulbglow"  runat="server" class="YellowBox">
            <table>
                <tr>
                    <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                   <td id ="IscTd">
                         <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_13 %>
                    </td>
                </tr>
            </table>
        </div>
          <div class="contentBlock">
          <div id="dhcpserverdefinition"><span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_523 %></span></div>
          <div id="poolrangeefinition"><span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_14 %></span></div>
            <div id="ScopeRange" visible="false" runat="server">
              <table cellspacing="5" class="sw-form-wrapper">
                <tr class="sw-form-cols-normal">
                  <td class="sw-form-col-label" style="width: 145px;"></td>
                  <td class="sw-form-col-control"></td>
                  <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                  <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_524%></div></td>
                  <td><div class="sw-form-item" style="width: 180px">
                    <asp:TextBox runat="server" ID="txtStartAddress" CssClass="x-form-text x-form-field" Width="150px" />
                  </div></td>
                  <td><asp:RequiredFieldValidator ID="StartAddressRequire" runat="server" ControlToValidate="txtStartAddress"
                        Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_525 %>"
                    /><asp:CustomValidator ID="StartAddressIPv4" runat="server" ControlToValidate="txtStartAddress"
                        Display="Dynamic" ClientValidationFunction="$SW.Valid.Fns.ipv4" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_526 %>"
                    /><asp:CustomValidator ID="StartAddressNotFirstIP" runat="server" ControlToValidate="txtStartAddress" 
                        Display="Dynamic" OnServerValidate="OnFirstAddressValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_527 %>"
                    /><asp:CustomValidator ID="StartAddressToEnd" runat="server" ControlToValidate="txtStartAddress"
                        Display="Dynamic" OnServerValidate="OnStartAddressValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_528 %>"
                    /><asp:CustomValidator ID="StartAddressExclusionRangeValidator" runat="server" ControlToValidate="txtStartAddress"
                        Display="Dynamic" ClientValidationFunction="$SW.IPAMValid.ValidateIPRange" OnServerValidate="OnExclusionsRangeValidate" 
                    /><asp:CustomValidator ID="StartAddressToScope" runat="server" ControlToValidate="txtStartAddress"
                        Display="Dynamic" OnServerValidate="OnStartAddressToScopeValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_529 %>"
                    /></td>
                </tr>
                <tr>
                  <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_530 %></div></td>
                  <td><div class="sw-form-item" style="width: 180px">
                    <asp:TextBox runat="server" ID="txtEndAddress" CssClass="x-form-text x-form-field" Width="150px" />
                  </div></td>
                  <td><asp:RequiredFieldValidator ID="EndAddressRequire" runat="server" ControlToValidate="txtEndAddress"
                        Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_531 %>"
                    /><asp:CustomValidator ID="EndAddressIPv4" runat="server" ControlToValidate="txtEndAddress"
                        Display="Dynamic" ClientValidationFunction="$SW.Valid.Fns.ipv4" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_532 %>"
                    /><asp:CustomValidator ID="EndAddressNotLastIP" runat="server" ControlToValidate="txtEndAddress" 
                        Display="Dynamic" OnServerValidate="OnLastAddressValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_533 %>"
                    /><asp:CustomValidator ID="EndAddressExclusionRangeValidator" runat="server" ControlToValidate="txtEndAddress"
                        Display="Dynamic" ClientValidationFunction="$SW.IPAMValid.ValidateIPRange" OnServerValidate="OnExclusionsRangeValidate"
                    /><asp:CustomValidator ID="EndAddressToScope" runat="server" ControlToValidate="txtEndAddress"
                        Display="Dynamic" OnServerValidate="OnEndAddressToScopeValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_534  %>"
                    /></td>
                </tr>
              </table>
            </div>
            <div id="ScopeAddress" visible="true" runat="server">
              <table cellspacing="5" class="sw-form-wrapper">
                <tr class="sw-form-cols-normal">
                  <td class="sw-form-col-label" style="width: 145px;"></td>
                  <td class="sw-form-col-control"></td>
                  <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                  <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_43%></div></td>
                  <td><div class="sw-form-item" style="width: 180px">
                    <asp:TextBox runat="server" ID="txtSubnetAddress" CssClass="x-form-text x-form-field" Width="150px" />
                  </div></td>
                  <td><asp:RequiredFieldValidator runat="server" ControlToValidate="txtSubnetAddress"
                        Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_45 %>"
                    /><asp:CustomValidator runat="server" ControlToValidate="txtSubnetAddress"
                        Display="Dynamic" OnServerValidate="OnSubnetAddressValidate" ClientValidationFunction="$SW.Valid.Fns.ipv4" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_46 %>"
                    /><asp:CustomValidator runat="server" ControlToValidate="txtSubnetAddress" OtherControl="txtSubnetMask"
                        Display="Dynamic" ClientValidationFunction="$SW.Valid.Fns.ipv4subnet" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_47 %>"
                    /><asp:CustomValidator runat="server" ControlToValidate="txtSubnetAddress"
                        Display="Dynamic" OnServerValidate="OnExclusionsRangeValidate" 
                    /></td>
                </tr>
              </table>
            </div>
            <div id="PoolandIpAddressRange" visible="false" runat="server">
                <orion:IPRange ID="IPRangeGrid" runat="server" />
            </div>
          </div>
          <div><!-- ie6 --></div>
          <div>
              <table cellspacing="5" class="sw-form-wrapper">
                <tr class="sw-form-cols-normal">
                  <td style="width: 145px;"></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                  <div id="divJustScopeExclusions" visible="false" runat="server">
                  <span onclick="$SW.IPAM.SwitchFilter();"> <asp:CheckBox runat="server" id="cbJustScopeExclusions" type="checkbox" runat="server" Checked="true" Text="<%$ Resources: IPAMWebContent,IPAMWEBDATA_MK0_1  %>" /></span>
                  </div>
                  <div id="divExclusions" runat="server">
                    <orion:Exclusions ID="Exclusions" ScopeIPRangeSupplyFn="$SW.IPAM.ScopeIPRangeSupplyFn" runat="server"/>
                  </div></td>
                </tr>
              </table>
          </div>
          <div><!-- ie6 --></div>
          <div id ="subnetsize" class="contentBlock" style="margin-bottom: 50px;">
              <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_535 %></span>
              <table cellspacing="5" class="sw-form-wrapper">
                <tr class="sw-form-cols-normal">
                  <td class="sw-form-col-label" style="width: 145px;"></td>
                  <td class="sw-form-col-control"></td>
                  <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                  <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_44 %></div></td>
                  <td><div class="sw-form-item" style="width: 60px">
                    <asp:TextBox runat="server" ID="txtCidr" MaxLength="2" Width="30px" CssClass="x-form-text x-form-field" Text="24" />
                  </div></td>
                  <td><asp:RequiredFieldValidator ID="CidrRequire" runat="server" ControlToValidate="txtCidr"
                        Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_49 %>"
                    /><asp:CustomValidator ID="CidrIsv4" runat="server" ControlToValidate="txtCidr" ClientValidationFunction="$SW.Valid.Fns.ipv4cidr"
                        OtherControl="txtSubnetMask" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_TM0_51 %>"
                    /><asp:CompareValidator ID="CidrAbove" runat="server" ControlToValidate="txtCidr" Operator="GreaterThan"
                        ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_51 %>"
                    /><asp:CompareValidator ID="CidrBelow" runat="server" ControlToValidate="txtCidr" Operator="LessThan"
                        ValueToCompare="31" Type="Integer" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_536 %>"
                    /><asp:CustomValidator ID="CidrRangeValidator" runat="server" ControlToValidate="txtCidr"
                        Display="Dynamic" OnServerValidate="OnCidrRangeValidate" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_537 %>"
                    /></td>
                </tr>
                <tr>
                  <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_538 %></div></td>
                  <td><div class="sw-form-item x-item-disabled" style="width: 180px">
                    <input type="text" runat="server" id="txtSubnetMask" readonly="readonly" 
                           class="x-form-text x-form-field sw-withlblnum disabled-textbox" value="255.255.255.0" style="width: 150px" />
                  </div></td>
                  <td></td>
                </tr>
                <tr>
                  <td class="leftLabelColumn"></td>
                  <td><div class="sw-form-item">
                    <asp:CheckBox ID="cbAddIPAddresses" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_539 %>" checked="true" runat="server"/>
                    <div class="sw-form-clue" id="subnettoolarge" style="display: none;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_540 %></div>
                  </div></td>
                  <td></td>
                </tr>
              </table>
          </div>
          <div><!-- ie6 --></div>
          <div class="sw-btn-bar-wizard">
            <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click"
                DisplayType="Secondary" LocalizedText="Back" />
            <orion:LocalizableButton runat="server" ID="imgbNext" CausesValidation="true" OnClientClick="return GetAddedSubnetCount()" OnClick="imgbNext_Click"
                DisplayType="Primary" LocalizedText="Next" />
            <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="Cancel" />
          </div>
        </div>
      </td>
    </tr>
  </table></div>
  <asp:ValidationSummary ID="valSummary" ShowSummary="false" ShowMessageBox="true" runat="server" DisplayMode="BulletList" />
</asp:Content>
