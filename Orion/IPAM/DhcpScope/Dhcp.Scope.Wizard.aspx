<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dhcp.Scope.Wizard.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeWizard"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_511%>" %>

<%@ Import Namespace="SolarWinds.IPAM.BusinessObjects" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register Src="~/Orion/IPAM/DhcpScope/ProgressIndicator.ascx" TagName="Progress" TagPrefix="orion" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMAGCreatingDHCPScopes" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
.disabled-textbox { 
    background-color:#fff;      
    background-image: url(/Orion/IPAM/res/images/sw/textBox-bg.gif);
    border-color:#b5b8c8;        
}
.scopeAvailability {
        margin: 5px 10px 5px 0px;
        padding: 5px 0 5px 10px;
        background-color:#FFF7CD;
}
.sw-form-cols-normal td.sw-form-col-label {
    width: 250px;
}
.leftLabelColumn {
    word-wrap: break-word;
}
    </IPAMmaster:CssBlock>
    <IPAMmaster:JsBlock Requires="ext,sw-dhcp-scopewizard.js" Orientation="jsPostInit" runat="server">
    
$(document).ready(function(){

    $SW.IPAM.SubNetCalculation($nsId);
    var scope = $SW.ns$($nsId, 'scopeNameRequire');
    var serverId = $SW.nsGet($nsId, 'ddlDhcpServers');
    var serverCb = $SW.TransformDropDown(serverId, serverId, { hiddenId: serverId } );
    var hdnServerTypeId = $SW.nsGet($nsId, 'hdnServerType');
    
    var sharedNWId = $SW.nsGet($nsId, 'ddlSharedNetwork');
    var sharedNWCb = $SW.TransformDropDown(sharedNWId, sharedNWId, { hiddenId: sharedNWId,editable:false,emptyText:"@{R=IPAM.Strings;K=IPAMWEBJS_SE_41;E=js}" + ' ' } );

    getServerType();

    function getServerType()
    {
        var iComboValue = serverCb.getValue();
        var serverType = iComboValue.split('-');
        
        var sType = getServerEnumValue();

        if (serverType[1].toLowerCase() == sType) {
            $SW.IPAM.GetSharedNetworkSelectHandler(sharedNWCb,serverCb.getValue().split('-')[2].toLowerCase(),getDefaultSelectValue());
        }

        showhidecaption(serverType[1].toLowerCase(),sType);
        ValidatorCheck()
    }
    serverCb.on('select',function(combo, records, eOpts) 
    {
        getServerType();
    });
    
    sharedNWCb.on('select',function(combo, records, eOpts) 
    {
        $SW.IPAM.AssignValueToHdn(records.data.IDS,records.data.NetworkName);
    });
    
    $SW.IPAM.GetAvailableCount = function(id){
        var index = sharedNWCb.store.find('SharedNetID',id );
        var selectedObj = sharedNWCb.store.getAt(index);
        if(selectedObj){
            var server = serverCb.getRawValue();
            var data = [];
            data.push(selectedObj.data.NetworkName);
            data.push(selectedObj.data.AvailabeCount);
            data.push(server);

            return data;
        }
        else
            return null;
    }
});
    
    </IPAMmaster:JsBlock>
    <script type="text/javascript">

        if (!$SW.IPAM) $SW.IPAM = {};

        function getServerEnumValue() {
            return "<%= Convert.ToInt32(DhcpServerType.ISC) %>";
        };

        function getDefaultSelectValue() {
            return "<%= defaultToSelect %>";
        };

        $SW.IPAM.AssignValueToHdn = function (sharedNetIds, name) {
            document.getElementById("<%=selectedSNetwork.ClientID%>").value = sharedNetIds;
            document.getElementById("<%=selectedSNetworkName.ClientID%>").value = name;
        };

        function showhidecaption(serverType, sType) {
            document.getElementById("<%=hdnServerType.ClientID%>").value = serverType;
            if (serverType == sType) {
                $("#otherscope").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_1 %>");
                $("#vlandescription").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_4 %>");
                $("#definescope").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_7 %>");
                $("#description").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_6 %>");

                $("#netmask").show();
                $("#addip").show();
                $("#iscsubnet").show();
                $("#OtherTd").hide();
                $("#IscTd").show();
                $("#SharedNW").show();
                $("#scopename").hide();

            } else {
                $("#otherscope").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_275 %>");
                $("#vlandescription").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_245 %>");
                $("#definescope").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_275 %>");
                $("#description").html("<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_512 %>");

                $("#netmask").hide(0);
                $("#addip").hide(0);
                $("#iscsubnet").hide(0);
                $("#IscTd").hide();
                $("#OtherTd").show();
                $("#SharedNW").hide(0);
                $("#scopename").show();

            }
        };

        function ValidatorCheck() {
            var sType = "<%= Convert.ToInt32(DhcpServerType.ISC) %>";
            var isAddMode = "<%=IsAddMode.ToString().ToLower()%>";

            var scopeNameValidator = document.getElementById("<%=scopeNameRequire.ClientID%>");

            var networkAddressRequire = document.getElementById("<%=NetworkAddressRequire.ClientID%>");
            var networkAddressIPv4 = document.getElementById("<%=NetworkAddressIPv4.ClientID%>");
            var networkSubnet = document.getElementById("<%=NetworkAddressSubnet.ClientID%>");
            var validatorCidrRequire = document.getElementById("<%=CidrRequire.ClientID%>");
            var validatorCidrAbove = document.getElementById("<%=CidrAbove.ClientID%>");
            var validatorCidrBelow = document.getElementById("<%=CidrBelow.ClientID%>");
            var validatorCidrIsv4 = document.getElementById("<%=CidrIsv4.ClientID%>");
            var validatorAsyncAddress = document.getElementById("<%=AsyncAddressValidator.ClientID%>");
            var validatorNetMaskRequire = document.getElementById("<%=NetMaskRequire.ClientID%>");
            var validatorNetMaskRequireCustom = document.getElementById("<%=NetMaskRequireCustomValidator.ClientID%>");
            if (document.getElementById("<%=hdnServerType.ClientID%>").value == sType) {

                ValidatorEnable(scopeNameValidator, false);
                ValidatorEnable(validatorCidrAbove, true);
                ValidatorEnable(validatorCidrBelow, true);
                ValidatorEnable(validatorCidrIsv4, true);
                ValidatorEnable(validatorNetMaskRequireCustom, true);
                ValidatorEnable(networkSubnet, true);
                ValidatorEnable(validatorAsyncAddress, true);
                ValidatorEnable(networkAddressIPv4, true);
                if ((isAddMode == "false") || (sType == 4)) {
                    ValidatorEnable(validatorAsyncAddress, false);
                }

                ValidatorEnable(networkAddressRequire, true);
                ValidatorEnable(validatorCidrRequire, true);
                ValidatorEnable(validatorNetMaskRequire, true);

            } else {
                ValidatorEnable(scopeNameValidator, true);

                ValidatorEnable(networkAddressRequire, false);
                ValidatorEnable(networkAddressIPv4, false);
                ValidatorEnable(networkSubnet, false);
                ValidatorEnable(validatorCidrRequire, false);
                ValidatorEnable(validatorCidrAbove, false);
                ValidatorEnable(validatorCidrBelow, false);
                ValidatorEnable(validatorCidrIsv4, false);
                ValidatorEnable(validatorAsyncAddress, false);
                ValidatorEnable(validatorNetMaskRequire, false);
                ValidatorEnable(validatorNetMaskRequireCustom, false);
            }
        }

        function AllowToDeleteSharedNetwork() {
            var sType = "<%= Convert.ToInt32(DhcpServerType.ISC) %>";
            var isAddMode = "<%=IsAddMode.ToString().ToLower()%>";

            if (document.getElementById("<%=hdnServerType.ClientID%>").value == sType && isAddMode == "false") {
                var orgSharedNetId = "<%= this.orgSharedNetName %>";
                var currentSharedNetId = document.getElementById("<%=selectedSNetwork.ClientID%>").value.split(',')[0];

                if (orgSharedNetId != currentSharedNetId && orgSharedNetId != "-2" && orgSharedNetId != "-1") {

                    if ("<%= this.defaultToSelect %>" == currentSharedNetId)
                        return true;

                    var selectedObj = $SW.IPAM.GetAvailableCount(orgSharedNetId);

                    if (selectedObj[1] - 1 <= 0 && currentSharedNetId != '') {

                        var r = confirm(String.format("<%= Resources.IPAMWebContent.IPAMWEBDATA_SE_22%>", "\"" + selectedObj[0] + "\"", "\"" + selectedObj[2] + "\""));

                        if (r == true)
                            return true;
                        else
                            return false;
                    }
                }
            }
        }

    </script>
    <orion:Include File="breadcrumb.js" runat="server" />
    <IPAMui:ValidationIcons runat="server" />
    <input type="hidden" id="hdnServerType" runat="server" />
    <input type="hidden" id="hdnMode" runat="server" />
    <input type="hidden" id="selectedSNetwork" runat="server" />
    <input type="hidden" id="selectedSNetworkName" runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="100%" id="sw-navhdr" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                            { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/scopes" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                                { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <!-- ie6 -->
        </div>

        <div style="width: 1000px;">
            <asp:hiddenfield id="hdWebId" runat="server" />

            <orion:Progress ID="ProgressIndicator1" runat="server" />

            <div class="GroupBox" id="NormalText">

                <span class="ActionName" id="definescope"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_512 %></span>
                <br />
                <span id="description"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_513 %></span>
                <br />
                <div id="Div1" runat="server" class="scopeAvailability">
                    <table>
                        <tr>
                            <td>
                                <img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                            <td id="OtherTd">A <b>scope</b> is a consecutive range of IP addresses that a DHCP server is allowed to lease to a DHCP client. Defining one or more scopes on your DHCP servers allows the server to manage the distribution and assignment of IP address to DHCP clients.
                                 
                            </td>
                            <td id="IscTd">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_5 %>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="contentBlock">
                    <table cellspacing="5" cellpadding="0" class="sw-form-wrapper">
                        <tr class="sw-form-cols-normal">
                            <td class="sw-form-col-label" />
                            <td class="sw-form-col-control" />
                            <td class="sw-form-col-comment" />
                        </tr>
                        <tr>
                            <td class="leftLabelColumn">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_243 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:dropdownlist id="ddlDhcpServers" runat="server" autopostback="False"></asp:dropdownlist>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="SharedNW">
                            <td class="leftLabelColumn">
                                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_20 %></div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:dropdownlist id="ddlSharedNetwork" runat="server" autopostback="true" />
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="iscsubnet">
                            <td class="leftLabelColumn">
                                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_43 %></div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox id="txtNetworkAddress" cssclass="x-form-text x-form-field sw-withlblnum" runat="server" />
                                    <span class="sw-pairedlbl"><%= Resources.IPAMWebContent.IPAMWEBDATA_TM0_44 %></span>
                                    <asp:textbox id="txtCidr" cssclass="x-form-text x-form-field sw-pairednum" text="24" runat="server" />
                                </div>
                            </td>
                            <td>
                                <asp:requiredfieldvalidator id="NetworkAddressRequire" runat="server"
                                    controltovalidate="txtNetworkAddress"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_45 %>" />
                                <asp:customvalidator id="NetworkAddressIPv4" runat="server"
                                    controltovalidate="txtNetworkAddress"
                                    clientvalidationfunction="$SW.Valid.Fns.ipv4"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_46 %>" />
                                <asp:customvalidator
                                    id="NetworkAddressSubnet"
                                    runat="server"
                                    controltovalidate="txtNetworkAddress"
                                    othercontrol="txtNetmask"
                                    clientvalidationfunction="$SW.Valid.Fns.ipv4subnet"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_47 %>" />
                                <asp:requiredfieldvalidator id="CidrRequire" runat="server"
                                    controltovalidate="txtCidr"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_49 %>" />
                                <asp:comparevalidator id="CidrAbove" runat="server"
                                    controltovalidate="txtCidr"
                                    operator="GreaterThan"
                                    valuetocompare="0"
                                    type="Integer"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_51 %>" />
                                <asp:comparevalidator id="CidrBelow" runat="server"
                                    controltovalidate="txtCidr"
                                    operator="LessThanEqual"
                                    valuetocompare="31"
                                    type="Integer"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_536 %>" />
                                <asp:customvalidator id="CidrIsv4" runat="server"
                                    controltovalidate="txtCidr"
                                    clientvalidationfunction="$SW.Valid.Fns.ipv4cidr"
                                    othercontrol="txtNetmask"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_51 %>" />
                                <asp:customvalidator id="AsyncAddressValidator" runat="server" display="Dynamic"
                                    clientvalidationfunction="$SW.Valid.Fns.ipv4Async"
                                    addresscontrol="txtNetworkAddress" cidrcontrol="txtCidr"
                                    errormessage="{msg}" />
                            </td>
                        </tr>
                        <tr id="netmask">
                            <td class="leftLabelColumn">
                                <div id="labelnetmask" class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_2 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox runat="server" id="txtNetmask" cssclass="x-form-text x-form-field" />
                                </div>
                            </td>
                            <td>
                                <asp:requiredfieldvalidator id="NetMaskRequire" runat="server"
                                    controltovalidate="txtNetmask"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_DF1_8 %>" />
                                <asp:customvalidator id="NetMaskRequireCustomValidator" runat="server"
                                    controltovalidate="txtNetmask"
                                    clientvalidationfunction="$SW.Valid.Fns.ipv4"
                                    display="Dynamic"
                                    errormessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_46 %>" />
                            </td>
                        </tr>
                        <tr id="addip">
                            <td class="leftLabelColumn"></td>
                            <td>
                                <div class="sw-form-item">
                                    <div>
                                        <asp:checkbox id="cbAddIPAddresses" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_539 %>" checked="true" runat="server" />
                                        <div class="sw-form-clue" id="subnettoolarge" style="display: none;"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_540 %></div>
                                    </div>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr id="scopename">
                            <td class="leftLabelColumn">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_275 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox runat="server" id="txtScopeName" cssclass="x-form-text x-form-field" />
                                </div>
                            </td>
                            <td>
                                <asp:requiredfieldvalidator id="scopeNameRequire" runat="server"
                                    controltovalidate="txtScopeName" display="Dynamic"
                                    errormessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_514 %>" />
                                <asp:customvalidator id="ciscoScopeNameNoSpace" runat="server"
                                    controltovalidate="txtScopeName" onservervalidate="ValidateScopeName" display="Dynamic"
                                    errormessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_ED1_514 %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftLabelColumn">
                                <div class="sw-field-label" id="scopedescription">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_515 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox runat="server" id="txtScopeDesc" cssclass="x-form-text x-form-field" />
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="leftLabelColumn">
                                <div class="sw-field-label" id="vlandescription">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_245 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox id="txtScopeVLAN" cssclass="x-form-text x-form-field" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="leftLabelColumn">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_159 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:textbox id="txtScopeLocation" cssclass="x-form-text x-form-field" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>

                    <div>
                        <!-- ie6 -->
                    </div>
                    <br />
                    <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %></span>
                    <br />
                    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
                        <div class="group-box white-bg" style="margin-bottom: 0px">
                            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
                        </div>
                    </div>

                    <div>
                        <!-- ie6 -->
                    </div>
                    <table cellspacing="5" class="sw-form-wrapper">
                        <tr class="sw-form-cols-normal">
                            <td class="sw-form-col-label" />
                            <td class="sw-form-col-control" />
                            <td class="sw-form-col-comment" style="width: 30px" />
                        </tr>
                        <tr>
                            <td class="leftLabelColumn">&nbsp;
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton runat="server" ID="imgbNext" OnClientClick="return AllowToDeleteSharedNetwork();ValidatorCheck();" OnClick="imgbNext_Click" DisplayType="Primary"
                            LocalizedText="Next" />
                        <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                            OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="Cancel" />
                    </div>
                </div>
            </div>
        </div>

        <asp:validationsummary id="valSummary" runat="server" showsummary="false" showmessagebox="true"
            displaymode="BulletList" />

    </div>
</asp:Content>
