﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dns.Server.Add.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DnsServerAdd"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_175 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NodeSelector.ascx" TagName="NodeSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DNSCredentialsSelector.ascx" TagName="DNSCredentialsSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHDnsServerAdd" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    <IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
#breadcrumb td div { font-size: 100%; }
#breadcrumb a { line-height: 25px; }
#breadcrumb { margin-top: 0px; }
p { margin-top: 0; margin-bottom: 10px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
.warningbox-inside .inner a { text-decoration: underline; }
.warningbox-inside { border: 1px solid #d1d0c9; max-width: 870px; background: #f7f596 url(/orion/ipam/res/images/sw/bg.warning.float.gif) repeat-x scroll left bottom; }
.warningbox-inside .inner { background:transparent url(/orion/ipam/res/images/sw/icon.notification.32.gif) no-repeat scroll left top; }
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
div.sw-form-heading { font-size: small; margin-top: 8px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: medium; margin: 0; }
    </IPAMmaster:CssBlock>
    <asp:PlaceHolder runat="server" ID="SelecedIds" />
    <orion:Include File="breadcrumb.js" runat="server" />
    <IPAMmaster:JsBlock Requires="ext-quicktips,sw-admin-cred.js,sw-helpserver" Orientation="jsInit" runat="server">
if (!$SW.IPAM) $SW.IPAM = {};

$SW.NodeSelectedDetails = null;
$SW.IPAM.NodeSelectionChange = function(me,nodes){
  nodes = nodes || [];
  if( nodes.constructor != [].constructor ) nodes = [nodes];

  var row = nodes[0] ? nodes[0].attributes : null;
  $SW.NodeSelectedDetails = row;
  row = row || {};
  var txt = $.trim(row.DNS)||$.trim(row.SysName)||$.trim(row.IPAddress)||'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_19;E=js}';
  $SW.ns$($nsId,'SelectedNodeName').text( txt );
  $SW.ns$($nsId,'SelectedNodeId')[0].value = ''+row.NodeId;

  if( $SW.Env.ExistingDnsServers[''+row.NodeId] )
    $('#warningNodeAdded').show(250);
  else
    $('#warningNodeAdded').hide(250);
  };

$SW.IPAM.NodeSelectionOk = function(source,args){
 args.IsValid =  !($SW.ns$($nsId,'SelectedNodeId')[0].value == '' || $SW.Env.ExistingDnsServers[$SW.ns$($nsId,'SelectedNodeId')[0].value]);
};

$SW.IPAM.NodeSelectionCredentials = function(source,args){
 args.IsValid = !($SW.ns$($nsId,'SelectedNodeId')[0].value == '');
};

$(document).ready(function(){
  var syncscanarea = function(me){
    if( !me ) return;
    if( me.checked ) $('#DnsScanning').show(250);
    else $('#DnsScanning').hide(250);
  };

  syncscanarea( $SW.ns$($nsId, 'cbEnableScanning')[0] );
  $SW.ns$( $nsId, 'cbEnableScanning' ).click(function(e){
    syncscanarea(e.target || e.srcElement);
  });

});

$SW.IPAM.DMultiCreds.testState(false);

$SW.IPAM.DnsServerSave = function(me)
{
    if ( $SW.IPAM.PageValidate(me) == false)
         return false;

    if ( $SW.IPAM.DMultiCreds.testState() )
        return true;      
    else
    {
       me.name = me.id.replace(/_/g,"$");
       Ext.Msg.show({
               title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_10;E=js} ',
               msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_11;E=js}',
               buttons: Ext.Msg.YESNO,
               width: 400,
               icon: Ext.MessageBox.QUESTION,
               fn: function(btn){ 
                        if( btn == 'yes' ) {
                           WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(me.name, "", true, "", "", false, true));
                           return true;                  
                        } 
                    }
               });
               
      return false;
      }
}

$SW.IPAM.IncrementalZoneTransfer = function(show){
    var el = $('.incremental-zone-tansfer');
    if(show) el.show(250); else el.hide(250);
}

$SW.IPAM.PageValidate = function(me){
 var cred=!$SW.IPAM.DMultiCreds.validate(undefined, 'IsNewCredentialRequire', '');
 var scan=$SW.ns$($nsId,'cbEnableScanning')[0].checked;
 var vg;
 
 if( cred && scan ) vg = /^(SelNodeCredential|NewCred|SelNode|)$/;
 else if( cred ) vg = /^(SelNodeCredential|NewCred|SelNode|)$/;
 else if( scan ) vg = /^(SelNode|)$/;
 else vg = /^(SelNode|)$/;

 WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(me.name, "", true, vg, "", false, false));
 return !!window.Page_IsValid;
};
    </IPAMmaster:JsBlock>
    <IPAMui:ValidationIcons runat="server" />

<div style="margin-left: 10px; margin-right: 10px;">
  <div  style="margin: 10px 0px 0px 0px;">   
  <table class="titleTable" id="sw-navhdr" cellpadding="0" cellspacing="0" style="width: auto">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
      <ul class="breadcrumb">
        <li class="bc_item" style="white-space: nowrap;">
            <a href="/api2/ipam/ui/dns" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
    </ul>
<% } %>
        <h1 style="padding-left: 0px"><%=Page.Title%></h1>
     </td></tr>
  </table>
  </div> 
   
    <div class="warningbox warningbox-inside">
        <div class="inner">
            <asp:LinkButton ID="addNodeLink" OnClick="OnAddOrionNodeClick" runat="server" Visible="False" />
            <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_176, AddNodeLinkBeginTag(), "</a>", "<a href=\"/Orion/Discovery/Config/ \">", "</a>")%><br />
            <div style="padding-top: 4px;"><b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_177 %></b></div>
        </div>
    </div>

        <div class="sw-form-heading">
            <h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_178 %></h2>
        </div>
        <ipam:NodeSelector ID="PickHost" MultipleSelection="false" OnSelectChange="$SW.IPAM.NodeSelectionChange"
            runat="server" />
        <asp:HiddenField runat="server" ID="SelectedNodeId" Value="" />
        <asp:CustomValidator ID="RequireSelectedNode" runat="server" Display="None" ClientValidationFunction="$SW.IPAM.NodeSelectionOk"
            ValidationGroup="SelNode" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_151 %>"></asp:CustomValidator>
        <div style="max-width: 888px;">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="vertical-align: bottom;">
                        <div class="sw-form-heading" style="white-space: nowrap">
                            <h2><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_152, "")%><span id="SelectedNodeName" runat="server"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_153 %></span></h2>
                        </div>
                    </td>
                    <td style="width: 400px; padding: 4px 0px 4px 0px;">
                        <div id="warningNodeAdded" class="x-tip x-form-invalid-tip" style="display: none;
                            visibility: visible; position: static; width: 400px;">
                            <div class="x-tip-tl" style="zoom: 1;">
                                <div class="x-tip-tr" style="zoom: 1;">
                                    <div class="x-tip-tc" style="zoom: 1;">
                                        <div class="x-tip-header x-unselectable" style="zoom: 1; -moz-user-select: none;">
                                            <span class="x-tip-header-text" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="x-tip-bwrap">
                                <div class="x-tip-ml">
                                    <div class="x-tip-mr">
                                        <div class="x-tip-mc">
                                            <div class="x-tip-body" style="height: auto; width: auto;">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_180 %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="x-tip-bl x-panel-nofooter">
                                    <div class="x-tip-br">
                                        <div class="x-tip-bc" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="warningbox sw-credential-warning" style="max-width: 874px; display: none;">
            <div class="inner">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_181 %>
            </div>
        </div>
        <div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">
            <ipam:DNSCredentialsSelector ValidationGroup="SelNodeCredential" id="CredSelector" runat="server"
                FnSupplyServerAddress="function(fnContinue){ fnContinue($SW.ns$($nsId,'SelectedNodeId')[0].value); }" />
            <asp:CustomValidator ID="RequireSelectedNode2" runat="server" Display="None" ClientValidationFunction="$SW.IPAM.NodeSelectionCredentials"
                ValidationGroup="SelNodeCredential" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_165 %>"></asp:CustomValidator>
        </div>
        <div class="sw-form-heading">
            <h2>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_182 %></h2>
        </div>
        <div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">
            <div>
                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-normal">
                        <td class="sw-form-col-label" style="width: 400px;">
                        </td>
                        <td style="width: 308px;">
                        </td>
                        <td class="sw-form-col-comment">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="sw-form-item sw-check-align">
                                <asp:CheckBox ID="cbEnableScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_183 %>" runat="server" Checked="true" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div id="DnsScanning">
                                <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td >
                                            <div class="sw-field-label">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_184 %> &nbsp;&nbsp;
                                            </div>
                                        </td>
                                        <td  style="width: 150px;">
                                            <div class="sw-form-item">
                                                <ipam:TimeSpanEditor ID="ServerScanInterval" ValidIf="cbEnableScanning" TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_205 %>" Value="04:00:00"
                                                    MinValue="00:10:00" MaxValue="7.00:00:00" runat="server" />
                                            </div>
                                        </td>
                                        <td style="width: 308px;"></td> 
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="sw-form-item sw-check-align incremental-zone-tansfer">
                                                <asp:CheckBox ID="cbIncrementalZoneTransfer" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_185 %>"
                                                    runat="server" Checked ="true"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="false"
            ShowMessageBox="true" DisplayMode="BulletList" />
        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnSave" OnClientClick="return $SW.IPAM.DnsServerSave(this);" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_186 %>" DisplayType="Primary" />
            <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="/api2/ipam/ui/dns" LocalizedText="Cancel" CausesValidation="false" DisplayType="Secondary"/>
        </div>
    </div>
</asp:Content>
