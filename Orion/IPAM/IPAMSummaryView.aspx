<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/IPAM/IPAMView.master" CodeFile="IPAMSummaryView.aspx.cs" Inherits="Orion_IPAM_Admin_IPAMSummaryView" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagPrefix="ipam" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ipam" TagName="NavBar" Src="~/Orion/IPAM/Controls/NavigationTabBar.ascx" %>    
<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">

        <% if (Profile.AllowCustomize)
           { %>
        <a href='/Orion/Admin/CustomizeView.aspx?ViewID=<%=((OrionView)Page).ViewInfo.ViewID %>&ReturnTo=<%= ((OrionView)Page).ReturnUrl %>'
            style="padding: 2px 0px 2px 20px; background: transparent url(/Orion/IPAM/res/images/sw/Page.Icon.CustomPg.gif) scroll no-repeat left center;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_638 %></a>
        <%}%>
        <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
          { %>
        <a href='/Orion/IPAM/Admin/Admin.Overview.aspx' style="padding: 2px 0px 2px 20px;
            background: transparent url(/Orion/IPAM/res/images/sw/Page.Icon.MdlSettings.gif) scroll no-repeat left center;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6%></a>
        <%}%>

        <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMSummary" />

        <div style="font-size: 8pt; margin: 6px 0px 6px 0px; text-align: right">
        <%= DateTime.Now.ToString("F") %>
        </div>
        </asp:Content>
<asp:Content ID="SummaryTitle" ContentPlaceHolderID="IPAMPageTitle" Runat="Server">

<IPAMmaster:CssBlock Requires="sw-base.css,ext-all.css,sw-ext-tabs.css,ext-theme-gray.css,~/Orion/styles/MainLayout.css" runat="server">
div.dateArea a img { vertical-align: bottom; margin-right: 4px; }
div.dateArea a { margin-left: 8px; }
div.dateArea a.sw-helptxt { margin-left: 4px; }
#ipamResourceWrapper { padding 0px 0px; }
.ResourceWrapper th tr { padding: auto !important; }
</IPAMmaster:CssBlock>	
	<div id="sw-navhdr" >
	    <h1><%= this.ViewTitle %></h1>
	</div>	
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="IPAMMainContentPlaceHolder" runat="server">        
<IPAMmaster:JsBlock ID="JsBlock1" runat="server">
$(document).ready(function(){ 
  // paresource laoyout fix
  var panel = $('#ipamResourceWrapper');
  var w = $('table.ResourceContainer').outerWidth(true) + 12;
  if(w<500) w=500;
  panel.css('width',''+w+'px');
  
  // resource cell-padding fix
  $('.ResourceWrapper > table').each(function(i){
    var pg = this.cellPadding;
    if(!pg || pg=="" || pg=="0") return true;
    if(pg.indexOf('px') < 0) pg=pg+'px';
    $('td', this).css('padding', pg);
  });
});
  

</IPAMmaster:JsBlock>
  <div style="display: table; width: 100%; margin-top: -7px;">
      <div id="ipamResourceWrapper">
        <orion:ResourceHostControl ID="ResourceHostControl1" runat="server">
            <orion:ResourceContainer runat="server" ID="resContainer" />
        </orion:ResourceHostControl>
      </div>
  </div>
  <IPAM:LicenseStatus runat="server" />
</asp:Content>