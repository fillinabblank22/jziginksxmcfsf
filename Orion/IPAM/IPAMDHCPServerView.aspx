﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/View.master" CodeFile="IPAMDHCPServerView.aspx.cs" Inherits="Orion_IPAM_IPAMDHCPServerView" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="Refresher" Src="~/Orion/Controls/Refresher.ascx" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>

<asp:Content runat="server" ContentPlaceHolderID="ViewPageTitle">

<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
  { %>
<style type="text/css">
a.IpamSettingsLink {
    margin-left: 5px; padding: 2px 0 2px 20px;
    background: url("/Orion/IPAM/res/images/sw/Page.Icon.MdlSettings.gif") no-repeat left center;
}
</style>
<script type='text/javascript' src='/Orion/IPAM/res/js/injectDateAreaLinks.js'>
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () { $('div.dateArea').IpamSettingsLinkInjector({ pos: 2, before: false }); });
</script>
<%}%>

<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <br />
    <div style="margin-left: 10px;" >
        <orion:DropDownMapPath ID="DhcpServerSiteMapPath" runat="server" OnInit="OnInitDhcpServerSiteMapPath" >
            <RootNodeTemplate>
                <a href="<%# Eval("url") %>" ><u> <%# Eval("title") %></u> </a>
            </RootNodeTemplate>
            <CurrentNodeTemplate>
	            <a href="<%# Eval("url") %>" ><%# Eval("title") %> </a>
	        </CurrentNodeTemplate>
        </orion:DropDownMapPath>
	</div>
<% } %>

	<h1><%=Page.Title %><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_637, "")%><asp:HyperLink ID="DhcpServerLinkURL" runat="server"><asp:Label ID="DhcpServerName" runat="server" /></asp:HyperLink></h1>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="MainContentPlaceHolder">
    <IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

	<IPAM:IPAMDhcpServerResourceHost ID="DhcpServerResHost" runat="server">
		<orion:ResourceContainer ID="resContainer" runat="server"/>
	</IPAM:IPAMDhcpServerResourceHost>
    <IPAM:LicenseStatus runat="server" />
</asp:Content>


