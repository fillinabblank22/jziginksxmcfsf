using System;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;

namespace SolarWinds.IPAM.WebSite.Admin
{
	public partial class ScanSettings : CommonPageServices
	{
        protected void Page_Load(object sender, EventArgs e)
		{
            if (IsPostBack)
            {
            }
            else
            {
                ReInitFields();
            }
		}

        private void ReInitFields()
        {
            List<BusinessObjects.Setting> Settings = null;

            // get the account object.
            try
            {
                using (IpamClientProxy proxy = SwisConnector.GetProxy())
                {
                    Settings = proxy.AppProxy.Setting.Get(null, BusinessObjects.SettingCategory.Global, "ScanSetting.%");

                    if (Settings != null)
                    {
                        foreach (BusinessObjects.Setting setting in Settings)
                        {
                            string n = setting.Name.Substring(setting.Name.IndexOf('.') + 1);

                            switch (n)
                            {
                                case "TransientPeriod":
                                    {
                                        TimeSpan? ts = null;
                                        if (setting.TryGetValue(out ts) && ts != null)
                                        {
                                            bool unlimited= (ts == TimeSpan.MaxValue);
                                            cbTransientPeriod.Checked = unlimited;
                                            txtTransientDuration.Text = (unlimited == false) ?
                                                ts.Value.TotalDays.ToString() : string.Empty;
                                        }
                                        break;
                                    }
                                case "SnmpEnableScanning":
                                    {
                                        bool? chk = null;
                                        if (setting.TryGetValue(out chk))
                                            cbSnmpScanning.Checked = chk.Value;
                                        break;
                                    }
                                case "SnmpEnableNeighborScanning":
                                    {
                                        bool? chk = null;
                                        if (setting.TryGetValue(out chk))
                                            cbNeighborScanning.Checked = chk.Value;
                                        break;
                                    }

                                case "SnmpTimeout":
                                    if (setting.Value != null)
                                        txtSnmpTimeout.Text = setting.Value;
                                    break;

                                case "SnmpRetries":
                                    if (setting.Value != null)
                                        txtSnmpRetries.Text = setting.Value;
                                    break;


                                case "IcmpEnableScanning":
                                    {
                                        bool? chk = null;
                                        if (setting.TryGetValue(out chk))
                                            cbIcmpScanning.Checked = chk.Value;
                                        break;
                                    }


                                case "IcmpPingRetries":
                                    if (setting.Value != null)
                                        txtPingsPerAddress.Text = setting.Value;
                                    break;

                                case "IcmpPingDelay":
                                    if (setting.Value != null)
                                        txtDelayBetweenPings.Text = setting.Value;
                                    break;

                                case "IcmpPingTimeout":
                                    if (setting.Value != null)
                                        txtPingTimeout.Text = setting.Value;
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                LogFile.Error("unable to communicate with IPAM-SWIS", ex);
                ExceptionContext.MoveToExceptionPage(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (SaveObject())
            {
                Response.Redirect("admin.overview.aspx");
            }
        }

        private int? GetInt(string value, int min, int max)
        {
            if (string.IsNullOrEmpty(value) )
                return null;

            int i;
            if (int.TryParse(value, out i))
            {
                if (i < min)
                    return min;
                if (i >= max)
                    return max;
                return i;
            }

            return null;
        }

        private double? GetDouble(string value, double min, double max)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            double d;
            if (double.TryParse(value, out d))
            {
                if (d < min)
                    return min;
                if (d >= max)
                    return max;
                return d;
            }

            return null;
        }

        private bool SaveObject()
        {
            TimeSpan? TransientPeriod = GetTransientPeriod();
            bool SnmpEnabled = cbSnmpScanning.Checked;
            bool NeighborEnabled = cbNeighborScanning.Checked;
            int? SnmpTimeout = GetInt(txtSnmpTimeout.Text, 10, 6500);
            int? SnmpRetries = GetInt(txtSnmpRetries.Text, 0, 5);
            bool IcmpEnabled = cbIcmpScanning.Checked;
            int? IcmpPingTimeout = GetInt(txtPingTimeout.Text, 1000, 6500);
            int? IcmpPingRetries = GetInt(txtPingsPerAddress.Text, 0, 5);
            int? IcmpPingDelay = GetInt(txtDelayBetweenPings.Text, 10, 500);

            // get the account object.
            try
            {
                using (IpamClientProxy proxy = SwisConnector.GetProxy())
                {
                    List<BusinessObjects.Setting> settings = new List<BusinessObjects.Setting>();

                    settings.Add(new BusinessObjects.Setting(
                            BusinessObjects.SettingCategory.Global,
                            ScanSetting.IcmpEnableScanning,
                            IcmpEnabled));

                    settings.Add(new BusinessObjects.Setting(
                            BusinessObjects.SettingCategory.Global,
                            ScanSetting.SnmpEnableScanning,
                            SnmpEnabled));

                    settings.Add(new BusinessObjects.Setting(
                            BusinessObjects.SettingCategory.Global,
                            ScanSetting.SnmpEnableNeighborScanning,
                            NeighborEnabled));


                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.TransientPeriod,
                        TransientPeriod));

                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.SnmpTimeout,
                        SnmpTimeout));

                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.SnmpRetries,
                        SnmpRetries));

                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.IcmpPingTimeout,
                        IcmpPingTimeout));

                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.IcmpPingDelay,
                        IcmpPingDelay));

                    settings.Add(new BusinessObjects.Setting(
                        BusinessObjects.SettingCategory.Global,
                        ScanSetting.IcmpPingRetries,
                        IcmpPingRetries));

                    proxy.AppProxy.Setting.UpdateMany(settings.ToArray());
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                ExceptionContext.MoveToExceptionPage(ex);
            }

            return false;
        }

        private TimeSpan? GetTransientPeriod()
        {
            // check for unlimited value
            if (this.cbTransientPeriod.Checked)
            {
                return TimeSpan.MaxValue;
            }

            // get duration value (in days)
            double? duration = GetDouble(txtTransientDuration.Text, 0.25, 340);
            if (duration == null)
            {
                return null;
            }

            return new TimeSpan(0, 0, (int)Math.Round(86400.0 * duration.Value));
        }
	}

    public enum ScanSetting
    {
        TransientPeriod,
        SnmpEnableScanning,
        SnmpTimeout,
        SnmpRetries,
        IcmpEnableScanning,
        IcmpPingRetries,
        IcmpPingDelay,
        IcmpPingTimeout,
        SnmpEnableNeighborScanning,
    }
}
