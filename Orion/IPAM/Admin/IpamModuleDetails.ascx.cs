﻿using System;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using System.Data;
using SolarWinds.IPAM.Client.Caching;

public partial class Orion_IPAM_Admin_IpamModuleDetails : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                GetModuleAndLicenseInfo("IPAM");
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Error while displaying details for Orion Core module. Details: {0}", ex.ToString());
            }
        }
    }

    private void GetModuleAndLicenseInfo(string moduleName)
    {
        foreach (var module in ModuleDetailsHelper.LoadModuleInfoForEngines("Primary", false))
        {
            if (!module.ProductDisplayName.StartsWith(moduleName, StringComparison.OrdinalIgnoreCase))
                continue;

            var values = new Dictionary<string, string>();

            values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_839, module.ProductName);
            values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_840, module.Version);
            values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_841, String.IsNullOrEmpty(module.HotfixVersion) ? Resources.IPAMWebContent.IPAMWEBDATA_VB1_58 : module.HotfixVersion);
            if (!String.IsNullOrEmpty(module.LicenseInfo))
                values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_842, module.LicenseInfo);

            AddIpamSpecificInfo(values);

            this.IpamDetails.Name = module.ProductDisplayName;
            this.IpamDetails.DataSource = values;
            break; // we support only one "Primary" engine
        }
    }

    /// <param name="values"></param>
    private void AddIpamSpecificInfo(Dictionary<string, string> values)
    {
        try
        {
            IpamClientProxy proxy = SwisConnector.OpenCacheConnection();
            proxy.AppProxy.InternalStatus.EnsureCache(false);
            InternalStatusCache cache = InternalStatusCache.GetInstance();

            // Allowed number of used IP Addresses
            string AllowedLicenses = cache.GetCachedEntry(InternalStatusKey.LicensingCountMax.ToString());
            ulong licenses = ulong.MaxValue;
            if (false == ulong.TryParse(AllowedLicenses, out licenses))
            {
                AllowedLicenses = string.Empty;
            }
            if (licenses >= 1000000) { AllowedLicenses = Resources.IPAMWebContent.IPAMWEBDATA_VB1_843; }

            values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_844, AllowedLicenses);

            // Current number of used IP Addresses
            string CurrentLicenses = cache.GetCachedEntry(InternalStatusKey.LicensingCountCurrent.ToString());
            values.Add(Resources.IPAMWebContent.IPAMWEBDATA_VB1_845, CurrentLicenses);
        }
        catch (Exception ex)
        {
            _logger.Error("Cannot get number of licensed elements.", ex);
        }
    }
}
