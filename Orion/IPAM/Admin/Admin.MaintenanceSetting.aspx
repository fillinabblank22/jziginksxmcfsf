﻿<%@ Page Language="C#" AutoEventWireup="true" validateRequest="false"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_101 %>" 
    MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master"
    Inherits="SolarWinds.IPAM.WebSite.Admin.MaintenanceSetting"
    CodeFile="Admin.MaintenanceSetting.aspx.cs" %>

<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHMainSettings" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminStyleSheetPlaceholder" runat="server">
    <link rel="stylesheet" type="text/css" href="/Orion/styles/Breadcrumb.css" />
  <style type="text/css">
    #value_table .alternateRow { background-color: #E4F1F8; }
    #value_table td { white-space:nowrap; padding: 5px; vertical-align: middle; }
    #value_table input { font-size: 9pt; padding: 2px; }
    #value_table .validator { vertical-align: bottom; }
    #value_table td.Property img { width: 15px; height: 15px; }
    #value_table .header td { padding-left: 0px; }
    .period-time { width: 60px !important; }
    #formbox ul { padding: 10px; list-style: inherit !important; }
    .sw-helpimg { vertical-align: top; }
    .sw-helpimg img { margin-top: -1px; margin-right: 5px; }
  </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminTitlePlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
<orion:Include File="breadcrumb.js" runat="server" />

<div style="margin: 10px 0px 0px 10px;">
<table class="titleTable" cellpadding="0" cellspacing="0" style="width: auto !important;">
<tbody><tr><td>
    <% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <ul class="breadcrumb" style="margin: 0px">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_135 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.MaintenanceSetting.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_101 %></a></li>
        </ul></li>
    </ul>

    <% } %>
    <h1><%=Page.Title%></h1>
  </td></tr></tbody>
  </table>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">

  <div id="formbox">

    <asp:ValidationSummary runat="server" DisplayMode="BulletList" />

    <table id="value_table" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr class="header">
            <td colspan="3">
                <h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_116 %></h2>
            </td>
        </tr>
        <tr class="alternateRow">
            <td class="PropertyHeader">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_117 %>
            </td>
            <td class="Property">
                <asp:TextBox ID="txtEventsRetention" Columns="5" runat="server"/>
                <asp:RequiredFieldValidator ID="EventsRetentionRequired" runat="server" Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_136 %>"
                  ControlToValidate="txtEventsRetention" CssClass="validator">
                    <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
                </asp:RequiredFieldValidator>
                <asp:RangeValidator ID="EventsRetentionRange" runat="server" Display="Dynamic"
                  Type="Integer" MinimumValue="1" MaximumValue="5000" CultureInvariantValues="true"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_137 %>"
                  ControlToValidate="txtEventsRetention" CssClass="validator">
                    <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
                </asp:RangeValidator>
                &nbsp;<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_118 %>
            </td>
            <td class="Property">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_119 %>
            </td>
        </tr>
        <tr>
            <td class="PropertyHeader">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_120 %>
            </td>
            <td class="Property">
                <asp:TextBox ID="txtIPHistoryRetention" Columns="5" runat="server"/>
                <asp:RequiredFieldValidator ID="IPHistoryRetentionRequired" runat="server" Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_138 %>"
                  ControlToValidate="txtIPHistoryRetention" CssClass="validator">
                    <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
                </asp:RequiredFieldValidator>
                <asp:RangeValidator ID="IPHistoryRetentionRange" runat="server" Display="Dynamic"
                  Type="Integer" MinimumValue="1" MaximumValue="5000" CultureInvariantValues="true"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_139 %>"
                  ControlToValidate="txtIPHistoryRetention" CssClass="validator">
                    <img src="/Orion/IPAM/res/images/default/form/exclamation.gif" alt="*" />
                </asp:RangeValidator>
                &nbsp;<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_118 %>
            </td>
            <td class="Property">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_121 %>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <br /><br />
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" LocalizedText="Save" DisplayType="Primary" OnClick="ClickSave" />
                    <orion:LocalizableButton runat="server" CausesValidation="false" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="ClickCancel" />
                </div> 
            </td>
        </tr>
    </table>
    
    <div><!-- ie6 --></div>

  </div>

</asp:Content>
