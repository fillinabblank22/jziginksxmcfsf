﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Admin;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.IPAM.BusinessObjects;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminCredentialsISCEdit : CommonPageServices
    {
        private const string KEY_PASSWORD = ".pw";

        private PageCredentialsEditor<DhcpServerType> editor;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.editor = new PageCredentialsEditor<DhcpServerType>(this, this.ViewState, DhcpServerType.ISC);
            editor.Initialize();

            bool isResyncRequired = this.editor.ResyncPassword(
                this.txtPassword.Text, KEY_PASSWORD);
            if (!this.IsPostBack || isResyncRequired)
            {
                BindControls();
            }
            this.editor.ShowPasswordHint(this.txtPassword, KEY_PASSWORD);
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.FixName.Visible = (CredentialNameExists.IsValid == false);
            base.OnPreRender(e);
        }

        #region Event Handlers

        protected void MsgSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void OnIsValueRequire(object source, ServerValidateEventArgs args)
        {
            // 'false', whether the validated value is null or empty string
            args.IsValid = !string.IsNullOrEmpty(args.Value) && (args.Value.Trim() != string.Empty);
        }

        protected void OnIsPasswordRequire(object source, ServerValidateEventArgs args)
        {
            string password = this.editor.GetChangedPassword(KEY_PASSWORD);

            // check, whether the password didn't changed
            if ((password == null) && (this.editor.CredID != null))
            {
                args.IsValid = true;
                return;
            }

            // 'false', whether the validated value is empty string
            args.IsValid = !string.IsNullOrEmpty(password) && (password.Trim() != string.Empty);
        }

        protected void OnCredentialNameExists(object source, ServerValidateEventArgs args)
        {
            // skip identical name
            if ((editor.Credential != null) &&
                (editor.Credential.Name == args.Value))
            {
                args.IsValid = true;
                return;
            }
            args.IsValid = !editor.IsNameExist(args.Value);
        }

        protected void OnBtnFixNameClick(object sender, EventArgs e)
        {
            this.txtCredentialName.Text = editor.GetUniqueName(this.txtCredentialName.Text);
            this.CredentialNameExists.IsValid = true;
            this.FixName.Visible = false;
        }

        #endregion // Event Handlers

        #region Private Methods

        private void BindControls()
        {
            ISCCredential credential = this.editor.Credential as ISCCredential;
            if (credential == null)
            {
                this.txtCredentialName.Text = string.Empty;
                this.txtUserName.Text = string.Empty;
                this.txtPath.Text = string.Empty;

                // show empty password
                this.editor.CleanupPassword(KEY_PASSWORD);

                this.CLITelnetProtocol.Checked = true;
                this.CLISSHProtocol.Checked = false;
                this.txtCLIPort.Text = "23";
                return;
            }

            this.txtCredentialName.Text = credential.Name;
            this.txtUserName.Text = credential.UserName;
           

            // show asterix passwords
            this.editor.SetPassword(null, KEY_PASSWORD);

            // set protocol and port
            if (credential.Protocol == CLIProtocolType.Telnet)
            {
                this.CLITelnetProtocol.Checked = true;
            }
            if (credential.Protocol == CLIProtocolType.SSH)
            {
                this.CLISSHProtocol.Checked = true;
            }
            this.txtCLIPort.Text = credential.Port.ToString();
        }

        private void Save()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            StoreCredential();

            if (this.HideChrome)
            {
                string page = "~/Orion/IPAM/DialogWindowDone.aspx";
                this.Server.Transfer(page);
            }
            else
            {
                this.Response.Redirect("~/Orion/IPAM/Admin/Admin.Credentials.List.aspx");
            }
        }

        private void StoreCredential()
        {
            Nullable<int> id = this.editor.CredID;
            ISCCredential credential = this.editor.Credential as ISCCredential;
            if (credential == null)
            {
                credential = new ISCCredential();
            }

            // set credential's properties from form
            credential.IsRescanRequired = false;
            credential.Name = this.txtCredentialName.Text;
            credential.UserName = this.txtUserName.Text;

            // set password if changed
            string password = this.editor.GetChangedPassword(KEY_PASSWORD);
            if (!string.IsNullOrEmpty(password))
            {
                credential.Password = password;
            }

            // get protocol type (telnet or SSH)
            if ((this.CLITelnetProtocol != null) &&
                (this.CLITelnetProtocol.Checked == true))
            {
                credential.Protocol = CLIProtocolType.Telnet;
            }
            if ((this.CLISSHProtocol != null) &&
                (this.CLISSHProtocol.Checked == true))
            {
                credential.Protocol = CLIProtocolType.SSH;
            }

            // get port number
            if (this.txtCLIPort != null)
            {
                int cliPort;
                Int32.TryParse(this.txtCLIPort.Text, out cliPort);
                credential.Port = cliPort;
            }

            // use correct method to store credential
            if ((id != null) && (id.HasValue))
            {
                credential.ID = id.Value;
                this.editor.Update(credential);
            }
            else
            {
                credential.ID = 0;
                this.editor.Create(credential);
            }
        }

        #endregion // Private Methods
    }
}