﻿<%@ Page Language="C#" AutoEventWireup="true" validateRequest="false"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_1 %>"   
    MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master"
    Inherits="SolarWinds.IPAM.WebSite.Admin.SystemSettings"
    CodeFile="Admin.SystemSetting.aspx.cs" %>

<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHEnableDuplicate" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminStyleSheetPlaceholder" runat="server">
  <IPAMmaster:CssBlock Requires="sw-admin.css,~/Orion/styles/Breadcrumb.css" runat="server"/>
  <orion:Include File="breadcrumb.js" runat="server" />
  <IPAMmaster:JsBlock Requires="ext,ext-quicktips" Orientation='jsPostInit' runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="adminTitlePlaceholder" runat="server">
    
<style type="text/css">
    .fill { white-space: normal !important; }
</style>

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<div style="margin: 10px 0px 0px 10px;">
   <table class="titleTable" cellpadding="0" cellspacing="0" style="width: auto !important;">
   <tbody><tr><td>
   <% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
   
    <ul class="breadcrumb" style="margin: 0px">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_7 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.SystemSetting.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_1 %></a></li>
        </ul></li>
    </ul>

    <% } %>
    <h1><%=Page.Title%></h1>
   </td></tr></tbody>
   </table>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAMmaster:JsBlock Requires="ext" Orientation="jsInit" runat="server">
if(!$SW.IPAM) $SW.IPAM = {};
var txt = '#'+$SW.nsGet($nsId, 'tbScanInterval');
var ddl = '#'+$SW.nsGet($nsId, 'ddlScanInterval');
$(document).ready(function(){
    $(ddl).bind("change", function(){
        $SW.Valid.Retest( $(txt)[0] ); 
    });
});

$SW.IPAM.ValidateTimeSpan = function(src,args){
    var freq = parseFloat( $.trim( $(txt).val().replace(',','.') ) )
    var mult = parseFloat( $.trim( $(ddl).val() ) );
  
    if( isNaN(freq) ) return args.IsValid = false;
    if( (freq*mult) < '600' ) return args.IsValid = false;
    if( (freq*mult) > '604800' ) return args.IsValid = false;
    return args.IsValid = true;
};
</IPAMmaster:JsBlock>

  <div id="formbox">
    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0" style="width: 100%; table-layout: auto">
        <tr><td colspan="3"><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_8 %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_9 %></th>
            <td class="value">
                <div class="sw-form-item">
                    <asp:CheckBox ID="cbDuplicatedSubnets" runat="server"/>
                </div>
            </td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_10 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_NEW_UI_KEY %></th>
            <td class="value">
                <div class="sw-form-item">
                    <asp:CheckBox ID="checkBoxNewUI" runat="server"/>
                </div>
            </td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_NEW_UI_DESCRIPTION %></td>
        </tr>
        <tr><td colspan="3"><br /><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_11 %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_12 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbCritLvL" Columns="3" Text="" runat="server"/> %
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_13 %>
                <img src="../res/images/sw/icon.subnet.critical.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_110 %>" />&nbsp;<img src="../res/images/sw/icon.supernet.critical.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_111 %>" /></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_14 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbWarnLvL" Columns="3" Text="" runat="server"/> %
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_15 %>
                <img src="../res/images/sw/icon.subnet.warning.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_110 %>" />&nbsp;<img src="../res/images/sw/icon.supernet.warning.gif" alt="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_111 %>" /></td>
        </tr>
        <tr><td colspan="3"><br /><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_16 %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_17 %></th>
            <td class="value"><asp:CheckBox ID="cbScanEnable" runat="server"/></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_18 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_19 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbScanInterval" Columns="1" runat="server"/>
                <asp:DropDownList ID="ddlScanInterval" runat="server">
                    <asp:ListItem Value="60" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_20 %>" />
                    <asp:ListItem Value="3600" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_21 %>" />
                    <asp:ListItem Value="86400" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_22 %>" />
                </asp:DropDownList>
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_23 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_24 %></th>
            <td class="value"><asp:CheckBox ID="cbAddIPs" runat="server"/></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_25 %></i></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_26 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbCIDR" Columns="3" runat="server"/>
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_27 %></td>
        </tr>
        <tr><td colspan="3"><br /><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_28 %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_29 %></th>
            <td class="value"><asp:CheckBox ID="cbTreeSort" runat="server"/></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_30 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_31 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbTreeMax" Columns="3" runat="server"/>
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_32 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_33 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbPageSizeGroups" Columns="3" runat="server"/>
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_34 %></td>
        </tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_35 %></th>
            <td class="value"><div class="sw-form-item">
                <asp:TextBox ID="tbPageSizeIPs" Columns="3" runat="server"/>
            </div></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_36 %></td>
        </tr>
        <tr><td colspan="3"><br /><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_37 %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_38 %></th>
            <td class="value"><asp:CheckBox ID="cbShowParentChangeConfirmDlg" runat="server" /></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_39 %></td>
        </tr>
        <tr><td colspan="3"><br /><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_LINUX_SETTINGS %></h2></td></tr>
        <tr>
            <th><%= Resources.IPAMWebContent.IPAMWEBDATA_NO_PRESERVE_TIMESTAMPS %></th>
            <td class="value"><asp:CheckBox ID="cbNoPreserveTimestamps" runat="server" /></td>
            <td class="fill"><%= Resources.IPAMWebContent.IPAMWEBDATA_NO_PRESERVE_TIMESTAMPS_FILL %></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <br />
                <div class="sw-btn-bar">
                  <orion:LocalizableButton ID="LocalizableButton1" runat="server" OnClick="ClickSave" CausesValidation="true" DisplayType="Primary" LocalizedText="Save" />
                  <orion:LocalizableButton ID="LocalizableButton2" runat="server" OnClick="ClickCancel" CausesValidation="false" DisplayType="Secondary" LocalizedText="Cancel" />
                </div>
            </td>
        </tr>       
    </table>
    
    <div><!-- ie6 --></div>

  </div>

  <asp:RequiredFieldValidator ControlToValidate="tbCritLvL" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_40 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbCritLvL" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_41 %>"
                        Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbCritLvL" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_42 %>"
                        Operator="LessThanEqual" ValueToCompare="100" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbCritLvL" ControlToCompare="tbWarnLvL"
                        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_43 %>"
                        Operator="GreaterThanEqual" Type="Integer" runat="server" Display="Dynamic"/>

  <asp:RequiredFieldValidator ControlToValidate="tbWarnLvL" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_44 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbWarnLvL" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_45 %>"
                        Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbWarnLvL" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_46 %>"
                        Operator="LessThanEqual" ValueToCompare="100" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbWarnLvL" ControlToCompare="tbCritLvL"
                        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_47 %>"
                        Operator="LessThanEqual" Type="Integer" runat="server" Display="Dynamic"/>

  <asp:RequiredFieldValidator ControlToValidate="tbScanInterval" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_48 %>" runat="server" Display="Dynamic" />
  <asp:CustomValidator  ControlToValidate="tbScanInterval" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_49 %>"
                        runat="server" Display="Dynamic" ValidateEmptyText="true" EnableViewState="false"
                        ClientValidationFunction="$SW.IPAM.ValidateTimeSpan" />

  <asp:RequiredFieldValidator ControlToValidate="tbCIDR" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_50 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbCIDR" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_51 %>"
                        Operator="GreaterThan" ValueToCompare="0" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbCIDR" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_52 %>"
                        Operator="LessThanEqual" ValueToCompare="32" Type="Integer" runat="server" Display="Dynamic"/>

  <asp:RequiredFieldValidator ControlToValidate="tbTreeMax" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_53 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbTreeMax" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_54 %>"
                        Operator="GreaterThanEqual" ValueToCompare="150" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbTreeMax" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_55 %>"
                        Operator="LessThanEqual" ValueToCompare="65536" Type="Integer" runat="server" Display="Dynamic"/>

  <asp:RequiredFieldValidator ControlToValidate="tbPageSizeGroups" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_56 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbPageSizeGroups" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_57 %>"
                        Operator="GreaterThanEqual" ValueToCompare="100" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbPageSizeGroups" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_58 %>"
                        Operator="LessThanEqual" ValueToCompare="65536" Type="Integer" runat="server" Display="Dynamic"/>
  
  <asp:RequiredFieldValidator ControlToValidate="tbPageSizeIPs" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_59 %>" runat="server" Display="Dynamic" />
  <asp:CompareValidator ControlToValidate="tbPageSizeIPs" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_60 %>"
                        Operator="GreaterThanEqual" ValueToCompare="100" Type="Integer" runat="server" Display="Dynamic"/>
  <asp:CompareValidator ControlToValidate="tbPageSizeIPs" ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_61 %>"
                        Operator="LessThanEqual" ValueToCompare="65536" Type="Integer" runat="server" Display="Dynamic"/>

  <IPAMui:ValidationIcons runat="server" />
  <asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />

</asp:Content>
