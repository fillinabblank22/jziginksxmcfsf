<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" Inherits="SolarWinds.IPAM.WebSite.Admin.AdminOverview" CodeFile="Admin.Overview.aspx.cs" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_7 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewIPAMSettings" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="adminContentPlaceholder">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    
    <%--Commenting out IPAT  Changes--%>
    <%--  <script type="text/javascript">
         var navigateButtons = [];
         navigateButtons.push($('#addDhcpServer'));
         navigateButtons.push($('#addDnsServer'));
         $.each(navigateButtons, function (idx, btn) {
             $(btn).on("click", function (event) {
                 if ('<%= this.isFreeTool %>' == "True" && '<%= this.isOtherModuleInstalled %>' == "False") {
                     alert('<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_45 %>');
                     return false;
                 }
             });
         })
     </script>--%>
    <%--Commenting out IPAT  Changes--%>
<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Orientation="Inline" runat="server">
Ext.onReady(function(){
  var noaccess = function(){
    $SW.Alert(
      '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_6;E=js}',
      '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_7;E=js}',
      { buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING }
    );
    return false;
  };

  $("a.secured").each(function(i){
    this.className = 'noaccess';
    this.onclick = noaccess;
  });
});
</IPAMmaster:JsBlock>
<%}%>

<IPAMmaster:CssBlock Requires="~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css" runat="server">
#adminContent { padding: 5px 10px 10px 20px; }
#adminContent p { border:medium none; font-size:12px; padding: 3px 0 0; width:auto; }
#adminContent td.bucketIcon { width: 36px; vertical-align: middle !important; }
#adminContent .LinkColumn { white-space: nowrap; }
#adminContent .highlightBucket { background-color: #FFFAC6; }
#adminContent .warningHighlight { background-color: #F9E379; }
#adminContent a.noaccess { color: #888 !important; text-decoration: none !important; }
#adminContent .LinkArrow { vertical-align: top; }
 .BucketHeader
        {
            font-weight: bold;
            font-size: 14px;
        }
        .BucketLinkContainer
        {
            padding-left: 45px;
        }
</IPAMmaster:CssBlock>

<orion:Include File="breadcrumb.js" runat="server" />

<div id="adminContent">

<div style="margin: 10px 0px 0px 0px;"> 
  <table width="100%" id="sw-navhdr" cellpadding="0" cellspacing="0" style="width: auto !important;">
    <tr>
      <td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <ul class="breadcrumb"  >
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
    </ul>
<% } %>
	    <h1 style="margin-top: 10px;"><%=Page.Title%></h1>
      </td>
    </tr>
  </table>
</div>

  <div style="font-size: 11px; margin-bottom: 4px;">
      <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_65, "<i>" + System.Text.RegularExpressions.Regex.Replace(SolarWinds.IPAM.Web.Common.Utility.RegistrySettings.GetVersionDisplayString(), "^(?:SolarWinds\\s+)?(?:Orion\\s+)?IP Address Manager ", "") + "</i>") %>
  </div>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;" id="moduleSettings">
    <tbody>
	  <tr>
	    <td class="column1of2">
	    
	      <!-- Begin Setup and Manage Applications Bucket -->
          <div class="ContentBucket highlightBucket" id="OrphanedScopesBox" runat="server">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.warning.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_66 %></div>
                <p><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_67, "<span class=\"warningHighlight\">" + String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_113, mgr.OrphanedScopeCount) + "</span>") %></p>
              </td>
            </tr></tbody></table>          
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn"><p><span class="LinkArrow">&#0187;</span> <a href="Admin.DhcpScopeOrphans.aspx" class=""><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_68 %></a></p></td>
              <td width="33%" class="LinkColumn"></td>
              <td width="33%" class="LinkColumn"></td>
            </tr></tbody></table>
          </div>

           <div class="ContentBucket highlightBucket" id="OrphanedZones" runat="server">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.warning.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_69 %></div>
                <p><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_70, "<span class=\"warningHighlight\">" + String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_114, mgr.OrphanedZoneCount) + "</span>") %></p>
              </td>
            </tr></tbody></table>          
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn"><p><span class="LinkArrow">&#0187;</span> <a href="Admin.DnsZoneOrphans.aspx" class=""><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_71 %></a></p></td>
              <td width="33%" class="LinkColumn"></td>
              <td width="33%" class="LinkColumn"></td>
            </tr></tbody></table>
          </div>
	      
          <div class="ContentBucket highlightBucket" id="OrphanedIPsBox" runat="server">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.warning.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_72 %></div>
                <p><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_73, "<span class=\"warningHighlight\">" + String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_115, mgr.OrphanedIpCount) + "</span>") %></p>
              </td>
            </tr></tbody></table>          
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn"><p><span class="LinkArrow">&#0187;</span> <a href="../import.subnetmanagement.aspx" class=""><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_74 %></a></p></td>
              <td width="33%" class="LinkColumn"></td>
              <td width="33%" class="LinkColumn"></td>
            </tr></tbody></table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.mgmt.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_75 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer">
              <tbody>
                <tr>
                  <td width="33%" class="LinkColumn">
                    <p><span class="LinkArrow">&#0187;</span> <a href="../subnets.aspx" class="">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></p>
                  </td>
                  <td width="33%" class="LinkColumn">
                    <p><span class="LinkArrow">&#0187;</span> <a href="../Subnet.Calculator.aspx">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_76 %></a></p>
                  </td>
                  <td width="33%" class="LinkColumn">
                    <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/IPAM/AddSubnetIPAddresses.aspx">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_19 %></a></p>
                  </td>
                </tr>
                <tr>
                  <td width="33%" class="LinkColumn">
                    <p><span class="LinkArrow">&#0187;</span> <a href="/ui/ipam/ipRequestSettings" class="">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_761 %></a></p>
                  </td>
                  <td width="33%" class="LinkColumn"></td>
                  <td width="33%" class="LinkColumn"></td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.dhcp.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_77 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="manageDhcpServer" href="../../../api2/ipam/ui/dhcp"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_78 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="manageScopes" href="../../../api2/ipam/ui/scopes"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_79 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="addDhcpServer" href="../Dhcp.Server.Add.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_80 %></a></p>
              </td>
            </tr><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="manageDnsServer" href="../../../api2/ipam/ui/dns"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_81 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="manageZones" href="../../../api2/ipam/ui/zones"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_82 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a id ="addDnsServer" href="../Dns.Server.Add.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_83 %></a></p>
              </td>
            </tr></tbody></table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.scan.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_84 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_85 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="Admin.ScanSettings.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_86 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a href="../subnet.scanstatus.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_87 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
              </td>
            </tr></tbody></table>
          </div>

		<div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.integration.png" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_434 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_435 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="../res/packages/com.solarwinds.ipam.package"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_436 %></a></p>
              </td>
            </tr></tbody></table>
          </div>
        </td>

	    <td class="column2of2">
           <!-- Begin 2nd Column -->
          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.started.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_96 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_97 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="100%" class="LinkColumn" colspan="3"s>
                <p><span class="LinkArrow">&#0187;</span> <a href="../IPAMGettingStarted.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_102 %></a></p>
                <p><span class="LinkArrow">&#0187;</span> <a href="<%= SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPAMPHImportToolsetdb") %>" style="font-weight: normal;" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_98 %></a></p>
                <p><span class="LinkArrow">&#0187;</span> <a href="<%= SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPAMPHViewAddSNMPCredentials") %>" style="font-weight: normal;" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_99 %></a></p>
              </td>
            </tr></tbody></table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_112 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_100 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="Admin.MaintenanceSetting.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_101 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="Admin.SystemSetting.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_1 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
<% if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.AllowAdministratorRights()) { %>
                <p><span class="LinkArrow">&#0187;</span> <a href="/Orion/Admin/Accounts/Accounts.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_103 %></a></p>
<% } else { %>
                <p><span class="LinkArrow">&#0187;</span> <a class="noaccess" href="#"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_103 %></a></p>
<% } %>
              </td>
            </tr></tbody></table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.credentials.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_104 %></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_105 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer"><tbody><tr>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="Admin.SnmpCred.List.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_106 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
                <p><span class="LinkArrow">&#0187;</span> <a class="secured" href="Admin.Credentials.List.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_107 %></a></p>
              </td>
              <td width="33%" class="LinkColumn">
              </td>
            </tr></tbody></table>
          </div>

          <div class="ContentBucket">
            <table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr>
              <td class="bucketIcon">
                <img src="../res/images/sw-admin/icon.ipam.settings.thwack.gif" class="BucketIcon">
              </td>
              <td>
                <div class="BucketHeader"><a href="http://thwack.com/" target="blank"><img src="../res/images/sw-admin/thwack.gif"></a></div>
                <p><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_108 %></p>
              </td>
            </tr></tbody></table>
            <table class="NewBucketLinkContainer">
              <tbody>
                <tr>
                  <td width="33%" class="LinkColumn">
                    <p><span class="LinkArrow">&#0187;</span><a
                        href="https://thwack.solarwinds.com/community/network-management/orion-ip-address-manager-ipam"
                        target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_109 %>
                      </a>
                    </p>
                  </td>
                  <td width="66%" class="LinkColumn">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
				
        </td>
	  </tr>
    </tbody>
  </table>
</div>

</asp:Content>
