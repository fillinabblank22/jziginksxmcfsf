﻿using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminCredentialsList : CommonPageServices
    {
        public GridBox Grid
        {
            get { return grid; }
        }
    }
}
