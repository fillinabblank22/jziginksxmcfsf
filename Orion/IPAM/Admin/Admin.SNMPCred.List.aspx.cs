using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Common.Utility;
using SolarWinds.IPAM.BusinessObjects;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminSNMPCredList : CommonPageServices
    {
        public GridBox Grid
        {
            get { return grid; }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (FIPSHelper.FIPSRestrictionEnabled)
            {
                GridPreloader1.WhereClause = $"A.SNMPV3AuthMethod != '{BusinessObjects.SNMPAuthMethod.MD5.ToString()}'";
            }
	    }
    }
}
