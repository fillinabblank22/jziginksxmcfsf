﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminDnsZoneOrphans : CommonPageServices
    {
        public AdminDnsZoneOrphans()
        {
        }

        public GridBox Grid
        {
            get { return grid; }
        }

        protected override void OnLoad(EventArgs e)
        {
            GridPreloader1.WhereClause = "A.GroupId IS NULL";
            GridPreloader1.JoinClause = " LEFT JOIN IPAM.DnsServer server ON {0}.NodeId = server.NodeId JOIN IPAM.GroupNode g on g.GroupId =server.GroupId LEFT JOIN  IPAM.DnsView v ON {0}.DnsViewId=v.DnsViewId ";
        }
    }
}
