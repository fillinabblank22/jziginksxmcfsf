<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Admin.SNMPCred.Add.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.Admin.AdminSNMPCredAdd"
   Title="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_40%>" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHViewAddSNMPCredentials" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server"/>

<orion:Include File="breadcrumb.js" runat="server" />
 <orion:Include ID="Include1" runat="server" Framework="Ext" FrameworkVersion="3.4" />
<IPAMmaster:JsBlock Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server>
Ext.onReady(function(){ $SW.AdminSnmpCredEditorInit($nsId); });
</IPAMmaster:JsBlock>

<div id=warningbox class=warningbox runat="server">
  <div class=inner>
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_38 %>
  </div>
</div>

<div style="margin: 10px 0px 0px 10px;">

  <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li><li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_64 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a
            ><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            ><ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.SnmpCred.List.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_39 %></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="margin-top: 10px;"><%=Page.Title%></h1>
    </td></tr>
  </table>

  <IPAMui:ValidationIcons runat=server />

</div>

  <div id=formbox>
    
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_41 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_42 %>" runat="server" />
            </div></td>
            <td>
            
            <orion:LocalizableButton runat="server" ID="FixName" style="margin-left: 8px;" Visible="False" OnClick="btnFixName_Click" DisplayType="Secondary" Text="<%$ Resources :IPAMWebContent, IPAMWEBDATA_VB1_43  %>"/>
            
            <asp:RequiredFieldValidator
                ID="DisplayNameRequiredValidator"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtDisplayName"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_44 %>"></asp:RequiredFieldValidator>
            <asp:CustomValidator 
                ID="NameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate ="txtDisplayName"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_45 %>"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_46 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlSNMPVersion" runat="server">
                    <asp:ListItem Selected=true Value="2" Text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_47 %>"/>
                </asp:DropDownList>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div id=layoutv2only><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbVersion2Only" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_48 %>"  runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_49 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPPort" CssClass="x-form-text x-form-field" runat="server" Text="161" />
            </div>
            </td>
            <td>
                <asp:CompareValidator ID="SNMPPortTypeValidator" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Operator="GreaterThan"
                    ValueToCompare="0"
                    Type="Integer"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_50 %>"></asp:CompareValidator>
                <asp:CompareValidator ID="SNMPPortTypeValidator2" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Operator="LessThanEqual"
                    ValueToCompare="65535"
                    Type="Integer"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_52 %>" ></asp:CompareValidator>
                 <asp:RequiredFieldValidator ID="SNMPPortRequiredValidator" runat="server"
                    ControlToValidate="txtSNMPPort"
                    Display="Dynamic"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_51  %>"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>
   
    <div id="layoutSNMPv1_2">
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_63%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtCommunityString" CssClass="x-form-text x-form-field" runat="server" Text="" />
            </div></td>
            <td>
                <asp:RequiredFieldValidator ID="CommunityStringRequiredValidator" runat="server"
                    ControlToValidate="txtCommunityString"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_53 %>"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table></div>
    </div>

    <div><!-- ie6 --></div>

    <div id="layoutSNMPv3">
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_54 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPv3Username" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_55%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSNMPv3Context" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan=3><div class=sw-form-subheader>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_56%>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_57 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlAuthMethod" runat="server">
                    <asp:ListItem Selected=true Value="None" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_58 %>"/>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_59%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="tbPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
            </div></td>
        </tr>
        <tr>
            <td colspan=3><div class=sw-form-subheader>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_60 %>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_61%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlSecurityMethod" runat="server">
                    <asp:ListItem Selected=true Value="None" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_58 %>"/>
                </asp:DropDownList> 
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_62%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSecurityPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
            </div></td>
        </tr>
<%--        <tr>
            <td>
            </td>
            <td>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="btnSave_Click" LocalizedText="Save" DisplayType="Primary"/>
                    <orion:LocalizableButtonLink runat="server" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="admin.snmpcred.list.aspx"/>
                </div>
            </td>
        </tr>--%>
    </table></div>
  </div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
      <table>
          <tr>
              <td style="width: 117px">
                  &nbsp;
              </td>
              <td>
                  <div class="sw-btn-bar">
                      <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="btnSave_Click"
                          LocalizedText="Save" DisplayType="Primary" />
                      <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" CausesValidation="false"
                          LocalizedText="Cancel" DisplayType="Secondary" NavigateUrl="admin.snmpcred.list.aspx" />
                  </div>
              </td>
      </table>
</div>
</asp:Content>
