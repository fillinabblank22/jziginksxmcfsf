<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.Admin.ScanSettings"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_36%>" CodeFile="Admin.ScanSettings.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>


<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHViewSubnetScanSettings" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock Orientation=jsPostInit runat=server>

var $ScanSettings = {
  nsId: undefined,
  TransDurReqd: undefined,
  PingPerAddrReqd: undefined,
  DelayBwPingsReqd: undefined,
  PingTimeoutReqd: undefined,
  SnmpRetryReqd: undefined,
  snmpTimeoutReqd: undefined,
  TransCheckBox: undefined,
  IcmpCheckBox: undefined,
  SnmpCheckBox: undefined,
  NeighborCheckBox: undefined,
  TransDiv: undefined,
  IcmpDiv: undefined,
  SnmpDiv: undefined,
  TransDurRange: undefined,
  PingsPerAddressRange: undefined,
  DelayBetweenPingsRange: undefined,
  PingTimeoutRange: undefined,
  SnmpRetriesRange: undefined,
  SnmpTimeoutRange: undefined,

  Init: function($nsId) {
    this.nsId = $nsId;
    this.TransDurReqd = $SW.ns$($nsId,'TransientDurationRequre');
    this.PingPerAddrReqd= $SW.ns$($nsId,'RequirePingsPerAddress');
    this.DelayBwPingsReqd= $SW.ns$($nsId,'RequireDelayBetweenPings');
    this.PingTimeoutReqd= $SW.ns$($nsId,'RequirePingTimeout');
    this.SnmpRetryReqd= $SW.ns$($nsId,'RequireSnmpRetries');
    this.snmpTimeoutReqd= $SW.ns$($nsId,'RequireSnmpTimeout');
    this.TransCheckBox = $SW.ns$($nsId,'cbTransientPeriod');
    this.IcmpCheckBox = $SW.ns$($nsId,'cbIcmpScanning');
    this.SnmpCheckBox = $SW.ns$($nsId,'cbSnmpScanning');
    this.NeighborCheckBox = $SW.ns$($nsId,'cbNeighborScanning');
    this.TransDiv = $('#transcontrols');
    this.IcmpDiv = $('#icmpcontrols');
    this.SnmpDiv = $('#snmpcontrols');
    this.TransDurRange = $SW.ns$($nsId,'TransientDurationRange');
    this.PingsPerAddressRange = $SW.ns$($nsId,'RangePingsPerAddress');
    this.DelayBetweenPingsRange = $SW.ns$($nsId,'RangeDelayBetweenPings');
    this.PingTimeoutRange = $SW.ns$($nsId,'RangePingTimeout');
    this.SnmpRetriesRange = $SW.ns$($nsId,'RangeSnmpRetries');
    this.SnmpTimeoutRange = $SW.ns$($nsId,'RangeSnmpTimeout');
    
    if(this.TransCheckBox){ this.TransCheckBox.click( function(){ $SW.ScanSettings.SyncTrans(false); }); }
    if(this.IcmpCheckBox){ this.IcmpCheckBox.click( function(){ $SW.ScanSettings.SyncIcmp(false); }); }
    if(this.SnmpCheckBox){ this.SnmpCheckBox.click( function(){ $SW.ScanSettings.SyncSnmp(false); }); }
    if(this.NeighborCheckBox){ this.NeighborCheckBox.click( function(){ $SW.ScanSettings.SyncSnmp(false); }); }
    $SW.ScanSettings.SyncTrans(true);
    $SW.ScanSettings.SyncIcmp(true);
    $SW.ScanSettings.SyncSnmp(true);
  },
  SyncTrans: function(initRun){
	var cb = this.TransCheckBox,
	    div = this.TransDiv,
        req = this.TransDurReqd,
        range = this.TransDurRange,
	    delay = initRun ? 0 : 200;
	if(!cb || !cb[0] || !div){ return; }
	if(cb[0].checked){
	    div.hide(delay,$SW.ReflowFooter);
    if(req.length >0)
        ValidatorEnable(req[0], false);
    if(range.length >0)
        ValidatorEnable(range[0], false);
	} else {
	    div.show(delay, $SW.ReflowFooter);
        if(req.length >0)
         ValidatorEnable(req[0], true);
        if(range.length >0)
         ValidatorEnable(range[0], true);
    }
  },
  SyncIcmp: function(initRun){
	var cb = this.IcmpCheckBox,
	    div = this.IcmpDiv,
	    cbX = this.SnmpCheckBox,
        reqPingPerAddr=this.PingPerAddrReqd,
        reqDelbwPing=this.DelayBwPingsReqd,
        reqPingTimeout=this.PingTimeoutReqd,
        rangePingPerAddr=this.PingsPerAddressRange,
        rangeDelbwPing=this.DelayBetweenPingsRange,
        rangePingTimeout=this.PingTimeoutRange,
	    delay = initRun ? 0 : 200;
	if(!cb || !cb[0] || !div){ return; }
	if(cb[0].checked){
	    div.show(delay, $SW.ReflowFooter);
        if(cbX && cbX[0]){
          cbX[0].disabled = false;
          this.SyncSnmp(false);
        if(reqPingPerAddr.length >0)
             ValidatorEnable(reqPingPerAddr[0], true);
        if(reqDelbwPing.length >0)
                 ValidatorEnable(reqDelbwPing[0], true);
        if(reqPingTimeout.length >0)
                 ValidatorEnable(reqPingTimeout[0], true);
        if(rangePingPerAddr.length >0)
                 ValidatorEnable(rangePingPerAddr[0], true);
        if(rangeDelbwPing.length >0)
                 ValidatorEnable(rangeDelbwPing[0], true);
        if(rangePingTimeout.length >0)
                 ValidatorEnable(rangePingTimeout[0], true);
        
        }
	} else {
	    div.hide(delay,$SW.ReflowFooter);
        if(cbX && cbX[0]){
          cbX[0].disabled = true;
          cbX[0].checked = false;
          this.SyncSnmp(false);
        if(reqPingPerAddr.length >0)
             ValidatorEnable(reqPingPerAddr[0], false);
        if(reqDelbwPing.length >0)
                 ValidatorEnable(reqDelbwPing[0], false);
        if(reqPingTimeout.length >0)
                 ValidatorEnable(reqPingTimeout[0], false);
        if(rangePingPerAddr.length >0)
                 ValidatorEnable(rangePingPerAddr[0], false);
        if(rangeDelbwPing.length >0)
                 ValidatorEnable(rangeDelbwPing[0], false);
        if(rangePingTimeout.length >0)
                 ValidatorEnable(rangePingTimeout[0], false);
        }
	}
  },
  SyncSnmp: function(initRun){
	var cb1 = this.SnmpCheckBox,
	    cb2 = this.NeighborCheckBox,
	    div = this.SnmpDiv,
        reqSnmpRetry=this.SnmpRetryReqd,
        reqSnmpTimeout=this.snmpTimeoutReqd,
        RangeSnmpRetries=this.SnmpRetriesRange,
        RangeSnmpTimeout=this.SnmpTimeoutRange,
	    delay = initRun ? 0 : 200;
    if(!cb1 || !cb1[0]){ return; }
    if(!cb2 || !cb2[0]){ return; }
    if(!div){ return; }

	if(cb1[0].checked || cb2[0].checked){
	    div.show(delay, $SW.ReflowFooter);
        if(reqSnmpRetry.length >0)
             ValidatorEnable(reqSnmpRetry[0], true);
        if(reqSnmpTimeout.length >0)
                 ValidatorEnable(reqSnmpTimeout[0], true);
        if(RangeSnmpRetries.length >0)
             ValidatorEnable(RangeSnmpRetries[0], true);
        if(RangeSnmpTimeout.length >0)
                 ValidatorEnable(RangeSnmpTimeout[0], true);
	} else {
	    div.hide(delay,$SW.ReflowFooter);
         if(reqSnmpRetry.length >0)
             ValidatorEnable(reqSnmpRetry[0], false);
        if(reqSnmpTimeout.length >0)
                 ValidatorEnable(reqSnmpTimeout[0], false);
        if(RangeSnmpRetries.length >0)
             ValidatorEnable(RangeSnmpRetries[0], false);
        if(RangeSnmpTimeout.length >0)
                 ValidatorEnable(RangeSnmpTimeout[0], false);
	}
  }
};

$SW.ScanSettings = $ScanSettings;

Ext.onReady(function(){ $SW.ScanSettings.Init($nsId); });

</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
.sw-form-item a, .sw-form-item a:active, .sw-form-item a:visited
{
    color:gray;
    font-size: 10px;
    font-weight: normal;
    text-decoration: underline;
}
.sw-form-item a:hover
{
    color: #F99D1C;
}
.sw-form-label-wide{
    width: 220px !important;
}
.sw-form-label-wide{
    width: 270px !important;
}
.sw-form-item-slim{
    width: 150px !important;
}
</IPAMmaster:CssBlock>


<div>

<div style="margin: 10px 0px 0px 10px;"><table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_64 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.ScanSettings.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_6 %></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="margin-top: 10px;"><%=Page.Title%></h1>
    </td></tr>
  </table></div>

  <IPAMui:ValidationIcons runat=server />
  
  <div id=formbox>
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class="sw-form-col-label sw-form-label-wide"></td>
            <td class="sw-form-col-control sw-form-item-slim"></td>
            <td class=sw-form-col-comment></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><div class=sw-form-subheader>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_10%>
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbTransientPeriod" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_11 %>"  runat="server"/>
            </div></td>
        </tr>
    </table></div>
    
    <div><!-- ie6 --></div>

    <div id=transcontrols>
    <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
              <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_12 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtTransientDuration" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="TransientDurationRequre" runat="server"
                  ControlToValidate="txtTransientDuration"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_13 %>"
                /><asp:RangeValidator id="TransientDurationRange" runat="server"
                  ControlToValidate="txtTransientDuration" 
                  Display="Dynamic"
                  CultureInvariantValues="true"
                  Type="Double"
                  MinimumValue="0.25"
                  MaximumValue="340"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_14 %>"
                 /></td>
        </tr>
    </table></div>
    </div>
      
    <div><!-- ie6 --></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3"><div class=sw-form-subheader>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_15%>
            </div></td>
        </tr>
        
        <tr>
            <td></td>
            <td><div class=sw-form-item>
                <asp:CheckBox ID="cbIcmpScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_16 %>"  runat="server"/>
            </div></td>
        </tr>
        
    </table></div>

    <div><!-- ie6 --></div>

    <div id=icmpcontrols>
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_17%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtPingsPerAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="RequirePingsPerAddress" runat="server"
                  ControlToValidate="txtPingsPerAddress"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_18 %>"
                /><asp:RangeValidator id="RangePingsPerAddress" runat="server"
                  ControlToValidate="txtPingsPerAddress" 
                  Display="Dynamic"
                  Type="Integer"
                  MinimumValue="0"
                  MaximumValue="5"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_19 %>"
                /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_20%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDelayBetweenPings" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="RequireDelayBetweenPings" runat="server"
                  ControlToValidate="txtDelayBetweenPings"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_21 %>"
                /><asp:RangeValidator id="RangeDelayBetweenPings" runat="server"
                  ControlToValidate="txtDelayBetweenPings" 
                  Display="Dynamic"
                  Type="Integer"
                  MinimumValue="10"
                  MaximumValue="500"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_22 %>"
                /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_23 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtPingTimeout" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="RequirePingTimeout" runat="server"
                  ControlToValidate="txtPingTimeout"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_24 %>"
                /><asp:RangeValidator id="RangePingTimeout" runat="server"
                  ControlToValidate="txtPingTimeout" 
                  Display="Dynamic"
                  Type="Integer"
                  MinimumValue="1000"
                  MaximumValue="6500"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_25 %>"
                /></td>
        </tr>
      </table></div>
      </div>

    <div><!-- ie6 --></div>

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan=3><div class=sw-form-subheader>
               <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_26 %>
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><div class=sw-form-item>
                <asp:CheckBox ID="cbSnmpScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_27 %>" runat="server"/>
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><div class=sw-form-item>
                <asp:CheckBox ID="cbNeighborScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_28 %>" runat="server"/>
                <a href="<%= SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPAMPHNeighborScanning") %>" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_29 %></a>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div id=snmpcontrols>
    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_30 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSnmpRetries" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="RequireSnmpRetries" runat="server"
                  ControlToValidate="txtSnmpRetries"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_31 %>"
                /><asp:RangeValidator id="RangeSnmpRetries" runat="server"
                  ControlToValidate="txtSnmpRetries" 
                  Display="Dynamic"
                  Type="Integer"
                  MinimumValue="0"
                  MaximumValue="5"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_32 %>"
                /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_33%>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSnmpTimeout" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="RequireSnmpTimeout" runat="server"
                  ControlToValidate="txtSnmpTimeout"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_34 %>"
                /><asp:RangeValidator id="RangeSnmpTimeout" runat="server"
                  ControlToValidate="txtSnmpTimeout" 
                  Display="Dynamic"
                  Type="Integer"
                  MinimumValue="10"
                  MaximumValue="6500"
                  ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_35 %>"
                 /></td>
        </tr>
    </table></div>
    </div>

    <div><!-- ie6 --></div>

  </div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    
    <table>
        <tr>
            <td>
                <div style="width: 140px;">
                    &nbsp;
                </div>
            </td>
            <td>
                <div class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="btnSave" OnClick="btnSave_Click" DisplayType="Primary"
                        LocalizedText="Save" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" runat="server" NavigateUrl="admin.overview.aspx"
                        CausesValidation="False" DisplayType="Secondary" LocalizedText="Cancel" />
                </div>
            </td>
        </tr>
    </table>

</div>
</asp:Content>
