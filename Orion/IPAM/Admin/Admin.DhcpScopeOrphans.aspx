﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Admin.DhcpScopeOrphans.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.Admin.AdminDhcpScopeOrphans" 
Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_236 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHDHCPScopesFound" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.Any" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock requires="ext,sw-helpserver" Orientation="jsPostInit" runat="server" >

$SW.SetGridFilterToInternal = function(){
    var baseParams = $SW['store'].baseParams;
    if( !baseParams ) $SW['store'].baseParams = {};
    
    $SW['store'].on('load',function(that,rs,opts){
      if( that.totalLength == 0 )
      {
        Ext.MessageBox.show({
         title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_34;E=js}',
         msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_35;E=js}",
         buttons: Ext.MessageBox.OK,
         closable: false,
         fn:function(btn){
           window.location = '/apps/ipam/dhcp-dns/scopes';
         }
        });
      }
    });
};

$SW.DhcpOrphansAddSelected = function(el){

  var sel = Ext.getCmp('grid').getSelectionModel();
  var c = sel.getCount();
  if( c < 1 ){
    Ext.Msg.show({
      title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
      msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_36;E=js}",
      buttons: Ext.Msg.OK,
      animEl: el.id,
      icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var ids = [];
  Ext.each( sel.getSelections(), function(o){ ids.push(parseInt(o.id)); });

  $SW.Tasks.DoProgress( "AddScopes", { ids: ids }, {
        title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_37;E=js}",
        buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
    },function(d){
    if( d.success ) {
        if ( d.results > 0 ) {
            Ext.MessageBox.show({
              title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_37;E=js}",
              msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_AK1_38;E=js}" + " (<a href='http://www.solarwinds.com/documentation/kbLoader.aspx?kb=4280&lang=en' target='_blank' style='text-decoration:underline'>@{R=IPAM.Strings;K=IPAMWEBJS_JT1_1;E=js}</a>)</b>",
              buttons: Ext.MessageBox.OK,
              icon: Ext.MessageBox.INFO,
              fn: function(){ Ext.getCmp('grid').getStore().load(); }
            });
        }
        else {
            Ext.getCmp('grid').getStore().load();
        }
    }
    else
    {
        var errMsg = ( d.message == "Canceled" ) ? "@{R=IPAM.Strings;K=IPAMWEBJS_SE_55;E=js}" : "<b>" + "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}" + "</b><br />" + Ext.util.Format.htmlEncode( d.message )

        Ext.MessageBox.show({
          title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_37;E=js}",
          msg: errMsg,
          buttons: Ext.MessageBox.OK,
          icon: ( d.message == "Canceled" ) ? Ext.MessageBox.INFO : Ext.MessageBox.ERROR,
          fn: function(){ Ext.getCmp('grid').getStore().load(); }
        });
    }
  },
  function(){
    Ext.getCmp('grid').getStore().load();
  });
};

$SW.EnabledColumnRenderer = function(value){
   return value == 'True' ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}';
};
$(document).ready(function(){
    $SW.ns$($nsId,'btnAddSelectedScopes').click( function() {  
        $SW.DhcpOrphansAddSelected(this); return false;
     });
});
</IPAMmaster:JsBlock>


<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }

.sw-grid-buttons { text-align: right; }
.sw-grid-cancel { margin-right: 6px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }

.warningbox a { text-decoration: underline; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-ux-wait.js" Orientation="jsPostInit" runat="server" />

<div style="margin: 10px;">
  <div>
  <table class="titleTable" cellpadding="0" cellspacing="0" style="width: auto">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/scopes" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_7 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.DhcpScopeOrphans.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_237 %></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="padding-left: 0px"><%=Page.Title%></h1>
    </td></tr>
    </table>
    </div>
    <div style="margin: 10px 0px 10px 0px;">
    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_238 %>
    </div>

    <div id="gridframe" style="margin: 0px 0px 0px 0px;"> <!-- --> </div>

    <div style="margin: 10px 0px 16px 0px;">
    <table width="100%"><tbody><tr>
        <td width="50%">
            <a href="/apps/ipam/dhcp-dns/scopes" runat="server" class="backto"><span class="raquo">&raquo;</span> <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_239 %></a>
        </td>
        <td width="50%" align="right">
            <orion:LocalizableButtonLink runat="server" ID="btnAddSelectedScopes" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_246 %>" DisplayType="Primary" />
        </td>
    </tr></tbody></table>
    </div>

        <IPAMlayout:GridBox runat="server"
            width="640"
            maxBodyHeight="500"
            autoHeight="false"
            autoFill="false"
            autoScroll="false"
            id="grid"
            emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_240 %>"
            borderVisible="true"
            RowSelection="false"
            clientEl="gridframe"
            deferEmptyText="false"
            StripeRows="true"
            UseLoadMask="True"
            listeners="{ beforerender: function(o){$SW.ext.gridSnap(o);$SW.SetGridFilterToInternal();} }"
            SelectorName="selector">
            <TopMenu>
                <IPAMui:ToolStripItem id="addSelected" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_246 %>" iconCls="mnu-add" runat="server">
                <handler>function(el){ $SW.DhcpOrphansAddSelected(el); }</handler>
                </IPAMui:ToolStripItem>
            </TopMenu>
              <PageBar pageSize="100" displayInfo="true" />
              <Columns>
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_241 %>" width="210" dataIndex="FoundAddress" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_242 %>" width="180" dataIndex="FoundCIDR" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_243 %>" width="210" dataIndex="ServerName" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_244 %>" width="160" dataIndex="DisabledAtServer" isSortable="true" runat="server" renderer="function(value){ return $SW.EnabledColumnRenderer(value); }" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_245 %>" width="150" dataIndex="FirstSeen" renderer="Ext.util.Format.dateRenderer($SW.ExtLocale.DateFull)" isSortable="true" runat="server" />
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="/Orion/IPAM/extdataprovider.ashx" proxyEntity="IPAM.DhcpScope" AmbientSort="FirstSeen DESC,FoundAddressN"
                JoinClause=" LEFT JOIN IPAM.DhcpServer server ON {0}.NodeId = server.NodeId "
                WhereClause=" {0}.GroupId IS NULL "
                listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                <Fields>
                    <IPAMlayout:GridStoreField dataIndex="ScopeId" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ServerGroupId" clause="server.GroupId AS ServerGroupId" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ServerName" clause="server.FoundAddress AS ServerName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="FoundAddress" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="DisabledAtServer" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="FoundCIDR" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="FirstSeen" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                </Fields>
              </Store>
              </IPAMlayout:GridBox>

    <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />



</div>

</asp:Content>
