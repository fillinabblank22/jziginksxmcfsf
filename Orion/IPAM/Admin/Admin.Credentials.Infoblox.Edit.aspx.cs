using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Admin;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.Credentials;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminCredentialsInfobloxEdit : CommonPageServices
    {
        private PageCredentialsEditor<DhcpServerType> editor;
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.editor = new PageCredentialsEditor<DhcpServerType>(this, this.ViewState, DhcpServerType.Infoblox);
            editor.Initialize();

            bool isResyncRequired = this.editor.ResyncPassword(this.txtPassword.Text, string.Empty);
            if (!this.IsPostBack || isResyncRequired)
            {
                BindControls();
            }
            this.editor.ShowPasswordHint(this.txtPassword, string.Empty);
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.FixName.Visible = (CredentialNameExists.IsValid == false);
            base.OnPreRender(e);
        }

        #region Event Handlers

        protected void MsgSave(object sender, EventArgs e)
        {
            OnSave();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            OnSave();
        }

        protected void OnIsValueRequire(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !string.IsNullOrEmpty(args.Value) && (args.Value.Trim() != string.Empty);
        }

        protected void OnIsPasswordRequire(object source, ServerValidateEventArgs args)
        {
            string password = this.editor.GetChangedPassword(string.Empty);

            if ((password == null) && (this.editor.CredID != null))
            {
                args.IsValid = true;
                return;
            }

            args.IsValid = !string.IsNullOrEmpty(password) && (password.Trim() != string.Empty);
        }

        protected void OnCredentialNameExists(object source, ServerValidateEventArgs args)
        {
            if ((editor.Credential != null) &&
                (editor.Credential.Name == args.Value))
            {
                args.IsValid = true;
                return;
            }

            args.IsValid = !editor.IsNameExist(args.Value);
        }

        protected void OnBtnFixNameClick(object sender, EventArgs e)
        {
            this.txtCredentialName.Text = editor.GetUniqueName(this.txtCredentialName.Text);
            this.CredentialNameExists.IsValid = true;
            this.FixName.Visible = false;
        }

        #endregion

        #region Private Methods

        private void BindControls()
        {
            var credential = this.editor.Credential as InfobloxCredential;
            if (credential == null)
            {
                this.txtCredentialName.Text = string.Empty;
                this.txtUserName.Text = string.Empty;

                // show empty password
                this.editor.CleanupPassword(string.Empty);
                return;
            }

            this.txtCredentialName.Text = credential.Name;
            this.txtUserName.Text = credential.UserName;

            // show asterix password
            this.editor.SetPassword(null, string.Empty);
        }

        private void OnSave()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            StoreCredential();

            if (this.HideChrome)
            {
                string page = "~/Orion/IPAM/DialogWindowDone.aspx";
                this.Server.Transfer(page);
            }
            else
            {
                this.Response.Redirect("~/Orion/IPAM/Admin/Admin.Credentials.List.aspx");
            }
        }

        private void StoreCredential()
        {
            Nullable<int> id = this.editor.CredID;
            var credential = this.editor.Credential as InfobloxCredential;
            if (credential == null)
            {
                credential = new InfobloxCredential();
            }

            credential.IsRescanRequired = false;
            credential.Name = this.txtCredentialName.Text;
            credential.UserName = this.txtUserName.Text;
            
            string password = this.editor.GetChangedPassword(string.Empty);
            if (!string.IsNullOrEmpty(password))
            {
                credential.Password = password;
            }

            if ((id != null) && (id.HasValue))
            {
                credential.ID = id.Value;
                this.editor.Update(credential);
            }
            else
            {
                credential.ID = 0;
                this.editor.Create(credential);
            }
        }

        #endregion
    }
}