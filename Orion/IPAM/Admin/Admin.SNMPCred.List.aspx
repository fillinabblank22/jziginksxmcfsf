<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.Admin.AdminSNMPCredList"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_39%>" CodeFile="Admin.SNMPCred.List.aspx.cs" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHViewSNMPCredentials" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
</IPAMmaster:CssBlock>

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-admin-snmpcred.js" Orientation="jsInit" runat="server" />

<div style="margin: 10px 0px 0px 10px;">
	
  <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1%>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_64 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.SnmpCred.List.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_39%></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="margin-top: 10px;"><%=Page.Title%></h1>
    </td></tr>
  </table>

	    <p><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_66%></p>

        <div id="gridframe" style="margin: 0px 0px 0px 0px;"> <!-- --> </div>

        <IPAMlayout:GridBox runat="server"
            width="640"
            maxBodyHeight="500"
            autoHeight="false"
            autoFill="false"
            autoScroll="false"
            id="grid"
            emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_67 %>"
            borderVisible="true"
            RowSelection="false"
            clientEl="gridframe"
            deferEmptyText="false"
            StripeRows="true"
            UseLoadMask="True"
            listeners="{ beforerender: $SW.ext.gridSnap }"
            SelectorName="selector">
              <TopMenu>
                <IPAMui:ToolStripItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_68 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_69 %>" iconCls="mnu-add" runat=server>
                  <handler>function(){ window.location = 'admin.snmpcred.add.aspx'; }</handler>
                </IPAMui:ToolStripItem>
                <IPAMui:ToolStripSeparator runat="server" />
                <IPAMui:ToolStripItem id="btnEdit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70  %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_71 %>" iconCls="mnu-edit" runat=server>
                  <handler>$SW.AdminSnmpCredEdit</handler>
                 </IPAMui:ToolStripItem>
                <IPAMui:ToolStripSeparator runat="server" />
                <IPAMui:ToolStripItem id="btnUp" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_72 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_73 %>" iconCls="mnu-up" runat=server>
                  <handler>$SW.AdminSnmpCredUp</handler>
                </IPAMui:ToolStripItem>
                <IPAMui:ToolStripSeparator runat="server" />
                <IPAMui:ToolStripItem id="btnDown" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_74 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_75 %>" iconCls="mnu-down" runat=server>
                  <handler>$SW.AdminSnmpCredDown</handler>
                </IPAMui:ToolStripItem>
                <IPAMui:ToolStripItem id="btnDelete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_77 %>" iconCls="mnu-delete" runat=server>
                  <handler>$SW.AdminSnmpCredDelete</handler>
                </IPAMui:ToolStripItem>
              </TopMenu>
              <PageBar pageSize="50" displayInfo=true />
              <Columns>
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_78 %>" width="300" dataIndex="DisplayName" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_79 %>" width="70" dataIndex="SNMPVersion" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_80 %>" width="70" dataIndex="AgentPort" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_81 %>" width="70" dataIndex="DisplayOrder" isSortable="true" runat="server" />
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../extdataprovider.ashx" proxyEntity="IPAM.SNMPCred" AmbientSort="DisplayOrder" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                <Fields>
                    <IPAMlayout:GridStoreField dataIndex="CredId" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="DisplayName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="SNMPVersion" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="DisplayOrder" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="AgentPort" dataType="int" runat="server" />
                </Fields>
              </Store>
              </IPAMlayout:GridBox>

    <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat=server />

</div>

</asp:Content>
