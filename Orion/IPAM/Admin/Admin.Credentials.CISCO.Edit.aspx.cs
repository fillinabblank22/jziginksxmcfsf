﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Admin;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminCredentialsCISCOEdit : CommonPageServices
    {
        private const string KEY_PASSWORD = ".pw";
        private const string KEY_ENABLE_PASSWORD = ".epw";

        private PageCredentialsEditor<DhcpServerType> editor;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.editor = new PageCredentialsEditor<DhcpServerType>(this, this.ViewState, DhcpServerType.CISCO);
            editor.Initialize();

            bool isResyncRequired = this.editor.ResyncPassword(
                this.txtPassword.Text, KEY_PASSWORD);
            bool isEnablePasswordResyncRequired = this.editor.ResyncPassword(
                this.txtEnablePassword.Text, KEY_ENABLE_PASSWORD);
            if (!this.IsPostBack || isResyncRequired || isEnablePasswordResyncRequired)
            {
                BindControls();
            }
            this.editor.ShowPasswordHint(this.txtPassword, KEY_PASSWORD);
            this.editor.ShowPasswordHint(this.txtEnablePassword, KEY_ENABLE_PASSWORD);
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.FixName.Visible = (CredentialNameExists.IsValid == false);
            base.OnPreRender(e);
        }

        #region Event Handlers

        protected void MsgSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void OnIsValueRequire(object source, ServerValidateEventArgs args)
        {
            // 'false', whether the validated value is null or empty string
            args.IsValid = !string.IsNullOrEmpty(args.Value) && (args.Value.Trim() != string.Empty);
        }

        protected void OnIsPasswordRequire(object source, ServerValidateEventArgs args)
        {
            string password = this.editor.GetChangedPassword(KEY_PASSWORD);

            // check, whether the password didn't changed
            if ((password == null) && (this.editor.CredID != null))
            {
                args.IsValid = true;
                return;
            }

            // 'false', whether the validated value is empty string
            args.IsValid = !string.IsNullOrEmpty(password) && (password.Trim() != string.Empty);
        }

        protected void OnCredentialNameExists(object source, ServerValidateEventArgs args)
        {
            // skip identical name
            if ((editor.Credential != null) &&
                (editor.Credential.Name == args.Value))
            {
                args.IsValid = true;
                return;
            }
            args.IsValid = !editor.IsNameExist(args.Value);
        }

        protected void OnBtnFixNameClick(object sender, EventArgs e)
        {
            this.txtCredentialName.Text = editor.GetUniqueName(this.txtCredentialName.Text);
            this.CredentialNameExists.IsValid = true;
            this.FixName.Visible = false;
        }

        #endregion // Event Handlers

        #region Private Methods

        private void BindControls()
        {
            CISCOCredential credential = this.editor.Credential as CISCOCredential;
            if (credential == null)
            {
                this.txtCredentialName.Text = string.Empty;
                this.txtUserName.Text = string.Empty;
                this.CLITelnetProtocol.Checked = true;
                this.CLISSHProtocol.Checked = false;
                this.txtCLIPort.Text = "23";

                this.CLIEnableLevel.SelectedValue = CLICredential.NO_ENABLE_LOGIN.ToString();

                // show empty passwords
                this.editor.CleanupPassword(KEY_PASSWORD);
                this.editor.CleanupPassword(KEY_ENABLE_PASSWORD);
                return;
            }

            this.txtCredentialName.Text = credential.Name;
            this.txtUserName.Text = credential.UserName;

            this.CLIEnableLevel.SelectedValue = credential.EnableLevel.ToString();

            // show asterix passwords
            this.editor.SetPassword(null, KEY_PASSWORD);
            this.editor.SetPassword(null, KEY_ENABLE_PASSWORD);

            // set protocol and port
            if (credential.Protocol == CLIProtocolType.Telnet)
            {
                this.CLITelnetProtocol.Checked = true;
            }
            if (credential.Protocol == CLIProtocolType.SSH)
            {
                this.CLISSHProtocol.Checked = true;
            }
            this.txtCLIPort.Text = credential.Port.ToString();
        }

        private void Save()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            StoreCredential();

            if (this.HideChrome)
            {
                string page = "~/Orion/IPAM/DialogWindowDone.aspx";
                this.Server.Transfer(page);
            }
            else
            {
                this.Response.Redirect("~/Orion/IPAM/Admin/Admin.Credentials.List.aspx");
            }
        }

        private void StoreCredential()
        {
            Nullable<int> id = this.editor.CredID;
            CISCOCredential credential = this.editor.Credential as CISCOCredential;
            if (credential == null)
            {
                credential = new CISCOCredential();
            }

            // set credential's properties from form
            credential.IsRescanRequired = false;
            credential.Name = this.txtCredentialName.Text;
            credential.UserName = this.txtUserName.Text;

            // set password if changed
            string password = this.editor.GetChangedPassword(KEY_PASSWORD);
            if (!string.IsNullOrEmpty(password))
            {
                credential.Password = password;
            }

            // get protocol type (telnet or SSH)
            if ((this.CLITelnetProtocol != null) &&
                (this.CLITelnetProtocol.Checked == true))
            {
                credential.Protocol = CLIProtocolType.Telnet;
            }
            if ((this.CLISSHProtocol != null) &&
                (this.CLISSHProtocol.Checked == true))
            {
                credential.Protocol = CLIProtocolType.SSH;
            }

            // get port number
            if (this.txtCLIPort != null)
            {
                int cliPort;
                Int32.TryParse(this.txtCLIPort.Text, out cliPort);
                credential.Port = cliPort;
            }

            // set enable level
            if (this.CLIEnableLevel != null)
            {
                int enableLevel;
                Int32.TryParse(this.CLIEnableLevel.SelectedValue, out enableLevel);
                credential.EnableLevel = enableLevel;
            }

            // set enable password if changed
            string enablePassword = this.editor.GetChangedPassword(KEY_ENABLE_PASSWORD);
            if (!string.IsNullOrEmpty(enablePassword))
            {
                credential.EnablePassword = enablePassword;
            }

            // use correct method to store credential
            if ((id != null) && (id.HasValue))
            {
                credential.ID = id.Value;
                this.editor.Update(credential);
            }
            else
            {
                credential.ID = 0;
                this.editor.Create(credential);
            }
        }

        #endregion // Private Methods
    }
}