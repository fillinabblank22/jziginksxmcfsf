using System;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;

namespace SolarWinds.IPAM.WebSite.Admin
{
	public partial class AdminOverview : CommonPageServices
	{
        public bool isFreeTool = false;
        public bool isOtherModuleInstalled = false;
        protected PageAdmin mgr;

        public AdminOverview() { mgr = new PageAdmin(this); }

        protected override void OnLoad(EventArgs e)
        {
 	         base.OnLoad(e);
             isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);
        }
	}
}
