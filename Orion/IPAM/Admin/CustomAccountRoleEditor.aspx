﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
    Inherits="Orion_IPAM_Admin_EditViews_CustomRoleEditor"
    CodeFile="CustomAccountRoleEditor.aspx.cs"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_254 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="orion" TagName="HelpLink" Src="~/Orion/Controls/HelpLink.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="ux-all.css,ux-all-plugins.css,sw-account-roles.css" runat="server">
.x-tree-node { font-size: small !important; }
#sw-navhdr h1 {font-size: large; }
#helptext { padding: 10px 0; }
.helpLink { color: #336699 !important; font-size: 11px !important; }
</IPAMmaster:CssBlock>
<IPAMmaster:JsBlock Orientation="jsPostInit" runat="server"
    Requires="ext,ext-ux,sw-account-roles.js,sw-subnet-manage.js" />

<asp:HiddenField ID="AccountId" runat="server" />
<asp:HiddenField ID="WebId" runat="server" />
<script type="text/javascript">
$SW.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
(function () {
    var t = $SW.IPAM.AccountRoles;
    t.AccountField = $('#<%=this.AccountId.ClientID %>');
    t.WebIdField = $('#<%=this.WebId.ClientID %>');

    t.fnQueryBuilder = function () {
        var inAccounts = "{0}.AccountId IN ('" + t.WebIdField.val() + "')";
        return function (node, loader) {            
            var vrf_roots = "{0}.GroupType = 1";
            var w_normal = "{0}.Distance = 1 AND {0}.AncestorId = " + node.attributes.id;
            loader.query.where = (node.isRoot ? vrf_roots : w_normal) + " AND " + inAccounts;
            return $SW.SWQLbuild(node, loader);
        };
    };
    t.fnRemap = function () {
        return function (node, rows) {
            jQuery.each(rows, function (idx, row) {
                row.id = '' + row.GroupId;
                row.leaf = (row.Type == 8) || (row.Type == 512);
                row.uiProvider = 'col';
                if (row.Type != 1 && row.Type != 2)
                    row.iconCls = row.GroupIconPrefix + '-' + row.StatusIconPostfix;

                // [oh] get correct role
                var i = (row.Inherited == "1");
                row.role = { role: t.RoleByName(row.Role), inhr: i };
                row.inherited = i;
            });

            return rows;
        };
    };
})();
</script>

<div style="padding: 10px; height:550px; min-width:1050px;">
  <div id="sw-navhdr">
    <h1><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_255 %> '<asp:Label ID="AccountsList" runat="server" />'</h1>
    <div id="helptext">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_256 %>
    </div>
  </div>
  <div style="float: left;">
    <IPAMlayout:TreeGrid id="roleeditor" runat="server"
        MultipleSelection="true"
        width="800"
        height="450"
        borderVisible="true"
        autoScroll="false"
        EnableDragAndDrop="false"
        EnableSort="false"
        listeners="$SW.IPAM.AccountRoles.RoleEditor.getTreeListeners({
    pu: 'btnPowerUser',
    op: 'btnOperator',
    ro: 'btnReadOnly',
    na: 'btnNoAccess',
    clr: 'btnClear'
})">
        <ConfigOptions runat="server">
            <Options>
                <IPAMlayout:ConfigOption runat="server" Name="checkboxes" Value="true" IsRaw="true" />
                <IPAMlayout:ConfigOption runat="server" Name="animate" Value="false" IsRaw="true" />
            </Options>
        </ConfigOptions>
        <Store ID="TestBoxStore" runat="server" limit="255"
            proxyEntity="IPAM.GroupRoleNode"
            proxyUrl="/Orion/IPAM/Services/Information.asmx/Query"
            orderByClause="'{0}.Distance ASC, {0}.GroupType ASC, {0}.AddressN ASC, {0}.FriendlyName ASC, {0}.CIDR ASC'"
            >
        <fnRemap>$SW.IPAM.AccountRoles.fnRemap()</fnRemap>
        <fnQueryBuilder>$SW.IPAM.AccountRoles.fnQueryBuilder()</fnQueryBuilder>
        <Fields>
            <IPAMlayout:TreeStoreField dataIndex="GroupId" isKey="true" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="Type" clause="{0}.GroupType" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="Text" clause="{0}.FriendlyName" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="Address" clause="{0}.Address" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="Mask" clause="{0}.AddressMask" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="CIDR" clause="{0}.CIDR" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="AvailableCount" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="UsedCount" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="ReservedCount" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="TransientCount" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="PercentUsed" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="Comments" runat="server" />
            <IPAMlayout:TreeStoreField dataIndex="StatusName" runat="server" />   
            <IPAMlayout:TreeStoreField dataIndex="GroupIconPrefix" runat="server" />  
            <IPAMlayout:TreeStoreField dataIndex="StatusIconPostfix" runat="server" />  

            <IPAMlayout:TreeStoreField dataIndex="Role" runat="server" />  
            <IPAMlayout:TreeStoreField dataIndex="Inherited" runat="server" />  
        </Fields>
        </Store>
        <Columns>
            <IPAMlayout:TreeColumn width="200" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_220 %>" DataIndex="Text" Visible="true" HideInGrid="false" runat="server" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_221 %>" DataIndex="Comments" width="150" HideInGrid="true" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_403 %>" DataIndex="text" width="95" HideInGrid="false" Visible="true" runat="server"
                                    template="$SW.IPAM.AccountRoles.RoleTemplate()" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_240 %>" DataIndex="Address" width="150" HideInGrid="false" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_241 %>" DataIndex="CIDR" width="100" HideInGrid="false" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_242 %>" DataIndex="Mask" width="120" HideInGrid="false" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_404 %>" width="100" HideInGrid="false" Visible="true" runat="server"
                                    template="$SW.IPAM.AccountRoles.InheritedTemplate()" />
        </Columns>
        <TopMenu>
            <IPAMui:ToolStripMenuItem id="btnPowerUser" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_257 %>" RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="false" iconCls="mnu-PowerUser" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.RoleEditor.SetRoleValue('roleeditor', 'PowerUser', $SW.IPAM.AccountRoles.WebIdField)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripMenuItem id="btnOperator" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_258 %>" RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="false" iconCls="mnu-Operator" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.RoleEditor.SetRoleValue('roleeditor', 'Operator', $SW.IPAM.AccountRoles.WebIdField)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripMenuItem id="btnReadOnly" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_398 %>" RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="false" iconCls="mnu-ReadOnly" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.RoleEditor.SetRoleValue('roleeditor', 'ReadOnly', $SW.IPAM.AccountRoles.WebIdField)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripMenuItem id="btnNoAccess" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_259 %>" RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="false" iconCls="mnu-NoAccess" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.RoleEditor.SetRoleValue('roleeditor', 'NoAccess', $SW.IPAM.AccountRoles.WebIdField)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripSeparator runat="server" />
            <IPAMui:ToolStripMenuItem id="btnClear" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_400 %>" RoleAclOneOf="IPAM.SiteAdmin" ExceptDemoMode="false" iconCls="mnu-Clear" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.RoleEditor.SetRoleValue('roleeditor', 'Clear', $SW.IPAM.AccountRoles.WebIdField)</handler>
            </IPAMui:ToolStripMenuItem>
        </TopMenu>
    </IPAMlayout:TreeGrid>
  </div>
  <div style="float: left;">
      <ul style="margin: 0 10px; padding: 10px; background-color: #FFFDCC;">
        <li style="padding: 5px;">
            <b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_260 %></b> &nbsp;
            <orion:HelpLink ID="CustomRolesHelpLink" runat="server" HelpUrlFragment="OrionIPAMPHCustomRoles"
                HelpDescription="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_261 %>" CssClass="helpLink" />
        </li>
        <li style="background: url('/Orion/IPAM/res/images/sw-account-roles/poweruser.gif') no-repeat scroll left center transparent; padding: 2px 2px 2px 20px;">
            <b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_257 %></b>&nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_262 %>
        </li>
        <li style="background: url('/Orion/IPAM/res/images/sw-account-roles/operator.gif') no-repeat scroll left center transparent; padding: 2px 2px 2px 20px;">
            <b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_258 %></b>&nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_263 %>
        </li>
        <li style="background: url('/Orion/IPAM/res/images/sw-account-roles/readonly.gif') no-repeat scroll left center transparent; padding: 2px 2px 2px 20px;">
            <b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_264 %></b>&nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_265 %>
        </li>
        <li style="background: url('/Orion/IPAM/res/images/sw-account-roles/hide.gif') no-repeat scroll left center transparent; padding: 2px 2px 2px 20px;">
            <b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_259 %></b>&nbsp; <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_266 %>
        </li>
      </ul>
  </div>
</div>
<div class="sw-btn-bar" style="padding-left: 20px">
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="OnSubmitClick" DisplayType="Primary" OnClientClick="return $SW.IPAM.AccountRoles.ValidateUserChangeOfRole();" />
    <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="OnCancelClick" DisplayType="Secondary" />
</div>
</asp:Content>