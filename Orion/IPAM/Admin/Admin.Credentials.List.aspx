﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    Inherits="SolarWinds.IPAM.WebSite.Admin.AdminCredentialsList"
    CodeFile="Admin.Credentials.List.aspx.cs"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_82%>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHCredentialsList" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
</IPAMmaster:CssBlock>

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-helpserver,sw-admin-cred.js,sw-ux-ext.js" Orientation="jsInit" runat=server />

<IPAMlayout:DialogWindow jsID="editCredential" helpID="credentialedit"
                         Name="editCredential" Url="~/Orion/IPAM/res/html/loading.htm"
                         title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_83 %>" height="400" width="545" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="editCredSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['editCredential'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="editCredCancel" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['editCredential'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg ID="editCredReady" Name="ready" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['editCredential'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="editCredUnload" Name="unload" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['editCredential'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="editCredClose" Name="close" runat="server">
        <handler>function(n,o){ $SW['editCredential'].hide(); $SW.AdminCredEditorRefresh(); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="editCredLicense" Name="license" runat="server">
        <handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<div style="margin: 10px 0px 0px 10px;">
	
  <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_64 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Credentials.List.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_82%></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="margin-top: 10px;"><%=Page.Title%></h1>
    </td></tr>
  </table>

        <div id="gridframe" style="margin: 10px 0px 0px 0px;"> <!-- --> </div>

        <IPAMlayout:GridBox runat="server"
            width="640"
            maxBodyHeight="500"
            autoHeight="false"
            autoFill="false"
            autoScroll="false"
            id="grid"
            emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_84 %>"
            borderVisible="true"
            RowSelection="false"
            clientEl="gridframe"
            deferEmptyText="false"
            StripeRows="true"
            UseLoadMask="True"
            listeners="{ beforerender: $SW.ext.gridSnap }"
            SelectorName="selector">
              <TopMenu>
                <IPAMui:ToolStripMenu ID="ToolStripMenu1" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_85 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_86 %>" iconCls="mnu-add" runat="server">
                    <IPAMui:ToolStripItem ID="btnAddWinCred" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_87 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_88 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'Windows'); }</handler>
                    </IPAMui:ToolStripItem>
                    <IPAMui:ToolStripItem ID="btnAddCliCred" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_89 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_90 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'Cisco'); }</handler>
                    </IPAMui:ToolStripItem>
                    <IPAMui:ToolStripItem ID="btnAddASACred" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_1 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_2 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'ASA'); }</handler>
                    </IPAMui:ToolStripItem>
                    <IPAMui:ToolStripItem ID="ToolStripItem1" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_11 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_OH1_12 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'Bind'); }</handler>
                    </IPAMui:ToolStripItem>
                     <IPAMui:ToolStripItem ID="btnAddISCCred" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_1 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_2 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'ISC'); }</handler>
                    </IPAMui:ToolStripItem>
                     <IPAMui:ToolStripItem ID="btnAddIbxCred" text="<%$ Resources : IPAMWebContent, IPAMWEBCODE_VB1_869 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_870 %>" iconCls="mnu-add" runat="server">
                      <handler>function(that){ $SW.AdminCredentialAdd(that, 'Infoblox'); }</handler>
                    </IPAMui:ToolStripItem>
                </IPAMui:ToolStripMenu>
             
                <IPAMui:ToolStripSeparator ID="ToolStripSeparator1" runat="server" />
                <IPAMui:ToolStripItem ID="btnEdit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_91 %>" iconCls="mnu-edit" runat=server>
                  <handler>$SW.AdminCredentialEdit</handler>
                 </IPAMui:ToolStripItem>
                <IPAMui:ToolStripSeparator ID="ToolStripSeparator2" runat="server" />
                <IPAMui:ToolStripItem ID="btnDelete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_76 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_92 %>" iconCls="mnu-delete" runat=server>
                  <handler>$SW.AdminCredentialDelete</handler>
                </IPAMui:ToolStripItem>
              </TopMenu>
              <PageBar pageSize="50" displayInfo=true />
              <Columns>
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="200" dataIndex="Type" isSortable="true" runat="server" renderer="$SW.CredTypeRenderer" />
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_94 %>" width="200" dataIndex="Name" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_95 %>" width="200" dataIndex="UserName" isSortable="true" runat="server" />
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../extcredentialsprovider.ashx" proxyEntity="IPAM.CRED.LIST" AmbientSort="Name" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                <Fields>
                    <IPAMlayout:GridStoreField dataIndex="ID" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="Type" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="Name" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="UserName" runat="server" />
                </Fields>
              </Store>
        </IPAMlayout:GridBox>

</div>

</asp:Content>

