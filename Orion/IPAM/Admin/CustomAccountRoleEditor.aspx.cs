﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.Orion.Web;
using SolarWinds.IPAM.Client;
using Proxy = SolarWinds.IPAM.Web.Common.Utility;

public partial class Orion_IPAM_Admin_EditViews_CustomRoleEditor : CommonPageServices
{
    private const string DEMO_GUESTACCOUNT = "Guest";
    private const string DEMO_WEBEDIT_ID = "00000000-0000-0000-0000-000000000000";

    private string[] accounts;
    protected string[] Accounts
    {
        get
        {
            if (this.accounts == null)
            {
                this.accounts = (AccountWorkflowManager.Accounts ?? new List<string>()).ToArray();
            }
            return this.accounts;
        }
    }
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            btnSubmit.OnClientClick = "javascript:demoAction('IPAM_Edit_Role_Permission',null); return false;";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        // skip in case of any postbacks (button handlers, etc.)
        if (this.Page.IsPostBack)
            return;

        // [oh] demo uses "Guest" account hardcoded
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            SetUpEditorDemo(DEMO_GUESTACCOUNT);
            return;
        }

        // get first account (we allow edit only single account)
        if (this.Accounts.Length > 0)
        {
            string accountId = this.Accounts[0];
            SetUpEditor(accountId);
        }
    }

    protected void OnCancelClick(object sender, EventArgs e)
    {
     
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            Response.Redirect("~/Orion/IPAM/IPAMSummaryView.aspx");
        }
        

        OnButtonClick(false);
    }

    protected void OnSubmitClick(object sender, EventArgs e)
    {
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
        OnButtonClick(true);
    }

    private void OnButtonClick(bool submit)
    {
        string accountId = TearDownEditor(submit);

        string url = string.Format(
            "/Orion/Admin/Accounts/EditAccount.aspx?ReturnTo={0}&AccountID={1}",
            ReferrerRedirectorBase.GetReferrerUrl(), accountId);

        ReferrerRedirectorBase.Return(url);
    }

    private void SetUpEditor(string accountId)
    {
        // show list of accounts
        this.AccountsList.Text = accountId;

        // store accountId
        this.AccountId.Value = accountId;

        // clone account to web edit
        string webId = string.Empty;
        Proxy.SwisConnector.RunSwisOps(new Proxy.SwisOpCtx[]{ new Proxy.SwisOpCtx(
            string.Format("Clone custom roles for Account: '{0}'", accountId),
            delegate(IpamClientProxy proxy, object param){
                webId = proxy.AppProxy.GroupRole.WebEditAccountClone(accountId);
                return null;
            }
        )});
        this.WebId.Value = webId;
    }

    private void SetUpEditorDemo(string accountId)
    {
        // show list of accounts
        this.AccountsList.Text = accountId;

        // store accountId
        this.AccountId.Value = accountId;

        // use demo account to web edit
        this.WebId.Value = DEMO_WEBEDIT_ID;
    }

    private string TearDownEditor(bool submit)
    {
        string accountId = this.AccountId.Value;
        string webId = this.WebId.Value;

        if (submit)
        {
            Proxy.SwisConnector.RunSwisOps(new Proxy.SwisOpCtx[]{ new Proxy.SwisOpCtx(
                string.Format("Submit custom roles for Account: '{0}'", accountId),
                delegate(IpamClientProxy proxy, object param){
                    proxy.AppProxy.GroupRole.WebEditAccountSubmit(accountId, webId);
                    return null;
                }
            )});
        }
        else
        {
            Proxy.SwisConnector.RunSwisOps(new Proxy.SwisOpCtx[]{ new Proxy.SwisOpCtx(
                "Cleanup clones of custom roles",
                delegate(IpamClientProxy proxy, object param){
                    proxy.AppProxy.GroupRole.WebEditAccountCancel(webId);
                    return null;
                }
            )});
        }

        return accountId;
    }
}