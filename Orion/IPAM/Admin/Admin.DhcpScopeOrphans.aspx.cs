using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminDhcpScopeOrphans : CommonPageServices
	{
        public AdminDhcpScopeOrphans()
        {
        }

        public GridBox Grid
        {
            get { return grid; }
        }

		protected override void OnLoad(EventArgs e)
		{
			GridPreloader1.WhereClause = "A.GroupId IS NULL";
            GridPreloader1.JoinClause = " LEFT JOIN IPAM.DhcpServer server ON {0}.NodeId = server.NodeId ";
		}
	}
}
