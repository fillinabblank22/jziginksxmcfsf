﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class MaintenanceSetting : CommonPageServices
    {
        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (this.IsPostBack == false)
                {
                    ReadMaintenanceSettings();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                LogFile.Error("Unable to communicate with IPAM-SWIS", ex);
                ExceptionContext.MoveToExceptionPage(ex);
            }

            base.OnInit(e);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            try
            {
                SaveMaintenanceSettings();
                Response.Redirect("Admin.Overview.aspx");
            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                LogFile.Error("Unable to communicate with IPAM-SWIS", ex);
                ExceptionContext.MoveToExceptionPage(ex);
            }
        }

        protected void ClickCancel(object sender, EventArgs e)
        {
            Response.Redirect("Admin.Overview.aspx");
        }

        #endregion // Event Handlers

        #region Methods

        private void ReadMaintenanceSettings()
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                int maxEventsRetention = ReadIntegerSetting(
                    SettingName.SETTING_EVENTS_MAXLIFE, SettingName.Default_Events_MaxLife, proxy);
                if (this.txtEventsRetention != null)
                {
                    this.txtEventsRetention.Text = maxEventsRetention.ToString();
                }

                int maxIPHistoryRetention = ReadIntegerSetting(
                    SettingName.SETTING_IPHISTORY_MAXLIFE, SettingName.Default_IPHistory_MaxLife, proxy);
                if (this.txtIPHistoryRetention != null)
                {
                    this.txtIPHistoryRetention.Text = maxIPHistoryRetention.ToString();
                }
            }
        }

        private void SaveMaintenanceSettings()
        {
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                List<Setting> settings = new List<Setting>();

                #region Events Retention

                if (this.txtEventsRetention != null)
                {
                    // read control value
                    int controlValue = GetIntegerValue(
                        this.txtEventsRetention.Text, 1, 5000, SettingName.Default_Events_MaxLife);

                    // read existing setting value
                    int settingValue = ReadIntegerSetting(SettingName.SETTING_EVENTS_MAXLIFE,
                        SettingName.Default_Events_MaxLife, proxy);

                    if (controlValue != settingValue)
                    {
                        settings.Add(new Setting(SettingCategory.Global,
                            SettingName.Events.MaxLife, controlValue));
                    }
                }

                #endregion // Events Retention

                #region IPHistory Retention

                if (this.txtIPHistoryRetention != null)
                {
                    // read control value
                    int controlValue = GetIntegerValue(
                        this.txtIPHistoryRetention.Text, 1, 5000, SettingName.Default_IPHistory_MaxLife);

                    // read existing setting value
                    int settingValue = ReadIntegerSetting(SettingName.SETTING_IPHISTORY_MAXLIFE,
                        SettingName.Default_IPHistory_MaxLife, proxy);

                    if (controlValue != settingValue)
                    {
                        settings.Add(new Setting(SettingCategory.Global,
                            SettingName.IPHistory.MaxLife, controlValue));
                    }
                }

                #endregion // IPHistory Retention

                if (settings.Count > 0)
                {
                    proxy.AppProxy.Setting.UpdateMany(settings.ToArray());
                }
            }
        }

        private int ReadIntegerSetting(string name, int defaultValue, IpamClientProxy proxy)
        {
            Setting setting = proxy.AppProxy.Setting.GetGlobal(SettingCategory.Global, name);
            if (setting != null)
            {
                int? value = null;
                if (setting.TryGetValue(out value))
                {
                    return value.HasValue ? value.Value : defaultValue;
                }
            }
            return defaultValue;
        }

        private int GetIntegerValue(string text, int min, int max, int defaultValue)
        {
            if ((min > max) || (defaultValue < min) || (defaultValue > max))
            {
                throw new ApplicationException(
                    "The value constrains must fullfill this condition: Min <= Default <= Max");
            }

            // convert text into integer value
            int value = defaultValue;
            if (!string.IsNullOrEmpty(text) && int.TryParse(text, out value))
            {
                if (value < min) value = min;
                if (value > max) value = max;
            }

            return value;
        }

        #endregion // Methods
    }
}