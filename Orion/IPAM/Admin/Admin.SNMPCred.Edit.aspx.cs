using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security;
using System.Runtime.InteropServices;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using SolarWinds.Logging;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminSNMPCredEdit : CommonPageServices
    {
        PageSnmpCredEditor editor;
        public AdminSNMPCredEdit() { editor = new PageSnmpCredEditor(this,false); }

		//#region Controls
		//protected global::System.Web.UI.HtmlControls.HtmlGenericControl warningbox;
		//protected global::System.Web.UI.WebControls.CustomValidator NameExists;
		//protected global::System.Web.UI.WebControls.ImageButton FixName;
		//protected global::System.Web.UI.WebControls.TextBox txtDisplayName;
		//#endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (warningbox != null)
            {
                warningbox.EnableViewState = false;
                warningbox.Visible = !Request.IsLocal && !Request.IsSecureConnection;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            editor.Save(true);

            if (NameExists.IsValid == false)
                FixName.Visible = true;
        }

        protected void btnFixName_Click(object sender, EventArgs e)
        {
            txtDisplayName.Text = editor.GetUniqueName(txtDisplayName.Text);
            NameExists.IsValid = true;
            FixName.Visible = false;
        }
		
		public override void Validate()
		{
			byte version = byte.Parse(ddlSNMPVersion.SelectedValue);

			// common validators
			DisplayNameRequiredValidator.Validate();
			NameExists.Validate();
			SNMPPortTypeValidator.Validate();
			SNMPPortTypeValidator2.Validate();
			SNMPPortRequiredValidator.Validate();

			if (version != 3)
				CommunityStringRequiredValidator.Validate();
			
		}
    }
}
