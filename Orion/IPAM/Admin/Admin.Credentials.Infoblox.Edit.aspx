<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
   Inherits="SolarWinds.IPAM.WebSite.Admin.AdminCredentialsInfobloxEdit"
   Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_871%>" CodeFile="Admin.Credentials.Infoblox.Edit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.SiteAdmin" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-admin-cred.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){
  $SW.msgq.UPSTREAM('ready',document);
  $SW.AdminCredEditorInit($nsId, 'txtPassword');
});
</IPAMmaster:JsBlock>

<div id="warningbox" class="warningbox sw-credential-warning" style="display: none; background: url(../res/images/sw/bg.warning1.jpg) top left repeat-x;">
    <div class="inner">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_140%>
    </div>
</div>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

<script type="text/javascript">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
</script>

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%# Resources.IPAMWebContent.IPAMWEBDATA_VB1_871%></div>
   
    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98%></div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtCredentialName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td>
                <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="FixName" visible="false" OnClick="OnBtnFixNameClick"/>
                <asp:CustomValidator
                    ID="CredentialNameRequired"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtCredentialName"
                    OnServerValidate="OnIsValueRequire"
                    ClientValidationFunction="$SW.OnIsValueRequire"
                    ValidateEmptyText="true"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99 %>"></asp:CustomValidator>
                <asp:CustomValidator 
                    ID="CredentialNameExists"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtCredentialName"
                    OnServerValidate="OnCredentialNameExists"
                    ClientValidationFunction="$SW.OnCredentialNameExists"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101%></div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtUserName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td>
                <asp:CustomValidator
                    ID="UserNameRequired"
                    runat="server"
                    Display="Dynamic"
                    OnServerValidate="OnIsValueRequire"
                    ClientValidationFunction="$SW.OnIsValueRequire"
                    ControlToValidate="txtUserName"
                    ValidateEmptyText="true"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label></div></td>
            <td colspan="2">
                <div class="sw-form-item sw-form-clue">
                    <div style="padding-bottom:10px;font-size:12px">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_103%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104%></div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtPassword" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
            </div></td>
            <td>
                <asp:CustomValidator
                    ID="PasswordRequire"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtPassword"
                    OnServerValidate="OnIsPasswordRequire"
                    ValidateEmptyText="true"       
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding-top:8px;">
            </td>
        </tr>
    </table>

</div>
   
<asp:ValidationSummary ID="ValidationSummary1" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

<div id="ChromeButtonBar" runat="server" class="sw-form-buttonbar">
    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary"/>
    <orion:LocalizableButtonLink runat="server" NavigateUrl="~/Orion/IPAM/Admin/Admin.Credentials.List.aspx" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary"/>
</div>   

</asp:Content>