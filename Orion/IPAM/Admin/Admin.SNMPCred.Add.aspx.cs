using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class AdminSNMPCredAdd : CommonPageServices
    {
        PageSnmpCredEditor editor;
        public AdminSNMPCredAdd() { editor = new PageSnmpCredEditor(this, true); }

		//#region Controls
		//protected global::System.Web.UI.HtmlControls.HtmlGenericControl warningbox;
		//protected global::System.Web.UI.WebControls.CustomValidator NameExists;
		//protected global::System.Web.UI.WebControls.ImageButton FixName;
		//protected global::System.Web.UI.WebControls.TextBox txtDisplayName;
		//#endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (warningbox != null)
            {
                warningbox.EnableViewState = false;
                warningbox.Visible = !Request.IsLocal && !Request.IsSecureConnection;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            editor.Save(true);

            if (NameExists.IsValid == false)
                FixName.Visible = true;
        }

        protected void btnFixName_Click(object sender, EventArgs e)
        {
            txtDisplayName.Text = editor.GetUniqueName(txtDisplayName.Text);
            NameExists.IsValid = true;
            FixName.Visible = false;
        }

		public override void Validate()
		{
			byte version = byte.Parse(ddlSNMPVersion.SelectedValue);

			// common validators
			DisplayNameRequiredValidator.Validate();
			NameExists.Validate();
			SNMPPortTypeValidator.Validate();
			SNMPPortTypeValidator2.Validate();
			SNMPPortRequiredValidator.Validate();

			if (version != 3)
				CommunityStringRequiredValidator.Validate();

		}
		
    }
}
