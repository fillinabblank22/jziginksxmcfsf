﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Admin.DnsZoneOrphans.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.Admin.AdminDnsZoneOrphans" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_247 %>"  %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHDHCPScopesFound" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.Any" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-helpserver,sw-ux-wait.js" Orientation="jsPostInit" runat="server" >

$SW.SetGridFilterToInternal = function(){
    var baseParams = $SW['store'].baseParams;
    if( !baseParams ) $SW['store'].baseParams = {};
    
    $SW['store'].on('load',function(that,rs,opts){
      if( that.totalLength == 0 )
      {
        Ext.MessageBox.show({
         title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_34;E=js}',
         msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_39;E=js}",
         buttons: Ext.MessageBox.OK,
         closable: false,
         fn:function(btn){
           window.location = '/api2/ipam/ui/zones';
         }
        });
      }
    });
};

$SW.DnsOrphansAddSelected = function(el){

  var sel = Ext.getCmp('grid').getSelectionModel();
  var c = sel.getCount();
  if( c < 1 ){
    Ext.Msg.show({
      title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
      msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_42;E=js}",
      buttons: Ext.Msg.OK,
      animEl: el.id,
      icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var ids = [];
  Ext.each( sel.getSelections(), function(o){ ids.push(parseInt(o.id)); });

  $SW.Tasks.DoProgress( "AddZones", { ids: ids }, {
        title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_55;E=js}",
        buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
    }, function(d){
    if( d.success ) {
        if ( d.results > 0 ) {
            Ext.MessageBox.show({
              title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_40;E=js}",
              msg: "<b>" + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_41}', d.results) + "</b>",
              buttons: Ext.MessageBox.OK,
              icon: Ext.MessageBox.INFO,
              fn: function(){ Ext.getCmp('grid').getStore().load(); }
            });
        }
        else {
            Ext.getCmp('grid').getStore().load();
        }
    }
    else 
    {
        var errMsg = ( d.message == "Canceled" ) ? "@{R=IPAM.Strings;K=IPAMWEBJS_SE_56;E=js}" : "<b>" + "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}" + "</b><br />" + Ext.util.Format.htmlEncode( d.message )

        Ext.MessageBox.show({
          title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_40;E=js}",
          msg: errMsg,
          buttons: Ext.MessageBox.OK,
          icon: ( d.message == "Canceled" ) ? Ext.MessageBox.INFO : Ext.MessageBox.ERROR,
          fn: function(){ Ext.getCmp('grid').getStore().load(); }
        });
    }
  },
  function(){
    Ext.getCmp('grid').getStore().load();
    
     
  });
};
 $(document).ready(function(){
    $SW.ns$($nsId,'btnAddSelectedScopes').click( function() {  
        $SW.DnsOrphansAddSelected(this); return false;
     });
});
</IPAMmaster:JsBlock>


<IPAMmaster:CssBlock ID="CssBlock1" Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }

.sw-grid-buttons { text-align: right; }
.sw-grid-cancel { margin-right: 6px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }

.warningbox a { text-decoration: underline; }
</IPAMmaster:CssBlock>

<div style="margin: 10px;">
  <div>
  <table class="titleTable" cellpadding="0" cellspacing="0" style="width: auto">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb">
        <li class="bc_item">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/zones" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_7 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.DhcpScopeOrphans.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_237 %></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="padding-left: 0px"><%=Page.Title%></h1>
    </td>
    </tr>
    </table>
    </div>
    <div style="margin: 10px 0px 10px 0px;">
    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_248 %>
    </div>

    <div id="gridframe" style="margin: 0px 0px 0px 0px;"> <!-- --> </div>

    <div style="margin: 10px 0px 16px 0px;">
    <table width="100%"><tbody><tr>
        <td width="50%">
            <a id="A1" href="/api2/ipam/ui/zones" runat="server" class="backto"><span class="raquo">&raquo;</span> <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_239 %></a>
        </td>
        <td width="50%" align="right">
            <orion:LocalizableButtonLink runat="server" ID="btnAddSelectedScopes"   Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_249 %>" DisplayType="Primary" />
        </td>
    </tr></tbody></table>
    </div>

        <IPAMlayout:GridBox runat="server"
            width="640"
            maxBodyHeight="500"
            autoHeight="false"
            autoFill="false"
            autoScroll="false"
            id="grid"
            emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_250 %>"
            borderVisible="true"
            RowSelection="false"
            clientEl="gridframe"
            deferEmptyText="false"
            StripeRows="true"
            UseLoadMask="True"
            listeners="{ beforerender: function(o){$SW.ext.gridSnap(o);$SW.SetGridFilterToInternal();} }"
            SelectorName="selector">
            <TopMenu>
                <IPAMui:ToolStripItem id="addSelected" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_249 %>" iconCls="mnu-add" runat="server">
                <handler>function(el){ $SW.DnsOrphansAddSelected(el); }</handler>
                </IPAMui:ToolStripItem>
            </TopMenu>
              <PageBar pageSize="100" displayInfo="true" />
              <Columns>
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_251 %>" width="210" dataIndex="Name" isSortable="true" runat="server" /> 
                  <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VV0_5 %>" width="210" dataIndex="ViewName" isSortable="true" runat="server" />                
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_252 %>" width="210" dataIndex="ServerName" isSortable="true" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_253 %>" width="75" dataIndex="ZoneType" isSortable="true" runat="server" isHidden="true"/>               
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="/Orion/IPAM/extdataprovider.ashx" proxyEntity="IPAM.DnsZone" AmbientSort="Name DESC"
                JoinClause=" LEFT JOIN IPAM.DnsServer server ON {0}.NodeId = server.NodeId JOIN IPAM.GroupNode g on g.GroupId =server.GroupId  LEFT JOIN  IPAM.DnsView v ON {0}.DnsViewId=v.DnsViewId "
                WhereClause=" {0}.GroupId IS NULL "
                listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                <Fields>
                    <IPAMlayout:GridStoreField dataIndex="DnsZoneId" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ServerGroupId" clause="server.GroupId AS ServerGroupId" runat="server" />
                     <IPAMlayout:GridStoreField dataIndex="ViewName" clause="v.Name AS ViewName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ServerName" clause="g.FriendlyName AS ServerName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="Name" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ZoneType" runat="server" />                 
                </Fields>
              </Store>
              </IPAMlayout:GridBox>
    <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />
</div>
</asp:Content>
