using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web.UI;
using System.Reflection;
using System.Web.Profile;
using SolarWinds.Orion.Web;
using System.Collections.Generic;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Common.Security;
using System.Text;

public partial class Orion_IPAM_Admin_EditViews_EditIPAMAccountRole : ProfilePropEditUserControl
{
	private static AccountRole[] roles = new AccountRole[]
		{
			AccountRole.SiteAdmin,
			AccountRole.PowerUser,
			AccountRole.Operator,
//          AccountRole.Guest, // [pc] useful for 'directlink' accounts (so preferences are not saved)
            AccountRole.ReadOnly,
            AccountRole.NoAccess
		};

	public override string PropertyValue
	{
        get
        {
            string selection = rbRoles.SelectedValue;
            foreach (AccountRole r in roles)
            {
                string role = r.ToString();
                if (string.Equals(selection, role, StringComparison.OrdinalIgnoreCase))
                {
                    return role;
                }
            }
            return string.Empty;
        }
		set
		{
            ListItem item = rbRoles.Items.FindByValue(value) ?? rbCustomRole;
            rbRoles.ClearSelection();
            item.Selected = true;
		}
	}

    protected List<string> Accounts
    {
        get
        {
            List<string> result = AccountWorkflowManager.Accounts;
            return result ?? new List<string>();
        }
    }

    private string _account = null;
    protected string SingleAccount
    {
        get
        {
            if (_account == null)
            {
                List<string> accounts = this.Accounts;
                _account = (accounts.Count == 1) ? accounts[0] : string.Empty;
            }
            return _account;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        rbSiteAdminRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw/icon.admin.settings.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block; , white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_AK1_2 + "</b></td><td>" + Resources.IPAMWebContent.IPAMWEBDATA_VB1_706 +
            "</td></tr></table>";

        rbPowerUserRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw-account-roles/poweruser.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block; , white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_AK1_257 + "</b></td><td>" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_707 + "</td></tr></table>";

        rbOperatorRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw-account-roles/operator.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block;, white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_AK1_258 + "</b></td><td>" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_708 + "</td></tr></table>";

        rbReadOnlyRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw-account-roles/readonly.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block;, white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_AK1_264 + "</b></td><td>" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_709 + "</td></tr></table>";

        rbNoAccesRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw-account-roles/hide.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block;, white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_AK1_259 + "</b></td><td>" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_710 + "</td></tr></table>";

        rbCustomRole.Text =
            "<table><tr><td style=\"width:253px\"><img src='/Orion/IPAM/res/images/sw-account-roles/custom-role.gif' style=\"padding-right: 5px;\"/><b class=\"li-name\" style=\"width: auto; display: inline-block;, white-space: nowrap;\">" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_711 + "</b></td><td>" +
            Resources.IPAMWebContent.IPAMWEBDATA_VB1_712 + "</td></tr></table>";

        if (!Page.IsPostBack && !string.IsNullOrEmpty(this.SingleAccount))
        {
            SetupAccountVariables(this.SingleAccount);
        }
    }

    private string CreateCustomRolesSummary(string accountId, out bool hasCustom)
    {
        int total = 0;
        int custom = 0;
        Dictionary<AccountRole, int> roles;
        using (SolarWinds.IPAM.Client.IpamClientProxy proxy =
            SolarWinds.IPAM.Web.Common.Utility.SwisConnector.GetProxy())
        {
            proxy.AppProxy.GroupRole.CountCustomRoles(accountId, out roles, out total, out custom);

            // [oh] sum SiteAdmin into PowerUser role
            int sumCount = roles[AccountRole.PowerUser] + roles[AccountRole.SiteAdmin];
            roles.Remove(AccountRole.PowerUser);
            roles.Remove(AccountRole.SiteAdmin);
            roles.Add(AccountRole.PowerUser, sumCount);
        }

        hasCustom = (custom > 0);
        if (!hasCustom || total <= 0 || roles.Count <= 0)
        {
            return string.Empty;
        }

        bool isFirst = true;
        StringBuilder summary = new StringBuilder();
        foreach (AccountRole role in roles.Keys)
        {
            int count = roles[role];
            if (count < 1)
                continue;

            if (isFirst == false)
                summary.Append(Resources.IPAMWebContent.IPAMCOMMA_SEPARATOR);
            isFirst = false;

            string roleText = LocalizationHelper.GetEnumDisplayString(
                typeof(AccountRole), role);

            summary.AppendFormat(Resources.IPAMWebContent.IPAMWEBDATA_VB1_717, count, roleText);
        }

        string fullSummary = "";
        if (total == 1)
            fullSummary = string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_718, summary);
        else
            fullSummary = string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_719, summary);

        return fullSummary;
    }

    private void SetupAccountVariables(string accountId)
    {
        this.SingleAccountBox.Visible = true;
        this.MultiAccountBox.Visible = false;

        bool hasCustom = false;
        this.summary.Value = CreateCustomRolesSummary(accountId, out hasCustom);
        this.rbCustomRole.Selected = hasCustom;

        //[oh] if it's new account of inconsistent state of custom role(without items)
        if (this.rbRoles.SelectedItem == null)
        {
            // [oh] force account to be ReadOnly!
            using (IpamClientProxy proxy = SwisConnector.GetProxy())
            {
                proxy.AppProxy.GroupRole.ForceAccountToRole(
                    accountId, AccountRole.ReadOnly, true);
            }
            this.rbReadOnlyRole.Selected = true;
        }
    }

    protected void OnEditClick(object sender, EventArgs e)
    {
        Response.Redirect(
            "/Orion/IPAM/Admin/CustomAccountRoleEditor.aspx?NoChrome=false&ReturnTo=" +
            ReferrerRedirectorBase.GetReturnUrl()
        );
    }
}
