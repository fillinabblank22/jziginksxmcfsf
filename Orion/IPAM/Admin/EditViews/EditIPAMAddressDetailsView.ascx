﻿<%@ Control Language="C#" ClassName="EditIPAMAddressDetailsView" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl" %>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<script runat="server">
	public override string PropertyValue
	{
		get { return ViewSelector.PropertyValue; }
		set { ViewSelector.PropertyValue = value; }
	}
</script>

<div style="width: 282px; float: left;">
   &nbsp;<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="IPAM Address Details View" />
</div>
<div>
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_703 %>
</div>
