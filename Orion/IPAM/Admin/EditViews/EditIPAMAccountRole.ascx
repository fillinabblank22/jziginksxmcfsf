<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditIPAMAccountRole.ascx.cs"
    Inherits="Orion_IPAM_Admin_EditViews_EditIPAMAccountRole" %>
<!--
    IPAM Roles define user access privileges. For more information, see <a style="text-decoration: underline;" href="http://www.solarwinds.com/NetPerfMon/SolarWinds/default.htm?context=SolarWinds&amp;file=OrionIPAMPHRoles.htm">IPAM Roles</a>.
-->
<script type="text/javascript">
    $(document).ready(function () {
        var div = $('#CustomRoleBox');
        var rbs = $('.rbRoles :input');
        var rb = $('#rbCustomRole :input');

        var last = undefined;
        var toggle = function (f) {
            var fn = f || function () { };
            return function () {
		var chckd = rb.prop ? rb.prop('checked') : rb.attr('checked');
                var ch = (chckd == true);
                if (!ch && last == rb[0])
                    if (fn(this) !== true) return;

                if (ch === true) div.show(); else div.hide();
                last = this;
            };
        };

        var summary = function () {
            var nonRole = '<%= ControlHelper.EncodeJsString(Resources.IPAMWebContent.IPAMWEB_JS_DATA_VB1_1) %>';
            var sumRole = $('#<%= this.summary.ClientID %>');
            return {
                get: function () {
                    var v = sumRole.val();
                    return (!v || v == '') ? undefined : v;
                },
                set: function (v) {
                    sumRole.val(v || '');
                    this.apply();
                },
                apply: function () {
                    var txt = this.get();
                    if (!txt) txt = nonRole;
                    $('#SummaryText').html(txt);
                }
            };
        } ();

        var Fn = function (el) {
            // [oh] skip is there is no custom role set
            if (!summary.get()) return true;

            // [oh] ask to delete custom roles
            var role = $(el).next().find('.li-name').html();
            var message = String.format("<%= ControlHelper.EncodeJsString(Resources.IPAMWebContent.IPAMWEB_JS_DATA_VB1_2) %>", role);
            if (!confirm(message) === true) {
                rb.attr('checked', true); div.show(); last = rb[0]; return false;
            }

            // [oh] delete all custom roles for this customer
            var success = false, error = undefined;
            $.ajax({
                url: "/Orion/IPAM/ExtCmdProvider.ashx", type: "POST", async: false,
                data: {
                    entity: 'IPAM.AccountRoles',
                    verb: 'Cleanup',
                    Account: '<%= this.SingleAccount %>',
                    GroupId: ''
                },
                success: function (json) {
                    try {
                        var res = eval('(' + json + ')');
                        if (res.success == true && res.license && res.license.app == "IPAM") {
                            summary.set(''); success = true;
                        }
                        error = res.message;
                    }
                    catch (e) { }
                }
            });
            // [oh] if fail select custom role .. and return result
            if (!success) {
                rb.attr('checked', true); div.show(); last = rb[0];
                if (error) alert(error);
            }
            return success;
        };

        rbs.click(toggle(Fn)).each(toggle());
        summary.apply();
    });
</script>

<style type = "text/css">
table.rbRoles input { float: left; margin: 5px 3px; } 
table.rbRoles label { display: block; } 
</style>

<div id="SingleAccountBox" visible="false" runat="server">
    <asp:RadioButtonList ID="rbRoles" CssClass="rbRoles" runat="server">
        <asp:ListItem Value="SiteAdmin"  id="rbSiteAdminRole" runat="server"></asp:ListItem>
        <asp:ListItem Value="PowerUser" id="rbPowerUserRole" runat="server"></asp:ListItem>
        <asp:ListItem Value="Operator" id="rbOperatorRole" runat="server"></asp:ListItem>
        <asp:ListItem Value="ReadOnly" id="rbReadOnlyRole" Selected="True" runat="server"></asp:ListItem>
        <asp:ListItem Value="NoAccess" id="rbNoAccesRole" runat="server"></asp:ListItem>
        <asp:ListItem Value="Custom" id="rbCustomRole" runat="server"></asp:ListItem>
    </asp:RadioButtonList>
    <asp:HiddenField ID="summary" runat="server" />
    <table id="CustomRoleBox" cellpadding="0" cellspacing="0" border="0" style="padding: 5px 30px;
        background-color: #E4F1F8; display: none;">
        <tbody>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin)) { %>
            <tr>
                <td style="padding: 0; vertical-align: middle;">
                    <span id="SummaryText" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td style="padding: 0; vertical-align: middle;">
                    <orion:LocalizableButton runat="server" ID="btnEditRoles" CausesValidation="False" OnClick="OnEditClick" DisplayType="Small" LocalizedText="Edit"/>
                </td>
            </tr>
<% } else { %>
        <tr><td>
            <h3><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_713 %></h3>
            <p><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_714 %></p>
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_715 %>
        </td></tr>
<% } %>
        </tbody>
    </table>
</div>
<div id="MultiAccountBox" visible="true" runat="server">
    <div style="width: 170px; float: left;">
        &nbsp;</div>
    <i><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_716 %></i>
</div>
