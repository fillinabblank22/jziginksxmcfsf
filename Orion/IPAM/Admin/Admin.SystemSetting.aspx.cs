﻿using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Storage;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.UISetting;
using SolarWinds.IPAM.Web.Common.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SolarWinds.IPAM.WebSite.Admin
{
    public partial class SystemSettings : CommonPageServices
    {
        private const string TresholdCategory = "Threshold.";
        private const string DefaultsCategory = "Defaults.";
        private const string VisualsCategory = "UI.";

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (this.IsPostBack == false)
                {
                    using (IpamClientProxy proxy = SwisConnector.GetProxy())
                    {
                        ReadGeneralSettings(proxy);
                        ReadTresholdsSettings(proxy);
                        ReadConfigurationDefaults(proxy);
                        ReadVisualSettings(proxy);
                        ReadPersonalSettings(proxy, HttpContext.Current);
                        ReadLinuxSetting(proxy);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                LogFile.Error("Unable to communicate with IPAM-SWIS", ex);
                ExceptionContext.MoveToExceptionPage(ex);
            }

            base.OnInit(e);
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            try
            {
                using (IpamClientProxy proxy = SwisConnector.GetProxy())
                {
                    bool doUpdateStatus;
                    List<Setting> settings = new List<Setting>();
                    settings.AddRange(SaveGeneralSettings(proxy));
                    settings.AddRange(SaveTresholdsSettings(proxy, out doUpdateStatus));
                    settings.AddRange(SaveConfigurationDefaults(proxy));
                    settings.AddRange(SaveVisualSettings(proxy));
                    settings.AddRange(SavePersonalSettings(proxy, HttpContext.Current));
                    settings.Add(SaveLinuxSetting(proxy));

                    if (settings.Count > 0)
                    {
                        proxy.AppProxy.Setting.UpdateMany(settings.ToArray());
                        if (doUpdateStatus)
                        {
                            proxy.AppProxy.GroupNode.GlobalUpdateStatus();
                        }
                    }

                }
                Response.Redirect("Admin.Overview.aspx");

            }
            catch (Exception ex)
            {
                ExceptionHandlerSWIS.HandlePageException(ex, LogFile);

                // many issues could make us have a thrown exception.
                // we will have to handle them all and act appropriately.
                LogFile.Error("Unable to communicate with IPAM-SWIS", ex);
                ExceptionContext.MoveToExceptionPage(ex);
            }
        }

        protected void ClickCancel(object sender, EventArgs e)
        {
            Response.Redirect("Admin.Overview.aspx");
        }

        #endregion // Event Handlers

        #region Methods

        private void ReadGeneralSettings(IpamClientProxy proxy)
        {
            var settings = proxy.AppProxy.Setting.Get(null,
                BusinessObjects.SettingCategory.Global, "SystemSetting.%");
            if (settings != null)
            {
                foreach (BusinessObjects.Setting setting in settings)
                {
                    string n = setting.Name.Substring(setting.Name.IndexOf('.') + 1);

                    switch (n)
                    {
                        case "EnableDuplicatedSubnets":
                            {
                                bool? chk = null;
                                if (setting.TryGetValue(out chk))
                                {
                                    cbDuplicatedSubnets.Checked = chk.Value;
                                }
                                break;
                            }
                        case "NewUserInterface":
                            {
                                bool? settingValue = null;
                                if (setting.TryGetValue(out settingValue))
                                {
                                    if (settingValue != null)
                                    {
                                        checkBoxNewUI.Checked = settingValue.Value;
                                    }
                                }
                                break;
                            }
                    }
                }
            }
        }

        private Setting[] SaveGeneralSettings(IpamClientProxy proxy)
        {
            Setting duplicatedSubnets = new BusinessObjects.Setting(
                    BusinessObjects.SettingCategory.Global,
                    SystemSetting.EnableDuplicatedSubnets,
                    cbDuplicatedSubnets.Checked);

            Setting newUserInterface = new BusinessObjects.Setting(
                BusinessObjects.SettingCategory.Global,
                SystemSetting.NewUserInterface,
                checkBoxNewUI.Checked);

            return new BusinessObjects.Setting[]{
                duplicatedSubnets,
                newUserInterface
            };
        }

        private void ReadPersonalSettings(IpamClientProxy proxy, HttpContext context)
        {
            List<BusinessObjects.Setting> settings = proxy.AppProxy.Setting.Get(
                context.Profile.UserName, SettingCategory.User, UISetting.UIS_CONFIRMPARENTCHANGE);

            bool? value = true;
            if (settings != null && settings.Count() > 0)
            {
                BusinessObjects.Setting setting = settings.First();
                if (setting == null || setting.TryGetValue(out value) == false)
                    value = true;
            }

            cbShowParentChangeConfirmDlg.Checked = value.Value;
        }

        private Setting[] SavePersonalSettings(IpamClientProxy proxy, HttpContext context)
        {
            Setting showParentChangeConfirmDlg = new BusinessObjects.Setting(
                    BusinessObjects.SettingCategory.User,
                    UISetting.GetEnumValue(UISetting.UIS_CONFIRMPARENTCHANGE),
                    (bool?)cbShowParentChangeConfirmDlg.Checked);
            showParentChangeConfirmDlg.AssignedTo = context.Profile.UserName;

            // set chached personal settings
            SettingsCache.GetInstance().SetSetting(
                UISetting.UIS_CONFIRMPARENTCHANGE, showParentChangeConfirmDlg);

            return new BusinessObjects.Setting[]{
                showParentChangeConfirmDlg
            };
        }

        private void ReadTresholdsSettings(IpamClientProxy proxy)
        {
            int warning = ThresholdsHelper.DefaultWarningLevel;
            int critical = ThresholdsHelper.DefaultCriticalLevel;

            try
            {
                TresholdsLevelsSettings(proxy, ref warning, ref critical);
            }
            finally
            {
                this.tbWarnLvL.Text = warning.ToString();
                this.tbCritLvL.Text = critical.ToString();
            }
        }

        private Setting[] SaveTresholdsSettings(IpamClientProxy proxy, out bool doUpdateStatus)
        {
            int newWarning = ThresholdsHelper.DefaultWarningLevel;
            int newCritical = ThresholdsHelper.DefaultCriticalLevel;
            TresholdsLevelsControls(ref newWarning, ref newCritical);

            int oldWarning = ThresholdsHelper.DefaultWarningLevel;
            int oldCritical = ThresholdsHelper.DefaultCriticalLevel;
            TresholdsLevelsSettings(proxy, ref oldWarning, ref oldCritical);

            List<Setting> settings = new List<Setting>();

            Setting warning = new Setting(
                    SettingCategory.Global,
                    SettingName.Threshold.SubnetUsageWarning,
                    ThresholdsHelper.DefaultWarningLevel);
            if (newWarning != oldWarning)
            {
                warning.SetValue(newWarning);
                settings.Add(warning);
            }

            Setting critical = new Setting(
                    SettingCategory.Global,
                    SettingName.Threshold.SubnetUsageCritical,
                    ThresholdsHelper.DefaultCriticalLevel);
            if (newCritical != oldCritical)
            {
                critical.SetValue(newCritical);
                settings.Add(critical);
            }

            // [oh] at least one treshold value was updated,
            // therefore is needed to update status of groups.
            doUpdateStatus = (settings.Count > 0);

            return settings.ToArray();
        }

        private void ReadConfigurationDefaults(IpamClientProxy proxy)
        {
            bool scanEnabled = true;
            bool addIPs = true;
            int cidr = 24;
            TimeSpan scanInterval = new TimeSpan(4, 0, 0);

            try
            {
                var settings = proxy.AppProxy.Setting.Get(null,
                    BusinessObjects.SettingCategory.Global, DefaultsCategory + "%");

                if (settings == null)
                    return;

                foreach (BusinessObjects.Setting setting in settings)
                {
                    if (string.IsNullOrEmpty(setting.Name) || string.IsNullOrEmpty(setting.Name.Trim()))
                        continue;

                    string name = setting.Name.Trim();
                    if (name.Equals(SettingName.SETTING_DEFAULT_SCANENABLE, StringComparison.OrdinalIgnoreCase))
                    {
                        bool? check = null;
                        if (setting.TryGetValue(out check) && check.HasValue)
                            scanEnabled = check.Value;

                        continue;
                    }
                    
                    if (name.Equals(SettingName.SETTING_DEFAULT_SCANINTERVAL, StringComparison.OrdinalIgnoreCase))
                    {
                        TimeSpan? interval = null;
                        if (setting.TryGetValue(out interval) && interval.HasValue)
                            scanInterval = interval.Value;

                        continue;

                    }

                    if (name.Equals(SettingName.SETTING_DEFAULT_ADDIPS, StringComparison.OrdinalIgnoreCase))
                    {
                        bool? check = null;
                        if (setting.TryGetValue(out check) && check.HasValue)
                            addIPs = check.Value;

                        continue;

                    }

                    if (name.Equals(SettingName.SETTING_DEFAULT_CIDR, StringComparison.OrdinalIgnoreCase))
                    {
                        int? size = null;
                        if (setting.TryGetValue(out size) && size.HasValue)
                            cidr = size.Value;

                        continue;
                    }
                }
            }
            finally
            {
                this.cbScanEnable.Checked = scanEnabled;
                SetDefaultScanInterval(scanInterval);
                this.cbAddIPs.Checked = addIPs;
                this.tbCIDR.Text = cidr.ToString();
            }
        }

        private Setting[] SaveConfigurationDefaults(IpamClientProxy proxy)
        {
            bool scanEnabled = this.cbScanEnable.Checked;
            bool addIPs = this.cbAddIPs.Checked;
            TimeSpan scanInterval = GetDefaultScanInterval();
            int cidr;
            if (!int.TryParse(this.tbCIDR.Text, out cidr))
                throw new ArgumentException("Tree max items is not a number.");

            Setting setting;
            List<Setting> settings = new List<Setting>();

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANENABLE) ??
                new Setting(SettingCategory.Global, SettingName.Defaults.ScanEnable, true);
            if (RequireToStore(setting, scanEnabled))
            {
                setting.SetValue(scanEnabled);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_DEFAULT_ADDIPS) ??
                new Setting(SettingCategory.Global, SettingName.Defaults.SubnetAddIPs, true);
            if (RequireToStore(setting, addIPs))
            {
                setting.SetValue(addIPs);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_DEFAULT_CIDR) ??
                new Setting(SettingCategory.Global, SettingName.Defaults.SubnetCidr, true);
            if (RequireToStore(setting, cidr))
            {
                setting.SetValue(cidr);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANINTERVAL) ??
                new Setting(SettingCategory.Global, SettingName.Defaults.ScanInterval, true);
            if (RequireToStore(setting, scanInterval))
            {
                setting.SetValue(scanInterval);
                settings.Add(setting);
            }

            return settings.ToArray();
        }

        private void ReadVisualSettings(IpamClientProxy proxy)
        {
            bool treeAddressSort = true;
            int treeMaxItems = 150;
            int pageSizeGroups = 100;
            int pageSizeIPs = 100;

            try
            {
                var settings = proxy.AppProxy.Setting.Get(null,
                    BusinessObjects.SettingCategory.Global, VisualsCategory + "%");

                if (settings == null)
                    return;

                foreach (BusinessObjects.Setting setting in settings)
                {
                    if (string.IsNullOrEmpty(setting.Name) || string.IsNullOrEmpty(setting.Name.Trim()))
                        continue;

                    string name = setting.Name.Trim();
                    if (name.Equals(SettingName.SETTING_UI_MGT_TREESORTBYADDR, StringComparison.OrdinalIgnoreCase))
                    {
                        bool? check = null;
                        if (setting.TryGetValue(out check) && check.HasValue)
                            treeAddressSort = check.Value;

                        continue;
                    }

                    if (name.Equals(SettingName.SETTING_UI_MGT_TREEMAXITEMS, StringComparison.OrdinalIgnoreCase))
                    {
                        int? max = null;
                        if (setting.TryGetValue(out max) && max.HasValue)
                            treeMaxItems = max.Value;

                        continue;
                    }

                    if (name.Equals(SettingName.SETTING_UI_MGT_GRIDPAGESIZE, StringComparison.OrdinalIgnoreCase))
                    {
                        int? pagesize = null;
                        if (setting.TryGetValue(out pagesize) && pagesize.HasValue)
                            pageSizeGroups = pagesize.Value;

                        continue;
                    }

                    if (name.Equals(SettingName.SETTING_UI_MGT_IPGRIDPAGESIZE, StringComparison.OrdinalIgnoreCase))
                    {
                        int? pagesize = null;
                        if (setting.TryGetValue(out pagesize) && pagesize.HasValue)
                            pageSizeIPs = pagesize.Value;

                        continue;
                    }
                }
            }
            finally
            {
                this.cbTreeSort.Checked = treeAddressSort;
                this.tbTreeMax.Text = treeMaxItems.ToString();
                this.tbPageSizeGroups.Text = pageSizeGroups.ToString();
                this.tbPageSizeIPs.Text = pageSizeIPs.ToString();
            }
        }

        private Setting[] SaveVisualSettings(IpamClientProxy proxy)
        {
            bool treeAddressSort = this.cbTreeSort.Checked;

            int treeMaxItems, pageSizeGroups, pageSizeIPs;
            if (!int.TryParse(this.tbTreeMax.Text, out treeMaxItems))
                throw new ArgumentException("Tree max items is not a number.");
            if (!int.TryParse(this.tbPageSizeGroups.Text, out pageSizeGroups))
                throw new ArgumentException("Network view items is not a number.");
            if (!int.TryParse(this.tbPageSizeIPs.Text, out pageSizeIPs))
                throw new ArgumentException("IP Address view items is not a number.");

            Setting setting;
            List<Setting> settings = new List<Setting>();

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_UI_MGT_TREESORTBYADDR) ??
                new Setting(SettingCategory.Global, SettingName.UI.MgtTreeSortByAddr, true);
            if (RequireToStore(setting, treeAddressSort))
            {
                setting.SetValue(treeAddressSort);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_UI_MGT_TREEMAXITEMS) ??
                new Setting(SettingCategory.Global, SettingName.UI.MgtTreeMaxItems, 150);
            if (RequireToStore(setting, treeMaxItems))
            {
                setting.SetValue(treeMaxItems);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                 SettingCategory.Global, SettingName.SETTING_UI_MGT_GRIDPAGESIZE) ??
                 new Setting(SettingCategory.Global, SettingName.UI.MgtGridPageSize, 100);
            if (RequireToStore(setting, pageSizeGroups))
            {
                setting.SetValue(pageSizeGroups);
                settings.Add(setting);
            }

            setting = proxy.AppProxy.Setting.GetGlobal(
                SettingCategory.Global, SettingName.SETTING_UI_MGT_IPGRIDPAGESIZE) ??
                new Setting(SettingCategory.Global, SettingName.UI.MgtIPGridPageSize, 100);
            if (RequireToStore(setting, pageSizeIPs))
            {
                setting.SetValue(pageSizeIPs);
                settings.Add(setting);
            }

            return settings.ToArray();
        }


        private bool RequireToStore(Setting setting, bool value)
        {
            if (setting == null)
                throw new ArgumentNullException("setting");

            bool require = true;
            bool? check = null;
            if (setting.TryGetValue(out check) && check.HasValue)
            {
                require = (value != check.Value);
            }

            return require;
        }

        private bool RequireToStore(Setting setting, int value)
        {
            if (setting == null)
                throw new ArgumentNullException("setting");

            bool require = false;
            int? check = null;
            if (setting.TryGetValue(out check) && check.HasValue)
            {
                require = (value != check.Value);
            }

            return require;
        }

        private bool RequireToStore(Setting setting, TimeSpan value)
        {
            if (setting == null)
                throw new ArgumentNullException("setting");

            bool require = false;
            TimeSpan? check = null;
            if (setting.TryGetValue(out check) && check.HasValue)
            {
                require = (value != check.Value);
            }

            return require;
        }

        private void TresholdsLevelsSettings(IpamClientProxy proxy, ref int warning, ref int critical)
        {
            var settings = proxy.AppProxy.Setting.Get(null,
                BusinessObjects.SettingCategory.Global, TresholdCategory + "%");

            if (settings == null)
                return;

            foreach (BusinessObjects.Setting setting in settings)
            {
                if (string.IsNullOrEmpty(setting.Name) || string.IsNullOrEmpty(setting.Name.Trim()))
                    continue;

                int? percent = null;
                if (!setting.TryGetValue(out percent) || !percent.HasValue)
                    continue;

                string name = setting.Name.Trim();
                if (name.Equals(SettingName.SETTING_THRESHOLD_SUBNETUSAGEWARNING, StringComparison.OrdinalIgnoreCase))
                    warning = percent.Value;
                else if (name.Equals(SettingName.SETTING_THRESHOLD_SUBNETUSAGECRITICAL, StringComparison.OrdinalIgnoreCase))
                    critical = percent.Value;
            }
        }

        private void TresholdsLevelsControls(ref int warning, ref int critical)
        {
            warning = ThresholdsHelper.DefaultWarningLevel;
            critical = ThresholdsHelper.DefaultCriticalLevel;

            if (!int.TryParse(this.tbWarnLvL.Text, out warning))
                throw new ArgumentException("Warning Level is not a number.");

            if (!int.TryParse(this.tbCritLvL.Text, out critical))
                throw new ArgumentException("Critical Level is not a number.");

            if (warning > critical)
                throw new ArgumentException("Warning Level must not be greater than Critical Level.");
        }

        private void SetDefaultScanInterval(TimeSpan interval)
        {
            double w, d;
            w = interval.TotalDays;
            d = w - Math.Floor(w);
            if (w >= 1)
            {
                if (w > 2 || (d == 0 || d == 0.5))
                {
                    this.ddlScanInterval.SelectedValue = "86400";
                    this.tbScanInterval.Text = interval.TotalDays.ToString();
                    return;
                }
            }

            w = interval.TotalHours;
            d = w - Math.Floor(w);
            if (w >= 1)
            {
                if (w > 2 || (d == 0 || d == 0.5))
                {
                    this.ddlScanInterval.SelectedValue = "3600";
                    this.tbScanInterval.Text = interval.TotalHours.ToString();
                    return;
                }
            }

            this.ddlScanInterval.SelectedValue = "60";
            this.tbScanInterval.Text = interval.TotalMinutes.ToString();
        }

        private TimeSpan GetDefaultScanInterval()
        {
            double frequency, multiplier;
            if (!double.TryParse(this.tbScanInterval.Text, out frequency))
                throw new ArgumentException("Scan interval is not a number.");

            if (!double.TryParse(this.ddlScanInterval.SelectedValue, out multiplier))
                throw new ArgumentException("Incorrect multiplier selection.");

            TimeSpan interval = TimeSpan.FromSeconds(Math.Round(frequency * multiplier));

            if (interval < TimeSpan.FromMinutes(10) || interval > TimeSpan.FromDays(7))
                throw new ArgumentException("Scan interval must be a value between 10 minutes and 7 days.");
            
            return interval;
        }

        private void ReadLinuxSetting(IpamClientProxy proxy)
        {
            var setting = proxy.AppProxy.Setting.Get(null,
                BusinessObjects.SettingCategory.Global, SettingName.SETTING_NO_PRESERVE_TIMESTAMPS)
                .FirstOrDefault();
            if (setting != null)
            {
                bool? chk = null;
                if (setting.TryGetValue(out chk))
                {
                    cbNoPreserveTimestamps.Checked = chk.Value;
                }
            }
        }

        private Setting SaveLinuxSetting(IpamClientProxy proxy)
        {
            return new BusinessObjects.Setting(
                BusinessObjects.SettingCategory.Global,
                LinuxSetting.NoPreserveTimestamps,
                cbNoPreserveTimestamps.Checked);
        }

        #endregion // Methods
    }

    public enum SystemSetting
    {
        EnableDuplicatedSubnets,
        NewUserInterface
    }

    public enum LinuxSetting
    {
        NoPreserveTimestamps
    }
}