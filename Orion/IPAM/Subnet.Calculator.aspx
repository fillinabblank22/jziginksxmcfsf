<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Subnet.Calculator.aspx.cs" Inherits="SubnetCalculator"
 Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_76 %>"%>
 
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHWindowSubnetAllocationWizard" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" Runat="Server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" ExceptDemoMode="true" runat="server" />

<script type="text/javascript">
function show(id)
{
	var item = document.getElementById(id);
	item.style.display = "";
}

function hide(id)
{
	var item;
	if (id.style)
		item = id;
	else
		item = document.getElementById(id);
	item.style.display = "none";
}

function IsPostback()
{
    return <%= IsPostBack.ToString().ToLower() %>;
};
</script>

<IPAMmaster:CssBlock runat="server">
.formbox { margin: 10px 10px 0px 0px; padding: 1em; border: 1px solid #dfdfde; background: #fff; }
#selectedsubnetlist ul { padding: 0px 0px 10px 20px; }
#selectedsubnetlist li { list-style-type: disc; line-height: 150%; }
.sw-form-cols-normal td.sw-form-col-label { width: 180px; }

.warningbox-inside .inner a { text-decoration: underline; }
.warningbox-inside { border: 1px solid #d1d0c9; max-width: 870px; background: #f7f596 url(/orion/ipam/res/images/sw/bg.warning.float.gif) repeat-x scroll left bottom; }
.warningbox-inside .inner { background:transparent url(/orion/ipam/res/images/sw/icon.notification.32.gif) no-repeat scroll left top; }
.sw-form-wrapper div.sw-form-invalid-icon { left: 305px; }
</IPAMmaster:CssBlock>

<div style="margin-left: 10px; margin-right: 10px;">

    <table id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb">
        <div style="margin: 15px 0px 11px 0px;"><a href="/Orion/IPAM/Subnets.aspx"><%= IPAMWebContent.IPAMWEBDATA_AK1_4%></a></div>
	    <h1><%= Page.Title %></h1>
    </td></tr>
    </table>

    <div style="margin: 10px 0px 10px 0px;">
      <%= IPAMWebContent.IPAMWEBDATA_TM0_95 %>
    </div>

    <asp:Wizard ID="AddSubnetsWizard" runat="server" DisplaySideBar="false">
        <HeaderTemplate>
            <IPAMui:ValidationIcons runat="server" />
        </HeaderTemplate>
        <WizardSteps>
            <asp:WizardStep ID="CreateSubnets" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_96 %>">

                <IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
                        $SW['IsSubnetListValid'] = true;
                        $SW.grid_firstload = true;
                        $SW.subnetwizard={};

                        $SW.subnetwizard.checklimitation = function()
                        {
                            var spcidr = $SW.ns$($nsId,'ddlSPCidr')[0].value;
                            var sbcidr = $SW.ns$($nsId,'ddlSubnetSize')[0].value;
                            
                            var numberofsubnets = Math.pow(2,(sbcidr - spcidr));
                            return numberofsubnets <= 4096;
                        };
                        
                        $SW.subnetwizard.IsSubnetlistempty = function()
                        {
                            var hfSubnetListobj = document.getElementById($SW.nsGet($nsId,'hfSubnetList'));
                                                        
                            var sel = Ext.getCmp('grid').getSelectionModel();
                            var c = sel.getCount();
                            return c<1;
                        };
                        
                        $SW.subnetwizard.Cleaner = function()
                        {
                            Ext.getCmp('grid').getSelectionModel().clearSelections();
                            var hfSubnetListobj = $SW.ns$($nsId,'hfSubnetList')[0].value="";
                            //grid.store.removeAll();
                        };

                        $SW.subnetwizard.resetparams = function(){
                          var ok = Page_ClientValidate();
                          if(ok){
                              $SW.subnetwizard.Cleaner();
                              if($SW.subnetwizard.checklimitation())
                              {
                                  var spip   = $SW.ns$($nsId,'txtSPNetworkAddress')[0].value;
                                  var spcidr = $SW.ns$($nsId,'ddlSPCidr')[0].value;
                                  var sbcidr = $SW.ns$($nsId,'ddlSubnetSize')[0].value;
                                  var ignoreexist = $SW.ns$($nsId,'cbIgnoreExistSubnet')[0].checked;;
                                  
                                  $SW.store.baseParams.supernetip = spip;
                                  $SW.store.baseParams.supernetcidr = spcidr;
                                  $SW.store.baseParams.subnetcidr = sbcidr;
                                  $SW.store.baseParams.ignoreexistsubnets = ignoreexist;
                                  $SW.store.baseParams.verb="subnetstocreate";
                                  Ext.getCmp('grid').store.load();
                                  
                              }else
                              {
                                      Ext.Msg.show({
                                         title:' @{R=IPAM.Strings;K=IPAMWEBJS_TM0_10;E=js}',
                                         msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_11;E=js}',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.ERROR
                                      });        
                              }
                              
                            $SW['IsSubnetListValid'] = true;
                          }
                        };
                        
                        $SW.subnetwizard.createsubnetlist = function()
                        {
                            
                            var ok = Page_ClientValidate();
                            if(ok)
                            {
                                if( !$SW.subnetwizard.IsSubnetlistempty())
                                {
                                    if($SW['IsSubnetListValid'])
                                    {
                                        var sel = Ext.getCmp('grid').getSelectionModel();
                                        var items = sel.getSelections();
                                        var listsubnet = [];
                                        
                                        Ext.each(items,function(o){
                                            listsubnet.push(o.data.SubnetStart);
                                            });
                                        
                                        var hfSubnetListobj = $SW.ns$($nsId,'hfSubnetList')[0];
                                        hfSubnetListobj.value = listsubnet.join('|');
                                    }else
                                    {
                                        Ext.Msg.show({
                                         title:' @{R=IPAM.Strings;K=IPAMWEBJS_TM0_12;E=js}',
                                         msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_13;E=js}',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.ERROR
                                      }); 
                                      return false;  
                                    }
                                    
                                }else
                                {
                                    Ext.Msg.show({
                                         title:' @{R=IPAM.Strings;K=IPAMWEBJS_TM0_12;E=js}',
                                         msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_14;E=js}',
                                         buttons: Ext.Msg.OK,
                                         icon: Ext.MessageBox.ERROR
                                      }); 
                                      return false;  
                                }
                                
                                return true;
                            }
                            
                            return false;
                        };
                        
                        $SW.subnetwizard.SubnetSize = function(src,args)
                        {
                            var spcidr = $SW.ns$($nsId,'ddlSPCidr')[0].value;
                            var sbcidr = $SW.ns$($nsId,'ddlSubnetSize')[0].value;
                            
                            var subnetCidr = parseInt(sbcidr);
                            var supernetCidr = parseInt(spcidr);
                            
                            if(subnetCidr <= supernetCidr)
                                args.IsValid = false;
                            else
                                args.IsValid = true;
                        };

                        $SW.subnetwizard.SPNetworkAddressChanged = function()
                        {
                           $SW['IsSubnetListValid'] = false;
                        };

                        $SW.grid_reloaded = function(o)
                        {
                            if( IsPostback() && $SW.grid_firstload )
                            {
                                $SW.grid_firstload = false;
                                
                                var grid = Ext.getCmp('grid');
                                var store = grid.store;
                                var subnetList = $SW.ns$($nsId,'hfSubnetList')[0].value.split('|');
                                var selRowIds=[];
                                
                                for(var i = 0; i < subnetList.length; i++)
                                {
                                    var index = store.indexOfId(subnetList[i]);
                                    if(index >= 0)
                                     selRowIds.push(index);
                                }
                                
                                grid.getSelectionModel().selectRows(selRowIds);
                            }
                        };

                        Ext.onReady(function(){
                          
                            $SW.SubnetCalcInit($nsId);

                            $SW.subnetwizard.spIP = $SW.ns$($nsId,'txtSPNetworkAddress')[0].value;
                            $SW.subnetwizard.spCIDR = $SW.ns$($nsId,'ddlSPCidr')[0].value;
                            $SW.subnetwizard.sbCIDR = $SW.ns$($nsId,'ddlSubnetSize')[0].value;
                            $SW.subnetwizard.ignoreexistsb =  $SW.ns$($nsId,'cbIgnoreExistSubnet')[0].checked;

                            $SW.subnetwizard.setparams = function(){
                              $SW.store.baseParams.supernetip = $SW.subnetwizard.spIP;
                              $SW.store.baseParams.supernetcidr = $SW.subnetwizard.spCIDR;
                              $SW.store.baseParams.subnetcidr = $SW.subnetwizard.sbCIDR;
                              $SW.store.baseParams.ignoreexistsubnets = $SW.subnetwizard.ignoreexistsb;
                              $SW.store.baseParams.verb="subnetstocreate";
                              
                              if(IsPostback() == true)
                                {
                                    var grid = Ext.getCmp('grid');
                                    grid.store.load();
                                }
                            };
                          });
            </IPAMmaster:JsBlock>
                <asp:HiddenField ID="hfSubnetList" runat="server" />

                <div id="CreateSubnetsformbox" class="formbox" style="width: 640px;">
                
                    <div visible="true" runat="server">
                        <%= IPAMWebContent.IPAMWEBDATA_TM0_97 %>
                    </div>
                
                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_11 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_TM0_99 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:TextBox ID="txtSPNetworkAddress" onchange ="$SW.subnetwizard.SPNetworkAddressChanged();" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="NetworkAddressRequire" runat="server"
                                    ControlToValidate="txtSPNetworkAddress"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_JT1_1 %>"/>
                                    <asp:CustomValidator ID="NetworkAddressIPv4" runat="server"
                                    ControlToValidate="txtSPNetworkAddress"
                                    ClientValidationFunction="$SW.Valid.Fns.ipv4" 
                                    Display="Dynamic" 
                                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_101 %>"/>
                                    <asp:CustomValidator ID="NetworkAddressSubnet" runat="server" 
                                    ControlToValidate="txtSPNetworkAddress"
                                    OtherControl="txtSPNetworkMask"
                                    ClientValidationFunction="$SW.Valid.Fns.ipv4subnet"
                                    Display="Dynamic"
                                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_102 %>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_TM0_103 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:DropDownList ID="ddlSPCidr" CssClass="sw-pairednum" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_TM0_104 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item x-item-disabled">
                                        <asp:TextBox ID="txtSPNetworkMask" EnableViewState="true" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_TM0_105 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:DropDownList ID="ddlSubnetSize" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <asp:CustomValidator ID="SubnetSize" runat="server" 
                                    ControlToValidate="ddlSubnetSize"
                                    ClientValidationFunction="$SW.subnetwizard.SubnetSize" 
                                    Display="Dynamic" 
                                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_106 %>" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="warningbox sw-credential-warning" style="display: none;">
                                        <div class="inner"><%= IPAMWebContent.IPAMWEBDATA_VV0_1%></div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td >
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_107 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-form-item">
                                        <orion:LocalizableButton runat="server" ID="btnrefreshsubnetlist" LocalizedText="Refresh" OnClientClick="$SW.subnetwizard.resetparams();return false;" DisplayType="Secondary" />
                                    </div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbIgnoreExistSubnet" Checked="true" Text="Show subnets not already allocated" runat="server" />
                                        <asp:Label ID="lblOverlappedSubnetsInfo" class="sw-field-label" runat="server"><%= IPAMWebContent.IPAMWEBDATA_TM0_14%></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                     
                        
                        <div id="gridframe" style="margin: 16px 0px 0px 0px;">
                            <!-- -->
                        </div>
                        <IPAMlayout:GridBox runat="server"
                        width="640"
                        EnableHeaderMenu="false"
                        ID="grid"
                        minBodyHeight="50" 
                        maxBodyHeight="500" 
                        autoFill="false"
                        autoScroll="true"
                        autoHeight="false"
                        emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_109 %>" 
                        borderVisible="true" 
                        isFramed="false" 
                        RowSelection="false"
                        clientEl="gridframe" 
                        deferEmptyText="false"
                        StripeRows="true"
                        listeners="{ beforerender: function(){ $SW.ext.gridSnap; }, render: function(){ $SW.subnetwizard.setparams; } }">
                        <Columns>
                            <IPAMlayout:GridColumn id="GridColumn1" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_17 %>" dataIndex="SubnetStart" width="160" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn2" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_18 %>" dataIndex="SubnetStartEnd" width="160" isFixed="true" runat="server" />
                            <IPAMlayout:GridColumn id="GridColumn3" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_20 %>" dataIndex="SubnetMask" width="160" isFixed="true" runat="server" />
                        </Columns>
                        <Store ID="store" dataRoot="rows" dataCount="count" proxyUrl="SubnetsCalculatorProvider.ashx" autoLoad="false" runat="server"
                         listeners="{ load: $SW.grid_reloaded }">
                            <Fields>
                               <IPAMlayout:GridStoreField ID="GridStoreField1" isKey="true" dataIndex="SubnetStart" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField2" dataIndex="SubnetStartEnd" runat="server" />
                               <IPAMlayout:GridStoreField ID="GridStoreField3" dataIndex="SubnetMask" runat="server" />
                            </Fields>
                        </Store>
                        </IPAMlayout:GridBox>
                        
                    </div>
                    
                </div>
                
                <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
            </asp:WizardStep>
            <asp:WizardStep ID="SubnetsProperties" Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_28 %>">
                <IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
                Ext.onReady(function(){$SW.SubnetEditorInit($nsId);});
                
                $SW.backroundSubnetsCreationInfoMsg = function(obj)
                {
                    if($('#isDemoMode').length > 0) {demoAction('IPAM_Subnet_AllocationWizard', null); return false; }
                    if(!Page_ClientValidate()) return false;
                    alert('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_15;E=js}')
                    return true;
                };
                </IPAMmaster:JsBlock>
                
                <div id="SubnetsPropertiesformbox" class="formbox" style="width: 550px">

                <div id="hints2" visible="false" runat="server"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                  <tr class="sw-form-cols-normal">
                   <td class="sw-form-col-label"></td>
                   <td class="sw-form-col-control"></td>
                   <td class="sw-form-col-comment"></td>
                  </tr>
                  <tr>
                    <td colspan="2"><div class="sw-form-subheader">
                        <%= IPAMWebContent.IPAMWEBDATA_TM0_119 %>
                    </div></td>
                  </tr>
                  <tr>
                    <td colspan="3">
                  <ul style="padding: 10px 0px 10px 15px;">
                    <li style="list-style-type: disc; line-height: 150%;"><%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_23, blSubnets.Items.Count)%></li>
                    <li style="list-style-type: disc; line-height: 150%;"><%= IPAMWebContent.IPAMWEBDATA_TM0_24 %></li>
                  </ul>
                    </td>
                  </tr>
                </table></div>

                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_25%></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <div class="sw-form-subheader">
                                            <img id="open" alt="open" src="/Orion/images/Button.Expand.gif" onclick="hide(this);show('close');show('selectedsubnetlist')" />
                                            <img id="close" alt="close" style="display: none" src="/Orion/images/Button.Collapse.gif" onclick="hide(this);show('open');hide('selectedsubnetlist')" />
                                            <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_26, blSubnets.Items.Count)%> 
                                        </div>
                                        <div id="selectedsubnetlist" style="display: none">
                                            <asp:BulletedList ID="blSubnets" runat="server"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbCreateSupernet" Checked="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_117 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbMoveIntoExistSupernets" Checked="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_118 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader">
                                        <%= IPAMWebContent.IPAMWEBDATA_TM0_28%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"> <%= IPAMWebContent.IPAMWEBDATA_AK1_147%> </div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                    <asp:TextBox ID="txtComment" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-item sw-form-clue"><%= IPAMWebContent.IPAMWEBDATA_VB1_184%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_VB1_185%></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                     <asp:TextBox ID="txtVLAN" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label"><%= IPAMWebContent.IPAMWEBDATA_VB1_186 %></div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:TextBox ID="txtLocation" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" runat="server" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="sw-form-subheader"><%= IPAMWebContent.IPAMWEBDATA_TM0_33 %></div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbDisableAutoScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_34 %>" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="ScanSettings">
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2">
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="cbRetainUserData" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_35 %>" runat="server"/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="sw-field-label">
                                        <%= IPAMWebContent.IPAMWEBDATA_TM0_36%>
                                    </div>
                                </td>
                                <td>
                                    <div class="sw-form-item" id='scanSlider' style="width: 400px;">
                                        <asp:TextBox ID="txtScanInterval" style="position: static; left: 0px; top: 0px;" CssClass="x-form-text x-form-field" Text="240" runat="server" />
                                        <span id="lblScanInterval"><%= IPAMWebContent.IPAMWEBDATA_AK1_20%></span>
                                    </div>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="reqriredScanInterval" runat="server" ControlToValidate="txtScanInterval" Display="Dynamic"
                                        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VV0_14 %>"/>
                                    <asp:RangeValidator ID="rangeScanInterval" runat="server" ControlToValidate="txtScanInterval" Display="Dynamic" Type="Integer"
                                                        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VV0_13 %>" MinimumValue="10" MaximumValue="10080" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div>
                        <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"></td>
                                <td class="sw-form-col-control"></td>
                                <td class="sw-form-col-comment"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <!-- ie6 -->
                </div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
            </asp:WizardStep>
        </WizardSteps>
        <StartNavigationTemplate>
            <div id="buttonbarStart" runat="server" class="sw-btn-bar-wizard">
                <orion:LocalizableButtonLink runat="server" ID="A1" LocalizedText="Back" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
                <orion:LocalizableButton runat="server" ID="StartNext" LocalizedText="Next" DisplayType="Primary" CommandName="MoveNext" OnClientClick="return $SW.subnetwizard.createsubnetlist();" CausesValidation="true" Enabled="true" />
                <orion:LocalizableButtonLink runat="server" ID="A2" LocalizedText="Cancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
            </div>
        </StartNavigationTemplate>
        <FinishNavigationTemplate>
            <div id="buttonbarFinish" runat="server" class="sw-btn-bar-wizard">
                <orion:LocalizableButton runat="server" ID="FinishBack" LocalizedText="Back" CommandName="MovePrevious" DisplayType="Secondary" CausesValidation="False"/>
                <orion:LocalizableButton runat="server" ID="Done" LocalizedText="Done" CommandName="MoveComplete" OnClientClick="return $SW.backroundSubnetsCreationInfoMsg();" OnClick="Done_Click" DisplayType="Primary" />
                <orion:LocalizableButtonLink runat="server" ID="A3" LocalizedText="Cancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
            </div>
        </FinishNavigationTemplate>
    </asp:Wizard>
 </div>
</asp:Content>


