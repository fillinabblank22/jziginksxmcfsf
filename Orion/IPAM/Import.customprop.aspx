<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="True" Async="True" CodeFile="Import.customprop.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportGroupPickcolumns"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_358 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
    
<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewSpreadsheetColumnsImport" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
   
   <IPAM:AccessCheck ID ="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
   
   <IPAMmaster:JsBlock Requires="sw-admin-snmpcred.js" Orientation="jsPostInit" runat="server">
    $SW['PossibleValues'] = [];
    $SW['ColumnUsage'] = {};
    $SW['ColumnBoxes'] = [];
    $SW['hfAddAll'] = '';
    $SW['hfAddSingle'] = '';
    $SW['ColumnNameCP'] = '';
    $SW['DescriptionCP'] = '';
    var _inProgress = true;

    $SW.StartValidation = function(){
      // Sending CmbBox values in Query
      var s='';
      Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( c ) 
           s += c.value+'|';
        });

      $.ajax({
        type: "POST",
        url: location.href+'&SaveCmb='+s,
        async: false,
        data: '',
        success: function() {
          $SW.ShowValidationProgressBar();
        },
        error: function(jqXHR, textStatus, errorThrown) { 
            err(jqXHR.statusText + '<br/>' + jqXHR.responseText);
            return false;
        }
      });

      return false;
    };

    Ext.onReady(function()
     {
        var boxes = $SW['ColumnBoxes'];
        var transform = function(id){
            var item = $('#'+id)[0];
            var converted = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                transform: id,
                forceSelection: true,
                disabled: item.disabled || false
            });

            return converted;
        };

        $("select").each(function(i,o){
            var cb = transform(o.id);
            cb.store.sortInfo = { field: "value", direction: "ASC" };
            cb.on('change',function(that,newVal,oldVal){
              $SW.ComboUsageInUse(newVal,that.id);
              $SW.ComboUsageNotInUse(oldVal,that.id);
            });
            boxes.push(cb.id);
        });

        $SW.InitPossibleValue();

        $SW['hfAddAll'] = $SW.nsGet($nsId, 'hfAddAll');
        $SW['hfAddSingle'] = $SW.nsGet($nsId, 'hfAddSingle');
        $SW['ColumnNameCP'] = $SW.nsGet($nsId, 'ColumnNameCP');
        $SW['DescriptionCP']  = $SW.nsGet($nsId, 'DescriptionCP');
     });

     $SW.ComboUsageInUse = function(val,by)
     {
        if( val == "-1" ) return; // do not import okay for all.

        var r = $SW.ColumnUsage[val];
        if( !r ) return;
        r.inuse = by;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c || c.id == by ) return;

         var exist = c.store.getById(val);
         if( exist )
           c.store.remove(exist);
        });
     };

     $SW.ComboUsageNotInUse = function(val)
     {
        var r = $SW.ColumnUsage[val];
        if( !r ) return;
        r.inuse = null;

        Ext.each( $SW['ColumnBoxes'], function(o){
         var c = Ext.getCmp(o);
         if( !c ) return;

         var exist = c.store.getById(val);
         if( !exist )
            c.store.addSorted(new Ext.data.Record({ value: val, text: r.text }, val));
        });
     };

     $SW.InitPossibleValue = function()
     {
        var seen = {};
        var vals = [];

        Ext.each( $SW['ColumnBoxes'], function(o){
          var c = Ext.getCmp(o), v;
          if( !c ) return;
           v = c.getValue();

           c.store.each(function(r){
            if( !seen[r.data.value] )
            {
              var n = Ext.apply({ inuse: null }, r.data);
              seen[r.data.value] = n;
              vals.push(n);
            }
            return true;
           });

           if( v == "-1" )
             return;

           seen[v].inuse = c.id;
        });

        Ext.each($SW['ColumnBoxes'],function(id){
          var c = Ext.getCmp(id), v;
          if( !c ) return;
          Ext.each(vals,function(o){
            if( o.inuse && o.inuse != id )
            {
              var r = c.store.getById(o.value);
              if( r ) c.store.remove(r);
            }
          });
        });

        $SW['ColumnUsage'] = seen;
     }
   
    Ext.getUrlParam = function(param)
    {
       var params = Ext.urlDecode(location.search.substring(1));
       return param ? params[param] : params;
    };

    function StartTask()
    {
        $SW['ObjectID'] = Ext.getUrlParam('ObjectID');
        $SW['Type'] = Ext.getUrlParam('Type');
        $SW['GroupID'] = Ext.getUrlParam('GroupID');
        $SW['ImportType'] = Ext.getUrlParam('ImportType');
        $SW['CreateType'] = Ext.getUrlParam('CreateType');
        $SW['IsZipImport'] = Ext.getUrlParam('IsZipImport');

        var dataparams = '';
        if ($SW['ImportType'] == 1)
            dataparams = 'requesttype=validateimportip';
        else
            dataparams = 'requesttype=validateimportstruct';
    
        dataparams +=  '&verb=CreateTask&filetype='+$SW['Type']+'&objectid='+$SW['ObjectID'];

        if($SW['GroupID'] != null)
            dataparams += "&GroupID=" + $SW['GroupID'];

        if($SW['CreateType'] != null)
            dataparams += "&createtype=" + $SW['CreateType'];

        if ($SW['IsZipImport'] != null)
            dataparams += "&IsZipImport=" + $SW['IsZipImport'];
        
        $.ajax({
            type: "POST",
            url: 'ImportExportProvider.ashx',
            async: false,
            data: dataparams,
            error: err
          });
      
    }

    function RecvTaskStatus(o)
    {
      if( o.status == 'Completed' ) {
          _inProgress = false;
          ProgressEnd('ok',o);
      } else
        if (o.status == 'Failed')
           ProgressEnd('failed',o.error);
        else
        {
            var val = typeof(o.pos) == "string" ? eval(o.pos) : o.pos;
            val /= 100;
            window.localStorage.setItem("lastAction", Date.now());
            Ext.MessageBox.updateProgress(val , o.msg, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}');
        }
    }

    function GetTaskStatus()
    {
      var requestType = $SW['ImportType'] == 1 ? 'validateimportip' : 'validateimportstruct';

      Ext.Ajax.request({
           url: 'ImportExportProvider.ashx',
           method: 'POST',
           success: function(result, request)
           {
                var o = Ext.util.JSON.decode(result.responseText);
                RecvTaskStatus(o);
           },
           failure: function(result, request) {
                      if(runner!=null)
                         runner.stopAll();
                      },
           params:
           {
            requesttype: requestType,
            verb: 'GetStatusValidateTask',
            objectid:$SW['ObjectID']
           }
           });
    }

    function ProgressEnd(btnid,value)
    {
        Ext.MessageBox.hide();
        if(runner!=null) {
            runner.stopAll();
        }

        if( btnid == 'ok' ) {            
            var link = $SW.appRoot();
            var commonParams = 'ObjectID=' + $SW['ObjectID'] +'&Type=' + $SW['Type'] + '&OpenedFrom=4';
            if($SW['GroupID'] != null)
            commonParams += "&GroupID=" + $SW['GroupID'];

            if ($SW['CreateType'] != null)
            commonParams += "&CreateType=" + $SW['CreateType'];
            
            if ($SW['IsZipImport'] != null)
                commonParams += "&IsZipImport=" + $SW['IsZipImport'];
        
            if (value.validationSuccess === false) {
                commonParams += "&ImportType=" + $SW['ImportType'];
                link += 'Orion/IPAM/Import.preview.aspx?' + commonParams;            
                window.location.href = link;
            }
            else {
                if ($SW['IsZipImport'].toLowerCase() === 'true' && $SW['ImportType'] == 2) {
                    commonParams += "&ImportType=1";
                    link += 'Orion/IPAM/Import.pickcolumns.aspx?' + commonParams;
                } else if ($SW['IsZipImport'].toLowerCase() === 'true' && $SW['ImportType'] == 1) {
                    commonParams += "&ImportType=3";
                    link += 'Orion/IPAM/Import.confirm.aspx?' + commonParams;
                } else {
                    commonParams += "&ImportType=" + $SW['ImportType'];
                    link += 'Orion/IPAM/Import.confirm.aspx?' + commonParams;  
                }
                window.location.href = link;
            }
        }
   
        if( btnid == 'failed' && _inProgress === true) {
            err(value);
        }
    }

    var runner = new Ext.util.TaskRunner();

    var StatusTask = {
        run: function(){
            GetTaskStatus();
        },
        interval: 500 //1 second
    }

    $SW.ShowValidationProgressBar = function(el)
    {
         _inProgress = true;
         StartTask();

         Ext.MessageBox.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_79;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_78;E=js}',
            progressText: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_62;E=js}',
            width:300,
            progress:true,
            closable:true,
            animEl: el ? el.id : null
         });
   
       runner.start(StatusTask);
    };

    $SW.AddAllProperties = function(){
        
        Ext.MessageBox.buttonText.yes = "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_81;E=js}";
        Ext.MessageBox.buttonText.no = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_73;E=js}";

        Ext.MessageBox.show(
        {
         title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_81;E=js}',
         msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_82;E=js}',
         fn: $SW.processAddAllResult,
         buttons: Ext.Msg.YESNO,
         buttonText: { ok: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_83;E=js}", no: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_73;E=js}" }
         });
      return false;
    };

    $SW.processAddAllResult = function(btn)
    {
        if (btn === 'yes')
        {
            var hfAddAll = $('#'+$SW['hfAddAll'])[0];
            hfAddAll.value = 'true';
            var form = $('#aspnetForm');
            form.submit();
        }
    }

    function err(msg)
    {
       msg = msg || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}';

       Ext.Msg.show({
           title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
           msg: msg,
           buttons: Ext.Msg.OK,
           animEl: this,
           icon: Ext.MessageBox.ERROR
       });        
    }

    $SW.AddCustomProperty = function(object)
    {
        var importType = Ext.getUrlParam('ImportType') || 'unknown';
        var div = object.parentNode;
        var columnName = div.children[0].value;
        var description = div.children[1].value;
        
        var hfAddSingle = $('#'+$SW['hfAddSingle'])[0];
        hfAddSingle.value = 'true';

        var ColumnNameCP = $('#'+$SW['ColumnNameCP'])[0];
        ColumnNameCP.value = columnName;

        var DescriptionCP = $('#'+$SW['DescriptionCP'])[0];
        DescriptionCP.value = description;
        
        var form = $('#aspnetForm');
        form.submit();
    }

    $SW.RefreshCustomPropertiesPage = function()
    {
        var hfAddSingle = $('#'+$SW['hfAddSingle'])[0];
        hfAddSingle.value = 'true';
        
        var form = $('#aspnetForm');
        form.submit();
    }
   </IPAMmaster:JsBlock>
   
<IPAMmaster:CssBlock runat="server">
.sw-form-wrapper-custom { width:850px; border-style: solid solid none solid; table-layout: fixed; }
.sw-form-cols-table td.sw-form-col-label-custom { width: 250px; border-style:none solid solid none; border-width: 0 1px 2px 0; }
.sw-form-cols-table td.sw-form-col-control-custom { width: 320px; border-style:none solid solid none; border-width: 0 2px 2px 0; }
</IPAMmaster:CssBlock>

<div style="margin-left: 10px;">

     <table width="auto" style="table-layout: fixed;  margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><%= Page.Title %></h1>
        </td>
        </tr>
      </table>

    <div id="formbox">

        <div style="margin: 0px 0px 10px 0px;">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_359 %>
        </div>

        <asp:Repeater ID="CustomColumnsRepeater" runat="server" >
            <HeaderTemplate>
                <table class="sw-form-wrapper-custom" border="0" cellspacing="0" cellpadding="0">
                    <tr class="sw-form-cols-table">
                        <td class="sw-form-col-label-custom"><div class="sw-form-subheader" style="text-align:center;"><b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_360 %></b></div></td>
                        <td class="sw-form-col-control-custom"><div class="sw-form-subheader" style="text-align:center;"><b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_361 %></b></div></td>
                        <td class="sw-form-col-comment-custom"><div class="sw-form-subheader" style="text-align:center;"><b><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_362 %></b></div></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="sw-form-cols-table">
                    <td class="sw-form-col-cell-custom">
                        <div style="margin-top: 4px">
                            <asp:Label ID="customPropertyName" runat="server" Text=""></asp:Label>
                            <asp:Label ID="customPropertyId" runat="server" Text="" Visible="false"></asp:Label>
                        </div>
                    </td>
                    <td class="sw-form-col-cell-custom">
                        <div class="sw-form-item" style="margin-top: 4px">
                            <asp:DropDownList ID="ddlAttributes" runat="server" Visible="false"></asp:DropDownList>
                            <asp:Label ID="spreadsheetName" runat="server" Visible="false"></asp:Label>
                        </div>
                    </td>
                    <td class="sw-form-col-cell-right-custom">
                        <div class="sw-field-label" style="margin-top: 4px; text-align: center;">
                            <asp:HiddenField ID="ColumnName" runat="server" />
                            <asp:HiddenField ID="Description" runat="server" />
                            <orion:LocalizableButton runat="server" ID="AddCustom" Visible="false" OnClientClick="$SW.AddCustomProperty(this); return false;" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_141 %>" DisplayType="Small" />
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <IPAMlayout:DialogWindow jsID="mainWindow" Name="mainWindow" helpID="customadd" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_141 %>" height="400" width="550" runat="server">
            <Buttons>
                <IPAMui:ToolStripButton id="mainWindowSave" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
                    <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
                </IPAMui:ToolStripButton>
                <IPAMui:ToolStripButton ID="mainWindowCancel" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
                    <handler>function(){ $SW['mainWindow'].hide(); }</handler>
                </IPAMui:ToolStripButton>
            </Buttons>
            <Msgs>
                <IPAMmaster:WindowMsg ID="ReadyAddCustomProperty" Name="ready" runat="server">
                    <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
                </IPAMmaster:WindowMsg>
                <IPAMmaster:WindowMsg ID="CloseAddCustomProperty" Name="close" runat="server">
                    <handler>function(n,o){ $SW['mainWindow'].hide(); $SW.RefreshCustomPropertiesPage(); }</handler>
                </IPAMmaster:WindowMsg>
            </Msgs>
        </IPAMlayout:DialogWindow>

        <asp:HiddenField ID="hfAddSingle" runat="server" />
        <asp:HiddenField ID="ColumnNameCP" runat="server" />
        <asp:HiddenField ID="DescriptionCP" runat="server" />
        
        <table class="sw-form-wrapper-custom" border="0" cellspacing="0" cellpadding="0" style="border-style: none; margin-top: 20px;">
            <tr>
                <td colspan="3" class="sw-form-cell-add-all">
                    <div id="addAllDiv" style="text-align: right">
                        <asp:HiddenField ID="hfAddAll" runat="server" />
                        <orion:LocalizableButton runat="server" ID="AddAll" OnClientClick="$SW.AddAllProperties(); return false;" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_363 %>" DisplayType="Small" />
                    </div>
                </td>
            </tr>
        </table>

        <div id="errorsList">
            <asp:Label ID="errorText" Visible="false" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_364 %>" ForeColor="Red" runat="server" />
            <asp:BulletedList ID="errorsBulletList" Visible="false" ForeColor="Red" runat="server" BulletStyle="Disc" Style="list-style-position:inside;"/>
        </div>
    </div>

    <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
         <orion:LocalizableButton runat="server" ID="Back" OnClick="Back_Click" LocalizedText="Back" DisplayType="Secondary" />
         <orion:LocalizableButton runat="server" ID="Next" OnClientClick="$SW.StartValidation(); return false;" LocalizedText="Next" DisplayType="Primary" />
         <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
   </div>

</div>
</asp:Content>
