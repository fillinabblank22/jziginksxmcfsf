﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
	CodeFile="Dhcp.Management.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpManagementPage"
	Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_5%>" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMcommon" Namespace="SolarWinds.IPAM.Web.Common" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ipam" TagName="NavBar" Src="~/Orion/IPAM/Controls/NavigationTabBar.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
	<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
	  { %>
	<a class="HelpText" href="/Orion/IPAM/Admin/Admin.Overview.aspx" style="background: transparent url(/Orion/IPAM/res/images/sw/Page.Icon.MdlSettings.gif) scroll no-repeat left center;
		padding: 2px 0px 2px 20px;">
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a>
	<%}%>
	
	<ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDHCPScopesView" />

</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">
	<ipam:AccessCheck RoleAclOneOf="IPAM.ReadOnly" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
		runat="server" />

<ipam:DHCPManagementCapability JsID="dhcpCapability" runat="server" />

<script type="text/javascript">
		function setFooter() { };
</script>
	<IPAMmaster:JsBlock Requires="ext,ext-ux,ext-quicktips,sw-helpserver,sw-subnet-manage.js,sw-dhcp-groupby.js,sw-dhcp-manage.js,sw-ux-wait.js"
		Orientation="jsPostInit" runat="server">

Ext.onReady(function(){$('#container').hide();});
/* called when we insert an eval banner */

$SW.ReflowPage = function(){
var hdr = Ext.getCmp('headerlayout');
hdr.setHeight( Ext.get('content').getHeight() );
hdr.syncSize();
Ext.getCmp('mainlayout').doLayout();
};

$SW.ConfirmClearNotice = function(that){

  var ClearFn = function()
  {
	 var fail = $SW.ext.AjaxMakeFailDelegate( 'Clear New DHCP Scope Notice' );
	 var pass = $SW.ext.AjaxMakePassDelegate( 'Clear New DHCP Scope Notice', function(){
		$SW.ns$($nsId,'warningbox').hide(1500, $SW.ReflowPage);
	   });

	  Ext.Ajax.request({
	   url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

	   success: function(result, request){
		var res = Ext.util.JSON.decode(result.responseText);
		pass(result,request);
	   },

	   failure: fail,
	   timeout: fail,

	   params:{
		entity: 'IPAM.DhcpScope',
		verb: 'AckAll'
	   }
	   });
  };

  var btnsrc = that;

  Ext.Msg.show({
	   title:'Clear Confirmation',
	   msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_252;E=js}',
	   buttons: Ext.Msg.YESNO,
	   animEl: btnsrc.id,
	   icon: Ext.MessageBox.QUESTION,
	   fn: function(btn){ if( btn == 'yes' ) ClearFn(); }
	   });
	
  return false;
};
$SW.IPAM.ColumnRendererNoYes = function(value){ return (value && value.toUpperCase() == 'TRUE') ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}'; };
$SW.OfferDelayRenderer = function(value){ return value ? String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_128;E=js}', value) : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_118;E=js}'; };
$SW.ColumnRendererYesNo = function(value){ return value == 'True' ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}'; };
	</IPAMmaster:JsBlock>
	<IPAMmaster:JsBlock runat="server" Orientation="jsPostInit">
$SW.IPAM.ExpandNode = function(that)   
{
      $SW.Env["PageLoadBehav"] = {id:that,ViewAll:"True"}; 
      var tree = window.tab;
      that == 0 ?  tree.getRootNode().reload():tree.getNodeById(that).reload();         
      return true;
};
$SW.IPAM.ShrinkNode = function(that)   
{
      $SW.Env["PageLoadBehav"] = {id:that,ViewAll:"False"}; 
      var tree = window.tab;
      that == 0 ?  tree.getRootNode().reload():tree.getNodeById(that).reload();    
      return true;
};
$SW.IPAM.SelectDNSServer = function(s) {
        $SW.IPAM.SelectedDNSServer = s;
        return true;
    };
$SW.IPAM.DhcpOpt = $SW.IPAM.GroupBy.MakeOptions($nsId, 'Dhcp',
		{scopes: 'tabScopes',servers:'tabScopes'});
$SW.IPAM.DnsOpt  = $SW.IPAM.GroupBy.MakeOptions($nsId, 'Dns', 
	{zones:  'tabZones', servers: 'tabDnsServers'});
$SW.IPAM.GetActiveOpt = function(){
	var isDhcp = !Ext.getCmp('DhcpGroupBySelect').hidden;
	return (isDhcp) ? $SW.IPAM.DhcpOpt : $SW.IPAM.DnsOpt;
};
$SW.IPAM.SetCboxGroupByVisibility = function(isDhcp){
	var invalidateGridData = function(tabs){
		Ext.iterate(tabs, function(seq, tab){
			var t = Ext.getCmp(tab);
			if(t) t.dirty = true; 
		});
	};
	return (isDhcp) ? function(){
		if($SW.IPAM.DhcpOpt.cbox.hidden){
			$SW.IPAM.DhcpOpt.cbox.show();
			$SW.IPAM.DnsOpt.cbox.hide();
			invalidateGridData($SW.IPAM.DnsOpt.tabs);

			$SW.IPAM.DhcpOpt.cbox.forceReSelect(true);
		}
	} : function(){
		if($SW.IPAM.DnsOpt.cbox.hidden){
			$SW.IPAM.DnsOpt.cbox.show();
			$SW.IPAM.DhcpOpt.cbox.hide();
			invalidateGridData($SW.IPAM.DhcpOpt.tabs);

			$SW.IPAM.DnsOpt.cbox.forceReSelect(true);
		}
	};
};

$SW.IPAM.manager = $SW.IPAM.DhcpDnsManage($SW.IPAM.DhcpOpt, $SW.IPAM.DnsOpt);

$(document).ready(function(){ 
	$SW.IPAM.GroupBy.Init($SW.IPAM.DhcpOpt);
	$SW.IPAM.GroupBy.Init($SW.IPAM.DnsOpt);
    var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);
    $SW.IPAM.SetExistingScopeBtnState();
});
	</IPAMmaster:JsBlock>
	<IPAMmaster:CssBlock runat="server" Requires="ux-all.css,ux-all-plugins.css,sw-account-roles.css">
body #content { padding-bottom: 0px; }
body #footer { position: static; border: none; }
.warningbox a { text-decoration: underline !important; }
.warningbox .inner { background:transparent url(/orion/ipam/res/images/sw/icon.lightbulb.gif) no-repeat scroll left top; }
* html .warningbox .inner { height: 32px; overflow: visible; }

#formbox a { color: #336699; text-decoration: underline; }
#formbox a:hover { color: orange; }
.rptlegend .rptcount { width: 64px; text-align: right; padding: 4px 0px 4px 0px; }
.rptlegend .rptlabel { padding: 4px 0px 4px 30px; background-position: left; }
.rptlegend .rptcount span, .rptlegend .rptlabel div { padding-right: 4px; }
.rptlegend .rptcount span { padding-left: 6px; }
.rptlegend .rptsum .rptcount { padding-top: 7px; }
.rptlegend .rptsum .rptlabel { padding-left: 0px; }
.rptlegend .rptsum span, .rptlegend .rptsum div { border-top: 3px double black; }
.ext-indent {padding: 0px 10px !important; background:none repeat scroll 0 0 #FAF9F5 !important; }
.x-treegrid-col {vertical-align: middle;}
.x-tree-node a, .x-tree-node a:active, .x-tree-node a:visited {color: #000000 !important; }
.x-tree-node a:hover {text-decoration: underline !important;}

.x-tree-node-collapsed .x-tree-node-icon, .x-tree-node-expanded .x-tree-node-icon, .x-tree-node-leaf .x-tree-node-icon {width: 27px;}
	</IPAMmaster:CssBlock>

	<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>
    
    <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
        <div id="isPowerUser" style="display:none;"></div>
	<%}%>

	<div id="warningbox" class="warningbox" runat="server">
		<div class="inner">
			<table border="0">
				<tr>
					<td>
						<div id="scopeLink" runat="server">
							<asp:Literal ID="warningMessage" runat="server" EnableViewState="false" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_444 %>"/>&#187; <a href="Admin/Admin.DhcpScopeOrphans.aspx">
									<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_445 %></a>
						</div>
					</td>
					<td>
						<div style="margin-left: 14px; vertical-align: middle;">
							<orion:LocalizableButtonLink runat="server" OnClick="return $SW.ConfirmClearNotice(this);" DisplayType="Small" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_448 %>"/>
						</div>
					</td>
				</tr>                
			</table>
		</div>
	</div>
    <div id="newUserInterfaceEnabled" class="warningbox" runat="server">
        <div class="inner">
            <table border="0">
                <tr>
                    <td>
                        <div id="newUserInterfacePageLink" runat="server">
                            <asp:Literal ID="newUserInterfaceLink" runat="server" EnableViewState="false" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_NEW_UI_INFO_MESSAGE %>"/>&#187; <a href="/apps/ipam/dhcp-dns/dhcp">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_NEW_UI_LINK_MESSAGE %></a>
                        </div>
                        <div id="newUserInterfaceLegacyBrowserMessage" runat="server" visible="false">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_NEW_UI_LEGACY_BROWSER_MESSAGE %>
                        </div>
                    </td>
                </tr>                
            </table>
        </div>
    </div>
	<table width="auto" id="sw-navhdr" style="table-layout: fixed; height: 55px;" cellpadding="0"
		cellspacing="0">
		<tr>
			<td>
				<div style="padding-left: 10px; width: auto !important">
					<h1>
						<%=Page.Title%></h1>
				</div>
			</td>
		</tr>
	</table>
	<IPAMlayout:DialogWindow jsID="deleteConfirm" helpID="deletehelp" Name="deleteConfirm"
		Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_218 %>" height="400"
		width="580" runat="server">
		<Buttons>
			<IPAMui:ToolStripButton ID="ToolStripButton1" text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_219 %>" cls="sw-enableonload"
				runat="server">
				<handler>function(){ $SW.msgq.DOWNSTREAM( $SW['deleteConfirm'], 'dialog', 'delete' ); }</handler>
			</IPAMui:ToolStripButton>
			<IPAMui:ToolStripButton ID="ToolStripButton2" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
				<handler>function(){ $SW['deleteConfirm'].hide(); }</handler>
			</IPAMui:ToolStripButton>
		</Buttons>
		<Msgs>
			<IPAMmaster:WindowMsg Name="ready" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['deleteConfirm'] ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="unload" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['deleteConfirm'], true ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="license" runat="server">
				<handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="close" runat="server">
				<handler>function(n,o){
		 $SW['deleteConfirm'].hide();
		 $SW.IPAM.manager.DhcpScope.DeleteCurrentDone();
	 }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="confirmed" runat="server">
				<handler>function(n,o,info){ 
			$SW.IPAM.manager.DeleteComplete(info); 
		}</handler>
			</IPAMmaster:WindowMsg>
		</Msgs>
	</IPAMlayout:DialogWindow>
	<IPAMlayout:DialogWindow jsID="mainWindow" helpID="IPAMEdit" Name="mainWindow"
		Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_449 %>" height="600"
		width="650" runat="server">
		<Buttons>
			<IPAMui:ToolStripButton ID="mainWindowSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
				<handler>function(){ 
					if($("#isDemoMode").length > 0) { demoAction('IPAM_Server_Edit', null); return false; }
					$SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
			</IPAMui:ToolStripButton>
			<IPAMui:ToolStripButton ID="ToolStripButton3" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
				<handler>function(){ $SW['mainWindow'].hide(); }</handler>
			</IPAMui:ToolStripButton>
		</Buttons>
		<Msgs>
			<IPAMmaster:WindowMsg Name="ready" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="unload" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'], true ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="close" runat="server">
				<handler>function(n,o){ 
	$SW['mainWindow'].hide(); 
	var mgr = $SW.IPAM.manager.ActiveTabManager();
	if(mgr && mgr.RefreshGridPage) mgr.RefreshGridPage();
}</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="license" runat="server">
				<handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="refresh" runat="server">
				<handler>function(n,o,info){ window.location = 'Dhcp.Management.aspx?opento=' + info['id']; }</handler>
			</IPAMmaster:WindowMsg>
		</Msgs>
	</IPAMlayout:DialogWindow>
	<IPAMlayout:DialogWindow jsID="scopeEditWindow" helpID="scopeedit" Name="scopeEditWindow"
		Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_450 %>" height="400"
		width="480" runat="server">
		<Buttons>
			<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
				<handler>function(){ $SW.msgq.DOWNSTREAM( $SW['scopeEditWindow'], 'dialog', 'save' ); }</handler>
			</IPAMui:ToolStripButton>
			<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
				<handler>function(){ $SW['scopeEditWindow'].hide(); }</handler>
			</IPAMui:ToolStripButton>
		</Buttons>
		<Msgs>
			<IPAMmaster:WindowMsg Name="ready" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['scopeEditWindow'] ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="unload" runat="server">
				<handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['scopeEditWindow'], true ); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="close" runat="server">
				<handler>function(n,o){ $SW['scopeEditWindow'].hide(); $SW.IPAM.manager.DhcpScope.RefreshGridPage(); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="license" runat="server">
				<handler>function(n,o,info){ $SW.LicenseListeners.recv(info); }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="refresh" runat="server">
				<handler>function(n,o,info){ window.location = 'Dhcp.Management.aspx'; }</handler>
			</IPAMmaster:WindowMsg>
			<IPAMmaster:WindowMsg Name="addips" runat="server">
				<handler>function(n,o,info){ $SW.subnet.addiprangecomplete(info); }</handler>
			</IPAMmaster:WindowMsg>
		</Msgs>
	</IPAMlayout:DialogWindow>
	<IPAMlayout:DialogWindow jsID="scopeAddressLeasesWindow" helpID="scopeaddressleases"
		Name="scopeAddressLeasesWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_451 %>"
		height="400" width="680" runat="server">
		<Buttons>
			<IPAMui:ToolStripButton ID="ToolStripButton7" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_301 %>" runat="server">
				<handler>function(){ $SW['scopeAddressLeasesWindow'].hide(); }</handler>
			</IPAMui:ToolStripButton>
		</Buttons>
	</IPAMlayout:DialogWindow>
	<IPAMlayout:DialogWindow jsID="dnsRecordsWindow" helpID="dnsrecordswindow"
		Name="dnsRecordsWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_291 %>"
		height="400" width="600" runat="server">
		<Buttons>
			<IPAMui:ToolStripButton ID="ToolStripButton6" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_301 %>" runat="server">
				<handler>function(){ $SW['dnsRecordsWindow'].hide(); }</handler>
			</IPAMui:ToolStripButton>
		</Buttons>
	</IPAMlayout:DialogWindow>

	<IPAMlayout:ViewPort ID="viewport" runat="server">
  <IPAMlayout:BorderLayout id="mainlayout" runat="server">
  <North>
	<IPAMlayout:BoxComponent id="headerlayout" layout="fit" runat="server" borderVisible="false" clientEl="content" />
  </North>
  <Center>
	<IPAMlayout:BorderLayout cssClass="ext-indent" runat="server" borderVisible="false">
	  <West>
		<IPAMlayout:BorderLayout id="groupbylayout" runat="server" borderVisible="false" width="200" split="true" >
		<North>
		  <IPAMlayout:BoxComponent runat="server" borderVisible="true" clientEl="GroupByCell" listeners="{resize:$SW.IPAM.DhcpSplitterResized}" />
		</North>
		<Center>
		  <IPAMlayout:GroupByList id="GroupByList" runat="server" borderVisible="true" autoScroll="true"
		  listeners="$SW.IPAM.GroupBy.GetGroupByListListeners($SW.IPAM.GetActiveOpt)" />
		</Center>
		</IPAMlayout:BorderLayout>
	  </West>
	  <Center>
		<IPAMlayout:BorderLayout runat="server" borderVisible="false"  split="true">
			<Center>
				<IPAMlayout:TabLayout id="ipam_tabs" runat="server" cssClass="sw-tabpanel-top" tabPosition="top"
				 activeTab="tabScopes" borderVisible="false"  listeners="{ beforetabchange: function (me, nt,ct) {
if (nt.dirty) { nt.dirty = false; window.setTimeout(function () { 
                    if(ct.id =='tabZones' && nt.id == 'tabDnsServers' )
                    {
                     $SW.IPAM.tabSwitch = false;
                    }
                    else if(ct.id =='tabDnsServers' && nt.id == 'tabZones' )
                    {
                     $SW.IPAM.tabSwitch = false;
                    }
                    else
                    {
                     $SW.IPAM.tabSwitch = true;
                    }
                     if (nt.id =='tabScopes'){nt.getRootNode().reload();} else{ nt.store.load();} }, 15); } }}">
				   <IPAMlayout:TreeGrid id="tabScopes" runat="server" title="DHCP" borderVisible="true"
                                        autoScroll="false" EnableDragAndDrop="false" width="240" MultipleSelection="True"
                                        EnableSort="True" listeners="$SW.IPAM.treelisteners()" Plugins="
                        new Ext.ux.plugins.TreeGridState(),
                        new Ext.ux.plugins.TreeGridMultiSelectDragDrop(),
                        new $SW.IPAM.ChartToolTip({}),
                       new $SW.IPAM.FailoverChartToolTip({})">
			<ConfigOptions runat="server">
            <Options>
                <IPAMlayout:ConfigOption runat="server" Name="checkboxes" Value="true" IsRaw="true" />
                <IPAMlayout:ConfigOption runat="server" Name="animate" Value="true" IsRaw="true" />
            </Options>
        </ConfigOptions>
				<Store ID="TestBoxStore" runat="server" proxyEntity="IPAM.DHCPView" limit="5" proxyUrl="ExtTreeProvider.ashx" ambientSort="FriendlyName"
					   JoinClause=" LEFT JOIN Orion.Nodes Nodes ON {0}.NodeId=Nodes.NodeId"
				       listeners="$SW.IPAM.GroupBy.GetTabStoreListeners($SW.IPAM.DhcpOpt, $SW.IPAM.DhcpOpt.tabs.scopes, $SW.IPAM.SetCboxGroupByVisibility(true))">
			<fnRemap>
			function(node,rows){          
            var qs = $SW.Env["PageLoadBehav"];
            var expID=0;
            var ViewAll=false;
            if(typeof (qs) != "undefined")
              {
                if(qs.id !=null)
                {
                    expID = qs.id;
                }
                if(qs.ViewAll !=null)
                {
                    ViewAll = qs.ViewAll;
                }
              }
                Ext.each(rows, function(row, i) {
                row.id = ''+row.GroupId;
			    row.leaf = (row.GroupType == 64);                
                row.expanded = ((expID == row.GroupId));
                row.iconCls = row.GroupIconPrefix+'-'+row.StatusIconPostfix;
                });
             var q = node.getOwnerTree().getLoader().query;
                if($SW.IPAM.isExpand)
                {
                  node.getOwnerTree().expandAll();
                }

             var parentId = 0;
            if(rows.length > 0)
            {
                if(rows[0].ParentId !=null)
                {
                    if(rows[0].ParentId == "")
                    {
                     parentId =0;
                    }
                    else if(rows[0].ViewType == 3 && rows[0].ViewInnerType == 3 && node.attributes.ServerType == 4)
                    {
                     parentId =rows[0].SharedNetworkId;
                    }
                    else
                    {
                     parentId =rows[0].ParentId;
                    }
                }
             }
          
            var limitText="@{R=IPAM.Strings;K=IPAMWEBJS_MR_08;E=js}";
            var clickFn ="return $SW.IPAM.ExpandNode("+parentId+");";
            if(parentId == 0)
            {
                if(ViewAll == "True")
                {
                 clickFn ="return $SW.IPAM.ShrinkNode("+parentId+");";             
                 limitText="@{R=IPAM.Strings;K=IPAMWEBJS_MR_11;E=js}";
                }
                else 
                {
                 limitText="@{R=IPAM.Strings;K=IPAMWEBJS_MR_10;E=js}";
                }                
            }                      
            else if(parentId == expID)
            {
                if(ViewAll == "True")
                {
                 clickFn ="return $SW.IPAM.ShrinkNode("+parentId+");";              
                 limitText="@{R=IPAM.Strings;K=IPAMWEBJS_MR_09;E=js}";
                }                
            }
             
             var TreeMaxItems = $SW.Env["TreeMaxItems"];
			 if (rows.length >= TreeMaxItems){
                var text = String.format("<a href='#' onclick='{0}'>{1} </a>",clickFn,limitText);   
			   rows.push({id:-1, leaf: true, FriendlyName: text, iconCls: "tree-limit", qtip: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_33;E=js}" });
			 };
			 return rows;
			}
			</fnRemap>
			<fnQueryBuilder>
			function(node,loader){
			var w_root = "{0}.ViewType = 1";
            var query;
            var bparams;
            var postRequest = [];
            var columns;
            var applyJoins;
            var isScope = loader.baseParams.isScope;
            var IsCustomScope = loader.baseParams.isCustomScope;
            var qs = $SW.Env["PageLoadBehav"];
            var ViewAll=(typeof (qs) == "undefined")?"False":
                (qs.id ==  node.attributes.id && qs.ViewAll == "True") ? "True":
                (node.attributes.id ==  'Hierarchy' && qs.ViewAll == "True")?"True":"False";
			var customPropPrefix = "CustomProperties";
            Ext.each(loader.fields, function(field, i) {
                    var len = ((field.name).length);
                    if (i == 0) {
                        if (node.isRoot && IsCustomScope && field.name == "GroupId")
                            columns = "Distinct A." + field.name;
                        else
                            columns = field.name;
                    } else {
                        if (field.name.length >= customPropPrefix.length && field.name.substring(0, customPropPrefix.length) == customPropPrefix)
                        {
                            columns = columns + "," + "A." + field.name + " as \"" + (field.name).substring(customPropPrefix.length+1, len) + "\"";
                        }
                        else
                            columns = columns + "," + field.name;
                    }
                });

            if (loader.baseParams.filter_0_field != null) {
                if (node.isRoot) {
                    query = w_root;
                    bparams = loader.baseParams;
                    applyJoins = true;  
                } else if (node.attributes.ViewType == 1 && node.attributes.ViewInnerType == 0) {
                   <%-- Display Scopes for othres--%>
                    if (node.attributes.ServerType == "4") {
                        query = "{0}.ViewType = 2 and {0}.ParentId = " + node.attributes.id;
                    } else {
                        query = "{0}.ViewType = 3 and {0}.ParentId = " + node.attributes.id;
                    }
                } else if (node.attributes.ViewType == 2 && node.attributes.ViewInnerType == 1) {
                      <%-- Display Scopes & Shared Networks Under Group--%>
                    query = "{0}.ViewType = 4 and {0}.DhcpGroupId = " + node.attributes.id;
                } else if (node.attributes.ViewType == 2 && node.attributes.ViewInnerType == 2) {
                       <%-- Display Scopes Under Shared Network--%>
                    query = "{0}.ViewType = 3 and {0}.SharedNetworkId = " + node.attributes.id;
                } else if (node.attributes.ViewType == 2) {
                     <%-- Display Scopes from Others excluding Shared Network and scopes--%> 
                    query = "{0}.ViewType = 3 and {0}.ParentId = " + node.attributes.id;
                } else if (node.attributes.ViewType == 4 && node.attributes.ViewInnerType == 2) {
                      <%-- Display Scopes Under Shared Network--%>
                    query = "{0}.ViewType = 3 and {0}.SharedNetworkId = " + node.attributes.id;
                } 
                if(isScope || IsCustomScope)
                {
                  bparams = loader.baseParams;
                }
                postRequest.push({ baseParams: bparams, entity: loader.query.entity, col: columns, identity: loader.query.identity, limit: loader.query.limit, whereClause: query ,isScope:isScope,isRoot:node.isRoot,sortDir: loader.baseParams.sortDir,sortCol:loader.baseParams.sortCol,ViewAll:ViewAll,isCustomScope:IsCustomScope,applyJoin:applyJoins, join:loader.query.join});
            } else if (node.isRoot) {
                postRequest.push({ baseParams: bparams, entity: loader.query.entity, col: columns, identity: loader.query.identity, limit: loader.query.limit, whereClause: w_root, isScope:isScope,isRoot:node.isRoot,sortDir: loader.baseParams.sortDir,sortCol:loader.baseParams.sortCol,ViewAll:ViewAll,isCustomScope:IsCustomScope,applyJoin:applyJoins, join:loader.query.join});
            }
            return postRequest;
			}
			</fnQueryBuilder>
			<Fields>
				<IPAMlayout:TreeStoreField dataIndex="GroupId" isKey="true" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="ServerType" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="ServerAddress" runat="server" />   
				<IPAMlayout:TreeStoreField dataIndex="CIDR" dataType="int" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="Location" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="VLAN" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="PercentUsed" dataType="float" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="UsedCount" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="AvailableCount" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="TotalCount" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="FriendlyName" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="GroupType" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ViewType" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ScopeIPRanges" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="GroupIconPrefix" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="StatusIconPostfix" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ViewInnerType" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="DhcpGroupId" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="SharedNetworkId" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="AllocSize" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="SubnetID" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="Address" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="PoolData" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="LastDiscovery" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="AddressMask" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="DisabledAtServer" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ScopeId" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="StatusShortDescription" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="StatusName" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="AddressN" runat="server" />
                <IPAMlayout:TreeStoreField dataIndex="ParentId" runat="server" />
			    <IPAMlayout:TreeStoreField dataIndex="RelationshipName" runat="server" />
				<IPAMlayout:TreeStoreField dataIndex="Nodes.MachineType" runat="server" />
			    <IPAMlayout:TreeStoreField dataIndex="FailoverExists" runat="server" />
			    <IPAMlayout:TreeStoreField dataIndex="Mode" runat="server" /> 
			    <IPAMlayout:TreeStoreField dataIndex="ClusterId" runat="server" /> 
			</Fields>
			</Store>
			<Columns>
            <IPAMlayout:TreeColumn width="200" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_4 %>" dataIndex="FriendlyName" template="$SW.IPAM.FriendlyNameTemplate()" Visible="true" HideInGrid="False" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_17 %>" DataIndex="ServerType" template="$SW.IPAM.DhcpServerTypeTemplate()" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="60" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_46 %>" DataIndex="Failover" template="$SW.IPAM.FailoverTypeTemplate()" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_6 %>" DataIndex="ServerAddress" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_7 %>" cls="dhcp-scope-chart" template="$SW.IPAM.ChartTemplate()" DataIndex="ScopeIPRanges" HideInGrid="False" Visible="true" runat="server" Sortable="false"  />
            <IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_19 %>" DataIndex="Address" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_26 %>" DataIndex="CIDR" HideInGrid="false" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_242 %>" DataIndex="AddressMask" HideInGrid="false" Visible="true" runat="server" />
            <IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_244 %>" template="$SW.IPAM.DisabledAtServer()" DataIndex="DisabledAtServer" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_159 %>" DataIndex="Location" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_18 %>" DataIndex="VLAN" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_10 %>" DataIndex="PercentUsed" template="$SW.IPAM.DhcpIpUsageTemplate(2)" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_11 %>" DataIndex="TotalCount" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_12 %>" DataIndex="UsedCount" HideInGrid="false" Visible="true" runat="server" />
			<IPAMlayout:TreeColumn width="120" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_13 %>" DataIndex="AvailableCount" HideInGrid="false" Visible="true" runat="server" />
			</Columns>
			<TopMenu borderVisible="false">
					<IPAMui:ToolStripMenu text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_85 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_222 %>" RoleAclOneOf="IPAM.PowerUser"  ExceptDemoMode="true" iconCls="mnu-add" runat="server">
						<IPAMui:ToolStripMenuItem id="ToolStripMenuItem2" RoleAclOneOf="IPAM.PowerUser"  ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_243 %>" runat="server">
							<handler>$SW.IPAM.manager.DhcpTab.Add</handler>
						</IPAMui:ToolStripMenuItem>

						<IPAMui:ToolStripMenuItem RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"   iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_466 %>" runat="server">
							<handler>$SW.IPAM.manager.DhcpTab.AddScope</handler>
						</IPAMui:ToolStripMenuItem>

						<IPAMui:ToolStripMenuItem id="btnAddExistingScope" RoleAclOneOf="IPAM.PowerUser"  iconCls="mnu-add" disabled="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_453 %>" runat="server">
							<handler>function(that){ window.location = '/orion/ipam/admin/admin.dhcpscopeorphans.aspx'; }</handler>
						</IPAMui:ToolStripMenuItem>
					</IPAMui:ToolStripMenu>
				
					<IPAMui:ToolStripSeparator runat="server" />

					  <IPAMui:ToolStripMenu text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_866 %>"  RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-splitScope" runat="server">
					    <IPAMui:ToolStripButton id="btnDHCPSplitScope" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_454 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-splitScope" disabled="true" runat="server">
						    <handler>$SW.IPAM.manager.DhcpTab.Split</handler>
					    </IPAMui:ToolStripButton>
                        <IPAMui:ToolStripButton id="btnDHCPReplicateScope" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_850 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-failover" disabled="true" runat="server">
						    <handler>$SW.IPAM.manager.DhcpTab.ReplicateScope</handler>
					    </IPAMui:ToolStripButton>
                        <IPAMui:ToolStripButton id="btnDHCPReplicateRelationship" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_6 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-failover" disabled="true" runat="server">
						    <handler>$SW.IPAM.manager.DhcpTab.ReplicateRelationship</handler>
					    </IPAMui:ToolStripButton>
                        <IPAMui:ToolStripButton id="btnDHCPDeconfigureFailover" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_YV_2 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-failover-deconfigure" disabled="true" runat="server">
						    <handler>$SW.IPAM.manager.DhcpTab.DeconfigureFailover</handler>
					    </IPAMui:ToolStripButton>
                    </IPAMui:ToolStripMenu>

					<IPAMui:ToolStripSeparator runat="server" />

					<IPAMui:ToolStripButton id="btnDHCPEdit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_70 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-edit" runat="server" >
						<handler>$SW.IPAM.manager.DhcpTab.Edit</handler>
					</IPAMui:ToolStripButton>
                
                        <IPAMui:ToolStripSeparator runat="server" />
				
						<IPAMui:ToolStripButton id="btnDHCPScan" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_468 %>" RoleAclOneOf="IPAM.PowerUser"  toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_469 %>" iconCls="mnu-scan" runat="server" disabled="true">
							<handler>$SW.IPAM.manager.DhcpTab.Scan</handler>
						</IPAMui:ToolStripButton>
                    
                        <IPAMui:ToolStripSeparator runat="server" />

						<IPAMui:ToolStripButton id="btnViewDetails" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_257 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_470 %>" iconCls="mnu-details" runat="server" disabled="true">
							<handler>$SW.IPAM.manager.DhcpTab.DetailView</handler>
						</IPAMui:ToolStripButton>

					<IPAMui:ToolStripSeparator runat="server" />

					<IPAMui:ToolStripButton id="btnDHCPGraph" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_456 %>" RoleAclOneOf="IPAM.ReadOnly" toolTip="" iconCls="mnu-graph" runat="server">
						<handler>$SW.IPAM.DHCPGraphView</handler>
					</IPAMui:ToolStripButton>

					<IPAMui:ToolStripSeparator runat="server" />

					<IPAMui:ToolStripButton id="btnDHCPAdrsLeases" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_451 %>" RoleAclOneOf="IPAM.ReadOnly" toolTip="" iconCls="mnu-details" runat="server" disabled="true">
						<handler>$SW.IPAM.manager.DhcpTab.ViewAddressLeases</handler>
					</IPAMui:ToolStripButton>

					<IPAMui:ToolStripSeparator runat="server" />

					<IPAMui:ToolStripButton id="btnDHCPDelete" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_16 %>" RoleAclOneOf="IPAM.PowerUser"  ExceptDemoMode="true" toolTip="" iconCls="mnu-delete" runat="server">
						<handler>$SW.IPAM.manager.DhcpTab.Delete</handler>
					</IPAMui:ToolStripButton>
				</TopMenu>
				 </IPAMlayout:TreeGrid>				 

				<IPAMlayout:GridBox
					runat="server"
					id="tabZones"
					title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_474 %>"
					UseLoadMask="True"
					autoHeight="false"
					emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_475 %>"
					borderVisible="true"
					deferEmptyText="false" 
					listeners="$SW.IPAM.DnsZoneGridListeners($SW.IPAM.DnsOpt)">
										 
				 <TopMenu borderVisible="false">

				<IPAMui:ToolStripMenu text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_85 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_222 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" runat="server">
					<IPAMui:ToolStripMenuItem  RoleAclOneOf="IPAM.PowerUser" iconCls="mnu-add"  ExceptDemoMode="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_252 %>" runat="server" >
						<handler>$SW.IPAM.manager.DnsServer.Add</handler>
					</IPAMui:ToolStripMenuItem>
					<IPAMui:ToolStripMenuItem RoleAclOneOf="IPAM.PowerUser" iconCls="mnu-add" ExceptDemoMode="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_284 %>" runat="server" >
						<handler>$SW.IPAM.manager.DnsZone.Add</handler>
					</IPAMui:ToolStripMenuItem>
					<IPAMui:ToolStripMenuItem ID="btnAddExistingZone2" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" disabled="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_476 %>" runat="server">
						<handler>function(that){ window.location = '/orion/ipam/admin/admin.dnszoneorphans.aspx'; }</handler>
					</IPAMui:ToolStripMenuItem>
				   
				</IPAMui:ToolStripMenu>

				<IPAMui:ToolStripSeparator runat="server" />

				<IPAMui:ToolStripButton id="btnDNSEdit" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_477 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip=""  iconCls="mnu-edit" runat="server">
					<handler>$SW.IPAM.manager.DnsZone.Edit</handler>
				</IPAMui:ToolStripButton>

				 <IPAMui:ToolStripSeparator runat="server" />

				<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_291 %>" RoleAclOneOf="IPAM.ReadOnly" toolTip="" iconCls="mnu-details" runat="server">
					<handler>$SW.IPAM.manager.DnsZone.ViewDnsRecords</handler>
				</IPAMui:ToolStripButton>
								
				 <IPAMui:ToolStripSeparator runat="server" />
		   
				<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_468 %>" RoleAclOneOf="IPAM.PowerUser"  toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SV1_1 %>" iconCls="mnu-scan"   runat="server">
					<handler>$SW.IPAM.manager.DnsZone.Scan</handler>
				 </IPAMui:ToolStripButton>
				
				 <IPAMui:ToolStripSeparator runat="server" />
				
				<IPAMui:ToolStripButton id="btnDNSDelete"  text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_478 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-delete" runat="server">
					<handler>$SW.IPAM.manager.DnsZone.Delete</handler>
				</IPAMui:ToolStripButton>

				</TopMenu>
				<PageBar pageSize="100" displayInfo="true" />
				<Columns>
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>" width="140" isSortable="true" dataIndex="FriendlyName" renderer="$SW.IPAM.DnsZoneRenderer()"  runat="server" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VV0_5 %>" width="140" isSortable="true" dataIndex="DnsViewName" runat="server" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_479 %>" width="120" isSortable="true" dataIndex="StatusName" runat="server"  renderer="function (value, meta, r) {return r.data.StatusShortDescription;}" /> 
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_253 %>" width="120" isSortable="true" dataIndex="ZoneType" renderer="$SW.IPAM.DnsZoneTypeRenderer()" runat="server"   />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_480 %>" width="120" isSortable="true" dataIndex="LookUpType" renderer="$SW.IPAM.DnsLookupTypeRenderer()" runat="server"   />                  
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_481 %>" width="140" isSortable="true" dataIndex="ServerName"   runat="server" renderer="$SW.IPAM.DnsServerNameRenderer()" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_460 %>" width="90" isSortable="true" dataIndex="ServerType" renderer="$SW.IPAM.DnsServerTypeRenderer()" runat="server" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_269 %>" width="100" isSortable="true" dataIndex="ServerAddress" runat="server" /> 
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_482 %>" width="100" isSortable="true" dataIndex="IncrementalZoneTransfer" runat="server" renderer="function(value){return $SW.ColumnRendererYesNo(value);}" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_483 %>" width="180" isSortable="true" dataIndex="LastDiscovery" renderer="$SW.ext.DateRenderer($SW.ExtLocale.DateFull)" runat="server" />
					<IPAMlayout:GridColumn  title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_484 %>" width="140" isSortable="true" dataIndex="DynamicUpdate" runat="server" renderer="$SW.IPAM.DynamicUpdateRenderer()" />  
				   </Columns>
					 <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="AddressN, FriendlyName"
						 JoinClause=" LEFT JOIN IPAM.DnsZone zone ON {0}.DnsZoneId = zone.DnsZoneId LEFT JOIN IPAM.DnsView view ON zone.DnsViewId = view.DnsViewId"
						 listeners="$SW.IPAM.GroupBy.GetTabStoreListeners($SW.IPAM.DnsOpt, $SW.IPAM.DnsOpt.tabs.zones, $SW.IPAM.SetCboxGroupByVisibility(false) )" runat="server">                  
			   
					<Fields>
						<IPAMlayout:GridStoreField dataIndex="GroupId" isKey="true" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="FriendlyName" runat="server" />
						<IPAMlayout:GridStoreField clause ="view.Name As DnsViewName" dataIndex="DnsViewName" runat="server" />
						<IPAMlayout:GridStoreField clause="(A.Parent.FriendlyName) as ServerName" dataIndex="ServerName" runat="server" />    
						<IPAMlayout:GridStoreField clause="(A.Parent.StatusIconPostfix) as ServerIconPostfix" dataIndex="ServerIconPostfix" runat="server" />
						<IPAMlayout:GridStoreField clause="(A.Parent.GroupIconPrefix) as ServerIconPrefix" dataIndex="ServerIconPrefix" runat="server" />
						<IPAMlayout:GridStoreField clause="(A.Parent.StatusShortDescription) as ServerStatusShortDescription" dataIndex="ServerStatusShortDescription" runat="server" />                    
						<IPAMlayout:GridStoreField dataIndex="StatusName" runat="server" />
						<IPAMlayout:GridStoreField clause="(A.Parent.Address) as ServerAddress" dataIndex="ServerAddress" runat="server" /> 
						<IPAMlayout:GridStoreField dataIndex="ZoneTransfer" runat="server" /> 
						<IPAMlayout:GridStoreField dataIndex="StatusIconPostfix" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="StatusShortDescription" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="GroupTypeText" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="GroupIconPrefix" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="DnsZoneId" runat="server" />
						<IPAMlayout:GridStoreField clause="zone.IncrementalZoneTransfer AS IncrementalZoneTransfer" dataIndex="IncrementalZoneTransfer" runat="server" />
						<IPAMlayout:GridStoreField clause="zone.ZoneType as ZoneType" dataIndex="ZoneType" runat="server" />
						<IPAMlayout:GridStoreField clause="zone.LookUpType as LookUpType" dataIndex="LookUpType" runat="server" />
						<IPAMlayout:GridStoreField clause="zone.DynamicUpdate as DynamicUpdate" dataIndex="DynamicUpdate" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="LastDiscovery" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="ServerType" runat="server" />
					 </Fields>
				  </Store>
				 
					</IPAMlayout:GridBox>

				<IPAMlayout:GridBox
					runat="server"
					id="tabDnsServers"
					title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_485 %>"
					UseLoadMask="True"
					autoHeight="false"
					emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_486 %>"
					borderVisible="true"
					deferEmptyText="false"
					listeners="$SW.IPAM.DnsServerGridListeners($SW.IPAM.DnsOpt)">

				<TopMenu borderVisible="false">

				<IPAMui:ToolStripMenu text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_85 %>" toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_222 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" runat="server">
					<IPAMui:ToolStripMenuItem RoleAclOneOf="IPAM.PowerUser" iconCls="mnu-add" ExceptDemoMode="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_252 %>" runat="server">
						<handler>$SW.IPAM.manager.DnsServer.Add</handler>
					</IPAMui:ToolStripMenuItem>
					<IPAMui:ToolStripMenuItem ID="ToolStripMenuItem1" RoleAclOneOf="IPAM.PowerUser" iconCls="mnu-add" ExceptDemoMode="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_284 %>" runat="server">
						<handler>$SW.IPAM.manager.DnsServer.AddZone</handler>
					</IPAMui:ToolStripMenuItem> 
					<IPAMui:ToolStripMenuItem id="btnAddExistingZone" RoleAclOneOf="IPAM.PowerUser" iconCls="mnu-add" ExceptDemoMode="true" disabled="true" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_476 %>" runat="server">
						<handler>function(that){ window.location = '/orion/ipam/admin/admin.dnszoneorphans.aspx'; }</handler>
					</IPAMui:ToolStripMenuItem>
				</IPAMui:ToolStripMenu>

				<IPAMui:ToolStripSeparator runat="server" />

				<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_192 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-edit" runat="server">
					<handler>$SW.IPAM.manager.DnsServer.Edit</handler>
				</IPAMui:ToolStripButton>

				<IPAMui:ToolStripSeparator runat="server" />
				
				<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_267 %>" RoleAclOneOf="IPAM.PowerUser"  toolTip="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_487 %>" iconCls="mnu-scan" runat="server">
					<handler>$SW.IPAM.manager.DnsServer.Scan</handler>
				</IPAMui:ToolStripButton> 
				
				 <IPAMui:ToolStripSeparator runat="server" />
				
				<IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_488 %>" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" toolTip="" iconCls="mnu-delete" runat="server">
					<handler>$SW.IPAM.manager.DnsServer.Delete</handler>
				</IPAMui:ToolStripButton>

				</TopMenu>
				<PageBar pageSize="100" displayInfo="true" />
				<Columns>
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_252 %>" width="140" isSortable="true" dataIndex="FriendlyName" renderer="$SW.IPAM.DnsServerRenderer()" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_460 %>" width="90" isSortable="true" dataIndex="A.DnsServer.ServerType" renderer="$SW.IPAM.DnsServerTypeRenderer()" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_269 %>" width="100" isSortable="true" dataIndex="Address" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_482 %>" width="100" isSortable="true" dataIndex="ZoneTransfer" runat="server" renderer="function(value){return $SW.ColumnRendererYesNo(value);}" />                    
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_461 %>" width="150" isSortable="true" dataIndex="LastDiscovery" renderer="$SW.ext.DateRenderer($SW.ExtLocale.DateFull)" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_489 %>" width="100" isSortable="true" dataIndex="CountOfZones" runat="server" /> 
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_159 %>" width="80" isSortable="true" dataIndex="Location" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_245 %>" width="80" isSortable="true" dataIndex="VLAN" runat="server" />
					<IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_246 %>" width="80" isSortable="true" dataIndex="Comments" runat="server" />
			   </Columns>
				  <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="AddressN, FriendlyName"
						 listeners="$SW.IPAM.GroupBy.GetTabStoreListeners($SW.IPAM.DnsOpt, $SW.IPAM.DnsOpt.tabs.servers, $SW.IPAM.SetCboxGroupByVisibility(false) )" runat="server">                  
					<Fields>
						<IPAMlayout:GridStoreField dataIndex="GroupId" isKey="true" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="FriendlyName" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="Address" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="LastDiscovery" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="CountOfZones" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="ZoneTransfer" runat="server" />  
						<IPAMlayout:GridStoreField dataIndex="StatusShortDescription" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="GroupTypeText" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="GroupIconPrefix" runat="server" />     
						<IPAMlayout:GridStoreField dataIndex="StatusIconPostfix" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="VLAN" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
						<IPAMlayout:GridStoreField dataIndex="A.DnsServer.ServerType" runat="server" />
					</Fields>
				  </Store>
				</IPAMlayout:GridBox>

				</IPAMlayout:TabLayout>
			</Center>
		</IPAMlayout:BorderLayout>
	  </Center>
	  </IPAMlayout:BorderLayout>
	</Center>
	<South>
		<IPAMlayout:BoxComponent ID="ipamFooter" borderVisible="false" clientEl="footer" layout="fit" runat="server" />
	</South>
	</IPAMlayout:BorderLayout>

	</IPAMlayout:ViewPort>
	<div id="GroupByCell" style="padding: 6px; background: #e1e1e1 url(/Orion/IPAM/res/images/sw/bg.groupby.gif) repeat-x top left;"
		class="x-hide-display">
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_154%>
		<div class="sw-form-item sw-form-cb-stretch">
			<asp:DropDownList id="DhcpGroupBySelect" runat="server">
			    <%-- <asp:ListItem value="none."  Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_490 %>"/>--%>
			     <asp:ListItem value="none." Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_501 %>"></asp:ListItem>
                <asp:ListItem value="dhcp.ServerType" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_501 %>"></asp:ListItem>
				<asp:ListItem value="dhcp.TotalCount" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_501 %>"></asp:ListItem>
			</asp:DropDownList><asp:DropDownList id="DnsGroupBySelect" style="display: none; visibility: hidden;"
				runat="server">
						 <asp:ListItem value="none."  Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_490 %>"/>
						 <asp:ListItem value="zones.Parent.FriendlyName"  Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_507 %>"/>
						 <asp:ListItem value="zones.Parent.FriendlyName"  Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_508 %>"/>
			</asp:DropDownList></div>
	</div>
	<div id="ScopeGrid" class="x-hide-display">
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_509 %></div>
	<div id="ServerGrid" class="x-hide-display">
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_510 %></div>
	<div id="GraphViewContent" class="x-hide-display">
		<div id="reportsummary">
			<div id='reportloading' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
				<img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/default/tree/loading.gif" />
				<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_144 %>
			</div>
			<div id='reportfailure' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
				<img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/sw/icon.warning.gif" />
				<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_208 %>
			</div>
			<div id='Div1' style="width: 300px; padding-top: 40px;" class='x-hide-display'>
				<img style="vertical-align: middle; margin-right: 4px;" src="/orion/ipam/res/images/sw/icon.warning.gif" />
				<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_208 %>
			</div>
			<div id='reportcontent' style="padding-left: 300px;" class="rptlegend">
				<div style="position: relative;">
					<img id='reportimage' style="position: absolute; margin-left: -300px;" xw="300" xh="280"
						src="/orion/ipam/res/images/default/s.gif" border="0" />
				</div>
				<div>
					<table style="height: 280px; width: 100%;">
						<tr>
							<td valign="middle" id='reportlegend'>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
