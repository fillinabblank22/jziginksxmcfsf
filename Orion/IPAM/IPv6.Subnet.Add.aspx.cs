﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Utility;

public partial class IPv6SubnetAdd : CommonPageServices
{
    #region Constructors

    private PageIPv6SubnetEditor Editor { get; set; }
    public IPv6SubnetAdd()
    {
        this.Editor = new PageIPv6SubnetEditor(this, true);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.ParentId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // init subnet object
        GroupNode subnet = this.Editor.RetrieveSubnetObject();

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            InitControls(subnet);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(GroupNode subnet)
    {
        int parentId = this.Editor.ParentId;
        List<PrefixAggregate> parents =
            PageIPv6EditorHelper.GetPrefixParentsList(parentId);

        int maxSubnetSize = PageIPv6EditorHelper.MaxPrefixSize;
        int minSubnetSize = maxSubnetSize - PageIPv6EditorHelper.CountMaxPrefixSize(parents);
        int defSubnetSize = (minSubnetSize > 64) ? minSubnetSize : 64;
        string fnAddressListener = string.Empty;

        // init subnet address (and size)
        if (this.SubnetAddress != null)
        {
            this.SubnetAddress.Init(
                PageIPv6EditorHelper.GetSuggestedAddressValue(parents),
                GroupNodeType.IPv6Subnet, defSubnetSize, parents, parentId, null, Editor.ClusterId);
            fnAddressListener = this.SubnetAddress.Listener;
        }

        // init subnet size control
        if (this.SubnetSize != null)
        {
            this.SubnetSize.Init(minSubnetSize, maxSubnetSize, defSubnetSize, fnAddressListener);
        }

        CustomPropertyRepeater.RetrieveCustomProperties(subnet, new List<int>(){ subnet.GroupId });

        if (this.cbRetainUserData != null)
        {
            this.cbRetainUserData.Checked = false;
        }

        if (this.TransientPeriod != null)
        {
            this.TransientPeriod.Duration = null;
        }

        bool iScanningDisabled = false;
        TimeSpan scanInterval = new TimeSpan();

        this.Editor.ReadAutomaticScanSettings(out iScanningDisabled, out scanInterval);

        if (this.cbDisableAutoScanning != null)
        {
            this.cbDisableAutoScanning.Checked = iScanningDisabled;
        }

        if (this.ScanInterval != null)
        {
            this.ScanInterval.Value = scanInterval;
        }

        ReInitScanSettings();
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        GroupNode subnet = this.Editor.RetrieveSubnetObject();
        if (subnet != null)
        {
            PrefixAggregate parentPrefix =
                PageIPv6EditorHelper.GetParentPrefix(this.Editor.ParentId);

            GroupNode updatedSubnet = UpdateObject(subnet, parentPrefix);
            this.Editor.Store(updatedSubnet);
        }
    }
    private GroupNode UpdateObject(GroupNode subnet, PrefixAggregate parentPrefix)
    {
        subnet.GroupType = GroupNodeType.IPv6Subnet;
        subnet.ClusterId = parentPrefix.ClusterId;

        if (this.txtName != null)
        {
            subnet.FriendlyName = this.txtName.Text;
        }
        if (this.txtDescription != null)
        {
            subnet.Comments = this.txtDescription.Text;
        }
        if (this.txtVLAN != null)
        {
            subnet.VLAN = this.txtVLAN.Text;
        }
        if (this.txtLocation != null)
        {
            subnet.Location = this.txtLocation.Text;
        }

        if (this.SubnetAddress != null)
        {
            string ip = this.SubnetAddress.IPv6Value;
            

            subnet.Address = ip;
        }
        if (this.SubnetSize != null)
        {
            subnet.CIDR = this.SubnetSize.PrefixSizeValue;
            subnet.AddressMask = GroupNode.MaskFromCIDR(subnet.CIDR, System.Net.Sockets.AddressFamily.InterNetworkV6);
        }

        if (this.TransientPeriod != null)
        {
            subnet.TransientPeriod = this.TransientPeriod.Value;
        }

        if (this.cbRetainUserData != null)
        {
            subnet.RetainUserData = this.cbRetainUserData.Checked;
        }

        if (this.cbDisableAutoScanning != null)
            subnet.DisableAutoScanning = this.cbDisableAutoScanning.Checked;

        if (this.ScanInterval != null)
            subnet.ScanInterval = Convert.ToInt32(Math.Round(this.ScanInterval.Value.TotalMinutes));

        CustomPropertyRepeater.GetChanges(subnet);

        return subnet;
    }

    private void ReInitScanSettings()
    {
        bool isIcmpEnabled = false;
        bool isSnmpEnabled = false;
        bool isNeighborEnabled = false;

        // Read ScanSettings
        try
        {
            this.Editor.RetrieveScanSettings(out isIcmpEnabled, out isSnmpEnabled, out isNeighborEnabled);
        }
        catch (Exception ex)
        {
            ExceptionContext.MoveToExceptionPage(ex);
        }

        // set controls visibility
        if (this.IcmpScanInfo != null)
        {
            this.IcmpScanInfo.Visible = !isIcmpEnabled;
        }
    }

    #endregion // Methods
}
