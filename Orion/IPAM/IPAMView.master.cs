using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.IPAM.Web;
using SolarWinds.Orion.Web;

public partial class Orion_IPAM_IPAMView : SolarWinds.IPAM.Web.Common.CommonMasterPageServices
{
	protected void Page_Load(object sender, EventArgs e)
	{
        Page.Title = "IP Address Manager Summary";
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
	}
}
