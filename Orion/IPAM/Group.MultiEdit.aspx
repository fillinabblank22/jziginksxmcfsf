﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" 
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_405%>" CodeFile="Group.MultiEdit.aspx.cs" Inherits="GroupMultiEdit" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.IPv6AddressEditor.ascx" TagName="IPv6AddressEditor" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<script type="text/javascript">
$(document).ready(function() {
    $SW.GroupsExclusion = '<%=this.GroupsExclusion%>';
    $SW.ShowParentChangeConfirmDlg = <%=this.ShowParentChangeConfirmDlg %>;
    $SW.ClusterId = <%=this.ClusterId %>;
});
</script>
<IPAMmaster:JsBlock ID="JS" Orientation="jsPostInit" runat="server"
    Requires="ext,sw-ux-tree.js,ext-quicktips,sw-expander.js,sw-subnet-edit.js,sw-dialog.js,sw-treecombo.js">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready(function(){
    /* parent selector */
    var chbParent = $SW.ns$($nsId, 'chbParent');
    var hfParent = $SW.ns$($nsId, 'hfParent');
    var SetParentHiddenValue = function(el, node){
        if(el && hfParent){ hfParent.val(el.value); }
    };
    var ClearParentValue = function(el, node){
        if(el && hfParent){ el.clearValue(); hfParent.val(''); }
    };
    var treeLoader = $SW.MultiEdit.GetParentTreeLoader();
    treeLoader.query = $SW.MultiEdit.GroupsQuery($SW.ClusterId);
    treeLoader.query.exclusion = $SW.GroupsExclusion;
    var parentTree = new Ext.ux.form.TreeCombo( {
        loader: treeLoader,
        expandTree: false,
        listeners: {
            beforeselect: function(el, node){
                return $SW.MultiEdit.ChangeParentPermission(node.attributes.Role);
            },
            select: $SW.MultiEdit.ConfirmParentChange(SetParentHiddenValue, ClearParentValue)
        }
    });
    parentTree.render('parentSelector');
    $SW.MultiEdit.CheckBoxParentInit(chbParent[0], parentTree);


    var opt = {header: $SW.ns$($nsId, 'txtGroupsCountLabel') };
    $('#selectedGroupsList').expander(opt);
    $('.expander').expander();
});
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-helpserver,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
.sw-field-label { word-wrap: break-word; }
.sw-form-disabled { color: #777; }
.sw-form-listheader { font-weight: bold; margin-bottom: 4px; }
#formbox .group-box { margin-bottom: 20px; padding: 8px; 6px; max-width: 876px; }
#formbox .white-bg { background: none repeat scroll 0 0 transparent; }
#formbox .blue-bg { background: none repeat scroll 0 0 #E4F1F8; }
#separator hr { height: 1px; border: none; color: #B5B8C8; background-color: #B5B8C8;}
#selectedGroupsList li { height: 16px; padding-left: 60px; margin-bottom: 2px; background-position: 36px center; background-repeat: no-repeat;}
</IPAMmaster:CssBlock>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_406 %></div>
    
    <asp:ValidationSummary id="valSummary" runat="server"
                           ShowSummary="true"
                           ShowMessageBox="true"
                           DisplayMode="BulletList" />

    <div class="sw-form-subheader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_304%></div>
    
    <asp:Label ID="txtGroupsCountLabel" style="display: none;" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_407 %>"/>
    <div id="selectedGroupsList" collapsed="true" style="display: none;">
        <asp:BulletedList ID="GroupsList" runat="server" />
    </div>
    <div id="separator">
        <hr align="center" />
    </div>
    
    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-checkbox></td>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td class="sw-form-col-checkbox">
                <asp:CheckBox runat="server" ID="chbParent" />
            </td>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_408 %>
            </div></td>
            <td>
                <div class="sw-form-item" id="parentSelector">
                    <asp:HiddenField ID="hfParent" runat="server" />
                </div>
            </td>
        </tr>        
        <tr>
            <td class="sw-form-col-checkbox">
                <asp:CheckBox runat="server" ID="chbComments" />
            </td>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_147 %>
            </div></td>
            <td><div class="sw-form-item">
              <asp:TextBox ID="txtComments" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

  <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_142 %>" style="display: none;">
    <div class="group-box blue-bg">
        <ipam:CustomPropertyEdit ID="CustomPropertyEdit" runat="server" CustomPropertyObject="IPAM_GroupAttrData"/>
  </div>
  </div>
  
    <div><!-- ie6 --></div>
    
    <asp:CustomValidator ID="SomethingSelectedValidator" runat="server"
        ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_322 %>" Display="None"
        OnServerValidate="OnCheckBoxesServerValidate" />
        
</div>

    <table>
        <tr>
            <td style="width: 100px">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButton runat="server" CausesValidation="false" ID="btnCancel" OnClick="ClickCancel"
                        LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>

</asp:Content>
