﻿using System;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Master;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPAMGettingStarted : CommonPageServices
    {
        public bool isFreeTool = false;
        public bool isOtherModuleInstalled = false;
        private bool IsRBChecked(string rbId)
        {
            RadioButton rb = Locator.FindControlRecursive<RadioButton>(
                this, rbId, default(RadioButton));
            return rb.Checked;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);
        }

        protected void OnNextClick(object sender, EventArgs e)
        {
            if (IsRBChecked("rbImport"))
            {
                Response.Redirect("~/Orion/IPAM/AddSubnetIPAddresses.aspx");
                return;
            }
            if (IsRBChecked("rbDhcpServer"))
            {
                Response.Redirect("~/Orion/IPAM/dhcp.server.add.aspx");
                return;
            }
            if (IsRBChecked("rbDnsServer"))
            {
                Response.Redirect("~/Orion/IPAM/dns.server.add.aspx");
                return;
            }
            if (IsRBChecked("rbSingleSubnet"))
            {
                Response.Redirect("~/Orion/IPAM/subnet.add.aspx?ObjectId=0&ParentId=0");
                return;
            }
            if (IsRBChecked("rbBulkSubnets"))
            {
                Response.Redirect("~/Orion/IPAM/Subnet.BulkAdd.aspx?ObjectID=0");
                return;
            }
            if (IsRBChecked("rbSupernet"))
            {
                Response.Redirect("~/Orion/IPAM/Subnet.Calculator.aspx?ObjectID=0");
                return;
            }
        }
    }
}
