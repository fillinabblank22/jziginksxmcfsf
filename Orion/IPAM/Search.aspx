<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.Search"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_123 %>" ValidateRequest="false" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMcommon" Namespace="SolarWinds.IPAM.Web.Common" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>
<%@ Register TagPrefix="ipam" TagName="TimePeriodSelector" Src="~/Orion/IPAM/Controls/TimePeriodSelector.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <div style="text-align: right;">
        <span id="dateArea"></span>
        <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHViewIPAddressSearchResults" />
    </div>        
    <div style="padding: 8px 0;">                               
    <IPAMui:SearchBox id="searchbox1" emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_212 %>" runat="server" listeners="{beforerender:function(that){$SW.SearchInit(that,$SW.SearchInfo)}}">
       <handler>$SW.SearchGo</handler>
       <Menu ID="Menu1" runat="server"  />
    </IPAMui:SearchBox>
    <IPAMcommon:SearchState ID="SearchState1" runat="server" />
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="adminTitlePlaceholder" runat="server">

<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
span.sw-search-q, span.sw-search-col { font-weight: bold; }

/* Fix ridiculously wide column sizes in Ext grid on IE. */
.x-grid3-header-offset { width:auto; }

.sw-history-clue { color: gray; font-size: 11px; font-style: normal; white-space: nowrap; }
.sw-history-clue a { color: #336699; text-decoration: none; }
.sw-history-clue a:hover { color: orange; }
#time-period { margin-left: 10px; display: none; position:absolute; z-index: 1999; }
#time-filter { display:none; font-size: small; padding-left: 16px; }
#time-filter .time-value { border: 1px solid silver; font-weight: bold; vertical-align:middle; }
#time-filter .time-trigger { margin-left: 2px; vertical-align:middle; }
span.highlight { background: #FDE9A0; }
a.sw-grid-click span.highlight { text-decoration: underline; }
</IPAMmaster:CssBlock>

<script type="text/javascript">
$SW.search={};
$SW.search.entity = 'IPAM.IPNodeWithHistory';
$SW.search.key = <%= EncodedSearchKey %>;
$SW.search.cols = <%= EncodedSearchColumns %>;
$SW.search.timeperiod = {
    history: '<%=TimePeriodEnabled%>',
    timeperiod: '<%=TimePeriodData%>',
    begin: '<%=BeginDate%>',
    end: '<%=EndDate%>'
};
$SW.search.setparams = function(){
  $SW.store.baseParams.q = $SW.search.key;
  $SW.store.baseParams.columns = $SW.search.cols;
  $SW.store.baseParams.history = $SW.search.timeperiod.history;
  $SW.store.baseParams.timeperiod = $SW.search.timeperiod.timeperiod;
};

 // declare own renderer
$SW.search.RegExWildcard = function(str){
  var specials = new RegExp("[.*+?|()\\[\\]{}\\\\]", "g"); // .*+?|()[]{}\
  // Escape the pattern to make it regex-safe. Wildcards use only '*' and '?'.
  // Then convert '\*' and '\?' to their respective regex equivalents, '.*' and '..'
  return str.replace(specials, "\\$&").replace("\\*", ".*").replace("\\?", ".");
}
$SW.search.Renderer = function(value, meta) {
  if (typeof value == "string") {
    if (value.length > 0) {
      var q = $SW.search.RegExWildcard($SW.search.key);
      return value.replace(new RegExp(q, "ig"), '<span class="highlight">$&</span>');
    } else { return "&#160;"; }
  }
  return value;
}
// replace default grid renderer with own renderer
Ext.grid.ColumnModel.defaultRenderer = $SW.search.Renderer;



</script>

<IPAMmaster:JsBlock ID="JsBlock1" requires="ext-quicktips,sw-subnet-manage.js" Orientation="jsPostInit" runat="server">
$SW.search.SetHistoryPanel = function(el) {
    var tf = $('#time-filter');
    var tp = $('#time-period');
    if(!el || !tf || !tp) return;
    if(el.checked){ tf.show(); } else { tf.hide(); tp.hide(); }
};

$(document).ready(function(){
    var rb = $('#' + $SW.nsGet($nsId, 'rbHistoryYes'));
    if(rb && (rb.length==1)){
        rb.change( function(){ $SW.search.SetHistoryPanel(this); } );
        $SW.search.SetHistoryPanel(rb[0]);
    }
});

$SW.search.searchResultData = function(btn) {
    var searchData = Ext.getCmp('SearchGrid').getStore();
    if(searchData.getTotalCount() > 0)
    {
        return searchData;
    } 
    Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
        msg: String.format("@{R=IPAM.Strings;K=IPAMWEBJS_TM0_22;E=js}", btn.text),
        buttons: Ext.Msg.OK,
        animEl: btn.id,
        icon: Ext.MessageBox.WARNING
    });
};

$SW.search.RedirectExportSearchResult = function(el) {
    var listIp = [];
    var propList = [];

    var fields = el.fields;
    
    var properties = fields.keys.join(',');
    var searchKey = $SW.search.key;
    var searchColumns = $SW.search.cols;
    var entity = $SW.search.entity;
    var timePeriod = $SW.search.timeperiod.timeperiod;
    var history = $SW.search.timeperiod.history;
    var userColumn = $SW.search.usrcols;
    var numberOfIps = el.getTotalCount();
    Ext.DomHelper.append(document.body, {
        id: 'post-forward',
        tag: 'form',
        method: 'POST',
        action: 'SearchExport.pickcolumns.aspx',
        children: [{ tag: 'input', type: 'hidden', value: searchKey, name: 'searchKey'},
        { tag: 'input', type: 'hidden', value: searchColumns, name: 'SearchColumns'},
        { tag: 'input', type: 'hidden', value: entity, name: 'Entity'},
        { tag: 'input', type: 'hidden', value: timePeriod, name: 'TimePeriod'},
        { tag: 'input', type: 'hidden', value: properties, name: 'Properties'},
        { tag: 'input', type: 'hidden', value: history, name: 'History'},
        { tag: 'input', type: 'hidden', value: numberOfIps, name: 'NumberOfIps'}
        ]
    }).submit();
};

$SW.search.RedirectIPAddressManage = function(row) {
    if(!row) { return; }
    window.location = "Subnets.aspx?opento=" + row.SubnetId + "&ip=" + row.IPAddress;
};

$SW.search.RedirectIPAddressDetails = function(row) {
    if(!row) { return; }
    window.location = 'IPAMAddressDetailsView.aspx?NetObject=IPAMN:' + row.IpNodeId;
};

$SW.search.drilldown = function(grid,rowidx,e){
    var store = grid.getStore();
    var row = store.getAt(rowidx).data;
    $SW.search.RedirectIPAddressManage(row);
};

/* menu buttons */
$SW.search.singleSelect = function(btn) {
    var sel = Ext.getCmp('SearchGrid').getSelectionModel();
    var c = sel.getCount();
    if (c != 1) {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
            msg: String.format("@{R=IPAM.Strings;K=IPAMWEBJS_TM0_20;E=js}", btn.text),
            buttons: Ext.Msg.OK,
            animEl: btn.id,
            icon: Ext.MessageBox.WARNING
        });
        return;
    } else {
        var items = sel.getSelections();
        return items[0].data;
    }
};

$SW.search.manage = function(el) {
    var row = $SW.search.singleSelect(el);
    $SW.search.RedirectIPAddressManage(row);
};

$SW.search.details = function(el) {
    var row = $SW.search.singleSelect(el);
    $SW.search.RedirectIPAddressDetails(row);
};

$SW.search.export = function(el) {
    var store = $SW.search.searchResultData(el);
    $SW.search.RedirectExportSearchResult(store)
};

$SW.search.checkRow = function(el, row) {
    if(!row) return false;
};

$SW.search.history = function(el, paramFn, checkFn) {
    if(!checkFn) checkFn = $SW.search.checkRow;

    var row = $SW.search.singleSelect(el);
    if(checkFn(el, row) == false) { return; }
    
    var param = (paramFn !== undefined) ? paramFn(row) : '';
    var url = 'Resources/DetachIPAddressHistory.aspx' + param;

    var timeperiod = $SW.search.timeperiod.timeperiod;
    var form = Ext.DomHelper.append( Ext.getBody(), {
        tag: 'form', method: 'POST', action: url,
        children: [ { tag: 'input', type: 'hidden', name: 'TimePeriod', value: timeperiod } ]
    });
    form.submit();
};

$SW.search.safeURIEncode = function(val) {
    return encodeURIComponent($('<div/>').html(val).text());
};

$SW.search.checkEmptyValue = function(el, val, param) {
    if(!val || typeof(val)!="string" || val=="" || val=="&nbsp;"){
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_21;E=js}',
            msg: String.format("@{R=IPAM.Strings;K=IPAMWEBJS_TM0_22;E=js}", param),
            buttons: Ext.Msg.OK,
            animEl: el.id,
            icon: Ext.MessageBox.WARNING
        });
        return false;
    }
    return true;
};
</IPAMmaster:JsBlock>

<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
  { %>
<IPAMmaster:JsBlock requires="injectDateAreaLinks.js" Orientation="jsPostInit" runat="server">
    $(document).ready(function() { $('#dateArea').IpamSettingsLinkInjector({ pos: 1, before: true }); });
</IPAMmaster:JsBlock>
<%}%>

<div style="margin-left: 10px;">
  <table  id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td class="crumb" style="vertical-align: top;">
        <div style="margin: 15px 0px 11px 0px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	    <h1><%= Page.Title %></h1>
    </td></tr>
    <tr><td>
      <div style="margin-top: 10px; margin-bottom: 10px;">
        <asp:Label id="searchtext" runat="server" />
        <asp:Literal id="fieldlist" runat="server" />
      </div>
    </td></tr>
    <tr><td>
      <div style="margin-bottom: 5px;">
        <asp:RadioButton ID="rbHistoryNo" GroupName="grpHistory" AutoPostBack="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_124 %>" runat="server" /><br />
      </div>
    </td></tr>
    <tr><td>
      <div style="margin-bottom: 5px;">
        <asp:RadioButton ID="rbHistoryYes" GroupName="grpHistory" AutoPostBack="true" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_125 %>" runat="server" /> &nbsp;
        <span class="sw-history-clue"><%= IPAMWebContent.IPAMWEBDATA_TM0_126 %></span>
        <div id="time-filter">
            <span style="vertical-align: middle;">
                <%= IPAMWebContent.IPAMWEBDATA_TM0_127 %>
            </span><span class="time-value">
                &nbsp;<asp:Label ID="TimePeriodText" Text="{TimePeriod}" runat="server" />&nbsp;
            </span><a onclick="$('#time-period').toggle();"><img class="time-trigger" src="/Orion/IPAM/res/images/sw-navigation/pick-btn.gif" alt="select" /></a>
        </div>
      </div>
    </td></tr>
   </table>
</div>
  
<div id="time-period">
    <ipam:TimePeriodSelector ID="TimePeriodSelector" runat="server"
        OnClientSave="$('#time-period').hide();"
        OnClientCancel="$('#time-period').hide();"
    />
</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.Any" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<div style="margin-left: 10px; margin-right: 10px;">
    <div id="gridframe"> <!-- --> </div>

    <div style="margin: 10px 0px;">
        <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_128,
                          "<div><a id=\"A1\" href=\"/Orion/IPAM/subnets.aspx\" class=\"backto\"><span class=\"raquo\">&raquo;</span>", 
                          "</a></div><div style=\"margin-top: 4px;\">",
                          "<a href=\"/Orion/IPAM/IPAMSummaryView.aspx\" class=\"backto\">", "</a></div>") %>
    </div>
</div>

      <IPAMlayout:GridBox runat="server"
        width="640"
        minBodyHeight="150"
        maxBodyHeight="500"
        autoFill="false"
        autoScroll="false"
        autoHeight="false"
        id="SearchGrid"
        title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_129 %>"
        emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_130 %>"
        borderVisible="true"
        isFramed="false"
        RowSelection="false"
        clientEl="gridframe"
        deferEmptyText="false"
        StripeRows="true"
        listeners="{
    beforerender: $SW.ext.gridSnap,
    cellclick: $SW.ext.gridClickToDrilldown,
    render: $SW.search.setparams,
    drilldown: $SW.search.drilldown,
    statesave: $SW.subnet.GridColumnChangedHandler
}">
      <PageBar pageSize="100" displayInfo="true" />
      <TopMenu borderVisible="false">

        <IPAMui:ToolStripButton text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_131 %>" toolTip="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_131 %>" iconCls="mnu-manage-ip" runat="server">
            <handler>function(that){ $SW.search.manage(that); }</handler>
        </IPAMui:ToolStripButton>

        <IPAMui:ToolStripSeparator runat="server" />

        <IPAMui:ToolStripButton text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_257 %>" toolTip="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_258 %>" iconCls="mnu-details" runat="server">
            <handler>function(that){$SW.search.details(that); }</handler>
        </IPAMui:ToolStripButton>

        <IPAMui:ToolStripSeparator runat="server" />

        <IPAMui:ToolStripMenu id="HistoryDropDown" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_132 %>" iconCls="mnu-history" runat="server">
            <IPAMui:ToolStripMenuItem id="IPAssignedHistory" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_133 %>" runat="server">
                <handler>function(that){ $SW.search.history(that,
    function(row){ return '?NetObject=IPAMN:'+row.IpNodeId; }
                ); }</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripMenuItem id="MACAssignedHistory" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_134 %>" runat="server">
                <handler>function(that){ $SW.search.history(that,
    function(row){ return '?MAC='+$SW.search.safeURIEncode(row.MAC); },
    function(el, row){ if(!row) return false; return $SW.search.checkEmptyValue(el, row.MAC, 'MAC'); }
                ); }</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripMenuItem id="DNSAssignedHistory" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_135 %>" runat="server">
                <handler>function(that){ $SW.search.history(that,
    function(row){ return '?DNS='+$SW.search.safeURIEncode(row.DnsBackward); },
    function(el, row){ if(!row) return false; return $SW.search.checkEmptyValue(el, row.DnsBackward, 'DNS'); }
                ); }</handler>
            </IPAMui:ToolStripMenuItem>
        </IPAMui:ToolStripMenu>

        <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_237_2 %>" iconCls="mnu-export" runat="server">
            <handler>function(that){$SW.search.export(that); }</handler>
        </IPAMui:ToolStripButton>

      </TopMenu>
      <Columns>
        <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" dataIndex="Status" runat="server" renderer="$SW.subnet.ipStatusImgRenderer()" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_225 %>" width="120" isSortable="true" dataIndex="SubnetName" renderer="$SW.ext.gridRendererDrilldown($SW.search.Renderer, false)" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="120" isSortable="true" dataIndex="IPAddress" renderer="$SW.ext.gridRendererDrilldown($SW.search.Renderer, true)" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_158 %>" width="90" isSortable="true" dataIndex="Status"  renderer="$SW.subnet.ipStatusRenderer($SW.search.Renderer)" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_273 %>" width="120" isSortable="true" isHidden="true" dataIndex="IPMapped" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_278 %>" width="120" isSortable="true" isHidden="true" dataIndex="Alias" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="86" isSortable="true" isHidden="true" dataIndex="AllocPolicy" runat="server" renderer="$SW.subnet.ipTypeRenderer()" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_280 %>" width="120" isSortable="true" dataIndex="DnsBackward" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_281 %>" width="120" isSortable="true" dataIndex="DhcpClientName" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_274 %>" width="80" isSortable="true" dataIndex="MAC" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_155 %>" width="120" isSortable="true"  dataIndex="Vendor" runat="server" renderer="$SW.subnet.iconImgRenderer()" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_283 %>" width="120" isSortable="true" dataIndex="LastSync" renderer="$SW.subnet.ipLastResponse($SW.ExtLocale.DateFull)" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_285 %>" width="120" isSortable="true" isHidden="true" dataIndex="sysName" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_286 %>" width="120" isSortable="true" dataIndex="Description" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_287 %>" width="120" isSortable="true" dataIndex="Location" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_288 %>" width="120" isSortable="true" dataIndex="Contact" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_289 %>" width="120" isSortable="true" dataIndex="ResponseTime" renderer="$SW.subnet.ipResponseTimeRenderer()" runat="server" />
        <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_290 %>" width="120" isSortable="true" dataIndex="Comments" runat="server" />
      </Columns>
      <Store id="store" runat="server" dataRoot="rows" dataCount="count" autoLoad="false" 
            AmbientSort="IpAddressN,IsHistory ASC,HistoryTime DESC" proxyEntity="IPAM.IPNodeWithHistory"
            proxyUrl="extsearchprovider.ashx">
        <Fields>
            <IPAMlayout:GridStoreField dataIndex="IpNodeId" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="SubnetId" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="SubnetName" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IPOrdinal" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IPAddress" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Alias" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="MAC" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Vendor" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="VendorIcon" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="sysName" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IPMapped" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Status" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="AllocPolicy" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="DnsBackward" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="DhcpClientName" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="LastSync" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Description" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Location" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Contact" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="ResponseTime" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Comments" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IsHistory" runat="server" />
        </Fields>
      </Store>
      </IPAMlayout:GridBox>

     <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />
</asp:Content>
