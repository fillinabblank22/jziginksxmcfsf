using System;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.BusinessObjects;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common.DataBinding;
using System.Web.UI.WebControls;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPMultiEdit : CommonPageServices
    {
        const string StatusHelpFragment = "OrionIPAMPHStatus";
        const string TypeHelpFragment = "OrionIPAMPHAddressTypes";

        const string CustomPropertyCheckBoxName = "chbEditEnabled";

        PageIpMultiEditor editor;

        public IPMultiEdit()
        {
            this.editor = new PageIpMultiEditor(this);
            editor.SaveCustomProperties = SaveCustomProperties;
        }

        public void SaveCustomProperties(ICustomProperties node)
        {
            CustomPropertyRepeater.GetMultiEditIpChanges(node);
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            Save();
        }

        void Save()
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                AlterCheckBoxStates();
                this.editor.SaveChanges(true, true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Init();
            this.editor.Initialize();
            if (this.AccessCheck != null)
                this.AccessCheck.GroupIds.Add(this.editor.Subnet.GroupId);

            this.editor.LoadNodeIds();

            if (!this.IsPostBack)
            {
                this.StatusHelpLink.NavigateUrl = GetHelpUrl(StatusHelpFragment);
                this.TypeHelpLink.NavigateUrl = GetHelpUrl(TypeHelpFragment);

                bool displayingOnlyAvailableIps = this.editor.DisplayingOnlyAvailableIPs();
                GenerateIpList();

                CustomPropertyRepeater.RetrieveCustomProperties(new IPNode(), this.editor.NodeIds);

                IPNodeStatus selectedStatus = IPNodeStatus.Available;
                if (!displayingOnlyAvailableIps)
                {
                    selectedStatus = IPNodeStatus.Used;
                }
                
                DataBindHelper.BindEnums<IPNodeStatus>(ddlStatus, new IPNodeStatus[] { IPNodeStatus.Available, IPNodeStatus.Reserved, IPNodeStatus.Transient, IPNodeStatus.Used },
                    selectedStatus);
                
                DataBindHelper.BindEnums<IPAllocPolicy>(ddlType, new IPAllocPolicy[] { IPAllocPolicy.Static, IPAllocPolicy.Dynamic },
                    IPAllocPolicy.Static);
            }

            if (this.editor.DisplayingOnlyAvailableIPs())
            {
                string toggleVariable = "$ToggleAreaShouldBeHidden = true;";
                jsBlock.AddScript(toggleVariable);
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            string javascript = this.editor.GetJavascript(CustomPropertyRepeater.CheckBoxesClientIds);
            string modJs = "$(document).ready( function(){ " + javascript + " });";
            jsBlock.AddScript(modJs);
        }

        void GenerateIpList()
        {
            blAddresses.Items.Clear();

            int ipCount;
            List<string> ips;
            this.editor.GetIpAddresses(out ips, out ipCount);

            AddressCountLabel.Text = String.Format("{0}",ipCount);

            foreach (var ip in ips)
            {
                blAddresses.Items.Add(ip);
            }
        }

        void Init()
        {
            this.editor.RegisterControl(chbEditComment, txtComment, o => o.Comments);
            this.editor.RegisterControl(chbEditMachineType, txtMachineType, o => o.MachineType);
            this.editor.RegisterControl(chbEditSysContact, txtSysContact, o => o.Contact);
            this.editor.RegisterControl(chbEditSysName, txtSysName, o => o.sysName);
            this.editor.RegisterControl(chbEditSysDescription, txtSysDescr, o => o.Description);
            this.editor.RegisterControl(chbEditSysLocation, txtSysLocation, o => o.Location);
            this.editor.RegisterControl(chbEditVendor, txtVendor, o => o.Vendor);

            this.editor.RegisterControl(chbEditType, ddlType, o => o.AllocPolicy, false);
            this.editor.RegisterControl(chbEditScanning, ddlScanning, o => o.SkipScan, false, true);
            this.editor.RegisterControl(chbEditStatus, ddlStatus, o => o.Status, false);

        }

        void AlterCheckBoxStates()
        {
            if (chbEditStatus.Checked && ddlStatus.SelectedValue == IPNodeStatus.Available.ToString())
            {
                chbEditComment.Checked = false;
                chbEditMachineType.Checked = false;
                chbEditSysContact.Checked = false;
                chbEditSysDescription.Checked = false; chbEditSysLocation.Checked = false; chbEditSysName.Checked = false;
                chbEditVendor.Checked = false;
            }
        }

        protected void SomethingSelectedValidator_OnServerValidate(object source, ServerValidateEventArgs e)
        {
            List<CheckBox> checkBoxes = new List<CheckBox>();

            checkBoxes.Add(chbEditStatus);
            checkBoxes.Add(chbEditScanning);
            checkBoxes.Add(chbEditType);

            bool availableSelected = ddlStatus.SelectedValue == IPNodeStatus.Available.ToString();

            bool userDetailsVisible = chbEditStatus.Checked && !availableSelected
                || !chbEditStatus.Checked && !this.editor.DisplayingOnlyAvailableIPs();

            if (userDetailsVisible)
            {
                checkBoxes.Add(chbEditComment);
                checkBoxes.Add(chbEditMachineType);
                checkBoxes.Add(chbEditSysContact);
                checkBoxes.Add(chbEditSysName);
                checkBoxes.Add(chbEditSysDescription);
                checkBoxes.Add(chbEditSysLocation);
                checkBoxes.Add(chbEditVendor);
            }

            e.IsValid = checkBoxes.Exists(o=> o.Checked) || CustomPropertyRepeater.CheckIfCpChanged();
        }
    }
}
