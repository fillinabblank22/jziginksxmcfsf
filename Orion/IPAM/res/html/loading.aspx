﻿<html>
<head>
    <title>
        <asp:Literal ID="title" runat="server" Text="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_143%>" />
    </title>
    <link rel="stylesheet" type="text/css" href="../css/sw-light.css" />
</head>
<body class="sw-chromeless">
    <h4 style="margin-left: 16px;">
        <asp:Literal ID="msg" runat="server" Text="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_144%>" />
    </h4>
</body>
</html>
