﻿if (!window.$SW) {
    window.$SW = new (function() {
        this.appRoot = function() { return "/"; };
        this.timediff = new Date().getTime() - 1271244374017;
    })();
}
(function($) {
    $SW.FillSearchFilter = function(filter) {
        var elem = filter.elem;
        var isAllFields = undefined, keys = [], txt = [];
        $.each(filter.items, function() {
            if (this.key == "separator") return true;
            if (this.checked == undefined || this.checked) {
                keys.push(this.key); txt.push(jQuery(this.text).length > 0 ? jQuery(this.text).text() : this.text);
                if (this.isAllField) isAllFields = this;
            }
        });

        if (isAllFields) elem.val(isAllFields.text);
        else elem.val(txt.join(','));
    };

    $SW.SearchAllChanged = function(menu, checked) {
        // set checkboxes state
        $.each(menu.checkboxes, function() { this.check(checked); });
        // set also fields for postback
        $.each(menu.items, function() { this.checked = checked; });
    };

    $SW.SearchItemChanged = function(menu, checked) {
        // handle only un-check state
        if (checked == true) return;

        // find 'SearchAll' fields
        $.each(menu.items, function() {
            if (this.isAllField && this.checked) {
                // re-set field for postback
                this.checked = false;

                // uncheck checkbox
                var $key = this.key;
                $.each(menu.checkboxes, function() {
                    if ($key == this.item.key) this.check(false);
                });
            }
        });
    };

    $SW.CheckBoxItemClick = function(checked, checkbox) {
        var menu = this;

        // find item by checkbox key
        var item = undefined, key = checkbox.item.key;
        $.each(menu.items, function() {
            if (this.key == key) { item = this; return false; }
        });
        if (!item) return;

        // set field for postback
        item.checked = checked;

        // sync SearchAll checkbox state
        if (item.isAllField) {
            $SW.SearchAllChanged(menu, checked);
        } else {
            $SW.SearchItemChanged(menu, checked);
        }

        $SW.FillSearchFilter(menu);
    };

    $.dynFormPost = function(action, method, params) {
        var $form = $('<form>').attr('method', method).attr('action', action);
        $.each(params, function(name, value) {
            $('<input type="hidden">').attr('name', name).attr('value', value).appendTo($form);
        });
        $form.appendTo('body');
        $form.submit();
    };

    $SW.SearchResultGo = function(valueId, filterId) {
        var value = $(valueId)[0].value;
        if (value == '') {
            alert('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_17;E=js}');
            return false;
        }
        var filter = $.jItemFind(filterId);
        var conf = filter.config, keys = [];
        $.each(filter.items, function() {
            if (this.key == "separator") return true;
            if (this.checked == undefined || this.checked) keys.push(this.key);
        });
        if (keys.length < 1) {
            alert('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_18;E=js}');
            return false;
        }
        $.dynFormPost(
            conf.action + "?q=" + encodeURIComponent(value),
            conf.method || "POST",
            {
                columns: keys.join(','),
                storecolumns: conf.storecolumns,
                history: '',
                timeperiod: ''
            }
        );
        return false;
    };
})(jQuery);