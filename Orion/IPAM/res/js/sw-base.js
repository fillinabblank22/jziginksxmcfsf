// [pc] Requires jQuery

$SW.VersionCheck = function () {
    try {
        var browser = jQuery.browser;

        var vers = browser.version.split('.');
        var maj = vers[0];
        var min = vers[1] || 0;

        if (browser.mozilla) {
            if (maj >= 2) return true;
            return (maj == 1) && (min >= 5);
        }
        if (browser.msie) {
            if (maj >= 6) return true;
            return false;
        }
        if (browser.opera) {
            if (maj >= 9) return true;
            return false;
        }
        if (browser.safari) {
            return maj >= 523;
        }
    }
    catch (ex) {
    }
    return false;
};

$SW.ReflowFooter = function (e) {

    var winHeight = $(window).height();
    var scrollVis = document.body.scrollHeight > winHeight;
    var that = $SW.ReflowFooter;

    if ((typeof (that.last) != "undefined") && that.last == scrollVis)
        return;

    that.last = scrollVis;

    var cp2 = $('#copyrightBar');

    if (that.last)
        cp2.css({ position: 'static', bottom: '' });
    else
        cp2.css({ position: 'absolute', bottom: '0px' });
};

$SW.MakeStackTrace = function (kind) {
    var data = $SW['exinner' + kind];
    if (!data) return;

    var header = (kind == 'remote') ? "<h3>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_295;E=js}</h3><p>" : "<h3>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_296;E=js}</h3><p>";
    var html = [];
    var o = data;
    var c = 1;
    while (o) {
        html.push(header);
        if (o.Type) html.push("<i>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_297;E=js} " + o.Type + "</i><br>");
        html.push("<i>" + o.Message + "</i>");
        html.push("</p><div class=trace><pre>" + (o.StackTrace || "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_298;E=js}") + "</pre></div>");
        o = o.Inner;
        c++;
    }

    $('#stack').parent().append(html.join(''));
};

$SW.InitStackTrace = function (e) {

    $('#stack').css({ display: 'none' }).before("<a id=showstack onclick='return false;' href='#'>Show</a>");

    $('#showstack').click(function () {
        $SW.MakeStackTrace('local');
        $SW.MakeStackTrace('remote');
        $('#stack').css({ display: '' });
        $('a#showstack').css({ display: 'none' });
        $SW.ReflowFooter();
    });

    $SW.ReflowFooter();
};

$SW.NotImplemented = function (el) {
    if (el && typeof (el) != "string")
        el = el.id;

    Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_299;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_300;E=js}',
        buttons: Ext.Msg.OK,
        animEl: el,
        icon: Ext.MessageBox.WARNING
    });
};

if (!$SW.LicenseListeners)
    $SW.LicenseListeners = [];

$SW.LicenseListeners.recv = function (o) {
    var lic = o;
    jQuery.each($SW.LicenseListeners, function (i, v) { try { v(lic); } catch (e) { } });
};


$SW.PeriodicLicenseRefresh = function (ms) {
    var skipLicenseRefresh = true;

    Ext.TaskMgr.start({ run: function () {
        if (skipLicenseRefresh == false)
            LicenseRefreshTask();
        else
            skipLicenseRefresh = false;
    },
        interval: ms
    });

    function LicenseRefreshTask() {
        Ext.Ajax.request({
            url: '/Orion/IPAM/ExtCmdProvider.ashx',
            method: 'POST',
            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText);
                if (res.success == true && res.license && res.license.app == "IPAM") {
                    $SW.LicenseListeners.recv(res.license);
                }
            },
            failure: function (result, request) { },
            params:
           {
               entity: 'IPAM.InternalStatus',
               verb: 'Get'
           }
        });
    }
}



$SW.LicenseListeners.push(function (obj) {
    var o = obj, _dirty = true;
    function EvalBanner(o) {
        if (/^(evaluation|evalextension|beta)$/i.test(o.mode || "") && o.daysremain != "") {
            var banner = $('#evalBanner')[0], m;
            if (!banner) { $('#moduleList').before("<div id='evalBanner' style='width: 100%;'></div>"); banner = $('#evalBanner')[0]; _dirty = true; }
            if (!banner) return;

            var fmt1 = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_301;E=js}";
            var fmt2 = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_302;E=js}";

            var t = $(banner).text(), remain = o.daysremain, newt;
            if (m = t.match(/^\s*(?:this|Orion) evaluation has ([0-9]+) days left./i)) {
                var orion = parseInt(m[1]), i = parseInt(o.daysremain);

                if (orion == o.daysremain) {
                    newt = String.format(fmt1, o.daysremain, o.daysremain);
                }
                else {
                    if (orion < i) i = orion;
                    newt = String.format(fmt2, orion, i);
                }
            }
            else {
                newt = String.format(fmt1, o.daysremain, o.daysremain);
            }

            $(banner).text(newt);
        }
    }
    $(document).ready(function () {
        if (o.app != "IPAM") return;
        EvalBanner(o);
        if (_dirty && $SW.ReflowPage) { _dirty = false; $SW.ReflowPage.defer(250); }
    });
});


// iframe upstream / downstream message queue

(function () {

    if (!$SW.msgq) {
        $SW.msgq = new function () {
            var Q = {};
            this.Q = Q;

            this.UPSTREAM = function (eventid, info) {

                if (window.parent == window || !window.parent.$SW || !window.parent.$SW.msgq)
                    return false;

                var m = ('' + window.location.search).match(/(?:^|[?]|&)msgq=([^&]*)/);
                if (!m) return false;

                var id = decodeURIComponent(m[1]);
                window.parent.$SW.msgq.TX(id, eventid, info);
                return true;
            };

            this.DOWNSTREAM = function (extdialogwindow, id, eventid, info) {
                // requires ext
                var ex, doc, win;
                if ((ex = extdialogwindow.items.get(0)) &&
         (doc = $(ex.getEl().dom).contents()[0]) &&
         (win = (doc.defaultView || doc.parentWindow)) &&
         win.$SW &&
         win.$SW.msgq) {
                    return win.$SW.msgq.TX(id, eventid, info);
                }

                return false;
            };

            this.TX = function (id, eventid, info) {
                var fn;
                id = (id || '').toLowerCase();
                if (fn = Q[id])
                    return fn(eventid, info);
            };

            this.Register = function (id, fn) { Q[(id || '').toLowerCase()] = fn; };

            this.init = function (nsId, config) {

                var meta = config || {};
                var server = {};
                var client = {};
                var $nsId = nsId;
                var undef;

                jQuery.each(meta.msgs || [], function (i, o) {
                    var n = o.name = (o.name || '').toLowerCase();
                    if (o.onmsg) client[o.name] = o.onmsg;
                    else server[o.name] = { dovalidate: o.dovalidate || false, vgroup: o.vgroup || undef };
                });

                this.Register(meta.name, function (eventid, info) {
                    var v;

                    eventid = (eventid || '').toLowerCase();

                    if (v = client[eventid]) {
                        v($nsId, eventid, info);
                        return true;
                    }

                    if (v = server[eventid]) {
                        if (v.dovalidate && window.Page_ClientValidate) {
                            if (!Page_ClientValidate(v.vgroup)) return;
                        }

                        var form = $('#' + meta.formid)[0] || document.forms[0];
                        if (!form) return;

                        if (form.onsubmit) {
                            if (!(form.onsubmit.apply(form, [])))
                                return;
                        }

                        var getinput = function (name, value) {
                            var ret = form[name];
                            if (!ret) {
                                ret = document.createElement("input");
                                ret.type = 'hidden';
                                ret.name = name;
                                $(form).append(ret);
                            }
                            ret.value = value;
                            return ret;
                        };

                        getinput('__EVENTTARGET', meta.myid);
                        getinput('__EVENTARGUMENT', eventid);
                        form.submit();
                        return true;
                    }

                    return false;
                });

                var that = this;
                jQuery.each(meta.ups || [], function (i, o) { that.UPSTREAM(o.id, o.info || null); });
            };
        };
    }

})();
