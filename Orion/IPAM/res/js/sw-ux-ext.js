
// Ext extensions

Ext.ux.SWArrayReader = Ext.extend(Ext.data.JsonReader, {

    readRecords : function(o){
        var sid = this.meta ? this.meta.id : null;
    	var recordType = this.recordType, fields = recordType.prototype.fields;
    	var records = [];
    	var s = this.meta;

        if( !this.ef ) {
            if(s.totalProperty) { this.getTotal = this.getJsonAccessor(s.totalProperty); }
	        if(s.successProperty) { this.getSuccess = this.getJsonAccessor(s.successProperty); }
	        this.getRoot = s.root ? this.getJsonAccessor(s.root) : function(p){return p;};
	        this.ef=[];
        }

    	var root = this.getRoot(o), c = root.length, totalRecords = c, success = true;

    	if(s.totalProperty){
            var v = parseInt(this.getTotal(o), 10);
            if(!isNaN(v)){
                totalRecords = v;
            }
        }

        if(s.successProperty){
            var v = this.getSuccess(o);
            if(v === false || v === 'false'){
                success = false;
            }
        }
    	
	    for(var i = 0; i < root.length; i++){
		    var n = root[i];
	        var values = {};
	        var id = ((sid || sid === 0) && n[sid] !== undefined && n[sid] !== "" ? n[sid] : null);
	        for(var j = 0, jlen = fields.length; j < jlen; j++){
                var f = fields.items[j];
                var k = f.mapping !== undefined && f.mapping !== null ? f.mapping : j;
                var v = n[k] !== undefined ? n[k] : f.defaultValue;
                v = f.convert(v, n);
                values[f.name] = v;
            }
	        var record = new recordType(values, id);
	        record.json = n;
	        records[records.length] = record;
	    }

	    return {
	        records : records,
	        totalRecords : totalRecords,
	        success : success
	    };
    }
});


$SW.SwqlJoinBuild = function(node, loader) {
    var t;
    var mainPrefix = "A";
    var joinPrefix = "B";
    var q = [];
    var l = loader.query.limit ? 'TOP ' + (loader.query.limit + 1) + ' ' : '';
    var jsonObj;

    var fmt = function(clause, prefix, scope) {
        if (!clause) return null;
        if (typeof (clause) == 'string') return String.format(clause, prefix);
        if (typeof (clause) == 'function') return clause(prefix, scope);
        return null;
    };

    q.push("SELECT " + l);

    jQuery.each(loader.fields,
        function(n, el) {
            if (n) q.push(', ');
            if (el.code) q.push(String.format(el.code, mainPrefix));
            else q.push(mainPrefix + "." + el.name);
            if (el.as) q.push(" as \"" + el.as + "\"");
        });

    if (loader.query.count) {
        q.push(String.format(", " + loader.query.count, joinPrefix));
    }

    q.push(" FROM " + loader.query['entity'] + " " + mainPrefix + " ");
    if (loader.query.leftJoin) {
        q.push(String.format(" LEFT JOIN " + loader.query.leftJoin, mainPrefix, joinPrefix));
    }

    if (t = fmt(loader.query.where, mainPrefix, node)) {
         q.push(" WHERE " + t);
    }

    jsonObj = { query: q.join('') };
    if (loader.query.union && loader.query.union != null) {
        var union = [];
        union.push("SELECT ");
        jQuery.each(loader.fields,
            function(n, el) {
                if (n) union.push(', ');
                if (el.code) union.push(String.format(el.code, pre));
                else union.push(pre + "." + el.name);
                if (el.as) union.push(" as \"" + el.as + "\"");
            });

        union.push(" FROM " + loader.query['entity'] + " " + pre + " ");
        if (t = fmt(loader.query.where, pre, node)) union.push(" WHERE " + t);
        if (t = fmt(loader.query.union, pre, node)) union.push(" " + t);

        jsonObj = { query: q.join(''), unionQuery: (union.length > 0) ? union.join('') : '' };
    }

    return jsonObj;
};

$SW.SWQLbuild = function (node, loader) {
      var t, pre = "A", q = [], l= loader.query.limit ? 'TOP ' + (loader.query.limit+1) + ' ' : '',jsonObj = null;

      var fmt = function(clause,prefix,scope){
        if(!clause) return null;
        if(typeof(clause)=='string') return String.format(clause,prefix);
        if(typeof(clause)=='function') return clause(prefix,scope);
        return null;
      };

     q.push( "SELECT " + l );

     jQuery.each( loader.fields, function(n,el){
      if( n ) q.push(', ');
      if( el.code ) q.push( String.format( el.code, pre ) );
      else q.push( pre + "." + el.name );
      if( el.as ) q.push( " as \"" + el.as + "\"" );
     });

     q.push( " FROM " + loader.query['entity'] + " " + pre + " " );
     if( t = fmt( loader.query.f, pre, node ) ) q.push(" " + t + " ");
     if( t = fmt( loader.query.where, pre, node ) ) q.push(" WHERE " + t);
     if( t = fmt( loader.query.groupby, pre, node ) ) q.push(" GROUP BY " + t);
     if (t = fmt(loader.query.having, pre, node)) q.push(" HAVING " + t);
     if (t = fmt(loader.query.orderby, pre, node)) q.push(" ORDER BY " + t);
     jsonObj = { query: q.join('')};
    
     if (loader.query.union && loader.query.union != null) {
        var union = [];
        union.push("SELECT ");
        jQuery.each(loader.fields, function (n, el) {
            if (n) union.push(', ');
            if (el.code) union.push(String.format(el.code, pre));
            else union.push(pre + "." + el.name);
            if (el.as) union.push(" as \"" + el.as + "\"");
        });

        union.push(" FROM " + loader.query['entity'] + " " + pre + " ");
        if (t = fmt(loader.query.where, pre, node)) union.push(" WHERE " + t);
        if (t = fmt(loader.query.union, pre, node)) union.push(" " + t);
        
        jsonObj = { query: q.join(''), unionQuery: (union.length > 0) ? union.join('') : '' };
    }

    return jsonObj;
};

Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
     onRender : function(ct, position){
         this.el = ct.createChild({ tag: 'iframe', id: 'iframe-' + this.id, name: 'iname-' + this.id, frameBorder: 0, src: this.url });
     }
});
Ext.reg('sw-iframe', Ext.ux.IFrameComponent);


/**
 * @class Ext.ux.SliderTip
 * @extends Ext.Tip
 * Simple plugin for using an Ext.Tip with a slider to show the slider value
 */
Ext.ux.SliderTip = Ext.extend(Ext.Tip, {
    minWidth: 10,
    offsets : [0, -10],
    init : function(slider){
        slider.on('dragstart', this.onSlide, this);
        slider.on('drag', this.onSlide, this);
        slider.on('dragend', this.hide, this);
        slider.on('destroy', this.destroy, this);
    },

    onSlide : function(slider){
        this.show();
        this.body.update(this.getText(slider));
        this.doAutoWidth();
        this.el.alignTo(slider.thumb, 'b-t?', this.offsets);
    },

    getText : function(slider){
        return slider.getValue();
    }
});

(function() {
    if (!$SW.ext) $SW.ext = {};

    var re = /sw-enableonload/;

    function ctor() {
        this.beforehide = function(that) {
            that.items.get(0).getEl().dom.src = 
        window.location.protocol + "//" +
        window.location.host +
        $SW.appRoot() +
        "Orion/IPAM/res/html/loading.aspx";
            that.needsReset = true;
            return true;
        };

        this.beforeshow = function(that) {

            Ext.each(that.buttons || [], function(o, i) {
                if (re.test(o.initialConfig.cls || ''))
                    o.disable();
            });

            if (that.needsReset) {
                that.needsReset = false;

                var ex, doc, win;
                if ((ex = that.items.get(0)) &&
             (doc = $(ex.getEl().dom).contents()[0]) &&
             (win = (doc.defaultView || doc.parentWindow))) {
                    win.location.replace(ex.url);
                }
            }

            return true;
        };
    };

    $SW.ext.dialogWindowListeners = function() { return new ctor() };

    $SW.ext.dialogWindowEnableButtons = function(extdialogwindow, disablemode) {
        disablemode = !!disablemode;
        Ext.each(extdialogwindow.buttons || [], function(o, i) {
            if (re.test(o.initialConfig.cls || '')) {
                if (disablemode) o.disable();
                else o.enable();
            }
        });
    };

    $SW.ext.dialogWindowOpen = function(extdlgwindow, el, title, url, helpid) {
        var that = extdlgwindow;

        if (title)
            that.setTitle(title);

        if (helpid)
            that.initialConfig.helpid = helpid;

        if (url) {
            var ex = that.items.get(0);
            var m = url.match(/^(~\/?)(.*)/);

            if (m)
                url = window.location.protocol + "//" + window.location.host + $SW.appRoot() + m[2];

            ex.url = url;
            that.needsReset = true;
        }

        if (el)
            that.setAnimateTarget(el);

        var box = $(window), h = box.height(), w = box.width();

        if (that.constrain || that.constrainHeader) {
            if (that.rendered) {
                var c = null;
                var b = that.getBox();
                if (b.width > w) c = that.setWidth(w);
                if (b.height > h) c = that.setHeight(h) || c;
                if (c) that.center();
            }
            else {
                if (typeof (that.width) == "Number" && that.width > w) that.width = w;
                if (typeof (that.height) == "Number" && that.height > h) that.height = h;
            }
        }

        that.show(el);
    };

    $SW.ext.dialogWindowHelp = function(e, el, panel) {

        var tempFragmentId = panel.initialConfig.tempFragmentId;
        var fragmentUrl = panel.initialConfig.FragmentUrl;

        var v = panel.initialConfig.helpid;
        if (v) {
            var replacedUrl = fragmentUrl.replace(tempFragmentId + '.htm', v);
            window.open(replacedUrl, '_blank');
            return true;
        }

        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_299;E=js}',
            msg: String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_303;E=js}", panel.initialConfig.helpid),
            buttons: Ext.Msg.OK,
            animEl: el,
            icon: Ext.MessageBox.WARNING
        });
    };

    $SW.ext.HttpProxyException = function(objthis, options, response, e) {
        if ($SW.ext.AjaxIsErrorOrExpire({ cmd: "Load Grid Data", text: response ? response.responseText : null, onlylogout: true }))
            return;
    };

    $SW.ext.AjaxMakePassDelegate = function(title, delegate, extidscope) {
        var t = title, d = delegate, id = extidscope || null;
        return function(response, conn) {
            var ret, text = response ? response.responseText : null;
            try { ret = Ext.util.JSON.decode(text); } catch (ex) { }
            if ($SW.ext.AjaxIsErrorOrExpire({ cmd: t, obj: ret, text: text }) == false) {
                var f = extidscope ? Ext.getCmp(extidscope) : 0;
                if (f) d.apply(f, [ret, conn]);
                else d(ret, conn);
            }
        };
    };
    
    $SW.ext.AjaxMakeFailDelegate = function (title, delegate, extidscope) {
        var t = title, d = delegate, id = extidscope || null;
        return function(response, conn) {
            var text = response ? response.responseText : null;

            if (delegate == undefined && extidscope == undefined)
            { //qurey time out
                $SW.ext.AjaxIsErrorOrExpire({ cmd: t, obj: null, text: text });
            }
            else {
                if ($SW.ext.AjaxIsErrorOrExpire(delegate, { cmd: t, obj: null, text: text }) == false) {
                    var f = extidscope ? Ext.getCmp(extidscope) : 0;
                    if (f) d.apply(f, [response, conn]);
                    else d(response, conn);
                }
            }
        };
    };

    $SW.ext.AjaxIsErrorOrExpire = function(delegate, params) {
        var d = delegate
        if (!params)
            return false;
        
        var opts = { buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR };
        if (params.btn) opts.animEl = params.btn;

        if (!params.obj)
        {
            if (params.text && /ctl00\$ContentPlaceHolder1\$Username/i.test(params.text))
            {
                opts.title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_304;E=js}';
                opts.msg = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_305;E=js}";
                opts.fn = function(btn) {
                    if (btn == 'ok') {
                        var loc = '' + window.location;
                        if (!/[?]/.test(loc)) loc += "?";
                        loc = loc.replace(/rnd=[0-9]+(&|$)/i, '');
                        if (!/[&?]$/.test(loc)) loc += "&";
                        loc += "rnd=" + (new Date()).getTime() + "&";
                        window.location = loc;
                    }
                };
            }
            else if (!params.onlylogout) {
                opts.title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_201;E=js}';
                opts.msg = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_202;E=js}";    
                opts.fn = function(btn) {
                    if (btn == 'ok') {
                        d();
                    }
                };           
            }
            else return false;

            Ext.Msg.show(opts);
            return true;
        }

        if (params.obj.success == false) {
            opts.title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_306;E=js}';
            opts.msg = String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_307;E=js}", params.cmd);
            if (params.obj.message) opts.msg += "<p>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_308;E=js} " + params.obj.message + "</p>";

            Ext.Msg.show(opts);
            return true;
        }

        return false;
    };

    $SW.DoQuery = function(query, passFn, opts) {
        opts = opts || {};
        var pass, fail, service = opts.service || 'Orion/IPAM', convert = Ext.emptyFn;

        function proc(results) {
            var r, ret = [], d = results.d || {}, cols = d.Columns || [], rows = d.Rows || [], ncols = cols.length;
            jQuery.each(rows, function(n, row) {
                ret.push(r = {});
                for (var i = 0; i < ncols; ++i) r[cols[i]] = row[i];
            });
            passFn(ret);
        };

        pass = $SW.ext.AjaxMakePassDelegate(opts.desc || 'Ajax Query', service.raw ? passFn : proc, opts.extid);
        fail = $SW.ext.AjaxMakeFailDelegate(opts.desc || 'Ajax Query', opts.fail || Ext.emptyFn, opts.extid);
        
        Ext.Ajax.request({
            type: 'POST',
            url: '/' + service + '/services/information.asmx/Query',
            jsonData: { query: query },
            success: pass,
            failure: fail
        });
    };

    // ext grid panel, snap-to container

    $SW.ext.gridSnap = function(o) {
        var lastwidth = null;
        var gridid = o.id;

        function dosnap() {
            var obj = Ext.getCmp(gridid), w = $(obj.el.dom).parent().width();
            if (lastwidth == null || w != lastwidth) obj.setWidth(w);
            lastwidth = w;
        }

        $(window).bind('resize', dosnap);
        o.getView().on('rowsinserted', dosnap);
        o.getView().on('rowsremoved', dosnap);
        window.setTimeout(dosnap, 20);
        return true;
    };

    $SW.ext.gridClickToDrilldown = function(grid, rowIndex, columnIndex, e) {
        var record, be = e.browserEvent.target || e.browserEvent.srcElement;
        // handle also nested 'highlighted' span tags
        if(be.tagName == 'SPAN' && /highlight/.test(be.className)){
            var p = $(be).parent(); if(p.length > 0) be = p[0];
        }
        if (be.tagName == 'A' && /sw-grid-click/.test(be.className))
            grid.fireEvent('drilldown', grid, rowIndex, columnIndex, e);
        return true;
    };
    $SW.ext.gridRendererDrilldown = function(highlighting, toolset) {
        return function(value, meta) {
            var ts = (toolset === true) ? (' ip="' + value + '"') : ('');
            if (jQuery.isFunction(highlighting)) { value = highlighting(value, meta); }
            return '<a href="#" class="sw-grid-click" onclick="return false;"' + ts + '>' + value + '</a>';
        };
    };

    $SW.ext.ipAddressToolsetIntegration = function(v, m) {
        return '<a href="#" class="sw-ip-toolset" onclick="return false;" ip="' + v + '">' + v + '</a>';
    };

    $SW.ext.DateRenderer = function (pat) {
        return function (v) {
            if (v != null)
                v = SW.Core.Date.ConvertToDisplayDate(v);
            return Ext.util.Format.date(v, pat);
        };

    };
     
    $SW.ext.scanStatusGridRendererDrilldown = function(highlighting) {
        return function(value, meta,record) 
        {
            var GroupType = record.data['A.Subnet.GroupType'];
            //2048 Zone Type
      
            if (GroupType == 2048) return value;
            
            if (jQuery.isFunction(highlighting))            
            { 
                value = highlighting(value, meta); 
            }
            return "<a href='#' class='sw-grid-click' onclick='return false;'>" + value + "</a>";
        };
    };

    //
    // asp.net + ext validation
    //

    function valid_ctor() {
        var _origupdate, _nsid, _map = {}, _ico = {}, _supd = {}, _ids = [], _loaded = false;

        // register a new validator control
        function Reg(o) {
            if (!_map[o]) {
                _ids.push(o);
                _map[o] = [];
            }
        };

        function UpdateIcon(ctrl) {
            if (!ctrl) return false;

            var icon, redir = ctrl.findParent('.sw-validicon-redir', 8, true), elp =
        redir ||
        ctrl.findParent('.x-form-element', 5, true) ||
        ctrl.findParent('.x-form-field-wrap', 5, true) ||
        ctrl.findParent('.sw-form-item', 5, true);

            if (!elp) return false;
            if (redir) elp = (Ext.get($(redir.dom).find('.sw-validicon-target')[0]) || elp);

            if (!(icon = _ico[elp.dom.id])) {
                var icodiv = elp.createChild({ cls: 'x-form-invalid-icon sw-form-invalid-icon' });
                icon = _ico[elp.dom.id] = { id: icodiv.dom.id, owners: [ctrl.dom.id] };
            }
            else if (jQuery.inArray(ctrl.dom.id, icon.owners) < 0)
                icon.owners.push(ctrl.dom.id);

            var msgs = [], ico = Ext.get(icon.id);

            // gather all the errors.
            jQuery.each(icon.owners, function(i, o) {
                var m = _map[o];
                if (m && m.length)
                    msgs = msgs.concat(m);
            });

            if (msgs.length < 1) {
                ico.qtip = '';
                ico.hide();
            }
            else {
                ico.dom.qtip = msgs.join('<br />');
                ico.dom.qclass = 'x-form-invalid-tip';
                ico.show();
            }

            return true;
        };

        function Touch(c) {
            if (!_loaded) return true;

            var a = _map[c], mark = a ? a.length > 0 : false, ctrl = Ext.get(c);
            if (!UpdateIcon(ctrl))
                return;

            if (mark)
                ctrl.addClass('x-form-invalid');
            else
                ctrl.removeClass('x-form-invalid');

            return true;
        }

        // add a message from an icon (by validator, or by control)
        function Add(val, msg) {
            var a, idx, c = val.controltovalidate || val.id;
            Reg(c);

            if (a = _map[c]) {
                if ((idx = jQuery.inArray(msg, a)) < 0) {
                    a.push(msg);
                    if (_supd[c] = Touch(c)) return;
                }
                else return;
            }

            if (_origupdate) _origupdate(val);
        }

        // remove a message from an icon (by validator, or by control)
        function Del(val, msg) {
            var a, idx, c = val.controltovalidate || val.id;
            Reg(c);

            // remove this msg from the validation queue
            if (a = _map[c]) {
                if ((idx = jQuery.inArray(msg, a)) >= 0) {
                    a.splice(idx, 1);
                    if (_supd[c]) {
                        Touch(c);
                        return;
                    }
                }
                else return;
            }

            if (_origupdate) _origupdate(val);
        }

        // this is where visual update messages are coming through from asp.net validation system
        function Update(val) {
            var msg = val.display == "None" ? val.errormessage : $(val).text();
            if (val.isvalid) Del(val, msg);
            else Add(val, msg);
        }

        // go through all the validators and register / add messages that should normally be visible
        function Init() {
            jQuery.each(window.Page_Validators || [], function(i, o) {
                var jo = $(o), vis = jo.is(':visible'), hid = jo.is(':hidden');
                if (vis && !hid) {
                    if (o.display == 'Dynamic') jo.css('display', 'none');
                    else jo.css('visiblity', 'hidden');
                    Add(o, jo.text());
                }
                else Reg(o.controltovalidate);
            });
            _loaded = true;
            Ext.each(_ids, Touch);
        }

        // get an ordered list of error messages for the entire page
        function Alerts() {
            var rows = [], r, o, f;
            Ext.each(_ids, function(s) {
                if ((o = _map[s]) && o.length) {
                    rows = rows.concat(o);
                    if (!f) f = s;
                }
            });
            if (rows.length) r = '<li>' + rows.join('</li><li>') + '</li>';
            else r = '';
            return { html: r, focus: f };
        }

        function IsVGMatch(control, vg) {
            if ((typeof (vg) == "undefined") || (vg == null)) return true;
            var cg = "";
            if (typeof (control.validationGroup) == "string") cg = control.validationGroup;
            if (vg.constructor == /^/.constructor) return vg.test(cg);
            return (cg == vg);
        }

        // called by the framework to initialize the validation upgrade
        function Hook(nsId) {
            // client now has regex validation group matching
            window.IsValidationGroupMatch = IsVGMatch;

            this.nsId = _nsId = nsId;
            _origupdate = window.ValidatorUpdateDisplay;
            window.ValidatorUpdateDisplay = Update;

            var fn = window.ValidationSummaryOnSubmit;
            if (fn) {
                // the validation summary may use an 'alert' which blocks execution and shows up in the current
                // window. That function is being upgraded to use Ext's alert in a chromeless-aware manner.

                var fnalert = function(s) {
                    var info = Alerts();
                    var fn = function() { $('#' + info.focus).focus(); };
                    $SW.Alert('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_1;E=js}', '<h3>' + '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_2;E=js}' + '</h3><ul class=sw-warnings>' + info.html + '<\/ul>', { buttons: Ext.MessageBox.OK, fn: fn });
                };

                eval(fn.toString().replace(/^[^(]*([^{]*){/, 'window.ValidationSummaryOnSubmit=function$1{var alert=fnalert;'));
            }

            $(document).ready(Init);
        }

        // handy function to cause the forced retesting of a particular form control
        function Retest(ctrl, vgroup) {
            var ret = true;
            jQuery.each(window.Page_Validators || [], function(i, o) {
                if (o.controltovalidate == ctrl.id)
                    window.ValidatorValidate(o, vgroup);
                ret &= o.isvalid;
            });
            return ret;
        }

        function RetestAll(vgroup) {
            var ret = true;
            jQuery.each(window.Page_Validators || [], function(i, o) {
                window.ValidatorValidate(o, vgroup);
                ret &= o.isvalid;
            });
            return ret;
        }

        this.Add = Add;
        this.Del = Del;
        this.Retest = Retest;
        this.RetestAll = RetestAll;
        this.Hook = Hook;
    }

    $SW.Valid = new valid_ctor();

    //
    // ip stuff
    //

    function ipv4(v) {
        if (/[^0-9. ]/.test(v)) v = '';
        return jQuery.map(v.split('.'), function(n, i) {
            var r = parseInt(n, 10);
            return (isNaN(r) || r < 0 || r > 255) ? -1 : r;
        });
    };

    function ipv4_isok(a) { return (a.length == 4) && (jQuery.inArray(-1, a) < 0); }

    function ipv6(v) {
        var scope, parts, pos, expand = -1, more, expto = 8;

        if ((pos = v.indexOf('%')) >= 0) {
            scope = v.substring(pos + 1);
            v = v.substring(0, pos);
        }

        if ((pos = v.indexOf('::')) >= 0) {
            parts = v.substring(0, pos).split(':');
            if (parts.length == 1 && parts[0] == '') parts = [];
            expand = parts.length;
            var p2 = v.substring(pos + 2);
            more = p2.split(':');
            if (p2.substring(0, 1) == ':' || p2.indexOf('::') >= 0 || more.length < 1)
                return [-1];

            parts = parts.concat(more);
            if (more[more.length - 1].indexOf('.') >= 0) expto--;
            while (parts.length < expto) parts.splice(expand, 0, '0');
        }
        else parts = v.split(':');

        if (parts.length == 7 && (parts[6].indexOf('.') >= 0)) {
            var ip = ipv4(parts[6]), n;

            if (!ipv4_isok(ip))
                return [-1];

            parts[6] = (ip[0] * 256 + ip[1]).toString(16);
            parts[7] = (ip[2] * 256 + ip[3]).toString(16);
        }

        var r = jQuery.map(parts, function(n, i) {
            var r = parseInt(n, 16);
            return (isNaN(r) || r < 0 || r > 65535) ? -1 : r;
        });
        r.scope = scope;
        return r;
    };

    function ipv6_isok(a) { return (a.length == 8) && (jQuery.inArray(-1, a) < 0); }

    function ipv4_toint(a) {
        var n = 0;
        jQuery.each(a, function(i, o) { n <<= 8; n += o; });
        return n;
    };

    function ipv4_usecidr(n) {
        var b = [], nmax = 32, j, m, v;
        n = n || 0;
        for (v = 0; nmax; nmax -= 8, b.push(v))
            for (v = 0, m = 128, j = 8; j; m >>>= 1, --n, --j)
            if (n > 0) v |= m;
        return b;
    };

    function h_fillbitsfromleft(num) {
        if (num >= 8) {
            return (255);
        }
        var bitpat = 0xff00;
        while (num > 0) {
            bitpat = bitpat >> 1;
            num--;
        }
        return (bitpat & 0xff);
    }

    function ipv4_cidr(cidr) {
        var mask = [], tmvar;
        tmpvar = parseInt(cidr, 10);
        for (var i = 0; i < 3; i++) {
            if (tmpvar >= 8) {
                mask[i] = 255;
            } else {
                mask[i] = h_fillbitsfromleft(tmpvar);
            }
            tmpvar -= 8;
        }
        mask[3] = h_fillbitsfromleft(tmpvar);
        return mask;
    }

    function ipv4_ismask(a) {
        var last = 255, ok = true;
        jQuery.each(a, function(i, n) {
            if (last == 255) {
                if (!/^25[542]|24[80]|224|192|128|0$/.test('' + n)) return ok = false;
                last = n;
            }
            else if (n) return ok = false;
        });
        return ok;
    };

    function ipv4_issubnet(a, mask) {
        for (var i = 0; i < 4; ++i)
            if ((a[i] & mask[i]) != a[i])
            return false;
        return true;
    };

    function ipv4_subnet(a, mask) {
        var s = [], e = [], i, imax, n, m;
        for (i = 0, imax = mask.length; i < imax; ++i) {
            m = mask[i];
            s.push(n = (a[i] & m));
            e.push(n |= ~m & 0xff);
        }
        return { start: s, end: e };
    };

    function ipv6_fromguid(guid, scope) {
        if (guid == null || guid == '') return '';
        if (scope == '0' || !scope) scope = '';

        guid = guid.replace(/[-]/g, '');
        var parts = [];
        for (var i = 0; i < 32; i += 4)
            parts.push(guid.substr(i, 4).toLowerCase() /*.replace(/^0+/,'0').replace(/^0+([^0])/, '$1' )*/);

        /*
        var largest = 0, largepos = -1, c;
        for( var j = 0; j < 16; ++j )
        {
        c = 0;
        for( var i = j; i < 16; ++i )
        if( parts[i] == '0' ) ++c;
        else break;
        if( largest < c ) { largest = c; largepos = j; }
        }

    if( largest > 0 )
        {
        parts[largepos+largest-1] = '';
        parts.splice(largepos,largest-1);
        if( !largepos ) parts.splice(0,0,'');
        if( largepos+largest>=8 ) parts.push('');
        }
        */

        guid = parts.join(':');

        var s = '';
        if (scope != undefined && scope != '') s = '%' + scope;
        return guid + s;
    };

    var int_to_ipv4 = function (n) {
        var a = [];
        for (var i = 3; i >= 0; i--) {
            a[i] = (n - ((n >> 8) << 8)); n >>= 8;
        }
        return a;
    };

    $SW.IP =
  {
      v4: ipv4,
      v6: ipv6,
      v4_cidr: ipv4_cidr,
      v4_subnet: ipv4_subnet,
      v4_usecidr: ipv4_usecidr,
      v4_isok: ipv4_isok,
      v6_isok: ipv6_isok,
      v4_ismask: ipv4_ismask,
      v4_issubnet: ipv4_issubnet,
      v4_toint: ipv4_toint,
      v4_fromint: int_to_ipv4,
      v6_fromguid: ipv6_fromguid
  };

    //
    // validators
    //

    function getother(src, args, attr, nofailmsg) {
        var o, oid, c, msg;

        if (o = $(src).attr(attr)) {
            oid = $SW.nsGet($SW.Valid.nsId, o);
            if (c = $('#' + oid)[0]) return c;
            msg = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_309;E=js}"
        }
        else {
            msg = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_310;E=js}";
        }

        if (!nofailmsg) $SW.Valid.Add(src, String.format(msg, attr, src.id, o));
        args.IsValid = false;
        return null;
    };

    function isValidIpv4(ipValue) {
        var ipValues = ipv4(ipValue);
        return ipv4_isok(ipValues) && ipValue == ipValues.join(".");
    }

    function checkipv4(src, args) {
        return args.IsValid = isValidIpv4(args.Value);
    }

    function checkipv6(src, args) {
        return args.IsValid = ipv6_isok(ipv6(args.Value));
    }

    function checkipv4mask(src, args) {
        var a = ipv4(args.Value);
        if (!isValidIpv4(args.Value) || !ipv4_ismask(a)) return args.IsValid = false;
        if (other = getother(src, args, 'OtherValidator', true)) window.ValidatorValidate(other);
        return args.IsValid = true;
    }

    function checkipv4subnet(src, args) {
        var other, mask;
        // this checks only if mask & ip are okay
        var a = ipv4(args.Value);
        if (!isValidIpv4(args.Value)) return args.IsValid = true;
        if (!(other = getother(src, args, 'OtherControl', true))) return false;
        if (!ipv4_isok(mask = ipv4(other.value))) return args.IsValid = true;
        return args.IsValid = ipv4_issubnet(a, mask);
    }

    function checkipv4cidr(src, args) {
        var other, a, v;
        // this checks cidr if mask is okay
        if (/[^0-9]/.test(args.Value)) return args.IsValid = false;
        v = parseInt(args.Value, 10);
        if (v < 0 || v > 32) return args.IsValid = false;
        if (!(other = getother(src, args, 'OtherControl'))) return false;
        if (!ipv4_isok(a = ipv4(other.value))) return args.IsValid = true;
        mask = ipv4_usecidr(v);
        return args.IsValid = (a[0] == mask[0] && a[1] == mask[1] && a[2] == mask[2] && a[3] == mask[3]);
    }

    function AsyncAddressValidator(src, arg) {
        var addr = getother(src, arg, 'AddressControl');
        var cidr = getother(src, arg, 'CidrControl');
        if (!addr || !cidr) return false;
        var subnet = $(src).attr('SubnetId');
        if (subnet == null) subnet = -1;
        var clusterId = $(src).attr('ClusterId');
        if (clusterId == null) clusterId = -1;
        var res, opt = { addr: addr.value, cidr: cidr.value, subnet: subnet, clusterId: clusterId };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Orion/IPAM/services/IPv4Provider.asmx/ValidateAddress",
            data: Ext.util.JSON.encode(opt),
            dataType: "json",
            async: false,
            success: function(r) { res = r.d; }
        });
        arg.IsValid = res.isvalid;
        if (!res.isvalid) {
            $SW.Valid.Del(src, src.errormessage);
            src.display = "None";
            src.errormessage = res.message;
        }
        return true;
    }

    function checklink(src, args) {
        var otherc, otherv;
        if (!(otherc = getother(src, args, 'OtherControl', true)) && !(otherv = $(src).attr('OtherValidators'))) {
            $SW.Valid.Add(src, String.format("OtherControl nor OtherValidators could be used for control '{1}'.", src.id));
            return args.IsValid = false;
        }

        if (otherc)
            Retest(otherc, src.validationGroup);

        if (otherv) {
            jQuery.each(otherv.split(','), function(i, o) {
                if (o = jQuery.trim(o)) {
                    var ctrl = $SW.ns$($SW.Valid.nsId, o)[0];
                    if (ctrl) window.ValidatorValidate(ctrl);
                }
            });
        }

        return args.IsValid = true;
    }

    function checkipv4subnetwithinparent(src, args) {
        var pdigitarray, paddr, pmaskdigitarray, pcidr, subnetobj, pstart_n, pend_n, ip_n;

        paddr = src.attributes['parentAddr'].value;
        pdigitarray = $SW.IP.v4(paddr);

        digitarray = $SW.IP.v4(args.Value);
        if (!$SW.IP.v4_isok(digitarray)) return args.IsValid = false;

        //Get mask for parent
        pcidr = src.attributes['parentCIDR'].value;
        pmaskdigitarray = $SW.IP.v4_cidr(pcidr);
        if (!$SW.IP.v4_isok(pmaskdigitarray)) return args.IsValid = false;

        //Get the start and the end for the subnet with:
        psubnetobj = ipv4_subnet(pdigitarray, pmaskdigitarray);
        pstart_n = $SW.IP.v4_toint(psubnetobj.start);
        pend_n = $SW.IP.v4_toint(psubnetobj.end);
        ip_n = $SW.IP.v4_toint(digitarray);

        return args.IsValid = (ip_n >= pstart_n && ip_n <= pend_n);
    }

    function checkipv4ipcomparator(src, args) {
        var startingip_n, endingip_n, other;

        if (!(other = getother(src, args, 'OtherControl'))) return false;

        if (!isValidIpv4(args.Value)) return args.IsValid = true;
        if (!ipv4_isok(ipv4(other.value))) return args.IsValid = true;

        // this checks only if starting &  ending ip are okay
        startingip_n = $SW.IP.v4_toint($SW.IP.v4(args.Value));
        endingip_n = $SW.IP.v4_toint($SW.IP.v4(other.value));

        return args.IsValid = (endingip_n >= startingip_n);
    }

    function checkipv4numberinrange(src, args) {
        var startingip_n, endingip_n, other, ipnumber;

        if (!(other = getother(src, args, 'OtherControl'))) return false;

        if (!isValidIpv4(args.Value)) return args.IsValid = true;
        if (!ipv4_isok(ipv4(other.value))) return args.IsValid = true;

        // this checks only if starting &  ending ip are okay
        startingip_n = $SW.IP.v4_toint($SW.IP.v4(args.Value));
        endingip_n = $SW.IP.v4_toint($SW.IP.v4(other.value));
        ipnumber = endingip_n - startingip_n;
        return args.IsValid = (ipnumber <= 262144);
    }

    $SW.Valid.Fns = {
        ipv4: checkipv4,
        ipv6: checkipv6,
        ipv4mask: checkipv4mask,
        ipv4subnet: checkipv4subnet,
        ipv4cidr: checkipv4cidr,
        link: checklink,
        ipv4subnetwithinparent: checkipv4subnetwithinparent,
        ipv4ipcomparator: checkipv4ipcomparator,
        ipv4ipnumberinrange: checkipv4numberinrange,
        ipv4Async: AsyncAddressValidator
    };

    //
    // chromeless compatible window popup
    //

    $SW.Alert = function(title, msg, cfg) {
        cfg = cfg || {};
        if (title) cfg.title = title;
        if (msg) cfg.msg = msg;
        if (!cfg.minWidth) cfg.minWidth = 720;

        if (window.parent != window && window.parent.Ext) {
            window.parent.Ext.Msg.show(cfg);
            return;
        }
        Ext.Msg.show(cfg);
    };

    // 
    // search menu
    // 
    Ext.menu.SearchMenu = Ext.extend(Ext.menu.Menu, {
        showAt: function(xy, parentMenu, /* private: */_e) {
            this.parentMenu = parentMenu;
            if (!this.el) { this.render(); }
            if (_e !== false) {
                this.fireEvent("beforeshow", this);
                xy = this.el.adjustForConstraints(xy);
            }
            this.el.setXY(xy);

            // get max height from body height minus y cordinate from this.el
            var maxHeight = Ext.getBody().getHeight() - xy[1];

            // decrease by footer size from cfg
            if (this.footerSize) maxHeight -= this.footerSize;

            // store orig element height
            if (!this.el.origHeight) {
                this.el.origHeight = this.el.getHeight();
            }
            // if orig height bigger than max height
            if ((this.el.origHeight > maxHeight) && (maxHeight > 0)) {
                // set element with max height and apply scrollbar
                this.setSizeEx(maxHeight);
                this.el.applyStyles('overflow-y: auto;');
            } else {
                // set the orig height
                this.setSizeEx(this.el.origHeight);
            }

            this.el.show();
            this.hidden = false;
            this.focus();
            this.fireEvent("show", this);
        },
        setSizeEx: function(h) {
            if (h < 150) h = 150;
            this.el.setHeight(h);
            if (this.pel && this.pel.offsetWidth) {
                var w = this.pel.offsetWidth;
                if (this.twin) w -= 17;
                this.el.setWidth(w);
            }
        }
    });

    $SW.SearchMenuClick = function(e, el) {
        if (!this.menu) {
            var i = this.initialConfig, c = i && i.menuConfig && i.menuConfig.menu;
            if (!c) return;
            this.menu = new Ext.menu.SearchMenu(c);
        }

        this.menu.twin = this.twin;
        this.menu.pel = $(this.el.dom).parent('div')[0];
        this.menu.show(this.menu.pel, 'tl-bl?');
    };

    $SW.SearchItemClick = function(that) {
        var box = Ext.getCmp(that.initialConfig['x-search-id']),
        ord = that.initialConfig['x-search-ord'],
        check = !that.checked;
        if (!box) return;

        var item = box.initialConfig.menuConfig.columns[ord];
        if (item) item.checked = check;

        $SW.SearchAllSync(box, check);
    };

    $SW.SearchAllSync = function(box, check) {
        // handle only un-check
        if (check == true) return;

        if (!box.menu || !box.menu.items || !box.menu.items.items) return;
        Ext.each(box.initialConfig.menuConfig.columns, function(item, n) {
            if (item.isAllField && item.checked) {
                // set field for postback
                item.checked = false;
                // set checkbox
                var chck = box.menu.items.items[n];
                if (chck && chck.setChecked) chck.setChecked(false);
            }
        });
    };

    $SW.SearchAllClick = function(that) {
        var box = Ext.getCmp(that.initialConfig['x-search-id']),
        ord = that.initialConfig['x-search-ord'],
        check = !that.checked;
        if (!box) return;

        var allItem = box.initialConfig.menuConfig.columns[ord];
        if (allItem) allItem.checked = check;

        $SW.SearchItemsSync(box, ord, check);
    };

    $SW.SearchItemsSync = function(box, ord, check) {
        if (!box.menu || !box.menu.items || !box.menu.items.items) return;
        Ext.each(box.initialConfig.menuConfig.columns, function(item, n) {
            if (!item.isAllField && (item.text != "separator")) {
                // set field for postback
                item.checked = check;
                // set checkbox
                var chck = box.menu.items.items[n];
                if (chck && chck.setChecked) chck.setChecked(check);
            }
        });
    };

    $SW.SearchGo = function(e) {
        var that = this;
        var v = that.getValue();
        e.stopEvent();
        if (v == '') {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_16;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_17;E=js}',
                buttons: Ext.Msg.OK,
                animEl: that,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }

        var keys = [];
        var menuConfig = that.initialConfig.menuConfig;

        Ext.each(menuConfig.columns, function(o) {
            if (o.checked == undefined || o.checked)
                keys.push(o.key);
        });

        if (keys.length < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_16;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_18;E=js}',
                buttons: Ext.Msg.OK,
                animEl: that,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }

        var history = '';
        var timeperiod = '';
        if ($SW.search && $SW.search.timeperiod) {
            history = $SW.search.timeperiod.history;
            timeperiod = $SW.search.timeperiod.timeperiod;
        }

        var dh = Ext.DomHelper;
        var form = dh.append(Ext.getBody(), {
            tag: 'form',
            action: menuConfig.action + "?q=" + encodeURIComponent(v),
            method: menuConfig.method || 'POST',
            children:
        [
            { tag: 'input', type: 'hidden', name: 'columns', value: keys.join(',') },
            { tag: 'input', type: 'hidden', name: 'storecolumns', value: menuConfig.storecolumns },
            { tag: 'input', type: 'hidden', name: 'history', value: history },
            { tag: 'input', type: 'hidden', name: 'timeperiod', value: timeperiod }
        ]
        });

        form.submit();
    };

    $SW.SearchInit = function(that, info) {
        var box = that;
        var columns = info.columns;
        var storecolumns = info.storecolumns;
        var menuConfig = box.initialConfig.menuConfig;
        if (!menuConfig) menuConfig = box.initialConfig.menuConfig = {};
        Ext.applyIf(menuConfig, info);
        var menu = menuConfig.menu;
        if (!menu) menu = menuConfig.menu = {};
        var items = menu.items;
        if (!items) items = menu.items = [];

        Ext.each(columns, function(o, n) {
            if (o.text == "separator")
                items.push(new Ext.menu.Separator());
            else {
                items.push(new Ext.menu.CheckItem({
                    'x-search-id': that.id,
                    'x-search-ord': n,
                    handler: (o.isAllField ? $SW.SearchAllClick : $SW.SearchItemClick),
                    text: o.text,
                    hideOnClick: false,
                    checked: (o.checked == undefined ? true : o.checked)
                }));
            }
        });

        menuConfig.menu.footerSize = 50;
        menuConfig.storecolumns = storecolumns;
    };

    $SW.TransformDropDown = function(id, extid, opts) {

        var item = $('#' + id)[0];
        if (!item) return null;

        var o = {
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            disabled: item.disabled || false,
            width: 'auto'
        };

        if (extid) o.id = extid;

        return new Ext.form.ComboBox(Ext.apply(o, opts || {}));
    };

    $SW.SearchResultItemClick = function(that) {
        var filterName = that.initialConfig['x-search-id'];
        if (!filterName) return;

        $SW.SearchItemClick(that);
        $SW.FillSearchFilter(filterName);
    };

    $SW.FillSearchFilter = function(filterName) {
        var box = Ext.getCmp(filterName);
        if (!box) return;

        var keys = [];
        var isAllFields = false;
        var menuConfig = box.initialConfig.menuConfig;
        Ext.each(menuConfig.columns, function(o) {
            if (o.checked == undefined || o.checked)
            { keys.push(o.key); if (o.key == 'All Fields') isAllFields = true; }
        });
        if (isAllFields) box.setRawValue('All Fields');
        else box.setRawValue(keys.join(','));
    };

    $SW.SearchAllResultClick = function(that) {
        var filterName = that.initialConfig['x-search-id'];
        if (!filterName) return;

        $SW.SearchAllClick(that);
        $SW.FillSearchFilter(filterName);
    };

    $SW.SearchResultGo = function(thatName, filterName) {
        var that = $(thatName)[0];
        var filter = Ext.getCmp(filterName);

        var v = that.value;
        if (v == '') {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_16;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_17;E=js}',
                buttons: Ext.Msg.OK,
                animEl: that,
                icon: Ext.MessageBox.ERROR
            });
            return false;
        }

        var keys = [];
        var menuConfig = filter.initialConfig.menuConfig;

        Ext.each(menuConfig.columns, function(o) {
            if (o.checked == undefined || o.checked)
                keys.push(o.key);
        });

        if (keys.length < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_16;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_TM0_18;E=js}',
                buttons: Ext.Msg.OK,
                animEl: filter,
                icon: Ext.MessageBox.ERROR
            });
            return false;
        }

        var dh = Ext.DomHelper;
        var form = dh.append(Ext.getBody(), {
            tag: 'form',
            action: menuConfig.action + "?q=" + encodeURIComponent(v),
            method: menuConfig.method || 'POST',
            children:
        [
            { tag: 'input', type: 'hidden', name: 'columns', value: keys.join(',') },
            { tag: 'input', type: 'hidden', name: 'storecolumns', value: menuConfig.storecolumns },
            { tag: 'input', type: 'hidden', name: 'history', value: '' },
            { tag: 'input', type: 'hidden', name: 'timeperiod', value: '' }
        ]
        });

        form.submit();
        return false;
    };

    $SW.SearchResultInit = function(that, info) {
        var box = that;
        var columns = info.columns;
        var storecolumns = info.storecolumns;
        var menuConfig = box.initialConfig.menuConfig;
        if (!menuConfig) menuConfig = box.initialConfig.menuConfig = {};
        Ext.applyIf(menuConfig, info);
        var menu = menuConfig.menu;
        if (!menu) menu = menuConfig.menu = {};
        var items = menu.items;
        if (!items) items = menu.items = [];
        var searchid = info.searchid;
        if (!searchid) searchid = that.id;

        Ext.each(columns, function(o, n) {
            if (o.text == "separator")
                items.push(new Ext.menu.Separator());
            else {
                items.push(new Ext.menu.CheckItem({
                    'x-search-id': searchid,
                    'x-search-ord': n,
                    handler: (o.isAllField ? $SW.SearchAllResultClick : $SW.SearchResultItemClick),
                    text: o.text,
                    hideOnClick: false,
                    checked: (o.checked == undefined ? true : o.checked)
                }));
            }
        });

        $SW.FillSearchFilter(searchid);
    };


    if (!$SW.renderers)
        $SW.renderers = {};

    $SW.renderers.CustomLinkRenderer = function() {
        return function(value, meta) {
            var ret = value;
            if (ret) {
                var str = new String(value).trim();
                var check = str.toLowerCase();
                if ($SW.renderers.ValidUrl(check)) {                
                    ret = "<a class='sw-grid-customlink' target='_blank' href='" + str + "'>" +
                    Ext.util.Format.htmlEncode(value) + "</a>";
                }
            }

            return ret;
        };
    };
    
    $SW.renderers.ValidUrl = function(str) {
        var regex = /^(https?:\/\/)((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/;
        return regex.test(str);
    }
    
    $SW.renderers.cancelBubble = function (evt) {
        if (evt.stopPropagation) evt.stopPropagation();
        if (evt.cancelBubble != null) evt.cancelBubble = true;
    }

    $SW.renderers.CustomLinkTemplate = function(dataIndex) {
        var di = dataIndex || 'value';

        return new Ext.XTemplate(
            '<tpl if="this.isLink(values)">',
            '<a style="text-decoration:underline" href="{' + dataIndex + ':this.formatLink()}" target="_blank" onclick="var event = arguments[0] || window.event; $SW.renderers.cancelBubble(event);";>{[this.LinkTitle(values)]}</a>',
            '</tpl>',
            '<tpl if="!this.isLink(values)">',
            '<p>{'+dataIndex+'}</p>',
            '</tpl>', {
                isLink: function(v) {
                    if (!v || !v[di]) return false;
                    return $SW.renderers.ValidUrl(v[di]);
                },
                LinkTitle: function(v) {
                    var val = (v && v[di]) ? new String(v[di]).trim() : "Link";
                    return Ext.util.Format.htmlEncode(val);
                },
                formatLink: function(v) {
                    return (v ? new String(v).trim() : "");
                }
            });
    };

    $SW.renderers.IntegrationLinkRenderer = function(title, href) {
        var di = {title: title, href: href};

         return function(value, meta, r) {
            var href = r.data[di.href];
            var title = r.data[di.title];
            if (href && title) {
                return "<a class='sw-grid-click' target='_blank' href='" + href + "'>" + Ext.util.Format.htmlEncode(title) + "</a>";
            } else if (value) {
                return Ext.util.Format.htmlEncode(value);
            }
            return "";
        };
    };

    $SW.renderers.UDTUsersRenderer = function(dataIndex) {
        var di = dataIndex || 'udt_users';
        var usersTpl = new Ext.XTemplate(
            '<ul><tpl for=".">',
                '<tpl if="xindex &lt; 11">',
                '<li style="display: inline">',
                    '<tpl if="this.isEmpty(u_nm)">...</tpl>',
                    '<tpl if="!this.isEmpty(u_nm)"><a class="sw-grid-click" href="/Orion/UDT/UserDetails.aspx?NetObject=UU:{u_id}">{u_nm}</a></tpl>',
                    '<tpl if="xindex<xcount">,</tpl>',
                '</li>',
                '</tpl>',
                '<tpl if="xindex == 11">',
                '<li style="display: inline;">...</li>',
                '</tpl>',
            '</tpl></ul>',
            { isEmpty: function (data) { return Ext.isEmpty(data); } }
        );
        return function(value, meta, r) {
            meta.css = 'udt-users-list';
            if (r && r.data) {
                var data = Ext.util.Format.htmlDecode(r.data[di]);
                var users = Ext.util.JSON.decode(data);
                if(Ext.isArray(users)) return usersTpl.apply(users);
            }
            return "";
        };
    };

    $SW.renderers.UDTPortsRenderer = function(dataIndex) {
        var di = dataIndex || 'udt_switch';
        var switchesTpl = new Ext.XTemplate(
            '<ul><tpl for=".">',
                '<tpl if="xindex &lt; 11">',
                '<li style="display: inline">',
                    '<tpl if="this.isEmpty(s_nm)">...</tpl>',
                    '<tpl if="!this.isEmpty(s_nm)"><a class="sw-grid-click" href="{s_url}">{s_led:this.IconSwitch}{s_nm}</a></tpl>',
                    '&nbsp;(',
                        '<tpl if="this.isEmpty(p_nm)"><span style="font-size:8pt">Unk</span></tpl>',
                        '<tpl if="!this.isEmpty(p_nm)"><a class="sw-grid-click" href="{p_url}">{p_nm}</a></tpl>',
                    ')',
                    '<tpl if="xindex<xcount">,</tpl>',
                '</li>',
                '</tpl>',
                '<tpl if="xindex == 11">',
                '<li style="display: inline;">...</li>',
                '</tpl>',
            '</tpl></ul>',
            {
                isEmpty: function (data) { return Ext.isEmpty(data); },
                IconSwitch: function(v){ if(Ext.isEmpty(v)) v = 'Unknown.gif';
                    return '<img src="/Orion/images/StatusIcons/Small-'+v+'" class="StatusIcon" style="vertical-align: bottom">'; },
                IconPort: function(v){   if(Ext.isEmpty(v)) v = 'Unknown.gif';
                    return '<img src="/Orion/IPAM/res/images/sw-udt/icon_port_'+v+'_dot.gif" style="vertical-align: bottom">'; }
            }
        );

        var handleEmptyData = function(data){
            
        };

        return function(value, meta, r) {
            meta.css = 'udt-switch-list';
            if (r && r.data) {
                var data = Ext.util.Format.htmlDecode(r.data[di]);
                var switches = Ext.util.JSON.decode(data);
                if(Ext.isArray(switches)){ return switchesTpl.apply(switches); }
            }
            return "";
        };
       
      };
})();


