﻿(function () {

    if (!$SW.IPAM) $SW.IPAM = {};
    
    $SW.IPAM.GetSharedNetworkSelectHandler = function (sharedNetworkSelector,nodeId,defaultToSelect) {

        if (!sharedNetworkSelector) return null;
        
        var setEmptyText = function (text) {
            sharedNetworkSelector.emptyText = text;
            sharedNetworkSelector.clearValue();
        };
        
        sharedNetworkSelector.clearValue();
        sharedNetworkSelector.store.clearData();
        sharedNetworkSelector.disabled = true;

        var fail = $SW.ext.AjaxMakeFailDelegate('Get list of shared networks');
        var success = $SW.ext.AjaxMakePassDelegate('Get list of shared  networks.', function (r, request) {

            if (!r || !r.data || r.data.length == 0) {
                setEmptyText("@{R=IPAM.Strings;K=IPAMWEBJS_SE_43;E=js}");
                return;
            }

            sharedNetworkSelector.valueField = 'IDS';
            sharedNetworkSelector.displayField = 'NetworkName';
            sharedNetworkSelector.bindStore(store);
            sharedNetworkSelector.store.clearData();
            
            sharedNetworkSelector.store.on("load", function () {
                sharedNetworkSelector.disabled = false;
                setEmptyText("@{R=IPAM.Strings;K=IPAMWEBJS_SE_41;E=js}" + ' ');
                var index = store.find('SharedNetID', defaultToSelect);
                
                var selectedObj = sharedNetworkSelector.store.getAt(index);
                if (selectedObj) {
                    sharedNetworkSelector.setValue(selectedObj.data.IDS);
                    $SW.IPAM.AssignValueToHdn(selectedObj.data.IDS, selectedObj.data.NetworkName);
                }

            });

            sharedNetworkSelector.store.loadData(r.data);
        }, this);

        // set proper combobox store
        var store = new Ext.data.JsonStore({
            data: [],
            fields: [
                { name: 'NetworkName', type: 'string' },
                { name: 'IDS', type: 'string' },
                { name: 'SharedNetID', type: 'string' },
                { name: 'AvailabeCount', type: 'string' }
            ]
        });
        
        if (!nodeId) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_190;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_191;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }
        
        sharedNetworkSelector.setValue = function (v) {
            Ext.form.ComboBox.prototype.setValue.call(this, v);
            if (this.rendered && this.el) {
                this.el.dom.innerHTML = this.value;
            }
            return this;
        };
        
        //call ajax to get shared networks for node id
        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            failure: fail,
            success: success,
            timeout: fail,
            params: {
                entity: 'IPAM.SharedNetwork',
                verb: 'GetSharedNetworkById',
                nodeId: nodeId
            }
        });
    };

    $SW.IPAM.InitMaskCalculation = function (nsId, cidrId, maskId, chbId) {
        update_checkbox();
        function read_cidr(cidrbox) {
            if (!cidrbox || /[^0-9]/.test(cidrbox.value)) return null;
            var cidr = parseInt(cidrbox.value);
            if (isNaN(cidr) || cidr < 0 || cidr > 32) return null;
            return cidr;
        }

        function update_mask(cidr, maskbox) {
            if (!cidr || !maskbox) return;
            maskbox.value = $SW.IP.v4_usecidr(cidr).join('.');
            $SW.Valid.Retest(maskbox);
        }

        function update_checkbox() {
            var cidr = $SW.ns$(nsId, 'txtCidr')[0].value;
            var checkbox = $SW.ns$(nsId, 'cbAddIPAddresses');
            if (!cidr || !checkbox) return;
            var chb = checkbox[0], wrap = checkbox.parent();
            if (chb) chb.disabled = (cidr < 20);
            if (cidr < 20) {
                wrap.addClass('x-item-disabled');
                $('#subnettoolarge').removeClass('sw-form-disabled').css("display", "");
            } else {
                wrap.removeClass('x-item-disabled');
                $('#subnettoolarge').css("display", "none").removeClass('sw-form-disabled');
            }
        }

        function getCidrUpdatedFn(maskbox, checkbox) {
            return function (e) {
                e = e || window.event;
                if (e.keyCode == 9) return; // ignore keyup on a tab
                var cidr = read_cidr(e.target || e.srcElement);
                update_mask(cidr, maskbox);
                update_checkbox();
            };
        };

        var cidrTxtBox = $SW.ns$(nsId, cidrId);
        var maskTxtBox = $SW.ns$(nsId, maskId)[0];
        var checkBox = $SW.ns$(nsId, chbId);
        cidrTxtBox.bind('change keyup', getCidrUpdatedFn(maskTxtBox, checkBox));
    };
    $SW.IPAM.SubNetCalculation = function (nsId) {
        var maskbox = $SW.ns$(nsId, 'txtNetmask'),
            cidrbox = $SW.ns$(nsId, 'txtCidr'),
            namebox = $SW.ns$(nsId, 'txtScopeName'),
            ipbox = $SW.ns$(nsId, 'txtNetworkAddress');
            update_checkbox();
        //if (ipbox[0] && namebox[0])
        namebox.bind('change keyup', function() { sync_fields(true); });

        function update_checkbox() {
            var cidr = $SW.ns$(nsId, 'txtCidr')[0].value;
            var checkbox = $SW.ns$(nsId, 'cbAddIPAddresses');
            if (!cidr || !checkbox) return;
            var chb = checkbox[0], wrap = checkbox.parent();
            if (chb) chb.disabled = (cidr < 20);
            if (cidr < 20) {
                $('#subnettoolarge').removeClass('sw-form-disabled').css("display", "");
                wrap.addClass('x-item-disabled');
            } else {
                $('#subnettoolarge').css("display", "none").removeClass('sw-form-disabled');
                wrap.removeClass('x-item-disabled');
            }
        }
        if (maskbox[0] && cidrbox[0]) {

            function addressupdated(e) {
                e = e || window.event;
                if (e.keyCode == 9) return; // ignore keyup on a tab
                sync_fields(false);
            };

            function cidrupdated(e) {
                e = e || window.event;
                if (e.keyCode == 9) return; // ignore keyup on a tab

                var mask = mask_fromcidr(e.target || e.srcElement);
                update_netmask(mask);
                sync_fields(false);
                update_checkbox();
            
            };

            cidrbox.bind('change keyup', cidrupdated);
            ipbox.bind('change keyup', addressupdated);
        }
        
        function cidr_frommask(box) {
            var maskArray = box.split('.');
            var sumofbits = 0;
            for (var maskIndex = 0; maskIndex < maskArray.length; maskIndex++)
            {
                tmpvar = parseInt(maskArray[maskIndex], 10);
                if (isNaN(tmpvar)) {
                    return null;
                }
               var bitsfromleft = h_countbitsfromleft(tmpvar);
                if (isNaN(bitsfromleft)) {
                    return null;
                }
                sumofbits += bitsfromleft;
            }
            return sumofbits;
        }
        function h_countbitsfromleft(num) {
            if (num == 255) {
                return (8);
            }
            i = 0;
            bitpat = 0xff00;
            while (i < 8) {
                if (num == (bitpat & 0xff)) {
                    return (i);
                }
                bitpat = bitpat >> 1;
                i++;
            }
            return (Number.NaN);
        }
        function mask_fromcidr(box) {
            if (!box || /[^0-9]/.test(box.value)) return null;
            var cidr = parseInt(box.value);
            if (isNaN(cidr) || cidr < 0 || cidr > 31) return null;
            return $SW.IP.v4_usecidr(cidr).join('.')
        }
        function update_netmask(mask) {
            if (!mask) return;
            d = $SW.ns$(nsId, 'txtNetmask')[0];
            d.value = mask;
            $SW.Valid.Retest(d);
        }
        function update_cidr(cidr) {
            d = $SW.ns$(nsId, 'txtCidr')[0];
            if (!cidr) {
                $SW.Valid.Retest(d);
                return;
            }
            d.value = cidr;
            $SW.Valid.Retest(d);
        }
        function sync_fields(namechanged) {
            var addrbox = $SW.ns$(nsId, 'txtNetworkAddress')[0],
                cidrbox = $SW.ns$(nsId, 'txtCidr')[0],
                namebox = $SW.ns$(nsId, 'txtScopeName')[0],
                maskbox = $SW.ns$(nsId, 'txtNetmask')[0];

            if (!addrbox || !cidrbox || !namebox || !maskbox) return;

            var v = namebox.value.replace(/^\s+|\s+$/g, ''),
                m = v.match(/^([0-9. \t]+)\s*([\\\/])\s*([0-9. \t]+)$/),
                slash = m ? m[2] : '/',
                ip = m ? $SW.IP.v4(m[1]) : null,
                mask = m ?  $SW.IP.v4(m[3]) : null,
                ipok = ip ? $SW.IP.v4_isok(ip) : false,
                maskok = mask ? $SW.IP.v4_isok(mask) : false;

            if (namechanged) {
                if (/^[^?]+edit[.]aspx([?]|$)/i.test(window.location)) {
                    // edit page: do not change the address!
                } else if (ipok && maskok) {
                    addrbox.value = ip.join('.');
                    maskbox.value = mask.join('.');
                    var cidr = cidr_frommask(maskbox.value);
                    update_cidr(cidr);

                } else {
                    if (addrbox.value == '') {
                        var a = $SW.IP.v4(namebox.value),
                            aok = $SW.IP.v4_isok(a),
                            c = $SW.IP.v4(maskbox.value),
                            cok = $SW.IP.v4_isok(c),
                            issubnet = (aok && cok) ? $SW.IP.v4_issubnet(a,c) : false;

                        if (issubnet) {
                            addrbox.value = a.join('.');
                        }
                    }
                }
            } else {
                var a = $SW.IP.v4(addrbox.value),
                    aok = $SW.IP.v4_isok(a),
                    c = $SW.IP.v4(maskbox.value),
                    cok = $SW.IP.v4_isok(c),
                    issubnet = (aok && cok) ? $SW.IP.v4_issubnet(a, c) : false;
                if (issubnet) {
                    if (v == '' || (ipok && maskok))
                        namebox.value = a.join('.') + ' ' + slash + c.join('.');
                    else if ($SW.IP.v4_isok($SW.IP.v4(namebox.value)))
                        namebox.value = a.join('.');
                }

            }
        }
    };

    if (!$SW.IPAM.DHCPExclusions) $SW.IPAM.DHCPExclusions = {};
    Ext.apply($SW.IPAM.DHCPExclusions, {
        GridId: 'ExclusionsGrid',
        getGrid: function () {
            return Ext.getCmp($SW.IPAM.DHCPExclusions.GridId);
        },
        getIPRangeParam: function () {
            var fn = $SW.IPAM.DHCPExclusions.ScopeIPRangeSupplyFn;
            return (fn) ? fn() : '';
        },
        getWebIdParam: function () {
            var w = $SW.IPAM.DHCPExclusions.WebId;
            return (w && w != '') ? '&w=' + w : '';
        },
        AddExclusionBtnPressed: function () {
            return function (el) {
                var params = $SW.IPAM.DHCPExclusions.getIPRangeParam();
                if (params == '' || params.indexOf('start=&') >= 0 || params.indexOf('end=&') >= 0) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_263;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_264;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                var ipAddresses = $SW.IPAM.GetStartEndIpAddresses(params);
                if (!$SW.IPAM.ValidateIpV4Addresses(ipAddresses)) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_263;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_DM1_1;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                params = $SW.IPAM.DHCPExclusions.getWebIdParam() + params;
                $SW.ext.dialogWindowOpen(
                        $SW['exclusionsWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_265;E=js}',
                        '/Orion/IPAM/DhcpScope/Dhcp.Scope.Exclusions.IPRange.aspx?NoChrome=True&msgq=exclusionsWindow' + params,
                        'OrionIPAMAGEditingDHCPScopes.htm');
            };
        },
        EditExclusionBtnPressed: function () {
            return function (el) {
                var params = $SW.IPAM.DHCPExclusions.getIPRangeParam();
                if (params == '' || params.indexOf('start=&') >= 0 || params.indexOf('end=&') >= 0) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_266;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_267;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }

                var grid = $SW.IPAM.DHCPExclusions.getGrid();

                var sm = grid.getSelectionModel(), c = sm.getCount();
                if (c < 1 || c > 1) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_268;E=js}',
                        msg: 'Please select a single Exclusion before clicking "Edit".',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    var item = sm.getSelected();
                    params = $SW.IPAM.DHCPExclusions.getWebIdParam() + params;
                    $SW.ext.dialogWindowOpen(
                        $SW['exclusionsWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_269;E=js}',
                        '/Orion/IPAM/DhcpScope/Dhcp.Scope.Exclusions.IPRange.aspx?NoChrome=True&msgq=exclusionsWindow&ObjectId=' + item.data['ID'] + params,
                        'OrionIPAMAGEditingDHCPScopes.htm');
                }
            };
        },
        RemoveExclusionBtnPressed: function () {
            return function (el) {
                var grid = $SW.IPAM.DHCPExclusions.getGrid();

                var sm = grid.getSelectionModel(), c = sm.getCount();

                if (c == 0) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_268;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_270;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.WARNING
                    });
                } else {
                    var items = sm.getSelections();
                    var list = [];
                    Ext.each(items, function (o) {
                        if (o.id) list.push(o.id);
                    });

                    var title = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_272;E=js}";
                    var msg = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_271;E=js}";
                    var fail = $SW.ext.AjaxMakeFailDelegate("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_273;E=js}");

                    Ext.Msg.show({
                        title: title,
                        msg: msg,
                        buttons: Ext.Msg.YESNO,
                        width: 400,
                        icon: Ext.MessageBox.QUESTION,
                        fn: function (btn) {
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url: $SW.appRoot() + "Orion/IPAM/SessionDataProvider.ashx",

                                    success: function (result, request) {
                                        var res = Ext.util.JSON.decode(result.responseText);
                                        if (res.success == true) {
                                            $SW.IPAM.DHCPExclusions.RefreshGrid();
                                        } else {
                                            fail(res.message);
                                        }
                                    },
                                    failure: fail,
                                    timeout: fail,

                                    params: {
                                        entity: 'IPAM.DhcpExclusions',
                                        verb: 'Remove',
                                        ids: list.join(','),
                                        w: $SW.IPAM.DHCPExclusions.WebId
                                    }
                                });
                            }
                            return true;
                        }
                    });
                }
            };
        },
        SelectionChangedPlugin: function (config) {
            Ext.apply(this, config);
        },
        RefreshGrid: function () {
            var grid = $SW.IPAM.DHCPExclusions.getGrid();
            var store = grid.getStore();
            store.reload();
        },
        FilterBy: function (fn) {
            var grid = $SW.IPAM.DHCPExclusions.getGrid();
            var store = grid.getStore();
            store.filterBy(fn);
        },
        ClearFilter: function () {
            var grid = $SW.IPAM.DHCPExclusions.getGrid();
            var store = grid.getStore();
            store.clearFilter();
        }

    });
    Ext.extend($SW.IPAM.DHCPExclusions.SelectionChangedPlugin, Ext.util.Observable, {
        SelectionChanged: function () {
            var btnAdd = Ext.getCmp(this.add);
            var btnEdit = Ext.getCmp(this.edit);
            var btnRemove = Ext.getCmp(this.remove);
            return function (sm) {
                if (btnAdd) btnAdd.enable();
                if (btnEdit && sm.getCount() == 1) btnEdit.enable(); else btnEdit.disable();
                if (btnRemove && sm.getCount() > 0) btnRemove.enable(); else btnRemove.disable();
            };
        },
        init: function (grid) {
            grid.getSelectionModel().on('selectionchange', this.SelectionChanged());
        }
    });
    $SW.IPAM.DhcpScopeErrMsgTpl = new Ext.XTemplate(
         '<tpl if="!this.hasErrs(results)">{message:this.toHtml}</tpl>',
         '<tpl if="this.hasErrs(results)">',
                '<tpl for="results"><tpl if="this.isValid(ErrorCode)">',
                    '<br>■ &ensp; {.:this.translate}',
                '</tpl></tpl>',
        '</tpl>',
        {
            toHtml: function (msg) {  return Ext.util.Format.htmlEncode(msg); },
            hasErrs: function (rs) {  return (rs && rs.length > 0); },
            isValid: function (ec) {  return (ec != undefined && ec != 0); },
            translate: function (err) {
                if (err.ErrorCode == "9822") {
                    var data = Ext.util.JSON.decode(err.ErrorMessage);
                    return new Ext.XTemplate('@{R=IPAM.Strings;K=IPAMWEBJS_OH1_14;E=js}').apply(data);
                } else if (err.ErrorCode == "9823") {
                    var data = Ext.util.JSON.decode(err.ErrorMessage);
                    return new Ext.XTemplate('@{R=IPAM.Strings;K=IPAMWEBJS_OH1_15;E=js}').apply(data);
                } else if (err.ErrorCode == "74") {
                    return '<b>' + "Error While Managing: " + '</b>&nbsp;' + err.ErrorMessage;
                }
                else {
                    return  err.ErrorMessage;
                }
            }
        }
    );

    $SW.IPAM.DhcpScopeWarnMsgTpl = new Ext.XTemplate(
        '{message:this.toHtml}',
        '<br /><hr>',
            '<tpl for="results"><tpl if="!Success">',
                '<br>■ &ensp; {.:this.translate}',
            '</tpl></tpl>',
        {
            toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
            translate: function (err) {
                if (err.Method === "SetSubnetDelayOffer") {
                    var message = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_276;E=js}';
                    if (err.ErrorServer)
                        message += " " + err.ErrorServer;
                    return message;
                }
                if (err.ErrorCode == "9820") {
                    var data = Ext.util.JSON.decode(err.ErrorMessage);
                    return new Ext.XTemplate('@{R=IPAM.Strings;K=IPAMWEBJS_OH1_12;E=js}').apply(data);
                }
                if (err.ErrorCode == "9821") {
                    var data = Ext.util.JSON.decode(err.ErrorMessage);
                    return new Ext.XTemplate('@{R=IPAM.Strings;K=IPAMWEBJS_OH1_13;E=js}').apply(data);
                }
                if (err.ErrorCode == "9825") {
                    var data = Ext.util.JSON.decode(err.ErrorMessage);
                    return new Ext.XTemplate('@{R=IPAM.Strings;K=IPAMWEBJS_OH1_16;E=js}').apply(data);
                }
                return '<b>' + err.Method + '</b>&nbsp;' + err.ErrorMessage;
            }
        }
    );

    $SW.IPAM.GetStartEndIpAddresses = function (paramsString) {
        var startIp = paramsString.split('start=').pop().split('&end').shift();
        var endIp = paramsString.split('end=').pop().split('&cidr').shift();

        return [startIp, endIp];
    }

    $SW.IPAM.ValidateIpV4Addresses = function (addressesArray) {
        var ipV4Regex =
            /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        for (var i = 0; i < addressesArray.length; i++) {
            if (!ipV4Regex.test(addressesArray[i])) {
                return false;
            }
        }

        return true;
    };
})();
