﻿if (!$SW.IPAM) $SW.IPAM = {};

var colId, colScopeId, colPoolId, colStartIPAddr, colEndIPAddr, colAddrCount, colPoolType, colRangeId, ipRrangeToolbar, ipRangeTree, rangeStore, templatecol, maskText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_12;E=js}', mask;

var webId, hideToolBar;

$SW.IPAM.RefreshTreeRangeGrid = function () {
    if (ipRangeTree != null && ipRangeTree != undefined)
        ipRangeTree.store.load();
};

$SW.IPAM.IPRangeTreeGrid = function (webid, hidebuttons) {

    webId = webid;
    hideToolBar = hidebuttons;

    $SW.IPAM.InitializeColumns();

    $SW.IPAM.InitializeToolBar();
    
    Ext4.define('IPRange', {
        extend: 'Ext4.data.Model',
        fields: [
           {
               name: 'ScopeId',
               type: 'string',
               mapping: 'ScopeId'
           }, {
               name: 'PoolId',
               type: 'string',
               mapping: 'PoolId'
           },
            {
                name: 'StartIPAddr',
                type: 'string',
                mapping: 'StartIPAddr'
            },
        {
            name: 'EndIPAddr',
            type: 'string',
            mapping: 'EndIPAddr'
        },
        {
            name: 'IPAddrCount',
            type: 'string',
            mapping: 'IPAddrCount'
        },
         {
             name: 'AutoId',
             type: 'string',
             mapping: 'AutoId'
         },
         {
             name: 'PoolType',
             type: 'string',
             mapping: 'PoolType'
         },
         {
             name: 'RangeId',
             type: 'string',
             mapping: 'RangeId'
         }]
    });
    
    rangeStore = Ext4.create('Ext4.data.TreeStore', {
        extend: 'Ext.data.TreeStore',
        id: 'rangeStoreid',
        autoLoad: true,
        model: 'IPRange',
        requires: 'IPRange',
        proxy: {
            type: 'ajax',
            async: false,
            url: "/Orion/IPAM/sessiondataprovider.ashx",
            extraParams: {
                entity: 'IPRange',
                verb: 'GetAll',
                w: webId
            },
            reader:
            {
                type: 'json',
                root: 'rows'
            }
        },
        root: {
            expanded: true
        },
        folderSort: true,
        sorters: [{
            property: 'ScopeId',
            direction: 'DESC'
        }],
        
    });
    
    /*
    Overrides for fixing clearOnLoad for TreeStore
    */
    Ext4.override(Ext4.data.TreeStore, {
        load: function (options) {
            options = options || {};
            options.params = options.params || {};

            var me = this,
                node = options.node || me.tree.getRootNode();

            // If there is not a node it means the user hasnt defined a rootnode yet. In this case lets just
            // create one for them.
            if (!node) {
                node = me.setRootNode({
                    expanded: true
                });
            }

            if (me.clearOnLoad) {
                node.removeAll(false);
            }

            Ext.applyIf(options, {
                node: node
            });
            options.params[me.nodeParam] = node ? node.getId() : 'root';

            if (node) {
                node.set('loading', true);
            }

            return me.callParent([options]);
        }
    });
    
      var sm = new Ext4.selection.CheckboxModel({
        mode: "MULTI",
        checkOnly: true,
        cls: Ext4.baseCSSPrefix + 'y-no-checkbox-header',
        renderer: function (value, meta, record) {
            meta.tdCls = Ext4.baseCSSPrefix + "grid-cell-special";
            meta.style = "padding:2px 4px 2px 4px;";
            if (record.data['PoolType'] == "PoolChild")
                return "<div></div>";
            else
                return "<div class='" + Ext4.baseCSSPrefix + "grid-row-checker'></div>";
        },
        listeners: {
            selectionchange: function (checkselectionmodel, selections) {
                ipRangeTree.down('#editButton').setDisabled(selections.length == 0 || selections.length > 1);
                ipRangeTree.down('#removeButton').setDisabled(selections.length == 0);
            }
        }
    });
     
    ipRangeTree = Ext4.create('Ext4.tree.Panel', {
        renderTo: 'scopeIpRangediv',
        id: 'IpRangeTreeGrid',
        store: rangeStore,
        multiSelect: false,
        hideHeaders: false,
        rootVisible: false,
        collapsible: false,
        singleExpand: false,
        useArrows: true,
        enableDD: true,
        enableSort: true,
        height:200,
        loadMask: true,
        width: 950,
        selModel:  ((hideToolBar == "True") ? null : sm),
        viewConfig: {
            animate: true,
            emptyText: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_40;E=js}',
            deferEmptyText: false
        },
        columns:
        {
            items: [colStartIPAddr, colEndIPAddr, colAddrCount, colScopeId, colPoolId,colId,colPoolType,colRangeId]
        },
        tbar: ipRrangeToolbar,
        rowLines: true
    });
};

$SW.IPAM.InitializeColumns = function () {

    colId = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_1;E=js}',
        dataIndex: 'AutoId',
        width: 0,
        hidden: true,
        hideable: false
    };

  colScopeId = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_1;E=js}',
        dataIndex: 'ScopeId',
        width: 0,
        hidden: true,
        hideable:false
    };
    
    colPoolId = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_2;E=js}',
        dataIndex: 'PoolId',
        width: 0,
        hidden: true,
        hideable: false
    };
    
    colStartIPAddr = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_3;E=js}',
        xtype: 'treecolumn',
        dataIndex: 'StartIPAddr',
        width: 325,
        hidden: false,
        hideable: true
    };
    
    colEndIPAddr = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_4;E=js}',
        dataIndex: 'EndIPAddr',
        width: 325,
        hidden: false,
        hideable: true
    };
    
    colAddrCount = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_5;E=js}',
        dataIndex: 'IPAddrCount',
        width: 270,
        hidden: false,
        align: 'center',
        hideable: true
    };

    colPoolType = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_5;E=js}',
        dataIndex: 'PoolType',
        width: 0,
        hidden: true,
        align: 'center',
        hideable: false
    };
    colRangeId = {
        header: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_5;E=js}',
        dataIndex: 'RangeId',
        width: 0,
        hidden: true,
        align: 'center',
        hideable: false
    };
    return;
};

$SW.IPAM.InitializeToolBar = function () {
    ipRrangeToolbar = Ext4.create('Ext4.toolbar.Toolbar', {
        width: 500,
        border: false,
        hidden: ((hideToolBar == "True") ? true : false),
        items: [
            {
                text: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_6;E=js}',
                iconCls: 'mnu-add',
                menu: {
                    items:[{
                        text: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_7;E=js}',
                        iconCls: 'range-cell',
                        listeners: {
                            click: {
                                    fn: function(el) {
                                        var rightPanelContainerId = $('#LayoutContent');
                                        if (rightPanelContainerId != null)
                                            rightPanelContainerId.html('');
                                        $SW.IPAM.IPRangeInitializeGrid(webId, -1, 0, "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_17;E=js}", "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_15;E=js}", 0, -1);
                                    }
                                }
                            }
                        },
                        {
                            text: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_8;E=js}',
                        iconCls: 'pool-cell',
                        listeners: {
                            click: {
                                fn: function(el) {
                                    var rightPanelContainerId = $('#LayoutContent');
                                    if (rightPanelContainerId != null)
                                        rightPanelContainerId.html('');

                                    $SW.IPAM.IPRangeInitializeGrid(webId, -1, 0, "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_18;E=js}", "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_16;E=js}", -1, -1);
                                }
                            }
                        }
                    }
                ]}
            },
            '-',
            {
                text: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_9;E=js}',
                iconCls: 'mnu-edit',
                disabled: true,
                itemId: 'editButton',
                listeners: {
                    click: {
                        fn: function (el) {
                            var autoid = ipRangeTree.getSelectionModel().getSelection()[0].data.AutoId;
                            var poolid = ipRangeTree.getSelectionModel().getSelection()[0].data.PoolId;
                            var poolType = ipRangeTree.getSelectionModel().getSelection()[0].data.PoolType;
                            var rangeId = ipRangeTree.getSelectionModel().getSelection()[0].data.RangeId;
                            var scopeId = ipRangeTree.getSelectionModel().getSelection()[0].data.ScopeId;
                            var rightPanelContainerId = $('#LayoutContent');
                            if (rightPanelContainerId != null)
                                rightPanelContainerId.html('');

                            if (poolType == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_18;E=js}") {
                                $SW.IPAM.IPRangeInitializeGrid(webId, autoid, poolid, poolType, "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_20;E=js}", 0,scopeId);
                            } else if (poolType == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_17;E=js}") {
                                $SW.IPAM.IPRangeInitializeGrid(webId, autoid, poolid, poolType, "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_19;E=js}", rangeId, scopeId);
                            }
                        }
                    }
                }
            },
            '-',
            {
                text: '@{R=IPAM.Strings;K=IPAMWEBJS_DF_10;E=js}',
                iconCls: 'mnu-delete',
                disabled: true,
                itemId: 'removeButton',
                listeners: {
                    click: {
                        fn: function () {
                            var records = ipRangeTree.getSelectionModel().getSelection();
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_13;E=js}',
                                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_SE_39;E=js}', records.length),
                                buttons: Ext.Msg.OKCANCEL,
                                icon: Ext.MessageBox.INFO,
                                width: 350,
                                fn: function (btn) {
                                    if (btn === "ok") {
                                        var poolIDs = "", rangeIDs = "";
                                        
                                        Ext4.Array.each(records, function (rec) {
                                            if (rec.get('PoolType') == "Pool")
                                                poolIDs += (poolIDs == "") ? rec.get('PoolId') : "," + rec.get('PoolId');
                                            else if (rec.get('PoolType') == "IPAddress")
                                                rangeIDs += (rangeIDs == "") ? rec.get('AutoId') : "," + rec.get('AutoId');
                                        });
                                        
                                        Ext.Ajax.request({
                                            url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                                            success: function (result, request) {
                                                var res = Ext.util.JSON.decode(result.responseText);
                                                if (res == null)
                                                    return;
                                                if (res.success == true) {
                                                    $SW.IPAM.RefreshTreeRangeGrid();
                                                }
                                                else
                                                    Ext.Msg.show({
                                                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                                        msg: res.message,
                                                        buttons: Ext.Msg.OK,
                                                        icon: Ext.MessageBox.ERROR,
                                                    });
                                            },
                                            failure: function (message) {
                                                Ext.Msg.show({
                                                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                                    msg: message,
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.ERROR
                                                });
                                            },
                                            params:
                                            {
                                                entity: 'IPRange',
                                                verb: 'iprangepool',
                                                PoolID: poolIDs,
                                                RangeID: rangeIDs,
                                                w: webId
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
            }
        ]
    });
};
