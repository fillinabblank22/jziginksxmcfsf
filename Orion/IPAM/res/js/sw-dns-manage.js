(function () {

    var createZoneFilter = function (baseParams, nodeId) {
        var filter = [], bp = baseParams || [], rm = [];
        // set dns zone filter
        filter.push({ op: 'eq', type: 'numeric', field: 'Distance', value: 0 });
        filter.push({ op: 'eq', type: 'numeric', field: 'GroupType', value: 2048 });
        filter.push({ op: 'eq', type: 'numeric', field: 'DnsZone.NodeId', value: nodeId });
        // remove existing filter
        for (var k in bp) if (/^filter/i.test(k)) rm.push(k);
        Ext.each(rm, function (txt) { delete bp[txt]; });
        // add new filter
        for (var n = 0; n < filter.length; n++) {
            bp["filter[" + n + "][data][comparison]"] = filter[n].op;
            bp["filter[" + n + "][data][type]"] = filter[n].type;
            bp["filter[" + n + "][data][value]"] = '' + filter[n].value;
            bp["filter[" + n + "][field]"] = filter[n].field;
        }
        return bp;
    };

    if (!$SW.IPAM) $SW.IPAM = {};
    $SW.IPAM.InitDnsZoneSelector = function (serverEl, zoneEl) {
        var zoneId = zoneEl.getValue(),
            nodeId = serverEl.getValue(),
            grid = zoneEl.getContent();

        if (Ext.isEmpty(zoneId)) {
            // handle unselected zone init state
            var noServerSelected = nodeId === "-1" || zoneEl.disabled;
            var emptyText = noServerSelected ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_187;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_186;E=js}";
            zoneEl.clearValue(emptyText);
        } else if (!Ext.isEmpty(nodeId) && !serverEl.disabled) {
            // handle selected zone init state with server selected
            zoneEl.disable();
            grid.store.baseParams = createZoneFilter(grid.store.baseParams, nodeId);
            grid.getStore().load({ callback: function (r) { if(!serverEl.disabled) zoneEl.enable(); } });
        }

        var serverSelected = function (combo, records) {
            if (!records || !records.data || !zoneEl) return;
            var nId = records.data.value;

            if (!nId) {
                Ext.Msg.show({
                    buttons: Ext.Msg.OK, icon: Ext.MessageBox.ERROR,
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_190;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_191;E=js}'
                });
            } else if (nId === "-1") {   // no server selected
                zoneEl.clearValue("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_192;E=js}");
                zoneEl.disable();
            } else if (!grid) {             // missing grid in drop down grid
                zoneEl.clearValue("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_186;E=js}");
                zoneEl.disable();
            } else if(!serverEl.disabled) { // load grid if server is not disabled
                zoneEl.clearValue("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_193;E=js}");
                zoneEl.disable();
                grid.store.baseParams = createZoneFilter(grid.store.baseParams, nId);
                grid.getStore().load({
                    callback: function (r) {
                        var isEmpty = (!r || r.length == 0),
                            emptyText = isEmpty ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_188;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_189;E=js}";
                        zoneEl.enable();
                        zoneEl.clearValue(emptyText);
                    }
                });
            }
        };
        serverEl.on("select", serverSelected);
    };

    $SW.IPAM.DnsRecordTypeRenderer = function () {
        return function (value, meta, r) {
            return $SW.IPAM.dnsRecordTypes[value] || "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
        };
    };

    $SW.IPAM.DnsRecordDataRenderer = function () {
        return function (value, meta, d) {
            var ipN = d.data['IPAddressN'];

            if (ipN && ipN != '')
                return "<a class='sw-grid-click' href='subnets.aspx?ipN=" + ipN + "'>" + value + "</a>";

            return value;
        };
    };

    $SW.IPAM.DnsRecordServerRenderer = function () {
        return function (value, meta, d) {
            var nodeId = d.data['NodeId'];

            if (nodeId && nodeId != '')
                return "<a class='sw-grid-click' href='/api2/ipam/ui/dns/node/" + nodeId + "'>" + Ext.util.Format.htmlEncode(value) + "</a>";

            return Ext.util.Format.htmlEncode(value);
        };
    };

    $SW.IPAM.DnsRecordRowClass = function (zone) {
        var z = zone;
        return function (r, i) {
            if (z && z != r.data['DnsZoneId'])
                return 'filter-row-out';
        };
    };

    $SW.IPAM.DnsMaster_renderer_buttons = function () {
        var up_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.up.png";
        var down_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.down.png";
        var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
        var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
        return function (value, meta, record) {
            var ret = '';
            meta.css = 'sw-grid-buttons';

            ret += "<a href='#' class='sw-grid-up' onclick='return false;'><img src='" + up_url + "' /></a> &nbsp;";
            ret += "<a href='#' class='sw-grid-down' onclick='return false;'><img src='" + down_url + "' /></a> &nbsp;";
            ret += "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
            return ret + "<a href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
        }
    };

    if (!$SW.IPAM.DNSZoneServers)
        $SW.IPAM.DNSZoneServers = function (opt) {
            var options = opt || {};
            $.extend(options, { GridId: undefined, WebId: undefined });
            var that = {
                getGrid: function () {
                    return $SW[options.GridId];
                },
                getWebIdParam: function () {
                    var w = options.WebId;
                    return (w && w != '') ? '&w=' + w : '';
                },
                SetWebId: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore();
                    store.baseParams.w = options.WebId;
                },
                AddServerBtnPressed: function (el) {
                    var params = that.getWebIdParam();
                    $SW.ext.dialogWindowOpen(
                        $SW['masterServerWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_194;E=js}',
                        '/Orion/IPAM/DnsZones/DNS.Zone.MasterServerIP.aspx?NoChrome=True&msgq=masterServerWindow' + params,
                        'OrionIPAMAGEditingDNSZones.htm');
                },
                ServerRefresh: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore();
                    store.reload();
                },
                EditServer: function (el, r) {
                    var params = that.getWebIdParam();
                    $SW.ext.dialogWindowOpen(
                        $SW['masterServerWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_195;E=js}',
                        '/Orion/IPAM/DnsZones/DNS.Zone.MasterServerIP.aspx?NoChrome=True&msgq=masterServerWindow&serverAddress=' + encodeURIComponent(r.data['serverAddress']) + params,
                        'OrionIPAMAGEditingDHCPScopes.htm');
                },
                ServerRemove: function (el, r) {
                    Ext.Ajax.request({
                        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                        success: function (p1, p2, p3) {
                            that.ServerRefresh();
                        },
                        failure: function () {
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                buttons: Ext.Msg.OK,
                                animEl: that.id,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        params:
                   {
                       entity: 'MasterDnsServers',
                       verb: 'Remove',
                       ids: r.data["ID"],
                       value: r.data['serverAddress'],
                       w: options.WebId
                   }
                    });
                },
                ServerUp: function (el, r) { that.ServerMove(el, r, 'Up'); },
                ServerDown: function (el, r) { that.ServerMove(el, r, 'Down'); },
                ServerMove: function (el, r, direction) {
                    Ext.Ajax.request({

                        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",

                        success: function (p1, p2, p3) {
                            that.ServerRefresh();
                        },

                        failure: function () {

                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                buttons: Ext.Msg.OK,
                                animEl: that.id,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        params:
                   {
                       entity: 'MasterDnsServers',
                       verb: 'Resequence',
                       ids: r.data["ID"],
                       value: r.data['serverAddress'],
                       moveUp: direction == 'Up' ? "true" : "false",
                       w: options.WebId
                   }
                    });
                }
            };
            return that;
        };

})();
