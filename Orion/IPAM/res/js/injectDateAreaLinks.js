﻿(function($) {

    $.fn.IpamSettingsLinkInjector = function(options) {
        var o = $.extend({}, $.fn.IpamSettingsLinkInjector.defaults, options);

        return this.each(function() {
            var div = $(this);

            var anchor = $('<a/>').
                attr('id', 'IpamSettingsLink').
                addClass('IpamSettingsLink').
                attr('href', '/Orion/IPAM/Admin/Admin.Overview.aspx').
                html('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_19;E=js}');

            var childs = div.children('a');
            var pos = (o.pos || childs.length) - 1;
            var el = childs.get(pos);
            if (!el) { div.append(anchor); return; }
            if (o.before) { $(el).before(anchor); } else { $(el).after(anchor); }
        });
    };

})(jQuery);
