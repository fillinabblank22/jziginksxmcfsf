﻿if (!$SW.IPAM) $SW.IPAM = {};

// show warning dialog when DNS server scanning is disabled
$SW.IPAM.ShowDisableScanning = function (callback) {
    if ($SW.IPAM.DisableDNSServerAutoScanning === "True") {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_24;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_25;E=js}',
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.WARNING,
            fn: callback
        });
    } else callback();
};

$SW.IPAM.DnsRecordShowEditDialog = function (src, id, header, hlpSuffix, zoneId, zoneName) {
    var page = 'Dns.Record.AddEdit.aspx',
        helpid = 'OrionIPAMPHWindowsCredentials' + hlpSuffix + '.htm',
        objectId = (id && id != '') ? '&ObjectID=' + id : '';

    $SW.ext.dialogWindowOpen($SW['dnsRecordsWindow'], src.id, header,
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=dnsRecordsWindow' + objectId + '&ZoneId=' + zoneId + '&ZoneName=' + zoneName, helpid);
};

$SW.IPAM.DnsRecordAdd = function (that, zoneId, zoneName) {
    var btnSrc = that;
    var header = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_170;E=js}';
    $SW.IPAM.ShowDisableScanning(function () {
        $SW.IPAM.DnsRecordShowEditDialog(btnSrc, '', header, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_171;E=js}', zoneId, zoneName);
    });
};

$SW.IPAM.DnsRecordGridRefresh = function () {
    var store = $SW['store'];
    store.load();
};

$SW.IPAM.IsRecordNameValid = function (src, args) {
    var rName = args.Value;
    var n = rName.split(" ");
    args.IsValid = (n.length == 1);

    var type = Ext.getCmp('_ddlRecordType').value;

    if (type == "CNAME") {
        args.IsValid = (args.Value != '');
    }

    return args.IsValid;
};

$SW.IPAM.IsStartWithZonePrefix = function (src, args) {
    var breturn = true;
    var recordName = $('#' + $SW.IPAM.txtRecordNameField).val();
    var type = Ext.getCmp('_ddlRecordType').value;

    if (recordName == '' || $SW.IPAM.AddMode == false)
        breturn = true;
    else {
        switch (type) {
            case "PTR":
                {
                    if (!$SW.Valid.Fns.ipv4(src, args) && !$SW.Valid.Fns.ipv6(src, args))
                        breturn = false;
                    else if (recordName.indexOf($SW.IPAM.IpPrefix) != 0) {
                        $('#' + src.id).text('Record name should start with ' + $SW.IPAM.IpPrefix);
                        breturn = false;
                    }
                    else if (recordName.indexOf("::") >= 0) {
                        $('#' + src.id).text('IPv6 Address short notation fromat not allowed.');
                        breturn = false;
                    }
                }
            }
    }
    return args.IsValid = breturn;
};

$SW.IPAM.IsSupportedRecordType = function (rtype) {
    var bReturn = false;
    if (rtype == '1' || rtype == '28' || rtype == '5' || rtype == '15' || rtype == '12')
        bReturn = true;
    return bReturn;
};

$SW.IPAM.DnsRecordEdit = function (that) {
    var btnSrc = that;

    var selector = $SW['selector'];
    if (!selector) return;
    if (selector.getCount() != 1) {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_172;E=js}',
            buttons: Ext.Msg.OK,
            animEl: btnSrc.id,
            icon: Ext.MessageBox.WARNING
        });
    } else {
        var item = selector.getSelected();
        var header = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_173;E=js}';
        if (!$SW.IPAM.IsSupportedRecordType(item.data.Type)) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_174;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_175;E=js}',
                buttons: Ext.Msg.OK,
                animEl: btnSrc.id,
                icon: Ext.MessageBox.WARNING
            });
        }
        else $SW.IPAM.ShowDisableScanning(function () {
            $SW.IPAM.DnsRecordShowEditDialog(btnSrc, item.data.DnsRecordId, header, 'Edit', item.data.ZoneId, item.data.ZoneName);
        });
    }
};

$SW.IPAM.EditOnIsValueRequire = function (src, args) {
    return args.IsValid = (args.Value != '');
};

$SW.IPAM.RecordTypeChanged = (function () {
    var LUT = {
        'A': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_181;E=js}',
        'AAAA': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_182;E=js}',
        'CNAME': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_183;E=js}',
        'MX': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_184;E=js}',
        'PTR': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_185;E=js}'
    };
    return function () {
        var data = $('#' + $SW.IPAM.txtDataField).val();
        var recordName = $('#' + $SW.IPAM.txtRecordNameField).val();
        var type = Ext.getCmp('_ddlRecordType').value;
        var checkboxDiv = $('#' + $SW.IPAM.chkPTR);
        var obj = $('.smallText');

        var html = LUT[type];
        obj.html(html);

        if (type == 'A') {
            checkboxDiv.show();
        } else {
            checkboxDiv.hide();
            if ($SW.IPAM.AddMode && type == 'PTR') {
                $('#' + $SW.IPAM.txtRecordNameField).val($SW.IPAM.IpPrefix);
                if (data == $SW.IPAM.IpPrefix)
                    $('#' + $SW.IPAM.txtDataField).val('');
            }
        }

        if (data != '' && data != $SW.IPAM.IpPrefix)
            var isValid = Page_ClientValidate('Datavalidation');
    };
})();

function TryParseInt(str, defaultValue) {
    return (str != null && str.length > 0 && !isNaN(str)) ? parseInt(str) : defaultValue;
}

$SW.IPAM.ValidateData2 = function (src, args) {
    var breturn = true;
    var data = $('#' + $SW.IPAM.txtDataField).val();
    var type = Ext.getCmp('_ddlRecordType').value;

    if (data == '') breturn = false;

    switch (type) {
        case "A":
            {
                var chkPTRBox = $('#' + $SW.IPAM.chkPTRBox);
                var checkedState = (chkPTRBox != null) ? chkPTRBox[0].checked : false;

                if (checkedState && $SW.IPAM.ZoneId > 0) {

                        breturn = $SW.Valid.Fns.ipv4(src, args);
                        if (!breturn) {
                            breturn = true; // to avoid two error message. if the ip address is not valid then, already validate data method should have thrown error
                        }else {
                            $.ajax({
                                type: "POST",
                                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                                async: false,
                                data: { entity: 'IPAM.DnsServer', verb: 'ReverseZoneLookup', IPAddress: data, ZoneId: $SW.IPAM.ZoneId },

                                success: function(result, request) {
                                    var res = Ext.util.JSON.decode(result);
                                    if (res.success == false) {
                                        Ext.Msg.show({
                                            title: 'DNS Record',
                                            msg: "No Reverse zone found for this IP Address.",
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR,
                                            fn: function() {
                                            }
                                        });
                                        breturn = false;
                                    }
                                },
                                failure: function(result, request) {
                                    breturn = false;
                                }
                            });
                        }

                    }
                break;
            }
        case "AAAA":
            break;
        case "MX":
            break;
        case "CNAME":
            break;
        case "PTR": break;
    }

    return args.IsValid = breturn;
};




$SW.IPAM.ValidateData = function (src, args) {
    var breturn = true;
    var data = $('#' + $SW.IPAM.txtDataField).val();
    var type = Ext.getCmp('_ddlRecordType').value;

    if (data == '') breturn = false;
   
    switch (type) {
        case "A":
            {
                breturn = $SW.Valid.Fns.ipv4(src, args);
                break;
            }
        case "AAAA": { breturn = $SW.Valid.Fns.ipv6(src, args); break; }
        case "MX":
            {
                var n = data.split(" ");
                breturn = (n.length == 2);
                if (breturn) {
                    var tv = TryParseInt(n[0], -1);
                    if (tv < 0 || tv > 65535) {
                        breturn = false;
                    }
                }
                break;
            }
        case "CNAME": break;
        case "PTR": break;
    }

    return args.IsValid = breturn;
};

$SW.IPAM.DeleteRecordFn = function (opts, refreshFn) {
    opts.IsDeleteOption = true;

    var success = function () {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_177;E=js}',
            msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_179;E=js}",
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.INFO,
            fn: function () { refreshFn(); }
        });
    };
    var fail = function () {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_177;E=js}',
            msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_180;E=js}",
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.INFO,
            fn: function () { }
        });
    };

    Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_177;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_178;E=js}',
        buttons: Ext.Msg.YESNO,
        width: 400,
        icon: Ext.MessageBox.QUESTION,
        fn: function (btn) {
            if (btn != 'yes') return;
            $SW.Tasks.DoProgress(
                'DnsRecordManagement',
                opts,
                { title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_199;E=js}" },
                function (d) {
                    if (d.success) success(); else fail();
                },
                null
            );
        }
    });
};

$SW.IPAM.DeleteRecord = function (that) {
    var btnSrc = that;
    var selector = $SW['selector'];

    if (!selector) return;
    if (selector.getCount() != 1) {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_176;E=js}',
            buttons: Ext.Msg.OK,
            animEl: btnSrc.id,
            icon: Ext.MessageBox.WARNING
        });
    } else {
        var item = selector.getSelected();
        var opts = { RecordID: parseInt(item.data.DnsRecordId) };

        if (!$SW.IPAM.IsSupportedRecordType(item.data.Type)) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_174;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_175;E=js}',
                buttons: Ext.Msg.OK,
                animEl: btnSrc.id,
                icon: Ext.MessageBox.WARNING
            });
        }
        else $SW.IPAM.ShowDisableScanning(function () {
            $SW.IPAM.DeleteRecordFn(opts, $SW.IPAM.DnsRecordGridRefresh);
        });
    }
};

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    return (IsDemoMode() === true);
}

Ext.ux.grid.DNSRecordEditor = Ext.extend(Ext.ux.grid.RowEditor, {
    NewRecordErrTpl: new Ext.XTemplate(
        '<tpl if="this.hasErrs(values)">',
            '<br />',           
                '<tpl for="values">',
                    '<tpl if="!this.isPtrCreationMsg(field)">',
                        String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_397;E=js}', '<br>■ &ensp;', '<b>{columnHeader}</b> <tpl if="this.hasMessage(message)">{message}</tpl><tpl if="!this.hasMessage(message)">', '</tpl>'),
                    '</tpl>',
                    '<tpl if="this.isPtrCreationMsg(field)">',
                        String.format('{0} {1} {2}', '<br>■ &ensp;', '<b>{columnHeader}</b> <tpl if="this.hasMessage(message)">{message}</tpl><tpl if="!this.hasMessage(message)">', '</tpl>'),
                    '</tpl>',
                '</tpl>',
        '</tpl>',
        {
            hasErrs: function (errs) { return (errs && errs.length > 0); },
            hasMessage: function (message) { return !Ext.isEmpty(message); },
            isPtrCreationMsg: function (field) { return (field && field == 'PTRCreation'); }
        }
    ),
    //[oh] disable edit on click / dblclick
    onRowClick: function () { },
    onRowDblClick: function () { },
    onGridKey: function () { },

    _getZoneValue: function (record) {
        return { 'DnsZoneId': record.data['ZoneId'], 'FriendlyName': record.data['ZoneName'] };
    },
    startEditing: function (rowIndex, doFocus) {
        var editors = {}, g = this.grid,
            record = g.store.getAt(rowIndex),
            cm = g.getColumnModel();
        $.each(cm.lookup, function (i, e) { editors[e.dataIndex] = e.editor; });

        // set empty string to combo boxes without value
        var se = editors['ServerId'], sd = record.data['ServerId'] || "-1";
        if (se.store == undefined)
            se.store = $SW.IPAM.DnsZoneSelector.ServerSelector.store;
        if (se) se.setValue(sd);

        var ze = editors['ZoneId'];
        if (ze) ze.setValue(this._getZoneValue(record));
        $SW.IPAM.InitDnsZoneSelector(se, ze);

        return Ext.ux.grid.DNSRecordEditor.superclass.startEditing.apply(this, arguments);
    },
    preEditValue: function (r, field) {
        if (field == 'ZoneId') return this._getZoneValue(r);
        return Ext.ux.grid.DNSRecordEditor.superclass.preEditValue.apply(this, arguments);
    },
    stopEditing: function (saveChanges) {
        if (saveChanges === true && CheckDemoMode()) {
            demoAction('IPAM_IPEdit_DNSRecord', null);
            return false;
        }
        if (this.isVisible() && saveChanges !== false && this.isValid()) {
            var changes = {}, r = this.record, hasChange = false, cm = this.grid.colModel, fields = this.items.items;
            for (var i = 0, len = cm.getColumnCount(); i < len; i++) {
                if (!cm.isHidden(i)) {
                    var dindex = cm.getDataIndex(i);
                    if (!Ext.isEmpty(dindex)) {
                        var oldValue = r.data[dindex], value = this.postEditValue(fields[i].getValue(), oldValue, r, dindex);
                        if (String(oldValue) !== String(value)) {
                            changes[dindex] = value;
                            hasChange = true;
                        }
                    }
                }
            }
            if (!hasChange)
                this.fireEvent('validateedit', this, changes, this.record, this.rowIndex);
        }
        return Ext.ux.grid.DNSRecordEditor.superclass.stopEditing.apply(this, arguments);
    },
    listeners: {
        canceledit: function (editor, isCancel) {
            editor.stopEditing();
            var s = editor.grid.store, ri = editor.rowIndex, r = s.getAt(ri);
            var id = (r && r.data) ? r.data['DnsRecordId'] || -1 : -1;

            if (id < 0) s.removeAt(ri);
            editor.grid.getView().refresh();
        },
        validateedit: function (editor, m, r, ri) {
            if (!r || !m) return false;
            var id = (r && r.data) ? r.data['DnsRecordId'] || -1 : -1;
            var valid = true, addMode = id < 0, errs = [];

            var s = editor.grid.store, zone=null;
            
            // search for zone by serverid and zoneid
            var serverid = m["ServerId"] || r.data["ServerId"],
                zoneid = m["ZoneId"] || r.data["ZoneId"];

            if (zone != null && zone.ZoneType != "Primary") {
                valid = false;
                errs.push({ message: "@{R=IPAM.Strings;K=IPAMWEBJS_GK_3;E=js}" });
            }
            var cm = editor.grid.getColumnModel();
            var hasChange = false;
            
            String.prototype.endsWithDot = function (str) {
                str = (str && str.length > 0) ? str.substring(str.length - 1) : "";
                return (this.match(".") == str);
            };

            function IsRecordEndsZoneName(recName,zoneName) {
			//Record Name length should be greater than zone name
                if (recName.length < zoneName.length)
                    return false;
					//Remove empty spaces for record name array 
				for(var i = recName.length - 1; i >= 0; i--) {
						if(recName[i] === "") {
						recName.splice(i, 1);
						}
					}
					//Remove empty spaces for Zone name array 
				for(var i = zoneName.length - 1; i >= 0; i--) {
						if(zoneName[i] === "") {
						zoneName.splice(i, 1);
						}
					}	
				var ret = false;
				if(recName[0] == zoneName[0] )
				{
				  ret = true;
				}
				else 
				{
				   ret = false;
				}
				return ret;
            };
			
            var testDnsRecordName = function (recordType, text) {
                if ((recordType == 1 || recordType == 28) && addMode) return;

                var ddg = $SW.IPAM.DnsRecords.DnsZoneDropDownGrid,
                    zoneid = ddg.getValue() || '-1',
                    zonename = ddg.getRawValue();
                if (zoneid == '-1' || $.trim(zonename) === "") return;

                var recName = text.endsWithDot(text) ? text.substring(1, text.length - 1) : text;

                var ok = true;

                $.each(recName.split('.'), function () {
                    ok &= !this || $.trim(this) === "" || /^(?![0-9]+$)(?!-)[a-zA-Z0-9-]{1,63}$/g.test(this); 
                });

                if (!ok) return;

                zonename = zonename.toLowerCase() + (zonename.endsWithDot(zonename) ? "" : ".");
                var recSuffix = (recName.toLowerCase() + ".").split('.').reverse();

                if (!IsRecordEndsZoneName(recSuffix, $.trim(zonename).split('.').reverse())) return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_398;E=js}";
            };

            var testDnsRecordData = function (recordType, text) {
                //A
                if (recordType == 1 && !$SW.IP.v4_isok($SW.IP.v4(text))) {
                    return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_399;E=js}";
                }
                //AAAA
                if (recordType == 28 && !$SW.IP.v6_isok($SW.IP.v6(text))) {
                    return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_400;E=js}";
                }
                //MX
                if (recordType == 15) {
                    var n = text.split(" ");
                    if (n.length != 2) {
                        return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_401;E=js}";
                    }
                    var tv = TryParseInt(n[0], -1);
                    if (tv < 0 || tv > 65535) {
                        return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_402;E=js}";
                    }
                }
            };

            // dataIndes of fields which need to be tested
            $.each(["DnsRecordId", "Name", "Type", "Data", "ServerId", "ZoneId"], function () {
                var dataIndex = this;

                if (addMode && dataIndex == "DnsRecordId") return;
                if (m[dataIndex] != undefined) hasChange = true;

                //check for empty fields
                var text = m[dataIndex] != undefined ? m[dataIndex] : r.data[dataIndex],
                    recordType = m["Type"] || r.data["Type"];

                var ci = cm.findColumnIndex(dataIndex),
                    cn = (ci >= 0) ? cm.getColumnHeader(ci) : dataIndex;
                
                // test for empty text or '-1' (for ID values)
                if ((dataIndex != "Name") && (Ext.isEmpty(text) || text == "-1")) {
                    valid = false;
                    errs.push({ field: dataIndex, columnIndex: ci, columnHeader: cn, message: "" });
                }
                else if (dataIndex == "Name" && !Ext.isEmpty(text)) {
                    var msg = testDnsRecordName(recordType, text);
                    if (msg !== undefined) {
                        valid = false;
                        errs.push({ field: dataIndex, columnIndex: ci, columnHeader: cn, message: msg });
                    }
                }
                else if (dataIndex == "Data") {
                    var msg = testDnsRecordData(recordType, text);
                    if (msg !== undefined) {
                        valid = false;
                        errs.push({ field: dataIndex, columnIndex: ci, columnHeader: cn, message: msg });
                    }
                }
            });

            if (errs && errs.length == 0) {

                var text = m['Data'] != undefined ? m['Data'] : r.data['Data'];
                var ddg = $SW.IPAM.DnsRecords.DnsZoneDropDownGrid, zoneId = ddg.getValue() || '-1',isChecked = $SW.IPAM.DnsRecords.DnsPtrRecordCreationField.checked || false;
                var ci = cm.findColumnIndex('PTRCreation'), cn = (ci >= 0) ? cm.getColumnHeader(ci) : 'PTRCreation';

                if (($SW.IPAM.DnsRecords.DnsPtrRecordCreationField && isChecked) && zoneId > 0 && text) {

                    $.ajax({
                        type: "POST",
                        url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                        async: false,
                        data: { entity: 'IPAM.DnsServer', verb: 'ReverseZoneLookup', IPAddress: text, ZoneId: zoneId },

                        success: function(result, request) {
                            var res = Ext.util.JSON.decode(result);
                            if (res.success == false) {
                                valid = false;
                                errs.push({ field: 'PTRCreation', columnIndex: ci, columnHeader: '', message: "@{R=IPAM.Strings;K=IPAMWEBJS_SE_45;E=js}" });
                            }
                        },
                        failure: function(result, request) {
                            var res = Ext.util.JSON.decode(result);
                            if (res.success == false) {
                                valid = false;
                                errs.push({ field: 'PTRCreation', columnIndex: ci, columnHeader: '', message: res.message });
                            }
                        }
                    });
                }
            }

            // store entered DNS records data (not to lose them)
            if (hasChange && addMode && !valid) {
                r.beginEdit();
                Ext.iterate(m, function (n, v) {
                    r.set(n, v);
                });
                r.endEdit();
            }

            if (errs && errs.length > 0) {
                Ext.MessageBox.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_281;E=js}',
                    msg: editor.NewRecordErrTpl.apply(errs),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR,
                    width: 350,
                    fn: function () {
                        editor.startEditing(ri, false);
                    }
                });
                return false;
            }
            return valid;
        },
        afteredit: function (editor, obj, rec, index) {
            var m = rec.modified, s = editor.grid.store;
            if (!m || !s) return;
            var src = rec.data;
            var reloadFn = function () { s.load(); };
            var isChecked = ($SW.IPAM.DnsRecords.DnsPtrRecordCreationField && $SW.IPAM.DnsRecords.DnsPtrRecordCreationField.checked ? $SW.IPAM.DnsRecords.DnsPtrRecordCreationField.checked : false);

            if (($SW.IPAM.DnsRecords.initialDataValue == src.Data) && ($SW.IPAM.DnsRecords.initialCheckBoxStatus == true) == (isChecked == true)) //no Data changed
            {
                var addMode = src.DnsRecordId && src.DnsRecordId <= 0;
                var msgWnd = { title: (addMode ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_282;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_283;E=js}"), msg: '', buttons: Ext.Msg.OK };
                msgWnd.msg = addMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_284;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_285;E=js}';
                msgWnd.icon = Ext.MessageBox.INFO;
                msgWnd.width = 400;
                Ext.Msg.show(msgWnd);
                reloadFn();
                return;
            }

            var data = [];
            data["Data"] = src.Data;
            data["DnsRecordId"] = src.DnsRecordId;
            data["ZoneId"] = src.ZoneId;
            data["Name"] = src.Name;
            data["Type"] = src.Type;
            data["InitialCheckBoxStatus"] = $SW.IPAM.DnsRecords.initialCheckBoxStatus;
            data["PairPTRCheck"] = isChecked;
            $SW.IPAM.DnsRecords.AddEditActionProgress(data, reloadFn);
        }
    }
});
Ext.preg('sw-dnsrecord-editor', Ext.ux.grid.DNSRecordEditor);

if (!$SW.IPAM.DnsRecords) $SW.IPAM.DnsRecords = (function () {
    var DnsRecordTypeEditorData = function () {
        var data = [], src = $SW.IPAM.dnsRecordTypes;
        $.each(src, function (t, txt) { data.push({ type: t, text: txt }); });
        return data;
    };
    var ChangeFieldStatus = function (editMode) {
        var dnsEditors = $SW.IPAM.DnsRecords,
            server = dnsEditors.DnsServerComboBox,
            zone = dnsEditors.DnsZoneDropDownGrid,
            name = dnsEditors.DnsRecordNameTextField;
        
        if (server && server.setDisabled) server.setDisabled(editMode);
        if (zone && zone.setDisabled) zone.setDisabled(editMode);
        if (name && name.setDisabled) name.setDisabled(editMode);
    };
    var DnsRecordMgtMsgTpl = new Ext.XTemplate(
        '<tpl if="!this.hasErrs(results)">{message:this.toHtml}</tpl>',
        '<tpl if="this.hasErrs(results)">',
                '<tpl for="results">',
                     '<br>■ &ensp; {.:this.translate}',
                '</tpl>',
        '</tpl>',
        {
            toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
            hasErrs: function (rs) { return (rs && rs.length > 0); },
            translate: function (err) {
                return err;
            }
        }
    );
    function GetAssociatedDnsRecordId(recType, recordId) {
        var isRecordExists = false;
        $.ajax({
            type: "POST",
            url: $SW.appRoot() + "Orion/IPAM/Services/Information.asmx/GetAssociateRecordForAandPtr",
            async: false,
            data: { recordType: recType, DnsRecordId: recordId },
            success: function (result, request) {
                var res = Ext.util.JSON.decode(result);
                if (res.success == true) {
                    isRecordExists = (res.data && !Ext.isEmpty(res.data) && parseInt(res.data) > 0) ? true : false;
                }
            },
            failure: function (result, request) {
                isRecordExists = false;
            }
        });
        return isRecordExists;
    }
    return {
        AddRecord: function (grid, IpAddressN) {
            //make a new empty record and stop any current editing
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            //add new record as the first row, select it
            grid.plugins.stopEditing(false);

            // get selected server & zone from zone selector control
            var sel = $SW.IPAM.DnsZoneSelector,
                server = (sel && sel.ServerSelector) ? sel.ServerSelector : null,
                zone = (sel && sel.ZoneSelector) ? sel.ZoneSelector : null;

            grid.store.insert(0, new blankRecord({
                Name: "",
                Data: $SW.IPAM.CurrentIP,
                DnsRecordId: undefined,
                Type: 1,
                IPAddressN: IpAddressN,
                ServerId: (server && server.getValue() != "-1") ? server.getValue() : "-1",
                ServerName: (server && server.getValue() != "-1") ? server.getRawValue() : "-1",
                ZoneId: (zone && zone.getValue() != "-1") ? zone.getValue() : "",
                ZoneName: (zone && zone.getValue() != "-1") ? zone.getRawValue() : "",
                PTRCreation: '1'
            }));
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);

            ChangeFieldStatus(false);

            $SW.IPAM.DnsRecords.initialCheckBoxStatus = false;
            $SW.IPAM.DnsRecords.initialDataValue = "";
            
            //start editing new record
            grid.plugins.startEditing(0, false);
        },
        EditRecord: function (grid, ri, ci, r, target) {
            var isReverseLookupFound = r.data['PTRCreation'] > 0 ? true : false; //GetAssociatedDnsRecordId(r.data['Type'], r.data['DnsRecordId']);
            $SW.IPAM.DnsRecords.initialCheckBoxStatus = isReverseLookupFound;
            r.data['PTRCreation'] = (isReverseLookupFound) ? '1' : '0';
            $SW.IPAM.DnsRecords.initialDataValue = r.data['Data'];

            grid.plugins.startEditing(ri, true);
        },
        RemoveRecord: function (grid, ri, ci, r, target) {
            if (CheckDemoMode()) {
                demoAction('IPAM_IPEdit_DNSRecord', null);
                return false;
            }
            var opts = { RecordID: r.data['DnsRecordId'] };
            var refreshFn = function () { grid.store.load(); };
            $SW.IPAM.DeleteRecordFn(opts, refreshFn);
        },
        AddEditActionProgress: function (data, reloadFn) {
            var params = {
                RecordID: data["DnsRecordId"] || 0,
                IsDeleteOption: false,
                Name: data["Name"],
                Type: data["Type"],
                ZoneId: data["ZoneId"],
                Values: { IPAddress: data["Data"] },
                IPAddress: data["Data"],
                InitialCheckBoxStatus: data["InitialCheckBoxStatus"],
                PairPTRCheck: data["PairPTRCheck"]
            };
            var addMode = params.RecordID <= 0;
            var title = (addMode ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_282;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_283;E=js}");

            var handler = function (d) {
                var warning = false;
                $.each(d.results || [], function (i, r) {
                    if (r.Success === false) warning = true;
                });

                var msgWnd = { title: title, msg: '', buttons: Ext.Msg.OK };
                if (!d.success) {
                    msgWnd.msg = DnsRecordMgtMsgTpl.apply(d),
                    msgWnd.icon = Ext.MessageBox.ERROR;
                } else if (warning) {
                    msgWnd.msg = DnsRecordMgtMsgTpl.apply(d);
                    msgWnd.icon = Ext.MessageBox.WARNING;
                } else {
                    msgWnd.msg = addMode ?
                    '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_284;E=js}' :
                    '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_285;E=js}';
                    msgWnd.icon = Ext.MessageBox.INFO;
                }
                Ext.Msg.show(msgWnd);
                reloadFn();
            };

            $SW.Tasks.DoProgress('DnsRecordManagement', params, { title: title }, handler, null);
        },
        DnsRecordTypeRenderer: function () {
            return function (value, meta, r) {
                return $SW.IPAM.dnsRecordTypes[value] || "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
            };
        },
        DnsRecordTypeEditor: function (o) {
            var opt = $.extend(o, {
                xtype: 'combo',
                disabled: true,
                mode: 'local',
                valueField: 'type',
                displayField: 'text',
                store: new Ext.data.JsonStore({
                    data: DnsRecordTypeEditorData(),
                    fields: [
                        { name: 'type', type: 'string' },
                        { name: 'text', type: 'string' }
                    ]
                }),
                forceSelection: true,
                editable: false,
                typeAhead: false,
                triggerAction: 'all',
                hiddenName: 'val',
                setValue: function (v) {
                    Ext.form.ComboBox.prototype.setValue.call(this, v);
                    if (this.rendered && this.el) {
                        $(this.el).val(this.lastSelectionText);
                    }
                    return this;
                }
            });
            return new Ext.form.ComboBox(opt);
        },
        DnsServerRenderer: function () {
            return function (value, meta, r) {
                var val = r && r.data ? r.data['ServerName'] : null;
                return ($.type(val)==="string") ? val : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_286;E=js}";
            };
        },
        DnsServerEditor: function (o) {
            var opt = $.extend(o, {
                xtype: 'combo',
                disabled: false,
                mode: 'local',
                valueField: 'value',
                displayField: 'text',
                store: undefined,
                forceSelection: true,
                editable: false,
                typeAhead: false,
                triggerAction: 'all',
                hiddenName: 'val',
                setValue: function (v) {
                    Ext.form.ComboBox.prototype.setValue.call(this, v);
                    if (this.rendered && this.el) {
                        $(this.el).val(this.lastSelectionText);
                    }
                    return this;
                }
            });
            var cb = new Ext.form.ComboBox(opt);
            $SW.IPAM.DnsRecords.DnsServerComboBox = cb;
            return cb;
        },
        DnsZoneRenderer: function () {
            return function (value, meta, r) {
                var val = r && r.data ? r.data['ZoneName'] : null;
                return ($.type(val) === "string") ? val : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_287;E=js}";
            };
        },
        DnsZoneEditor: function (o) {
            return $SW.IPAM.DnsRecords.DnsZoneDropDownGrid = Ext.getCmp('ddgDnsZonesX');
        },
        DnsRecordNameEditor: function (o) {
            var opt = $.extend(o, { xtype: 'textfield', disabled: true});
            var tf = new Ext.form.TextField(opt);
            $SW.IPAM.DnsRecords.DnsRecordNameTextField = tf;
            return tf;
        },
        DeleteImgButtonRenderer: function () {
            var tpl = '<a href="#" class="sw-grid-{0}" onclick="return false;">' +
                         '<img src="' + $SW.appRoot() + 'Orion/IPAM/res/images/sw/icon.{0}.gif" />' +
                      '</a>';
            return function (value, meta, record) {
                var res = String.format(tpl, 'delete');
                if ($SW.IPAM.IsSupportedRecordType(record.data['Type']))
                    res = String.format(tpl, 'edit') + '&nbsp;' + res;

                meta.css = 'sw-grid-buttons';
                return res;
            };
        },
        grid_cellclick: function (grid, ri, ci, ev) {
            $SW.ext.gridClickToDrilldown(grid, ri, ci, ev);
            var targetEl = ev.browserEvent.target || ev.browserEvent.srcElement;
            if (targetEl.tagName.toUpperCase() == 'IMG') targetEl = targetEl.parentNode;
            if (targetEl.tagName.toUpperCase() != 'A') return false;
            var record = grid.getStore().getAt(ri);

            if (/sw-grid-edit/.test(targetEl.className)) {
                ChangeFieldStatus(true);
                $SW.IPAM.DnsRecords.EditRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-delete/.test(targetEl.className)) {
                $SW.IPAM.DnsRecords.RemoveRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
        }
    };
})();
