﻿(function () {

    if (!$SW.IPAM) $SW.IPAM = {};

    $SW.IPAM.AllOptions_renderer_Editbuttons = function () {
        var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
        return function (value, meta, record) {
            meta.css = 'sw-grid-buttons';
                return "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
           }
    };
    
    $SW.IPAM.AllOptions_renderer_Deletebuttons = function () {
        var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
       
        return function (value, meta, record) {
            var optCode = record.data['OptionCode'];
            
            if (optCode == 51 && (serverType == 'CISCO' || serverType == 'WINDOWS' || serverType == 'ASA')) {
                hasLeaseTime = true;

            } else {
                 meta.css = 'sw-grid-buttons';
                    return "<a href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
            }
        }
    };
    
     $SW.IPAM.AllOptions_ValidateLeaseOption = function () {

         if ((serverType == 'CISCO' || serverType == 'WINDOWS' || serverType == 'ASA') && !hasLeaseTime) {
             Ext.Msg.show({
                 title: '@{R=IPAM.Strings;K=IPAMWEBJS_GK_18;E=js}',
                 msg: '@{R=IPAM.Strings;K=IPAMWEBJS_GK_19;E=js}',
                 buttons: Ext.Msg.OK,
                 icon: Ext.MessageBox.ERROR
             });
             return false;
         } else {
             return true;
         }
     };

    $SW.IPAM.AllOptions_renderer_OptionValue = function() {
        return function(value, meta,r) {
            var optCode = r.data['OptionCode'];
            if (optCode == 81) {
               var dnsString = ParseDnsOption(value);
                return dnsString;

            } else if (optCode == 19 || optCode == 20 || optCode == 27 || optCode == 29 || optCode == 30 || optCode == 31 || optCode == 34 || optCode == 36 || optCode == 39) {
                if (value > 0) {
                    return '@{R=IPAM.Strings;K=IPAMWEBJS_GK_11;E=js}';
                } else {
                    return '@{R=IPAM.Strings;K=IPAMWEBJS_GK_12;E=js}';
                }
            } else if (optCode == 2 || optCode == 23 || optCode == 24 || optCode == 26 || optCode == 35 || optCode == 37 || optCode == 38 || optCode == 51 || optCode == 58 || optCode == 59) {
                if (value !="") {
                    return (value + ' @{R=IPAM.Strings;K=IPAMWEBJS_GK_13;E=js}');
                } else {
                    return value;
                }
            }
            else if (optCode == 21 || optCode == 33) {
                var ipPairTemplate = '';
                var splitarr = value.split(",");
                
                $.each(splitarr, function (index) {
                    if (index % 2 == 0) {
                        ipPairTemplate += (index == 0 ? '' : '&emsp;&emsp;') + splitarr[index] + (splitarr[index + 1] == undefined ? '' : ',' + splitarr[index + 1]);
                    }
                });
                return ipPairTemplate;
            }
            else {
               return value;
            }
        };
    };
    var ParseDnsOption = function (value) {
        var strText = "";
        var Enum = {
                    NoFlags: 0,
                    EnableDynamicUpdates: 1,
                    DynamicallyUpdateWhenNotRequestUpdates: 2,
                    DiscardOnLeaseDeleting: 4,
                    DynamicallyUpdateAlways: 16
                };
                if ((value & Enum.DynamicallyUpdateAlways) == Enum.DynamicallyUpdateAlways) {
                    strText = '&bull; @{R=IPAM.Strings;K=IPAMWEBJS_GK_7;E=js}<br/>';
                } else {
                    strText = '&bull; @{R=IPAM.Strings;K=IPAMWEBJS_GK_8;E=js}<br/>';
                }
                if ((value & Enum.DiscardOnLeaseDeleting) == Enum.DiscardOnLeaseDeleting) {
                    strText += '&bull; @{R=IPAM.Strings;K=IPAMWEBJS_GK_9;E=js}<br/>';
                }
                if ((value & Enum.DynamicallyUpdateWhenNotRequestUpdates) == Enum.DynamicallyUpdateWhenNotRequestUpdates) {
                    strText += '&bull; @{R=IPAM.Strings;K=IPAMWEBJS_GK_10;E=js}<br/>';
                }
                return strText;
    };

    if (!$SW.IPAM.OptionsList)
        $SW.IPAM.OptionsList = function (opt) {
            var options = opt || {};
            $.extend(options, { GridId: undefined, WebId: undefined });
            var that = {
                getGrid: function () {
                    return $SW[options.GridId];
                },
                getWebIdParam: function () {
                    var w = options.WebId;
                    return (w && w != '') ? '&w=' + w : '';
                },
                ResizeColWidth: function() {

                    var totalWidth = 0;
                    var dhcpOptionGrid = that.getGrid();

                    if (dhcpOptionGrid == undefined)
                        return;
                        
                    var cm = dhcpOptionGrid.colModel;
                    for (var i = 0, len = cm.getColumnCount(); i < len; i++) {
                        if (!cm.isHidden(i))
                            totalWidth += cm.getColumnWidth(i);
                    }

                    dhcpOptionGrid.setWidth((totalWidth == 0) ? 950 : (totalWidth < 950) ? 950 : totalWidth);
                },
                SetWebId: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore(); 
                    store.baseParams.w = options.WebId;
                },
                AddOptionBtnPressed: function (el) {
                    var gridid = that.getGrid();
                    gridid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                    $SW.IPAM.ShowLayout({
                        WebId: options.WebId, OptionCode: '',isAddMode:true,CallbackFn: function () { var grid = that.getGrid(); var store = grid.getStore(); store.reload(); }, loadMaskFn: function () { gridid.getEl().unmask(); }
                    });
                },
                ServerRefresh: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore();
                    store.reload();
                },
                Editoption: function (el, r) {
                    var gridid = that.getGrid();
                    gridid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading');
                    $SW.IPAM.ShowLayout({ WebId: options.WebId, OptionCode: r.data['OptionCode'], isAddMode: false, CallbackFn: function () { var grid = that.getGrid(); var store = grid.getStore(); store.reload(); }, loadMaskFn: function () { gridid.getEl().unmask(); } });
                },
                OptionRemove: function (el, r) {
                    var result =confirm('@{R=IPAM.Strings;K=IPAMWEBJS_GK_6;E=js}');
                    if (result) {
                        Ext.Ajax.request({
                            url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                            success: function(p1, p2, p3) {
                                that.ServerRefresh();
                            },
                            failure: function() {
                                Ext.Msg.show({
                                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                    buttons: Ext.Msg.OK,
                                    animEl: that.id,
                                    icon: Ext.MessageBox.ERROR
                                });
                            },
                            params:
                            {
                                entity: 'AllOptions',
                                verb: 'Remove',
                                ids: r.data["OptionCode"],
                                value: r.data['Values'],
                                w: options.WebId
                            }
                        });
                    }
                },
            };
            return that;
        };

})();
