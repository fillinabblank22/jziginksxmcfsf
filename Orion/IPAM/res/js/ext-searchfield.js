﻿Ext.ns('Ext.ux.grid');

Ext.ux.grid.SearchFieldEx = Ext.extend(Ext.form.TwinTriggerField, {
    initComponent: function () {
        Ext.ux.grid.SearchFieldEx.superclass.initComponent.call(this);
        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER) {
                this.onTrigger2Click();
            }
        }, this);
    },

    validationEvent: false,
    validateOnBlur: false,
    trigger1Class: 'x-form-clear-trigger',
    trigger2Class: 'x-form-search-trigger',
    hideTrigger1: true,
    width: 180,
    hasSearch: false,
    paramWhereClause: 'whereClause',

    grid: undefined,
    getGrid: function () { return Ext.getCmp(this.grid); },
    ensureStore: function () {
        if (this.store === undefined) this.store = this.getGrid().store;
        return this.store !== undefined;
    },

    onTrigger1Click: function () {
        if (this.hasSearch) {
            this.el.dom.value = '';
            var o = { start: 0 };
            if (this.ensureStore()) {
                this.store.baseParams = this.store.baseParams || {};
                this.store.baseParams[this.paramWhereClause] = '';
                this.store.reload({ params: o });
            }
            this.triggers[0].hide();
            this.hasSearch = false;
        }
    },

    onTrigger2Click: function () {
        if (this.ensureStore()) {
            this.store.baseParams = this.store.baseParams || {};

            var v = this.getRawValue();
            if (v.length < 1) { this.onTrigger1Click(); return; }

            var where = this.searchSupplyFn(this.grid, v);
            if (!Ext.isEmpty(where)) where = '(' + where + ')';
            this.store.baseParams[this.paramWhereClause] = where;

            var o = { start: 0 };
            this.store.reload({ params: o });
        }
        this.hasSearch = true;
        this.triggers[0].show();
    },

    searchSupplyFn: function (grid, val) {
        var g = Ext.getCmp(grid), cm = g.colModel;
        var cols = g.SearchByColumns || [], clauses = [];
        if (cols.length < 1) cols = this.searchfields || [];

        Ext.each(cols, function (c, i) {
            var i = cm.findColumnIndex(c), col = cm.getColumnAt(i);
            if (col) clauses.push("{0}." + col.dataIndex + " LIKE '%" + val + "%'");
        });

        return clauses.join(' OR ');
    }
});

Ext.override(Ext.grid.GridView, {
    renderHeaders: function () {
        var cm = this.cm,
            ts = this.templates,
            ct = ts.hcell,
            cb = [],
            p = {},
            len = cm.getColumnCount(),
            last = len - 1;
        for (var i = 0; i < len; i++) {
            p.id = cm.getColumnId(i);
            p.value = cm.getColumnHeader(i) || '';
            p.style = this.getColumnStyle(i, true);
            p.tooltip = this.getColumnTooltip(i);
            p.css = i === 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
            if (cm.config[i].headerCls) p.css += cm.config[i].headerCls;
            if (cm.config[i].align == 'right') {
                p.istyle = 'padding-right:16px';
            } else {
                delete p.istyle;
            }
            cb[cb.length] = ct.apply(p);
        }
        return ts.header.apply({
            cells: cb.join(''),
            tstyle: 'width:' + this.getTotalWidth() + ';'
        });
    }
});

Ext.ux.grid.SearchFieldEx.onSearchByItemCheck = function (gid) {
    var gridid = gid;
    return function (item, checked) {
        var g = Ext.getCmp(gridid), sbc = g.SearchByColumns;
        if (!sbc) sbc = g.SearchByColumns = [];

        var i = g.colModel.findColumnIndex(item.dataIndex),
            col = g.colModel.getColumnAt(i);
        if (!col) return;

        if (checked) {
            sbc.push(item.dataIndex);
            col.headerCls = 'searchby-highlight';
        } else {
            sbc.remove(item.dataIndex);
            col.headerCls = '';
        }
        g.getView().updateHeaders();
    };
};
