﻿var rangeDialog,webId, Id, poolId, poolType, rangeValue, Mode, RangeId,ScopeId,rowCount, processType, ipRangeGridCols, popupToolbar, rowEditing, hideToolBar,msgTitle;

$SW.IPAM.IPRangeInitializeGrid = function (webid, id, poolid, pooltype, mode, rangeId,scopeId) {
    
    webId = webid;
    Id = id;
    poolId = poolid;
    poolType = pooltype;
    Mode = mode;
    RangeId = rangeId;
    ScopeId = scopeId;
    rangeValue = Id + "," + poolId + "," + poolType + "," + Mode + "," + RangeId + "," + ScopeId;
    if (Mode == '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_16;E=js}')
        msgTitle = '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_11;E=js} ';
    else if (Mode == '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_20;E=js}')
        msgTitle = '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_12;E=js} ';
    else if (Mode == '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_15;E=js}')
        msgTitle = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_34;E=js}';
    else if (Mode == '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_19;E=js}')
        msgTitle = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_35;E=js}';

    $SW.IPAM.LoadIpRangeGrid('value', webId, rangeValue);
    
    var btnAddIpRange = Ext.getCmp('AddIpRange');
    if (Mode == 'EditIPAddress')
        btnAddIpRange.disable();
    else
        btnAddIpRange.enable();

    $SW.IPAM.ShowIpRangeLayoutDialog.ShowDialog(function () {
        $SW.IPAM.RefreshTreeRangeGrid();
        rangeDialog.off("dialogclose");
    });
};

function matchRegularExp(str, expressionStat) {
    var expPattern = new RegExp(expressionStat);
    return expPattern.test(str);
};

function GetErrorsHTML(errArray) {

    var errorsHtml = '';

    errorsHtml += '<tpl> <b>' + '@{R=IPAM.Strings;K=IPAMWEBJS_SE_10;E=js}' + '</b><br />';

    $.each(errArray, function (index, value) {
        errorsHtml += '<br>■ &ensp;' + value.message;
    });

    errorsHtml += '</ul></tpl>';

    return errorsHtml;
}

$SW.IPAM.LoadIpRangeDialogLayout = function (layoutId) {
    rangeDialog = $("#" + layoutId).dialog({
        autoOpen: false,
        position: 'center',
        draggable: true,
        width: 482,
        height: 393,
        modal: true,
        title: msgTitle,
    });

    $SW.IPAM.ShowIpRangeLayoutDialog = function (rangeDialog) {
        return {
            ShowDialog: function (closeFunction) {
                if (typeof (closeFunction) != "undefined") {
                    rangeDialog.on("dialogclose", closeFunction);
                } else {
                    rangeDialog.off("dialogclose");
                }
                rangeDialog.dialog('option', 'title', msgTitle);
                rangeDialog.dialog("open");
            },
            HideDialog: function () {
                rangeDialog.dialog("close");
            },
            SaveShowStatus: function (onSuccess, onFailure) {
                //Write code if we need to do something on closing panel 
            }
        };
    }(rangeDialog);
};

Ext.ux.grid.IpRangeRecordEditor = Ext.extend(Ext.ux.grid.RowEditor, {

    saveText: '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_20;E=js}',
    //[oh] disable edit on click / dblclick
    onRowClick: function () { },
    onRowDblClick: function () { },
    onGridKey: function () { },
    preEditValue: function (r, field) {
        var src = $SW.IPAM.IpAddrRecords;
        return Ext.ux.grid.IpRangeRecordEditor.superclass.preEditValue.apply(this, arguments);
    },
    stopEditing: function (saveChanges) {
        if (saveChanges === true && CheckDemoMode()) {
            demoAction('IPAM_IPEdit_Option', null);
            return false;
        }
        if (this.isVisible() && saveChanges !== false && this.isValid()) {
            var changes = {},
            r = this.record,
            hasChange = false,
            cm = this.grid.colModel,
            fields = this.items.items;
            for (var i = 0, len = cm.getColumnCount() ; i < len; i++) {
                if (!cm.isHidden(i)) {
                    var dindex = cm.getDataIndex(i);
                    if (!Ext.isEmpty(dindex)) {
                        var oldValue = r.data[dindex],
                        value = this.postEditValue(fields[i].getValue(), oldValue, r, dindex);
                        if (String(oldValue) !== String(value)) {
                            changes[dindex] = value;
                            hasChange = true;
                        }
                    }
                }
            }
            if (!hasChange)
                this.fireEvent('validateedit', this, changes, this.record, this.rowIndex);
        }
        Ext.ux.grid.IpRangeRecordEditor.superclass.stopEditing.apply(this, arguments);

    },
    listeners: {
        canceledit: function (editor, isCancel) {
            editor.stopEditing();
            var s = editor.grid.store, ri = editor.rowIndex, r = s.getAt(ri);
            var id = (r && r.data) ? r.data['AutoId'] || -1 : -1;
            if (id < 0) s.removeAt(ri);
            editor.grid.getView().refresh();
        },
        beforeedit: function (editor, e) {
            if (editor.grid.colIdx == 2)
                return false;
        },
        validateedit: function (editor, m, r, ri) {
            
            var s = editor.grid.store;
            
            if (!r || !m) return false;
            var id = (r && r.data) ? r.data['AutoId'] || -1 : -1;
            var valid = true, addMode = id < 0, errs = [];
            var hasChange = false;
            
            var ValidateIpRangeData = function (startAddr, endAddr) {
                var emptyErrorMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_05;E=js}';
                var isNotValidMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_11;E=js}';
                var isAlreadyExistsMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_29;E=js}';

                if (startAddr.length == 0 && endAddr.length == 0)
                    return emptyErrorMsg;

                if (startAddr == '')
                    return emptyErrorMsg;
                else if (!$SW.IP.v4_isok($SW.IP.v4(startAddr)))
                    return isNotValidMsg;
                else if (!matchRegularExp(startAddr, "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$"))
                    return isNotValidMsg;

                if (endAddr != '') {
                    if (!$SW.IP.v4_isok($SW.IP.v4(endAddr)))
                        return isNotValidMsg;
                    else if (!matchRegularExp(endAddr, "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$"))
                        return isNotValidMsg;
                }

                var count = 0;
                endAddr = (endAddr == '') ? startAddr : endAddr;

                s.each(function (rec) {
                    if ((rec.get('StartIPAddr') == startAddr) && (rec.get('EndIPAddr') == endAddr)) {
                        count += 1;
                    }
                });

                if ((count > 1 && addMode) || (count > 1 && hasChange && !addMode))
                    return isAlreadyExistsMsg;
            };

            if (m["StartIPAddr"] != r.data["StartIPAddr"] || m["EndIPAddr"] != r.data["EndIPAddr"])
                hasChange = true;

            //check for empty fields
            var ipAddress1 = m["StartIPAddr"] != undefined ? m["StartIPAddr"] : r.data["StartIPAddr"];
            var ipAddress2 = m["EndIPAddr"] != undefined ? m["EndIPAddr"] : r.data["EndIPAddr"];

            var msg = ValidateIpRangeData(ipAddress1, ipAddress2);

            if (msg !== undefined) {
                valid = false;
                errs.push({ message: msg });
            }
            
            var serverValidationMsg = (!valid) ? '' : ServerValidation(editor, ipAddress1, ipAddress2);
            
            if (serverValidationMsg != '' && serverValidationMsg != undefined) {
                valid = false;
                errs.push({ message: serverValidationMsg });
            }
            
            if (hasChange && addMode && !valid) {
                r.beginEdit();
                Ext.iterate(m, function (n, v) {
                    r.set(n, v);
                });
                r.endEdit();
            }

            if (errs && errs.length > 0) {
                Ext.MessageBox.show({
                    title: msgTitle,
                    msg: GetErrorsHTML(errs),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR,
                    width: 350,
                    fn: function (btn) {
                        if (btn == "ok") {
                            editor.startEditing(ri, false);
                        }
                    }
                });
                valid = false;
            }

          return valid;
        },
        afteredit: function (editor, obj, rec, index) {
            
            Id = editor.record.data.AutoId;
            $SW.IPAM.LoadIpRangeGrid('value', webId, rangeValue);

        }
    }
});

function ServerValidation(editor, ipAddress1, ipAddress2) {
    var epoolId;
    var msg = '';
    if (Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_20;E=js}") {
        epoolId = poolId;
    } else if (Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_16;E=js}") {
        epoolId = editor.record.data.PoolId;
    } else
        epoolId = -1;

    var values = editor.record.data.AutoId + "," + editor.record.data.ScopeId + "," + epoolId + "," + ipAddress1 + "," + ((ipAddress2 == "" || ipAddress2 == null) ? ipAddress1 : ipAddress2) + "," + editor.record.data.PoolType + "," + editor.record.data.RangeId + "," + Mode;
    $.ajax({
        type: "POST",
        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
        success: function (result, request) {
            var res = Ext.util.JSON.decode(result.responseText);
            if (res.success == false)
                msg = res.message;
        },
        failure: function (message) {
            msg = message;
        },
        complete: function (result, request) {
            var res = Ext.util.JSON.decode(result.responseText);
            if (res.success == false)
                msg = res.message;
        },
        async: false,
        data:
        {
            entity: 'IPRange',
            verb: 'SaveEdit',
            value: values,
            w: webId
        }
    });

    return msg;
}

Ext.preg('sw-ipRangeRecord-editor', Ext.ux.grid.IpRangeRecordEditor);

$SW.IPAM.GlobalSaveRecord = function () {
    
    if (rowCount <= 0 && (Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_15;E=js}" || Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_16;E=js}")) {
        Ext.MessageBox.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_22;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_14;E=js}',
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width: 350
            
        });
        return;
    }
    var resultSet = "";

    resultSet = Mode + "," + poolType + "," + poolId + "," + Id + "," + RangeId;

    Ext.Ajax.request({
        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
        success: function (result, request) {
            var res = Ext.util.JSON.decode(result.responseText);
            if (res.success == true) {
                $SW.IPAM.ShowIpRangeLayoutDialog.HideDialog();
             
            } else {
                Ext.Msg.show({
                    title: msgTitle,
                    msg: res.message,
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        },
        failure: function () {
            Ext.Msg.show({
                title: msgTitle,
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
        },
        params:
        {
            entity: 'IPRange',
            verb: 'GlobalSave',
            value: resultSet,
            w: webId
        }
    });
};

if (!$SW.IPAM.IpRangeRecords) $SW.IPAM.IpRangeRecords = (function () {
    return {
        AddRecord: function (grid) {

            var ePoolType;
            var eRangeId;
            if (Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_16;E=js}" || Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_20;E=js}") {
                ePoolType = "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_21;E=js}";
                eRangeId = -1;
            } else {
                ePoolType = "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_17;E=js}";
                eRangeId = 0;
            }
            if ((Mode == "@{R=IPAM.Strings;K=IPAMWEBJS_DF1_19;E=js}") && grid.getStore().getCount() > 0)
                return;
            
            //make a new empty record and stop any current editing
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            //add new record as the first row, select it
            grid.plugins.stopEditing(false);

            grid.store.insert(0, new blankRecord({
                AutoId: -1, ScopeId: (ScopeId != -1) ? ScopeId : -1, PoolId: 0, StartIPAddr: "", EndIPAddr: "", PoolType: ePoolType, RangeId: eRangeId
            }));
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);

            //start editing new record
            grid.plugins.startEditing(0, false);
        },
        EditRecord: function (grid, ri, ci, r, target) {
            grid.plugins.startEditing(ri, true);
        },
        RemoveRecord: function (grid, ri, ci, r) {
            
            Ext.Msg.show({
                title: msgTitle,
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_33;E=js}',
                buttons: Ext.Msg.OKCANCEL,
                icon: Ext.MessageBox.INFO,
                width: 350,
                fn: function (btn) {
                    if (btn === "ok") {

                        var resultSet = "";
                        resultSet = r.data["AutoId"] + "," + r.data["PoolId"];
                        Ext.Ajax.request({
                            url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                            success: function (result, request) {
                                var res = Ext.util.JSON.decode(result.responseText);
                                if (res.success == true) {
                                    $SW.IPAM.LoadIpRangeGrid('value', webId, rangeValue);
                                }
                                else
                                    Ext.Msg.show({
                                        title: msgTitle,
                                        msg: res.message,
                                        buttons: Ext.Msg.OK,
                                        icon: Ext.MessageBox.ERROR
                                    });
                            },
                            failure: function () {
                                Ext.Msg.show({
                                    title: msgTitle,
                                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            },
                            params:
                               {
                                   entity: 'IPRange',
                                   verb: 'Remove',
                                   value: resultSet,
                                   w: webId
                               }
                        });
                    }
                }
            });
        },
        grid_cellclick: function (grid, ri, ci, ev) {
            $SW.ext.gridClickToDrilldown(grid, ri, ci, ev);
            var targetEl = ev.browserEvent.target || ev.browserEvent.srcElement;
            if (targetEl.tagName.toUpperCase() == 'IMG') targetEl = targetEl.parentNode;
            if (targetEl.tagName.toUpperCase() != 'A') return false;
            var record = grid.getStore().getAt(ri);

            if (/sw-grid-edit/.test(targetEl.className)) {
                //  ChangeFieldStatus(true);
                $SW.IPAM.IpRangeRecords.EditRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-delete/.test(targetEl.className)) {
                $SW.IPAM.IpRangeRecords.RemoveRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
        }
    };
})();

$SW.IPAM.IpRangeList_renderer_buttons = function () {
    var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
    var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
    return function (value, meta, record) {
        var ret = '';
        meta.css = 'sw-grid-buttons';

        ret += "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
        if (Mode != 'EditIPAddress')
            ret += "<a id='rangeDelete' href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
        return ret;
    };
};

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        return true;
    }
    return false;
}

$SW.IPAM.LoadIpRangeGrid = function (paramVerb, w, paramValue) {
    $SW.store.baseParams.verb = paramVerb;
    $SW.store.baseParams.w = w;
    $SW.store.baseParams.value = paramValue;
    
    var store = Ext.getCmp('IpRangeGrid').getStore();
    store.reload();
};