﻿(function () {

    if (!$SW.IPAM) $SW.IPAM = {};
    var isAddMode = true, curreneEditCode,myData, cols, getSessionStorage, fields, gridStore, grid, displayPanel, webId, optCode, dialog, mask, errs = [], maskText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_12;E=js}';

    $SW.IPAM.InitializeControlvalues = function () {

        getSessionStorage = function () {

            myData = null;

            $.ajax({
                type: "POST",
                url: "/Orion/IPAM/sessiondataprovider.ashx",
                data: {
                    entity: 'AllOptions',
                    verb: 'GetAllWebMeta',
                    w: webId
                },
                async: false,
                success: function (r) {
                    myData = Ext.util.JSON.decode(r);
                },
                failure: function (xhr, status, error) {
                    if (xhr.responseText) {
                        var errorMsg = eval("(" + xhr.responseText + ")");
                        Ext.MessageBox.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                            msg: errorMsg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            width: 350,
                            fn: function () {
                            }
                        });
                    }
                }
            });
        };

        getSessionStorage();

        // Generic fields array to use in both store defs.
        fields = [
            { name: 'OptionCode', mapping: 0 },
            { name: 'OptionDescription', mapping: 1 },
            { name: 'Description', mapping: 4 },
            { name: 'DataType', mapping: 2 },
            { name: 'ControlName', mapping: 3 }
        ];

        // create the data store
        gridStore = new Ext.data.JsonStore({
            fields: fields,
            data: myData,
            root: 'rows'
        });

        // Column Model shortcut array
        cols = [
            { id: 'OptionCode', width: '0%', hidden: true, sortable: false, dataIndex: 'OptionCode', editable: false, },
            { id: 'OptionDescription', width: '100%', hidden: false, sortable: true, dataIndex: 'OptionDescription', editable: false },
            { id: 'Description', width: '0%', sortable: false, hidden: true, dataIndex: 'Description', editable: false, },
            { id: 'DataType', width: '0%', sortable: false, hidden: true, dataIndex: 'DataType', editable: false, },
            { id: 'ControlName', width: '0%', sortable: false, hidden: true, dataIndex: 'ControlName', editable: false, }
        ];
    };

    $SW.IPAM.InitializeGridLayoutControl = function () {

        grid = new Ext.grid.GridPanel({
            ddGroup: 'gridDDGroup',
            id: 'groupingGrid',
            store: gridStore,
            columns: cols,
            enableDragDrop: false,
            stripeRows: false,
            autoScroll: true,
            enableColumnMove: false,
            autoExpandColumn: 'OptionDescription',
            region: 'west',
            cls: 'hide-header',
            loadMask: true,
            anchor: '0 0',
            viewConfig: {
                forceFit: true,
                getRowClass: function (record, rowIndex, rowParams, store) {
                    if (isAddMode == false && record.get("OptionCode") != curreneEditCode && curreneEditCode != null && curreneEditCode != undefined)
                        return 'x-item-disabled';
                },
                emptyText: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_01;E=js}'
            },
            split: false,
            selModel: new Ext.grid.RowSelectionModel({ singleSelect: true }),
            listeners: {
                cellclick: function (mygrid, row, cell, e) {
                    var record = grid.getSelectionModel().getSelected();
                    if (isAddMode == false && record.get("OptionCode") == curreneEditCode && curreneEditCode != null && curreneEditCode != undefined)
                        $SW.IPAM.LoadUserControl(row);
                    else if (isAddMode == true)
                        $SW.IPAM.LoadUserControl(row);
                }
            }
        });
        
        //Set Grid Column Size
        var colModel = grid.getColumnModel();
        colModel.setColumnWidth(1, 200);
        grid.doLayout();
        grid.on('keydown', function (e) {
            if (e.keyCode == 40 || e.keyCode == 38) {
                var record = grid.getSelectionModel().getSelected();
                if (isAddMode == false && record.get("OptionCode") == curreneEditCode && curreneEditCode != null && curreneEditCode != undefined)
                    $SW.IPAM.LoadUserControl(grid.store.indexOf(record));
                else if(isAddMode == true)
                    $SW.IPAM.LoadUserControl(grid.store.indexOf(record));
            }
        });


        grid.store.on('beforeload', function () { grid.getEl().mask('@{R=Core.Strings;K=WEBJS_VB0_1; E=js}', 'x-mask-loading'); });

        grid.getSelectionModel().on("select", function (selMod, rowIndex, record) {
            if (isAddMode == false && record.get("OptionCode") == curreneEditCode && curreneEditCode != null && curreneEditCode != undefined)
                $SW.IPAM.LoadUserControl(rowIndex);
            else if (isAddMode == true)
                $SW.IPAM.LoadUserControl(rowIndex);
        });

        grid.store.on("exception", function (dataProxy, type, action, options, response, arg) {
            if (response.responseText) {
                var error = eval("(" + response.responseText + ")");
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                    msg: error.Message,
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
            grid.getEl().unmask();
        });
    };

    $SW.IPAM.InitializePanelLayoutControl = function () {

        //Simple 'border layout' panel to house both grids
        displayPanel = new Ext.Panel({
            region: 'west',
            split: false,
            anchor: '0 0',
            collapsible: false,
            layout: 'table',
            width: 200,
            viewConfig: { forceFit: true },
            renderTo: 'leftpanel',
            cls: 'panel-no-border',
            items: [grid]
        });

        displayPanel.on("bodyresize", function () {
            grid.setSize(displayPanel.getSize().width, 200);
        });
    };

    $SW.IPAM.InitializeLayoutControl = function () {

        $SW.IPAM.InitializeControlvalues();

        $SW.IPAM.InitializeGridLayoutControl();

        $SW.IPAM.InitializePanelLayoutControl();
    };
    
    function reloadContainer() {
        var rightPanelContainer = $('#UserControlContainer');
        if (rightPanelContainer != null) {
            rightPanelContainer.remove();
            $('#loadMaskContainer').html('<div id="UserControlContainer" style="margin-top: 2px;"></div>');
        }
        return;
    };

    $SW.IPAM.LoadUserControl = function (rowIndex) {

        $('#IPAddressGrid').addClass('hidden');
        $('#IPAddressPairGrid').addClass('hidden');
        reloadContainer();

        var optionCode = grid.store.data.items[rowIndex].data['OptionCode'];
        var optionDataType = grid.store.data.items[rowIndex].data['DataType'];
        var controlName = grid.store.data.items[rowIndex].data['ControlName'];

        if (!validateOptionParameter(optionCode, optionDataType, controlName)) {
            reloadContainer();
        }
        else {
            optCode = optionCode;

            if (controlName == '~/Orion/IPAM/Controls/GenericDhcpOptions/IPAddrType.ascx') {
                $SW.IPAM.SetOptionCode(optCode, webId, function() { $SW.IPAM.IpAddrRecords.LoadHelpText(optCode, webId); });
                $SW.IPAM.reloadGridStore();
                $('#IPAddressGrid').removeClass('hidden');
                reloadContainer();
            } else if (controlName == '~/Orion/IPAM/Controls/GenericDhcpOptions/IPPairType.ascx') {
                $SW.IPAM.SetOptionCode(optCode, webId, function() { $SW.IPAM.IpPairRecords.LoadHelpText(optCode, webId); });
                $SW.IPAM.reloadPairGridStore();
                $('#IPAddressPairGrid').removeClass('hidden');
                reloadContainer();
            } else {
                SW.Core.Loader.Control('#UserControlContainer', {
                    Control: controlName,
                    OptionCode: optionCode,
                    OptionDataType: optionDataType,
                    WebId: webId
                }, {}, 'replace', function(e) {
                    if (e == 'session.timeout')
                        window.location = '/orion/login.aspx?sessionTimeout=yes&ReturnUrl=' +
                            encodeURIComponent(window.location.pathname + window.location.search + window.location.hash) + '&';
                });
            }
        }
    };
   
    $SW.IPAM.LoadDialogLayout = function (layoutId,webParamId) {

        webId = webParamId;
        
        dialog = $("#" + layoutId).dialog({
            autoOpen: false,
            position: 'center',
            draggable: true,
            width: 1000,
            height: 570,
            modal: true,
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_02;E=js}',
            close: function () {
                $SW.IPAM.ShowOptionLayoutDialog.SaveShowStatus(function () {
                }, function () {
                });
            }
        });

        $SW.IPAM.ShowOptionLayoutDialog = function (dialog) {
            return {
                ShowDialog: function (closeFunction) {
                    if (typeof (closeFunction) != "undefined") {
                        dialog.on("dialogclose", closeFunction);
                    } else {
                        dialog.off("dialogclose");
                    }
                    dialog.dialog("open");
                },
                HideDialog: function () {
                    dialog.dialog("close");
                },
                SaveShowStatus: function (onSuccess, onFailure) {
                    //Write code if we need to do something on closing panel 
                }
            };
        }(dialog);
        
        $SW.IPAM.InitializeLayoutControl();
    };

    $SW.IPAM.ShowLayout = function (config) {
        
        optCode = config.OptionCode;
        webId = config.WebId;
        isAddMode = config.isAddMode;
        curreneEditCode = config.OptionCode;

        grid.getView().refresh();
        
        var gridStoreCountValue = grid.getStore().getCount();
        var store = grid.getStore();
        var index = store.find('OptionCode', optCode);
        
        //$('#leftpanel').animate({ scrollTop: ((index / 2) * 32) + index }, 'high');
        
        var rightPanelContainerId = $('#UserControlContainer');
        if (rightPanelContainerId != null)
            rightPanelContainerId.html('');

        selectDefaultOptionCode(config, gridStoreCountValue, index);
        
        $SW.IPAM.ShowOptionLayoutDialog.ShowDialog(function () {
            var rightPanelContainer = $('#UserControlContainer');

            if (rightPanelContainer != null)
                rightPanelContainer.html('');

            config.CallbackFn();
            dialog.off("dialogclose");
        });
        
        $('#leftpanel').animate({ scrollTop: index * ((grid.getGridEl().dom.clientHeight - $('#leftpanel').outerHeight() + $('#leftpanel').position().top) / gridStoreCountValue) }, 'high');
    };

    $SW.IPAM.GetOptionValue = function (webParam, optionCode) {

        mask = new Ext.LoadMask(Ext.get('loadMaskContainer'), { msg: maskText });
        mask.show();
        var ret = {};

        $.ajax({
            type: "POST",
            url: "/Orion/IPAM/sessiondataprovider.ashx",
            data: {
                entity: 'AllOptions',
                verb: 'GetByCode',
                webID: webParam,
                optCode: optionCode
            },
            async: false,
            success: function (r) {
                ret = Ext.util.JSON.decode(r);
            },
            failure: function (xhr, status, error) {
                if (xhr.responseText) {
                    var errorMsg = eval("(" + xhr.responseText + ")");
                    Ext.MessageBox.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                        msg: errorMsg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 350,
                        fn: function () {
                        }
                    });
                }
            }
        });
        mask.hide();
        return ret;
    };

    $SW.IPAM.SetOptionValue = function (webParam, optionCode, optionValue,isIpAddrArrayType) {

        mask = new Ext.LoadMask(Ext.get('loadMaskContainer'), { msg: maskText });
        mask.show();

        $.ajax({
            type: "POST",
            url: "/Orion/IPAM/sessiondataprovider.ashx",
            data: {
                entity: 'AllOptions',
                verb: 'SetByCode',
                webID: webParam,
                optCode: optionCode,
                optValue: optionValue,
                OptionDataType: isIpAddrArrayType,
            },
            async: false,
            success: function (r) {
                $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
            },
            failure: function (xhr, status, error) {
                if (xhr.responseText) {
                    var errorMsg = eval("(" + xhr.responseText + ")");
                    Ext.MessageBox.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                        msg: errorMsg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 350,
                        fn: function () {
                        }
                    });
                }
            }
        });
        mask.hide();
    };

    $SW.IPAM.HandleCommonTypeValidation = function (optValue, optDataType) {

        errs = [];

        if (checkEmpty(optValue))

            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_06;E=js}');

        else {

            optDataType = TryParseInt(optDataType, 0);

            switch (optDataType) {
                case 0:
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_06;E=js}');
                    break;
                case 2:
                    $SW.IPAM.CommonTypeValidator.longTypeValidation(optValue);
                    break;
                case 3:
                    $SW.IPAM.CommonTypeValidator.stringTypeValidation(optValue);
                    break;
                case 4:
                    $SW.IPAM.CommonTypeValidator.binaryArrayTypeValidation(optValue);
                    break;
                case 5:
                    $SW.IPAM.CommonTypeValidator.ipAddressTypeValidation(optValue);
                    break;
                case 6:
                    $SW.IPAM.CommonTypeValidator.intTypeValidation(optValue);
                    break;
                case 7:
                    $SW.IPAM.CommonTypeValidator.smallIntTypeValidation(optValue);
                    break;
                case 8:
                    $SW.IPAM.CommonTypeValidator.intArrayTypeValidation(optValue);
                    break;
                case 9:
                    $SW.IPAM.CommonTypeValidator.binaryArrayTypeValidation(optValue);
                    break;
                case 10:
                    $SW.IPAM.CommonTypeValidator.stringArrayTypeValidation(optValue);
                    break;
                default:
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_06;E=js}');
                    break;
            }
        }

        return (GetErrors(errs) == false) ? false : true;
    };

    $SW.IPAM.CommonTypeValidator = function () {
        return {
            intTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, "^[+-]?([0-9]+(\.[0-9]+)?)$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
                if (optValue < 0 || optValue > 65535)
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_07;E=js}');
            },
            stringTypeValidation: function (optValue) {
            },
            smallIntTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, "^[+-]?([0-9]+(\.[0-9]+)?)$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
                if (optValue < 0 || optValue > 255)
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_27;E=js}');
            },
            longTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, "^[+-]?([0-9]+(\.[0-9]+)?)$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
            },
            intArrayTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, "^([+-]?[0-9]+(\.[0-9]+)?)(,[+-]?[0-9]+(\.[0-9]+)?)*$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
            },
            stringArrayTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, ".+(,.+)*$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
            },
            binaryArrayTypeValidation: function (optValue) {

                if (!matchRegularExp(optValue, "^[0-9A-F]{2}( [0-9A-F]{2})*$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_08;E=js}');
            },
            ipAddressTypeValidation: function (optValue) {
                
                if (!$SW.IP.v4_isok($SW.IP.v4(optValue)) || !matchRegularExp(optValue, "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$"))
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_11;E=js}');
            }
        };
    }();
    
    $SW.IPAM.LeaseTimeValidator = function () {
        return {
            dayValidation: function (dayValue, errMessage) {
                if (checkEmpty(dayValue))
                    errs.push(errMessage[0]);
                else if (dayValue < 0)
                    errs.push(errMessage[1]);
                else if (dayValue > 999)
                    errs.push(errMessage[2]);
            },
            hourValidation: function (hourValue, errMessage) {
                if (checkEmpty(hourValue))
                    errs.push(errMessage[3]);
                else if (hourValue < 0)
                    errs.push(errMessage[4]);
                else if (hourValue > 23)
                    errs.push(errMessage[5]);
            },
            minutesValidation: function (minutesValue, errMessage) {
                if (checkEmpty(minutesValue))
                    errs.push(errMessage[6]);
                else if (minutesValue < 0)
                    errs.push(errMessage[7]);
                else if (minutesValue > 59)
                    errs.push(errMessage[8]);
            },
        };
    }();
    
    $SW.IPAM.LeaseTimeValidation = function(limitSelected,dayValue,hourValue,minutesValue,errorMessage,serverType) {

        errs = [];
        var errMessages = errorMessage.split(",");
        
        if (limitSelected)
        {
            $SW.IPAM.LeaseTimeValidator.dayValidation(dayValue, errMessages);
            
            $SW.IPAM.LeaseTimeValidator.hourValidation(hourValue, errMessages);
            
            $SW.IPAM.LeaseTimeValidator.minutesValidation(minutesValue, errMessages);
            
            if (errs.length == 0){
                var leaseTimeValue = (dayValue * 86400) + (hourValue * 3600) + (minutesValue * 60);
                if (leaseTimeValue == '' || leaseTimeValue == '0') {
                    errs.push(errMessages[9]);
                }
                else if (((leaseTimeValue < 300) || (leaseTimeValue > 1048575)) && serverType == 'ASA') {
                    errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_32;E=js}');
                }
            }
            
            return (GetErrors(errs) == false) ? false : true;
        }
        else
        {
            return true;
        }
    };

    function GetErrors(errArray) {

        var errorsHtml = '';
        var status = true;

        if (errArray && errArray.length > 0) {

            errorsHtml += '<tpl> <b>' + '@{R=IPAM.Strings;K=IPAMWEBJS_SE_10;E=js}' + '</b><br />';
            errorsHtml += '<ul style="list-style: square inside;"><br />';

            $.each(errArray, function (index, value) {
                errorsHtml += '<li>' + value + '</li>' + '<br />';
            });

            errorsHtml += '</ul></tpl>';

            Ext.MessageBox.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                msg: errorsHtml,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR,
                width: 500,
                fn: function () { }
            });

            status = false;
        }

        return status;
    }

    function TryParseInt(str, defaultValue) {
        var retValue = defaultValue;
        if (str != null) {
            if (str.length > 0) {
                if (!isNaN(str)) {
                    retValue = parseInt(str);
                }
            }
        }
        return retValue;
    }

    function matchRegularExp(str, expressionStat) {
        var expPattern = new RegExp(expressionStat);
        return expPattern.test(str);
    }

    function checkEmpty(str) {
        if (str == '' || str == undefined)
            return true;
        else
            return false;
    }

    function validateOptionParameter(optionCode, optionDataType, controlName) {

        errs = [];

        if (checkEmpty(optionCode))
            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_13;E=js}');
        else if (TryParseInt(optionCode, -1) < 0)
            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_14;E=js}');

        if (checkEmpty(optionDataType))
            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_15;E=js}');
        else if (TryParseInt(optionDataType, -1) < 0)
            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_16;E=js}');

        if (checkEmpty(controlName))
            errs.push('@{R=IPAM.Strings;K=IPAMWEBJS_SE_17;E=js}');

        return (GetErrors(errs) == false) ? false : true;
    };

    function selectDefaultOptionCode(config, storeCount, index) {

        if (storeCount > 0) {
            if (optCode != null && optCode != undefined && optCode != '') {
                if (index != -1) {
                    grid.getSelectionModel().selectRow(index);
                    $SW.IPAM.LoadUserControl(index);
                }
            }
            else {
                grid.getSelectionModel().selectFirstRow();
                grid.getView().focusRow(0);
                $SW.IPAM.LoadUserControl(0);
            }
        }

        if (typeof (config.loadMaskFn()) != "undefined")
            config.loadMaskFn();

        return;
    }
    
    $SW.IPAM.GetHelpTextExample = function (optDataType) {

        var exampleHelpText = '';
        
        optDataType = TryParseInt(optDataType, 0);

        switch (optDataType) {
            case 0:
                exampleHelpText = '';
                break;
            case 2:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_18;E=js}';
                break;
            case 3:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_28;E=js}';
                break;
            case 4:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_20;E=js}';
                break;
            case 5:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_21;E=js}';
                break;
            case 6:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_22;E=js}';
                break;
            case 7:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_23;E=js}';
                break;
            case 8:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_24;E=js}';
                break;
            case 9:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_25;E=js}';
                break;
            case 10:
                exampleHelpText = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_26;E=js}';
                break;
            default:
                exampleHelpText = '';
                break;
        }

        return exampleHelpText;
    };

})();
