(function($) {

    $.jItems = {};
    $.jItemFind = function(id) { return $.jItems[id]; };
    $.jItemAdd = function(id, item) { if (id) $.jItems[id] = item; return item; };

    /*
    * jQuery.fn.jcombo - custom drawn combo box
    */
    $.fn.jcombo = function(o) {
        o = o || {};
        var opt = $.extend(o, {
            minWidth: o.minWidth || 32,
            minHeight: o.minHeight || 18,
            triggerWidth: o.triggerWidth || 18,

            inputCls: o.inputCls || 'jcombo-input',
            triggerCls: o.triggerCls || 'jcombo-trigger',
            contentCls: o.contentCls || 'jcombo-content',

            wraperCls: 'jcombo-wrap',
            focusCls: 'jcombo-focus',
            triggerHover: '-hover',
            triggerDown: '-down',

            scope: o.scope || this,
            onRender: o.onRender || function() { },
            onReady: o.onReady || function() { },
            onShow: o.onShow || function() { },
            onHide: o.onHide || function() { },
            onClick: o.onClick || function() { }
        });
        opt.hoverCls = opt.triggerCls + opt.triggerHover;
        opt.clickCls = opt.triggerCls + opt.triggerDown;

        // show / hide content functions
        var onTriggerClicked = function(elem, data, content) {
            var h = elem.outerHeight();
            var installHideContent = function() {
                $(document).click(hideContent);
            };
            var showContent = function() {
                var pos = elem.offset(); pos.top += h;
                content.show(); content.offset(pos);

                window.setTimeout(installHideContent, 200);
                opt.onShow.apply(opt.scope, [elem, data]);
            };
            var hideContent = function(e) {
                var cnt = content[0], el = e.target;
                if (cnt == el || $.contains(cnt, el) == true) return;
                $(document).unbind('click', hideContent);
                content.hide();
                opt.onHide.apply(opt.scope, [elem, data]);
            };

            return function(e) {
                e.preventDefault();
                if (content.is(':visible') == false) showContent();
                opt.onClick.apply(opt.scope, [elem, data]);
            };
        };

        return this.each(function() {
            var elem = $(this).addClass(opt.inputCls);
            var wrap = $('<span/>').addClass(opt.wraperCls);
            var trig = $('<div/>').addClass(opt.triggerCls);
            var content = $('<div/>').addClass(opt.contentCls).hide();

            // calc exact size / position
            var w = (opt.width || elem.outerWidth());
            var h = opt.height || elem.outerHeight();
            if (w < opt.minWidth) w = opt.minWidth;
            if (h < opt.minHeight) h = opt.minHeight;

            wrap.width(w).height(h + 2);
            elem.width(w - opt.triggerWidth).height(h);
            trig.width(opt.triggerWidth).height(h + 1);
            content.width(w);

            // animate trigger button
            trig.hover(
                function() { $(this).addClass(opt.hoverCls); },
                function() { $(this).removeClass(opt.hoverCls); }
            ).mousedown(
                function() { $(this).addClass(opt.clickCls); }
            ).mouseup(
                function() { $(this).removeClass(opt.clickCls); }
            );

            // render content data
            var data = opt.onRender.apply(opt.scope, [elem]);
            $.jItemAdd(opt.id, data);

            // handle trigger click
            var trigEvnt = onTriggerClicked(elem, data, content);
            trig.click(trigEvnt);

            // insert elements into DOM
            elem.wrap(wrap); elem.after(trig);
            elem.wrap($('<span>'));
            trig.wrap($('<span>').css('float', 'none'));
            content.append(data.content).appendTo($('body'));

            opt.onReady.apply(opt.scope, [data]);
        });
    };

    /*
    * jQuery.ImgCheckBox - custom drawn image check-box
    */
    $.ImgCheckBox = function(o) {
        o = o || {};
        var opt = $.extend(o, {
            id: o.id,
            label: o.label || '',
            checked: o.checked || false,

            cls: o.cls || 'jmenu-checkbox',
            checkCls: o.checkCls || 'jmenu-checked',
            uncheckCls: o.uncheckCls || 'jmenu-unchecked',
            hoverCls: o.hoverCls || 'jmenu-hover',

            scope: o.scope || this,
            onClick: o.onClick || function(checked) { }
        });

        var setClass = function() {
            if (opt.checked == true) {
                chb.removeClass(opt.uncheckCls);
                chb.addClass(opt.checkCls);
            } else {
                chb.removeClass(opt.checkCls);
                chb.addClass(opt.uncheckCls);
            }
        };

        var onImgClicked = function(e) {
            e.preventDefault();
            opt.checked = opt.checked ? false : true;
            setClass();
            opt.onClick.apply(opt.scope, [opt.checked, opt]);
        };
        var onImgKeypress = function(e) {
            // trigger on space or enter keys
            var k = (e.which) ? e.which : ((e.keyCode) ? e.keyCode : 0);
            if (k == 13 || k == 32) { $(this).click(); }
        };

        var chb = $('<div/>').attr('id', opt.id).addClass(opt.cls).css({ cursor: 'pointer' })
            .click(onImgClicked).keypress(onImgKeypress).hover(
                function() { $(this).addClass(opt.hoverCls); },
                function() { $(this).removeClass(opt.hoverCls); }
            ).html(opt.label);
        setClass();

        return $.extend(opt, {
            element: chb,
            check: function(checked) {
                opt.checked = checked;
                setClass(checked);
            },
            isChecked: function() { return opt.checked; }
        });
    };

    $.fn.ImgCheckBox = function(o) {
        var index = 0;
        return this.each(function() {
            var $this = $(this);
            o.id = 'chb-gen-' + $this.attr('id') || index++;
            $.ImgCheckBox(o).element.appendTo($this);
        });
    };

    /*
    * jQuery.jmenu - context menu like check-box menu
    */
    var jMenuIndex = 0; // global jmenu item index
    $.fn.jMenuItems = function(items, onItemClick, onItemClickScope) {
        items = items || [];
        var checkboxes = [];
        this.each(function() {
            var $this = $(this);
            $.each(items, function(i, value) {
                var listItem = $('<li/>').hover(
                    function() { $(this).addClass('jmenu-list-hover'); },
                    function() { $(this).removeClass('jmenu-list-hover'); }
                );

                if (value.text == 'separator') {
                    var separator = $('<span>&nbsp;</span>');
                    listItem.addClass('jmenu-list-sep').append(separator).appendTo($this);
                    return true;
                }

                var chb = $.ImgCheckBox({
                    id: value.id || ('jmenu-item-' + jMenuIndex),
                    label: '' + value.text,
                    item: value,
                    checked: value.checked,
                    scope: onItemClickScope,
                    onClick: onItemClick
                });

                listItem.append(chb.element).appendTo($this);
                checkboxes.push(chb);
            });
            jMenuIndex = jMenuIndex + 1;
        });
        return checkboxes;
    };

    $.jmenu = function(o) {
        o = o || {};
        var opt = $.extend(o, {
            id: o.id,
            items: o.items || [],
            cls: o.cls || 'jmenu-list'
        });

        opt.content = $('<ul>').addClass(opt.cls);
        opt.checkboxes = opt.content.jMenuItems(opt.items, opt.onClick, opt);
        return opt;
    };
})(jQuery);
