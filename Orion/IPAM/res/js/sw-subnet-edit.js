
$SW.LicenseInfo = null;
$SW.LicenseListeners.push(function(lic){ $SW.LicenseInfo = lic; });

$SW.CustomPropertyFormDisable = function(showAvailableLabel, disabled, divName){    
    var el = document.getElementById(divName),
    availableLabel = $('#availableLabel'),
    inputElement = el.getElementsByTagName('input'),
    selectElement = el.getElementsByTagName('select'),
    i,j,k;

    for (i = 0; i < inputElement.length; i++) {
        inputElement[i].disabled = disabled;
        inputElement[i].readOnly = disabled;
    }
    for (j = 0; j < selectElement.length; j++) {
        selectElement[j].disabled = disabled;
        selectElement[j].readOnly = disabled;
    }

    if(showAvailableLabel){
        availableLabel.show();
        $('.cpValueFormat .sw-validation-error').remove();
    }
    else{
        availableLabel.hide();
    }
}

$SW.SubnetSyncScanDisable = function($nsId,initialRun)
{
	var cbDisable = $SW.ns$( $nsId, 'cbDisableAutoScanning' )[0],
	    div = $('#ScanSettings'),
	    req = $SW.ns$($nsId, 'reqriredScanInterval'),
	    range = $SW.ns$($nsId, 'rangeScanInterval'),
	    delay = initialRun ? 0 : 200;
	
	if( !cbDisable ) return;
	if (cbDisable.checked){
	    div.hide(delay);
	    if (req.length > 0)
	        ValidatorEnable(req[0], false);
	    if (range.length > 0)
	        ValidatorEnable(range[0], false);
	}
	else{
	    div.show(delay);
	    if (range.length > 0)
	        ValidatorEnable(range[0], true);
	    if (req.length > 0)
	        ValidatorEnable(req[0], true);
	}
};

$SW.SubnetEditorInit = function(nsId)
{
    var $nsId = nsId;
    var width, scanTextBox, scanInterval, scanSlider,
        maskbox = $SW.ns$(nsId,'txtNetworkMask'),
        cidrbox = $SW.ns$(nsId,'txtCidr'),
        namebox = $SW.ns$(nsId,'txtDisplayName'),
        ipbox = $SW.ns$(nsId,'txtNetworkAddress'),
        addcheck = $SW.ns$(nsId,'cbAddIPAddresses');

    // subnet mask & cidr are joined (they update each other)
    // also, they update the ip address list below

    function ips_frombox(box)
    {
      var a;
      if( box && $SW.IP.v4_isok(a = $SW.IP.v4( box.value )) )
        return a;
       return null;
    }
    
    var lastcount = null;
    
    function watchaddcheckbox(e)
    {
      if( lastcount != null )
        updateaddcheckbox(lastcount,true);
    }

    function updateaddcheckbox(count,ischk)
    {
        var cb = $SW.ns$($nsId,'cbAddIPAddresses');
        if( !cb[0] ) return;

        lastcount = count;
        ischk = !!ischk;

        if( count == null )
        {
          $('#subnettoolarge').css("display","none").removeClass('sw-form-disabled');
          $(cb[0].parentNode).removeClass('sw-form-disabled');
          return;
        }

        var lic_max = parseInt($SW.LicenseInfo.countmax) || 0;
        var lic_pos = parseInt($SW.LicenseInfo.countcurrent);
        if( isNaN(lic_pos) ) licpos = lic_max;
        var avail, noroom, toolarge;

        if( lic_pos >= lic_max )
        {
           avail = 0;
           noroom = true;
           toolarge = false;
        }
        else
        {
           avail = lic_max - lic_pos;
           noroom = count > avail;
           toolarge = count > 4096;
        }
        
        if( count > 4096 )
          cb[0].checked = false;
        else if( !ischk && count <= 4096 )
          cb[0].checked = true;

        if( toolarge )
        {
          $(cb[0].parentNode).addClass('sw-form-disabled');
          $('#subnettoolarge').removeClass('sw-form-disabled').css("display","");
        }
        else if( !cb[0].checked )
        {
          $('#subnettoolarge').css("display","none").removeClass('sw-form-disabled');
          $(cb[0].parentNode).removeClass('sw-form-disabled');
        }
        else if( noroom )
        {
          $(cb[0].parentNode).removeClass('sw-form-disabled');
          $('#subnettoolarge').css("display","none").removeClass( "sw-form-disabled" );
        }
        else
        {
          $(cb[0].parentNode).removeClass('sw-form-disabled');
          $('#subnettoolarge').removeClass('sw-form-disabled').css("display","none");
        }
    }

    function updateipcount()
    {
        var ipcount = $('#ipcount'),
            addr = $SW.ns$(nsId,'txtNetworkAddress'),
            mask = $SW.ns$(nsId,'txtNetworkMask'),
            cidr = $SW.ns$(nsId,'txtCidr');

        if( !ipcount[0] ) return;

        var a = ips_frombox(addr[0]), m = ips_frombox(mask[0]);

        if( a && m && $SW.IP.v4_ismask(m) )
        {
            var range = $SW.IP.v4_subnet(a,m),
                start = $SW.IP.v4_toint(range.start),
                end = $SW.IP.v4_toint(range.end),
                count = end-start+1;

            updateaddcheckbox(count);
            if( count > 2 ) count-=2;

            ipcount.text( String.format('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_9;E=js}', count, range.start.join('.'), range.end.join('.') ) );
            return;
        }

        updateaddcheckbox(null);
        ipcount.text('@{R=IPAM.Strings;K=IPAMWEBJS_TM0_4;E=js}');
    };

    function mask_fromcidr(box)
    {
      if( !box || /[^0-9]/.test( box.value ) ) return null;
      var cidr = parseInt(box.value);
      if( isNaN(cidr) || cidr<0 || cidr>32 ) return null;
      return $SW.IP.v4_usecidr(cidr).join('.')              
    }
    function update_mask(mask) {
        if (!mask) return;
        d = $SW.ns$($nsId, 'txtNetworkMask')[0];
        d.value = mask;
        $SW.Valid.Retest(d);
    }

    function sync_fields(namechanged)
    {
        var addrbox = $SW.ns$(nsId,'txtNetworkAddress')[0],
            cidrbox = $SW.ns$(nsId,'txtCidr')[0],
            namebox = $SW.ns$(nsId,'txtDisplayName')[0],
            maskbox = $SW.ns$(nsId,'txtNetworkMask')[0];

        if( !addrbox || !cidrbox || !namebox || !maskbox ) return;

        var v = namebox.value.replace( /^\s+|\s+$/g, '' ),
            m = v.match( /^([0-9. \t]+)\s*([\\\/])\s*([0-9]+)$/ ),
            slash = m ? m[2] : '/',
            ip = m ? $SW.IP.v4(m[1]) : null,
            cidr = m ? m[3] : null,
            ipok = ip ? $SW.IP.v4_isok(ip) : false,
            cidrok = cidr ? (parseInt(cidr,10)<=32) : false;

        if(namechanged)
        {
            if( /^[^?]+edit[.]aspx([?]|$)/i.test( window.location ) )
            {
              // edit page: do not change the address!
            }
            else if( ipok && cidrok )
            {
                addrbox.value = ip.join('.');
                cidrbox.value = cidr;

                var mask = mask_fromcidr(cidrbox);
                update_mask(mask);
                
                updateipcount();
            }
            else
            {
                if( addrbox.value == '' )
                {
                    var a = $SW.IP.v4(namebox.value),
                        aok = $SW.IP.v4_isok(a),
                        c = parseInt(cidrbox.value),
                        cok = !isNaN(c) && c <=32 && c >= 0,
                        issubnet = (aok && cok) ? $SW.IP.v4_issubnet( a, $SW.IP.v4_cidr( ''+c ) ) : false;

                    if( issubnet )
                    {
                        addrbox.value = a.join('.');
                        updateipcount();
                    }
                }
            }
        }
        else
        {
            var a = $SW.IP.v4(addrbox.value),
                aok = $SW.IP.v4_isok(a),
                c = parseInt(cidrbox.value),
                cok = !isNaN(c) && c <= 32 && c >= 0,
                issubnet = (aok && cok) ? $SW.IP.v4_issubnet( a, $SW.IP.v4_cidr( ''+c ) ) : false;

            if( issubnet )
            {
                if( v == '' || (ipok && cidrok) )
                    namebox.value = a.join('.') + ' ' + slash + c;
                else if( $SW.IP.v4_isok($SW.IP.v4(namebox.value)) )
                    namebox.value = a.join('.');
            }

            updateipcount();
        }
    }

    if( maskbox[0] && cidrbox[0] )
    {
        function addressupdated(e)
        {
          e = e || window.event;
          if( e.keyCode == 9 ) return; // ignore keyup on a tab
          sync_fields(false);
        };

        function cidrupdated(e)
        {
          e = e || window.event;
          if( e.keyCode == 9 ) return; // ignore keyup on a tab

          var mask = mask_fromcidr(e.target || e.srcElement);
          update_mask(mask);
          sync_fields(false);
        };

        cidrbox.bind( 'change keyup', cidrupdated );
        ipbox.bind( 'change keyup', addressupdated );
    }

    updateipcount();

    // when a person types in an ip address for the name, we'll bring that into the ip value (if blank)
    if( ipbox[0] && namebox[0] )
       namebox.bind( 'change keyup', function(){sync_fields(true);} );

    if( addcheck[0] )
       addcheck.bind( 'click', watchaddcheckbox );

    var transform = function(id){

        var item = $('#'+id)[0];
        if( !item) return null;

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };

    $SW.ns$($nsId,'cbDisableAutoScanning').click( function(){ $SW.SubnetSyncScanDisable($nsId); } );

    $SW.SubnetSyncScanDisable($nsId,true);

    if( scanTextBox )
        scanTextBox.hide();
};

$SW.NeighborSyncScanDisable = function($nsId,initialRun)
{
	var cbDisable = $SW.ns$( $nsId, 'cbDisableNeighborScanning' )[0],
	    div = $('#NeighborSettings'),
	    delay = initialRun ? 0 : 200;
	
	if( !cbDisable ) return;
	if(cbDisable.checked) div.hide(delay);
	else div.show(delay);
};


var SWDefDesiredSubnetSize = [0,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,22,23,24,25,26,27,28,29,30,30,30,30,30,30,30,30];

$SW.SubnetCalcInit = function(nsId)
{

    var $nsId = nsId;
    
   
    var transform = function(id, width){

        var item = $('#'+id)[0];
        if( !item) return null;

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            hiddenId:id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };
    
    function mask_fromcidr(box)
    {
      if( !box || /[^0-9]/.test( box.value ) ) return null;
      var cidr = parseInt(box.value);
      if( isNaN(cidr) || cidr<0 || cidr>32 ) return null;
      return $SW.IP.v4_usecidr(cidr).join('.')              
    }
    
    var sbcidrbox = transform($SW.nsGet($nsId,'ddlSubnetSize'));
    sbcidrbox.on('select', subnetcidrupdate);
    
    function subnetcidrupdate(e) {
        $SW['IsSubnetListValid'] = false;
        var val = e.value;

        if (val >= 21)
            $('.sw-credential-warning').hide(200);
        else
            $('.sw-credential-warning').show(200);
    }
    
    function supernetcidrupdate(e)
    {
        var mask = mask_fromcidr(e);
        if( mask )
        {
         var d = $SW.ns$($nsId,'txtSPNetworkMask')[0];
         d.value = mask;
        }
        $SW['IsSubnetListValid']=false;
        sbcidrbox.setValue(SWDefDesiredSubnetSize[e.value]);
    }
    
    var  spcidrbox = transform($SW.nsGet($nsId,'ddlSPCidr'));
    spcidrbox.on('select',supernetcidrupdate);
};

// Subnet Multi Edit
if (!$SW.MultiEdit) $SW.MultiEdit = {};

$SW.MultiEdit.SetEnabled = function(ctrl, enbl) {
    if (!ctrl) { return; }
    if (enbl) {
        ctrl.readOnly = '';
        ctrl.disabled = '';
        $(ctrl).parent().removeClass('x-item-disabled');
    } else {
        ctrl.readOnly = 'readonly';
        ctrl.disabled = 'disabled';
        $(ctrl).parent().addClass('x-item-disabled');
    }
};

$SW.MultiEdit.CheckBoxInit = function(chbId, ctrlId) {
    var chb = $('#' + chbId)[0];
    var ctrl = $('#' + ctrlId)[0];
    if (!chb || !ctrl) { return; }

    $SW.MultiEdit.SetEnabled(ctrl, false);
    $(chb).bind('click', function() { $SW.MultiEdit.SetEnabled(ctrl, chb.checked); });
};

$SW.MultiEdit.CheckBoxParentInit = function(chb, ctrl) {
    if (!chb || !ctrl) { return; }

    if (!$SW.MultiEdit.Validators) $SW.MultiEdit.Validators = [];
    $SW.MultiEdit.Validators.push(chb);

    var SetEnabled = function(crtl, enbl) {
        if (enbl) ctrl.enable();
        else ctrl.disable();
    };

    SetEnabled(ctrl, false);
    $(chb).bind('click', function() { SetEnabled(ctrl, chb.checked); });
};

$SW.MultiEdit.CheckBoxScanningInit = function(chb, ctrl) {
    if (!chb || !ctrl) { return; }

    if (!$SW.MultiEdit.Validators) $SW.MultiEdit.Validators = [];
    $SW.MultiEdit.Validators.push(chb);

    var SetEnabled = function(crtl, enbl) {
        if (enbl) ctrl.show(200);
        else ctrl.hide(200);
    };

    SetEnabled(ctrl, false);
    $(chb).bind('click', function() { SetEnabled(ctrl, chb.checked); });
};

$SW.MultiEdit.AllGroupsQuery = function() {
    return {
        entity: "IPAM.GroupNode",
        identity: "GroupId",
        join: ' LEFT JOIN IPAM.DhcpScope scope ON {0}.GroupId = scope.SubnetId LEFT JOIN IPAM.GroupNode S ON scope.GroupId = S.GroupId',
        orderby: '{0}.GroupType ASC, {0}.AddressN ASC, {0}.FriendlyName ASC, {0}.CIDR ASC',
        whereBuilder: function(node) {
            return node.isRoot ? "{0}.GroupId = 0" : "{0}.Distance = 1 AND {0}.AncestorId = " + node.attributes.id;
        }
    };
};
$SW.MultiEdit.GroupsQuery = function (clusterId) {
    return {
        entity: "IPAM.GroupNode",
        identity: "GroupId",
        orderby: '{0}.GroupType ASC, {0}.FriendlyName ASC',
        exclusion: '',
        whereBuilder: function(node) {
            if (node.isRoot) {
                return "{0}.GroupId = " + clusterId;
            } else {
                var e = this.exclusion, q = "{0}.GroupType = '2' AND {0}.Distance = 1 AND {0}.AncestorId = " + node.attributes.id;
                if (e && e != "") { q += " AND {0}.GroupId NOT IN " + e; }
                return q;
            }
        }
    };
};
$SW.MultiEdit.SubnetsQuery = function (range, clusterId) {
    var supernet = function() {
        if (!range || range.from == 'Null' || range.into == 'Null') return "";
        return " OR ({0}.GroupType='4' AND {0}.AddressN <='" + range.from + "' AND {0}.AddressEnd >='" + range.into + "')";
    };
    return {
        entity: "IPAM.GroupNode",
        identity: "GroupId",
        orderby: '{0}.GroupType ASC, {0}.FriendlyName ASC',
        whereBuilder: function(node) {
            if (node.isRoot) {
                return "{0}.GroupId = " + clusterId;
            } else {
                return "({0}.GroupType='2'" + supernet() + ") AND {0}.Distance = 1 AND {0}.AncestorId = " + node.attributes.id;
            }
        }
    };
};

$SW.MultiEdit.ChangeParentPermission = function () {
    var result = true;
    Ext.each(Ext.combine([], arguments), function (r, i) {
        result &= (r == 'SiteAdmin') || (r == 'PowerUser');
    });
    return !!result;
};

$SW.MultiEdit.GetParentTreeLoader = function() {
    return new Ext.ux.SWTreeLoader({
        dataUrl: "/Orion/IPAM/Services/Information.asmx/Query",
        fnQueryBuilder: function(node, loader) {
            loader.query.where = loader.query.whereBuilder(node);
            return $SW.SWQLbuild(node, loader);
        },
        fnRemap: function(node, rows) {
            jQuery.each(rows, function(idx, row) {
                row.id = '' + row.GroupId;
                row.leaf = (row.type == 8) || (row.type == 512);
                if (row.type != 1 && row.type != 2) {
                    row.iconCls = row.GroupIconPrefix + '-' + row.StatusIconPostfix;
                }
                row.uiProdiver = Ext.tree.ColumnNodeUI;
                if (row.Role == 'NoAccess')
                    row.cls += ' sw-treenode-noaccess';
                if ($SW.MultiEdit.ChangeParentPermission(row.Role) !== true)
                    row.cls += ' sw-treenode-noselect';
            });
            return rows;
        },
        fields: [
            { name: "GroupId" },
            { as: "type", code: "{0}.GroupType" },
            { as: "text", code: "{0}.FriendlyName" },
            { name: "GroupIconPrefix" },
            { name: "StatusIconPostfix" },
            { name: "Role" }
          ]
    });
};
$SW.MultiEdit.ConfirmParentChange = function (fnOk, fnCancel, id) {
    var tpl = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';
    var url = String.format(tpl, $SW.HelpServer || '/', 'OrionIPAMPHCustomRoles');
    return function(el, node){
        if ($SW.ShowParentChangeConfirmDlg !== true)
            return fnOk(el, node);

        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_50;E=js}',
            msg: "@{R=IPAM.Strings;K=IPAMWEBJS_TM0_6;E=js}" +
                        "<br/><br/>" +
                        '<a href="'+url+'" target="_blank" style="color: #336699; font-size: 11px; text-decoration: none;">' +
                        "&#0187; @{R=IPAM.Strings;K=IPAMWEBJS_TM0_7;E=js}</a><br/><br>" +
                        '<input id="dont-show-chb" type="checkbox" /> @{R=IPAM.Strings;K=IPAMWEBJS_TM0_8;E=js}',
            buttons: Ext.Msg.OKCANCEL,
            animEl: id,
            width: 500,
            icon: Ext.Msg.WARNING,
            fn: function (btn, text) {
                if (btn == 'ok') {
                    $SW.MultiEdit.SetParentChangeConfirmDlg();
                    fnOk(el,node);
                } else fnCancel(el,node);
            }
        });
     };
};
$SW.MultiEdit.SetParentChangeConfirmDlg = function () {
    var show = !$('#dont-show-chb').attr('checked');
    if (!show && show != $SW.ShowParentChangeConfirmDlg) {
        $SW.ShowParentChangeConfirmDlg = show;
        var title = "@{R=IPAM.Strings;K=IPAMWEBJS_TM0_5;E=js}";
        var fail = $SW.ext.AjaxMakeFailDelegate(title, function () { });
        var pass = $SW.ext.AjaxMakePassDelegate(title, function () { });

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            success: pass, failure: fail, timeout: fail,
            params: { entity: 'IPAM.InternalStatus', verb: 'Get',
                ShowPCDlg: $SW.ShowParentChangeConfirmDlg
            }
        });
    }
};
$SW.SubnetNoAccessOnClick = function () {
    this.className = 'noaccess';
    this.onclick = function () {
        $SW.Alert(
            '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_6;E=js}',
            '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_7;E=js}',
            { buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING }
        );
        return false;
    };
}

$SW.SubnetPollingEngineEdit = function () {
    var horizon = 2; //2 minutes

    function GetStatusImage(dbTimeDiff) {

        if ((null != dbTimeDiff) && (undefined !== dbTimeDiff)) {
            if (dbTimeDiff < horizon) {
                return "Small-Up.gif";
            } else if (dbTimeDiff > horizon * 1.5) {
                return "Small-Down.gif";
            } else {
                return "Small-Warning.gif";
            }
        }
        return "";
    }

    function renderStatus(keepAlive, meta, record) {

        if ((null != record.data.DatabaseTimeDiff) && (undefined != record.data.DatabaseTimeDiff)) {
            if (record.data.DatabaseTimeDiff < horizon) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Active;E=js}');
            } else if (record.data.DatabaseTimeDiff > horizon * 1.5) {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Down;E=js}');
            } else {
                return String.format('<span style="vertical-align: top;">{0}</span>', '@{R=Core.Strings;K=StatusDesc_Warning;E=js}');
            }
        }
        return "";
    }

    function renderEngineLink(record) {
        return String.format('<a href="/Orion/Admin/Details/Engines.aspx" target="_blank"><img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;">{1} ({2})</span></a>',
            GetStatusImage(record[0].data.DatabaseTimeDiff), record[0].data.ServerName, getServerType(record[0].data.ServerType));
    }

    function renderEngineName(record) {
        return String.format('<span><img src="/NetPerfMon/images/{0}"/>&nbsp;<span style="vertical-align: top;" data-polling-engine-id=\"{3}\">{1} ({2})</span></span>',
            GetStatusImage(record[0].data.DatabaseTimeDiff), record[0].data.ServerName, getServerType(record[0].data.ServerType), record[0].data.EngineID);
    }

    function renderText(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(value));
    }

    function renderName(value, meta, record) {
        return String.format('<img src="/NetPerfMon/images/{0}"/>&nbsp;<span title="{1}" style="vertical-align: top;">{1}</span>',
            GetStatusImage(record.data.DatabaseTimeDiff), Ext.util.Format.htmlEncode(value));
    }

    function renderServerType(value, meta, record) {
        return String.format('<span style="vertical-align: top;">{0}</span>', Ext.util.Format.htmlEncode(getServerType(value)));
    }

    function getServerType(serverType) {
        switch (serverType) {
            case "Primary":
                return "@{R=Core.Strings;K=ServerType_primary;E=js}";
            case "Additional":
                return "@{R=Core.Strings;K=ServerType_additional;E=js}";
            default:
                return serverType;
        }
    }

    selectorModel = new Ext.sw.grid.RadioSelectionModel({
        width: 40,
        hideable: false
    });

    dataStore = new ORION.WebServiceStore(
        "/Orion/Services/NodeManagement.asmx/GetEnginesTable", [{
            name: 'EngineID',
            mapping: 0
        },
            {
                name: 'ServerName',
                mapping: 1
            },
            {
                name: 'ServerType',
                mapping: 2
            },
            {
                name: 'KeepAlive',
                mapping: 3
            },
            {
                name: 'Elements',
                mapping: 4
            },
            {
                name: 'DatabaseTimeDiff',
                mapping: 5
            }
        ],
        "ServerName");
    dataStore.addListener({
        load: function() {
            this.clearFilter();
            this.filterBy(function (record, id) {
                return record.get('ServerType') !== 'RemoteCollector';
            });
        }
    })

    var selectPollingEngineRadioButton = function (pollingEngine) {
        var indexToSelect = 0;
        $.each(grid.store.data.items,
            function (index, value) {
                if (value.data.EngineID === parseInt(pollingEngine)) {
                    indexToSelect = index;
                }
            });

        grid.getSelectionModel().selectRow(indexToSelect);
    }

    var clickSelectedPollingEngineRadioButton = function () {
        var selectedEngineId = grid.getSelectionModel().getSelections()[0].data.EngineID;
        selectPollingEngineRadioButton(selectedEngineId);
    }

    var clickActivePollingEngineRadioButton = function () {
        var currPollingEngine = $("span[id*='pollEngineDescr']").children('label')
            .children('span').attr('data-polling-engine-id');
        if (currPollingEngine === undefined)
            currPollingEngine = $("input[id$=newEngineId]").val();

        selectPollingEngineRadioButton(currPollingEngine);
    };

    var pagingToolbarButtonClicked = false; // uses to manage event execution in paging toolbar.
    var pagingToolbar = new Ext.PagingToolbar({
        store: dataStore,
        pageSize: 5,
        displayInfo: true,
        split: true,
        displayMsg: '@{R=Core.Strings;K=WEBJS_AK0_4;E=js}',
        emptyMsg: "@{R=Core.Strings;K=WEBJS_AK0_3;E=js}",
        listeners: {
            beforechange: function () {
                pagingToolbarButtonClicked = true;
            },
            change: function () {
                if (pagingToolbarButtonClicked) {
                    pagingToolbarButtonClicked = false;
                    clickActivePollingEngineRadioButton();
                }
            }
        }
    });

    grid = new Ext.grid.GridPanel({
        region: 'center',
        store: dataStore,
        columns: [
            selectorModel,
            {
                header: '@{R=Core.Strings;K=WEBJS_AK0_5;E=js}',
                width: 0,
                hidden: true,
                hideable: false,
                sortable: false,
                dataIndex: 'EngineID',
                resizable: false
            },
            {
                header: '@{R=Core.Strings;K=WEBJS_AK0_9;E=js}',
                width: 250,
                hideable: false,
                sortable: true,
                dataIndex: 'ServerName',
                renderer: renderName,
                resizable: false
            },
            {
                header: '@{R=Core.Strings;K=WEBJS_AK0_6;E=js}',
                width: 60,
                sortable: true,
                dataIndex: 'KeepAlive',
                renderer: renderStatus,
                resizable: false
            },
            {
                header: '@{R=Core.Strings;K=WEBJS_AK0_7;E=js}',
                width: 90,
                sortable: true,
                dataIndex: 'ServerType',
                renderer: renderServerType,
                resizable: false
            },
            {
                header: '@{R=Core.Strings;K=WEBJS_AK0_8;E=js}',
                width: 110,
                sortable: true,
                dataIndex: 'Elements',
                renderer: renderText,
                resizable: false
            }
        ],
        listeners: {
            afterrender: function () {
                this.addListener({
                    sortchange: clickSelectedPollingEngineRadioButton
                });
            }
        },
        sm: selectorModel,
        layout: 'fit',
        autoScroll: 'true',
        loadMask: true,
        width: 600,
        height: 400,
        stripeRows: true,
        bbar: pagingToolbar
    });

    var refreshObjects = function (callback) {
        grid.store.removeAll();
        grid.store.proxy.conn.jsonData = { search: "" };
        grid.store.load({ callback: callback });
    };

    var win = new Ext.Window({
        title: '@{R=Core.Strings;K=WEBJS_AK0_10;E=js}',
        draggable: false,
        resizable: true,
        closable: true,
        closeAction: 'hide',
        width: 580,
        height: 200,
        plain: true,
        modal: true,
        layout: 'border',
        items: [grid],
        buttons: [{
            cls: "change-poll-engine-btn",
            text: '@{R=Core.Strings;K=WEBJS_AK0_11;E=js}',
            handler: function() {
                    var selEngineID = grid.getSelectionModel().getSelections()[0].data.EngineID; //getting sel engine
                    // check if we are on Manage Nodes page
                    $("input[id$=newEngineId]").val(selEngineID);
                    $("span[id*='pollEngineDescr']")[0].innerHTML = renderEngineName(grid.getSelectionModel().getSelections());
                    win.hide();
                }
            },
        {
            cls: "change-poll-engine-btn", text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}', handler: function () { win.hide(); }
            }],
        listeners: {
            beforeshow: clickActivePollingEngineRadioButton,
        }
    });
    $("a[id*='changePollEngineLink']").click(function () {
        if (!$(this).parent().hasClass("Disabled")) {
            grid.store.removeAll();
            refreshObjects(function () {
                win.show();
            });
        }
        return false;
    });
    
}
    

