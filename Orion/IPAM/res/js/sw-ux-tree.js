
// - loader

Ext.ux.SWTreeLoader = function(config){
    this.fields = null;
    this.fnRemap = null;
    Ext.ux.SWTreeLoader.superclass.constructor.call(this, config);
};

Ext.extend(Ext.ux.SWTreeLoader, Ext.tree.TreeLoader, {

    getParams: function(node){
      return (this.fnQueryBuilder || $SW.SWQLbuild)( node, this );
    },

    translateData: function(node,o){

        var map = {}, src, dest, field, rows=o.d.Rows, ret=[], i, imax, cols=o.d.Columns, ncols=cols.length, jmax, v;

        jQuery.each( this.fields, function(n,el){
         if(el.as) map[el.as] = n;
         else map[el.name] = n;
        });
        var fs = this.fields;
        jQuery.each( cols, function(n,el){ map[n] = fs[map[el]]; });

        for(i=0,imax=rows.length; i < imax; ++i )
        {
          src = rows[i];
          ret.push( dest = {} );
          jmax = (src.length > ncols) ? ncols : src.length;
          for( var j=0; j < jmax; ++j ) {
            v = src[j] + ''; v = (v == 'null') ? '' : v;
            if( field = map[j] ) dest[field.as || field.name] = v;
            else dest[cols[j]] = v;
          }
        }

        if( this.fnRemap )
         ret = this.fnRemap(node,ret);

        return ret;
    },

    requestData : function(node, callback){
        if(this.fireEvent("beforeload", this, node, callback) !== false){
            this.transId = Ext.Ajax.request({
                method:this.requestMethod,
                url: this.dataUrl||this.url,
                success: this.handleResponse,
                failure: this.handleFailure,
                scope: this,
                argument: {callback: callback, node: node},
                jsonData: this.getParams(node)
            });
        }else{
            
            if(typeof callback == "function"){
                callback();
            }
        }
    },

    processResponse : function(response, node, callback, scope){
        var json = response.responseText;
        var o = response.responseData || Ext.decode(json);
        o = this.translateData(node, o);
        node.beginUpdate();
        if (!isIE()) {
            try {
                processTree(this, o, response, node, false);
            } catch (e) {
                this.handleFailure(response);
            }
        } else {
            processTree(this, o, response, node, true);
        }

        node.endUpdate();
        this.runCallback(callback, scope || node, [node]);
    }
});

var isIE = function() {
    if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent))
    {
		return true;
	}
	if (/Edge\/\d./i.test(navigator.userAgent))
    {
		return true;
	}
	
	return false;
}

var processTree = function (that, o, response, node, ie) {
    for (var i = 0, len = o.length; i < len; i++) {
        var n = that.createNode(o[i]);
        if (n) {
            // process each element under try block is required using IE.
            if (!ie) {
                node.appendChild(n);
            } else {
                try {
                    node.appendChild(n);
                } catch (e) {
                    that.handleFailure(response);
                }
            }
        }
    }
}

// tree ui

Ext.ux.CheckboxTreeNodeUI = Ext.extend(Ext.tree.TreeNodeUI, {

    iconType: 'x-checkable-square',

    // focus: Ext.emptyFn, // prevent odd scrolling behavior

    renderElements : function(n, a, targetNode, bulkRender){
        
        this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

        var cb = typeof a.checked == 'boolean';

        var href = a.href ? a.href : Ext.isGecko ? "" : "#";
        var buf = ['<li class="x-tree-node x-checkable ', this.iconType, '"><div ext:tree-node-id="',n.id,'" class="x-tree-node-el x-tree-node-leaf x-unselectable ', a.cls,'" unselectable="on">',
            '<span class="x-tree-node-indent">',this.indentMarkup,"</span>",
            '<img src="', this.emptyIcon, '" class="x-tree-ec-icon x-tree-elbow" />',
            '<img src="', this.emptyIcon, '" class="x-tree-node-icon x-tree-check x-tree-node-inline-icon" />',
            '<img src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon',(a.icon ? " x-tree-node-inline-icon" : ""),(a.iconCls ? " "+a.iconCls : ""),'" unselectable="on" />',
            cb ? ('<input class="x-tree-node-cb" type="checkbox" ' + (a.checked ? 'checked="checked" />' : '/>')) : '',
            '<a hidefocus="on" class="x-tree-node-anchor" href="',href,'" tabIndex="1" ',
             a.hrefTarget ? ' target="'+a.hrefTarget+'"' : "", '><span unselectable="on">',n.text,"</span></a></div>",
            '<ul class="x-tree-node-ct" style="display:none;"></ul>',
            "</li>"].join('');

        var nel;
        if(bulkRender !== true && n.nextSibling && (nel = n.nextSibling.ui.getEl())){
            this.wrap = Ext.DomHelper.insertHtml("beforeBegin", nel, buf);
        }else{
            this.wrap = Ext.DomHelper.insertHtml("beforeEnd", targetNode, buf);
        }
        
        this.elNode = this.wrap.childNodes[0];
        this.ctNode = this.wrap.childNodes[1];
        var cs = this.elNode.childNodes;
        this.indentNode = cs[0];
        this.ecNode = cs[1];
        this.iconNode = cs[2];
        var index = 3;
        if(cb){
            this.checkbox = cs[3];
			
			this.checkbox.defaultChecked = this.checkbox.checked;			
            index++;
        }
        this.anchor = cs[index];
        this.textNode = cs[index].firstChild;
    }
});

Ext.ux.RadioTreeNodeUI = Ext.extend(Ext.ux.CheckboxTreeNodeUI, { iconType: 'x-checkable-circle' });


(function(){

 var cb_getpath = function(node,attrn){
  if( node == null ) return '';
  return ((node.isRoot || !node.parentNode) ? '' : cb_getpath(node.parentNode, attrn) +
    encodeURIComponent(node.attributes[attrn]) + ':');
 };

 var sel_parse = function(value){
  var e = {}, s = {};
  if( value )
   Ext.each( value.replace( /[&]+/g, '&' ).replace(/^[&]|[&]$/g, '' ).split('&'), function(t){ 
    var ses = t.replace( /:+/g, ':' ).replace(/^[:]|[:]$/g, '' ).split(':'), ns = ses.length-1;
    jQuery.each( ses, function(n,txt){
      (n==ns?s:e) [decodeURIComponent(txt)] = 1;
    });
  });
  return { selects: s, expands: e };
 };

 var init_selects = function(){
  var r = sel_parse($('#'+this.stateFormControl)[0].value);
  this._expands = r.expands;
  this._selects = r.selects;
 };

 var sel_init = function(tree){
  this.tree = tree;

  tree.getTreeEl().on("keydown", this.onKeyDown, this);
  tree.on("click", this.onNodeClick, this);
  tree.getLoader().on("load", this.onNodeLoad, this);
  this.on("selectionchange", this.onSelectionChange, this);
 };
 
 var sel_get = function(){
   return $('#'+this.stateFormControl)[0].value;
 };

 var _last_expand = null;
 var sel_onload = function(loader,node){

  if( node.isRoot )
  {
   this.noserialize = true;
   this.clearSelections();
   this.noserialize = false;
   init_selects.apply(this, []);
   _last_expand = null;
  }

  var e = this._expands, s = this._selects;
  var expand, resets = [];

  jQuery.each( node.childNodes, function(n,child){
    var id = child.id;
    if( typeof(id) != 'string' ) id = ''+id;
    
    if( e[id] ){
     child.expand();
     delete e[id];
     _last_expand = expand = id;
    }

    if( s[id] ){
     if( !child.isSelected() )
       resets.push(id);
     delete s[id];
    }
  });

  // [oh] can't find item for select (max show items count reached)
  if (!expand && (resets.length == 0))
    resets.push(_last_expand);

  if( resets.length )
  {
   var treeid = node.getOwnerTree().id;
   window.setTimeout( function(){
    var c, n, sel;
    if( (c = Ext.getCmp(treeid)) && (sel = c.getSelectionModel()) ){
     sel.noserialize = true;
     Ext.each( resets, function(nid){
        if(n = c.getNodeById(nid))
          sel.select( n, null, true );
     });
     sel.noserialize = false;
     }
    }, 0);
   }
 };

Ext.ux.SingleCheckboxSelectModel = Ext.extend(Ext.tree.DefaultSelectionModel, {

 init: sel_init,
 parseraw: sel_parse,
 getraw: sel_get,

 onNodeClick: function(node, e){
     if (node.attributes.noselect) {
         node.toggle();
     } else {
         $SW.selectedParent = this.select(node, e, e.ctrlKey);
     }
 },

 onSelectionChange: function(model,newnode,oldnode){
  var d;
  if( !this.noserialize && this.stateFormControl && (d = Ext.getDom(this.stateFormControl)) )
   d.value = cb_getpath(newnode,'id').replace( /:$/, '' );
 },

 onNodeLoad: sel_onload
});

Ext.ux.MultiCheckboxSelectModel = Ext.extend(Ext.tree.MultiSelectionModel, {

 init: sel_init,
 parseraw: sel_parse,
 getraw: sel_get,

 onNodeClick: function (node, e) {
     var isDhcpTreeNode = (node.ownerTree.id && node.ownerTree.id == 'tabScopes') ? true : false;
     
         if (e.shiftKey) e.preventDefault();
         if (node.attributes.noselect) node.toggle();
         else if (!node.isSelected()) {
             $SW.selectedParent = this.select(node, e, (isDhcpTreeNode) ? true : e.ctrlKey);
         }
         else {
             this.unselect(node);
             // Improved selection code. A normal single click on a group of selected
             // nodes (non-ctrl click) will deselect the other nodes as expected.
             if (!isDhcpTreeNode)
                 if (!e.ctrlKey) {
                     $SW.selectedParent = this.select(node, e, false);
                 }
         }
         if (typeof (e.target.href) != "undefined") {
             window.location = e.target.href;
     }
 },
 
 onSelectionChange: function(model,nodes){
  var d, ret = [];
  if( !this.noserialize && this.stateFormControl && (d = Ext.getDom(this.stateFormControl)) ){
   Ext.each(nodes,function(el){ ret.push( cb_getpath(el,'id').replace( /:$/, '' ) ); });
   d.value = ret.join('&');
   }
  },

  select : function(node, e, keepExisting) {
     var selectFn = Ext.ux.MultiCheckboxSelectModel.superclass.select;
     if (e && e.shiftKey && this.selNodes.length > 0) {
         this.lastSelNode = this.lastSelNode || this.selNodes[0];
         var before = (this._compareNodeOrder(this.lastSelNode, node) > 0);
         var sel = this._selecttraverse(
             before ? node : this.lastSelNode,
             before ? this.lastSelNode : node
         );
         Ext.each(sel || [],
             function(n) {
                 if ((n != node) && !this.isSelected(n)) {
                     this.selNodes.push(n);
                     this.selMap[n.id] = n;
                     n.ui.onSelectedChange(true);
                 }
             },
             this);
         return selectFn.call(this, node, e, true);
     }
     if (this.isSelected(node)) {
         this.lastSelNode = node;
     } else if (this.fireEvent('beforeselect', this, node) !== false) {
         return selectFn.call(this, node, e, keepExisting);
     }
     return node;
 },

  onNodeLoad: sel_onload,

  _selecttraverse: function(f, l){
    if(!f || !l) return [];
    var ns = [], p = f, nxt = null;
    //[oh] select in actual subtree
    for (var n=f;n!=null;n=n.nextSibling) {
        ns.push(n); if(n==l) return ns;
        if(!n.isExpanded()) continue;
        if(n.contains(l)) {
            var fnd; n.cascade(function(c) {
                if(fnd || c == l){ fnd = true; return false; }
                ns.push(c);
            }, this);
            return ns;
        } else n.cascade(function(c) { ns.push(c); }, this);
    }
    //[oh] bubble up parents
    while((p = p.parentNode) != null && (nxt = p.nextSibling) == null);
    if(nxt == null) return ns;
    //[oh] select in next subtree
    return $.merge(ns, this._selecttraverse(nxt, l));
  },

  _compareNodeOrder: document.compareDocumentPosition ?
	function(n1, n2) {
		return 3 - (n1.ui.elNode.compareDocumentPosition(n2.ui.elNode) & 6);
	} : 
	(typeof document.documentElement.sourceIndex !== "undefined" ? 
		function(n1, n2) { // IE source index method
			return n1.ui.elNode.sourceIndex - n2.ui.elNode.sourceIndex;	
		} :
		function(n1, n2) { // Safari workaround
			if (n1 == n2) return 0;
			var r1 = document.createRange(); r1.selectNode(n1.ui.elNode); r1.collapse(true);
			var r2 = document.createRange(); r2.selectNode(n2.ui.elNode); r2.collapse(true);
			return r1.compareBoundaryPoints(Range.START_TO_END, r2);
		}		
	)
});

Ext.ux.MultiSelectTreeDragZone = Ext.extend(Ext.tree.TreeDragZone, {
	isMultiSelect: true,

    onBeforeDrag : function(data, e){
        var fn = Ext.ux.MultiSelectTreeDragZone.superclass.onBeforeDrag;
        var scope = this; ns = data.nodes || [];
		if (ns && ns.length > 0) {
            var drg = true;
            Ext.each(ns, function(n) {
                if(!fn.call(scope, {node: n})) drg = false;
            });
            return drg;
        }

        return fn.call(scope, data, e);
	},
    getDragData : function(e) {
    	var sm = this.tree.getSelectionModel();
		
        // get event target
		var target = Ext.dd.Registry.getHandleFromEvent(e);
		// if no target (die)
		if (target == null) return;
		if (target.node.isSelected() && e.ctrlKey) {
		    sm.unselect(target.node);
			return;
		}

		// use tree selection model..
        var s = [];
		if (sm.getSelectedNodes) {
			if (!target.node.isSelected() || e.shiftKey)
				sm.select(target.node, e, e.ctrlKey);
			s = sm.getSelectedNodes();
		} else s = [target.node];
        
        // if no nodes selected stop now...
		if (!s || s.length < 1) return;

		// create a container for the proxy...
		var div = $('<ul class="x-tree-node-leaf" />');
        Ext.each(s, function(n, i) {
			div.append( $('<li />')
                .append(n.ui.getIconEl().cloneNode(true))
                .append(n.ui.getTextEl().cloneNode(true))
			);
		});
		return { nodes: s, ddel: div[0] };
	},
    selectNodes : function(nodes, e){
        var sm = this.tree.getSelectionModel();
        sm.clearSelections(true);
        Ext.each(nodes, function(n){
            sm.select(n, e, true);
        });
        return sm.getSelectedNodes();
    },
	onInitDrag : function(e){
		var data = this.dragData;
        var sel = this.selectNodes(data.nodes || [data.node], e);
		this.tree.eventModel.disable();
		this.proxy.update("");
		this.proxy.ghost.dom.appendChild(data.ddel);
		this.tree.fireEvent("startdrag", this.tree, sel, e);
	},
	getTreeNode: function() {
		return this.dragData.nodes;
	},
	getRepairXY : function(e, data){
		var proxyPos = this.getProxy().getEl().getXY();
		if (proxyPos[0] < -1000) return proxyPos;
		return data.nodes[0].ui.getDDRepairXY();
	},
	onEndDrag : function(data, e){
        var nodes = data.nodes || [data.node];
		this.tree.eventModel.enable.defer(100, this.tree.eventModel);
		this.tree.fireEvent("enddrag", this.tree, nodes, e);
	},
	onValidDrop : function(dd, e, id){
        var nodes = this.dragData.nodes || [this.dragData.node];
		this.tree.fireEvent("dragdrop", this.tree, nodes, dd, e);
		this.hideProxy();
	},
	beforeInvalidDrop : function(e, id){
        var nodes = this.dragData.nodes || [this.dragData.node];
        this.selectNodes(nodes, e);
    }
});

Ext.ux.MultiSelectTreeDropZone = Ext.extend(Ext.tree.TreeDropZone, {
    isValidDropPoint : function(n, pt, dd, e, data){
        // don't allow dropping a treenode inside itself...
        var contains, dropNodes = data.nodes || [data.node];
        Ext.each(dropNodes, function(dn){
            if(dn == n.node || dn.contains(n.node)){
                contains = true; return false;
            }
        });
        if(contains === true) return false;

        return Ext.ux.MultiSelectTreeDropZone.superclass.isValidDropPoint.call(this, n, pt, dd, e, data);
    },
	afterRepair : function(data){
		if(data && Ext.enableFx){
            Ext.each((data.nodes || [data.node]), function(n){
                n.ui.highlight();
            });
		}
		this.hideProxy();
	}
});

})();
