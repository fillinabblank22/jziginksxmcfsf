(function () {

    function decodeUrlParameter(value) {
        return decodeURIComponent((value + "").replace(/\+/g, "%20"));;
    }

    var filterValue = function () {
        var op = (this.op == 'eq') ? '=' : this.op;
        return this.field + op + this.value;
    };
    var dhcpScopeFilter = { field: '{0}.GroupType', op: 'eq', value: '64', val: filterValue };
    var dhcpServerFilter = { field: '{0}.GroupType', op: 'eq', value: '32', val: filterValue };
    var dhcpFilter = { field: '{0}.ViewType', op: 'eq', value: '1', val: filterValue };   
    var dhcpNoGrouping = function(id) {
        if (id == 'scopes') {
            return dhcpFilter;
        }
        
         else {
            return dhcpServerFilter;
        }
    };

    var dnsZoneFilter = { field: '{0}.GroupType', op: 'eq', value: '2048', val: filterValue };
    var dnsServerFilter = { field: '{0}.GroupType', op: 'eq', value: '1024', val: filterValue };
    var dnsNoGrouping = function (id) { return (id == 'zones') ? dnsZoneFilter : dnsServerFilter; };
    $SW.IPAM.isExpand = {};
    $SW.IPAM.DHCPLastSelected = null;
    $SW.IPAM.DNSLastSelected = null;
    //$SW.IPAM.DNSServerLastSelected = null;
    $SW.IPAM.tabSwitch = false;
    if (!$SW.IPAM) $SW.IPAM = {};
    Array.prototype.findIndex = function (value) {
        var ctr = -1;
        for (var i = 0; i < this.length; i++) {
            // use === to check for Matches. ie., identical (===), ;
            if (typeof (this[i]) != "undefined") {
                if (this[i] != null) {
                    if (typeof (this[i].id) != "undefined") {
                        if (this[i].id != null) {
                            if (this[i].id == value) {
                                return i;
                            }
                        }
                    }
                }
            }
        }
        return ctr;
    };
   
    if (!$SW.IPAM.GroupBy) $SW.IPAM.GroupBy = {
        TabLayout: 'ipam_tabs',
        DhcpDataSet: {
            map: {}, all: [],
            none: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_232;E=js}', constraint: dhcpNoGrouping}],
            defaultGrouping: 'none.',
            scopes: [
               {
                   name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_03;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpFilter, groupby: 'ServerType',
                   templatehtml:
                   '<li id="{domid}" class="sw-groupby-item {cls}" {[ (values.text != null) ? "sw:rel=\\"" + fm.htmlEncode(values.id) + "\\"" : "" ]}><div><a href="#">' +
                       '{[(values.text=="1")?"Windows":((values.text=="2")?"Cisco":((values.text=="3")?"ASA":"ISC"))]} ({n})' +
                   '</a></div></li>'
               },               
               { name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_07;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpFilter, groupby: 'VLAN' },
               { name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_06;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpFilter, groupby: 'Location' },
               { name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_04;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpFilter, groupby: 'StatusShortDescription' }
                ,
               { name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_02;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpScopeFilter, groupby: 'FriendlyName', isScope: 'True'},
               { name: '@{R=IPAM.Strings;K=IPAMWEBJS_MR_05;E=js}', entity: 'IPAM.DHCPView', constraint: dhcpScopeFilter, groupby: 'StatusName', isScope: 'True' }
            ],
            scopes_cp: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_246;E=js}' }],
            scopes_tpl: function (r) {
                return { name: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_240;E=js}', r.data.Field), entity: 'IPAM.DHCPView', constraint: dhcpScopeFilter, groupby: 'CustomProperties.' + r.data.Field, orderby: '{0}.CustomProperties.' + r.data.Field + " ASC", nullfix: 0, isCustomScope: 'True' };
            },
            servers: [
            ],
            servers_cp: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_246;E=js}'}],
            servers_tpl: function (r) {
                return { name: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_247;E=js}', r.data.Field), entity: 'IPAM.DHCPView', constraint: dhcpFilter, groupby: 'CustomProperties.' + r.data.Field, orderby: '{0}.CustomProperties.' + r.data.Field + " ASC", nullfix: 0 };
            }
        },
        DnsDataSet: {
            map: {}, all: [],
            none: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_232;E=js}', constraint: dnsNoGrouping}],
            defaultGrouping: 'zones.Parent.FriendlyName',
            zones: [
                { name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_248;E=js}', entity: 'IPAM.GroupNode', constraint: dnsZoneFilter, groupby: 'Parent.FriendlyName', nullfix: 1 },
                { name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_249;E=js}', entity: 'IPAM.GroupNode', constraint: dnsZoneFilter, groupby: 'StatusShortDescription' }
            ],
            zones_cp: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_250;E=js}'}],
            zones_tpl: function (r) {
                return { name: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_251;E=js}', r.data.Field), entity: 'IPAM.GroupNode', constraint: dnsZoneFilter, groupby: 'CustomProperties.' + r.data.Field, orderby: '{0}.CustomProperties.' + r.data.Field + " ASC", nullfix: 1 };
            },
            servers: [
                { name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_241;E=js}', entity: 'IPAM.GroupNode', constraint: dnsServerFilter, groupby: 'StatusShortDescription' },
                { name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_242;E=js}', entity: 'IPAM.GroupNode', constraint: dnsServerFilter, groupby: 'DnsServer.ServerType',
                   templatehtml:
                   '<li id="{domid}" class="sw-groupby-item {cls}" {[ (values.text != null) ? "sw:rel=\\"" + fm.htmlEncode(values.id) + "\\"" : "" ]}><div><a href="#">' +
                       '{[(values.text=="1")?"Windows":((values.text=="2")?"BIND":(values.text=="3")?"Infoblox":"@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}")]} ({n})' +
                   '</a></div></li>'
               }

            ],
            servers_cp: [{ name: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_246;E=js}'}],
            servers_tpl: function (r) {
                return { name: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_247;E=js}', r.data.Field), entity: 'IPAM.GroupNode', constraint: dnsServerFilter, groupby: 'CustomProperties.' + r.data.Field, orderby: '{0}.CustomProperties.' + r.data.Field + " ASC", nullfix: 1 };
            }
        },

        MakeOptions: function ($nsId, type, tabs) {
            var dset = type + 'DataSet', id = type + 'GroupBySelect';
            return {
                id: id,
                nsId: $nsId,
                type: type,
                tabs: tabs,
                dataset: $SW.IPAM.GroupBy[dset],
                combo: $SW.nsGet($nsId, id)
            };
        },
        Init: function (options) {
            if (!options.dataset || !options.combo) return;

            options.list = Ext.getCmp('GroupByList');
            if (!options.list) return;

            options.cbox = $SW.IPAM.GroupBy.Load(options);
            Ext.iterate(options.tabs, function (key, val) {
                Ext.getCmp(val).dirty = true;
            });

            $SW.IPAM.GroupBy.DataSetSync(options);
            return options;
        },

        DataSetSync: function (options) {
            var ds = options.dataset || {};
            var downloading = (ds.cpdownload !== true);
            ds.all = []; ds.map = {};

            if (downloading) {
                ds.cpdownload = true;
                $SW.IPAM.GroupBy.LoadCustomProperties(options);
            }

            var process = function (tab, sequence, datastore) {
                Ext.each(ds[sequence], function (item) {
                    item.tab = tab || '';
                    item.sequence = sequence || '';
                    item.key = sequence + '.' + (item.groupby || '');
                    item.orderby = item.orderby || ("{0}." + item.groupby + " ASC ");
                    datastore.all.push(item);
                    datastore.map[item.key] = item;
                });
            };

            process('none', 'none', ds);
            ds.map[''] = ds['none'][0];

            Ext.iterate(options.tabs, function (key, val) {
                process(key, key, ds);
                process(key, key + '_cp', ds);
            });

            // fill all combobox values
            (function () {
                var cbox = options.cbox, val = cbox.getValue();
                //Set Scopes Name in groupby filter if opento parameter came 
                var queryParams = unescape(window.location.search).split(/[?|&]+/);
                var findIndex = -1;
                var tabName = "";
                Ext.iterate(queryParams, function (params, i) {
                   
                    if (params.indexOf("zones") > -1) {
                        tabName = params;
                    }
                    if (params.indexOf("opento") > -1) {
                        findIndex = i;
                    }
                   
                });
                if (findIndex != -1) {
                        val = 'scopes.FriendlyName';
                }
                if (tabName == "tab=zones")
                {
                    val = 'none.';                  
                }

                var s = cbox.store; s.suspendEvents(); s.removeAll();

                Ext.each(ds.all, function (item) {
                    s.add([new s.recordType({ text: item.name, value: item.key })]);
                });

                s.resumeEvents(); s.fireEvent('datachanged');
                var idx = s.find('value', val);
                if (idx >= 0) cbox.setValue(val);
            })();

            if (downloading) {
                var cbox = options.cbox, val = cbox.getValue();
                var s = cbox.store, idx = s.find('value', val);
                if (!cbox.hidden && idx >= 0) {
                    cbox.fireEvent('select', cbox, s.getAt(idx), idx);
                }
            }
        },

        LoadCustomProperties: function (options) {
            var tabs = options.tabs;
            var ds = options.dataset || {};
            var callback = function () {
                $SW.IPAM.GroupBy.DataSetSync(options);
            };
            function pass(json, opt) {
                var cols = [];
                jQuery.each(json.d.Columns, function (n, el) { cols.push({ name: el, mapping: n }); });
                var reader = new Ext.data.ArrayReader({}, Ext.data.Record.create(cols));
                var records = reader.readRecords(json.d.Rows).records;

                Ext.iterate(tabs, function (key, val) {
                    var data = ds[key + '_cp'];
                    var tpl = ds[key + '_tpl'] || function () { };
                    data.length = 0;
                    Ext.each(records, function (r) {
                        var cfg = tpl(r); 
                        data.push(cfg);
                    });
                });
                callback();
            };
            function fail() {
                Ext.iterate(tabs, function (key, val) { ds[key + '_cp'] = []; });
                callback();
            };

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/information.asmx/Query',
                jsonData: { query: "SELECT c.Field from Orion.CustomProperty c WHERE c.TargetEntity like 'IPAM.GroupsCustomProperties'" },
                success: $SW.ext.AjaxMakePassDelegate('Downloading Custom Properties', pass),
                failure: $SW.ext.AjaxMakeFailDelegate('Downloading Custom Properties', fail)
            });
        },

        Load: function (options) {
            var el = $('#' + options.combo);
            var cbox = new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                transform: options.combo,
                id: options.id,
                autoWidth: true,
                width: 'auto',
                forceSelection: true,
                disabled: el.is(':disabled'),
                hidden: el.is(':hidden'),
                forceReSelect: function (select) {
                    var ds = options.dataset, s = this.store;
                    var key = this.getValue(), value = ds.map[key];
                    if (!value) {                      
                        select = true; this.setValue(key = ds.defaultGrouping);
                    }
                    var idx = s.find('value', key);
                    if (select) this.fireEvent('select', this, s.getAt(idx), idx);
                    return value;
                }
            });
            cbox.on("expand", $SW.IPAM.GroupBy.onExpand);
            cbox.on("beforeselect", $SW.IPAM.GroupBy.onBeforeSelect(options));
            cbox.on("select", $SW.IPAM.GroupBy.onSelect(options));
            return cbox;
        },
        onExpand: function (me) {
            var tr = me.trigger.getBox(),
                bo = me.list.getBorderWidth("lr"),
                b1 = Ext.fly(me.container.dom).getBox(),
                w1 = tr.x + tr.width - b1.x;

            me.list.setStyle('width', '' + (w1 - bo) + 'px');
            me.innerList.setStyle('width', 'auto');
        },
        onBeforeSelect: function (options) {
            var ds = options.dataset || { map: [] };
            return function (me, record) {
                var val = ds.map[record.data.value];
                if (!val.entity && !/none/.test(val.key))
                    return false;
            };
        },
        onSelect: function (options) {
            var ds = options.dataset || { map: [] };
            return function (me, record) {
                var val = ds.map[record.data.value];
                if (!val.entity) {
                    options.list.clear();
                    $SW.IPAM.GroupBy.RefreshTab(options);
                    return;
                }
                var filter = (val.constraint.val() || '1=1');
                if (val.isScope || val.isCustomScope) {
                    filter = filter + " and {0}.ViewType= 3";
                }
                if (options.type == "Dhcp") {
                    options.list.load({
                        query: {
                            entity: val.entity,
                            groupby: '{0}.' + val.groupby,
                            where: filter,
                            orderby: val.orderby || null
                        },
                        fields: [
                           { name: val.groupby, as: 'text' },
                           { code: 'COUNT(ISNULL({0}.' + val.groupby + ",''))", as: 'n' }
                        ],
                        templateEx: val.templatehtml || null
                    });

                } else {
                    options.list.load({
                        query: {
                            entity: val.entity,
                            groupby: '{0}.' + val.groupby,
                            where: '{0}.Distance=0 AND ' + filter,
                            orderby: val.orderby || null
                        },
                        fields: [
                           { name: val.groupby, as: 'text' },
                           { code: 'COUNT(ISNULL({0}.' + val.groupby + ",''))", as: 'n' }
                        ],
                        templateEx: val.templatehtml || null
                    });
                }
                
            };
        },

        GetGroupByListListeners: function (optFn) {
           
            $SW.IPAM.GroupBy.Last = null;
            var selectchangeFn = function (panel, nval, pre) {
                if (nval == null) $SW.IPAM.GroupBy.Last = typeof (pre) == "string" ? pre : null;

                if (panel.selectedValue == null && panel.records && panel.records.length) {
                    if ($SW.IPAM.GroupBy.Last == null || !panel.select($SW.IPAM.GroupBy.Last))
                        panel.select(panel.records[0].id);
                } else {
                    $SW.IPAM.GroupBy.Last = panel.selectedValue;
                    var tab = $SW.IPAM.GroupBy.RefreshTab(optFn());
                    tab.dirty = false;
                    if (tab.id == "tabScopes") {
                        tab.getRootNode().reload();
                    } else {
                        tab.bottomToolbar.doLoad();
                    }
                }

                if (nval != null)
                    $SW.IPAM.GroupBy.Last = nval;
            };
            var afterloadFn = function (panel, nval) {          
                if ($SW.IPAM.tabSwitch)
                    $SW.IPAM.tabSwitch = false;
                var options = optFn();
                if (panel.selectedValue == null && panel.records && panel.records.length) {
                    var queryParams = window.location.search.split(/[?|&]+/);
                    var findIndex = -1;
                    var opentoVal = "";
                    Ext.iterate(queryParams, function (params, i) {
                        if (params.indexOf("opento") > -1) {
                            findIndex = i;
                            opentoVal = params;
                        }
                    });                 
                    if (findIndex != -1 && options.type != "Dns" && $SW.IPAM.DHCPLastSelected == null) {                     
                        opentoVal = opentoVal.split('=');
                        if (opentoVal.length > 0) {
                            opentoVal = decodeUrlParameter(opentoVal[1]);
                            panel.select($SW.IPAM.GroupBy.Last = opentoVal);                        
                        }
                    } else {                     
                        if (panel.records.length > 0) {  
                            if (options.type == "Dhcp" && $SW.IPAM.DHCPLastSelected == null) {                             
                                panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);                           
                                $SW.IPAM.DHCPLastSelected = panel.records[0].id;
                            }
                            else if(options.type == "Dhcp")
                            {                            
                                if (panel.records.findIndex($SW.IPAM.DHCPLastSelected) > -1) {
                                    panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.DHCPLastSelected);
                                } else {
                                    panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);                                   
                                }                               
                            }
                            else if (options.type != "Dhcp" && $SW.IPAM.DNSLastSelected == null) {                             
                                panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);
                                $SW.IPAM.DNSLastSelected = $SW.IPAM.GroupBy.Last;
                                $SW.IPAM.GroupBy.RefreshTab(optFn());
                            }
                            else if (options.type != "Dhcp") {                              
                                if (panel.records.findIndex($SW.IPAM.DNSLastSelected) > -1) {
                                    panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.DNSLastSelected);
                                } else {
                                    panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);
                                }
                                $SW.IPAM.GroupBy.RefreshTab(optFn());
                            }
                        }
                    }
                } else {                   
                    if (options.type == "Dhcp" && $SW.IPAM.DHCPLastSelected == null) {                      
                        panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);                     
                        $SW.IPAM.DHCPLastSelected = panel.records[0].id;
                    }
                    else if (options.type == "Dhcp") {                     
                        if (panel.records.findIndex($SW.IPAM.DHCPLastSelected) > -1) {
                            panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.DHCPLastSelected);
                        }
                        else {
                            panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);
                        }
                    }
                    else if (options.type != "Dhcp" && $SW.IPAM.DNSLastSelected == null) {                    
                        if ($SW.IPAM.SelectedDNSServer == null) {
                            panel.select($SW.IPAM.GroupBy.Last = panel.records[0].id);
                        } else {
                            panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.SelectedDNSServer);
                        }
                            
                        
                        $SW.IPAM.DNSLastSelected = $SW.IPAM.GroupBy.Last;
                    }
                    else if (options.type != "Dhcp") {                     
                        if ($SW.IPAM.SelectedDNSServer == null) {
                            panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.DNSLastSelected);
                        } else {
                            panel.select($SW.IPAM.GroupBy.Last = $SW.IPAM.SelectedDNSServer);
                        }
                    }                    
                  
                    $SW.IPAM.GroupBy.RefreshTab(optFn());
                }
            };
            return { selectchange: selectchangeFn, afterload: afterloadFn };
        },

        GetTabStoreListeners: function (options, tabid, cboxFn) {
            var opt = options, tid = tabid;
            var ds = options.dataset || { map: [] };
            var cboxGroupByVisibility = cboxFn || function () { }; ;

            var nogrouping_constraint,Scope_constraint;
            Ext.iterate(opt.tabs, function (seq, tab) {
                if (tab == tid) {
                    nogrouping_constraint = ds['none'][0].constraint(seq);
                    Scope_constraint = ds['none'][0].constraint('tabscopes');
                    return false;
                }
            });

            var ReselectGrid = function (val) {
                // [oh] set visibility of combo boxes
                cboxGroupByVisibility();

                if (val.tab == 'none' || opt.tabs[val.tab] == tid) return false;

                // [oh] find 'oposite' equivalent of combo option by groupby field
                var key, cbox = opt.cbox, store = cbox.store;
                Ext.each(ds.all, function (obj) {
                    if ((val.groupby == obj.groupby) && (val.sequence != obj.sequence)) {
                        key = obj.key; return false;
                    }
                });

                // [oh] set found value, or [No Grouping] elsewhere
                var idx = store.find('value', key);
                if (idx < 0) key = 'none.';
                idx = store.find('value', key);
                cbox.setValue(key);
                cbox.fireEvent('select', cbox, store.getAt(idx), idx);
                return true;
            };
           
            var beforeloadFn = function () {
                var tab = Ext.getCmp(tid), rm = [];
                var store;
                if (tab.id == "tabScopes") {
                    store = tab.getLoader();
                } else {
                    store = tab.store;
                }
                if (typeof(opt.cbox) == "undefined") {
                    return false;
                }
                var cbox = opt.cbox, val = ds.map[cbox.getValue()];
                var groupby = opt.list.selectedValue;
                if (groupby == "") {
                    if (opt.list.records !=null) {
                        groupby = opt.list.records[0].id;
                    }
                }             
                if (ReselectGrid(val)) return false;
                $SW.IPAM.GroupBy.RefreshTab(options);
                if (groupby == null) {                  
                    if (tab.id == "tabScopes" && $SW.IPAM.DHCPLastSelected != null) {
                        groupby = $SW.IPAM.DHCPLastSelected;
                    } else if (tab.id != "tabScopes" && $SW.IPAM.DNSLastSelected != null) {
                        groupby = $SW.IPAM.DNSLastSelected;
                    }
                }
                else if ($SW.IPAM.tabSwitch) {                  
                    $SW.IPAM.tabSwitch = false;
                    if (tab.id == "tabScopes" && $SW.IPAM.DHCPLastSelected != null) {
                        groupby = $SW.IPAM.DHCPLastSelected;                        
                     
                    } else if (tab.id != "tabScopes" && $SW.IPAM.DNSLastSelected != null) {
                        groupby = $SW.IPAM.DNSLastSelected;                        
                    }
                }
                else {
                   
                    if (tab.id == "tabScopes") {                       
                        $SW.IPAM.DHCPLastSelected = groupby;
                    } else {                   
                        $SW.IPAM.DNSLastSelected =groupby;
                    }
                }
                if (groupby == $SW.IPAM.DNSLastSelected && tab.id == "tabScopes" && $SW.IPAM.DHCPLastSelected != null) {
                    groupby = $SW.IPAM.DHCPLastSelected;
                }
            else if (groupby == $SW.IPAM.DHCPLastSelected && tab.id != "tabScopes" && $SW.IPAM.DNSLastSelected != null) {
                    groupby = $SW.IPAM.DNSLastSelected;
                }
                if ($SW.IPAM.tabSwitch) {
                    $SW.IPAM.tabSwitch = false;
                }
                if (ReselectGrid(val)) return false;
               
                for (var k in store.baseParams) if (/^filter/i.test(k)) rm.push(k);
                Ext.each(rm, function (txt) { delete store.baseParams[txt]; });

                var filter = [];
                var ret = [];
                var constraint = (val.entity) ? val.constraint : nogrouping_constraint;
                if (val.isScope) {
                    constraint = nogrouping_constraint;
                }
                if (constraint != null) {
                    var flt = constraint, field = flt.field.replace(/^\{0\}\./, '');
                    if (tab.id != "tabScopes") {
                        filter.push({ op: 'eq', type: 'numeric', field: 'Distance', value: '0' });
                    }
                    filter.push({ op: flt.op, type: 'numeric', field: field, value: flt.value });
                }

                // isnull doesn't handle navigation properties ok in where clause
                if (val.nullfix) {
                    filter.push({ op: 'nullfix', type: 'text', field: val.groupby, value: '' });
                }
                
                if (val.groupby) {
                    // [oh] filter even if it is string.Empty, to filter only unknown values.
                        filter.push({ op: 'eq', type: 'text', field: val.groupby, value: groupby });
                }

                for (var n = 0; n < filter.length; n++) {
                    if (tab.id == "tabScopes") {
                        store.baseParams["filter_" + n + "_data_comparison"] = filter[n].op;
                        store.baseParams["filter_" + n + "_data_type"] = filter[n].type;
                        store.baseParams["filter_" + n + "_data_value"] = filter[n].value;
                        store.baseParams["filter_" + n + "_field"] = filter[n].field;
                    } else {
                        store.baseParams["filter[" + n + "][data][comparison]"] = filter[n].op;
                        store.baseParams["filter[" + n + "][data][type]"] = filter[n].type;
                        store.baseParams["filter[" + n + "][data][value]"] = filter[n].value;
                        store.baseParams["filter[" + n + "][field]"] = filter[n].field;
                    }
                }
                store.baseParams["isScope"] = val.isScope;
                store.baseParams["sortDir"] = tab.dir;
                store.baseParams["sortCol"] = tab.property;
                store.baseParams["isCustomScope"] = val.isCustomScope;
                $SW.IPAM.isExpand = val.isScope;
            };
            var afterloadFn = function (that, records) {
                var panel = Ext.getCmp('GroupByList');
                if (panel.records != null) {
                    var recIndex = panel.records.findIndex(panel.selectedValue);
                    panel.body.scrollTo('top', recIndex * 15);
                }
                var sel = $SW.Env['PageLoadSelection'];
                if (!sel) return;
                var ctrl = Ext.getCmp(tid);
                var gId = '' + sel.GroupId, rn = null;
                Ext.each(records, function (o, i) {
                    if (ctrl.id == "tabScopes") {
                        if (o.childNodes != null) {
                            if (o.childNodes.length > 0) {
                                if (o.childNodes[0].attributes.GroupId == gId) {
                                    rn = i;
                                    return false;
                                }
                            }
                        }
                    } else {
                        if (o.data.GroupId == gId) {
                            rn = i;
                            return false;
                        }
                    }
                });
                
                if (rn == null) return;

                window.setTimeout(function () {
                    if (ctrl.id != "tabScopes") {
                        var grid = Ext.getCmp(tid), view = grid.getView();
                        grid.getSelectionModel().selectRow(rn);
                        Ext.get(view.getRow(rn)).scrollIntoView(view.scroller);
                    }
                }, 50);

                $SW.Env['PageLoadSelection'] = null;
            };
            return {
                loadexception: $SW.ext.HttpProxyException,
                beforeload: beforeloadFn,
                load: afterloadFn
            };
        },

        RefreshTab: function (options) {
            var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);

            var val = options.cbox.forceReSelect(false);
            var tabId = options.tabs[val.tab];
            if (!tabId && val.tab == 'none')
                tabId = layout.getActiveTab().id;

            Ext.each(layout.items.items, function (t) {
                t.dirty = (t.id != tabId);
            });

            tab = Ext.getCmp(tabId);
            layout.setActiveTab(tab);
            return tab;
        }

        /* $SW.IPAM.GroupBy */
    };
})();
