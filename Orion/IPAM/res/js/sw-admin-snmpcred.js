$SW.AdminSnmpCredEditorOptionInit = function(nsId) {
    var $nsId = nsId, snmp = {};

    snmp.nsId = $nsId;
    snmp.DisplayName = $SW.nsGet($nsId, 'txtDisplayName');
    snmp.Version2Only = $SW.nsGet($nsId, 'cbVersion2Only');
    snmp.SNMPPort = $SW.nsGet($nsId, 'txtSNMPPort');
    snmp.CommunityString = $SW.nsGet($nsId, 'txtCommunityString');
    snmp.SNMPv3Username = $SW.nsGet($nsId, 'txtSNMPv3Username');
    snmp.SNMPv3Context = $SW.nsGet($nsId, 'txtSNMPv3Context');
    snmp.PasswordKey = $SW.nsGet($nsId, 'tbPasswordKey');
    snmp.SecurityPasswordKey = $SW.nsGet($nsId, 'txtSecurityPasswordKey');
    
    return snmp;
};

$SW.AdminSnmpCredEditorInit = function(nsId, ValFn) {

    var $nsId = nsId;
    var transform = function(id){

        var item = $('#'+id)[0];

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            disabled: item.disabled || false
        });

        return converted;
    };

    var setDefaultValue = function (id) {

        var item = $('#' + id);

        if ($(item).val() == '') {
            $(item).val('public');
        }
    };

    $($SW.ns$($nsId,'warningbox')).css( { display: 'none' } );

    var ver = transform( $SW.nsGet($nsId, 'ddlSNMPVersion' ) );
    var speed = 0;
    var csVal = $SW.ns$( $nsId, 'CommunityStringRequiredValidator' )[0];
    var ValEnable = ValFn || ValidatorEnable;
    var syncVersion = function(){

        var value = ver.value;
        
        if( value == 3 )
        {   
             if(csVal!=null && ValEnable)
                ValEnable(csVal, false);
	         $('#layoutSNMPv1_2').hide(speed);
	         $('#layoutSNMPv3').show(speed);
	         $('#layoutv2only').hide(speed);
        }
        else if( value == 2 )
        {
             //Set Default value 'public' in txtCommunityString field if empty
             setDefaultValue($SW.nsGet($nsId, 'txtCommunityString'));

             if(csVal!=null && ValEnable)
                ValEnable(csVal, true);
             $('#layoutSNMPv1_2').show(speed);
             $('#layoutSNMPv3').hide(speed);
	         $('#layoutv2only').show(speed);
        }
        else
        {
             //Set Default value 'public' in txtCommunityString field if empty
             setDefaultValue($SW.nsGet($nsId, 'txtCommunityString'));

             if(csVal!=null && ValEnable)
                ValEnable(csVal, true);
             $('#layoutSNMPv1_2').show(speed);
             $('#layoutSNMPv3').hide(speed);
	         $('#layoutv2only').hide(speed);
        }
    };

    ver.on( 'select', syncVersion );

    var authmethod = transform( $SW.nsGet($nsId, 'ddlAuthMethod' ) );
    var authkeyid = $SW.nsGet($nsId, 'tbPasswordKey');
    var divAuthPasswordIsKey = $SW.nsGet($nsId, 'divAuthPasswordIsKey');
    var chbAuthPasswordIsKey = $SW.nsGet($nsId, 'chbAuthPasswordIsKey');
    
    var securemethod = transform( $SW.nsGet($nsId, 'ddlSecurityMethod' ) );
    var securekeyid = $SW.nsGet($nsId, 'txtSecurityPasswordKey');
    var divSecurityPasswordIsKey = $SW.nsGet($nsId, 'divSecurityPasswordIsKey');
    var securityPasswordIsKey = $SW.nsGet($nsId, 'chbSecurityPasswordIsKey');

    var syncMethod = function(obj,twinid,divId,chbId){

        var twin = $('#'+twinid);
        var val = obj.value;

        if( val == 'None' )
        {
            twin.parent().addClass('x-item-disabled');
            twin[0].disabled = true;

            if (divId) {
                $("#" + divId).children().prop('disabled', true);
                $("#" + divId).children().addClass('x-item-disabled');
            }
            
            if (chbId)
                $("#" + chbId).prop('checked', false);
        }
        else
        {
            twin.parent().removeClass('x-item-disabled');
            twin[0].disabled = false;
            
            if (divId) {
                $("#" + divId).children().prop('disabled', false);
                $("#" + divId).children().removeClass('x-item-disabled');
            }
        }
    };

    authmethod.on('select', function () { syncMethod(authmethod, authkeyid, divAuthPasswordIsKey, chbAuthPasswordIsKey); });
    securemethod.on('select', function () { syncMethod(securemethod, securekeyid, divSecurityPasswordIsKey, securityPasswordIsKey); });

    var pwwarn = function(e)
    {
        e = e || window.event;
        var d = e.target || e.srcElement;
        var val = d.value;
        var last = d.prevValue;

        if( val != '' && val != '**********' )
        {
            if( val != last )
                $($SW.ns$($nsId,'warningbox')).show(200);
        }
        else
        {
            $($SW.ns$($nsId,'warningbox')).hide(200);
        }

        d.prevValue = val;
    };

    $( '#' + $SW.nsGet($nsId,'txtSecurityPasswordKey') ).bind( 'change keyup', pwwarn );
    $( '#' + $SW.nsGet($nsId,'tbPasswordKey') ).bind( 'change keyup', pwwarn );

    syncMethod(authmethod, authkeyid, divAuthPasswordIsKey, chbAuthPasswordIsKey);
    syncMethod(securemethod, securekeyid, divSecurityPasswordIsKey, securityPasswordIsKey);
    syncVersion();

    speed = 250;

    var snmp = $SW.AdminSnmpCredEditorOptionInit($nsId);
    snmp.SNMPVersion = ver;
    snmp.AuthMethod = authmethod;
    snmp.SecurityMethod = securemethod;
    $SW.AdminSnmpCredOpt = snmp;
};


// list menu commands

$SW.AdminSnmpCredMove = function(that,direction) {

  var selector = $SW['selector'];
  if( !selector ) return;

  if( selector.getCount() < 1 )
  {
    Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
       msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_4;E=js}', direction),
       buttons: Ext.Msg.OK,
       animEl: that.id,
       icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var ids = [];
  
  Ext.each( selector.getSelections(), function(o){
    ids.push(o.id);
  });

  Ext.Ajax.request({
   url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
   
   success: function(p1,p2,p3)
   {
    $SW.AdminSnmpRefresh();
   },

   failure: function(){

       Ext.Msg.show({
           title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
           msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
           buttons: Ext.Msg.OK,
           animEl: that.id,
           icon: Ext.MessageBox.ERROR
        });        
   },

   params:
   {
    entity: 'IPAM.SNMPCred',
    verb: 'Resequence',
    ids: ids.join(','),
    moveUp: direction == 'Up' ? "true" : "false"
   }
   });
};

$SW.AdminSnmpCredDelete = function(that) {

  var btnsrc = that;
  var selector = $SW['selector'];
  if( !selector ) return;

  if( selector.getCount() < 1 )
  {
    Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
       msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_7;E=js}',
       buttons: Ext.Msg.OK,
       animEl: btnsrc.id,
       icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var ids = [];
  
  Ext.each( selector.getSelections(), function(o){
    ids.push(o.id);
  });

  var fnContinue = function()
  {
      Ext.Ajax.request({
       url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
       
       success: function(p1,p2,p3)
       {
        $SW.AdminSnmpRefresh();
       },

       failure: function(){

           Ext.Msg.show({
               title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
               msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
               buttons: Ext.Msg.OK,
               animEl: btnsrc.id,
               icon: Ext.MessageBox.ERROR
            });        
       },

       params:
       {
        entity: 'IPAM.SNMPCred',
        verb: 'DeleteMany',
        ids: ids.join(',')
       }
       });
   };

    Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
       msg: String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_9;E=js}", ids.length), 
       buttons: Ext.Msg.YESNO,
       animEl: btnsrc.id,
       icon: Ext.MessageBox.QUESTION,
       fn: function(btn){ if( btn == 'yes' ) fnContinue(); }
       });
};

$SW.AdminSnmpCredEdit = function(that) {

    var sel = $SW['selector'];
    var c = sel.getCount();
    if( c < 1 || c > 1 )
    {
        Ext.Msg.show({
           title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
           msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_11;E=js}',
           buttons: Ext.Msg.OK,
           animEl: that.id,
           icon: Ext.MessageBox.WARNING
        });
    }
    else
    {
        window.location = 'admin.snmpcred.edit.aspx?objectid=' + sel.getSelected().id;
    }
};

$SW.AdminSnmpRefresh = function() { $SW['store'].load(); };
$SW.AdminSnmpCredUp = function(that) { $SW.AdminSnmpCredMove(that,'Up'); };
$SW.AdminSnmpCredDown = function(that) { $SW.AdminSnmpCredMove(that,'Down'); };


$SW.AdminSnmpCredEditorToJson = function() {
    var snmp = $SW.AdminSnmpCredOpt;
    if(!snmp.nsId){ return; }

    var ReadValue = function(id){
      var crtl = $('#'+id);
      if(crtl && crtl[0]) { return crtl[0].value;}
      return '';
    };
    var ReadChecked = function(id){
      var crtl = $('#'+id);
      if(crtl && crtl[0]) { return crtl[0].checked;}
      return false;
    };
    
    var json = {};
    // strings
    json.DisplayName=ReadValue(snmp.DisplayName);
    json.SNMPPort=ReadValue(snmp.SNMPPort);
    json.CommunityString=ReadValue(snmp.CommunityString);
    json.SNMPv3Username=ReadValue(snmp.SNMPv3Username);
    json.SNMPv3Context=ReadValue(snmp.SNMPv3Context);
    json.PasswordKey=ReadValue(snmp.PasswordKey);
    json.SecurityPasswordKey=ReadValue(snmp.SecurityPasswordKey);
    json.Version2Only=ReadChecked(snmp.Version2Only);
    if(snmp.SNMPVersion) {json.SNMPVersion = snmp.SNMPVersion.value;}
    if(snmp.AuthMethod) {json.AuthMethod = snmp.AuthMethod.value;}
    if(snmp.SecurityMethod) {json.SecurityMethod = snmp.SecurityMethod.value;}
    return json;
}
