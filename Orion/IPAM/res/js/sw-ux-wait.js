$SW.Tasks = new (function () {

    var map = {}, runner = new Ext.util.TaskRunner(100);

    var complete = function (id) {
        try {
            var item = map[id];
            if (item.task) runner.stop(item.task);

            if (item.config.fnOk) item.config.fnOk(item.status);
        }
        catch (ex) { /*console.log(ex);*/ }
    };

    var recv = function (id, status, isCancelled) {
        try {
            var taskid = id, item = map[id];
            item.status = status;

            if (status.success != null && status.success != undefined) {
                Ext.MessageBox.hide();
                if (status.success == false && status.state == 4) {
                    if (item.config.fnC) item.config.fnC(item.status);
                }

                complete(id);
                return;
            }

            if (item.config.fnUpdate && isCancelled == false) item.config.fnUpdate(item.status);
            item.inprogress = 0;
            item.next = (new Date()).getTime() + (item.config.options.refresh || 2000);
        } catch (ex) { /*console.log(ex);*/ }
    };

    var fail = function (id) {
        try {
            var item = map[id];
            if (item.task) runner.stop(item.task);
            if (item.config.fnFail) item.config.fnFail();
        } catch (ex) { /*console.log(ex);*/ }
    };

    var cancel = function (id) {
        try {
            var taskid = id, item = map[id];

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/Tasks.asmx/Cancel',
                jsonData: { taskid: id },
                success: $SW.ext.AjaxMakePassDelegate(item.config.description, function (ret) { recv(ret.d.taskid, ret.d, true); }),
                failure: $SW.ext.AjaxMakeFailDelegate(item.config.description, function () { fail(taskid); })
            });
        } catch (ex) { /*console.log(ex);*/ }
    };


    var touch = function (id) {
        try {
            var taskid = id, item = map[id];
            if (item.inprogress) return;
            if (item.next && ((new Date()).getTime() < item.next)) return;
            item.inprogress = 1;

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/Tasks.asmx/Status',
                jsonData: { taskid: id, autokill: true },
                success: $SW.ext.AjaxMakePassDelegate(item.config.description, function (ret) { recv(ret.d.taskid, ret.d,false); }),
                failure: $SW.ext.AjaxMakeFailDelegate(item.config.description, function () { fail(taskid); })
            });
        } catch (ex) { /*console.log(ex);*/ }
    };

    var reg = function (id, status, regdata) {
        try {
            var taskid = id, item = map[id] = {
                id: id,
                config: regdata,
                status: status,
                last: null
            };

            if (item.success != null && item.success != undefined) {
                item.status = status;
                complete(id);
            }
            else {
                item.task = { run: touch, args: [taskid], interval: item.config.interval || 1000 };
                runner.start(item.task);

                if (item.config.fnUpdate)
                    item.config.fnUpdate(item.status);
            }
        } catch (ex) { /*console.log(ex);*/ }
    };

    var start = function (tname, data, fnOk, opts) {
        try {
            opts = opts || {};

            var regdata = {
                fnOk: fnOk,
                fnFail: opts.fnFail || null,
                fnCancel: opts.fnCancel || null,
                fnUpdate: opts.fnUpdate || null,
                description: opts.description || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_29;E=js}',
                options: opts
            };

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/Tasks.asmx/Start',
                jsonData: { taskname: tname, options: data },
                success: $SW.ext.AjaxMakePassDelegate(regdata.description, function (ret) { reg(ret.d.taskid, ret.d, regdata); }),
                failure: $SW.ext.AjaxMakeFailDelegate(regdata.description, regdata.fnFail)
            });
        } catch (ex) { /*console.log(ex);*/ }
    };

    var attachExisting = function (taskId, fnOk, opts) {
        try {
            opts = opts || {};

            var regdata = {
                fnOk: fnOk,
                fnFail: opts.fnFail || null,
                fnCancel: opts.fnCancel || null,
                fnUpdate: opts.fnUpdate || null,
                description: opts.description || '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_29;E=js}',
                options: opts
            };

            reg(taskId, '', regdata);

        } catch (ex) { /*console.log(ex);*/ }
    };

    function initiateCancellationProgress(title) {
        Ext.MessageBox.wait("Processing...", 'Please Wait');
    }

    var DoProgress = function (tname, data, msgbox, fnOk, fnCancel) {
        msgbox = msgbox || {};

        var taskid = null, fnC = fnCancel, fnComp = fnOk,
          fn = function (btn) {
              if (btn != 'cancel' || !taskid) return;
              cancel(taskid);
              Ext.MessageBox.hide();
              initiateCancellationProgress(msgbox.title);
          };

        Ext.applyIf(msgbox, { title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_30;E=js}", msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_31;E=js}", progressText: "-", progress: true, closable: false, width: 300, fn: fn });
        if (fnCancel) msgbox.buttons = Ext.apply({}, msgbox.buttons, Ext.MessageBox.CANCEL);

        var update = function (status) {
            taskid = status.taskid;
            if (status.posmax != null && status.posmax != undefined) {

                var i = status.pos / status.posmax;

                //make sure we don't output non-sense percentage
                if (i > 1)
                    i = 1;

                Ext.MessageBox.updateProgress(i, Math.round(100 * i) + '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_32;E=js}', status.message || undefined);
            }
        };

        Ext.MessageBox.show(msgbox);

        start(tname, data, function (status) {
            Ext.MessageBox.hide();
            if (fnComp) fnComp(status);
        },
          { fnUpdate: update, fnFail: function () { Ext.MessageBox.hide(); }, refresh: 2500 }
        );
    };

    var Attach = function (taskId, msgbox, fnOk, fnCancel) {
        msgbox = msgbox || {};

        var fnC = fnCancel, fnComp = fnOk,
        fn = function (btn) {
            if (btn != 'cancel' || !taskid) return;
            cancel(taskid);
            Ext.MessageBox.hide();
            initiateCancellationProgress(msgbox.title);
        };

        Ext.applyIf(msgbox, { title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_30;E=js}", msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_31;E=js}", progressText: "-", progress: true, closable: false, width: 300, fn: fn });
        if (fnCancel) msgbox.buttons = Ext.apply({}, msgbox.buttons, Ext.MessageBox.CANCEL);

        var update = function (status) {
            taskid = status.taskid;
            if (status.posmax != null && status.posmax != undefined) {
                var i = status.pos / status.posmax;
                Ext.MessageBox.updateProgress(i, Math.round(100 * i) + '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_32;E=js}', status.message || undefined);
            }
        };

        Ext.MessageBox.show(msgbox);

        attachExisting(taskId, function (status) {
            Ext.MessageBox.hide();
            if (fnComp) fnComp(status);
        },
        { fnUpdate: update, fnFail: function () { Ext.MessageBox.hide(); }, refresh: 2500 }
      );
    };

    this.start = start;
    this.DoProgress = DoProgress;
    this.Attach = Attach;

})();
