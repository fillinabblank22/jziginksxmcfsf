﻿(function () {

    if (!$SW.IPAM) $SW.IPAM = {};

    $SW.IPAM.RouterList_renderer_buttons = function () {
        var up_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.up.png";
        var down_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.down.png";
        var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
        var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
        return function (value, meta, record) {
            var ret = '';
            meta.css = 'sw-grid-buttons';

            ret += "<a href='#' class='sw-grid-up' onclick='return false;'><img src='" + up_url + "' /></a> &nbsp;";
            ret += "<a href='#' class='sw-grid-down' onclick='return false;'><img src='" + down_url + "' /></a> &nbsp;";
            ret += "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
            return ret + "<a href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
        }
    };

    if (!$SW.IPAM.RouterList)
        $SW.IPAM.RouterList = function (opt) {
            var options = opt || {};
            $.extend(options, { GridId: undefined, WebId: undefined });
            var that = {
                getGrid: function () {
                    return $SW[options.GridId];
                },
                getWebIdParam: function () {
                    var w = options.WebId;
                    return (w && w != '') ? '&w=' + w : '';
                },
                SetWebId: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore(); 
                    store.baseParams.w = options.WebId;
                },
                AddServerBtnPressed: function (el) {
                    var params = that.getWebIdParam();
                    $SW.ext.dialogWindowOpen(
                        $SW['masterServerWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_JI_4;E=js}',
                        '/Orion/IPAM/DhcpScope/Dhcp.Scope.RouterIP.aspx?NoChrome=True&msgq=masterServerWindow' + params,
                        'OrionIPAMAGEditingDHCPScopes.htm');
                },
                ServerRefresh: function () {
                    var grid = that.getGrid();
                    var store = grid.getStore();
                    store.reload();
                },
                EditServer: function (el, r) {
                    var params = that.getWebIdParam();
                    $SW.ext.dialogWindowOpen(
                        $SW['masterServerWindow'],
                        el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_JI_5;E=js}',
                        '/Orion/IPAM/DhcpScope/Dhcp.Scope.RouterIP.aspx?NoChrome=True&msgq=masterServerWindow&serverAddress=' + r.data['serverAddress'] + params,
                        'OrionIPAMAGEditingDHCPScopes.htm');
                },
                ServerRemove: function (el, r) {
                 Ext.Ajax.request({
                        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                        success: function (p1, p2, p3) {
                            that.ServerRefresh();
                        },
                        failure: function () {
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                buttons: Ext.Msg.OK,
                                animEl: that.id,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        params:
                   {
                       entity: 'Routers',
                       verb: 'Remove',
                       ids: r.data["ID"],
                       value: r.data['serverAddress'],
                       w: options.WebId
                   }
                    });
                },
                ServerUp: function (el, r) { that.ServerMove(el, r, 'Up'); },
                ServerDown: function (el, r) { that.ServerMove(el, r, 'Down'); },
                ServerMove: function (el, r, direction) {
                    Ext.Ajax.request({

                        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",

                        success: function (p1, p2, p3) {
                            that.ServerRefresh();
                        },

                        failure: function () {

                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                buttons: Ext.Msg.OK,
                                animEl: that.id,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        params:
                   {
                       entity: 'Routers',
                       verb: 'Resequence',
                       ids: r.data["ID"],
                       value: r.data['serverAddress'],
                       moveUp: direction == 'Up' ? "true" : "false",
                       w: options.WebId
                   }
                    });
                }
            };
            return that;
        };

})();
