
$SW.AdminAccountDisableExpirationDate = function($nsId)
{
	var txtExp = $SW.ns$( $nsId, 'txtExpirationDate' )[0];
	var cbExp  = $SW.ns$( $nsId, 'cbExpiration' )[0];
	var dv = $SW.ns$( $nsId, 'DateValidator' )[0];
	
	if(cbExp.checked)
	{
	    txtExp.disabled=false;
	    $SW['expiresField'].enable();
	    if(dv!=null)
	        ValidatorEnable(dv, true); 
	}
	else
	{
	    txtExp.disabled=true;
	    $SW['expiresField'].disable();
	    if(dv!=null)
	        ValidatorEnable(dv, false); 
	}
};

$SW.AdminAccountEditorInit = function(nsId) {

    var $nsId = nsId;
    var transform = function(id){

        var item = $('#'+id)[0];

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            disabled: item.disabled || false
        });

        return converted;
    };

    var roles = transform( $SW.nsGet($nsId, 'ddlRoles' ) );

    $SW.ns$($nsId,'cbExpiration').click(function(){ $SW.AdminAccountDisableExpirationDate($nsId); });

    //

    var dt = $SW.ns$($nsId,'txtExpirationDate')[0];
    var disabled = dt.disabled;
    var val = dt.value;

    $SW['expiresField'] = new Ext.form.DateField({
        format: 'm/d/Y',
        applyTo: $SW.nsGet($nsId,'txtExpirationDate'),
        value: val,
        emptyText: "No date has been set",
        disabled: disabled
    });
};

// list menu commands

$SW.AdminAccountDelete = function() {

  var selector = $SW['selector'];
  if( !selector ) return;

  if( selector.getCount() < 1 )
  {
    Ext.Msg.show({
       title:'Multiple-Item Selection',
       msg: 'Please select at least one Account before clicking "Delete".',
       buttons: Ext.Msg.OK,
       animEl: 'btnDelete',
       icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var ids = [];
  
  Ext.each( selector.getSelections(), function(o){
    ids.push(o.id);
  });

  var fnContinue = function()
  {
      Ext.Ajax.request({
       url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
       
       success: function(p1,p2,p3)
       {
        $SW.AdminAccountRefresh();
       },

       failure: function(){

           Ext.Msg.show({
               title:'Command Failed',
               msg: 'Operation Failed!',
               buttons: Ext.Msg.OK,
               animEl: 'btnDelete',
               icon: Ext.MessageBox.ERROR
            });        
       },

       params:
       {
        entity: 'IPAM.Account',
        verb: 'DeleteMany',
        ids: ids.join(',')
       }
       });
   };

    Ext.Msg.show({
       title:'Delete Confirmation',
       msg: 'Are you sure you want to delete the [' + ids.length + '] selected Accounts?',
       buttons: Ext.Msg.YESNO,
       animEl: 'btnDelete',
       icon: Ext.MessageBox.QUESTION,
       fn: function(btn){ if( btn == 'yes' ) fnContinue(); }
       });
};

$SW.AdminAccountEdit = function() {

    var sel = $SW['selector'];
    var c = sel.getCount();
    if( c < 1 || c > 1 )
    {
        Ext.Msg.show({
           title:'Single-Item Selection',
           msg: 'Please select a single Account before clicking "Edit".',
           buttons: Ext.Msg.OK,
           animEl: 'btnEdit',
           icon: Ext.MessageBox.WARNING
        });
    }
    else
    {
        window.location = 'admin.account.edit.aspx?objectid=' + sel.getSelected().id;
    }
};

$SW.AdminAccountRoleRenderer = function(){
 var ref = $SW['AccountRoleMap'];
 return function(value) { return ref[value] || "Unknown"; };
};

$SW.AdminAccountEnabledRenderer = function(){
 return function(value,meta) { if( value == "True" ) return "YES"; meta.css = 'sw-cell-bad'; return "NO"; };
};

$SW.AdminAccountExpiresRenderer = function(){
 return function(value,meta)
 {
    if( !value )
        return "NO";

    if( value.getTime() < (new Date()).getTime() )
        meta.css = 'sw-cell-bad'; 

    return value.format('m / d / Y');
 };
};

$SW.AdminAccountRefresh = function() {

$SW['store'].load();

};
