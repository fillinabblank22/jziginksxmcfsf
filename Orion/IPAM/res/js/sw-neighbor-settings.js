﻿var $NSS = {
  delay: 200,
  nsId: undefined,
  TestResult: '',
  ControlDivId: '',
  RadioId: '',
  IPAddressId: '',
  SkipValFn: function(){ return false; },

  GetCtrl: function(id){ return $SW.ns$(this.nsId,id)[0]; },
  
  RadioChecked: function(){
    var r = this.Radio;
    return (r && r[0] && r[0].checked);
  },
  
  ValidateIPAddressRequire: function(value){
    var ip = this.IPAddress;
    if( ip && ip[0] && ip[0].value!='' ) return true;
    return (value!='');
  },

  OnIsIPAddressRequire: function(src, args) {
    if (this.SkipValFn() == true) { return args.IsValid = true; }
    if (this.RadioChecked()) { return args.IsValid = true; }
    return args.IsValid = this.ValidateIPAddressRequire(args.Value);
  },
  
  OnIsIPAddressIPv4: function(src,args){
    if (this.SkipValFn() == true) { return args.IsValid = true; }
    if (this.RadioChecked()) { return args.IsValid = true; }
    return $SW.Valid.Fns.ipv4(src,args);
  },
  
  NeighborTest: function (vgroup, fn) {
      // reset test result
      if (CheckDemoMode()) {
          demoAction('IPAM_NeighbourScanning_Test', null);
          return false;
      }
    this.SyncTestResult('');
    
    if(Page_ClientValidate(vgroup)==false){ return false; }
    var fnContinue = function(val){
      if($SW.NSS.RadioChecked()){ return false; }
      $SW.NSS.fnNeighborContinue(val);
    };
    fn(fnContinue);
    return false;
  },
  SyncTestResult: function(res){
    this.TestResult = res;
    var pass = $('#testpass'),
        warn = $('#testnoarp'),
        fail = $('#testfail');
    if(res=='1') pass.show(150); else pass.hide();
    if(res=='2') warn.show(150); else warn.hide();
    if(res=='3') fail.show(150); else fail.hide();
  },

  Init: function() {
    this.ControlDiv = $(this.ControlDivId);
    this.IPAddress = $SW.ns$(this.nsId,this.IPAddressId);
    this.Radio = $SW.ns$(this.nsId,this.RadioId);
    
    this.Radio.click( function(){ $SW.NSS.SyncScanDisable(false); });
    this.SyncScanDisable(true);
  },
  SyncScanDisable: function(initRun){
	var radio = this.Radio,
	    div = this.ControlDiv,
	    delay = initRun ? 0 : this.delay;
    
    if( !radio || !radio[0]) return;
    if(div){ if(radio[0].checked) div.hide(delay); else div.show(delay); }
    
    // reset test result
	this.SyncTestResult('');
  },

  fnTestOk: function(d){
    $SW.NSS.SyncTestResult(d.results);
  },
  fnTestCancel: function(){
  },
  fnNeighborContinue: function(val){
    var opts = {};
    opts.value = val;
    opts.IPAddress = this.IPAddress[0].value; 
    opts.Credentials = {};
    $SW.Tasks.DoProgress( 'NeighborScanTest',
      opts,
      { title: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_54;E=js}", buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}'} },
      this.fnTestOk, this.fnTestCancel);
  },
  Help: function(file){
    var url = String.format('{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}',
        $SW.HelpServer || '/', file);
    window.open(url, '_blank');
    return false;
  }
};
function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        return true;
    }
    return false;
}
