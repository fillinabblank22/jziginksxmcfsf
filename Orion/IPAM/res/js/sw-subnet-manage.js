﻿$SW.LicenseInfo = null;
$SW.LicenseListeners.push(function (lic) { $SW.LicenseInfo = lic; });

(function () {
    if (!$SW.subnet) $SW.subnet = {};
    var selectedGroupNodeIds = null;
    var treeitem = null;
    var gridfilter = null;
    $SW.subnet.ipv6Gridfilter = null;
    var gridfilterEmptyText = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_34;E=js}";
    var MASK_ROOT = 1, MASK_GROUP = 2, MASK_SUPERNET = 4, MASK_SUBNET = 8;
    var MASK_GLOBAL_PREFIX = 128, MASK_LOCAL_PREFIX = 256, MASK_IPv6_SUBNET = 512;
    var ipstatusmap_txt = { 'unknown': 0, 'used': 1, 'available': 2, 'reserved': 4, 'transient': 8, 'blocked': 16 };
    var typemap = {
        1: 'Hierarchy Group', 2: 'Group', 4: 'Supernet', 8: 'Subnet',
        128: 'IPv6 Global Prefix', 256: 'IPv6 Site', 512: 'IPv6 Subnet'
    };
    var $nsId = null;
    var account_roles = { 'NoAccess': 0, 'ReadOnly': 1, 'Operator': 2, 'PowerUser': 3, 'SiteAdmin': 4 };
    var toolbarstates = function () {
        var types = arguments;
        var _ids = {};

        // tree grid
        if ($.inArray('tree', types) >= 0) Ext.apply(_ids, {
            add: 'BtnAddMenu',
            edt: 'BtnEditCurrent',
            del: 'BtnDeleteCurrent',
            vrfGroup: 'btnAddVrfGroup',
            group: 'btnAddGroup',
            supernet: 'btnAddSupernet',
            subnet: 'btnAddSubnet',
            wizard: 'btnAddSubnetWizard',
            ipv6Glob: 'btnAddGlobalPrefix',
            ipv6Locl: 'btnAddPrefix',
            ipv6Subn: 'btnAddSubnetIPv6',
            ipv6NeighbourDisc: 'ipv6NeighbourDiscovery',
            // group import depends on tree node selection
            grpImp: 'grpImport',
            grpImpSheet: 'grpImportSheet',
            grpImpBulk: 'grpBulkImport',
            grpExp: 'grpExport',
            grpExpAll: 'grpExportAll',
            grpExpStructureOnly: 'grpExportStructureOnly'

        });
        // tabGroup
        if ($.inArray('group', types) >= 0) Ext.apply(_ids, {
            grpEdt: 'grpEdit',
            grpDel: 'grpDelete'
        });
        // tabSubnet
        if ($.inArray('ipv4', types) >= 0) Ext.apply(_ids, {
            ip4add: 'ipv4AddRange',
            ip4sel: 'ipv4SelectRange',
            ip4scan: 'ipv4ScanNow',
            ip4Imp: 'ipv4Import',
            ip4ImpIP: 'ipv4IPImport',
            ip4ImpBulk: 'ipv4BulkImport',
            ip4Exp: 'ipv4Export',
            ip4ExpAll: 'ipv4ExportAll',
            ip4ExpStructureOnly: 'ipv4ExportStructureOnly'
        });
        if ($.inArray('ipv4Sel', types) >= 0) Ext.apply(_ids, {
            ip4edt: 'ipv4Edit',
            ip4del: 'ipv4Delete',
            ip4stat: 'ipv4SetStatus',
            ip4avail: 'ipv4Available',
            ip4used: 'ipv4Used',
            ip4trans: 'ipv4Transient',
            ip4resv: 'ipv4Reserved'
        });
        // tabIPv6Subnet
        if ($.inArray('ipv6', types) >= 0) Ext.apply(_ids, {
            ip6add: 'ipv6AddIP',
            ip6scan: 'ipv6ScanNow',
            ip6imp: 'ipv6Import',
            ip6Exp: 'Ipv6Export',            
            ip6ExpAll: 'Ipv6ExportAll',
            ip6ExpStructureOnly: 'Ipv6ExportStructureOnly'
        });
        if ($.inArray('ipv6Sel', types) >= 0) Ext.apply(_ids, {
            ip6edt: 'ipv6Edit',
            ip6del: 'ipv6Delete',
            ip6stat: 'ipv6SetStatus',
            ip6avail: 'ipv6Available',
            ip6used: 'ipv6Used',
            ip6trans: 'ipv6Transient',
            ip6resv: 'ipv6Reserved'
        });

        var _allow = [];
        var _has = function (v) { return ($.inArray(v, _allow) >= 0); };
        return {
            reset: function () { _allow = []; },
            allow: function (id) { if (!_has(id)) _allow.push(id); },
            eval: function () {
                var _t = this, has = _has;
                $.each(_ids, function (b, id) {
                    var btn = Ext.getCmp(id);
                    if (has.apply(_t, [b])) btn.enable(); else btn.disable();
                });

            }
        };
    };

    $SW.subnet.tabGridPlugin = function (config) {
        Ext.apply(this, config);
    };
    Ext.extend($SW.subnet.tabGridPlugin, Ext.util.Observable, {
        groupSelFn: function (sm) {
            var tb = new toolbarstates('group');
            tb.reset();
            if (sm.getCount() > 0) {
                var role = undefined;
                $.each(sm.getSelections(), function (i, sel) {
                    var r = account_roles[sel.data["Role"]];
                    if (isNaN(role) || (!isNaN(r) && r < role)) role = r;
                });
                if (!isNaN(role) && (role >= 3 || $SW.IsDemoServer)) {
                    tb.allow('grpEdt');
                    if (role >= 3)
                        tb.allow('grpDel');
                }

            }
            tb.eval();
        },
        subnet4SelFn: function (sm) {
            if (treeitem == null) return;
            var tb = new toolbarstates('ipv4Sel');
            var role = account_roles[treeitem.attributes["Role"]] || 0;
            if ((sm.getCount() > 0) && (role >= 2)) {
                tb.allow('ip4edt'); tb.allow('ip4del'); tb.allow('ip4stat');
                tb.allow('ip4avail'); tb.allow('ip4used');
                tb.allow('ip4trans'); tb.allow('ip4resv');
            }
            if ((sm.getCount() > 0) && $SW.IsDemoServer) {
                // enable 'set status' 'Edit' & 'Delete' group button for demo server
                tb.allow('ip4stat');
                tb.allow('ip4edt');
                tb.allow('ip4del');
                // enable 'set status' dropdown items for demo server
                tb.allow('ip4avail'); tb.allow('ip4used');
                tb.allow('ip4trans'); tb.allow('ip4resv');
            }
            tb.eval();
        },
        subnet6SelFn: function (sm) {
            if (treeitem == null) return;
            var tb = new toolbarstates('ipv6Sel');
            var role = account_roles[treeitem.attributes["Role"]] || 0;
            if ((sm.getCount() > 0) && (role >= 2)) {
                tb.allow('ip6edt'); tb.allow('ip6del'); tb.allow('ip6stat');
                tb.allow('ip6avail'); tb.allow('ip6used');
                tb.allow('ip6trans'); tb.allow('ip6resv');
            }
            if ((sm.getCount() > 0) && $SW.IsDemoServer) {
                // enable 'set status' 'Edit' & 'Delete' group button for demo server
                tb.allow('ip6stat');
                tb.allow('ip6edt');
                tb.allow('ip6del');
                // enable 'set status' dropdown items for demo server
                tb.allow('ip6avail'); tb.allow('ip6used');
                tb.allow('ip6trans'); tb.allow('ip6resv');
            }
            tb.eval();
        },
        init: function (grid) {
            var fn, id = grid.id;
            if (id == 'tabGroup') fn = this.groupSelFn;
            else if (id == 'tabSubnet') fn = this.subnet4SelFn;
            else if (id == 'tabIPv6Subnet') fn = this.subnet6SelFn;
            if (fn) grid.getSelectionModel().on('selectionchange', fn);
        }
    });


    //
    // IP Count
    //

    $SW.subnet.recvlic = function (obj) {
        var lic = obj; $(document).ready(function () {
            var ipcount = $('#ipcount');
            if (!ipcount[0]) { $(document.body).prepend("<div id='ipcount' style='width: auto !important'></div>"); ipcount = $('#ipcount'); }

            var max = parseInt(lic.countmax);
            var pos = parseInt(lic.countcurrent);
            var cls = "ok";
            if (parseInt(lic.countmax) >= 1000000) { max = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_166;E=js}"; cls = "ok"; }
            if (max * 0.9 < pos) cls = "critical";
            else if (max * 0.7 < pos) cls = "warn";
            ipcount[0].className = cls;
            ipcount.html(String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_35;E=js}", "<span class='ipcountlabel'>", "</span>", "<span class='ipcountcurrent'>", lic.countcurrent, "<span class='ipcountmax'>", max));
        });
    };

    //
    // selection mgmt
    //
    $SW.subnet.Help = function () {
        var activetab = Ext.getCmp('ipam_tabs').getActiveTab();
        var activeid = activetab ? activetab.id : undefined;
        var v = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';

        if (activeid == 'tabReport') {
            window.open(String.format(v, $SW.HelpServer || '/', 'OrionIPAMPHViewManageSubnetsIPAddressesChart'), '_blank');
        }
        else if (activeid == 'tabSubnet') {
            window.open(String.format(v, $SW.HelpServer || '/', 'OrionIPAMPHViewManageSubnetsIPAddressesIP'), '_blank');
        }
        else if (activeid == 'tabGroup') {
            window.open(String.format(v, $SW.HelpServer || '/', 'OrionIPAMPHViewManageSubnetsIPAddressesNetwork'), '_blank');
        }
        else if (activeid == 'tabIPv6Subnet') {
            window.open(String.format(v, $SW.HelpServer || '/', 'OrionIPAMPHViewManageSubnetsIPv6AddressesNetwork'), '_blank');
        }
    };

    $SW.subnet.ShowTab = function (showid, hideids) {
        var tabs = Ext.getCmp('ipam_tabs');
        var activetab = tabs.getActiveTab();
        var activeid = activetab ? activetab.id : undefined;

        if (activeid == showid)
            return;

        $.each(hideids, function (i, id) { tabs.hideTabStripItem(id); });
        tabs.unhideTabStripItem(showid);

        if (activeid != 'tabReport')
            tabs.setActiveTab(showid);
    };

    $SW.subnet.ChartReportRow = new Ext.XTemplate(
        '<tr>',
        '<td class=rptcount><span>{n}</span></td>',
        '<td class="rptlabel {cls}"><div>{t} ({p})</div></td>',
        '</tr>'
    );
    $SW.subnet.ChartReportRow.compile();

    $SW.subnet.ChartReportTpl = new Ext.XTemplate(
        // -- summary
        '<table border="0" cellspacing="0" cellpadding="0" ',
        'style="<tpl if="isHidden">-moz-opacity: 0.5; opacity: .50; filter: alpha(opacity=50);</tpl>">',
        '{[ this.chart_rows(values) ]}',
        '<tr class="rptsum">',
        '<td class="rptcount"><span>',
        '<tpl if="!isHidden">{TotalCount}</tpl>',
        '<tpl if="isHidden">&#133;%</tpl>',
        '</span></td>',
        '<td class=rptlabel><div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_36;E=js}</div></td>',
        '</tr>',
        '<tr><td>&nbsp;</td></tr>',
        '</table>',
        // -- explanation
        '<table border="0" cellspacing="0" cellpadding="0" ',
        'style="table-layout: fixed; width: 100%; <tpl if="isHidden">-moz-opacity: 0.5; opacity: .50; filter: alpha(opacity=50);</tpl>">',
        '<tr>',
        '<td class=rptcount><span>{[ this.available(values) ]}</span></td>',
        '<td>' + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_37;E=js}', '') + '</td>',
        '</tr>',
        '<tr>',
        '<td class=rptcount><span>{[ this.n_available(values) ]}</span></td>',
        '<td>' + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_38;E=js}', '') + '</td>',
        '</tr>',
        {
            fn: function () {
                if (!this._fn) this._fn = $SW.subnet.ipUsageRenderer(2);
                return this._fn.apply(this, arguments);
            },
            percent: function (n, o) {
                if (o.isHidden) return '&#133;%';
                return o.TotalCount ? this.fn(n / o.TotalCount * 100) : "0%";
            },
            chart_rows: function (o) {
                var r = [''], t = this;
                o.vals.sort(function (o1, o2) { return o2.n - o1.n; });
                Ext.each(o.vals, function (v) {
                    v.p = t.percent.apply(t, [v.n, o]);
                    if (o.isHidden) v.n = '&#133;';
                    r.push($SW.subnet.ChartReportRow.apply(v));
                });
                return r.join('');
            },
            available: function (v) {
                return this.percent(v.AvailableCount, v);
            },
            n_available: function (v) {
                return this.percent((v.TotalCount - v.AvailableCount), v);
            }
        }
    );
    $SW.subnet.ChartReportTpl.compile();

    $SW.subnet.RefreshReport = function () {
        $('#reportimage')[0].src = "/orion/ipam/res/images/default/s.gif";
        $('#reportheading').html(treeitem.attributes.text);
        $('#reportcontent').addClass('x-hide-display');
        $('#reportloading').removeClass('x-hide-display');

        var ShowErrorMsg = function (text) {
            $('#reportloading,#reportcontent').addClass('x-hide-display');
            $('#reportfailure').removeClass('x-hide-display');

            // if the session has expired, offer to relogin.
            $SW.ext.AjaxIsErrorOrExpire({
                cmd: 'Retrieve Subnet Statistics',
                text: text
            });
        };

        var RenderReport = function (data) {
            data.isHidden = (data["Role"] == 'NoAccess');
            Ext.each(["UsedCount", "AvailableCount", "ReservedCount", "TransientCount", "TotalCount", "AllocSize"],
                function (s) { if (isNaN(data[s] = parseFloat(data[s])) || data.isHidden) data[s] = 0; }
            );

            data.vals = [
                { n: data.UsedCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_39;E=js}", c: "F8D677", cls: 'ip-used' },
                { n: data.ReservedCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_40;E=js}", c: "A492C4", cls: 'ip-availrsvd' },
                { n: data.AvailableCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_41;E=js}", c: "7CC364", cls: 'ip-avail' },
                { n: data.TransientCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_42;E=js}", c: "13CFE3", cls: 'ip-transient' }
            ];
            data.vals.sort(function (o1, o2) { return o1.n - o2.n; });

            var colors = [], numbers = [];
            if (data.TotalCount == 0) {
                colors.push("FEFEFE");
                numbers.push("1");
            } else {
                Ext.each(data.vals, function (o) {
                    if (!o.n) return;
                    colors.push(o.c);
                    numbers.push(o.n);
                });
            }

            var img = $('#reportimage')[0];
            img.src = String.format("ImgChartProvider.ashx?w={0}&h={1}&d={2}&c={3}",
                $(img).attr('xW'), $(img).attr('xH'), numbers.join('|'), colors.join('|'));

            var table = $SW.subnet.ChartReportTpl.apply(data);

            $('#reportlegend').html(table + "</table>");
            $('#reportloading,#reportfailure').addClass('x-hide-display');
            $('#reportcontent').removeClass('x-hide-display');
        };

        // fetch calculations for this group/supernet/subnet
        var conn = new Ext.data.Connection();
        conn.request({
            url: 'ExtDataProvider.ashx',
            method: 'POST',
            reqid: treeitem.id,
            params: {
                'entity': 'IPAM.GroupNode',
                'filter[0][data][comparison]': 'eq',
                'filter[0][data][type]': 'numeric',
                'filter[0][data][value]': '' + treeitem.id,
                'filter[0][field]': 'GroupId',
                'filter[1][data][comparison]': 'eq',
                'filter[1][data][type]': 'numeric',
                'filter[1][data][value]': '0',
                'filter[1][field]': 'Distance',
                'properties': 'GroupId,Role,UsedCount,AvailableCount,ReservedCount,TransientCount,TotalCount,AllocSize'
            },
            success: function (reqid) {
                return function (responseObject) {
                    // is this request for us? if not, ignore it.
                    if (reqid != treeitem.id)
                        return;

                    var reader = new Ext.ux.SWArrayReader(
                        { root: "rows", totalProperty: "count", id: 0 },
                        [{ name: "GroupId" },
                        { name: "Role" },
                        { name: "UsedCount" },
                        { name: "AvailableCount" },
                        { name: "ReservedCount" },
                        { name: "TransientCount" },
                        { name: "TotalCount" },
                        { name: "AllocSize" }]);

                    var ret = null;
                    try { ret = reader.read(responseObject); } catch (e) { }

                    if (!ret || !ret.success || !ret.totalRecords) {
                        ShowErrorMsg(responseObject.responseText);
                    } else {
                        RenderReport(ret.records[0].data);
                    }
                }
            }(treeitem.id),
            failure: function (reqid) {
                return function () {
                    // is this request for us? if not, ignore it.
                    if (reqid != treeitem.id)
                        return;

                    $('#reportloading,#reportcontent').addClass('x-hide-display');
                    $('#reportfailure').removeClass('x-hide-display');

                };
            }(treeitem.id)
        });
    };

    $SW.subnet.RefreshTab = function (parentchanged) {

        if (treeitem == null) return;
        var currentGridFilter = null;

        var opt = {
            typeid: parseInt(treeitem.attributes.t, 10),
            grid: undefined,
            filter: []
        };

        if (opt.typeid & MASK_SUBNET) {
            opt.grid = Ext.getCmp('tabSubnet');
            opt.filter.push({ op: 'eq', type: 'numeric', field: 'SubnetId', value: treeitem.attributes.id });
            if (gridfilter != "ffConflicts" && gridfilter != "ffAll" && gridfilter != "ffDynamic")
                opt.filter.push({ op: 'neq', type: 'numeric', field: 'IsConflict', value: 1 });
            currentGridFilter = gridfilter;
        }
        else if (opt.typeid & MASK_IPv6_SUBNET) {
            opt.grid = Ext.getCmp('tabIPv6Subnet');
            opt.filter.push({ op: 'eq', type: 'numeric', field: 'SubnetId', value: treeitem.attributes.id });
            currentGridFilter = ($SW.subnet.ipv6Gridfilter && typeof ($SW.subnet.ipv6Gridfilter) != "undefined" && $SW.subnet.ipv6Gridfilter != '') ? $SW.subnet.ipv6Gridfilter : 'ffIPv6All';
        }
        else {
            opt.grid = Ext.getCmp('tabGroup');
            opt.filter.push({ op: 'eq', type: 'numeric', field: 'AncestorId', value: treeitem.attributes.id });
            opt.filter.push({ op: 'eq', type: 'numeric', field: 'Distance', value: '1' });
            currentGridFilter = gridfilter;
        }

        var activetab = Ext.getCmp('ipam_tabs').getActiveTab();
        if (activetab.id == 'tabReport') {
            $SW.subnet.RefreshReport();
            return;
        }

        // refresh grid

        var toolbarPg = opt.grid.getBottomToolbar();
        var store = opt.grid.getStore();
        var baseParams = store.baseParams || {};

        // clear old filters
        var regex = new RegExp("^filter\[[0-9]+\]");
        for (var key in baseParams)
            if (regex.test(key))
                delete baseParams[key];

        var startn = 0;

        var isIPv4View = (opt.grid.id == 'tabSubnet');
        var isIPv6View = (opt.grid.id == 'tabIPv6Subnet');
        if (isIPv4View || isIPv6View) {
            var vcfg = opt.grid.initialConfig.viewConfig;
            var view = opt.grid.getView();
            if (currentGridFilter && currentGridFilter != 'ffAll' && currentGridFilter != 'ffIPv6All') {
                if (!vcfg.emptyText) vcfg.emptyText = view.emptyText;
                view.emptyText = gridfilterEmptyText;

                var prop = (currentGridFilter == 'ffDynamic') ? 'AllocPolicy' : (currentGridFilter == 'ffConflicts') ? 'IsConflict' : 'Status';
                var map = (!isIPv6View) ? { 'ffNone': 0, 'ffAvail': '2', 'ffUsed': '1', 'ffRsvd': '4', 'ffTransient': '8', 'ffDynamic': '4', 'ffConflicts': '1', 'ffBlocked': '16' }
                    : { 'ffNone': 0, 'ffIPv6Avail': '2', 'ffIPv6Used': '1', 'ffIPv6Rsvd': '4', 'ffIPv6Transient': '8', 'ffBlocked': '16' };
                opt.filter.push({ op: 'eq', type: 'numeric', field: prop, value: map[currentGridFilter] });
            }
            else {
                view.emptyText = opt.grid.initialConfig.viewConfig.emptyText || view.emptyText;
            }

            var sel = $SW['initialselection'];
            if (sel && sel.subnet == treeitem.attributes.id)
                if (sel.offset && toolbarPg.pageSize)
                    startn = sel.offset - (sel.offset % toolbarPg.pageSize);
        }
        for (var n = 0; n < opt.filter.length; n++) {
            baseParams["filter[" + n + "][data][comparison]"] = opt.filter[n].op;
            baseParams["filter[" + n + "][data][type]"] = opt.filter[n].type;
            baseParams["filter[" + n + "][data][value]"] = opt.filter[n].value;
            baseParams["filter[" + n + "][field]"] = opt.filter[n].field;
        }
        activetab.lastSeen = treeitem.id;
        toolbarPg.doLoad(startn);
        if (parentchanged) {
            var tree = Ext.getCmp('treeSubnet');
            var ti = treeitem;
            tree.loader.load(treeitem, function () { ti.expand(); });
        }
    };

    $SW.subnet.RefreshGridPage = function () {

        if (treeitem == null)
            return;

        var typeid = parseInt(treeitem.attributes.t, 10);
        var gridObj;

        if (typeid & MASK_SUBNET) {
            gridObj = Ext.getCmp('tabSubnet');
        }
        else if (typeid & MASK_IPv6_SUBNET) {
            gridObj = Ext.getCmp('tabIPv6Subnet');
        }
        else {
            gridObj = Ext.getCmp('tabGroup');
        }

        $SW.ClearGridSelection(gridObj);

        var toolbarPg = gridObj.getBottomToolbar();
        var store = gridObj.getStore();

        var handler = function (records, options, success) {
            var pageData = toolbarPg.getPageData();
            if (pageData.activePage > pageData.pages) {
                $SW.subnet.RefreshTab();
            }
        };

        var reloadOptions = { callback: handler };
        store.reload(reloadOptions);

    };

    function ChangeParentPermission() {
        var result = true;
        Ext.each(Ext.combine([], arguments), function (r, i) {
            result &= (r == 'SiteAdmin') || (r == 'PowerUser');
        });
        return !!result;
    };

    function treelisteners() {
        this.statechanged = function (t, s, newCS, oldCS) {
            UpdateUISetting('UI.MgtTreeGridColumns:' + newCS);
        };

        var beforeselect = function (that, node) {
            if (node == null) return true;
            if (node.attributes.t == -1) {
                node.parentNode.select();
                return false;
            }
            return true;
        };

        var multiselectionchange = function (nodes) {
            var root, role, tb = new toolbarstates('tree');

            $.each(nodes, function (i, n) {
                if (n.id == 0) root = true;
                var r = account_roles[n.attributes["Role"]] || 0;
                if (!isNaN(r) && (!role || role > r)) role = r;
            });

            tb.reset();
            if (role >= 3) {
                tb.allow('edt');
                if (root !== true) tb.allow('del');
            }
            tb.eval();
        };

        var selectionchange = function (sm) {
            var s = sm.getSelectedNodes() || [], c = s.length;
            if (c == 0) return;
            else if (c > 1) {
                return multiselectionchange(s);
            }
            var tree = sm.tree, node = s[0];

            // this happens when a long click occurs and it is released.
            if (node == null) return;
            if (treeitem == node) return;

            if (tree.dontselect == node.id) tree.dontselect = null;
            else if (!node.leaf && !node.expanded) node.expand();

            treeitem = node;

            var tb = new toolbarstates('tree', 'group', 'ipv4', 'ipv6');
            tb.reset();

            var typeid = parseInt(node.attributes.t, 10);
            var role = account_roles[node.attributes["Role"]] || 0;
            if (role >= 3) { // PowerUser
                // GSS TreeGrid buttons
                tb.allow('add');
                tb.allow('edt');
                if (node.id != 0 && !node.attributes.HasSubnets) {
                    tb.allow('del');
                }

                // GSS add buttons
                tb.allow('vrfGroup');
                if (typeid & (MASK_ROOT | MASK_GROUP)) {
                    tb.allow('group'); tb.allow('supernet'); tb.allow('subnet');
                } else if (typeid & MASK_SUPERNET) {
                    tb.allow('supernet'); tb.allow('subnet');
                } else if (typeid & MASK_SUBNET) {
                    tb.allow('subnet');
                }

                // group import depends on tree node selection
                tb.allow('grpImp');
                tb.allow('grpImpSheet');
                tb.allow('grpImpBulk');
            }

            if (role >= 2) { // Operator
                // IPv4 Subnet
                tb.allow('ip4add'); tb.allow('ip4sel');
                tb.allow('ip4Imp'); tb.allow('ip4ImpIP'); tb.allow('ip4ImpBulk');
                if ($SW.SubnetScanningEnabled && (role >= 3)) tb.allow('ip4scan');
                // IPv6 Subnet
                tb.allow('ip6add'); tb.allow('ip6imp');
                if ($SW.SubnetScanningEnabled && (role >= 3)) tb.allow('ip6scan');
            }

            if ($SW.IsDemoServer || role >= 3) { // PowerUser
                tb.allow('grpImp');
                tb.allow('grpImpSheet');
                tb.allow('add');
                tb.allow('edt');

                //This validation added because subnet menubutton shown when selecting IPv6 Global or Local or IPv6 Subnet in Subnet Tree Grid
                if (typeid & MASK_SUBNET)
                    tb.allow('subnet');


                // enable subnet allocation wizard
                if (typeid & (MASK_ROOT | MASK_GROUP | MASK_SUPERNET | MASK_SUBNET))
                    tb.allow('wizard');

                if (typeid & (MASK_GLOBAL_PREFIX | MASK_LOCAL_PREFIX | MASK_IPv6_SUBNET)) {
                    var c = Ext.getCmp('btnNeighbourDiscovery');
                    var d = Ext.getCmp('grpDiscoverSubnet');
                    if (c) {
                        c.show();
                        c.enable();
                        d.hide();
                    }
                } else {
                    var c = Ext.getCmp('btnNeighbourDiscovery');
                    var d = Ext.getCmp('grpDiscoverSubnet');
                    if (c) {
                        c.hide();
                        d.show();
                        d.enable();
                    }
                }

                if (typeid & (MASK_IPv6_SUBNET)) {
                    tb.allow('ipv6NeighbourDisc');
                }

                if (typeid & (MASK_ROOT | MASK_GROUP)) {
                    tb.allow('ipv6Glob');
                } else if (typeid & (MASK_GLOBAL_PREFIX | MASK_LOCAL_PREFIX)) {
                    tb.allow('ipv6Locl'); tb.allow('ipv6Subn');
                }

                // enable import group buttons for demo server
                tb.allow('grpImp'); tb.allow('ip4Imp'); tb.allow('ip4add');
            }

            if (role >= 0) { // Any
                tb.allow('grpExp');
                tb.allow('grpExpAll');
                tb.allow('grpExpStructureOnly');
                tb.allow('ip4Exp'); tb.allow('ip4ExpAll'); tb.allow('ip4ExpStructureOnly');
                tb.allow('ip6Exp'); tb.allow('ip6ExpAll'); tb.allow('ip6ExpStructureOnly');
            }

            tb.eval();

            var tabGroup = 'tabGroup', tabIPv4 = 'tabSubnet', tabIPv6 = 'tabIPv6Subnet';
            if (typeid & MASK_SUBNET) {
                $SW.subnet.ShowTab(tabIPv4, [tabGroup, tabIPv6]);
            } else if (typeid & MASK_IPv6_SUBNET) {
                $SW.subnet.ShowTab(tabIPv6, [tabGroup, tabIPv4]);
            } else {
                $SW.subnet.ShowTab(tabGroup, [tabIPv4, tabIPv6]);
            }
            $SW.ClearGridSelection(Ext.getCmp(tabGroup));
            $SW.ClearGridSelection(Ext.getCmp(tabIPv4));
            $SW.ClearGridSelection(Ext.getCmp(tabIPv6));
            $SW.subnet.RefreshTab();

            var fn = function () {
                var topPosition = node.getUI().wrap.offsetTop - tree.getRootNode().getUI().wrap.offsetTop;
                if (topPosition > 0) {
                    tree.innerBody.dom.scrollTop = topPosition;
                    tree.syncScroll();
                }
            };

            var sel = $SW['treegridInitialselection'];
            if (sel && sel.groupId && sel.groupId == treeitem.id) {
                window.setTimeout(fn, 50);
            }
        };

        this.collapsenode = function (node) {
            if (treeitem != null) {
                for (var pos = treeitem.parentNode; pos; pos = pos.parentNode) {
                    if (pos.id == node.id) {
                        node.getOwnerTree().dontselect = node.id;
                        node.select();
                        break;
                    }
                }
            }

            return true;
        };

        var treeLastWidth = null;

        this.bodyresize = function (that, w, height) {
            if (!w) return;
            if (!treeLastWidth) treeLastWidth = that.initialConfig.width;
            if (treeLastWidth == w) return;

            treeLastWidth = w;
            var jsonvalue = Ext.util.JSON.encode({ width: w });
            var params = "UI.MgtSplitter:" + jsonvalue;
            UpdateUISetting(params);
        };

        this.beforerender = function (that) {
            // unhide first column (if it's hidden)
            var fc = that.columns[0];
            if (fc && fc.hidden === true) fc.hidden = false;

            // hook the selection model to load the appropriate grid.
            that.getSelectionModel().on('selectionchange', selectionchange);
            that.getSelectionModel().on('beforeselect', beforeselect);

            return true;
        };

        this.beforenodedrop = function (evt) {

            if ($SW.ShowParentChangeConfirmDlg !== true) return;

            // [oh] cancel drop in default
            evt.tree.dragZone.proxy.animRepair = false;
            evt.cancel = true;

            // [oh] show confirmation dialog
            var tpl = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';
            var url = String.format(tpl, $SW.HelpServer || '/', 'OrionIPAMPHCustomRoles');
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_43;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_44;E=js}" +
                    '<a href="' + url + '" target="_blank" style="color: #336699; font-size: 11px; text-decoration: none; white-space: nowrap;">' +
                    "&#0187; @{R=IPAM.Strings;K=IPAMWEBJS_VB1_45;E=js}</a><br/><br>" +
                    '<input id="dont-show-chb" type="checkbox" /> @{R=IPAM.Strings;K=IPAMWEBJS_VB1_46;E=js}',
                buttons: Ext.Msg.OKCANCEL,
                animEl: id,
                icon: Ext.Msg.WARNING,
                fn: function (btn, text) {
                    var drop = evt.tree.dropZone, t = evt.target;
                    evt.tree.dragZone.proxy.animRepair = true;
                    if (btn == 'ok') {
                        $SW.ShowParentChangeConfirmDlg = !$('#dont-show-chb').attr('checked');
                        if (evt.point == 'append' && !t.isExpanded()) {
                            t.expand(false, null, function () { drop.completeDrop(evt); }.createDelegate(drop));
                        } else {
                            drop.completeDrop(evt);
                        }
                    }
                }
            });
        };

        var displayNodeMoveErrorMessage = function () {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_DM1_3;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_DM1_4;E=js}",
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.WARNING
            });
        }

        var checkMoveValidation = function (oldparent, newparent) {
            if (oldparent.attributes.t == 1 && newparent.attributes.t == 1) {
                displayNodeMoveErrorMessage();
                return false;
            }

            if (oldparent.id == newparent.id) {
                displayNodeMoveErrorMessage();
                return false;
            }

            if (oldparent.attributes.t == 1) {
                if (newparent.attributes.ClusterId != oldparent.id) {
                    displayNodeMoveErrorMessage();
                    return false;
                }
            }
            else if (newparent.attributes.t == 1) {
                if (oldparent.attributes.ClusterId != newparent.id) {
                    displayNodeMoveErrorMessage();
                    return false;
                }
            }
            else if (oldparent.attributes.ClusterId != newparent.attributes.ClusterId) {
                displayNodeMoveErrorMessage();
                return false;
            }

            return true;
        }

        this.beforemovenode = function (that, node, oldparent, newparent) {
            if (!checkMoveValidation(oldparent, newparent)) {
                return false;
            }

            var wasError = null;
            $.ajax({
                type: "POST",
                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                async: false,
                data: {
                    entity: 'IPAM.GroupNode',
                    verb: 'Move',
                    ObjectId: node.id,
                    ParentId: newparent.id,
                    ShowPCDlg: $SW.ShowParentChangeConfirmDlg,
                    _dc: (new Date()).getTime()
                },
                success: function (msg) {
                    var ret;
                    try { ret = Ext.util.JSON.decode(msg); } catch (ex) { }
                    wasError = $SW.ext.AjaxIsErrorOrExpire({ cmd: "Move Item", btn: that, obj: ret, text: msg });
                }
            });

            if (wasError == null)
                wasError = AjaxIsErrorOrExpire({ cmd: "Move Item", btn: that, obj: null, text: null });

            return !wasError;
        };

        this.movenode = function (that, node, oldparent, newparent, idx) {
            if (treeitem == node) node.select(); else node.unselect();
            // updatecrumbs(treeitem);
        };

        this.nodedragover = function (evt) {
            var t = evt.target, pt = evt.point;
            var ns = evt.data.nodes || [evt.dropNode];
            if (!ns || (ns.length < 1) || !t || !pt) return;

            // [oh] bubble up (to target parent) when it's not append into target node
            if ((pt == "above" || pt == "below") && t.parentNode) {
                t = t.parentNode;
            }

            // [oh] create mixed nodes type and roles array to check
            var nType = 0, tType = t.attributes.t;
            var roles = [t.attributes.Role];
            $.each(ns, function (i, n) {
                nType |= n.attributes.t;
                roles.push(n.attributes.Role);
            });

            // [oh] node and target must have sufficient permission
            if (ChangeParentPermission.apply(this, roles) !== true) {
                evt.cancel = true;
                return;
            }

            // drop into root or groups
            if (nType & (MASK_GROUP | MASK_SUPERNET | MASK_SUBNET | MASK_GLOBAL_PREFIX) &&
                tType & (MASK_ROOT | MASK_GROUP)) {
                evt.cancel = false;
                return;
            }

            // supernets receive supernet or subnet which fits
            if (nType & (MASK_SUBNET | MASK_SUPERNET) &&
                tType & MASK_SUPERNET) {
                var ipaddrobj_t = $SW.IP.v4_subnet($SW.IP.v4(t.attributes.a), $SW.IP.v4_cidr(t.attributes.cidr));
                var tstart_n = $SW.IP.v4_toint(ipaddrobj_t.start);
                var tend_n = $SW.IP.v4_toint(ipaddrobj_t.end);

                evt.cancel = false;
                $.each(ns, function (i, n) {
                    var ipaddrobj_d = $SW.IP.v4_subnet($SW.IP.v4(n.attributes.a), $SW.IP.v4_cidr(n.attributes.cidr));
                    var dstart_n = $SW.IP.v4_toint(ipaddrobj_d.start);
                    var dend_n = $SW.IP.v4_toint(ipaddrobj_d.end);
                    var fit = ((dstart_n >= tstart_n) && (dend_n <= tend_n));
                    evt.cancel = evt.cancel || !fit;
                });
                return;
            }

            evt.cancel = true;
        };

        var getOpenToFromUrl = function () {
            var urlStr = window.location.href;
            var url = new URL(urlStr);
            return url.searchParams.get("opento");
        }

        this.append = function (tree, parent, node, idx) {
            var openTo = getOpenToFromUrl();
            var open = !openTo && node.id == "0";
            if (!node.expanded && open) {
                node.expand();
            }

            return true;
        };
    };

    function tablisteners() {
        this.tabchange = function (that, tab) {
            if (tab.lastSeen == undefined) $SW.subnet.RefreshTab();
            else if (treeitem != null && treeitem.id != tab.lastSeen) $SW.subnet.RefreshTab();

            return true;
        };

        this.render = function (that) {
            that.hideTabStripItem('tabSubnet');
            that.hideTabStripItem('tabIPv6Subnet');
            return true;
        };
    };

    function treeloadlisteners() {
        this.load = function (that, node, response) {
            return true;
        };
    };

    function pgtoolbarlisteners() {
        this.beforerender = function () {
            $SW.subnet.SetGridLoadHighlight();
        };
        this.render = function (t) {
            if (t.items) {
                t.swFilter = t.items.getRange(0, 2) || [];
                t.swFilterBtn = t.swFilter[1];
            }

            var filter = ($SW.Env && $SW.Env['ipfilter']) || 'ffAll';
            $SW.subnet.SetGridFilter(Ext.getCmp(filter));
            return true;
        };
    };

    if ($SW.ShowParentChangeConfirmDlg === undefined)
        $SW.ShowParentChangeConfirmDlg = true;

    $SW.subnet.canChangeParent = ChangeParentPermission;
    $SW.subnet.treelisteners = function () { return new treelisteners(); };
    $SW.subnet.treeloadlisteners = function () { return new treeloadlisteners(); };
    $SW.subnet.tablisteners = function () { return new tablisteners(); };
    $SW.subnet.pgtoolbarlisteners = function () { return new pgtoolbarlisteners(); };

    $SW.subnet.groupdbleclick = function (grid, rowidx, e) {
        var store = grid.getStore();
        var row = store.getAt(rowidx);
        var tree = Ext.getCmp('treeSubnet');
        var ti = tree.getNodeById(row.id);
        if (ti) {
            ti.select();
            ti.expand();
        }
        else { // node not displayed bc max number of nodes is already shown

            if (treeitem && row && row.data) {
                var data = row.data, attributes = {};
                $.each(data || [], function (map, d) {
                    if (!map && typeof (map) != 'string') return true;

                    // [oh] custom properties
                    var rex = /^A\.CustomProperties\./i;
                    map = rex.test(map) ? map.replace(rex, '') : map;

                    attributes[map] = d || "";
                });
                $.extend(attributes, {
                    id: data.GroupId,
                    text: data.FriendlyName,
                    t: parseInt(data.GroupType),
                    m: data.AddressMask,
                    a: data.Address
                });

                if (attributes.t != 1 && attributes.t != 2)
                    attributes.iconCls = data.GroupIconPrefix + '-' + data.StatusIconPostfix;

                var newNode = new Ext.tree.TreeNode(attributes);

                newNode.leaf = attributes.t == 8;

                if (treeitem.childNodes.length == 0) {
                    treeitem.appendChild(newNode);
                }
                else {
                    treeitem.insertBefore(newNode, treeitem.lastChild);
                }

                newNode.select();
                newNode.expand();
            }
        }
    };

    //
    // dialog
    //
    $SW.subnet.editone = function (el, id, typeid, isDiscoveredSubnet) {
        if (typeof (typeid) == 'string')
            typeid = parseInt(typeid, 10);

        var tt;
        var page = isDiscoveredSubnet ? 'SubnetDiscovery/Config/DiscoverySubnetEdit.aspx' : "subnet.edit.aspx";
        var helpid;

        if (typeid & MASK_SUBNET) {
            tt = typemap[MASK_SUBNET];
            helpid = 'OrionIPAMPHWindowEditSubnetProperties.htm';
        }
        else if (typeid & MASK_SUPERNET) {
            tt = typemap[MASK_SUPERNET];
            page = "supernet.edit.aspx";
            helpid = 'OrionIPAMPHWindowEditSupernetProperties.htm';
        }
        else if (typeid & MASK_GROUP || typeid & MASK_ROOT) {
            tt = typemap[MASK_GROUP];
            page = "group.edit.aspx";
            helpid = 'OrionIPAMPHWindowEditGroupProperties.htm'
        }
        else if (typeid & MASK_GLOBAL_PREFIX) {
            tt = typemap[MASK_GLOBAL_PREFIX];
            page = "IPv6.GlobalPrefix.Edit.aspx";
            helpid = 'OrionIPAMPHWindowEditGlobalPrefix.htm'
        }
        else if (typeid & MASK_LOCAL_PREFIX) {
            tt = typemap[MASK_LOCAL_PREFIX];
            page = "IPv6.Prefix.Edit.aspx";
            helpid = 'OrionIPAMPHWindowEditPrefix.htm'
        }
        else if (typeid & MASK_IPv6_SUBNET) {
            tt = typemap[MASK_IPv6_SUBNET];
            page = "IPv6.Subnet.Edit.aspx";
            helpid = 'OrionIPAMPHWindowEditIPv6Subnet.htm'
        }
        else {
            return;
        }

        $SW['mainWindow'].width = 620;


        $SW.ext.dialogWindowOpen(
            $SW['mainWindow'],
            el.id,
            String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_47;E=js}', tt),
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + id +'&TypeId='+ typeid,
            helpid);
    };

    $SW.subnet.editmany = function (el, sel, items, isDiscoveredSubnet) {
        var groups = { ids: [], subnets: [] };
        Ext.each(items, function (o) {
            if (!o.id) return true;
            groups.ids.push(isDiscoveredSubnet ? o.data.DiscoveredSubnetID : o.id);

            // IPv4 or IPv6 subnets
            typeid = (o.data) ? o.data.GroupType : o.attributes.t;
            if (typeof (typeid) == 'string') { typeid = parseInt(typeid, 10); }
            if ((typeid & MASK_SUBNET) || (typeid & MASK_IPv6_SUBNET)) {
                groups.subnets.push(isDiscoveredSubnet ? o.data.DiscoveredSubnetID : o.id);
            }
        });
        var oids, page, title, hlp;
        if (groups.ids.length != groups.subnets.length) {
            page = isDiscoveredSubnet ? 'SubnetDiscovery/Config/AutomaticSubnet.MultiEdit.aspx' : 'Group.MultiEdit.aspx';
            title = isDiscoveredSubnet ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_49;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_48;E=js}';
            hlp = isDiscoveredSubnet ? 'OrionIPAMPHWindowMultiEditSubnet.htm' : 'OrionIPAMPHWindowMultiEditGroup.htm';
            oids = groups.ids.join('|');
        } else {
            page = isDiscoveredSubnet ? 'SubnetDiscovery/Config/AutomaticSubnet.MultiEdit.aspx' : 'Subnet.MultiEdit.aspx';
            title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_49;E=js}';
            hlp = 'OrionIPAMPHWindowMultiEditSubnet.htm';
            oids = groups.subnets.join('|');
        }
        var parentId = isDiscoveredSubnet ? -1 : treeitem.attributes.id;
        var pageUrl = '/Orion/IPAM/' + page + '?NoChrome=True&msgq=multiEditWindow&ParentId=' + parentId;
        var settings = new Object();
        settings.parameterName = 'ObjectID';
        settings.data = oids;
        settings.url = pageUrl;

        var settingsContainer = 'MultiEditSettings';
        if (!window.ipam) { window['ipam'] = {}; }
        window.ipam[settingsContainer] = settings;

        var dialogUrl = '/Orion/IPAM/DialogWindowLoader.aspx?DataContainer=' + settingsContainer;
        $SW.ext.dialogWindowOpen($SW['multiEditWindow'], el.id, title, dialogUrl, hlp);
    };

    $SW.subnet.edit = function (el, isDiscoveredSubnet) {
        var sel = "";
        if (isDiscoveredSubnet) {
            sel = Ext.getCmp('grid').getSelectionModel();
        }
        else {
            sel = Ext.getCmp('tabGroup').getSelectionModel();
        }
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_51;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
        } else {
            var items = sel.getSelections();
            if (c > 1) {
                $SW.subnet.editmany(el, sel, items, isDiscoveredSubnet);
            }
            else {
                var item = sel.getSelected();
                var id = isDiscoveredSubnet ? item.data.DiscoveredSubnetID : item.id;
                var GroupType = isDiscoveredSubnet ? 8 : item.data.GroupType;
                $SW.subnet.editone(el, id, GroupType, isDiscoveredSubnet);
            }
        }
    };

    $SW.subnet.importWithRestriction = function () {
        var id = treeitem.id;
        window.location = "import.upload.intro.aspx?GroupID=" + id;
    };

    $SW.subnet.selected = function (gId, noneFn, singleFn, multiFn, allFn) {
        var g = Ext.getCmp(gId), sm = g.getSelectionModel();
        var all = (g.getAllSelected) ? g.getAllSelected() : false;
        var c = sm.getCount();
        if (c < 1) if (noneFn) return noneFn();
        if (c == 1) if (singleFn) return singleFn(sm);
        if (c > 1 && !all) if (multiFn) return multiFn(sm);
        if (c > 1 && all) if (allFn) return allFn(g);
    };

    $SW.subnet.exporterStructureStep = function (element) {
        var selection = Ext.getCmp('tabGroup').getSelectionModel();
        var countSelection = selection.getCount();

        var listsubnet = [];
        var listgroup = [];

        if (countSelection < 1) {
            if (treeitem !== null) {

                if (parseInt(treeitem.attributes.t) === 8 || parseInt(treeitem.attributes.t) === 512) // for subnets and ipv6subnets
                    listsubnet.push(treeitem.id);
                else
                    listgroup.push(treeitem.id);
    
                var groupIds = listgroup.join('|');
                var subnetIds = listsubnet.join('|');

                Ext.DomHelper.append(document.body, {
                    id: 'post-forward',
                    tag: 'form',
                    method: 'POST',
                    action: 'ExportStructure.PickColumns.aspx',
                    children: [{ tag: 'input', type: 'hidden', value: treeitem.id, name: 'GroupNodeIds' },
                            { tag: 'input', type: 'hidden', value: groupIds, name: 'GroupID' },
                            { tag: 'input', type: 'hidden', value: subnetIds, name: 'SubnetID' },
                            { tag: 'input', type: 'hidden', value: 'all', name: 'ExportType' }]
                }).submit();
            } else {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                    msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_52;E=js}",
                    buttons: Ext.Msg.OK,
                    animEl: element.id,
                    icon: Ext.MessageBox.WARNING
                });
            }
        } else {
            var items = selection.getSelections();

            Ext.each(items, function (o) {
                if (o.id)
                    if (parseInt(o.data.GroupType) === 8 || parseInt(o.data.GroupType) === 512) // for subnets and ipv6subnets
                        listsubnet.push(o.id);
                    else
                        listgroup.push(o.id);
            });

            var groupIds = listgroup.join('|');
            var subnetIds = listsubnet.join('|');

            var allGroupIds = listgroup.concat(listsubnet).join('|');

            Ext.DomHelper.append(document.body, {
                id: 'post-forward',
                tag: 'form',
                method: 'POST',
                action: 'ExportStructure.PickColumns.aspx',
                children: [{ tag: 'input', type: 'hidden', value: allGroupIds, name: 'GroupNodeIds' }, 
                { tag: 'input', type: 'hidden', value: groupIds, name: 'GroupID' },
                { tag: 'input', type: 'hidden', value: subnetIds, name: 'SubnetID' },
                { tag: 'input', type: 'hidden', value: 'all', name: 'ExportType' }]
            }).submit();
        }
    };

    $SW.subnet.exportStructureOnly = function (el) {
        var selection = Ext.getCmp('tabGroup').getSelectionModel();
        var selectionCount = selection.getCount();

        if (selectionCount < 1) {
            if (treeitem !== null) {
                Ext.DomHelper.append(document.body, {
                    id: 'post-forward',
                    tag: 'form',
                    method: 'POST',
                    action: 'ExportStructure.PickColumns.aspx',
                    children: [{ tag: 'input', type: 'hidden', value: treeitem.id, name: 'GroupNodeIds' },
                    {tag: 'input', type: 'hidden', value: 'structure', name: 'ExportType' }]
                }).submit();
            } else {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                    msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_52;E=js}",
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.WARNING
                });
            }
        } else {
            var items = selection.getSelections();
            var listgroup = [];

            Ext.each(items, function (o) {
                if (o.id) {
                    listgroup.push(o.id);
                }
            });

            var groupIds = listgroup.join('|');

            Ext.DomHelper.append(document.body, {
                id: 'post-forward',
                tag: 'form',
                method: 'POST',
                action: 'ExportStructure.PickColumns.aspx',
                children: [{ tag: 'input', type: 'hidden', value: groupIds, name: 'GroupNodeIds' },
                {tag: 'input', type: 'hidden', value: 'structure', name: 'ExportType' }]
            }).submit();
        }
    };

    $SW.subnet.properties = function (el, isDiscoveredSubnet) {
        if (!treeitem) return;

        var sm = treeitem.getOwnerTree().getSelectionModel();
        var sel = sm.getSelectedNodes() || [], c = sel.length;
        if (c == 1) {
            $SW.subnet.editone(el, treeitem.attributes.id, treeitem.attributes.t, isDiscoveredSubnet);
        } else if (c > 1) {
            $SW.subnet.editmany(el, sm, sel, isDiscoveredSubnet);
        } else return;
    };

    $SW.subnet.add = function (el, typeid) {
        typeid = typeid || MASK_SUBNET;

        var tt;
        var helpid;
        var page = "subnet.add.aspx";
        var pid = treeitem.id;
        var ptype = parseInt(treeitem.attributes.t, 10);
        var clusterId = treeitem.attributes.ClusterId;

        if (ptype === 1) {
            clusterId = treeitem.attributes.GroupId;
        }

        if (ptype & MASK_SUBNET) {
            pid = treeitem.parentNode.id;
        }

        if (typeid & MASK_SUBNET) {
            tt = typemap[MASK_SUBNET];
            helpid = 'OrionIPAMPHWindowAddSubnet.htm';
        }
        else if (typeid & MASK_SUPERNET) {
            tt = typemap[MASK_SUPERNET];
            page = "supernet.add.aspx";
            helpid = 'OrionIPAMPHWindowAddSupernet.htm';
        }
        else if (typeid & MASK_ROOT) {
            tt = typemap[MASK_ROOT];
            page = "group.add.aspx";
            helpid = 'OrionIPAMPHWindowAddGroup.htm';
        }
        else {
            tt = typemap[MASK_GROUP];
            page = "group.add.aspx";
            helpid = 'OrionIPAMPHWindowAddGroup.htm';
        }

        $SW.ext.dialogWindowOpen(
            $SW['mainWindow'],
            el.id,
            String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_53;E=js}', tt),
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + treeitem.attributes.id +
            '&ParentId=' + pid + '&ClusterId=' + clusterId + '&TypeId=' + typeid, helpid);
    };

    $SW.subnet.wizard = function () {
        var pid = treeitem.id;
        window.location = '/Orion/IPAM/Subnet.Calculator.aspx?ObjectID=' + pid;
    }

    $SW.subnet.bulkAdd = function () {
        var ptype = parseInt(treeitem.attributes.t, 10);
        var clusterId = treeitem.attributes.ClusterId;
        if (ptype === 1) {
            clusterId = treeitem.attributes.GroupId;
        }

        window.location = '/Orion/IPAM/Subnet.BulkAdd.aspx?ObjectID=' + clusterId;
    }

    $SW.subnet.discoverSubnet = function () {
        var url = '/Orion/IPAM/SubnetDiscovery/Config/AddNodesToDiscover.aspx';

        if ($SW.selectedParent) {
            if($SW.selectedParent["attributes"].ClusterId) {
                url += "?ClusterId=" + $SW.selectedParent["attributes"].ClusterId;
            }
            else if($SW.selectedParent["attributes"].GroupId) {
                url += "?ClusterId=" + $SW.selectedParent["attributes"].GroupId;
            } 
        }

        window.location = url;
    }

    $SW.subnet.importSpreadsheet = function () {
        var pid = treeitem.id;
        window.location = '/Orion/IPAM/Import.upload.intro.aspx?GroupID=' + pid;
    }


    $SW.subnet.scanme = function (el) {
        var fnOK = function () {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_54;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_55;E=js}',
                buttons: Ext.Msg.YESNO,
                animEl: el.id,
                icon: Ext.MessageBox.QUESTION,
                fn: function (btn) { if (btn == 'yes') { window.location = "subnet.scanstatus.aspx"; } }
            });
        };

        var fail = $SW.ext.AjaxMakeFailDelegate('Scan Network');
        var pass = $SW.ext.AjaxMakePassDelegate('Scan Network', fnOK);

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            success: pass,
            failure: fail,
            timeout: fail,
            params:
            {
                entity: 'IPAM.GroupNode',
                verb: 'Scan',
                ids: treeitem.id
            }
        });
    };

    $SW.subnet.ndpdiscovery = function (el) {
        var fail = $SW.ext.AjaxMakeFailDelegate('NDP Scan');

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            success: openNdpDialog,
            failure: fail,
            timeout: fail,
            params:
            {
                entity: 'IPAM.Setting',
                verb: 'Get',
                Type: 'UserSetting',
                Validate: 'NDP',
                setting: 'UI.NDPLastRouters'
            }
        });

        function openNdpDialog(response) {

            var rcvd = Ext.util.JSON.decode(response.responseText);
            if (!rcvd.success) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_7;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_8;E=js}',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.WARNING,
                    animEl: el.id,
                });
                return;
            }

            var routers = rcvd.data.value.replace(/,/g, '\n');

            function handleNDPClick(text) {

                if ($.trim(text).length == 0) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_3;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_17;E=js}',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        animEl: el.id,
                    });
                    return false;
                }

                var invalidIps = '';
                var isValid = false;
                var validIps = '';
                var regX = /\r\n|\r|\n/g;

                Ext.each(text.replace(regX, ',').split(','), function (ip) {

                    if (ip && ip != '' && ip != 'undefined') {

                        isValid = $SW.IP.v4_isok($SW.IP.v4($.trim(ip)));

                        if (isValid == false && $.trim(ip).indexOf(':') > 0)
                            isValid = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/.test($.trim(ip));

                        if (isValid == false) {
                            if (invalidIps == '')
                                invalidIps = $.trim(ip);
                            else
                                invalidIps += ',' + $.trim(ip);
                        } else if (isValid == true) {
                            if (validIps == '')
                                validIps = $.trim(ip);
                            else
                                validIps += ',' + $.trim(ip);
                        }
                    }
                });
                jsonparams = '{ routers:\'' + validIps + '\' }';

                if (invalidIps && $.trim(invalidIps) != '') {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_3;E=js}',
                        msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_SE_54;E=js}', invalidIps),
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        animEl: el.id,
                    });
                    return false;
                }

                isValid = false;

                if ($SW.IsDemoServer) {
                    demoAction('IPAM_NDP_Discovery', null);
                    return true;
                }

                $.ajax({
                    type: "POST",
                    url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                    success: function (result, request) {
                        var res = Ext.util.JSON.decode(result.responseText);
                        if (res.success == true)
                            isValid = true;
                    },
                    failure: fail,
                    complete: function (result, request) {
                        var res = Ext.util.JSON.decode(result.responseText);
                        if (res.success == true)
                            isValid = true;
                    },
                    async: false,
                    data:
                    {
                        entity: 'IPAM.GroupNode',
                        verb: 'NDPScan',
                        ids: treeitem.id,
                        settings: jsonparams,
                    }
                });

                return isValid;
            };

            var fnShowNDPDialog = function () {
                new Ext.Window({
                    modal: true,
                    forcefit: true,
                    region: 'center',
                    width: 536,
                    height: 286,
                    split: false,
                    title: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_3;E=js}',
                    closable: true,
                    closeAction: 'close',
                    resizable: false,
                    id: 'ndpWindow',
                    buttonAlign: 'center',
                    bodyStyle: 'padding: 5px 0px 0px 5px', //(top, right, bottom, left)
                    defaultFocus: 'ndpAddresses',
                    layout: 'fit',
                    items: [new Ext.form.Label({ html: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_4;E=js}' }), new Ext.form.TextArea({ multiline: true, width: 510, height: 110, id: 'ndpAddresses' })],
                    buttons: [
                        {
                            text: '<b>@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_6;E=js}</b>',
                            scale: 'small',
                            listeners: {
                                click: function () {
                                    var isSuccess = handleNDPClick(Ext.getCmp('ndpAddresses').getValue());
                                    if (isSuccess)
                                        Ext.getCmp('ndpWindow').close();
                                }
                            }
                        },
                        {
                            text: '@{R=IPAM.Strings;K=IPAMWEBDATA_MK0_5;E=js}',
                            scale: 'small',
                            listeners: {
                                click: function () {
                                    Ext.getCmp('ndpWindow').close();
                                }
                            }
                        }
                    ],
                    listeners: {
                        'afterrender': function () {
                            var addressBox = Ext.getCmp('ndpAddresses');
                            if (addressBox)
                                addressBox.setValue(routers);
                        }
                    }
                }).show();
            };

            fnShowNDPDialog();
        }
    };

    $SW.subnet.addforip = function (el) {
        var warnFn = function () {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_56;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_57;E=js}',
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
        };
        var singleFn = function (sm) {
            var item = sm.getSelected();
            if (item.data['IPAddress'].indexOf(':') >= 0) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_56;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_58;E=js}',
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.INFO
                });
            } else {
                $SW.ext.dialogWindowOpen(
                    $SW['mainWindow'],
                    el.id,
                    '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_59;E=js}',
                    '/Orion/IPAM/subnet.add.aspx?NoChrome=True&msgq=mainWindow&ClusterID=' + 0 + '&ObjectId=' + 0 + '&ParentId=' + 0 + '&IPAddr=' + item.data['IPAddress'],
                    'OrionIPAMPHWindowAddSubnet.htm');
            }
        };
        $SW.subnet.selected('hiddenSubnet', warnFn, singleFn, warnFn, warnFn);
    };

    $SW.subnet.delcurrent = function (el) {
        if ((treeitem == null || treeitem.id == '0') ||
            (treeitem.childNodes.length > 0 && treeitem.attributes.t == 1)) {
            return;
        }

        var sel = treeitem.getOwnerTree().getSelectionModel().getSelectedNodes() || [];
        var ids = [], header, root, c = sel.length;
        if (c == 1) {
            if (treeitem.id == '0') root = true;
            ids.push(treeitem.id);
            header = String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_60;E=js}", treeitem.attributes.text);
        } else if (c > 1) {
            ids = $.map(sel, function (n, i) {
                if (n.id == '0') root = true;
                return n.id;
            });
            header = String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_61;E=js}", ids.length);
        } else return;

        if (root === true) return;

        var page = "subnet.deleteconfirm.aspx";

        $SW.ext.dialogWindowOpen(
            $SW['deleteConfirm'],
            el.id,
            header,
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=deleteConfirm&ObjectId=' + ids.join('|'),
            'OrionIPAMPHWindowDelete.htm');
    };

    $SW.subnet.delcurrentdone = function (el) {
        var n = treeitem;
        if (n != null) {
            if (n.parentNode && n.parentNode != null) n = n.parentNode;
            window.location = 'Subnets.aspx?opento=' + n.id;
        }
    };

    $SW.subnet.del = function (el) {
        var sel = Ext.getCmp('tabGroup').getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_62;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
        }
        else {
            var items = sel.getSelections();
            var list = [];
            var page = "subnet.deleteconfirm.aspx";

            Ext.each(items, function (o) {
                if (o.id) list.push(o.id);
            });

            $SW.ext.dialogWindowOpen(
                $SW['deleteConfirm'],
                el.id,
                String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_63;E=js}', items.length),
                '/Orion/IPAM/' + page + '?NoChrome=True&msgq=deleteConfirm&ObjectId=' + list.join('|'),
                'OrionIPAMPHWindowDelete.htm');
        }
    };

    $SW.subnet.UiTaskComplete = function (n, op, info, fn) {
        fn = fn || $SW.subnet.RefreshTab;
        if (!info || !info.taskId) return fn();

        var ErrMsgTpl = new Ext.XTemplate(
            '<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />',
            '{message:this.toHtml}',
            '<tpl if="this.hasErrs(results)">',
            '<hr>',
            '<tpl for="results"><tpl if="this.isValid(ErrorCode)">',
            '<br>■ &ensp;{ErrorMessage}',
            '</tpl></tpl>',
            '</tpl>',
            {
                toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
                hasErrs: function (rs) { return (rs && rs.length > 0); },
                isValid: function (ec) { return (ec != undefined && ec != 0); }
            }
        );

        $SW.Tasks.Attach(info.taskId, { title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_64;E=js}" }, function (d) {
            if (d.success) {
                if (d.results && d.results.LicenseInfo) {
                    var licInfo = Ext.util.JSON.decode(d.results.LicenseInfo);
                    $SW.LicenseListeners.recv(licInfo);
                }
                fn();
            }
            else {
                if (d.state && d.state != 4)
                    Ext.MessageBox.show({
                        title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_65;E=js}",
                        msg: ErrMsgTpl.apply(d),
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        fn: function () { $SW.subnet.RefreshTab(true); }
                    });
            }
        }, fn);
    };

    $SW.subnet.EditManyComplete = function (info) {


        if (info) {
            //Using UiTask to save async
            var taskId = info.taskId;
            var title = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_66;E=js}";

            $SW.Tasks.Attach(taskId, { title: title }, function (d) {
                if (d.success) {
                    if (d.results) {
                        if (d.results.LicenseInfo) {
                            var licInfo = Ext.util.JSON.decode(d.results.LicenseInfo);
                            $SW.LicenseListeners.recv(licInfo);
                        }

                        if (d.results.IpRejectedCount != 0) {

                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_67;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_68;E=js}',
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        }

                        $SW.subnet.RefreshGridPage();
                    } else {
                        $SW.ClearGridSelection(Ext.getCmp('tabSubnet'));
                        $SW.subnet.RefreshTab(true);
                    }
                }
                else {
                    if (d.state && d.state != 4)
                        Ext.MessageBox.show({
                            title: title,
                            msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () { $SW.subnet.RefreshTab(true); }
                        });
                }
            },
                function () { $SW.subnet.RefreshTab(true); }
            );

        }
        else { //sync save already finished
            $SW.subnet.RefreshGridPage();
        }
    }

    $SW.subnet.displayMultiEditWarning = function (availableCount, usedCount, fnUnselectAvailable, fnUnselectUsed) {

        var warning = String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_69;E=js}", availableCount, usedCount);
        var warningTitle = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_70;E=js}';
        var confirmText = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_71;E=js}';
        var unselectUsedText = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_72;E=js}';
        var cancelText = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_73;E=js}'

        Ext.Msg.show({
            title: warningTitle,
            msg: warning,
            buttons: { ok: confirmText, no: unselectUsedText, cancel: cancelText },
            width: 400,
            icon: Ext.MessageBox.QUESTION,
            fn: function (btn) {
                if (btn == 'ok') {
                    fnUnselectAvailable();
                    return true;
                }
                else if (btn == 'no') {
                    //unselect used IPs
                    fnUnselectUsed();
                    return true;
                }
            }
        });
    }

    $SW.subnet.editip = function (el, unselectAvailableRows, unselectUsedRows) {
        var sel = Ext.getCmp('tabSubnet').getSelectionModel();

        //Happens if this calls itself recursive way
        if (unselectAvailableRows || unselectUsedRows) {
            var items = sel.getSelections();
            var itemsToSelect = [];

            sel.clearSelections();
            Ext.each(items, function (o) {
                if (o.id) {
                    var isAvailable = o.data.Status == '2';
                    if (unselectAvailableRows && !isAvailable
                        || unselectUsedRows && isAvailable) {
                        itemsToSelect.push(o);
                    }
                }
            });

            if (itemsToSelect.length != 0)
                sel.selectRecords(itemsToSelect);
        }

        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_51;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        }
        else if (c > 1) {
            var items = sel.getSelections();
            var list = [];

            var availableCount = 0;
            var usedCount = 0;
            var containsUsed = false;
            Ext.each(items, function (o) {
                if (o.id) {
                    var isAvailable = o.data.Status == '2';
                    if (!isAvailable) {
                        containsUsed = true;
                        usedCount++;
                    }
                    else {
                        availableCount++;
                    }

                    list.push(o.data.IpNodeId);
                }
            });

            if (containsUsed ^ (availableCount != 0)) {

                var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_74;E=js}';
                var page = "Ip.MultiEdit.aspx";
                var win = $SW['multiEditWindow'];
                var helpUrl = 'OrionIPAMPHWindowMultipleEditIPAddress.htm';
                var paramAvailable = availableCount != 0 ? "True" : "False";


                var settingsContainerName = 'MultiEditSettings';
                var pageUrl = '/Orion/IPAM/' + page + '?NoChrome=True&msgq=multiEditWindow&SubnetId=' + treeitem.attributes.id + '&AvailableIPs=' + paramAvailable;
                var dialogUrl = '/Orion/IPAM/DialogWindowLoader.aspx?DataContainer=' + settingsContainerName;

                var parameterName = "ObjectID";
                var data = list.join('|');

                var settings = new Object();
                settings.data = data;
                settings.parameterName = parameterName;
                settings.url = pageUrl;

                if (!window.ipam)
                    window['ipam'] = {};
                window.ipam[settingsContainerName] = settings;

                $SW.ext.dialogWindowOpen(
                    win,
                    el.id,
                    title,
                    dialogUrl,
                    helpUrl);
            }
            else {

                $SW.subnet.displayMultiEditWarning(availableCount, usedCount,
                    function () {
                        //unselect available IPs so we don't confuse user
                        $SW.subnet.editip(el, true, false);
                    },
                    function () {
                        //unselect used IPs 
                        $SW.subnet.editip(el, false, true);
                    });
            }
        }
        else {
            var items = sel.getSelections();
            var page = "ip.edit.aspx";
            var win = $SW['mainWindow'];

            /*
            var w = $(window).width();
            if( w > 800 )
            {
            if( !win.rendered ) win.width = w-42;
            else
            {
            var winb = win.getBox();
            if( winb.width < 800 )
            {
            win.setWidth( w-42 );
            win.center();
            }
            }
            }
            */

            win.width = 800;
            win.height = 550;

            $SW.ext.dialogWindowOpen(
                win,
                el.id,
                '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_75;E=js}',
                '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&SubnetId=' + treeitem.attributes.id + '&nodeId=' + items[0].data["IpNodeId"] + '&ipOrdinal=' + items[0].data.IPOrdinal,
                'OrionIPAMPHWindowEditIPAddress.htm');
        }
    };


    $SW.subnet.ipdetails = function (el) {
        var pid = treeitem.attributes.id;
        var sel = Ext.getCmp('tabSubnet').getSelectionModel();
        var c = sel.getCount();
        if (c != 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_76;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        } else {
            var items = sel.getSelections();
            var oid = items[0].data.IpNodeId;
            window.location = 'IPAMAddressDetailsView.aspx?NetObject=IPAMN:' + oid;
        }
    };

    $SW.subnet.addiprange = function (el) {
        var page = "ip.addrange.aspx";

        $SW.ext.dialogWindowOpen(
            $SW['mainWindow'],
            el.id,
            '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_77;E=js}',
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectID=' + treeitem.attributes.id,
            'OrionIPAMPHWindowAddIPAddressRange.htm');
    };

    $SW.subnet.addiprangecomplete = function (data) {
        $SW['mainWindow'].hide();
        var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_77;E=js}';

        $SW.Tasks.DoProgress('AddIPRange', data, {
            title: title,
            buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
        }, function (d) {
            if (d.success)
                $SW.subnet.RefreshGridPage();
            else {
                if (d.state == 4 /*UITaskState.Canceled*/) {
                    Ext.MessageBox.show({
                        title: title,
                        msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_79;E=js}",
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        fn: function () { $SW.subnet.RefreshTab(true); }
                    });
                }
                else {
                    Ext.MessageBox.show({
                        title: title,
                        msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        fn: function () { $SW.subnet.RefreshTab(true); }
                    });
                }
            }
        },
            function () { $SW.subnet.RefreshTab(true); }
        );
    };

    $SW.subnet.deletecomplete = function (data) {

        $SW['deleteConfirm'].hide();
        var t = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_80;E=js}';

        $SW.Tasks.DoProgress('DeleteGroups', data, {
            title: t,
            buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
        }, function (d) {
            if (d.success)
                $SW.subnet.delcurrentdone();
            else {
                if (d.state == 4 /*UITaskState.Canceled*/) {
                    Ext.MessageBox.show({
                        title: t,
                        msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_81;E=js}",
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        fn: function () { $SW.subnet.RefreshTab(true); }
                    });
                }
                else {
                    Ext.MessageBox.show({
                        title: t,
                        msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        fn: function () { $SW.subnet.RefreshTab(true); }
                    });
                }
            }
        }, function () { $SW.subnet.RefreshTab(true); });
    };

    $SW.subnet.selectIpRangeForEditRemove = function (el) {

        var page = "Ip.SelectRange.aspx";

        $SW.ext.dialogWindowOpen(
            $SW['selectRangeWindow'],
            el.id,
            '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_82;E=js}',
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=selectRangeWindow&ObjectID=' + treeitem.attributes.id,
            'OrionIPAMPHWindowEditIPAddressRange.htm');
    };

    $SW.subnet.multiEditDialogForRange = function (subnetId, rangeStart, rangeEnd, paramAvailable) {

        var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_74;E=js}';
        var page = "Ip.MultiEdit.aspx";
        var win = $SW['multiEditWindow'];
        var helpUrl = 'OrionIPAMPHWindowMultipleEditIPAddress.htm';

        var pageUrl = '/Orion/IPAM/' + page + '?NoChrome=True&msgq=multiEditWindow&SubnetId=' + subnetId + '&RangeStart=' + rangeStart + '&RangeEnd=' + rangeEnd + '&AvailableIPs=' + paramAvailable;

        $SW.ext.dialogWindowOpen(
            win,
            null,
            title,
            pageUrl,
            helpUrl);

    }

    $SW.subnet.selectIpRangeForEditRemoveComplete = function (data) {

        $SW['selectRangeWindow'].hide();

        var subnetId = data.rangeData.subnetid;
        var rangeStart = data.rangeData.ipstart;
        var rangeEnd = data.rangeData.ipend;
        var actionName = data.actionData.actionName;

        if (actionName == 'edit') {

            if (rangeStart == rangeEnd) {
                //Single IP edit
                var page = "ip.edit.aspx";
                var win = $SW['mainWindow'];

                $SW.ext.dialogWindowOpen(
                    win,
                    null,
                    '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_75;E=js}',
                    '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&SubnetId=' + subnetId + '&ip=' + rangeStart,
                    'OrionIPAMPHWindowEditIPAddress.htm');
            }
            else {

                var ipAvailableCount = data.actionData.availableIpCount;
                var ipUsedCount = data.actionData.usedIpCount;

                if (ipAvailableCount == 0 || ipUsedCount == 0) {
                    var paramAvailable = ipAvailableCount != 0 ? "True" : "False";
                    $SW.subnet.multiEditDialogForRange(subnetId, rangeStart, rangeEnd, paramAvailable);
                }
                else {

                    $SW.subnet.displayMultiEditWarning(ipAvailableCount, ipUsedCount,
                        function () {
                            //unselect available IPs so we don't confuse user
                            $SW.subnet.multiEditDialogForRange(subnetId, rangeStart, rangeEnd, 'False');
                        },
                        function () {
                            //unselect used IPs
                            $SW.subnet.multiEditDialogForRange(subnetId, rangeStart, rangeEnd, 'True');
                        });

                }
            }

        }
        else if (actionName == 'remove') {

            var taskData = { ipstart: rangeStart, ipend: rangeEnd, subnetid: subnetId };
            var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_83;E=js}';

            var deleteDelegate = function () {
                $SW.Tasks.DoProgress('DelIPRange', taskData, {
                    title: title,
                    buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
                }, function (d) {
                    if (d.success)
                        $SW.subnet.RefreshGridPage();
                    else {
                        if (d.state == 4 /*UITaskState.Canceled*/) {
                            Ext.MessageBox.show({
                                title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_83;E=js}",
                                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_81;E=js}",
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO,
                                fn: function () { $SW.subnet.RefreshTab(true); }
                            });
                        }
                        else {
                            Ext.MessageBox.show({
                                title: title,
                                msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR,
                                fn: function () { $SW.subnet.RefreshTab(true); }
                            });
                        }
                    }
                },
                    function () { $SW.subnet.RefreshTab(true); }
                );
            };

            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_84;E=js}', rangeStart, rangeEnd),
                buttons: Ext.Msg.YESNO,
                icon: Ext.MessageBox.QUESTION,
                fn: function (btn) {
                    if (btn == 'yes') {
                        $SW.ClearGridSelection(Ext.getCmp('tabSubnet'));
                        deleteDelegate();
                    }
                }
            });
        }
    };


    $SW.subnet.delipfromorphaned = function (el) {
        var btn = el;
        var noneFn = function () {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_85;E=js}',
                buttons: Ext.Msg.OK,
                animEl: btn.id,
                icon: Ext.MessageBox.ERROR
            });
        };
        var selectFn = function (sm) {
            var ids = [];
            Ext.each(sm.getSelections(), function (o) {
                ids.push(o.id);
            });

            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_86;E=js}', ids.length),
                buttons: Ext.Msg.YESNO,
                animEl: btn.id,
                icon: Ext.MessageBox.QUESTION,
                fn: function (b) { if (b == 'yes') $SW.subnet.DeleteIPsById(ids, 1, $SW.subnet.RefreshHidden); }
            });
        };
        var allFn = function (grid) {
            var count = grid.store.totalLength;
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_86;E=js}', count),
                buttons: Ext.Msg.YESNO,
                animEl: btn.id,
                icon: Ext.MessageBox.QUESTION,
                fn: function (b) { if (b == 'yes') $SW.subnet.DeleteAllOrphanedIPs($SW.subnet.RefreshHidden); }
            });
        };
        $SW.subnet.selected('hiddenSubnet', noneFn, selectFn, selectFn, allFn);
    };

    //Manage & Unmanage ip stuff

    $SW.subnet.unmanageip = function (el) {
        if ($SW.IsDemoServer) {
            demoAction('IPAM_Subnet_IP_Delete', null);
            return false;
        }
        var sel = Ext.getCmp('tabSubnet').getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_62;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
        } else {
            var items = sel.getSelections();
            var list = [];
            var subnetid = treeitem.attributes.id;

            Ext.each(items, function (o) {
                if (o.id) list.push(o.id);
            });

            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_86;E=js}', list.length),
                buttons: Ext.Msg.YESNO,
                animEl: el.id,
                icon: Ext.MessageBox.QUESTION,
                fn: function (btn) {
                    if (btn == 'yes') {
                        $SW.ClearGridSelection(Ext.getCmp('tabSubnet'));
                        DeleteIPs(list, subnetid, $SW.subnet.RefreshGridPage);
                    }
                }
            });
        }
    };

    var DeleteIPs = function (ipords, subnetid, successdelegate) {

        var fail = $SW.ext.AjaxMakeFailDelegate('Delete IP addresses');
        var pass = $SW.ext.AjaxMakePassDelegate('Delete IP addresses', successdelegate);

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText);
                if (res.success == true && res.license && res.license.app == "IPAM") {
                    $SW.LicenseListeners.recv(res.license);
                }
                pass(result, request);
            },
            failure: fail,
            timeout: fail,

            params:
            {
                entity: 'IPAM.IPNode',
                verb: 'DeleteMany',
                ids: ipords.join(','),
                ObjectId: subnetid
            }
        });
    };

    $SW.subnet.DeleteIPs = function (successFn, opt) {
        var params = Ext.apply({ entity: 'IPAM.IPNode' }, opt);
        var fail = $SW.ext.AjaxMakeFailDelegate('Delete IP addresses');
        var pass = $SW.ext.AjaxMakePassDelegate('Delete IP addresses', successFn);

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText);
                if (res.success == true && res.license && res.license.app == "IPAM") {
                    $SW.LicenseListeners.recv(res.license);
                }
                pass(result, request);
            },
            failure: fail,
            timeout: fail,
            params: params
        });
    };
    $SW.subnet.DeleteAllOrphanedIPs = function (successFn) {
        $SW.subnet.DeleteIPs(successFn, {
            entity: 'IPAM.IPNode',
            verb: 'DeleteAllOrphans'
        });
    };
    $SW.subnet.DeleteIPsById = function (ids, subnetid, successFn) {
        $SW.subnet.DeleteIPs(successFn, {
            entity: 'IPAM.IPNode',
            verb: 'DeleteManyById',
            ids: ids.join(','),
            ObjectId: subnetid
        });
    };

    function RedirectManageSubnet() {
        window.location.href = "/Orion/IPAM/Subnets.aspx";
    };

    $SW.subnet.DoDiscoverySubnetImport = function (data) {
        $SW.Tasks.DoProgress('ImportSubnets', data, {
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_52;E=js}',
            buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}' }
        },
            function (d) {
                if (d.success)
                    RedirectManageSubnet();
                else {
                    if (d.state == 4 /*UITaskState.Canceled*/) {
                        RedirectManageSubnet();
                    }
                    else {
                        Ext.MessageBox.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_52;E=js}',
                            msg: (d.message && d.message.length > 0) ? Ext.util.Format.htmlEncode(d.message) : '@{R=IPAM.Strings;K=IPAMWEBJS_SE_53;E=js}',
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () { RedirectManageSubnet(); }
                        });
                    }
                }
            },
            function () {
                RedirectManageSubnet();
            }
        );
    };

    $SW.subnet.AssignNsId = function (nsId) {
        $nsId = nsId;
    }
    $SW.subnet.setstatus = function (el, status, isIPv6SetStatus) {
        var that = el;
        var status_v = ipstatusmap_txt[status];
        var gridTabId = (!isIPv6SetStatus) ? 'tabSubnet' : 'tabIPv6Subnet';

        if (typeof (status_v) == 'undefined') {
            Ext.Msg.show({
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_87;E=js}', status),
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.ERROR
            });
            return;
        }

        if ($SW.IsDemoServer && (status_v == 1 || status_v == 2 || status_v == 8)) {
            demoAction('IPAM_Subnet_SetIPStatus', null);
            return false;
        }

        var sel = Ext.getCmp(gridTabId).getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_88;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        }

        var items = sel.getSelections();
        var nodeIds = [], reservations = [], ordinals = [];
        var someIpIsReserved = false;

        Ext.each(items, function (o) {
            if (o.data) {
                var d = {
                    ordinal: o.data["IPOrdinal"] || '',
                    IpNodeId: o.data["IpNodeId"] || '',
                    IpAddress: o.data["IPAddress"] || '',
                    Name: o.data["DhcpClientName"] || '',
                    Description: o.data["Comments"] || '',
                    DhcpLease: o.data["DhcpLease"] || '',
                    Status: o.data["Status"] || '',
                    Mac: o.data["MAC"] || ''
                };
                if (d.ordinal != '') ordinals.push(d.ordinal);
                if (d.IpNodeId != '') nodeIds.push(d.IpNodeId);
                if (d.IpNodeId != '') reservations.push(d);
                if ((d.DhcpLease == '2') || (d.Status == '4'))
                    someIpIsReserved = true;

                //if (d.IpAddress != '' && (d.IpAddress).indexOf(':') > 0)
                //    isIPv6SetStatus = true;
            }
        });

        var ajaxVerb = (!isIPv6SetStatus) ? 'UpdateManyStatus' : 'UpdateManyStatusById';

        $SW.subnet.ConfirmDhcpReservation(status_v, nodeIds, reservations, someIpIsReserved, function (dhcpResFn) {
            return setStatusInternal(status_v, (!isIPv6SetStatus) ? ordinals : nodeIds, dhcpResFn, gridTabId, ajaxVerb);
        });
    };

    function setStatusInternal(status_v, list, dhcpResFn, gridTabId, ajaxVerb) {
        var ret = null;

        function updatestatus(result) {
            // for now, just refresh the page.
            $SW.ClearGridSelection(Ext.getCmp(gridTabId));
            $SW.subnet.RefreshGridPage();

            if (!result)
                return;

            if (result.license && result.license.app == "IPAM")
                $SW.LicenseListeners.recv(result.license);

            if (result.data && result.data.rejected && result.data.rejected == true)
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_67;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_68;E=js}',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            else if (dhcpResFn) dhcpResFn();
        }

        function startupdatestatus(fnPass, ti, newvalue, idlist) {
            var fail = $SW.ext.AjaxMakeFailDelegate('Change Status');
            var pass = $SW.ext.AjaxMakePassDelegate('Change Status', updatestatus);

            Ext.Ajax.request({

                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

                success: pass,
                failure: fail,
                timeout: fail,

                params:
                {
                    entity: 'IPAM.IPNode',
                    verb: ajaxVerb,
                    ObjectId: ti.id,
                    ipstatus: newvalue,
                    ids: idlist.join(','),
                    _dc: (new Date()).getTime()
                }
            });
        }

        if (status_v == 2) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_89;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_90;E=js}',
                buttons: Ext.Msg.YESNO,
                width: 400,
                icon: Ext.MessageBox.QUESTION,
                fn: function (btn) { if (btn == 'yes') { startupdatestatus(updatestatus, treeitem, status_v, list); } }
            });

            return true;
        }

        startupdatestatus(updatestatus, treeitem, status_v, list);
        return true;
    };

    $SW.subnet.ReservTpl = new Ext.XTemplate(
        '<b>{title}</b>',
        '<br />',
        '{msg:this.toHtml}',
        '<tpl if="this.hasDetails(data)">',
        '<ul style="list-style:square inside; max-height: 250px; overflow:auto;">',
        '<tpl for="data"><tpl if="this.isValid(ErrorCode)">',
        '<li>IP: <u>{Key}</u> - <i>{Method}</i> {ErrorMessage}</li>',
        '</tpl></tpl>',
        '</ul>',
        '</tpl>',
        {
            toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
            hasDetails: function (d) { return (d && d.length > 0); },
            isValid: function (ec) { return (ec != undefined && ec != 0); }
        }
    );

    $SW.subnet.ConfirmDhcpReservation = function (status_v, nodeIds, items, someIpIsReserved, fn) {
        if (!items || items.length < 1)
            return fn();

        fn = fn || function () { };
        var mbId = Ext.id();

        var confirmDialog = function (d) {
            if ($SW.IsDemoServer && d.data.length < 1) {
                demoAction('IPAM_Subnet_SetIPStatus', null);
                return false;
            }
            if (!d || !d.data) return fn();

            var cbStore = new Ext.data.JsonStore({
                data: d.data,
                fields: [
                    { name: 'FriendlyName', type: 'string' },
                    { name: 'GroupId', type: 'integer' }
                ]
            });

            var fnYesCreate = function () {
                var serverGroupId = $('[name=' + mbId + '-val]').val();
                var type = $('input[name=dhcpResType]:checked').val();
                var itemsWithoutEmptyMac = [];

                // Ignoring empty MAC/Name/ records
                var ignoredIps = [];
                Ext.each(items, function (o) {
                    if (o.Name != '' && o.Mac != '') itemsWithoutEmptyMac.push(o);
                    else ignoredIps.push(o.IpAddress);
                });
                if (itemsWithoutEmptyMac.length > 0) {
                    var createDhcpRes = function () {
                        // UiTask for create reservation(groupId, type, itemsWithoutEmptyMac)

                        var data = { ServerId: parseInt(serverGroupId), Type: parseInt(type), Items: itemsWithoutEmptyMac };
                        var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_91;E=js}';
                        $SW.Tasks.DoProgress('CreateDhcpReservation', data, { title: title }, function (d) {
                            if (d.success) {
                                Ext.Msg.show({
                                    title: title,
                                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_92;E=js}',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.INFO,
                                    minWidth: 400,
                                    fn: function () {
                                        $SW.subnet.RefreshGridPage();
                                    }
                                });
                            }
                            else Ext.MessageBox.show({
                                title: title,
                                msg: $SW.subnet.ReservTpl.apply({
                                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_93;E=js}',
                                    msg: d.message,
                                    data: d.results
                                }),
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR,
                                minWidth: 400,
                                fn: $SW.subnet.RefreshGridPage
                            });
                        },
                            null /* do not show cancel button */
                        );

                    };
                    var warnSkippingIps = function () {
                        Ext.MessageBox.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_94;E=js}',
                            msg: getFmtSkippingMsg(ignoredIps, itemsWithoutEmptyMac),
                            buttons: Ext.MessageBox.YESNO,
                            icon: Ext.MessageBox.WARNING,
                            minWidth: 400,
                            fn: function (btn) {
                                if (btn == 'yes')
                                    createDhcpRes();
                                else $SW.subnet.RefreshGridPage();
                            }
                        })
                    };
                    var createDhcpResInner = createDhcpRes;
                    if (itemsWithoutEmptyMac.length != items.length)
                        createDhcpResInner = warnSkippingIps;

                    fn(createDhcpResInner);

                } else {
                    fn(function () {
                        Ext.MessageBox.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_94;E=js}',
                            msg: getFmtSkippingMsg(ignoredIps),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.WARNING,
                            minWidth: 400,
                            fn: $SW.subnet.RefreshGridPage
                        });
                    });
                }
            };
            var fnYesRemove = function () {
                var serverGroupId = $('[name=' + mbId + '-val]').val();
                var type = 0; // Unknown
                fn(function () {
                    // UiTask for remove reservation(groupId, items)

                    var data = { ServerId: parseInt(serverGroupId), Type: parseInt(type), Items: items };
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_95;E=js}';
                    $SW.Tasks.DoProgress('RemoveDhcpReservation', data, { title: title }, function (d) {
                        if (d.success) {
                            Ext.Msg.show({
                                title: title,
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_96;E=js}',
                                buttons: Ext.Msg.OK,
                                minWidth: 400,
                                icon: Ext.MessageBox.INFO,
                                fn: function () {
                                    $SW.subnet.RefreshGridPage();
                                }
                            });
                        }
                        else Ext.MessageBox.show({
                            title: title,
                            msg: $SW.subnet.ReservTpl.apply({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_97;E=js}',
                                msg: d.message,
                                data: d.results
                            }),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            minWidth: 400,
                            fn: $SW.subnet.RefreshGridPage
                        });
                    },
                        null /* do not show cancel button */
                    );

                });
            };
            if (status_v == 4) {
                var nodeId = nodeIds[0];
                var fail = $SW.ext.AjaxMakeFailDelegate('Get Server Type');
                var serverType;
                Ext.Ajax.request({
                    url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                    timeout: fail, failure: fail,
                    success: function (result) {
                        var res = Ext.util.JSON.decode(result.responseText);
                        serverType = res.data.ServerType;
                        CreateDhcpReservationMsgBox(mbId, cbStore, fnYesCreate, fn, serverType);
                    },
                    params: { entity: 'IPAM.DhcpServer', verb: 'GetDhcpServerType', nodeId: nodeId }
                });

            }
            else if (someIpIsReserved) {
                RemoveDhcpReservationMsgBox(mbId, cbStore, fnYesRemove, fn);
            }
            else {
                fn();
            }

            return true;
        };

        var fail = $SW.ext.AjaxMakeFailDelegate('Getting DHCP Servers', fn);
        var pass = $SW.ext.AjaxMakePassDelegate('Getting DHCP Servers', confirmDialog);
        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            timeout: fail, failure: fail, success: pass,
            params: { entity: 'IPAM.DhcpServer', verb: 'GetServersByIpNodes', items: nodeIds }
        });

        return true;
    };

    function getFmtSkippingMsg(ignoredIps, filledIps) {
        var dStr = '';
        if (ignoredIps) {
            Ext.each(ignoredIps, function (d) {
                dStr += '<li>' + d + '</li>';
            });
        }
        if (ignoredIps.length > 1)
            dStr = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_98;E=js}', '<br/><ul style="list-style:square inside; height:100px; overflow:auto">', dStr, '</ul>');
        else
            dStr = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_99;E=js}', '<br/><ul style="list-style:square inside; height:100px; overflow:auto">', dStr, '</ul>');

        dStr += '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_100;E=js}';

        var filledStr = ''
        if (filledIps && filledIps.length > 0) {
            Ext.each(filledIps, function (d) {
                filledStr += '<li>' + d.IpAddress + '</li>';
            });
            if (filledIps.length > 1)
                filledStr = '<br/><br/>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_101;E=js} <ul style="list-style:square inside">' + filledStr + '</ul>';
            else
                filledStr = '<br/><br/>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_102;E=js} <ul style="list-style:square inside">' + filledStr + '</ul>';

            dStr = dStr + filledStr;
        }
        return dStr;
    }

    function CreateDhcpReservationMsgBox(mbId, cbStore, fnYes, fnNo, serverType) {
        fnYes = fnYes || function () { };
        fnNo = fnNo || function () { };
        var fields = { display: 'FriendlyName', value: 'GroupId' };
        var tpl = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';
        var url = String.format(tpl, $SW.HelpServer || '/', 'OrionIPAMPHDHCPReservations');

        var btns = { ok: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_111;E=js}', yes: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_112;E=js}' };
        if (serverType == 5) {
            var disableInfobloxReservation = function (msg) {
                Ext.iterate(msg.buttons, function (b) {
                    if (b && b['text'] == btns.ok) {
                        b.setDisabled(true);
                    }
                });
                $("#" + mbId + "-dhcponly").prop("disabled", true).prop("checked", false);
                $("#" + mbId + "-bootponly").prop("disabled", true);
                $("#" + mbId + "-both").prop("disabled", true);
            };
            Ext.MessageBox.getDialog().on({ show: disableInfobloxReservation, single: true, delay: 1 });
        }
        if (Ext.isIE) {
            var showFn = function (msg) {
                Ext.iterate(msg.buttons, function (b) {
                    if (b && b['text'] == btns.yes)
                        b.getEl().child('button').setWidth(170);
                });
            };
            Ext.MessageBox.getDialog().on({ show: showFn, single: true, delay: 1 });
        }

        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_103;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_104;E=js}'
                + '<br/><br/><span>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_105;E=js}</span>'
                + '<br/><div ><input type=\'radio\' id="' + mbId + '-dhcponly" checked=\'true\' name="dhcpResType" value="1" style="vertical-align:middle"/><label for="' + mbId + '-dhcponly" style="margin-left:2px; margin-right:10px">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_106;E=js}</label>'
                + '<input type=\'radio\' id="' + mbId + '-bootponly" name="dhcpResType" value="2" style="vertical-align:middle"/><label for="' + mbId + '-bootponly" style="margin-left:2px; margin-right:10px">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_107;E=js}</label>'
                + '<input type=\'radio\' id="' + mbId + '-both" name="dhcpResType" value="3" style="vertical-align:middle"/><label for="' + mbId + '-both" style="margin-left:2px; margin-right:10px">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_108;E=js}</label>'
                + '</div><table><tr><td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_109;E=js}</td>'
                + '<td><input type="text" id="' + mbId + '-cb" /></td></tr></table>'
                + '<br/><a href="' + url + '" target="_blank"><span style=\'color:blue\'>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_110;E=js}</span></a>'
            ,
            buttons: btns,
            width: 400,
            icon: Ext.MessageBox.QUESTION,
            fn: function (btn) {
                if ($SW.IsDemoServer) {
                    demoAction('IPAM_Subnet_SetIPStatus', null);
                    return false;
                }
                if (btn == 'cancel')
                    return;
                if (btn == 'ok')
                    fnYes();
                else fnNo();
            }
        });

        var dhcpSrvCmb = new Ext.form.ComboBox({
            applyTo: mbId + '-cb',
            store: cbStore,
            displayField: fields.display,
            valueField: fields.value,
            mode: 'local',
            forceSelection: true,
            typeAhead: false,
            triggerAction: 'all',
            editable: false,
            hiddenName: mbId + '-val',
            listeners: {
                afterrender: function (cb) {

                    var s = cb.getStore(), c = s.getCount();
                    if (c > 0) {
                        if (c <= 1) cb.disable();
                        cb.setValue(s.getAt(0).get(fields.value));
                    }
                }
            }
        });

        return true;
    }

    function RemoveDhcpReservationMsgBox(mbId, cbStore, fnYes, fnNo) {
        fnYes = fnYes || function () { };
        fnNo = fnNo || function () { };
        var fields = { display: 'FriendlyName', value: 'GroupId' };
        var tpl = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';
        var url = String.format(tpl, $SW.HelpServer || '/', 'OrionIPAMPHDHCPReservations');

        var btns = { ok: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_113;E=js}', yes: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_114;E=js}' };
        if (Ext.isIE) {
            var showFn = function (msg) {
                Ext.iterate(msg.buttons, function (b) {
                    if (b && b['text'] == btns.yes)
                        b.getEl().child('button').setWidth(170);
                });
            };
            Ext.MessageBox.getDialog().on({ show: showFn, single: true, delay: 1 });
        }

        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_115;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_116;E=js}'
                + '<br/><br/>'
                + '<table><tr><td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_109;E=js}</td>'
                + '<td><input type="text" id="' + mbId + '-cb" /></td></tr></table>'
                + '<br/><a href="' + url + '" target="_blank"><span style=\'color:blue\'>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_110;E=js}</span></a>',
            buttons: btns,
            width: 400,
            icon: Ext.MessageBox.QUESTION,
            fn: function (btn) {
                if (btn == 'cancel')
                    return;
                if (btn == 'ok') fnYes();
                else fnNo();
            }
        });

        var dhcpSrvCmb = new Ext.form.ComboBox({
            applyTo: mbId + '-cb',
            store: cbStore,
            displayField: fields.display,
            valueField: fields.value,
            mode: 'local',
            forceSelection: true,
            typeAhead: false,
            triggerAction: 'all',
            editable: false,
            hiddenName: mbId + '-val',
            listeners: {
                afterrender: function (cb) {

                    var s = cb.getStore(), c = s.getCount();
                    if (c > 0) {
                        if (c <= 1) cb.disable();
                        cb.setValue(s.getAt(0).get(fields.value));
                    }
                }
            }
        });

        return true;
    }

    //
    // tree rendering
    //

    var statusTxtMap = { '0': '', '1': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Used;E=js}', '2': '@{R=IPAM.Strings;K=Entity_IPAM_DhcpServer_AvailableCount;E=js}', '4': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Reserved;E=js}', '5': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Reserved;E=js}', '8': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Transient;E=js}', '16': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_APIBlocked;E=js}', 'history': '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_117;E=js}', 'IsConflict': '@{R=IPAM.Strings;K=IPAMWEBJS_DF1_23;E=js}' };
    var statusIcoMap = { '0': '', '1': 'ip-used', '2': 'ip-avail', '4': 'ip-availrsvd', '5': 'ip-usedrsvd', '8': 'ip-transient', '16': 'ip-blocked', 'history': 'ip-history' };

    $SW.subnet.grpStatusImgRenderer = function () {
        return function (value, meta, record) {
            if (value) {
                var d = record.data, pre = d.GroupIconPrefix, post = d.StatusIconPostfix, tip = d.StatusShortDescription;

                // todo: remove this map (up->ok)
                if (post == 'up') post = 'ok';

                meta.css = 'status-bg ' + pre + '-' + post;
                meta.attr = 'qtip="' + tip + '"';
            }

            return "";
        };
    };

    $SW.subnet.ipStatusImgRenderer = function () {
        return function (value, meta, r) {
            if (r.data["IsHistory"] == '1') { value = 'history'; }
            else if (r.data["IsConflict"] == '1') {
                meta.style += "background-image: url(res/images/sw/" + r.data["ConflictTypeIcon"] + ") !important; background-repeat:no-repeat ; background-position: center center; ";
                return "";
            }

            if (value) {
                var cssClass = statusIcoMap[value] || null;
                var qtip = statusTxtMap[value] || null;
                if (cssClass) meta.css = cssClass;
                if (qtip) meta.attr = 'qtip="' + qtip + '"';
            }

            return "";
        };
    };

    $SW.subnet.iconImgRenderer = function (that) {
        return function (value, meta, r) {
            var displayText = "";
            if (value) {
                if (r.data.VendorIcon && r.data.VendorIcon != '') {
                    meta.style += "background-image:url(../../../../NetPerfmon/images/Vendors/" + r.data.VendorIcon + ") !important; background-repeat:no-repeat";
                    displayText = "&emsp;";
                }
                displayText = displayText + value;
            }
            return displayText;
        };
    };

    $SW.subnet.ipTypeRenderer = function () {
        return function (value, meta) {
            var ret = "";
            if (value) {
                if ((value & 4) == 0) ret = "@{R=IPAM.Strings;K=Enum_IPAllocPolicy_Static;E=js}";
                else ret = "@{R=IPAM.Strings;K=Enum_IPAllocPolicy_Dynamic;E=js}";
            }

            return ret;
        };
    };

    $SW.subnet.ipScopeNameRenderer = function () {
        return function (value, meta, r) {
            var ret = "", d = r.data;
            if (value) {
                if (d && d.ScopeId)
                    ret = "<a class='sw-grid-click' href='../../api2/ipam/ui/scope?name=" + encodeURIComponent(d.ScopeName) + "'>" +
                        Ext.util.Format.htmlEncode(d.ScopeName) + "</a>";
                else ret = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_118;E=js}";
            }

            return ret;
        };
    };

    $SW.subnet.DhcpScopesRenderer = function () {
        var ttipTpl = new Ext.XTemplate(
            '<table cellspacing="3" cellpadding="0"  style="width:180px">',
            '<tr><th colspan="5">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_21;E=js}</th></tr>',
            '<tr>',
            '<td colspan="5"><div class="dhcpserver-{svst:this.mapUpToOk} scope-list-server">{sv}</div></td>',
            '</tr>',
            '<tr><th colspan="5" style="padding-top: 8px;">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_18;E=js}</th></tr>',
            '<tpl if="this.isEmpty(rang)"><tr><td colspan="5" class="r-no-ranges-hint">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_22;E=js}</td></tr></tpl>',
            '<tpl for="rang"><tr>',
            '<td>&nbsp;</td>',
            '<td class="start">{start}</td>',
            '<td>&nbsp;to&nbsp;</td>',
            '<td class="end">{end}</td>',
            '<td style="width:100%">&nbsp;</td>',
            '</tr></tpl>',
            '<tr><th colspan="5" style="padding-top: 8px;">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_19;E=js}</th></tr>',
            '<tpl if="this.isEmpty(excl)"><tr><td colspan="5" class="r-no-ranges-hint">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_23;E=js}</td></tr></tpl>',
            '<tpl for="excl"><tr>',
            '<td>&nbsp;</td>',
            '<td class="start">{start}</td>',
            '<td>&nbsp;to&nbsp;</td>',
            '<td class="end">{end}</td>',
            '<td style="width:100%">&nbsp;</td>',
            '</tr></tpl>',
            '</table>',
            {
                mapUpToOk: function (s) { return s == 'up' ? 'ok' : s; },
                isEmpty: function (data) { return Ext.isEmpty(data); }
            }
        );

        var scopesTpl = new Ext.XTemplate(
            '<ul><tpl for=".">',
            '<tpl if="xindex &lt; 11">',
            '<li class="scope-{st:this.mapUpToOk} scope-list">',
            '<a class="sw-grid-click" href="../../api2/ipam/ui/scope?name={n:this.encodeScopeName}">{n}</a>',
            '<div class="scope-ttip">{.:this.ttip}</div>',
            '<tpl if="xindex<xcount">,</tpl>',
            '</li>',
            '</tpl>',
            '<tpl if="xindex == 11">',
            '<li style="display: inline;">...</li>',
            '</tpl>',
            '</tpl></ul>',
            {
                mapUpToOk: function (s) { return s == 'up' ? 'ok' : s; },
                encodeScopeName: function (n) { return encodeURIComponent(n); },
                ttip: function (val) { return ttipTpl.apply(val); }
            }
        );


        var rangeSort = function (a, b) {
            return (a.ns != b.ns) ? (a.ns - b.ns) : (a.ne - b.ne);
        };
        var ttipData = function (row, data) {
            var ttip = { rang: [], excl: [] };
            ttip.address = row['Address']; ttip.cidr = row['CIDR'];
            var a = $SW.IP.v4(ttip.address), a_n = $SW.IP.v4_toint(a);
            Ext.each(data, function (d, i) {
                var range = {
                    ns: a_n + d.s, ne: a_n + d.e, // sorting purpose
                    start: $SW.IP.v4_fromint(a_n + d.s).join('.'),
                    end: $SW.IP.v4_fromint(a_n + d.e).join('.')
                };
                if (d.t == 1) ttip.excl.push(range);
                if (d.t == 2) ttip.rang.push(range);
            });
            ttip.excl.sort(rangeSort);
            ttip.rang.sort(rangeSort);
            return ttip;
        };

        return function (value, meta, r) {
            meta.css = 'dhcp-scopes-list';
            if (r && r.data) {
                var data = Ext.util.Format.htmlDecode(r.data['DhcpScopes']);
                var scopes = Ext.util.JSON.decode(data);

                if (Ext.isArray(scopes)) {
                    Ext.each(scopes, function (scope, i) {
                        Ext.apply(scope, ttipData(r.data, scope['chart']));
                    });

                    return scopesTpl.apply(scopes);
                }
            }
            return "";
        };
    };

    $SW.subnet.ipDhcpLeaseRenderer = function () {
        return function (v, meta) {
            return (v && v == 2) ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}";
        };
    };

    $SW.subnet.ipDnsRecordsRenderer = function () {
        return function (v, meta) {
            return (v && v > 0) ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}";
        };
    };

    // return IPv6 address parsed into its 8 blocks as string
    $SW.subnet.ip6ToBlocks = function (value) {
        var parts = value.split('::');
        var part1 = parts[0].split(':');
        var part2 = [];
        if (parts.length > 1) {
            part2 = parts[1].split(':');
        }
        var blocks = [];

        for (var i = 0; i < 8; ++i) {
            if (i < part1.length) {
                blocks[i] = part1[i];
            }
            else if (i < 8 - part2.length) {
                blocks[i] = '0';
            }
            else {
                blocks[i] = part2[i - (8 - part2.length)];
            }
        }

        return blocks;
    }

    $SW.subnet.ip6Renderer = function () {
        return function (value, meta) {
            return value;
        };
    };

    $SW.subnet.ip6RendererOnlyEID = function () {
        return function (value, meta) {
            var ret = "";
            if (value) {
                ret = ':' + $SW.subnet.ip6ToBlocks(value).slice(-4).join(':');
            }
            else if (value) {
                ret = value;
            }
            return ret;
        };
    };

    $SW.subnet.ipMapped6Renderer = function () {
        return function (value, meta, r) {
            var ret = "", d = r.data;
            if (value && d.IPAddress6) {
                ret = "<a class='sw-grid-click' href='subnets.aspx?"
                    + "opento=" + d.SubnetId6
                    + "&ip=" + d.IPAddress6 + "'>"
                    + Ext.util.Format.htmlEncode(value) + "</a>";
            }
            else if (value) {
                ret = Ext.util.Format.htmlEncode(value);
            }
            return ret;
        };
    };

    $SW.subnet.ipMapped4Renderer = function () {
        return function (value, meta, r) {
            var ret = "", d = r.data;
            if (value && d.IPAddress4) {
                ret = "<a class='sw-grid-click' ip='" + d.IPAddress4
                    + "' href='subnets.aspx?"
                    + "opento=" + d.SubnetId4
                    + "&ip=" + d.IPAddress4 + "'>"
                    + Ext.util.Format.htmlEncode(value) + "</a>";
            }
            else if (value) {
                ret = Ext.util.Format.htmlEncode(value);
            }
            return ret;
        };
    };

    $SW.subnet.ipScanRenderer = function () {
        return function (value, meta) {
            if (value.toLowerCase() == "true")
                return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}";
            else
                return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}";
        };
    };

    $SW.subnet.ipStatusRenderer = function (highlighting) {
        return function (value, meta, r) {
            if (value) {
                var d = r.data;
                if (d.IsConflict == "1")
                    return qtip = statusTxtMap["IsConflict"];

                var qtip = statusTxtMap[value] || null;
                if (qtip) meta.attr = 'qtip="' + qtip + '"';
                if (jQuery.isFunction(highlighting)) { qtip = highlighting(qtip, meta); }
                return qtip || "";
            }
            return "";
        };
    };

    $SW.subnet.ipUsageRenderer = function (places) {
        var digits = places || 0;
        return function (value, meta) {
            if (typeof (value) == "number" && !isNaN(value)) {
                // two decimal places now.
                if (digits == 1) return '' + Math.round(value);
                var div = Math.pow(10, digits), t = '' + (Math.round(value * div) / div), pos = t.indexOf('.'), d;
                if (pos < 0) { pos = t.length; t += "."; }
                for (d = t.length - pos; d <= digits; ++d) t += '0';
                return t + "%";
            }

            return value || "";
        };
    };

    $SW.subnet.ipDaysRenderer = function (places) {
        var digits = places || 0;
        return function (value, meta) {
            if (value && typeof (value) == "object" && value.constructor === Date) {
                // Unlimited  is set to 11/31/9999
                if (value.getFullYear() == 9999) return "Infinite";

                var secs = (value.getTime() - new Date().getTime()) / 1000;
                if (secs < 0) secs = 0;
                value = secs / 86400;

                var days = Math.floor(secs / 86400); secs %= 86400;
                var hours = Math.floor(secs / 3600); secs %= 3600;
                var mins = Math.round(secs / 60);

                // two decimal places now.
                if (digits == 1) return '' + Math.round(value);
                var div = Math.pow(10, digits), t = '' + (Math.round(value * div) / div), pos = t.indexOf('.'), d;
                if ((pos < 0) && digits) { pos = t.length; t += "."; }
                for (d = t.length - pos; d <= digits; ++d) t += '0';


                var ret = [];
                if (days) ret.push(String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_121;E=js}', days));
                if (hours) ret.push(String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_122;E=js}', hours));
                if (mins) ret.push(String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_123;E=js}', mins));
                if (!days && !hours && !mins) { t = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_124;E=js}"; ret.push(t); }
                else t += String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_121;E=js}', '');
                meta.attr = 'qtip="' + ret.join('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_125;E=js}') + '"';
                return t;
            }

            return value || "";
        };
    };

    $SW.subnet.ipLeaseExpires = function (pat) {
        var fn = Ext.util.Format.dateRenderer(pat);
        return function (value, meta) {
            if (!value)
                return "";
            else
                value = SW.Core.Date.ConvertToDisplayDate(value);
            return fn(value, meta);
        };
    };

    $SW.subnet.ipLastResponse = function (pat) {
        var fn = Ext.util.Format.dateRenderer(pat);
        return function (value, meta) {
            if (!value)
                return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_126;E=js}";
            else
                value = SW.Core.Date.ConvertToDisplayDate(value);
            return fn(value, meta);
        };
    };

    $SW.subnet.ipResponseTimeRenderer = function () {
        return function (value, meta) {
            var val = parseInt(value, 10);
            if (value == "" || isNaN(val))
                return "";

            if (val < 1)
                return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_127;E=js}";
            else
                return String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_128;E=js}', val);
        };
    };

    $SW.subnet.pollingEngineRenderer = function () {
        return function (value, meta, record) {
            if (value != null) {
                if (record.data["EngineType"] === "Primary") {
                    value += " (Primary)";
                }


                text = String.format(
                    "<a href='#NetObject=N:{4}' onclick='return false;' " +
                    "community='GUID({0})' " +
                    "nodehostname='{1}' " +
                    "ip='{2}'>" +
                    "<img src='/Orion/images/StatusIcons/Small-{3}' style='vertical-align: top; margin-top: 1px;' class='StatusIcon' netobject='N:{4}'/>" +
                    "<span>{5}</span></a>",
                    record.data["EngineCommunityString"], // replace with community
                    ($.trim(record.data["EngineDNS"]) || $.trim(record.data["EngineSysName"]) || $.trim(record.data["EngineIPAddress"])),
                    record.data["EngineIPAddress"], // replace with ip
                    $.trim(record.data["EngineStatus"]) || 'unknown.gif', // replace with status
                    record.data["EngineNodeId"],
                    value);



                return text;
            }
        }
    };

    //
    // grid load highlighter
    //

    $SW.subnet.SetGridLoadHighlight = function () {
        var grid = Ext.getCmp('tabSubnet');
        grid.getStore().on('load', function (that, records) {
            $SW.subnet.SelectInitialRow(that, records, grid);
        });
    };

    $SW.subnet.SelectInitialRow = function (that, records, grid) {
        var sel = $SW['initialselection'];
        if (!sel) return;

        if (sel.subnet == treeitem.attributes.id) {
            var sm = grid.getSelectionModel(), p = 0;
            var view = grid.getView();
            var rown = null;

            var sel_ipn = ('' + sel.ipn).toLowerCase();
            Ext.each(records, function (o) {
                var rec_ipn = ('' + o.data.IPAddressN).toLowerCase();
                if (sel_ipn == rec_ipn) rown = p;
                ++p;
            });

            if (rown != null) {
                var fn = function () {
                    grid.getSelectionModel().selectRow(rown);
                    Ext.get(view.getRow(rown)).scrollIntoView(view.scroller);
                };

                window.setTimeout(fn, 50);
            }
        }

        $SW['initialselection'] = null;
    };

    //
    // grid filter
    //  

    $SW.subnet.SetGridFilter = function (that) {
        $SW.ClearGridSelection(Ext.getCmp('tabSubnet'));

        if (that.id == gridfilter)
            return;

        if (gridfilter != null) {
            // if the item came with a iconclass, remove it.
            var chk = Ext.getCmp(gridfilter);
            chk.setChecked(false);
            chk.setIconClass(chk.initialConfig.iconCls || '');
        }

        gridfilter = that.id;
        that.setIconClass('');
        that.setChecked(true);

        var ff = Ext.getCmp('futureFilter');
        if (ff) {
            ff.setIconClass(that.initialConfig.iconCls || '');
            ff.setText(that.text);
        }

        var pgbar = Ext.getCmp('tabSubnet').getBottomToolbar();
        if (pgbar) {
            var filter = pgbar.swFilter || [], btn = pgbar.swFilterBtn;
            if (that.id != 'ffAll') {
                // set correct filter name
                btn.setIconClass(that.initialConfig.iconCls || '');
                btn.setText(that.text);
                // show the pagingtoolbarfilter.
                Ext.each(filter, function () { this.show() });
            }
            else {
                Ext.each(filter, function () { this.hide() });
            }
        }
        $SW.subnet.RefreshTab();
    };

    //Toolset Integration
    $SW.subnet.ToolsetIntegration = function (grid, rowidx, e) {
        var strTemp = '';
        var store = grid.getStore();
        var row = store.getAt(rowidx);

        try {
            if (SWTool.readyState == 4) {
                strTemp = TIBuildParamString(row);
                if (strTemp.length) {
                    SWTool.ShowContextMenu(strTemp, window);
                    // return CancelContext(); need to investigate: why general IE context menu is displayed too
                    return false
                }
                else if (SWTool.Showing) {
                    return CancelContext(); //comes from Orion ToolsetIntegrations.js
                    // this is here rightmouse click on Solarwinds Map objects,
                    //  where were are responding to the Area right click,
                    //  and ignoring the IMG oncontext.
                }
            }

        } catch (errorObject) { ; }

        return false;
    }

    function TIBuildParamString(row) {
        var strOut = '';

        if (row.data.IPAddress != null && row.data.IPAddress.length > 0)
            strOut = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_129;E=js}' + row.data.IPAddress + '\t';

        if (row.data.DnsBackward != null && row.data.DnsBackward.length > 0)
            strOut += '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_130;E=js}' + row.data.DnsBackward + '\t';

        var credObj = GetSnmpCred(treeitem.id, row.data.IPOrdinal);

        if (credObj != null) {
            if (!credObj.browserintegration)
                return '';

            if (credObj.isset)
                strOut += '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_131;E=js}' + credObj.snmpcred.community + '\t';
            else
                strOut += '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_132;E=js}' + '\t';
        }
        return strOut;
    }

    function GetSnmpCred(subnetid, ipordinal) {
        var credobj = null;

        $.ajax({
            type: "POST",
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            async: false,
            data: "entity=IPAM.SNMPCred&verb=CredByIp&ObjectId=" + subnetid + "&IpOrd=" + ipordinal,
            success: function (msg) {
                try {
                    credobj = eval('(' + msg + ')');
                }
                catch (e) {
                }
            }
        });

        return credobj;

    }

    // Personalization stuff

    $SW.subnet.GridColumnChangedHandler = function (component, state) {
        if (component == null)
            return;

        var gridCM = component.getColumnModel();
        var settingName = '';

        if (component.id == 'tabGroup')
            settingName = 'UI.MgtGroupColumns'

        if (component.id == 'tabSubnet')
            settingName = 'UI.MgtSubnetColumns'

        if (component.id == 'tabIPv6Subnet')
            settingName = 'UI.MgtIPv6SubnetColumns'

        if (component.id == 'hiddenSubnet')
            settingName = 'UI.HiddenSubnetColumns'

        if (component.id == 'SearchGrid')
            settingName = 'UI.SearchColumns'

        if (gridCM != null && settingName != '') {

            var jsonvalue = GridColumnsToJson(gridCM);
            var params = String.format("{0}:{1}", settingName, jsonvalue);
            UpdateUISetting(params);
        }
    };

    //helpers
    function GridColumnsToJson(columnmodel) {
        var cmconfig = columnmodel.config;
        var list = [];

        // [pc] skip first col.
        for (var i = 1; i < cmconfig.length; i++) {
            var obj = {};
            obj.id = cmconfig[i].header;
            obj.width = cmconfig[i].width;
            obj.visible = !cmconfig[i].hidden;
            list.push(obj);
        }

        var jsonstate = Ext.util.JSON.encode(list);
        return jsonstate;
    }

    function UpdateUISetting(jsonparams) {
        var fail = $SW.ext.AjaxMakeFailDelegate('UpdateUISettings');

        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            failure: fail,
            timeout: fail,
            params:
            {
                entity: 'IPAM.Setting',
                verb: 'Update',
                Type: 'UserSetting',
                settings: jsonparams
            }
        });
    }


    //Ext.quickTips.init();
    //Ext.apply(Ext.Quicktips.getQuicktip(), { showDelay: 0, trackmouse: true });
    
    //Export return bar
    $SW.subnet.ExportFinished = function()
    {
        Ext.Msg.show({
            title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_56_2;E=js}',
            msg: "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_56_1;E=js}",
            minWidth: 200,
            buttons: Ext.Msg.OK,
            fn: function(){ window.location = $SW.appRoot() + 'Orion/IPAM/subnets.aspx'; },
            closable: false,
            icon: Ext.MessageBox.INFO
        });
    };
    
    $SW.ClearGridSelection = function (grid) {
        try {
            if (grid != null) {
                var sm = grid.getSelectionModel();
                if (sm.grid) sm.clearSelections();
                Ext.get(grid.getView().getHeaderCell(0)).select("div.x-grid3-hd-inner").removeClass('x-grid3-hd-checker-on');
            }
        } catch (e) {
        }
    };

    // IPv6
    if (!$SW.IPv6Handlers) $SW.IPv6Handlers = {};
    $SW.IPv6Handlers.GlobalPrefixAdd = function (el) {
        $SW.IPv6.GlobalPrefixAdd(el, treeitem);
    };
    $SW.IPv6Handlers.PrefixAdd = function (el) {
        $SW.IPv6.LocalPrefixAdd(el, treeitem);
    };
    $SW.IPv6Handlers.SubnetAdd = function (el) {
        $SW.IPv6.SubnetAdd(el, treeitem);
    };

    $SW.IPv6Handlers.IPAddressAdd = function (el) {
        $SW.IPv6.ipAdd(el, treeitem);
    };
    $SW.IPv6Handlers.IPAddressEdit = function (el) {
        $SW.IPv6.ipEdit(el, treeitem);
    };
    $SW.IPv6Handlers.IPAddressDetails = function (el) {
        $SW.IPv6.ipDetails(el, treeitem);
    };
    $SW.IPv6Handlers.IPAddressRemove = function (el) {
        $SW.IPv6.ipRemove(el, treeitem);
    };
    $SW.IPv6Handlers.IPsImport = function (el) {
        $SW.IPv6.ipImport(el, treeitem);
    };
    $SW.IPv6Handlers.IPsExport = function (el) {
        $SW.IPv6.ipExport(el, treeitem);
    };
})();
$SW.subnet.RefreshHidden = function () { $SW['store'].load(); };



