
// - loader

Ext.ux.SWGroupByList = function(config){
    var fmt = '<ul class="sw-groupby-list"><li class="{0}"><div><img style="vertical-align: middle;" src="/Orion/IPAM/res/images/{1}">';
    this.templatehtml = '<li id="{domid}" class="sw-groupby-item {cls}" {[ (values.text != null) ? "sw:rel=\\"" + fm.htmlEncode(values.id) + "\\"" : "" ]}><div><a href="#">{[fm.htmlEncode(values.text)]} ({n})</a></div></li>';
    this.loading = String.format( fmt, 'sw-loading', 'default/tree/loading.gif' ) + ' @{R=IPAM.Strings;K=IPAMWEBJS_VB1_311;E=js}</div></li></ul>';
    this.failing = String.format( fmt, 'sw-failing', 'sw/icon.warning.gif' ) + ' {0}</div></li></ul>';
    this.url = '/Orion/IPAM/services/information.asmx/Query';
    this.actionDescription = "Get Group-By List";
    this.loaddata = {};
    this.selectedDom = null;
    this.selectedValue = null;
    this.records = null;
    Ext.ux.SWGroupByList.superclass.constructor.call(this, config);
    this.template = new Ext.XTemplate( this.templatehtml );
};


Ext.extend(Ext.ux.SWGroupByList, Ext.Panel, {

 onClickH : function(e){
   var tgt, li;
   if( e.within( this.body.dom ) && (tgt = Ext.get(Ext.fly(e.target).findParent('a'))) ){
     e.preventDefault();
     li = Ext.get( Ext.fly(e.target).findParent('li.sw-groupby-item') );
     if( this.selectedDom ){ var s=Ext.fly(this.selectedDom); if(s) s.removeClass('sw-selected'); }
     this.selectedDom = li.id;
     this.selectedValue = li.getAttributeNS('sw','rel')||'';
     li.addClass('sw-selected');
     this.fireEvent('selectchange', this, this.selectedValue, e);
   }
 },

 initComponent : function(){
    Ext.ux.SWGroupByList.superclass.initComponent.call(this);
    this.on('render', this.init );
 },

 init: function(){
    this.body.on('click', this.onClickH, this);
 },

 select: function(id){
   if( id === this.selectedValue ) return true;

   var me = this, tgt, domid=null;
   Ext.each( me.records || [], function(item){
     if( !item ) return;
     if( item.id == id ) { domid = item.data.domid; item.data.selected = true; }
     else item.data.selected = false;
   });

   if( !domid ) return false;
   if(tgt = Ext.get(me.selectedDom)) tgt.removeClass('sw-selected');
   me.selectedDom = domid;
   me.selectedValue = id;
   if( tgt = Ext.get(domid) ) tgt.addClass('sw-selected');
   me.fireEvent('selectchange', me, me.selectedValue );
   return true;
 },
 
 translateData: function(records){
   var unknowns = null;
   jQuery.each(records, function (n, elm) {
     var txt = elm.data.text;
     elm.data.text = (txt == undefined || txt === null) ? null : '' + txt;

     elm.id = elm.data.id = elm.data.text;
     elm.data.domid = Ext.id();

     if (elm.data.text == null || elm.data.text == '') {
       if( !unknowns ) unknowns = elm.copy();
       else unknowns.data.n += elm.data.n;
       records[n] = null;
     }

     if( elm.id == this.selectedValue ) elm.selected = true;
   });

   if( unknowns ){
     unknowns.data.text = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_312;E=js}';
     unknowns.data.id = unknowns.id = '';
     records.splice(0,0,unknowns);
   }

   return records;
 },
 
 RenderBody: function(records){
    var me = this, tpl = me.template, stuff = ['<ul class="sw-groupby-list">'];
    var templateEx = (me.loaddata) ? me.loaddata.templateEx : null;
    if(templateEx) { tpl = new Ext.XTemplate( templateEx ); }
    Ext.each( records, function(item){
      if( !item ) return;
      if( item.data.selected ){
        item.data.cls = (item.data.cls||'') + 'sw-selected';
        me.selectedDom = item.data.domid;
      }
      stuff.push( tpl.applyTemplate( item.data ) );
    });
    stuff.push('</ul>');
    Ext.DomHelper.overwrite( me.body.dom, stuff.join('') );

    if( !me.selectedDom && me.selectedValue != null ){
      var pre = me.selectedValue;
      me.selectedValue = null;
      this.fireEvent('selectchange', this, null, pre );
    }
 },

 handleResponse: function(json,options){
    var cols = [], me = this;
    jQuery.each( json.d.Columns, function(n,el){ cols.push({ name: el, mapping: n }); });
    me.reader = new Ext.data.ArrayReader({},Ext.data.Record.create( cols ));
    me.records = me.reader.readRecords( json.d.Rows ).records;
    if( this.translateData ) me.records = this.translateData( me.records );
    this.fireEvent('afterload', me, me.records );
    this.RenderBody(me.records);
 },
 
 loadfailure: function(){
     this.clear();
     Ext.DomHelper.overwrite( this.body.dom, String.format( this.failing, "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_313;E=js}" ) );
 },
    load: function(options){
    options = options || {};
    var me = this,
     fail = $SW.ext.AjaxMakeFailDelegate( me.actionDescription, me.loadfailure, me.id ),
     pass = $SW.ext.AjaxMakePassDelegate( me.actionDescription, me.handleResponse, me.id ),
     uc = options.userctx || null;

    Ext.DomHelper.overwrite( me.body.dom, this.loading );

    this.selectedDom = null;
    this.loaddata = options || me.loaddata;

    Ext.Ajax.request({
      type: 'POST',
      url: me.url,
      jsonData: $SW.SWQLbuild(me,me.loaddata),
      usercontext: uc,
      success: pass,
      failure: fail,
      timeout: fail
    });

    me=fail=pass=uc=null;
  },

  clear: function(){
    var me=this, selected = me.selectedValue != null;
    me.records = me.selectedValue = me.selectedDom = me.loaddata = null;
    Ext.DomHelper.overwrite( me.body.dom, '' );
    this.fireEvent('selectchange', this, null );
  }
});
