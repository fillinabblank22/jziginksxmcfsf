﻿(function () {
    function GridColumnsToJson(columnmodel, startIndex) {
        var cmconfig = columnmodel.config;
        var list = [];

        // [pc] skip first col.
        for (var i = (startIndex != undefined) ? startIndex : 1; i < cmconfig.length; i++) {
            var obj = {};
            obj.id = cmconfig[i].header;
            obj.width = cmconfig[i].width;
            obj.visible = !cmconfig[i].hidden;
            list.push(obj);
        }

        var jsonstate = Ext.util.JSON.encode(list);
        return jsonstate;
    }
    function UpdateUISetting(jsonparams) {
        var fail = $SW.ext.AjaxMakeFailDelegate('UpdateUISettings');
        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            failure: fail,
            timeout: fail,
            params: {
                entity: 'IPAM.Setting',
                verb: 'Update',
                Type: 'UserSetting',
                settings: jsonparams
            }
        });
    }

    function IsPowerUser() {
        return $("#isPowerUser").length > 0;
    }

    function CheckIsPowerUser() {
        if (IsPowerUser()) {
            return true;
        }
        return false;
    }

    function IsDemoMode() {
        return $("#isDemoMode").length > 0;
    }

    function CheckDemoMode() {
        if (IsDemoMode()) {
            return true;
        }
        return false;
    }

    function SetExistingZoneBtnState() {
        $SW.DoQuery('SELECT TOP 1 Z.DnsZoneId FROM IPAM.DnsZone Z WHERE Z.GroupId IS NULL', function (values) {
            if (values == null || values.length > 0) {
                Ext.getCmp('btnAddExistingZone2').enable();
                Ext.getCmp('btnAddExistingZone').enable();
            }
            else {
                Ext.getCmp('btnAddExistingZone2').disable();
                Ext.getCmp('btnAddExistingZone').disable();
            }
        });
    }

    // [oh] stores the Dhcp/Dns server scan task information
    var _dhcpScanTaskData = undefined;
    var _dnsScanTaskData = undefined;

    var manager = {
        RefreshGridPage: function (tab, options) {
            var grid = Ext.getCmp(tab);
            if (tab != "tabScopes") {
                $SW.ClearGridSelection(grid);
            }
            var cbox = options.cbox, s = cbox.store;
            var key = cbox.getValue(), idx = s.find('value', key);
            cbox.setValue(key);
            cbox.fireEvent('select', cbox, s.getAt(idx), idx);
        },
        Delete: function (el, tab, pageTpl, helpid) {
            var grid = Ext.getCmp(tab), sel = grid.getSelectionModel(), c = sel.getCount();
            if (c < 1) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
                    msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_62;E=js}",
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.WARNING
                });
            }
            else {
                var list = [], items = sel.getSelections();

                Ext.each(items, function (o) {
                    if (o.id) {
                        list.push(o.id);
                    }
                });

                $SW.ext.dialogWindowOpen(
                    $SW['deleteConfirm'], el.id,
                    String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_63;E=js}', items.length),
                    pageTpl + list.join('|'), helpid
                );
            }
        }
    };

    var DHCPTabmanager = {
        RefreshTreeGridPage: function (tab, options) {
            var grid = Ext.getCmp(tab);
            $SW.ClearGridSelection(grid);
            var cbox = options.cbox, s = cbox.store;
            var key = cbox.getValue(), idx = s.find('value', key);
            cbox.setValue(key);
            cbox.fireEvent('select', cbox, s.getAt(idx), idx);
        },
        Delete: function (el, tab, pageTpl, helpid, viewInnerType) {
            var treeGrid = Ext.getCmp(tab);
            var sel = treeGrid.getSelectionModel() || [];
            var list = [], items = sel.getSelectedNodes();
            var type = [];
            Ext.each(items, function (o) {
                list.push(o.id);
                type.push(o.attributes.ViewInnerType);
            });
            var objectType = (viewInnerType == 0) ? 'dhcp_servers' : 'dhcp_scopes';
            var tpl = String.format(pageTpl, 'Dhcp.Scope.DeleteConfirm', objectType, list, type);
            $SW.ext.dialogWindowOpen(
                $SW['deleteConfirm'], el.id,
                String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_63;E=js}', items.length),
                tpl, helpid
            );
        }
    };


    if (!$SW.IPAM) $SW.IPAM = {};

    $SW.IPAM.DhcpScopeDeleteErrMsgTpl = new Ext.XTemplate(
        '<tpl if="!this.hasErrs(results)">{message:this.toHtml}</tpl>',
        '<tpl if="this.hasErrs(results)">',
        '<tpl for="results"><tpl if="this.isValid(ErrorCode)">',
        '<br>■ &ensp; {.:this.translate}',
        '</tpl></tpl>',
        '</tpl>',
        {
            toHtml: function (msg) { return Ext.util.Format.htmlEncode(msg); },
            hasErrs: function (rs) { return (rs && rs.length > 0); },
            isValid: function (ec) { return (ec != undefined && ec != 0); },
            translate: function (err) {
                if (err.ErrorCode == "74")
                    return '<b>' + "Error While Removing: " + '</b>&nbsp;' + err.ErrorMessage;
                else
                    return err.ErrorMessage;
            }
        }
    );

    if (!$SW.IPAM.DhcpDnsManage) $SW.IPAM.DhcpDnsManage =
        function (dhcp, dns) {
            var opt = {
                DhcpOpt: dhcp, DhcpServer: {}, DhcpScope: {}, DhcpTab: {},
                DnsOpt: dns, DnsServer: {}, DnsZone: {}
                //,
                //  DhcpTabOpt: dhTab, DhcpTabServer: {}, DhcpTabScope: {}
            };
            var tpl = '/Orion/IPAM/{0}.aspx?NoChrome=True&ObjectType={1}&msgq=deleteConfirm&ObjectId={2}&Type={3}';
            var tpl1 = '/Orion/IPAM/{0}.aspx?NoChrome=True&ObjectType={1}&msgq=deleteConfirm&ObjectId=';

            opt.Help = function () {
                var page = this.ActiveTabManager().hlpFragment;
                var v = '{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}.htm';
                if (page) window.open(String.format(v, $SW.HelpServer || '/', page), '_blank');
            };

            opt.ActiveTabManager = function () {
                var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);
                var tab = layout.getActiveTab(), tid = tab ? tab.id : undefined;
                if (tid == dhcp.tabs['scopes']) {
                    return this.DhcpScope;
                } else if (tid == dhcp.tabs['servers']) {
                    return this.DhcpServer;
                } else if (tid == dns.tabs['zones']) {
                    return this.DnsZone;
                } else if (tid == dns.tabs['servers']) {
                    return this.DnsServer;
                }
            };

            opt.DeleteComplete = function (data) {
                $SW['deleteConfirm'].hide();
                var t = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_80;E=js}', that = this;
                var refreshFn = function () {
                    var d = data['objecttype'].split('_');
                    var type = d[0], tabid = d[1];
                    if (type == 'dhcp') {
                        var o = that.DhcpOpt;
                        manager.RefreshGridPage('tabScopes', o);
                        $SW.IPAM.SetExistingScopeBtnState();
                    } else if (type == 'dns') {
                        var o = that.DnsOpt;
                        SetExistingZoneBtnState.apply(that);
                        manager.RefreshGridPage(o.tabs[tabid], o);
                    }
                };
                $SW.Tasks.DoProgress('DeleteGroups', data, {
                    title: t,
                    buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_4;E=js}' }
                },
                    function (d) {
                        if (d.success)
                            refreshFn();
                        else {
                            if (d.state == 4 /*UITaskState.Canceled*/) {
                                refreshFn();
                            }
                            else {
                                Ext.MessageBox.show({
                                    title: t,
                                    msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + d.message,
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.ERROR,
                                    fn: refreshFn
                                });
                            }
                        }
                    }, refreshFn);
            };

            var SingleItemSelection = function (tab, el, action, callback) {
                var grid = Ext.getCmp(tab);
                var sm = grid.getSelectionModel(), c = sm.getCount();

                if (c == 1) { callback(el, sm.getSelected(), grid); return; }

                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
                    msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_133;E=js}', action),
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.WARNING
                });
                return;
            };
            var DHCPSingleItemSelection = function (tab, el, action, callback) {
                var treeGrid = Ext.getCmp(tab);
                var s = treeGrid.getSelectionModel().getSelectedNodes() || [], c = s.length;
                if (c == 1 || action == "Remove") { callback(el, s[0], treeGrid); return; }

                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
                    msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_133;E=js}', action),
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.WARNING
                });
                return;
            };

            var DHCPSplitScopeWizardCheck = function (el, item, grid) {
                var type = item.parentNode.attributes.ServerType;
                var server = item.id, fn = function () {
                    window.location = "/Orion/IPAM/DhcpSplitScope/Dhcp.SplitScope.Wizard.aspx?GroupId=" + server;
                };
                return $SW.IPAM.DhcpDnsManage.SplitScopeWizardCheck(el, type, server, fn);
            };

            var SplitScopeWizardCheck = function (el, item, grid) {
                var type = item.data["ServerType"], server = item.id, fn = function () {
                    window.location = "/Orion/IPAM/DhcpSplitScope/Dhcp.SplitScope.Wizard.aspx?GroupId=" + server;
                };
                return $SW.IPAM.DhcpDnsManage.SplitScopeWizardCheck(el, type, server, fn);
            };

            $SW.IPAM.SetExistingScopeBtnState = function () {
                $SW.DoQuery('SELECT TOP 1 S.ScopeId FROM IPAM.DhcpScope S WHERE S.GroupId IS NULL', function (values) {
                    if (values == null || values.length > 0)
                        Ext.getCmp('btnAddExistingScope').enable();
                    else
                        Ext.getCmp('btnAddExistingScope').disable();
                });
            };

            /* Dhcp&Scope */
            Ext.apply(opt.DhcpTab, {
                ID: 'dhcpscopes',
                hlpFragment: 'OrionIPAMPHDHCPServersView',
                RefreshGridPage: function () {
                    manager.RefreshGridPage(dhcp.tabs['scopes'], dhcp);
                },
                Add: function (el) {
                    window.location = "/Orion/IPAM/dhcp.server.add.aspx";
                },
                AddScope: function (el, item, grid) {
                    var treeGrid = Ext.getCmp(dhcp.tabs['scopes']);
                    var sel = treeGrid.getSelectionModel().getSelectedNodes() || [], c = sel.length;
                    if (c < 1) { window.location = "/Orion/IPAM/DhcpScope/Dhcp.Scope.Wizard.aspx"; return; }
                    else if (c == 1) {
                        window.location = "/Orion/IPAM/DhcpScope/Dhcp.Scope.Wizard.aspx?ObjectId=" + sel[0].id; return;
                    } else {
                        DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'AddScope', function (el, item, grid) { return });
                    }
                },
                Edit: function (el) {
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Edit', function (el, item, grid) {
                        if (item.attributes.ViewInnerType == 0) {

                            var page = "Dhcp.Server.Edit.aspx";
                            var helpid = 'OrionIPAMPHDHCPServerEdit.htm';
										
                            $SW['mainWindow'].width = 700;
                            $SW.ext.dialogWindowOpen($SW['mainWindow'], el.id,
                                String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_134;E=js}', item.attributes.FriendlyName),
                                '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + item.attributes.id,
                                helpid
                            );
                        }
                        else if (item.attributes.ViewInnerType == 3) {

                            var page = "Dhcp.Scope.Edit.aspx";
                            var helpid = 'OrionIPAMPHDHCPScopeEdit.htm';

                            DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Edit', function (el, item, grid) {
                                var type = item.parentNode.attributes.ServerType;
                                // check capability of edit scope
                                if ($SW['dhcpCapability'].test(type, 'IScopeManagement')) {
                                    var url = "/Orion/IPAM/DhcpScope/Dhcp.Scope.Wizard.aspx?ScopeId=" +
                                        item.attributes.GroupId;
                                    if (item.attributes.ClusterId) {
                                        url += "&ClusterId=" + item.attributes.ClusterId;
                                    }
                                    window.location = url;
                                } else {
                                    $SW.ext.dialogWindowOpen($SW['scopeEditWindow'], el.id,
                                        String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_275;E=js}', item.attributes.FriendlyName),
                                        '/Orion/IPAM/' + page + '?NoChrome=True&msgq=scopeEditWindow&ObjectId=' + item.attributes.GroupId,
                                        helpid
                                    );
                                }
                            });
                        }
                    });
                },
                Delete: function (el) {
                    if (CheckDemoMode()) {
                        demoAction('IPAM_DHP_ServerDelete', null);
                        return false;
                    }
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Remove', function (el, item, grid) {
                        var treeGrid = Ext.getCmp(dhcp.tabs['scopes']);
                        var sel = treeGrid.getSelectionModel() || [];
                        var items = sel.getSelectedNodes();
                        if (items.length < 1) {
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
                                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_62;E=js}",
                                buttons: Ext.Msg.OK,
                                animEl: el.id,
                                icon: Ext.MessageBox.WARNING
                            });
                        } else {
                            DHCPTabmanager.Delete(el, dhcp.tabs['scopes'], tpl,
                                'OrionIPAMPHDHCPDeleteConfirmation.htm', item.attributes.ViewInnerType);
                        }
                    });
                },
                DetailView: function (el) {
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'View Details', function (el, item, grid) {
                        window.location = 'IPAMDHCPServerView.aspx?NetObject=IPAMD:' + item.attributes.GroupId;
                    });
                },
                ReplicateScope: function (el) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_BO1_1;E=js}';
                    var successFn = function (d) {
                        Ext.Msg.show({
                            title: title,
                            msg: d.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () {
                                var id = parseInt(d.results);
                                if (id) {
                                    $SW.IPAM.manager.DhcpTab.Scan.call(null, null, id);
                                }
                            }
                        });
                    };
                    var failFn = function (d) {
                        Ext.MessageBox.show({
                            title: title,
                            msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + d.message,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () {
                                $SW.IPAM.GroupBy.RefreshTab(dhcp);
                            }
                        });
                    };
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Replicate Scope', function (el, item, grid) {
                        var data = { ScopeGroupId: item.id, ServerGroupId: parseInt(item.parentNode.id) };
                        $SW.Tasks.DoProgress('ReplicateDhcpScope', data, {
                            title: title,
                        },
                            function (d) {
                                if (d.success)
                                    successFn(d);
                                else {
                                    if (d.state == 4 /*UITaskState.Canceled*/) {
                                        /*todo: refresh page*/
                                    }
                                    else {
                                        failFn(d);
                                    }
                                }
                            },
                            function () { /*todo: refresh page*/ });
                    });
                },
                ReplicateRelationship: function (el) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_BO1_3;E=js}';
                    var successFn = function (d) {
                        Ext.Msg.show({
                            title: title,
                            msg: d.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () {
                                var id = parseInt(d.results);
                                if (id) {
                                    $SW.IPAM.manager.DhcpTab.Scan.call(null, null, id);
                                }
                            }
                        });
                    };
                    var failFn = function (d) {
                        Ext.MessageBox.show({
                            title: title,
                            msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () {
                                $SW.IPAM.GroupBy.RefreshTab(dhcp);
                            }
                        });
                    };
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Replicate Relationship', function (el, item, grid) {
                        var data = { ScopeGroupId: item.id, ServerGroupId: parseInt(item.parentNode.id) };
                        $SW.Tasks.DoProgress('ReplicateDhcpRelationship', data, {
                            title: title,
                        },
                            function (d) {
                                if (d.success)
                                    successFn(d);
                                else {
                                    if (d.state == 4 /*UITaskState.Canceled*/) {
                                        /*todo: refresh page*/
                                    }
                                    else {
                                        failFn(d);
                                    }
                                }
                            },
                            function () { /*todo: refresh page*/ });
                    });
                },
                Split: function (el) {
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Split Scope', function (el, item, grid) {
                        DHCPSplitScopeWizardCheck(el, item, grid);
                    });
                },
                Scan: function (el, id) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_136;E=js}';
                    var successFn = function (d) {
                        Ext.Msg.show({
                            title: title,
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_135;E=js}',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () {
                                var task = _dhcpScanTaskData;
                                if (task && task.run) task.run();
                                $SW.IPAM.GroupBy.RefreshTab(dhcp);
                                manager.RefreshGridPage(dhcp.tabs['scopes'], dhcp);
                                $SW.IPAM.SetExistingScopeBtnState();
                            }
                        });
                    };
                    var failFn = function (d) {
                        Ext.MessageBox.show({
                            title: title,
                            msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () {
                                $SW.IPAM.GroupBy.RefreshTab(dhcp);
                            }
                        });
                    };
                    var scan = function (id) {
                        var data = { ServerId: parseInt(id) };
                        $SW.Tasks.DoProgress('DhcpServerScan',
                            data,
                            {
                                title: title,
                                buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}' }
                            },
                            function (d) {
                                if (d.success)
                                    successFn(d);
                                else {
                                    if (d.state == 4 /*UITaskState.Canceled*/) {
                                        /*todo: refresh page*/
                                    } else {
                                        failFn(d);
                                    }
                                }
                            },
                            function () { /*todo: refresh page*/ });
                    };
                    if (parseInt(id)) {
                        scan(id);
                    } else {
                        DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Scan', function (el, item, grid) {
                            scan(item.id);
                        });
                    }
                },
                ViewAddressLeases: function (el) {
                    var page = "Dhcp.Scope.AddressLeases.aspx";
                    var helpid = 'OrionIPAMPHDHCPAddressLeases.htm'
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Address Leases', function (el, item, grid) {
                        //Todo Change Group id to scope id 
                        $SW.ext.dialogWindowOpen(
                            $SW['scopeAddressLeasesWindow'], el.id,
                            String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_274;E=js}', item.attributes.FriendlyName),
                            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=scopeAddressLeasesWindow&ObjectId=' + item.attributes.ScopeId,
                            helpid
                        );
                    });
                },
                DeconfigureFailover: function (el) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_407;E=js}';
                    var successFn = function (d) {
                        Ext.Msg.show({
                            title: title,
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_405;E=js}',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                            fn: function () {
                                $SW.IPAM.GroupBy.RefreshTab(dhcp);
                                manager.RefreshGridPage(dhcp.tabs['scopes'], dhcp);
                            }
                        });
                    };
                    var failFn = function (d) {
                        Ext.MessageBox.show({
                            title: title,
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_406;E=js}</b></br>' +
                                String.format('@{R=IPAM.Strings;K=IPAMWEBJS_DM1_2;E=js}', Ext.util.Format.htmlEncode(d.message)),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () {
                                DHCPSingleItemSelection(dhcp.tabs['scopes'],
                                    el,
                                    'Deconfigure Failover',
                                    function (el, item, grid) {
                                        var serverId = parseInt(item.parentNode.id)
                                        $SW.IPAM.manager.DhcpTab.Scan.call(null, null, serverId);
                                    });
                            }
                        });
                    }
                    DHCPSingleItemSelection(dhcp.tabs['scopes'], el, 'Deconfigure Failover', function (el, item, grid) {
                        var data = { scopeId: item.id, dhcpServerId: parseInt(item.parentNode.id) };
                        $SW.Tasks.DoProgress('DeconfigureFailover', data, {
                            title: title,
                        },
                            function (d) {
                                if (d.success)
                                    successFn(d);
                                else {
                                    if (d.state == 4 /*UITaskState.Canceled*/) {
                                        /*todo: refresh page*/
                                    }
                                    else {
                                        failFn(d);
                                    }
                                }
                            },
                            function () { /*todo: refresh page*/ });
                    });
                }
            });

            Ext.apply(opt.DnsServer, {
                ID: 'dnsservers',
                hlpFragment: 'OrionIPAMPHDNSServerView',
                RefreshGridPage: function () {
                    manager.RefreshGridPage(dns.tabs['servers'], dns);
                },
                Add: function () {
                    window.location = "/Orion/IPAM/dns.server.add.aspx";
                },
                AddZone: function () {
                    var page = "/Orion/IPAM/DnsZones/Dns.Zone.Wizard.aspx";
                    var grid = Ext.getCmp(dns.tabs['servers']);
                    var sm = grid.getSelectionModel(), c = sm.getCount();
                    if (c == 1)
                        page += "?ObjectId=" + sm.getSelected().id;
                    window.location = page;
                },
                Edit: function (el) {
                    var page = "Dns.Server.Edit.aspx";
                    var helpid = 'OrionIPAMPHDNSServerEdit.htm';
                    SingleItemSelection(dns.tabs['servers'], el, 'Edit', function (el, item, grid) {
                        $SW.ext.dialogWindowOpen($SW['mainWindow'], el.id,
                            String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_134;E=js}', item.data.FriendlyName),
                            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + item.id,
                            helpid
                        );
                    });
                },
                Delete: function (el) {
                    if (CheckDemoMode()) {
                        demoAction('IPAM_DNS_ServerDelete', null);
                        return false;
                    }
                    manager.Delete(el, dns.tabs['servers'],
                        String.format(tpl1, 'Dns.Zone.DeleteConfirm', 'dns_servers'),
                        'OrionIPAMPHDNSDeleteConfirmation.htm'
                    );
                },
                Scan: function (el) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_137;E=js}';
                    var successFn = function (d, server) {
                        $SW.IPAM.StartPeriodicDnsScanChecks(server.GroupId, server.FriendlyName, server.LastDiscovery, $SW.Env['checktimeout'] || 60000, false);

                        var task = _dnsScanTaskData;
                        if (task && task.run) task.run();
                        $SW.IPAM.GroupBy.RefreshTab(dns);
                        SetExistingZoneBtnState();
                    };
                    var failFn = function (d) {
                        Ext.MessageBox.show({
                            title: title,
                            msg: "<b>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}</b><br />" + Ext.util.Format.htmlEncode(d.message),
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.ERROR,
                            fn: function () {
                                $SW.IPAM.GroupBy.RefreshTab(dns);
                            }
                        });
                    };
                    SingleItemSelection(dns.tabs['servers'], el, 'Scan', function (el, item, grid) {
                        var data = { ServerId: parseInt(item.id) };
                        var server = item.data;
                        $SW.Tasks.DoProgress('DnsServerScan', data, {
                            title: title,
                            buttons: { cancel: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}' }
                        },
                            function (d) {
                                if (d.success)
                                    successFn(d, server);
                                else {
                                    if (d.state == 4 /*UITaskState.Canceled*/) {
                                        /*todo: refresh page*/
                                    }
                                    else {
                                        failFn(d);
                                    }
                                }
                            },
                            function () { /*todo: refresh page*/ }
                        );
                    });
                }
            });

            Ext.apply(opt.DnsZone, {
                ID: 'dnszones',
                hlpFragment: 'OrionIPAMPHDNSZonesView',
                RefreshGridPage: function () {
                    manager.RefreshGridPage(dns.tabs['zones'], dns);
                },
                Add: function () {
                    window.location = "/Orion/IPAM/DnsZones/Dns.Zone.Wizard.aspx";
                },
                Edit: function (el) {
                    var page = "Dns.zone.Edit.aspx";
                    var helpid = 'OrionIPAMPHDNSZoneEdit.htm';
                    SingleItemSelection(dns.tabs['zones'], el, 'Edit', function (el, item, grid) {
                        window.location = "/Orion/IPAM/DnsZones/Dns.Zone.Wizard.aspx?ZoneId=" + item.id;
                    });
                },
                Delete: function (el) {
                    if (CheckDemoMode()) {
                        demoAction('IPAM_DNS_ZoneDelete', null);
                        return false;
                    }
                    manager.Delete(el, dns.tabs['zones'],
                        String.format(tpl1, 'Dns.Zone.DeleteConfirm', 'dns_zones'),
                        'OrionIPAMPHDNSDeleteConfirmation.htm'
                    );
                },
                ViewDnsRecords: function (el) {
                    var page = "Dns.Records.aspx";
                    var helpid = 'OrionIPAMDNSZonerecords.htm'
                    SingleItemSelection(dns.tabs['zones'], el, 'DNS Records', function (el, item, grid) {
                        window.location = "/Orion/IPAM/Dns.Records.aspx?ObjectId=" + item.data["GroupId"];
                    });
                },
                Scan: function (el) {
                    var title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_139;E=js}';
                    var successScanFn = function (zoneName) {
                        Ext.Msg.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_138;E=js}',
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_55;E=js}',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.MessageBox.QUESTION,
                            fn: function (btn) { if (btn == 'yes') { window.location = "subnet.scanstatus.aspx"; } }
                        });
                    };
                    var successDoPollFn = function (zoneName) {
                        Ext.Msg.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_138;E=js}',
                            msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VV0_8;E=js}', zoneName),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    };

                    SingleItemSelection(dns.tabs['zones'], el, 'Scan', function (el, item, grid) {
                        var zoneName = Ext.util.Format.htmlEncode(item.data["FriendlyName"]);
                        var typeScan = item.data["ServerType"] == 1;
                        var successFn = typeScan ? function () { return successScanFn(zoneName); } : function () { return successDoPollFn(zoneName); };
                        var pass = $SW.ext.AjaxMakePassDelegate('Scan Zones', successFn);
                        var fail = $SW.ext.AjaxMakeFailDelegate('Scan Zones');

                        Ext.Ajax.request({
                            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                            success: pass,
                            failure: fail,
                            timeout: fail,
                            params: {
                                entity: 'IPAM.DnsZone',
                                verb: typeScan ? 'Scan' : 'DoPoll',
                                ids: item.id
                            }
                        });
                    });
                }
            });

            return Ext.apply(opt, manager);
        };

    $SW.IPAM.DhcpDnsManage.SplitScopeWizardCheck = function (el, serverType, serverId, fn) {
        var type = serverType, queryid = serverId;

        var dhcpServerCountCheck = function (count) {
            if (count && count > 1) {
                fn(); return;
            }
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_5;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_6;E=js}',
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
        };

        var conn = new Ext.data.Connection({
            url: '/Orion/IPAM/ExtDataProvider.ashx',
            method: 'POST'
        });
        conn.request({
            reqid: queryid,
            params: {
                'entity': 'IPAM.DhcpServer',
                'filter[0][data][comparison]': 'eq',
                'filter[0][data][type]': 'numeric',
                'filter[0][data][value]': '' + type,
                'filter[0][field]': 'ServerType',
                'properties': 'NodeId'
            },
            success: function (reqid) {
                return function (responseObject) {
                    if (reqid != queryid) return;

                    var reader = new Ext.ux.SWArrayReader(
                        { root: "rows", totalProperty: "count", id: 0 },
                        [{ name: "NodeId" }]
                    );
                    var ret = null;
                    try { ret = reader.read(responseObject); } catch (e) { }

                    if (ret && ret.success)
                        return dhcpServerCountCheck(ret.totalRecords);
                    else {
                        $SW.ext.AjaxIsErrorOrExpire({
                            cmd: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_5;E=js}',
                            text: responseObject.responseText
                        });
                    }
                };
            }(queryid),
            failure: function (reqid) {
                return function () {
                    if (reqid != queryid) return;
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_5;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_8;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.id,
                        icon: Ext.MessageBox.ERROR
                    });

                };
            }(queryid)
        });
        return;
    };

    $SW.IPAM.DnsServerGridListeners = function (dnsOpt) {
        var _GotoServer = function (txtname) {
            var val = $SW.IPAM.GroupBy.DnsDataSet.defaultGrouping;
            var cbox = dnsOpt.cbox, s = cbox.store, idx = s.find('value', val);
            if (idx >= 0) {
                dnsOpt.list.selectedValue = $SW.IPAM.GroupBy.Last = txtname;
                cbox.setValue(val);
                cbox.fireEvent('select', cbox, s.getAt(idx), idx);
            }
        };
        var drilldownFn = function (grid, rowidx, colidx, e) {
            var didx = grid.getColumnModel().getDataIndex(colidx);
            var row = grid.getStore().getAt(rowidx);
            if (didx == 'FriendlyName')
                _GotoServer(row.data[didx]);
            return false;
        };
        var statesaveFn = function (el, state) {
            if (el == null) return;
            if (el.id != dnsOpt.tabs['servers']) return;

            var cm = el.getColumnModel();
            if (cm == null) return;

            var jsonvalue = GridColumnsToJson(cm, undefined);
            UpdateUISetting("UI.DnsServerColumns:" + jsonvalue);
        };
        return { cellclick: $SW.ext.gridClickToDrilldown, drilldown: drilldownFn, statesave: statesaveFn };
    };
    $SW.IPAM.DnsZoneGridListeners = function (dnsOpt) {
        var _SelectServer = function (grpid) {
            var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);
            var cbox = dnsOpt.cbox, s = cbox.store, val = 'none.', idx = s.find('value', val);
            if (idx >= 0) {
                dnsOpt.list.selectedValue = $SW.IPAM.GroupBy.Last = '';
                $SW.Env["PageLoadSelection"] = { GroupId: grpid, offset: 0 };
                layout.setActiveTab(dnsOpt.tabs['servers']);
                cbox.setValue(val);
                cbox.fireEvent('select', cbox, s.getAt(idx), idx);
            }
        };
        var drilldownFn = function (grid, rowidx, colidx, e) {
            var didx = grid.getColumnModel().getDataIndex(colidx);
            var row = grid.getStore().getAt(rowidx);
            if (didx == 'ServerName')
                _SelectServer(row.data.ParentId);
            return false;
        };
        var statesaveFn = function (el, state) {
            if (el == null) return;
            if (el.id != dnsOpt.tabs['zones']) return;

            var cm = el.getColumnModel();
            if (cm == null) return;

            var jsonvalue = GridColumnsToJson(cm, undefined);
            UpdateUISetting("UI.DnsZoneColumns:" + jsonvalue);
        };

        var dnsButtonsContainer = (function () {
            var self = {};
            var isInit = false;

            self.Init = function () {
                if (!isInit) {
                    self.Edit = Ext.getCmp('btnDNSEdit');
                    self.Delete = Ext.getCmp('btnDNSDelete');
                }
            }

            self.EnableAll = function () {
                if (!isInit) {
                    self.Init();
                }

                self.Edit.enable();
                self.Delete.enable();
            }

            self.DisableAll = function () {
                for (var buttonName in self) {
                    if (self.hasOwnProperty(buttonName)) {
                        self[buttonName].disable && self[buttonName].disable();
                    }
                }
            }

            return self;
        })();

        var treeselectionchange = function (sm) {
            dnsButtonsContainer.Init();
            dnsButtonsContainer.EnableAll();

            if (sm.selections.items.length >= 1 && sm.selections.items[0].data && sm.selections.items[0].data.ServerType == 3) {
                dnsButtonsContainer.Edit.disable();
                dnsButtonsContainer.Delete.disable();
            }
        }

        var beforeselect = function (that, node) {
            if (node == null) return true;
            if (node.attributes.t == -1) {
                node.parentNode.select();
                return false;
            }
            return true;
        };

        var beforerender = function (that) {
            // hook the selection model to load the appropriate grid.
            that.getSelectionModel().on('selectionchange', treeselectionchange);
            that.getSelectionModel().on('beforeselect', beforeselect);

            return true;
        };

        return { cellclick: $SW.ext.gridClickToDrilldown, drilldown: drilldownFn, statesave: statesaveFn, beforerender: beforerender };
    };


    $SW.IPAM.DhcpAddressLeasesStateSave = function (el, state) {
        if (el == null) return;
        if (el.id != 'AddressLeases') return;

        var cm = el.getColumnModel();
        if (cm == null) return;

        var jsonvalue = GridColumnsToJson(cm, 0);
        UpdateUISetting("UI.DhcpAddressLeaseColumns:" + jsonvalue);
    }

    $SW.IPAM.DnsRecordsStateSave = function (el, state) {
        if (el == null) return;
        if (el.id != 'ZoneRecords') return;

        var cm = el.getColumnModel();
        if (cm == null) return;

        var jsonvalue = GridColumnsToJson(cm, 0);
        UpdateUISetting("UI.DnsZoneRecordsColumns:" + jsonvalue);
    }

    /* Renderers */
    $SW.IPAM.DnsServerTypeRenderer = function () {
        return function (value, meta, r) {
            var ret = "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
            if (value == "1") { ret = "Windows"; }
            if (value == "2") { ret = "BIND"; }
			if (value == "3") { ret = "Infoblox"; }
            return ret;
        };
    };

    $SW.IPAM.DhcpServerTypeTemplate = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.ChRender(values)]}</tpl>', {
                ChRender: function (v) {
                    var ret = "";
                    if (v.ViewInnerType == "0") {
                        if (v.ServerType === "1") { ret = "Windows"; }
                        if (v.ServerType === "2") { ret = "Cisco"; }
                        if (v.ServerType === "3") { ret = "ASA"; }
                        if (v.ServerType === "4") { ret = "ISC"; }
                        if (v.ServerType === "5") { ret = "Infoblox"; }
                    }
                    return ret;
                }
            });
    };

    $SW.IPAM.GetFailoverMode = function (failoverMode) {
        if (failoverMode === '1') {
            return '@{R=IPAM.Strings;K=IPAMWEBJS_FD_13;E=js}';
        }

        return '@{R=IPAM.Strings;K=IPAMWEBJS_FD_14;E=js}';
    }

    $SW.IPAM.FailoverTypeTemplate = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.ChRender(values)]}</tpl>',
            {
                ChRender: function (v) {
                    if (v.GroupIconPrefix === "dhcpserver" && v.FailoverExists === "True") {
                        return '<label class="dhcp-failover-chart"><img src="/orion/ipam/res/images/default/grid/cluster.png" class="image-centered-with-text" style="pointer-events: none"> Enabled</label>';
                    }

                    if (v.GroupIconPrefix === "scope") {
                        if (v.RelationshipName) {
                            return '<label class="dhcp-failover-chart"><img src="/orion/ipam/res/images/default/grid/cluster.png" class="image-centered-with-text" style="pointer-events: none"> '
                                + $SW.IPAM.GetFailoverMode(v.Mode) + '</label>';
                        }
                    }
                    return '';
                }
            });
    };

    $SW.IPAM.DhcpLeaseTypeTxtMap = { '0': '', '1': '@{R=IPAM.Strings;K=Enum_IPAllocPolicy_Static;E=js}', '2': '@{R=IPAM.Strings;K=Enum_IPAllocPolicy_Dynamic;E=js}' };
    $SW.IPAM.DhcpLeaseStatusTxtMap = { '0': '', '1': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Used;E=js}', '2': '@{R=IPAM.Strings;K=Enum_IPNodeStatus_Reserved;E=js}' };

    $SW.IPAM.DhcpLeaseTypeRenderer = function (highlighting) {
        return function (value, meta) {
            if (value) {
                var qtip = $SW.IPAM.DhcpLeaseTypeTxtMap[value] || null;
                if (qtip) meta.attr = 'qtip="' + qtip + '"';
                if (jQuery.isFunction(highlighting)) { qtip = highlighting(qtip, meta); }
                return qtip || "";
            }
            return "";
        };
    };
    $SW.IPAM.DhcpLeaseStatusRenderer = function (highlighting) {
        return function (value, meta) {
            if (value) {
                var qtip = $SW.IPAM.DhcpLeaseStatusTxtMap[value] || null;
                if (qtip) meta.attr = 'qtip="' + qtip + '"';
                if (jQuery.isFunction(highlighting)) { qtip = highlighting(qtip, meta); }
                return qtip || "";
            }
            return "";
        };
    };
    $SW.IPAM.DhcpIpLeaseExpiresRenderer = function (pat) {
        var fn = Ext.util.Format.dateRenderer(pat);
        return function (value, meta) {
            if (value) {
                var fntip = Ext.util.Format.dateRenderer($SW.ExtLocale.DateFull);
                value = SW.Core.Date.ConvertToDisplayDate(value);
                meta.attr = 'qtip="' + fntip(value, meta) + '"';
                return fn(value, meta);
            }
            return "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_126;E=js}";
        };
    };

    $SW.IPAM.DnsServerNameRenderer = function () {
        return function (value, meta, r) {
            var ret = "", img, d = r.data;
            if (value) {
                var pre = d.ServerIconPrefix, post = d.ServerIconPostfix;
                meta.css = 'status-bg ' + pre + '-' + post;
                meta.attr = 'qtip="' + d.ServerStatusShortDescription + '"';
                ret = "<a class='sw-grid-click' href='#' onclick='return false;'>" + Ext.util.Format.htmlEncode(value) + "</a>";
            }

            return ret;
        };
    };
    $SW.IPAM.DnsServerRenderer = function () {
        return function (value, meta, r) {
            var ret = "", img, d = r.data;
            if (value) {
                var pre = d.GroupIconPrefix, post = d.StatusIconPostfix;

                // todo: remove map up->ok
                if (post == 'up') post = 'ok';

                meta.css = 'status-bg ' + pre + '-' + post;
                meta.attr = 'qtip="' + d.StatusShortDescription + '"';

                var clickFn = String.format("onclick = 'return $SW.IPAM.SelectDNSServer(\"{0}\");'", value);
                ret = String.format("<a class='sw-grid-click' href='#' {0}>" + Ext.util.Format.htmlEncode(value) + "</a>", clickFn);
            }

            return ret;
        };
    };

    $SW.IPAM.SelectedDNSServer = null;

    $SW.IPAM.DnsZoneTypeRenderer = function () {
        return function (value, meta, r) {
            var type = { '1': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_140;E=js}", '2': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_141;E=js}", '3': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_142;E=js}" };
            return type[value] || "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
        };
    };

    $SW.IPAM.DnsLookupTypeRenderer = function () {
        return function (value, meta, r) {
            var type = { '1': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_143;E=js}", '2': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_144;E=js}" };
            return type[value] || "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
        };
    };


    $SW.IPAM.DynamicUpdateRenderer = function () {
        return function (value, meta, r) {
            var type = { '0': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_145;E=js}", '1': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_146;E=js}", '2': "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_147;E=js}" };
            return type[value] || "@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}";
        };
    };

    $SW.IPAM.DnsZoneRenderer = function () {
        return function (value, meta, r) {
            var ret = "", img, d = r.data;
            if (value) {
                var pre = d.GroupIconPrefix, post = d.StatusIconPostfix;

                // todo: remove map up->ok
                if (post == 'up') post = 'ok';

                meta.css = 'status-bg ' + pre + '-' + post;
                meta.attr = 'qtip="' + d.StatusShortDescription + '"';
                ret = Ext.util.Format.htmlEncode(value);
            }

            return ret;
        };
    };

    /* UI methods */
    $SW.IPAM.DhcpSplitterResized = function (that, adjWidth, adjHeight, rawWidth, rawHeight) {
        var jsonvalue = Ext.util.JSON.encode({ width: adjWidth });
        var params = "UI.DhcpSplitter:" + jsonvalue;
        UpdateUISetting(params);
    };

    /* Activity window methods */
    $SW.IPAM.MakeActivtyWindow = function (html) {
        var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);
        var win = $SW.IPAM.ActivityWin;

        if (!$SW.IPAM.ActivityWin) {
            win = $SW.IPAM.ActivityWin = new Ext.Window({
                layout: 'fit',
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_148;E=js}',
                autoHeight: true,
                width: 500,
                closeAction: 'hide',
                plain: false,
                border: false,
                items: new Ext.BoxComponent({
                    id: 'sw-msg-div',
                    autoHeight: true,
                    autoEl: { tag: 'div', html: html }
                })
            });
        } else {
            var msgCt = Ext.get('sw-msg-div');
            Ext.DomHelper.overwrite(msgCt, { html: html });
        }

        win.show();
        win.alignTo(layout.body.id, 'br-br', [-32, -48]);
    };

    $SW.IPAM.GetSpecificNodeDetailsUrl = function(nodeId) {
        return nodeId ? String.format("../NetPerfMon/NodeDetails.aspx?NetObject=N:{0}", nodeId) : "#";
    }

    $SW.IPAM.GetNodeDetailsUrl = function () {
        var urlParams = new URLSearchParams(window.location.search);
        return $SW.IPAM.GetSpecificNodeDetailsUrl(urlParams.get('nodeid'));
    }

    $SW.IPAM.ShowActivityWindow = function () {
        var h = $SW.IPAM.FoundHistory, t = $SW.IPAM.DnsServersForScan, s = $SW.IPAM.DhcpServersForScan, d = $SW.IPAM.DnsScanHistory, m = [];
        var scanInProgress = false;

        jQuery.each(s, function (i, o) {
            if (o.finished)
                return;

            scanInProgress = true;
            m.push(String.format(
                '<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_149;E=js}</div>' +
                '<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_150;E=js}</div>', Ext.util.Format.htmlEncode(o.name)));
        });

        jQuery.each(t, function (i, o) {
            if (o.finished)
                return;

            if (o.added)
                m.push(String.format('<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_151;E=js}</div>', Ext.util.Format.htmlEncode(o.name)));

            scanInProgress = true;
            m.push(String.format('<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_152;E=js}</div>', Ext.util.Format.htmlEncode(o.name)));
        });

        if (scanInProgress)
            m.push('<br />');

        if (h.found) m.push(String.format("<div>{0} <a href='/orion/ipam/admin/admin.dhcpscopeorphans.aspx'>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_153;E=js}</a></div>", h.found));

        jQuery.each(h.servers, function (i, o) {
            if (o.name != null) {
                m.push(String.format("<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_156;E=js}<a href='{1}' target='_blank'>{2}</a></div>", o.added, $SW.IPAM.GetSpecificNodeDetailsUrl(o.nodeId), Ext.util.Format.htmlEncode(o.name)));
            }
        });

        jQuery.each(d.data, function (i, o) {
            if (o.success)
                m.push(String.format("<div>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_154;E=js}</div>", o.name));
            else
                m.push(String.format("<div>{0}</div>", o.message));
        });

        if (scanInProgress)
            m.push('<div style="padding-top: 8px;"><i>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_155;E=js}</i></div>');

        $SW.IPAM.MakeActivtyWindow(m.join(''));
    };

    $SW.IPAM.DhcpServersForScan = {};

    $SW.IPAM.StartPeriodicChecks = function (ms, lastscope, servername) {
        var lastid = lastscope;
        var name = servername;
        $SW.IPAM.FoundHistory = { map: {}, servers: [], added: 0, found: 0 };

        function ProcessScopes(results) {
            var largest = lastid, h = $SW.IPAM.FoundHistory;

            jQuery.each(results || [], function (i, o) {
                if (o.ScopeId > largest) largest = o.ScopeId;
                var s = h.map[o.ServerId];
                if (!s) h.servers.push(h.map[o.ServerId] = s = { name: o.ServerName, found: 0, added: 0, id: o.ServerId, nodeId: o.NodeId });
                if (o.GroupId) { s.added++; h.added++; }
                else { s.found++; h.found++; }
            });


            if (lastid != largest) {
                if (Ext.isEmpty(name)) return;

                var k = $SW.IPAM.DhcpServersForScan;
                k[name].finished = true;

                var mgr = $SW.IPAM.manager.ActiveTabManager();
                if (mgr && mgr.ID && (mgr.ID === 'dhcpscopes' || mgr.ID === 'dhcpservers') && mgr.RefreshGridPage) mgr.RefreshGridPage();

                $SW.IPAM.ShowActivityWindow();
                lastid = largest;
            }
        }

        function LicenseRefreshTask() {
            $SW.DoQuery(
                "select distinct S.ScopeId, G.GroupId, G.Parent.GroupId as ServerId, S.NodeId, G.Parent.FriendlyName as ServerName, ISNULL( G.FriendlyName, '') AS GroupName from IPAM.DhcpScope S " +
                "left join IPAM.GroupNode G on S.GroupId = G.GroupId and G.Distance = 0 " +
                "where S.ScopeId > " + lastid + " " +
                "order by S.ScopeId ASC "
                , ProcessScopes);
        }

        if (servername) {
            var t = $SW.IPAM.DhcpServersForScan;
            t[servername] = { name: servername, finished: false };

            $SW.IPAM.ShowActivityWindow();
            // To display pop-up above activity window.
            $(".x-shadow").css("z-index", "1002");
            $(".x-window").css("z-index", "1003");
        }

        _dhcpScanTaskData = { run: LicenseRefreshTask, interval: ms };
        Ext.TaskMgr.start(_dhcpScanTaskData);
    };

    $SW.IPAM.DnsServersForScan = {};
    $SW.IPAM.DnsScanHistory = { map: {}, data: [] };

    $SW.IPAM.StartPeriodicDnsScanChecks = function (serverId, friendlyName, lastDiscovery, interval, added) {
        if (serverId < 0)
            return;

        var o = $SW.IPAM.DnsScanHistory;
        var map = o.map[serverId];
        if (map) {
            var idx = o.data.indexOf(map);
            if (idx != -1) o.data.splice(idx, 1);
            delete o.map[serverId];
        }

        function ProcessLastUpdate(results) {
            if (!results || !results.data)
                return;

            var data = results.data;

            if (!data.IsFinished)
                return;

            var groupId = data.GroupId;

            var l = $SW.IPAM.DnsServersForScan;
            var server = l[groupId];
            if (!server)
                return;

            var h = $SW.IPAM.DnsScanHistory;
            var s = h.map[groupId];
            if (!s) h.data.push(h.map[groupId] = { name: server.name, success: data.ScanSuccess, message: data.ErrorMessage });

            server.finished = true;

            var mgr = $SW.IPAM.manager.ActiveTabManager();
            if (mgr && mgr.ID && (mgr.ID === 'dnszones' || mgr.ID === 'dnsservers') && mgr.RefreshGridPage) mgr.RefreshGridPage();

            $SW.IPAM.ShowActivityWindow();
        }

        function LastUpdateRefreshTask() {
            var groupId = this.args[0];

            var l = $SW.IPAM.DnsServersForScan;
            var server = l[groupId];
            if (!server)
                return;

            if (server.finished)
                return;

            var fail = $SW.ext.AjaxMakeFailDelegate('Get DNS Server Scan status');
            var pass = $SW.ext.AjaxMakePassDelegate('Get DNS Server Scan status', ProcessLastUpdate);

            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                success: pass,
                failure: fail,
                timeout: fail,
                params:
                {
                    entity: 'IPAM.DnsServer',
                    verb: 'GetScanStatus',
                    GroupId: server.groupId,
                    LastUpdate: server.lastUpdate
                }
            });
        }

        if (friendlyName) {
            var t = $SW.IPAM.DnsServersForScan;
            t[serverId] = { groupId: serverId, name: friendlyName, lastUpdate: lastDiscovery, finished: false, added: added };

            $SW.IPAM.ShowActivityWindow();
        }

        _dnsScanTaskData = { run: LastUpdateRefreshTask, interval: interval, args: [serverId] };
        Ext.TaskMgr.start(_dnsScanTaskData);
    };


    /*Integrated DHCP Tab graph view */
    $SW.IPAM.DHCPGraphView = function (el) {

        var swqlSelect = "SELECT SUM(A.UsedCount) AS UsedCount, SUM(A.AvailableCount) AS AvailableCount," +
            " SUM(A.ReservedCount)AS ReservedCount, SUM(A.TransientCount) AS TransientCount, " +
            " SUM(A.TotalCount) AS TotalCount ,SUM(A.AllocSize) AS AllocSize " +
            " FROM IPAM.GroupNode A {0}";

        var windowTitle = "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_396;E=js}";

        var layout = Ext.getCmp($SW.IPAM.GroupBy.TabLayout);
        var activeTab = layout.getActiveTab();
        var selModel = activeTab.getSelectionModel();
        var items = selModel.getSelectedNodes();
        if (items.length > 0) {
            var list = [];
            var nameList = [];
            Ext.each(items, function (o) {
                if (o.id) {
                    list.push(o.id);
                    nameList.push(o.attributes.FriendlyName);
                }
            });

            if (nameList.length <= 3)
                windowTitle = String.format(windowTitle, nameList.join(','));
            else
                windowTitle = String.format(windowTitle, (activeTab.id == "tabScopes" ? String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_157;E=js}", list.length) : String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_158;E=js}", list.length)));

            swqlSelect = String.format(swqlSelect, "WHERE A.Distance=0 AND A.GroupId IN (" + list.join(',') + ")");
        } else {
            var whereclause;
            var key = Ext.getCmp('DhcpGroupBySelect').getValue();
            var v = $SW.IPAM.GroupBy.DhcpDataSet.map[key];
            var selValue = Ext.getCmp('GroupByList').selectedValue;

            if (v.sequence != "none") {
                var constraint = String.format(v.constraint.val(), "A");
                if (constraint == "A.ViewType=1") {
                    constraint = "A.GroupType = 32";
                }
                if (selValue == "" && (v.groupby == "VLAN" || v.groupby == "Location")) {
                    selValue = " IS NULL OR " + v.groupby + "= ''";
                }
                else {
                    selValue = " = '" + selValue + "'";
                }
                whereclause = String.format("WHERE A.Distance=0 AND {0} AND {1}", constraint, "A." + v.groupby + selValue);
                windowTitle = String.format(windowTitle, activeTab.id == 'tabScopes' ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_159;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_160;E=js}");
            } else {
                whereclause = " WHERE A.Distance=0 AND A.GroupType =" + (activeTab.id == "tabScopes" ? '64' : '32');
                windowTitle = String.format(windowTitle, activeTab.id == 'tabScopes' ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_161;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_162;E=js}");
            }

            swqlSelect = String.format(swqlSelect, whereclause);
        }
        // display loading
        $('#reportimage')[0].src = "/orion/ipam/res/images/default/s.gif";
        $('#reportcontent').addClass('x-hide-display');
        $('#reportloading').removeClass('x-hide-display');

        $SW.DoQuery(swqlSelect, function (values) {
            if (!values) {
                $('#reportloading,#reportcontent').addClass('x-hide-display');
                $('#reportfailure').removeClass('x-hide-display');

                // if the session has expired, offer to relogin.
                $SW.ext.AjaxIsErrorOrExpire({ cmd: 'Retrieve Subnet Statistics', text: responseObject.responseText });
            } else {
                var data = values[0];
                var img = $('#reportimage')[0];
                var w = $(img).attr('xW');
                var h = $(img).attr('xH');
                var ttl = parseFloat(data.TotalCount);

                Ext.each(
                    ["UsedCount", "AvailableCount", "ReservedCount", "TransientCount", "TotalCount", "AllocSize"],
                    function (s) { if (isNaN(data[s] = parseFloat(data[s]))) data[s] = 0; }
                );

                data.UsedCount = parseInt(data.UsedCount);

                var vals = [
                    { n: data.UsedCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_39;E=js}", c: "F8D677", cls: 'ip-used' },
                    //{ n: data.ReservedCount, t: "Reserved IP addresses", c: "A492C4", cls: 'ip-availrsvd' },
                    { n: data.AvailableCount, t: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_41;E=js}", c: "7CC364", cls: 'ip-avail' }
                    //,
                    //{ n: data.TransientCount, t: "Transient IP addresses", c: "13CFE3", cls: 'ip-transient' }
                ];

                vals.sort(function (o1, o2) { return o1.n - o2.n; });

                var colors = '';
                var numbers = '';

                if (data.TotalCount == 0) {
                    colors = "FEFEFE";
                    numbers = "1";
                } else {
                    Ext.each(vals, function (o) {
                        if (!o.n) return;
                        if (colors.length) colors += '|';
                        if (numbers.length) numbers += '|';
                        colors += o.c;
                        numbers += o.n;
                    });
                }

                var src = String.format("ImgChartProvider.ashx?w={0}&h={1}&d={2}&c={3}", w, h, numbers, colors);
                img.src = src;

                vals.sort(function (o1, o2) { return o2.n - o1.n; });
                var table = "<table style='' border='0' cellspacing='0' cellpadding='0'>";

                var fn = $SW.subnet.ipUsageRenderer(2);

                Ext.each(vals, function (o) {
                    table += String.format("<tr><td class=rptcount><span>{1}</span></td><td class='rptlabel {0}'><div>{2} ({3})</div></td></tr>", o.cls, o.n, o.t, ttl ? fn(o.n / ttl * 100) : "0%");
                });

                table += String.format("<tr class=rptsum><td class=rptcount><span>{0}</span></td><td class=rptlabel><div>{1}</div></td></tr>", data.TotalCount, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_36;E=js}');

                // -- explanation

                table += "<tr><td>&nbsp;</td></tr>";
                table += "</table><table style='table-layout: fixed; width: 100%;' border='0' cellspacing='0' cellpadding='0'>";
                table += String.format("<tr><td class=rptcount><span>{0}</span></td><td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_163;E=js}</td></tr>", data.TotalCount ? fn(data.AvailableCount / data.TotalCount * 100) : "0%");
                table += String.format("<tr><td class=rptcount><span>{0}</span></td><td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_164;E=js}</td></tr>", data.TotalCount ? fn((data.TotalCount - data.AvailableCount) / data.TotalCount * 100) : "0%");

                $('#reportlegend').html(table + "</table>");
                $('#reportloading,#reportfailure').addClass('x-hide-display');
                $('#reportcontent').removeClass('x-hide-display');
            }
        });

        var win = Ext.getCmp('DhcpReportWindow');
        if (!win) {
            win = new Ext.Window({
                id: 'DhcpReportWindow',
                title: windowTitle,
                closeAction: 'hide',
                closable: true,
                width: 800,
                height: 350,
                modal: true,
                layout: 'fit',
                cls: 'sw-window',
                contentEl: 'GraphViewContent',
                constrainHeader: true,
                buttons: [{
                    text: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_165;E=js}',
                    handler: function () { win.hide(); }
                }]
            });
        } else
            win.setTitle(windowTitle);

        win.show(el);

    };

    $SW.IPAM.DhcpScopeChartTTip = function (config) { Ext.apply(this, config); };

    $SW.IPAM.DhcpFailoverScopeChartTTip = function (config) { Ext.apply(this, config); };

    $SW.IPAM.DhcpScopeChartTTip.TTipTpl = new Ext.XTemplate(
        '<table cellspacing="3" cellpadding="0">',
        '<tr><th colspan="4">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_17;E=js}</th></tr>',
        '<tr>',
        '<td class="subnet-width"><div class="subnet">&nbsp;</td></td>',
        '<td colspan="3" class="start">',
        '<a href="subnets.aspx?opento={subnet}&filter=ffDynamic">{address} /{cidr}</a>',
        '</td>',
        '</tr>',
        '<tr><th colspan="4">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_18;E=js}</th></tr>',
        '<tpl if="this.isEmpty(rang) && ServerType == 4"><tr><td colspan="4" class="r-no-ranges-hint">@{R=IPAM.Strings;K=IPAMWEBJS_MR1_13;E=js}</td></tr></tpl>',
        '<tpl for="rang"><tr>',
        '<td><div class="range">&nbsp;</div></td>',
        '<td class="start">{start}</td>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_395;E=js}</td>',
        '<td class="end">{end}</td>',
        '</tr></tpl>',
        '<tpl if="ServerType != 4"><tr><th colspan="4">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_19;E=js}</th></tr>',
        '<tpl for="excl"><tr>',
        '<td><div class="exclusion">&nbsp;</div></td>',
        '<td class="start">{start}</td>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_395;E=js}</td>',
        '<td class="end">{end}</td>',
        '</tr></tpl></tpl>',
        '<tpl if="ServerType == 4 || ServerType == 3"><tr><th colspan="4">@{R=IPAM.Strings;K=IPAMWEBJS_MR1_01;E=js}</th></tr>',
        '<tpl for="pool"><tr>',
        '<td><div class="pool">&nbsp;</div></td>',
        '<td class="start">{start}</td>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_VB1_395;E=js}</td>',
        '<td class="end">{end}</td>',
        '</tr></tpl></tpl>',
        '<tr><th colspan="4">@{R=IPAM.Strings;K=IPAMWEBJS_OH1_20;E=js}</th></tr>',
        '</table>',
        '<div class="r-scopes">',
        '<img src="/Orion/IPAM/res/images/sw-resources/chart-loading.gif" alt=""/>',
        '</div>',
        {
            isEmpty: function (data) {
                return Ext.isEmpty(data);
            }
        }
    );

    $SW.IPAM.DhcpFailoverScopeChartTTip.TTipTpl = new Ext.XTemplate(
        '<table cellspacing="0" cellpadding="7" class="dhcp-failover-scope-chart">',
        '<tr>',
        '<th>@{R=IPAM.Strings;K=IPAMWEBJS_FD_01;E=js}</th>',
        '<th></th>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_02;E=js}:</td>',
        '<td>{relationshipName}</td>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_03;E=js}:</td>',
        '<tpl if="isNodeId">',
        '<td><span><img class="StatusIcon" alt="Down" src="{iconPath}" style="width: 16px; height: 16px;">&nbsp;' +
        '<a class="sw-grid-click" href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:{nodeId}">' +
        '{partnerServer}</a>',
        '</td>',
        '</tpl>',
        '<tpl if="!isNodeId">',
        '<td>{partnerServer}</td>',
        '</tpl>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_04;E=js}:</td>',
        '<td>{maxClientLeadTimeInSeconds}</td>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_05;E=js}:</td>',
        '<td>{mode}</td>',
        '</tr>',
        '<tpl if="isHotStandby">',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_06;E=js}:</td>',
        '<td>{failoverServerType}</td>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_07;E=js}:</td>',
        '<td>{percentage} %</td>',
        '</tr>',
        '</tpl>',
        '<tpl if="!isHotStandby">',
        '<tr>',
        '<td>%&nbsp;@{R=IPAM.Strings;K=IPAMWEBJS_FD_08;E=js}:</td>',
        '<td>{percentage} %</td>',
        '</tr>',
        '<tr>',
        '<td>%&nbsp;@{R=IPAM.Strings;K=IPAMWEBJS_FD_09;E=js}:</td>',
        '<td>{partnerPercentage} %</td>',
        '</tr>',
        '</tpl>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_10;E=js}:</td>',
        '<td>{safePeriodInSeconds}</td>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_11;E=js}:</td>',
        '<td>{state}</td>',
        '</tr>',
        '<tr>',
        '<td>@{R=IPAM.Strings;K=IPAMWEBJS_FD_12;E=js}:</td>',
        '<td>{stateOfPartnerServer}</td>',
        '</tr>',
        '</table>',
        {
            isEmpty: function (data) {
                return Ext.isEmpty(data);
            }
        }
    );

    //Compare "a" and "b" in some fashion, and return -1, 0, or 1
    var rangeSort = function (a, b) {
        return (a.ns != b.ns) ? (a.ns - b.ns) : (a.ne - b.ne);
    };

    $SW.IPAM.DhcpScopeChartTTip.TTipDataExt = function (row) {
        var ttip = { rang: [], excl: [], pool: [] }, data = Ext.util.JSON.decode(row.attributes.ScopeIPRanges);
        ttip.address = row.attributes.Address;
        ttip.cidr = row.attributes.CIDR;
        ttip.subnet = row.attributes.SubnetID;
        ttip.ServerType = row.parentNode.attributes.ServerType;

        var poolData = Ext.util.JSON.decode(row.attributes.PoolData);
        var a = $SW.IP.v4(ttip.address), a_n = $SW.IP.v4_toint(a);

        Ext.each(data,
            function (d, i) {
                var range = {
                    ns: a_n + d.s,
                    ne: a_n + d.e, // sorting purpose
                    start: $SW.IP.v4_fromint(a_n + d.s).join('.'),
                    end: $SW.IP.v4_fromint(a_n + d.e).join('.')
                };
                if (d.t == 1 && (d.p == "" || d.p == -1)) ttip.excl.push(range);
                if (d.t == 2 && (d.p == "" || d.p == -1)) ttip.rang.push(range);
            });

        Ext.each(poolData,
            function (d, i) {
                var range = {
                    start: d.s,
                    end: d.e
                };
                if (d.t == 3) ttip.pool.push(range);
            });

        ttip.excl.sort(rangeSort);
        ttip.rang.sort(rangeSort);

        return ttip;
    };

    var getTime = function (totalSeconds) {
        var hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        var minutes = Math.floor(totalSeconds / 60);

        return "{0} hour(s) {1} minute(s)".format(hours, minutes);
    };

    /*prototype for string format*/
    String.prototype.format = function () {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };

    $SW.IPAM.DhcpFailoverScopeChartTTip.TTipDataExt = function (row, ttip, tip, tGrid, selectedNode) {
        var data = { rang: [], excl: [], pool: [] };

        var fail = $SW.ext.AjaxMakeFailDelegate('Get Failover Info');
        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            failure: fail,
            timeout: fail,
            params: {
                entity: 'IPAM.DhcpServer',
                verb: 'GetFailoverInfoBySubnetIp',
                SubnetId: row.id
            },
            success: function (msg) {
                var ret;
                try {
                    ret = Ext.util.JSON.decode(msg.responseText);
                    if (ret.data) {
                        data.isNodeId = ret.data.NodeId > 0;
                        data.isHotStandby = ret.data.Mode == "HotStandby";
                        var isPrimaryServer = ret.data.FailoverServerType == "PrimaryServer";

                        data.groupId = row.id;
                        data.primaryServer = ret.data.PrimaryServer;
                        data.secondaryServer = ret.data.SecondaryServer;
                        data.mode = data.isHotStandby ? "@{R=IPAM.Strings;K=IPAMWEBJS_FD_13;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_FD_14;E=js}";
                        data.failoverServerType = isPrimaryServer ? "@{R=IPAM.Strings;K=IPAMWEBJS_FD_15;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_FD_16;E=js}";
                        data.state = getStateOfServer(ret.data.State);
                        data.maxClientLeadTimeInSeconds = getTime(ret.data.MaxClientLeadTimeInSeconds);
                        data.safePeriodInSeconds = ret.data.SafePeriodInSeconds >= 0 ? (ret.data.SafePeriodInSeconds / 60) + " @{R=IPAM.Strings;K=IPAMWEBJS_FD_19;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_FD_18;E=js}";
                        data.relationshipName = ret.data.RelationshipName;
                        data.primaryServerName = ret.data.PrimaryServerName;
                        data.secondaryServerName = ret.data.SecondaryServerName;
                        data.percentage = data.isHotStandby ? (isPrimaryServer ? 100 - ret.data.Percentage : ret.data.Percentage) : ret.data.Percentage;
                        data.partnerPercentage = !data.isHotStandby ? (100 - ret.data.Percentage) : ret.data.Percentage;
                        data.sharedSecret = ret.data.SharedSecret;
                        data.partnerServer = isPrimaryServer ? ret.data.SecondaryServer : ret.data.PrimaryServer;

                        if (data.isNodeId) {
                            data.iconPath = ret.data.PartnerStatus === 1 ? "/Orion/images/StatusIcons/Small-Up.gif" : "/Orion/images/StatusIcons/Small-Down.gif";
                            data.nodeId = ret.data.NodeId;
                        }

                        data.stateOfPartnerServer = getStateOfServer(ret.data.State);

                        formatFailoverChartToolTip(data, ttip, tip, tGrid, selectedNode);
                    }

                } catch (ex) {
                    $SW.ext.AjaxIsErrorOrExpire({ cmd: "Move Item", text: msg });
                }
            }
        });
    };

    var getStateOfServer = function (state) {
        var serserState;
        switch (state) {
            case 0:
                serserState = "No State";
                break;
            case 1:
                serserState = "Init";
                break;
            case 2:
                serserState = "Startup";
                break;
            case 3:
                serserState = "Normal";
                break;
            case 4:
                serserState = "Communication Int";
                break;
            case 5:
                serserState = "Partner Down";
                break;
            case 6:
                serserState = "Potential Conflict";
                break;
            case 7:
                serserState = "Conflict Done";
                break;
            case 8:
                serserState = "Resolution Int";
                break;
            case 9:
                serserState = "Recover";
                break;
            case 10:
                serserState = "Recover Wait";
                break;
            case 11:
                serserState = "Recover Done";
                break;
            case 12:
                serserState = "Paused";
                break;
            case 13:
                serserState = "Shutdown";
                break;
            default:
                serserState = "No State";
        }

        return serserState;
    };

    $SW.IPAM.CheckDeconfigureFailoverCapability = function (scopeId, relationshipName, deconfigureContainer) {
        if (!scopeId) {
            return;
        }
        var query = 'SELECT COUNT(G.GroupId) as ManagedByIPAM FROM IPAM.GroupNode G ' + 
					'JOIN IPAM.DhcpServer S2 ON G.SecondaryServer = S2.FoundAddress ' + 
					'JOIN IPAM.DhcpServer S1 ON G.PrimaryServer = S1.FoundAddress ' + 
					'WHERE G.GroupId = ' + scopeId;

        $SW.DoQuery(query, function (values) {
            if ((values != null && values[0].ManagedByIPAM > 1) && (relationshipName && relationshipName.length > 0)) {
                deconfigureContainer.enable();
            }
        });
    };

    /*Tool Tip for DHCP Tab Chart */
    $SW.IPAM.ChartToolTip = function (config) {
        Ext.apply(this, config);
    };

    $SW.IPAM.FailoverChartToolTip = function (config) {
        Ext.apply(this, config);
    };

    $SW.IPAM.FailoverChartToolTip.TTipScopesTpl = new Ext.XTemplate(
        '<table cellspacing="3" cellpadding="0">',
        '<tpl for="."><tr>',
        '</table>',
        { mapUpToOk: function (s) { return s == 'up' ? 'ok' : s; } }
    );

    $SW.IPAM.ChartToolTip.TTipScopesTpl = new Ext.XTemplate(
        '<table cellspacing="3" cellpadding="0">',
        '<tpl for="."><tr>',
        '<td class="status-bg {type}-{status:this.mapUpToOk}">',
        '<div class="x-grid3-cell-inner">',
        '<a class="sw-grid-click" href="Dhcp.Management.aspx?opento={name}&tab=scopes">{name}</a>',
        '</div>',
        '</td>',
        '<td class="r-scopes-server">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_403;E=js}</td>',
        '<td class="status-bg {stype}-{sstatus}">',
        '<div class="x-grid3-cell-inner">{sname}</div>',
        '</td>',
        '</tr></tpl>',
        '</table>',
        { mapUpToOk: function (s) { return s == 'up' ? 'ok' : s; } }
    );

    $SW.IPAM.FailoverChartToolTip.TTipScopesExt = function (d) {
        var sId = d.attributes.SubnetID, gId = d.attributes.GroupId;

        var query = 'SELECT A.GroupId as id, (A.Parent.GroupId) as sid,' +
            'A.FriendlyName as name,' +
            'GroupIconPrefix as type,' +
            'StatusIconPostfix as status,' +
            '(A.Parent.FriendlyName) as sname,' +
            '(A.Parent.GroupIconPrefix) as stype,' +
            '(A.Parent.StatusIconPostfix) as sstatus' +
            ' FROM IPAM.GroupNode A' +
            ' WHERE A.Distance = 0 AND A.GroupType=64 AND' +
            " A.Scope.SubnetId='" + sId + "' AND A.GroupId <> '" + gId + "'";

        $SW.DoQuery(query, function (values) {
            var content = '<p class="r-scopes-hint">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_394;E=js}</p>';
            if (values != null && values.length > 0) {
                content = $SW.IPAM.FailoverChartToolTip.TTipScopesTpl.apply(values);
            }

            $('.r-scopes').empty();
            $('.r-scopes').append(content);
        });
    };

    $SW.IPAM.ChartToolTip.TTipScopesExt = function (d) {
        var sId = d.attributes.SubnetID, gId = d.attributes.GroupId;

        var query = 'SELECT A.GroupId as id, (A.Parent.GroupId) as sid,' +
            'A.FriendlyName as name,' +
            'GroupIconPrefix as type,' +
            'StatusIconPostfix as status,' +
            '(A.Parent.FriendlyName) as sname,' +
            '(A.Parent.GroupIconPrefix) as stype,' +
            '(A.Parent.StatusIconPostfix) as sstatus' +
            ' FROM IPAM.GroupNode A' +
            ' WHERE A.Distance = 0 AND A.GroupType=64 AND' +
            " A.Scope.SubnetId='" + sId + "' AND A.GroupId <> '" + gId + "'";

        $SW.DoQuery(query, function (values) {
            var content = '<p class="r-scopes-hint">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_394;E=js}</p>';
            if (values != null && values.length > 0) {
                content = $SW.IPAM.ChartToolTip.TTipScopesTpl.apply(values);
            }

            $('.r-scopes').empty();
            $('.r-scopes').append(content);
        });
    };

    $SW.IPAM.ChartToolTip.TTipRender = function (treeGrid) {
        if (treeGrid.ttip) return;

        treeGrid.ttip = new Ext.ToolTip({
            target: treeGrid.el,                    // the overall target element.
            baseCls: 'custom-x-tip',            // custom css style
            dismissDelay: 0,                    // disable automatic hiding
            showDelay: 0,                       // delay before the tooltip displays
            delegate: '.dhcp-scope-chart',   // each grid row causes its own seperate show and hide.
            trackMouse: false,                  // moving within the row should not hide the tip.
            anchor: 'left',
            autoWidth: true,
            autoHide: false,
            renderTo: Ext.getBody(),            // render immediately so that tip.body can be referenced prior to the first show.
            listeners: {                        // change content dynamically depending on which element triggered the show.
                beforeshow: function updateTipBody(tip) {
                    if (tip.triggerElement.className != "x-treegrid-col dhcp-scope-chart") return false;
                    var tEl = tip.triggerElement, tElParent = tip.triggerElement.firstChild,
                        tipBody = $(tElParent).children('.scope-ttip');

                    var treeGrid1 = Ext.getCmp('tabScopes');
                    var selectedNode = treeGrid1.getNodeById(tEl.parentNode.attributes.getNamedItem('ext:tree-node-id').value);
                    if (selectedNode != null) {
                        if (selectedNode.attributes.ScopeIPRanges == "") {
                            return false;
                        }
                        var ttip = $SW.IPAM.DhcpScopeChartTTip,
                            data = ttip.TTipDataExt(selectedNode);
                        var content = ttip.TTipTpl.apply(data);
                        tip.update(content);
                        window.clearTimeout(treeGrid.ttip_rscopes);
                        treeGrid.ttip_rscopes = setTimeout(function () {
                            $SW.IPAM.ChartToolTip.TTipScopesExt(selectedNode);
                        }, 1000);
                    }
                }
            }
        });
    };

    $SW.IPAM.FailoverChartToolTip.TTipRender = function (tGrid) {
        tGrid.ttip = new Ext.ToolTip({
            target: tGrid.el, // the overall target element.
            baseCls: 'custom-x-tip', // custom css style
            dismissDelay: 1000, // disable automatic hiding
            showDelay: 500, // delay before the tooltip displays
            delegate: '.dhcp-failover-chart', // each grid row causes its own seperate show and hide.
            trackMouse: false, // moving within the row should not hide the tip.
            anchor: 'left',
            autoWidth: true,
            autoHide: false,
            hideDelay: 200,
            renderTo: Ext.getBody(), // render immediately so that tip.body can be referenced prior to the first show.
            listeners: { // change content dynamically depending on which element triggered the show.
                beforeshow: function updateTipBody(tip) {
                    tip.el.shadow = null; // To not display shadows in pop-ups.

                    var failOverType = tip.triggerElement.childNodes[1].nodeValue;
                    var loadingToolTipHeight = (failOverType === ' Enabled') ? 450 : ((failOverType === ' Hot Standby') ? 362 : 342); //Fix loading tooltip height to avoid anchor and tooltip split in the bottom of screen
                    tip.update('<div style="width: 400px; height: {0}px; background-color: white;">Loading...</div>'.format(loadingToolTipHeight));

                    var tEl = tip.triggerElement,
                        tElParent = tip.triggerElement.firstChild,
                        tipBody = $(tElParent).children('.scope-ttip');

                    var treeGrid1 = Ext.getCmp('tabScopes');
                    var selectedNode =
                        treeGrid1.getNodeById(tEl.parentNode.parentNode.parentNode.attributes.getNamedItem('ext:tree-node-id').value);
                    if (selectedNode != null) {

                        if (selectedNode.attributes.ServerType == 1 && selectedNode.attributes.FailoverExists == "True") {
                            $('.custom-x-tip-body').css("max-height", 450);
                            $('.custom-x-tip-body').css("overflow-y", "auto");
                            $('.custom-x-tip').css("z-index", 1002);
                            var serverTtip = $SW.IPAM.DhcpServerFailoverChartToolTip;
                            serverTtip.TTipDataExt(selectedNode, serverTtip, tip, tGrid, selectedNode);
                            return true;
                        }

                        if (selectedNode.attributes.ScopeIPRanges == "") {
                            return false;
                        }


                        if (!selectedNode.attributes.RelationshipName) {
                            return false;
                        }

                        var ttip = $SW.IPAM.DhcpFailoverScopeChartTTip;
                        ttip.TTipDataExt(selectedNode, ttip, tip, tGrid, selectedNode);

                        return true;
                    }
                }
            }
        });
    };

    var formatFailoverChartToolTip = function (data, ttip, tip, tGrid, selectedNode) {
        var content = ttip.TTipTpl.apply(data);
        tip.update(content);
        window.clearTimeout(tGrid.ttip_rscopes);

        tGrid.ttip_rscopes = setTimeout(function () {
            $SW.IPAM.FailoverChartToolTip.TTipScopesExt(selectedNode);}, 1000);
    };


    $SW.IPAM.DhcpServerFailoverChartToolTip = function (config) { Ext.apply(this, config); };

    $SW.IPAM.DhcpServerFailoverChartToolTip.TTipDataExt = function (row, ttip, tip, tGrid, selectedNode) {

        var data = { rang: [], excl: [], pool: [] };

        var popupMarkup = "";

        var fail = $SW.ext.AjaxMakeFailDelegate('Get Failover Info');
        Ext.Ajax.request({
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
            failure: fail,
            timeout: fail,
            params: {
                entity: 'IPAM.DhcpServer',
                verb: 'GetDhcpServerFailovers',
                GroupId: row.id
            },
            success: function (msg) {
                var ret;
                try {
                    ret = Ext.util.JSON.decode(msg.responseText);

                    if (ret.data != null && ret.data != undefined) {
                        var index;

                        for (index = 0; index < ret.data.length; ++index) {

                            var isHotStandby = ret.data[index].Mode == "1";
                            var mode = isHotStandby ? "@{R=IPAM.Strings;K=IPAMWEBJS_FD_13;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_FD_14;E=js}";

                            var primaryIconPath = ret.data[index].PrimaryServerStatus === 0 ? "/Orion/images/StatusIcons/Small-Up.gif" : "/Orion/images/StatusIcons/Small-Down.gif";
                            var secondaryIconPath = ret.data[index].SecondaryServerStatus === 0 ? "/Orion/images/StatusIcons/Small-Up.gif" : "/Orion/images/StatusIcons/Small-Down.gif";

                            popupMarkup += '<tr><td colspan="2"><b>' + ret.data[index].RelationshipName + ' (' + mode + ')' + '</b></td></tr>';

                                popupMarkup += '<td><span><img class="StatusIcon" alt="Down" src=' + secondaryIconPath
                                    + ' style="width: 10px; height: 10px;">&nbsp;' + '<a class="sw-grid-click" href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:'
                                    + ret.data[index].PrimaryServerNodeId + '">' + ret.data[index].PrimaryServerName + '</a></td>';
                                popupMarkup +=
                                    '<td><span><img class="StatusIcon" alt="Down" src=' + primaryIconPath
                                    + ' style="width: 10px; height: 10px;">&nbsp;' + '<a class="sw-grid-click" href="/Orion/NetPerfMon/NodeDetails.aspx?NetObject=N:'
                                    + ret.data[index].SecondaryServerNodeId + '">' + ret.data[index].SecondaryServerName + '</a></td></tr>';
                                if (isHotStandby) {
                                    popupMarkup += '<tr><td><b style="margin-left:3em">ACTIVE</b></td><td><b style="margin-left:3em">STANDBY</b></td></tr>';

                                } else {
                                    popupMarkup += '<tr><td><b style="margin-left:3em">' + ret.data[index].LoadBalanceLocalServerPercentage + '%</b></td><td><b style="margin-left:3em">' + ret.data[index].LoadBalancePartnerServerPercentage + '%</b></td></tr>';
                                }

                            var scopePairs = ret.data[index].FailoverScopesData;

                            for (y = 0; y < scopePairs.length; ++y) {

                                var scopePrimaryIconPath = scopePairs[y].FirstScopeStatus === 1 ? "/Orion/images/StatusIcons/Small-Up.gif" : "/Orion/images/StatusIcons/Small-Down.gif";
                                var scopeSecondaryIconPath = scopePairs[y].SecondScopeStatus === 1 ? "/Orion/images/StatusIcons/Small-Up.gif" : "/Orion/images/StatusIcons/Small-Down.gif";

                                popupMarkup += '<tr><td><span style="margin-left:1em"><img class="StatusIcon" alt="Down" src=' + scopePrimaryIconPath + ' style="width: 8px; height: 8px;">&nbsp;' + scopePairs[y].FirstScopeFriendlyName + '</span> </td>';
                                popupMarkup += '<td><span style="margin-left:1em"><img class="StatusIcon" alt="Down" src=' + scopeSecondaryIconPath + ' style="width: 8px; height: 8px;">&nbsp;' + scopePairs[y].SecondScopeFriendlyName + '</span></td></tr>';

                            }

                            popupMarkup += "<tr><th></th><th></th></tr>";
                        }
                        data.popupMarkup = popupMarkup;
                        formatFailoverChartToolTip(data, ttip, tip, tGrid, selectedNode);
                    }

                } catch (ex) {
                    $SW.ext.AjaxIsErrorOrExpire({ cmd: "Move Item", text: msg });
                }
            }
        });
    };

    $SW.IPAM.DhcpServerFailoverChartToolTip.TTipTpl = new Ext.XTemplate(
        '<table cellspacing="0" cellpadding="6" class="dhcp-failover-server-chart" style=" overflow-y: scroll;">',
        '<tr>',
        '<th>@{R=IPAM.Strings;K=IPAMWEBJS_FD_20;E=js}:</th>',
        '<th></th>',
        '{popupMarkup}',
        '</table>',
        {
            isEmpty: function (data) {
                return Ext.isEmpty(data);
            }
        }
    );

    Ext.extend($SW.IPAM.DhcpFailoverScopeChartTTip, Ext.util.Observable, {
        init: function (grid) {
            grid.on('render', $SW.IPAM.DhcpFailoverScopeChartTTip.TTipRender);
        }
    });

    Ext.extend($SW.IPAM.DhcpServerFailoverChartToolTip, Ext.util.Observable, {
        init: function (tGrid) {
            tGrid.on('render', $SW.IPAM.DhcpServerFailoverChartToolTip.TTipRender);
        }
    });

    Ext.extend($SW.IPAM.DhcpScopeChartTTip, Ext.util.Observable, {
        init: function (grid) { grid.on('render', $SW.IPAM.DhcpScopeChartTTip.TTipRender); }
    });

    //Failover View Tool Tip
    Ext.extend($SW.IPAM.FailoverChartToolTip, Ext.util.Observable, {
        init: function (tGrid) {
            tGrid.on('render', $SW.IPAM.FailoverChartToolTip.TTipRender);
        }
    });

    //TreeGrid View Tool Tip
    Ext.extend($SW.IPAM.ChartToolTip, Ext.util.Observable, {
        init: function (treeGrid) { treeGrid.on('render', $SW.IPAM.ChartToolTip.TTipRender); }
    });

    $SW.IPAM.DhcpServerGridSelFn = function (addScopeBtn) {
        return function (sm) {
            var btn = Ext.getCmp(addScopeBtn); btn.disable();
            if (sm.getCount() > 1) return;
            if (sm.getCount() < 1) { btn.enable(); return; }

            var type = sm.getSelected().data['A.Server.ServerType'];
            // check capability of add scope
            if ($SW['dhcpCapability'].test(type, 'IScopeManagement')) btn.enable();
        };
    };
    $SW.IPAM.DhcpScopeGridSelFn = function (splitScopeBtn) {
        return function (sm) {
            var btn = Ext.getCmp(splitScopeBtn); btn.disable();

            if (sm.getCount() != 1) return;
            var type = sm.getSelected().data['ServerType'];
            var cidr = sm.getSelected().data['CIDR'];

            if (cidr > 30) return;

            // check capability of split scope
            if ($SW['dhcpCapability'].test(type, 'ISplitScopeManagement')) btn.enable();
        };
    };
    $SW.IPAM.CheckSplitScopeCapability = function (type, cidr, buttonObj) {

        buttonObj.disable();

        if (cidr > 30) return false;

        // check capability of split scope
        if ($SW['dhcpCapability'].test(type, 'ISplitScopeManagement')) {
            buttonObj.enable();
            return true;
        }

        return false;
    };

    $SW.IPAM.FriendlyNameTemplate = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.d(values)]}</tpl>', {
                d: function (v) {
                    return v.ViewInnerType == 3 ? "<a class='sw-grid-click' href='subnets.aspx?filter=ffDynamic&opento=" + v.SubnetID + "'onclick='return false;'>" + Ext.util.Format.htmlEncode(v.FriendlyName) + "</a>" : v.FriendlyName;
                }
            });
    };

    $SW.IPAM.RenderIPInPercent = function (val1) {
        var digits = 2;
        if (typeof (val1) == 'string') {
            var val = parseInt(val1, 10);
            if (typeof (val) == "number" && !isNaN(val)) {
                var div = Math.pow(10, digits), t = '' + (Math.round(val * div) / div), pos = t.indexOf('.'), d;
                if (pos < 0) { pos = t.length; t += "."; }
                for (d = t.length - pos; d <= digits; ++d) t += '0';
                return t + "%";
            }
            return value || "";
        }
    };

    $SW.IPAM.DhcpIpUsageTemplate = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.RenderIP(values.PercentUsed)]}</tpl>', {
                RenderIP: function (v) {
                    return (v) ? $SW.IPAM.RenderIPInPercent(v) : '';
                }
            });
    };

    $SW.IPAM.DisabledAtServer = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.d(values.DisabledAtServer)]}</tpl>', {
                d: function (v) {
                    return (v) ? $SW.IPAM.ColumnRendererNoYes(v) : '';
                }
            });
    };



    $SW.IPAM.ChartTemplate = function () {
        return new Ext.XTemplate(
            '<tpl>{[this.ChRender(values)]}</tpl>', {
                ChRender: function (v) {
                    return $SW.IPAM.DhcpScopeChartTemplate(v);
                }
            });
    };
    /*Chart template */
    $SW.IPAM.chartTpl = new Ext.XTemplate(
        '<ul class="wrapper"><tpl for=".">',
        '<li class="',
        '<tpl if="type==\'1\'">exclusion</tpl>',
        '<tpl if="type==\'2\'">range</tpl>',
        '" style="left:{left}%; width:{width}%;">',
        '<img src="/Orion/IPAM/res/images/default/s.gif" alt=""/>',
        '</li>',
        '</tpl></ul>'
    );

    /*Creating Tooltip for Chart */
    $SW.IPAM.DhcpScopeChartTemplate = function (values) {

        var norm = function (n, size, min, max, calcType) {

            if (n > 0 && calcType == 'L')
                n = n - 1;

            min = min || 0;
            max = max || 100;
            var v = (n * 100) / size;
            var n = (v > min) ? ((v < max) ? v : max) : min;
            return Math.round(n);
        };

        var chart = [];
        var allocSize = values.AllocSize;
        allocSize = (!allocSize || allocSize < 1) ? 1 : allocSize;
        var data = Ext.util.JSON.decode(values.ScopeIPRanges);
        if (Ext.isArray(data))
            Ext.each(data, function (d, i) {
                chart.push({
                    left: norm(d.s, allocSize, 0, 100, 'L'),
                    width: norm(d.e - d.s, allocSize, 1, 100, 'W'),
                    type: d.t
                });
            });
        var noRange = "<span class = \"r-no-ranges-hint\"></span>";
        if (values.ViewInnerType == 3) {
            noRange = "<span class = \"r-no-ranges-hint\">@{R=IPAM.Strings;K=IPAMWEBJS_MK1_12;E=js}</span>";
            var pNode = $SW.IPAM.pNodeServerType;
            if (typeof (pNode) != "undefined") {
                if (pNode.serverType == 4) {
                    noRange = "<span class = \"r-no-ranges-hint\">@{R=IPAM.Strings;K=IPAMWEBJS_MK1_1;E=js}</span>";
                }
            }
        }
        var chartcontent = $SW.IPAM.chartTpl.apply(chart);
        return Ext.isEmpty(chart) ? noRange : chartcontent;
    };
    $SW.IPAM.pNodeServerType = {};
    $SW.IPAM.treelisteners = function () { return new treelisteners(); };
    $SW.IPAM.treeloadlisteners = function () { return new treeloadlisteners(); };
    function treeloadlisteners() {
        this.load = function (that, node, response) {
            return true;
        };
    };
    treelisteners = function () {
        this.statechanged = function (t, s, newCS, oldCS) {
            UpdateUISetting("UI.DhcpServerTreeColumns:" + newCS);
        };
        this.headerclick = function (c, el, i) {
            if (c.dataIndex == "ScopeIPRanges" || c.dataIndex == "") {
                return;
            }
            var me = this;
            me.property = c.dataIndex;
            me.dir = c.dir = (c.dir === 'desc' ? 'asc' : 'desc');
            me.sortType = c.sortType;
            me.caseSensitive === Ext.isBoolean(c.caseSensitive) ? c.caseSensitive : this.caseSensitive;
            me.sortFn = c.sortFn || this.defaultSortFn;
            var sortClasses = ["sort-asc", "sort-desc"];
            var hds = this.innerHd.select('td').removeClass(sortClasses);
            hds.item(i).addClass(sortClasses[c.dir == 'desc' ? 1 : 0]);
            this.getRootNode().reload();
        };
        this.afterrender = function () {
            if (this.hmenu) {
                this.hmenu.insert(1,
                    { itemId: 'asc', text: "Sort Ascending", cls: 'xg-hmenu-sort-asc' },
                    { itemId: 'desc', text: "Sort Descending", cls: 'xg-hmenu-sort-desc' }
                );
            }
            var sortClasses = ["sort-asc", "sort-desc"];
            var hds = this.innerHd.select('td').removeClass(sortClasses);
            hds.item(1).addClass(sortClasses[0]);

        };
        this.headermenuclick = function (c, id, i) {

            if (id === 'asc' || id === 'desc') {
                if (c.dataIndex == "ScopeIPRanges") {
                    return;
                }
                var me = this;
                me.property = c.dataIndex;
                me.dir = id;
                me.sortType = c.sortType;
                me.caseSensitive === Ext.isBoolean(c.caseSensitive) ? c.caseSensitive : this.caseSensitive;
                me.sortFn = c.sortFn || this.defaultSortFn;
                var sortClasses = ["sort-asc", "sort-desc"];
                var hds = this.innerHd.select('td').removeClass(sortClasses);
                hds.item(i).addClass(sortClasses[id == 'desc' ? 1 : 0]);
                this.getRootNode().reload();
            }
        };
        this.load = function (t, r, s, o) {
            this.expandPath(t.getPath());
        };

        var beforeselect = function (that, node) {
            if (node == null) return true;
            if (node.attributes.t == -1) {
                node.parentNode.select();
                return false;
            }
            return true;
        };
        var dhcpButtonsContainer = (function () {
            var self = {};
            var isInit = false;

            self.Init = function () {
                if (!isInit) {
                    self.SplitScope = Ext.getCmp('btnDHCPSplitScope');
                    self.AdrsLeases = Ext.getCmp('btnDHCPAdrsLeases');
                    self.ViewDetails = Ext.getCmp('btnViewDetails');
                    self.Scan = Ext.getCmp('btnDHCPScan');
                    self.Edit = Ext.getCmp('btnDHCPEdit');
                    self.Graph = Ext.getCmp('btnDHCPGraph');
                    self.Delete = Ext.getCmp('btnDHCPDelete');
                    self.ReplicateScope = Ext.getCmp('btnDHCPReplicateScope');
                    self.ReplicateRelationship = Ext.getCmp('btnDHCPReplicateRelationship');
                    self.DeconfigureFailover = Ext.getCmp('btnDHCPDeconfigureFailover');
                }
            }

            self.DisableAll = function () {
                for (var buttonName in self) {
                    if (self.hasOwnProperty(buttonName)) {
                        self[buttonName].disable && self[buttonName].disable();
                    }
                }
            }
            return self;
        })();
        
        var processDhcpButtonStatusServer = function () {
            dhcpButtonsContainer.ViewDetails.enable();
            dhcpButtonsContainer.Graph.enable();

            if (CheckIsPowerUser()) {
                dhcpButtonsContainer.Scan.enable();
                dhcpButtonsContainer.Edit.enable();
                dhcpButtonsContainer.Delete.enable();
            }
        }

        var processDhcpButtonStatusScope = function (machineType, relationshipName) {
            dhcpButtonsContainer.Graph.enable();
            dhcpButtonsContainer.AdrsLeases.enable();

            if (CheckIsPowerUser()) {
                dhcpButtonsContainer.SplitScope.enable();
                dhcpButtonsContainer.Edit.enable();
                dhcpButtonsContainer.Delete.enable();
                if (relationshipName && relationshipName.length > 0) {
                    dhcpButtonsContainer.ReplicateScope.enable();
                    dhcpButtonsContainer.ReplicateRelationship.enable();
                }
            }
        }

        var processDhcpButtonDeconfigureFailover = function (scopeId, relationshipName) {
            $SW.IPAM.CheckDeconfigureFailoverCapability(scopeId, relationshipName, dhcpButtonsContainer.DeconfigureFailover);
        }

        var nodeIsInfoblox = function (node) {
            return node.parentNode.attributes.ServerType == 5;
        }

        var processInfobloxNode = function (node) {
            if (nodeIsInfoblox(node)) {
                dhcpButtonsContainer.AdrsLeases.disable();
                dhcpButtonsContainer.Edit.disable();
                dhcpButtonsContainer.Delete.disable();
                dhcpButtonsContainer.ReplicateScope.disable();
                dhcpButtonsContainer.ReplicateRelationship.disable();
                dhcpButtonsContainer.DeconfigureFailover.disable();
                dhcpButtonsContainer.SplitScope.disable();

                return true;
            }

            return false;
        }

        var treeselectionchange = function (sm) {
            var s = sm.getSelectedNodes() || [], selectedNodesNumber = s.length;
            dhcpButtonsContainer.Init();
            dhcpButtonsContainer.DisableAll();
            if (selectedNodesNumber !== 1) {
                if (selectedNodesNumber > 1 && CheckIsPowerUser())
                    dhcpButtonsContainer.Delete.enable();
                
                for(var n=0; n<selectedNodesNumber; n++){
                    if(processInfobloxNode(s[n])){
                        return;
                    }
                }

                return;
            }

            var tree = sm.tree, node = s[0];
            if (node == null) return;
            if (tree.dontselect == node.id) tree.dontselect = null;
            if (s[s.length - 1].id == -1) {
                this.unselect(s[s.length - 1]);
            }
            treeitem = node;

            //ViewInnerType == 0 -> Server
            //ViewInnerType == 2 -> SharedNetwork
            //ViewInnerType == 3 -> Scope

            if (node.attributes.ViewInnerType == 0) {
                processDhcpButtonStatusServer();
            } else if (node.attributes.ViewInnerType == 3) {
                if (!processInfobloxNode(node)) {
                    processDhcpButtonStatusScope(node.parentNode.attributes.MachineType, node.attributes.RelationshipName);
                    processDhcpButtonDeconfigureFailover(node.attributes.GroupId, node.attributes.RelationshipName);
                }
            } else if (node.attributes.ViewInnerType == 2) {
                dhcpButtonsContainer.Delete.enable();
            }

            if ((CheckIsPowerUser() || CheckDemoMode()) && !nodeIsInfoblox(node)) {
                $SW.IPAM.CheckSplitScopeCapability(node.parentNode.attributes.ServerType,
                    node.attributes.CIDR,
                    dhcpButtonsContainer.SplitScope);
            }
        };

        var treeLastWidth = null;
        this.bodyresize = function (that, w, height) {
            if (!w) return;
            if (!treeLastWidth) treeLastWidth = that.initialConfig.width;
            if (treeLastWidth == w) return;
            treeLastWidth = w;
            var jsonvalue = Ext.util.JSON.encode({ width: w });
        };

        this.beforerender = function (that) {
            // unhide first column (if it's hidden)
            var fc = that.columns[0];
            if (fc && fc.hidden === true) fc.hidden = false;
            // hook the selection model to load the appropriate grid.
            that.getSelectionModel().on('selectionchange', treeselectionchange);
            that.getSelectionModel().on('beforeselect', beforeselect);

            return true;
        };
        this.append = function (tree, parent, node, idx) {
            if (node.id == "0" && !node.expanded) node.expand();
            return true;
        };
        this.beforeexpandnode = function (node) {
            $SW.IPAM.pNodeServerType = { serverType: node.attributes.ServerType };
        };
        this.expandnode = function (t) {
            if (t.id == 'Hierarchy') {
                return;
            }
            var expandNode = $SW.Env["PageLoadBehav"];
            if (typeof (expandNode) == "undefined") {
                $SW.Env["PageLoadBehav"] = { id: t.id };
            }
            else if (expandNode.id != t.id) {
                $SW.Env["PageLoadBehav"] = { id: t.id };
            }
        };
        this.beforecollapsenode = function () {
            $SW.Env["PageLoadBehav"] = { id: -1 };
        };
    };
})();