﻿(function () {

    if (!$SW.IPAM) $SW.IPAM = {};
    var $SWAccRoles = $SW.IPAM.AccountRoles = {
        RoleChangedByUser: false,

        ValidateUserChangeOfRole: function () {
            if (this.RoleChangedByUser)
                return true;

            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_203;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_204;E=js}',
                buttons: Ext.Msg.OK,
                animEl: id,
                icon: Ext.MessageBox.WARNING
            });

            return false;
        },

        RolesComboData: [
            { name: 'Admin', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_43;E=js}',
                text: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_205;E=js}', '<span class="account-role-hint">', '</span>')
            },
            { name: 'PowerUser', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_44;E=js}',
                text: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_206;E=js}', '<span class="account-role-hint">', '</span>')
            },
            { name: 'Operator', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_45;E=js}',
                text: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_207;E=js}', '<span class="account-role-hint">', '</span>')
            },
            { name: 'ReadOnly', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_46;E=js}',
                text: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_208;E=js}'
            },
            { name: 'NoAccess', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_47;E=js}',
                text: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_209;E=js}', '<span class="account-role-hint">', '</span>')
            },
            { name: 'Clear', tpl: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_48;E=js}',
                text: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_210;E=js}'
            }
        ],

        InheritedTpl: new Ext.XTemplate(
            '<tpl if="values.inherited"><img src="/Orion/Admin/Accounts/images/icons/ok_enabled.png"> @{R=IPAM.Strings;K=IPAMWEBJS_VB1_119;E=js}</tpl>',
            '<tpl if="!values.inherited"><img src="/Orion/Admin/Accounts/images/icons/disable.png"> @{R=IPAM.Strings;K=IPAMWEBJS_VB1_120;E=js}</tpl>'
        ),

        TypeRenderer: function () {
            return function (value, meta, record) {
                var type = (value == '2') ? 'Windows' : 'Orion';
                meta.css = 'account-type account-type-' + type;
                meta.attr = 'qtip="' + type + '"';
                return '';
            };
        },

        NameRenderer: function () {
            return function (value, meta, record) {
                return '<div class="account-name">' + value + '</div>';
            };
        },

        RoleRenderer: function () {
            return function (value, meta, record) {
                var t = $SW.IPAM.AccountRoles, v = record.data;
                return (v) ? t.RenderRole(v.role, v.inherited) : '';
            };
        },

        InheritedRenderer: function () {
            return function (value, meta, record) {
                var v = record.data, tpl = $SW.IPAM.AccountRoles.InheritedTpl;
                return (v) ? tpl.apply({ values: v }) : ''; ;
            };
        },

        RoleTemplate: function () {
            var t = $SW.IPAM.AccountRoles;
            return new Ext.XTemplate(
                '<tpl>{[this.RenderRole(values.role)]}</tpl>', {
                    RenderRole: function (v) {
                        return (v) ? t.RenderRole(v.role, v.inhr, 'tpl') : '---';
                    }
                });
        },

        InheritedTemplate: function () {
            return $SW.IPAM.AccountRoles.InheritedTpl;
        },

        StoreDataChanged: function () {
            this.each(function (r) {
                var v = $SW.IPAM.AccountRoles.ParseRole(r);
                if (v) {
                    r.data['role'] = v.role;
                    r.data['inherited'] = v.inhr;
                    r.data['distance'] = v.d;
                }
            });
        },

        RoleByName: function (name) {
            var role, roles = $SW.IPAM.AccountRoles.RolesComboData;
            $.each(roles, function (i, r) {
                if (r.name == name) { role = r; return false; }
            });
            return role;
        },

        ParseRole: function (record) {
            var v, d, data = record.data, roles = $SW.IPAM.AccountRoles.RolesComboData;

            // handle all other roles
            $.each(roles, function (i, role) {
                d = parseInt(data[role.name]);
                if (isNaN(d)) return true;
                if (!v || v.d > d) v = { role: role, d: d, inhr: (d != 0) };
            });
            return (v && v.role && !isNaN(v.d)) ? v : undefined;
        },

        RenderRole: function (role, inhr, fld) {
            var cls = 'account-role' + (inhr ? ' account-role-inherited' : '') +
                      ' account-role-' + role.name + (inhr ? '-in' : '');
            return '<div class="' + cls + '">' + role[fld || 'text'] + '</div>';
        },

        RoleSelectorData: function () {
            var data = [], t = $SW.IPAM.AccountRoles;
            $.each(t.RolesComboData, function (i, role) {
                // skip adding of admin role
                if (role.name == 'Admin') return true;

                var text = t.RenderRole(role, 0);
                data.push({ name: role.name, text: text });
            });
            return data;
        },

        RoleSelector: function () {
            return new Ext.form.ComboBox({
                defaultAutoCreate: { tag: 'div', cls: 'role-editor-field' },
                valueField: 'name',
                displayField: 'text',
                mode: 'local',
                store: $SW.IPAM.AccountRoles.RoleSelectorStore,
                forceSelection: true,
                typeAhead: false,
                triggerAction: 'all',
                editable: false,
                hiddenName: 'val',
                setValue: function (v) {
                    Ext.form.ComboBox.prototype.setValue.call(this, v);
                    if (this.rendered && this.el) {
                        this.el.dom.innerHTML = this.lastSelectionText;
                    }
                    return this;
                }
            });
        },
        AjaxQueryCustomRole: function (p, successFn, failFn) {
            var dFn = successFn || function () { };
            var fail = $SW.ext.AjaxMakeFailDelegate('Update Account Role', failFn || dFn);
            var pass = $SW.ext.AjaxMakePassDelegate('Update Account Role', dFn);

            var role = $SW.IPAM.AccountRoles.RoleByName(p ? p.Role : '');
            var params = Ext.apply({
                entity: 'IPAM.AccountRoles',
                verb: (p && p.Role != 'Clear') ? 'Set' : 'Clear',
                tltrole: role ? role.tpl : 'unknown'
            }, p);

            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",
                params: params,
                timeout: fail,
                failure: fail,
                success: function (result, request) {
                    try {
                        var ret = Ext.util.JSON.decode(result.responseText);
                        if (ret.success == true && ret.license && ret.license.app == "IPAM") {
                            $SW.LicenseListeners.recv(ret.license);
                        }
                    } catch (ex) { }

                    var ret, text = result ? result.responseText : null;
                    try { ret = Ext.util.JSON.decode(text); } catch (ex) { }
                    if (!ret || ret.success == true) {
                        pass(result, request);
                    } else {
                        Ext.Msg.show({
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR,
                            title: (params.Silent ? String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_211;E=js}", params.tltrole) : String.format("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_212;E=js}", params.tltrole, params.Account)),
                            msg: ret.message.replace(/\n/g, '<br/>')
                        });
                        if (failFn) failFn();
                    }
                }
            });
        }
    };

    $SWAccRoles.AccountGridPlugin = function (config) {
        Ext.apply(this, config);
    };
    Ext.extend($SWAccRoles.AccountGridPlugin, Ext.util.Observable, {
        // [oh] do not change order! keep it ordered by gained permission.
        roles: ['Admin', 'PowerUser', 'Operator', 'ReadOnly', 'NoAccess'],

        getSelectionChangeFn: function () {
            var opt = this || {}, btn = {};
            for (var id in opt) {
                if (typeof (opt[id]) != "string") continue;
                btn[id] = Ext.getCmp(opt[id]) || undefined;
            }

            var roleToInt = function (name) {
                var v; $.each(opt.roles, function (i, r) {
                    if (r == name) { v = i; return false; }
                }); return v;
            };

            var setBtnState = function (i, clr, cnt) {
                if (cnt == 1) btn.role.enable(); else btn.role.disable();
                if (clr) btn.clr.enable(); else btn.clr.disable();
                if (i > 0) btn.pu.enable(); else btn.pu.disable();
                if (i > 1) btn.op.enable(); else btn.op.disable();
                if (i > 2) btn.ro.enable(); else btn.ro.disable();
                if (i > 3) btn.na.enable(); else btn.na.disable();
            };

            return function (sm) {
                var i, index, clr = false, root = false, s = sm.getSelections();
                $.each(s, function (i, sel) {
                    var data = sel.data, role = data["role"];
                    if (isNaN(i = roleToInt(role.name))) return true;
                    // [oh] check if role is custom defined
                    if (data["inherited"] === false) clr = true;
                    // [oh] check if group is root
                    if (data["GroupId"] == 0) root = true;
                    // [oh] get min index from nodes role
                    if (!index || index > i) index = i;
                });
                if (root) { index = 4; clr = false; }
                setBtnState(index, clr, s.length);
            };
        },
        init: function (grid) {
            grid.getSelectionModel().on('selectionchange',
                this.getSelectionChangeFn.apply(this));
        }
    });

    $SWAccRoles.RemapGridSelections = function (sm, fnData) {
        var sel = sm.getSelections() || [], p = [];
        $.each(sel, function (i, s) {
            var d = fnData(s.data); if (d) { p.push(d); }
        });
        return p;
    };
    $SWAccRoles.ChangeRoleProgress = function (p, txt, fn) {
        fn = fn || function () { };
        if (p.length <= 0) return;

        Ext.MessageBox.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_213;E=js}',
            msg: txt, width: 300, progress: true, closable: false
        });

        var cnt = p.length;
        var changeRoleFn = function () {
            if (p.length <= 0) {
                Ext.MessageBox.updateProgress(100, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_214;E=js}');
                (function () { Ext.MessageBox.hide(); fn(); }).defer(1000, Ext.MessageBox);
                return;
            }
            var pct = 1 - (p.length / cnt);
            var params = p.pop();
            Ext.MessageBox.updateProgress(pct, params.Account);
            $SW.IPAM.AccountRoles.AjaxQueryCustomRole(params, changeRoleFn, fn);
        };
        changeRoleFn();
    };
    $SWAccRoles.ClearRoleValue = function (el, fnAccount, fnGroupId) {
        var id = el;
        var fnAccount = fnAccount || function () { };
        var fnGroupId = fnGroupId || function () { };
        return function (t) {
            var grid = Ext.getCmp(id); if (!grid) return;
            var sm = grid.getSelectionModel(), store = grid.store;

            var p = $SWAccRoles.RemapGridSelections(sm, function (d) {
                if (d["inherited"] === true) return null;
                return { data: d, Account: fnAccount(d), GroupId: fnGroupId(d), Role: 'Clear' };
            });
            $SWAccRoles.ChangeRoleProgress(p, "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_215;E=js}", function () {
                sm.clearSelections(); store.load();
            });
        };
    };

    $SWAccRoles.SetRoleValue = function (el, r, fnAccount, fnGroupId, helpLink) {
        var id = el, role = r, hlp = (helpLink || '#');
        var fnAccount = fnAccount || function () { };
        var fnGroupId = fnGroupId || function () { };
        return function (t) {
            var grid = Ext.getCmp(id); if (!grid) return;
            var sm = grid.getSelectionModel(), c = sm.getCount(), store = grid.store;
            if (c < 1 || c > 1) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_216;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_217;E=js}',
                    buttons: Ext.Msg.OK,
                    animEl: id,
                    icon: Ext.MessageBox.WARNING
                });
            } else {
                var sm = grid.getSelectionModel(), store = grid.store;
                var params = $SWAccRoles.RemapGridSelections(sm, function (d) {
                    return {
                        ForceSetToCustom: (d['IsSettingRole'] == "1") || (d['distance'] == 10001),
                        Account: fnAccount(d),
                        GroupId: fnGroupId(d),
                        Role: role
                    };
                });
                var cback = function () {
                    $SWAccRoles.ChangeRoleProgress(params, "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_218;E=js}", function () {
                        sm.clearSelections(); store.load();
                    });
                };

                if (params[0].ForceSetToCustom !== true) {
                    cback(); return;
                }

                Ext.Ajax.request({
                    url: 'ExtCustomProvider.ashx',
                    method: 'POST',
                    success: function (result, request) {
                        var o = Ext.util.JSON.decode(result.responseText);
                        if (o.data.value == false) {
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_219;E=js}',
                                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_220;E=js}" +
                                '<a href="' + hlp + '" target="_blank" style=\"color: #336699; font-size: 11px; text-decoration: none;">' +
                                "&#0187; @{R=IPAM.Strings;K=IPAMWEBJS_VB1_221;E=js}</a> <br/><br/>" +
                                "<input type='checkbox' ID='cbDontShow'/> &nbsp; @{R=IPAM.Strings;K=IPAMWEBJS_VB1_222;E=js}",
                                buttons: Ext.Msg.YESNO,
                                animEl: id,
                                icon: Ext.Msg.QUESTION,
                                fn: function (btn, text) {
                                    if (btn == 'yes') {
                                        var cbDontShow = $('#cbDontShow')[0];
                                        if (cbDontShow.checked) {
                                            Ext.Ajax.request({
                                                url: 'ExtCustomProvider.ashx',
                                                method: 'POST',
                                                success: function (result, request) {
                                                },
                                                failure: function (result, request) {
                                                    Ext.Msg.show({
                                                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}',
                                                        buttons: Ext.Msg.OK,
                                                        animEl: id,
                                                        icon: Ext.MessageBox.ERROR
                                                    });
                                                },
                                                params:
                                                {
                                                    requesttype: 'dontShowCustomWarning',
                                                    verb: 'SetStatus',
                                                    value: 'true'
                                                }
                                            });
                                        }
                                        cback();
                                    }
                                }
                            });
                        }
                        else
                            cback();
                    },
                    failure: function (result, request) {
                        Ext.Msg.show({
                            title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_223;E=js}',
                            buttons: Ext.Msg.OK,
                            animEl: id,
                            icon: Ext.MessageBox.ERROR
                        });
                    },
                    params:
                    {
                        requesttype: 'dontShowCustomWarning',
                        verb: 'GetStatus'
                    }
                });
            }
        };
    };

    $SWAccRoles.RoleSelectorStore = new Ext.data.JsonStore({
        data: $SW.IPAM.AccountRoles.RoleSelectorData(),
        fields: [
            { name: 'name', type: 'string' },
            { name: 'text', type: 'string' }
        ]
    });

    var $SWAccRoleEditor = $SWAccRoles.RoleEditor = {
        // [oh] do not change order! keep it ordered by gained permission.
        roles: ['Admin', 'PowerUser', 'Operator', 'ReadOnly', 'NoAccess'],

        // [oh] append node and expand root node
        getAppendFn: function () {
            var scope = this;
            return function (tree, parent, node, idx) {
                if (node.id == "0" && !node.expanded) node.expand();
                return false;
            };
        },
        // [oh] unhide first column (if it's hidden)
        getBeforeRenderFn: function () {
            var scope = this;
            return function (that) {
                var fc = that.columns[0];
                if (fc && fc.hidden === true) fc.hidden = false;

                // hook the selection model events.
                that.getSelectionModel().on('selectionchange',
                    scope.getSelectionChangeFn.apply(scope));

                return true;
            };
        },
        getSelectionChangeFn: function () {
            var opt = this || {}, btn = {};
            for (var id in opt) {
                if (typeof (opt[id]) != "string") continue;
                btn[id] = Ext.getCmp(opt[id]) || undefined;
            }

            var roleToInt = function (name) {
                var v; $.each(opt.roles, function (i, r) {
                    if (r == name) { v = i; return false; }
                }); return v;
            };

            var setBtnState = function (i, clr) {
                if (clr) btn.clr.enable(); else btn.clr.disable();
                if (i > 0) btn.pu.enable(); else btn.pu.disable();
                if (i > 1) btn.op.enable(); else btn.op.disable();
                if (i > 2) btn.ro.enable(); else btn.ro.disable();
                if (i > 3) btn.na.enable(); else btn.na.disable();
            };

            // [oh] buttons are disabled in demo mode
            if ($SW.IsDemoServer)
                return function (that, nodes) { setBtnState(0, false); };

            return function (that, nodes) {
                var i, index, clr = false, root = false;
                $.each(nodes, function (i, n) {
                    var data = n.attributes;
                    if (isNaN(i = roleToInt(data.Role))) return true;
                    // [oh] check if role is custom defined
                    if (!data.inherited) clr = true;
                    // [oh] check if group is root
                    if (data["GroupId"] == 0) root = true;
                    // [oh] get min index from nodes role
                    if (!index || index > i) index = i;
                });
                if (nodes.length == 1 && root) { index = 4; clr = false; }
                setBtnState(index, clr);
            };
        },

        getTreeListeners: function (o) {
            var opt = Ext.apply({}, $SWAccRoles.RoleEditor, o);
            return {
                beforerender: opt.getBeforeRenderFn.apply(opt),
                append: opt.getAppendFn.apply(opt)
            };
        },
        SetRoleValue: function (el, r, accEl) {
            var id = el, role = r, acc = accEl, clr = (role == 'Clear');
            if (!role) return function () { };

            // [oh] setting the role is disabled in demo mode
            if ($SW.IsDemoServer)
                return function (t) { };

            return function (t) {
                var tree = Ext.getCmp(id), t = $SW.IPAM.AccountRoles;
                var nodes = (!tree) ? [] : tree.getSelectionModel().getSelectedNodes() || [];

                var reloadFn = function () { tree.root.getLoader().load(tree.root); };
                var ids = []; $.each(nodes, function (i, n) {
                    var id = n.attributes['id']; if (!clr || id > 0) ids.push(id);
                });
                t.AjaxQueryCustomRole({
                    Account: accEl.val(),
                    GroupId: ids.join(','),
                    Role: role,
                    Silent: true
                }, reloadFn);

                $SW.IPAM.AccountRoles.RoleChangedByUser = true;
            };
        }
    };
})();

Ext.ux.grid.SWRoleEditor = Ext.extend(Ext.ux.grid.RowEditor, {
    idColumn: 'GroupId',
    preEditValue: function (r, f) {
        var value = r.data[f];
        if (f == this.idColumn) {
            var val = $SW.IPAM.AccountRoles.ParseRole(r);
            value = (val) ? val.role.name : '';
        }
        return value;
    },
    listeners: {
        afteredit: function (edt, obj, rec, index) {
            var m = rec.modified, c = this.idColumn;
            if (!m || !m[c]) return;

            var gridstore = edt.grid.store, reloadFn = function () { gridstore.load(); };

            // [oh] arguments: account, groupId, role
            $SW.IPAM.AccountRoles.AjaxQueryCustomRole({ Account: rec.id, GroupId: m[c], Role: rec.get(c) }, reloadFn);
        }
    }
});
Ext.preg('sw-role-editor', Ext.ux.grid.SWRoleEditor);
