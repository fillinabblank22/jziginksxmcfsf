﻿
$SW.AdminCredEditorInit = function(nsId, pwId){
    var $nsId = nsId, speed = 250;
    $('.sw-credential-warning').css({display:'none'});

    var pwwarn = function(e){
        var sig = '**********';
        var pwkey = $SW.ns$($nsId,pwId)[0];
        if((pwkey.value == sig) || (pwkey.value == ''))
            $('.sw-credential-warning').hide(200);
        else
            $('.sw-credential-warning').show(200);
    };

    $('#'+$SW.nsGet($nsId,pwId)).bind('change keyup',pwwarn);
};
$SW.AdminCredEditorRefresh = function() { $SW['store'].load(); };

$SW.AdminCredTypeRenderer = function (type) {
  var bind = /bind/gi;
  return (bind.test(type)) ? 'BIND' : type;
}

$SW.CredTypeRenderer = function (value, meta, r) {
    return $SW.AdminCredTypeRenderer(value);
};

// list menu commands
$SW.AdminShowEditDialog = function(src, type, id, header, hlpSuffix){
  var page = '', helpid = '', objectId = '', bind = /bind/gi;
  if(type == 'Windows') {
    page = "Admin/Admin.Credentials.Win.Edit.aspx";
    helpid = 'OrionIPAMPHWindowsCredentials'+hlpSuffix+'.htm';
  }
  if (type == 'Cisco') {
    page = "Admin/Admin.Credentials.CISCO.Edit.aspx";
    helpid = (hlpSuffix == 'Add') ? 
        'OrionIPAMPHWCLICredentialsAdd.htm': 'OrionIPAMPHWCLIEnableLevel.htm';
  }
  if (type == 'ASA') {
    page = "Admin/Admin.Credentials.ASA.Edit.aspx";
    helpid = (hlpSuffix == 'Add') ?
        'OrionIPAMPHWCLICredentialsAdd.htm' : 'OrionIPAMPHWCLIEnableLevel.htm';
  }
  if (bind.test(type)) {
      page = "Admin/Admin.Credentials.BIND.Edit.aspx";
    helpid = (hlpSuffix == 'Add') ?
        'OrionIPAMPHWBINDCredentialsAdd.htm' : 'OrionIPAMPHWBINDPath.htm';
  }
  if (type == 'ISC') {
      page = "Admin/Admin.Credentials.ISC.Edit.aspx";
      helpid = (hlpSuffix == 'Add') ?
          'OrionIPAMPHISCCredentialsAdd.htm' : 'OrionIPAMPHISCCredentialsEdit.htm';
  }
  if (type == 'Infoblox') {
    page = "Admin/Admin.Credentials.Infoblox.Edit.aspx";
    helpid = (hlpSuffix == 'Add') ?
        'OrionIPAMPHInfobloxCredentialsAdd.htm' : 'OrionIPAMPHInfobloxCredentialsEdit.htm';
}
    
  if (id && id != '') { objectId = '&ObjectID=' + id; }
  
  $SW.ext.dialogWindowOpen(
    $SW['editCredential'], src.id, header,
    '/Orion/IPAM/'+page+'?NoChrome=True&msgq=editCredential'+objectId,
    helpid);
}

$SW.AdminCredentialAdd = function(that, type){
  var btnSrc = that;
  var header = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_12;E=js}', $SW.AdminCredTypeRenderer(type));
  $SW.AdminShowEditDialog(btnSrc,type,'',header,'Add');
}

$SW.AdminCredentialEdit = function(that) {
  var btnSrc = that;
  
  var selector = $SW['selector'];
  if( !selector ) return;
  if( selector.getCount() != 1 ) {
    Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_10;E=js}',
      msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_14;E=js}',
      buttons: Ext.Msg.OK,
      animEl: btnSrc.id,
      icon: Ext.MessageBox.WARNING
    });
  } else {
    var item = selector.getSelected();
    var header = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_13;E=js}', $SW.AdminCredTypeRenderer(item.data.Type), item.data.Name);
    $SW.AdminShowEditDialog(btnSrc, item.data.Type, item.data.ID, header, 'Edit');
  }
};

$SW.AdminCredentialDelete = function(that) {
  var btnSrc = that;

  var selector = $SW['selector'];
  if( !selector ) return;
  if( selector.getCount() < 1 ){
    Ext.Msg.show({
      title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_3;E=js}',
      msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_15;E=js}',
      buttons: Ext.Msg.OK,
      animEl: btnSrc.id,
      icon: Ext.MessageBox.WARNING
    });
    return;
  }

  var CheckInUseMessage = function(data) { 
    var json = eval('(' + data + ')');;
    if(json && !json.success && json.message == 'CREDENTIALS_IN_USE')
    {
      Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_16;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_17;E=js}',
        buttons: Ext.Msg.OK,
        icon: Ext.MessageBox.ERROR
      });
    }
  };

  var ids = [];
  Ext.each( selector.getSelections(), function(o){ ids.push(o.id); } );

  var fnContinue = function(){
    Ext.Ajax.request({
      url: $SW.appRoot() + "Orion/IPAM/ExtCredentialsProvider.ashx",
      method: 'POST',
      success: function(result, request) { 
        CheckInUseMessage(result.responseText);
        $SW.AdminCredEditorRefresh();
      },
      failure: function(result, request) { Ext.Msg.show({
        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
        buttons: Ext.Msg.OK,
        animEl: btnSrc.id,
        icon: Ext.MessageBox.ERROR
      } ); },
      params:{ entity: 'IPAM.CRED.DELETE', properties: ids.join(',') }
    });
  };
  
  Ext.Msg.show({
    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
    msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_18;E=js}', ids.length),
    buttons: Ext.Msg.YESNO,
    animEl: btnSrc.id,
    icon: Ext.MessageBox.QUESTION,
    fn: function(btn){ if( btn == 'yes' ) fnContinue(); }
  });
};

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        return true;
    }
    return false;
}
$SW.TestCedentialTask = function (taskname, params) {
    if (CheckDemoMode()) {
        demoAction('IPAM_DNS_CredentialTest', null);
        return false;
    }
    $SW.Tasks.DoProgress(taskname, params,
      { title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_19;E=js}" },
      function (d) {
          $SW.IPAM.DMultiCreds.testState(d.success);
          if (d.success) {
              $('#credtestpasstxt').text(d.message);
              $('#credtestfail').hide();
              $('#credtestpass').show(150);
          }
          else {
              if (d.state && d.state != null && d.state == 4) /*UITaskState.Canceled*/
                  return;
              
              if (d.message == "@{R=IPAM.Strings;K=IPAMWEBJS_JR1_14;E=js}")
                  $('#service').show();
              else
                  $('#service').hide();
              
              $('#credtestfailtxt').text(d.message+'   ');
              var a = $('#credTestFailLink')[0];
              a.href = 'https://support.solarwinds.com/SuccessCenter/s/article/Troubleshoot-DHCP-and-DNS-connection-errors-in-IPAM';
              a.target = '_blank';

              $('#credtestpass').hide();
              $('#credtestfail').show(150);
          }
      },
      function () { }
    );
};

$SW.fnWinContinue = function (nsId, nsIdArea, val) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtWinUserName')[0].value;
        var pw = $SW.ns$($area, 'txtWinPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.WIN.PW';
        else
            opts.password = pw;
    }

    $SW.TestCedentialTask('WindowsCredTest', opts);
};

$SW.fnInfobloxDnsContinue = function(nsId, nsIdArea, val){
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtInfobloxUserName')[0].value;
        var pw = $SW.ns$($area, 'txtInfobloxPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.IBX.PW';
        else
            opts.password = pw;
    }

    $SW.TestCedentialTask('InfobloxDnsCredTest', opts);
}

$SW.fnInfobloxDhcpContinue = function(nsId, nsIdArea, val){
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtInfobloxUserName')[0].value;
        var pw = $SW.ns$($area, 'txtInfobloxPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.IBX.PW';
        else
            opts.password = pw;
    }

    $SW.TestCedentialTask('InfobloxDhcpCredTest', opts);
}

$SW.fnCLIContinue = function (nsId, nsIdArea, val, protocol, port) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtCLIUserName')[0].value;

        var pw = $SW.ns$($area, 'txtCLIPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.CLI.PW';
        else
            opts.password = pw;

        opts.enablelevel = $SW.ns$($area, 'CLIEnableLevel')[0].value;
        var enablepw = $SW.ns$($area, 'txtEnablePassword')[0].value;
        if (enablepw == '**********')
            opts.enablepwkey = 'SW.CLI.ENBL.PW';
        else
            opts.enablepw = enablepw;

        if (protocol) opts.protocol = protocol;
        if (port) opts.port = port;
    }

    $SW.TestCedentialTask('CISCOCredTest', opts);
};

$SW.fnASAContinue = function (nsId, nsIdArea, val, protocol, port) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtASAUserName')[0].value;

        var pw = $SW.ns$($area, 'txtASAPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.ASA.PW';
        else
            opts.password = pw;

        opts.enablelevel = $SW.ns$($area, 'ASAEnableLevel')[0].value;
        var enablepw = $SW.ns$($area, 'txtASAEnablePassword')[0].value;
        if (enablepw == '**********')
            opts.enablepwkey = 'SW.ASA.ENBL.PW';
        else
            opts.enablepw = enablepw;

        if (protocol) opts.protocol = protocol;
        if (port) opts.port = port;
    }

    $SW.TestCedentialTask('ASACredTest', opts);
};
$SW.fnISCContinue = function (nsId, nsIdArea, val, protocol, port) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtISCUserName')[0].value;

        var pw = $SW.ns$($area, 'txtISCPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.ISC.PW';
        else
            opts.password = pw;

        if (protocol) opts.protocol = protocol;
        if (port) opts.port = port;
    }

    $SW.TestCedentialTask('ISCCredTest', opts);
};


$SW.IPAM.fnInheritedWMIContinue = function (nsId, nsIdArea, val) {
    var opts = { nodeid: parseInt(val), credential: -1 };
    $SW.TestCedentialTask('WMICredTest', opts);
};

$SW.IPAM.fnWMIContinue = function (nsId, nsIdArea, val) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtWMIUserName')[0].value;
        var pw = $SW.ns$($area, 'txtWMIPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.WMI.PW';
        else
            opts.password = pw;
    }

    $SW.TestCedentialTask('WMICredTest', opts);
};

$SW.IPAM.fnBINDContinue = function (nsId, nsIdArea, val, protocol, port) {
    var $nsId = nsId, $area = nsIdArea;
    var opts = { nodeid: parseInt(val) };

    var credid = $SW.ns$($nsId, 'CredentialSelector')[0].value;
    if (credid != "")
        opts.credential = credid;
    else {
        opts.username = $SW.ns$($area, 'txtBINDUserName')[0].value;

        var pw = $SW.ns$($area, 'txtBINDPassword')[0].value;
        if (pw == '**********')
            opts.sessionkey = 'SW.BIND.PW';
        else
            opts.password = pw;

        opts.path = $SW.ns$($area, 'txtBINDPath')[0].value;

        if (protocol) opts.protocol = protocol;
        if (port) opts.port = port;
    }

    $SW.TestCedentialTask('BINDCredTest', opts);
};

$SW.OnIsValueRequire = function(src,args){
  return args.IsValid = (args.Value != '');
};
$SW.OnCredentialNameExists = function (src, args) {
  return args.IsValid = true;
};

// [oh] cli credentials flip-flop port numbers
if (!$SW.IPAM.CliPortHandler) $SW.IPAM.CliPortHandler = function () {
    $('.cli-data').find('.cli-telnet').children('input:radio').click(function (e) {
        var chckd = $(this).prop('checked');
        var port = $(this).closest('.cli-data').find('.cli-port');
        if (chckd !== true || !port) return;
        if (port.val() == '22') port.val('23');
    });
    $('.cli-data').find('.cli-ssh').children('input:radio').click(function (e) {
        var chckd = $(this).prop('checked');
        var port = $(this).closest('.cli-data').find('.cli-port');
        if (chckd !== true || !port) return;
        if (port.val() == '23') port.val('22');
    });
    // [oh] return any value to prevent init more than once
    return true;
};

// --------------------------------------------------------------
// --- DHCP Multi Credentials -----------------------------------
// --------------------------------------------------------------
if (!$SW.IPAM.DMultiCreds) $SW.IPAM.DMultiCreds = (function () {
    // object with properties
    // type: {
    //  nsId: - namespace
    //  rbId: - type selector radio button
    //  cbId: - combobox to select new/existind
    //  area: - toggable new credential area
    //  areaNsId: - namespace for subcontrols
    //  pwId: - password control
    //  fnContinue: function(nsId, value){} - function to test credential
    //  validators: {} - collection of invokable validators
    //  chbClickFn: function(){} - click on checkbox
    //}
    var creds = {};
    var CredentialsTestSuccess = false;

    var syncCredCBox = function (type, speed) {
        var t = type.toLowerCase();
        var s = speed !== undefined ? speed : 250;
        $.each(creds, function (tp, c) {
            var cb = c.cbox; if (!cb) return;
            if (tp == t) { cb.show(); cb.enable(); }
            else { cb.disable(); cb.hide(); }
            if (cb.sync) cb.sync(cb, undefined, undefined, speed);
        });
    };
    var syncCredArea = function (areaId) {
        var area = $('#' + areaId);
        return function (me, record, index, speed) {
            var value = me.getValue();
            if (record) { value = record.data.value; }

            var s = speed !== undefined ? speed : 250;
            if (!me.hidden && value == "") area.show(s); else area.hide(s);
        };
    };

    var initRadio = function (type, rbId) {
        var cred = creds[type],
            selected = function () { return false },
            onClickFn = (typeof (cred.chbClickFn) == "function") ? cred.chbClickFn : function () { };

        var rb = $('#' + rbId);
        if (rb && rb[0]) {
            selected = function () { return rb[0].checked };
            var syncRBClick = function () { if (selected()) { syncCredCBox(type); onClickFn(cred); } };
            rb.click(syncRBClick); syncRBClick();
        }
        return selected;
    };
    var transformCBox = function (cbId, extId) {
        var cb = $('#' + cbId);
        if (!cb || !cb[0]) return null;

        return new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: cbId,
            hiddenName: cb.name,
            hiddenId: cbId,
            id: extId,
            forceSelection: true,
            disabled: cb.disabled || false,
            width: 'auto',
            listeners: {
                expand: function (combo) {
                    var w = combo.el.getWidth();
                    combo.list.setWidth(w);
                    combo.innerList.setWidth(w);
                }
            }
        });
    };

    var fnContinue = function (value) {
        $.each(creds, function (type, cred) {
            if (!cred.selected())
                return true; // continue
            if (cred.fnContinue)
                cred.fnContinue(cred.nsId, cred.nsIdArea, value);
        });
    };

    var _validate = function (type, valId, value) {
        var cred = creds[type.toLowerCase()], vals = cred.validators;
        var val = vals[valId] || function () { return false; };

        // check, whether the credential type is selected
        if (!cred.selected()) return true;

        return val(value, cred.nsId, cred);
    };

    return {
        register: function (type, cred, fnContinue, validators) {
            creds[type.toLowerCase()] = $.extend({
                type: type.toLowerCase(),
                fnContinue: fnContinue || function () { },
                validators: validators || {}
            }, cred);
        },
        init: function () {
            var selType = undefined;
            $.each(creds, function (type, cred) {
                var nsRBtnId = $SW.nsGet(cred.nsId, cred.rbId);
                var nsCBoxId = $SW.nsGet(cred.nsId, cred.cbId);
                var nsAreaId = $SW.nsGet(cred.nsId, cred.area);

                cred.cbox = transformCBox(nsCBoxId, cred.cbId);
                if (cred.cbox) {
                    cred.cbox.sync = syncCredArea(nsAreaId);
                    cred.cbox.on('select', cred.cbox.sync);
                }

                cred.selected = initRadio(type, nsRBtnId);
                if (cred.selected()) selType = type;

                $SW.AdminCredEditorInit(cred.nsIdArea, cred.pwId);
            });
            if (selType) syncCredCBox(selType, 0);
        },
        validate: function (type, valId, value) {
            if (type !== undefined)
                return _validate(type, valId, value);

            var res = true;
            $.each(creds, function (t, c) {
                res = res && _validate(t, valId, value);
            });
            return res;
        },
        test: function (vgroup, fn) {
            $('#credtestpass').hide();
            $('#credtestfail').hide();
            CredentialsTestSuccess = false;
            if (Page_ClientValidate(vgroup) == false)
                return false;

            fn(fnContinue);
            return false;
        },
        testState: function (state) {
            if (typeof (state) == "boolean") CredentialsTestSuccess = state;
            return CredentialsTestSuccess;
        },
        select: function (type) {
            var cred = creds[type.toLowerCase()];
            var rb = $('#' + $SW.nsGet(cred.nsId, cred.rbId));
            // [oh] select and do CLICK!
            if (rb) rb.attr("checked", true).click();
        }
    };
})();