
$(document).ready(function() {

    $('.sw-dialog-url').focus(function(eventObject) {

        if (this.value == '') {
            this.value = 'http://';

            var o = this;
            if (o.setSelectionRange) {     /* DOM */
                setTimeout(function()
                { o.setSelectionRange(o.value.length, o.value.length); }, 2);
            } else if (o.createTextRange) {    /* IE */
                var r = o.createTextRange();
                r.moveStart('character', o.value.length);
                r.select();
            } 
            
        }

    });

    $('.sw-dialog-url').blur(function(eventObject) {

        if (this.value == 'http://')
            this.value = '';

    });


});