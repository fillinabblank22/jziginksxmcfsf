﻿if (!$SW.IPAM) $SW.IPAM = {};

var ipGridID, ipWebID, ipOptionCode;

$SW.IPAM.SetOptionCode = function (ipAddrOptCode, ipAddrWebId,callBackFn) {
    ipOptionCode = ipAddrOptCode;
    ipWebID = ipAddrWebId;
    $SW.store.baseParams.w = ipAddrWebId;
    $SW.store.baseParams.optCode = ipAddrOptCode;

    callBackFn();
};

$SW.IPAM.reloadGridStore = function () {
    var store = Ext.getCmp('IpAddrGridBox').getStore(); 
    store.baseParams.w = ipWebID;
    store.baseParams.optCode = ipOptionCode;
    store.reload();
    return;
};

$SW.IPAM.SaveIPAddrressArrayValues = function() {
    $SW.IPAM.SetOptionValue(ipWebID, ipOptionCode, '', "True");
    return;
};

$SW.IPAM.reloadPairGridStore = function () {
    var store = Ext.getCmp('IpPairGridBox').getStore();
    store.baseParams.w = ipWebID;
    store.baseParams.optCode = ipOptionCode;
    store.reload();
    return;
};

$SW.IPAM.IpAddrList_renderer_buttons = function () {
    var up_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.up.png";
    var down_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.down.png";
    var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
    var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
    return function (value, meta, record) {
        var ret = '';
        meta.css = 'sw-grid-buttons';

        ret += "<a href='#' class='sw-grid-up' onclick='return false;'><img src='" + up_url + "' /></a> &nbsp;";
        ret += "<a href='#' class='sw-grid-down' onclick='return false;'><img src='" + down_url + "' /></a> &nbsp;";
        ret += "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
        return ret + "<a href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
    };
};

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        return true;
    }
    return false;
}

function GetErrorsHTML(errArray) {

    var errorsHtml = '';

    errorsHtml += '<tpl> <b>' + '@{R=IPAM.Strings;K=IPAMWEBJS_SE_10;E=js}' + '</b><br />';

    $.each(errArray, function (index, value) {
        errorsHtml += '<br>■ &ensp;' + value.message;
    });

    errorsHtml += '</tpl>';
        
    return errorsHtml;
}

function matchRegularExp (str, expressionStat) {
    var expPattern = new RegExp(expressionStat);
    return expPattern.test(str);
};

Ext.ux.grid.IpAddrRecordEditor = Ext.extend(Ext.ux.grid.RowEditor, {
    saveText: '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_20;E=js}',
    //[oh] disable edit on click / dblclick
    onRowClick: function () { },
    onRowDblClick: function () { },
    onGridKey: function () { },

    preEditValue: function (r, field) {
        var src = $SW.IPAM.IpAddrRecords;
        return Ext.ux.grid.IpAddrRecordEditor.superclass.preEditValue.apply(this, arguments);
    },

    stopEditing: function (saveChanges) {
        if (saveChanges === true && CheckDemoMode()) {
            demoAction('IPAM_IPEdit_Option', null);
            return false;
        }
        if (this.isVisible() && saveChanges !== false && this.isValid()) {
            var changes = {},
            r = this.record,
            hasChange = false,
            cm = this.grid.colModel,
            fields = this.items.items;
            for (var i = 0, len = cm.getColumnCount() ; i < len; i++) {
                if (!cm.isHidden(i)) {
                    var dindex = cm.getDataIndex(i);
                    if (!Ext.isEmpty(dindex)) {
                        var oldValue = r.data[dindex],
                        value = this.postEditValue(fields[i].getValue(), oldValue, r, dindex);
                        if (String(oldValue) !== String(value)) {
                            changes[dindex] = value;
                            hasChange = true;
                        }
                    }
                }
            }
            if (!hasChange)
                this.fireEvent('validateedit', this, changes, this.record, this.rowIndex);
        }
        Ext.ux.grid.IpAddrRecordEditor.superclass.stopEditing.apply(this, arguments);

    },
    listeners: {
        canceledit: function (editor, isCancel) {
            editor.stopEditing();
            var s = editor.grid.store, ri = editor.rowIndex, r = s.getAt(ri);
            var id = (r && r.data) ? r.data['ID'] || -1 : -1;

            if (id < 0) s.removeAt(ri);
            editor.grid.getView().refresh();
        },
        validateedit: function (editor, m, r, ri) {
            if (!r || !m) return false;
            var id = (r && r.data) ? r.data['ID'] || -1 : -1;
            var valid = true, addMode = id < 0, errs = [];

            var s = editor.grid.store;

            var cm = editor.grid.getColumnModel();
            var hasChange = false;

            var testIPAddrData = function (text) {

                var emptyErrorMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_05;E=js}';
                var isNotValidMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_11;E=js}';
                var isAlreadyExistsMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_29;E=js}';

                if (text.length == 0)
                    return emptyErrorMsg;
                
                if (!$SW.IP.v4_isok($SW.IP.v4(text)))
                    return isNotValidMsg;

                if (!matchRegularExp(text, "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$"))
                    return isNotValidMsg;
                
                var count = 0;
                s.each(function (r) {
                    if (r.get('IPAddress') == text)
                        count += 1;
                });

                if ((count > 0 && addMode) || (count > 0 && hasChange && !addMode))
                    return isAlreadyExistsMsg;
            };

            s.fields.each(function (f) {
                if (addMode && f.name == "ID")
                    return;

                if (m[f.name] != undefined)
                    hasChange = true;

                //check for empty fields
                var text = m[f.name] != undefined ? m[f.name] : r.data[f.name];
                var fn = f.name, ci = cm.findColumnIndex(fn);
                var colh = (ci < 0) ? fn : cm.getColumnHeader(ci);
                if (f.name == "IPAddress") {
                    var msg = testIPAddrData(text);
                    if (msg !== undefined) {
                        valid = false;
                        errs.push({ field: f.name, columnIndex: ci, columnHeader: colh, message: msg });
                    }
                }
            });

            if (hasChange && addMode && !valid) {
                r.beginEdit();
                Ext.iterate(m, function (n, v) {
                    r.set(n, v);
                });
                r.endEdit();
            }

            if (errs && errs.length > 0) {
                Ext.MessageBox.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_03;E=js}',
                    msg: GetErrorsHTML(errs),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR,
                    width: 350,
                    fn: function () {
                        editor.startEditing(ri, false);
                    }
                });
                return false;
            }
            return valid;
        },
        afteredit: function (editor, obj, rec, index) {
            var m = rec.modified, s = editor.grid.store;
            var grid = editor.grid;

            if (!m || !s) return;
            var src = rec.data;

            var data = [];
            data["ID"] = src.ID;
            data["IPAddress"] = src.IPAddress;

            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        $SW.IPAM.reloadGridStore();
                        grid.getView().refresh();
                    }
                },
                failure: function (message) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                        msg: message,
                        buttons: Ext.Msg.OK,
                        animEl: grid.ID,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                params:
                   {
                       entity: 'IPAddr',
                       verb: 'SaveEdit',
                       ids: src.ID,
                       value: src.IPAddress,
                       w: ipWebID,
                       optCode: ipOptionCode
                   }
            });
        }
    }
});

Ext.preg('sw-ipAddrrecord-editor', Ext.ux.grid.IpAddrRecordEditor);

Ext.ux.grid.IpPairRecordEditor = Ext.extend(Ext.ux.grid.RowEditor, {
    saveText: '@{R=IPAM.Strings;K=IPAMWEBJS_VN0_20;E=js}',
    //[oh] disable edit on click / dblclick
    onRowClick: function () { },
    onRowDblClick: function () { },
    onGridKey: function () { },

    preEditValue: function (r, field) {
        var src = $SW.IPAM.IpPairRecords;
        return Ext.ux.grid.IpPairRecordEditor.superclass.preEditValue.apply(this, arguments);
    },

    stopEditing: function (saveChanges) {
        if (saveChanges === true && CheckDemoMode()) {
            demoAction('IPAM_IPEdit_Option', null);
            return false;
        }
        if (this.isVisible() && saveChanges !== false && this.isValid()) {
            var changes = {},
            r = this.record,
            hasChange = false,
            cm = this.grid.colModel,
            fields = this.items.items;
            for (var i = 0, len = cm.getColumnCount() ; i < len; i++) {
                if (!cm.isHidden(i)) {
                    var dindex = cm.getDataIndex(i);
                    if (!Ext.isEmpty(dindex)) {
                        var oldValue = r.data[dindex],
                        value = this.postEditValue(fields[i].getValue(), oldValue, r, dindex);
                        if (String(oldValue) !== String(value)) {
                            changes[dindex] = value;
                            hasChange = true;
                        }
                    }
                }
            }
            if (!hasChange)
                this.fireEvent('validateedit', this, changes, this.record, this.rowIndex);
        }
        Ext.ux.grid.IpPairRecordEditor.superclass.stopEditing.apply(this, arguments);

    },
    listeners: {
        canceledit: function (editor, isCancel) {
            editor.stopEditing();
            var s = editor.grid.store, ri = editor.rowIndex, r = s.getAt(ri);
            var id = (r && r.data) ? r.data['ID'] || -1 : -1;

            if (id < 0) s.removeAt(ri);
            editor.grid.getView().refresh();
        },
        validateedit: function (editor, m, r, ri) {
            if (!r || !m) return false;
            var id = (r && r.data) ? r.data['ID'] || -1 : -1;
            var valid = true, addMode = id < 0, errs = [];
            var s = editor.grid.store;

            var cm = editor.grid.getColumnModel();
            var hasChange = false;

            var testIPpairData = function (ipPairAddress) {
                var emptyErrorMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_GK_16;E=js}';
                var isNotValidMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_11;E=js}';
                var isNotValidFormat = '@{R=IPAM.Strings;K=IPAMWEBJS_GK_15;E=js}';
                var isAlreadyExistsMsg = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_30;E=js}';
                var isPairSame = '@{R=IPAM.Strings;K=IPAMWEBJS_SE_31;E=js}';
                
                if (ipPairAddress.length == 0)
                    return emptyErrorMsg;
                
                var Ips = ipPairAddress.split(" ");
                if (Ips.length != 2)
                    return isNotValidFormat;
                
                if (!$SW.IP.v4_isok($SW.IP.v4(Ips[0])) || !$SW.IP.v4_isok($SW.IP.v4(Ips[1])))
                    return isNotValidMsg;
                
                if (!matchRegularExp(Ips[0], "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$") || !matchRegularExp(Ips[1], "^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$"))
                    return isNotValidMsg;
                
                if (Ips[0] == Ips[1])
                    return isPairSame;
                
                var count = 0;
                s.each(function (r) {
                    if ((r.get('Address1') + " " + r.get('Address2')) == ipPairAddress)
                        count += 1;
                });
                if ((count > 0 && addMode) || (count > 0 && hasChange && !addMode))
                    return isAlreadyExistsMsg;
               
            };

            if (m["Address1"] != undefined || m["Address2"] != undefined)
                hasChange = true;

            //check for empty fields
            var ipAddress1 = m["Address1"] != undefined ? m["Address1"] : r.data["Address1"];
            var ipAddress2 = m["Address2"] != undefined ? m["Address2"] : r.data["Address2"];

            var msg = testIPpairData(ipAddress1 +" "+ ipAddress2);
            if (msg !== undefined) {
                valid = false;
                errs.push({ message: msg });
            }

            if (hasChange && addMode && !valid) {
                r.beginEdit();
                Ext.iterate(m, function (n, v) {
                    r.set(n, v);
                });
                r.endEdit();
            }

            if (errs && errs.length > 0) {
                Ext.MessageBox.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_GK_14;E=js}',
                    msg: GetErrorsHTML(errs),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR,
                    width: 350,
                    fn: function (btn) {
                        if (btn == "ok") {
                            editor.startEditing(ri, false);
                        }
                    }
                });
                return false;
            }
            return valid;
        },
        afteredit: function (editor, obj, rec, index) {
            var m = rec.modified, s = editor.grid.store;
            var grid = editor.grid;

            if (!m || !s) return;
            var src = rec.data;

            var data = [];
            data["ID"] = src.ID;
            data["Address1"] = src.Address1;
            data["Address2"] = src.Address2;
            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        $SW.IPAM.reloadPairGridStore();
                        grid.getView().refresh();
                    }
                },
                failure: function (message) {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                        msg: message,
                        buttons: Ext.Msg.OK,
                        animEl: grid.ID,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                params:
                {
                    entity: 'IPPair',
                    verb: 'SaveEdit',
                    ids: src.ID,
                    value: src.Address1 + " " + src.Address2,
                    w: ipWebID,
                    optCode: ipOptionCode
                }
            });
        }
    }
});
Ext.preg('sw-ipPairrecord-editor', Ext.ux.grid.IpPairRecordEditor);

if (!$SW.IPAM.IpAddrRecords) $SW.IPAM.IpAddrRecords = (function () {
    return {
        AddRecord: function (grid) {
            
            if (grid.getStore().getTotalCount() > $SW.IPAM.AllowedRouterIpCount - 1 && (serverType == 'CISCO' || serverType == 'ASA') && ipOptionCode == 3) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                    msg: $SW.IPAM.RouterIpExceedsMsg,
                    buttons: Ext.Msg.OK,
                    animEl: grid.ID,
                    icon: Ext.MessageBox.ERROR,
                    fn: function () { grid.plugins.stopEditing(false); }
                });
                return;
            }
            
            //make a new empty record and stop any current editing
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            //add new record as the first row, select it
            grid.plugins.stopEditing(false);

            grid.store.insert(0, new blankRecord({
                ID: "",
                IPAddress: ""
            }));
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);

            //start editing new record
            grid.plugins.startEditing(0, false);
        },
        EditRecord: function (grid, ri, ci, r, target) {
            grid.plugins.startEditing(ri, true);
        },
        RemoveRecord: function (gridEl, ri, ci, r, target) {
            if (CheckDemoMode()) {
                demoAction('IPAM_IPEdit_Options', null);
                return false;
            }
            
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_33;E=js}',
                buttons: Ext.Msg.OKCANCEL,
                icon: Ext.MessageBox.INFO,
                width:350,
                fn: function (btn) {
                    if (btn === "ok") {

                        Ext.Ajax.request({
                            url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                            success: function (result, request) {
                                var res = Ext.util.JSON.decode(result.responseText);
                                if (res.success == true) {
                                    $SW.IPAM.reloadGridStore();
                                    gridEl.getView().refresh();
                                }
                            },
                            failure: function () {
                                Ext.Msg.show({
                                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                    buttons: Ext.Msg.OK,
                                    animEl: grid.ID,
                                    icon: Ext.MessageBox.ERROR
                                });
                            },
                            params:
                               {
                                   entity: 'IPAddr',
                                   verb: 'Remove',
                                   ids: r.data["ID"],
                                   value: r.data['IPAddress'],
                                   w: ipWebID,
                                   optCode: ipOptionCode
                               }
                        });
                    }
                }
            });
        },
        IPAddrMove: function (el, r, direction) {

            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",

                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        $SW.IPAM.reloadGridStore();
                        el.getView().refresh();
                    }
                },

                failure: function () {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.ID,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                params:
               {
                   entity: 'IPAddr',
                   verb: 'Resequence',
                   ids: r.data["ID"],
                   value: r.data['IPAddress'],
                   moveUp: direction == 'Up' ? "true" : "false",
                   w: ipWebID,
                   optCode: ipOptionCode
               }
            });
        },
        grid_cellclick: function (grid, ri, ci, ev) {
            $SW.ext.gridClickToDrilldown(grid, ri, ci, ev);
            var targetEl = ev.browserEvent.target || ev.browserEvent.srcElement;
            if (targetEl.tagName.toUpperCase() == 'IMG') targetEl = targetEl.parentNode;
            if (targetEl.tagName.toUpperCase() != 'A') return false;
            var record = grid.getStore().getAt(ri);

            if (/sw-grid-edit/.test(targetEl.className)) {
                //  ChangeFieldStatus(true);
                $SW.IPAM.IpAddrRecords.EditRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-delete/.test(targetEl.className)) {
                $SW.IPAM.IpAddrRecords.RemoveRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-down/.test(targetEl.className)) {
                $SW.IPAM.IpAddrRecords.IPAddrMove(grid, record, 'Down');
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-up/.test(targetEl.className)) {
                $SW.IPAM.IpAddrRecords.IPAddrMove(grid, record, 'Up');
                ev.stopPropagation(); return false;
            }
        }
    };
})();

if (!$SW.IPAM.IpPairRecords) $SW.IPAM.IpPairRecords = (function () {
    return {
        AddRecord: function (grid) {
            //make a new empty record and stop any current editing
            var blankRecord = Ext.data.Record.create(grid.store.fields);

            //add new record as the first row, select it
            grid.plugins.stopEditing(false);

            grid.store.insert(0, new blankRecord({
                ID: "",
                Address1: "",
                Address2: ""
            }));
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);

            //start editing new record
            grid.plugins.startEditing(0, false);
        },
        EditRecord: function (grid, ri, ci, r, target) {
            grid.plugins.startEditing(ri, true);
        },
        RemoveRecord: function (gridEl, ri, ci, r, target) {
            if (CheckDemoMode()) {
                demoAction('IPAM_IPEdit_Options', null);
                return false;
            }
            
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_SV1_4;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_33;E=js}',
                buttons: Ext.Msg.OKCANCEL,
                icon: Ext.MessageBox.INFO,
                width:350,
                fn: function (btn) {
                    if (btn === "ok") {
                        
                        Ext.Ajax.request({
                            url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                            success: function (result, request) {
                                var res = Ext.util.JSON.decode(result.responseText);
                                if (res.success == true) {
                                    $SW.IPAM.reloadPairGridStore();
                                    gridEl.getView().refresh();
                                }
                            },
                            failure: function () {
                                Ext.Msg.show({
                                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.ERROR
                                });
                            },
                            params:
                               {
                                   entity: 'IPPair',
                                   verb: 'Remove',
                                   ids: r.data["ID"],
                                   value: r.data['Address1'] + " " + r.data['Address2'],
                                   w: ipWebID,
                                   optCode: ipOptionCode
                               }
                        });
                    }
                }
            });
        },
        IPpairMove: function (el, r, direction) {

            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        $SW.IPAM.reloadPairGridStore();
                        el.getView().refresh();
                    }
                },
                failure: function () {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                params:
           {
               entity: 'IPPair',
               verb: 'Resequence',
               ids: r.data["ID"],
               value: r.data['Address1'] + " " + r.data['Address2'],
               moveUp: direction == 'Up' ? "true" : "false",
               w: ipWebID,
               optCode: ipOptionCode
           }
            });
        },
        grid_cellclick: function (grid, ri, ci, ev) {
            $SW.ext.gridClickToDrilldown(grid, ri, ci, ev);
            var targetEl = ev.browserEvent.target || ev.browserEvent.srcElement;
            if (targetEl.tagName.toUpperCase() == 'IMG') targetEl = targetEl.parentNode;
            if (targetEl.tagName.toUpperCase() != 'A') return false;
            var record = grid.getStore().getAt(ri);

            if (/sw-grid-edit/.test(targetEl.className)) {
                //  ChangeFieldStatus(true);
                $SW.IPAM.IpPairRecords.EditRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-delete/.test(targetEl.className)) {
                $SW.IPAM.IpPairRecords.RemoveRecord(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-down/.test(targetEl.className)) {
                $SW.IPAM.IpPairRecords.IPpairMove(grid, record, 'Down');
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-up/.test(targetEl.className)) {
                $SW.IPAM.IpPairRecords.IPpairMove(grid, record, 'Up');
                ev.stopPropagation(); return false;
            }
        }
    };
})();

