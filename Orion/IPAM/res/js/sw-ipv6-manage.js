(function() {
    if (!$SW.IPv6) $SW.IPv6 = {};
    
    $SW.IPv6.Sync_Fields = function (nsId) {
        var $nsId = nsId;
        $SW.ns$($nsId, 'cbDisableAutoScanning').click(function () { SubnetSyncScanDisable($nsId); });

        SubnetSyncScanDisable($nsId, true);
    };

    function SubnetSyncScanDisable($nsId, initialRun) {
        var cbDisable = $SW.ns$($nsId, 'cbDisableAutoScanning')[0],
            div = $('#ScanSettings'),
            req = $SW.ns$($nsId, 'reqriredScanInterval'),
            range = $SW.ns$($nsId, 'rangeScanInterval'),
            delay = initialRun ? 0 : 200;

        if (!cbDisable) return;
        if (cbDisable.checked) {

            div.hide(delay);
            if (req.length > 0)
                ValidatorEnable(req[0], false);
            if (range.length > 0)
                ValidatorEnable(range[0], false);
        }
        else {
            div.show(delay);
            if (range.length > 0)
                ValidatorEnable(range[0], true);
            if (req.length > 0)
                ValidatorEnable(req[0], true);
        }
    };

    var GetClusterId = function (treeitem) {
        if (treeitem.attributes.t == 1) {
            return treeitem.attributes.id;
        }

        return treeitem.attributes.ClusterId;
    }

    $SW.IPv6.Dialog = function(id, title, page, objid, pid, clusterId, helpid) {
        $SW['mainWindow'].width = 580;
        $SW['mainWindow'].height = 500;
        $SW.ext.dialogWindowOpen(
            $SW['mainWindow'], id, title,
            '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + objid + '&ParentId=' + pid +
            '&ClusterId=' + clusterId, helpid);
    };

    $SW.IPv6.GlobalPrefixAdd = function(el, treeitem) {
        var id = treeitem.id;
        var clusterId = GetClusterId(treeitem);
        var oid = treeitem.attributes.id;
        $SW.IPv6.Dialog(el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_224;E=js}', 'IPv6.GlobalPrefix.Add.aspx', oid, id, clusterId, 'OrionIPAMPHWindowAddGlobalPrefix.htm');
    };

    $SW.IPv6.LocalPrefixAdd = function(el, treeitem) {
        var id = treeitem.id;
        var clusterId = GetClusterId(treeitem);
        var oid = treeitem.attributes.id;
        $SW.IPv6.Dialog(el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_225;E=js}', 'IPv6.Prefix.Add.aspx', oid, id, clusterId, 'OrionIPAMPHWindowAddPrefix.htm');
    };

    $SW.IPv6.SubnetAdd = function(el, treeitem) {
        var id = treeitem.id;
        var clusterId = GetClusterId(treeitem);
        var oid = treeitem.attributes.id;
        $SW.IPv6.Dialog(el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_226;E=js}', 'IPv6.Subnet.Add.aspx', oid, id, clusterId, 'OrionIPAMPHWindowAddIPv6Subnet.htm');
    };

    $SW.IPv6.ipAdd = function(el, treeitem) {
        var clusterId = GetClusterId(treeitem);
        var pid = treeitem.attributes.id;
        var oid = -1;
        $SW.IPv6.Dialog(el.id, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_227;E=js}', 'IPv6.IPAddress.Add.aspx', oid, pid, clusterId, 'OrionIPAMPHWindowAddIPv6Address.htm');
    };
    $SW.IPv6.ipEdit = function(el, treeitem) {
        var pid = treeitem.attributes.id;

        var sel = Ext.getCmp('tabIPv6Subnet').getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_51;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        } else {
            var items = sel.getSelections();
            if (c > 1)
                $SW.IPv6._ipMultiEdit(el.id, sel, items, pid);
            else
                $SW.IPv6._ipSingleEdit(el.id, items, pid);
        }
    };
    $SW.IPv6._ipSingleEdit = function(eid, items, pid) {
        var oid = items[0].data.IpNodeId;
        $SW.IPv6._ipSingleEditDialog(eid, pid, oid);
    };
    $SW.IPv6._ipSingleEditDialog = function(eid, pid, oid) {
        $SW.IPv6.Dialog(eid, '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_228;E=js}', 'IPv6.IPAddress.Edit.aspx',
        oid, pid, 'OrionIPAMPHWindowEditIPv6Address.htm');
    };
    $SW.IPv6._ipMultiEdit = function(eid, sel, items, pid) {
        var ips = { ids: [], avail: [], used: [] };
        Ext.each(items, function(o) {
            if (!o.id) return true;
            ips.ids.push(o.id);
            if (o.data.Status == '2')
                ips.avail.push(o);
            else
                ips.used.push(o);
        });

        if ((ips.used.length > 0) && (ips.avail.length > 0)) {
            $SW.subnet.displayMultiEditWarning(ips.avail.length, ips.used.length,
                function() {
                    //unselect available IPs so we don't confuse user
                    var oids = $SW.IPv6._ipMultiEditAlterSelection(sel, ips.used);
                    $SW.IPv6._ipMultiEditDialog(eid, pid, oids,false);
                },
                function() {
                    //unselect used IPs
                    var oids = $SW.IPv6._ipMultiEditAlterSelection(sel, ips.avail);
                    $SW.IPv6._ipMultiEditDialog(eid, pid, oids,true);
                });
        } else {
            var oids = ips.ids;
            $SW.IPv6._ipMultiEditDialog(eid, pid, oids, (ips.used.length > 0) ? false:true);
        }
    };
    $SW.IPv6._ipMultiEditAlterSelection = function(sel, items) {
        sel.clearSelections();
        if (items.length > 0) sel.selectRecords(items);
        var ids = [];
        Ext.each(items, function(o) { if (o.id) ids.push(o.id); });
        return ids;
    };
    $SW.IPv6._ipMultiEditDialog = function (eid, pid, oids, availableCount) {
        // check for single item
        if (oids.length == 1) {
            return $SW.IPv6._ipSingleEditDialog(eid, pid, oids[0]);
        }

        var paramAvailable = availableCount && availableCount == true ? "True" : "False";

        var page = 'IPv6.IPAddress.MultiEdit.aspx';
        var pageUrl = '/Orion/IPAM/' + page + '?NoChrome=True&msgq=multiEditWindow&ParentId=' + pid + '&AvailableIPs=' + paramAvailable;

        var settings = new Object();
        settings.parameterName = 'ObjectID';
        settings.data = oids.join('|'); ;
        settings.url = pageUrl;

        var settingsContainer = 'MultiEditSettings';
        if (!window.ipam) { window['ipam'] = {}; }
        window.ipam[settingsContainer] = settings;

        var dialogUrl = '/Orion/IPAM/DialogWindowLoader.aspx?DataContainer=' + settingsContainer;
        $SW.ext.dialogWindowOpen(
            $SW['multiEditWindow'], eid, '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_49;E=js}',
            dialogUrl, 'OrionIPAMPHWindowMultiEditIPv6.htm');
    };

    $SW.IPv6.ipDetails = function(el, treeitem) {
        var pid = treeitem.attributes.id;
        var sel = Ext.getCmp('tabIPv6Subnet').getSelectionModel();
        var c = sel.getCount();
        if (c != 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_76;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        } else {
            var items = sel.getSelections();
            var oid = items[0].data.IpNodeId;
            window.location = 'IPAMAddressDetailsView.aspx?NetObject=IPAMN:' + oid;
        }
    };


    $SW.IPv6.ipRemove = function(el, treeitem) {
        var sel = Ext.getCmp('tabIPv6Subnet').getSelectionModel();
        var c = sel.getCount();
        if (c < 1) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_50;E=js}',
                msg: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_62;E=js}",
                buttons: Ext.Msg.OK,
                animEl: el.id,
                icon: Ext.MessageBox.WARNING
            });
            return;
        } else {
            var pid = treeitem.attributes.id;

            var items = sel.getSelections();
            var ids = [];
            Ext.each(items, function(o) { if (o.id) ids.push(o.id); });

            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_229;E=js}', ids.length),
                buttons: Ext.Msg.YESNO,
                animEl: el.id,
                icon: Ext.MessageBox.QUESTION,
                fn: function(btn) {
                    if (btn == 'yes') {
                        $SW.ClearGridSelection(Ext.getCmp('tabIPv6Subnet'));
                        $SW.subnet.DeleteIPsById(ids, pid, $SW.subnet.RefreshGridPage);
                    }
                }
            });
        }
    };

    $SW.IPv6.ipExport = function(el, treeitem) {
        var id = treeitem.id;

        Ext.DomHelper.append(document.body, {
            id: 'post-forward',
            tag: 'form',
            method: 'POST',
            action: 'Export.pickcolumns.aspx',
            children: [{ tag: 'input', type: 'hidden', value: '', name: 'GroupID' },
                       { tag: 'input', type: 'hidden', value: id, name: 'SubnetID'}]
        }).submit();
    };
    
    $SW.IPv6.ipImport = function(el, treeitem) {
        var id = treeitem.id;
        window.location = "import.upload.intro.aspx?GroupID=" + id;
    };

    // Prefix Size Control
    $SW.IPv6.PrefixSizeInit = function(sizeBox, opt) {
        if (!opt) opt = { minValue: 1, maxValue: 128 };
        opt.incrementValue = 1;

        // create updown box
        var upDown = new Ext.ux.form.UpDownBox(opt);
        upDown.applyToMarkup(sizeBox[0]);
        // value change handler
        upDown.on('change', function () { $SW.IPv6.PrefixSizeChange(upDown, opt); });
        sizeBox.bind('keydown', function (event) { $SW.IPv6.AllowNumbersOnly(event); });
        sizeBox.bind('change textinput', function () { $SW.IPv6.PrefixSizeChange(upDown, opt); });
        // init
        $SW.IPv6.PrefixSizeChange(upDown, opt);
    };
    
    $SW.IPv6.PrefixSizeChange = function(el, opt) {
        if (!el || !opt) return;
        var val = el.ReadIntValue(), v = opt.maxValue - val;
        var cnt = Math.pow(2, v);
        var cntHtml = (v > 16) ? ('2^' + v) : cnt;
        if (opt.subnets) { opt.subnets.html(cntHtml); opt.subnets.attr('cnt', cnt); }
        if (opt.fn) opt.fn(val);
    };
    
    $SW.IPv6.AllowNumbersOnly = function (e) {
        var keyCode = e.which; // Capture the event

        if (keyCode != 8 && keyCode != 9 && keyCode != 13 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40 && keyCode != 46 && keyCode != 110 && keyCode != 190) {
            if (keyCode < 48) {
                e.preventDefault();
            } else if (keyCode > 57 && keyCode < 96) {
                e.preventDefault();
            } else if (keyCode > 105) {
                e.preventDefault();
            }
        }
    };
    
    // IPv6 Address Control
    $SW.IPv6.SetIPv6AddressCustomValidator = function(el, opt) {
        if (!el || !opt) { return; }
        if (!el.opt) el.opt = {};
        el.opt.ipv6 = "";
        el.opt.type = opt.type;
        el.opt.parent = opt.parent;
        el.opt.size = opt.size;
        el.opt.pid = opt.pid;
        el.opt.oid = opt.oid;
        el.opt.clusterId = opt.clusterId;
    };
    $SW.IPv6.IPv6AddressValidator = function(src, arg) {
        if (!src || !arg) { return arg.IsValid = false; }
        if (!src.opt) src.opt = {};
        var opt = src.opt; opt.ipv6 = arg.Value;
        var proc = (opt.type == 'IPv6Node') ? 'ValidateIPv6Address' : 'ValidateIPv6Prefix';
        var res = { isvalid: false, message: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_230;E=js}" };
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Orion/IPAM/services/IPv6Provider.asmx/" + proc,
            data: Ext.util.JSON.encode(opt),
            dataType: "json",
            async: false,
            success: function(r) { res = r.d; }
        });
        arg.IsValid = res.isvalid;
        if (!res.isvalid) {
            // clear old error message
            $SW.Valid.Del(src, src.errormessage);
            // set to show new message
            src.display = "None";
            src.errormessage = res.message;
        }
        return true;
    };

    // IPv6 Address Add/Edit
    $SW.IPv6.CreateCombo = function(id, newid) {
        var item = $('#' + id)[0];
        var ops = {
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            disabled: item.disabled || false
        };

        if (newid) { ops.id = newid; }
        return new Ext.form.ComboBox(ops);
    };
    $SW.IPv6.ScanningChanged = function(nsId, el) {
        var isreadonly = (el.getValue() == '1');
        var ids = ['txtSysLocation', 'txtSysContact', 'txtSysDescr', 'txtSysName', 'txtVendor', 'txtMachineType', 'txtMAC'];
        jQuery.each(ids, function(i, o) {
            var item = $SW.ns$(nsId, o)[0];
            if (isreadonly) {
                item.readOnly = 'readonly';
                $(item).parent().addClass('x-item-disabled');
            } else {
                item.readOnly = '';
                $(item).parent().removeClass('x-item-disabled');
            }
        });
    };

    String.prototype.endsWith = function(str) {
        return (this.match(str + "$") == str)
    };

    $SW.IPv6.ClearUserFields = function(nsId) {
        var f = document.forms[0], imax = f.length, elm;
        for (var i = 0; i < imax; ++i) {
            elm = f[i];
            if( elm.type == 'select-one') elm.selectedIndex = 0;
            if (elm.type != 'text') continue;
            // skip IPv6 address field
            if (elm.name.endsWith('txtAddress')) continue;
            if (!$(elm).siblings('.x-form-trigger')[0]) elm.value = '';
        }
    };
    $SW.IPv6.StatusChanged = function(c, r, o) {
        c.collapse();
        var cVal = c.getValue();
        var rVal = r.data.value;
        if (!o.lic_taken && (o.lic_pos >= o.lic_max && cVal == 'Available' && rVal != 'Available')) {
            $SW.Alert('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_67;E=js}', '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_231;E=js}', { buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO });
            return false;
        }
        if (cVal != 'Available' && rVal == 'Available') {
            var answ = confirm("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_169;E=js}");
            if (answ) {
                $SW.IPv6.ClearUserFields(o.nsId);
                $SW.CustomPropertyFormDisable(true, true, 'cp');
            } else { return false; }
        }
        else{
            $SW.CustomPropertyFormDisable(false, false, 'cp');
        }

        window.setTimeout($SW.Valid.RetestAll, 250);
        return true;
    };

    $SW.IPv6.ValidateNotAvailableStatus = function(src, args) {
        var val = Ext.getCmp('ddlStatus').el.dom.value;
        if (val == 'Available') {
            if (args.Value.replace(/[ \t]/g, '') != '') {
                args.IsValid = false;
                return;
            }
        }
        args.IsValid = true;
    };

    // IPv6 Address MultiEdit
    $SW.IPv6.SetEnabled = function(ctrl, enbl) {
        if (!ctrl) { return; }
        if (enbl) {
            ctrl.readOnly = '';
            ctrl.disabled = '';
            $(ctrl).parent().removeClass('x-item-disabled');
        } else {
            ctrl.readOnly = 'readonly';
            ctrl.disabled = 'disabled';
            $(ctrl).parent().addClass('x-item-disabled');
        }
    };

    $SW.IPv6.CheckBoxInit = function(chbId, ctrlId) {
        var chb = $('#' + chbId)[0];
        var ctrl = $('#' + ctrlId)[0];
        if (!chb || !ctrl) { return; }

        $SW.IPv6.SetEnabled(ctrl, false);
        $(chb).bind('click', function() { $SW.IPv6.SetEnabled(ctrl, chb.checked); });
    };
    $SW.IPv6.CheckBoxInitExt = function(chbId, ctrl) {
        var chb = $('#' + chbId)[0];
        if (!chb || !ctrl) { return; }

        $(chb).bind('click', function() { ctrl.setDisabled(!chb.checked); });
    };
    $SW.IPv6.StatusChangedMultiEdit = function(c, fn) {
        var cVal = c.getValue();
        var rVal = $SW.IPv6.ddlStatus;
        if (cVal == 'Available' && rVal != 'Available') {
            var answ = confirm("@{R=IPAM.Strings;K=IPAMWEBJS_VB1_169;E=js}");
            if (!answ) { if (rVal) c.setValue(rVal); return true; }
        }
        $SW.IPv6.ddlStatus = cVal;
        if (fn) { fn(cVal); }
        return true;
    };
    
    $SW.IPv6.SetGridLoadHighlight = function () {
        var grid = Ext.getCmp('tabIPv6Subnet');
        grid.getStore().on('load', function (that, records) {
            $SW.subnet.SelectInitialRow(that, records, grid);
        });
    };
    
    $SW.IPv6.SetIPv6GridFilter = function (that) {

        $SW.ClearGridSelection(Ext.getCmp('tabIPv6Subnet'));

        if (that.id == $SW.subnet.ipv6Gridfilter)
            return;

        if ($SW.subnet.ipv6Gridfilter != null) {
            // if the item came with a iconclass, remove it.
            var chk = Ext.getCmp($SW.subnet.ipv6Gridfilter);
            chk.setChecked(false);
            chk.setIconClass(chk.initialConfig.iconCls || '');
        }

        $SW.subnet.ipv6Gridfilter = that.id;
        that.setIconClass('');
        that.setChecked(true);

        var ff = Ext.getCmp('ipv6Filter');
        if (ff) {
            ff.setIconClass(that.initialConfig.iconCls || '');
            ff.setText(that.text);
        }

        var pgbar = Ext.getCmp('tabIPv6Subnet').getBottomToolbar();
        if (pgbar) {
            var filter = pgbar.swFilter || [], btn = pgbar.swFilterBtn;
            if (that.id != 'ffIPv6All') {
                // set correct filter name
                btn.setIconClass(that.initialConfig.iconCls || '');
                btn.setText(that.text);
                // show the pagingtoolbarfilter.
                Ext.each(filter, function () { this.show(); });
            }
            else {
                Ext.each(filter, function () { this.hide(); });
            }
        }

        $SW.subnet.RefreshTab();
    };
    
    $SW.IPv6.ToolbarListeners = function() { return new IPv6ToolbarListeners(); };
    function IPv6ToolbarListeners() {
        this.beforerender = function () {
            $SW.IPv6.SetGridLoadHighlight();
        };
        this.render = function (t) {
            if (t.items) {
                t.swFilter = t.items.getRange(0, 2) || [];
                t.swFilterBtn = t.swFilter[1];
            }

            var filter = 'ffIPv6All';
            $SW.IPv6.SetIPv6GridFilter(Ext.getCmp(filter));
            return true;
        };
    };
})();

// UpDownBox
Ext.namespace("Ext.ux.form");
Ext.ux.form.UpDownBox = function(config){
	Ext.ux.form.UpDownBox.superclass.constructor.call(this, config);
	this.addEvents({'up':true,'down':true,'change':true});
}
Ext.extend(Ext.ux.form.UpDownBox, Ext.form.TriggerField, {
    triggerClass: 'x-form-UpDownBox-trigger',

    defaultValue: 0,
    minValue: undefined,
    maxValue: undefined,
    incrementValue: 1,

    onRender: function(ct, position) {
        // render twin trigger
        Ext.ux.form.UpDownBox.superclass.onRender.call(this, ct, position);

        // [oh] do not hook events for disabled state
        if(this.el.dom.disabled) return;

        // hook keyboard events
        this.keyNav = new Ext.KeyNav(this.el, {
            "up": function(e) { e.preventDefault(); this.onNumberUp(); },
            "down": function(e) { e.preventDefault(); this.onNumberDown(); },
            scope: this
        });

        // hook mouse events
        this.repeater = new Ext.util.ClickRepeater(this.trigger, { delay: 350 });
        this.repeater.on("click", this.onTriggerClick, this, { preventDefault: true });
        this.trigger.on("mouseover", this.onMouseOver, this, { preventDefault: true });
        this.trigger.on("mouseout", this.onMouseOut, this, { preventDefault: true });
        this.trigger.on("mousemove", this.onMouseMove, this, { preventDefault: true });
        this.trigger.on("mousedown", this.onMouseDown, this, { preventDefault: true });
        this.trigger.on("mouseup", this.onMouseUp, this, { preventDefault: true });
        this.wrap.on("mousewheel", this.handleMouseWheel, this);
    },

    initTrigger: function() {
        this.trigger.addClassOnOver('x-form-trigger-over');
        this.trigger.addClassOnClick('x-form-trigger-click');
    },

    setTriggerClass: function(type) {
        if (this.disabled) return;

        this.clearTriggerClasses();

        var middle = this.getMiddle();
        this.tcls = undefined;
        if (Ext.EventObject.getPageY() < middle)
            this.tcls = 'x-form-UpDownBox-' + type + '-up';
        else
            this.tcls = 'x-form-UpDownBox-' + type + '-down';
        this.trigger.addClass(this.tcls);
    },
    clearTriggerClasses: function() {
        this.trigger.removeClass('x-form-UpDownBox-over-up');
        this.trigger.removeClass('x-form-UpDownBox-over-down');
        this.trigger.removeClass('x-form-UpDownBox-click-up');
        this.trigger.removeClass('x-form-UpDownBox-click-down');
        this.tcls = undefined;
    },

    onMouseOver: function() { this.setTriggerClass('over'); },
    onMouseOut: function() { this.clearTriggerClasses(); },
    onMouseMove: function() { this.setTriggerClass('over'); },
    onMouseDown: function() { this.setTriggerClass('click'); },
    onMouseUp: function() { this.clearTriggerClasses(); },

    //private
    onTriggerClick: function() {
        if (this.disabled || this.getEl().dom.readOnly) return;

        var middle = this.getMiddle();
        var ud = (Ext.EventObject.getPageY() < middle) ? 'Up' : 'Down';
        this['onNumber' + ud]();
    },

    getMiddle: function() { return this.trigger.getTop() + (this.trigger.getHeight() / 2); },

    handleMouseWheel: function(e) {
        if (this.disabled || this.getEl().dom.readOnly) {
            Ext.EventObject.preventDefault(); //prevent scrolling when disabled/readonly
            return;
        }
        var delta = e.getWheelDelta();
        if (delta > 0) {
            this.onNumberUp();
            e.stopEvent();
        } else if (delta < 0) {
            this.onNumberDown();
            e.stopEvent();
        }
    },

    onNumberUp: function() {
        this.changeNumber(this, false);
        this.fireEvent("up", this);
        this.fireEvent("change", this);
    },

    onNumberDown: function() {
        this.changeNumber(this, true);
        this.fireEvent("down", this);
        this.fireEvent("change", this);
    },

    changeNumber: function(field, down) {
        var v = parseInt(field.getValue());
        (down == true) ? v -= this.incrementValue : v += this.incrementValue;
        v = (isNaN(v)) ? this.defaultValue : v;
        v = this.checkBoundaries(v);
        field.setRawValue(v);
    },
    checkBoundaries: function (v) {
        if (this.minValue != undefined && v < this.minValue) v = this.minValue;
        if (this.maxValue != undefined && v > this.maxValue) v = this.maxValue;
        return v;
    },
    ReadIntValue: function() {
        var v = parseInt(this.getValue());
        if (isNaN(v)) { v = this.defaultValue; }
        var res = this.checkBoundaries(v);
        if (res != v) { this.setRawValue(res); }
        return res;
    }
});

Ext.reg('uxUpDownBox', Ext.ux.form.UpDownBox);
