
var delDialog;
var pollDialog;

function ObjectStringPropertiesToSecureString(obj) {
   if ((obj != null) && typeof(obj)=='object')
   {
       try {
          obj = Object.create(obj); // create copy of object
       }
       catch(e) {}
   }
   
   if (typeof(obj) != 'undefined')
   {
       for(var p in obj)
       {
           if (typeof(p) != 'undefined')
           {
              if (typeof(obj[p])=='string')
              {
                  obj[p] = obj[p].toSecureString();
              }
           }
       }
   }
   
   return obj;
};

String.prototype.toSecureString = function () {
    return this.replace(/</ig, "&lt").replace(/>/ig, "&gt;");
};

String.prototype.format = function(params) {
	var pattern = /{([^}]+)}/g;
	return this.replace(pattern, function($0, $1) { return  params[$1]; });
};

Array.prototype.unique = function() {
	var a = [], i, l = this.length;
	for (i = 0; i<l; i++) {
		if ($.inArray(this[i], a) < 0) { a.push( this[i] ); }
	}
	return a;
};

Array.prototype.contains = function (element) {
	for (var i = 0; i < this.length; i++)
		if (this[i] == element) return true;
	return false;
};

var Pager =
{
    isInSearchMode: false,
    forbidReset: false,
    actualPage: 1,
    defaultPageSize: 40,
    maxPageSize: 250,
    pageSize: 40,
    totalCount: null,
    pageCount: null,
    GoToNextPage: function () {        
        Pager.actualPage++;
        MNG.Prefs.save("ActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToPreviousPage: function () {        
        Pager.actualPage--;
        MNG.Prefs.save("ActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToFirstPage: function () {
        Pager.actualPage = 1;
        MNG.Prefs.save("ActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    GoToLastPage: function () {
        Pager.actualPage = Pager.pageCount;
        MNG.Prefs.save("ActualPage", Pager.actualPage);
        Pager.MakePaging();
        Pager.Init();
        return false;
    },
    Init: function () {
        //console.info("Pager.Init - PageSize: " + Pager.pageSize + " ActualPage: " + Pager.actualPage + " TotalCount: " + Pager.totalCount);
        Pager.SetTotalObjectsCount(Pager.totalCount);

        // cause hiding select all panel if displayed
        MNG.DisplaySelectPan(false);

        if (Pager.totalCount <= Pager.pageSize && (Pager.totalCount < 2)) {
            $("#extPager").hide();
            $("#extPagerTop").hide();
        }
        else {
            $("#extPager").show();
            $("#extPagerTop").show();
            // if it isn't first page
            if (Pager.actualPage > 1) {
                $("#previousPageTable").removeClass("x-item-disabled");
                $("#previousPage").unbind('click', Pager.GoToPreviousPage);
                $("#previousPage").click(Pager.GoToPreviousPage);
                $("#previousPageTableTop").removeClass("x-item-disabled");
                $("#previousPageTop").unbind('click', Pager.GoToPreviousPage);
                $("#previousPageTop").click(Pager.GoToPreviousPage);

                $("#firstPageTable").removeClass("x-item-disabled");
                $("#firstPage").unbind('click',Pager.GoToFirstPage);
                $("#firstPage").click(Pager.GoToFirstPage);
                $("#firstPageTableTop").removeClass("x-item-disabled");
                $("#firstPageTop").unbind('click', Pager.GoToFirstPage);
                $("#firstPageTop").click(Pager.GoToFirstPage);
            }
            else {
                $("#previousPageTable").addClass("x-item-disabled");
                $("#previousPage").unbind('click', Pager.GoToPreviousPage);
                $("#previousPageTableTop").addClass("x-item-disabled");
                $("#previousPageTop").unbind('click', Pager.GoToPreviousPage);

                $("#firstPageTable").addClass("x-item-disabled");
                $("#firstPage").unbind('click', Pager.GoToFirstPage);
                $("#firstPageTableTop").addClass("x-item-disabled");
                $("#firstPageTop").unbind('click', Pager.GoToFirstPage);
            }

            //if it isn't last Page            
            if (Pager.actualPage < Pager.pageCount) {
                $("#nextPage").unbind('click', Pager.GoToNextPage);               
                $("#nextPage").click(Pager.GoToNextPage);
                $("#nextPageTable").removeClass("x-item-disabled");
                $("#nextPageTop").unbind('click',Pager.GoToNextPage);
                $("#nextPageTop").click(Pager.GoToNextPage);
                $("#nextPageTableTop").removeClass("x-item-disabled");

                $("#lastPage").unbind('click',Pager.GoToLastPage);
                $("#lastPage").click(Pager.GoToLastPage);
                $("#lastPageTable").removeClass("x-item-disabled");
                $("#lastPageTop").unbind('click',Pager.GoToLastPage);
                $("#lastPageTop").click(Pager.GoToLastPage);
                $("#lastPageTableTop").removeClass("x-item-disabled");
            }
            else {
                $("#lastPage").unbind('click', Pager.GoToLastPage);
                $("#lastPageTable").addClass("x-item-disabled");
                $("#lastPageTop").unbind('click', Pager.GoToLastPage);
                $("#lastPageTableTop").addClass("x-item-disabled");

                $("#nextPage").unbind('click', Pager.GoToNextPage);
                $("#nextPageTable").addClass("x-item-disabled");
                $("#nextPageTop").unbind('click', Pager.GoToNextPage);
                $("#nextPageTableTop").addClass("x-item-disabled");
            }

            $("#pageNumber").val(Pager.actualPage);
            $("#pageNumberTop").val(Pager.actualPage);

            $("#totalPageCount").html("@{R=Core.Strings;K=WEBJS_TM0_96;E=js} " + Pager.pageCount);
            $("#totalPageCountTop").html("@{R=Core.Strings;K=WEBJS_TM0_96;E=js} " + Pager.pageCount);

            $("#displayInfo").html(Pager.GetDisplayInfo());
            $("#displayInfoTop").html(Pager.GetDisplayInfo());

            $("#pageSize").val(Pager.pageSize);
            $("#pageSizeTop").val(Pager.pageSize);
        }

        return false;
    },
    Reset: function () {
        Pager.actualPage = 1;
        Pager.totalCount = 0;
        Pager.isInSearchMode = false;
        return false;
    },
    MakePaging: function () {
        if (Pager.isInSearchMode == true) {
            var query = MNG.getSearchTerm();
            MNG.RefreshObjects = function () {
                MNG.Sort.setArrow();
                MNG.search(query);
            };
        }
        else {
            MNG.RefreshObjects = MNG.RefreshObjectsByGroup;
        }

        MNG.RefreshObjects();
        return false;
    },
    SetPageSize: function (pageSize) {
        Pager.pageSize = pageSize;
        MNG.Prefs.save("PageSize", Pager.pageSize);
        //Pager.pageCount = (Pager.totalCount - Pager.totalCount % Pager.pageSize) / Pager.pageSize + 1;
        Pager.pageCount = Math.max(1, Math.ceil(Pager.totalCount / Pager.pageSize));
        if (Pager.actualPage > Pager.pageCount) {
            Pager.actualPage = 1;
        }
        return false;
    },
    SetTotalObjectsCount: function (totalCount) {
        Pager.totalCount = totalCount;
        //Pager.pageCount = (Pager.totalCount - Pager.totalCount % Pager.pageSize) / Pager.pageSize + 1;
        Pager.pageCount = Math.max(1, Math.ceil(Pager.totalCount / Pager.pageSize));
        if (Pager.actualPage > Pager.pageCount) {
            Pager.SetActualPage(Pager.pageCount);
        }
        return false;
    },
    SetActualPage: function (actualPage) {
        Pager.actualPage = actualPage;
        MNG.Prefs.save("ActualPage", Pager.actualPage);
    },
    GetDisplayInfo: function () {
        var info;
        var endInterval = Pager.actualPage * Pager.pageSize;

        if (endInterval > Pager.totalCount) {
            info = String.format("@{R=Core.Strings;K=WEBJS_AK0_54;E=js}", ((Pager.actualPage - 1) * Pager.pageSize + 1), Pager.totalCount, Pager.totalCount);
        }
        else {
            info = String.format("@{R=Core.Strings;K=WEBJS_AK0_54;E=js}", ((Pager.actualPage - 1) * Pager.pageSize + 1), endInterval, Pager.totalCount);
        }

        return info;
    },
    LoadSettings: function () {
        var pageSize = MNG.Prefs.load("PageSize", Pager.defaultPageSize);
        Pager.SetPageSize(pageSize);
        var totalCount = MNG.Prefs.load("TotalCount", 0);
        Pager.SetTotalObjectsCount(totalCount);
        var actualPage = MNG.Prefs.load("ActualPage", 1);
        Pager.actualPage = actualPage;
        return false;
    },
    PageSizeChanging: function (e) {
        if (e.which == 13) {
            if ($(this).val() < 1)
            {
                $(this).val(1);
                alert(String.format("@{R=Core.Strings;K=WEBJS_AK0_49;E=js}", Pager.defaultPageSize, Pager.maxPageSize));
            }
            else
            {
                if ($(this).val() > Pager.maxPageSize) {
                    $(this).val(Pager.maxPageSize);
                    alert(String.format("@{R=Core.Strings;K=WEBJS_AK0_50;E=js}", Pager.maxPageSize, Pager.defaultPageSize));
                }
            }
            Pager.SetPageSize($(this).val());
            Pager.MakePaging();
            Pager.Init();
            return false;
        }
        else {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        }
    },
    PageNumberChanging: function (e) {
        if (e.which == 13) {
            if ($(this).val() < 1 || $(this).val() > Pager.pageCount) {
                $(this).val(Pager.actualPage);
                return false;
            }
            else {
                Pager.SetActualPage($(this).val());
                Pager.MakePaging();
                Pager.Init();
                return false;
            }
        }
        else {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //$("#pageNumber").val(Pager.actualPage);
                return false;
            }
        }
    },
    Hide: function () {
        $("#extPager").hide();
        $("#extPagerTop").hide();
    },
    GetSetting: function () {
        return "pageSize: " + Pager.pageSize + " actualPage: " + Pager.actualPage + " totalCount: " + Pager.totalCount + " pageCount: " + Pager.pageCount;
    }
};

ColumnCategories = {
    SYSTEM_PROPERTIES: 0,
    NODE_CUSTOM_PROPERTIES: 1,
    INTERFACE_CUSTOM_PROPERTIES: 2
};

var MNG = {
    allSelected: false,
    allowInterfaces: function() {
        var allowInterfaces = $('#AllowInterfaces').val();
        if (allowInterfaces == "True")
            return true;
        return false;
    },
    isNPMInstalled: function () {
        var isNpmInstalled = $('#IsNPMInstalled').val();
        if (isNpmInstalled == "True")
            return true;
        return false;
    },
    isEnergyWise: function () {
        var isEnergyWise = $('#IsEnergyWise').val();
        if (isEnergyWise == "True")
            return true;
        return false;
    },
    RefreshObjects: null,
    CancelLoading: function() { /* do nothing */ },
    expandImg: '/Orion/images/Button.Expand.gif',
    collapseImg: '/Orion/images/Button.Collapse.gif',
    //chooseColumnsImg: '/Orion/images/double_chevron_rt.gif',
    editPropertiesUrl: '/Orion/Nodes/{0}Properties.aspx?ReturnTo={1}',
    interfaceEditPropertiesUrl: '/Orion/Interfaces/{0}Properties.aspx',
    listResourcesUrl: '/Orion/Nodes/ListResources.aspx?Nodes={0}&ReturnTo={1}&SubType={2}',
    customPollerUrl: '/Orion/NPM/{0}CustomPollers.aspx?ReturnTo={1}',
    nodePageSize: 250,
    suspendEnableDisableMenu: false,
    Nodes: {
        AvailableColumns: [
			{ sortType: "Node", sortProp: "IPAddress", header: "IP Address", headerText: "@{R=Core.Strings;K=WEBJS_TP0_5;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
            { sortType: "Node", sortProp: "IPAddressType", header: "IP Version", headerText: "@{R=Core.Strings;K=WEBJS_TM0_95;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES }
            //,{ sortType: "Node", sortProp: "StatusDescription", header: "Status", headerText: "@{R=Core.Strings;K=WEBJS_AK0_6;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
			//{ sortType: "Node", sortProp: "Contact", header: "Contact", headerText: "@{R=Core.Strings;K=WEBJS_AK0_24;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
			//{ sortType: "Node", sortProp: "Location", header: "Location", headerText: "@{R=Core.Strings;K=WEBJS_AK0_23;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
			//{ sortType: "Node", sortProp: "ObjectSubType", header: "Polling Method", headerText: "@{R=Core.Strings;K=DiscoveryWizardResultsStep_5;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
            //{ sortType: "Node", sortProp: "ServerName", header: "Polling Engine", headerText: "@{R=Core.Strings;K=WEBJS_AK0_42;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
            //{ sortType: "Node", sortProp: "UnManageFrom", header: "Planned Outage", headerText: "@{R=Core.Strings;K=WEBJS_VS1_2;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES }
		],
        DefaultColumns: "IP Address,Status"
    },
    Interfaces: {
        AvailableColumns: [
			{ sortType: "Node", sortProp: "Caption", selectAlias: "NodeCaption", header: "Node", headerText: "@{R=Core.Strings;K=WEBJS_AK0_44;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
			{ sortType: "Interface", sortProp: "TypeDescription", header: "Type", headerText: "@{R=Core.Strings;K=WEBJS_AK0_7;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
			{ sortType: "Node", sortProp: "Contact", header: "Contact", headerText: "@{R=Core.Strings;K=WEBJS_AK0_24;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES },
            { sortType: "Interface", sortProp: "UnManageFrom", header: "Planned Outage", headerText: "@{R=Core.Strings;K=WEBJS_VS1_2;E=js}", category: ColumnCategories.SYSTEM_PROPERTIES }
		],
        DefaultColumns: "Node,Type"
    },
    NetObjects: null,
    Sort: {
        type: "Node",
        prop: "Caption",
        ascending: true,
        getSortHeader: function() {
            return $('th[sortProp=\'' + MNG.Sort.prop + '\'][sortType=\'' + MNG.Sort.type + '\']');
        },
        setArrow: function() {
            
            $("#sortArrow").parent().removeClass('sortColumn');
            var $element = $(MNG.Sort.getSortHeader()).find('table th:eq(0)');
            if ($element.length > 0) {
                $("#sortArrow").attr('src', '/Orion/Nodes/images/icons/arrow_sort' + (MNG.Sort.ascending ? 'Asc' : 'Desc') + '.gif').appendTo($element);
            }
            else {
                $("#sortArrow").attr('src', '/Orion/Nodes/images/icons/arrow_sort' + (MNG.Sort.ascending ? 'Asc' : 'Desc') + '.gif').appendTo(MNG.Sort.getSortHeader());
            }

            $("#sortArrow").parent().addClass('sortColumn');
            
        },
        getOrderBy: function(nodeAlias, interfaceAlias) {
            var tableAlias = (MNG.Sort.prop == "ServerName") ? "E" : (MNG.Sort.type == "Node") ? nodeAlias : interfaceAlias;

            if (MNG.Sort.prop == "UnManageFrom") {
                return " ORDER BY {0}{0}.{1} {3}, {0}{0}.{2} {3}".format([tableAlias, "UnManageFrom", "UnManageUntil", MNG.Sort.ascending ? " ASC" : " DESC"]);
            }

            return " ORDER BY {0}.{1} {2}".format([tableAlias, MNG.Sort.prop, MNG.Sort.ascending ? " ASC" : " DESC"]);
        },
        by: function(newType, newProp) {
            if (newProp == this.prop && newType == this.type) {
                this.ascending = !this.ascending;
            } else {
                this.ascending = true;
                this.prop = newProp;
                this.type = newType;
            }
            MNG.Prefs.save(MNG.NetObjects.typename + "SortPropType", this.type);
            MNG.Prefs.save(MNG.NetObjects.typename + "SortProp", this.prop);
            MNG.Prefs.save(MNG.NetObjects.typename + "SortAsc", this.ascending);
            this.setArrow();
        },
        loadPrefs: function() {
            this.type = MNG.Prefs.load(MNG.NetObjects.typename + "SortPropType", "Node");
            this.prop = MNG.Prefs.load(MNG.NetObjects.typename + "SortProp", "Caption");
            if (this.getSortHeader().length == 0) {
                this.type = "Node";
                this.prop = "Caption";
            }
            this.ascending = (MNG.Prefs.load(MNG.NetObjects.typename + "SortAsc", "true") != "false");
            this.setArrow();
        }
    },
    Prefs: {
        load: function(prefName, defaultValue) {
            return $("#WebNodeManagement_" + prefName).val() || defaultValue;
        },
        save: function(prefName, value) {
            $("#WebNodeManagement_" + prefName).val(value);
        }
    },
    removeBrackets: function(ident) {
        if (ident.charAt(0) === '[' && ident.charAt(ident.length - 1) === ']')
            return ident.slice(1, ident.length - 1);
        else
            return ident;
    }
};

var _InterfacesBaseWithEnergyWise = " FROM Orion.NPM.Interfaces (nolock=true) I \
        INNER JOIN Orion.Nodes (nolock=true) N ON I.NodeID=N.NodeID \
        INNER JOIN Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID \
        LEFT JOIN Orion.NPM.EW.Nodes (nolock=true) EW ON (I.NodeID = EW.NodeID) \
        WHERE ";

var _InterfacesBaseNoEnergyWise = " FROM Orion.NPM.Interfaces (nolock=true) I \
        INNER JOIN Orion.Nodes (nolock=true) N ON I.NodeID=N.NodeID \
        INNER JOIN Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID \
        WHERE ";

var SWQLBaseQueries = {
    InterfacesBase: (MNG.isEnergyWise()) ? _InterfacesBaseWithEnergyWise : _InterfacesBaseNoEnergyWise,
    NodesBase:
        " From Orion.Nodes (nolock=true) N \
        INNER JOIN Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID \
        WHERE ",
    NpmNodesBase:
        " From Orion.Nodes (nolock=true) N \
        INNER JOIN Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID \
        LEFT JOIN Orion.NPM.EW.Nodes (nolock=true) EW ON (N.NodeID = EW.NodeID) WHERE "
};

var SWQLQueries = {
    SelectInterfaceIds:
        "SELECT I.InterfaceID" + SWQLBaseQueries.InterfacesBase,

    SelectInterfaceAndEngineIds:
        "SELECT N.EngineID, I.InterfaceID" + SWQLBaseQueries.InterfacesBase,

    SelectInterfaceIdsAndNames:
        "SELECT I.InterfaceID, I.Caption " + SWQLBaseQueries.InterfacesBase,

    SelectNodeIds:
	    "SELECT N.NodeID " + SWQLBaseQueries.NodesBase,

    SelectNodeAndEngineIds:
	    "SELECT N.EngineID, N.NodeID " + SWQLBaseQueries.NodesBase,

    SelectNpmNodeIds:
	    "SELECT N.NodeID " + SWQLBaseQueries.NpmNodesBase,

    SelectNpmNodeAndEngineIds:
	    "SELECT N.EngineID, N.NodeID " + SWQLBaseQueries.NpmNodesBase,

    SelectSNMPNodesIds:
	    "SELECT N.NodeID FROM Orion.Nodes (nolock=true) N WHERE N.ObjectSubType = 'SNMP' AND ",

    SelectUnmanageNodeCount:
	    "SELECT COUNT(N.NodeID) AS Cnt " + SWQLBaseQueries.NodesBase + " N.UnManaged = 1 AND ",

    SelectManageNodeCount:
	    "SELECT COUNT(N.NodeID) AS Cnt " + SWQLBaseQueries.NodesBase + " N.UnManaged = 0 AND ",

    SelectNpmUnmanageNodeCount:
	    "SELECT COUNT(N.NodeID) AS Cnt " + SWQLBaseQueries.NpmNodesBase + " N.UnManaged = 1 AND ",

    SelectNpmManageNodeCount:
	    "SELECT COUNT(N.NodeID) AS Cnt " + SWQLBaseQueries.NpmNodesBase + " N.UnManaged = 0 AND ",

    SelectUnmanageInterfaceCount:
	    "SELECT COUNT(I.InterfaceID) AS Cnt " + SWQLBaseQueries.InterfacesBase + " I.UnManaged = 1 AND ",

    SelectManageInterfaceCount:
	    "SELECT COUNT(I.InterfaceID) AS Cnt " + SWQLBaseQueries.InterfacesBase + " I.UnManaged = 0 AND ",

    SelectNonExternalNodeCount:
        "SELECT COUNT(N.NodeID) AS Cnt FROM Orion.Nodes N WHERE N.Status <> 11 AND ",

    GetSearchWhereFromPage: function () {
        var fields = { Node: [], Interface: [] };

        $("th[sortProp]").each(function () {
            fields[$(this).attr('sortType')].push($(this).attr('sortProp'));
        });

        var query = MNG.getSearchTerm();

        var searchClauses = function (fields, tableAlias) {
            var searchClause = function (field) { return "{0}.{1} LIKE '%{2}%'".format([tableAlias, field, MNG.quoteValueForSql(query)]); }
            return $.map(fields, searchClause);
        };

        var clauses = searchClauses(fields.Node, "N").concat(searchClauses(fields.Interface, "I"));
        return "(" + clauses.join(" OR ") + ")";
    }
};

var Utils = {
    // returns selected option for grouping combo like Vendor, Machine Type, SNMP Version ...
    SelectedType: function () {
        return $("#groupByProperty option:selected").attr("propertyType");
    },

    // returns selected option value for grouping combo like Vendor, MachineType, SNMPVersion ...
    SelectedProperty: function () {
        return $("#groupByProperty option:selected").val();
    },

    // returns selected list item
    SelectedItem: function () {
        return $(".SelectedNodeGroupItem a");
    },

    // returns selected list item value
    SelectedValue: function () {
        return $(".SelectedNodeGroupItem a").attr('value');
    },

    EnableByID: function (id, enabled) {
        if (enabled) {
            $("#" + id).parent().removeClass("Disabled");
        }
        else {
            $("#" + id).parent().addClass("Disabled");
        }
    },

    IsEnabledByID: function (id) {
        return !$("#" + id).parent().hasClass("Disabled");
    }
};

var GUI = {
    allPagesSelected: false,

    SelectAllPages: function (value) {
        GUI.allPagesSelected = value;

        GUI.EnableDeleteButton(value);
    },

    EnableDeleteButton: function (enabled) { Utils.EnableByID("deleteLink"); },

    ProgressDiscDlg: function (options) {
        var width = 600;
        var bufferSize = 100;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var progressText = $('<div></div>').addClass('ProgressText').appendTo(statusBox);
        var lastError = $('<div></div>').addClass('LastError').appendTo(statusBox).hide();

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_VB1_5;E=js}', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;
        var updateTree = function (arr, res) {
            if (arr.length >= 2) {
                if (arr[0].toUpperCase() == "N") {

                    $("img.StatusIcon[NetObject=N:" + arr[1] + "]").attr('src', '/Orion/images/StatusIcons/Small-' + res[0] + ((res[0].indexOf('-') < 0) || MNG.BlinkingNodeIcons ? "" : "-noblink") + '.gif');
                    $.each($("input:checkbox[name=N:" + arr[1] + "]"), function () {

                        $(this).data('netobject').Unmanaged = (res[2] == 'True');
                        for (var n = 0; n < MNG.Nodes.selectedColumns.length; ++n) {
                            if (MNG.Nodes.selectedColumns[n].sortProp == "StatusDescription") {
                                $(this).parent().parent().find("td:eq(" + (n + 2) + ")").text(res[1]);
                                break;
                            }
                        }
                    });
                }
                else if (arr[0].toUpperCase() == "I") {
                    $("img.StatusIcon[NetObject='I:" + arr[1] + "']").attr('src', '/Orion/images/StatusIcons/Small-' + res[0] + '.gif');
                    $.each($("input:checkbox[name='I:" + arr[1] + "']"), function () {
                        $(this).data('netobject').AdminStatus = res[1];
                    });
                }
            }
        };

        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');

            if (succeeded && ++numSucceeded >= options.items.length) {
                MNG.EnableDisableMenu();
                setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1500);
            }

            if (numFinished == numSucceeded) {
                progressText.text("@{R=Core.Strings;K=WEBJS_AK0_51;E=js}".format([numFinished, options.items.length]));
            }
            else {
                progressText.text("@{R=Core.Strings;K=WEBJS_AK0_52;E=js}".format([numFinished, options.items.length, numFinished - numSucceeded]));
            }

            if (options.afterFinished && options.items.length == numFinished) {
                options.afterFinished();
            }

        };

        var firstItems;
        var bufferedItems;
        if (options.items.length > bufferSize) {
            firstItems = options.items.slice(0, bufferSize);
            bufferedItems = options.items.slice(bufferSize, options.items.length);
        }
        else {
            firstItems = options.items;
            bufferedItems = new Array();
        }

        var serverMethodCall = function (item) {
            var ids = [];
            ids.push(options.getArg(item).toString());

            if ($.browser.msie && $.browser.version.substr(0, 1) > 7) {
                $.ajax({
                    type: 'Post',
                    url: '/Orion/Services/NodeManagement.asmx/' + options.methodName,
                    data: "{'netObjectIds':" + "'" + ids + "\'}",
                    contentType: 'application/json; charset=utf-8',
                    timeout: '12000000',
                    success: function (result) {
                        onFinished(true);

                        var m = /^\{"d":"([^\u0000]*)"\}$/.exec((result || ''));

                        if (m && m[1]) {
                            var arr = item.split(":");
                            var res = m[1].split("::");
                            updateTree(arr, res);
                        }

                        if (bufferedItems.length > 0)
                            serverMethodCall(bufferedItems.pop());
                    },
                    error: function (error) {
                        MNG.ReportError(error, lastError);

                        onFinished(false);

                        if (bufferedItems.length > 0)
                            serverMethodCall(bufferedItems.pop());
                    }
                });
            }
            else {
                options.serverMethod(options.getArg(item).toString(), function (result) {
                    onFinished(true, item);

                    if (result) {
                        var arr = item.split(":");
                        var res = result.split("::");
                        updateTree(arr, res);
                    }

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());

                }, function (error) {
                    options.afterFail(error, lastError);

                    onFinished(false);

                    if (bufferedItems.length > 0)
                        serverMethodCall(bufferedItems.pop());
                });
            }
        };

        $.each(firstItems, function () {
            serverMethodCall(this);
        });

        statusBox.dialog({
            width: width, height: 110, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });
    },

    ProgressDlg: function (options) {
        var width = 800;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var statusItems = $('<ul></ul>').addClass('InterfacesText').appendTo(statusContent);

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_VB1_5;E=js}', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('destroy').remove(); });
        $('body').append(statusBox);
        var numFinished = 0, numSucceeded = 0;
        var onFinished = function (succeeded) {
            progressBarInner.width((++numFinished / options.items.length) * 100.0 + '%');

            if (succeeded && ++numSucceeded >= options.items.length)
                $('.StatusDialog').fadeOut('slow', function () { statusBox.dialog('destroy').remove(); });
        };
        $.each(options.items, function () {
            var item = this;

            var statusBlock = $('<li></li>').appendTo(statusItems);
            var spinner = $('<span>&nbsp;</span>').addClass('Spinner').appendTo(statusBlock);
            var caption = $('<span></span>').text(options.getObject(item)).appendTo(statusBlock);

            options.serverMethod(options.getArg(item), function () {
                statusBlock.append(' - ' + '<font color="green">ok</font>');
                spinner.css({ 'display': 'none' });
                options.afterSucceeded(item);
                onFinished(true);
            }, function (error) {
                var before = statusItems.height();
                statusBlock.append(' - ' + '<font color="red">' + error.get_message() + '</font>');
                spinner.css({ 'display': 'none' });
                if (statusItems.height() > before) {
                    var val = $('.StatusDialog').dialog("option", "height") + statusItems.height() - before;
                    $('.StatusDialog').dialog("option", "height", val);
                }
                onFinished(false);
            });
        });

        statusBox.dialog({
            width: width, height: statusContent.height() + 70, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });
    },

    ProgressTopologyDlg: function (options) {
        var width = 600;
        var statusBox = $('<div></div>').addClass('StatusDialog').width(width);
        var statusContent = $('<div></div>').appendTo(statusBox);
        var progressBarOuter = $('<div></div>').addClass('ProgressBarOuter').appendTo(statusContent);
        var progressBarInner = $('<div></div>').addClass('ProgressBarInner').appendTo(progressBarOuter);
        var progressText = $('<div></div>').addClass('ProgressText').appendTo(statusBox);
        var lastError = $('<div></div>').addClass('LastError').appendTo(statusBox).hide();

        $(SW.Core.Widgets.Button('@{R=Core.Strings;K=WEBJS_VB1_5;E=js}', { cls: 'CloseButton' })).appendTo(statusBox).click(function () { statusBox.dialog('close').remove(); });
        $('body').append(statusBox);
        var onFinished = function (succeeded) {
            progressBarInner.width(100.0 + '%');

            if (succeeded) {
                setTimeout("$('.StatusDialog').fadeOut('slow', function() { $('.StatusDialog').dialog('destroy').remove(); })", 1500);
                progressText.text("@{R=Core.Strings;K=WEBJS_TM0_110;E=js}");
            }
            else {
                progressText.text("@{R=Core.Strings;K=WEBJS_TM0_111;E=js}");
            }
        };
        var serverMethodCall = function () {
            {
                options.serverMethod(function (result) {
                    onFinished(true);
                }, function (error) {
                    MNG.ReportError(error, lastError);
                    onFinished(false);
                });
            }
        };

        serverMethodCall(this);

        statusBox.dialog({
            width: width, height: 110, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, title: options.title
        });
    },

    InfoDialog: function (title, text) {
        //create the dialog window if it doesn't already exist
        if (!pollDialog) {
            pollDialog = new Ext.Window({
                applyTo: 'pollDialog',
                layout: 'fit',
                width: 310,
                height: 205,
                closeAction: 'destroy',
                modal: true,
                plain: true,
                resizable: false,
                //ids: ids,
                items: new Ext.BoxComponent({
                    applyTo: 'pollDialogBody',
                    layout: 'fit',
                    border: false
                }),
                buttons: [
                        {
                            text: '@{R=Core.Strings;K=WEBJS_AK0_53;E=js}',
                            handler: function () {
                                pollDialog.hide();
                            }
                        }
                        ]
            });
        }
        $("#pollDialogHeader").text(title);
        $("#pollDialogDescription").text(text);
        // Set the location
        pollDialog.alignTo(document.body, "c-c");
        pollDialog.show();
    }
};

// Operation delagates
var Operations = {

    ShowStringArray: function (value) {
        //alert(value.join(","));
    },

    DeleteObjects: function (value) {
        if (confirm(String.format(value.length == 1 ? '@{R=Core.Strings;K=WEBJS_TM0_101;E=js}' : '@{R=Core.Strings;K=WEBJS_TM0_100;E=js}', value.length))) {
            NodeManagement.DeleteObjects(value);
            MNG.LoadGroupByValues();
        }
    },

    DeleteNetObjects: function (value) {
        if (confirm(String.format(value.length == 1 ? '@{R=Core.Strings;K=WEBJS_TM0_101;E=js}' : '@{R=Core.Strings;K=WEBJS_TM0_100;E=js}', value.length))) {
            NodeManagement.DeleteNetObjects(value, function () { }, function (error) {
                if (error.get_message() == "Authentication failed.") {
                    // login cookie expired. reload the page so we get bounced to the login page.
                    alert('@{R=Core.Strings;K=WEBJS_AK0_26;E=js}');
                    window.location.reload();
                } else {
                    window.location = "/Orion/WCFError.aspx?Message=" + error.get_message();
                }
            });
            MNG.LoadGroupByValues();
        }
    },

    UnmanageObjects: function (objectsIds) {
        var ids = { N: [], I: [] };

        if (MNG.NetObjects.SelectedPrefix() == "N") {
            $(objectsIds).each(function () { ids.N.push(this); });
        }
        else if (MNG.NetObjects.SelectedPrefix() == "I" || MNG.NetObjects.SelectedPrefix() == "IW") {
            $(objectsIds).each(function () { ids.I.push(this); });
        }

        showUnmanageDialog(ids);
    },

    EditProperties: function (joinedIds) {
        if (MNG.NetObjects.SelectedPrefix() == "N") {
            $('body').append(Operations.PreparePostForm("Node", joinedIds, $('#ReturnToUrl').val()));
        }
        else if (MNG.NetObjects.SelectedPrefix() == "I" || MNG.NetObjects.SelectedPrefix() == "IW") {
            $('body').append(Operations.PreparePostForm("Interface", joinedIds, $('#ReturnToUrl').val()));
        }

        $('#selectedNetObjects').submit();
    },

    DoDelete: function (NodeIds) {
        GUI.ProgressDiscDlg({
            title: "@{R=Core.Strings;K=WEBJS_AK0_48;E=js}",
            items: NodeIds,
            serverMethod: NodeManagement.DeleteObjNow,
            methodName: 'DeleteObjNow',
            getArg: function (id) { return [id]; },
            afterSucceeded: function (element, lastError) { },
            afterFinished: function () { MNG.LoadGroupByValues(); },
            afterFail: function (error, lastError) {
                MNG.ReportError(error, lastError);
            }
        });
    },

    DoPollNow: function (ids) {
        GUI.ProgressDiscDlg({
            title: "@{R=Core.Strings;K=WEBJS_AK0_47;E=js}",
            items: ids,
            serverMethod: NodeManagement.PollNetObjNow,
            methodName: 'PollNetObjNow',
            getArg: function (id) { return [id]; },
            afterFinished: function () {
                GUI.InfoDialog("@{R=Core.Strings;K=WEBJS_AK0_57;E=js}", "@{R=Core.Strings;K=WEBJS_AK0_58;E=js}");
                $('#lastErrorMessage').hide();
            },
            afterFail: function (error, lastError) {
                MNG.ReportError(error, lastError);
            }
        });

        //                NodeManagement.PollNow(ids.NetObjectIDs, function() {
        //                    $.each(ids.N, function() { MNG.UpdateStatusIconForNode(this); });
        //                    $.each(ids.I, function() { MNG.UpdateStatusIconForInterface(this); });
        //                    // no status icons for volumes
        //                }, MNG.ReportError);
    },

    DoRediscoverNow: function (ids) {
        GUI.ProgressDiscDlg({
            title: "@{R=Core.Strings;K=WEBJS_AK0_45;E=js}",
            items: ids,
            serverMethod: NodeManagement.RediscoverNow,
            methodName: 'RediscoverNow',
            getArg: function (id) { return [id]; },
            afterFinished: function () {
                $('#lastErrorMessage').hide();
                GUI.InfoDialog("@{R=Core.Strings;K=WEBJS_AK0_60;E=js}", "@{R=Core.Strings;K=WEBJS_AK0_59;E=js}");
            },
            afterFail: function (error, lastError) {
                MNG.ReportError(error, lastError);
            }
        });

        //NodeManagement.Rediscover(ids.NetObjectIDs, function() { /*alert('Rediscovered ' + ids.NetObjectIDs);*/ }, MNG.ReportError);
    },

    ShutdownEnableInterface: function (ids, shutdown) {
        //Operations.ShowStringArray(ids);

        var GetObjectFunction;
        var Objects = [];

        if (ids.Name != null) {
            Objects = ids.ID;

            GetObjectNameFunction = function (interfaceId) {

                for (var i = 0; i < ids.ID.length; i++) {
                    if (ids.ID[i] == interfaceId) {
                        return ids.Name[i];
                    }
                }
                return "I" + interfaceId;
            };
        }
        else {

            Objects = ids;
            GetObjectNameFunction = function (interfaceId) { return $("input:checkbox[name='I:" + interfaceId + "']").data('netobject').Caption; }
        }

        if (!shutdown || window.confirm(Objects.length == 1 ? "@{R=Core.Strings;K=WEBJS_TM0_97;E=js}" : "@{R=Core.Strings;K=WEBJS_TM0_98;E=js}")) {
            GUI.ProgressDlg({
                title: shutdown ? "@{R=Core.Strings;K=WEBJS_AK0_62;E=js}" : "@{R=Core.Strings;K=WEBJS_AK0_63;E=js}",
                items: Objects,
                getObject: GetObjectNameFunction,
                serverMethod: shutdown ? NodeManagement.AdminstrativelyShutDown : NodeManagement.AdministrativelyEnable,
                getArg: function (interfaceId) { return ['I:' + interfaceId]; },
                afterSucceeded: MNG.UpdateStatusIconForInterface
            });
        }
    },

    ShutdownInterface: function (ids) {
        Operations.ShutdownEnableInterface(ids, true);
    },

    EnableInterface: function (ids) {
        Operations.ShutdownEnableInterface(ids, false);
    },

    OverridePowerLevel: function (ids) {
        if (ids.length) {
            showPowerLevelDialog(ids, '', true);
        }
        //window.location = "/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces=" + ids.I.join(",");
    },

    RemanageObjects: function (ids) {
        if (MNG.NetObjects.SelectedPrefix() == "N") {
            remanageNetObjects(ids, []);
        }
        else if (MNG.NetObjects.SelectedPrefix() == "I" || MNG.NetObjects.SelectedPrefix() == "IW") {
            remanageNetObjects([], ids);
        }
    },

    EnableDisableManageButton: function (cnt, managed) {
        if (managed) {
            if (cnt > 0) {
                Utils.EnableByID("unmanageLink", true);
            }
            else {
                Utils.EnableByID("unmanageLink", false);
            }
        }
        else {
            if (cnt > 0) {
                Utils.EnableByID("remanageLink", true);
            }
            else {
                Utils.EnableByID("remanageLink", false);
            }
        }
    },

    UpdateTopology: function () {
        GUI.ProgressTopologyDlg({
            title: "@{R=Core.Strings;K=WEBJS_TM0_99;E=js}",
            serverMethod: NodeManagement.CalculateSystemTopology,
            methodName: 'CalculateSystemTopology',
            afterFail: function (error, lastError) {
                MNG.ReportError(error, lastError);
            }
        });
    },

    PreparePostForm: function (netObjectType, ids, returnUrl) {
        var stringForm = "<form id='selectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='{1}s' value='{2}'/> \
                            <input type='hidden' name='ReturnTo' value='{3}'/> \
                             <input type='hidden' name='GuidID' value='{4}'/> \
                         </form>";
        var params = ids.join(",");
        var actionUrl = (netObjectType == "Interface") ? MNG.interfaceEditPropertiesUrl.format([netObjectType, returnUrl]) : MNG.editPropertiesUrl.format([netObjectType, returnUrl]);
        var guid = guidGenerator();
        return $(stringForm.format([actionUrl, netObjectType, params, returnUrl, guid]));
    },

    PrepareExportPostForm: function (netObjectType, ids, returnUrl) {
        var stringForm = "<form id='selectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='EntityType' value='{1}'/> \
                            <input type='hidden' name='NetObjectsIds' value='{2}'/> \
                         </form>";
        var params = (ids.length) ? ids.join(",") : '';
        var actionUrl = "/Orion/Admin/CPE/ExportCP.aspx?ReturnTo=" + returnUrl;
        return $(stringForm.format([actionUrl, netObjectType, params]));
    },

    PrepareCPPostForm: function (netObjectType, ids, returnUrl) {
        var stringForm = "<form id='selectedNetObjects' action='{0}' method='POST'> \
                            <input type='hidden' name='CustomPropertyIds' value='{1}'/> \
                         </form>";
        var cpIds = [];
        if (ids.length) {
            $(ids).each(function () {
                cpIds.push(netObjectType + ":" + this);
            });
        }
        var params = (cpIds.length) ? cpIds.join(",") : netObjectType + ':';
        var actionUrl = "/Orion/Admin/CPE/InlineEditor.aspx?ReturnTo=" + returnUrl;
        return $(stringForm.format([actionUrl, params]));
    },

    PrepareImportPostForm: function (netObjectType, returnUrl) {
        var stringForm = "<form id='selectedEntityType' action='{0}' method='POST'> \
                            <input type='hidden' name='EntityType' value='{1}'/> \
                         </form>";
        var actionUrl = "/Orion/Admin/CPE/ImportCP.aspx?ReturnTo=" + returnUrl;
        return $(stringForm.format([actionUrl, netObjectType]));
    }
};

function guidGenerator() {
    var guidPiece = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (guidPiece() + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + "-" + guidPiece() + guidPiece() + guidPiece());
}

MNG.Nodes.typename = "Node";
MNG.Interfaces.typename = "Interface";

formatColumnHeader = function (c) {
    if (c.sortProp.indexOf('CustomProperties.') > -1) {
        if (c.sortType == MNG.Interfaces.typename)
            return '<th sortType="{sortType}" sortProp="{sortProp}" title="@{R=Core.Strings;K=WEBJS_IB0_45;E=js}">{headerText} <span class="cp-hint">@{R=Core.Strings;K=WEBJS_IB0_46;E=js}</span> </th>'.format(c);
        return '<th sortType="{sortType}" sortProp="{sortProp}" title="@{R=Core.Strings;K=WEBJS_IB0_47;E=js}">{headerText} <span class="cp-hint">@{R=Core.Strings;K=WEBJS_IB0_46;E=js}</span></th>'.format(c);
    }
    return '<th sortType="{sortType}" sortProp="{sortProp}"><span>{headerText} </span></th>'.format(c);
};
MNG.Nodes.createColumnHeaders = function () {
    $('#NodeTree2 thead tr').html('<th><input type="checkbox" id="selectAll" /></th>\
							<th sortType="Node" sortProp="Caption">@{R=Core.Strings;K=WEBJS_AK0_9;E=js} <img src="" alt="" style="margin-right: 10px" id="sortArrow" /></th>' +
							$.map(MNG.Nodes.selectedColumns, formatColumnHeader).join(''));

    var $element = $('#NodeTree2 thead tr th:last');
    $element.css({ "padding": "0" });
    $('.nodeCommand').show();
    MNG.SetMenuWidth();
};

MNG.Interfaces.createColumnHeaders = function () {
    $('#NodeTree2 thead tr').html('<th><input type="checkbox" id="selectAll" /></th>\
							<th sortType="Interface" sortProp="Caption">@{R=Core.Strings;K=WEBJS_AK0_9;E=js} <img src="" alt="" id="sortArrow" /></th>' +
							$.map(MNG.Interfaces.selectedColumns, formatColumnHeader).join(''));

    var $element = $('#NodeTree2 thead tr th:last');
    $element.css({ "padding": "0" });
    var content = $element[0].innerHTML;
    content = "<table style='border-collapse: collapse; border: 0; width: 100%;'><thead><tr><th style='width: 100%; padding: 0 5px'>" + content + "</th><th style='text-align: right' onclick='var event = arguments[0] || window.event; event.cancelBuble = true; event.stopPropagation ? event.stopPropagation() : event.cancelBubble = true;'></th></tr></thead></table>";
    $element.html(content);

    $('.nodeCommand').hide();
    MNG.SetMenuWidth();
};
MNG.DoQuery = function (query, succeeded) {

    MNG.clearError();
    Information.Query(query, function (result) {
        var table = [];
        for (var y = 0; y < result.Rows.length; ++y) {
            var row = result.Rows[y];
            var tableRow = {};
            for (var x = 0; x < result.Columns.length; ++x) {
                tableRow[result.Columns[x]] = row[x];
            }
            table.push(tableRow);
        }
        succeeded({ Rows: table });
        $('#lastErrorMessage').hide();
    }, function (error) {
        MNG.ReportError(error);
        //			$("#originalQuery").text(query);
        //			$("#test").text(error.get_message());
        //			$("#stackTrace").text(error.get_stackTrace());
    });
};

MNG.SaveUserSetting = function(name, value) {
	$("#" + name).val(value);
	NodeManagement.SaveUserSetting(name, value, function() { $('#lastErrorMessage').hide(); },
    function(error) {
        MNG.ReportError(error);
    });
	/*function(error) {
		$("#originalQuery").text(name + '=' + value);
		$("#test").text(error.get_message());
		$("#stackTrace").text(error.get_stackTrace());
	});*/
};

MNG.clearError = function() {
	$("#originalQuery,#test,#stackTrace").text('');
};

MNG.LoadGroupByValues = function () {
    Pager.Hide();
    // cause hiding select all panel if displayed
    MNG.DisplaySelectPan(false);
    var prop = $("#groupByProperty option:selected").val();

    var nonParsedTypes = ["SYSTEM.SINGLE", "SYSTEM.INT32", "SYSTEM.DATETIME"];
    var propType = $("#groupByProperty option:selected").attr("propertyType");

    if (propType == null || propType == undefined)
        propType = 'unknown';

    MNG.Prefs.save('GroupBy', prop);
    if (prop) {
        MNG.ClearTable();
        var nodeGroupItems = $(".NodeGroupItems").text("@{R=Core.Strings;K=WEBJS_VB0_1;E=js}");
        // Due to current IS version incorrectly parsing nested queries we have to use such select
        var SQL;

        if (prop == 'Outage') {
            // Get select for outage groups for nodes or interfaces
            if (MNG.NetObjects.typename == "Node") {
                SQL = " SELECT NULL AS UnManageFrom, NULL AS UnManageUntil, Count(N.NodeId) AS Cnt, 'N' AS Prefix " +
                      " FROM Orion.Nodes (nolock=true) N " +
                      " WHERE N.Engine.ServerType <> 'RemoteCollector'"
                      " AND N.UnManageUntil < getUtcDate() OR N.UnManageUntil IS NULL " +
                      " UNION " +
                      " ( " +
                      "   SELECT UnManageFrom, UnManageUntil, Count(NodeId) AS Cnt, 'N' AS Prefix " + 
                      "   FROM Orion.Nodes (nolock=true) N " +
                      "   WHERE N.Engine.ServerType <> 'RemoteCollector'"
                      "   GROUP BY UnManageFrom, UnManageUntil " +
                      "   HAVING UnManageUntil > getUtcDate() AND UnmanageUntil IS NOT NULL  " +
                      " ) " +
                      " ORDER BY UnManageFrom ASC, UnManageUntil ASC ";
            }
            else {
                SQL = " SELECT NULL AS UnManageFrom, NULL AS UnManageUntil, Count(I.NodeId) AS Cnt, 'I' AS Prefix " +
                      " FROM Orion.NPM.Interfaces (nolock=true) I " +
                      " WHERE I.UnManageUntil < getUtcDate() OR I.UnManageUntil IS NULL " +
                      " AND N.Node.Engine.ServerType <> 'RemoteCollector' " +
                      " UNION " +
                      " ( " +
                      "     SELECT I.UnManageFrom, I.UnManageUntil, Count(InterfaceId) AS Cnt, 'I' AS Prefix " +
                      "     FROM Orion.NPM.Interfaces (nolock=true) I " +
                      "     WHERE I.Node.Engine.ServerType <> 'RemoteCollector' "
                      "     GROUP BY UnManageFrom, UnManageUntil " + 
                      "     HAVING UnManageUntil > getUtcDate() AND UnmanageUntil IS NOT NULL " +
                      " ) " +
                      " ORDER BY UnManageFrom ASC, UnManageUntil ASC ";
            }
        }
        else if (prop == 'EnergyWise') {
            if (MNG.NetObjects.typename == "Node") {
                SQL = "SELECT EW.EnergyWise AS Value, COUNT(ISNULL(EW.EnergyWise,'')) AS Cnt FROM Orion.NPM.EW.Nodes (nolock=true) EW LEFT JOIN Orion.Nodes (nolock=true) N ON (EW.NodeID = N.NodeID) WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY EW.EnergyWise";
            }
            else {
                SQL = "SELECT EW.EnergyWise AS Value, COUNT(I.InterfaceID) AS Cnt FROM Orion.NPM.EW.Nodes (nolock=true) EW LEFT JOIN Orion.Nodes (nolock=true) N ON (EW.NodeID = N.NodeID) LEFT JOIN Orion.NPM.Interfaces I ON N.NodeID=I.NodeID WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY EW.EnergyWise";
            }
        }
        else if (prop == 'EngineID') {
            if (MNG.NetObjects.typename == "Node")
                SQL = "SELECT E.EngineID as EngineID, ISNULL(E.ServerName,'') AS Value, COUNT(ISNULL(N.EngineID,'')) AS Cnt FROM Orion.Nodes (nolock=true) N Inner Join Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID WHERE E.ServerType <> 'RemoteCollector' GROUP BY E.EngineID, ISNULL(E.ServerName,'') ORDER BY E.EngineID, ISNULL(E.ServerName,'') ASC";
            else
                SQL = "SELECT E.EngineID as EngineID, ISNULL(E.ServerName,'') AS Value, COUNT(I.InterfaceID) AS Cnt FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.NPM.Interfaces (nolock=true) I ON N.NodeID=I.NodeID Inner Join Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID WHERE E.ServerType <> 'RemoteCollector' GROUP BY E.EngineID, ISNULL(E.ServerName,'') ORDER BY E.EngineID, ISNULL(E.ServerName,'') ASC";
        }
        else if (prop == 'VlanId') {
            if (MNG.NetObjects.typename == "Node")
                SQL = "SELECT NV.VlanId AS Value, Count(N.NodeID) AS Cnt FROM Orion.NodeVlans (nolock=true) NV RIGHT JOIN Orion.Nodes (nolock=true) N ON NV.NodeID=N.NodeID WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY NV.VlanId ORDER BY NV.VlanId ASC";
            else 
                SQL = "SELECT M.VlanId AS Value, COUNT(I.InterfaceIndex) AS Cnt FROM Orion.NPM.Interfaces (nolock=true) I LEFT JOIN Orion.NodePortInterfaceMap (nolock=true) M ON I.InterfaceIndex = M.IfIndex AND I.NodeID=M.NodeID LEFT JOIN Orion.Nodes N on I.NodeID = N.NodeID WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY (M.VlanId) ORDER BY M.VlanId ASC";
        }
        else {
            if (nonParsedTypes.contains(propType.toUpperCase())) {
                if (MNG.NetObjects.typename == "Node")
                    SQL = "SELECT N.{0} AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY N.{0} ORDER BY N.{0} ASC".format([prop]);
                else
                    SQL = "SELECT N.{0} AS Value, COUNT(I.InterfaceID) AS Cnt FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.NPM.Interfaces (nolock=true) I ON N.NodeID=I.NodeID WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY N.{0} ORDER BY N.{0} ASC".format([prop]);
            }

            else {
                if (MNG.NetObjects.typename == "Node")
                    SQL = "SELECT ISNULL(N.{0},'') AS Value, COUNT(ISNULL(N.{0},'')) AS Cnt FROM Orion.Nodes (nolock=true) N WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY ISNULL(N.{0},'') ORDER BY ISNULL(N.{0},'') ASC".format([prop]);
                else
                    SQL = "SELECT ISNULL(N.{0},'') AS Value, COUNT(I.InterfaceID) AS Cnt FROM Orion.Nodes (nolock=true) N LEFT JOIN Orion.NPM.Interfaces (nolock=true) I ON N.NodeID=I.NodeID WHERE N.Engine.ServerType <> 'RemoteCollector' GROUP BY ISNULL(N.{0},'') ORDER BY ISNULL(N.{0},'') ASC".format([prop]);
            }
        }

        MNG.DoQuery(SQL, function (result, eventArgs) {
            nodeGroupItems.empty();
            var nodeGroupItemsElement = $(".NodeGroupItems");

            $(result.Rows).each(function () {
                var value = this.Value;
                var engineID = (this.EngineID != undefined) ? this.EngineID : 0;

                //format disp to default culture patterns 
                var disp = ($.trim(String((Date.isInstanceOfType(value)) ?
                    value.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + " " + value.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern)
                    : (Number.isInstanceOfType(value)) ? String(value).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : this.Value))
                    || "@{R=Core.Strings;K=WEBJS_VB0_70;E=js}") + " (" + this.Cnt + ")";

                if (prop == "Status") {
                    disp = MNG.GetStatusText(String(this.Value)) + " (" + this.Cnt + ")";
                }

                if (prop == "IPAddressType") {
                    disp = MNG.GetIPAddressTypeText(String(this.Value)) + " (" + this.Cnt + ")";
                }

                if (prop == "ObjectSubType") {
                    disp = MNG.GetPollingMethodText(String(this.Value)) + " (" + this.Cnt + ")";
                }

                // Handle outage groups
                if (prop == "Outage") {
                    disp = MNG.FormatOutage(this.UnManageFrom, this.UnManageUntil, "@{R=Core.Strings;K=WEBJS_VS1_4;E=js}")
                           + " (" + this.Cnt + ")";

                    // Value for outage is stored as N/I;FROMms/NULL;TOms/NULL
                    if (this.UnManageFrom == null || this.UnManageUntil == null) {
                        value = this.Prefix + ";NULL;NULL";
                    }
                    else {
                        value = this.Prefix + ";" +
                            this.UnManageFrom.getTime() + ";" +
                            this.UnManageUntil.getTime();
                    }
                }
                else if (value == null) {
                    disp = "@{R=Core.Strings;K=WEBJS_VB0_70;E=js} (" + this.Cnt + ")";
                    value = "null";
                }
                else if (Date.isInstanceOfType(value)) {
                    //value = new Date(value.getTime() + value.getTimezoneOffset() * 60 * 1000); // to UTC
                    value = value.localeFormat(Sys.CultureInfo.InvariantCulture.dateTimeFormat.SortableDateTimePattern);
                }
                if (value != null && value.replace != undefined) {
                    value = value.replace(/\'/g, '&#39;');
                }

                if (engineID != null && engineID != undefined && engineID > 0) {
                    value = engineID;
                }

                var groupItem = $("<li class='NodeGroupItem'><a href='#' value='" + value + "'>" + disp + "</a></li>");
                $("a", groupItem).click(MNG.SelectGroup);
                groupItem = groupItem.appendTo(nodeGroupItemsElement);

            });

            if (MNG.RefreshObjects == MNG.RefreshObjectsByGroup) {

                var lastItem = MNG.Prefs.load("GroupByValue");
                //
                if (lastItem) {
                    $(".NodeGroupItem a[value='" + MNG.Prefs.load("GroupByValue") + "']").click();
                } else {
                    // try highlight the [Unknown] item if it exists in the list

                    if ($(".NodeGroupItem a[value='SNMP']").text()) {
                        $(".NodeGroupItem a[value='SNMP']").click();
                    }
                    else if ($(".NodeGroupItem a[value='ICMP']").text()) {
                        $(".NodeGroupItem a[value='ICMP']").click();
                    }
                    else if ($(".NodeGroupItem a[value='WMI']").text()) {
                        $(".NodeGroupItem a[value='WMI']").click();
                    }
                    else {
                        $(".NodeGroupItem a[value='']").click();
                    }
                }

            } else {
                MNG.RefreshObjects();
            }
            // add vendor icons
            if (prop === "Vendor") {
                MNG.AddVendorIcons();
            }
        });
    } else {
        $(".NodeGroupItems").empty();
        Pager.Reset();
        MNG.RefreshObjects();
    }
};

MNG.GetStatusText = function (statusValue) {
    switch (statusValue) {
        case "1": return "@{R=Core.Strings;K=StatusDesc_Up;E=js}";
        case "2": return "@{R=Core.Strings;K=StatusDesc_Down;E=js}";
        case "3": return "@{R=Core.Strings;K=StatusDesc_Warning;E=js}";
        case "9": return "@{R=Core.Strings;K=StatusDesc_Unmanaged;E=js}";
        case "11": return "@{R=Core.Strings;K=StatusDesc_External;E=js}";
        case "12": return "@{R=Core.Strings;K=StatusDesc_Unreachable;E=js}";
        default: return "@{R=Core.Strings;K=StatusDesc_Unknown;E=js}";
    }
};

MNG.GetIPAddressTypeText = function (addressType) {
    switch (addressType) {
        case "IPv4": return "@{R=Core.Strings;K=IP_V_4;E=js}";
        case "IPv6": return "@{R=Core.Strings;K=IP_V_6;E=js}";
        default: return addressType;
    }
};

MNG.GetPollingMethodText = function (pollingMethod) {
    switch (pollingMethod) {
        case "SNMP": return "@{R=Core.Strings;K=Discovery_NetworkSonarWizard_SNMP;E=js}";
        case "ICMP": return "@{R=Core.Strings;K=WEBJS_TM0_112;E=js}";
        case "WMI": return "@{R=Core.Strings;K=WEBJS_TM0_113;E=js}";
        default: return pollingMethod;
    }
};

MNG.GetObjectTypeText = function (objectType) {
    switch (objectType) {
        case "Orion.Nodes": return "@{R=Core.Strings;K=WEBJS_VB0_77;E=js}";
        case "Orion.NPM.Interfaces": return "@{R=Core.Strings;K=Entity_interfaces;E=js}";
        default: return objectType;
    }
};

MNG.SelectGroup = function() {
    $(this).parent().addClass("SelectedNodeGroupItem").siblings().removeClass("SelectedNodeGroupItem");    
    if (Pager.forbidReset == false) {
        Pager.Reset();        
    }
    else {
        Pager.forbidReset = false;
    }    
    MNG.RefreshObjects = MNG.RefreshObjectsByGroup;
    MNG.RefreshObjects();
    return false;
};

MNG.NumericTypes = ["System.Single", "System.Double", "System.Int32"];

MNG.RefreshObjects = MNG.RefreshObjectsByGroup = function () {
    MNG.CancelLoading();
    MNG.Sort.setArrow();

    if (!Utils.SelectedProperty() || Utils.SelectedItem().length > 0) {

        var value = Utils.SelectedValue();

        // check if value is not null (as an [Unknown] item has an empty string value, which we want to save)
        if (value != null) {
            MNG.Prefs.save('GroupByValue', value);
        }

        MNG.ClearTable();
        if ($("#selectAll").length > 0)
            $("#selectAll").get(0).checked = false;
        MNG.NetObjects.Load(MNG.GetWhere, 0);
    }
};

// Gets where statement for table alias like N, I, V based on filter
MNG.GetWhere = function (tableAlias, secondTableAlias) {
    if (Pager.isInSearchMode) {
        return SWQLQueries.GetSearchWhereFromPage();
    }

    var prop = Utils.SelectedProperty();
    var type = Utils.SelectedType();
    var value = Utils.SelectedValue();

    if (!prop || Utils.SelectedItem().length > 0) {
        if (prop) {

            if (prop == "Outage") {
                // Assemble where clause from outage definition " N/I;FROMms/NULL;TOms/NULL"
                // Dates are in UTC
                var array = value.split(";");

                var noWindowWhereFormat = "({0}.UnManageUntil < getUtcDate() OR {0}.UnManageUntil IS NULL)";

                if (array[1] == "NULL" || array[2] == "NULL") {
                    return noWindowWhereFormat.format([array[0]]);
                }

                var toDate = new Date(parseInt(array[2]));

                if (toDate.getFullYear() == 1899)
                    return noWindowWhereFormat.format([array[0]]);

                // Need to convert data to local time format because ISO time format contains time zone offset
                var from = SW.Core.DateHelper.utcToLocal(new Date(parseInt(array[1]))).toJSON();
                var to = SW.Core.DateHelper.utcToLocal(toDate).toJSON();

                return "({0}.UnManageFrom = '{1}' AND {0}.UnManageUntil = '{2}')".format([array[0], from, to]);
            }

            if (prop == 'ServerName')
                tableAlias = 'E';
            if (prop == 'EnergyWise')
                tableAlias = 'EW';
            if (prop == 'VlanId') {
                if (secondTableAlias && secondTableAlias == 'I')
                    tableAlias = "I.InterfacePortMaps";
                else
                    tableAlias = "N.NodeVlans";
            }
            if (prop == 'EngineID')
                return "E.EngineID='{0}'".format([MNG.quoteValueForSql(value)]);
            if (prop == 'MachineType' && (value == "null" || value.trim() == ""))
                return "({0}.{1} IS NULL OR {0}.{1}='' OR {0}.{1}='Unknown')".format([tableAlias, prop]);
            if (value == "null" || value.trim() == "") {
                if ($.inArray(type, MNG.NumericTypes) > -1) {
                    return "({0}.{1} IS NULL)".format([tableAlias, prop]);
                } else {
                    return "({0}.{1} IS NULL OR {0}.{1}='')".format([tableAlias, prop]);
                }
            } else {
                return "{0}.{1}='{2}'".format([tableAlias, prop, MNG.quoteValueForSql(value)]);
            }
        }
        else {
            return "1=1";
        }
    }
    else {
        return "1=1";
    }
};

MNG.AddVendorIcons = function () {
    MNG.DoQuery("SELECT DISTINCT N.Vendor, N.VendorIcon FROM Orion.Nodes N", function (result) {
        $(result.Rows).each(function () {
            var processingLink = $(".NodeGroupItems a[value='" + this.Vendor + "']");
            if (processingLink != null &&
            processingLink.length > 0 &&
            processingLink[0].firstChild != null &&
            processingLink[0].firstChild.nodeName != "IMG")
                $('<img src="/NetPerfMon/images/Vendors/' + this.VendorIcon + '" />').error(function () {
                    this.src = "/NetPerfMon/images/Vendors/Unknown.gif";
                    return true;
                }).prependTo(".NodeGroupItems a[value='" + this.Vendor + "']").after(" ");
        });
    });
};

MNG.MakeNodeLink = function(node) {
    return '<a href="/Orion/View.aspx?NetObject=N:{NodeID}" EngineID="{EngineID}" IP="{IPAddress}" NodeHostname="{Hostname}" Community="{GUID}"><img NetObject=N:{NodeID} class="StatusIcon" src="/Orion/images/StatusIcons/Small-{GroupStatus}" alt="{StatusDescription}"/><span>{Caption}</span></a>'.format(ObjectStringPropertiesToSecureString(node));
};

MNG.MakeInterfaceOrVolumeLink = function(obj, nodeLink) {
    obj.StatusDescription = obj.StatusLED.replace(".gif", "");
    obj.ParamString = '';
    if (obj.Type == 'I' && nodeLink)
        obj.ParamString = 'IFName="{Name}" IFIndex="{Index}" '.format(obj) +
			'IP="{0}" NodeHostname="{1}" Community="{2}"'.format([nodeLink.attr('IP'), nodeLink.attr('NodeHostname'), nodeLink.attr('Community')]);
    if (nodeLink) {
        obj.EngineID = '{0}'.format([nodeLink.attr('EngineID')]);
    }
    
    return '<a href="/Orion/View.aspx?NetObject={Type}:{ID}" {ParamString} EngineID="{EngineID}"><img NetObject={Type}:{ID} class="StatusIcon" src="{IconPath}{StatusLED}" alt="{StatusDescription}"/><span>{Caption}</span></a>'.format(ObjectStringPropertiesToSecureString(obj));
};

MNG.MakeNodeExpander = function (node) {
    var hidden = ''; 
    if ((node.NumInterfaces || 0) + (node.NumVolumes || 0) == 0)
        hidden = 'style="visibility:hidden;"';
    return "<img class='NodeExpand' src='{0}' nodeid='{1}' {2}/>".format([MNG.expandImg, node.NodeID, hidden]);
};

MNG.MakeNodeCheckbox = function(node, check) {
	return '<input type="checkbox" name="N:'+ node.NodeID + '"' + (check ? 'checked="checked"' : '') + '/>';
};

MNG.MakeCheckbox = function(obj) {
	if (obj.Type == "I" && obj.ObjectSubType == "WMI") return '<input type="checkbox" name="IW:{ID}" />'.format(obj);
    return '<input type="checkbox" name="{Type}:{ID}" />'.format(obj);
};

MNG.MakeRow = function (cells) {
    return $("<tr>" + $.map(cells, function (cell) { return "<td>" + cell + "</td>"; }).join("") + "</tr>");
};

MNG.AddRow = function (tree, cells) {
    return MNG.MakeRow(cells).appendTo(tree);
};

MNG.ExpandNode = function () {
    var that = this;
    var nodeRow = $(this).parents("tr").eq(0);
    var nodeLink = $("a[ip]", nodeRow);
    var parentData = $('input[name*=":"]', nodeRow).data('netobject') // INPUT element for node which contains ":" and its data
    this.src = MNG.collapseImg;
    var nodeId = this.attributes["nodeid"].value;
    var query = "";

    if (MNG.allowInterfaces())
        query = "SELECT 'I' as Type, I.InterfaceID as ID, I.StatusLED, I.Caption, I.Index, I.Name, I.Unmanaged, I.AdminStatus, I.ObjectSubType FROM Orion.NPM.Interfaces (nolock=true) I WHERE I.NodeID=@nodeid UNION " +
        				" (SELECT 'V' as Type, V.VolumeID as ID, V.Icon as StatusLED, V.Caption, 0 AS Index, '' AS Name, 'false' as Unmanaged, '0' as AdminStatus, '' as ObjectSubType FROM Orion.Volumes (nolock=true) V WHERE V.NodeID=@nodeid)";
    else
        query = "SELECT 'V' as Type, V.VolumeID as ID, V.Icon as StatusLED, V.Caption, 0 AS Index, '' AS Name, 'false' as Unmanaged, '0' as AdminStatus, '' as ObjectSubType FROM Orion.Volumes (nolock=true) V WHERE V.NodeID=@nodeid";

    MNG.DoQuery(query.replace(/@nodeid/g, nodeId), function (results) {
        var iconPaths = { I: '/Orion/images/StatusIcons/Small-', V: '/NetPerfMon/images/Volumes/' };
        var ifxRows = [];
        var className = 'childOfN' + nodeId;
        var numCols = $("#NodeTree2 th").length - 1;

        if ($("." + className).length == 0) {
            var i = 0;
            for (i = 0; i < results.Rows.length; i++) {
                var row = results.Rows[i];
                row.IconPath = iconPaths[row.Type];
                row.ParentUnmanaged = parentData.Unmanaged;
                var node = $("<tr class='" + className + "'><td>&nbsp;</td><td class='ifxname' colspan='" + numCols + "'>" + MNG.MakeCheckbox(row) + MNG.MakeInterfaceOrVolumeLink(row, nodeLink) + "</td></tr>");
                node.find(":checkbox").data('netobject', row);
                ifxRows.push(node[0]);

                if (i >= 1000) {
                    var warnStrip = $(String.format("<tr><td>&nbsp;</td><td class='ifxname' colspan='{0}'><a id='warnStrip' href='#' title='link'>{1}</a></td></tr>", numCols, String.format("@{R=Core.Strings;K=WEBJS_TM0_117;E=js}", results.Rows.length)));
                    $("#warnStrip", warnStrip).attr("title", "@{R=Core.Strings;K=WEBJS_TM0_108;E=js}");
                    ifxRows.push(warnStrip[0]);
                    break;
                }
            }
            $(ifxRows).insertAfter(nodeRow);
            MNG.stripeTree();
        }
        else {
            $("." + className).show();
            MNG.stripeTree();
        }

        $("#NodeTree2 tr td :checkbox").click(function () {
            var ids = MNG.GetSelectedNodeIDs();
        });
        $(that).unbind().click(function () {
            $("." + className).hide();
            MNG.stripeTree();
            $(this).unbind().click(MNG.ExpandNode);
            this.src = MNG.expandImg;
        });
    });
};

MNG.ClearTable = function() {
	$("#NodeTree2>tbody").empty();
	MNG.EnableDisableMenu();	
};

MNG.FormatCell = function(val) {
    //format value to default culture patterns 
    var str = val == null ? '' : (Date.isInstanceOfType(val)) ? val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) +
            ' ' + val.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.LongTimePattern) :
            (Number.isInstanceOfType(val)) ? String(val).replace(".", Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator) : String(val);
    return $.trim(str) || '&nbsp;';
};

MNG.FormatOutage = function (from, to, noneString) {
    if (SW.Core.DateHelper.localToUtc(new Date()) >= to) {
        return noneString;
    }

    if (from > to) {
        return noneString;
    }

    var fromLocal = SW.Core.DateHelper.utcToLocal(from);
    var toLocal = SW.Core.DateHelper.utcToLocal(to);

    var str = fromLocal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' '
                + fromLocal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern) + ' - ' +
                toLocal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortDatePattern) + ' '
                + toLocal.localeFormat(Sys.CultureInfo.CurrentCulture.dateTimeFormat.ShortTimePattern);

    return $.trim(str) || '&nbsp;';
};

MNG.FormatNodeCell = function(node) {
    return '<a href="/Orion/View.aspx?NetObject=N:{NodeID}" IP="{IPAddress}" NodeHostname="{Hostname}" Community="{GUID}">{NodeCaption}</a>'.format(ObjectStringPropertiesToSecureString(node));
};

MNG.Nodes.Load = function (getWhereFragment, onComplete) {
    var canceled = false;
    MNG.CancelLoading = function () { canceled = true; }
    var loadSome = function (nodesLoaded) {
        //$("#loadingStatus").show();
        var fields = ["Caption", "NodeID", "Status", "GroupStatus", "StatusDescription", "IPAddress", "DNS", "SysName",
			"WebCommunityString.GUID", "Unmanaged", "ObjectSubType", "EngineID", "ServerName", "IPAddressType", "ObjectSubType"]
            .concat($.map(MNG.Nodes.selectedColumns, function (c) { return c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp; })).unique();

        if (MNG.Sort.prop == "UnManageFrom") {
            var index = fields.indexOf(MNG.Sort.prop);

            if (index != -1) {
                fields.splice(index, 1);
            }
        }

        var addTableAliasPrefix = function (clauses, prefix) { return $.map(clauses, function (s) { return s == "ServerName" ? "E" + "." + s : prefix + "." + s; }); };

        var prop = Utils.SelectedProperty();

        var engineJoin = " Inner Join Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID ";

        var energyWiseJoin = (MNG.isEnergyWise()) ? " LEFT JOIN Orion.NPM.EW.Nodes (nolock=true) EW ON (N.NodeID = EW.NodeID) " : "";

        var unmanageJoin = " JOIN " +
                           " ( " +
                           "   SELECT NN.NodeId, NULL AS UnManageFrom, NULL AS UnManageUntil " +
                           "   FROM Orion.Nodes (nolock=true) NN WHERE NN.UnManageUntil < getUtcDate() OR NN.UnManageUntil IS NULL " +
                           "   UNION " +
                           "   ( " +
                           "     SELECT NN.NodeId, NN.UnManageFrom, NN.UnManageUntil FROM Orion.Nodes (nolock=true) NN " +
                           "     WHERE UnManageUntil > getUtcDate() AND UnmanageUntil IS NOT NULL " +
                           "   ) " +
                           " ) NN ON N.NodeId=NN.NodeID ";

        var q = "";
        if (MNG.allowInterfaces())
            q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
			    ", I2.NumInterfaces, V2.NumVolumes, N.UnManageFrom, N.UnManageUntil " +
			    " FROM Orion.Nodes (nolock=true) N " +
			    energyWiseJoin +
			    " LEFT JOIN (SELECT I.NodeID, COUNT(I.InterfaceID) AS NumInterfaces FROM Orion.NPM.Interfaces (nolock=true) I GROUP BY I.NodeID) I2 ON N.NodeID=I2.NodeID" +
			    " LEFT JOIN (SELECT V.NodeID, COUNT(V.VolumeID) AS NumVolumes FROM Orion.Volumes (nolock=true) V GROUP BY V.NodeID) V2 ON N.NodeID=V2.NodeID" +
                unmanageJoin +
                engineJoin +
                " WHERE " + getWhereFragment("N") + 
                " AND E.ServerType <> 'RemoteCollector' " +
                MNG.Sort.getOrderBy("N") +
			    " WITH ROWS " + ((Pager.actualPage - 1) * Pager.pageSize + 1) + " TO " + (Pager.actualPage * Pager.pageSize);
        else
            q = "SELECT " + addTableAliasPrefix(fields, 'N').join(", ") +
			    ", V2.NumVolumes, N.UnManageFrom, N.UnManageUntil " +
			    " FROM Orion.Nodes (nolock=true) N " +
			    " LEFT JOIN (SELECT V.NodeID, COUNT(V.VolumeID) AS NumVolumes FROM Orion.Volumes (nolock=true) V GROUP BY V.NodeID) V2 ON N.NodeID=V2.NodeID" +
                engineJoin +
                " WHERE " + getWhereFragment("N") +
                " AND E.ServerType <> 'RemoteCollector' " +
                MNG.Sort.getOrderBy("N") +
			    " WITH ROWS " + ((Pager.actualPage - 1) * Pager.pageSize + 1) + " TO " + (Pager.actualPage * Pager.pageSize);

        var q_totalcount = "SELECT COUNT(N.NodeID) AS TotalCount" +
			" FROM Orion.Nodes (nolock=true) N " +
			energyWiseJoin +
            engineJoin +
			" WHERE " + getWhereFragment("N") +
            " AND E.ServerType <> 'RemoteCollector' ";

        MNG.DoQuery(q_totalcount, function (results) {
            if (results.Rows.length > 0) {
                Pager.totalCount = results.Rows[0]["TotalCount"];
                Pager.Init();
            }
        });

        MNG.DoQuery(q, function(results) {
            if (canceled) return;
            var check = null;
            if (typeof($("#selectAll").get(0)) != 'undefined')
                check = $("#selectAll").get(0).checked;
            $(results.Rows).each(function() {
                var row = this;
                row.UnManageFrom = MNG.FormatOutage(row.UnManageFrom, row.UnManageUntil, "");
                row.Hostname = ($.trim(row.DNS) || $.trim(row.SysName) || row.IPAddress);
                row.GUID = "GUID{" + row.GUID + "}";
                row.IPAddressType = MNG.GetIPAddressTypeText(row.IPAddressType);
                var objectSubType = row.ObjectSubType;
                row.ObjectSubType = MNG.GetPollingMethodText(row.ObjectSubType);
                MNG.AddRow($("#NodeTree2>tbody"), [MNG.MakeNodeCheckbox(row, check), MNG.MakeNodeLink(row)]
                    .concat($.map(MNG.Nodes.selectedColumns, function(c) { return MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); })))
                    .find(":checkbox").data('netobject', row);
                row.ObjectSubType = objectSubType;
            });
            $("#NodeTree2 .NodeExpand").click(MNG.ExpandNode);
            $("#NodeTree2 tr td :checkbox").click(MNG.GetSelectedNodeCheckBox);
            $("#NodeTree2 tr th :checkbox").click(MNG.GetSelectedAllNodeCheckBox);
            MNG.stripeTree();
            if (typeof (onComplete) == 'function')
                onComplete();
            var reteriveValue = $SW.Env["PageLoadBehav"];
            if (typeof (reteriveValue) != 'undefined') {
                for (var a = 0 ; a < reteriveValue.length; a++) {
                    $('input[name = "N:' + reteriveValue[a] + '"]').prop('checked', true);
                    $('input[name = "N:' + reteriveValue[a] + '"]').closest('tr').css('backgroundColor', '#BFBFBF');
                }
            }
        });
      
      
    };
    loadSome(0);
};

MNG.Interfaces.Load = function (getWhereFragment, onComplete) {
    var canceled = false;
    MNG.CancelLoading = function () { canceled = true; };
    var loadSome = function (objectsLoaded) {
        //$("#loadingStatus").show();        
        //'I' as Type, I.InterfaceID as ID, I.StatusLED, I.Caption, I.Index, I.Name
        var ifxFields = ["InterfaceID AS ID", "StatusLED", "Caption", "Index", "Name", "FullName", "Unmanaged", "AdminStatus"]
			.concat($.map(MNG.Interfaces.selectedColumns, function (c) { return c.sortType == 'Interface' ? (c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp) : null; })).unique();
        var nodeFields = ["EngineID", "NodeID", "IPAddress", "DNS", "SysName", "WebCommunityString.GUID", "IPAddressType", "ObjectSubType"]
			.concat($.map(MNG.Interfaces.selectedColumns, function (c) { return c.sortType == 'Node' ? (c.selectAlias ? (c.sortProp + ' AS ' + c.selectAlias) : c.sortProp) : null; })).unique();

        if (MNG.Sort.prop == "UnManageFrom") {
            var index = ifxFields.indexOf(MNG.Sort.prop);

            if (index != -1) {
                ifxFields.splice(index, 1);
            }
        }

        var addTableAliasPrefix = function (clauses, prefix) { return $.map(clauses, function (s) { return prefix + "." + s; }); };
        var prop = Utils.SelectedProperty();
        var engineJoin = (prop == "EngineID") ? " Inner Join Orion.Engines (nolock=true) E ON N.EngineID=E.EngineID " : "";
        var energyWiseJoin = (MNG.isEnergyWise()) ? " LEFT JOIN Orion.NPM.EW.Nodes (nolock=true) EW ON (I.NodeID = EW.NodeID) " : "";

        var unmanageJoin = " JOIN " +
                           " ( " +
                           "   SELECT II.InterfaceId, NULL AS UnManageFrom, NULL AS UnManageUntil " +
                           "   FROM Orion.NPM.Interfaces (nolock=true) II WHERE II.UnManageUntil < getUtcDate() OR II.UnManageUntil IS NULL " +
                           "   UNION " +
                           "   ( " +
                           "     SELECT II.InterfaceId, II.UnManageFrom, II.UnManageUntil FROM Orion.NPM.Interfaces (nolock=true) II " +
                           "     WHERE UnManageUntil > getUtcDate() AND UnmanageUntil IS NOT NULL " +
                           "   ) " +
                           " ) II ON I.InterfaceId = II.InterfaceId ";

        var q = "\
	SELECT {ifxFields}, {nodeFields}, II.UnManageFrom, II.UnManageUntil \
	FROM Orion.NPM.Interfaces (nolock=true) I \
	INNER JOIN Orion.Nodes (nolock=true) N ON I.NodeID=N.NodeID \
	{energyWiseJoinFormat} \
    {engineJoinFormat} \
    {unmanageJoin} \
	WHERE {whereFragment} {orderBy} \
	WITH ROWS {startRow} TO {endRow}".format({
	    ifxFields: addTableAliasPrefix(ifxFields, 'I').join(", "),
	    nodeFields: addTableAliasPrefix(nodeFields, 'N').join(", "),
	    whereFragment: getWhereFragment("N", "I"),
	    orderBy: MNG.Sort.getOrderBy("N", "I"),
	    startRow: (Pager.actualPage - 1) * Pager.pageSize + 1,
	    endRow: Pager.actualPage * Pager.pageSize,
	    engineJoinFormat: engineJoin,
	    energyWiseJoinFormat: energyWiseJoin,
	    unmanageJoin: unmanageJoin
	});

        var q_totalcount = "SELECT COUNT(I.InterfaceID) AS TotalCount" +
            " FROM Orion.NPM.Interfaces (nolock=true) I " +
            " INNER JOIN Orion.Nodes (nolock=true) N ON I.NodeID=N.NodeID" +
            energyWiseJoin +
            engineJoin +
			" WHERE " + getWhereFragment("N", "I");

        MNG.DoQuery(q_totalcount, function (results) {
            if (results.Rows.length > 0) {
                Pager.totalCount = results.Rows[0]["TotalCount"];
                Pager.Init();
            }
        });

        MNG.DoQuery(q, function (results) {
            if (canceled) return;
            var check = $("#selectAll").get(0).checked;
            $(results.Rows).each(function () {
                var row = this;
                row.UnManageFrom = MNG.FormatOutage(row.UnManageFrom, row.UnManageUntil, "");
                row.IconPath = '/Orion/images/StatusIcons/Small-';
                row.Type = 'I';
                row.Hostname = ($.trim(row.DNS) || $.trim(row.SysName) || row.IPAddress);
                row.GUID = "GUID{" + row.GUID + "}";
                row.IPAddressType = MNG.GetIPAddressTypeText(row.IPAddressType);
                row.ObjectSubType = MNG.GetPollingMethodText(row.ObjectSubType);
                MNG.AddRow($("#NodeTree2>tbody"), [MNG.MakeCheckbox(row), MNG.MakeInterfaceOrVolumeLink(row)]
					.concat($.map(MNG.Interfaces.selectedColumns, function (c) { return c.header == "Node" ? MNG.FormatNodeCell(row) : MNG.FormatCell(row[MNG.removeBrackets(c.selectAlias || c.sortProp)]); })))
					.find(":checkbox").data('netobject', row);
            });
            $("#NodeTree2 tr td :checkbox").click(MNG.EnableDisableMenu);
          
            MNG.stripeTree();
            if (typeof (onComplete) == 'function') {
                onComplete();
            }
            //$("#testoutput").html(results.Rows.length + " " + MNG.nodePageSize);

            //            if (results.Rows.length >= MNG.nodePageSize) {
            //                //setTimeout(function() {
            //                //if (canceled) return;
            //                loadSome(objectsLoaded + results.Rows.length);
            //                //}, 3000);
            //                //$("#loadingStatus span").text("Loaded " + (nodesLoaded + results.Rows.length) + " nodes, continuing...");
            //            } else {
            //                //$("#loadingStatus").hide();
            //                if (typeof (onComplete) == 'function')
            //                    onComplete();
            //            }
        });
    };
    loadSome(0);
};

MNG.Nodes.WithAllIDs = function (callback) {
    var query = (MNG.isEnergyWise()) ? SWQLQueries.SelectNpmNodeIds : SWQLQueries.SelectNodeIds;
    MNG.DoQuery(query + MNG.GetWhere("N"), function (results) {
        var ids = [];
        $(results.Rows).each(function () {
            ids.push("N:" + this.NodeID);
        });
        callback(ids);
    });
};

MNG.Nodes.WithAllIDsWithEngineIds = function (callback) {
    var query = (MNG.isEnergyWise()) ? SWQLQueries.SelectNpmNodeAndEngineIds : SWQLQueries.SelectNodeAndEngineIds;
    MNG.DoQuery(query + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() {
            ids.push("N:" + this.NodeID + ":" + this.EngineID);
        });
        callback(ids);
    });
};

MNG.Nodes.WithAllIDsWithoutPrefix = function (callback) {
    var query = (MNG.isEnergyWise()) ? SWQLQueries.SelectNpmNodeIds : SWQLQueries.SelectNodeIds;
    MNG.DoQuery(query + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push(this.NodeID); });
        callback(ids);
    });
};

MNG.Nodes.WithManagedObjectCount = function (callback) {
    var query = (MNG.isEnergyWise()) ? SWQLQueries.SelectNpmManageNodeCount : SWQLQueries.SelectManageNodeCount;
    MNG.DoQuery(query + MNG.GetWhere("N"), function(results) {
        callback(results.Rows[0].Cnt, true);
    });
};

MNG.Nodes.WithUnmanagedObjectCount = function (callback) {
    var query = (MNG.isEnergyWise()) ? SWQLQueries.SelectNpmUnmanageNodeCount : SWQLQueries.SelectUnmanageNodeCount;
    MNG.DoQuery(query + MNG.GetWhere("N"), function (results) {
        callback(results.Rows[0].Cnt, false);
    });
};

MNG.Nodes.WithAllFormatedSNMPIDs = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectSNMPNodesIds + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push(this.NodeID); });
        callback(ids.join(","), "N");
    });
};

MNG.Nodes.StoreAllFormatedSNMPIDs = function () {
    NodeManagement.StoreAllIDs(SWQLQueries.SelectSNMPNodesIds + MNG.GetWhere("N"), "Nodes", function () {
        window.location = MNG.customPollerUrl.format(["Node", $('#ReturnToUrl').val()]);
    }, function(error) {alert("@{R=Core.Strings;K=WEBJS_TM0_105;E=js}\n"+error.get_message());});
};

MNG.Interfaces.WithAllIDs = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectInterfaceIds + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push("I:" + this.InterfaceID); });
        callback(ids);
    });
};

MNG.Interfaces.WithAllIDsWithEngineIds = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectInterfaceAndEngineIds + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push("I:" + this.InterfaceID + ":" + this.EngineID); });
        callback(ids);
    });
};

MNG.Interfaces.WithAllIDsWithoutPrefix = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectInterfaceIds + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push(this.InterfaceID); });
        callback(ids);
    });
};

MNG.Interfaces.WithAllFormatedSNMPIDs = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectInterfaceIds + MNG.GetWhere("N"), function(results) {
        var ids = [];
        $(results.Rows).each(function() { ids.push(this.InterfaceID); });
        callback(ids.join(","), "I");
    });
};

MNG.Interfaces.StoreAllFormatedSNMPIDs = function () {
    NodeManagement.StoreAllIDs(SWQLQueries.SelectInterfaceIds + MNG.GetWhere("N"), "Interfaces", function () {
        window.location = MNG.customPollerUrl.format(["Interface", $('#ReturnToUrl').val()]);
    }, function (error) { alert("@{R=Core.Strings;K=WEBJS_TM0_106;E=js}\n" + error.get_message()); });
};

MNG.Interfaces.WithAllIDsAndNames = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectInterfaceIdsAndNames + MNG.GetWhere("N"), function(results) {
        var ids = { ID: [], Name: [] };
        $(results.Rows).each(function() { ids.ID.push(this.InterfaceID); ids.Name.push(this.Caption); });
        callback(ids);
    });
};

MNG.Interfaces.WithManagedObjectCount = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectManageInterfaceCount + MNG.GetWhere("N"), function(results) {
        callback(results.Rows[0].Cnt, true);
    });
};

MNG.Interfaces.WithUnmanagedObjectCount = function(callback) {
    MNG.DoQuery(SWQLQueries.SelectUnmanageInterfaceCount + MNG.GetWhere("N"), function(results) {
        callback(results.Rows[0].Cnt, false);
    });
};

MNG.Nodes.SelectedPrefix = function() { return "N"; };
MNG.Interfaces.SelectedPrefix = function() { return "I"; };

MNG.GetSelectedObjectIDs = function () {
    var ids =
    {
        N: [],
        I: [],
        IW: [],
        V: [],
        ICMP: [],
        WMI: [],
        SNMP: [],
        Unmanaged: [],
        Managed: [],
        NetObjectIDs: [],
        EngineNetObjectIDs: [],
        AdminShutdown: [],
        AdminNotShutdown: [],
        External: [],
        NonExternal: [],
        UnmanagedUnderUnmanaged: [],
        PlannedOutage: [],
        NoPlannedOutage: [],
        ObjectSubType: []
    };

    $('[name*=":"]:checked').each(function () {
        ids.NetObjectIDs.push(this.name);
        var parts = this.name.split(":");
        var obj = $(this).data('netobject');
        ids.EngineNetObjectIDs.push(this.name + ':' + obj.EngineID);
        ids[parts[0]].push(parts[1]);
        if (parts[0] == "N") {
            ids[obj.ObjectSubType].push(parts[1]);
            ids["ObjectSubType"].push(obj.ObjectSubType);
            ids[obj.Unmanaged ? "Unmanaged" : "Managed"].push(parts[1]);
            ids[obj.Status == "11" ? "External" : "NonExternal"].push(parts[1]);
        }
        // Only SNMP interfaces can be "Shutdown" or "Enable"
        if (parts[0] == "I") {
            (obj.AdminStatus == 4 ? ids.AdminShutdown : ids.AdminNotShutdown).push(parts[1]);
        }
        if (parts[0] == "I" || parts[0] == "IW") {
            ids[obj.Unmanaged ? "Unmanaged" : "Managed"].push(parts[1]);
            ids["ObjectSubType"].push("");
        }
        if (obj.Unmanaged == true && obj.hasOwnProperty("ParentUnmanaged") && obj.ParentUnmanaged == true) {
            ids.UnmanagedUnderUnmanaged.push(this.name);
        }

        if (typeof (obj.UnManageFrom) == 'undefined' || obj.UnManageFrom == "") {
            ids["NoPlannedOutage"].push(parts[1]);
        }
        else {
            ids["PlannedOutage"].push(parts[1]);
        }
    });
    return ids;
};

MNG.GetSelectedNodeIDs = function() {
	return MNG.GetSelectedObjectIDs().N;
};
MNG.GetSelectedNodeSubTypes = function () {
    return MNG.GetSelectedObjectIDs().ObjectSubType;
};

MNG.GetSelectedInterfaceIDs = function () {
    var selected = MNG.GetSelectedObjectIDs();
    var ids = [];
    $.each(selected.I, function () { ids.push(this); });
    $.each(selected.IW, function () { ids.push(this); });
    return ids;
};
MNG.GetSelectedNodeCheckBox = function () {
        var selid = $(this).attr('name');
        selid = selid.replace("N:", "");
        var ids = [];
        if (typeof ($SW.Env["PageLoadBehav"]) != "undefined") {
            ids = $SW.Env["PageLoadBehav"];
        }
        if ($(this).is(':checked')) {
            ids.push(selid);
            $(this).closest('tr').css('backgroundColor', '#BFBFBF');
        }
        else {
            if ($(this).closest('tr').is(':nth-child(odd)'))
                $(this).closest('tr').css('backgroundColor', '#ECF6FB');
            else
                $(this).closest('tr').css('backgroundColor', 'white');
            
            for (var i =0 ; i < ids.length; i++) {
                if (ids[i] === selid) ids.splice(i, 1);
            }
        }
        var result = [];
        $.each(ids, function (i, e) {
            if ($.inArray(e, result) == -1) result.push(e);
        });
        $SW.Env["PageLoadBehav"] = ids;
};
MNG.GetSelectedAllNodeCheckBox = function () {
    var ids = [];
    if (typeof ($SW.Env["PageLoadBehav"]) != "undefined") {
        ids = $SW.Env["PageLoadBehav"];
    }
    var selectAllNodes = [];
    var deSelectAllNodes = [];
    var isChecked = true;
    
    $("#NodeTree2 tr td :checkbox").each(function () {
       
        if ($(this).is(':checked'))
        {
            isChecked = true;
            selectAllNodes.push($(this).attr('name').replace("N:", ""));
            
            $(this).closest('tr').css('backgroundColor', '#BFBFBF');
        }
        else
        {
            isChecked = false;
            deSelectAllNodes.push($(this).attr('name').replace("N:", ""));
            
            if ($(this).closest('tr').is(':nth-child(odd)'))
                $(this).closest('tr').css('backgroundColor', '#ECF6FB');
            else
                $(this).closest('tr').css('backgroundColor', 'white');
        }
    });
    
    if (isChecked == true) {
        for (var i = 0 ; i < selectAllNodes.length; i++)
        {
            if ($.inArray(selectAllNodes[i], ids) < 0)
            {
                {
                    ids.push(selectAllNodes[i]);
                }
            }
        }
    }
    else {
        deSelectAllNodes.sort();
        ids.sort();
        var len = ids.length;

        for (var j = 0; j < len; j++) {
            var index = $.inArray(deSelectAllNodes[j], ids);
            if (index != -1) {
                ids.splice(index, 1);
            }
        }
    }
   
    ids = $.unique(ids);
    $SW.Env["PageLoadBehav"] = ids;
};
MNG.EnableDisableMenu = function () {
    if (MNG.suspendEnableDisableMenu) return;
    var ids = MNG.GetSelectedObjectIDs();
    var NetObjectIDs = ids.NetObjectIDs.length, N = ids.N.length, I = ids.I.length, IW = ids.IW.length, V = ids.V.length, SNMP = ids.SNMP.length, WMI = ids.WMI.length, ICMP = ids.ICMP.length,
		Managed = ids.Managed.length, Unmanaged = ids.Unmanaged.length, External = ids.External.length,
		NonExternal = ids.NonExternal.length, AdminShutdown = ids.AdminShutdown.length, AdminNotShutdown = ids.AdminNotShutdown.length;

    var disableRemanage = ids.UnmanagedUnderUnmanaged.length > 0;

    var disable = function (id) { $("#" + id + "Link").parent().addClass("Disabled"); };
    var disablePluginItems = function (NetObjectIDs) {
        $(".pluginActionListItem").each(function() {

            var jsEnabledCallback = $(this).attr("data-enabled");
            if (jsEnabledCallback != '' && jsEnabledCallback != null) {
                var fn = window[jsEnabledCallback];
                if (typeof fn === 'function') {
                    if (fn(MNG) == false) {
                        $(this).parent().addClass("Disabled");
                        return;
                    } else {
                        return;
                    }
                } else {
                    alert('Callback for disable action list item is not a function');
                }
            }
            else if ($(this).hasClass('alwaysEnabled')) {
                 return;
            }

            if (NetObjectIDs == 0) {
                $(this).parent().addClass("Disabled");
            }
        });
    }

    $(".NodeManagementMenu li").removeClass("Disabled");
    if ((V > 0 && I > 0) || (N > 0 && I > 0) || (V > 0 && IW > 0) || (N > 0 && IW > 0) || (N > 0 && V > 0) || (I == 0 && IW == 0 && N == 0 && V == 0)) { disable('edit'); disable('exportCP'); }
    if ((V > 0 && I > 0) || (N > 0 && I > 0) || (V > 0 && IW > 0) || (N > 0 && IW > 0) || (N > 0 && V > 0)) { disable('importCP'); }
    if (I == 0 || IW > 0 || N > 0) disable('powerLevel');
    if (N == 0 || I > 0 || IW > 0 || V > 0) disable("changePollEngine");
    if ((SNMP != 1 && WMI != 1) || I > 0 || IW > 0 || V > 0 || External > 0) disable('listResources');
    if (SNMP > 0 && WMI > 0) disable('listResources');
    if (Managed == 0 || V > 0) { disable('unmanage'); }
    if (Unmanaged == 0 || V > 0 || disableRemanage) { disable('remanage'); }
    if (V > 0 || WMI > 0 || IW > 0 || (SNMP > 0 && I > 0) || (SNMP == 0 && I == 0)) disable('pollers');
    if (External > 0) {
        if (External == N) {
            if (!MNG.allSelected) {
                disable('pollNow'); disable('rediscover'); disable('unmanage'); disable('remanage');
            }
            else {
                MNG.DoQuery(SWQLQueries.SelectNonExternalNodeCount + MNG.GetWhere("N"), function (results) {
                    if (results.Rows[0].Cnt == 0) {
                        disable('pollNow'); disable('rediscover'); disable('unmanage'); disable('remanage');
                    }
                });
            }
        }
    }
    if (N == 0 && I == 0 && IW == 0 && V == 0) { disable('delete'); disable('pollNow'); disable('rediscover'); disable('energyWise'); disable('powerLevel'); }
    if (AdminNotShutdown == 0 || AdminShutdown > 0 || N > 0 || V > 0 || IW > 0) disable('shutdown');
    if (AdminShutdown == 0 || AdminNotShutdown > 0 || N > 0 || V > 0 || IW > 0) disable('enable');
    if ($("#isDemoMode").length != 0) {
        disable('edit'); disable('powerLevel'); disable('listResources'); disable('unmanage'); disable('remanage'); disable('pollers'); disable('importCP');
        disable('pollNow'); disable("rediscover"); disable('shutdown'); disable('enable'); disable('energyWise'); disable('delete'); disable('add'); disable('topology');
        if ($("#addLink")[0]) $("#addLink")[0].href = "#";
    }
    var objectType = MNG.Prefs.load("ObjectType", "Orion.Nodes").split(".").pop().substring(0, 1);
    if ($('[name*="' + objectType + ':"]:checked').length != $('[name*="' + objectType + ':"]').length) {
        MNG.allSelected = false;
        $("#selectAllPan").empty();
        $("#selectAll").removeAttr("checked");
    }

    if (ids.NoPlannedOutage.length > 0 || ids.PlannedOutage.length == 0) {
        disable('cancelOutages');
    }

    disablePluginItems(NetObjectIDs);

    // all object across pages are selected lets find out if we can do unmanage or remanage
    if (MNG.allSelected) {        
        MNG.NetObjects.WithManagedObjectCount(Operations.EnableDisableManageButton);
        MNG.NetObjects.WithUnmanagedObjectCount(Operations.EnableDisableManageButton);
    }
};

MNG.stripeTree = function() {
	$("#NodeTree2 tbody tr:nth-child(odd)").css('background-color', '#ECF6FB');
	$("#NodeTree2 tbody tr:nth-child(even)").css('background-color', 'white');
};

MNG.quoteValueForSql = function(query) {
	return query.replace(/'/g, "''");
};

MNG.formatIPAddressString = function (ipAddress) {
    var res = ipAddress;
    $.ajax({
        type: 'Post',
        url: '/Orion/Services/NodeManagement.asmx/GetIPAddressString',
        data: "{'ipAddress':" + "'" + ipAddress + "\'}",
        contentType: 'application/json; charset=utf-8',
        timeout: '12000000',
        async: false,
        success: function (result) {
            res = result.d;
        },
        error: function (error) {
        }
    });
    return res;
};

MNG.search = function (query) {
    MNG.CancelLoading();
    $(".SelectedNodeGroupItem").removeClass("SelectedNodeGroupItem");
    MNG.ClearTable();

    var searchIP = MNG.formatIPAddressString(query);

    if ($("#selectAll").length > 0)
        $("#selectAll").get(0).checked = false;
    var fields = { Node: [], Interface: [] };
    $("th[sortProp]").each(function () {
        fields[$(this).attr('sortType')].push($(this).attr('sortProp'));
    });

    // this function is quick workaround how to solve problem with invalid
    // alias for columns from Engine table. It get name of property from table and
    // original alias, if property is from EngineTable then alias E is returned, otherwise
    // original alias is returned
    var changeEngineAlias = function (propName, tableAlias) {
        switch (propName) {
            case "ServerName":
                return "E";
            default:
                return tableAlias;
        }
    }

    var searchClauses = function (fields, tableAlias) {
        var searchClause = function (field) {
            var validTableAlias = changeEngineAlias(field, tableAlias);
            return "{0}.{1} LIKE '%{2}%'".format([validTableAlias, field, (field == "IPAddress") ? MNG.quoteValueForSql(searchIP) : MNG.quoteValueForSql(query)]);
        }
        return $.map(fields, searchClause);
    };

    var quoteForRegExp = function (str) {
        var toQuote = '\\/[](){}?+*|.^$'; // note: backslash must be first
        for (var i = 0; i < toQuote.length; ++i)
            str = str.replace(toQuote.charAt(i), '\\' + toQuote.charAt(i));
        return str;
    };
    MNG.NetObjects.Load(function (nodeAlias, interfaceAlias) {
        var clauses = searchClauses(fields.Node, nodeAlias).concat(searchClauses(fields.Interface, interfaceAlias));
        return "(" + clauses.join(" OR ") + ")";
    }, function () {
        // search term highlighting adapted from http://dossy.org/archives/000338.html        
        var re = new RegExp('(' + quoteForRegExp(query) + '|' + quoteForRegExp(searchIP) + ')', 'ig');
        if ($.trim(query) != '') {
            $("#NodeTree2 tbody *").each(function () {
                if ($(this).children().size() > 0) return;
                var html = $(this).html();
                var newhtml = html.replace(re, '<span class="searchterm">$1</span>');
                if (html) {
                    $(this).html(newhtml);
                }
            });
        }
        if ($("#NodeTree2>tbody>tr").length == 0) {
            var netObjectType = (MNG.NetObjects.typename.toLowerCase() == MNG.Nodes.typename.toLowerCase()) ? "@{R=Core.Strings;K=WEBJS_VB0_134;E=js}" : "@{R=Core.Strings;K=WEBJS_VB0_135;E=js}";
            $("#NodeTree2>tbody").append("<tr><td style='border-right: none;'></td><td></td></tr>").find("td:last").attr('colSpan', $("#NodeTree2 th").length - 1).css("text-align", "center").css("padding", "1em 5em 1em 1em")
				.text("@{R=Core.Strings;K=WEBJS_VB0_136;E=js}".format([netObjectType, query]));
        }
    });
};

MNG.Nodes.selectAll = function () {
    
    var c = this.checked;
    MNG.suspendEnableDisableMenu = true;    
    $('input:checkbox[name^="N:"]').each(function() {
        this.checked = c;
       
    });
    // always uncheck any interfaces and volumes when doing select all on nodes
    $('input:checkbox[name*=":"]').not('[name^="N:"]').each(function() {
        this.checked = false;
    });
    MNG.suspendEnableDisableMenu = false;
    MNG.EnableDisableMenu();

    MNG.DisplaySelectPan(this.checked);
};

MNG.Interfaces.selectAll = function() {
	var c = this.checked;
	MNG.suspendEnableDisableMenu = true;
	$('input:checkbox[name^="I:"]').each(function () {
	    this.checked = c;
	});
	$('input:checkbox[name^="IW:"]').each(function () {
	    this.checked = c;
	});
	MNG.suspendEnableDisableMenu = false;
	MNG.EnableDisableMenu();

	MNG.DisplaySelectPan(this.checked);
};

MNG.showObjectType = function(objectType, skipLoadingObjects) {
    Pager.LoadSettings();
    Pager.forbidReset = true;

    MNG.Prefs.save("ObjectType", objectType);

    //FB19675
    //When license expires
    //we set the NetObjects to Nodes, ignoring what the user might have selected in the
    //now hidden showObjectType selectbox.
    if (MNG.allowInterfaces()) {
        MNG.NetObjects = (objectType == "Orion.Nodes") ? MNG.Nodes : MNG.Interfaces;
    }
    else {
        MNG.NetObjects = MNG.Nodes;
    }

    MNG.NetObjects.selectedColumns = MNG.loadColumns(MNG.NetObjects, MNG.NetObjects.DefaultColumns);
    MNG.NetObjects.createColumnHeaders();
    $("#NodeTree2 thead th:last").click(function() {
        MNG.chooseColumns();
        return false;
    });
    
    //$("<img id='addColumns' alt='@{R=Core.Strings;K=WEBJS_AK0_46;E=js}' src='" + MNG.chooseColumnsImg + "'>").appendTo("#NodeTree2 thead th:last").css({ 'padding-top': '2px' }).click(function() {
    //    MNG.chooseColumns();
    //    return false;
    //});
    
    MNG.Sort.loadPrefs();
    $("#selectAll").click(MNG.NetObjects.selectAll);
    $("th[sortProp]").click(function() {
        MNG.Sort.by($(this).attr('sortType'), $(this).attr('sortProp'));
        MNG.RefreshObjects();
    });
    MNG.LoadGroupByValues();
   
};

MNG.chooseColumns = function () {
    var list = $("#availableColumnList").html('');

    MNG.NetObjects.AvailableColumns.sort(AvailableColumnsComparer);
    var previousCategory = -1;
    $(MNG.NetObjects.AvailableColumns).each(function (i) {
        if (this.headerText == null)
            this.headerText = this.header;
        if (this.category != previousCategory) {
            switch (this.category) {
                case ColumnCategories.NODE_CUSTOM_PROPERTIES:
                    $("<li class='column-category-title'><b>@{R=Core.Strings;K=WEBJS_IB0_50;E=js}<b></li>").appendTo(list);
                    break;
                case ColumnCategories.INTERFACE_CUSTOM_PROPERTIES:
                    $("<li class='column-category-title'><b>@{R=Core.Strings;K=WEBJS_IB0_49;E=js}<b></li>").appendTo(list);
                    break;
                default:
                    $("<li class='column-category-title'><b>@{R=Core.Strings;K=WEBJS_IB0_48;E=js}<b></li>").appendTo(list);
                    break;
            }
            previousCategory = this.category;
        }
        $("<li><input type='checkbox' id='{id}' header='{header}' /> <label for='{id}'>{headerText}</label></li>".format({ id: "columnChooser" + i, header: this.header, headerText: this.headerText })).appendTo(list);
    });
    var check = function (obj) {
        if ($.browser.msie && $.browser.version == "6.0")
            obj.defaultChecked = true;
        else
            obj.checked = true;
    };
    $(MNG.NetObjects.selectedColumns).each(function () {
        $(":checkbox[header='" + this.header + "']").each(function () { check(this); });
    });
    $(".columnChooserOK").one('click', function () {
        var headers = [];
        $("#availableColumnList :checked[header]").each(function () {
            headers.push($(this).attr('header'));
        });
        MNG.Prefs.save(MNG.NetObjects.typename + "Columns", headers.join(','));
        MNG.showObjectType($("#showObjectType").length ? $("#showObjectType").val() : "Nodes");

        // close the dialog
        $("#columnChooserDialog").dialog('close');

    });
    $(".columnChooserCancel").one('click', function () {
        $("#columnChooserDialog").dialog('close');
    });
    var contents = $("#columnChooserDialogContents");
    var height = contents.appendTo("#content").height(); // need to put columnChooserDialogContents under the body to correctly calculate its height
    contents.appendTo("#columnChooserDialog");

    $("#columnChooserDialog").show().dialog({
        width: 600, height: height + 60, modal: true, overlay: { "background-color": "black", opacity: 0.4 }, bgiframe: true
    });
};

MNG.getSearchTerm = function() {
    var searchTerm = $("#search").val();

    if (MNG.WatermarkTextbox === searchTerm) {
        searchTerm = '';
    }
    return $.trim(searchTerm);
};

MNG.startSearch = function () {
    var query = MNG.getSearchTerm();
    Pager.Reset();
    Pager.isInSearchMode = true;
    MNG.RefreshObjects = function() {
        MNG.Sort.setArrow();
        MNG.search(query);
    };
    MNG.RefreshObjects();
};

MNG.loadColumns = function (type, defaults) {
    var selectedHeaders = MNG.Prefs.load(type.typename + "Columns", defaults).split(',');
    return $.grep(type.AvailableColumns, function (col) {
        // NPM10.6 Used to store selected CP columns without CustomProperties. prefix, 10.7 add this prefix but only for Nodes
        // so we need to handle this when compare columns 
        for (var j = 0; j < selectedHeaders.length; j++) {
            if (selectedHeaders[j].replace("CustomProperties.", "") == col.header.replace("CustomProperties.", ""))
                return true;
        }
        return false; 
    });
};

MNG.ReportError = function (error, lastError) {
    if (error == null || MNG.stopProcessing) return;
    var msg;
    var statusCode;
    if (typeof (error.get_message) == 'function') { //this is ASP.NET type error
        msg = error.get_message();
        statusCode = error.get_statusCode();
    }
    else { // jQuery type error
        msg = error.statusText;
        statusCode = error.status;
    }
    if (statusCode == 401 || statusCode == 403) {
        // login cookie expired. reload the page so we get bounced to the login page.
        MNG.stopProcessing = true;
        alert('@{R=Core.Strings;K=WEBJS_AK0_66;E=js}');
        window.location.reload();
    }
    if (statusCode == 500) {
        // Internal Server Error
        msg = "@{R=Core.Strings;K=WEBJS_YK0_1;E=js}";
    }
    if (msg == "Unknown") {
        msg = "@{R=Core.Strings;K=WEBJS_PS0_17;E=js}";
    }
	
    if (lastError) {
        lastError.text("@{R=Core.Strings;K=WEBJS_TM0_107;E=js} " + msg).show();
    }
    else {
        $('#lastErrorMessage').text("@{R=Core.Strings;K=WEBJS_TM0_109;E=js} " + msg).show();
    }
};

MNG.UpdateStatusIconForInterface = function(interfaceId) {
	MNG.DoQuery("SELECT I.StatusLED, I.AdminStatus FROM Orion.NPM.Interfaces (nolock=true) I WHERE I.InterfaceID=" + interfaceId, function (results) {
		$("img.StatusIcon[NetObject='I:" + interfaceId + "']").attr('src', '/Orion/images/StatusIcons/Small-' + results.Rows[0].StatusLED);
		$.each($("input:checkbox[name='I:" + interfaceId + "']"), function () {
			$(this).data('netobject').AdminStatus = results.Rows[0].AdminStatus;
		});
		MNG.EnableDisableMenu();
	});
};

/* NOT USED */
MNG.UpdateStatusIconForNode = function(nodeId) {
    MNG.DoQuery("SELECT N.GroupStatus, N.Unmanaged, N.StatusDescription FROM Orion.Nodes (nolock=true) N WHERE N.NodeId=" + nodeId, function(results) {
        $("img.StatusIcon[NetObject='N:" + nodeId + "']").attr('src', '/Orion/images/StatusIcons/Small-' + results.Rows[0].GroupStatus);
        $.each($("input:checkbox[name='N:" + nodeId + "']"), function() {
            $(this).data('netobject').Unmanaged = results.Rows[0].Unmanaged;
            for (var n = 0; n < MNG.Nodes.selectedColumns.length; ++n) {
                if (MNG.Nodes.selectedColumns[n].sortProp == "StatusDescription") {
                    $(this).parent().parent().find("td:eq(" + (n + 2) + ")").text(results.Rows[0].StatusDescription);
                    break;
                }
            }
        });
        MNG.EnableDisableMenu();
    });
};

GetAllNodeIds = function () {
   
    var query = SWQLQueries.SelectNodeIds;
    MNG.DoQuery(query + MNG.GetWhere("N"), function (results) {
        var ids = [];

        if (typeof ($SW.Env["PageLoadBehav"]) != "undefined") {
            ids = $SW.Env["PageLoadBehav"];
        $(results.Rows).each(function () {
            ids.push(""+ this.NodeID + "");
        });
        ids = GetUnique(ids);
        $SW.Env["PageLoadBehav"] = ids;
        }
    });
};

function GetUnique(inputArray) {
    var outputArray = [];
    for (var i = 0; i < inputArray.length; i++) {
        if ((jQuery.inArray(inputArray[i], outputArray)) == -1) {
            outputArray.push(inputArray[i]);
        }
    }
    return outputArray;
}
MNG.DisplaySelectPan = function (checked) {
    $("#selectAllPan").empty();
    if (checked && (Pager.totalCount > Pager.pageSize)) {
        var objectType = MNG.Prefs.load("ObjectType", "Orion.Nodes");
        objectType = MNG.GetObjectTypeText(objectType);
        var countOfObjectsOnPage = Pager.pageSize;
        if ((Pager.actualPage == Pager.pageCount) && ((Pager.totalCount % Pager.pageSize) != 0)) {
            countOfObjectsOnPage = Pager.totalCount % Pager.pageSize;
        }
        $("#selectAllPan").append(String.format("<span>@{R=Core.Strings;K=WEBJS_TM0_114;E=js} </span>", objectType, countOfObjectsOnPage) +
                                  String.format("<a href='#'>@{R=Core.Strings;K=WEBJS_TM0_115;E=js}</a>", objectType, Pager.totalCount)).addClass("select-whole-page");
        $("#selectAllPan a").click(function () {
            var objectType = MNG.Prefs.load("ObjectType", "Orion.Nodes");
            objectType = MNG.GetObjectTypeText(objectType);
            $("#selectAllPan").empty();
            $("#selectAllPan").append(String.format("<span>@{R=Core.Strings;K=WEBJS_TM0_116;E=js}</span>", objectType)).addClass("select-all-objects");
            GetAllNodeIds(); 
            MNG.allSelected = true;
            MNG.EnableDisableMenu();
        })
    }
    else {
        MNG.allSelected = false;
    }
};

MNG.SetMenuWidth = function () {
    // this fix is basically for IE only. Other browsers respect nowrap in the css
    // for PDF, we have "width: auto !important" to override this, since it does respect nowrap and doesn't understand this constant width setting

    var width = 0;
    $(".NodeManagementMenu > li:not(:hidden)").each(function () { width += $(this).outerWidth(); });
    $(".NodeManagementMenuWrapper").width(width + 11);
};

AvailableColumnsComparer = function (a, b) {
    if (a.category < b.category) return -1;
    if (a.category > b.category) return 1;
    return a.headerText.localeCompare(b.headerText);
};

$(function () {

    if ($('#NodeInheritedProperties').val()) {
        var properties = $('#NodeInheritedProperties').val();
        var parsedProperties = JSON.parse(properties);

        $(parsedProperties).each(function (index, value) {
            MNG.Nodes.AvailableColumns.push({
                sortType: "Node",
                sortProp: value.NavigationName,
                selectAlias: "[n_" + value.NavigationName.replace('.', '') + "]",
                header: value.NavigationName,
                headerText: value.DisplayName,
                category: ColumnCategories.SYSTEM_PROPERTIES
            });

            MNG.Interfaces.AvailableColumns.push({
                sortType: "Node",
                sortProp: value.NavigationName,
                selectAlias: "[n_" + value.NavigationName.replace('.', '') + "]",
                header: "Node " + value.NavigationName,
                headerText: String.format("@{R=Core.Strings;K=WEBJS_TM0_119;E=js}", value.DisplayName),
                category: ColumnCategories.SYSTEM_PROPERTIES
            });
        });
    }

    if ($('#InterfaceCustomProperties').val()) {
        $($('#InterfaceCustomProperties').val().split(',')).each(function () {
            MNG.Interfaces.AvailableColumns.push({
                sortType: "Interface",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[i_" + this + "]",
                header: "CustomProperties." + this,
                headerText: this,
                category: ColumnCategories.INTERFACE_CUSTOM_PROPERTIES
            });
        });
    }

    if ($('#NodeCustomProperties').val()) {
        $($('#NodeCustomProperties').val().split(',')).each(function () {
            MNG.Nodes.AvailableColumns.push({
                sortType: "Node",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[n_" + this + "]",
                header: "CustomProperties." + this,
                headerText: this,
                category: ColumnCategories.NODE_CUSTOM_PROPERTIES
            });
            MNG.Interfaces.AvailableColumns.push({
                sortType: "Node",
                sortProp: "CustomProperties.[" + this + "]",
                selectAlias: "[n_" + this + "]",
                header: "Node CustomProperties." + this,
                headerText: this,
                category: ColumnCategories.NODE_CUSTOM_PROPERTIES
            });
        });
    }

    var objectType = MNG.Prefs.load("ObjectType", "Orion.Nodes");
    $("#groupByProperty").val(MNG.Prefs.load("GroupBy", "ObjectSubType")).change(MNG.LoadGroupByValues);
    $("#showObjectType").val(objectType).change(function () { MNG.showObjectType($(this).val()); });
    MNG.showObjectType(objectType, true);

    // causes double data loading during page load
    //$("#groupByProperty").change();

    $("form").submit(function () { return false; });
    MNG.WatermarkTextbox = 'Search...';
    
    function searchSnmpNodes() {
        var ENTER_KEY_CODE = 13;

        $("#search").keyup(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);

            // Enter keycode
            if (code == ENTER_KEY_CODE)
                MNG.startSearch();
        });
    }
    
    function createWatermarkTextBox() {
        var watermark = new SW.Core.Widgets.WatermarkTextbox($("#search"), MNG.WatermarkTextbox);
    }
    
    createWatermarkTextBox();
    
    searchSnmpNodes();
    
    $(".searchButton").click(MNG.startSearch);

    $(".NodeManagementMenu li").hover(function () {
        $(this).addClass("over");
    }, function () {
        $(this).removeClass("over");
    });

    if ($("#isDemoMode").length == 0) {
        $("#editLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithEngineIds(Operations.EditProperties);
                }
                else {
                    var ids = MNG.GetSelectedObjectIDs();

                    if (ids.N.length) {
                        $('body').append(Operations.PreparePostForm("Node", ids.EngineNetObjectIDs, $('#ReturnToUrl').val()));
                    }
                    else if (ids.I.length || ids.IW.length) {
                        $('body').append(Operations.PreparePostForm("Interface", ids.EngineNetObjectIDs, $('#ReturnToUrl').val()));
                    }
                    else if (ids.V.length) {
                        $('body').append(Operations.PreparePostForm("Volume", ids.EngineNetObjectIDs, $('#ReturnToUrl').val()));
                    }

                    $('#selectedNetObjects').submit();
                }
            }
            return false;
        });
        $("#listResourcesLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                window.location = MNG.listResourcesUrl.format([MNG.GetSelectedNodeIDs()[0], $('#ReturnToUrl').val(), MNG.GetSelectedNodeSubTypes()[0]]);
            }
            return false;
        });
        $("#energyWiseLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                var ids = MNG.GetSelectedObjectIDs();
                if (ids.N.length)
                    window.location = "/Orion/NCM/Resources/EnergyWise/EWNodes/ManageEWNodes.aspx?Nodes=" + ids.N.join(",");
                else if (ids.I.length)
                    window.location = "/Orion/NCM/Resources/EnergyWise/EWInterfaces/ManageEWInterfaces.aspx?Interfaces=" + ids.I.join(",");
            }
            return false;
        });

        $("#powerLevelLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.OverridePowerLevel);
                }
                else {
                    Operations.OverridePowerLevel(MNG.GetSelectedObjectIDs().I);
                }
                return false;
            }
        });

        $("#unmanageLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.UnmanageObjects);
                }
                else {
                    var ids = MNG.GetSelectedObjectIDs();
                    $.each(ids.IW, function () { ids.I.push(this); });
                    showUnmanageDialog(ids);
                }
            }
            return false;
        });

        $("#remanageLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.RemanageObjects);
                }
                else {
                    remanageNetObjects(MNG.GetSelectedNodeIDs(), MNG.GetSelectedInterfaceIDs());
                }
            }
            return false;
        });

        $("#cpInlineEditorLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                var ids = [];
                var showObjectType = $("#showObjectType").val();
                if (showObjectType == "Orion.Nodes") {
                    $(MNG.Nodes.selectedColumns).each(function () {
                        if ($.inArray(this.header.replace("CustomProperties.", "").trim(), $('#NodeCustomProperties').val().split(',')) > -1) {
                            ids.push(this.header.replace("CustomProperties.", ""));
                        }
                    });
                }
                else if (showObjectType == "Orion.NPM.Interfaces") {
                    $(MNG.Interfaces.selectedColumns).each(function () {
                        if ($.inArray(this.header.replace("CustomProperties.", "").trim(), $('#InterfaceCustomProperties').val().split(',')) > -1) {
                            ids.push(this.header.replace("CustomProperties.", ""));
                        }
                    });
                }

                $('body').append(Operations.PrepareCPPostForm(showObjectType, ids, $('#ReturnToUrl').val()));
                $('#selectedNetObjects').submit();
            }
            return false;
        });

        $("#importCPLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                var ids = MNG.GetSelectedObjectIDs();

                if (ids.N.length) {
                    $('body').append(Operations.PrepareImportPostForm("Nodes", $('#ReturnToUrl').val()));
                }
                else if (ids.I.length) {
                    $('body').append(Operations.PrepareImportPostForm("Interfaces", $('#ReturnToUrl').val()));
                }
                else if (ids.IW.length) {
                    $('body').append(Operations.PrepareImportPostForm("Interfaces", $('#ReturnToUrl').val()));
                }
                else if (ids.V.length) {
                    $('body').append(Operations.PrepareImportPostForm("Volumes", $('#ReturnToUrl').val()));
                } else {
                    $('body').append(Operations.PrepareImportPostForm("", $('#ReturnToUrl').val()));
                }
                $('#selectedEntityType').submit();
            }
            return false;
        });

        $("#pollersLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                if (MNG.allSelected) {
                    // do for all
                    MNG.NetObjects.StoreAllFormatedSNMPIDs();
                }
                else {
                    // do only for selected items or all on page
                    var ids = MNG.GetSelectedObjectIDs();
                    if (ids.SNMP.length) {
                        NodeManagement.StoreSelectedIDs(ids.SNMP, "Nodes", function (result) {
                            window.location = MNG.customPollerUrl.format(["Node", $('#ReturnToUrl').val()]);
                        }, function (error) { alert("@{R=Core.Strings;K=WEBJS_TM0_105;E=js}\n" + error.get_message()); });

                    }
                    else if (ids.I.length) {
                        NodeManagement.StoreSelectedIDs(ids.I, "Interfaces", function (result) {
                            window.location = MNG.customPollerUrl.format(["Interface", $('#ReturnToUrl').val()]);
                        }, function (error) { alert("@{R=Core.Strings;K=WEBJS_TM0_105;E=js}\n" + error.get_message()); });
                    }
                }
            }
            return false;
        });

        $("#deleteLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                //set the appropriate message
                var layout = $("#delDialogDescription");
                if (MNG.allSelected) {
                    // do for all
                    layout.text('@{R=Core.Strings;K=WEBJS_TM0_104;E=js}');
                }
                else {
                    var ids = MNG.GetSelectedObjectIDs();
                    var count = ids.EngineNetObjectIDs;
                    if (count.length > 1) {
                        layout.text(String.format('@{R=Core.Strings;K=WEBJS_TM0_103;E=js}', count.length));
                    }
                    else {
                        layout.text('@{R=Core.Strings;K=WEBJS_TM0_102;E=js}');
                    }
                }

                //create the dialog window if it doesn't already exist
                if (!delDialog) {
                    delDialog = new Ext.Window({
                        applyTo: 'delDialog',
                        layout: 'fit',
                        width: 310,
                        height: 205,
                        closeAction: 'hide',
                        modal: true,
                        plain: true,
                        resizable: false,
                        ids: ids,
                        items: new Ext.BoxComponent({
                            applyTo: 'delDialogBody',
                            layout: 'fit',
                            border: false
                        }),
                        buttons: [
                        {
                            text: '@{R=Core.Strings;K=CommonButtonType_Delete;E=js}',
                            handler: function () {
                                if (MNG.allSelected) {
                                    // do for all
                                    var allIDs = MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoDelete);
                                }
                                else {
                                    // do only for selected items or all on page                    
                                    var ids = MNG.GetSelectedObjectIDs();
                                    Operations.DoDelete(ids.EngineNetObjectIDs);
                                }
                                delDialog.hide();
                            }
                        },
                        {
                            text: '@{R=Core.Strings;K=CommonButtonType_Cancel;E=js}',
                            style: 'margin-left: 5px;',
                            handler: function () {
                                confirmDelete = false;
                                delDialog.hide();
                            }
                        }]
                    });
                }
                // Set the location
                delDialog.alignTo(document.body, "c-c");
                delDialog.show();
            }
            return false;
        });

        $("#pollNowLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {

                if (MNG.allSelected) {

                    MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoPollNow);
                }
                else {
                    Operations.DoPollNow(MNG.GetSelectedObjectIDs().EngineNetObjectIDs);
                }
            }
            return false;
        });

        $("#rediscoverLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithEngineIds(Operations.DoRediscoverNow);
                }
                else {
                    Operations.DoRediscoverNow(MNG.GetSelectedObjectIDs().EngineNetObjectIDs);
                }
            }
            return false;
        });

        $("#shutdownLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsAndNames(Operations.ShutdownInterface);
                }
                else {
                    Operations.ShutdownInterface(MNG.GetSelectedObjectIDs().I);
                }
            }
            return false;
        });

        $("#enableLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsAndNames(Operations.EnableInterface);
                }
                else {
                    Operations.EnableInterface(MNG.GetSelectedObjectIDs().I);
                }
            }
            return false;
        });

        $("#topologyLink").click(function () {
            if (!$(this).parent().hasClass("Disabled")) {
                Operations.UpdateTopology();
            }
            return false;
        });
    }

    $("#cancelOutagesLink").click(function () {
        if ($("#isDemoMode").length != 0) {
            demoAction("Core_Cancel_Of_Planned_Maintenance", this);
            return false;
        }
        if (!$(this).parent().hasClass("Disabled")) {
            if (MNG.allSelected) {
                MNG.NetObjects.WithAllIDsWithoutPrefix(Operations.RemanageObjects);
            }
            else {
                remanageNetObjects(MNG.GetSelectedNodeIDs(), MNG.GetSelectedInterfaceIDs());
            }
        }
        return false;
    });

    $(".pluginActionListItem").click(function() {
        if (!$(this).parent().hasClass("Disabled")) {
            var fn = window[$(this).attr("data-callback")];
            if (typeof fn === 'function') {
                fn(MNG);
            }
        }
    });

    $("#exportCPLink").click(function () {
        if (!$(this).parent().hasClass("Disabled")) {
            var ids = MNG.GetSelectedObjectIDs();

            var exportCP = function (netObjectType, objectIDs) {
                if (MNG.allSelected) {
                    MNG.NetObjects.WithAllIDsWithoutPrefix(function (ids) {
                        $('body').append(Operations.PrepareExportPostForm(netObjectType, ids, $('#ReturnToUrl').val()));
                        $('#selectedNetObjects').submit();
                    });
                } else {
                    $('body').append(Operations.PrepareExportPostForm(netObjectType, objectIDs, $('#ReturnToUrl').val()));
                    $('#selectedNetObjects').submit();
                }
            };
            if (ids.N.length) {
                exportCP("Orion.Nodes", ids.N);
            }
            else if (ids.I.length) {
                exportCP("Orion.NPM.Interfaces", ids.I);
            }
            else if (ids.IW.length) {
                exportCP("Orion.NPM.Interfaces", ids.IW);
            }
            else if (ids.V.length) {
                exportCP("Orion.Volumes", ids.V);
            }
        }
        return false;
    });

    MNG.EnableDisableMenu();

    $("#pageNumber").keypress(Pager.PageNumberChanging);
    $("#pageNumberTop").keypress(Pager.PageNumberChanging);
    $("#pageSize").keypress(Pager.PageSizeChanging);
    $("#pageSizeTop").keypress(Pager.PageSizeChanging);
    $(window).unload(function () { MNG.Prefs.save("ActualPage", Pager.actualPage); });
    $(window).load(MNG.SetMenuWidth);

});
