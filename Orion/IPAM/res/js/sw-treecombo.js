﻿Ext.ns('Ext.ux', 'Ext.ux.form');

Ext.ux.form.TreeCombo = new Ext.extend(Ext.form.TriggerField, {
    id: Ext.id(),
    triggerClass: 'x-form-tree-trigger',

    tree: undefined,
    loader: undefined,
    value: '',

    treeConfig: {
        autosize: false,
        minsize: { width: 200, height: 200 },
        maxsize: undefined
    },

    initComponent: function () {
        this.readOnly = false;
        this.editable = false;
        Ext.ux.form.TreeCombo.superclass.initComponent.call(this);

        this.on('afterrender', this.initTree, this);
        this.on('specialkey', function (f, e) {
            if (e.getKey() == e.ENTER)
                this.onTriggerClick();
        }, this);
    },

    isExpanded: function () {
        return this.tree && this.tree.getEl().isVisible();
    },

    validateBlur: function () {
        return !this.isExpanded();
    },

    onTriggerClick: function () {
        if (this.disabled)
            return;

        if (this.isExpanded())
            this.collapse();
        else
            this.expand();

        this.el.focus();
    },

    collapse: function () {
        if (this.tree && this.isExpanded()) {
            this.tree.hide();
        }
    },

    collapseIf: function (e) {
        if (e.within(this.wrap) || e.within(this.tree.getEl()))
            return;

        this.collapse();
    },

    expand: function () {
        if (this.tree && !this.isExpanded()) {
            this.resizeTree();
            this.tree.show();
        }
    },

    resizeTree: function () {
        if (!this.tree) return;
        this.tree.getEl().alignTo.apply(this.tree.getEl(), [this.el, 'tl-bl?']);
    },

    initTree: function () {
        if (this.tree)
            return;

        this.clearValue();
        this.treeRootNode = new Ext.tree.AsyncTreeNode();

        this.treeWidth = Math.max(
			this.treeConfig.minsize.width,
			+this.wrap.getWidth(),
			+this.width || 0
		);

        this.treeHeight = Math.max(
			this.treeConfig.minsize.height,
			+this.treeHeight || 0
		);

        this.treeListeners = {
            hide: this.onTreeHide,
            show: this.onTreeShow,
            click: this.onTreeNodeClick,
            expandnode: this.onExpandOrCollapseNode,
            collapsenode: this.onExpandOrCollapseNode,
            resize: function () { this.resizeTree(); },
            scope: this
        };

        this.tree = new Ext.tree.TreePanel({
            floating: true,
            hidden: true,
            autoScroll: true,
            width: this.treeWidth,
            height: this.treeHeight,
            minWidth: this.treeConfig.minsize.width,
            minHeight: this.treeConfig.minsize.height,

            renderTo: Ext.getBody(),
            listeners: this.treeListeners,
            loader: this.loader,

            root: this.treeRootNode,
            rootVisible: false
        });

        this.relayEvents(
			this.tree.loader,
			['beforeload', 'load', 'loadexception']
		);
    },

    getValue: function () {
        if (this.tree) this.getValueFromTree();
        return this.value || '';
    },

    getValueFromTree: function () {
        var node = this.tree.getSelectionModel().selNode;
        if (node) {
            this.value = node.id;
            this.valueText = node.text;
        } else {
            this.value = '';
            this.valueText = '';
        }
        this.setRawValue(this.valueText);
        return node;
    },

    _setNodeValue: function (node) {
        if (!node) return;
        this.value = node['id'];
        this.valueText = node['text'];
        this.setRawValue(this.valueText);
    },

    clearValue: function () {
        this.value = '';
        this.valueText = '';
        this.setRawValue('');
        if (this.tree) this.tree.getSelectionModel().clearSelections();
    },

    onTreeNodeClick: function (node, e) {
        if (this.fireEvent("beforeselect", this, node) === false) {
            return false;
        }
        this.collapse();
        this._setNodeValue(node);
        this.fireEvent('select', this, node);
    },

    onTreeShow: function () {
        var doc = Ext.getDoc();
        doc.on('mousewheel', this.collapseIf, this);
        doc.on('mousedown', this.collapseIf, this);
    },

    onTreeHide: function () {
        var doc = Ext.getDoc();
        doc.un('mousewheel', this.collapseIf, this);
        doc.un('mousedown', this.collapseIf, this);
    },

    onExpandOrCollapseNode: function () {
        if (!this.treeConfig.autosize)
            return;

        var el = this.tree.getTreeEl();
        var padding = el.getHeight() - el.dom.clientHeight;

        var required = el.child('ul').getHeight() + padding;
        required = Math.min(this.treeConfig.maxsize['height'],
            Math.max(this.treeConfig.minsize['height'], required));

        if(!isNaN(required)) this.tree.setHeight(required);
    },

    LoadInitialValue: function (node_id, node_text) {
        this._setNodeValue({ id: node_id, text: node_text });
    }
});