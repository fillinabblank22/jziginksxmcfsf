﻿if (!$SW.IPAM.DiscoveryProgressBar) $SW.IPAM.DiscoveryProgressBar = {};
$SW.IPAM.DiscoveryProgressBar = new (function () {

    var map = {}, runner = new Ext.util.TaskRunner(100),currentTaskId = null;

    var complete = function (id) {
        try {
            var item = map[id];
            if (item.task) runner.stop(item.task);
            if (item.config.ShowPollingStatus) item.config.ShowPollingStatus(item.status);

        } catch (ex) { /*console.log(ex);*/ }
    };

    var recv = function (id, status) {
        try {
            var item = map[id];
            item.status = status;
            if (status.success != null && status.success != undefined) {
                complete(id);
            }
            else {
                if (item.config.UpdateProgress) item.config.UpdateProgress(item.status);
                item.inprogress = 0;
                item.next = (new Date()).getTime() + (item.config.refresh || 1000);
            }
        } catch (ex) { /*console.log(ex);*/ }
    };

    var fail = function (id) {
        try {
            var item = map[id];
            if (item.task) runner.stop(item.task);
            if (item.config.CloseProgress) item.config.CloseProgress();
        } catch (ex) { /*console.log(ex);*/ }
    };

    var updateFail = function(id) {
        try {
            var item = map[id];
            if (item.task) runner.stop(item.task);
            if (item.config.FailedToReciveResponse) item.config.FailedToReciveResponse();
            complete(id);
        } catch(ex) { /*console.log(ex);*/
        }
    };

    var cancel = function () {
        try {
            var taskid = currentTaskId, item = map[currentTaskId];
            if (item) {
                if (!item.config.CancelDiscovery())
                    return;

                Ext.Ajax.request({
                    type: 'POST',
                    url: '/Orion/IPAM/services/Tasks.asmx/Cancel',
                    jsonData: { taskid: taskid },
                    success: $SW.ext.AjaxMakePassDelegate(item.config.description, function(ret) { recv(ret.d.taskid, ret.d); }),
                    failure: $SW.ext.AjaxMakeFailDelegate(item.config.description, function() { fail(taskid); })
                });
            }
        }
        catch (ex) { /*console.log(ex);*/ }
    };

    var touch = function (id) {
        try {
            var taskid = id, item = map[id];
            if (item.inprogress) return;
            if (item.next && ((new Date()).getTime() < item.next)) return;
            item.inprogress = 1;

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/Tasks.asmx/Status',
                jsonData: { taskid: id, autokill: true },
                success: $SW.ext.AjaxMakePassDelegate(item.config.description, function (ret) { recv(ret.d.taskid, ret.d); }),
                failure: $SW.ext.AjaxMakeFailDelegate(item.config.description, function () { updateFail(taskid); })
            });
        } catch (ex) { /*console.log(ex);*/ }
    };

    var reg = function (id, status, regdata) {
        try {
            var taskid = id, item = map[id] = {
                id: id,
                config: regdata,
                status: status,
                last: null
            };

            currentTaskId = id;
            if (status.success != null && status.success != undefined) {
                complete(id);
            }
            else {
                item.task = { run: touch, args: [taskid], interval: item.config.interval || 1000 };
                runner.start(item.task);

                if (item.config.UpdateProgress) item.config.UpdateProgress(item.status);
            }
        } catch (ex) { /*console.log(ex);*/ }
    };

    var start = function (tname, data) {
        try {
            
            $.extend($SW.IPAM.DiscoveryProgressBar.fnHelper || {}, { description: 'Subnet Discovery Delegate' });

            Ext.Ajax.request({
                type: 'POST',
                url: '/Orion/IPAM/services/Tasks.asmx/Start',
                jsonData: { taskname: tname, options: data },
                success: $SW.ext.AjaxMakePassDelegate($SW.IPAM.DiscoveryProgressBar.fnHelper.description, function (ret) {
                    reg(ret.d.taskid, ret.d, $SW.IPAM.DiscoveryProgressBar.fnHelper);
                }),
                failure: $SW.ext.AjaxMakeFailDelegate($SW.IPAM.DiscoveryProgressBar.fnHelper.description, $SW.IPAM.DiscoveryProgressBar.fnHelper.StartFailed)
            });
        } catch (ex) { /*console.log(ex);*/ }
    };

    var attachExisting = function (taskId) {
        try {
            $.extend($SW.IPAM.DiscoveryProgressBar.fnHelper || {}, { description: 'Subnet Discovery Delegate' });

            reg(taskId, '', $SW.IPAM.DiscoveryProgressBar.fnHelper);

        } catch (ex) { /*console.log(ex);*/ }
    };

    var DoProgress = function (tname, data) {

        $SW.IPAM.DiscoveryProgressBar.fnHelper.StartDiscovery();

        start(tname,data);
    };

    var Attach = function (taskId) {
        
        $SW.IPAM.DiscoveryProgressBar.fnHelper.StartDiscovery();

        attachExisting(taskId);
    };

    this.start = start;
    this.DoProgress = DoProgress;
    this.Attach = Attach;
    this.CancelDiscoveryJob = cancel;
})();