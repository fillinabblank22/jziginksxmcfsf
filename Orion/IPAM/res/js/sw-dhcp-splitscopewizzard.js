﻿(function () {
    if (!$SW.IPAM) $SW.IPAM = {};

    var int_toipv4 = function (n) {
        var a = [];
        for (var i = 3; i >= 0; i--) {
            a[i] = (n - ((n >> 8) << 8)); n >>= 8;
        }
        return a;
    };

    var createLeftSliderPart = function (s) {
        var el = s.el, tEl = s.thumbs[0].el, lsEl = s.leftSliderEl;

        // We will use, or build an element for left part
        if (!(lsEl = el.down('.ux-slider-left')))
            s.leftSliderEl = lsEl = el.createChild('<div class="ux-slider-left"></div>');

        // Prevent from flicking
        lsEl.setVisible(false);
        lsEl.setHeight(s.getHeight());

        // z-index 999 is thumb element z-index minus one
        if (lsEl.getStyle('z-index') != 999) lsEl.setStyle('z-index', 999)

        // force to recalculate thumb screen position
        s.value = s.value;

        // Calc and set width according the half of original slider   
        var width = tEl.getX() + tEl.getWidth() / 2 - lsEl.getX();
        lsEl.setWidth(width);

        // Move has to be after width change to prevent align issues
        lsEl.moveTo(el.getX(), el.getY(), false);
        lsEl.setVisible(true);
    };


    var legendTpl = new Ext.XTemplate(
        '<div class="ux-slider-up{align}legend" style="float:left">',
            '{address}',
        '</div>'
    );

    var createSliderLegend = function (s, range, steps) {
        var el = s.el;

        // StartAddress use, or build an element for left part
        if (!(s.elUpLeft = el.down('.ux-slider-upleftlegend'))) {
            var html = legendTpl.apply({ address: range.start.join('.'), align: 'left' });
            s.elUpLeft = el.insertHtml('beforeBegin', html, true);
        }

        // use, or build an element for right part
        if (!(s.elUpRight = el.down('.ux-slider-uprightlegend'))) {
            var html = legendTpl.apply({ address: range.end.join('.'), align: 'right' });
            s.elUpRight = el.insertHtml('beforeBegin', html, true);
        }

        if (!(s.elBottomGraph = el.down('.ux-slider-bottomgraphlegend'))) {
            var html = '<div class="ux-slider-bottomgraphlegend"></div>';
            s.elBottomGraph = el.insertHtml('afterEnd', html, true);
        }
        s.elBottomGraph.setWidth(el.getWidth());
        s.elBottomGraph.moveTo(el.getX(), el.getY() + el.getHeight());
        s.elBottomGraph.setHeight(20);

        if (!(s.elBottomText = el.down('.ux-slider-bottomtextlegend'))) {
            var html = '<div class="ux-slider-bottomtextlegend">@{R=IPAM.Strings;K=IPAMWEBJS_VB1_288;E=js}</div>';
            s.elBottomText = s.elBottomGraph.insertHtml('afterEnd', html, true);
        }

        // Render graphic legend
        var step = s.getWidth() / steps;
        var show_percents = [0, 20, 50, 80, 100];
        for (var i = 0; i <= steps; i++) {
            var perc = Math.round(100.0 * i / steps);
            var shw = ($.inArray(perc, show_percents) > -1);
            var tick = shw ? '<div class="percent">' + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_289;E=js}', perc) + '</div>' : '<div class="tick" />';

            var e = s.elBottomGraph.insertHtml('beforeEnd', tick, true);

            var left = s.elBottomGraph.getX() + i * step;
            var w = e.getWidth(), offset = w * i / steps;
            var top = s.elBottomGraph.getY(); if (!shw) top += 5;
            e.moveTo(left - offset, top);

        }

        UpdateSliderLegend(s);
    };

    var changeRanges = function (s, start_n) {
        var v = s.getValue(), max = s.maxValue, min = s.minValue;
        var percent = Math.round((v * 100.0) / max);
        var b = v + 0; b = b < min ? min : (b > max ? max : b);
        var a = v + 1; a = a < min ? min : (a > max ? max : a);

        var ip4SplitBefore = int_toipv4(start_n + b).join('.');
        var ip4SplitAfter = int_toipv4(start_n + a).join('.');
        var f = s.txtFields;
        f.scope1_include.setValue(ip4SplitBefore);
        f.scope1_exclude.setValue(ip4SplitAfter);
        f.scope2_include.setValue(ip4SplitAfter);
        f.scope2_exclude.setValue(ip4SplitBefore);
        f.scope1_percent.setValue(percent);
        f.scope2_percent.setValue(100.0 - percent);
    };

    var adjustWidthOnSliderChange = function (slider) {
        if (!slider.leftSliderEl) return;
        var sliderLeftX = slider.leftSliderEl.getX();
        var sliderLeftY = slider.leftSliderEl.getY();
        var sliderX = slider.el.getX();
        var sliderY = slider.el.getY();

        var width = slider.thumbs[0].el.getX() - sliderLeftX;
        slider.leftSliderEl.setWidth(width);

        if (sliderLeftX !== sliderX || sliderLeftY !== sliderY) {
            slider.leftSliderEl.moveTo(sliderX, sliderY, false);
        }
    };

    var getSliderTipPlugin = function (start_n, max) {
        var tipTpl = new Ext.XTemplate(
            '<center>',
                '<b>{percent:number("0")}@{R=IPAM.Strings;K=IPAMWEBJS_VB1_290;E=js}</b>',
                '<br/>{address}',
            '</center>'
        );
        return new Ext.slider.Tip({
            getText: function (thumb) {
                var ipv4 = int_toipv4(start_n + thumb.value);
                var perc = (thumb.value * 100.0) / max;
                return tipTpl.apply({ address: ipv4.join('.'), percent: perc });
            }
        });
    };

    var UpdateSliderLegend = function (s) {
        var el = s.el, x = el.getX(), y = el.getY(), w = el.getWidth();
        if (s.elUpLeft) s.elUpLeft.moveTo(x, y - 20);
        if (s.elUpRight) s.elUpRight.moveTo(x + w - s.elUpRight.getWidth(), y - 20);
    };

    // elId  - id of element for slider
    // address - start ip address of subnet (as string)
    // mask    - mask of the scope subnet (as string)
    $SW.IPAM.SplitScopeSlider = function (nsId, elId, address, mask, range_start, range_end, split) {
        var $nsId = nsId;
        var r = $SW.IP.v4_subnet($SW.IP.v4(address), $SW.IP.v4(mask)),
            r_s = $SW.IP.v4_toint(r.start) + 1,
            r_e = $SW.IP.v4_toint(r.end) - 2,
            s = $SW.IP.v4_toint($SW.IP.v4(range_start)),
            e = $SW.IP.v4_toint($SW.IP.v4(range_end));
        var start = (s >= r_s) ? s : r_s;
        var end = (e <= r_e) ? e : r_e;
        var count = end - start;

        var splitEl = $SW.ns$($nsId, 'hdnSplitAddress');
        var initSplit = Ext.isEmpty(split) ? (count / 5 * 4) : split;
        var alignedRange = { start: int_toipv4(start), end: int_toipv4(end) };

        var sliderListeners = {
            change: function (slider) {
                adjustWidthOnSliderChange(slider);
                changeRanges(slider, start);
                splitEl.val(slider.getValue());
            },
            afterrender: function (slider) {
                createLeftSliderPart(slider);
                createSliderLegend(slider, alignedRange, 20);
                changeRanges(slider, start);

                Ext.EventManager.onWindowResize(function () {
                    UpdateSliderLegend(slider);
                });
            }
        };

        var slider = new Ext.Slider({
            renderTo: elId,
            minValue: 0, maxValue: count, value: initSplit,

            animate: false,
            width: 500, height: 30, clickRange: [5, 30],

            plugins: getSliderTipPlugin(start, count),
            listeners: sliderListeners,

            txtFields: {
                scope1_percent: new Ext.form.TextField({
                    id: 'txtPercentOfAddress1',
                    applyTo: $SW.nsGet($nsId, 'txtPercentOfAddress1'),
                    enableKeyEvents: true
                }),
                scope1_include: new Ext.form.TextField({
                    id: 'txtIncludeIPTo',
                    applyTo: $SW.nsGet($nsId, 'txtIncludeIPTo'),
                    enableKeyEvents: true
                }),
                scope1_exclude: new Ext.form.TextField({
                    id: 'txtExcludeIPFrom',
                    applyTo: $SW.nsGet($nsId, 'txtExcludeIPFrom')
                }),
                scope2_percent: new Ext.form.TextField({
                    id: 'txtPercentOfAddress2',
                    applyTo: $SW.nsGet($nsId, 'txtPercentOfAddress2'),
                    enableKeyEvents: true
                }),
                scope2_include: new Ext.form.TextField({
                    id: 'txtIncludeIPFrom2',
                    applyTo: $SW.nsGet($nsId, 'txtIncludeIPFrom2'),
                    enableKeyEvents: true
                }),
                scope2_exclude: new Ext.form.TextField({
                    id: 'txtExcludeIPTo2',
                    applyTo: $SW.nsGet($nsId, 'txtExcludeIPTo2')
                })
            }
        });

        // [oh] handle TAB and enter value
        var eatSpecialKeys = function (box, e) {
            if (e.keyCode == e.TAB) return; // ignore keyup on a tab
            if (e.keyCode == e.ENTER) e.stopEvent(); // supress enter
        };
        slider.txtFields.scope1_percent.on('specialkey', eatSpecialKeys);
        slider.txtFields.scope2_percent.on('specialkey', eatSpecialKeys);
        slider.txtFields.scope1_include.on('specialkey', eatSpecialKeys);
        slider.txtFields.scope2_include.on('specialkey', eatSpecialKeys);

        var ordinal_fromperc = function (box, scale, reverse) {
            if (!box || !/^[0-9]{1,3}\.?([0-9]+)?$/.test(box.getValue())) return null;
            var p = parseFloat(box.getValue());
            if (isNaN(p)) return null;

            p = (p < 0) ? 0 : (p > 100) ? 100 : p;
            p = reverse ? (100.0 - p) : p;
            return (p / 100.0) * scale;
        };
        var ordinal_fromaddress = function (box, min, max) {
            if (!box || Ext.isEmpty(box.getValue())) return null;
            var ip = $SW.IP.v4(box.getValue());
            if (!$SW.IP.v4_isok(ip)) return null;

            ip_n = $SW.IP.v4_toint(ip);
            return (ip_n < min) ? min : (ip_n > max) ? max : ip_n;
        };

        var sync_percent = function (reverse) {
            return function (box, e) {
                if (e.keyCode == e.TAB) return;
                var v = ordinal_fromperc(box, end - start, reverse);
                slider.setValue(v);
            };
        };
        var sync_address = function (offest) {
            return function (box, e) {
                if (e.keyCode == e.TAB) return;
                var v = ordinal_fromaddress(box, (start + offest), end);
                slider.setValue(v - (start + offest));
            };
        };

        slider.txtFields.scope1_percent.on('keyup', sync_percent(false));
        slider.txtFields.scope2_percent.on('keyup', sync_percent(true));
        slider.txtFields.scope1_include.on('keyup', sync_address(0));
        slider.txtFields.scope2_include.on('keyup', sync_address(1));
    };

    // [oh] IE slider fix.
    Ext.override(Ext.dd.DragTracker, {
        onMouseMove: function (e, target) {
            var isIE9 = Ext.isIE && (/msie 9/.test(navigator.userAgent.toLowerCase())) && document.documentMode != 6;
            if (this.active && Ext.isIE && !isIE9 && !e.browserEvent.button) {
                e.preventDefault();
                this.onMouseUp(e);
                return;
            }
            e.preventDefault();
            var xy = e.getXY(), s = this.startXY;
            this.lastXY = xy;
            if (!this.active) {
                if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
                    this.triggerStart(e);
                } else {
                    return;
                }
            }
            this.fireEvent('mousemove', this, e);
            this.onDrag(e);
            this.fireEvent('drag', this, e);
        }
    });


    $SW.IPAM.SplitScopeTask = function (taskId) {
        title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_291;E=js}';
        $SW.Tasks.Attach(taskId, { title: title }, function (d) {
            if (!d.success) {
                title = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_292;E=js}';
                Ext.MessageBox.show({
                    title: title, msg: $SW.IPAM.DhcpScopeErrMsgTpl.apply(d),
                    buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR
                });
                return;
            }

            var warning = false;
            if (d.results) {
                $.each(d.results, function (i, result) {
                    if (result.Success === false) warning = true;
                });
                if (d.results.LicenseInfo) {
                    var licInfo = Ext.util.JSON.decode(d.results.LicenseInfo);
                    $SW.LicenseListeners.recv(licInfo);
                }
            }

            if (warning) {
                d.message = '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_293;E=js}';
                Ext.Msg.show({
                    title: title, msg: $SW.IPAM.DhcpScopeWarnMsgTpl.apply(d),
                    buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING,
                    fn: function () { window.location = "../../../api2/ipam/ui/scopes"; }
                });
                return;
            }

            Ext.Msg.show({
                title: title, msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_294;E=js}',
                buttons: Ext.Msg.OK, icon: Ext.MessageBox.INFO,
                fn: function () { window.location = "../../../api2/ipam/ui/scopes"; }
            });
        });
    };

})();
