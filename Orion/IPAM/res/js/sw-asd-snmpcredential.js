﻿if (!$SW.IPAM) $SW.IPAM = {};

function IsDemoMode() {
    return $("#isDemoMode").length > 0;
}

function CheckDemoMode() {
    if (IsDemoMode()) {
        return true;
    }
    return false;
}

if (!$SW.IPAM.DiscoverySnmpCredential) $SW.IPAM.DiscoverySnmpCredential = (function () {
    function getQueryParams(objectId, mode, webId) {
        return ((objectId && objectId != '') ? '&ObjectID=' + objectId : '') +
                ('&IsAddMode=' + mode) +
                ('&WebId=' + $SW.IPAM.DiscoverySnmpCredential.WebID);
    }
    return {
        SnmpCredentialDialog: function (src, id, header, hlpSuffix, isAddMode) {
            var page = 'SubnetDiscovery/Config/Discovery.SNMPCred.AddEdit.aspx';

            $SW.ext.dialogWindowOpen($SW['snmpCredentialWindow'], src.id, header,
                '/Orion/IPAM/' + page + '?NoChrome=True&msgq=snmpCredentialWindow' + getQueryParams(id, isAddMode, $SW.IPAM.DiscoverySnmpCredential.WebID), hlpSuffix + '.htm');
        },
        grid_cellclick: function (grid, ri, ci, ev) {
            $SW.ext.gridClickToDrilldown(grid, ri, ci, ev);
            var targetEl = ev.browserEvent.target || ev.browserEvent.srcElement;
            if (targetEl.tagName.toUpperCase() == 'IMG') targetEl = targetEl.parentNode;
            if (targetEl.tagName.toUpperCase() != 'A') return false;
            var record = grid.getStore().getAt(ri);

            if (/sw-grid-edit/.test(targetEl.className)) {
                //  ChangeFieldStatus(true);
                $SW.IPAM.DiscoverySnmpCredential.Edit(grid, ri);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-delete/.test(targetEl.className)) {
                $SW.IPAM.DiscoverySnmpCredential.Delete(grid, ri, ci, record, targetEl);
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-down/.test(targetEl.className)) {
                $SW.IPAM.DiscoverySnmpCredential.Move(grid, record, 'Down');
                ev.stopPropagation(); return false;
            }
            if (/sw-grid-up/.test(targetEl.className)) {
                $SW.IPAM.DiscoverySnmpCredential.Move(grid, record, 'Up');
                ev.stopPropagation(); return false;
            }
        },
        action_buttons: function () {
            var up_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.up.png";
            var down_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.down.png";
            var edit_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.edit.gif";
            var delete_url = $SW.appRoot() + "Orion/IPAM/res/images/sw/icon.delete.gif";
            return function (value, meta, record) {
                var ret = '';
                meta.css = 'sw-grid-buttons';

                ret += "<a href='#' class='sw-grid-up' onclick='return false;'><img src='" + up_url + "' /></a> &nbsp;";
                ret += "<a href='#' class='sw-grid-down' onclick='return false;'><img src='" + down_url + "' /></a> &nbsp;";
                ret += "<a href='#' class='sw-grid-edit' onclick='return false;'><img src='" + edit_url + "' /></a> &nbsp;";
                return ret + "<a href='#' class='sw-grid-delete' onclick='return false;'><img src='" + delete_url + "' /></a> &nbsp;";
            };
        },
        Add: function (that) {
            $SW.IPAM.DiscoverySnmpCredential.SnmpCredentialDialog(that, -1, '@{R=IPAM.Strings;K=IPAMWEBJS_SE_49;E=js}', 'OrionIPAMPHViewAddSNMPCredentials', 'true');
        },
        getSelectedItemCount: function() {
            var grid = $SW.IPAM.DiscoverySnmpCredential.getGrid();
            return grid.getSelectionModel().getCount();
        },
        TotalRecordCount: function () {
            var grid = $SW.IPAM.DiscoverySnmpCredential.getGrid();
            return grid.getStore().getTotalCount() > 0 ? true : false;
        },
        Edit: function (el,ri) {
            var grid = $SW.IPAM.DiscoverySnmpCredential.getGrid();
            var sm = grid.getSelectionModel(), c = sm.getCount();
            if (c < 1 || c > 1) {
                Ext.Msg.show({
                    title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_268;E=js}',
                    msg: 'Please select a single Exclusion before clicking "Edit".',
                    buttons: Ext.Msg.OK,
                    animEl: el.id,
                    icon: Ext.MessageBox.WARNING
                });
            } else {
                var item = sm.getSelected();
                $SW.IPAM.DiscoverySnmpCredential.SnmpCredentialDialog(el, item.data['ListIndex'], '@{R=IPAM.Strings;K=IPAMWEBJS_SE_50;E=js}', 'OrionIPAMPHViewEditSNMPCredentials', 'false');
            }
        },
        Move: function (el, r, direction) {
            Ext.Ajax.request({
                url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",

                success: function (result, request) {
                    var res = Ext.util.JSON.decode(result.responseText);
                    if (res.success == true) {
                        $SW.IPAM.DiscoverySnmpCredential.RefreshGrid();
                    }
                },

                failure: function () {
                    Ext.Msg.show({
                        title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                        msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                        buttons: Ext.Msg.OK,
                        animEl: el.ID,
                        icon: Ext.MessageBox.ERROR
                    });
                },
                params:
                {
                    entity: 'IPAM.DiscoveryCredential',
                    verb: 'Resequence',
                    ids: r.data["ListIndex"],
                    moveUp: direction == 'Up' ? "true" : "false",
                    webID: $SW.IPAM.DiscoverySnmpCredential.WebID
                }
            });
        },
        getGrid: function () {
            return Ext.getCmp($SW.IPAM.DiscoverySnmpCredential.GridId);
        },
        RefreshGrid: function () {
            var grid = $SW.IPAM.DiscoverySnmpCredential.getGrid();
            var store = grid.getStore();
            store.baseParams.webID = $SW.IPAM.DiscoverySnmpCredential.WebID;
            store.reload();
        },
        Delete: function (gridEl, ri, ci, r) {
            Ext.Msg.show({
                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_8;E=js}',
                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_SE_51;E=js}',
                buttons: Ext.Msg.OKCANCEL,
                icon: Ext.MessageBox.INFO,
                width: 350,
                fn: function(btn) {
                    if (btn == "ok") {
                        Ext.Ajax.request({
                        url: $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",

                        success: function(result, request) {
                            var res = Ext.util.JSON.decode(result.responseText);
                            if (res.success == true) {
                                $SW.IPAM.DiscoverySnmpCredential.RefreshGrid();
                            }
                        },

                        failure: function() {
                            Ext.Msg.show({
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
                                buttons: Ext.Msg.OK,
                                animEl: el.ID,
                                icon: Ext.MessageBox.ERROR
                            });
                        },
                        params:
                        {
                            entity: 'IPAM.DiscoveryCredential',
                            verb: 'Remove',
                            ids: r.data["ListIndex"],
                            webID: $SW.IPAM.DiscoverySnmpCredential.WebID
                        }
                        });
                    }
                }
            });
        }
    };
})();