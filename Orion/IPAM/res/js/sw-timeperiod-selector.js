﻿Function.prototype['InheritBaseClass'] = function(def) {
    for (var n in def) { this.prototype[n] = def[n]; }
    return this;
};

window.$SW = window.$SW || {};

$SW.PeriodSelectorBase = {
    _enableEl: function(el, enbl) {
        if (!el || (enbl === undefined)) return;
        if (enbl) {
            el.removeClass('out');
            el.removeAttr('disabled');
        } else {
            el.addClass('out');
            el.attr('disabled', 'disabled');
        }
    },
    _setRBState: function(rb, ctrls) {
        if (!rb || rb.length == 0) return;

        var self = this, chck = rb[0].checked;
        $.each(ctrls, function(id, cid) {
            if (!$.isFunction(cid)) {       // contorls
                var el = $('#' + cid);
                self._enableEl(el, chck);
            } else { cid(chck); }           // functions
        });
    },
    _RBClick: function(self) {
        var opt = self.options;
        $.each(opt.rbs, function(id, cid) {
            var rb = $('#' + cid);
            self._setRBState(rb, opt[id]);
        });
    },

    init: function() {
        var self = this, opt = this.options;
        if (!opt || !opt.rbs) return;

        $.each(opt.rbs, function(id, cid) {
            var rb = $('#' + cid);
            if (!rb || rb.length == 0) {
                return true; // continue
            }
            rb.click(function() { self._RBClick(self); });
        });

        // init states
        this._RBClick(this);
    }
};

$SW.DatePickersBase = {
    _initDatePicker: function(el, rs) {
        if (!el || !rs) return;

        return el.datepicker({
            setDefaults: rs,
            showOn: 'both',
            duration: 'fast',
            buttonImage: '/Orion/js/extjs/resources/images/default/shared/calendar.gif',
            buttonImageOnly: true,
            closeAtTop: false,
            mandatory: true,
            dateFormat: rs.dateFormat,
            changeMonth: true,
            changeYear: true,
            gotoCurrent: true,
            showButtonPanel: true
        });
    },
    initDatePickers: function() {
        var rs = this.options.RegionalSettings;
        var absolute = this.options.rbAbsolutePeriod;
        var start = $('#' + absolute.tbStartDate);
        var end = $('#' + absolute.tbEndDate);
        this._initDatePicker(start, rs);
        this._initDatePicker(end, rs);
        // add functions to e/d datepickers
        absolute.start = function(b) { start.datepicker(b ? 'enable' : 'disable'); };
        absolute.end = function(b) { end.datepicker(b ? 'enable' : 'disable'); };
    }
};


$SW.TimePickersBase = {
    _initTimePicker: function(el, rs) {
        if (!el || !rs) return;

        return el.timePicker({
            separator: rs.timeSeparator,
            show24Hours: rs.show24Hours,
            step: 60
        });
    },
    initTimePickers: function() {
        var rs = this.options.RegionalSettings;
        var absolute = this.options.rbAbsolutePeriod;
        var start = $('#' + absolute.tbStartTime);
        var end = $('#' + absolute.tbEndTime);
        this._initTimePicker(start, rs);
        this._initTimePicker(end, rs);
    }
};

$SW.PeriodSelector = function(opt) {
    // setup internal options
    this.options = {};
    opt = $.extend(this.options, opt || {});

    // inherit from base classes
    $SW.PeriodSelector.InheritBaseClass($SW.DatePickersBase);
    this.initDatePickers();

    $SW.PeriodSelector.InheritBaseClass($SW.TimePickersBase);
    this.initTimePickers();

    $SW.PeriodSelector.InheritBaseClass($SW.PeriodSelectorBase);
    this.init();
    
    return this;
};

$SW.TimePeriodSaveBtnClick = function(fn) {
    Page_ClientValidate();
    if (Page_IsValid == true && fn !== undefined) { fn.apply(this); }
};

$SW.RelativeTimePeriodValidator = function(src, arg) {
    var ctv = $('#' + src.controltovalidate);
    if (!ctv || ctv.length < 1 || ctv[0].disabled) { return arg.IsValid = true; }
    return arg.IsValid = (arg.Value != "" && arg.Value >= 1 && arg.Value <= 5000);
};

$SW.AbsoluteTimePeriodValidator = function(src, arg, begin, end) {
    // check empty values
    if (!begin || begin == "" || !end || end == "") { return arg.IsValid = false; }
//    // check begin > end
//    var b = Date.parse(begin), e = Date.parse(end);
//    if (isNaN(b) || isNaN(e) || (b > e)) { return arg.IsValid = false; }
    // .. valid dates
    return arg.IsValid = true;
};