﻿// create namespace for plugins
Ext.namespace('Ext.ux.plugins');

Ext.ux.plugins.TreeGridState = function (config) {
    Ext.apply(this, config);
};
Ext.extend(Ext.ux.plugins.TreeGridState, Ext.util.Observable, {
    init: function (treegrid) {
        Ext.apply(treegrid, {
            getState: function () {
                var o = { columns: [] }, c = this.columns || [];
                for (var i = 0; i < c.length; i++) {
                    o.columns.push({
                        id: c[i].header,
                        width: c[i].width,
                        visible: (i == 0) ? true : !c[i].hidden
                    });
                }
                return o;
            },
            initState: treegrid.initColumns.createSequence(function () {
                var state = this.getState();
                this.columnState = Ext.util.JSON.encode(state.columns);
                this.fireEvent('stateready', this, state, this.columnState);
            }),
            updateColumnWidths: treegrid.updateColumnWidths.createSequence(function () {
                // get state, old & new column state
                var state = this.getState();
                var oldCS = this.columnState;
                var newCS = Ext.util.JSON.encode(state.columns);

                if (oldCS != newCS) {
                    this.columnState = newCS;
                    this.fireEvent('statechanged', this, state, newCS, oldCS);
                }
            })
        });
    }
});


Ext.ux.plugins.TreeGridMultiSelectDragDrop = function (config) {
    Ext.apply(this, config);
};
Ext.extend(Ext.ux.plugins.TreeGridMultiSelectDragDrop, Ext.util.Observable, {
    init: function (treegrid) {
        treegrid.initEvents = function () {
            if (this.enableDD || this.enableDrop) {

                // [oh] TreeGrid DropZone should register scroll manager
                var treeBody = Ext.get(this.innerBody);
                treeBody.ddScrollConfig = { vthresh: 25, hthresh: 25, frequency: 100, increment: 16 };

                this.dropZone = new Ext.ux.MultiSelectTreeDropZone(this, { containerScroll: true, el: treeBody });
                this.dragZone = new Ext.ux.MultiSelectTreeDragZone(this, {});
            }
            Ext.ux.tree.X3TreeGrid.superclass.initEvents.call(this);
        }
    }
});

Ext.ux.tree.X3TreeGridSorter = Ext.extend(Ext.tree.TreeSorter, {
    sortClasses: ['sort-asc', 'sort-desc'],
    sortAscText: 'Sort Ascending',
    sortDescText: 'Sort Descending',
    constructor: function (tree, config) {
        var ambientsort = tree.loader.ambientsort || '';
        if (!Ext.isObject(config)) {
            config = {
                property: ambientsort || tree.columns[0].dataIndex || 'text',
                folderSort: true
            }
        }
        Ext.ux.tree.TreeGridSorter.superclass.constructor.apply(this, arguments);
        this.tree = tree;
        tree.on('headerclick', this.onHeaderClick, this);
        tree.ddAppendOnly = true;
        var me = this;
        this.defaultSortFn = function (n1, n2) {
            var desc = me.dir && me.dir.toLowerCase() == 'desc',
            prop = me.property || ambientsort || 'text',
            sortType = me.sortType,
            caseSensitive = me.caseSensitive === true,
            leafAttr = me.leafAttr || 'leaf',
            attr1 = n1.attributes,
            attr2 = n2.attributes;
            if (me.folderSort) {
                if (attr1[leafAttr] && !attr2[leafAttr]) {
                    return 1;
                }
                if (!attr1[leafAttr] && attr2[leafAttr]) {
                    return -1;
                }
            }
            var prop1 = attr1[prop],
            prop2 = attr2[prop],
            v1 = sortType ? sortType(prop1) : (caseSensitive ? prop1 : prop1.toUpperCase());
            v2 = sortType ? sortType(prop2) : (caseSensitive ? prop2 : prop2.toUpperCase());
            if (v1 < v2) {
                return desc ? +1 : -1;
            } else if (v1 > v2) {
                return desc ? -1 : +1;
            } else {
                return 0;
            }
        };
        tree.on('afterrender', this.onAfterTreeRender, this, { single: true });
        tree.on('headermenuclick', this.onHeaderMenuClick, this);
    },
    findSortColumn: function (columns, dataIndex) {
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].dataIndex == dataIndex ||
                columns[i].dataIndex.trim() == dataIndex) {
                return i;
            }
        }
        return 0;
    },
    onAfterTreeRender: function () {
        var sort = this.findSortColumn(this.tree.columns, this.property);
        if (this.tree.hmenu) {
            this.tree.hmenu.insert(sort,
            { itemId: 'asc', text: this.sortAscText, cls: 'xg-hmenu-sort-asc' },
            { itemId: 'desc', text: this.sortDescText, cls: 'xg-hmenu-sort-desc' }
            );
        }
        this.updateSortIcon(sort, 'asc');
    },
    onHeaderMenuClick: function (c, id, index) {
        if (id === 'asc' || id === 'desc') {
            this.onHeaderClick(c, null, index);
            return false;
        }
    },
    onHeaderClick: function (c, el, i) {
        if (c && !this.tree.headersDisabled) {
            // do not order by non-sortable columns
            if (c.sortable === false) return;

            var me = this;
            me.property = c.dataIndex;
            me.dir = c.dir = (c.dir === 'desc' ? 'asc' : 'desc');
            me.sortType = c.sortType;
            me.caseSensitive === Ext.isBoolean(c.caseSensitive) ? c.caseSensitive : this.caseSensitive;
            me.sortFn = c.sortFn || this.defaultSortFn;

            this.updateSortRoot(me.tree);
            this.updateSortIcon(i, c.dir);
        }
    },
    updateSortRoot: function (tree) {
        if(tree) tree.getRootNode().reload();
    },
    updateSortCascade: function () {
        var me = this, tree = this.tree;
        tree.getRootNode().cascade(function (n) {
            if (!n.isLeaf()) me.updateSort(tree, n);
        });
    },
    // private
    updateSortIcon: function (col, dir) {
        var sc = this.sortClasses,
        hds = this.tree.innerHd.select('td').removeClass(sc);
        hds.item(col).addClass(sc[dir == 'desc' ? 1 : 0]);
    }
});


/**
* @class Ext.ux.tree.X3TreeGrid
* This class handles quite a few things for our implementation of the ExTJS TreeGrid as well as fixing a few bugs:<ul>
* <li>Create custom events columnWidthsUpdated and sortHasChanged which are pretty self-explanatory. You can use
*   these to be set as the stateEvents if you want to save the state of the column widths and sort.</li>
* <li>Apply a default sort if the initial column definition contains a column with a 'dir' property.</li>
* <li>Fix a bug where any sort other than the default sort was not working in the 3.1.1 release of the ExtJS TreeGrid</li>
* <li>Fix a bug where the horizontal scrollbar would disappear if you set a top toolbar</li>
* <li>Fix a bug where the header would not sync. probably under IE when doing an horizontal scrollbar.</li>
* <li>Implement a getState method that will automatically make a callback to the URL you specify if desired, POSTing 
* a JSON string with the column width and sort order so that it can be saved server-side.</li></ul>
* @author <a href="mailto:sylvain.bujold@emaint.com">Sylvain Bujold</a>
* @extends Ext.ux.tree.TreeGrid
* 
* @xtype x3treegrid
*/
Ext.ux.tree.X3TreeGrid = Ext.extend(Ext.ux.tree.TreeGrid, {
    checkboxes: false,
    initComponent: function () {
        // add custom events that can be used in stateEvents if desired
        this.addEvents('columnWidthsUpdated', 'sortHasChanged');

        // This event must be registered before the parent class because the
        // handler in TreeGridSorter returns false causing the event propagation to stop
        // and therefore our onHeaderMenuClick handler would not fire
        this.on('headermenuclick', this.onHeaderMenuClick, this);

        // Tie into headerclick event to fire our custom events
        this.on('headerclick', this.onHeaderClick, this);

        if (this.checkboxes)
            this.initCheckBoxes();

        // call parent init component
        var back = this.enableSort;
        this.enableSort = false;
        Ext.ux.tree.X3TreeGrid.superclass.initComponent.call(this);
        this.enableSort = back;

        // AfterRender event handler that will apply the default sort and fix a display bug
        this.on('afterrender', this.onAfterRender, this, { single: true });

        // drill down the mouse click event
        this.on('click', this.onClickToDrilldown);

        /**
        * The defaultSortFn in Ext.ux.tree.TreeGridSorter has a bug. It calls sortType passing the full node
        * instead of just the value. fix this in our subclass instead of modifying TreeGridsorter.js
        * @ignore
        */
    },

    initCheckBoxes: function () {
        this.columns = [{
            header: '<div class="x-treegrid-hd-checker">&#160;</div>',
            width: 19,
            sortable: false,
            fixed: true,
            hideable: false,
            dataIndex: '',
            id: 'checker'
        }].concat(this.columns);

        this.columnResize = Ext.apply({
            onBeforeStart: function (e) {
                this.dragHd = this.activeHd;
                if (this.dragHd) {
                    // skip to resize fixed columns
                    var index = this.tree.findHeaderIndex(this.dragHd);
                    var col = this.tree.columns[index];
                    if (col && col.fixed) return false;
                }
                return !!this.dragHd;
            }
        }, this.columnResize);
    },

    /**
    * AfterRender event handler that will apply the default sort and fix a display bug
    * @private
    * @name Ext.ux.tree.X3TreeGrid#onAfterRender
    */
    onAfterRender: function () {
        //Appy the default sort. Right after render, the first column that we find a 'dir' property filled in with a 
        //sort direction we call the header click handler which will apply the sort. This implies that the default
        //sort you want must be 'reversed' since the click handler switches from asc to desc and vice-versa.
        for (var i = 0; i < this.columns.length; i++) {
            if (this.columns[i].dir && this.columns[i].dir.trim() !== '') {
                this.treeGridSorter.onHeaderClick(this.columns[i], null, i);
                break;
            }
        }

        //This will fix a bug when the TreeGrid Panel' has a toolbar defined, 
        //the Panel's body it the same size as the root node div and therefore it hides the scrollbar. Make room for it.
        if (this.tbar) {
            var tree = this.getTreeEl();
            if (tree) {
                tree.setHeight(tree.getHeight() + 23);
            }
        }
    },

    /**
    * Handle a header menu click, if it's a change sort option that was click call the 'onHeaderClick' handler as if
    * the corresponding header had been clicked directly.
    * @name Ext.ux.tree.X3TreeGrid#onHeaderMenuClick
    */
    onHeaderMenuClick: function (c, id, index) {
        if (id === 'asc' || id === 'desc') {
            this.onHeaderClick(c, null, index);
        }
    },

    /**
    * Handle a header click. The TreeGridSorter class also listen to this same event and is the one responsible to do
    * the actual sort. This handler is mostly responisble to fire our custom event 'sortHaschanged'
    * @name Ext.ux.tree.X3TreeGrid#onHeaderClick
    */
    onHeaderClick: function (c, el, i) {
        if (c && !this.headersDisabled) {

            // handle selectall checkbox
            if (this.checkboxes && i == 0)
                return this.chbSelectAll();

            // do not order by non-sortable columns
            if (c.sortable === false) return;

            var me = {};
            me.property = c.dataIndex;
            me.dir = (c.dir === 'desc' ? 'asc' : 'desc');
            this.fireEvent('sortHasChanged', this, me);
        }
    },

    chbSelectAll: function () {
        if (!this.checkboxes && this.columns.length < 1)
            return;
        var i = 0, selCls = 'x-tree-selected',
            hd = this.innerHd.select('td').item(i),
            isChecked = hd.hasClass(selCls),
            sm = this.getSelectionModel();
        if (isChecked) {
            sm.clearSelections();
            hd.removeClass(selCls);
            var btnDHCPGraph = Ext.getCmp('btnDHCPGraph');
            if (typeof (btnDHCPGraph) != "undefined") {
                btnDHCPGraph.enable();
            }
        } else {
            // [oh] select only DHCP servers
            //var nodes = this.getRootNode().childNodes;

            // [oh] traverse to select all "loaded" nodes
            var nodes = []; this.getRootNode().cascade(function (n) { if (!n.isRoot) nodes.push(n); });

            // [oh] h4x - select in reverse mode, that selection will not scroll down the grid :)
            sm.clearSelections();
            $.each(nodes.reverse(), function () { sm.select(this, null, true); });
            hd.addClass(selCls);
        }
    },

    /**
    * Subclas of the Ext.ux.tree.TreeGrid updateColumnWidths method so that we can fire a custom
    * 'columnWidthsUpdated' event.
    * @name Ext.ux.tree.X3TreeGrid#updateColumnWidths
    */
    updateColumnWidths: function () {
        //Call parent class code
        Ext.ux.tree.X3TreeGrid.superclass.updateColumnWidths.call(this);
        //Fire custom event
        this.fireEvent('columnWidthsUpdated', this);
    },

    forceRedrawElement: function (force) {
        var el = $(this);
        el.addClass('fr').removeClass('fr');
        if (force) { el.hide().show(); }
    },

    /**
    * Subclass of the Ext.ux.tree.TreeGrid syncHeaderScroll method so that we can fix a bug where the header
    * doesn't scroll horizontally correctly under IE 6 and 7.
    * @name Ext.ux.tree.X3TreeGrid#syncHeaderScroll
    * @private
    */
    syncHeaderScroll: function () {
        //Call parent class code
        Ext.ux.tree.X3TreeGrid.superclass.syncHeaderScroll.call(this);

        //Force the browser to redraw by 'modifying' the Dom...
        // NOTE: For IE9 in compatibility view of IE7 .. force with hide/show operations.
        this.forceRedrawElement.apply(this.innerHd.dom, [Ext.isIE7]);
    },

    /**
    * function called by the treegrid getState function. This normally should have been handled by
    * an implementation of the ExtJS State Manager that would use Ajax hits to save/get the state
    * in a userpref, but it was simple enough to just have a save method and handle the state upon load.
    * @name Ext.ux.tree.X3TreeGrid#saveTreeGridState
    * @private
    */
    saveTreeGridState: function saveTreeGridState(state) {

        var save = false;
        var stateJson = Ext.encode(state);

        //Verify if the state has changed since the last time we saved it
        if (this.treeGridSavedState) {
            if (this.treeGridSavedState !== stateJson) {
                save = true;
            }
        } else {
            save = true;
        }

        //The state has changed, save it on the server
        if (save && this.stateSaveUrl) {
            Ext.Ajax.request({
                url: this.stateSaveUrl,
                method: "POST",
                callback: Ext.emptyFn,
                jsonData: stateJson
            });
            this.treeGridSavedState = stateJson;
        }
    },

    /**
    * Keeps a copy of the state in JSON as it was the alst time it was saved so that it can be compared against before making
    * a callback.
    * @name Ext.ux.tree.X3TreeGrid#treeGridSavedState
    * @private
    * @type object
    */
    treeGridSavedState: null,

    /**
    * Retrieve the 'state' which in this implementation consists of the column width and the sord order.
    * @name Ext.ux.tree.X3TreeGrid#getState
    */
    getState: function getState() {
        var state = {};
        state.columns = {};
        for (var i = 0; i < this.columns.length; i++) {
            state.columns[this.columns[i].dataIndex] = this.columns[i].width;
        }
        state.sort = {
            field: this.treeGridSorter.property,
            dir: this.treeGridSorter.dir
        };
        this.saveTreeGridState(state);
        return state;
    },
    onClickToDrilldown: function (el, e) {
        var t = e.getTarget(); if (t == null) return;
        if (t.tagName == 'A' && t.href && /sw-grid-customlink/.test(t.className)) {
            e.preventDefault();
            window.open(t.href);
        }
    },

    setRootNode: function (node) {
        if (!this.checkboxes) {
            return Ext.ux.tree.X3TreeGrid.superclass.setRootNode.call(this, node);
        }
        node.attributes.uiProvider = Ext.ux.tree.CheckBoxTreeGridRootNodeUI;
        node = Ext.ux.tree.TreeGrid.superclass.setRootNode.call(this, node);
        if (this.innerCt) {
            this.colgroupTpl.insertFirst(this.innerCt, { columns: this.columns });
        }
        return node;
    },
    registerNode: function (n) {
        if (!this.checkboxes) {
            return Ext.ux.tree.X3TreeGrid.superclass.registerNode.call(this, n);
        }
        Ext.ux.tree.TreeGrid.superclass.registerNode.call(this, n);
        if (!n.uiProvider && !n.isRoot && !n.ui.isTreeGridNodeUI) {
            n.ui = new Ext.ux.tree.CheckBoxTreeNodeUI(n);
        }
    },
    handleHdOver: function (e, t) {
        if (this.checkboxes) {
            var hd = e.getTarget('.x-treegrid-hd');
            if (hd && !this.headersDisabled) {
                index = this.findHeaderIndex(hd);
                if (index == 0) return;
            }
        }
        return Ext.ux.tree.X3TreeGrid.superclass.handleHdOver.call(this, e, t);
    },
    handleHdOut: function (e, t) {
        return Ext.ux.tree.X3TreeGrid.superclass.handleHdOut.call(this, e, t);
    }
});

Ext.ns('Ext.ux.tree');
Ext.ux.tree.CheckBoxTreeNodeUI = Ext.extend(Ext.ux.tree.TreeGridNodeUI, {
    isTreeGridNodeUI: true,
    renderElements: function (n, a, targetNode, bulkRender) {
        var offset = 1, 
            t = n.getOwnerTree(),
            cols = t.columns,
            c = cols[offset],
            i, buf, len;
        this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

        buf = [
             '<tbody class="x-tree-node">',
                '<tr ext:tree-node-id="', n.id, '" class="x-tree-node-el x-tree-node-leaf ', a.cls, '">',
                    '<td class="x-treegrid-col x-treegrid-td-checker">',
                       '<div class="x-treegrid-row-checker">&#160;</div>',
                    '</td>',
                    '<td class="x-treegrid-col"><div class="x-treegrid-nowrap">',
                        '<span class="x-tree-node-indent">', this.indentMarkup, "</span>",
                        '<img src="', this.emptyIcon, '" class="x-tree-ec-icon x-tree-elbow" />',
                        '<img src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon', (a.icon ? " x-tree-node-inline-icon" : ""), (a.iconCls ? " " + a.iconCls : ""), '" unselectable="on" />',
                        '<a hidefocus="on" class="x-tree-node-anchor" href="', a.href ? a.href : '#', '" tabIndex="1" ',
                            a.hrefTarget ? ' target="' + a.hrefTarget + '"' : '', '>',
                        '<span unselectable="on">', (c.tpl ? c.tpl.apply(a) : a[c.dataIndex] || c.text), '</span></a>',
                    '</div></td>'
        ];

        for (i = offset + 1, len = cols.length; i < len; i++) {
            c = cols[i];
            buf.push(
                    '<td class="x-treegrid-col ', (c.cls ? c.cls : ''), '">',
                        '<div unselectable="on" class="x-treegrid-text x-treegrid-nowrap"', (c.align ? ' style="text-align: ' + c.align + ';"' : ''), '>',
                            (c.tpl ? c.tpl.apply(a) : a[c.dataIndex]),
                        '</div>',
                    '</td>'
            );
        }

        buf.push(
            '</tr><tr class="x-tree-node-ct"><td colspan="', cols.length, '">',
            '<table class="x-treegrid-node-ct-table" cellpadding="0" cellspacing="0" style="table-layout: fixed; display: none; width: ', t.innerCt.getWidth(), 'px;"><colgroup>'
        );
        buf.push('<col style="width: 19px;" />');
        for (i = offset, len = cols.length; i < len; i++) {
            buf.push('<col style="width: ', (cols[i].hidden ? 0 : cols[i].width), 'px;" />');
        }
        buf.push('</colgroup></table></td></tr></tbody>');

        if (bulkRender !== true && n.nextSibling && n.nextSibling.ui.getEl()) {
            this.wrap = Ext.DomHelper.insertHtml("beforeBegin", n.nextSibling.ui.getEl(), buf.join(''));
        } else {
            this.wrap = Ext.DomHelper.insertHtml("beforeEnd", targetNode, buf.join(''));
        }

        this.elNode = this.wrap.childNodes[0];
        this.ctNode = this.wrap.childNodes[1].firstChild.firstChild;
        var cs = this.elNode.childNodes[offset].firstChild.childNodes;
        this.indentNode = cs[0];
        this.ecNode = cs[1];
        this.iconNode = cs[2];
        this.anchor = cs[3];
        this.textNode = cs[3].firstChild;
    },
    animExpand: function (cb) {
        this.ctNode.style.display = "";
        Ext.ux.tree.TreeGridNodeUI.superclass.animExpand.call(this, cb);
    }
});

Ext.ux.tree.CheckBoxTreeGridRootNodeUI = Ext.extend(Ext.ux.tree.CheckBoxTreeNodeUI, {
    isTreeGridNodeUI: true,
    render: function () {
        if (!this.rendered) {
            this.wrap = this.ctNode = this.node.ownerTree.innerCt.dom;
            this.node.expanded = true;
        }

        if (Ext.isWebKit) {
            // weird table-layout: fixed issue in webkit
            var ct = this.ctNode;
            ct.style.tableLayout = null;
            (function () {
                ct.style.tableLayout = 'fixed';
            }).defer(1);
        }
    },
    destroy: function () {
        if (this.elNode) {
            Ext.dd.Registry.unregister(this.elNode.id);
        }
        delete this.node;
    },
    collapse: Ext.emptyFn,
    expand: Ext.emptyFn
});

Ext.ux.plugins.GridSelectionPlugin = function (config) {
    Ext.apply(this, config);
};
Ext.extend(Ext.ux.plugins.GridSelectionPlugin, Ext.util.Observable, {
    init: function (grid) {
        var fn = this.fn || function (sm) { };
        if (fn) grid.getSelectionModel().on('selectionchange', fn);
    }
});


Ext.ux.plugins.GridSelectAll = function (config) {
    Ext.apply(this, config);
};
Ext.extend(Ext.ux.plugins.GridSelectAll, Ext.util.Observable, {
    selectTxtTpl: "@{R=IPAM.Strings;K=IPAMWEBJS_OH1_9;E=js}",
    selectBtnTpl: "@{R=IPAM.Strings;K=IPAMWEBJS_OH1_10;E=js}",
    selectAllTpl: "@{R=IPAM.Strings;K=IPAMWEBJS_OH1_11;E=js}",
    allselected: false,
    getAllSelectedFn: function () {
        var that = this; return function () { return (that.allselected === true); }
    },
    init: function (grid) {
        this.grid = grid;

        grid.getAllSelected = this.getAllSelectedFn();
        grid.onRender = grid.onRender.createSequence(this.onRender, this);

        var sm = grid.getSelectionModel();
        sm.on('selectionchange', this.toggle, this);
    },
    onRender: function () {
        if (this.tb) return;

        this.tb = $('<div class="x-panel-header all-selector"><span/> <a href="#"/></div>');
        this.text = this.tb.children('span');
        this.button = this.tb.children('a');

        var that = this;
        this.button.on('click', function () {
            that.updateTotalCount(that.text, that.selectAllTpl);
            that.button.hide();
            that.allselected = true;
        });

        var hd = Ext.getDom(this.grid.getView().mainHd);
        this.tb.prependTo(hd).hide();
    },
    updateSelectedCount: function (el, tpl) {
        if (!this.grid || !this.text) return;
        var count = this.grid.getSelectionModel().getCount();
        var text = String.format(tpl || this.selectTxtTpl, count || '');
        if (el) el.html(text);
        return text;
    },
    updateTotalCount: function (el, tpl) {
        if (!this.grid || !this.button) return;
        var count = this.grid.store.totalLength;
        var text = String.format(tpl || this.selectBtnTpl, count || '');
        if (el) el.html(text);
        return text;
    },
    toggleTb: function (toShow) {
        if (toShow == this.toShow) return false;
        if (toShow) this.tb.show(); else this.tb.hide();

        this.toShow = toShow;
        return toShow == true;
    },
    toggle: function (sm) {
        var p = this; if (!p.tb) return;

        // un-select all every time, that selection is changed
        this.allselected = false;

        var count = this.grid.store.getCount();
        var total = this.grid.store.totalLength;
        var selected = this.grid.getSelectionModel().getCount();

        var toShow = (selected == count) && (selected < total);
        if (this.toggleTb(toShow)) {
            this.updateSelectedCount(this.text);
            this.updateTotalCount(this.button);
            this.button.show();
        }
    }
});


Ext.namespace('Ext.ux.plugins');
Ext.ux.plugins.DhcpScopesTTip = function (config) { Ext.apply(this, config); };
Ext.ux.plugins.DhcpScopesTTip.TTipRender = function (grid) {
    if (grid.scopeTTip) return;

    grid.scopeTTip = new Ext.ToolTip({
        target: grid.el,                    // the overall target element.
        baseCls: 'custom-x-tip',            // custom css style
        dismissDelay: 0,                    // disable automatic hiding
        showDelay: 0,                       // delay before the tooltip displays
        delegate: 'li.scope-list',          // each scope list item causes its own seperate show and hide.
        trackMouse: true,                  // moving within the row should not hide the tip.
        autoWidth: true,
        renderTo: Ext.getBody(),            // render immediately so that tip.body can be referenced prior to the first show.
        listeners: {                        // change content dynamically depending on which element triggered the show.
            beforeshow: function updateTipBody(tip) {
                var tEl = tip.triggerElement,
                    ttip = $(tEl).children('.scope-ttip');
                if (ttip && ttip.html)
                 tip.update(ttip.html());
            }
        }
    });
};

Ext.extend(Ext.ux.plugins.DhcpScopesTTip, Ext.util.Observable, {
    init: function (grid) { grid.on('render', Ext.ux.plugins.DhcpScopesTTip.TTipRender); }
});

/* Drop Down Grid */
Ext.ns('Ext.ux.form');
Ext.ux.form.DropDownGrid = Ext.extend(Ext.form.TriggerField, {
    hiddenId: undefined,
    hiddenName: undefined,
    hiddenValue: undefined,
    minWidth: 100,
    contentHeight: 350,
    contentSupplyFn: function () { for (var i in this.items) return this.items[i]; },

    /* init content (like hook events) */
    contentInitFn: function (c) { },
    /* returns submit data from value */
    findValueData: function (v) { return v; },
    /* returns display text from value */
    findValueText: function (v) { return v; },

    // internal
    content: null,
    innerCnt: null,
    getContent: function () { return this.innerCnt; },
    defaultAutoCreate: { tag: "input", type: "text", size: "24", autocomplete: "off" },

    initComponent: function () {
        Ext.ux.form.DropDownGrid.superclass.initComponent.call(this);
        this.addEvents('select');
    },
    initEvents: function () {
        Ext.ux.form.DropDownGrid.superclass.initEvents.call(this);
        this.keyNav = new Ext.KeyNav(this.el, {
            "down": function (e) { this.onTriggerClick(); },
            "esc": function (e) { this.collapse(); },
            scope: this,
            forceKeyDown: true
        });
    },

    initValue: function () {
        Ext.ux.form.DropDownGrid.superclass.initValue.call(this);
        if (this.hiddenField) {
            var value = Ext.isDefined(this.hiddenValue) ? this.hiddenValue : this.value;
            this.hiddenField.value = Ext.value(this.findValueData(value), '');
        }
    },
    clearValue: function (emptyText) {
        if (Ext.isString(emptyText)) this.emptyText = emptyText;
        if (this.hiddenField) this.hiddenField.value = '';
        this.setRawValue('');
        this.lastSelectionText = '';
        this.applyEmptyText();
        this.value = undefined;
    },
    getValue: function () {
        return Ext.value(this.hiddenField ? this.hiddenField.value : this.value, '');
    },
    setValue: function (v) {
        var value = Ext.value(this.findValueData(v), ''),
            text = Ext.value(this.findValueText(v), ''); ;
        this.lastSelectionText = text;
        if (this.hiddenField) this.hiddenField.value = value;
        Ext.ux.form.DropDownGrid.superclass.setValue.call(this, text);
        this.value = v;
        return this;
    },
    onRender: function (ct, position) {
        if (this.hiddenName && !Ext.isDefined(this.submitValue)) this.submitValue = false;
        Ext.ux.form.DropDownGrid.superclass.onRender.call(this, ct, position);
        if (this.hiddenName) {
            var opt = { tag: 'input', type: 'hidden', name: this.hiddenName, id: (this.hiddenId || Ext.id()) };
            this.hiddenField = this.el.insertSibling(opt, 'before', true);
        }
        if (Ext.isGecko) this.el.dom.setAttribute('autocomplete', 'off');
        this.initContent();
    },
    onDestroy: function () {
        Ext.destroy(this.content, this.innerCnt, this.keyNav);
        Ext.destroyMembers(this, 'hiddenField');
        Ext.ux.form.DropDownGrid.superclass.onDestroy.call(this);
    },
    onEnable: function () {
        Ext.ux.form.DropDownGrid.superclass.onEnable.apply(this, arguments);
        if (this.hiddenField) this.hiddenField.disabled = false;
    },
    onDisable: function () {
        Ext.ux.form.DropDownGrid.superclass.onDisable.apply(this, arguments);
        if (this.hiddenField) this.hiddenField.disabled = true;
        this.collapse();
    },
    onResize: function (w, h) {
        Ext.ux.form.DropDownGrid.superclass.onResize.apply(this, arguments);
        if (!isNaN(w) && this.isVisible() && this.content)
            this.doResize(w); else this.bufferSize = w;
    },
    doResize: function (width) {
        if (isNaN(width)) width = this.getWidth() || this.wrap.getWidth();
        var w = Math.max(width, this.minWidth),
            h = this.contentHeight;
        this.content.setSize(isNaN(w) ? width : w, h);
        this.innerContent.setSize(
            this.content.getWidth() - this.content.getFrameWidth('lr'),
            this.content.getHeight()
        );
        if (this.innerCnt) this.innerCnt.setWidth(this.innerContent.getWidth() - this.innerContent.getFrameWidth('lr'));
    },
    isExpanded: function () {
        return this.content && this.content.isVisible();
    },
    isTargetInContent: function (target) {
        return this.content.contains(target) || this.isTargetInContentChild(target);
    },
    isTargetInContentChild: function (target) {
        return false;
    },
    validateBlur: function (e) {
        // allow blur when not expanded, or click outside content
        var hasContent = !this.content || !this.content.isVisible();
        return hasContent || !this.isTargetInContent(e.target);
    },
    postBlur: function () {
        Ext.ux.form.DropDownGrid.superclass.postBlur.call(this);
        this.collapse();
    },
    onTriggerClick: function () {
        if (this.readOnly || this.disabled) return;
        if (this.isExpanded()) {
            this.collapse();
            this.el.focus();
        } else {
            this.onFocus({});
            this.el.focus();
            this.expand();
        }
    },
    getZIndex: function (contentParent) {
        var contentParent = contentParent || Ext.getDom(document.body || Ext.getBody());
        var zindex = parseInt(Ext.fly(contentParent).getStyle('z-index'), 10);
        return (zindex || 12000) + 5;
    },
    initContent: function () {
        var contentParent = Ext.getDom(document.body || Ext.getBody());

        this.content = new Ext.Layer({
            parentEl: contentParent,
            shadow: 'sides',
            cls: 'x-combo-list',
            constrain: false
        });
        this.content.swallowEvent('mousewheel');
        this.innerContent = this.content.createChild({ cls: 'x-combo-list-inner' });

        var cnt = this.contentSupplyFn.apply(this);
        if (cnt) {
            this.innerConfig = Ext.apply({ applyTo: this.innerContent, ownerCt: this.innerContent }, cnt);
            this.innerCnt = Ext.create(this.innerConfig, this.innerConfig.xtype || 'grid');
            this.contentInitFn.apply(this, [this.innerCnt]);
        }

        this.doResize();
    },
    expand: function () {
        if (this.isExpanded() || !this.hasFocus) return;

        if (this.bufferSize) {
            this.doResize(this.bufferSize);
            delete this.bufferSize;
        } else {
            this.doResize();
        }

        this.content.alignTo.apply(this.content, [this.el].concat('tl-bl?'));
        this.content.setZIndex(this.getZIndex());
        this.content.show();
        if (Ext.isGecko2) {
            this.innerContent.setOverflow('auto');
        }
    },
    collapse: function () {
        if (!this.isExpanded()) return;
        this.content.hide();
    }
});
