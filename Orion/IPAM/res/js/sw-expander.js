﻿(function ($) {

    $.fn.expander = function (options) {
        var opts = $.extend({}, $.fn.expander.defaults, options);
        var states = { exp: 'exp-expanded', col: 'exp-collapsed' };

        var deferFn = function (delay, fn) {
            if (!delay || isNaN(delay)) fn.call(this);
            else window.setTimeout(fn, delay);
        };

        var SetState = function (el, hdr) {
            var isExp = hdr.hasClass(states.exp);
            if (isExp) {
                hdr.removeClass(states.exp);
                hdr.addClass(states.col);
                el.hide();
            } else {
                hdr.removeClass(states.col);
                hdr.addClass(states.exp);
                el.show();
            }
        };

        return this.each(function () {
            var el = $(this);
            var title = el.attr('header');
            var state = el.attr('collapsed') ? states.exp : states.col;

            var o = $.meta ? $.extend({}, opts, el.data()) : opts;
            if (o.header) { title = o.header.html(); }

            // create header
            var text = $('<span/>').html(title ? title : '&nbsp;');
            var header = $('<div class="exp-hdr" />').addClass(state).append(text);
            header.bind('click', function () { SetState(el, header); });
            el.before(header);

            deferFn(o.delay, function () { SetState(el, header); });
        });
    };

})(jQuery);

