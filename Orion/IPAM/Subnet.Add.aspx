<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.SubnetAdd"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_61 %>" CodeFile="Subnet.Add.aspx.cs" validateRequest="false" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NeighborScanSettings.ascx" TagName="NeighborScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TransientPeriodSettings.ascx" TagName="TransientPeriodSettings" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"  ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation=true OnMsg="MsgSave" runat=server />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server" >
#subnettoolarge { padding: 2px 2px 2px 0px; }
.sw-form-disabled { color: #777; }
.sw-form-cols-normal td.sw-form-col-label { width: 180px; }
.sw-form-cols-normal td.sw-form-col-control { width: 300px; }
div.sw-form-item .sw-pairednum { left: 78px; } 
.sw-form-item .sw-withlblnum { width: 119px; }
.sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation=jsPostInit runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){
    try { $SW.SubnetEditorInit($nsId); }catch(err){}
    $('.expander').expander();
});
</IPAMmaster:JsBlock>

<% if( SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false ){ %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>

<IPAMui:ValidationIcons runat="server" />

<div id=formbox>
     <%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
    <input type="hidden" id="isDemoMode" />
	<%}%>
    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= IPAMWebContent.IPAMWEBDATA_TM0_40 %></div>

    <div class="group-box white-bg"><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_TM0_41 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="DisplayNameRequire" runat="server"
                  ControlToValidate="txtDisplayName"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_42 %>">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_TM0_43 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtNetworkAddress" CssClass="x-form-text x-form-field sw-withlblnum" runat="server" />
                <span class=sw-pairedlbl><%= IPAMWebContent.IPAMWEBDATA_TM0_44 %></span>
                <asp:TextBox ID="txtCidr" CssClass="x-form-text x-form-field sw-pairednum" Text="24" runat="server" />
            </div></td>
            <td><IPAMmaster:JsBlock Orientation=inline runat=server>document.getElementById($SW.nsGet($nsId,'txtNetworkAddress')).focus();
            </IPAMmaster:JsBlock><asp:RequiredFieldValidator ID="NetworkAddressRequire" runat="server"
                  ControlToValidate="txtNetworkAddress"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_45 %>" />
                <asp:CustomValidator ID="NetworkAddressIPv4" runat="server"
                  ControlToValidate="txtNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_46 %>"
              /><asp:CustomValidator
                  ID="NetworkAddressSubnet"
                  runat="server"
                  ControlToValidate="txtNetworkAddress"
                  OtherControl="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnet"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_47 %>"
              /><asp:CustomValidator
                  ID="SubnetWithinParent"
                  runat="server"
                  ControlToValidate="txtNetworkAddress"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4subnetwithinparent"
                  OtherControl="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_48 %>"
              /><asp:RequiredFieldValidator ID="CidrRequire" runat="server"
                  ControlToValidate="txtCidr"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_49 %>"
             /><asp:CompareValidator ID="CidrAbove" runat="server"
                  ControlToValidate="txtCidr"
                  Operator="GreaterThan"
                  ValueToCompare="0"
                  Type="Integer"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_51 %>"
              /><asp:CompareValidator ID="CidrBelow" runat="server"
                  ControlToValidate="txtCidr"
                  Operator="LessThanEqual"
                  ValueToCompare="32"
                  Type="Integer"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_52 %>"
              /><asp:CustomValidator ID="CidrIsv4" runat="server"
                  ControlToValidate="txtCidr"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4cidr"
                  OtherControl="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_51 %>"
              /><asp:CustomValidator ID="AsyncAddressValidator" runat="server" Display="Dynamic"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4Async"
                  AddressControl="txtNetworkAddress" CidrControl="txtCidr"
                  ErrorMessage="{msg}"
              /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_TM0_53 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtNetworkMask" CssClass="x-form-text x-form-field" ReadOnly=true Text="255.255.255.0" runat="server" />
            </div></td>
            <td><asp:RequiredFieldValidator ID="NetworkMaskRequire" runat="server"
                  ControlToValidate="txtNetworkMask"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_54 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4"
                  Display="Dynamic"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_55 %>"
              /><asp:CustomValidator ID="NetworkMaskIPv4Mask" runat="server"
                  ControlToValidate="txtNetworkMask"
                  ClientValidationFunction="$SW.Valid.Fns.ipv4mask"
                  OtherValidator="NetworkAddressSubnet"
                  Display=Dynamic
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_56 %>"
              /></td>
        </tr>
        <tr>
            <td></td>
            <td colspan=2><div id="ipcount" class="sw-form-item sw-form-clue"><%= IPAMWebContent.IPAMWEBDATA_TM0_57 %></div></td>
        </tr>
        <tr id="parentifo" runat=server> 
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_TM0_58 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
             <asp:TextBox ID="txtParentAddress" Enabled=false CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"><div class="sw-form-item">
                <asp:CheckBox ID="cbAddIPAddresses" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_62 %>" checked="true" runat="server"/>
                <div class="sw-form-clue" id='subnettoolarge' style="display: none;"><%= IPAMWebContent.IPAMWEBDATA_TM0_59 %></div>
                <div class="sw-form-clue" id='licensenoroom' style="display: none;"><%= IPAMWebContent.IPAMWEBDATA_TM0_60 %></div>
            </div></td>
        </tr>
        <tr>
            <td></td>
            <td><div class="sw-form-item">
                &nbsp;
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_AK1_147%>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>      
        <tr>
            <td colspan=2><div class="sw-form-item sw-form-clue">
              <%= IPAMWebContent.IPAMWEBDATA_VB1_184%>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_VB1_185%>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtVLAN" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>      
        <tr>
            <td><div class=sw-field-label>
                <%= IPAMWebContent.IPAMWEBDATA_VB1_186 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtLocation" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        </table></div>
        
    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
    <div class="group-box white-bg" style="margin-bottom: 0px">
        <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
    </div>
</div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= IPAMWebContent.IPAMWEBDATA_TM0_33 %>">

    <div id="IcmpScanInfo" class="hintbox" runat="server">
      <div class="inner info">
        <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_70, 
                          "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>") %>
      </div>
    </div>

    <div><!-- ie6 --></div>

  <div class="group-box blue-bg">

    <div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>        
        <tr>
            <td></td>
            <td><div class="sw-form-item">
                <asp:CheckBox ID="cbDisableAutoScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_34 %>" runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div><!-- ie6 --></div>

    <div id=ScanSettings><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td></td>
            <td><div class="sw-form-item">
                <asp:CheckBox ID="cbRetainUserData" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_35 %>" runat="server"/>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
              <%= IPAMWebContent.IPAMWEBDATA_TM0_36 %>
            </div></td>
            <td>
                <ipam:TimeSpanEditor Width="168" id="ScanInterval" ValidIf="cbDisableAutoScanning" ActiveWhenChecked="False" TextName="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_19 %>" runat="server" MinValue="00:10:00" MaxValue="7.00:00:00" />
            </td>
        </tr>
    </table></div>

  </div>

</div>

    <div><!-- ie6 --></div>

 <div class="expander" header="<%= String.Format("{0} &lt;a class=&#34;sw-form-helpTip&#34; href=&#34;#&#34; onclick=&#34;window.open('{1}', '_blank');&#34;&gt;{2}&lt;/a&gt;",
                                                 IPAMWebContent.IPAMWEBDATA_TM0_76, SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl("OrionIPAMPHNeighborScanning"), IPAMWebContent.IPAMWEBDATA_VB1_29) %>">

    <div id="NeighborScanInfo" class="hintbox" runat="server">
        <div class="inner info">
          <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_74, 
                            "<a href=\"Admin/Admin.ScanSettings.aspx\" target=\"_blank\">", "</a>")%>
        </div>
    </div>

    <div><!-- ie6 --></div>

    <div class="group-box blue-bg">
        <IPAM:NeighborScanSettings id="NeighborScan" runat="server" 
            FnSupplyServerAddress="function(fnContinue){ fnContinue('Subnet.Add: SNMP'); }" />
    </div>

</div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= IPAMWebContent.IPAMWEBDATA_VB1_10 %>">
    <div class="group-box blue-bg">
        <IPAM:TransientPeriodSettings id="TransientPeriod" runat="server" />
    </div>
</div>

    <div><!-- ie6 --></div>
    
    <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSave" LocalizedText="Save" OnClick="ClickSave" DisplayType="Primary" CausesValidation="true" />
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClick="ClickCancel" DisplayType="Secondary" CausesValidation="false" />
    </div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

</asp:Content>
