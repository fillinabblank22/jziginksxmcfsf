using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Layout;

public partial class SubnetBulkAdd : CommonPageServices
{
	private PageSubnetBulkAdd mgr;

    public SubnetBulkAdd()
    {
        mgr = new PageSubnetBulkAdd(this, "AddSubnetsWizard", "SubnetsProperties");
    }

	public GridBox Grid
	{
		get { return grid; }
	}

	public void Done_Click(object src, EventArgs args)
	{
		mgr.CompleteBtnClick();
	}

	public void Parse_Click(object src, EventArgs args)
	{
		mgr.ParseBtnClick();
	}
}
