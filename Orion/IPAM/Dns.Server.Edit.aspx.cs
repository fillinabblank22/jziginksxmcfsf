﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.IPAM.Client;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsServerEdit : CommonPageServices
    {
        private PageDnsServerEditor Editor { get; set; }

        #region Constructors

        public DnsServerEdit()
        {
            this.Editor = new PageDnsServerEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.Editor.InitPage();
            //this.AccessCheck.GroupIds.Add(this.Editor.SubnetId);
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            DnsServer server = this.Editor.RetrieveServerObject();
            if (server == null)
                return;

            // set DNS server's node is for 'Test' credentials button
            CredSelector.FnSupplyServerAddress = string.Format(
                "function(fnContinue){{ fnContinue({0}); }}",
                server.NodeId);

            // postbacks will not update controls values
            if (!Page.IsPostBack)
            {
                InitControls(server);
            }
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls(DnsServer server)
        {
            // init custom attributes
            CustomPropertyEdit.RetrieveCustomProperties(server, new List<int>(){ server.GroupId });

            if (this.txtServerType != null)
            {
                this.txtServerType.Text = GetDnsServerVendor(server.ServerType);
            }
            if (this.txtIpAddress != null)
            {
                this.txtIpAddress.Text = server.Address;
            }
            if (this.txtDescription != null)
            {
                this.txtDescription.Text = WebSecurityHelper.SanitizeHtml(server.Comments);
            }
            if (this.txtVLAN != null)
            {
                this.txtVLAN.Text = WebSecurityHelper.SanitizeHtml(server.VLAN);
            }
            if (this.txtLocation != null)
            {
                this.txtLocation.Text = WebSecurityHelper.SanitizeHtml(server.Location);
            }
            if (CredSelector != null)
            {
                // set credential (existing, or new one)
                CredSelector.TrySetCredentials(server.ServerType, server.CredentialId);
            }

            if (this.cbEnableScanning != null)
            {
                cbEnableScanning.Checked = server.DisableAutoScanning.HasValue ? !server.DisableAutoScanning.Value : true;
            }

            if (this.DnsScanInterval != null && server.ScanInterval != null)
                DnsScanInterval.Value = TimeSpan.FromMinutes(server.ScanInterval.Value);

            if (this.cbIncrementalZoneTransfer != null)
                cbIncrementalZoneTransfer.Checked = server.IncrementalZoneTransfer;

            if (this.areaIncrementalZoneTransfer != null)
            {
                var style = (server.ServerType == DnsServerType.Windows) ? "block" : "none";
                this.areaIncrementalZoneTransfer.Style["display"] = style;
            }
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            Save();
        }

        private void Save()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            DnsServer server = this.Editor.RetrieveServerObject();
            if (server != null)
            {
                DnsServer updatedServer = UpdateObjectProperties(server);
                this.Editor.Store(updatedServer);
            }
        }

        private DnsServer UpdateObjectProperties(DnsServer server)
        {
            if (CredSelector != null)
            {
                // server type by selected credentials type
                server.ServerType = CredSelector.DnsServerType;
                // Credentials
                server.CredentialId = StoreCredentials();
            }

            if (this.txtDescription != null)
            {
                server.Comments = this.txtDescription.Text;
            }
            if (this.txtVLAN != null)
            {
                server.VLAN = this.txtVLAN.Text;
            }
            if (this.txtLocation != null)
            {
                server.Location = this.txtLocation.Text;
            }

            // go through all the child controls of the repeater to lift out any custom properties.
            // the id for custom field is 'PropertyText'
            CustomPropertyEdit.GetChanges(server);

            if (cbEnableScanning != null)
                server.DisableAutoScanning = !cbEnableScanning.Checked;

            if (server.DisableAutoScanning.HasValue && !server.DisableAutoScanning.Value)
            {
                if (this.DnsScanInterval != null)
                    server.ScanInterval = Convert.ToInt32(Math.Round(DnsScanInterval.Value.TotalMinutes));
                if (cbIncrementalZoneTransfer != null)
                    server.IncrementalZoneTransfer = cbIncrementalZoneTransfer.Checked;
                if (server.ServerType != DnsServerType.Windows)
                    server.IncrementalZoneTransfer = false;
            }
            else
            {
                server.ScanInterval = null;
                server.IncrementalZoneTransfer = false;
            }

            return server;
        }

        private Nullable<int> StoreCredentials()
        {
            Nullable<int> credentialId = CredSelector.CredId;
            if (credentialId == null)
            {
                ICredentials credentials = CredSelector.GetCredentials();
                if (credentials is WindowsCredential && credentials.ID == -1)
                    return -1;

                SwisOpCtx createNewCredentialSwisOpCtx = new SwisOpCtx(
                    "Create New Credential",
                    delegate(IpamClientProxy proxy, object param)
                    {
                        credentialId = proxy.AppProxy.Credentials.Create(credentials);
                        return null;
                    }
                );
                SwisConnector.RunSwisOps(new SwisOpCtx[] { createNewCredentialSwisOpCtx });
            }
            return credentialId;
        }

        private string GetDnsServerVendor(DnsServerType dnsServerType)
        {
            // Get the vendor name by server type.
            switch (dnsServerType)
            {
                case DnsServerType.Windows:
                    return "Windows";

                case DnsServerType.Bind:
                    return "BIND";

                case DnsServerType.Infoblox:
                    return "Infoblox";

                default:
                    return string.Empty;
            }
        }

        #endregion // Methods
    }
}