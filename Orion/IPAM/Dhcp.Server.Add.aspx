<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Dhcp.Server.Add.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpServerAdd"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_147%>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NodeSelector.ascx" TagName="NodeSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DHCPCredentialsSelector.ascx" TagName="DHCPCredentialsSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/VrfGroupName.ascx" TagName="VrfGroupName" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDHCPServerAdd" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
#breadcrumb td div { font-size: 100%; }
#breadcrumb a { line-height: 25px; }
#breadcrumb { margin-top: 0px; }
p { margin-top: 0; margin-bottom: 10px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
.warningbox-inside .inner a { text-decoration: underline; }
.warningbox-inside { border: 1px solid #d1d0c9; max-width: 870px; background: #f7f596 url(/orion/ipam/res/images/sw/bg.warning.float.gif) repeat-x scroll left bottom; }
.warningbox-inside .inner { background:transparent url(/orion/ipam/res/images/sw/icon.notification.32.gif) no-repeat scroll left top; }
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
div.sw-form-heading { font-size: small; margin-top: 8px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: medium; margin: 0; }
</IPAMmaster:CssBlock>

<asp:PlaceHolder runat="server" id="SelecedIds" />

<orion:Include File="breadcrumb.js" runat="server" />

<IPAMmaster:JsBlock Requires="ext-quicktips,sw-admin-cred.js,sw-helpserver" Orientation="jsInit" runat=server>
$SW.NodeSelectedDetails = null;
$SW.NodeVendorLUT = {
    "windows": "Windows",
    "cisco": "CISCO",
    "unknown": "Windows",
    toDhcpType: function(vendor){
        var type = $SW.NodeVendorLUT[vendor];
        return type || $SW.NodeVendorLUT["unknown"];
    }
};
$SW.NodeSelectionChange = function(me,nodes){
  nodes = nodes || [];
  if( nodes.constructor != [].constructor ) nodes = [nodes];

  var row = nodes[0] ? nodes[0].attributes : null;
  $SW.NodeSelectedDetails = row;
  row = row || {};
  var txt = $.trim(row.DNS)||$.trim(row.SysName)||$.trim(row.IPAddress)||'[ No Selected Node ]';
  $SW.ns$($nsId,'SelectedNodeName').text( txt );
  $SW.ns$($nsId,'SelectedNodeId')[0].value = ''+row.NodeId;

  if( $SW.Env.ExistingDhcpServers[''+row.NodeId] )
    $('#warningNodeAdded').show(250);
  else
    $('#warningNodeAdded').hide(250);
    
  // set credentials by node's vendor
  var vendor = row.Vendor ? row.Vendor.toLowerCase() : '';
  var type = $SW.NodeVendorLUT.toDhcpType(vendor);
  $SW.IPAM.DMultiCreds.select(type);
};

$SW.NodeSelectionOk = function(source,args){
 args.IsValid =  !($SW.ns$($nsId,'SelectedNodeId')[0].value == '' || $SW.Env.ExistingDhcpServers[$SW.ns$($nsId,'SelectedNodeId')[0].value]);
};

$SW.NodeSelectionCredentials = function(source,args){
 args.IsValid = !($SW.ns$($nsId,'SelectedNodeId')[0].value == '');
};

$(document).ready(function(){

    var transform = function(id,extid){

        var item = $('#'+id)[0];
        if( !item) return null;

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            id: extid,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };

  var syncscanarea = function(me){
    if( !me ) return;
    if( me.checked ) $('#subnetscanarea').show(250);
    else $('#subnetscanarea').hide(250);
  };

  syncscanarea( $SW.ns$($nsId, 'cbEnableSubnetScanning')[0] );

  transform( $SW.nsGet($nsId, 'ServerScanMultiplier'), 'ServerScanMultiplier' );
  transform( $SW.nsGet($nsId, 'SubnetScanMultiplier'), 'SubnetScanMultiplier' );

  $SW.ns$( $nsId, 'cbEnableSubnetScanning' ).click(function(e){
    syncscanarea(e.target || e.srcElement);
  });
});

$SW.IPAM.DMultiCreds.testState(false);

$SW.DhcpServerSave = function(me)
{
    if ( $SW.PageValidate(me) == false)
         return false;
    
    if ( $SW.IPAM.DMultiCreds.testState() )
        return true;
    else {
       me.name = me.id.replace(/_/g,"$");
       Ext.Msg.show({
               title: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_27;E=js}',
               msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_28;E=js}',
               buttons: Ext.Msg.YESNO,
               width: 400,
               icon: Ext.MessageBox.QUESTION,
               fn: function(btn){ 
                        if( btn == 'yes' ) { 
                            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(me.name, "", true, "", "", false, true));
                            return true;                    
                        } 
                    }
               });
               
      return false;
    }
}

$SW.PageValidate = function(me){
 var cred=!$SW.IPAM.DMultiCreds.validate(undefined, 'IsNewCredentialRequire', '');
 var scan=$SW.ns$($nsId,'cbEnableSubnetScanning')[0].checked;
 var vg;
 
 if( cred && scan ) vg = /^(SelNodeCredential|NewCred|SelNode|)$/;
 else if( cred ) vg = /^(SelNodeCredential|NewCred|SelNode|)$/;
 else if( scan ) vg = /^(SelNode|)$/;
 else vg = /^(SelNode|)$/;

 WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(me.name, "", true, vg, "", false, false));
 return !!window.Page_IsValid;
};
</IPAMmaster:JsBlock>

<IPAMui:ValidationIcons runat="server" />

<div style="margin-left: 10px; margin-right: 10px;">
  <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div style="margin-top: 10px;"><ul class="breadcrumb">
        <li class="bc_item" style="white-space: nowrap;">
            <a href="/api2/ipam/ui/dhcp" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_2%></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
<% } %>
        </ul></li>
    </ul></div>
<% } %>
        <h1 style="margin: 10px 0;"><%=Page.Title%></h1>
    </td></tr>
  </table>
      
  <div class="warningbox warningbox-inside">
    <div class=inner>
        <asp:LinkButton ID="addNodeLink" OnClick="OnAddOrionNodeClick" runat="server" Visible="False" />
        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_148, AddNodeLinkBeginTag(), "</a>", "<a href=\"/Orion/Discovery/Config/ \">", "</a>.<br />")%>
        <div style="padding-top: 4px;"><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_149%></b></div>
    </div>
  </div>

<div class="sw-form-heading"><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_150 %></h2></div>

<ipam:NodeSelector id="PickHost" MultipleSelection="false" OnSelectChange="$SW.NodeSelectionChange" runat="server" />
<asp:HiddenField runat="server" id="SelectedNodeId" Value="" /><asp:CustomValidator 
    ID="RequireSelectedNode"
    runat="server"
    Display="None"
    ClientValidationFunction="$SW.NodeSelectionOk"
    ValidationGroup="SelNode"
    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_151 %>"></asp:CustomValidator>
        
<div style="max-width: 888px;"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
<td style="vertical-align: bottom;">
    <div class="sw-form-heading" style="white-space: nowrap"><h2><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_152, "")%><span id="SelectedNodeName" runat="server"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_153 %></span></h2></div>
</td>
<td style="width: 400px; padding: 4px 0px 4px 0px;">
    <div id="warningNodeAdded" class="x-tip x-form-invalid-tip" style="display: none; visibility: visible; position: static; width: 400px;">
        <div class="x-tip-tl" style="zoom: 1;">
            <div class="x-tip-tr" style="zoom: 1;">
                <div class="x-tip-tc" style="zoom: 1;">
                    <div class="x-tip-header x-unselectable" style="zoom: 1; -moz-user-select: none;">
                        <span class="x-tip-header-text" />
                    </div>
                </div>
            </div>
        </div>
        <div class="x-tip-bwrap">
            <div class="x-tip-ml">
                <div class="x-tip-mr">
                    <div class="x-tip-mc">
                        <div class="x-tip-body" style="height: auto; width: auto;">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_163%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x-tip-bl x-panel-nofooter">
                <div class="x-tip-br">
                    <div class="x-tip-bc"/>
                </div>
            </div>
        </div>
    </div>
</td>
</tr></table></div>


<div class="warningbox sw-credential-warning" style="max-width: 874px; display: none;">
  <div class=inner>
    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_164 %>
  </div>
</div>
<div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">
    <ipam:DHCPCredentialsSelector ValidationGroup="SelNodeCredential" id="CredSelector" runat="server"
        FnSupplyServerAddress="function(fnContinue){ fnContinue($SW.ns$($nsId,'SelectedNodeId')[0].value); }" />
    <asp:CustomValidator ID="RequireSelectedNode2" runat="server" Display="None" ClientValidationFunction="$SW.NodeSelectionCredentials"
        ValidationGroup="SelNodeCredential" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_165 %>"></asp:CustomValidator>
</div>

<div class="sw-form-heading"><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_166%></h2></div>

<div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">
    <div>
		<asp:ScriptManager ID="scriptManager" runat="Server" EnablePartialRendering="true" />
			<asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
			    <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="CredSelector" />
                </Triggers>
				<ContentTemplate>
				</ContentTemplate>
			</asp:UpdatePanel>
		<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
			<tr class="sw-form-cols-normal">
				<td class="sw-form-col-label" style="width: 300px;"></td>
				<td style="width: 168px;"></td>
				<td class="sw-form-col-comment"></td>
			</tr>
			<tr>
				<td>
					<div class="sw-field-label">
						<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_167 %>
					</div>
				</td>
				<td>
					<div class="sw-form-item" style="padding-left: 7px;">
						<IPAM:TimeSpanEditor id="ServerScanInterval" TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_272 %>" Value="04:00:00" MinValue="00:10:00" MaxValue="7.00:00:00" runat="server" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="sw-form-item sw-check-align">
						<asp:CheckBox ID="cbAutoAddScopes" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_168 %>"  runat="server" Checked="true"/>
					</div>
				</td>
			</tr>
			<tr>
				<ipam:VrfGroupName ID="vrfGroupName" runat="server" IsEnableClusterName="True"/>
			</tr>
		</table>
	</div>
</div>

<div class="sw-form-heading" style="max-width: 876px;"><h2><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_169%></h2>
<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_170 %>
</div>

<div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">

    <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label" style="width: 238px;"></td>
            <td style="width: 40px;"></td>
            <td style="width: 100px;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td colspan="4"><div class="sw-form-item sw-check-align">
                <asp:CheckBox ID="cbEnableSubnetScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_171 %>" Checked="true" runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div id="subnetscanarea"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label" style="width: 238px;"></td>
            <td style="width: 168px;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_172 %>
            </div></td>
            <td>
                <IPAM:TimeSpanEditor id="NewSubnetInterval" ValidIf="cbEnableSubnetScanning" ActiveWhenChecked="True" TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_271 %>" Value="04:00:00" MinValue="00:10:00" MaxValue="7.00:00:00" runat="server" />
            </td>
            <td></td>
        </tr>
    </table></div>

</div>

<asp:ValidationSummary  runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

<div class="sw-btn-bar">
    <orion:LocalizableButton runat="server" id="btnSave" OnClientClick="return $SW.DhcpServerSave(this);" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_173 %>" DisplayType="Primary"/>
    <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" CausesValidation="false" LocalizedText="Cancel" DisplayType="Secondary"/>
</div>

</div>
</asp:Content>
