﻿using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using System.Web.UI.MobileControls;
using System.Collections.Generic;
using SolarWinds.IPAM.Web.Common.UISetting;
using AjaxControlToolkit;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZoneRecords : CommonPageServices
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            int zoneid = base.GetParam("ObjectId", -1);
            if (zoneid >= 0)
            {
                this.ZoneRecords.Store.WhereClause = string.Format("{{0}}.{0} = '{1}'", DnsRecord.EIM_DNSZONEID, zoneid);
            }

        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            PerformPersonalization();
        }

        private void PerformPersonalization()
        {
            SettingsCache cache = SettingsCache.GetInstance();

            List<MgtColumn> columns = null;

            if (cache.TryParseJsonSetting(UISetting.UIS_DNSZONERECORDSCOLUMNS, out columns))
                SubnetMgrHelper.PersonalizeExtGrid(ZoneRecords, columns);


        }
    }
}