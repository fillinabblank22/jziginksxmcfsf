<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" CodeFile="Dhcp.Scope.DeleteConfirm.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpScopeDeleteConfirm" 
Title="<%$Resources :IPAMWebContent, IPAMWEBDATA_VB1_630%>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <style type="text/css">
        .checkbox label{
            padding-left:5px;
        }
        .backgroundrepeat {
            background-position: 6px center;
            background-repeat: no-repeat;
            padding-left: 37px;
        }
        .tableHeader {
            background: none repeat scroll 0 0 #E0E0E0;
            padding-top: 4px;
            padding-bottom: 4px;
        }
        .Property {
            border-bottom: 1px groove #D8D6D1;
            font-family: Arial,Verdana,Helvetica,sans-serif;
            font-size: 13px;
            font-weight: normal;
            margin-bottom: 0;
            margin-top: 0;
        }
    </style>

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Delete" OnMsg="MsgDelete" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-dhcp-manage.js" Orientation="jsPostInit" runat="server">
var dat = $SW.Env && $SW.Env['deleteopt'];
if( dat )
{
 $(window).load( function(){ $SW.msgq.UPSTREAM('confirmed',$SW.Env['deleteopt']); });
}
else
{
 $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
 $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
 $(document).ready( function(){ Ext.QuickTips.init(); });
}

   $SW.IPAM.EnableWarningBox = function(isEnable){
        var chb = $('#'+$SW.nsGet($nsId, 'RemoveDhcpServerScopesCheckBox' ));
        var div = $('#'+$SW.nsGet($nsId, 'IscWarningMessage' ));
        var c = chb[0] || {checked: false};
        if(c.checked && isEnable == '1') div.show(0); else div.hide(0);
    };
    
    $SW.IPAM.EnableWarningBox('0');

</IPAMmaster:JsBlock>

<script type="text/javascript">
    if (!$SW.IPAM) $SW.IPAM = {};

    function ChangeStatus() {
        $SW.IPAM.EnableWarningBox(document.getElementById("<%=isIscServerExists.ClientID%>").value);
    };
</script>

<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
.grid { max-height: 80px; overflow: auto; padding-top: 10px; margin-bottom: 15px; }
* html .grid { height: 80px; }
.gridColumn { background-color: #E5F1F7;  padding: 5px 15px 5px 0; vertical-align:middle;}
.gridColumnAlternate { background-color: #FFFFFF; padding: 5px 0 5px 0; vertical-align:middle; }

div.sw-form-heading { font-size: small; margin-top: 16px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: small; margin: 0; }
</IPAMmaster:CssBlock>

<input type="hidden" id="isIscServerExists" runat="server" />

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><asp:Literal runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_218 %>"/></div>

    <div class="warningbox"  style="padding: 0px;" id="IscWarningMessage" runat="server">
		<div class="inner">
			<table border="0">
				<tr>
					<td>
						<div id="scopeLink" runat="server">                              
							<asp:Literal ID="warningMessage" runat="server" EnableViewState="false" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_MR1_20 %>"/>
						</div>
					</td>
				</tr>                
			</table>
		</div>
	</div>
    
    <asp:PlaceHolder ID="DhcpServerPlaceHolder" runat="server" Visible="false">
        <div class="sw-form-heading">
            <asp:Label ID="Label1" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_631 %>"></asp:Label>
        </div>
        <div class="grid">
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td class="tableHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_397 %></td>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="ServerRepeater" runat="server">
                        <HeaderTemplate></HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="Property <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%> backgroundrepeat gridColumnAlternate">
                                        <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td class="Property gridColumn <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%> backgroundrepeat ">
                                        <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate></FooterTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="DhcpScopePlaceHolder" runat="server">

        <div class="sw-form-heading">
            <asp:Label ID="DhcScopeWarningLabel" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_632 %>"></asp:Label>
            <asp:Label ID="ScopeWarningMsgLabel" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_633 %>" Visible="false"></asp:Label>
        </div>

        <div class="grid">
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td class="tableHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_275 %></td>
                        <td class="tableHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_20 %></td>
                        <td class="tableHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_23 %></td>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="ScopeRepeater" runat="server">
                        <HeaderTemplate></HeaderTemplate>
                            <ItemTemplate> 
	                        <tr>
                                <td class="Property <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> backgroundrepeat gridColumnAlternate" style="width: 45%">
                                        <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                </td>
                                <td class="Property gridColumnAlternate" style="width: 35%">
                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"SharedNetworkName"))%>
                                </td>
                                <td class="Property gridColumnAlternate" style="width: 20%">
                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"ServerName"))%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td class="Property <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> backgroundrepeat gridColumn"  style="width: 45%">
                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                </td>
                                <td class="Property gridColumn" style="width: 35%">
                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"SharedNetworkName"))%>
                                </td>
                                <td class="Property gridColumn" style="width: 20%">
                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"ServerName"))%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate></FooterTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>

    </asp:PlaceHolder>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:PlaceHolder ID="RemoveScopePlaceHolder" runat="server">
                <div style="margin-bottom:10px">
                  <asp:CheckBox ID="RemoveDhcpServerScopesCheckBox" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_634 %>" AutoPostBack="true" Checked="false" CssClass="checkbox"/>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="SubnetPlaceHolder" runat="server">
                <asp:CheckBox ID="RemoveSubnetCheckBox" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_635 %>" OnCheckedChanged="RemoveSubnetCheckBox_OnCheckedChanged" AutoPostBack="true" Checked="false" CssClass="checkbox"/>
                <br />
                <asp:Panel runat="server" ID="SubnetPanel">
                    <div class="sw-form-heading">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_636 %>
                    </div>    
                    <div class="grid">
                        <table border="0" cellpadding="2" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <td class="tableHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_212 %></td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="SubnetRepeater" runat="server">
                                    <HeaderTemplate></HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="Property <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> backgroundrepeat gridColumnAlternate">
                                                    <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <tr>
                                            <td class="Property <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"GroupIconPrefix"))%>-<%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"StatusIconPostfix"))%> backgroundrepeat gridColumn">
                                                <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem,"FriendlyName"))%>
                                            </td>
                                        </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate></FooterTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
            </asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>