using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ExportPickcolumns : CommonPageServices
	{
        PageExportPickColumns mgr;
        public ExportPickcolumns() { mgr = new PageExportPickColumns(this); }

	    protected void Page_Load(object sender, EventArgs e)
		{
            if (WarnOnZeroItems != null)
                WarnOnZeroItems.Visible = mgr.NumberOfSubnets == 0 && !mgr.IsSearchExport;
            if(mgr.IsSearchExport)
            {
                if (lblHeader != null)
                {
                    lblHeader.Text = string.Format(lblHeader.Text, mgr.NumberOfIps);
                }
            }
            else
            {
                if (linkTitle != null)
                {
                    linkTitle.Text = string.Format(linkTitle.Text, mgr.NumberOfSubnets);
                }

                if (lblHeader != null)
                {
                    lblHeader.Text = string.Format(lblHeader.Text, mgr.NumberOfSubnets);
                }
            }
        }
    }
}
