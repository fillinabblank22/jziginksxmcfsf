﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.UITask;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Linq;
using System.Text;
using Type = SolarWinds.IPAM.BusinessObjects.DNS.Type;


namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsRecordAddEdit : CommonPageServices
    {
        private const string QueryZoneParam = "ZoneId";
        private const string QueryRecordParam = "ObjectID";
        private const string QueryZoneNameParam = "ZoneName";
        int _recordId;
        int _zoneid;
        string _zoneName;
        string _ipPrefix;
        Guid _taskId;
        bool _bAddTask = true;
        private int _asscoicateRecord;
        private bool closeWindow = false;
      
        public string TaskId
        {
            get
            {
                return _taskId != new Guid() ? _taskId.ToString() : string.Empty;
            }
        }

        public bool CloseWindow
        {
            get
            {
                return closeWindow;
            }
        }

        public bool AddMode
        {
            get
            {
                return _bAddTask;
            }
        }
        public int ZoneId
        {
            get
            {
                return _zoneid;
            }
        }
        public string ZoneName
        {
            get
            {
                return _zoneName;
            }
        }
        public string IpPrefix
        {
            get
            {
                return _ipPrefix;
            }
        }
        public bool IsValidationRequired { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadSupportedTypes();
        }

        private void LoadSupportedTypes()
        {
            Array list = Enum.GetValues(typeof(SolarWinds.IPAM.BusinessObjects.DNS.SupportedTypes));
            for (int i = 0; i < list.Length; i++)
            {
                ddlRecordType.Items.Add(list.GetValue(i).ToString());
            }
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void ClickCancel(object sender, EventArgs e)
        {
           
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            Save();
        }

        protected void OnIsNewRecordDataRequire(object source, ServerValidateEventArgs args)
        {
            if (!IsValidationRequired)
            {
                args.IsValid = true;
            }
            else 
            {
                // 'false', whether the validated value is null or empty string
                args.IsValid = !string.IsNullOrEmpty(args.Value) && (args.Value.Trim() != string.Empty) && ValidateData();
            }

        }

        bool ValidateData()
        {
            bool breturn = true;
            string data = txtData.Text;
            try
            {
                switch (ddlRecordType.Text)
                {
                    case "A":
                    case "AAAA":
                        {
                            IPAddress.Parse(data);
                            break;
                        }
                    case "MX":
                        {
                            string[] n = data.Split(' ');
                            breturn = (n.Length == 2);
                            if (breturn)
                            {
                                int tv = int.Parse(n[0]);
                                if (tv < 0 || tv > 10)
                                {
                                    breturn = false;
                                }
                            }
                            break;
                        }
                    case "PTR":
                    case "CNAME":
                        {
                            break;
                        }

                }
            }
            catch (Exception)
            {
                breturn = false;
            }
            return breturn;
        }

        Dictionary<string, object> GetValues()
        {
            Dictionary<string, object> values = new Dictionary<string, object>();
            values.Add("RecordType", ddlRecordType.Text);
            string Data = txtData.Text.Trim();

            switch (ddlRecordType.Text)
            {
                case "A":
                    {
                        values.Add("IPAddress", Data);                      
                        break;
                    }
                case "AAAA":
                    {
                        values.Add("IPv6Address", Data);  
                        break;
                    }
                case "CNAME":
                    {
                        values.Add("PrimaryName", Data);                         
                        break;
                    }
                case "MX":
                    {
                        string[] SpValues = Data.Split(' ');

                        values.Add("MailExchange", SpValues[1]);
                        values.Add("Preference", SpValues[0]);

                        break;
                    }
                case "PTR":
                    {
                        values.Add("PTRDomainName", Data); 
                        break;
                    }

            }
            return values;

        }
        void Save()
        {
            if ((Orion.Common.OrionConfiguration.IsDemoServer) ||
                (initialData.Value == txtData.Text &&
                 (initialCheckBoxStatus.Value == "1") == cbAddUpdatePTRRecord.Checked))
            {
                closeWindow = true;
                return;
            }

            Dictionary<string, object> options = new Dictionary<string,object>();
            options.Add("IsDeleteOption", false);
            options.Add("Name", txtRecordName.Text.Trim());
            options.Add("Type", ddlRecordType.Text.Trim());
            options.Add("ZoneId", _zoneid);
            options.Add("Values",GetValues()); 

            if (_recordId >= 0)
                options.Add("RecordID", _recordId);
            if (_asscoicateRecord > 0)
                options.Add("AssociateRecordID",_asscoicateRecord);

            if (ddlRecordType.Text.Trim() != "A")
                cbAddUpdatePTRRecord.Checked = false; //This check box is only for A record other record will be always false

            options.Add("PairPTRCheck",cbAddUpdatePTRRecord.Checked);
            options.Add("InitialCheckBoxStatus", (initialCheckBoxStatus.Value=="1"));
            UiTaskDnsRecordManagment task = new UiTaskDnsRecordManagment(options);
            UITaskRegistry.GetInstance().AddTask(task);
            task.Start();
            _taskId = task.TaskId;
        }

        protected void OnIsValueRequire(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !string.IsNullOrEmpty(args.Value) && (args.Value.Trim() != string.Empty);
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
           
            _zoneid = base.GetParam(QueryZoneParam, -1);
            _recordId = base.GetParam(QueryRecordParam, -1);
            _zoneName = base.GetParam(QueryZoneNameParam, string.Empty);

            cbAddUpdatePTRRecord.Text = (_recordId >= 0) ? Resources.IPAMWebContent.IPAMWEBDATA_JI_12 : Resources.IPAMWebContent.IPAMWEBDATA_JI_13;
           
            
            if (_recordId >= 0)
            {
                _bAddTask = false;
                if (IsPostBack)
                    return;

                cbAddUpdatePTRRecord.Checked = false;
                using (IpamClientProxy proxy = SwisConnector.OpenCacheConnection())
                {
                    DnsRecord objRecord = proxy.AppProxy.DnsRecord.Get(_recordId);
                    //If scan happen parallel then the record id gets changed. 
                    if (objRecord != null)
                    {
                        _zoneid = objRecord.DnsZoneId;
                        txtRecordName.Text = objRecord.Name;
                        initialData.Value = txtData.Text = objRecord.Data;
                        ddlRecordType.SelectedValue = objRecord.Type.ToString();

                        txtRecordName.Enabled = false;
                        ddlRecordType.Enabled = false;

                        if (objRecord.Type == Type.A)
                        {
                            cbAddUpdatePTRRecord.Checked = (objRecord.AssociateRecoredID > 0);
                            initialCheckBoxStatus.Value = (cbAddUpdatePTRRecord.Checked) ? "1" : "0";
                        }
                        _asscoicateRecord = objRecord.AssociateRecoredID;
                    }
                    else
                    {
                        Response.Write("<script>alert('Please reload the DNS Records page and try again.');</script>");
                    }
                }
            }
            else
            {
                _ipPrefix = GetFormattedIpPrefix(_zoneName);
            }
            if (_zoneid >= 0)
            {
                DnsZoneSelector.DnsZoneId = _zoneid;
            }
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                btnSave.OnClientClick = "javascript:demoAction('IPAM_ADD_New_Record',null); return false;";
               
            }
        }
        
        private string GetFormattedIpPrefix(string zoneName)
        {
            List<string> octets = zoneName.Replace(".in-addr.arpa", string.Empty).Replace(".ip6.arpa", string.Empty).Split('.').Reverse().ToList();

            if (_zoneName.EndsWith(".in-addr.arpa"))
                _ipPrefix = string.Join(".", octets);
            else if (_zoneName.EndsWith(".ip6.arpa"))
            {
                StringBuilder sb = new StringBuilder();
                int count = 0;
                foreach (var octet in octets)
                {
                    if (count == 4)
                    {
                        sb.Append(":");
                        count = 0;
                    }
                    sb.Append(octet);
                    ++count;
                }
                _ipPrefix = sb.ToString();
            }

            return _ipPrefix;
        }
    }
}