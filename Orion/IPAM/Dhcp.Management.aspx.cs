using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Text;
using Castle.Components.DictionaryAdapter;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Master;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpManagementPage : CommonPageServices
    {
        private const string ScanServerListKey = "ScanServerList";

        PageDhcpManager mgr;

        public DhcpManagementPage()
        {
            mgr = new PageDhcpManager(this);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            MoveToolsetControl();
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            RunBackgroundScan();

            if (CommonWebHelper.IsMobileBrowser() || CommonWebHelper.IsMobileView(this.Request))
            {
                if (this.ipamFooter != null)
                    this.ipamFooter.clientEl = "mobileFooter";
            }

            if(Request.Params["legacyBrowserRedirect"] != null)
            {
                newUserInterfacePageLink.Visible = false;
                newUserInterfaceLegacyBrowserMessage.Visible = true;
            }
        }

        private void MoveToolsetControl()
        {
            // without moving the toolset control, IE8 crashes on Win2k8

            Control ToolsetObj = null;
            ToolsetObj = Locator.FindControlRecursive<Control>(Page.Master.Master, "toolsetObject", ToolsetObj);

            if (ToolsetObj == null || ToolsetObj.Visible == false)
                return;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.IO.StringWriter tw = new System.IO.StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            ToolsetObj.RenderControl(hw);
            ToolsetObj.Visible = false;
            LiteralControl ctx = new LiteralControl(sb.ToString());

            try { Page.Form.Parent.Controls.Add(ctx); return; }
            catch (Exception) { }
            try { Page.Controls.Add(ctx); return; }
            catch (Exception) { }
        }

        private void StartServersScan(List<int> serversToScan)
        {
            List<SwisOpCtx> ops = new EditableList<SwisOpCtx>();

            if (serversToScan.Any())
            {
                ops.AddRange(serversToScan.Select(item => new SwisOpCtx("DHCP Server Scan", delegate (IpamClientProxy proxy, object param)
                {
                    proxy.AppProxy.DhcpServer.Scan(Guid.NewGuid(), item);
                    return null;
                })));

                //Run scan operations
                SwisConnector.RunSwisOps(ops.ToArray(), true);
            }
        }
        private void RunBackgroundScan()
        {
            if (Session[ScanServerListKey] != null)
            {
                List<int> serversToScan = (List<int>)Session[ScanServerListKey];
                Session[ScanServerListKey] = null;
                StartServersScan(serversToScan);
            }
        }
    }
}