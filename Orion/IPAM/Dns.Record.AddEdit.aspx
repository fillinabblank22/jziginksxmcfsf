﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true"
    CodeFile="Dns.Record.AddEdit.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DnsRecordAddEdit"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_832 %>" ValidateRequest="false" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DnsZoneSelector.ascx" TagName="DnsZoneSelector" %>
<asp:Content ContentPlaceHolderID="main" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <!-- parent window will be sending down events, which will cause a postback and run a delegate -->
    <IPAMmaster:WindowMsgListener ID="MsgListener" Name="dialog" runat="server">
        <Msgs>
            <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
        </Msgs>
    </IPAMmaster:WindowMsgListener>
    <IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server" />
    <IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
        #breadcrumb td div { font-size: 100%; }
        #breadcrumb a { line-height: 25px; }
        #breadcrumb { margin-top: 0px; }
        p { margin-top: 0; margin-bottom: 10px; }
        .sw-flush-tabnav .x-tab-strip-spacer { display: none; }
        div.sw-check-align input { vertical-align: middle; }
        div.sw-check-align { padding: 6px 0px 6px 0px; }
        div.sw-form-heading { font-size: small; margin-top: 8px; margin-bottom: 4px; }
        div.sw-form-heading h2 { font-size: medium; margin: 0; }

        .smallText {
            color: #979797;
            font-size: smaller;
        }
    </IPAMmaster:CssBlock>
    <IPAMui:ValidationIcons runat="server" />
    <IPAMmaster:JsBlock Requires="ext,sw-ux-wait.js,sw-dnsrecord-manage.js,sw-dialog.js,ext-ux,sw-subnet-manage.js,sw-dhcp-groupby.js,sw-dhcp-manage.js,sw-expander.js"
        Orientation="jsPostInit" runat="server">
if (!$SW.IPAM) $SW.IPAM = {};

$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){
    var toDisable = ($SW.IPAM.TaskId&& $SW.IPAM.TaskId!= '') != "" ? true : false;

    if(!toDisable && $SW.IPAM.ClosePopup == 'True')
        $SW.msgq.UPSTREAM('close',document);
    else
        $SW.msgq.UPSTREAM('ready',toDisable);

    $('.expander').expander();
});

$SW.IPAM.CheckForDemo = function(source, arguments) {
    arguments.IsValid = !$SW.IPAM.IsDemo;
    if ($SW.IPAM.IsDemo)
        javascript:demoAction('IPAM_Create_DNS_Record',null);
};

$(document).ready( function(){ 
  var rtypeId = $SW.nsGet($nsId, 'ddlRecordType');
  var serverCb = $SW.TransformDropDown(rtypeId, '_ddlRecordType', { hiddenId: rtypeId } );
  serverCb.on('select', function(combo, record, index){ $SW.IPAM.RecordTypeChanged(); });
  $SW.IPAM.RecordTypeChanged();

if($SW.IPAM.TaskId && $SW.IPAM.TaskId!= '')
{        
    var title = $SW.IPAM.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_326;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_327;E=js}';
    var errorTitle = $SW.IPAM.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_GK_1;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_GK_2;E=js}';
    var ErrMsgTpl = new Ext.XTemplate(
        '{message:this.toHtml}',
        '<tpl if="this.hasErrs(results)">',
            '<br /><hr>',
                '<tpl for="results"><tpl if="this.isValid(ErrorCode)">',
                    '<br>■ &ensp;<b>{Method}</b>{ErrorMessage}',
                '</tpl></tpl>',
        '</tpl>',
        {
            toHtml: function(msg){return Ext.util.Format.htmlEncode(msg); },
            hasErrs: function(rs){return (rs && rs.length > 0); },
            isValid: function(ec){return (ec != undefined && ec != 0); }
        }
    );
    
    var WarnMsgTpl = new Ext.XTemplate(
        '{message:this.toHtml}',
        '<br /><hr>',
            '<tpl for="results"><tpl if="!Success">',
                '<tpl if="this.isSubnetDelayOffer(Method)">',
                    '<br>■ &ensp;' + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_393;E=js}', '<b>', '</b>'),
                '</tpl>',
            '</tpl></tpl>',
        {
            toHtml: function(msg){return Ext.util.Format.htmlEncode(msg); },
            isSubnetDelayOffer: function(method) {return method === "SetSubnetDelayOffer";}
        }
    );

    $SW.Tasks.Attach($SW.IPAM.TaskId,
    { title: $SW.IPAM.AddMode ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_282;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_283;E=js}" },
    function(d) {
            if (d.success){
                var warning = false;
                if (d.results) {
                    $.each(d.results, function(i, result) {
                        if (result.Success === false)
                            warning = true;
                    });                   
                }
                
                if (warning) {
                    d.message = $SW.IPAM.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_328;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_329;E=js}'
                    Ext.Msg.show({
                            title: title,
                            msg: WarnMsgTpl.apply(d),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING,
                            fn: function() { 
                                    $SW.msgq.UPSTREAM('close',document);
                             }
                        });
                }
                else {
                    Ext.Msg.show({
                            title: title,
                            msg: $SW.IPAM.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_284;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_285;E=js}',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO ,
                            fn: function() { 
                                $SW.msgq.UPSTREAM('close',document);
                            
                            }
                        });
                }

            } else Ext.MessageBox.show({
                title: errorTitle,
                msg: (d.results == null) ? d.message : d.results,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR,
                width:430,
                fn: function() { 
                            $SW.msgq.UPSTREAM('ready',false);
                }      
            });
        });
}});
    </IPAMmaster:JsBlock>
    <script language="javascript" type="text/javascript">
        $SW.IPAM.TaskId = '<%=TaskId %>';
        $SW.IPAM.ZoneId = '<%=ZoneId %>';
        $SW.IPAM.AddMode = '<%=AddMode %>' == 'True' ? true : false;
        $SW.IPAM.txtDataField = '<%=this.txtData.ClientID %>';
        $SW.IPAM.chkPTR = '<%=this.divAddUpdatePTRRecord.ClientID %>';
        $SW.IPAM.chkPTRBox = '<%=this.cbAddUpdatePTRRecord.ClientID %>';
        $SW.IPAM.IsDemo = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
        $SW.IPAM.ClosePopup = '<%=CloseWindow %>';
        $SW.IPAM.ZoneName = '<%=ZoneName %>';
        $SW.IPAM.IpPrefix = '<%=IpPrefix %>';
        $SW.IPAM.txtRecordNameField = '<%=this.txtRecordName.ClientID %>';
    </script>
    <div id="formbox">
        <div id="ChromeFormHeader" runat="server" class="sw-form-header">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_832%>
        </div>   
        <div class="group-box white-bg">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0" width="100px">
                <tr class="sw-form-cols-normal">
                    <td class="sw-form-col-label">
                    </td>
                    <td class="sw-form-col-control">
                    </td>
                    <td class="sw-form-col-comment">
                    </td>
                </tr>              
                <tr>
                    <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_833 %></div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtRecordName" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td>                        
                        
                        <asp:CustomValidator runat="server" Display="Dynamic" ControlToValidate="txtRecordName"
                            ClientValidationFunction="$SW.IPAM.IsRecordNameValid" ValidateEmptyText="true"
                            OnServerValidate="OnIsNewRecordDataRequire" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_834 %>" />
                        
                        <asp:CustomValidator runat="server" Display="Dynamic" ControlToValidate="txtRecordName"
                            ClientValidationFunction="$SW.IPAM.IsStartWithZonePrefix" ValidateEmptyText="true" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_837 %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="sw-field-label">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_835 %></div>
                    </td>
                    <td>
                        <div class="sw-form-item">
                            <asp:DropDownList ID="ddlRecordType" CssClass="ddlRecordType" runat="server" onchange="$SW.IPAM.RecordTypeChanged(this);" />                           
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="sw-field-label">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_836 %></div>
                    </td>
                    <td style ="width :305px">
                        <div class="sw-form-item">
                            <asp:TextBox ID="txtData" CssClass="x-form-text x-form-field txtData" Text="" runat="server" />
                            <input type="hidden" id="initialData" runat="server" />
                        </div>
                    </td>
                    <td>
                        <asp:CustomValidator runat="server" Display="Dynamic" ClientValidationFunction="$SW.IPAM.ValidateData" ValidationGroup="Datavalidation"
                            ControlToValidate="txtData" ValidateEmptyText="true" 
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_837 %>" />

                        <asp:CustomValidator runat="server" Display="Dynamic" ClientValidationFunction="$SW.IPAM.ValidateData2" ValidationGroup="Datavalidation"
                            ControlToValidate="txtData" ValidateEmptyText="true" OnServerValidate="OnIsNewRecordDataRequire"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_JI_14 %>" />
                    </td>
                </tr>
                <tr>
                <td></td>
                    <td colspan="2">
                        <div class="smallText">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_838%>                            
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="sw-form-item" id="divAddUpdatePTRRecord" runat="server">
                            <asp:CheckBox ID="cbAddUpdatePTRRecord" Text="Test" runat="server"/>
                            <input type="hidden" id="initialCheckBoxStatus" runat="server" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
      <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_289 %>">
        <div class="group-box blue-bg">
            <ipam:DnsZoneSelector ID="DnsZoneSelector" runat="server" ReadOnly ="true" />
        </div>
      </div>
        <asp:CustomValidator runat="server" Display="None" ClientValidationFunction="$SW.IPAM.CheckForDemo" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_ED1_4 %>"/>
    </div>
        <asp:ValidationSummary runat="server" ShowSummary="false" ShowMessageBox="<%#!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer%>" DisplayMode="BulletList" />
    <div id="ChromeButtonBar" runat="server" class="sw-btn-bar-wizard">
        <orion:LocalizableButton CausesValidation="true" LocalizedText="Save" ID="btnSave" DisplayType="Primary" runat="server" OnClick="ClickSave" />
        <orion:LocalizableButton CausesValidation="false" LocalizedText="Cancel" ID="btnCancel" DisplayType="Secondary" OnClick="ClickCancel" runat="server" />
    </div>
</asp:Content>
