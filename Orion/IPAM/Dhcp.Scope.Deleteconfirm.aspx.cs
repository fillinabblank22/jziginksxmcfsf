using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.MobileControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.BusinessObjects.DHCPManagement;
using SolarWinds.IPAM.DHCPMultiDevice.Factory;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeDeleteConfirm : CommonPageServices
	{
        private PageDhcpScopeDelete del;
        public bool IsIscServerExists { get; set; }

        public DhcpScopeDeleteConfirm() 
        {
            del = new PageDhcpScopeDelete(this); 
        }

        protected override List<string> GetChromeControlIDs()
        {
            List<string> ret = base.GetChromeControlIDs();
            ret.Add( "ButtonBox" );
            return ret;
        }

        bool AreServersCapableOfDeleteScope(DataTable dt, string coloumn)
        {
            if (dt == null || dt.Rows.Count < 0)
            {
                return true;
            }

            var serverTypes = new List<DhcpServerType>();
            foreach (DataRow item in dt.Rows)
            {
                int serverType;
                if (Int32.TryParse(item[coloumn].ToString(), out serverType))
                    serverTypes.Add((DhcpServerType)serverType);
            }

            isIscServerExists.Value = serverTypes.Contains(DhcpServerType.ISC) ? "1" : "0";

            // [oh] check distinct types of DHCP servers for scope removal capability
            var canDeleteScope = serverTypes.Distinct().All(t => {
                return DhcpFactory.Instance.CheckedDhcpManagerCapability(t, typeof(IScopeManagement));
            });

            return canDeleteScope;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.del.Initialize();
            RemoveDhcpServerScopesCheckBox.Attributes.Add("onClick", "ChangeStatus();");
            this.del.DeleteSubnets = RemoveSubnetCheckBox.Checked;
            this.del.DeleteDhcpServerScopes = RemoveDhcpServerScopesCheckBox.Checked;

            warningMessage.Text = string.Format(warningMessage.Text, "<b>", "</b","<br/>");

            if (!this.IsPostBack)
            {
                isIscServerExists.Value = "0";

                DataTable servers = del.LoadDhcpServers();

                bool bServerTypeChecked = false;

                if (servers != null)
                {
                    DhcpServerPlaceHolder.Visible = true;
                    ServerRepeater.DataSource = servers;
                    ServerRepeater.DataBind();

                    RemoveDhcpServerScopesCheckBox.Enabled = AreServersCapableOfDeleteScope(servers, "ServerType");
                    bServerTypeChecked = true;
                }

                DataTable scopes = del.LoadDhcpScopes();
                RemoveDhcpServerScopesCheckBox.Text = Resources.IPAMWebContent.IPAMWEBCODE_VB1_9;
                if (scopes != null)
                {
                    if (scopes.Rows.Count > 1)
                        RemoveDhcpServerScopesCheckBox.Text = Resources.IPAMWebContent.IPAMWEBDATA_VB1_634;
                    
                    if (!bServerTypeChecked)
                    {
                        RemoveDhcpServerScopesCheckBox.Enabled = AreServersCapableOfDeleteScope(scopes, "ParentType");
                    }

                    RemoveScopePlaceHolder.Visible = true;

                    ScopeRepeater.DataSource = scopes;
                    ScopeRepeater.DataBind();
                }
                else
                {
                    DhcpScopePlaceHolder.Visible = false;
                    RemoveScopePlaceHolder.Visible = false;
                }

                DataTable subnets = del.LoadSubnets();
                if (subnets != null)
                {
                    SubnetRepeater.DataSource = subnets;
                    SubnetRepeater.DataBind();
                }
                else
                {
                    SubnetPlaceHolder.Visible = false;
                }

                ApplyRemoveSubnetCheckBoxState();
            }
        }

	    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            del.Delete(true);
        }
        
        protected void MsgDelete(object sender, EventArgs e)
        {
            del.Delete(true);
        }
        
        protected void RemoveSubnetCheckBox_OnCheckedChanged(object sender, EventArgs e)
        {
            ApplyRemoveSubnetCheckBoxState();
        }

        private void ApplyRemoveSubnetCheckBoxState()
        {
            SubnetPanel.Visible = RemoveSubnetCheckBox.Checked;
            if (RemoveSubnetCheckBox.Checked)
            {


            }
        }
	}
}
