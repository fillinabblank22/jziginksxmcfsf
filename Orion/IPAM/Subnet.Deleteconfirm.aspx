<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" CodeFile="Subnet.Deleteconfirm.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.SubnetDeleteconfirm" 
Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_120 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Delete" OnMsg="MsgDelete" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,ext-quicktips,sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
var dat = $SW.Env && $SW.Env['deleteopt'];
if( dat ) {
 $(window).load( function(){ $SW.msgq.UPSTREAM('confirmed',$SW.Env['deleteopt']); });
} else {
 $(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
 $(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
};
</IPAMmaster:JsBlock>

<div id=formbox>

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_218 %></div>

    <IPAM:GridPreloader ID="GridPreloader1" SourceID="Grid" runat="server" />

    <IPAMlayout:ViewPort id="viewport" runat="server">
        <IPAMlayout:BorderLayout ID="mainlayout" runat="server" borderVisible="false">
        <North>
           <IPAMlayout:BoxComponent ID="BoxComponent3" runat="server" borderVisible="false" visible=false serverEl="WarningMsg" /> 
        </North>
        <Center>
            <IPAMlayout:BorderLayout ID="BorderLayout2" runat="server" borderVisible="false">
                <North>
                    <IPAMlayout:BoxComponent ID="BoxComponent2" runat="server" borderVisible="false" visible="false" serverEl="breadcrumb" />
                </North>
                <Center>
                  <IPAMlayout:GridBox id="grid" autoHeight="false" forceFit="True" borderVisible="false" UseLoadMask="True" emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_122 %>" deferEmptyText="false" RowSelection="true" runat="server" SelectorName="selector">

                  <Columns>
                        <IPAMlayout:GridColumn id="GridColumn1" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_78 %>" width="120" isSortable="true" dataIndex="FriendlyName" renderer="function(value,meta,record){var d = record.data;
                          meta.css = 'status-bg ' + d.GroupIconPrefix + '-' + d.StatusIconPostfix;
                          meta.attr = 'qtip=&quot;'+ d.StatusShortDescription +'&quot;';
                          return value; }" runat="server" />
                        <IPAMlayout:GridColumn id="GridColumn5" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="90" isSortable="true" dataIndex="GroupTypeText" runat="server" />
                        <IPAMlayout:GridColumn id="GridColumn2" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_240 %>" width="90" isSortable="true" dataIndex="Address" runat="server" />
                        <IPAMlayout:GridColumn id="GridColumn3" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_19 %>" width="60" isSortable="true" dataIndex="CIDR" runat="server" />
                        <IPAMlayout:GridColumn id="GridColumn4" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_242 %>" width="90" dataIndex="AddressMask" runat="server" />
                  </Columns>
                  <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="GroupType,FriendlyName" runat="server">
                    <Fields>
                        <IPAMlayout:GridStoreField ID="GridStoreField5" dataIndex="GroupId" isKey="true" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField1" dataIndex="FriendlyName" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField2" dataIndex="Address" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField3" dataIndex="CIDR" dataType="int" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField4" dataIndex="AddressMask" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField6" dataIndex="GroupIconPrefix" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField7" dataIndex="StatusIconPostfix" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField8" dataIndex="StatusShortDescription" runat="server" />
                        <IPAMlayout:GridStoreField ID="GridStoreField9" dataIndex="GroupTypeText" runat="server" />
                    </Fields>
                  </Store>
                  </IPAMlayout:GridBox>
                </Center>
                 <South>
                     <IPAMlayout:BoxComponent ID="ButtonBox" borderVisible="false" clientEl="buttonbar" layout="fit" runat="server" />
                </South>
             </IPAMlayout:BorderLayout>
          </Center>
        </IPAMlayout:BorderLayout>
        
    </IPAMlayout:ViewPort>
    
    <div id="buttonbar" class="sw-form-buttonbar sw-btn-bar" visible="false">
        <orion:LocalizableButton runat="server" ID="btnDelete" LocalizedText="Delete" OnClick="btnDelete_Click" DisplayType="Primary" />
        <orion:LocalizableButtonLink runat="server" ID="A1" LocalizedText="Cancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" DisplayType="Secondary" />
    </div>
    
    <asp:Label ID="WarningMsg" runat="server"></asp:Label>

</div>
    
</asp:Content>
