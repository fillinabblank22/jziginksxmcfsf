<%@ Page Language="C#"  MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.preview.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportPreview"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_365 %>" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewImportValidationErrors" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck ID ="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

    <IPAMmaster:JsBlock Requires="ext-quicktips,ext-gridscrollfix.js" Orientation="jsPostInit"
        runat="server">
$SW['ValidationCellRenderer'] = function(value,meta,record,rowIndex,colIndex)
{
    var rowNumber = record.data["RowNumber"]

    var row = $SW.ValidationData[rowNumber];

    if(row)
    {
      var prevCell = row[colIndex - 1];
      var cellv = row[colIndex];
      var nextCell = row[colIndex + 1];

      if(cellv)
      {
        if (!prevCell && !nextCell)
            meta.css = 'sw-import-invalid-alone';
        else if (!prevCell)
            meta.css = 'sw-import-invalid-first';
        else if (!nextCell)
            meta.css = 'sw-import-invalid-last';
        else
            meta.css = 'sw-import-invalid-center';

        meta.attr = 'qtip="' + Ext.util.Format.htmlEncode(cellv) + '"';
      }
    }

    return value;
};



$SW.HasValidData = function( val)
{
       if(!val)
         {
            Ext.Msg.show({
                    title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_84;E=js}',
                    msg: '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_85;E=js}',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING});
          return false;
        }else
          return true;
}


    </IPAMmaster:JsBlock>
    
<div style="margin-left: 10px;">

     <table width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><asp:Label ID="TitleValidation" runat="server" /></h1>
        </td>
        </tr>
      </table>
    
        <div id="formbox">
            <div id="subnetPermissions" style="margin: 10px 0px 10px 0px;">
                <asp:Label ID="subnetPersmissionsLabel" runat="server" Text="" Visible="false" />
            </div>
            
            <div id="licenseExceeding" style="margin: 10px 0px 10px 0px;">
                <asp:Label ID="licenseExceedingLabel" runat="server" Text="" Visible="false" />
            </div>

            <div id="errorsDetected" style="margin: 10px 0px 10px 0px;">
                <asp:Label ID="errorsDetectedLabel" runat="server" Text="" />
            </div>

            <div id="commonErrors" style="margin: 10px 0px 10px 0px;">
                <asp:Label ID="commonErrorsLabel" runat="server" Text="" Visible="false" />
            </div>
            
            <div id="importPreviewHeader" style="margin: 30px 0px 10px 0px;">
                <asp:Label ID="importPreviewHeaderLabel" runat="server" Text="" />
            </div>

            <div id="gridframe" style="margin: 16px 0px 0px 0px;"> <!-- --> </div>
            
            <IPAM:GenericGridPreloader ID="PreLoader" SourceID="Grid" runat="server" />

            <IPAMlayout:GridBox runat="server"
                width="640"
                autoHeight="false"
                minBodyHeight="100"
                maxBodyHeight="500"
                autoFill="false"
                autoScroll="true"
                ID="grid"
                title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_366 %>"
                emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_367 %>"
                borderVisible="true"
                RowSelection="true"
                clientEl="gridframe"
                deferEmptyText="false"
                listeners="{ beforerender: $SW.ext.gridSnap }"
                StripeRows="true">

                <Store ID="store" dataId="PRELOAD" autoLoad="true" runat="server" />
            </IPAMlayout:GridBox>

            <div id="exportErrors" style="margin: 16px 0px 0px 0px; text-align: right">
                <orion:LocalizableButtonLink runat="server" ID="hrErrorsExport" OnClientClick="$SW.ConfirmBack(); return false;" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_347 %>" DisplayType="Small" />
            </div>
        </div>
        
        <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
            <orion:LocalizableButtonLink runat="server" ID="LinkBack" LocalizedText="Back" DisplayType="Secondary"/>
            <orion:LocalizableButtonLink runat="server" ID="Next" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_368 %>" DisplayType="Primary"/>
            <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>
</div>

</asp:Content>
