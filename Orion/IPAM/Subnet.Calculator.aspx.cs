using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Layout;

public partial class SubnetCalculator : CommonPageServices
{
	private PageSubnetCalculator mgr;

	public SubnetCalculator() { mgr = new PageSubnetCalculator(this, "AddSubnetsWizard", "SubnetsProperties"); }

	public GridBox Grid
	{
		get { return grid; }
	}

	public void Done_Click(object src, EventArgs args)
	{
        if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
		mgr.CompleteBtnClick();
	}
}
