using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using System.Web.UI.MobileControls;
using System.Collections.Generic;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpScopeEdit : CommonPageServices
    {
        private PageScopeEditor editor;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.editor = new PageScopeEditor(this);
            editor.Initialize();

            if (!this.IsPostBack)
            {
                BindControls();
            }
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            this.Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            this.Save();
        }

        void BindControls()
        {
            DhcpScope scope = this.editor.Scope;
            if (scope == null)
                return;

            CustomPropertyRepeater.RetrieveCustomProperties(scope, new List<int>(){ scope.GroupId });
            if (CustomPropertyRepeater.Count == 0)
            {
                CustomFieldsPlaceHolder.Visible = false;
            }

            txtDisplayName.Text = scope.FriendlyName;
            txtDhcpServer.Text = scope.DhcpServerName;
            txtNetworkAddress.Text = scope.FoundAddress; 
            txtCidr.Text = scope.FoundCIDR.ToString();
            txtNetworkMask.Text = scope.AddressMask; ;
            txtComment.Text = scope.Comments;
            txtVLAN.Text = scope.VLAN;
            txtLocation.Text = scope.Location;
        }

        protected void Save()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            List<SwisOpCtx> ops = new List<SwisOpCtx>();

            DhcpScope scope = this.editor.Scope;
            UpdateScope(scope);

            if (scope.IsDirty)
            {
                ops.Add(new SwisOpCtx("Update DHCP Scope",
                    delegate(IpamClientProxy proxy, object param)
                    {
                        //We only update location, VLAN and custom properties
                        return proxy.AppProxy.DhcpScope.Update(scope);
                    })
                );
            }

            SwisConnector.RunSwisOps(ops.ToArray());

            if (this.HideChrome)
            {
                string page = "~/Orion/IPAM/DialogWindowDone.aspx";

                this.Server.Transfer(page);
            }
            else
                this.Response.Redirect("~/api2/ipam/ui/scopes");
        }

        void UpdateScope(DhcpScope scope)
        {
            scope.VLAN = txtVLAN.Text;
            scope.Location = txtLocation.Text;

            CustomPropertyRepeater.GetChanges(scope);
        }
    }
}
