using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DialogWindowDone : CommonPageServices
    {
		//#region Controls
		//protected global::SolarWinds.IPAM.Web.Master.WindowMsgListener MsgListener;
		//#endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (MsgListener != null)
            {
                int refreshId = this.GetParam("forcerefresh", -1);
                bool doLicense = this.GetParam("sendlicense", false);
                string taskId = this.GetParam("taskId", String.Empty);

                if (refreshId >= 0)
                {
                    // current implementation of 'refresh this item' = refresh page.
                    MsgListener.SendUpstreamMessage(new WindowMsgUpstream("cancel", null));
                    MsgListener.SendUpstreamMessage(new WindowMsgUpstream("refresh", "{id:" + refreshId.ToString() + "}"));
                }
                else
                {
                    string param = null;
                    if (IsValidGuid(taskId))
                    {
                        param = "{taskId: '"+ taskId + "'}";
                    }

                    MsgListener.SendUpstreamMessage(new WindowMsgUpstream("close", param));
                }

                if (doLicense)
                {
                    MsgListener.SendUpstreamMessage(new WindowMsgUpstream("license", LicenseStatus.GetJson()));
                }
            }
        }

        bool IsValidGuid(string id)
        {
            if (String.IsNullOrEmpty(id))
                return false;

            try
            {
                Guid g = new Guid(id);
                return g != Guid.Empty;
            }
            catch
            {
            }

            return false;
        }
    }
}
