using System;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace SolarWinds.IPAM.WebSite
{
    public partial class ErrorMaster : MasterPage
    {
        protected override void OnLoad(EventArgs e)
        {
            cpcontent.Text = string.Format( cpcontent.Text, SolarWinds.IPAM.Web.Common.Utility.RegistrySettings.GetVersionDisplayString() );
            base.OnLoad(e);
        }
    }
}
