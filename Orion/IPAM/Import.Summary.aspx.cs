using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportSummary : CommonPageServices
	{
        PageImportSummary mgr;
        public ImportSummary() { mgr = new PageImportSummary(this); }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["GroupID"] != null)
                this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));
        }

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
    }
}
