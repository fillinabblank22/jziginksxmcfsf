﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Subnet.ScanStatus.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.SubnetScanStatus" 
Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_206 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <div class="links" style="text-align: right">
        <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHViewSubnetScanStatus" /><br/><br/>
        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_208, "<span id='refreshtime'>", "</span>")%>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.Any" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
<orion:Include File="breadcrumb.js" runat="server" />
<IPAMmaster:JsBlock requires="ext,sw-helpserver,ext-quicktips" Orientation=jsInit runat="server" />

<IPAMmaster:CssBlock Requires="~/Orion/styles/Breadcrumb.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }

.sw-grid-buttons { text-align: right; }
.sw-grid-cancel { margin-right: 6px; }
.sw-flush-tabnav .x-tab-strip-spacer { display: none; }
.sw-grid-edit span { z-index: -1; }
.warningbox a { text-decoration: underline; }
</IPAMmaster:CssBlock>

<script type="text/javascript">
    $SW.ScanEnable = <%=this.ScanEnable.ToString().ToLower() %>;
</script>

<IPAMmaster:JsBlock Requires="sw-subnet-manage.js" Orientation="jsInit" runat="server">
$SW.grid_lastload = new Date();
$SW.grid_loadlock = false;
$SW.grid_loadrate = 30000;
$SW.grid_firstload = true;
$SW.MinuteToMillisecondsRate = 60000;

$SW.update_polling_engine_column_visibility = function(){        
        var grid = Ext.getCmp('grid');
        var query = 'SELECT COUNT(EngineId) as Amount FROM Orion.Engines';
        $SW.DoQuery(query, function (values) {
                if (values != null){
                    if(values[0].Amount > 1){
                        grid.getColumnModel().config[5].hidden = false;
                    }
                }
            });
}

$SW.grid_reloaded = function(o){
  
  $SW.grid_loadlock = false;
  $SW.grid_lastload = new Date();
  
  if( $SW.grid_firstload )
  {
    $SW.grid_firstload = false;
    Ext.TaskMgr.start({ run: function(){
        
        var txt;

        if( $SW.grid_loadlock )
        {
            txt = 'now';
        }
        else
        {
            var now = new Date();
            var diff = (now - $SW.grid_lastload.getTime());
            
            if( diff >= $SW.grid_loadrate )
            {
                $SW.grid_refresh();
                txt = 'now';
            }
            else
            {
                txt = Math.round( ($SW.grid_loadrate - diff) / 1000 );
            }
        }

        $('#refreshtime').text(txt);
    },
    interval: 1000
    });
  }

  return true;
};

$SW.grid_refresh = function(){
  $SW.grid_loadlock = true;
  var tb = Ext.getCmp('grid').getBottomToolbar();
  tb.doLoad(tb.cursor);
};

$SW.grid_drilldown = function(grid, rowIndex) {
    record = grid.getStore().getAt(rowIndex);
    window.location = 'subnets.aspx?opento=' + record.id;
};

$SW.grid_editclick = function(el, r){
    var groupType = r.data['A.Subnet.GroupType'];
    var isIPv6Subnet = (groupType == 512);
    var type = groupType != 2048;
    var title = type ? '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_20;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_21;E=js}';
    if(type) {
        var page = isIPv6Subnet ? "IPv6.Subnet.Edit.aspx" : 'subnet.edit.aspx';
        var hlp = isIPv6Subnet ? 'OrionIPAMPHWindowEditIPv6Subnet.htm' : 'OrionIPAMPHWindowEditSubnetProperties.htm';
        var web = '/Orion/IPAM/' + page + '?NoChrome=True&msgq=mainWindow&ObjectId=' + r.id;
        $SW.ext.dialogWindowOpen($SW['mainWindow'], el, title, web, hlp);
    }
    else {
        window.location = "/Orion/IPAM/DnsZones/Dns.Zone.Wizard.aspx?ZoneId=" + r.id
    }
};

$SW.grid_cellclick = function(grid, rowIndex, columnIndex, e) {

  $SW.ext.gridClickToDrilldown(grid,rowIndex,columnIndex,e);

  var be = e.browserEvent.target || e.browserEvent.srcElement;
  if( be.tagName.toUpperCase() == 'SPAN' )
    be = be.parentNode;

  if( be.tagName.toUpperCase() != 'A' )
    return;

  var record = grid.getStore().getAt(rowIndex);

  if( /sw-grid-cancel/.test(be.className) )
  {
    var fnCancel = function()
    {
      Ext.Ajax.request({
       url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx",

       success: $SW.grid_refresh,

       failure: function(){

           Ext.Msg.show({
               title:'@{R=IPAM.Strings;K=IPAMWEBJS_VB1_5;E=js}',
               msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_6;E=js}',
               buttons: Ext.Msg.OK,
               icon: Ext.MessageBox.ERROR
            });        
       },

       params:
       {
        entity: 'IPAM.ScanInstance',
        verb: 'Cancel',
        ObjectId: ''+record.id
       }
       });
   };

    Ext.Msg.show({
       title:'@{R=IPAM.Strings;K=IPAMWEBJS_AK1_22;E=js}',
       msg: String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_23;E=js}', record.data['A.Subnet.FriendlyName']),
       buttons: Ext.Msg.YESNO,
       icon: Ext.MessageBox.QUESTION,
       fn: function(btn){ if( btn == 'yes' ) fnCancel(); }
       });
  }

  if( /sw-grid-edit/.test( be.className ) )
    $SW.grid_editclick(be, record);
}

$SW.renderer_buttons = function(){
    return function(value,meta,record){
        var ret = '';
        meta.css = 'sw-grid-buttons';
        
        if( record.data.Status == 1 )
            ret += '<a class="sw-btn sw-btn-small sw-grid-cancel"><span class="sw-btn-c sw-btn-t" style="padding-left: 10px;">@{R=IPAM.Strings;K=CommonButtonType_Cancel; E=js}</span></a>&nbsp;&nbsp;&nbsp;';
       
        return ret + '<a class="sw-btn sw-btn-small sw-grid-edit"><span class="sw-btn-c sw-btn-t" style="padding-left: 10px;">@{R=IPAM.Strings;K=CommonButtonType_Edit; E=js}</span></a>';
    }
};

$SW.renderer_type = function(){
    return function(value,meta,record){
        if( value == 2 ) return '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_32;E=js}';
        return '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_33;E=js}';
    }
};

$SW.renderer_jobtype = function() {
    return function(value,meta,record){
        if (value == 8) return '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_24;E=js}';
        if (value == 2048) return '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_25;E=js}';
        if (value == 512) return '@{R=IPAM.Strings;K=IPAMWEBJS_SE_44;E=js}';
        return '@{R=IPAM.Strings;K=Enum_SNMPAuthMethod_Unknown;E=js}';
    }
};

$SW.renderer_status = function(){

    function getminutes( ms )
    {
      if( ms < 60000 )
        return "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_26;E=js}";

      var r = Math.round( ms / 6000 ) / 10;
      return r + " @{R=IPAM.Strings;K=IPAMWEBJS_AK1_27;E=js}";
    }

    function getdatedesc( dist, time)
    {
        if( dist <= 0 )
        {
            return "@{R=IPAM.Strings;K=IPAMWEBJS_AK1_28;E=js}";
        }

        if( dist > 86400000 )
        {
            // more than a day.
            return Ext.util.Format.date(time,$SW.ExtLocale.DateFull);
        }
        else
        {
            // less than a day.
            return Ext.util.Format.date(time,$SW.ExtLocale.TimeShort);
        }
    }

    function getNumberWithOrdinal(n) {
        var s=["th","st","nd","rd"],
        v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
     }

    function minutesToMiliseconds(min)
    {
        return (min * $SW.MinuteToMillisecondsRate);
    }

    return function(value,meta,record){

        var r;
        var now = new Date();
        if( record.data.Status == 1 )
        {
            var start = record.data.ScanStartTimeStamp.getTime() + $SW.timediff;
            start -= minutesToMiliseconds(now.getTimezoneOffset());
            r = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_29;E=js}', getminutes( now.getTime() - start ));
        }
        else if( record.data.Status == 2 )
        {
            if(record.data.ScanStartTimeStamp != null)
            {
                r = '@{R=IPAM.Strings;K=IPAMWEBJS_AK1_30;E=js}';
            }
            else
            {
                var queuePositon = getNumberWithOrdinal(Number(record.data.QueuePosition) + 1)
                r = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_30_2;E=js}', queuePositon);
            }
        }
        else if( record.data.Status == 3 )
        {
            var queue = record.data.QueueTimeStamp.getTime() + $SW.timediff;
            queue -= minutesToMiliseconds(now.getTimezoneOffset());
            dist = queue - now.getTime();
            if(dist < -$SW.MinuteToMillisecondsRate)
            {
                r = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_28_2;E=js}', getminutes( now.getTime() - queue ));
            }
            else
            {
                r = String.format('@{R=IPAM.Strings;K=IPAMWEBJS_AK1_31;E=js}', getdatedesc( dist, new Date(queue) ));
            } 
        } 
        else
        {
            r = '';
        }

        meta.attr = 'qtip="' + r + '"';
        return r;
    }
};

$SW.renderer_pollingEngine = function(){
    return function(value,meta,record){
        if (value != null){
            if(record.data["A.Subnet.Engines.ServerType"] === "Primary"){
                value += " (Primary)";
            }
            text = String.format(
            "<img src='/Orion/images/StatusIcons/Small-{0}.gif' style='vertical-align: top; margin-top: 1px;' class='StatusIcon'/>",
            $.trim(record.data["A.Subnet.StatusIconPostfix"]) || 'unknown.gif');
        
            return text + " " + value;
        }
    }
};


$SW.subnet.SetGridFilterToInternal = function(){
    $SW.update_polling_engine_column_visibility();
    if($SW.ScanEnable)
        return;
    var baseParams = $SW['store'].baseParams;
    if( !baseParams ) $SW['store'].baseParams = {};
    baseParams["filter[0][data][comparison]"] = "eq";
    baseParams["filter[0][data][type]"] = "numeric";
    baseParams["filter[0][data][value]"] = "1";
    baseParams["filter[0][field]"] = "Status";
};
</IPAMmaster:JsBlock>

<div id="warningbox" class="warningbox" runat=server>
    <div class=inner>
        <b><span id="warningItems" runat="server"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_207 %></span></b> 
    </div>
</div>
  
<div style="margin: 10px 10px 0 10px;">
  <div>
  <table class="titleTable" style="width: auto; margin-top: 15px;" cellpadding="0" cellspacing="0">
    <tr><td>
<% if (!CommonWebHelper.IsBreadcrumbsDisabled) { %>
    <div>
    <ul class="breadcrumb" style="margin: 0px">
        <li class="bc_item" style="margin-left: 0px">
            <a href="/Orion/Admin" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_1 %></a><img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage"
            /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3 %></a></li>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_4 %></a></li>
                <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_5 %></a></li>
<%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser)) { %>
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a></li>
<% } %>
        </ul></li>
        <li class="bc_item">
            <a href="/Orion/IPAM/Admin/Admin.Overview.aspx" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_7 %>" class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_6 %></a>
            <img src="/Orion/images/breadcrumb_arrow.gif" class="bc_itemimage">
            <ul id="bc-submenu-subnet" class="bc_submenu" style="overflow: auto; height: auto;">
                <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnet.ScanStatus.aspx" class="bc_submenulink"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_206 %></a></li>
        </ul></li>
    </ul>
    </div>
<% } %>
        <h1 style="padding-left: 0px"><%=Page.Title%></h1>
    </td></tr>
  </table>
  </div>

    <div style="margin: 10px 0px 10px 0px;">
    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_209 %>
    </div>

<IPAMlayout:DialogWindow jsID="mainWindow" helpID="subnetedit" Name="mainWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_210 %>" height="600" width="650" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="mainWindowSave" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['mainWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="ToolStripButton1" text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['mainWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['mainWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="close" runat="server">
        <handler>function(n,o){ $SW['mainWindow'].hide(); $SW.grid_refresh(); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="refresh" runat=server>
        <handler>function(n,o,info){ $SW['mainWindow'].hide(); $SW.grid_refresh(); }</handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

        <div id="gridframe" style="margin: 0px 0px 0px 0px;"> <!-- --> </div>

        <IPAMlayout:GridBox runat="server"
            width="640"
            maxBodyHeight="500"
            autoFill="false"
            autoScroll="false"
            autoHeight="false"            
            id="grid"
            emptyText="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_211 %>"
            borderVisible="true"
            RowSelection="true"
            clientEl="gridframe"
            deferEmptyText="false"
            StripeRows="true"
            UseLoadMask="True"
            listeners="{ beforerender: $SW.ext.gridSnap, cellclick: $SW.grid_cellclick, drilldown: $SW.grid_drilldown }"
            SelectorName="selector">
              <PageBar pageSize="50" displayInfo="true" listeners="{ beforerender: $SW.subnet.SetGridFilterToInternal }" />
              <Columns>
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_212 %>" width="210" dataIndex="A.Subnet.FriendlyName" isSortable="false" renderer="$SW.ext.scanStatusGridRendererDrilldown()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_158 %>" width="200" dataIndex="Status" isSortable="false" renderer="$SW.renderer_status()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_213 %>" width="180" dataIndex="ScanInstanceType" isSortable="false" renderer="$SW.renderer_type()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_214 %>" width="150" dataIndex="A.Subnet.GroupType" isSortable="false" renderer="$SW.renderer_jobtype()" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_215 %>" width="180" isSortable="true" dataIndex="A.Subnet.LastDiscovery" renderer="$SW.ext.DateRenderer($SW.ExtLocale.DateShort)" runat="server" />
                <IPAMlayout:GridColumn title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_437 %>" width="180" isSortable="true" dataIndex="EngineName" isHidden="True" renderer="$SW.subnet.pollingEngineRenderer()" runat="server" />
                <IPAMlayout:GridColumn title="&nbsp;" width="180" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.renderer_buttons()" />
              </Columns>
              <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.ScanInstance" AmbientSort="Status,ScanInstanceType,QueueTimeStamp"
                listeners="{ load: $SW.grid_reloaded, loadexception: $SW.ext.HttpProxyException }" runat="server"
                WhereClause="A.Subnet.Distance = '0' AND A.Subnet.FriendlyName IS NOT NULL"
                JoinClause="LEFT JOIN Orion.Engines AS Engine ON Engine.EngineId = A.Subnet.EngineID">
                <Fields>
                    <IPAMlayout:GridStoreField dataIndex="SubnetId" isKey="true" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ScanInstanceType" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.FriendlyName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.Engines.ServerName" runat="server"/>
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.Engines.ServerType" runat="server"/>
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.StatusIconPostfix" runat="server"/>
                    <IPAMlayout:GridStoreField dataIndex="Status" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.GroupType" dataType="int" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="A.Subnet.LastDiscovery" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="QueueTimeStamp" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="ScanStartTimeStamp" dataType="date" dateFormat="Y-m-d H:i:s" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="QueuePosition" clause="(SELECT COUNT(*) as Queue FROM IPAM.ScanInstance si WHERE A.QueueTimeStamp >= si.QueueTimeStamp 
                        AND A.Status = 2 AND si.Status = 2 AND A.SubnetId > si.SubnetId and si.ScanStartTimeStamp IS NULL) as QueuePosition" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineName" clause="Engine.ServerName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineType" clause="Engine.ServerType" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineStatus" clause="(SELECT Node.GroupStatus FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineStatus" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineDNS" clause="(SELECT Node.DNS FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineDNS" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineSysName" clause="(SELECT Node.SysName FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineSysName" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineIPAddress" clause="(SELECT Node.IPAddress FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineIPAddress" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineNodeId" clause="(SELECT Node.NodeId FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineNodeId" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineText" clause="(SELECT Node.Caption FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineText" runat="server" />
                    <IPAMlayout:GridStoreField dataIndex="EngineCommunityString" clause="(SELECT ISNULL(Node.WebCommunityString.GUID,NULL) AS EngineCommunityString FROM Orion.Nodes AS Node where Engine.IP = Node.IPAddress) AS EngineCommunityString" runat="server" />
                </Fields>
              </Store>
              </IPAMlayout:GridBox>

    <div style="margin: 10px 0px 10px 0px;">
      <div><a id="A1" href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="backto"><span class="raquo">&raquo;</span> <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_216 %></a></div>
      <div style="margin-top: 4px;"><%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_AK1_217, 
                                                      String.Format("<a href=\"/Orion/IPAM/subnets.aspx\" class=\"backto\">{0}</a>", Resources.IPAMWebContent.IPAMWEBDATA_AK1_4),
                                                      String.Format("<a href=\"/api2/ipam/ui/dhcp\" class=\"backto\">{0}</a>", Resources.IPAMWebContent.IPAMWEBDATA_AK1_5))%></div>
    </div>

</div>

</asp:Content>
