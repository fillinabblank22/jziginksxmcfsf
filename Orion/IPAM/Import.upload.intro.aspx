<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true" CodeFile="Import.upload.intro.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.ImportUploadIntro"
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_328 %>" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAM" TagName="IconHelpButton" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" %>

<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="TopRightPageLinks">
    <IPAM:IconHelpButton runat="server" ID="btnHelp" HelpUrlFragment="OrionIPAMPHImportSpreadsheet" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminStyleSheetPlaceholder" runat="server">
  <style type="text/css">
    #formbox ul { padding: 10px 10px 10px 8px; list-style-type:disc;}
    #formbox li {  padding-left: 10px; }
    #formbox input[type="checkbox"] { margin: 10px 5px 0px 25px;}
    .DownloadHyperlink { text-decoration: underline }    
  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="adminContentPlaceholder" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,ext-fileupload" Orientation="jsPostInit" runat="server">
</IPAMmaster:JsBlock>
<div style="margin-left: 10px;">
    <table width="auto" style="table-layout: fixed; margin: 20px 0px 10px 10px;" id="sw-navhdr" cellpadding="0" cellspacing="0">
        <tr><td class="crumb">
            <div style="margin-bottom: 10px;"><a href="/Orion/IPAM/Subnets.aspx"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3 %></a></div>
	        <h1><%= Page.Title %></h1>
        </td>
        </tr>
      </table>
    <div style="margin: 0px 0px 10px 10px;">
      <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_329 %>
    </div>

        <div id="formbox">
            <table border="0" cellspacing="0" cellpadding="0" width="900px">
                    <tr class="sw-form-cols-normal">
                        <td valign="top">
                        <div style="padding-top:8px;padding-bottom:8px">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_330 %>
                        </div>
                        <div style="padding:8px 0px 8px 8px">
                            <ul>
                                <li><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_331 %></li>
                                <li><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_332 %></li>
                                <li><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_333 %></li>
                            </ul>
                        </div>

                        </td>
                        <td style="white-space: nowrap;"><asp:Image ImageUrl="~/Orion/IPAM/res/images/sw-admin/import.excel.ip.png" runat="server" />
                        <br />
                        <span class="LinkArrow">&#0187;&nbsp;</span><asp:HyperLink ID="HyperLink1" NavigateUrl="res\samples\ipaddress.xls" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_334 %>" runat="server" CssClass="DownloadHyperlink" Style="font-size: 12px"/>
                        </td>
                    </tr>
                      <tr>
                        <td valign="top">
                        <asp:CheckBox id="cbShowIntroPageAgain" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_335 %>" runat="server" />
                        </td>
                        <td style="white-space: nowrap;"><asp:Image ID="Image1" ImageUrl="~/Orion/IPAM/res/images/sw-admin/import.excel.struct.png" runat="server" />
                        <br />
                        <span class="LinkArrow">&#0187;&nbsp;</span><asp:HyperLink ID="HyperLink2" NavigateUrl="res\samples\Structure.xls" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_336 %>" runat="server" CssClass="DownloadHyperlink" Style="font-size: 12px"/>
                        </td>
                      </tr>                      
                      <tr>
                        <td></td>
                          <td style="white-space: nowrap;"><asp:Image ID="Image3" ImageUrl="~/Orion/IPAM/res/images/sw-admin/import.excel.struct.png" runat="server" />
                          <br />
                          <span class="LinkArrow">&#0187;&nbsp;</span><asp:HyperLink ID="HyperLink3" NavigateUrl="res\samples\StructureAndSubnets.zip" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_336_2 %>" runat="server" CssClass="DownloadHyperlink" Style="font-size: 12px"/>
                          </td>
                        </tr>
            </table>

        </div>
        
        <div id="buttonbar" runat="server" class="sw-btn-bar-wizard" style="padding-right: 20px">
            <orion:LocalizableButton runat="server" ID="Upload" OnClick="ImportFileUploadPage_Click" LocalizedText="Next" DisplayType="Primary" CausesValidation="false" />
            <orion:LocalizableButton runat="server" ID="btnCancel" OnClick="OnCancelClick" LocalizedText="Cancel" DisplayType="Secondary" CausesValidation="false" />
        </div>
</div>
</asp:Content>
