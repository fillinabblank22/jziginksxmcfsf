using System;
using System.Web;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Master;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportStructPickcolumns : CommonPageServices
	{
        private PageImportStructPickColumns mgr;
        public ImportStructPickcolumns() { mgr = new PageImportStructPickColumns(this); }

	    protected void Page_Load(object sender, EventArgs e)
		{
            if (Page.Request.QueryString["SaveCmb"] != null)
                mgr.Save();

            if (Request.QueryString["GroupID"] != null)
                this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));
		}


        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
    }
}
