﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.DnsZoneSelector" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<IPAMmaster:JsBlock Requires="ext,sw-dhcp-manage.js,sw-dns-manage.js,ext-searchfield.js" Orientation="jsPostInit" runat="server">
$(document).ready(function(){
  var serverId = $SW.nsGet($nsId, 'ddlDnsServers');
  var serverDD = $SW.TransformDropDown(serverId, serverId, { hiddenId: serverId } );
  var zoneDD = Ext.getCmp('ddgDnsZones');
  
  $SW.IPAM.DnsZoneSelector = { ServerSelector: serverDD, ZoneSelector: zoneDD };
  $SW.IPAM.InitDnsZoneSelector(serverDD, zoneDD);
});
</IPAMmaster:JsBlock>

<div id="dnsZoneSelectorMain">
    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control" style="width: 375px !important;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td>
                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_366 %></div>
            </td>
            <td>
                <div class="sw-form-item sw-form-cb-stretch">
                    <asp:DropDownList id="ddlDnsServers" Width="375" runat="server" />
                </div>
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_367 %></div>
            </td>
            <td>
                <div class="sw-form-item sw-form-cb-stretch">
                    <IPAMlayout:DropDownGrid id="ddgDnsZones" runat="server">
                        <FindValueDataFn>function(v){ return v ? v['DnsZoneId'] : null;}</FindValueDataFn>
                        <FindValueTextFn>function(v){ return v ? v['FriendlyName'] : null;}</FindValueTextFn>
                        <ContentInitFn>
function(content) {
    var onSelect = function (g, r, el) {
        var data = g.getStore().getAt(r).data || [];
        this.setValue(data);
        this.fireEvent('select', this, data);
        this.collapse();
    };

    // bind to doubleclick
    content.on('rowdblclick', onSelect, this);
}</ContentInitFn>
                        <ConfigOptions runat="server">
                            <Options>
                                <IPAMlayout:ConfigOption Name="width" Value="auto" IsRaw="false" runat="server" />
                                <IPAMlayout:ConfigOption Name="editable" Value="false" IsRaw="true" runat="server" />
                                <IPAMlayout:ConfigOption Name="contentHeight" Value="300" IsRaw="false" runat="server" />
                                <IPAMlayout:ConfigOption Name="triggerAction" Value="all" IsRaw="false" runat="server" />
                                <IPAMlayout:ConfigOption Name="typeAhead" Value="false" IsRaw="true" runat="server" />
                                <IPAMlayout:ConfigOption Name="forceSelection" Value="true" IsRaw="true" runat="server" />
                                <IPAMlayout:ConfigOption Name="isTargetInContentChild" IsRaw="true" runat="server" Value="
function (target) {
    var search = Ext.getCmp('searchmenu'); 
    if (!search || !search.isVisible()) return false;
    return search.el ? search.el.contains(target) : false;
}"/>
                           </Options>
                        </ConfigOptions>
                        <Content>
                            <IPAMlayout:GridBox runat="server" id="zones"
                                height="300" minBodyHeight="200" maxBodyHeight="220" forceFit="true" autoHeight="false"
                                emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_475 %>" deferEmptyText="false"
                                UseLoadMask="True" RowSelection="true" borderVisible="false">
                                         
                                <TopMenu borderVisible="false">
                                    <IPAMui:ToolStripSeparator text="'->'" runat="server" />
                                    <IPAMui:ToolStripSearchField MenuId="searchmenu" Grid="zones" Width="275" Title="Search" runat="server">
                                        <IPAMui:ToolStripMenuCheckItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>"
                                            DataIndex="FriendlyName" isChecked="false" hideOnClick="false" runat="server" />
                                        <IPAMui:ToolStripMenuCheckItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_479 %>"
                                            DataIndex="StatusName" isChecked="false" hideOnClick="false" runat="server" />
                                    </IPAMui:ToolStripSearchField>
                                </TopMenu>
                                <PageBar pageSize="24" displayInfo="true" />
                                <Columns>
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>" width="140" isSortable="true" isMenuDisabled="true" dataIndex="FriendlyName" renderer="$SW.IPAM.DnsZoneRenderer()"  runat="server" />
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_479 %>" width="120" isSortable="true" isMenuDisabled="true" dataIndex="StatusName" runat="server" renderer="function (value, meta, r) {return r.data.StatusShortDescription;}" /> 
                                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_253 %>" width="120" isSortable="true" isMenuDisabled="true" dataIndex="A.DnsZone.ZoneType" renderer="$SW.IPAM.DnsZoneTypeRenderer()" runat="server"   />
                                </Columns>
                                <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="AddressN, FriendlyName" runat="server">                  
                                    <Fields>
                                        <IPAMlayout:GridStoreField dataIndex="GroupId" isKey="true" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="DnsZoneId" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="FriendlyName" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="StatusName" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="StatusShortDescription" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="GroupIconPrefix" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="StatusIconPostfix" runat="server" />
                                        <IPAMlayout:GridStoreField dataIndex="A.DnsZone.ZoneType" runat="server" />
                                     </Fields>
                                </Store>
                            </IPAMlayout:GridBox>
                        </Content>
                    </IPAMlayout:DropDownGrid>
                </div>
            </td>
            <td />
        </tr>
    </table>
</div>