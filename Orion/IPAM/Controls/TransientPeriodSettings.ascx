﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.TransientPeriodSettings" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">
if(!$SW.Transient) $SW.Transient = {
    nsId: undefined,
    RadioId: '',
    DurationId: '',
    SkipValFn: function(){ <%=ClientSkipValidation%> },
    
    RadioChecked: function(){
        var r = $SW.ns$(this.nsId,this.RadioId);
        return (r && r[0] && r[0].checked);
    },
    
    ValidateDurationRequire: function(value){
        var v = $SW.ns$(this.nsId,this.DurationId);
        if( v && v.val()!='' ) return true;
        return (value!='');
    },
    ValidateDurationRange: function(value){
        var v = $SW.ns$(this.nsId,this.DurationId);
        if( !v ) return false;
        var val = v.val() || value;
        return (val >= 0.25 && val <= 340);
    },

    OnDurationRequire: function(src,args){
        if(this.SkipValFn() == true){ return args.IsValid = true; }
        if(!this.RadioChecked()){ return args.IsValid = true; }
        return args.IsValid = this.ValidateDurationRequire(args.Value);
    },
    OnDurationValidate: function(src,args){
        if(this.SkipValFn() == true){ return args.IsValid = true; }
        if(!this.RadioChecked()){ return args.IsValid = true; }
        return args.IsValid = this.ValidateDurationRange(args.Value);
    }
};
</script>

<IPAMmaster:JsBlock ID="JsBlock" Orientation="jsPostInit" runat="server" Requires="ext,ext-quicktips">
$(document).ready( function(){ try {
    $SW.Transient.nsId = $nsId;
    $SW.Transient.RadioId = 'rbCustom';
    $SW.Transient.DurationId = 'txtDuration';
}catch(err){} } );
</IPAMmaster:JsBlock>

<div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label" style="line-height: 22px;">
            <asp:RadioButton ID="rbInherit" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_90 %>"
                GroupName="grpTransient" runat="server" />
        </div></td>
        <td colspan="2"><div class="sw-field-item sw-form-clue">
            <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_91,
                                              "<a href=\"/Orion/IPAM/Admin/Admin.ScanSettings.aspx\" target=\"_blank\" style=\"text-decoration: underline;\">", "</a>")%>                            
        </div></td>
    </tr>
    <tr>
        <td colspan="3"><div class="sw-field-label" style="line-height: 22px;">
            <asp:RadioButton ID="rbUnlimited" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_92 %>"
                GroupName="grpTransient" runat="server" />
        </div></td>
    </tr>
    <tr>
        <td><div class="sw-field-label" style="line-height: 22px;">
            <asp:RadioButton ID="rbCustom" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_93 %>"
                GroupName="grpTransient" runat="server" />
        </div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtDuration" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td><asp:CustomValidator ID="TransientDurationRequre" runat="server" Display="Dynamic"
                  ControlToValidate="txtDuration"
                  OnServerValidate="OnDurationRequire"
                  ClientValidationFunction="$SW.Transient.OnDurationRequire"
                  ValidateEmptyText="true"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_13 %>"
            /><asp:CustomValidator ID="TransientDurationRange" runat="server" Display="Dynamic"
                  ControlToValidate="txtDuration"
                  OnServerValidate="OnDurationValidate"
                  ClientValidationFunction="$SW.Transient.OnDurationValidate"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_14 %>"
            /></td>
    </tr>
</table></div>
