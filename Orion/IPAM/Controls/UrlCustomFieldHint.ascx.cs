﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_IPAM_Controls_UrlCustomFieldHint : System.Web.UI.UserControl
{
    public int ColumnsTotalCount { get; set; }
    public int ColumnIndex { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ColumnsTotalCount == 0)
            return;

        int preColCount = ColumnIndex;
        int postColCount = ColumnsTotalCount - ColumnIndex - 1;

        BindRepeater(PreColumnRepeater, preColCount);
        BindRepeater(PostColumnRepeater, postColCount);
    }

    void BindRepeater(Repeater repeater, int columnsCount)
    {
        if (columnsCount <= 0)
            return;

        List<int> list = new List<int>();
        for (int i = 0; i < columnsCount; i++)
        {
            list.Add(i);
        }

        repeater.DataSource = list;
        repeater.DataBind();
    }
}
