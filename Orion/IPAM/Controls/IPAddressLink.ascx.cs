﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class IPAddressLink : System.Web.UI.UserControl
    {
        #region Properties

        public IPNode IPNode { get; set; }

        public string ValueTemplate { get; set; }

        private bool isValuesSet { get; set; }

        #endregion // Properties

        #region Constructors

        public IPAddressLink()
        {
            this.ValueTemplate = "IP: {0}; Alias: {1}; Status {2};";
        }

        #endregion // Constructors

        #region Events

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.Visible && !this.isValuesSet && (this.IPNode != null))
            {
                this.isValuesSet = true;
                SetValues(this.IPNode);
            }
        }

        #endregion // Events

        #region Methods

        private void SetValues(IPNode ipNode)
        {
            if (this.ipAddressValue != null)
            {
                string status = (ipNode.Status == IPNodeStatus.Unknown) ? "\xA0" :
                    LocalizationHelper.GetEnumDisplayString(typeof(IPNodeStatus), ipNode.Status);

                string value = string.Format(this.ValueTemplate,
                    ipNode.IPAddress, ipNode.Alias, status);

                var label = new LiteralControl(HttpUtility.HtmlEncode(value));

                this.ipAddressValue.Controls.Clear();
                this.ipAddressValue.Controls.Add(label);
            }

            Control img = FindControl(ipNode.Status.ToString());
            if (img != null)
            {
                img.Visible = true;
            }
        }

        #endregion // Methods
    }
}