﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPv6.IPv6AddressEditor.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.IPv6AddressEditor" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMctrl" Assembly="SolarWinds.IPAM.Web.Common" Namespace="SolarWinds.IPAM.Web.Common.Controls" %>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
#prefix-address .sw-form-clue { font-size: 12px; font-style: normal; white-space: nowrap; margin-bottom: 6px; }
#prefix-address a { text-decoration: none; color: #336699; }
#prefix-address .readonly { color: gray; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock ID="initAddressBox" Requires="ext,ext-quicktips,sw-ipv6-manage.js" Orientation="jsPostInit" runat="server" />

<div id="prefix-address">
    <div id="prefixes-list" class="sw-form-clue">
        <ul>
            <li class="item-type-IPv6Subnet">
                <div class="item-name"><asp:Label ID="txtSubnetName" runat="server" /></div>
                <div class="item-address"><asp:Label ID="txtSubnetAddress" runat="server" style="padding-left: 20px;" /></div>
            </li>
        </ul>
    </div>
    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAddress" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_335 %>" runat="server" />
            </td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtAddress" Text="::" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td>
                <asp:RequiredFieldValidator ID="IPv6AddressRequireValidator" runat="server" Display="Dynamic"
                    ControlToValidate="txtAddress" ValidateEmptyText="true"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_437 %>" />
                <asp:CustomValidator ID="IPv6AddressCustomValidator" runat="server" Display="Dynamic"
                    ControlToValidate="txtAddress" ValidateEmptyText="true"
                    OnServerValidate="OnAddressValidator"
                    ClientValidationFunction="$SW.IPv6.IPv6AddressValidator" EnableClientScript="true"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_437 %>" />
            </td>
        </tr>
    </table>        
</div>