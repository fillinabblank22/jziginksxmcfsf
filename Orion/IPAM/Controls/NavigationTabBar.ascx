<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NavigationTabBar.ascx.cs" Inherits="Orion_IPAM_Controls_NavigationTabBar" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<%
    this.AddTab("IPAM Summary", VirtualPathUtility.ToAbsolute("~/Orion/IPAM/IPAMSummaryView.aspx"), IsPageRegexMatch("(^|/)IPAMSummaryView.aspx$"));
    this.AddTab("Manage Subnets & IP Addresses", VirtualPathUtility.ToAbsolute("~/Orion/IPAM/subnets.aspx"), IsPageRegexMatch("(^|/)subnets[.]aspx$"), SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.ReadOnly) );
    this.AddTab("DHCP & DNS Management", VirtualPathUtility.ToAbsolute("~/api2/ipam/ui/dhcp"), IsPageRegexMatch("(^|/)Dhcp[.]Management[.]aspx$"), SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.ReadOnly));
    /* this.AddTab(this.PageTitleOverride ?? Page.Title, Request.Url.AbsolutePath, true); */
%>

<div class="sw-tabs"><div class="x-tab-panel-header x-unselectable x-tab-panel-header-plain" style="width: 100%;">

<div class="x-tab-strip-wrap">
  <%-- 
  <div class="x-tab-strip-spacer" style="display: none;"><!-- --></div>
  --%>

  <ul class="x-tab-strip x-tab-strip-top">

    <asp:Literal ID="TabPH" EnableViewState="False" runat="server" />

    <li class="x-tab-edge"></li>
    <div class="x-clear"><!-- --></div>
  </ul>

  <div class="x-tab-strip-spacer" style="border-bottom: none;<%= NoRightBorder ? " border-right: none;" : "" %>"><!-- --></div>
</div>

</div></div>

<IPAMmaster:CssBlock Orientation="top" runat="server">
.sw-tabs .x-tab-strip-text { font-size: 13px !important; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
$(document).ready(function(){
  $(".sw-tabs li.sw-tab").bind('mouseover', function(){ $(this).addClass('x-tab-strip-over'); });
  $(".sw-tabs li.sw-tab").bind('mouseout', function(){ $(this).removeClass('x-tab-strip-over'); });
});
</IPAMmaster:JsBlock>
