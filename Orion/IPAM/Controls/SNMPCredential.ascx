﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.SNMPCredential" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock ID="OnReadyJsBlock" Requires="sw-admin-snmpcred.js" Orientation=jsPostInit runat=server />

<div id="SNMPCredentialWarningBox" class="warningbox" runat="server">
  <div class="inner">
    All SNMP credentials are sent in clear text. Consider only updating credentials through a browser while locally logged into the IPAM server or over an HTTPS connection.
  </div>
</div>

<div id="SNMPContent" runat="server">

<div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Display Name:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtDisplayName" CssClass="x-form-text x-form-field" Text="New Credential" runat="server" />
        </div></td>
        <td>
        
        <asp:ImageButton style="margin-left: 8px;" AlternateText="Suggest" ID="FixName" ImageUrl="~/orion/ipam/res/images/sw/button.suggest.gif" visible=false OnClick="btnFixName_Click" runat="server"/>
        
        <asp:RequiredFieldValidator
            ID="DisplayNameRequiredValidator"
            runat="server"
            Display="Dynamic"
            ControlToValidate="txtDisplayName"
            ErrorMessage="Display Name must not be blank"></asp:RequiredFieldValidator>
        <asp:CustomValidator 
            ID="NameExists"
            runat="server"
            Display="Dynamic"
            ControlToValidate="txtDisplayName"
            OnServerValidate="OnNameExistsValidate"
            ErrorMessage="The display name already exists, please select another name"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            SNMP Version:
        </div></td>
        <td><div class=sw-form-item>
            <asp:DropDownList ID="ddlSNMPVersion" runat="server">
                <asp:ListItem Selected=true Value="2">SNMP v2c</asp:ListItem>
            </asp:DropDownList>
        </div></td>
    </tr>
</table></div>

<div><!-- ie6 --></div>

<div id=layoutv2only><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td></td>
        <td><div class=sw-form-item>
            <asp:CheckBox ID="cbVersion2Only" Text="Use SNMPv2c only"  runat="server"/>
        </div></td>
    </tr>
</table></div>

<div><!-- ie6 --></div>

<div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            SNMP Port:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtSNMPPort" CssClass="x-form-text x-form-field" runat="server" Text="161" />
        </div>
        </td>
        <td>
            <asp:CompareValidator ID="SNMPPortTypeValidator" runat="server"
                ControlToValidate="txtSNMPPort"
                Operator="GreaterThan"
                ValueToCompare="0"
                Type="Integer"
                Display="Dynamic"
                ErrorMessage="SNMP Port must be a number greater than zero."></asp:CompareValidator>
            <asp:CompareValidator ID="SNMPPortTypeValidator2" runat="server"
                ControlToValidate="txtSNMPPort"
                Operator="LessThanEqual"
                ValueToCompare="65535"
                Type="Integer"
                Display="Dynamic"
                ErrorMessage="SNMP Port must be a number less than 65536." ></asp:CompareValidator>
             <asp:RequiredFieldValidator ID="SNMPPortRequiredValidator" runat="server"
                ControlToValidate="txtSNMPPort"
                Display="Dynamic"
                ErrorMessage="SNMP Port must not be blank"></asp:RequiredFieldValidator>
        </td>
    </tr>
</table></div>

<div><!-- ie6 --></div>

<div id="layoutSNMPv1_2">
<div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Community String:<br />
            (read only)
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtCommunityString" CssClass="x-form-text x-form-field" runat="server" Text="" />
        </div></td>
        <td>
            <asp:RequiredFieldValidator ID="CommunityStringRequiredValidator" Display="Dynamic" runat="server"
                ControlToValidate="txtCommunityString" ErrorMessage="Community String must not be blank"></asp:RequiredFieldValidator>
        </td>
    </tr>
</table></div>
</div>

<div><!-- ie6 --></div>

<div id="layoutSNMPv3">
<div><table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
    <tr class=sw-form-cols-normal>
        <td class=sw-form-col-label></td>
        <td class=sw-form-col-control></td>
        <td class=sw-form-col-comment></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            SNMPv3 User Name:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtSNMPv3Username" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            SNMPv3 Context:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtSNMPv3Context" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
    </tr>
    <tr>
        <td colspan=3><div class=sw-form-subheader>
           SNMPv3 Authentication
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Method:
        </div></td>
        <td><div class=sw-form-item>
            <asp:DropDownList ID="ddlAuthMethod" runat="server">
                <asp:ListItem Selected=true Value="None">None</asp:ListItem>
            </asp:DropDownList>
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Password / Key:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="tbPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
        </div></td>
    </tr>
    <tr>
        <td colspan=3><div class=sw-form-subheader>
            SNMPv3 Privacy / Encryption
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Method:
        </div></td>
        <td><div class=sw-form-item>
            <asp:DropDownList ID="ddlSecurityMethod" runat="server">
                <asp:ListItem Selected=true Value="None">None</asp:ListItem>
            </asp:DropDownList> 
        </div></td>
    </tr>
    <tr>
        <td><div class=sw-field-label>
            Password / Key:
        </div></td>
        <td><div class=sw-form-item>
            <asp:TextBox ID="txtSecurityPasswordKey" runat="server" CssClass="x-form-text x-form-field" TextMode="Password" />
        </div></td>
    </tr>
    </table></div>
</div>

</div>
