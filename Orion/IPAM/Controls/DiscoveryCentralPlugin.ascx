﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscoveryCentralPlugin.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.DiscoveryCentralPlugin" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" TagPrefix="orion" TagName="ManagedNetObjectsInfo" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>

<Content>
    <%--Commenting out IPAT  Changes--%>
   <%--   <script type="text/javascript">
          var navigateButtons = [];
          navigateButtons.push($('#' + '<%= dhcpServerUrlId.ClientID %>'));
          navigateButtons.push($('#' + '<%= dnsServerUrlId.ClientID %>'));
          $.each(navigateButtons, function(idx, btn) {
              $(btn).on("click", function(event) {
                  if ('<%= this.isFreeTool %>' == "True" && '<%= this.isOtherModuleInstalled %>' == "False") {
                      alert('<%= Resources.IPAMWebContent.IPAMWEBDATA_DF1_45 %>');
                      return false;
                  }
              });
          });
         </script>--%>
    <%--Commenting out IPAT  Changes--%>
</Content>
<div class="coreDiscoveryIcon">
<asp:Image runat="server" ImageUrl="~/Orion/IPAM/res/images/DiscoveryCentralPlugin/ip_purple_32x32.png"/>
</div>


<div class="coreDiscoveryPluginBody">
    
    <h2>
        <%= IPAMWebContent.IPAMWEBDATA_TM0_1 %></h2>
    <div>
        <%= IPAMWebContent.IPAMWEBDATA_TM0_2 %></div>
    <span class="LinkArrow">&#0187;</span>
    <orion:HelpLink ID="DiscoveryHelpLink" runat="server" HelpUrlFragment="OrionIPAMPHDiscoveryCentralPlugin"
        HelpDescription="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_3 %>" CssClass="helpLink" />
        
    <orion:ManagedNetObjectsInfo ID="ipNodesInfo" runat="server" EntityName="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_1 %>" NumberOfElements="<%# IPNodeCount %>" />
    
    <div class="sw-btn-bar">
        <orion:LocalizableButtonLink ID="addSubnetUrlId" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_DF1_28 %>" 
                                     NavigateUrl="~/Orion/IPAM/AddSubnetIPAddresses.aspx" DisplayType="Secondary" />
        <orion:LocalizableButtonLink ID="dhcpServerUrlId" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_6 %>" 
                                     NavigateUrl="~/Orion/IPAM/dhcp.server.add.aspx" DisplayType="Secondary" />
        <orion:LocalizableButtonLink ID="dnsServerUrlId" runat="server" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_7 %>" 
                                     NavigateUrl="~/Orion/IPAM/dns.server.add.aspx" DisplayType="Secondary" />
    </div>
</div>
