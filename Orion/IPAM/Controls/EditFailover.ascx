﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.EditFailover" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">

    (function () {
            var focusElement;
                function restoreFocus(){
                    if(focusElement){
                        if(focusElement.id){
                            $('#'+focusElement.id).focus();
                        } else {
                    $(focusElement).focus();
        }
    }
}

$(document).ready(function () {
    $(document).on('focusin', function(objectData){
        focusElement = objectData.currentTarget.activeElement;
    });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(restoreFocus);
    });
    })();

</script>
<style>
    div.sw-form-invalid-icon { right: -16px; top: 1px;}
</style>
    
<div class="container">
    <input type="button" value="X" id="btnRemove" class="sw-btn-primary sw-btn" runat="server" onserverclick="Remove_OnClick" style="float: right; padding-top: 1px" CausesValidation = "false" />
    <div>
        <div class="menuItem">
            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_849 %></div>
            <div class="sw-form-item">
                <asp:TextBox ID="txtRelationshipName" runat="server" CssClass="x-form-text x-form-field" Width="150" Height="20" Enabled="False"/>
                <asp:CustomValidator
                    ID="TextValidator"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="txtRelationshipName"
                    OnServerValidate="ValidateRelationshipName"
                    ValidateEmptyText="true"
                    ErrorMessage="Name can't be empty">
                </asp:CustomValidator>
            </div>
        </div>
        <div class="menuItem" style="padding-top: 9.4px;">
            <br />
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_865 %>
        </div>
        <div class="menuItem">
            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_457 %></div>
            <div class="sw-form-item">
                <asp:DropDownList ID="ddlFailoverMode" runat="server" CssClass="x-form-text x-form-field" Width="150" Height="25" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="FailoverModeOnSelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
            <div class="menuItem" style="padding-top: 9.4px;">
                <br />
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_863 %>
                </div>
        <div class="menuItem">
            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_851 %></div>
            <div class="sw-form-item">
                <asp:DropDownList ID="ddlPartnerServers" runat="server" CssClass="x-form-text x-form-field" Width="150" Height="25" Enabled="False" OnSelectedIndexChanged="ControlOnValueChaged" AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <br style="clear: left;" />
    </div>
    <div style="float: left; width: 100%">
        <div class="menuItem">
            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_852 %></div>
            <div class="sw-form-item">
                <asp:ListBox ID="lbAvailableScopes" runat="server" CssClass="x-form-text x-form-field x-form-focus" Height="100"></asp:ListBox>
            </div>
        </div>
        <div class="menuItem">
            <br/>
            <br/>
            <input type="button" id="btnAddScope" class="sw-btn-primary sw-btn" runat="server" value=">" width="20" height="20" onserverclick="AddScope_Click" CausesValidation = "false" />
            <br />
            <input type="button" id="btnRemoveScope" class="sw-btn-primary sw-btn" runat="server" value="<" width="20" height="20" onserverclick="RemoveScope_Click" CausesValidation="False" />
        </div>
        <div class="menuItem">
            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_853 %></div>
            <div class="sw-form-item">
                <asp:ListBox ID="lbSelectedScopes" runat="server" CssClass="x-form-text x-form-field x-form-focus" Height="100"></asp:ListBox>
                <asp:CustomValidator 
                    ID="SelectedScopesValidator"
                    runat="server"
                    Display="Dynamic"
                    ControlToValidate="lbSelectedScopes"
                    OnServerValidate="SelectedScopesValidate"
                    ValidateEmptyText="true"
                    ErrorMessage="Selected scopes list can't be empty">
                </asp:CustomValidator>
            </div>
        </div>
    </div>
    <div>
        <div class="menuItem">
            <asp:Panel runat="server" ID="pnlLoadBalance" Visible="True">
                <table>
                    <tr class="row">
                        <td>
                            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_854 %></div>
                        </td>
                        <td>
                            <div class="sw-form-item">
                                <asp:TextBox ID="txtLoadBalanceLocalServerPercentage" Width="20" runat="server" CssClass="x-form-text x-form-field" Height="20" Enabled="False"  OnTextChanged="ControlOnValueChaged" AutoPostBack="True"/>
                                <asp:CustomValidator
                                    ID="LoadBalanceLocalValidator"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtLoadBalanceLocalServerPercentage"
                                    OnServerValidate="ValidateLocalServerLoadBalance"
                                    ValidateEmptyText="true"
                                    ErrorMessage="Invalid percentage value">
                                </asp:CustomValidator>
                                <br />
                            </div>
                        </td>
                    </tr>
                    <tr class="row">
                        <td>
                            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_855 %></div>
                        </td>

                        <td>
                            <div class="sw-form-item">
                                <asp:TextBox ID="txtLoadBalancePartnerServerPercentage" Width="20" runat="server" CssClass="x-form-text x-form-field" Height="20" Enabled="False" OnTextChanged="ControlOnValueChaged" AutoPostBack="True"/>
                                <asp:CustomValidator
                                    ID="LoadBalancePartnerValidator"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtLoadBalancePartnerServerPercentage"
                                    OnServerValidate="ValidatePartnerServerLoadBalance"
                                    ValidateEmptyText="true"
                                    ErrorMessage="Invalid percentage value">
                                </asp:CustomValidator>
                                <br />
                            </div>
                        </td>
                    </tr>
                    <tr class="row">
                        <td>
                            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_856 %></div>
                        </td>
                        <td>
                            <div class="sw-form-item">
                                <asp:TextBox ID="txtSwitchoverInterval" Width="20" runat="server" CssClass="x-form-text x-form-field" Height="20" Enabled="False" OnTextChanged="ControlOnValueChaged" AutoPostBack="True" />
                                <asp:CustomValidator
                                    ID="SwitchoverIntervalValidator"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtSwitchoverInterval"
                                    OnServerValidate="SwitchoverIntervalValidate"
                                    ValidateEmptyText="true"
                                    ErrorMessage="Invalid value">
                                </asp:CustomValidator>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlHotStandBy">
                <table>                  
                    <tr class="row">
                        <td>
                            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_861 %></div>
                        </td>
                        <td>
                            <div class="sw-form-item">
                                <asp:TextBox ID="txtAddressesReserved" Width="20" runat="server" CssClass="x-form-text x-form-field" Height="20" Enabled="False" OnTextChanged="ControlOnValueChaged" AutoPostBack="True" />
                                <asp:CustomValidator
                                    ID="AddressesReservedValidator"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtAddressesReserved"
                                    OnServerValidate="ValidateLocalServerLoadBalance"
                                    ValidateEmptyText="true"
                                    ErrorMessage="Invalid value">
                                </asp:CustomValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div>
                <table>                  
                    <tr class="row">
                        <td>
                            <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_858 %></div>
                        </td>
                        <td>
                            <div class="sw-form-item" style="padding-left: 8.9px;">
                                <asp:TextBox ID="txtSharedSecret" Width="110" runat="server" CssClass="x-form-text x-form-field" Height="20" Enabled="False" OnTextChanged="ControlOnValueChaged" AutoPostBack="True"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

