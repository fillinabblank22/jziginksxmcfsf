﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.DnsRecordsGrid" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<IPAMmaster:JsBlock Requires="ext,ext-ux,ext-quicktips,sw-ux-wait.js,sw-dns-constants.js,sw-dns-manage.js,sw-dnsrecord-manage.js" Orientation="jsPostInit" runat="server" >
    
    if (!$SW.IPAM.DnsRecords) $SW.IPAM.DnsRecords = {};
    
    $SW.IPAM.DnsRecords.PtrRecordCreationEditor = function (o) {
            var opt = $.extend(o, { xtype: 'checkbox', id: 'checkPTR', cls: 'checkRecord' });
            var tf = new Ext.form.Checkbox(opt);
            $SW.IPAM.DnsRecords.DnsPtrRecordCreationField = tf;
            return tf;
        };
    
     $SW.IPAM.DnsRecords.PtrRecordCreationRenderer = function () {
            return function (value, meta, r) {
                var checkId = "id_" + r.data['DnsRecordId'];
                var isCheck = (r.data['PTRCreation'] && parseInt(r.data['PTRCreation']) > 0) ? true : false;
                return (isCheck) ? '<input type="checkbox" id="'+ checkId +'" checked= "checked" disabled="disabled" />' : '<input type="checkbox" id="'+ checkId +'"  disabled="disabled" />';
            };
        }

</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock runat="server">
.checkRecord { margin-left: 13px !important; margin-top: 3px !important; }
</IPAMmaster:CssBlock>

<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
    <input type="hidden" id="isDemoMode" />
<%}%>

<div id="DnsRecordsGridMain">
  <div style="padding-top:10px">
    <span><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_291%></span>
  </div>
  <div style="display: none;">
    <IPAMlayout:DropDownGrid id="ddgDnsZonesX" runat="server">
        <FindValueDataFn>function(v){ return v ? v['DnsZoneId'] : null;}</FindValueDataFn>
        <FindValueTextFn>function(v){ return v ? v['FriendlyName'] : null;}</FindValueTextFn>
        <ContentInitFn>
function(content) {
    var onSelect = function (g, r, el) {
    var data = g.getStore().getAt(r).data || [];
    this.setValue(data);
    this.fireEvent('select', this, data);
    this.collapse();
};
content.on('rowdblclick', onSelect, this); // bind to doubleclick
}</ContentInitFn>
        <ConfigOptions runat="server">
            <Options>
                <IPAMlayout:ConfigOption Name="width" Value="auto" IsRaw="false" runat="server" />
                <IPAMlayout:ConfigOption Name="editable" Value="false" IsRaw="true" runat="server" />
                <IPAMlayout:ConfigOption Name="contentHeight" Value="300" IsRaw="false" runat="server" />
                <IPAMlayout:ConfigOption Name="minWidth" Value="500" IsRaw="false" runat="server" />
                <IPAMlayout:ConfigOption Name="triggerAction" Value="all" IsRaw="false" runat="server" />
                <IPAMlayout:ConfigOption Name="typeAhead" Value="false" IsRaw="true" runat="server" />
                <IPAMlayout:ConfigOption Name="forceSelection" Value="true" IsRaw="true" runat="server" />
                <IPAMlayout:ConfigOption Name="isTargetInContentChild" IsRaw="true" runat="server" Value="
function (target) {
    var search = Ext.getCmp('searchmenuX'); 
    if (!search || !search.isVisible()) return false;
    return search.el ? search.el.contains(target) : false;
}"/>
            </Options>
        </ConfigOptions>
        <Content>
            <IPAMlayout:GridBox runat="server" id="zones"
                height="300" minBodyHeight="200" maxBodyHeight="220" forceFit="true" autoHeight="false"
                emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_475 %>" deferEmptyText="false"
                UseLoadMask="True" RowSelection="true" borderVisible="false">
                                         
                <TopMenu borderVisible="false">
                    <IPAMui:ToolStripSeparator text="'->'" runat="server" />
                    <IPAMui:ToolStripSearchField MenuId="searchmenuX" Grid="zones" Width="275" Title="Search" runat="server">
                        <IPAMui:ToolStripMenuCheckItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>"
                            DataIndex="FriendlyName" isChecked="false" hideOnClick="false" runat="server" />
                        <IPAMui:ToolStripMenuCheckItem text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_479 %>"
                            DataIndex="StatusName" isChecked="false" hideOnClick="false" runat="server" />
                    </IPAMui:ToolStripSearchField>
                </TopMenu>
                <PageBar pageSize="24" displayInfo="true" />
                <Columns>
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_251 %>" width="140" isSortable="true" isMenuDisabled="true" dataIndex="FriendlyName" renderer="$SW.IPAM.DnsZoneRenderer()"  runat="server" />
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_479 %>" width="120" isSortable="true" isMenuDisabled="true" dataIndex="StatusName" runat="server" renderer="function (value, meta, r) {return r.data.StatusShortDescription;}" /> 
                    <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_253 %>" width="120" isSortable="true" isMenuDisabled="true" dataIndex="A.DnsZone.ZoneType" renderer="$SW.IPAM.DnsZoneTypeRenderer()" runat="server"   />
                </Columns>
                <Store id="store" dataRoot="rows" autoLoad="false" proxyUrl="extdataprovider.ashx" proxyEntity="IPAM.GroupNode" AmbientSort="AddressN, FriendlyName" runat="server">                  
                    <Fields>
                        <IPAMlayout:GridStoreField dataIndex="GroupId" isKey="true" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="DnsZoneId" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="FriendlyName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusName" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusShortDescription" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="GroupIconPrefix" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="StatusIconPostfix" runat="server" />
                        <IPAMlayout:GridStoreField dataIndex="A.DnsZone.ZoneType" runat="server" />
                        </Fields>
                </Store>
            </IPAMlayout:GridBox>
        </Content>
    </IPAMlayout:DropDownGrid>
  </div>
<div style="group-box">
  <div id="box"></div>
  <IPAMlayout:Panel runat="server" RenderTo="box" Layout="fit" Border="true">
    <IPAMlayout:GridBox runat="server" ID="DnsRecordsGridBox" UseLoadMask="True" forceFit="true" layout="fit"
        autoHeight="false" height="240" deferEmptyText="false" RowSelection="true" 
        emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_368 %>" borderVisible="false"
        listeners="{cellclick: $SW.IPAM.DnsRecords.grid_cellclick }">
        <ConfigOptions runat="server">
            <Options>
                <IPAMlayout:ConfigOption Name="plugins" Value="sw-dnsrecord-editor" runat="server" />
            </Options>
        </ConfigOptions>
        <ViewOptions runat="server">
            <Options>
                <IPAMlayout:ConfigOption Name="markDirty" Value="false" IsRaw="true" runat="server" />
            </Options>
        </ViewOptions>
        <PageBar pageSize="10" displayInfo="true" />
        <TopMenu borderVisible="false">
            <IPAMui:ToolStripMenuItem id="btnAdd" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_369 %>" runat="server">
            <handler>function() { $SW.IPAM.DnsRecords.AddRecord( Ext.getCmp('DnsRecordsGridBox'), '%IpAddressN%' ); }</handler>
            </IPAMui:ToolStripMenuItem>         
        </TopMenu>
        <Columns>
            <IPAMlayout:GridColumn title="&nbsp;" width="50" isFixed="true" isHideable="false" runat="server"
                                   renderer="$SW.IPAM.DnsRecords.DeleteImgButtonRenderer()" />
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_370 %>" width="175" isSortable="true" dataIndex="Name" runat="server" 
                                   editor = "$SW.IPAM.DnsRecords.DnsRecordNameEditor()" />
            <IPAMlayout:GridColumn title="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_93 %>" width="90" isSortable="true" dataIndex="Type" runat="server"
                                   renderer="$SW.IPAM.DnsRecords.DnsRecordTypeRenderer()"
                                   editor="$SW.IPAM.DnsRecords.DnsRecordTypeEditor()"/>
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_371 %>" isSortable="true" dataIndex="Data" runat="server"
                                   editor="{ xtype: 'textfield' }"/>
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_283 %>" width="100" isHideable="false" runat="server" dataIndex="ServerId"
                                   renderer="$SW.IPAM.DnsRecords.DnsServerRenderer()"
                                   editor="$SW.IPAM.DnsRecords.DnsServerEditor()"/>
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_284 %>" width="100" isHideable="false" runat="server" dataIndex="ZoneId"
                                   renderer="$SW.IPAM.DnsRecords.DnsZoneRenderer()"
                                   editor="$SW.IPAM.DnsRecords.DnsZoneEditor()"/>
            <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_30 %>" width="57" isHideable="false" runat="server" dataIndex="PTRCreation" isSortable="false" renderer="$SW.IPAM.DnsRecords.PtrRecordCreationRenderer()"
                                   editor = "$SW.IPAM.DnsRecords.PtrRecordCreationEditor()" />
        </Columns>
        <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx"
            proxyEntity="IPAM.DnsRecord" AmbientSort="DnsRecordId"
            JoinClause="JOIN IPAM.DnsServer AS Server ON Server.NodeId = {0}.DnsZone.NodeId LEFT OUTER JOIN IPAM.DnsRecord P ON A.Name = P.Data AND A.IPAddressN = P.IPAddressN AND P.DnsZone.NodeId = {0}.DnsZone.NodeId AND P.Type = 12 "
            WhereClause="{0}.DnsZone.GroupNode.Distance='0' AND Server.GroupNode.Distance='0' AND A.Type = 1"
            listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
            <Fields> 
                <IPAMlayout:GridStoreField dataIndex="DnsRecordId" isKey="True" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Name" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Type" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="Data" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="IPAddressN" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ZoneId" clause="A.DnsZone.DnsZoneId AS ZoneId" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ZoneName" clause="A.DnsZone.GroupNode.FriendlyName AS ZoneName" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ServerId" clause="A.DnsZone.NodeId AS ServerId" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="ServerName" clause="Server.GroupNode.FriendlyName AS ServerName" runat="server" />
                <IPAMlayout:GridStoreField dataIndex="PTRCreation" clause="IsNull(P.DnsRecordId,-1) AS PTRCreation"  runat="server" />
            </Fields>
        </Store>
    </IPAMlayout:GridBox>
  </IPAMlayout:Panel>
</div>

</div>
