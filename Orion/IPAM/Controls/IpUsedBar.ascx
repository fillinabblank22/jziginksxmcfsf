<%@ Control Language="C#" ClassName="IpUsedBar" %>
<%@ Import Namespace="SolarWinds.IPAM.Web.Common.Utility" %>

<script runat="server">
    private double _percentused;
    private string _className;

	public double Value
    {
        get { return _percentused; }
        set { _percentused = value; }
    }

    public string ClassName
    {
        get { return _className; }
        set { _className = value; }
    }
</script>
<div class="InlineBar <%=ClassName%>"><h3><div style="width:<%= Value.ToString().Replace(",",".") %>%; ; height: 10px; font-size: 7px;" >&nbsp;</div></h3></div>
