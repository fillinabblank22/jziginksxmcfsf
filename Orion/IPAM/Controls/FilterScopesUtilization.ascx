<%@ Control Language="C#" ClassName="FilterScopesUtilization" %>

<script runat="server">
	public TextBox FilterTextBox { get { return Filter; } }
</script>

<p>
	<b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_652 %></b><br />
	<asp:TextBox runat="server" ID="Filter" Width="330"></asp:TextBox>
</p>

<p><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_653 %></p>

<orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
	<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_654 %></b></TitleTemplate>
	
	<BlockTemplate>
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_655 %>

		<orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
			<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_656 %></b></TitleTemplate>
			<BlockTemplate>
		        <%= Resources.IPAMWebContent.IPAMWEBDATA_MR1_40 %>		
			</BlockTemplate>
		</orion:CollapsePanel>

	</BlockTemplate>
</orion:CollapsePanel>
