﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.IPAM.BusinessObjects;
using System.Text;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class IPv6CompoundAddress : System.Web.UI.UserControl
    {
        #region Constants

        private const string CustomValidatorScriptTemplate =
            @"$SW.IPv6.SetIPv6AddressCustomValidator($('#{0}')[0], {{ type: '{1}', parent: '{2}', size: s, pid: {3}, oid: {4}, clusterId: {5} }});";

        private const string IPv6PrefixTemplate = 
            "<div class=\"item-name\">{0}</div><div class=\"item-address\">{1}</div>";

        #endregion // Constants

        #region Properties

        public string IPv6Value
        {
            get
            {
                string result = string.Empty;
                if (this.txtAddress != null)
                {
                    result = this.txtAddress.Text;
                }
                return result;
            }
            private set
            {
                if (this.txtAddress != null)
                {
                    this.txtAddress.Text = value;
                }
            }
        }

        private GroupNodeType _IPv6ValueType;
        public GroupNodeType IPv6Type
        {
            get
            {
                return this._IPv6ValueType;
            }
            private set
            {
                this._IPv6ValueType = value;
                UpdateAddressCssClass();
            }
        }

        public string ParentIPv6Address
        {
            get;
            private set;
        }

        public int? ParentId
        {
            get;
            private set;
        }

        public int? ObjectId
        {
            get;
            private set;
        }

        public int? ClusterId
        {
            get;
            private set;
        }

        public string Listener
        {
            get
            {
                return "function(s){" + CustomValidatorScript() + "}";
            }
        }

        private bool _readonly;
        public bool ReadOnly
        {
            get
            {
                return this._readonly;
            }
            set
            {
                this._readonly = value;
                if (this.txtAddress != null)
                {
                    this.txtAddress.ReadOnly = value;
                    UpdateAddressCssClass();
                }
            }
        }

        #endregion // Properties

        #region Constructors

        public IPv6CompoundAddress()
        {
            this._readonly = false;
            this.IPv6Value = string.Empty;
            this.IPv6Type = GroupNodeType.Unknown;
            this.ParentIPv6Address = string.Empty;
        }

        #endregion // Constructors

        #region Event Handlers

        protected void OnIPv6AddressValidator(object source, ServerValidateEventArgs args)
        {
            // TODO: here put server based IPv6 validator
            args.IsValid = true;
        }

        #endregion // Event Handlers

        #region Methods

        public void Init(string ipV6Value, GroupNodeType type, int initSize, List<PrefixAggregate> prefixes,
            int? parentId, int? objectId, int? clusterId)
        {
            this.IPv6Value = ipV6Value;
            this.IPv6Type = type;

            string parentPrefixAddress = SetIPv6Prefixes(prefixes);
            this.ParentIPv6Address = parentPrefixAddress;
            this.ParentId = parentId;
            this.ObjectId = objectId;
            this.ClusterId = clusterId;

            bool validate = (this.ReadOnly == false);
            this.IPv6AddressRequireValidator.Enabled = validate;
            this.IPv6AddressCustomValidator.Enabled = validate;
            this.IPv6AddressCustomValidator.EnableClientScript = validate;
            if (validate)
            {
                InitCustomValidatorScript(initSize);
            }
        }

        private void InitCustomValidatorScript(int initIPv6Size)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("$(document).ready( function(){");

            sb.AppendFormat("var s={0};", initIPv6Size);
            sb.Append(CustomValidatorScript());

            sb.Append("});");

            this.initAddressBox.AddScript(sb.ToString());
        }

        private string CustomValidatorScript()
        {
            if (this.IPv6AddressCustomValidator == null)
            {
                return string.Empty;
            }

            return string.Format(
                IPv6CompoundAddress.CustomValidatorScriptTemplate,
                this.IPv6AddressCustomValidator.ClientID,
                this.IPv6Type,
                this.ParentIPv6Address,
                this.ParentId ?? -1,
                this.ObjectId ?? -1,
                this.ClusterId ?? -1);
        }

        /// <summary>
        /// Set IPv6 prefixes list.
        /// The Control will show list of prefixes ordered by index in given list of PrefixAggregates.
        /// </summary>
        /// <param name="prefixes">The prefixes in order from global prefix to subnet.</param>
        /// <returns>The most specific prefix address (last from given list).</returns>
        private string SetIPv6Prefixes(List<PrefixAggregate> prefixes)
        {
            string localPrefixAddress = string.Empty;

            // check, whether the control exists (is initialized)
            if (this.IPv6PrefixesList == null)
            {
                return localPrefixAddress;
            }
            
            // clean already set prefixes
            this.IPv6PrefixesList.Items.Clear();

            // check also inputs for zero / empty values
            if ((prefixes == null) || (prefixes.Count <= 0))
            {
                return localPrefixAddress;
            }

            // create list of items
            for (int index = 0; index < prefixes.Count; index++)
            {
                PrefixAggregate prefix = prefixes[index];

                localPrefixAddress = string.Format("{0}/{1}", prefix.FullPrefix, prefix.FullSize);
                string itemName = string.Format(
                    IPv6PrefixTemplate, prefix.FriendlyName, localPrefixAddress);

                ListItem listItem = new ListItem(itemName);

                // add two classes into list item:
                // .. 'type'  (for proper icon)
                // .. 'index' (for proper color)
                string cssClass = string.Format("item-type-{0} item-index-{1}", prefix.GroupType, index);
                listItem.Attributes.Add("class", cssClass);

                this.IPv6PrefixesList.Items.Add(listItem);
            }

            // set visibility of prefixes
            this.IPv6PrefixesList.Visible = (this.IPv6PrefixesList.Items.Count > 0);

            return localPrefixAddress;
        }

        private void UpdateAddressCssClass()
        {
            if (this.txtAddress == null)
            {
                return;
            }
            // set address label (class / text)
            switch (this._IPv6ValueType)
            {
                case GroupNodeType.GlobalPrefix:
                    this.tdAddress.Attributes["class"] = "GlobalPrefix-indent";
                    this.lblAddress.Text = IPAMWebContent.IPAMWEBCODE_VB1_2;
                    break;
                case GroupNodeType.PrefixAggregate:
                    this.tdAddress.Attributes["class"] = "PrefixAggregate-indent";
                    this.lblAddress.Text = IPAMWebContent.IPAMWEBCODE_VB1_3;
                    break;
                case GroupNodeType.IPv6Subnet:
                    this.tdAddress.Attributes["class"] = "IPv6Subnet-indent";
                    this.lblAddress.Text = IPAMWebContent.IPAMWEBCODE_VB1_4;
                    break;
                default:
                    break;
            }

            // set readonly class
            this.txtAddress.ReadOnlyCss(this._readonly);
        }

        #endregion // Methods
    }
}
