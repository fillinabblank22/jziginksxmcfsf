﻿using System;
using System.Collections.Generic;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client.Caching;
using SolarWinds.Logging;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class DiscoveryCentralPlugin : System.Web.UI.UserControl
    {
        private static Log _log;
        private static Log _logger { get { return _log ?? (_log = new Log()); } }

        private const string ImageButtonBaseURL = "~/Orion/IPAM/res/images/DiscoveryCentralPlugin/";

        protected int IPNodeCount { get; set; }
        public bool isFreeTool = false;
        public bool isOtherModuleInstalled = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                IPNodeCount = GetIPAddressCount();
            }
            catch (Exception ex)
            {
                _logger.ErrorFormat("Cannot get number of managed IP Addresses. Details: {0}", ex);
            }

            isFreeTool = LicenseInfoHelper.CheckFreeTool(out isOtherModuleInstalled);

            DataBind();
        }

        private int GetIPAddressCount()
        {
            IpamClientProxy proxy = SwisConnector.OpenCacheConnection();
            proxy.AppProxy.InternalStatus.EnsureCache(false);
            InternalStatusCache cache = InternalStatusCache.GetInstance();

            string CurrentLicenses = cache.GetCachedEntry(InternalStatusKey.LicensingCountCurrent.ToString());

            int licenses;
            if (false == int.TryParse(CurrentLicenses, out licenses))
            {
                return 0;
            }

            return licenses;
        }
    }
}