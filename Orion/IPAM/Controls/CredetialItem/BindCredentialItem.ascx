﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.BindCredentialItem" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">
$SW.OnIsNewBINDCredentialRequire = function (src, args) {
    return args.IsValid = $SW.IPAM.DMultiCreds.validate(
        '<%=ServerType %>', 'IsNewCredentialRequire', args.Value);
};
$SW.OnIsBINDPortRequire = function (src, args) {
    return args.IsValid = $SW.IPAM.DMultiCreds.validate(
        '<%=ServerType %>', 'IsValueRequire', args.Value);
};
</script>

<IPAMmaster:CssBlock runat="server">
.BINDAdvanced{ padding-top:8px; }
.BINDAdvanced a, .BINDAdvanced a:active, .BINDAdvanced a:visited { color:black; text-decoration:none; }
.sw-link a, .sw-link a.active, .sw-link a.visited { text-decoration:underline; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
$(document).ready(function(){
  // [oh] cli credentials flip-flop port numbers
  if(!$SW.IPAM.CliPortFn) $SW.IPAM.CliPortFn = $SW.IPAM.CliPortHandler();
});
</IPAMmaster:JsBlock>

<asp:HiddenField ID="hdnName" runat="server" Value="BIND Credential" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtBINDCredentialName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="BINDFixName" visible="false" OnClick="OnBtnBINDFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
            <asp:CustomValidator
                ID="BINDCredentialNameRequired"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtBINDCredentialName"
                OnServerValidate="OnIsNewBINDCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewBINDCredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99  %>"></asp:CustomValidator>
            <asp:CustomValidator 
                ID="BINDCredentialNameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtBINDCredentialName"
                OnServerValidate="OnBINDCredentialNameExists"
                ClientValidationFunction="$SW.OnCredentialNameExists"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtBINDUserName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="BINDUserNameRequired"
                runat="server"
                Display="Dynamic"
                OnServerValidate="OnIsNewBINDCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewBINDCredentialRequire"
                ControlToValidate="txtBINDUserName"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtBINDPassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="BINDPasswordRequire"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtBINDPassword"
                OnServerValidate="OnIsNewBINDCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewBINDCredentialRequire"
                ValidateEmptyText="true"       
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr><td colspan="3">&nbsp;</td></tr>
    <tr style="display: none;">
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_OH1_10%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtBINDPath" runat="server" CssClass="x-form-text x-form-field" EnableViewState="false"/>
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" class="BINDAdvanced">
            <orion:CollapsePanel Collapsed="true" runat="server">
                <TitleTemplate><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_128%></TitleTemplate>
                <BlockTemplate>
                    <table class="sw-form-wrapper cli-data" style="padding-top:8px;" border="0" cellspacing="0" cellpadding="0">
                        <tr class="sw-form-cols-normal">
                            <td class="sw-form-col-label"></td>
                            <td class="sw-form-col-control"></td>
                            <td class="sw-form-col-comment"></td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;">
                                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_129%></div>
                            </td>
                            <td><div class="sw-form-item">
                                <asp:RadioButton ID="BINDTelnetProtocol" GroupName="BINDProtocols" runat="server" CssClass="cli-telnet" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_130 %>" />
                                <br />
                                <asp:RadioButton ID="BINDSSHProtocol" GroupName="BINDProtocols" runat="server" CssClass="cli-ssh" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_131 %>" />
                            </div></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_132%></div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtBINDPort" CssClass="x-form-text x-form-field cli-port" runat="server" />
                            </div></td>
                            <td>
                                <asp:CustomValidator
                                    ID="BINDPortRequired"
                                    runat="server"
                                    Display="Dynamic"
                                    OnServerValidate="OnIsBINDPortRequire"
                                    ClientValidationFunction="$SW.OnIsBINDPortRequire"
                                    ControlToValidate="txtBINDPort"
                                    ValidateEmptyText="true"
                                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_133 %>"></asp:CustomValidator>
                                <asp:RegularExpressionValidator
                                    ID="BINDPortRange"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtBINDPort"
                                    ValidationExpression="^(0*[1-9][0-9]{0,3}|0*[1-5][0-9]{0,4}|0*6[0-4][0-9]{0,3}|0*6[0-5][0-4][0-9]{0,2}|0*6[0-5][0-5][0-2][0-9]|0*6[0-5][0-5][0-3][0-5])$"
                                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_134 %>"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><div class=sw-field-label></div></td>
                            <td colspan="2">
                                <div class="sw-form-item sw-form-clue">
                                    <div style="padding-bottom:10px;font-size:12px">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_135 %>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
</table>
