﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.ISCCredentialItem" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">
	$SW.OnIsNewISCCredentialRequire = function (src, args) {
		return args.IsValid = $SW.IPAM.DMultiCreds.validate(
			'<%=ServerType %>',
		'IsNewCredentialRequire',
		args.Value);
	};
	$SW.OnIsPortRequire = function (src, args) {
			return args.IsValid = $SW.IPAM.DMultiCreds.validate(
				'<%=ServerType %>', 'IsValueRequire', args.Value);
    };
</script>


<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
.ISCAdvanced{ padding-top:8px; }
.ISCAdvanced a, .ISCAdvanced a:active, .ISCAdvanced a:visited { color:black; text-decoration:none; }
.sw-link a, .sw-link a.active, .sw-link a.visited { text-decoration:underline; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
$(document).ready(function(){
  // [oh] cli credentials flip-flop port numbers
  if(!$SW.IPAM.CliPortFn) $SW.IPAM.CliPortFn = $SW.IPAM.CliPortHandler();
});
</IPAMmaster:JsBlock>

<asp:HiddenField ID="hdnISCName" runat="server"	Value="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VN0_23 %>" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
	<tr class="sw-form-cols-normal">
		<td class="sw-form-col-label"></td>
		<td class="sw-form-col-control"></td>
		<td class="sw-form-col-comment"></td>
	</tr>
	<tr>
		<td>
			<div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98 %></div>
		</td>
		<td>
			<div class="sw-form-item">
				<asp:TextBox ID="txtISCCredentialName" CssClass="x-form-text x-form-field" runat="server" />
			</div>
		</td>
		<td>
			 <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="ISCFixName" visible="false" OnClick="OnBtnISCFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
			<asp:CustomValidator
				ID="ISCCredentialNameRequired"
				runat="server"
				Display="Dynamic"
				ControlToValidate="txtISCCredentialName"
				OnServerValidate="OnIsNewISCCredentialRequire"
				ClientValidationFunction="$SW.OnIsNewISCCredentialRequire"
				ValidateEmptyText="true"
				ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99  %>"></asp:CustomValidator>
			<asp:CustomValidator 
				ID="ISCCredentialNameExists"
				runat="server"
				Display="Dynamic"
				ControlToValidate="txtISCCredentialName"
				OnServerValidate="OnISCCredentialNameExists"
				ClientValidationFunction="$SW.OnCredentialNameExists"
				ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
		</td>
	</tr>
	<tr>
		<td>
			<div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101%></div>
		</td>
		<td>
			<div class="sw-form-item">
				<asp:TextBox ID="txtISCUserName" CssClass="x-form-text x-form-field" runat="server" />
			</div>
		</td>
		<td>
			 <asp:CustomValidator
				ID="ISCUserNameRequired"
				runat="server"
				Display="Dynamic"
				OnServerValidate="OnIsNewISCCredentialRequire"
				ClientValidationFunction="$SW.OnIsNewISCCredentialRequire"
				ControlToValidate="txtISCUserName"
				ValidateEmptyText="true"
				ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
		</td>
	</tr>
	<tr>
		<td>
			<div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104%></div>
		</td>
		<td>
			<div class="sw-form-item">
				<asp:TextBox ID="txtISCPassword" runat="server" CssClass="x-form-text x-form-field"
					TextMode="Password" EnableViewState="false" />
			</div>
		</td>
		<td>
			   <asp:CustomValidator
				ID="ISCPasswordRequire"
				runat="server"
				Display="Dynamic"
				ControlToValidate="txtISCPassword"
				OnServerValidate="OnIsNewISCCredentialRequire"
				ClientValidationFunction="$SW.OnIsNewISCCredentialRequire"
				ValidateEmptyText="true"       
				ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" class="ISCAdvanced">
			<orion:CollapsePanel ID="CollapsePanel1" Collapsed="true" runat="server">
				<TitleTemplate><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_128%></TitleTemplate>
				<BlockTemplate>
					<table class="sw-form-wrapper cli-data" style="padding-top: 8px;" border="0" cellspacing="0" cellpadding="0">
						<tr class="sw-form-cols-normal">
							<td class="sw-form-col-label"></td>
							<td class="sw-form-col-control"></td>
							<td class="sw-form-col-comment"></td>
						</tr>
						<tr>
							<td style="vertical-align: top;">
								<div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_129%></div>
							</td>
							<td>
								<div class="sw-form-item">
									<asp:RadioButton ID="ISCTelnetProtocol" GroupName="ISCProtocols" runat="server" CssClass="cli-telnet" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_130 %>" />
									<br />
									<asp:RadioButton ID="ISCSSHProtocol" GroupName="ISCProtocols" runat="server" CssClass="cli-ssh" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_131 %>" />
								</div>
							</td>
							<td></td>
						</tr>
						<tr>
							<td>
								<div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_132%></div>
							</td>
							<td>
								<div class="sw-form-item">
									<asp:TextBox ID="txtISCPort" CssClass="x-form-text x-form-field cli-port" runat="server" />
								</div>
							</td>
							<td>
							 <asp:CustomValidator
									ID="ISCPortRequired"
									runat="server"
									Display="Dynamic"								
									ControlToValidate="txtISCPort"
                                    OnServerValidate="OnIsPortRequire"
									ClientValidationFunction="$SW.OnIsPortRequire"
									ValidateEmptyText="true"
									ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_133 %>"></asp:CustomValidator>
								<asp:RegularExpressionValidator
									ID="ISCPortRange"
									runat="server"
									Display="Dynamic"
									ControlToValidate="txtISCPort"
									ValidationExpression="^(0*[1-9][0-9]{0,3}|0*[1-5][0-9]{0,4}|0*6[0-4][0-9]{0,3}|0*6[0-5][0-4][0-9]{0,2}|0*6[0-5][0-5][0-2][0-9]|0*6[0-5][0-5][0-3][0-5])$"
									ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_134 %>"></asp:RegularExpressionValidator>
							</td>

						</tr>
						<tr>
							<td>
								<div class="sw-field-label"></div>
							</td>
							<td colspan="2">
								<div class="sw-form-item sw-form-clue">
									<div style="padding-bottom: 10px; font-size: 12px">
										<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_135 %>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</BlockTemplate>
			</orion:CollapsePanel>
		</td>
	</tr>
</table>
