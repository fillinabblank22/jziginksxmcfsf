﻿<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="SolarWinds.IPAM.Web.Common.Controls.ASACredentialItem" %>
        
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">
    $SW.OnIsNewASACredentialRequire = function (src, args) {
        return args.IsValid = $SW.IPAM.DMultiCreds.validate(
    '<%=ServerType %>',
    'IsNewCredentialRequire',
    args.Value);
    };

    $SW.OnIsASAPortRequire = function (src, args) {
        return args.IsValid = $SW.IPAM.DMultiCreds.validate(
    '<%=ServerType %>',
    'IsValueRequire',
    args.Value);
    };
</script>

<IPAMmaster:CssBlock runat="server">
.ASAAdvanced{ padding-top:8px; }
.ASAAdvanced a, .ASAAdvanced a:active, .ASAAdvanced a:visited { color:black; text-decoration:none; }
.sw-link a, .sw-link a.active, .sw-link a.visited { text-decoration:underline; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
$(document).ready(function(){
  var cbId = $SW.nsGet($nsId, 'ASAEnableLevel');
  
  var cb = $('#' + cbId);
  if (!cb || !cb[0]) return null;

  var extId = 'ASAEnableLevel';
  
  var enableLevelCBox = new Ext.form.ComboBox({
    typeAhead: true,
    triggerAction: 'all',
    transform: cbId,
    hiddenName: cb.name,
    hiddenId: cbId,
    id: extId,
    forceSelection: true,
    disabled: cb.disabled || false,
    width: 'auto',
    listeners: {
        expand: function (combo) {
            var w = combo.el.getWidth();
            combo.list.setWidth(w);
            combo.innerList.setWidth(w);
        }
    }
  });  
  
  // [oh] cli credentials flip-flop port numbers
  if(!$SW.IPAM.CliPortFn) $SW.IPAM.CliPortFn = $SW.IPAM.CliPortHandler();
});
</IPAMmaster:JsBlock>

<asp:HiddenField ID="hdnASAName" runat="server"
    Value="<%$ Resources : IPAMWebContent, IPAMWEBDATA_JI1_1 %>" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtASACredentialName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="ASAFixName" visible="false" OnClick="OnBtnASAFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
            <asp:CustomValidator
                ID="ASACredentialNameRequired"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtASACredentialName"
                OnServerValidate="OnIsNewASACredentialRequire"
                ClientValidationFunction="$SW.OnIsNewASACredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99  %>"></asp:CustomValidator>
            <asp:CustomValidator 
                ID="ASACredentialNameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtASACredentialName"
                OnServerValidate="OnASACredentialNameExists"
                ClientValidationFunction="$SW.OnCredentialNameExists"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtASAUserName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="ASAUserNameRequired"
                runat="server"
                Display="Dynamic"
                OnServerValidate="OnIsNewASACredentialRequire"
                ClientValidationFunction="$SW.OnIsNewASACredentialRequire"
                ControlToValidate="txtASAUserName"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtASAPassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="ASAPasswordRequire"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtASAPassword"
                OnServerValidate="OnIsNewASACredentialRequire"
                ClientValidationFunction="$SW.OnIsNewASACredentialRequire"
                ValidateEmptyText="true"       
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_107 %></div></td>
        <td><div class="sw-form-item">
            <asp:DropDownList id="ASAEnableLevel" runat="server">
                <asp:ListItem Value="-2" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_108 %>" />
                <asp:ListItem Value="-1" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_109 %>" />
                <asp:ListItem Value="0" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_110 %>" />
                <asp:ListItem Value="1" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_111 %>" />
                <asp:ListItem Value="2" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_112 %>" />
                <asp:ListItem Value="3" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_113 %>" />
                <asp:ListItem Value="4" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_114 %>" />
                <asp:ListItem Value="5" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_115 %>" />
                <asp:ListItem Value="6" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_116 %>" />
                <asp:ListItem Value="7" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_117 %>" />
                <asp:ListItem Value="8" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_118 %>" />
                <asp:ListItem Value="9" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_119 %>" />
                <asp:ListItem Value="10" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_120 %>" />
                <asp:ListItem Value="11" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_121 %>" />
                <asp:ListItem Value="12" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_122 %>" />
                <asp:ListItem Value="13" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_123 %>" />
                <asp:ListItem Value="14" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_124 %>" />
                <asp:ListItem Value="15" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_125 %>" />
            </asp:DropDownList>
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td><div class=sw-field-label></div></td>
        <td colspan="2">
            <div class="sw-form-item sw-form-clue sw-link">
                <a href="#" onclick="return $SW.CSHelp('OrionIPAMPHWCLIEnableLevel.htm');" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_126 %></a>
            </div>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_127 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtASAEnablePassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false"/>
        </div></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" class="ASAAdvanced">
            <orion:CollapsePanel Collapsed="true" runat="server">
                <TitleTemplate><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_128%></TitleTemplate>
                <BlockTemplate>
                    <table class="sw-form-wrapper cli-data" style="padding-top:8px;" border="0" cellspacing="0" cellpadding="0">
                        <tr class="sw-form-cols-normal">
                            <td class="sw-form-col-label"></td>
                            <td class="sw-form-col-control"></td>
                            <td class="sw-form-col-comment"></td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;">
                                <div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_129%></div>
                            </td>
                            <td><div class="sw-form-item">
                                <asp:RadioButton ID="ASATelnetProtocol" GroupName="ASAProtocols" runat="server" CssClass="cli-telnet" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_130 %>" />
                                <br />
                                <asp:RadioButton ID="ASASSHProtocol" GroupName="ASAProtocols" runat="server" CssClass="cli-ssh" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_131 %>" />
                            </div></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_132%></div></td>
                            <td><div class="sw-form-item">
                                <asp:TextBox ID="txtASAPort" CssClass="x-form-text x-form-field cli-port" runat="server" />
                            </div></td>
                            <td>
                                <asp:CustomValidator
                                    ID="ASAPortRequired"
                                    runat="server"
                                    Display="Dynamic"
                                    OnServerValidate="OnIsASAPortRequire"
                                    ClientValidationFunction="$SW.OnIsASAPortRequire"
                                    ControlToValidate="txtASAPort"
                                    ValidateEmptyText="true"
                                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_133 %>"></asp:CustomValidator>
                                <asp:RegularExpressionValidator
                                    ID="ASAPortRange"
                                    runat="server"
                                    Display="Dynamic"
                                    ControlToValidate="txtASAPort"
                                    ValidationExpression="^(0*[1-9][0-9]{0,3}|0*[1-5][0-9]{0,4}|0*6[0-4][0-9]{0,3}|0*6[0-5][0-4][0-9]{0,2}|0*6[0-5][0-5][0-2][0-9]|0*6[0-5][0-5][0-3][0-5])$"
                                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_134 %>"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><div class=sw-field-label></div></td>
                            <td colspan="2">
                                <div class="sw-form-item sw-form-clue">
                                    <div style="padding-bottom:10px;font-size:12px">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_135 %>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </BlockTemplate>
            </orion:CollapsePanel>
        </td>
    </tr>
</table>


