﻿<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="SolarWinds.IPAM.Web.Common.Controls.InfobloxDhcpCredentialItem" %>

<script type="text/javascript">
$SW.OnIsNewInfobloxCredentialRequire = function (src, args) {
    return args.IsValid = $SW.IPAM.DMultiCreds.validate(
        '<%=ServerType %>',
        'IsNewCredentialRequire',
        args.Value);
};
</script>

<asp:HiddenField ID="hdnName" runat="server"
    Value="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_868 %>" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtInfobloxCredentialName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="InfobloxFixName" visible="false" OnClick="OnBtnInfobloxFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
            <asp:CustomValidator
                ID="InfobloxCredentialNameRequired"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtInfobloxCredentialName"
                OnServerValidate="OnIsNewInfobloxCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewInfobloxCredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99 %>"></asp:CustomValidator>
            <asp:CustomValidator 
                ID="InfobloxCredentialNameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtInfobloxCredentialName"
                OnServerValidate="OnInfobloxCredentialNameExists"
                ClientValidationFunction="$SW.OnCredentialNameExists"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtInfobloxUserName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="InfobloxUserNameRequired"
                runat="server"
                Display="Dynamic"
                OnServerValidate="OnIsNewInfobloxCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewInfobloxCredentialRequire"
                ControlToValidate="txtInfobloxUserName"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent,  IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtInfobloxPassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="InfobloxPasswordRequire"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtInfobloxPassword"
                OnServerValidate="OnIsNewInfobloxCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewInfobloxCredentialRequire"
                ValidateEmptyText="true"       
                ErrorMessage="<%$ Resources :  IPAMWebContent, IPAMWEBDATA_VB1_105%>"></asp:CustomValidator>
        </td>
    </tr>
</table>
