﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.WMICredentialItem" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<script type="text/javascript">
$SW.OnIsNewWMICredentialRequire = function (src, args) {
    return args.IsValid = $SW.IPAM.DMultiCreds.validate(
        '<%=ServerType %>',
        'IsNewCredentialRequire',
        args.Value);
};
</script>

<IPAMmaster:CssBlock ID="CssBlock1" runat="server">
#WMICredentialHelp div { color: #979797; font-size: 10px; }  
#WMICredentialHelp .LinkArrow { font-size: 11px; vertical-align: text-top; }  
#WMICredentialHelp a.helpLink { color: #336699; text-decoration: none; }
#WMICredentialHelp a.helpLink:hover { color: orange; }
.sw-link a, .sw-link a.active, .sw-link a.visited { text-decoration:underline; }
</IPAMmaster:CssBlock>

<asp:HiddenField ID="hdnName" runat="server" Value="WMI Credential" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%=Resources.CoreWebContent.WEBDATA_TM0_235%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWMICredentialName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="WMIFixName" visible="false" OnClick="OnBtnWMIFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
            <asp:CustomValidator
                ID="WMICredentialNameRequired"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWMICredentialName"
                OnServerValidate="OnIsNewWMICredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWMICredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_99  %>"></asp:CustomValidator>
            <asp:CustomValidator 
                ID="WMICredentialNameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWMICredentialName"
                OnServerValidate="OnWMICredentialNameExists"
                ClientValidationFunction="$SW.OnCredentialNameExists"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%=Resources.CoreWebContent.WEBCODE_AK0_165%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWMIUserName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="WMIUserNameRequired"
                runat="server"
                Display="Dynamic"
                OnServerValidate="OnIsNewWMICredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWMICredentialRequire"
                ControlToValidate="txtWMIUserName"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td></td>
        <td id="WMICredentialHelp">
            <div><%= Resources.CoreWebContent.WEBDATA_AK0_522 %></div>
            <div>
                <span class="LinkArrow">&#0187;</span>
                <orion:HelpLink runat="server" HelpUrlFragment="OrionIPAMPHPrivileges"
                    HelpDescription="<%$ Resources: CoreWebContent, WEBDATA_AK0_516 %>" CssClass="helpLink" />
            </div>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%=Resources.CoreWebContent.WEBDATA_IB0_40%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWMIPassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="WMIPasswordRequire"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWMIPassword"
                OnServerValidate="OnIsNewWMICredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWMICredentialRequire"
                ValidateEmptyText="true"       
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%=Resources.CoreWebContent.WEBDATA_TM0_236%></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWMIPasswordConfirm" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="RequiredConfirmPasswordValidator"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWMIPasswordConfirm"
                OnServerValidate="OnIsNewWMICredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWMICredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_105 %>"></asp:CustomValidator>
            <asp:CompareValidator
                ID="CompareConfirmPasswordValidator"
                runat="server"
                Display="Dynamic"
                ControlToCompare="txtWMIPassword"
                ControlToValidate="txtWMIPasswordConfirm"
                ErrorMessage="<%$ Resources: CoreWebContent, WEBDATA_TM0_240 %>"></asp:CompareValidator>
        </td>
    </tr>
</table>
