﻿<%@ Control Language="C#" AutoEventWireup="true" 
    Inherits="SolarWinds.IPAM.Web.Common.Controls.WindowsCredentialItem" %>

<script type="text/javascript">
$SW.OnIsNewWinCredentialRequire = function (src, args) {
    return args.IsValid = $SW.IPAM.DMultiCreds.validate(
        '<%=ServerType %>',
        'IsNewCredentialRequire',
        args.Value);
};
</script>

<asp:HiddenField ID="hdnName" runat="server"
    Value="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_96 %>" />

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_98 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWinCredentialName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <orion:LocalizableButton runat="server" style="margin-left: 8px;" ID="WinFixName" visible="false" OnClick="OnBtnWinFixNameClick" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_43 %>" DisplayType="Secondary"/>
            <asp:CustomValidator
                ID="WinCredentialNameRequired"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWinCredentialName"
                OnServerValidate="OnIsNewWinCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWinCredentialRequire"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_99 %>"></asp:CustomValidator>
            <asp:CustomValidator 
                ID="WinCredentialNameExists"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWinCredentialName"
                OnServerValidate="OnWinCredentialNameExists"
                ClientValidationFunction="$SW.OnCredentialNameExists"
                ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_100 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_101 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWinUserName" CssClass="x-form-text x-form-field" runat="server" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="WinUserNameRequired"
                runat="server"
                Display="Dynamic"
                OnServerValidate="OnIsNewWinCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWinCredentialRequire"
                ControlToValidate="txtWinUserName"
                ValidateEmptyText="true"
                ErrorMessage="<%$ Resources : IPAMWebContent,  IPAMWEBDATA_VB1_102 %>"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td><div class=sw-field-label></div></td>
        <td colspan="2">
            <div class="sw-form-item sw-form-clue">
                <div style="padding-bottom:10px;font-size:12px">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_103 %>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_104 %></div></td>
        <td><div class="sw-form-item">
            <asp:TextBox ID="txtWinPassword" runat="server" CssClass="x-form-text x-form-field"
             TextMode="Password" EnableViewState="false" />
        </div></td>
        <td>
            <asp:CustomValidator
                ID="WinPasswordRequire"
                runat="server"
                Display="Dynamic"
                ControlToValidate="txtWinPassword"
                OnServerValidate="OnIsNewWinCredentialRequire"
                ClientValidationFunction="$SW.OnIsNewWinCredentialRequire"
                ValidateEmptyText="true"       
                ErrorMessage="<%$ Resources :  IPAMWebContent, IPAMWEBDATA_VB1_105%>"></asp:CustomValidator>
        </td>
    </tr>
</table>
