﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.Orion_IPAM_Controls_DNSCredentialsSelector" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Assembly="SolarWinds.IPAM.Web.Common" Namespace="SolarWinds.IPAM.Web.Common.Controls" %>
<%@ Reference Control="~/Orion/IPAM/Controls/CredetialItem/WMIInheritCredentialItem.ascx" %>
<%@ Reference Control="~/Orion/IPAM/Controls/CredetialItem/WMICredentialItem.ascx" %>
<%@ Reference Control="~/Orion/IPAM/Controls/CredetialItem/BindCredentialItem.ascx" %>

<IPAMmaster:CssBlock Requires="~/Orion/styles/Mainlayout.css" runat="server">
.credname { white-space: nowrap; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext-quicktips,sw-admin-cred.js,sw-helpserver,sw-ux-wait.js" Orientation="jsPostInit" runat="server">
$(document).ready(function(){ $SW.IPAM.DMultiCreds.init(); });
</IPAMmaster:JsBlock>
<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
$SW.CSHelp = function(file){
  var url = String.format('{0}NetPerfMon/SolarWinds/default.htm?context=SolarWinds&file={1}',
    $SW.HelpServer || '/', file);
  window.open(url, '_blank');
  return false;
};
</IPAMmaster:JsBlock>

<table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-checkbox"></td>
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>

<asp:Repeater ID="CredentialRepeater" OnItemDataBound="OnRepeaterItemBind" runat="server">
<ItemTemplate>
    <tr>
        <td><IPAM:TemplateRadioButton id="CredentialType" GroupName="CredentialsType" runat="server" 
          /><asp:HiddenField ID="DnsServerType" runat="server" /></td>
        <td style="height: 26px; vertical-align: middle;"><div class="sw-field-label">
                <asp:Label ID="CredentialName" Text="" runat="server" CssClass="credname" />
        </div></td>
        <td><div class="sw-form-item">
            <asp:DropDownList id="CredentialSelector" runat="server">
                <asp:ListItem Value="" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_97 %>" />
            </asp:DropDownList>
        </div></td>
    </tr>
    <tr>
        <td class="sw-form-col-checkbox"></td>
        <td colspan="3"><div id="newCredentialArea" runat="server">
            <asp:PlaceHolder ID="newCredentialPlaceHolder" runat="server" />
        </div></td>
    </tr>
</ItemTemplate>
<HeaderTemplate />
<FooterTemplate />
</asp:Repeater>

    <!-- Test credentials -->
    <tr>
        <td colspan="2">&nbsp;</td>
        <td><orion:LocalizableButton runat="server" id="TestCredential" CausesValidation="true"  DisplayType="Secondary" LocalizedText="Test"/></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="2">
            <div id="credtestfail" class="x-tip x-form-invalid-tip" style="display: none; visibility: visible; position: static; margin-top: 4px; width: 270px;">
                <div class="x-tip-tl" >
                    <div class="x-tip-tr">
                        <div class="x-tip-tc">
                            <div class="x-tip-header x-unselectable" style="-moz-user-select: none;">
                                <span class="x-tip-header-text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x-tip-bwrap">
                    <div class="x-tip-ml">
                        <div class="x-tip-mr">
                            <div class="x-tip-mc">
                                <div class="x-tip-body" style="height: auto; width: auto;">
        <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_136 %></b><br />
        <span id="credtestfailtxt"></span><a id="service" href='http://www.solarwinds.com/documentation/kbloader.aspx?kb=4675' target='_blank' style='text-decoration:underline'>Learn more &#187</a> 
        <br />
        <br />
        <%= String.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_137, "<a id=\"credTestFailLink\" style=\"text-decoration:underline;font-style:italic\">", "</a>") %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="x-tip-bl x-panel-nofooter">
                        <div class="x-tip-br">
                            <div class="x-tip-bc" />
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td colspan="2">
            <div id="credtestpass" class="x-tip x-form-valid-tip" style="display: none; visibility: visible; position: static; margin-top: 4px; width: 270px;">
                <div class="x-tip-tl">
                    <div class="x-tip-tr">
                        <div class="x-tip-tc">
                            <div class="x-tip-header x-unselectable" style="-moz-user-select: none;">
                                <span class="x-tip-header-text" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x-tip-bwrap">
                    <div class="x-tip-ml">
                        <div class="x-tip-mr">
                            <div class="x-tip-mc">
                                <div class="x-tip-body" style="height: auto; width: auto;">
        <b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_138 %></b><br />
        <span id="credtestpasstxt"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="x-tip-bl x-panel-nofooter">
                        <div class="x-tip-br">
                            <div class="x-tip-bc"/>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<asp:ValidationSummary runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
