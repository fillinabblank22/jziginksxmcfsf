﻿using System;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpOptionLeaseSetting : System.Web.UI.UserControl
    {
        #region Declarations

        public int OptionCode { get; set; }

        public string WebId { get; set; }

        #endregion // Declarations
        
        #region Properties

        public string WebParamId
        {
            get { return WebId;
            }
        }

        public ulong InfiniteLeaseTimeValue
        {
            get
            {
                TimeSpan leaseLast;
                leaseLast = TimeSpan.FromSeconds(UInt32.MaxValue);

                return Convert.ToUInt64(leaseLast.TotalSeconds);
            }
        }

        public string GetLeaseTimeErrorMessage
        {
            get
            {
                return Resources.IPAMWebContent.IPAMWEBDATA_VB1_547 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_548 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_549 + ","
                        + Resources.IPAMWebContent.IPAMWEBDATA_VB1_551 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_552 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_556 + ","
                        + Resources.IPAMWebContent.IPAMWEBDATA_VB1_554 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_555 + "," + Resources.IPAMWebContent.IPAMWEBDATA_VB1_557 + "," + Resources.IPAMWebContent.IPAMWEBCODE_VB1_8;
            }
        }

        #endregion

        #region Constructors

        public DhcpOptionLeaseSetting()
        {
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            OptionCode = Convert.ToInt32(Page.Request.QueryString["OptionCode"]);
            WebId = Page.Request.QueryString["WebId"];
        }

        #endregion // Event Handlers
    }
}