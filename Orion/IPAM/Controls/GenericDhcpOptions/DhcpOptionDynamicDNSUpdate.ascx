﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpOptionDynamicDNSUpdate.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpOptionDynamicDNSUpdate" %>

<script type="text/sw-javascript" language="javascript">

    if (!$SW.IPAMValid) $SW.IPAMValid = {};
    $SW.IPAMValid.WebId = '<%=HttpUtility.HtmlEncode(this.WebParamId) %>';
    
    var Enum = {
        NoFlags: 0,
        EnableDynamicUpdates: 1,
        DynamicallyUpdateWhenNotRequestUpdates: 2,
        DiscardOnLeaseDeleting: 4,
        DynamicallyUpdateAlways: 16
    };

    var optionCode = '<%=this.OptionCode %>';
    var webParamId = '<%=this.WebParamId %>';
    
    var SetDnsOptionParse = function () {
        
        var optValue = $SW.IPAM.GetOptionValue(webParamId,optionCode);

        $('#' + '<%= helpTextID.ClientID %>').html('');
        $('#' + '<%= helpTextID.ClientID %>').html('<p>' + optValue.data.HelpTextValue + '</p>');

        $($('#' + '<%= chEnableDynamicUpdates.ClientID %>')).attr('checked', ((optValue.data.optionValue & Enum.EnableDynamicUpdates) == Enum.EnableDynamicUpdates));
        
        $($('#' + '<%= rbDynamicallyUpdateAlways.ClientID %>')).attr('checked', ((optValue.data.optionValue & Enum.DynamicallyUpdateAlways) == Enum.DynamicallyUpdateAlways));
        
        var dynamicallyUpdateAlways = $($('#' + '<%= rbDynamicallyUpdateAlways.ClientID %>')).is(":checked");
        if (!dynamicallyUpdateAlways) {
            $($('#' + '<%= rbDynamicallyUpdateOnClientRequest.ClientID %>')).attr('checked', true);
        } 

        $($('#' + '<%= chDiscardOnLeaseDeleting.ClientID %>')).attr('checked', ((optValue.data.optionValue & Enum.DiscardOnLeaseDeleting) == Enum.DiscardOnLeaseDeleting));

        $($('#' + '<%= chDynamicallyUpdateWhenNotRequestUpdates.ClientID %>')).attr('checked', ((optValue.data.optionValue & Enum.DynamicallyUpdateWhenNotRequestUpdates) == Enum.DynamicallyUpdateWhenNotRequestUpdates));
        return;
    };
    
    function SaveDNSoptionValue () {
        
        var enableDynamicUpdates = $($('#' + '<%= chEnableDynamicUpdates.ClientID %>')).is(":checked");
        var dynamicallyUpdateAlways = $($('#' + '<%= rbDynamicallyUpdateAlways.ClientID %>')).is(":checked");
        var discardOnLeaseDeleting = $($('#' + '<%= chDiscardOnLeaseDeleting.ClientID %>')).is(":checked");
        var dynamicallyUpdateWhenNotRequestUpdates = $($('#' + '<%= chDynamicallyUpdateWhenNotRequestUpdates.ClientID %>')).is(":checked");
        
        var ret = "";
        if (enableDynamicUpdates) {
            ret = Enum.NoFlags;
            ret |= Enum.EnableDynamicUpdates;
            if (dynamicallyUpdateAlways) ret |= Enum.DynamicallyUpdateAlways;
            if (discardOnLeaseDeleting) ret |= Enum.DiscardOnLeaseDeleting;
            if (dynamicallyUpdateWhenNotRequestUpdates) ret |= Enum.DynamicallyUpdateWhenNotRequestUpdates;
        }

        $SW.IPAM.SetOptionValue(webParamId, optionCode, ret,"False");
       
        return false;
    };
    
    var EnableDynamicUpdatesClick = function (delay) {
        var chb = $($('#' + '<%= chEnableDynamicUpdates.ClientID %>'))[0];
        if (!chb) return;

        var div = $('#' + '<%= divEnableDynamicUpdates.ClientID %>');
        if (div)
            if (chb.checked === true) div.show(delay);
            else div.hide(delay);
        return;
    };

    var CancelControl = function () {
        $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
        return false;
    };

    $(document).ready(function() {

        $($('#' + '<%= chEnableDynamicUpdates.ClientID %>')).click(function() { EnableDynamicUpdatesClick(250); });

        SetDnsOptionParse();
        
        EnableDynamicUpdatesClick(0);
    });

</script>

<div id="UserControlContainer" style="padding: 8px 6px;margin-bottom: 20px;" runat="server">
    
    <table width="600px">

        <tr style="background-color:#FFF7CD;">
            <td>
                <div>
                    <div id="Div1" runat="server" class="OptionHelpDescription" style="margin:5px 10px 5px 0px;padding: 5px 0 5px 10px;">
                        <table>
                            <tr>
                                <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                <td>
                                    <div>
                                        <asp:Label ID="helpTextID" runat="server" />
                                    </div>                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr><td style="width: 3px; padding: 3px"></td></tr>
        
        <tr>
            <td>
                <div class="blueBox" id="NormalText">
                    <div id="dhcpScopeOptionsContent" runat="server">
                        <div id="divChkDynamicUpdate" runat="server" style="margin-left: 0px;">
                            <asp:CheckBox ID="chEnableDynamicUpdates" Checked="true" runat="server" Text = "<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_567 %>" CssClass="checkbox"/>
                            <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:17px">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_568 %>
                            </div>
                        </div>
                        <br />
                        <div id="divDynamicUpdates" runat="server">
                            <div id="divEnableDynamicUpdates" runat="server">
                                <table>
                                    <tr>
                                        <td style = "width : 1px"></td>
                                        <td>
                                            <div style="padding-left: 12px">
                                                <asp:RadioButton ID="rbDynamicallyUpdateOnClientRequest" runat="server" GroupName="DNSUpdate" />
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_569 %>
                                                <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:17px">
                                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_570 %>
                                                </div>
                                                <br />
                                                <asp:RadioButton ID="rbDynamicallyUpdateAlways" runat="server" GroupName="DNSUpdate" />
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_571%>
                                                <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:17px">
                                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_572 %>
                                                </div>
                                            </div>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan = "2">&nbsp;
                                            <asp:CheckBox ID="chDiscardOnLeaseDeleting" runat="server" Text="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_573 %>" CssClass="checkbox"/>
                                            <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:25px">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_574 %>
                                            </div>
                                            &nbsp;
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan = "2">&nbsp;
                                            <asp:CheckBox ID="chDynamicallyUpdateWhenNotRequestUpdates" runat="server" Text="<%$ Resources:IPAMWebContent,IPAMWEBDATA_VB1_575 %>" CssClass="checkbox"/>
                                            <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:25px">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_576 %>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="imgbSave" DisplayType="Primary" LocalizedText="Save" OnClientClick="SaveDNSoptionValue(); return false;"/>
                    <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="CancelControl();return false;" />
                </div>
            </td>
        </tr>
    </table>             
</div>

