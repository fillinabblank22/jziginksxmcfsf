﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddrType.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.IPAddrType" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<script type="text/javascript" language="javascript">

    if (!$SW.IPAM) $SW.IPAM = {};

    var SaveIpAddressValues = function () {
        $SW.IPAM.SaveIPAddrressArrayValues();
        return false;
    };
    
    var CancelControl = function () {
        $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
        return false;
    };
    
    $SW.IPAM.IpAddrRecords.SetWebId = function () {
        $SW.store.baseParams.w = '<%=this.WebId %>';
        $SW.store.baseParams.optCode = '<%=this.OptionCode %>';
    };
    
    $SW.IPAM.IpAddrRecords.LoadHelpText = function (ipAddrOptCode, ipAddrWebId) {
        var optValue = $SW.IPAM.GetOptionValue(ipAddrWebId, ipAddrOptCode);
        
        $('#' + '<%= helpTextID.ClientID %>').html('');
        $('#' + '<%= helpTextID.ClientID %>').html('<p>' + optValue.data.HelpTextValue + '</p>');

        $SW.IPAM.AllowedRouterIpCount = 0;
        if (optValue.data.ServerType != null && optValue.data.ServerType != '' && (optValue.data.ServerType == 'CISCO' || optValue.data.ServerType == 'ASA'))
        {
            var routerIpExceedsMsg = '<%=this.RouterIpExceedsMsg %>';
            routerIpExceedsMsg = routerIpExceedsMsg.replace(/AllowedRouterIpCount/gi, optValue.data.AllowedRouterIpCount);
            routerIpExceedsMsg = routerIpExceedsMsg.replace(/ServerType/gi, optValue.data.ServerType);
            
            $SW.IPAM.AllowedRouterIpCount = optValue.data.AllowedRouterIpCount;
            $SW.IPAM.RouterIpExceedsMsg = routerIpExceedsMsg;
        }
        
        return;
    };

</script>

<div id="UserControlContainer" style="padding: 8px 6px;margin-bottom: 20px;" runat="server">

    <table width="700px">
    
        <tr style="background-color:#FFF7CD;">
            <td>
                <div>
                    <div id="Div1" runat="server" class="OptionHelpDescription" style="margin:5px 10px 5px 0px;padding: 5px 0 5px 10px;">
                        <table>
                            <tr>
                                <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                <td>
                                    <div>
                                        <asp:Label ID="helpTextID" runat="server" />
                                    </div>                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr><td style="width: 3px; padding: 3px"></td></tr>
        
        <tr>
            <td>
                <div id="UserControl" runat="server">
    
                    <IPAMlayout:GridBox runat="server" ID="IpAddrGridBox" title="" UseLoadMask="True" autoHeight="false" height="278" width="700"
                        emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_12 %>" borderVisible="true" deferEmptyText="false"
                        SelectorName="selector" RowSelection="true" listeners="{cellclick: $SW.IPAM.IpAddrRecords.grid_cellclick,render: $SW.IPAM.IpAddrRecords.SetWebId }">

                        <ConfigOptions ID="ConfigOptions1" runat="server">
                            <Options>
                                <IPAMlayout:ConfigOption ID="ConfigOption1" Name="plugins" Value="sw-ipAddrrecord-editor" runat="server" />
                            </Options>
                        </ConfigOptions>
                        <ViewOptions ID="ViewOptions1" runat="server" />
    
                        <TopMenu borderVisible="false">
                            <IPAMui:ToolStripMenuItem id="btnAdd12" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_09 %>" runat="server">
                                    <handler>function() { $SW.IPAM.IpAddrRecords.AddRecord( Ext.getCmp('IpAddrGridBox')); }</handler>
                            </IPAMui:ToolStripMenuItem>         
                        </TopMenu>

                        <Columns>
                            <IPAMlayout:GridColumn title="Order" width="50" isSortable="true" runat="server" dataIndex="ID" />
                            <IPAMlayout:GridColumn title="IP Address" width="450" isSortable="true" runat="server" dataIndex="IPAddress" editor="{ xtype: 'textfield'}" />        
                            <IPAMlayout:GridColumn title="Actions" width="150" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.IpAddrList_renderer_buttons()" />
                        </Columns>

                        <Store id="store" dataRoot="rows" dataCount="count" autoLoad="false" proxyUrl="../sessiondataprovider.ashx"
                            proxyEntity="IPAddr" AmbientSort="ID" listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
                            <Fields>
                                <IPAMlayout:GridStoreField dataIndex="ID" runat="server" />
                                <IPAMlayout:GridStoreField dataIndex="IPAddress"  runat="server" />                      
                            </Fields>
                        </Store>

                    </IPAMlayout:GridBox>

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="imgIpAddrSave" DisplayType="Primary" LocalizedText="Save" OnClientClick="SaveIpAddressValues();return false;" />
                    <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="CancelControl();return false;"/>
                </div>
            </td>
        </tr>
    </table>            
</div>