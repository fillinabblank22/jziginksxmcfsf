﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpOptionCommonType.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpOptionCommonType" %>

<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<script type="text/sw-javascript" language="javascript">

    var optionCode = '<%=this.OptionCode %>';
    var webParamId = '<%=HttpUtility.HtmlEncode(this.WebParamId) %>';
    var optionDataType = '<%=this.OptionDataType %>';

    var SaveCommonTypeOptionValue = function () {
                
        var userInput = $('#' + '<%= txtValue.ClientID %>').val();
        if ($SW.IPAM.HandleCommonTypeValidation(userInput, optionDataType)) {
            $SW.IPAM.SetOptionValue(webParamId, optionCode, userInput,"False");
        }

        return false;
    };
    
    var CancelControl = function () {
        $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
        return false;
    };

    var SetCommonTypeOptionParse = function () {

        var optValue = $SW.IPAM.GetOptionValue(webParamId, optionCode);

        $('#' + '<%= helpTextID.ClientID %>').html('');
        $('#' + '<%= helpTextID.ClientID %>').html('<p>' + optValue.data.HelpTextValue + '</p>');
        
        $('#' + '<%= exampleText.ClientID %>').html($SW.IPAM.GetHelpTextExample(optionDataType));
        
        $('#' + '<%= txtValue.ClientID %>').val(optValue.data.optionValue);

        return;
    };
    
    $(document).ready(function () {
        SetCommonTypeOptionParse();
    });

</script>

<IPAMui:ValidationIcons runat="server" />

<div id="UserControlContainer" style="padding: 8px 6px;margin-bottom: 20px;" runat="server">

    <table width="600px">

        <tr style="background-color:#FFF7CD;">
            <td>
                <div>
                    <div id="Div1" runat="server" class="OptionHelpDescription" style="margin:5px 10px 5px 0px;padding: 5px 0 5px 10px;">
                        <table>
                            <tr>
                                <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                <td>
                                    <div>
                                        <asp:Label ID="helpTextID" runat="server" />
                                    </div>                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr><td style="width: 3px; padding: 3px"></td></tr>

        <tr>
            <td>
                <div>

                    <div class="blueBox">

                        <table class="sw-form-wrapper">
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label" />
                                <td class="sw-form-col-control"/>
                                <td class="sw-form-col-comment" />
                            </tr>
                            <tr>
                                <td class="leftLabelColumn" style="word-wrap: break-word; text-align: left;padding-left: 1px;">
                                    <div class="sw-field-label">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_SE_08 %>
                                    </div>
                                </td>
                                <td>
                                    <div class="sw-form-item">
                                        <asp:TextBox runat="server" ID="txtValue" CssClass="x-form-text x-form-field" Width="90%" EnableViewState="false" />
                                    </div>
                                    
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2">
                                    <div style="font-family:Arial;font-size:10px;color:#646464;padding-left:0px" id="exampleText" runat="server">
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </div>

                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="true" ShowMessageBox="true" DisplayMode="BulletList" />
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" ID="optionimgbSave" CausesValidation="true" DisplayType="Primary" LocalizedText="Save" OnClientClick="SaveCommonTypeOptionValue(); return false;" />
                    <orion:LocalizableButton runat="server" ID="optionimgbCancel" CausesValidation="false" OnClientClick="CancelControl(); return false;" DisplayType="Secondary" LocalizedText="Cancel" />
                </div>
            </td>
        </tr>
    </table>
</div>
