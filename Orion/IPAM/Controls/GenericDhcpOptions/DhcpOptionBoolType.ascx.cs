﻿using System;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpOptionBoolType : System.Web.UI.UserControl
    {
        #region Declarations

        public int OptionCode { get; set; }

        public string WebId { get; set; }

        #endregion // Declarations

        #region Properties

        public string WebParamId
        {
            get
            {
                return WebId;
            }
        }

        #endregion

        #region Constructors

        public DhcpOptionBoolType()
        {
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            OptionCode = Convert.ToInt32(Page.Request.QueryString["OptionCode"]);
            WebId = Page.Request.QueryString["WebId"];        
        }

       #endregion // Event Handlers
    }
}