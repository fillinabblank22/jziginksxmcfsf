﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpOptionLayoutBuilder.ascx.cs" Inherits="Orion_IPAM_DhcpOptionLayoutBuilder" %>

<%@ Register Src="~/Orion/IPAM/Controls/GenericDhcpOptions/IPAddrType.ascx" TagName="IpAddress" TagPrefix="IpArrayType" %>
<%@ Register Src="~/Orion/IPAM/Controls/GenericDhcpOptions/IPPairType.ascx" TagName="IpAddressPair" TagPrefix="IPAMUI" %>
<%--<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>--%>

<style>
    .contentBlock p { margin-bottom: 8px; }
    .LabelAlign {
        vertical-align: top;
        font-weight: bold;
    }
    .BoderCollapse{
        border-collapse:collapse;
    }
    .TblBorder{
	    border: solid 1px #dbdcd7;
	    background-color:#fff;
    }
    .EmptyRow {
        height: 10px;
    }
    .ControlAlign{
	    margin-right: 0;
	    margin-bottom: 0;
	    margin-top: 0;
        margin-left: 5px;
	    padding: 0px 0px 0px 0px;
    }
    .checkbox label{
        padding-left:5px;
    }
    .allignTop{
        padding-top: 20px;
        vertical-align: top;
        padding-left: 20px;
    }

</style>

<script type="text/javascript" language="javascript">
   
    jQuery(document).ready(function () {
        $SW.IPAM.LoadDialogLayout("<%= OptionLayout.ClientID %>", "<%= this.WebId %>");
    });

</script>

<asp:Panel ID="OptionLayout" runat="server" BackColor= "white">
     
    <div id="LayoutContent">
        
        <table class="BoderCollapse">
            
            <tr class="EmptyRow"></tr>

            <tr>
                <td class="LabelAlign">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_SE_02 %>
                </td>
                <td class="LabelAlign">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_SE_03 %>
                </td>
                <td>
                    <%--<div align="right">
                        <ipam:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionIPAMDHCPOptions" />
                    </div>--%>
                </td>
            </tr>
            
            <tr class="EmptyRow"></tr>

            <tr style="height: 470px;">
                <td style="width: 218px;"  class="TblBorder">
                    <div id="leftpanel" style="width: 218px;height: 470px;overflow-y:auto;overflow-x:auto;">

                    </div>
                </td>
                 <td style="width: 780px;" class="TblBorder allignTop"  colspan="2">
                    <div id="IPAddressGrid" class="hidden">
                        <IpArrayType:IpAddress ID="IPAddressTypeID" runat="server" />
                    </div>
                    <div id="IPAddressPairGrid" class="hidden">
                        <IPAMUI:IpAddressPair ID="IPAddressPairTypeID" runat="server" />
                    </div>
                    <div id="loadMaskContainer">
                        <div id="UserControlContainer"></div>
                    </div>
                </td>
            </tr>

        </table>

    </div>

</asp:Panel>
