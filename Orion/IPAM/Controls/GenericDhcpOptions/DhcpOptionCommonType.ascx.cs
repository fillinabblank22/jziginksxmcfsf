﻿using System;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpOptionCommonType : System.Web.UI.UserControl
    {
        #region Declarations

        public int OptionCode { get; set; }

        public string WebId { get; set; }

        public int OptionDataType { get; set; }
        
        #endregion // Declarations

        #region Properties

        public string WebParamId
        {
            get
            {
                return WebId; 
            }
        }

        #endregion

        #region Constructors

        public DhcpOptionCommonType()
        {
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            OptionCode = Convert.ToInt32(Page.Request.QueryString["OptionCode"]);
            OptionDataType = Convert.ToInt32(Page.Request.QueryString["OptionDataType"]);
            WebId = Page.Request.QueryString["WebId"];
        }

        #endregion // Event Handlers
        
    }
}