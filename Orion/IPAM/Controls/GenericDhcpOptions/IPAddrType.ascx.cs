﻿using System;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPAddrType : System.Web.UI.UserControl
    {
        #region Declarations

        public int OptionCode { get; set; }

        public string WebId { get; set; }

        public string RouterIpExceedsMsg
        {
            get
            {
                return string.Format(Resources.IPAMWebContent.IPAMWEBDATA_JI_7, "AllowedRouterIpCount", "ServerType");
            }
        }

        #endregion // Declarations

        #region Constructors

        public IPAddrType()
        {
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.IpAddrGridBox.jsID = this.IpAddrGridBox.ClientID;
        }

        #endregion // Event Handlers
    }
}