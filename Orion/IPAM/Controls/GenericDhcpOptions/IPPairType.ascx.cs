﻿using System;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class IPPairType : System.Web.UI.UserControl
    {
        #region Declarations

        public int OptionCode { get; set; }

        public string WebId { get; set; }

        #endregion // Declarations

        #region Constructors

        public IPPairType()
        {
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.IpPairGridBox.jsID = this.IpPairGridBox.ClientID;
        }

        #endregion
    }
}