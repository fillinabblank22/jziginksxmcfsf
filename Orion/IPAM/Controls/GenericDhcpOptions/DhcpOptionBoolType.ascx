﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpOptionBoolType.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpOptionBoolType" %>

<script type="text/sw-javascript" language="javascript">

<%--    if (!$SW.IPAMValid) $SW.IPAMValid = {};
    $SW.IPAMValid.WebId = '<%=this.WebParamId %>';--%>
    
    var optionCode = '<%=this.OptionCode %>';
    var webParamId = '<%=HttpUtility.HtmlEncode(this.WebParamId) %>';
    
    var SetBoolOptionParse = function () {
        
        var ret = $SW.IPAM.GetOptionValue(webParamId, optionCode);

        $('#' + '<%= helpTextID.ClientID %>').html('');
        $('#' + '<%= helpTextID.ClientID %>').html('<p>' + ret.data.HelpTextValue + '</p>');
            
        if(ret.data.optionValue == 0 || ret.data.optionValue < 0)
            $($('#' + '<%= rdoFalse.ClientID %>')).attr('checked', (ret.data.optionValue == 0));

        return;
    };
    
    function SaveBoolTypeOptionValue () {
        
        var isTrue = $($('#' + '<%= rdoTrue.ClientID %>')).is(":checked");
       
        var ret = 0;
        if (isTrue) 
            ret = 1;

        $SW.IPAM.SetOptionValue(webParamId, optionCode, ret,"False");
        
        return false;
    };
    
    var CancelControl = function () {
        $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
        return false;
    };
    
    $(document).ready(function() {
        SetBoolOptionParse();
    });
    
</script>

<div id="UserControlContainer" runat="server" style="padding: 8px 6px;margin-bottom: 20px;max-width: 876px;">

    <table width="600px">

       <tr style="background-color:#FFF7CD;">
            <td>
                <div>
                    <div id="Div1" runat="server" class="OptionHelpDescription" style="margin:5px 10px 5px 0px;padding: 5px 0 5px 10px;">
                        <table>
                            <tr>
                                <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                <td>
                                    <div>
                                        <asp:Label ID="helpTextID" runat="server" />
                                    </div>                                 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
        
        <tr><td style="width: 3px; padding: 3px"></td></tr>

        <tr>
            <td>
                <div class="blueBox">

                    <div>

                        <table class="sw-form-wrapper">
                            
                            <tr class="sw-form-cols-normal">
                                <td class="sw-form-col-label"/>
                                <td class="sw-form-col-control" />
                                <td class="sw-form-col-comment" />
                            </tr>
                            <tr>
                                <td colspan="3">

                                    <div>
                       
                                        <table><tr>
                                            <td>
                                                <asp:RadioButton runat="server" ID="rdoTrue" GroupName="LeaseTime" Checked ="True"/>
                                                <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_10%></span>
                                            </td>
                                        </tr><tr>
                                            <td>
                                                <asp:RadioButton runat="server" ID="rdoFalse" GroupName="LeaseTime" />
                                                <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_GK_11%></span>
                                            </td>
                                        </tr></table>
                                    </div>

                                </td>               
                            </tr>
           
                        </table>

                       </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="sw-btn-bar-wizard">
                    <orion:LocalizableButton runat="server" ID="optionimgbSave" CausesValidation="true" DisplayType="Primary" LocalizedText="Save" OnClientClick="SaveBoolTypeOptionValue(); return false;" />
                    <orion:LocalizableButton runat="server" ID="optionimgbCancel" CausesValidation="false" OnClientClick="CancelControl(); return false;" DisplayType="Secondary" LocalizedText="Cancel" />
                </div>
            </td>
        </tr>
    </table>
</div>