﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpOptionLeaseSetting.ascx.cs" Inherits="SolarWinds.IPAM.WebSite.DhcpOptionLeaseSetting" %>

<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<orion:Include File="breadcrumb.js" runat="server" />

<script type="text/sw-javascript" language="javascript">

    var optionCode  = '<%=this.OptionCode %>';
    var webParamId  = '<%=HttpUtility.HtmlEncode(this.WebParamId) %>';
    var ids         = ['txtDays', 'txtHours', 'txtMinutes'];
    var ret;
    var serverType='';
    
    var CancelControl = function () {
        $SW.IPAM.ShowOptionLayoutDialog.HideDialog();
        return false;
    };
                
    var LimitedBtnClick = function(button, delay) {
        var div = $('#' + '<%= limitedDiv.ClientID %>');
        var checked = button.checked;

        if (div) {
            if (checked === true)
                div.show(delay);
            else
                div.hide(delay);

            $.each(ids, function() {
                var item = $($('#' + '<%= this.ClientID %>'))[0];
                if (!item || !item.Validators)
                    return true;
                $.each(item.Validators, function() {
                    ValidatorEnable(this, checked);
                });
            });
        }
        return;
    };

    function SaveLeaseTimeOptionValue () {
        
        var isValid = $SW.IPAM.LeaseTimeValidation($('#' + '<%= LimitedBtn.ClientID %>').is(":checked"),$('#' + '<%= txtDays.ClientID %>').val(),$('#' + '<%= txtHours.ClientID %>').val(),$('#' + '<%= txtMinutes.ClientID %>').val(),'<%=this.GetLeaseTimeErrorMessage %>',serverType);
        var leaseTimeValue = '0';

        if (isValid) {
            if ($('#' + '<%= LimitedBtn.ClientID %>').is(":checked"))
                leaseTimeValue = ($('#' + '<%= txtDays.ClientID %>').val() * 86400) + ($('#' + '<%= txtHours.ClientID %>').val() * 3600) + ($('#' + '<%= txtMinutes.ClientID %>').val() * 60);
            else 
                leaseTimeValue = '<%=this.InfiniteLeaseTimeValue %>';

            $SW.IPAM.SetOptionValue(webParamId, optionCode, leaseTimeValue,"False");
        }

        return false;
    };
    
    var SetLeaseTimeParse = function() {
        
        ret = $SW.IPAM.GetOptionValue(webParamId,optionCode);
        
        $('#' + '<%= helpTextID.ClientID %>').html(''); 
        $('#' + '<%= helpTextID.ClientID %>').html('<p>' + ret.data.HelpTextValue + '</p>');

        if (ret.data.LimittedButton == "0")
            $('#' + '<%= UnlimitedBtn.ClientID %>').attr('checked', true);
        else {
            $('#' + '<%= LimitedBtn.ClientID %>').attr('checked', true);
            
            var div = $('#' + '<%= limitedDiv.ClientID %>');
            if (div) {
                div.show(0);
            }

            $('#' + '<%= txtDays.ClientID %>').val(ret.data.Days);
            $('#' + '<%= txtHours.ClientID %>').val(ret.data.Hours);
            $('#' + '<%= txtMinutes.ClientID %>').val(ret.data.Minutes);
        }
        
        var isCapableOfLeaseTime = (ret.data.isCapableOfLeaseTime == "1") ? true : false;
        var isCapableOfInfiniteTime = (ret.data.isCapableOfInfiniteTime == "1") ? true : false;

        if (!isCapableOfInfiniteTime) {
            $('#' + '<%= UnlimitedBtn.ClientID %>').attr("disabled", true);
            $('#' + '<%= LimitedBtn.ClientID %>').attr("disabled", true);
        }

        if (!isCapableOfLeaseTime) {
            $('#' + '<%= txtDays.ClientID %>').attr("disabled", true);
            $('#' + '<%= txtHours.ClientID %>').attr("disabled", true);
            $('#' + '<%= txtMinutes.ClientID %>').attr("disabled", true);
        }

        if (!isCapableOfLeaseTime)
            $('#' + '<%= divLeaseLast.ClientID %>').css("display", "none");

        serverType = ret.data.ServerType;
    };

    $(document).ready(function() {
        var button = $($('#' + '<%= LimitedBtn.ClientID %>'))[0];
        $($('#' + '<%= LimitedBtn.ClientID %>')).click( function(){ LimitedBtnClick(button, 250); } );
        $($('#' + '<%= UnlimitedBtn.ClientID %>')).click( function(){ LimitedBtnClick(button, 250); } );
        LimitedBtnClick(button, 0);
        
        $($('#' + '<%= imgbCancel.ClientID %>')).click(function () { $('#' + '<%= imgbCancel.ClientID %>').hide(0); });

        SetLeaseTimeParse();
    });
    
</script>
    
<IPAMui:ValidationIcons runat="server" />

<div id="UserControlContainer" style="padding: 8px 6px;margin-bottom: 20px;" runat="server">

    <div>
        
        <table width="600px" cellpadding="0" cellspacing="0">

            <tr style="background-color:#FFF7CD;">
                <td>
                    <div>
                        <div id="Div1" runat="server" class="OptionHelpDescription" style="margin:5px 10px 5px 0px;padding: 5px 0 5px 10px;">
                            <table>
                                <tr>
                                    <td><img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" /></td>
                                    <td>
                                        <div>
                                            <asp:Label ID="helpTextID" runat="server" />
                                        </div>                                 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>

            <tr><td style="width: 3px; padding: 3px"></td></tr>
            
            <tr><td>

                <div runat="server" class="blueBox" id="NormalText">
            
                    <div>
                        <div>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_542 %><br />
                        </div>

                        <div style="padding-left: 20px; padding-top: 10px">
                            <ul style="list-style-type: square">
                                <li><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_543 %></li>
                                <li><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_544 %></li>
                            </ul>
                        </div>
                        
                        <div><!-- ie6 --></div>

                        <div style="padding-top: 10px">
                    
                          <div id="divLeaseLast" runat="server">
            
                            <p><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545%></b></p>

                            <p style="padding-top: 5px;">
                                <asp:RadioButton runat="server" ID="UnlimitedBtn" GroupName="LeaseTime" Checked="True"/>
                                <span style="padding-left:2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545_2%></span>
                            </p>

                            <p style="padding-top: 5px;">
                                <asp:RadioButton runat="server" ID="LimitedBtn" GroupName="LeaseTime" />
                                <span style="padding-left: 2px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_545_1%></span>
                            </p>

                            <div id="limitedDiv" runat="server">
                            
                              <div style="padding:5px 20px; margin-bottom: 8px;"> <%--width: 93%;--%>
                              
                                <span><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_546%></span>
                                <asp:TextBox runat="server" ID="txtDays" Width="20" MaxLength="3" CssClass="x-form-text x-form-field" Text="8" />
                                <asp:RequiredFieldValidator ID="DaysRequire" runat="server" ControlToValidate="txtDays" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_547 %>" />
                                <asp:CompareValidator ID="DaysAbove" runat="server" ControlToValidate="txtDays" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ resources : IPAMWebContent, IPAMWEBDATA_VB1_548 %>" />
                                <asp:CompareValidator ID="DaysBelow" runat="server" ControlToValidate="txtDays" Operator="LessThanEqual" ValueToCompare="999" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_549 %>" />

                                <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_550 %></span>
                                <asp:TextBox runat="server" ID="txtHours" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" Text="0" />
                                <asp:RequiredFieldValidator ID="HoursRequire" runat="server" ControlToValidate="txtHours" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_551 %>" />
                                <asp:CompareValidator ID="HoursAbove" runat="server" ControlToValidate="txtHours" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_552 %>" />
                                <asp:CompareValidator ID="HoursBelow" runat="server" ControlToValidate="txtHours" Operator="LessThanEqual" ValueToCompare="23" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_556 %>" />

                                <span style="padding-left:25px"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_553 %></span>
                                <asp:TextBox runat="server" ID="txtMinutes" Width="20" MaxLength="2" CssClass="x-form-text x-form-field" Text="0" />
                                <asp:RequiredFieldValidator ID="MinutesRequire" runat="server" ControlToValidate="txtMinutes" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_554 %>" />
                                <asp:CompareValidator ID="MinutesAbove" runat="server" ControlToValidate="txtMinutes" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_555 %>" />
                                <asp:CompareValidator ID="MinutesBelow" runat="server" ControlToValidate="txtMinutes" Operator="LessThanEqual" ValueToCompare="59" Type="Integer" Display="None" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_557 %>" />

                              </div>

                              <div style="font-family:Arial; font-size:10px; color:#646464; margin-bottom: 8px;">
                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_558 %>
                              </div>

                            </div>

                          </div>

                          <div><!-- ie6 --></div>

                    </div>

                </div>

            </div>

        </td></tr>

        <tr>
            <td>
                <div>
                    <div class="sw-btn-bar-wizard">
                        <orion:LocalizableButton runat="server" CausesValidation="true" ID="imgbSave" DisplayType="Primary" LocalizedText="Save" OnClientClick="SaveLeaseTimeOptionValue();return false;" />
                        <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false" DisplayType="Secondary" LocalizedText="Cancel" OnClientClick="CancelControl();return false;" />
                    </div>
                </div>
            </td>
        </tr>

        <tr><td>
            <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" DisplayMode="BulletList" />
        </td></tr>

        </table>

    </div>

</div>