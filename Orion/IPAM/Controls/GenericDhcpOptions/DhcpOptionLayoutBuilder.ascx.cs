﻿using System;

public partial class Orion_IPAM_DhcpOptionLayoutBuilder : System.Web.UI.UserControl
{
    #region Declarations

    public string WebId { get; set; }

    public int ServerType { get; set; }

    #endregion // Declarations

    public Orion_IPAM_DhcpOptionLayoutBuilder()
    {

    }

    protected override void OnLoad(EventArgs e)
    {
        IPAddressTypeID.WebId = this.WebId;
        IPAddressPairTypeID.WebId = this.WebId;
    }
}