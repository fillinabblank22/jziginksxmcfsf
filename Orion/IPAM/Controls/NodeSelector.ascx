<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeSelector.ascx.cs" Inherits="Orion_IPAM_Controls_NodeSelector" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>

<IPAMmaster:CssBlock runat="server">
.test { max-width: 890px; }
.test .x-panel-body { min-height: 90px; }

.sw-noicon { display: none; }
.sw-blankbg { background: none !important; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Orientation="jsInit" runat="server">
if( !$SW.GroupBy ) $SW.GroupBy = {};

$SW.GroupBy.StatusMap = { "1": "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_22;E=js}", "2": "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_23;E=js}", "3": "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_24;E=js}", "9": "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_25;E=js}", "11": "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_26;E=js}" };
$SW.GroupBy.ApplyVendorIcons = function(id){
  if( !$SW.GroupBy.VendorIcons ) return;

  var node, cmp = Ext.getCmp(id);
  if( cmp )
  jQuery.each( $SW.GroupBy.VendorIcons || [], function(n,item){
    if( node = cmp.getNodeById('v'+(item.Vendor||'')) )
    {
       var ico = node.getUI().getIconEl();
       ico.src = node.icon = '/NetPerfmon/images/Vendors/' + item.VendorIcon;
       Ext.fly(ico).addClass('sw-blankbg').removeClass('sw-noicon')
       node.iconCls = 'sw-blankbg';
    }
  });
};

$SW.GroupBy.GetVendorIcons = function(id){
  var treeid = id;
  $SW.DoQuery( "SELECT DISTINCT N.Vendor, N.VendorIcon FROM Orion.Nodes N LEFT JOIN Orion.Engines E ON N.EngineID = E.EngineID WHERE E.ServerType <> 'RemoteCollector'", function(values){
    $SW.GroupBy.VendorIcons = values;
    if( !Ext.getCmp(treeid).getLoader().loading ) $SW.GroupBy.ApplyVendorIcons(treeid);
  }, { service: 'Orion' } );
};

$(document).ready(function(){

    var transform = function(id,extid){

        var item = $('#'+id)[0];
        if( !item) return null;

        var converted = new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            id: extid,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        });

        return converted;
    };

  transform( $SW.nsGet($nsId, 'GroupBySelect'), 'GroupBySelect' ).on( 'select', function(me){
    var tree = Ext.getCmp('GroupBySelector');
    tree.getLoader().load( tree.getRootNode() );
  });
});
</IPAMmaster:JsBlock>

<div><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_154 %><br />
    <div class="sw-form-item">
        <asp:DropDownList ID="GroupBySelect" runat="server">
            <asp:ListItem Value="Vendor" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_155 %>" />
            <asp:ListItem Value="MachineType" Selected="True" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_156 %>" />
            <asp:ListItem Value="SNMPVersion" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_157 %>" />
            <asp:ListItem Value="Status" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_158 %>" />
            <asp:ListItem Value="Location" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_159 %>" />
            <asp:ListItem Value="Contact" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_160 %>" />
            <asp:ListItem Value="Community" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_161 %>" />
            <asp:ListItem Value="RWCommunity" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_162 %>" />
        </asp:DropDownList>
    </div>
</div>

<IPAMlayout:TreeBox id="GroupBySelector" runat="server"
    MultipleSelection="true"
    borderVisible="true"
    cssClass="test">
<Store ID="Store1" runat="server"
    proxyUrl="/Orion/Services/Information.asmx/Query"
    listeners="{
beforeload: function(me){ me.loading=1; },
load: function(me,node){ 
  me.loading=0;

  if( node.isRoot ){
    var treeid = node.getOwnerTree().id, prop = Ext.getCmp('GroupBySelect').value;

    if( Ext.getCmp('GroupBySelect').value == 'Vendor' ) {
      window.setTimeout( function(){ $SW.GroupBy.ApplyVendorIcons(treeid); }, 10 );
    }
  }
},
loadexception: function(me){ me.loading=0; } }">
<fnRemap>
function(node,rows){
 var i, rm=[], unknowns=0;
 
 jQuery.each( rows, function(idx,row){

  if( node.isRoot ){

   var groupby_prop = Ext.getCmp('GroupBySelect').value;
   if( groupby_prop == 'MachineType'){ 
     if( !row.text || row.text == 'Unknown' ){ 
     rm.push(idx); unknowns += parseInt(row.n);
     return;
     }
   }

   row.id = row.text;
   if( row.id == null ) row.id = '';
   if( groupby_prop == 'Status' ) row.text = $SW.GroupBy.StatusMap[''+row.id] || '[Unknown]';
   else row.text = ((row.id === '') ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_21;E=js}' : row.id) + " (" + row.n + ")";
   row.id = 'v' + row.id;
   row.noselect = 1;
   row.iconCls = 'sw-noicon';
   
   if( groupby_prop == 'SNMPVersion' && row.id == 'v0' )
    row.text = 'Disabled';
  }
  else {
   row.uiProvider = 'checkable';
   row.leaf = true;
   row.iconCls = 'sw-noicon';
   
   row.text = String.format(
    "<a href='#NetObject=N:{4}' onclick='return false;' " +
    "community='GUID({0})' " +
    "nodehostname='{1}' " +
    "ip='{2}'>" +
    "<img src='/Orion/images/StatusIcons/Small-{3}' style='vertical-align: top; margin-top: 1px;' class='StatusIcon' netobject='N:{4}'/>" +
    "<span>{5}</span></a>",
      row.CommunityGuid, // replace with community
      ($.trim(row.DNS)||$.trim(row.SysName)||$.trim(row.IPAddress)),
      row.IPAddress, // replace with ip
      $.trim(row.GroupStatus) || 'unknown.gif', // replace with status
      row.NodeId,
      row.text );

   row.id = 'g' + row.id;
  }
 });
 
 if( i = rm.length )
    for( ; i-- ; )
        rows.splice( rm[i], 1 );

 if( unknowns ) rows.splice( 0, 0, { text: '[Unknown] (' + unknowns + ")", n: unknowns, id: 'v', noselect: 1, iconCls: 'sw-noicon' } ); 
 return rows;
}
</fnRemap>
<fnQueryBuilder>
function(node,loader){

  var groupby_prop = Ext.getCmp('GroupBySelect').value, treeid = node.getOwnerTree().id;

  if( node.isRoot )
  {
    if( groupby_prop == 'Vendor' ) $SW.GroupBy.GetVendorIcons(treeid);
    return { query: String.format( "SELECT N.{0} AS \"text\", COUNT(ISNULL(N.{0},'')) AS \"n\" FROM Orion.Nodes N LEFT JOIN Orion.Engines E ON N.EngineID = E.EngineID WHERE E.ServerType <> 'RemoteCollector' GROUP BY N.{0} ORDER BY N.{0} ASC", groupby_prop ) };
  }

  var where1 = "ISNULL(N.{0},'') = '{1}'",
      where2 = "ISNULL(N.{0},'') = '' OR N.{0} = 'Unknown'",
      fmt1 = "SELECT N.NodeId AS \"id\", N.NodeId, N.DNS, N.SysName, N.IPAddress, N.WebCommunityString.GUID as \"CommunityGuid\", N.GroupStatus, N.Caption As \"text\", N.Vendor FROM Orion.Nodes N LEFT JOIN Orion.Engines E ON N.EngineID = E.EngineID WHERE E.ServerType <> 'RemoteCollector' AND ({0}) ORDER BY N.Caption ASC";

  return { query: 
    String.format( fmt1,
        String.format( (groupby_prop == 'MachineType' && node.id == 'v') ? where2 : where1, groupby_prop, node.id.substring(1) )
        )
    };
}
</fnQueryBuilder>
<Fields>
    <IPAMlayout:TreeStoreField ID="TreeStoreField1" dataIndex="id" isKey="true" runat="server" />
    <IPAMlayout:TreeStoreField ID="TreeStoreField2" dataIndex="text" runat="server" />
</Fields>
</Store>
</IPAMlayout:TreeBox>
