﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPv6.PrefixSize.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.IPv6PrefixSize" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
#prefix-size .sw-form-clue { font-size: 11px; font-style: normal; 
    white-space: nowrap; margin: 4px 0 0; color: gray; position:relative; overflow: visible; }
#prefix-size a { font-size: 11px; text-decoration: none; color: #336699; }
#prefix-size a:hover { color: orange; }
#prefix-size .LinkArrow { font-size: 11px; vertical-align: text-top; }
</IPAMmaster:CssBlock>

<style type="text/css">
    .x-form-field-wrap .short-size {
    width: 30px !important;
}
</style>

<IPAMmaster:JsBlock ID="initUpDownBox" Requires="ext,ext-quicktips,sw-ipv6-manage.js" Orientation="jsPostInit" runat="server" />

<div id="prefix-size">
  <table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="width: 20px !important; white-space: normal"><asp:TextBox ID="txtSize" CssClass="x-form-text x-form-field short-size" runat="server" MaxLength="3" /></td>
        <td style="width: auto; text-align: left;">
            &nbsp;&nbsp;<asp:Label ID="MaxPrefixSize" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_441 %>" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="sw-form-clue"><span id="subnets-count">{X}</span><%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_439, "")%></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="sw-form-clue">
                <span class="LinkArrow">&#0187;</span>
                <asp:HyperLink ID="LearnMoreLink" Target="_blank"
                    Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_440 %>" runat="server" />
            </div>
        </td>
    </tr>
  </table>
</div>