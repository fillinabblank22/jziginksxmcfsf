﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VrfGroupName.ascx.cs" Inherits="Orion_IPAM_Controls_VrfGroupName" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:CssBlock runat="server">
    .new-vrf-group-name-field { width: 170px !important; }
    .new-vrf-group-name { width: 200px; }
    .drop-down-vrf-group-name { width: 155px; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,sw-ux-ext.js,ext-quicktips,sw-helpserver,sw-ux-wait.js" Orientation="jsPostInit" runat="server">
    $(document).ready(function(){
    var transformCBox = function(id,extid){
        var item = $('#'+id)[0];
        if( !item) return null;

        var converted = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        transform: id,
        hiddenName: item.name,
        hiddenId: id,
        id: extid,
        forceSelection: true,
        width: 'auto',
        autoLoad: true,
        listWidth:'200',
        emptyText:'Select one...',
        enableKeyEvents: true,
        disabled: item.disabled || false,
        listeners:{
            select: function(select) {
                if(select.lastSelectionText == '@{R=IPAM.Strings;K=IPAMWEBDATA_DM1_8;E=js}') {
                    $('.new-vrf-group-name').show();
                } else {
                    $('.new-vrf-group-name').hide();
                }
            },
            afterrender : function (select) {
                if(select.lastSelectionText == '@{R=IPAM.Strings;K=IPAMWEBDATA_DM1_8;E=js}') {
                    $('.new-vrf-group-name').show();
                } else {
                    $('.new-vrf-group-name').hide();
                }
            }
        }
    });

    return converted;
    };

    var enableLevelCBox = transformCBox( $SW.nsGet($nsId, 'ddlVrfGroupName'), 'ddlVrfGroupName' );
	transformCBox( $SW.nsGet($nsId, 'fakeGroupName'), 'ddlVrfGroupName2' );
});
</IPAMmaster:JsBlock>

<tr id='fake' style="visibility: collapse">
	<td>
		<div class="sw-field-label">
			<%= Resources.IPAMWebContent.IPAMWEBDATA_DM1_7%>
		</div>
	</td>
	<td>
		<div class="sw-form-item sw-form-cb-stretch drop-down-vrf-group-name">
			<asp:DropDownList ID="fakeGroupName" runat="server" Enabled="False">
				<asp:ListItem Value="" Text="IP Networks" runat="server" />
			</asp:DropDownList>
		</div>
	</td>
</tr>

<tr id='real'>
	<td>
		<div class="sw-field-label">
			<%= Resources.IPAMWebContent.IPAMWEBDATA_DM1_7%>
		</div>
	</td>
	<td>
		<div class="sw-form-item sw-form-cb-stretch drop-down-vrf-group-name">
			<asp:DropDownList ID="ddlVrfGroupName" runat="server">
				<asp:ListItem Value="" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_8 %>" runat="server" />
			</asp:DropDownList>
		</div>
	</td>
</tr>
<tr id='newGroupRow'>
	<td>
		<div class="sw-field-label new-vrf-group-name"></div>
	</td>
	<td>
		<div class="new-vrf-group-name sw-form-item">
			<asp:TextBox ID="newVrfGroupName" CssClass="new-vrf-group-name-field x-form-text x-form-field" runat="server" placeholder="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_12 %>" />
		</div>
	</td>
	<td>
        <asp:CustomValidator
            ID="NewVrfGroupNameRequired"
            runat="server"
            Display="Dynamic"
            ControlToValidate="newVrfGroupName"
            OnServerValidate="OnIsNewVrfGroupNameRequire"
            ValidateEmptyText="true"
            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_10 %>"></asp:CustomValidator>
        <asp:CustomValidator 
            ID="NewVrfGroupNameExists"
            runat="server"
            Display="Dynamic"
            ControlToValidate="newVrfGroupName"
            OnServerValidate="OnNewVrfGroupNameExists"
            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_DM1_11 %>"></asp:CustomValidator>
    </td>
</tr>
