<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.TimeSpanEditor" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock Requires="ext" Orientation="jsPostInit" runat="server">
$(document).ready(function(){
  var id = $SW.nsGet($nsId, 'ddlMultiplier');
  $SW.TransformDropDown( id, id, { hiddenId: id } ).on( "select", function(){
    $SW.Valid.Retest( $SW.ns$($nsId, 'txtFrequency' )[0] );
  });
});
</IPAMmaster:JsBlock>

<table style="table-layout: fixed; width: <%= Width %>;" border="0" cellspacing="0" cellpadding="0"><tr class="sw-validicon-redir">

<td style="width: 48px;"><div class="sw-form-item">
  <div style="margin-right: 14px;"><asp:TextBox ID="txtFrequency" CssClass="x-form-text x-form-field" style="width: 32px;" Text="" runat="server" /></div>
</div></td>

<td style="width: 130px"><div class="sw-form-item sw-form-cb-stretch"><asp:DropDownList id="ddlMultiplier" runat="server">
    <asp:ListItem Value="1" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_174 %>"/>
    <asp:ListItem Value="60" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_175 %>" />
    <asp:ListItem Value="3600" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_176 %>" />
    <asp:ListItem Value="86400" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_177 %>" />
</asp:DropDownList></div></td>

<td style="width: 20px;"><div style="position: relative; height: 20px;" id="icotarget" runat="server" class="sw-validicon-target">
<asp:CustomValidator
    ID="ValidateTimeSpan"
    runat="server"
    Display="Dynamic"
    ValidateEmptyText="true"
    ControlToValidate="txtFrequency"
    EnableViewState="false"
    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_178 %>"></asp:CustomValidator>
</div></td>

</tr></table>

<asp:Label runat="server" Visible="false" ID="ValidationScript">
$SW.ValidateTimeSpan = function(opts){
  opts = opts||{};
  var freq = opts.txtid, mul = opts.selid, timemin = opts.tmin, timemax = opts.tmax, validif = opts.validif || '';
  var activeWhenChecked = opts.activeWhenChecked == true;
  
  function isEmpty(str) {
      return (!str || 0 === str.length);
  }

  return function(src,args){

    var ctrl, val1 = parseFloat( $.trim( $('#'+freq)[0].value.replace(',','.') ) ), val2 = parseFloat( $.trim( $('#'+mul)[0].value ) );

    if(!isEmpty(validif))
    {
        if( ctrl = $('#'+validif)[0] )
        {
          if( (ctrl.type == 'checkbox' && (ctrl.checked ^ activeWhenChecked)) || ($.trim(ctrl.value) == '') )
            return args.IsValid = true;
        }
    }
    
    if( isNaN(val1) ) return args.IsValid = false;
    if( (val1*val2) < timemin ) return args.IsValid = false;
    if( (val1*val2) > timemax ) return args.IsValid = false;
    return args.IsValid = true;
  };
};
</asp:Label>