﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressLink.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.IPAddressLink" %>
<img id="Used" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_233 %>" src="/Orion/IPAM/res/images/sw/icon.ip.used.gif" visible="false" style="vertical-align: middle;" runat="server" />
<img id="Available" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_261 %>" src="/Orion/IPAM/res/images/sw/icon.ip.available.gif" visible="false" style="vertical-align: middle;" runat="server" />
<img id="Reserved" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_264 %>" src="/Orion/IPAM/res/images/sw/icon.ip.reserved.gif" visible="false" style="vertical-align: middle;" runat="server" />
<img id="Transient" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_262 %>" src="/Orion/IPAM/res/images/sw/icon.ip.transient.gif" visible="false" style="vertical-align: middle;" runat="server" />
<img id="Blocked" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_847 %>" src="/Orion/IPAM/res/images/sw/icon.ip.blocked.gif" visible="false" style="vertical-align: middle;" runat="server" />
<span style="vertical-align: middle;"><asp:PlaceHolder runat="server" ID="ipAddressValue" /></span>    
