﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPv6.CompoundAddress.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.IPv6CompoundAddress" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMctrl" Assembly="SolarWinds.IPAM.Web.Common" Namespace="SolarWinds.IPAM.Web.Common.Controls" %>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
#prefix-address .sw-form-clue { font-size: 12px; font-style: normal; white-space: nowrap; margin-bottom: 6px; }
#prefix-address a { text-decoration: none; color: #336699; }
#prefix-address .readonly { color: gray; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock ID="initAddressBox" Requires="ext,ext-quicktips,sw-ipv6-manage.js" Orientation="jsPostInit" runat="server" />

<div id="prefix-address">
    <div id="prefixes-list" class="sw-form-clue">
        <IPAMctrl:RichTextBulettedList id="IPv6PrefixesList" Visible="false" runat="server" />
    </div>
    <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td id="tdAddress" runat="server">
                <asp:Label ID="lblAddress" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_335 %>" runat="server" />
            </td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtAddress" Text="::" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td>
                <asp:RequiredFieldValidator ID="IPv6AddressRequireValidator" runat="server" Display="Dynamic"
                    ControlToValidate="txtAddress" ValidateEmptyText="true"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_437 %>" />
                <asp:CustomValidator ID="IPv6AddressCustomValidator" runat="server" Display="Dynamic"
                    ControlToValidate="txtAddress" ValidateEmptyText="true"
                    OnServerValidate="OnIPv6AddressValidator"
                    ClientValidationFunction="$SW.IPv6.IPv6AddressValidator" EnableClientScript="true"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_438 %>" />
            </td>
        </tr>
    </table>
</div>