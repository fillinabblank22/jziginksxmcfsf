<%@ Control Language="C#" ClassName="FilterScopesSql" %>

<script runat="server">
	public TextBox FilterTextBox { get { return Filter; } }
</script>

<p>
	<b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_652 %></b><br />
	<asp:TextBox runat="server" ID="Filter" Width="330"></asp:TextBox>
</p>

<p><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_653 %></p>

<orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
	<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_654 %></b></TitleTemplate>
	
	<BlockTemplate>
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_655 %>

		<orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
			<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_656 %></b></TitleTemplate>
			<BlockTemplate>
				<ul>
					<%foreach (string column in SolarWinds.IPAM.BusinessObjects.GroupNode.SQLFILTER_ALLSUBNETPROPERTIES) {%>
						<li>Scope.<%=column%></li>
					<%}%>
				</ul>
			</BlockTemplate>
		</orion:CollapsePanel>

	</BlockTemplate>
</orion:CollapsePanel>
