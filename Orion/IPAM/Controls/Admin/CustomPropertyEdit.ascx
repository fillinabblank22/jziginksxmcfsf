<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomPropertyEdit.ascx.cs" Inherits="CustomPropertyEdit" EnableViewState="true" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register Src="~/Orion/Controls/EditCpValueControl.ascx" TagPrefix="orion" TagName="EditCpValueControl" %>
<orion:Include ID="Include1" runat="server" File="CustomPropertyEditor.css" />
<style type="text/css">
    .cpRepeaterTable .leftLabelColumn {
        color: #000000;
        font-family: Arial, Verdana, Helvetica, sans-serif !important;
        font-size: small;
        vertical-align: left;
        padding: 0px 0px 0px 0px;
        margin 0 0 0 0;
    }
    
    .cpRepeaterTable .leftLabelColumn {
        width: 185px; 
    }

    .scopeAvailability {
        margin: 5px 10px 5px 0px;
        padding: 5px 0 5px 10px;
        background-color:#FFF7CD;
    }
</style>
<% if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.SiteAdmin) == false
     && SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer == false)
    { %>
<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js" Orientation="Inline" runat="server">
Ext.onReady(function(){ $(".ArrowedLink > a").each($SW.SubnetNoAccessOnClick); });
</IPAMmaster:JsBlock>
<%}%>
<asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true" />
<asp:UpdatePanel ID="CustomPropertyEditUpdatePanel" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <div id="CustomPropertiesHeader">
            <span id="availableLabel" style="display: none;" class="sw-field-label scopeAvailability">
                <table>
                    <tr>
                        <td>
                            <img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" />
                        </td>
                        <td>
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_911 %> <a href="#" name="ddlStatus"> status</a>
                        </td>
                    </tr>
                </table>
            </span>
        <div>
        <div id="CustomPropertiesContent">
            <table class="blueBox cpRepeaterTable">
                <asp:Repeater runat="server" ID="PropertiesRepeater" OnItemDataBound="repCustomProperties_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td class="leftLabelColumn" style="vertical-align: super;">
                                <div class="sw-field-label">
                                    <asp:CheckBox runat="server" ID="cbSelectedProperty" AutoPostBack="true" OnCheckedChanged="cbSelectedProperty_CheckedChanged" />
                                    <asp:Literal runat="server" ID="litPropertyName" ></asp:Literal><%= Resources.CoreWebContent.Punctuation_mark_colon %>
                                </div>
                            </td>
                            <td class="rightInputColumn">
                                <orion:EditCpValueControl ID="PropertyValue" runat="server" />
                                <span class="helpfulText" style="padding: 0px;">
                                    <asp:Literal ID="descriptionText" runat="server" Text="" />
                                </span>
                                <asp:PlaceHolder runat="server" ID="placeHolder"></asp:PlaceHolder>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div id="CustomPropertiesFooter">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">
                    <td class="sw-form-col-label"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="ArrowedLink">
                            <img src="/Orion/Nodes/images/icons/icon_edit.gif" alt="" />
                            <a target="_blank" href="/Orion/Admin/CPE/Default.aspx" onclick="if($('#isDemoMode').length > 0) {demoAction('IPAM_Custom_Properties', null); return false; }">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources : IPAMWebContent , IPAMWEBDATA_VB1_188%>"/>
                            </a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
