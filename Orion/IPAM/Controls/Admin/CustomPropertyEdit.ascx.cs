using System;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using SolarWinds.InformationService.Contract2;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Widgets;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.CPE;
using SolarWinds.Orion.Core.Common.Indications;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web.Helpers;
using System.Text;

public partial class CustomPropertyEdit : System.Web.UI.UserControl
{
    private List<CustomProperty> customProperties;
    private List<string> customPropertiesNamesList;

    public List<string> CheckBoxesClientIds { get; set; }
    public string CustomPropertyObject { get; set; }
    public IList<int> EntityIds { set; get; }

    public int Count
    {
        get { return customProperties != null ? customProperties.Count : 0; }
    }

    public bool IsMultiselected
    {
        get
        {
            IList<int> ids = EntityIds;

            if (ids == null)
            {
                return false;
            }

            return (ids.Count > 1);
        }
    }

    public Dictionary<string, object> CustomProperties
    {
        get
        {
            var result = new Dictionary<string, object>();
            foreach (RepeaterItem item in PropertiesRepeater.Items)
            {
                CheckBox cbSelectedProperty = (CheckBox)item.FindControl("cbSelectedProperty");
                Literal litPropertyName = (Literal)item.FindControl("litPropertyName");

                var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");
                result.Add(litPropertyName.Text, TryGetCustomPropertyValueWithType(litPropertyName.Text, txtCustomPropertyValue.Text));
            }
            return result;
        }
        set
        {
            if (value == null) return;
            foreach (RepeaterItem item in PropertiesRepeater.Items)
            {
                Literal litPropertyName = (Literal)item.FindControl("litPropertyName");
                if (!value.ContainsKey(litPropertyName.Text))
                {
                    continue;
                }

                var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");
                if (txtCustomPropertyValue != null)
                {
                    txtCustomPropertyValue.Text = value[litPropertyName.Text].ToString();
                }
            }
        }
    }

    public void RetrieveCustomProperties(ICustomProperties node, IList<int> ids)
    {
        if (customProperties != null)
        {
            return;
        }

        EntityIds = ids;

        IpamClientProxy proxy = SolarWinds.IPAM.Web.Common.Utility.SwisConnector.OpenCacheConnection();

        switch (CustomPropertyObject)
        {
            case "IPAM_GroupAttrData":
                customProperties = proxy.AppProxy.CustomProperty.GetAllFromGroupNode();
                break;
            case "IPAM_NodeAttrData":
                customProperties = proxy.AppProxy.CustomProperty.GetAllFromIPNode();
                break;
            default:
                throw new ArgumentException(nameof(CustomPropertyObject));
        }

        if (customProperties != null)
        {
            customPropertiesNamesList = customProperties.Select(cp => cp.PropertyName).ToList();
        }
    }

    public bool CheckIfCpChanged()
    {
        foreach (RepeaterItem item in PropertiesRepeater.Items)
        {
            var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl) item.FindControl("PropertyValue");
            if (txtCustomPropertyValue.Enabled)
            {
                return true;
            }
        }

        return false;
    }

    public void GetChanges(ICustomProperties node)
    {
        foreach (RepeaterItem item in PropertiesRepeater.Items)
        {
            var litPropertyName = (Literal)item.FindControl("litPropertyName");
            var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");

            if (txtCustomPropertyValue != null)
            {
                var value = TryGetCustomPropertyValueWithType(litPropertyName.Text, txtCustomPropertyValue.Text);
                node.SetCustomProperty(litPropertyName.Text, value.ToString());
            }
        }

        UpdateCustomPropertiesRestrictions();
    }

    public Dictionary<string, string> GetMultiEditChanges()
    {
        var result = new Dictionary<string, string>();
        foreach (RepeaterItem item in PropertiesRepeater.Items)
        {
            var litPropertyName = (Literal)item.FindControl("litPropertyName");
            var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");

            if (txtCustomPropertyValue != null)
            {
                if (txtCustomPropertyValue.Enabled)
                {
                    var value = TryGetCustomPropertyValueWithType(litPropertyName.Text, txtCustomPropertyValue.Text);
                    result.Add(litPropertyName.Text, value.ToString());
                }
            }
        }

        UpdateCustomPropertiesRestrictions();
        return result;
    }

    public void GetMultiEditIpChanges(ICustomProperties node)
    {
        var result = new Dictionary<string, string>();
        foreach (RepeaterItem item in PropertiesRepeater.Items)
        {
            var litPropertyName = (Literal)item.FindControl("litPropertyName");
            var txtCustomPropertyValue = (Orion_Controls_EditCpValueControl)item.FindControl("PropertyValue");

            if (txtCustomPropertyValue != null)
            {
                if (txtCustomPropertyValue.Enabled)
                {
                    var value = TryGetCustomPropertyValueWithType(litPropertyName.Text, txtCustomPropertyValue.Text);
                    node.SetCustomProperty(litPropertyName.Text, value.ToString());
                }
            }
        }

        UpdateCustomPropertiesRestrictions();
    }

    public void UpdateCustomPropertiesRestrictions()
    {
        foreach (var property in CustomProperties)
        {
            var customProperty = CustomPropertyMgr.GetCustomProperty(CustomPropertyObject, property.Key);

            CustomPropertyHelper.UpdateRestrictedValuesIfNeeded(customProperty,
                new[] { CustomPropertyHelper.GetInvariantCultureString(property.Value.ToString(), customProperty.PropertyType) });
        }
    }

    public string RegisterCheckboxesForValidation(string validators)
    {
        var js = new StringBuilder();
        if (CheckBoxesClientIds != null)
        {
            for (int i = 0; i < CheckBoxesClientIds.Count; i++)
            {
                js.AppendLine($"{validators}.push({CheckBoxesClientIds[i]});");
            }
        }

        return js.ToString();
    }

    public void DisableValidatorsByStatus(string statusName)
    {
        foreach (BaseValidator val in Page.Validators)
        {
            if (val.ValidationGroup == "EditCpValue"
                && val is CustomValidator
                && statusName == "Available")
            {
                val.Enabled = false;
            }
            else
            {
                val.Enabled = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CheckBoxesClientIds = new List<string>();
        if (!IsPostBack)
        {
            LoadCustomProperties();
        }
    }

    protected void cbSelectedProperty_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cbSelected = (CheckBox)sender;

        var txtPropertyValue = (Orion_Controls_EditCpValueControl)cbSelected.Parent.FindControl("PropertyValue");
        txtPropertyValue.Enabled = cbSelected.Checked;
    }

    protected void repCustomProperties_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			CheckBox cbSelectedProperty = (CheckBox)e.Item.FindControl("cbSelectedProperty");
		    CheckBoxesClientIds.Add(cbSelectedProperty.ClientID);
            Literal litCustomPropertyName = (Literal)e.Item.FindControl("litPropertyName");
			var customPropertyValue = (Orion_Controls_EditCpValueControl)e.Item.FindControl("PropertyValue");
			Literal txtDescr = (Literal)e.Item.FindControl("descriptionText");
			litCustomPropertyName.Text = e.Item.DataItem.ToString();

            CustomProperty customProperty = CustomPropertyMgr.GetCustomProperty(CustomPropertyObject, e.Item.DataItem.ToString());
            customPropertyValue.Configure(true, customProperty);

            var ids = EntityIds ?? new List<int>();
			var oldPropertyValue = "";
            txtDescr.Text = WebSecurityHelper.SanitizeHtmlV2(customProperty.Description);

            if ((ids == null || ids.Count == 0) && customProperty.Mandatory)
		    {
		        customPropertyValue.Text = string.Empty;
            }

			if (ids == null)
			{
				return;
			}

			if (ids.Count > 0)
			{
				oldPropertyValue = GetCustomProp(CustomPropertyObject, e.Item.DataItem.ToString(), ids[0]);
				customPropertyValue.Text = oldPropertyValue;
			}

			for (int i = 1; i < ids.Count; i++)
			{
				var goPropertyValue = GetCustomProp(CustomPropertyObject, e.Item.DataItem.ToString(), ids[i]);
				if (!oldPropertyValue.Equals(goPropertyValue, StringComparison.InvariantCultureIgnoreCase))
				{
					customPropertyValue.Text = string.Empty;
					cbSelectedProperty.Checked = false;

					break;
				}
				oldPropertyValue = goPropertyValue;
			}

            if (!IsMultiselected)
		    {
		        cbSelectedProperty.Visible = false;
		    }
		    else
		    {
		        cbSelectedProperty_CheckedChanged(cbSelectedProperty, EventArgs.Empty);
		    }
        }
	}

    private void LoadCustomProperties()
    {
        PropertiesRepeater.DataSource = customPropertiesNamesList;
        PropertiesRepeater.DataBind();
    }

    private object GetCPValue(object value)
	{
		if ((value == null) || (value == DBNull.Value))
			return string.Empty;
		else
			return value;
	}

	private PropertyBag GetChangedProperties(IDictionary<string, object> oldProperties, Dictionary<string, object> newProperties, bool isNew)
	{
		PropertyBag changedProperties = new PropertyBag();

		foreach (var property in newProperties)
		{
			object newValue = GetCPValue(property.Value);
			if (oldProperties.ContainsKey(property.Key))
			{
				object oldValue = GetCPValue(oldProperties[property.Key]);
				if (!newValue.Equals(oldValue))
					changedProperties.Add(property.Key, newValue);
			}
			else if (!isNew || !string.IsNullOrEmpty(GetCPValue(property.Value).ToString()))
			{
				changedProperties.Add(property.Key, newValue);
			}
		}

		return changedProperties;
	}

    private string GetCustomProp(string tableName, string propName, int objectID)
    {
        string columnName = string.Empty;
        switch (CustomPropertyObject)
        {
            case "IPAM_GroupAttrData":
                columnName = "GroupId";
                break;
            case "IPAM_NodeAttrData":
                columnName = "IPNodeId";
                break;
            default:
                throw new ArgumentException(nameof(CustomPropertyObject));
        }

        const string sql = "SELECT [{0}] FROM {1} WITH(NOLOCK) WHERE {2}={3}";

        using (SqlCommand cmd = SqlHelper.GetTextCommand(string.Format(sql, propName, tableName, columnName, objectID)))
        {
            object cp = SqlHelper.ExecuteScalar(cmd) ?? string.Empty;
            return cp.ToString();
        }
    }

    private object TryGetCustomPropertyValueWithType(string propertyName, string valueAsString)
    {
        Type cpType = CustomPropertyMgr.GetTypeForProp(CustomPropertyObject, propertyName);

        try
        {
            if ((cpType.Equals(typeof(Boolean))) && (string.IsNullOrEmpty(valueAsString)))
            {
                return false;
            }

            if (cpType.Equals(typeof(float)) || cpType.Equals(typeof(double)))
            {
                return Convert.ChangeType(valueAsString, cpType, CultureInfo.InvariantCulture);
            }

            if (cpType.Equals(typeof(DateTime)))
            {
                return ChangeDateToIsoFormat(valueAsString);
            }

            return Convert.ChangeType(valueAsString, cpType);
        }
        catch
        {
            return valueAsString;
        }
    }

    private string ChangeDateToIsoFormat(string dateToChange)
    {
        var res = DateTime.Parse(dateToChange, CultureInfo.InvariantCulture);
        return res.ToString("s");
    }
}
