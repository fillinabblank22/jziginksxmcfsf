﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.AccountRolesBox" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Src="~/Orion/IPAM/Controls/AccountRolesEditor.ascx" TagName="AccountRolesEditor" %>

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-expander.js" Orientation="jsPostInit" runat="server">
// [oh] defered call of collapse ... to take time to build grid and load data
$(document).ready( function(){ $('.exp-box').expander({delay: 100}); });
</IPAMmaster:JsBlock>

<div id="expBox" class="exp-box" header="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_393 %>" collapsed="true" runat="server">
    <div class="group-box white-bg">
        <IPAM:AccountRolesEditor id="AccountRolesEditor" runat="server" />
    </div>
</div>


