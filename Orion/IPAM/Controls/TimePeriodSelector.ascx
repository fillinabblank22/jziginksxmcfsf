﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodSelector.ascx.cs" Inherits="SolarWinds.IPAM.Web.Common.Controls.TimePeriodSelector" %>
<%@ Import Namespace="Resources" %>

<!-- Orion Core Version: '<%=this.OrionCore%>' -->

<script type="text/javascript" src="/Orion/js/jquery/ui.datepicker.js"></script>

<script type="text/javascript" src="/Orion/js/jquery/jquery.timePicker.js"></script>
<script type="text/javascript" src="/Orion/IPAM/res/js/sw-timeperiod-selector.js"></script>

<style type="text/css">
  .ui-datepicker-next {
    text-align: left;
  }
  
  .ui-datepicker {
    color: black;
   }
   
   .ui-datepicker .ui-datepicker-title {
     line-height: 1.6em;
   }
   
   #ui-datepicker-div 
   {
       padding-right: 6px;
       padding-left: 6px;
   }
   
   table.ui-datepicker-calendar td a.ui-state-default 
   {
       border: 0px;
       background: inherit;
       color: #1c94c4 !important;
   }
   table.ui-datepicker-calendar td a.ui-state-default:hover 
{
	color:#eb8f00 !important;
}
   

</style>

<script type="text/javascript">
$(document).ready(function() {
   var opt = {
RegionalSettings: <%= this.RegionalSettings %>,
<%= this.ControlsJSON %> 
   };
   var $PS_<%= this.ClientID %> = new $SW.PeriodSelector(opt);
});

function ValidateAbsoluteTimePeriod(src, arg){
    var chb = $('#<%=rbAbsolutePeriod.ClientID %>');
    if (!chb || chb.length < 1 || !chb[0].checked) { return arg.IsValid = true; }

    return arg.IsValid = $SW.AbsoluteTimePeriodValidator(src, arg,
        $('#<%=tbStartDate.ClientID %>').val() + ' ' + $('#<%=tbStartTime.ClientID %>').val(),
        $('#<%=tbEndDate.ClientID %>').val() + ' ' + $('#<%=tbEndTime.ClientID %>').val()
    );
};
</script>

<div class="period-selector" style="width: 440px">
    <div class="group" id="group-named">
        <span>
            <asp:RadioButton runat="server" ID="rbNamedPeriod" GroupName="PeriodType" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_136 %>" Checked="true" />
        </span>
        <div class="values">
            <asp:ListBox runat="server" ID="lbNamedPeriods" Rows="1" />
        </div>
    </div>
    <div class="group" id="group-relative">
        <span>
            <asp:RadioButton runat="server" ID="rbRelativePeriod" GroupName="PeriodType" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_137 %>" />
        </span>
        <div class="values">
            <%= IPAMWebContent.IPAMWEBDATA_TM0_141 %>
            <asp:TextBox runat="server" ID="tbRelativeTimeValue" Columns="5" MaxLength="9" />
            <asp:ListBox runat="server" ID="lbRelativeTimeTypes" Rows="1" />
        </div>
        <asp:CustomValidator runat="server" Display="None"
            ControlToValidate="tbRelativeTimeValue" ValidateEmptyText="true"
            ClientValidationFunction="$SW.RelativeTimePeriodValidator"
            ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_138 %>" />
    </div>
    <div class="group" id="group-absolute">
        <span>
            <asp:RadioButton runat="server" ID="rbAbsolutePeriod" GroupName="PeriodType" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_139 %>" />
        </span>
        <div class="values">
            <asp:TextBox ID="tbStartDate" runat="server" Width="75px" />  
            <asp:TextBox ID="tbStartTime" runat="server" Width="60px" />
            <%= IPAMWebContent.IPAMWEBDATA_TM0_142 %>
            <asp:TextBox ID="tbEndDate" runat="server" Width="75px" />
            <asp:TextBox ID="tbEndTime" runat="server" Width="60px" />
        </div>
        <asp:CustomValidator runat="server" Display="None"
            ClientValidationFunction="ValidateAbsoluteTimePeriod"
            ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_140 %>" />
        
    </div>
    <div class="errors-list">
        <asp:ValidationSummary runat="server" DisplayMode="BulletList"/>
    </div>
    <div class="sw-btn-bar" style="padding-left: 8px;">
        <orion:LocalizableButton runat="server" ID="btnSave" LocalizedText="Save" OnClick="OnSave" DisplayType="Primary" CausesValidation="true" />
        <orion:LocalizableButtonLink runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary" />
    </div>
</div>
