<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DhcpServerScanSettings.ascx.cs" Inherits="Orion_IPAM_Controls_DhcpServerScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/VrfGroupName.ascx" TagName="VrfGroupName" %>

<div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">

    <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label" style="width: 300px;"></td>
            <td style="width: 162px;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_202%>
            </div></td>
            <td>
                <ipam:TimeSpanEditor TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_274 %>" id="Duration" Value="00.01:00:00" MinValue="00:10:00" MaxValue="7.00:00:00" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3"><div class="sw-form-item sw-check-align">
                <asp:CheckBox ID="cbAutoAddScopes" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_203 %>"  runat="server"/>
            </div></td>
        </tr>
        <tr>
            <ipam:VrfGroupName ID="vrfGroupName" runat="server"/>
        </tr>
    </table></div>

</div>