<%@ Control Language="C#" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>

<script runat="server">
	private string _helpUrlFragment;
    private string _imageSource = "/Orion/images/Icon.Help.gif";

    public string HelpUrlFragment
    {
        get { return _helpUrlFragment; }
        set { _helpUrlFragment = value; }
    }

    public string ImageSource
    {
        get { return _imageSource; }
        set { _imageSource = value; }
    }
	
    protected string HelpURL
    {
        get
        {
            return HelpHelper.GetHelpUrl(this.HelpUrlFragment);
        }
    }
</script>

<span class="IconHelpButton">
<a class="HelpText" href="<%= this.HelpURL %>" style="background: transparent url(<%= this.ImageSource %>) scroll no-repeat left center; padding: 2px 0px 2px 20px;" target="_blank"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_37 %></a>
</span>
