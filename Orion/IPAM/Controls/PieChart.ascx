<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register TagPrefix="igChart" Namespace="Infragistics.WebUI.UltraWebChart" Assembly="Infragistics2.WebUI.UltraWebChart.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>
<%@ Assembly Name="Infragistics2.WebUI.Shared.v8.2, Version=8.2.20082.1000, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" %>

<igChart:UltraChart runat="server" ID="pieChart"  
    BackColor="white" Height="150px" Width="200px" ChartType="PieChart3D"
    Transform3D-XRotation="7"
    Transform3D-YRotation="0"
    Transform3D-ZRotation="-60"
    Transform3D-Perspective="10"
    Transform3D-Scale="100">
    
    <DeploymentScenario Scenario="Session" />
    <Tooltips Display="Never" />
    <Border Thickness="0" />
    <Axis>
        <PE ElementType="None" />
    </Axis>
    <TitleTop Visible="false" />
    <PieChart OthersCategoryPercent="0"  RadiusFactor="100" PieThickness="9" Labels-FormatString="<ITEM_LABEL>"></PieChart>
    <ColorModel ModelStyle="CustomLinear"></ColorModel>
</igChart:UltraChart>
