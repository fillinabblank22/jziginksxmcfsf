using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common.Helpers;

public partial class Orion_IPAM_Controls_NewDhcpScopeSettings : System.Web.UI.UserControl
{
    public bool EnableSubnetScanning
    {
        get { return cbEnableSubnetScanning.Checked; }
        set { cbEnableSubnetScanning.Checked = value; }
    }

    public int ScanInterval
    {
        get { return GetScanInterval(); }
        set { SetScanInterval(value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    void SetScanInterval(int minutes)
    {
        Duration.Value = TimeSpan.FromMinutes(minutes);
    }

    int GetScanInterval()
    {
        return Convert.ToInt32(Math.Round(Duration.Value.TotalMinutes));
    }
}
