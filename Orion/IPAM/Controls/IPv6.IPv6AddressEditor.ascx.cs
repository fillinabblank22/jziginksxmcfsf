﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using System.Text;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class IPv6AddressEditor : System.Web.UI.UserControl
    {
        #region Constants

        private const string CustomValidatorScriptTemplate =
            @"$SW.IPv6.SetIPv6AddressCustomValidator($('#{0}')[0], {{ type: 'IPv6Node', parent: '{1}', size: s, pid: '{2}' }});";

        #endregion // Constants

        #region Properties

        public string Address
        {
            get
            {
                string result = string.Empty;
                if (this.txtAddress != null)
                {
                    result = this.txtAddress.Text;
                }
                return result;
            }
            private set
            {
                if (this.txtAddress != null)
                {
                    this.txtAddress.Text = value;
                }
            }
        }

        private bool _readonly;
        public bool ReadOnly
        {
            get
            {
                return this._readonly;
            }
            set
            {
                if (this.txtAddress != null)
                {
                    this.txtAddress.ReadOnly = value;
                    this.txtAddress.ReadOnlyCss(value);
                }
                this._readonly = value;
            }
        }

        #endregion // Properties

        #region Constructors

        public IPv6AddressEditor()
        {
            this._readonly = false;
            this.Address = string.Empty;
        }

        #endregion // Constructors

        #region Event Handlers

        protected void OnAddressValidator(object source, ServerValidateEventArgs args)
        {
            // TODO: here put server based IPv6 validator
            args.IsValid = true;
        }

        #endregion // Event Handlers

        #region Methods

        public void Init(string value, GroupNode subnet)
        {
            string subnetAddress = string.Format("{0}/{1}", subnet.Address, subnet.CIDR);
            if (this.Page.IsPostBack == false)
            {
                this.Address = value;

                if (this.txtSubnetName != null)
                {
                    this.txtSubnetName.Text = subnet.FriendlyName;
                }
                if (this.txtSubnetAddress != null)
                {
                    this.txtSubnetAddress.Text = subnetAddress;
                }
            }

            bool validate = (this.ReadOnly == false);
            this.IPv6AddressRequireValidator.Enabled = validate;
            this.IPv6AddressCustomValidator.Enabled = validate;
            this.IPv6AddressCustomValidator.EnableClientScript = validate;
            if (validate)
            {
                InitCustomValidatorScript(subnetAddress, 128 - subnet.CIDR, subnet.GroupId);
            }
        }

        private void InitCustomValidatorScript(string subnet, int size, int pId)
        {
            if ((this.initAddressBox == null) ||
                (this.IPv6AddressCustomValidator == null))
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("$(document).ready( function(){");

            sb.AppendFormat("var s={0};", size);
            sb.AppendFormat(IPv6AddressEditor.CustomValidatorScriptTemplate,
                this.IPv6AddressCustomValidator.ClientID, subnet, pId);

            sb.Append("});");

            this.initAddressBox.AddScript(sb.ToString());
        }

        #endregion // Methods
    }
}
