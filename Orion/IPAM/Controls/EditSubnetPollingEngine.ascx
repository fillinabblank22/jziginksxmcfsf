﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.EditSubnetPollingEngine" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>
<orion:Include runat="server" File="OrionCore.js" />
<orion:Include runat="server" File="extjs/Ext.sw.grid.RadioSelectionModel.js" />

<IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-subnet-edit.js" Orientation="jsPostInit" runat="server">
$(document).ready(function() {
    Ext.namespace('SW');
    Ext.namespace('SW.Orion');
    $SW.SubnetPollingEngineEdit();
});
</IPAMmaster:JsBlock>
<IPAMmaster:CssBlock runat="server">
    [id$="managingDHCPServers"] td {
        border-bottom: 1px solid #ddd;
    }
    
    [id$="managingDHCPServers"] tr:first-child {
        font: normal 12px Arial,Helvetica,Sans-Serif;
        text-transform: uppercase;
        background-color: #dfdfde;
    }

    [id$="managingDHCPServers"] tr:first-child th {
        border: 1px solid #dfdfde;
        border-spacing:0;
        border-collapse:separate;
        padding:2px;
    }
    [id$="managingDHCPServers"] caption {
        font: normal 12px Arial,Helvetica,Sans-Serif;
    }
</IPAMmaster:CssBlock>
<asp:Table ID="managingDHCPServers" CellSpacing="0" Width="100%" runat="server"></asp:Table>
<div>
    <span id="pollEngineDescr" runat="server">
        <asp:Literal runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_49 %>" />
    </span>
    <orion:LocalizableButton runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_70 %>" ID="changePollEngineLink" OnClientClick="return false;" Style="margin-left: 10px;" />
</div>
<asp:HiddenField runat="server" ID="hfPollingEngineId" />
<input type="hidden" id="selPollingEngineId" value='<%= hfPollingEngineId.ClientID %>' />
<input type="hidden" id="objectEngineIDs" value='<%= string.Join(",", ObjectEngineIDs.Select(x=>x.ToString()).ToArray()) %>' />
<asp:HiddenField runat="server" ID="subnetId" />
<asp:HiddenField runat="server" ID="newEngineId" />


