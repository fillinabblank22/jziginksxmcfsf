﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Orion_IPAM_Controls_IPAMEventList : System.Web.UI.UserControl
{
	public object DataSource
	{
		get { return EventsGrid.DataSource; }
		set
		{
			EventsGrid.DataSource = value;
			EventsGrid.DataBind();
		}
	}
    public string GetEventType(string objecturl, string messageText)
    {
        if (objecturl == "")
        {
            return messageText;
        }
        else
        {
            return string.Format("<a href='{0}' style='color:#000000'>{1}</a>", objecturl, messageText);
        }
    }
}
