using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.Orion.Web;

public partial class Orion_IPAM_Controls_NodeSelector : System.Web.UI.UserControl
{
    private string _selectchangefn = null;

    public Boolean MultipleSelection
    {
        get { return GroupBySelector.MultipleSelection; }
        set { GroupBySelector.MultipleSelection = value; }
    }

    private string CleanPart(string value, string sep)
    {
        value = value.Trim();
        if (value.EndsWith(sep))
            value.Substring(0, value.Length - sep.Length);
        return value.Trim();
    }

    public string OnSelectChange
    {
        get { return _selectchangefn; }
        set { _selectchangefn = value; }
    }

    public List<int> SelectedNodes
    {
        get
        {
            List<int> ret = new List<int>();
            int id;
            foreach (string s in GroupBySelector.SelectionState.Split('&'))
            {
                string t = CleanPart(s, ":");
                if (t.Length == 0) continue;

                string[] parts = t.Split(':');
                if (parts.Length == 0) continue;

                t = parts[parts.Length - 1];
                if (t.StartsWith("g") && int.TryParse(t.Substring(1), out id))
                    ret.Add(id);
            }

            return ret;
        }

        set
        {
            StringBuilder sb = new StringBuilder();
            if (value != null)
                value.ForEach(delegate(int id) { sb.Append( "g" + id.ToString() + "&" ); });
            if (sb.Length > 0)
                sb.Length -= 1;
            GroupBySelector.SelectionState = sb.ToString();
        }
    }

    public string Selection
    {
        get 
        {
            StringBuilder sb = new StringBuilder();
            foreach( int id in SelectedNodes )
                sb.Append(id.ToString() + ",");
            if (sb.Length > 0)
                sb.Length -= 1;
            return sb.ToString();
        }

        set
        {
            int id;
            List<int> sel = new List<int>();
            foreach (string s in value.Split(',', ';', ' ', '\t'))
                if (int.TryParse(s.Trim(), out id))
                    sel.Add(id);
            SelectedNodes = sel;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            CertifyGroupByMethods();

            if (string.IsNullOrEmpty(GroupBySelector.SelectionState))
            {
                GroupBySelector.SelectionState = "vWindows:g";
            }

            IList<string> props = CustomPropertyManager.GetPropNamesForTable("Nodes", true);
            foreach (string prop in props)
                GroupBySelect.Items.Add(new ListItem(prop, "CustomProperties." + prop));
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (_selectchangefn != null)
            HookupClientSelectChange();
    }

    private void HookupClientSelectChange()
    {
        IWriteToCollector toCollector = Locator.FindCollector(Page);

        if (toCollector == null)
            return;

        int nsId = toCollector.ReserveNamespace(this);

        StringBuilder sb = new StringBuilder();

        sb.Append("$(document).ready(function(){var $nsId=" + nsId.ToString() + ";");
        sb.Append(Environment.NewLine);

        sb.Append("var tree = Ext.getCmp('GroupBySelector')," + Environment.NewLine);
        sb.Append("    sm = tree.getSelectionModel();" + Environment.NewLine);
        sb.Append("sm.on( 'selectionchange', ");
        sb.Append(_selectchangefn);
        sb.Append(");" + Environment.NewLine);
        sb.Append("});");
        toCollector.WriteToCollector("javascript", sb.ToString(), false);
    }

    protected void CertifyGroupByMethods()
    {
        if (SolarWinds.IPAM.Web.Common.Utility.RegistrySettings.IsCoreVersion(9, 5, 4, false) == false)
        {
            foreach( string txt in new string[]{ "Community", "RWCommunity" } ){
                ListItem li = GroupBySelect.Items.FindByValue(txt);
                if( li != null ) GroupBySelect.Items.Remove(li);
                Response.Write("<!-- BLARG -->");
            };
        }
    }
}
