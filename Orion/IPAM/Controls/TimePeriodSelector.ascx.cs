﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using System.Text;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Core.Common.Proxy.BusinessLayer;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class TimePeriodSelector : UserControl
    {
        #region Constants

        private static readonly string EventSave = "PostBackSave";

        #endregion // Constants

        #region Properties

        public bool IsGreatWall { get { return (OrionCore >= GreatWall); } }
        protected Version GreatWall = new Version(2012, 1);
        protected Version OrionCore { get { return GetCachedOrionCoreVersion(); } }
        
        public string OnClientSave { get; set; }

        public string OnClientCancel { get; set; }

        public event Action<object, TimePeriodValue> ServerSave
        {
            add
            {
                Events.AddHandler(EventSave, value);
            }
            remove
            {
                Events.RemoveHandler(EventSave, value);
            }
        }

        protected string RegionalSettings
        {
            get
            {
                return DatePickerRegionalSettings.GetDatePickerRegionalSettings();
            }
        }

        protected string ControlsJSON
        {
            get
            {
                return ControlsToJSON();
            }
        }

        protected string ValidatorsJSON
        {
            get
            {
                return ValidatorsToJSON();
            }
        }

        #endregion // Properties

        #region Events

        protected override void OnInit(EventArgs e)
        {
            // Register StyleSheets for period selector itself and it's components.
            ControlHelper.AddStylesheet(this.Page, "/Orion/js/jquery/ui.datepicker.css");
            ControlHelper.AddStylesheet(this.Page, "/Orion/js/jquery/timePicker.css");
            ControlHelper.AddStylesheet(this.Page, "/Orion/IPAM/res/css/sw-timeperiod-selector.css");

            if (!this.IsPostBack)
            {
                this.lbNamedPeriods.DataSource =
                    TimePeriodExtensions.LocalizedEnumeration<TimePeriodNames>();
                this.lbNamedPeriods.DataBind();

                this.lbRelativeTimeTypes.DataSource =
                    TimePeriodExtensions.LocalizedEnumeration<TimePeriodRelative>();
                this.lbRelativeTimeTypes.DataBind();

                if (string.IsNullOrEmpty(this.OnClientSave) == false)
                {
                    this.btnSave.OnClientClick = string.Format(
                        "$SW.TimePeriodSaveBtnClick(function(){{ {0} }});", this.OnClientSave);
                }
                if (string.IsNullOrEmpty(this.OnClientCancel) == false)
                {
                    this.btnCancel.Attributes.Add("onclick", this.OnClientCancel);
                }
            }

            SetNamedValue(TimePeriodNames.LastMonth);
            SetRelativeValue(1, TimePeriodRelative.Days);
            DateTime hour = DateTime.Today.AddHours(DateTime.Now.Hour);
            SetAbsoulteValue(hour, hour);

            base.OnLoad(e);
        }

        public virtual void OnSave(object sender, EventArgs evArgs)
        {
            Action<object, TimePeriodValue> evHandler =
                this.Events[EventSave] as Action<object, TimePeriodValue>;
            if (evHandler != null)
            {
                try
                {
                    TimePeriodValue value = ReadValue();
                    evHandler(this, value);
                } 
                catch{}
            }
        }

        #endregion // Events

        #region Methods

        public TimePeriodValue ReadValue()
        {
            try
            {
                if ((this.rbAbsolutePeriod != null) &&
                    (this.rbAbsolutePeriod.Checked == true))
                {
                    string beginString = this.tbStartDate.Text + " " + this.tbStartTime.Text;
                    string endString = this.tbEndDate.Text + " " + this.tbEndTime.Text;

                    DateTime beginDate;
                    DateTime endDate;
                    if (DateTime.TryParse(beginString, out beginDate) == true &&
                        DateTime.TryParse(endString, out endDate) == true)
                    {
                        return new TimePeriodValue(beginDate, endDate);
                    }
                    return null;
                }

                if ((this.rbRelativePeriod != null) &&
                    (this.rbRelativePeriod.Checked == true))
                {
                    string valueString = this.tbRelativeTimeValue.Text;
                    string typeString = this.lbRelativeTimeTypes.SelectedValue;

                    int value;
                    if (int.TryParse(valueString, out value) == false)
                    {
                        return null;
                    }
                    TimePeriodRelative type = typeString.ParseLocalizedEnumeration(
                        TimePeriodRelative.Minutes);

                    return new TimePeriodValue(value, type);
                }

                if ((this.rbNamedPeriod != null) &&
                    (this.rbNamedPeriod.Checked == true))
                {
                    string typeString = this.lbNamedPeriods.SelectedValue;

                    TimePeriodNames type = typeString.ParseLocalizedEnumeration(
                        TimePeriodNames.LastMonth);

                    return new TimePeriodValue(type);
                }
            }
            catch { }

            return null;
        }

        public void SetValue(TimePeriodValue value)
        {
            this.rbNamedPeriod.Checked = false;
            this.rbRelativePeriod.Checked = false;
            this.rbAbsolutePeriod.Checked = false;

            switch (value.Type)
            {
                case TimePeriodType.Named:
                    this.rbNamedPeriod.Checked = true;
                    SetNamedValue(value.NamedValue);
                    break;
                case TimePeriodType.Relative:
                    this.rbRelativePeriod.Checked = true;
                    SetRelativeValue(value.RelativeValue, value.RelativeType);
                    break;
                case TimePeriodType.Absolute:
                    this.rbAbsolutePeriod.Checked = true;
                    SetAbsoulteValue(value.BeginValue, value.EndValue);
                    break;
            }
        }

        private string ControlsToJSON()
        {
            bool first;
            StringBuilder sb = new StringBuilder();

            // radio buttons
            sb.Append("rbs: {");
            {
                first = true;
                JsonHelper.Out(sb, this.rbNamedPeriod.ID, this.rbNamedPeriod.ClientID, ref first);
                JsonHelper.Out(sb, this.rbRelativePeriod.ID, this.rbRelativePeriod.ClientID, ref first);
                JsonHelper.Out(sb, this.rbAbsolutePeriod.ID, this.rbAbsolutePeriod.ClientID, ref first);
            }
            sb.AppendLine("},");

            // named period controls
            sb.Append(this.rbNamedPeriod.ID + ": {");
            {
                first = true;
                JsonHelper.Out(sb, this.lbNamedPeriods.ID, this.lbNamedPeriods.ClientID, ref first);
            }
            sb.AppendLine("},");

            // relative period controls
            sb.Append(this.rbRelativePeriod.ID + ": {");
            {
                first = true;
                JsonHelper.Out(sb, this.tbRelativeTimeValue.ID, this.tbRelativeTimeValue.ClientID, ref first);
                JsonHelper.Out(sb, this.lbRelativeTimeTypes.ID, this.lbRelativeTimeTypes.ClientID, ref first);
            }
            sb.AppendLine("},");

            // absolute period controls
            sb.Append(this.rbAbsolutePeriod.ID + ": {");
            {
                first = true;
                JsonHelper.Out(sb, this.tbStartDate.ID, this.tbStartDate.ClientID, ref first);
                JsonHelper.Out(sb, this.tbStartTime.ID, this.tbStartTime.ClientID, ref first);
                JsonHelper.Out(sb, this.tbEndDate.ID, this.tbEndDate.ClientID, ref first);
                JsonHelper.Out(sb, this.tbEndTime.ID, this.tbEndTime.ClientID, ref first);
            }
            sb.Append("}");

            return sb.ToString();
        }

        private string ValidatorsToJSON()
        {
            return string.Empty;
        }

        private void SetNamedValue(TimePeriodNames name)
        {
            this.lbNamedPeriods.SelectedValue =
                LocalizationHelper.GetEnumDisplayString(
                typeof(TimePeriodNames), name);
        }

        private void SetRelativeValue(int time, TimePeriodRelative type)
        {
            this.tbRelativeTimeValue.Text = time.ToString();
            this.lbRelativeTimeTypes.SelectedValue =
                LocalizationHelper.GetEnumDisplayString(
                typeof(TimePeriodRelative), type);
        }

        private void SetAbsoulteValue(DateTime begin, DateTime end)
        {
            this.tbStartDate.Text = begin.ToShortDateString();
            this.tbStartTime.Text = begin.ToShortTimeString();
            this.tbEndDate.Text = end.ToShortDateString();
            this.tbEndTime.Text = end.ToShortTimeString();
        }

	    const string verCacheKey = "TimePerionSelector-InstalledOrionCoreVersion";
        private static Version GetCachedOrionCoreVersion()
        {
            Version version = HttpContext.Current.Application[verCacheKey] as Version;
            if (version == null)
            {
                using (var proxy = CoreBusinessLayerProxyCreatorFactory.GetCreator().Create(null))
                {
                    version = proxy.GetModuleVersion("Orion");
                }
                if (version == null)
                {
                    return new Version(1, 0);
                }
                HttpContext.Current.Application[verCacheKey] = version;
            }

            return version;
        }

        #endregion // Methods
    }
}