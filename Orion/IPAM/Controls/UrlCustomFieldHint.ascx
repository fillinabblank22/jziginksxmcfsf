﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UrlCustomFieldHint.ascx.cs" Inherits="Orion_IPAM_Controls_UrlCustomFieldHint" %>

<tr class="sw-form-helpTip">
    <asp:Repeater ID="PreColumnRepeater" runat="server">
        <ItemTemplate>
            <td class="sw-form-paddedBottom"></td>
        </ItemTemplate>
    </asp:Repeater>

    <td class="sw-form-paddedBottom">
        <div style="vertical-align:middle;">
            <img src="~/Orion/IPAM/res/images/sw/http.gif" style="vertical-align:middle" alt="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_190 %>" width="16" height="16" runat="server" />
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_189 %>
        </div>
    </td>

    <asp:Repeater ID="PostColumnRepeater" runat="server">
        <ItemTemplate>
            <td class="sw-form-paddedBottom"></td>
        </ItemTemplate>
    </asp:Repeater>


</tr>