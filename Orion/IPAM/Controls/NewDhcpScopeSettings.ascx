<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewDhcpScopeSettings.ascx.cs" Inherits="Orion_IPAM_Controls_NewDhcpScopeSettings" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>


<IPAMmaster:JsBlock ID="JsBlock2" Requires="ext-quicktips,sw-ux-ext.js" Orientation="jsInit" runat="server">
$(document).ready(function(){

  var syncscanarea = function(me){
    if( !me ) return;
    if( me.checked ) $('#subnetscanarea').show(250);
    else $('#subnetscanarea').hide(250);
  };

  syncscanarea( $SW.ns$($nsId, 'cbEnableSubnetScanning')[0] );

  $SW.TransformDropDown( $SW.nsGet($nsId, 'SubnetScanMultiplier'), 'SubnetScanMultiplier' );

  $SW.ns$( $nsId, 'cbEnableSubnetScanning' ).click(function(e){
    syncscanarea(e.target || e.srcElement);
  });
});

</IPAMmaster:JsBlock>

<div style="background: #e4f1f8; max-width: 876px; padding: 8px 6px;">

    <div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label" style="width: 238px;"></td>
            <td style="width: 162px;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td colspan="3"><div class="sw-form-item sw-check-align">
                <asp:CheckBox ID="cbEnableSubnetScanning" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_205 %>" Checked="true" runat="server"/>
            </div></td>
        </tr>
    </table></div>

    <div id="subnetscanarea"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label" style="width: 238px;"></td>
            <td style="width: 162px;"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_206 %>
            </div></td>
            <td>
                <ipam:TimeSpanEditor ValidIf="cbEnableSubnetScanning" ActiveWhenChecked="True" TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_AK1_271 %>" id="Duration" Value="01.00:00:00" MinValue="00:10:00" MaxValue="7.00:00:00" runat="server" />
            </td>
        </tr>
    </table></div>

</div>