﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.AccountRolesEditor" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<IPAMmaster:CssBlock Requires="ux-all.css,sw-account-roles.css" runat="server" />
<IPAMmaster:JsBlock Requires="ext,ext-ux,sw-account-roles.js" Orientation="jsPostInit" runat="server" />
<script type="text/javascript">
    $SW.IPAM.LearnMoreUrl = '<%=this.GetLearnMoreUrl("OrionIPAMPHRoles") %>';
</script>

<IPAMlayout:GridBox
    ID="AccountGrid"
    runat="server"
    autoHeight="False"
    height="250"
    width="550"
    borderVisible="True"
    UseLoadMask="True"
    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_394 %>"
    deferEmptyText="False">
    <ConfigOptions runat="server">
        <Options>
            <IPAMlayout:ConfigOption runat="server" Name="autoExpandColumn" Value="RoleColumn" />
            <IPAMlayout:ConfigOption Name="plugins" Value="[ new $SW.IPAM.AccountRoles.AccountGridPlugin({
    role: 'btnChangeRole',
    clr: 'btnClear',
    pu: 'btnPowerUser',
    op: 'btnOperator',
    ro: 'btnReadOnly',
    na: 'btnNoAccess'
}) ]" IsRaw="true" runat="server" />
        </Options>
    </ConfigOptions>
    <TopMenu>
        <IPAMui:ToolStripMenu id="btnChangeRole" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_395 %>" iconCls="mnu-ChangeRole" disabled="True" runat="server">
            <IPAMui:ToolStripMenuItem id="btnPowerUser" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_396 %>" iconCls="mnu-PowerUser" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.SetRoleValue('AccountGrid', 'PowerUser', function(d){ return d['AccountID']; }, function(d){ return d['GroupId']; }, $SW.IPAM.LearnMoreUrl)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripMenuItem id="btnOperator" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_397%>" iconCls="mnu-Operator" disabled="true" runat="server">
                <handler>$SW.IPAM.AccountRoles.SetRoleValue('AccountGrid', 'Operator', function(d){ return d['AccountID']; }, function(d){ return d['GroupId']; }, $SW.IPAM.LearnMoreUrl)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripMenuItem id="btnReadOnly" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_398 %>" iconCls="mnu-ReadOnly" disabled="true" runat="server">
                <handler>$SW.IPAM.AccountRoles.SetRoleValue('AccountGrid', 'ReadOnly', function(d){ return d['AccountID']; }, function(d){ return d['GroupId']; }, $SW.IPAM.LearnMoreUrl)</handler>
            </IPAMui:ToolStripMenuItem>
            <IPAMui:ToolStripMenuItem id="btnNoAccess" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_399 %>" iconCls="mnu-NoAccess" disabled="True" runat="server">
                <handler>$SW.IPAM.AccountRoles.SetRoleValue('AccountGrid', 'NoAccess', function(d){ return d['AccountID']; }, function(d){ return d['GroupId']; }, $SW.IPAM.LearnMoreUrl)</handler>
            </IPAMui:ToolStripMenuItem>
        </IPAMui:ToolStripMenu>

        <IPAMui:ToolStripSeparator runat="server" />

        <IPAMui:ToolStripButton id="btnClear" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_400 %>" iconCls="mnu-Clear" disabled="true" runat="server">
            <handler>$SW.IPAM.AccountRoles.ClearRoleValue('AccountGrid', function(d){ return d['AccountID']; }, function(d){ return d['GroupId']; })</handler>
        </IPAMui:ToolStripButton>

        <IPAMui:ToolStripSeparator runat="server" />

        <IPAMui:ToolStripButton text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_401 %>" RoleAclOneOf="IPAM.SiteAdmin" iconCls="mnu-settings" runat="server">
            <handler>function(that){ window.open('/Orion/Admin/Accounts/Accounts.aspx'); }</handler>
        </IPAMui:ToolStripButton>
    </TopMenu>
    <PageBar pageSize="20" displayInfo="True" />
    <Columns>
        <IPAMlayout:GridColumn title="&nbsp;" width="32" isFixed="true" isHideable="false" 
            dataIndex="AccountType" isSortable="true" runat="server" 
            renderer="$SW.IPAM.AccountRoles.TypeRenderer()"/>
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_402 %>" width="120"
            dataIndex="AccountID" isSortable="true" runat="server"
            renderer="$SW.IPAM.AccountRoles.NameRenderer()"/>
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent,IPAMWEBDATA_VB1_403 %>" id="RoleColumn"
            dataIndex="GroupId" isSortable="true" runat="server"
            renderer="$SW.IPAM.AccountRoles.RoleRenderer()"/>
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_404 %>" width="100"
            dataIndex="AccountID" isSortable="true" runat="server"
            renderer="$SW.IPAM.AccountRoles.InheritedRenderer()" />
    </Columns>
    <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="extdataprovider.ashx"
        proxyEntity="IPAM.AccountRoles" AmbientSort="AccountID"
        listeners="{ loadexception: $SW.ext.HttpProxyException, datachanged: $SW.IPAM.AccountRoles.StoreDataChanged }" runat="server">
        <Fields>
            <IPAMlayout:GridStoreField dataIndex="AccountID" isKey="True" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="GroupId" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="AccountType" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="IsSettingRole" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Admin" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="PowerUser" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="Operator" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="ReadOnly" runat="server" />
            <IPAMlayout:GridStoreField dataIndex="NoAccess"  runat="server" />
        </Fields>
    </Store>
</IPAMlayout:GridBox>

