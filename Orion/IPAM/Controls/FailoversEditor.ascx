﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.FailoversEditor"%>
<div>
    <asp:PlaceHolder ID="phEditorsHolder" runat="server" EnableViewState="True"/>
</div>
<div>
<br />
    <input type="button" value = "Add another"  id="addButton" class="sw-btn-primary sw-btn" runat="server" OnServerClick="OnAddFailoverClick" CausesValidation="False"/>
</div>
