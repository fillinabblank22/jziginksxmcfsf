<%@ Control Language="C#" ClassName="FilterSubnetsSql" %>

<script runat="server">
	public TextBox FilterTextBox { get { return Filter; } }
</script>

<p>
	<b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_660 %></b><br />
	<asp:TextBox runat="server" ID="Filter" Width="330"></asp:TextBox>
</p>

<p><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_661%></p>

<orion:CollapsePanel ID="CollapsePanel1" runat="server" Collapsed="true">
	<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_654%></b></TitleTemplate>
	
	<BlockTemplate>
		<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_662 %>

		<orion:CollapsePanel ID="CollapsePanel2" runat="server" Collapsed="true">
			<TitleTemplate><b><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_663 %></b></TitleTemplate>
			<BlockTemplate>
				<ul>
					<%foreach (string column in SolarWinds.IPAM.BusinessObjects.GroupNode.SQLFILTER_ALLSUBNETPROPERTIES) {%>
						<li>Subnet.<%=column%></li>
					<%}%>
				</ul>
			</BlockTemplate>
		</orion:CollapsePanel>

	</BlockTemplate>
</orion:CollapsePanel>
