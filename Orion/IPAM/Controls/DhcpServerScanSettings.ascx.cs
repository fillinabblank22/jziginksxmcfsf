using System;

public partial class Orion_IPAM_Controls_DhcpServerScanSettings : System.Web.UI.UserControl
{
    public int ParentId { get; set; }

    public int ClusterId { get; set; }

    public bool AutoAddNewScopes
    {
        get { return cbAutoAddScopes.Checked; }
        set { cbAutoAddScopes.Checked = value; }
    }

    public int ScanInterval
    {
        get { return GetScanInterval(); }
        set { SetScanInterval(value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        vrfGroupName.GroupId = ClusterId;
    }

    void SetScanInterval(int minutes)
    {
        Duration.Value = TimeSpan.FromMinutes(minutes);
    }

    int GetScanInterval()
    {
        return Convert.ToInt32(Math.Round(Duration.Value.TotalMinutes));
    }
}
