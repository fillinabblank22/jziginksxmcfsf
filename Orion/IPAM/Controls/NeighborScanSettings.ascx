﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.NeighborScanSettings" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>

<script type="text/javascript">
    $SW.NSS = $NSS;
    $SW.NSS.ControlDivId = '#NeighborSettings';
    $SW.NSS.RadioId = 'cbDisableNeighborScanning';
    $SW.NSS.IPAddressId = 'txtIPAddress';
    $SW.NSS.TestResult = 0;
    $SW.NSS.SkipValFn = function(){ <%=ClientSkipValidation%> };
</script>

<IPAMmaster:JsBlock ID="JsBlock" Orientation="jsPostInit" runat="server"
    Requires="ext,ext-quicktips,sw-helpserver,sw-subnet-edit.js,sw-dialog.js,sw-ux-wait.js,sw-neighbor-settings.js">
$(document).ready( function(){ try {
    $SW.NSS.nsId = $nsId;
    $SW.NSS.Init();
}catch(err){} } );
</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock runat="server">
.sw-form-subheader a, .sw-form-subheader a:active, .sw-form-subheader a:visited {
    color:gray;
    font-weight: normal;
}
.sw-test {
    width: 200px;
    font-size: 11px;
    visibility: visible;
    margin: 0px;
    padding: 3px 5px 3px 24px;
    position: static;
}
.sw-test a {
    color: #707070;
    font-size: 9px;
}
.sw-test-success {
    color: #00A000;
    font-weight: bold;
    background: url("/Orion/IPAM/res/images/sw/icon.check.gif") no-repeat scroll 2px 2px #DAF7CC;
}
.sw-test-warning {
    color: block;
    font-weight: normal;
    background: url("/Orion/IPAM/res/images/sw/icon.warning.gif") no-repeat scroll 2px 2px #FDF6C0;
}
.sw-test-failed {
    color: #ce0000;
    font-weight: bold;
    background: url("/Orion/IPAM/res/images/sw/icon.failed.gif") no-repeat scroll 2px 2px #FACECE;
}

</IPAMmaster:CssBlock>

<div><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <div class="sw-form-item">
                <asp:CheckBox ID="cbDisableNeighborScanning" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_79 %>" runat="server" />
            </div>
        </td>
    </tr>
</table></div>

<div><!-- ie6 --></div>

<div id="NeighborSettings">

<div id="NeighborData"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td class="sw-form-col-control"></td>
        <td class="sw-form-col-comment"></td>
    </tr>
    <tr>
        <td>
            <div class="sw-field-label">
                <%= IPAMWebContent.IPAMWEBDATA_TM0_80 %>
            </div>
        </td>
        <td>
            <div class="sw-form-item">
                <asp:TextBox ID="txtIPAddress" CssClass="x-form-text x-form-field" runat="server" />
            </div>
        </td>
        <td><asp:CustomValidator ID="IPAddressRequire" runat="server" Display="Dynamic"
                  ControlToValidate="txtIPAddress"
                  OnServerValidate="OnIsIPAddressRequire"
                  ClientValidationFunction="$SW.NSS.OnIsIPAddressRequire"
                  ValidateEmptyText="true"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_81 %>" />
            <asp:CustomValidator ID="IPAddressIPv4" runat="server" Display="Dynamic"
                  ControlToValidate="txtIPAddress"
                  ClientValidationFunction="$SW.NSS.OnIsIPAddressIPv4"
                  ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_TM0_82 %>" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="sw-form-item sw-form-clue">
                <%= IPAMWebContent.IPAMWEBDATA_VB1_184%>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="sw-field-label">
                <%= IPAMWebContent.IPAMWEBDATA_TM0_36 %>
            </div>
        </td>
        <td>
            <ipam:TimeSpanEditor Width="168" ID="NeighborScanInterval" ValidIf="cbDisableNeighborScanning"
                ActiveWhenChecked="False" TextName="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_19 %>" runat="server" MinValue="00:05:00"
                MaxValue="7.00:00:00" />
        </td>
    </tr>
</table></div>

<div><!-- ie6 --></div>

<div id="testButton"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
    <tr class="sw-form-cols-normal">
        <td class="sw-form-col-label"></td>
        <td width="75px">
            <orion:LocalizableButton runat="server" ID="TestNeighbor" LocalizedText="Test" DisplayType="Small" CausesValidation="true" />
        </td>
        <td>
            <div id="testpass" class="sw-test sw-test-success" style="display: none;">
                <%= IPAMWebContent.IPAMWEBDATA_TM0_86 %>
            </div>
            <div id="testnoarp" class="sw-test sw-test-warning" style="display: none;">
                <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_87,
                                  "<a href=\"#\" onclick=\"return $SW.NSS.Help('OrionIPAMPHArpTable.htm');\">",
                                  "</a>") %>
            </div>
            <div id="testfail" class="sw-test sw-test-failed" style="display: none;">
                <%= String.Format(IPAMWebContent.IPAMWEBDATA_TM0_88, 
                                  "<a href=\"#\" onclick=\"return $SW.NSS.Help('OrionIPAMPHTroubleshootConnection.htm');\">", "</a><br />", 
                                  "<a href=\"/Orion/IPAM/Admin/Admin.SnmpCred.List.aspx\" target=\"_blank\">", "</a>") %>
            </div>
        </td>
    <tr>
</table></div>

</div>
