﻿using System;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.Web.Master;
using System.IO;

public partial class Orion_IPAM_Controls_VrfGroupName : UserControl
{
    #region Variables

    private VrfGroupExistsHelper _vrfHelper;

    #endregion

    #region Properties

    public int GroupId { get; set; }
    public bool IsEnableClusterName { get; set; }

    public bool IsNewGroupName { get; set; }
	
	public bool Enabled
	{
		get { return true; }
		set {
			ScriptManager.RegisterStartupScript(Page, typeof(Page), "DisableGroupSelect",
			"var val=('"+value.ToString()+@"'=='True');
			
				var real = document.getElementById('real');
				var fake = document.getElementById('fake');
				var newgroup = document.getElementById('newGroupRow');
				
				if(val)
				{
					real.style.visibility = 'visible';
					newgroup.style.visibility = 'visible';
					fake.style.visibility = 'collapse';
				}
				else
				{
					real.style.visibility = 'collapse';
					newgroup.style.visibility = 'collapse';
					fake.style.visibility = 'visible';					
				}",
			true); 
			}
	}

    #endregion

    #region Controls

    protected global::System.Web.UI.WebControls.DropDownList DdlVrfGroupDownList;
    protected global::System.Web.UI.WebControls.TextBox NewVrfGroupName;


    protected void BindToControls()
    {
        BindCtrl<DropDownList>("ddlVrfGroupName", out DdlVrfGroupDownList);
        BindCtrl<TextBox>("newVrfGroupName", out NewVrfGroupName);
    }

    private void BindCtrl<T>(string id, out T result) where T : Control
    {
        result = default(T);
        result = Locator.FindControlRecursive(this, id, result);
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        BindToControls();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        _vrfHelper = new VrfGroupExistsHelper();
        if (!IsPostBack)
        {
            ddlVrfGroupName.Enabled = IsEnableClusterName;
            using (var proxy = SwisConnector.GetProxy())
            {
                if (IsEnableClusterName)
                {
                    var clusters = proxy.AppProxy.GroupNode.GetAllClusters();
                    foreach (var groupNode in clusters)
                    {
                        ddlVrfGroupName.Items.Add(new ListItem(groupNode.FriendlyName, groupNode.GroupId.ToString()));
                    }

                    if (ddlVrfGroupName.Items.Count > 0)
                    {
                        ddlVrfGroupName.SelectedValue = ddlVrfGroupName.Items.FindByValue("0").Value;
                    }
                }
                else
                {
                    newVrfGroupName.Visible = false;
                    var groupNode = proxy.AppProxy.GroupNode.Get(GroupId);
                    if (groupNode != null)
                    {
                        ddlVrfGroupName.Items.Add(new ListItem(groupNode.FriendlyName, groupNode.GroupId.ToString()));
                        ddlVrfGroupName.SelectedValue = groupNode.GroupId.ToString();
                    }
                }
            }
        }
    }

    protected void OnIsNewVrfGroupNameRequire(object source, ServerValidateEventArgs args)
    {
        args.IsValid = _vrfHelper.IsNewVrfGroupNameRequire(this.ddlVrfGroupName.SelectedValue, args.Value);
    }

    protected void OnNewVrfGroupNameExists(object source, ServerValidateEventArgs args)
    {
        args.IsValid = _vrfHelper.CheckVrfGroupNameNotExists(this.ddlVrfGroupName.SelectedValue, args.Value, true);
    }
}