﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SolarWinds.IPAM.Web.Common.Controls.DhcpIPReservation" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Import Namespace="SolarWinds.IPAM.BusinessObjects" %>

<IPAMmaster:CssBlock runat="server">
.dhcpReservations { padding:5px; margin-bottom: 4px; }
.supportedTypes { margin:5px 0px; }
.dhcp-res-fields input { margin: 1px; }
.dhcp-res-fields label { vertical-align:top; }
.dhcp-res-fields fieldset { border: 1px solid rgb(181,184,200); }
.dhcp-res-fields fieldset legend { margin-left: 5px; }
.dhcp-res-fields table { width: 100%; vertical-align:center; padding:5px; }
.dhcpReservations .x-item-disabled input { padding 0px; !important; }
</IPAMmaster:CssBlock>

<style type="text/css">
    .x-form-field-wrap {
        display: inline-block !important;
        *display: inline !important;
    }
    
</style>

<script type="text/javascript">

    function GetIscServerType() {
        return "<%= Convert.ToInt32(DhcpServerType.ISC) %>";
    }

</script>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-dhcp-manage.js" Orientation="jsPostInit" runat="server">
$(document).ready(function(){
    var id = 'ddlDHCPServers';
    var ddlSrv = $('#'+$SW.nsGet($nsId, id ))[0];
    var comboSrv = new Ext.form.ComboBox({
        id: id,
        transform: ddlSrv,
        typeAhead: true,
        triggerAction: 'all',
        forceSelection: true,
        width: 175, grow: false,
        disabled: ddlSrv.disabled || false
    });
    comboSrv.resizeEl.setWidth(175);

    var toggle = function(c, d, delay){
        var chb = c[0] || {checked: false};
        return function(el,r){
            if(chb.checked) d.show(delay); else d.hide(delay);
        };
    };
    
    var rblReservation0 = $('#'+$SW.nsGet($nsId, 'rblReservationType_0' ));
    var rblReservation1 = $('#'+$SW.nsGet($nsId, 'rblReservationType_1' ));
    var rblReservation2 = $('#'+$SW.nsGet($nsId, 'rblReservationType_2' ));

    enableFileName = function(isEnable) {
        return (function(e) {
            var iComboValue = comboSrv.getValue();
            var serverType = iComboValue.substr(iComboValue.indexOf("-")+1);
            var item = '#'+$SW.nsGet($nsId, 'txtBootFileName');

            if(serverType == GetIscServerType() && isEnable == true){
                $(item).prop("readonly", false);
                $(item).removeClass('x-item-disabled');
            }
            else{
                $(item).val('');
                $(item).prop("readonly", true);
                $(item).addClass('x-item-disabled');
            }
        });
    };

    var chb = $('#'+$SW.nsGet($nsId, 'chbHeader' ));
    var div = $('#'+$SW.nsGet($nsId, 'content' ));
    chb.bind('click', toggle(chb, div, 250));
    toggle(chb, div, 0)();
    
    rblReservation0.bind('change', enableFileName(false) );
    rblReservation1.bind('change', enableFileName(true) );
    rblReservation2.bind('change', enableFileName(false) );
});
</IPAMmaster:JsBlock>

<div id="<%=this.ID%>" class="dhcpReservations" style="<%=this.CssStyle%>">
    <div class="dhcp-res-fields">
        <asp:CheckBox ID="chbHeader" runat="server"/>
    </div>
    <div id="content" runat="server">
        <asp:Panel ID="pnlSupportedTypes" GroupingText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_365 %>" runat="server" class="dhcp-res-fields supportedTypes">
            <asp:RadioButtonList ID="rblReservationType" runat="server" RepeatDirection="Horizontal"  >
                <asp:ListItem Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_361 %>" Selected="True" />
                <asp:ListItem Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_362 %>" />    
                <asp:ListItem Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_363 %>" />
            </asp:RadioButtonList>
        </asp:Panel>
        <table id="dhcpreservationtable">
            <tr id="dhcpserver">
                <td style="display: inline-block">
                    <span><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_364 %></span>
                </td>
                <td>
                    <asp:DropDownList ID="ddlDHCPServers" runat="server"/>
                </td>
            </tr>
            <tr id="fileName">
                <td style="display: inline-block">
                    <span><%= Resources.IPAMWebContent.IPAMWEBDATA_SE_19 %></span>
                </td>
                <td>
                    <asp:TextBox ID="txtBootFileName" ReadOnly="false" CssClass="x-form-text x-form-field x-item-disabled" Width="170px" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</div>
