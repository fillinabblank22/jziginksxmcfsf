﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAMEventList.ascx.cs" Inherits="Orion_IPAM_Controls_IPAMEventList" %>

<asp:Repeater ID="EventsGrid" runat="server">
    <HeaderTemplate>
        <table width="100%" cellspacing="0" cellpadding="0" class="Events">
    </HeaderTemplate>

    <ItemTemplate>
        <tr>
            <td width="20" class="Event<%#DataBinder.Eval(Container.DataItem, "EventType") %>">&nbsp;</td>
            <td class="Event<%#DataBinder.Eval(Container.DataItem, "EventType") %>" width="106">
                <%#DataBinder.Eval(Container.DataItem, "EventTime", "{0:g}") %>
            </td>
            <td class="Event<%#DataBinder.Eval(Container.DataItem, "EventType") %>">
                <%#DataBinder.Eval(Container.DataItem, "UserName") %>
            </td>
            <td class="Event<%#DataBinder.Eval(Container.DataItem, "EventType") %>" width="19">
                <img src="/NetPerfMon/images/Event-<%#DataBinder.Eval(Container.DataItem, "EventType") %>.gif" border="0">
            </td>
            <td class="Event<%#DataBinder.Eval(Container.DataItem, "EventType") %>">
                 <%# GetEventType(Convert.ToString(DataBinder.Eval(Container.DataItem, "ObjectURL")), Convert.ToString(DataBinder.Eval(Container.DataItem, "Message"))) %>
            </td>
        </tr>
    </ItemTemplate>

    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
