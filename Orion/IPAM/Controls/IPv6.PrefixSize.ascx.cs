﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Resources;

namespace SolarWinds.IPAM.Web.Common.Controls
{
    public partial class IPv6PrefixSize : System.Web.UI.UserControl
    {
        #region Properties

        public int MinValue { get; private set; }
        public int MaxValue { get; private set; }
        public int PrefixSizeValue
        {
            get
            {
                if (this.txtSize == null)
                {
                    return 0;
                }

                int prefixSize = 0;
                int.TryParse(this.txtSize.Text, out prefixSize);
                return prefixSize;
            }
            private set
            {
                if (this.txtSize == null)
                {
                    return;
                }
                this.txtSize.Text = value.ToString();
            }
        }
        public string FnSupplyValue { get; set; }

        private bool _readonly;
        public bool ReadOnly
        {
            get
            {
                return this._readonly;
            }
            set
            {
                if (this.txtSize != null)
                {
                    this.txtSize.ReadOnly = value;
                    this.txtSize.Enabled = !value;
                }
                this._readonly = value;
            }
        }

        #endregion // Properties

        #region Constructors

        public IPv6PrefixSize()
        {
            this._readonly = false;
            this.MinValue = 1;
            this.MaxValue = 128;
            this.PrefixSizeValue = 1;
            this.FnSupplyValue = null;
        }

        #endregion // Constructors

        #region Methods

        public void Init(int minValue, int maxValue, int value, string fnSupplyValue)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
            this.PrefixSizeValue = value;
            this.FnSupplyValue = fnSupplyValue;

            this.initUpDownBox.AddScript(CreateInitScript());

            if (this.MaxPrefixSize != null)
            {
                this.MaxPrefixSize.Text = string.Format(IPAMWebContent.IPAMWEBCODE_VB1_5, this.MaxValue);
            }

            var page = this.Page as CommonPageServices;
            if ((this.LearnMoreLink != null) && (page != null))
            {
                this.LearnMoreLink.NavigateUrl = page.GetHelpUrl("OrionIPAMPHOrganizingIPv6Address");
            }
        }

        private string CreateInitScript()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("$(document).ready( function(){");

            sb.AppendFormat("  $SW.IPv6.PrefixSizeInit( $('#{0}'),", txtSize.ClientID);
            sb.Append("{subnets: $('#subnets-count'), ");
            sb.AppendFormat("minValue: {0}, maxValue: {1}", this.MinValue, this.MaxValue);
            if (!string.IsNullOrEmpty(this.FnSupplyValue))
            {
                sb.AppendFormat(", fn: {0}", this.FnSupplyValue);
            }
            sb.AppendLine("});");

            sb.Append("});");
            return sb.ToString();
        }

        #endregion // Methods
    }
}
