<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" Inherits="SolarWinds.IPAM.WebSite.DhcpServerEdit"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_179%>" CodeFile="Dhcp.Server.Edit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DHCPCredentialsSelector.ascx" TagName="DHCPCredentialsSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DhcpServerScanSettings.ascx" TagName="DhcpServerScanSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/NewDhcpScopeSettings.ascx" TagName="NewDhcpScopeSettings" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" TagName="CustomPropertyEdit" %>
<%@ Register TagPrefix="ipam" TagName="FailoversEditor" Src="~/Orion/IPAM/Controls/FailoversEditor.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){ $('.expander').expander(); });
</IPAMmaster:JsBlock>
<script type="text/javascript">

function ScanInform(infoString) {
        Ext.Msg.show({
            title: 'Scan information',
            msg: infoString,
            buttons: Ext.Msg.OK,
            icon: Ext.MessageBox.INFO,
            fn: function () {
                window.parent.location.reload();
            }
        });
        return false;
    }
</script>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
p { margin-top: 0; margin-bottom: 10px; }
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
div.sw-form-heading { font-size: small; margin-top: 16px; margin-bottom: 4px; }
div.sw-form-heading h2 { font-size: small; margin: 0; }
.sw-heading { width: 130px; }
.warningbox-inside .inner a { text-decoration: underline; }
.warningbox-inside { border: 1px solid #d1d0c9; max-width: 870px; background: #f7f596 url(/orion/ipam/res/images/sw/bg.warning.float.gif) repeat-x scroll left bottom; }
.warningbox-inside .inner { background:transparent url(/orion/ipam/res/images/sw/icon.notification.32.gif) no-repeat scroll left top; }
.sw-form-cols-normal td.sw-form-col-label { width: 200px; }
.sw-form-item, .sw-field-label { word-wrap: break-word; }
.menuItem { float:left; padding:5px; }
.menuItem input { width:30px; height:30px;}
.container{float:left; top: 0px; margin:0px; padding: 2px 2px 2px 2px; border-width: 2px; border-bottom-width:2px; border-bottom-color:#dfe0e1; border-bottom-style:solid; width: 100%; }
.inlined{ display: inline; margin-right: 40px; width: 50px; }
.row {height:35px;}
div.sw-check-align input { vertical-align: middle; }
div.sw-check-align { padding: 6px 0px 6px 0px; }
.formStyle{width: 650px !important}
.warningInformation {
    margin: 5px 10px 5px 0px;
    padding: 5px 0 5px 10px;
    background-color:#FFF7CD;
}
</IPAMmaster:CssBlock>

<%if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) {%>
		<div id="isDemoMode" style="display:none;"></div>
	<%}%>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox" class="formStyle">

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_180 %></div>

    <div class="group-box white-bg"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_181 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtServerType" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_182 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
            <asp:TextBox ID="txtIpAddress" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <div class="sw-field-label">
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_848 %>
                </div>
            </td>
            <td>
                <div class="sw-form-item">
                    <asp:Image ID="engineStatusImage" runat="server" style="vertical-align: middle" />
                    <asp:Label ID="txtPollingEngine" CssClass="x-form-field" style="vertical-align: middle" runat="server"></asp:Label>
                </div>
            </td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_183 %>
            </div></td>
            <td><div class="sw-form-item">
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan="2"><div class="sw-form-item sw-form-clue">
              <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_184 %>
            </div></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_185 %>
            </div></td>
            <td><div class="sw-form-item">
              <asp:TextBox ID="txtVLAN" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_186 %>
            </div></td>
            <td><div class="sw-form-item">
              <asp:TextBox ID="txtLocation" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        </table></div>
        
    <div><!-- ie6 --></div>
       
<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
    <div class="group-box white-bg" style="margin-bottom: 0px">
        <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
    </div>
</div>

    <div><!-- ie6 --></div>
     
<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_191 %>" collapsed="true">
    
    <div class="group-box white-bg"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_192%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtStartField" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_193%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtDiscovers" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_194%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtOffers" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_195%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtRequests" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_196%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtAcks" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_197%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtNacks" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_198%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtDeclines" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
        <tr>
            <td><div class="sw-field-label"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_199%></div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtReleases" CssClass="x-form-text x-form-field" Text="" runat="server" ReadOnly="true" />
            </div></td>
            <td></td>
        </tr>
    </table></div>

</div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_386 %>">

    <div class="warningbox sw-credential-warning" style="display: none;">
        <div class="inner">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_200%>
        </div>
    </div>
    <div><!-- ie6 --></div>
    <div class="group-box blue-bg">
        <ipam:DHCPCredentialsSelector ValidationGroup="NewCred" id="CredSelector" runat="server" />
    </div>
</div>

<div><!-- ie6 --></div> 
<asp:Panel ID="failoverpanel" runat="server" visible="false">   
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_864 %>" id="failoverExpander" >
        <div><!-- ie6 --></div>
        <div class="group-box blue-bg">
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" EnableViewState="True">    
                <ContentTemplate>
                    <div runat="server" id="failoverNotScannedYetText" class="warningInformation">
                        <img src="/Orion/IPAM/res/images/sw/icon.lightbulb.small.gif" />
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_WARNING_FAILOVER_NOT_SCANNED_YET %>                        
                    </div>                                        
                    <ipam:FailoversEditor ID="editFailover" runat="server" EnableViewState="True"/>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Panel>

<div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_166 %>">
    
    <div class="group-box blue-bg">
        <ipam:DhcpServerScanSettings ID="DhcpServerScanSettings" runat="server" />
    </div>

</div>
    
<div><!-- ie6 --></div>
     
<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_169 %>">

    <div class="group-box blue-bg">
        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_204 %>
        <ipam:NewDhcpScopeSettings ID="NewDhcpScopeSettings" runat="server" />
    </div>     

</div>

<div><!-- ie6 --></div>

<asp:ValidationSummary ID="ValidationSummary1" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 155px;">
                &nbsp;
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        OnClientClick="if(CheckDemoMode()) {demoAction('IPAM_DCHP_ServerEdit', null); return false; }" LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButtonLink ID="LocalizableButtonLink1" NavigateUrl="/Orion/IPAM/Dhcp.Management.aspx"
                        runat="server" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>
</div>


</asp:Content>

