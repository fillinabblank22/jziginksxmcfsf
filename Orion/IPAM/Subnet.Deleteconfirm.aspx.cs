using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class SubnetDeleteconfirm : CommonPageServices
	{
        private PageSubnetDelete del;
        public SubnetDeleteconfirm() { del = new PageSubnetDelete(this); }

        public GridBox Grid
        {
            get { return grid; }
        }

        protected override List<string> GetChromeControlIDs()
        {
            List<string> ret = base.GetChromeControlIDs();
            ret.Add( "ButtonBox" );
            return ret;
        }

		//#region Controls
		//protected global::SolarWinds.IPAM.WebSite.GridPreloader GridPreloader1;
		//protected global::SolarWinds.IPAM.Web.Layout.GridBox grid;
		//#endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            AccessCheck.GroupIds.AddRange(del.GroupIDs);
            GridPreloader1.WhereClause = del.WhereClause;
        }

	    protected void btnDelete_Click(object sender, EventArgs e)
        {
            del.Delete(true);
        }
        
        protected void MsgDelete(object sender, EventArgs e)
        {
            del.Delete(true);
        }
    }
}
