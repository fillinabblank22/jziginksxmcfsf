﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using System.Web.UI.WebControls;
using System.Linq;

public partial class IPv6AddressMultiEdit : CommonPageServices
{
    private const string StatusHelpFragment = "OrionIPAMPHStatus";
    private const string TypeHelpFragment = "OrionIPAMPHAddressTypes";

    private const string AttributeCheckBoxId = "chbAttribute";
    private const string PropertyTextId = "PropertyText";
    private const string EditingAvailableIPsParam = "AvailableIPs";

    #region Constructors

    private PageIPv6AddressMultiEditor Editor { get; set; }
    public IPv6AddressMultiEdit()
    {
        this.Editor = new PageIPv6AddressMultiEditor(this);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.Editor.LoadPage();
        this.AccessCheck.GroupIds.Add(this.Editor.SubnetId);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            List<IPNode> addresses = this.Editor.RetrieveAddressObjects();
            InitControls(addresses);
        }
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        this.ddlStatus.Enabled = this.chbStatus.Checked;
        this.ddlType.Enabled = this.chbType.Checked;
        this.ddlScanning.Enabled = this.chbScanning.Checked;
    }

    protected void OnCheckBoxesServerValidate(object source, ServerValidateEventArgs e)
    {
        List<CheckBox> checkBoxes = new List<CheckBox>();

        checkBoxes.Add(this.chbStatus);
        checkBoxes.Add(this.chbType);
        checkBoxes.Add(this.chbScanning);

        if (IsAvailable() == false)
        {
            // System Info
            checkBoxes.Add(this.chbVendor);
            checkBoxes.Add(this.chbMachineType);
            checkBoxes.Add(this.chbSystemDescription);
            checkBoxes.Add(this.chbSystemName);
            checkBoxes.Add(this.chbSystemContact);
            checkBoxes.Add(this.chbSystemLocation);
        }

        e.IsValid = checkBoxes.Exists(o => o.Checked) || CustomPropertyRepeater.CheckIfCpChanged();
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(List<IPNode> addresses)
    {
        #region Help Links

        if (this.StatusHelpLink != null)
        {
            this.StatusHelpLink.NavigateUrl = GetHelpUrl(StatusHelpFragment);
        }
        if (this.TypeHelpLink != null)
        {
            this.TypeHelpLink.NavigateUrl = GetHelpUrl(TypeHelpFragment);
        }

        var ipNodes = (addresses ?? new List<IPNode>());
        #endregion // Help Links

        #region IPv6 Address List

        if (this.IPv6AddressList != null)
        {
            foreach (IPNode ipNode in ipNodes)
            {
                ListItem li = new ListItem();
                li.Text = ipNode.IPAddress;
                li.Attributes.Add("class", ListItemCssClass(ipNode));
                this.IPv6AddressList.Items.Add(li);
            }

            if (this.txtAddressCountLabel != null)
            {
                this.txtAddressCountLabel.Text = string.Format(
                    Resources.IPAMWebContent.IPAMWEBCODE_AK1_3, this.IPv6AddressList.Items.Count);
            }
        }

        #endregion // IPv6 Address List

        #region Drop Down Lists

        IPNodeStatus selectedStatus = addresses.FirstOrDefault().Status;

        if (this.ddlStatus != null)
        {
            this.Editor.SetStatusList(this.ddlStatus, selectedStatus);
        }
        if (this.ddlType != null)
        {
            this.Editor.SetTypeList(this.ddlType, IPAllocPolicy.Static);
        }

        #endregion // Drop Down Lists

        SetJSCheckBoxControl(this.chbComment, this.txtComment);

        CustomPropertyRepeater.RetrieveCustomProperties(new IPNode(), ipNodes.Select(g => g.IpNodeId.Value).ToList());

        #region System Info

        SetJSCheckBoxControl(this.chbMachineType, this.txtMachineType);
        SetJSCheckBoxControl(this.chbVendor, this.txtVendor);
        SetJSCheckBoxControl(this.chbSystemName, this.txtSystemName);
        SetJSCheckBoxControl(this.chbSystemDescription, this.txtSystemDescription);
        SetJSCheckBoxControl(this.chbSystemContact, this.txtSystemContact);
        SetJSCheckBoxControl(this.chbSystemLocation, this.txtSystemLocation);

        #endregion // System Info
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        Page.Validate();
        if (Page.IsValid)
        {
            IPNode address = CreateTemplateObject();
            if ((address != null) && address.IsDirty)
            {
                this.Editor.Store(address, true);
            }
        }
    }
    private IPNode CreateTemplateObject()
    {
        IPNode address = new IPNode();
        address.AcceptChanges();
        address.TrackChanges = true;

        #region Drop Down Lists

        if ((this.ddlStatus != null) &&
            (this.chbStatus != null) && this.chbStatus.Checked)
        {
            string status = this.ddlStatus.SelectedValue;
            address.Status = (IPNodeStatus)Enum.Parse(typeof(IPNodeStatus), status);
        }

        if ((this.ddlType != null) &&
            (this.chbType != null) && this.chbType.Checked)
        {
            string type = this.ddlType.SelectedValue;
            address.AllocPolicy = (IPAllocPolicy)Enum.Parse(typeof(IPAllocPolicy), type);
        }

        if ((this.ddlScanning != null) &&
            (this.chbScanning != null) && this.chbScanning.Checked)
        {
            string scanning = this.ddlScanning.SelectedValue;
            // skip scan .. when 'Off' is selected
            address.SkipScan = (scanning == "0");
        }

        #endregion // Drop Down Lists

        // skip setting other values 
        // .. whether the 'Available' status is set
        if (IsAvailable())
        {
            return address;
        }

        if ((this.txtComment != null) &&
            (this.chbComment != null) && this.chbComment.Checked)
        {
            address.Comments = this.txtComment.Text;
        }

        CustomPropertyRepeater.GetMultiEditIpChanges(address);

        #region System Info

        if ((this.txtMachineType != null) &&
            (this.chbMachineType != null) && this.chbMachineType.Checked)
        {
            address.MachineType = this.txtMachineType.Text;
        }

        if ((this.txtVendor != null) &&
            (this.chbVendor != null) && this.chbVendor.Checked)
        {
            address.Vendor = this.txtVendor.Text;
        }

        if ((this.txtSystemName != null) &&
            (this.chbSystemName != null) && this.chbSystemName.Checked)
        {
            address.sysName = this.txtSystemName.Text;
        }

        if ((this.txtSystemDescription != null) &&
            (this.chbSystemDescription != null) && this.chbSystemDescription.Checked)
        {
            address.Description = this.txtSystemDescription.Text;
        }

        if ((this.txtSystemContact != null) &&
            (this.chbSystemContact != null) && this.chbSystemContact.Checked)
        {
            address.Contact = this.txtSystemContact.Text;
        }

        if ((this.txtSystemLocation != null) &&
            (this.chbSystemLocation != null) && this.chbSystemLocation.Checked)
        {
            address.Location = this.txtSystemLocation.Text;
        }

        #endregion // System Info

        return address;
    }

    private string ListItemCssClass(IPNode ipNode)
    {
        switch (ipNode.Status)
        {
            case IPNodeStatus.Available:
                return "ip-avail";
            case IPNodeStatus.Reserved:
                return "ip-availrsvd";
            case IPNodeStatus.Transient:
                return "ip-transient";
            case IPNodeStatus.Used:
                return "ip-used";
            default:
                return string.Empty;
        }
    }

    private void SetJSCheckBoxControl(Control checkBox, Control control)
    {
        if ((checkBox == null) || (control == null))
        {
            return;
        }

        this.JS.AddScript(
            string.Format("$SW.IPv6.CheckBoxInit('{0}', '{1}');{2}",
            checkBox.ClientID, control.ClientID, Environment.NewLine)
        );
    }

    private bool IsAvailable()
    {
        return this.chbStatus.Checked &&
            IPNodeStatus.Available.ToString().Equals(this.ddlStatus.SelectedValue);
    }

    public bool DisplayingOnlyAvailableIPs()
    {
        string editingAvailableIPs = GetParam(EditingAvailableIPsParam, false.ToString());

        bool yeah;
        if (bool.TryParse(editingAvailableIPs, out yeah) && yeah)
        {
            return true;
        }
        return false;
    }

    #endregion // Methods
}
 