<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="true" 
    Title="<%$ Resources: IPAMWebContent, IPAMWEBDATA_AK1_267 %>" CodeFile="IPv6.IPAddress.Add.aspx.cs" Inherits="IPv6AddressAdd" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMcrtl" Src="~/Orion/IPAM/Controls/IPv6.IPv6AddressEditor.ascx" TagName="IPv6AddressEditor" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.Operator" ExceptDemoMode="true" ErrorPage="/Orion/IPAM/ErrorPages/Error.AccessDenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>
    
<script type="text/javascript">
    $SW.IsDemoServer = <%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer.ToString().ToLower() %>;
</script>

<IPAMmaster:JsBlock Requires="ext,ext-quicktips,sw-expander.js,sw-ipv6-manage.js,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit" runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready(function(){
    var stat = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlStatus' ), 'ddlStatus' );
    var type = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlType' ) );
    var scan = $SW.IPv6.CreateCombo( $SW.nsGet($nsId, 'ddlScanning' ), 'ddlScanning' );

    $SW.IPv6.ScanningChanged($nsId, scan);
    scan.on('select', function(el){ $SW.IPv6.ScanningChanged($nsId, el); });

    var o = {nsId: $nsId};
    o.lic_max = parseInt($SW.LicenseInfo.countmax) || 0;
    o.lic_pos = parseInt($SW.LicenseInfo.countcurrent);
    o.lic_taken = false; // lic_taken always 'false', because adding the new ip
    // set default 'Available' status, when there are no more licenses
    if(o.lic_pos >= o.lic_max) { stat.setValue('Available'); }
    stat.on( 'beforeselect', function(c,r){ $SW.IPv6.StatusChanged(c,r,o); });
    
    if (Ext.getCmp('ddlStatus').getValue() == 'Available'){
        $SW.CustomPropertyFormDisable(true, true, 'cp');
    }

    $('.expander').expander();
});

</IPAMmaster:JsBlock>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server">
.sw-form-cols-normal td.sw-form-col-label { width: 180px; }
.sw-form-cols-normal td.sw-form-col-control { width: 300px; }
.sw-field-label { word-wrap: break-word; }
.sw-form-disabled { color: #777; }
#formbox .align-top { vertical-align: text-top; padding-top: 3px; }
#formbox .align-bottom { vertical-align: bottom; padding-bottom: 3px; }
#formbox hr { height: 1px; border: none; color: #D0D0D0; background-color: #D0D0D0;}
</IPAMmaster:CssBlock>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class=sw-form-header><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_268 %></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_269 %>" style="padding-top:2px; padding-bottom: 12px;">
        <IPAMcrtl:IPv6AddressEditor ID="AddressEditor" runat="server" />
    </div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_270 %>">
      <table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_306 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlStatus" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_308 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlType" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_328 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtAlias" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtAlias"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_329 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_330 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDNS" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtDNS"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_331 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label style="white-space: normal">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_332 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtDhcpClientName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtDhcpClientName"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_333 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_338 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtMAC" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RegularExpressionValidator ID="regexMac" runat="server"     
                    ControlToValidate="txtMAC"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_339 %>" 
                    Display="Dynamic"
                    ValidationExpression="^[0-9A-Fa-f][0-9A-Fa-f]([:\-][0-9A-Fa-f][0-9A-Fa-f]){5,}$"
               /><asp:CustomValidator ID="CustomValidator4" runat="server"
                    ControlToValidate="txtMAC"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_340 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_310 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:DropDownList ID="ddlScanning" runat="server">
                    <asp:ListItem Value="0" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_311 %>"></asp:ListItem>
                    <asp:ListItem Value="1" Selected="True" Text="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_312 %>"></asp:ListItem>
                </asp:DropDownList>
            </div></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_314 %>
            </div></td>
            <td><div class=sw-form-item>
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator8" runat="server"
                    ControlToValidate="txtComment"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_342 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
      </table>
    </div>

    <div><!-- ie6 --></div>

<div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
    <div class="group-box white-bg" style="margin-bottom: 0px">
        <div id="cp">
            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_NodeAttrData" />
        </div>
    </div>
</div>

    <div><!-- ie6 --></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_315 %>" collapsed="true" style="display: none;">
      <table class=sw-form-wrapper border=0 cellspacing="0" cellpadding="0">
        <tr class=sw-form-cols-normal>
            <td class=sw-form-col-label></td>
            <td class=sw-form-col-control></td>
            <td class=sw-form-col-comment></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_317 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtMachineType" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator1" runat="server"
                    ControlToValidate="txtMachineType"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_353 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_318 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtVendor" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator2" runat="server"
                    ControlToValidate="txtVendor"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_354 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_355 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSysName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator3" runat="server"
                    ControlToValidate="txtSysName"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_356 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label style="white-space: normal">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_319 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSysDescr" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator5" runat="server"
                    ControlToValidate="txtSysDescr"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_357 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_320 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSysContact" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator6" runat="server"
                    ControlToValidate="txtSysContact"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_358 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class=sw-field-label>
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_321 %>
            </div></td>
            <td><div class=sw-form-item>
                <asp:TextBox ID="txtSysLocation" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator ID="CustomValidator7" runat="server"
                    ControlToValidate="txtSysLocation"
                    ErrorMessage="<%$ Resources: IPAMWebContent, IPAMWEBDATA_VB1_359 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.IPv6.ValidateNotAvailableStatus"
            /></td>
        </tr>
      </table>
    </div>

    <div><!-- ie6 --></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />

<div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
    <orion:LocalizableButton runat="server" ID="btnSave" OnClientClick="if($SW.IsDemoServer) {demoAction('IPAM_Ipv6Subnet_AddIP', null); return false; }" OnClick="ClickSave" LocalizedText="Save" DisplayType="Primary" CausesValidation="true" />
    <orion:LocalizableButtonLink runat="server" ID="btnCancel" NavigateUrl="~/Orion/IPAM/subnets.aspx" LocalizedText="Cancel" DisplayType="Secondary"/>
</div>

</asp:Content>
