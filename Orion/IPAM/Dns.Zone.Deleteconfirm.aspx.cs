﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;
using System.Data;
using SolarWinds.IPAM.BusinessObjects;
using System.Linq;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZoneDeleteConfirm : CommonPageServices
    {
        private PageDnsZoneDelete del;
        public DnsZoneDeleteConfirm()
        {
            del = new PageDnsZoneDelete(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.del.Initialize();

            if (!this.IsPostBack)
            {
                DataTable servers = del.LoadDnsServers();
                if (servers != null)
                {
                    DhcpServerPlaceHolder.Visible = true;

                    ServerRepeater.DataSource = servers;
                    ServerRepeater.DataBind();
                }

                DataTable zones = del.LoadDnsZones();
                 
                if (zones != null)
                {
                     
                    ZoneRepeater.DataSource = zones;
                    ZoneRepeater.DataBind();
                    RemoveZonePlaceHolder.Visible = true;
                    ChkRemoveZoneFromDnsServer.Enabled = CheckIfRemoveZoneEnabled(zones, "ServerType");
                }
                else
                {
                    DnsZonePlaceHolder.Visible = false;
                    RemoveZonePlaceHolder.Visible = false;
                }
            }
        }

        protected override List<string> GetChromeControlIDs()
        {
            List<string> ret = base.GetChromeControlIDs();
            ret.Add("ButtonBox");
            return ret;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            del.DeleteDnsServerZones = ChkRemoveZoneFromDnsServer.Checked;
            del.Delete(true);
        }

        protected void MsgDelete(object sender, EventArgs e)
        {
            del.DeleteDnsServerZones = ChkRemoveZoneFromDnsServer.Checked;
            del.Delete(true);
        }

        private bool CheckIfRemoveZoneEnabled(DataTable dt, string column)
        {
             return (dt==null || dt.Rows.Count < 0) ? false : !GetSelectedServerTypes(dt, column).Contains(DnsServerType.Infoblox);
        }

        private IEnumerable<DnsServerType> GetSelectedServerTypes(DataTable dt, string column)
        {
            foreach (DataRow item in dt.Rows)
            {
                int serverType;
                Int32.TryParse(item[column].ToString(), out serverType);
                yield return (DnsServerType)serverType;

            }
        }
    }
}