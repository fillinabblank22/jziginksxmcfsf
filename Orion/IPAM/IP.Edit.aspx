<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True" Inherits="SolarWinds.IPAM.WebSite.IPEdit"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_302%>" CodeFile="IP.Edit.aspx.cs" validateRequest="false" %>
<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="ipam" TagName="DhcpIPReservation" Src="~/Orion/IPAM/Controls/DhcpIPReservation.ascx" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DnsZoneSelector.ascx" TagName="DnsZoneSelector" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/DnsRecordsGrid.ascx" TagName="DnsRecordsGrid" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ContentPlaceHolderID="main" runat="server">

<IPAM:AccessCheck ID="AccessCheck" ExceptDemoMode="true" RoleAclOneOf="IPAM.Operator" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />

<!-- parent window will be sending down events, which will cause a postback and run a delegate -->
<IPAMmaster:WindowMsgListener id="MsgListener" Name="dialog" runat="server">
<Msgs>
    <IPAMmaster:WindowMsg Name="Save" CausesValidation="true" OnMsg="MsgSave" runat="server" />
</Msgs>
</IPAMmaster:WindowMsgListener>

<IPAMmaster:CssBlock Requires="sw-ipv6-manage.css" runat="server" >    
    .x-panel-bwrap {
        overflow: visible;
    }
    .x-grid-panel .x-panel-body {
        overflow: visible !important;
    }
    .x-grid3 {
        overflow: visible;
    }
    .x-grid3-viewport {
        overflow: visible;
    }    
    .x-grid3-header-offset { 
        width:auto !important;
    }    
    .x-grid-panel .x-panel-mc .x-panel-body {
        z-index: 1;
    }
    .sw-form-cols-normal td.sw-form-col-label { width: 180px; }
    .sw-form-cols-normal td.sw-form-col-control { width: 300px; }
    .sw-form-item, .sw-field-label { word-wrap: break-word; }
</IPAMmaster:CssBlock>

<IPAMmaster:JsBlock Requires="ext,sw-subnet-edit.js,sw-dialog.js,sw-expander.js" Orientation="jsPostInit" runat="server">
Ext.getUrlParam = function(param)
{
    var params = Ext.urlDecode(location.search.substring(1));
    return param ? params[param] : params;
};

$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
$(document).ready( function(){
    $('.expander').expander();
});

    function transform(id,newid){
        var item = $('#'+id)[0];
        var ops = {
            typeAhead: true,
            triggerAction: 'all',
            transform: id,
            forceSelection: true,
            width: 'auto',
            disabled: item.disabled || false
        };

        if( newid ) ops.id = newid;
        return new Ext.form.ComboBox(ops);
    };

    function ScanningChanged(nsId, el) {
        var ids = ['txtSysLocation', 'txtSysContact', 'txtSysDescr', 'txtSysName', 'txtVendor', 'txtMachineType'];
        return (function(e) {
            var isreadonly = (el.getValue() == '1');
            $.each(ids, function() {
                var item = $SW.ns$(nsId, this)[0];
                if (isreadonly) {
                    item.readOnly = 'readonly';
                    $(item).parent().addClass('x-item-disabled');
                } else {
                    item.readOnly = '';
                    $(item).parent().removeClass('x-item-disabled');
                }
            });
        });
    }
    
	function CheckServerTypeForReservation(status, serverType) {
		var add = $('#addDhcpReservation');
		var rem = $('#remDhcpReservation');
		var resTable = $('#dhcpreservationtable #fileName');
		if (serverType != 5) {				
			if (status == 'Reserved'){
				add.show(); rem.hide();
				resTable.show();
			} else {
				add.hide(); rem.show();
				resTable.hide();
			}
		} else {
			add.hide(); rem.hide();
			resTable.hide();
		}
	}
	
    function ShowDhcpReservationAction(status) {	
    var nodeId = Ext.getUrlParam('nodeId');
    var fail = $SW.ext.AjaxMakeFailDelegate('Get Server Type');                
    Ext.Ajax.request({
        url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx", 
        timeout: fail, failure: fail,					
        success: function (result) {
            var res = Ext.util.JSON.decode(result.responseText);                        
            CheckServerTypeForReservation(status, res.data.ServerType);
        },
        params: { entity: 'IPAM.DhcpServer', verb: 'GetDhcpServerType', nodeId: nodeId }
    });	
    }
    
$(document).ready(function(){

    var s = transform( $SW.nsGet($nsId, 'ddlStatus' ), 'ddlStatus' );
    var t = transform( $SW.nsGet($nsId, 'ddlType' ) );
    var ddlScan = transform( $SW.nsGet($nsId, 'ddlScanning' ), 'ddlScanning' );
    var ddlScanChanged = ScanningChanged($nsId, ddlScan);

    var lic_max = parseInt($SW.LicenseInfo.countmax) || 0;
    var lic_pos = parseInt($SW.LicenseInfo.countcurrent);
    var lic_taken;

    if(s.getValue() == 'Available')
    {
        lic_taken = 0;
        $SW.CustomPropertyFormDisable(true ,true, 'cpEdit');
    }
    else
    {
        lic_taken = 1;
    }

    $SW.IPAM.CurrentIP = $('#' + $SW.nsGet($nsId,'txtNetworkAddress')).val();
    ddlScanChanged();
    ddlScan.on('select', ddlScanChanged);

    ShowDhcpReservationAction(s.getValue());
    s.on( 'beforeselect', function(c,r){
        c.collapse()

        var val = c.getValue();
        if(!lic_taken && (lic_pos >= lic_max && val == 'Available' && r.data.value != 'Available') )
        {
            $SW.Alert('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_67;E=js}','@{R=IPAM.Strings;K=IPAMWEBJS_VB1_168;E=js}',{ buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO });
            return false;
        }

        var f = document.forms[0], i = 0, imax = f.length, elm, ro1 = $SW.nsGet($nsId,'txtNetworkAddress'), ro2 = $SW.nsGet($nsId,'txtParent');
        if( r.data.value == 'Available' && val != 'Available' )
        {
            if( confirm( "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_169;E=js}" ) )
            {
                // blank out all textual values
                for( ; i < imax ; ++i )
                {
                    elm = f[i];
                    if( elm.type == 'select-one') elm.selectedIndex = 0;
                    if( elm.type != 'text' || elm.id == ro1 || elm.id == ro2 ) continue;
                    if( !$(elm).siblings('.x-form-trigger')[0] ) elm.value = '';
                    $SW.CustomPropertyFormDisable(true, true, 'cpEdit');
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            $SW.CustomPropertyFormDisable(false ,false, 'cpEdit');
        }

        ShowDhcpReservationAction(r.data.value);
        window.setTimeout( $SW.Valid.RetestAll, 250 );
        return true;
    });
});

$SW.ddlStatusValue = function(){
    return Ext.getCmp('ddlStatus').getValue();
};

$SW.IsDhcpReservation = function(){
    var chb = $('#addDhcpReservation :checkbox');
    if(chb && chb[0]) return chb[0].checked;
    else return false;
};

$SW.ValidateNotAvailableStatus = function(src,args){
  if( $SW.ddlStatusValue() == 'Available' ){
    if( args.Value.replace( /[ \t]/g, '' ) != '' ) {
      args.IsValid = false; return;
    }
  }
  args.IsValid = true;
};

$SW.ValidateDhcpReservation = function(src,args){
  if( $SW.ddlStatusValue() == 'Reserved' && $SW.IsDhcpReservation() === true){
    if( args.Value.replace( /[ \t]/g, '' ) == '' ){
      args.IsValid = false; return;
    }
  }
  args.IsValid = true;
};

$SW.ValidateEnsureNotBlocked = function(src,args){
  if( $SW.ddlStatusValue() == '' ){
      args.IsValid = false; return;
    }
  args.IsValid = true;
};

function isValueExists(src,args,querys) {

    $.ajax({
        type: "POST",
        url:'/Orion/IPAM/services/information.asmx/GetCount',
        async: false,
        data : { query : querys },
        success: function (result, request) {
            var res = Ext.util.JSON.decode(result);
            if (res.success == true) {
                var iCount = res.data;
                if(iCount == null || iCount == undefined || iCount == '')
                    args.IsValid = true;
                else if(iCount > 0)
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }else
                args.IsValid= true;
        },
        failure: function (result, request) {
            args.IsValid= true;
        }
    });
}

$SW.MacAlreadyExists = function(src,args) {
    
    if(args.Value != '' && $SW.ddlStatusValue() == 'Reserved' && $SW.IsDhcpReservation() === true) {
        var ipAddress = $SW.ns$($nsId, 'txtNetworkAddress');

        var query = 'SELECT TOP 1 Z.ClientMAC AS COUNTS FROM IPAM.DhcpLease Z WHERE Z.ClientMAC = ' + "\'" + args.Value + "\'" + ' AND Z.ClientIpAddress <> ' + "\'" + ipAddress.val() + "\'";
        isValueExists(src,args,query);
    }
};

$SW.ClientNameAlreadyExists = function(src,args) {
    
    if(args.Value != '' && $SW.ddlStatusValue() == 'Reserved' && $SW.IsDhcpReservation() === true) {
        var ipAddress = $SW.ns$($nsId, 'txtNetworkAddress');

        var query = 'SELECT TOP 1 Z.ClientMAC AS COUNTS FROM IPAM.DhcpLease Z WHERE Z.ClientName = ' + "\'" + args.Value + "\'" + ' AND Z.ClientIpAddress <> ' + "\'" + ipAddress.val() + "\'";
        isValueExists(src,args,query);
    }
};

</IPAMmaster:JsBlock>

<IPAMui:ValidationIcons runat="server" />

<div id="formbox">

    <div id="ChromeFormHeader" runat="server" class="sw-form-header"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_323 %></div>

    <div class="group-box white-bg"><table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
        <tr class="sw-form-cols-normal">
            <td class="sw-form-col-label"></td>
            <td class="sw-form-col-control"></td>
            <td class="sw-form-col-comment"></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_324 %>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtParent" readonly="true" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_325%>
            </div></td>
            <td><div class="sw-form-item x-item-disabled">
                <asp:TextBox ID="txtNetworkAddress" readonly="true" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_306%>
            </div></td>
            <td><div class="sw-form-item">
                <asp:DropDownList ID="ddlStatus" runat="server" />
            </div></td>
			<td><asp:CustomValidator runat="server"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_872 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateEnsureNotBlocked"
            /></td>
        </tr>
        <tr>
            <td></td>
            <td><ipam:DhcpIPReservation ID="addDhcpReservation" runat="server"
                    CssStyle="width: 268px; background-color: #F0F0F0;"
                    HeaderCheckboxText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_326 %>"
              /><ipam:DhcpIPReservation ID="remDhcpReservation" runat="server"
                    CssStyle="width: 268px; background-color: #F0F0F0;"
                    HideSupportedTypes="true"
                    HeaderCheckboxText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_327 %>"
              /></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_308%>
            </div></td>
            <td><div class="sw-form-item">
                <asp:DropDownList ID="ddlType" runat="server" />
            </div></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_328 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtAlias" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtAlias"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_329 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
            /></td>
        </tr>        
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_330 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtDNS" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtDNS"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_331 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
            /></td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_332 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtDhcpClientName" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtDhcpClientName"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_333 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
            /><asp:CustomValidator runat="server" ValidateEmptyText="true"
                    ControlToValidate="txtDhcpClientName"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_334 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateDhcpReservation"
                />
                <asp:CustomValidator runat="server" ValidateEmptyText="true"
                    ControlToValidate="txtDhcpClientName"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_24 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ClientNameAlreadyExists"
                />
            </td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_335 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtIPv6" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td>
            <asp:CustomValidator ID="jsIPv6Address" runat="server"     
                    ControlToValidate="txtIPv6"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_336 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.Valid.Fns.ipv6"
               /><asp:CustomValidator runat="server"     
                    ControlToValidate="txtIPv6"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_337 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
               /></td></tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_310 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:DropDownList ID="ddlScanning" runat="server" >
                    <asp:ListItem Value="0" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_311 %>"></asp:ListItem>
                    <asp:ListItem Value="1" Selected="True" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_312 %>"></asp:ListItem>
                </asp:DropDownList>
            </div></td>
        </tr>

        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_338 %>
            </div></td>
            <td><div class="sw-form-item">
                <asp:TextBox ID="txtMAC" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:RegularExpressionValidator ID="regexMac" runat="server"     
                    ControlToValidate="txtMAC"     
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_339 %>" 
                    Display="Dynamic"
                    ValidationExpression="^[0-9A-Fa-f][0-9A-Fa-f]([:\-][0-9A-Fa-f][0-9A-Fa-f]){5,}$"
               /><asp:CustomValidator runat="server"
                    ControlToValidate="txtMAC"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_340 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                /><asp:CustomValidator runat="server" ValidateEmptyText="true"
                    ControlToValidate="txtMAC"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_341 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateDhcpReservation"
                />
                <asp:CustomValidator runat="server" ValidateEmptyText="true"
                    ControlToValidate="txtMAC"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_SE_25 %>"
                    Display="Dynamic"
                    ClientValidationFunction="$SW.MacAlreadyExists" />
            </td>
        </tr>
        <tr>
            <td><div class="sw-field-label">
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_314 %>
            </div></td>
            <td><div class="sw-form-item">
              <asp:TextBox ID="txtComment" CssClass="x-form-text x-form-field" runat="server" />
            </div></td>
            <td><asp:CustomValidator runat="server"
                    ControlToValidate="txtComment"
                    ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_342 %>" 
                    Display="Dynamic"
                    ClientValidationFunction="$SW.ValidateNotAvailableStatus"
            /></td>
        </tr>
    </table>
    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_343 %>">
        <div class="group-box blue-bg">
            <ipam:DnsZoneSelector ID="DnsZoneSelector" ReadOnly="true" runat="server" />
            <ipam:DnsRecordsGrid ID="DnsRecordsGrid" runat="server" />
        </div>
    </div>
    
    <div><!-- ie6 --></div>
    
    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_344 %>">
        <div class="group-box blue-bg">
            <table class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">
                    <td class="sw-form-col-label"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_345 %>
                    </div></td>
                    <td><div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtLastSync" readonly="true" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_346 %>" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_347 %>
                    </div></td>
                    <td><div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtLastResponse" readonly="true" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_348 %>" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_349%>
                    </div></td>
                    <td><div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtLastCredential" readonly="true" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_58 %>" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_350%>
                    </div></td>
                    <td><div class="sw-form-item x-item-disabled">
                        <asp:TextBox ID="txtLeaseExpiration" readonly="true" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_351 %>" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                </tr>
            </table>
        </div>
    </div>

    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_352 %>">
        <div class="group-box blue-bg">
            <table class="sw-form-wrapper" border=0 cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">
                    <td class="sw-form-col-label"></td>
                    <td class="sw-form-col-control"></td>
                    <td class="sw-form-col-comment"></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_317 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtMachineType" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtMachineType"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_353 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_318 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtVendor" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtVendor"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_354 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_355 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtSysName" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtSysName"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_356 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_319 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtSysDescr" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtSysDescr"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_357 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_320 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtSysContact" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtSysContact"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_358 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
                <tr>
                    <td><div class="sw-field-label">
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_321 %>
                    </div></td>
                    <td><div class="sw-form-item">
                        <asp:TextBox ID="txtSysLocation" CssClass="x-form-text x-form-field" runat="server" />
                    </div></td>
                    <td><asp:CustomValidator runat="server"
                            ControlToValidate="txtSysLocation"
                            ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_359 %>" 
                            Display="Dynamic"
                            ClientValidationFunction="$SW.ValidateNotAvailableStatus"
                    /></td>
                </tr>
            </table>
        </div>
    </div>
    
    <div><!-- ie6 --></div>

    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
        <div class="group-box blue-bg" style="margin-bottom: 0px">
            <div id="cpEdit">
                <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_NodeAttrData" />
            </div>
        </div>
    </div>

    <div><!-- ie6 --></div>

</div>

<asp:ValidationSummary id="valSummary" runat="server"
    ShowSummary="false"
    ShowMessageBox="true"
    DisplayMode="BulletList" />
    <table>
        <tr>
            <td style="width: 130px">
            </td>
            <td>
                <div id="ChromeButtonBar" runat="server" class="sw-btn-bar">
                    <orion:LocalizableButton runat="server" CausesValidation="true" ID="btnSave" OnClick="ClickSave"
                        OnClientClick="if(CheckDemoMode()) {demoAction('IPAM_Subnet_EditIP', null); return false; }"
                        LocalizedText="Save" DisplayType="Primary" />
                    <orion:LocalizableButton runat="server" CausesValidation="false" ID="btnCancel" OnClick="ClickCancel"
                        LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
