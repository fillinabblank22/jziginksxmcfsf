using System;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Controls;
using SolarWinds.IPAM.Web.Master;

namespace SolarWinds.IPAM.WebSite
{
	public partial class ImportPickcolumns : CommonPageServices
	{
        private PageImportPickColumns mgr;
        public ImportPickcolumns() { mgr = new PageImportPickColumns(this); }

	    protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["GroupID"] != null)
               this.AccessCheck.GroupIds.Add(Convert.ToInt32(Request.QueryString["GroupID"]));

            if (Page.Request.QueryString["SaveCmb"] != null)
                mgr.Save();
		}

        protected void OnCancelClick(object src, EventArgs args)
        {
            string refUrl = "~/Orion/IPAM/Subnets.aspx";
            SolarWinds.Orion.Web.Helpers.DiscoveryCentralHelper.Return(refUrl);
        }
    }
}
