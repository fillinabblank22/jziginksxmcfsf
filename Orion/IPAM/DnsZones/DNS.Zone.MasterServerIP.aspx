﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/DefaultMaster.Master" AutoEventWireup="True"
    Inherits="SolarWinds.IPAM.WebSite.DNS_Zone_MasterServerIP" Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_797 %>"
    CodeFile="DNS.Zone.MasterServerIP.aspx.cs" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="IPAM" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="main" runat="server">
    <IPAM:AccessCheck ID="AccessCheck" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"
        ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    <!-- parent window will be sending down events, which will cause a postback and run a delegate -->
    <IPAMmaster:WindowMsgListener ID="MsgListener" Name="dialog" runat="server">
        <Msgs>
            <IPAMmaster:WindowMsg ID="WindowMsg1" Name="Save" CausesValidation="true" OnMsg="MsgSave"
                runat="server" />
        </Msgs>
    </IPAMmaster:WindowMsgListener>
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-subnet-edit.js,sw-dialog.js" Orientation="jsPostInit"
        runat="server">
$(window).load( function(){ $SW.msgq.UPSTREAM('load',window); });
$(document).ready( function(){ $SW.msgq.UPSTREAM('ready',document); });
    </IPAMmaster:JsBlock>
    <script type="text/javascript" language="javascript">
        if (!$SW.IPAMValid) $SW.IPAMValid = {};
    
    </script>
    <IPAMui:ValidationIcons ID="ValidationIcons" runat="server" />
    <div id="formbox">
        <div id="ChromeFormHeader" runat="server" class="sw-form-header">
            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_798 %></div>
        <div style="padding-bottom: 20px">
            
        </div>
        <div>
            <table  class="sw-form-wrapper" border="0" cellspacing="0" cellpadding="0">
                <tr class="sw-form-cols-normal">                     
                    <td  class="sw-form-col-label" style="width: 150px">
                        <div class="sw-field-label" style="width: 150px">
                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_799 %>
                        </div>
                    </td>
                    <td class="sw-form-col-control"> 
                        <div class="sw-form-item" >
                            <asp:TextBox ID="txtServerIPAddress" CssClass="x-form-text x-form-field" runat="server" />
                        </div>
                    </td>
                    <td class="sw-form-col-comment">
                        <asp:RequiredFieldValidator ID="NetworkAddressRequire" runat="server" ControlToValidate="txtServerIPAddress"
                            Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_800 %>" />
                        <asp:CustomValidator ID="NetworkAddressIPv4" runat="server" ControlToValidate="txtServerIPAddress"
                            ClientValidationFunction="$SW.Valid.Fns.ipv4" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_801 %>" />
                        <asp:CustomValidator ID="NetworkAddressReplicate" runat="server" ControlToValidate="txtServerIPAddress"
                            OnServerValidate="OnNetworkAddressReplicateValidate" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_JI_9 %>" />                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true"
        DisplayMode="BulletList" />
    <div id="ChromeButtonBar" runat="server" class="sw-btn-bar-wizard">
        <orion:LocalizableButton CausesValidation="true" ID="btnSave" OnClick="ClickSave" DisplayType="Primary" LocalizedText="Save" runat="server" />
        <orion:LocalizableButtonLink NavigateUrl="~/Orion/IPAM/DnsZones/Dns.Zone.TypeDefine.aspx" runat="server" DisplayType="Secondary" LocalizedText="Cancel"/>
    </div>
</asp:Content>
