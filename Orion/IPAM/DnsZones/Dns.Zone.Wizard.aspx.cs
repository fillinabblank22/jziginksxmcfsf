using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Globalization;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZonesWizard : CommonPageServices
    {
        #region Declarations

        private const string QueryServerParam = "ObjectID";
        private const string QueryZoneParam = "ZoneID";

        private PageDnsZoneEditor Editor { get; set; }
        private string WebId { get; set; }
        private int ServerId { get; set; }
        private int ZoneId { get; set; }

        #endregion // Declarations

        #region Properties

        public int? DnsZoneId { get; set; }
        public bool ReadOnly { get; set; }

        #endregion

        #region Constructors

        public DnsZonesWizard()
        {
            this.Editor = new PageDnsZoneEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DnsZoneWizardStep.Define;

            this.WebId = GetParam(DnsSessionStorage.PARAM_WEBID, null);

            //  Dns Server from server's GroupId
            this.ServerId = GetParam(QueryServerParam, -1);
            if (string.IsNullOrEmpty(this.WebId) && this.ServerId >= 0)
            {
                this.WebId = Editor.Session_SetServerInfo(this.ServerId);
            }

            //  Dns zone from zone's GroupId
            this.ZoneId = GetParam(QueryZoneParam, -1);
            if (string.IsNullOrEmpty(this.WebId) && this.ZoneId >= 0)
            {
                this.WebId = Editor.Session_SetZoneInfo(this.ZoneId);
            }
            // if no zone and server is selected
            if (this.ServerId < 0 && this.ZoneId < 0 && Editor.Session_IsAddMode(WebId))
            {
                this.WebId = Editor.Session_GetNewWebId();
            }

            if (!IsPostBack)
            {
                InitializeDropDowns();
                InitControls();
            }

            Title = Editor.Session_IsAddMode(WebId) ? Resources.IPAMWebContent.IPAMWEBCODE_VB1_28 : Resources.IPAMWebContent.IPAMWEBCODE_VB1_29;
            btnHelp.HelpUrlFragment = Editor.Session_IsAddMode(WebId) ? "OrionIPAMPHDNSZoneAdd" : "OrionIPAMAGEditDNSZone";
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.Session_Clear(webId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            this.hdWebId.Value = this.WebId;

            DnsZone zone; //= new DnsZone();

            Editor.Session_RetrieveZoneInfo(this.WebId, out zone);

            CustomPropertyRepeater.RetrieveCustomProperties(zone, new List<int>(){ zone.GroupId });

            var serverName = Editor.Session_RetrieveServerName(this.WebId);
            var serverItem = ddlDnsServers.Items.FindByText(serverName);
            ddlDnsServers.SelectedIndex = ddlDnsServers.Items.IndexOf(serverItem);

            bool addMode = Editor.Session_IsAddMode(this.WebId);
            if (!addMode)
            {
                ddlDnsServers.Enabled = false;
                ddlDnsServers.CssClass += " x-item-disabled disabled-textbox";
            }

            this.ProgressIndicator1.WebId = this.WebId;
            Editor.ActivateProgressStep(this.WebId, PageDnsZoneEditor.IPAM_DNSZONE_DEFAULT);
        }

        private void Next()
        {
            if (!ValidateUserInput())
                return;

            string webId = this.hdWebId.Value;
            DnsZone zone;

            Editor.Session_RetrieveZoneInfo(webId, out zone);

            if (ddlDnsServers.SelectedItem != null)
            {
                zone.NodeId = Convert.ToInt32(ddlDnsServers.SelectedItem.Value);
                zone.ParentId = zone.NodeId;
            }

            CustomPropertyRepeater.GetChanges(zone);

            Editor.Sesssion_UpdateServerInfo(webId, zone);

            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, webId);
            Response.Redirect(url, true);
        }

        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }

        private void InitializeDropDowns()
        {
            ddlDnsServers.Items.Clear();

            using (var proxy = SwisConnector.GetProxy())
            {
                var servers = proxy.AppProxy.DnsServer.GetAll();

                foreach (var server in servers)
                {
					if(server.ServerType == DnsServerType.Infoblox)
					{
						continue;
					}
                    var item = new ListItem
                    {
                        Text = server.FriendlyName,
                        Value = server.NodeId.ToString(CultureInfo.InvariantCulture)
                    };
                    ddlDnsServers.Items.Add(item);
                }

            }
            if (ddlDnsServers.Items.Count == 0)
            {
                var emptyItem = new ListItem
                {
                    Text = Resources.IPAMWebContent.IPAMWEBCODE_VB1_27,
                    Value = "-1"
                };
                ddlDnsServers.Items.Add(emptyItem);
                imgbNext.Enabled = false;
            }
        }

        #endregion // Methods
    }
}
