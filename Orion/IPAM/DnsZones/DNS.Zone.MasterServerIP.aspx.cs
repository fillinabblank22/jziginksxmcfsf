﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Net;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.IPAM.BusinessObjects.DNSManagement;
using SolarWinds.IPAM.DNSMultiDevice.Factory;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DNS_Zone_MasterServerIP : CommonPageServices
    {
        #region Constants

        private const string QUERY_OBJECT_ID_PARAM = "ObjectId";
        private const string QUERY_SERVER_PARAM = "serverAddress";
        #endregion

        #region Variables

        private int? _objectId;
        private IPAddress _serverAddress;
        private bool isEdit = false;
       
        private string _BindserverAddress="";
        private PageDnsZoneEditor Editor { get; set; }
        private bool isCapableOfViewManagement;
        #endregion

        #region   methods

           #region Constructors

        public DNS_Zone_MasterServerIP()
        {
            this.Editor = new PageDnsZoneEditor(this);
        }

        #endregion // Constructors
        protected void Page_Load(object sender, EventArgs e)
        {
            var serverId = Editor.Session_RetrieveServerGroupId(null);

            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);

            ParseParams();

            if (Page.IsPostBack)
                return;

            if (isCapableOfViewManagement)
                txtServerIPAddress.Text = _BindserverAddress;
            else
            {
              if (_serverAddress == null)
                    return;
                txtServerIPAddress.Text = _serverAddress.ToString();
            }
         }

        protected void MsgSave(object sender, EventArgs e)
        {
            if (Save())
                Redirect();
        }

        private void ParseParams()
        {
            //Optional 

            var start = HttpUtility.HtmlDecode((Page.Request[QUERY_SERVER_PARAM]));
            if (!String.IsNullOrEmpty(Convert.ToString(start)))
            {
                if (isCapableOfViewManagement)
                    _BindserverAddress = Convert.ToString(start);
                else
                {
                    if (!IPAddress.TryParse(Convert.ToString(start), out _serverAddress))
                    {
                        ExceptionContext.MoveToExceptionPage(null, Resources.IPAMWebContent.IPAMWEBCODE_VB1_34,
                                                             QUERY_SERVER_PARAM);
                        return;
                    }
                 }
                isEdit = true;
            }

        }
        public bool Save()
        {
            string enteredAddress = txtServerIPAddress.Text.Trim();
            if ((_serverAddress != null && enteredAddress.Equals(_serverAddress.ToString())) || (_BindserverAddress != "" && enteredAddress.Equals(_BindserverAddress)))
                return true;
            Page.Validate();

            if (!Page.IsValid)
                return false;

            using (DnsSessionStorage storage = new DnsSessionStorage(this.Page, null))
            {
                var masterServers = storage.GetMasterServersStorage();

                var serverAddress = txtServerIPAddress.Text;
                try
                {
                    if (serverAddress != string.Empty)
                    {
                        if (isEdit)
                        {
                            if (isCapableOfViewManagement &&
                                masterServers.ContainsValue(_BindserverAddress))
                            {
                                KeyValuePair<int, string> editAddress =
                                    masterServers.Where(x => x.Value == _BindserverAddress).First();
                                masterServers[editAddress.Key] = serverAddress.Trim();
                            }
                            else if(masterServers.ContainsValue(_serverAddress.ToString()))
                            {
                                KeyValuePair<int, string> editAddress = masterServers.Where(x => x.Value == _serverAddress.ToString()).First();
                                masterServers[editAddress.Key] = serverAddress.Trim();
                            }
                            else
                            {
                                masterServers.Add(masterServers.Count + 1, serverAddress.Trim());
                            }
                        }
                        else
                        {
                            masterServers.Add(masterServers.Count + 1, serverAddress.Trim());
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionContext.MoveToExceptionPage(ex, Resources.IPAMWebContent.IPAMWEBCODE_VB1_35, Resources.IPAMWebContent.IPAMWEBCODE_VB1_36);
                }
            }
            return true;
        }

        public void Redirect()
        {
            // var refUrl = "~/Orion/IPAM/DnsZones/Dns.Zone.TypeDefine.aspx";

            // chromeless page
            //if (Page.h)
            var refUrl = "~/Orion/IPAM/DialogWindowDone.aspx?sendlicense=false";

            Page.Server.Transfer(refUrl);
        }

        protected void ClickSave(object sender, EventArgs e)
        {

        }

        protected void ClickCancel(object sender, EventArgs e)
        {

        }


        protected void OnNetworkAddressReplicateValidate(object sender, ServerValidateEventArgs args)
        {
            using (DnsSessionStorage storage = new DnsSessionStorage(this.Page, null))
            {
                var masterServers = storage.GetMasterServersStorage();

                var serverAddress = txtServerIPAddress.Text;
                if (serverAddress != string.Empty)
                {
                    serverAddress = serverAddress.Trim();
                    if (masterServers.ContainsValue(serverAddress))
                    {
                        args.IsValid = false; return;
                    }
                }

                args.IsValid = true;
            }


        }
        
        #endregion
    }
}
