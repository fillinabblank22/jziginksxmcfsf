﻿using System;
using System.Web.UI;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Helpers;
using System.Collections.Generic;
using SolarWinds.IPAM.Storage.Audit;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.IPAM.DNSMultiDevice.Factory;
using SolarWinds.IPAM.BusinessObjects.DNSManagement;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZoneReview : CommonPageServices
    {
        #region Declarations

        private PageDnsZoneEditor Editor { get; set; }

        #endregion // Declarations

        #region Constructors

        public DnsZoneReview()
        {
            this.Editor = new PageDnsZoneEditor(this);
        }

        #endregion // Constructors

        #region Properties

        private const string PARAM_TASKID = "taskid";

        public string TaskId
        {
            get
            {
                return base.GetParam(PARAM_TASKID, string.Empty);
            }
        }

        public bool AddMode { get; set; }

        #endregion // Properties

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DnsZoneWizardStep.Review;

            if (!IsPostBack)
            {
                InitControls();
            }
            AddMode = true;
            Title = Editor.Session_IsAddMode(null) ? Resources.IPAMWebContent.IPAMWEBCODE_VB1_28 : Resources.IPAMWebContent.IPAMWEBCODE_VB1_29;
            btnHelp.HelpUrlFragment = Editor.Session_IsAddMode(null) ? "OrionIPAMPHDNSZoneAdd" : "OrionIPAMAGEditDNSZone";

            if (!Editor.Session_IsAddMode(null))
            {
                imgbNext.Text = Resources.IPAMWebContent.IPAMWEBDATA_VB1_824;
                AddMode = false;
            }
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                imgbNext.OnClientClick = "javascript:demoAction('IPAM_Create_DNS_Zone',null); return false;";
            }

        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            Editor.Session_Clear(null);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            var webId = GetParam(DnsSessionStorage.PARAM_WEBID, null);
            DNSServerLabel.Text = Editor.Session_RetrieveServerName(webId);
            DnsZone zone;
            Editor.Session_RetrieveZoneInfo(null, out zone);
            DNSZoneNameLabel.Text = zone.FriendlyName;

            var serverId = Editor.Session_RetrieveServerGroupId(null);
            
            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            var isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);

            if (!isCapableOfViewManagement)
                DnsViewDiv.Visible = false;
            else
            {
                DnsViewDiv.Visible = true;
                DnsViewNameLabel.Text = Editor.Session_RetrieveViewName(webId);
            }
            
            DNSZoneTypeLabel.Text = GetLocalizedProperty("DnsZoneType", zone.ZoneType.ToString());
            ZoneFileNameLabel.Text = zone.DataFileName;

            DNSLookupTypeLabel.Text = EventMsgHelper.GetEnumDisplayString(typeof(DnsLookUpType), zone.LookUpType);
            dvMasterDnsServers.Style["display"] = "none";
            if (zone.ZoneType != DnsZoneType.Primary)
            {
                Dictionary<int, string> masterDnsservers;
                Editor.Session_RetrieveMasterservers(null, out masterDnsservers);
                contentsRepeater.DataSource = masterDnsservers;
                contentsRepeater.DataBind();
                dvMasterDnsServers.Style["display"] = "block" ;
            }
            if (zone.ScanInterval.HasValue)
                transferInterval.Text = zone.ScanInterval.ToString();

            bool disabled;
            if (zone.DisableAutoScanning.HasValue)
            {
                disabled = zone.DisableAutoScanning.Value;
                EnableZoneTransferLabel.Style["display"] = (!disabled) ? "block" : "none";
                if (!disabled)
                {
                    disabled = (zone.ScanInterval.HasValue && zone.ScanInterval > 0);
                    DefinedzoneTransferLabel.Style["display"] = (disabled) ? "block" : "none";
                    DefaultzoneTransferLabel.Style["display"] = (!disabled) ? "block" : "none";
                    PreferIncrementalLabel.Style["display"] = (zone.IncrementalZoneTransfer) ? "block" : "none";
                }
            }
            Editor.ActivateProgressStep(null, PageDnsZoneEditor.IPAM_DNSZONE_REVIEW);
        }


        private void Next()
        {
            if (!ValidateUserInput())
                return;

            if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Editor.Save(null);

                string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
                Response.Redirect(url, true);
            }
        }

        private bool ValidateUserInput()
        {
            return true;
        }

        protected string GetLocalizedProperty(string prefix, string property)
        {
            ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
            string key = manager.CleanResxKey(prefix, property);
            return manager.SearchAll(key, manager.GetAllResourceManagerIds()) ?? property;
        }


        #endregion // Methods


    }
}