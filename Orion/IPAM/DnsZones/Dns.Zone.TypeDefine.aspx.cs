﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Common;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Globalization;
using SolarWinds.IPAM.DNSMultiDevice.Factory;
using SolarWinds.IPAM.BusinessObjects.DNSManagement;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZoneTypeDefine : CommonPageServices
    {
        #region Declarations

        private PageDnsZoneEditor Editor { get; set; }
        public DnsServer DnsServer { get; set; }

        #endregion // Declarations

        public string WebParamId
        {
            get
            {
                return GetParam(DnsSessionStorage.PARAM_WEBID, null);
            }
        }
        public int ZoneType
        {
            get
            {
                return Editor.Session_RetrieveServerZoneType(WebParamId);
            }
        }     
        public bool IsDsIntegreated
        {
            get
            {
                return Editor.Session_RetrieveDsIntegrated(WebParamId);
            }
        }

        public bool AddMode
        {
            get
            {
                return Editor.Session_IsAddMode(null);
            }
        }
        public int ServerGroupId
        {
            get {
                return Editor.Session_RetrieveServerGroupId(WebParamId);
            }
        }

        #region Constructors

        public DnsZoneTypeDefine()
        {
            this.Editor = new PageDnsZoneEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DnsZoneWizardStep.TypeDefine;

            var serverId = Editor.Session_RetrieveServerGroupId(null);
            this.DnsServer = Editor.GetServerByGroupId(serverId);
            bool addMode = Editor.Session_IsAddMode(null);    
           
            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            var isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);
            if (!IsPostBack)
            {
                if (!isCapableOfViewManagement)
                {
                    dnsViewDiv.Style["display"] = "none";
                    divPrimaryZoneAD.Style["display"] = "block";
                }
                else
                {
                    //stubDiv.Style["display"] = "none";                    
                    dnsViewDiv.Style["display"] = "block";
                    if (!addMode)
                    {
                        ddlDnsViews.Enabled = false;
                        ddlDnsViews.CssClass += " x-item-disabled disabled-textbox";
                    }
                    InitializeDropDowns(this.DnsServer.NodeId);
                    divPrimaryZoneAD.Style["display"] = "none";
                }
                
                InitControls();
            }
            var webId = GetParam(DnsSessionStorage.PARAM_WEBID, null);
            SecondaryZoneMasterServer.WebId = webId;
            StubZoneMasterServer.WebId = webId;

            Title = addMode ? Resources.IPAMWebContent.IPAMWEBCODE_VB1_28 : Resources.IPAMWebContent.IPAMWEBCODE_VB1_29;
            btnHelp.HelpUrlFragment = addMode ? "OrionIPAMPHDNSZoneAdd" : "OrionIPAMAGEditDNSZone";
        }
         
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            imgbNext.Click += new EventHandler(imgbNext_Click);
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.Session_Clear(webId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion // Event Handlers

        #region Methods

        private void InitControls()
        {
            DnsZone zone;
            Editor.Session_RetrieveZoneInfo(null, out zone);

            if (zone.DnsViewId > 0)
            {
                var index = ddlDnsViews.Items.FindByValue(zone.DnsViewId.ToString());
                ddlDnsViews.SelectedIndex = ddlDnsViews.Items.IndexOf(index);
            }

            switch (zone.ZoneType)
            {
                case DnsZoneType.Primary:
                    rdPrimaryZone.Checked = true;
                    chPrimaryZoneAD.Checked = zone.DsIntegrated;
                    break;
                case DnsZoneType.Secondary:
                    rdSecondaryZone.Checked = true;
                    break;
                case DnsZoneType.Stub:
                    rdStubZone.Checked = true;
                    break;
            }
            switch (zone.LookUpType)
            {
                case DnsLookUpType.Forward:
                    rdForwardType.Checked = true;
                    rdReverseType.Checked = false;
                    txtZoneName.Text = zone.FriendlyName;
                    break;
                case DnsLookUpType.Reverse:
                    rdReverseType.Checked = true;
                    rdForwardType.Checked = false;
                    string zoneName = Editor.Session_GetUserInputZoneName();
                    if(zoneName == string.Empty)
                        zoneName = zone.FriendlyName;

                    if (zoneName.Contains("in-addr.arpa"))
                    {
                        rdReverseName.Checked = true;
                        rdNetworkID.Checked = false;
                        txtReverseName.Text = zone.FriendlyName;
                    }
                    else
                    {
                        rdReverseName.Checked = false;
                        rdNetworkID.Checked = true;
                        txtNetworkID.Text = zoneName;
                    }
                    break;
            }
            if (zone.DnsZoneId != -1)
            {
                rdForwardType.Enabled = false;
                txtZoneName.Enabled = false;

                rdReverseType.Enabled = false;                
               

                rdReverseName.Enabled =false;
                rdNetworkID.Enabled = false;

                txtReverseName.Enabled = false;
                txtNetworkID.Enabled = false;
            }
            chPrimaryZoneAD.Enabled = false;
            

            Editor.ActivateProgressStep(null, PageDnsZoneEditor.IPAM_DNSZONE_TYPE);
            
        }

        private void Next()
        {
            if (!ValidateUserInput())
                return;

            DnsZone zone;
            Editor.Session_RetrieveZoneInfo(null, out zone);
            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            var isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);

            if (ddlDnsViews.SelectedItem != null && isCapableOfViewManagement)
            {
                zone.DnsViewId = Convert.ToInt32(ddlDnsViews.SelectedItem.Value);
                Editor.Sesssion_UpdateViewInfo(null, ddlDnsViews.SelectedItem.Text);
            }

            zone.LookUpType = (rdForwardType.Checked) ? DnsLookUpType.Forward : (rdReverseType.Checked) ? DnsLookUpType.Reverse : DnsLookUpType.Unknown;
            zone.ZoneType = (rdPrimaryZone.Checked) ? DnsZoneType.Primary : (rdSecondaryZone.Checked) ? DnsZoneType.Secondary : (rdStubZone.Checked) ? DnsZoneType.Stub : DnsZoneType.Unknown;
            zone.DsIntegrated = (zone.ZoneType == DnsZoneType.Primary && !isCapableOfViewManagement) ? chPrimaryZoneAD.Checked : false;
            //zone.MasterDnsServers =  ;

            string zoneName = string.Empty;
            if (rdForwardType.Checked)
            {
                zone.FriendlyName = txtZoneName.Text;              
            }
            if (rdReverseType.Checked && rdNetworkID.Checked)
            {
                zoneName = txtNetworkID.Text;
                Editor.Session_SetUserInputZoneName(zoneName);

                zone.FriendlyName = zoneName.Contains("in-addr.arpa") ? zoneName :   ReverseString(zoneName) + ".in-addr.arpa";
            }
            if (rdReverseType.Checked && rdReverseName.Checked)
            {
                zoneName = txtReverseName.Text;
                Editor.Session_SetUserInputZoneName(zoneName);

                zone.FriendlyName = zoneName.Contains("in-addr.arpa") ? zoneName : zoneName + ".in-addr.arpa";
            }
            
            Editor.Sesssion_UpdateZoneInfo(null, zone);

            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
            Response.Redirect(url, true);
        }

        private string ReverseString(string input)
        {
            string[] inputarray = input.Split(new char []{ '.'},StringSplitOptions.RemoveEmptyEntries);
            string output = string.Empty;
            if (inputarray.Length > 0)
            {
                for (int i = inputarray.Length; i > 0; i--)
                {
                    output += inputarray[i-1] + ".";
                }
                output = output.TrimEnd('.');
            }
            return output;
        }


        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }
                 

        private void InitializeDropDowns(int nodeId)
        {
            ddlDnsViews.Items.Clear();

            using (var proxy = SwisConnector.GetProxy())
            {
                var views = proxy.AppProxy.DnsView.GetViewsByNodeId(nodeId);

                foreach (var view in views)
                {
                    var item = new ListItem
                    {
                        Text = view.Name,
                        Value = view.DnsViewId.ToString(CultureInfo.InvariantCulture)
                    };
                    ddlDnsViews.Items.Add(item);
                }

            }

            if (ddlDnsViews.Items.Count == 0)
            {
                var emptyItem = new ListItem
                {
                    Text = Resources.IPAMWebContent.IPAMWEBCODE_VV0_9,
                    Value = "-1"
                };
                ddlDnsViews.Items.Add(emptyItem);
                ddlDnsViews.Enabled = false;
            }
        }
         
        #endregion // Methods

    }
}
