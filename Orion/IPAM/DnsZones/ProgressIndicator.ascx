
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProgressIndicator.ascx.cs"
    Inherits="DnsZone_ProgressIndicator" %>
    
<orion:Include ID="Include1" runat="server" File="ProgressIndicator.css" />

    <style type="text/css">
        .PI_on:link, .PI_on:visited, .PI_on:hover, .PI_on:active { color: white; }
        .PI_off:link, .PI_off:visited, .PI_off:hover, .PI_off:active { color: black; }
        .ProgressIndicator
        {
            padding-bottom:0px !important;
            margin-bottom:0px !important; 
            border-bottom: #dfdfde solid 1px !important; 
            height:17px !important;
            background: url('/Orion/images/ProgressIndicator/pi_bg_gradient_sm.gif') !important; 
            background-repeat: repeat-x !important; 
            background-position:top !important;
            white-space: nowrap !important;
        }
    </style>

    <div class="ProgressIndicator">
        <asp:PlaceHolder ID="phPluginImages" runat="server"></asp:PlaceHolder>
    </div>
    
