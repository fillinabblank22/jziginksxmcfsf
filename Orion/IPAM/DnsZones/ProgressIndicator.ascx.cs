using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Web.UI;

public enum DnsZoneWizardStep
{
    Define,
    TypeDefine,
    Properties, 
    Review
};

public struct DnsSteps
{
    public DnsZoneWizardStep StepName;
    public string Pageurl;
    public string ImageName;
    public string HighlightImage;
    public string DisplayText;
}

public partial class DnsZone_ProgressIndicator : System.Web.UI.UserControl
{
    private readonly string indicatorImageFolderPath = "~/Orion/IPAM/res/images/progress_indicator/";
    List<DnsSteps> zoneSteps = new List<DnsSteps>();

    public string WebId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        zoneSteps.Add(new DnsSteps() { StepName = DnsZoneWizardStep.Define, Pageurl = "Dns.Zone.Wizard.aspx", ImageName = "PI_definingscope_off.png", HighlightImage = "PI_definingscope_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBCODE_VB1_30 });
        zoneSteps.Add(new DnsSteps() { StepName = DnsZoneWizardStep.TypeDefine, Pageurl = "Dns.Zone.TypeDefine.aspx", ImageName = "PI_ipaddressrange_off.png", HighlightImage = "PI_ipaddressrange_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBCODE_VB1_31 });
        zoneSteps.Add(new DnsSteps() { StepName = DnsZoneWizardStep.Properties, Pageurl = "Dns.Zone.Properties.aspx", ImageName = "PI_scopeproperties_off.png", HighlightImage = "PI_scopeproperties_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBCODE_VB1_32 });
        zoneSteps.Add(new DnsSteps() { StepName = DnsZoneWizardStep.Review, Pageurl = "Dns.Zone.Review.aspx", ImageName = "PI_review_off.png", HighlightImage = "PI_review_on.png", DisplayText = Resources.IPAMWebContent.IPAMWEBCODE_VB1_33 });
        
        LoadProgressImages();       

    }

    #region public properties

    [Browsable(true)]
    [Category("Appearance")]
    public DnsZoneWizardStep CurrentStep
    {
        set;
        get;
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string NextStep
    {
        get
        {
            int index = zoneSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index + 1 < zoneSteps.Count) ? zoneSteps[index + 1].Pageurl : zoneSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string PrevStep
    {
        get
        {
            int index = zoneSteps.FindIndex(x => x.StepName == this.CurrentStep);
            return (index - 1 >= 0) ? zoneSteps[index - 1].Pageurl : zoneSteps[index].Pageurl;
        }
    }

    [Browsable(true)]
    [Category("Appearance")]
    public string HomePage
    {
        get
        {
            return "~/api2/ipam/ui/zones";
        }
    }
    #endregion

    #region public methods

    public void LoadProgressImages()
    {
        RenderProgressImages();
    }

    #endregion

    #region private methods

    private void RenderProgressImages()
    {
        phPluginImages.Controls.Clear();
        int count = zoneSteps.Count;

        for (int i = 0; i < count; i++)
        {
            bool nextActive;

            Image imgSeparator = new Image();
            imgSeparator.ID = string.Format("imgSep{0}", i + 1);
            bool isCurrentStep = false;
            string url = "";

            nextActive = (i < count - 1) ? (this.CurrentStep == zoneSteps[i + 1].StepName) : false;

            if (IsStepActive(this.zoneSteps[i].StepName.ToString()))
            {
                url = PageDnsZoneEditor.AddWebIdParam(this.Page, zoneSteps[i].Pageurl, this.WebId);
            }

            if (this.CurrentStep == zoneSteps[i].StepName)
            {
                isCurrentStep = true;
                imgSeparator.ImageUrl = string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "on", "off");
            }
            else
            {
                imgSeparator.ImageUrl = (nextActive) ? string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "on") : string.Format("{0}pi_sep_{1}_{2}.gif", indicatorImageFolderPath, "off", "off");

            }

            HyperLink tb = new HyperLink();
            tb.Text = zoneSteps[i].DisplayText;
            tb.NavigateUrl = url;
            tb.CssClass = string.Format("PI_{0}", isCurrentStep ? "on" : "off");
            tb.Attributes["automation"] = tb.Text;
            // Text of first step must be move to the right a little 
            if (i == 0)
            {
                tb.Style.Add("padding-left", "10px;");
            }

            phPluginImages.Controls.Add(tb);
            phPluginImages.Controls.Add(imgSeparator);
        }
    }


    private bool IsStepActive(string stepName)
    {
        using (var storage = new DnsSessionStorage(this.Page, this.WebId))
        {
            return storage[stepName] != null ? 
                Convert.ToBoolean(storage[stepName]) : false;
        }
    }

    #endregion
}
