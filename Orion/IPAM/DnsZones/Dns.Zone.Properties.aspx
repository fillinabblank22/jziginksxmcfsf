﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Orion/IPAM/DnsZones/Dns.Zone.Properties.aspx.cs"
    MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" Inherits="SolarWinds.IPAM.WebSite.DnsZoneProperties"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_764 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register Src="~/Orion/IPAM/DnsZones/ProgressIndicator.ascx" TagName="Progress"
    TagPrefix="orion" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/TimeSpanEditor.ascx" TagName="TimeSpanEditor" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <IPAM:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDNSZoneAdd" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <IPAM:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <IPAMmaster:CssBlock ID="CssBlock1" runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
        .disabled-textbox { 
    background-color:#fff;
    background-image: url(/Orion/IPAM/res/images/sw/textBox-bg.gif);
    border-color:#b5b8c8;        
        }
        .checkbox label{ padding-left:5px; }
        .sw-form-cols-normal td.sw-form-col-control { width: 700px; }
        .help-text {
            color: #646464;
            font-family: Arial;
            font-size: 10px;
        }
    </IPAMmaster:CssBlock>
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext, sw-dns-manage.js" Orientation="jsInit"
        runat="server">
    $(document).ready(function(){
    var EnableZoneTransferClick = function(nsId, delay){
        var div = $('#divZoneTransfers');
        var checked =  $SW.ns$(nsId,'chEnableZoneTransfer')[0].checked;
        if(div) if(checked === true) div.show(delay); else div.hide(delay);
     }
    $SW.ns$($nsId,'chEnableZoneTransfer').click( function(){ EnableZoneTransferClick($nsId, 250); } );
    EnableZoneTransferClick($nsId, 0); 
    });
    </IPAMmaster:JsBlock>
    <orion:Include ID="Include1" File="breadcrumb.js" runat="server" />
    <IPAMui:ValidationIcons ID="ValidationIcons1" runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0" style="min-width: 260px;">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                           { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/zones" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto;
                                            height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                              { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div style="width: 1000px;">
            <asp:HiddenField ID="hdWebId" runat="server" />
            <orion:Progress ID="ProgressIndicator1" runat="server" />
            <div class="GroupBox" id="NormalText">
                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_802%></span>
                <br />
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_803%>
                <div class="contentBlock">
                    <table cellspacing="5" cellpadding="0" class="sw-form-wrapper">
                        <tr class="sw-form-cols-normal">
                            <td class="leftLabelColumn" style="vertical-align: top">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_804%>
                                </div>
                            </td>
                            <td class="sw-form-col-control">
                                <div class="sw-form-item ">
                                    <asp:TextBox ID="txtZoneFileName" runat="server" CssClass="x-form-text x-form-field" /></div>
                                <div class ="help-text">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_19 %> </div>
                            </td>
                            <td class="sw-form-col-comment">
                                <asp:RequiredFieldValidator ID="ZoneFileNameRequire" runat="server" ControlToValidate="txtZoneFileName"
                                    Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_805 %>" />
                            </td>
                        </tr>
                        <tr>
                            <td class="leftLabelColumn" style="vertical-align: top">
                                <div class="sw-field-label" style="margin-top: 10px;">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_806 %>
                                </div>
                            </td>
                            <td colspan="2">
                                <div style="padding-left: 10px; width: auto" class="blueBox">
                                    <div class="sw-form-item">
                                        <asp:CheckBox ID="chEnableZoneTransfer" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_807 %>" runat="server" CssClass="checkbox"
                                            Checked="true" /></div>
                                    <div id="divZoneTransfers">
                                        <table border="0" cellspacing="5" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="sw-field-label">
                                                        <asp:RadioButton ID="rdoDefaultZoneTransferInterval" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_808 %>" CssClass="checkbox"
                                                            runat="server" GroupName="ZoneTransfers" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="font-style: italic">
                                                        <%= string.Format(Resources.IPAMWebContent.IPAMWEBDATA_VB1_809, string.Format("<a id=\"A1\" href='/Orion/IPAM/Dns.Server.Edit.aspx?NoChrome=False&ObjectId={0}'target=\"_blank\" style=\"text-decoration: underline; color: Blue\">", this.ServerGroupId), "</a>")%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="sw-field-label">
                                                        <asp:RadioButton ID="rdoDNSZoneTransferInterval" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_810 %>" CssClass="checkbox"
                                                            runat="server" GroupName="ZoneTransfers" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <IPAM:TimeSpanEditor Width="168" id="ZoneTransferInterval" ActiveWhenChecked="False"
                                                        TextName="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_811 %>" runat="server" MinValue="00:10:00" MaxValue="7.00:00:00" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CheckBox ID="cbIncrementalZoneTransfer" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_812 %>" CssClass="checkbox"
                                                        Checked="true" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class ="help-text">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_18 %> </div>
                            </td>
                        </tr>
                    </table>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div>
                        <table width="100%">
                            <tr>
                                <td class="leftLabelColumn">
                                    &nbsp;
                                </td>
                                <td>
                                    <div class="sw-btn-bar-wizard">
                                        <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click"
                                            DisplayType="Secondary" LocalizedText="BACK" />
                                        <orion:LocalizableButton runat="server" ID="imgbNext" CausesValidation="true" OnClick="imgbNext_Click"
                                            DisplayType="Primary" LocalizedText="NEXT" />
                                        <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                                            OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="CANCEL" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true"
                        DisplayMode="BulletList" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
