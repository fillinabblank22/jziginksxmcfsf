﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Dns.Zone.MasterServer.ascx.cs" Inherits="DnsZone_MasterServer" %>

<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMmaster" Namespace="SolarWinds.IPAM.Web.Master" Assembly="SolarWinds.IPAM.Web.Master" %>

<IPAMmaster:JsBlock requires="ext,ext-quicktips,sw-dns-manage.js" Orientation="jsPostInit" runat="server" />

<script type="text/javascript">
if (!$SW.IPAM.DNSZoneMasters) $SW.IPAM.DNSZoneMasters = [];
$SW.IPAM.DNSZoneMasters['<%=this.ID %>'] = $SW.IPAM.DNSZoneServers({
    GridId: '<%=this.ServersGrid.ClientID %>',
    WebId: '<%=this.WebId %>'
});


$SW.IPAM.DNSZoneMasters['<%=this.ID %>'].grid_cellclick = function (grid, rowIndex, columnIndex, e) {
        $SW.ext.gridClickToDrilldown(grid, rowIndex, columnIndex, e);

        var be = e.browserEvent.target || e.browserEvent.srcElement;
        if (be.tagName.toUpperCase() == 'IMG')
            be = be.parentNode;

        if (be.tagName.toUpperCase() != 'A')
            return;

        var record = grid.getStore().getAt(rowIndex);
        

        if (/sw-grid-edit/.test(be.className))
            $SW.IPAM.DNSZoneMasters['<%=this.ID %>'].EditServer(be, record);

        if (/sw-grid-up/.test(be.className))
            $SW.IPAM.DNSZoneMasters['<%=this.ID %>'].ServerUp(be, record);

        if (/sw-grid-down/.test(be.className))
            $SW.IPAM.DNSZoneMasters['<%=this.ID %>'].ServerDown(be, record); 
            
        if (/sw-grid-delete/.test(be.className))
            $SW.IPAM.DNSZoneMasters['<%=this.ID %>'].ServerRemove(be, record); 
    }
</script>

<IPAMlayout:DialogWindow jsID="masterServerWindow"  Name="masterServerWindow" Url="~/Orion/IPAM/res/html/loading.htm" title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_779 %>" height="200" width="550" runat="server">
<Buttons>
    <IPAMui:ToolStripButton id="exclusionsSave" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_141 %>" cls="sw-enableonload" runat="server">
        <handler>function(){ $SW.msgq.DOWNSTREAM( $SW['masterServerWindow'], 'dialog', 'save' ); }</handler>
    </IPAMui:ToolStripButton>
    <IPAMui:ToolStripButton ID="exlusionsCancel" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_142 %>" runat="server">
        <handler>function(){ $SW['masterServerWindow'].hide(); }</handler>
    </IPAMui:ToolStripButton>
</Buttons>
<Msgs>
    <IPAMmaster:WindowMsg Name="ready" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['masterServerWindow'] ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg Name="unload" runat="server">
        <handler>function(n,o,info){ $SW.ext.dialogWindowEnableButtons( $SW['masterServerWindow'], true ); }</handler>
    </IPAMmaster:WindowMsg>
    <IPAMmaster:WindowMsg ID="closeMsg" Name="close" runat="server">
        <handler>
        function(n,o,info){ $SW['masterServerWindow'].hide(); 
            $SW.IPAM.DNSZoneMasters['SecondaryZoneMasterServer'].ServerRefresh(); 
            $SW.IPAM.DNSZoneMasters['StubZoneMasterServer'].ServerRefresh();}
         </handler>
    </IPAMmaster:WindowMsg>
</Msgs>
</IPAMlayout:DialogWindow>

<IPAMlayout:GridBox runat="server" ID="ServersGrid" title="" UseLoadMask="True" autoHeight="true" width="525"
    emptyText="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_776 %>" borderVisible="true" deferEmptyText="false" 
    SelectorName="selector" RowSelection="true">
    
    <TopMenu borderVisible="false">
        <IPAMui:ToolStripMenuItem id="btnAdd" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" iconCls="mnu-add" text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_777 %>" runat="server">
        </IPAMui:ToolStripMenuItem>         
    </TopMenu>
    <Columns>
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_81 %>" width="90" isSortable="true" runat="server" dataIndex="ID" />
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_778 %>" width="260" isSortable="true" runat="server" dataIndex="serverAddress" />        
        <IPAMlayout:GridColumn title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_372 %>" width="150" isFixed="true" isHideable="false" dataIndex="SubnetId" runat="server" renderer="$SW.IPAM.DnsMaster_renderer_buttons()" />
        
    </Columns>
    <Store id="store" dataRoot="rows" dataCount="count" autoLoad="true" proxyUrl="../sessiondataprovider.ashx"
        proxyEntity="MasterDnsServers" AmbientSort="ID"
        listeners="{ loadexception: $SW.ext.HttpProxyException }" runat="server">
        <Fields>
            <IPAMlayout:GridStoreField dataIndex="ID"    runat="server" />
            <IPAMlayout:GridStoreField dataIndex="serverAddress"  runat="server" />                      
       </Fields>
    </Store>
</IPAMlayout:GridBox>