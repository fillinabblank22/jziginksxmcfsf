<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="~/Orion/IPAM/DnsZones/Dns.Zone.Wizard.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DnsZonesWizard"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_764 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register Src="~/Orion/IPAM/DnsZones/ProgressIndicator.ascx" TagName="Progress"
    TagPrefix="orion" %>
<%@ Register TagPrefix="ipam" TagName="CustomPropertyEdit" Src="~/Orion/IPAM/Controls/Admin/CustomPropertyEdit.ascx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDNSZoneAdd" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true" ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx"
        runat="server" />
    <IPAMmaster:CssBlock runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
        .disabled-textbox { 
    background-color:#fff;
    background-image: url(/Orion/IPAM/res/images/sw/textBox-bg.gif);
    border-color:#b5b8c8;        
        }
    .leftLabelColumn {
        word-wrap: break-word;
    }
    </IPAMmaster:CssBlock>
    <IPAMmaster:JsBlock Requires="ext, sw-dns-manage.js" Orientation="jsPostInit" runat="server">
$(document).ready(function(){
  var serverId = $SW.nsGet($nsId, 'ddlDnsServers');
  var serverCb = $SW.TransformDropDown(serverId, serverId, { hiddenId: serverId } );
});
    </IPAMmaster:JsBlock>
    <orion:Include File="breadcrumb.js" runat="server" />
    <IPAMui:ValidationIcons runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0" style="min-width: 260px;">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                            { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/zones" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto; height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                                { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div style="width: 1000px;">
            <asp:hiddenfield id="hdWebId" runat="server" />
            <orion:Progress ID="ProgressIndicator1" runat="server" />
            <div class="GroupBox" id="NormalText">
                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_178%></span>
                <br />
                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_765 %>
                <div class="contentBlock">
                    <table class="sw-form-wrapper" cellspacing="5" cellpadding="0">

                        <tr>
                            <td class="leftLabelColumn">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_283 %>
                                </div>
                            </td>
                            <td>
                                <div class="sw-form-item">
                                    <asp:dropdownlist id="ddlDnsServers" runat="server" autopostback="true" />
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <br />
                    <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187%></span>
                    <br />
                    <div class="expander" header="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_187 %>">
                        <div class="group-box white-bg" style="margin-bottom: 0px">
                            <ipam:CustomPropertyEdit ID="CustomPropertyRepeater" runat="server" CustomPropertyObject="IPAM_GroupAttrData" />
                        </div>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div>
                        <table width="100%">
                            <tr>
                                <td class="leftLabelColumn">&nbsp;
                                </td>
                                <td>
                                    <div class="sw-btn-bar-wizard">
                                        <orion:LocalizableButton LocalizedText="NEXT" DisplayType="Primary" runat="server"
                                            ID="imgbNext" OnClick="imgbNext_Click" />
                                        <orion:LocalizableButton LocalizedText="CANCEL" DisplayType="Secondary" runat="server"
                                            ID="imgbCancel" CausesValidation="false" OnClick="imgbCancel_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <asp:validationsummary id="valSummary" runat="server" showsummary="false" showmessagebox="true"
            displaymode="BulletList" />
    </div>
</asp:Content>
