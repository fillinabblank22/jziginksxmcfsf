﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Master;

public partial class DnsZone_MasterServer : UserControl
{
    #region Properties

    private string _scopeIpRangeSupplyFn;

    

    public string WebId { get; set; }

    #endregion

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.ServersGrid.jsID = this.ServersGrid.ClientID;
        this.ServersGrid.listeners = string.Format(@"{{ cellclick: $SW.IPAM.DNSZoneMasters['{0}'].grid_cellclick,  render: function(){{ return $SW.IPAM.DNSZoneMasters['{0}'].SetWebId(); }} }}", this.ID);
        this.btnAdd.handler = string.Format(@"function(el){{ return $SW.IPAM.DNSZoneMasters['{0}'].AddServerBtnPressed(el); }}", this.ID);         
        this.closeMsg.handler = this.closeMsg.handler.Replace("%ID%", this.ID);
             
    }
}