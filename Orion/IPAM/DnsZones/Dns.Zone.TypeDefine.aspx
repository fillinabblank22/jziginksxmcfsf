﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/Orion/IPAM/DnsZones/Dns.Zone.TypeDefine.aspx.cs"
    MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" Inherits="SolarWinds.IPAM.WebSite.DnsZoneTypeDefine"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_764 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>
<%@ Register TagPrefix="IPAMlayout" Namespace="SolarWinds.IPAM.Web.Layout" Assembly="SolarWinds.IPAM.Web.Layout" %>
<%@ Register Src="~/Orion/IPAM/DnsZones/Dns.Zone.MasterServer.ascx" TagName="MasterServer"
    TagPrefix="orion" %>
<%@ Register Src="~/Orion/IPAM/DnsZones/ProgressIndicator.ascx" TagName="Progress"
    TagPrefix="orion" %>

<asp:content id="Content3" contentplaceholderid="TopRightPageLinks" runat="server">
    <IPAM:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDNSZoneAdd" />
</asp:content>

<asp:content id="Content1" contentplaceholderid="adminContentPlaceholder" runat="server">
    <IPAM:AccessCheck ID="AccessCheck1" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"
        ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    <IPAMmaster:CssBlock ID="CssBlock1" runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
        .disabled-textbox { 
            background-color:#fff;
            background-image: url(/Orion/IPAM/res/images/sw/textBox-bg.gif);
            border-color:#b5b8c8;        
        }
        .radiobutton label{
            padding-left:5px;
        }
        .HelpTextBanner{
            background-color: #FFFFCC;
            height: 30px;
            width: 670px;
        }
        .bulb{
            margin-left : 4px;
        }
    </IPAMmaster:CssBlock>
    <script language="javascript" type="text/javascript">
        $SW.IPAM.AddMode = '<%=AddMode %>' == 'True' ? true : false;
        $SW.IPAM.serverGroupId = '<%=ServerGroupId %>';
        $SW.IPAM.serverType = '<%=DnsServer.ServerType %>';
        $SW.IPAM.DnsScanningDisabled = '<%=(DnsServer.DisableAutoScanning == true)%>';
        $SW.IPAM.bind = '<%= SolarWinds.IPAM.BusinessObjects.DnsServerType.Bind %>';
    </script>
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,sw-ux-wait.js,sw-dialog.js" Orientation="jsInit"
        runat="server">
$(document).ready(function () {
    if($SW.IPAM.DnsScanningDisabled === "True") {
        Ext.Msg.show({
            title: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_24;E=js}',
            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_OH1_25;E=js}',
            buttons: Ext.Msg.OK, icon: Ext.MessageBox.WARNING
        });
    }
});

$SW.IPAM.OnZoneNameRequire = function(src,args){   
    if(!$SW.RadioChecked('rdForwardType')) return true;
    return args.IsValid = ($SW.ns$($nsId,'txtZoneName')[0].value != '');     
};

    $(document).ready(function(){
    
    var viewId = $SW.nsGet($nsId, 'ddlDnsViews');
    var viewCb = $SW.TransformDropDown(viewId, viewId, { hiddenId: viewId } );

    var EnableZoneTypeClick = function(nsId, delay){
        var div = $('#divPrimaryZoneAD');
        var checked =  $SW.ns$(nsId,'rdPrimaryZone')[0].checked;
        if(div) if(checked === true) div.show(delay); else div.hide(delay);
        div = $('#divSecondaryZoneMasterServer');
        checked =  $SW.ns$(nsId,'rdSecondaryZone')[0].checked;
        if(div) if(checked === true) div.show(delay); else div.hide(delay);
        div = $('#divStubZoneMasterServer');
        checked =  $SW.ns$(nsId,'rdStubZone')[0].checked;
        if(div) if(checked === true) div.show(delay); else div.hide(delay);
    }
    $SW.ns$($nsId,'rdPrimaryZone').click( function(){ EnableZoneTypeClick($nsId, 250); } );
    $SW.ns$($nsId,'rdSecondaryZone').click( function(){ EnableZoneTypeClick($nsId, 250); } );
    $SW.ns$($nsId,'rdStubZone').click( function(){ EnableZoneTypeClick($nsId, 250); } );       
    EnableZoneTypeClick($nsId, 0); 

     $SW.RadioChecked = function(id){
        var radio=$SW.ns$($nsId,id)[0];
        return (radio && radio.checked);
    };

    var ToggleReverseZoneControls = function(nsId){
        if(!$SW.IPAM.AddMode) return; 
        if ($SW.RadioChecked('rdNetworkID'))  {        
            $SW.ns$($nsId,'txtNetworkID')[0].disabled = false;
            $SW.ns$($nsId,'txtReverseName')[0].disabled = true;
            $SW.ns$($nsId,'txtReverseName')[0].value = "";
            }
            else{
            $SW.ns$($nsId,'txtReverseName')[0].disabled = false;
            $SW.ns$($nsId,'txtNetworkID')[0].disabled = true;
            $SW.ns$($nsId,'txtNetworkID')[0].value = "";
        }
    };
                          
   var EnableLookupTypeClick = function(nsId, delay){
        var div = $('#divForwardTypeName');
        var checked =  $SW.ns$(nsId,'rdForwardType')[0].checked;
        var regVal = $SW.ns$($nsId,'regZoneName')[0];
        var reqVal = $SW.ns$($nsId,'reqZonename')[0];
        var arpaVal = $SW.ns$($nsId,'valZoneName')[0];                
        if(div) 
            if(checked === true) 
                div.show(delay); 
            else{
                $SW.ns$($nsId,'txtZoneName')[0].value = '';
                ValidatorEnable(regVal,false);
                ValidatorEnable(reqVal,false);
                ValidatorEnable(arpaVal,false);
                div.hide(delay);
            }
        div = $('#divReverseTypeName');
        checked =  $SW.ns$(nsId,'rdReverseType')[0].checked;
        if(div) 
            if(checked === true) {
                div.show(delay); 
                ToggleReverseZoneControls($nsId);
            }
            else{
                regVal.enabled = true;
                ValidatorUpdateDisplay(regVal);
                reqVal.enabled = true;
                ValidatorUpdateDisplay(reqVal);
                arpaVal.enabled = true;
                ValidatorUpdateDisplay(arpaVal);
                div.hide(delay);
            }       
     }
    $SW.ns$($nsId,'rdForwardType').click( function(){ EnableLookupTypeClick($nsId, 250); } );
    $SW.ns$($nsId,'rdReverseType').click( function(){ EnableLookupTypeClick($nsId, 250); } );
    EnableLookupTypeClick($nsId, 0);
   
   
     
     $SW.IPAM.OnNetworkIDRequire = function(src,args){
        if(!($SW.RadioChecked('rdNetworkID') && $SW.RadioChecked('rdReverseType')) ) return true; 
        return args.IsValid =  ($SW.ns$($nsId,'txtNetworkID')[0].value != '');
    };

     $SW.IPAM.OnReverseNameRequire = function(src,args){
        if(!($SW.RadioChecked('rdReverseName') && $SW.RadioChecked('rdReverseType')) ) return true; 
        return args.IsValid = ($SW.ns$($nsId,'txtReverseName')[0].value != '');
    };

    $SW.IPAM.OnReverseNameValidate = function(src,args){
        if(!($SW.RadioChecked('rdReverseName') && $SW.RadioChecked('rdReverseType')) ) return true; 
         return args.IsValid = $SW.IPAM.CheckReverseName();
    };
    $SW.IPAM.OnNetworkIDValidate = function(src,args){
        if(!($SW.RadioChecked('rdNetworkID') && $SW.RadioChecked('rdReverseType')) ) return true; 
        return args.IsValid = ipv4subset_isok(ipv4subset(args.Value));
    };

    $SW.IPAM.OnZoneNameValid =  function(src,args){
        if(!($SW.RadioChecked('rdForwardType') )) return true;
        var zoneName = $SW.ns$($nsId,'txtZoneName')[0].value;
        if(zoneName.length > 4){             
            return  args.IsValid = !(zoneName.match(/.arpa$/) || zoneName.match(/.arpa.$/)) ;
        }
        else return  true;  
    };

    function ipv4subset(v) {
        if (/[^0-9. ]/.test(v)) v = '';
        return jQuery.map(v.split('.'), function(n, i) {
            var r = parseInt(n, 10);
            return (isNaN(r) || r < 0 || r > 255) ? -1 : r;
        });
    };

    function ipv4subset_isok(a) { return (a.length < 4) && (jQuery.inArray(-1, a) < 0); }
   
    $SW.IPAM.CheckReverseName = function()
    {        
        var reverseName = $SW.ns$($nsId,'txtReverseName')[0].value;
        return (reverseName.indexOf("in-addr.arpa") > -1);
    };

    $SW.IPAM.PageValidate = function (me)
    {
        if($SW.RadioChecked('rdSecondaryZone') ||  $SW.RadioChecked('rdStubZone')) 
        {
                if(!$SW.IPAM.IsMasterServersAvailable())
                {
                    Ext.Msg.show({
                                    title: "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_314;E=js}",
                                    msg:  "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_315;E=js}",
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.MessageBox.WARNING,
                                    fn: function() {  }
                                });
                                return false;
                 }
         }
         if($SW.IPAMValid.ZoneType == 1 && $SW.IPAMValid.DsIntegreated == 'True' && !$SW.RadioChecked('rdPrimaryZone'))
            {
                me.name = me.id.replace(/_/g,"$");
                Ext.Msg.show({                       
                                title: '@{R=IPAM.Strings;K=IPAMWEBJS_JI_7;E=js}',
                                msg: '@{R=IPAM.Strings;K=IPAMWEBJS_JI_8;E=js}',
                                buttons: Ext.Msg.YESNO,
                                icon: Ext.MessageBox.WARNING,
                                fn: function(btn){
                                        if( btn == 'yes' )
                                        {                                          
                                            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(me.name, "", true, "", "", false, true));                                          
                                            return true;                         
                                        }
                                    }
                                });
                            return false;                   
             }
    };
     $SW.IPAM.IsMasterServersAvailable = function() {
      var isValid =false;
       
         $.ajax({
                type: "POST",
                url:  $SW.appRoot() + "Orion/IPAM/sessiondataprovider.ashx",
                data: {
                    entity:'MasterDnsServers',
                    verb:'Count',
                    w: $SW.IPAMValid.WebId
                },
                async: false,
                success: function(r) {
                  var ret = Ext.util.JSON.decode(r); 
                  isValid = (ret.data.Count > 0); }
            });
            return isValid; 
     };
   
    $SW.ns$($nsId,'rdNetworkID').click( function(){  ToggleReverseZoneControls($nsId); } );
    $SW.ns$($nsId,'rdReverseName').click( function(){ ToggleReverseZoneControls($nsId);   } );

    var err = function () {
        $SW.ns$($nsId,'chPrimaryZoneAD')[0].disabled = true;   
        $('#divADIntegrated').hide(0);
    }    
    if($SW.IPAM.AddMode && $SW.IPAM.serverType != $SW.IPAM.bind){
        $('#divADIntegrated').show(0);
        $.ajax({
            type: "POST",
            url: $SW.appRoot() + "Orion/IPAM/ExtCmdProvider.ashx", 
            async: true,
            data: {
                entity: 'IPAM.DnsServer',
                verb: 'GetADStatus',
                GroupId: $SW.IPAM.serverGroupId
                },
            success: function(r) {
                try
                {
                        var res = Ext.util.JSON.decode(r);                 
                            $SW.ns$($nsId,'chPrimaryZoneAD')[0].disabled = !res.data["IsDominMachine"];
                            $('#divADIntegrated').hide(0);                               
                }
                catch(e)
                {
                    err();
                }
                }, 
            failure: err
            });               
    } 
}); 
    </IPAMmaster:JsBlock>
    <script type="text/javascript" language="javascript">
        if (!$SW.IPAMValid) $SW.IPAMValid = {};
        $SW.IPAMValid.WebId = '<%=this.WebParamId %>';
        $SW.IPAMValid.ZoneType = '<%=this.ZoneType %>';
        $SW.IPAMValid.DsIntegreated = '<%=this.IsDsIntegreated %>';       
    </script>
    <orion:Include ID="Include1" File="breadcrumb.js" runat="server" />
    <IPAMui:ValidationIcons ID="ValidationIcons" runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0" style="min-width: 260px;">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                           { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/zones" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto;
                                            height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                              { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div style="width: 1000px;">
            <asp:HiddenField ID="hdWebId" runat="server" />
            <orion:Progress ID="ProgressIndicator1" runat="server" />
            <div class="GroupBox" id="NormalText">    
                    <div id ="dnsViewDiv" runat ="server">
                        <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_6 %></span>
                        <br />
                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_7 %>
                        <table class ="HelpTextBanner">
                        <tr>
                            <td>
                                <img src="../res/images/sw/icon.lightbulb.small.gif" class="bulb" />
                            </td>
                            <td> <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_9%> </td>
                        </tr>
                        </table>
                        <div class="contentBlock">
                            <table cellspacing="5" cellpadding="0">
                                <tr>
                                    <td class="leftLabelColumn">
                                        <div class="sw-field-label">
                                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_8 %>
                                        </div>
                                    </td>
                                    <td>
                                        <div style="padding-left: 15px; " class="sw-form-item">
                                            <asp:DropDownList ID="ddlDnsViews" runat="server"/>                                        
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div> 
                 <div>
                        <!-- ie6 -->
                    </div>
                    <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_766 %></span>
                    <br />
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_767 %>
                    <div class="contentBlock">
                    <table cellspacing="5" cellpadding="0">
                        <tr>
                            <td class="leftLabelColumn" style="vertical-align: top">
                                <div class="sw-field-label">
                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_768 %>
                                </div>
                            </td>
                            <td>
                                <div style="margin-left: -2px;">
                                    <asp:RadioButton ID="rdPrimaryZone" Checked="true" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_770 %>"
                                        GroupName="zonetype" CssClass="radiobutton"/>
                                    <div style="font-family: Arial; font-size: 10px; color: #646464; padding-left: 17px">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_769 %>
                                        <div id="divPrimaryZoneAD" runat ="server">
                                            <div style="padding-left: 10px; width: 650px; color: black;" class="blueBox">
                                                <asp:CheckBox ID="chPrimaryZoneAD" runat="server" CssClass="radiobutton" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_771 %>" />
                                                <div style="display: inline;"><span style="display: none;" id="divADIntegrated"  >
                                                    &nbsp;<img style="height: 15px" src="../../IPAM/res/images/sw/animated_loading_sm3_whbg.gif" />
                                                    <span style="font-weight: bold;">
                                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources: CoreWebContent, WEBDATA_IB0_8 %>" /></span>
                                                </span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="margin-left: -2px;">
                                    <asp:RadioButton ID="rdSecondaryZone" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_772 %>" GroupName="zonetype" CssClass="radiobutton"/>
                                    <div style="font-family: Arial; font-size: 10px; color: #646464; padding-left: 17px">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_773 %>
                                        <div style="padding-left: 10px; width: 650px; color: black;" class="blueBox" id="divSecondaryZoneMasterServer">
                                            <orion:MasterServer ID="SecondaryZoneMasterServer" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div  runat="server" id="stubDiv" style="margin-left: -2px;">
                                    <asp:RadioButton ID="rdStubZone" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_774 %>" GroupName="zonetype" CssClass="radiobutton" />
                                    <div style="font-family: Arial; font-size: 10px; color: #646464; padding-left: 17px">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_775 %>
                                        <div style="padding-left: 10px; width: 650px; color: black;" class="blueBox" id="divStubZoneMasterServer">
                                            <div class="sw-form-item" style="width: 300px">
                                                <orion:MasterServer ID="StubZoneMasterServer" runat="server" CssClass="x-form-text x-form-field" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>                   
                    <div>
                        <!-- ie6 -->
                    </div>
                    <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_780 %></span>
                    <br />
                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_781 %>
                    <div class="contentBlock">
                        <table cellspacing="5" cellpadding="0">
                            <tr>
                                <td class="leftLabelColumn" style="vertical-align: top">
                                    <div class="sw-field-label">
                                        <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_782 %>
                                    </div>
                                </td>
                                <td>
                                    <div style="margin-left: -2px;">
                                        <asp:RadioButton ID="rdForwardType" Checked="true" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_783 %>" CssClass="radiobutton"
                                            GroupName="lookuptype" />
                                        <div style="font-family: Arial; font-size: 10px; color: #646464; padding-left: 17px">
                                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_784%>
                                            <div style="padding-left: 10px; width: 650px; color: black;" class="blueBox" id="divForwardTypeName">
                                                <table class="sw-form-wrapper" style="table-layout: auto">
                                                    <tr>
                                                        <td width="220px" align="right">
                                                            <div class="sw-field-label" style="padding-right: 30px;">
                                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_785%>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="sw-form-item" style="width: 300px">
                                                                <asp:TextBox ID="txtZoneName" runat="server" CssClass="x-form-text x-form-field" /></div>
                                                        </td>
                                                        <td>
                                                            <asp:CustomValidator ID="reqZonename" runat="server" ClientValidationFunction="$SW.IPAM.OnZoneNameRequire"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_786 %>" ValidateEmptyText="true"
                                                                ControlToValidate="txtZoneName" /><asp:RegularExpressionValidator runat="server"
                                                                    ID="regZoneName" ControlToValidate="txtZoneName" ValidationExpression="[\w-_.]*"
                                                                    Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_787 %>" ValidateEmptyText="true" /><asp:CustomValidator
                                                                        runat="server" ClientValidationFunction="$SW.IPAM.OnZoneNameValid" Display="Dynamic"
                                                                        ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_788 %>" ValidateEmptyText="true" ControlToValidate="txtZoneName"
                                                                        ID="valZoneName" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div style="margin-left: -2px;">
                                        <asp:RadioButton ID="rdReverseType" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_789 %>" CssClass="radiobutton" GroupName="lookuptype" />
                                        <div style="font-family: Arial; font-size: 10px; color: #646464; padding-left: 17px">
                                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_790 %>
                                            <div style="padding-left: 10px; width: 650px; color: black;" class="blueBox" id="divReverseTypeName">
                                                <table class="sw-form-wrapper">
                                                    <tr>
                                                        <td width="220px" class="sw-form-col-label">
                                                            <div style="padding-left: 10px;">
                                                                <asp:RadioButton ID="rdNetworkID" Checked="true" runat="server" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_791 %>" CssClass="radiobutton"
                                                                    GroupName="reverseName" />
                                                            </div>
                                                        </td>
                                                        <td class="sw-form-col-control">
                                                            <div class="sw-form-item" style="width: 300px">
                                                                <asp:TextBox ID="txtNetworkID" runat="server" CssClass="x-form-text x-form-field" /></div>
                                                        </td>
                                                        <td class="sw-form-col-comment">
                                                            <asp:CustomValidator ID="reqNetworkID" runat="server" ClientValidationFunction="$SW.IPAM.OnNetworkIDRequire"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_792 %>" ValidateEmptyText="true"
                                                                ControlToValidate="txtNetworkID" /><asp:CustomValidator ID="valNetworkID" runat="server"
                                                                    ClientValidationFunction="$SW.IPAM.OnNetworkIDValidate" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_793 %>"
                                                                    ValidateEmptyText="true" ControlToValidate="txtNetworkID" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="220px" class="sw-form-col-label">
                                                            <div style="padding-left: 10px;">
                                                                <asp:RadioButton ID="rdReverseName" runat="server" CssClass="radiobutton" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_794 %>"
                                                                    GroupName="reverseName" /></div>
                                                        </td>
                                                        <td class="sw-form-col-control">
                                                            <div class="sw-form-item" style="width: 300px">
                                                                <asp:TextBox ID="txtReverseName" runat="server" CssClass="x-form-text x-form-field" />
                                                            </div>
                                                        </td>
                                                        <td class="sw-form-col-comment">
                                                            <asp:CustomValidator ID="reqReverseName" runat="server" ClientValidationFunction="$SW.IPAM.OnReverseNameRequire"
                                                                Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_795 %>" ValidateEmptyText="true"
                                                                ControlToValidate="txtReverseName" /><asp:CustomValidator ID="valReverseName" runat="server"
                                                                    ClientValidationFunction="$SW.IPAM.OnReverseNameValidate" Display="Dynamic" ErrorMessage="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_796 %>"
                                                                    ValidateEmptyText="true" ControlToValidate="txtReverseName" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <!-- ie6 -->
                    </div>
                    <div>
                        <table width="100%">
                            <tr>
                                <td class="leftLabelColumn">
                                    &nbsp;
                                </td>
                                <td>
                                    <div class="sw-btn-bar-wizard">
                                        <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click"
                                            DisplayType="Secondary" LocalizedText="BACK" />                    
                                        <orion:LocalizableButton runat="server" ID="imgbNext" OnClientClick="return $SW.IPAM.PageValidate(this);" LocalizedText="NEXT" DisplayType="Primary" />
                                        <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                                            OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="CANCEL" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true"
            DisplayMode="BulletList" />
    </div>
</asp:content>
