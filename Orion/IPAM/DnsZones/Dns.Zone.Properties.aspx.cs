﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Web.Common.Helpers;
using SolarWinds.Orion.Common;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Globalization;
using SolarWinds.IPAM.BusinessObjects.DNSManagement;
using SolarWinds.IPAM.DNSMultiDevice.Factory;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DnsZoneProperties : CommonPageServices
    {
        #region Declarations

        public int ServerGroupId { get; set; }
        private PageDnsZoneEditor Editor { get; set; }

        #endregion // Declarations

        #region Constructors

        public DnsZoneProperties()
        {
            this.Editor = new PageDnsZoneEditor(this);
        }

        #endregion // Constructors

        #region Event Handlers

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ProgressIndicator1.CurrentStep = DnsZoneWizardStep.Properties;

            if (!IsPostBack)
            {
                InitControls();
            }
            var webId = GetParam(DnsSessionStorage.PARAM_WEBID, null);

            Title = Editor.Session_IsAddMode(null) ? Resources.IPAMWebContent.IPAMWEBCODE_VB1_28 : Resources.IPAMWebContent.IPAMWEBCODE_VB1_29;
            btnHelp.HelpUrlFragment = Editor.Session_IsAddMode(null) ? "OrionIPAMPHDNSZoneAdd" : "OrionIPAMAGEditDNSZone";
        }

        protected void imgbNext_Click(object sender, EventArgs e)
        {
            Next();
        }

        protected void imgbPrev_Click(object sender, EventArgs e)
        {
            string url = Editor.AddWebIdParam(ProgressIndicator1.PrevStep, null);
            Response.Redirect(url, true);
        }

        protected void imgbCancel_Click(object sender, EventArgs e)
        {
            string webId = this.hdWebId.Value;
            Editor.Session_Clear(webId);
            Response.Redirect(ProgressIndicator1.HomePage);
        }

        #endregion // Event Handlers

        private void InitControls()
        {

            DnsZone zone;

            Editor.Session_RetrieveZoneInfo(null, out zone);
            bool isAddMode = Editor.Session_IsAddMode(null);
            this.ServerGroupId = zone.ParentId;

            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            var isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);

            if (isCapableOfViewManagement)
                cbIncrementalZoneTransfer.Style["display"] = "none";
            else
                cbIncrementalZoneTransfer.Style["display"] = "block";

            if (!isAddMode && !string.IsNullOrEmpty(zone.DataFileName))
                txtZoneFileName.Text = zone.DataFileName;
            else if (!string.IsNullOrEmpty(zone.FriendlyName))
                txtZoneFileName.Text = zone.FriendlyName + ".dns";

            if (zone.ZoneType == DnsZoneType.Stub)
            {
                txtZoneFileName.Enabled = isAddMode;
                chEnableZoneTransfer.Checked = chEnableZoneTransfer.Enabled = false;
            }
            else
            {
                bool disabled = false;
                if (zone.DisableAutoScanning.HasValue)
                    disabled = zone.DisableAutoScanning.Value;


                chEnableZoneTransfer.Checked = !disabled;

                if (zone.ScanInterval > 0)
                {
                    rdoDNSZoneTransferInterval.Checked = true;
                    ZoneTransferInterval.Value = TimeSpan.FromMinutes(zone.ScanInterval.Value);
                }
                else
                {
                    rdoDefaultZoneTransferInterval.Checked = true;
                }

                cbIncrementalZoneTransfer.Checked = zone.IncrementalZoneTransfer;
            }
            if (zone.ZoneType == DnsZoneType.Primary)
            {
                txtZoneFileName.Enabled = !zone.DsIntegrated;
            }

            Editor.ActivateProgressStep(null, PageDnsZoneEditor.IPAM_DNSZONE_PROPERTIES);
        }

        private void Next()
        {
            if (!ValidateUserInput())
                return;

            DnsZone zone;
            Editor.Session_RetrieveZoneInfo(null, out zone);

            zone.DataFileName = txtZoneFileName.Text;
            var dnsServerType = Editor.Session_RetrieveServerType(null);
            var factory = DnsFactory.Instance.GetDeviceTypeFactory(dnsServerType);
            var managerType = factory.GetDnsManagerType();
            var isCapableOfViewManagement = typeof(IViewManagement).IsAssignableFrom(managerType);

            if (chEnableZoneTransfer != null)
            {
                zone.DisableAutoScanning = !chEnableZoneTransfer.Checked;

                if (chEnableZoneTransfer.Checked)
                {
                    if (rdoDefaultZoneTransferInterval != null && rdoDefaultZoneTransferInterval.Checked)
                        zone.ScanInterval = null;
                    if (rdoDNSZoneTransferInterval != null && rdoDNSZoneTransferInterval.Checked)
                        zone.ScanInterval = Convert.ToInt32(Math.Round(this.ZoneTransferInterval.Value.TotalMinutes));
                    if (cbIncrementalZoneTransfer != null)
                        zone.IncrementalZoneTransfer = cbIncrementalZoneTransfer.Checked;

                    ServerGroupId = Editor.Session_RetrieveServerGroupId(null);

                    if (isCapableOfViewManagement)
                         zone.IncrementalZoneTransfer = false;
                }
            }


            Editor.Sesssion_UpdateZoneInfo(null, zone);

            string url = Editor.AddWebIdParam(ProgressIndicator1.NextStep, null);
            Response.Redirect(url, true);
        }


        private bool ValidateUserInput()
        {
            Page.Validate();
            return Page.IsValid;
        }
    }
}
