﻿<%@ Page Language="C#" MasterPageFile="~/Orion/IPAM/Admin/IPAMAdmin.master" AutoEventWireup="true"
    CodeFile="Dns.Zone.Review.aspx.cs" Inherits="SolarWinds.IPAM.WebSite.DnsZoneReview"
    Title="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_764 %>" %>

<%@ Register TagPrefix="IPAMmaster" Assembly="SolarWinds.IPAM.Web.Master" Namespace="SolarWinds.IPAM.Web.Master" %>
<%@ Register TagPrefix="ipam" Namespace="SolarWinds.IPAM.Web.Common.Controls" Assembly="SolarWinds.IPAM.Web.Common" %>
<%@ Register TagPrefix="ipam" Src="~/Orion/IPAM/Controls/IconHelpButton.ascx" TagName="IconHelpButton" %>
<%@ Register Src="~/Orion/IPAM/DnsZones/ProgressIndicator.ascx" TagName="Progress"
    TagPrefix="orion" %>
<%@ Register TagPrefix="IPAMui" Namespace="SolarWinds.IPAM.Web.Widgets" Assembly="SolarWinds.IPAM.Web.Widgets" %>

<asp:Content ID="Content3" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <ipam:IconHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionIPAMPHDNSZoneAdd" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="adminContentPlaceholder" runat="server">
    <ipam:AccessCheck ID="AccessCheck1" RoleAclOneOf="IPAM.PowerUser" ExceptDemoMode="true"
        ErrorPage="/orion/ipam/errorpages/error.accessdenied.aspx" runat="server" />
    <IPAMmaster:CssBlock ID="CssBlock1" runat="server" Requires="~/Orion/styles/NodeMNG.css,~/Orion/styles/Admin.css,~/Orion/styles/Breadcrumb.css">
.tablediv {display: table;}
.rowdiv  { width:850px;display: table-row;}
.summaryHeader {  font-size: small;font-weight: bold; width:250px;display: table-cell;float:left;padding-top: 10px;}
.summaryData{    font-size: small;font-weight: normal; width:600px;display: table-cell;float:left;padding-top: 10px;}
.dnsLabels{   display:none; list-style-type:circle; }
    </IPAMmaster:CssBlock>
    <orion:Include ID="Include1" File="breadcrumb.js" runat="server" />
    <IPAMmaster:JsBlock ID="JsBlock1" Requires="ext,ext-quicktips,sw-ux-wait.js" Orientation="jsInit"
        runat="server">
 $(document).ready(function(){ if($SW.TaskId && $SW.TaskId != ''){      
    var title = $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_316;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_317;E=js}';
    var ErrMsgTpl = new Ext.XTemplate(
        '{message:this.toHtml}',
        '<tpl if="this.hasErrs(results)">',
            '<br /><hr>',
                '<tpl for="results"><tpl if="this.isValid(ErrorCode)">',
                    '<br>■ &ensp; <b>{Method}</b>{ErrorMessage}',
                '</tpl></tpl>',
        '</tpl>',
        {
            toHtml: function(msg){return Ext.util.Format.htmlEncode(msg); },
            hasErrs: function(rs){return (rs && rs.length > 0); },
            isValid: function(ec){return (ec != undefined && ec != 0); }
        }
    );
    
    var WarnMsgTpl = new Ext.XTemplate(
        '{message:this.toHtml}',
        '<br /><hr>',
            '<tpl for="results"><tpl if="!Success">',
                '<tpl if="this.isSubnetDelayOffer(Method)">',
                    '<br>■ &ensp;' + String.format('@{R=IPAM.Strings;K=IPAMWEBJS_VB1_393;E=js}', '<b>', '</b>'),
                '</tpl>',
            '</tpl></tpl>',
        {
            toHtml: function(msg){return Ext.util.Format.htmlEncode(msg); },
            isSubnetDelayOffer: function(method) {return method === "SetSubnetDelayOffer";}
        }
    );

    $SW.Tasks.Attach($SW.TaskId,
    { title: $SW.AddMode ? "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_318;E=js}" : "@{R=IPAM.Strings;K=IPAMWEBJS_VB1_319;E=js}" },
    function(d) {
        if (d.success){
				var warning = false;
                var notReady = false
                if (d.results) {
                    if(d.results === "NotReady"){
                        notReady = true;
                    }
                    else{
                       $.each(d.results, function(i, result) {
                            warning = result.Success === false;
                        });
                    }
                    if (d.results.LicenseInfo) {
                        var licInfo = Ext.util.JSON.decode(d.results.LicenseInfo);
                        $SW.LicenseListeners.recv(licInfo);
                    }
                }
                if(notReady) {
                     Ext.Msg.show({
                            title: title,
                            msg: '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_404;E=js}',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO,
                        });
                }
                else if (warning) {
                    d.message = $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_320;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_321;E=js}'
                    Ext.Msg.show({
                            title: title,
                            msg: WarnMsgTpl.apply(d),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.WARNING,
                            fn: function() {  window.location = "/api2/ipam/ui/zones"; }
                        });
                }
                else {
                    Ext.Msg.show({
                            title: title,
                            msg: $SW.AddMode ? '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_322;E=js}' : '@{R=IPAM.Strings;K=IPAMWEBJS_VB1_323;E=js}',
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.INFO ,
                            fn: function() {  window.location = "/api2/ipam/ui/zones"; }
                        });
                }
            } else Ext.MessageBox.show({
                title: title,
                msg: (d.results == null) ? d.message : d.results,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR                 
            });
        });
}});
    </IPAMmaster:JsBlock>
    <script language="javascript" type="text/javascript">
        $SW.TaskId = '<%=TaskId %>';
        $SW.AddMode = '<%=AddMode %>' == 'True' ? true : false;
    </script>
    <IPAMui:ValidationIcons ID="ValidationIcons1" runat="server" />
    <div style="margin: 10px 0px 0px 10px;">
        <div>
            <table width="auto" id="sw-navhdr" cellpadding="0" cellspacing="0" style="min-width: 260px;">
                <tr>
                    <td>
                        <% if (!CommonWebHelper.IsBreadcrumbsDisabled)
                           { %>
                        <div style="margin-top: 10px;">
                            <ul class="breadcrumb">
                                <li class="bc_item"><a href="/api2/ipam/ui/zones" title="<%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%>"
                                    class="bc_link"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a><img src="/Orion/images/breadcrumb_arrow.gif"
                                        class="bc_itemimage" /><ul id="bc-submenu-root" class="bc_submenu" style="overflow: auto;
                                            height: auto;">
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/IPAMSummaryView.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_AK1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Subnets.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_3%></a></li>
                                            <li class="bc_submenuitem"><a href="/api2/ipam/ui/dhcp" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_4%></a></li>
                                            <%if (SolarWinds.IPAM.Web.Common.Utility.AuthorizationHelper.IsUserInRole(SolarWinds.IPAM.Common.Security.AccountRole.PowerUser))
                                              { %>
                                            <li class="bc_submenuitem"><a href="/Orion/IPAM/Admin/Admin.Overview.aspx" class="bc_submenulink">
                                                <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_5%></a></li>
                                            <% } %>
                                        </ul>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                        <h1 style="margin: 10px 0;">
                            <%=Page.Title%></h1>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <!-- ie6 -->
        </div>
        <div>
            <table width="1000px" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <orion:Progress ID="ProgressIndicator1" runat="server" />
                        <div class="GroupBox" id="NormalText">
                            <div>
                                <span class="ActionName"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_520 %> </span>
                                <br />
                            </div>
                            <div class="contentBlock">
                                <div class="tablediv" style="margin-left: 10px;">
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_814 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DNSServerLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv" id ="DnsViewDiv" runat ="server">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VV0_8 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DnsViewNameLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_815 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DNSZoneNameLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_768 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DNSZoneTypeLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_782 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="DNSLookupTypeLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_816 %></span> <span class="summaryData">
                                            <asp:Label runat="server" ID="ZoneFileNameLabel"></asp:Label></span>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div>
                                        <div class="rowdiv" id="dvMasterDnsServers" runat="server">
                                            <span class="summaryHeader"><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_817 %></span> <span class="summaryData">
                                                <asp:Repeater runat="server" ID="contentsRepeater">
                                                    <ItemTemplate>
                                                        <li style="list-style-type: none; *position: relative; *left: -18px;">
                                                            <%# DataBinder.Eval(Container.DataItem, "Value")%>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </span>
                                        </div>
                                    </div>
                                    <div>
                                        <!-- ie6 -->
                                    </div>
                                    <div class="rowdiv">
                                        <span class="summaryHeader"><span id="EnableZoneTransferLabel" runat="server" class="dnsLabels">
                                            <img style="border-width: 0px;" src="/Orion/IPAM/res/images/sw/Check.Green.gif" />
                                            <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_818 %></span></span>
                                        <div class="summaryData">
                                            <ul>
                                                <li runat="server" id="DefaultzoneTransferLabel" class="dnsLabels" style="margin-left: -3px">
                                                    <img style="border-width: 0px; vertical-align: middle" height="12px" src="/Orion/IPAM/res/images/sw/icon-bullet.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_819 %></li>
                                                <li runat="server" id="DefinedzoneTransferLabel" class="dnsLabels" style="margin-left: -3px">
                                                    <img style="border-width: 0px; vertical-align: middle" height="12px" src="/Orion/IPAM/res/images/sw/icon-bullet.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_820 %>
                                                    <asp:Label runat="server" ID="transferInterval"></asp:Label>
                                                    <%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_821 %></li>
                                                <li runat="server" id="PreferIncrementalLabel" class="dnsLabels" style="margin-left: -3px">
                                                    <img style="border-width: 0px; vertical-align: middle" height="12px" src="/Orion/IPAM/res/images/sw/icon-bullet.gif" /><%= Resources.IPAMWebContent.IPAMWEBDATA_VB1_822 %> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td class="leftLabelColumn">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <div class="sw-btn-bar-wizard">
                                                    <orion:LocalizableButton runat="server" ID="imgbPrev" CausesValidation="false" OnClick="imgbPrev_Click"
                                                        DisplayType="Secondary" LocalizedText="Back" />
                                                    <orion:LocalizableButton runat="server" ID="imgbNext" CausesValidation="true" OnClick="imgbNext_Click"
                                                        DisplayType="Primary" Text="<%$ Resources : IPAMWebContent, IPAMWEBDATA_VB1_823 %>" />
                                                    <orion:LocalizableButton runat="server" ID="imgbCancel" CausesValidation="false"
                                                        OnClick="imgbCancel_Click" DisplayType="Secondary" LocalizedText="Cancel" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
