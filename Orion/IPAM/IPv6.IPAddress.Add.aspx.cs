﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;

public partial class IPv6AddressAdd : CommonPageServices
{
    #region Constructors

    private string IPNodeStatus
    {
        get { return ddlStatus.SelectedValue; }
    }

    private PageIPv6AddressEditor Editor { get; set; }
    public IPv6AddressAdd()
    {
        this.Editor = new PageIPv6AddressEditor(this, true);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnInit(EventArgs e)
    {
        this.Editor.InitPage();
        this.AccessCheck.GroupIds.Add(this.Editor.SubnetId);
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // init subnet object
        IPNode address = this.Editor.RetrieveAddressObject();

                // subnet and address
        int subnetId = this.Editor.SubnetId;
        if (this.AddressEditor != null)
        {
            GroupNode subnet = PageIPv6EditorHelper.LoadSubnetObject(subnetId, false);
            this.AddressEditor.Init(subnet.Address, subnet);
        }

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            InitControls(address);
        }
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(IPNode address)
    {
        // drop down lists
        if (this.ddlScanning != null)
        {
            ddlScanning.SelectedValue = (address.SkipScan ? "0" : "1");
        }
        if (this.ddlStatus != null)
        {
            this.Editor.SetStatusList(this.ddlStatus, address.Status);
        }
        if (this.ddlType != null)
        {
            this.Editor.SetTypeList(this.ddlType, address.AllocPolicy);
        }

        // text values
        SetTextValue(this.txtAlias, address.Alias);
        SetTextValue(this.txtDNS, address.DnsBackward);
        SetTextValue(this.txtDhcpClientName, address.DhcpClientName);
        SetTextValue(this.txtMAC, address.MAC);
        SetTextValue(this.txtComment, address.Comments);
        SetTextValue(this.txtMachineType, address.MachineType);
        SetTextValue(this.txtVendor, address.Vendor);
        SetTextValue(this.txtSysName, address.sysName);
        SetTextValue(this.txtSysDescr, address.Description);
        SetTextValue(this.txtSysContact, address.Contact);
        SetTextValue(this.txtSysLocation, address.Location);
        //SetTextValue(this.txtLastSync, ..);
        //SetTextValue(this.txtLastResponse, ..);
        //SetTextValue(this.txtLeaseExpiration, ..);
        //SetTextValue(this.txtLastCredential, ..);
        var IpNodeIdValue = address.IpNodeId ?? 0;
        CustomPropertyRepeater.RetrieveCustomProperties(address, new List<int>(){ IpNodeIdValue });
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }

    private void Save()
    {
        CustomPropertyRepeater.DisableValidatorsByStatus(IPNodeStatus);

        IPNode address = this.Editor.RetrieveAddressObject();
        if (address != null)
        {
            IPNode updatedAddress = UpdateObject(address);
            this.Editor.Store(updatedAddress);
        }
    }
    private IPNode UpdateObject(IPNode address)
    {
        // subnet and address
        if (this.AddressEditor != null)
        {
            address.IPAddress = this.AddressEditor.Address;
        }

        // drop down lists
        if (this.ddlScanning != null)
        {
            address.SkipScan = (this.ddlScanning.SelectedValue == "0");
        }
        if (this.ddlStatus != null)
        {
            IPNodeStatus nodeStatus = ParseSelectedValue<IPNodeStatus>(this.ddlStatus);
            address.Status = nodeStatus;
        }
        if (this.ddlType != null)
        {
            IPAllocPolicy allocPolicy = ParseSelectedValue<IPAllocPolicy>(this.ddlType);
            address.AllocPolicy = allocPolicy;
        }

        // text values
        address.Alias = GetTextValue(this.txtAlias);
        address.DnsBackward = GetTextValue(this.txtDNS);
        address.DhcpClientName = GetTextValue(this.txtDhcpClientName);
        address.MAC = GetTextValue(this.txtMAC);
        address.Comments = GetTextValue(this.txtComment);
        address.MachineType = GetTextValue(this.txtMachineType);
        address.Vendor = GetTextValue(this.txtVendor);
        address.sysName = GetTextValue(this.txtSysName);
        address.Description = GetTextValue(this.txtSysDescr);
        address.Contact = GetTextValue(this.txtSysContact);
        address.Location = GetTextValue(this.txtSysLocation);
        //address. = SetTextValue(this.txtLastSync, ..);
        //address. = SetTextValue(this.txtLastResponse, ..);
        //address. = SetTextValue(this.txtLeaseExpiration, ..);
        //address. = SetTextValue(this.txtLastCredential, ..);

        CustomPropertyRepeater.GetChanges(address);

        // fill xxxBy columns for history tracking purposes
        address.SetChangesBy(ModifiedBy.Manual, false);

        return address;
    }

    private E ParseSelectedValue<E>(DropDownList ddl)
    {
        return (E)Enum.Parse(typeof(E), ddl.SelectedValue);
    }
    private string GetTextValue(TextBox tb)
    {
        if ((tb != null) && (tb.Text != null))
        {
            return tb.Text;
        }
        else
        {
            return string.Empty;
        }
    }
    private void SetTextValue(TextBox tb, string value)
    {
        if (tb != null)
        {
            tb.Text = value ?? string.Empty;
        }
    }

    #endregion // Methods
}
