﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Common.DataBinding;
using SolarWinds.IPAM.Client;
using SolarWinds.IPAM.Web.Common.Utility;
using System.Text;

public partial class SubnetMultiEdit : CommonPageServices
{
    #region Constants

    private const string AttributeCheckBoxId = "chbAttribute";
    private const string PropertyTextId = "PropertyText";

    #endregion // Constants
    #region Properties

    public SqlGuid FromRange { get; private set; }
    public SqlGuid IntoRange { get; private set; }
    public int ClusterId { get; private set; }

    public string ShowParentChangeConfirmDlg
    {
        get { return this.Editor.ShowParentChangeConfirmDlg.ToString().ToLower(); }
    }

    #endregion // Properties

    #region Constructors

    private PageGroupMultiEditor Editor { get; set; }
    public SubnetMultiEdit()
    {
        this.FromRange = SqlGuid.Null;
        this.IntoRange = SqlGuid.Null;
        this.Editor = new PageGroupMultiEditor(this);
    }

    #endregion // Constructors

    #region Event Handlers

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        this.Editor.LoadPage();

        if (this.AccessCheck != null)
            this.AccessCheck.GroupIds.AddRange(this.Editor.GroupIds);

        // postbacks will not update controls values
        if (this.Page.IsPostBack == false)
        {
            List<GroupNode> groups = this.Editor.RetrieveGroupObjects();
            InitControls(groups);
        }
    }

    protected void OnCheckBoxesServerValidate(object source, ServerValidateEventArgs e)
    {
        List<CheckBox> checkBoxes = new List<CheckBox>();

        // Subnet Fields
        checkBoxes.Add(this.chbParent);
        checkBoxes.Add(this.chbComments);
        checkBoxes.Add(this.chbVLAN);
        checkBoxes.Add(this.chbLocation);

        // DNS zone
        checkBoxes.Add(this.chbDnsZone);

        // Scanning Settings
        checkBoxes.Add(this.chbAutoScanning);
        checkBoxes.Add(this.chbNeighborScanning);
        checkBoxes.Add(this.chbTransientPeriod);
        checkBoxes.Add(this.chbPollingEngine);

        e.IsValid = checkBoxes.Exists(o => o.Checked) || CustomPropertyEdit.CheckIfCpChanged();
    }

    protected bool OnNeighborScanSkipValidation(object source)
    {
        return ((this.chbNeighborScanning != null) &&
                (this.chbNeighborScanning.Checked == false));
    }

    protected bool OnTransientPeriodSkipValidation(object source)
    {
        return ((this.chbTransientPeriod != null) &&
                (this.chbTransientPeriod.Checked == false));
    }

    #endregion // Event Handlers

    #region Methods

    private void InitControls(List<GroupNode> groups)
    {
        List<GroupNode> groupNodes = (groups ?? new List<GroupNode>());
        this.ClusterId = groupNodes.FirstOrDefault()?.ClusterId ?? 0;
        bool hasIPv6Subnets = false;
        foreach (GroupNode groupNode in groupNodes)
        {
            hasIPv6Subnets |= (groupNode.GroupType == GroupNodeType.IPv6Subnet);
            if (this.FromRange.IsNull || (this.FromRange > groupNode.AddressN))
            {
                this.FromRange = groupNode.AddressN;
            }
            if (this.IntoRange.IsNull || (this.IntoRange < groupNode.AddressEnd))
            {
                this.IntoRange = groupNode.AddressEnd;
            }
        }

        #region Groups List

        if (this.GroupsList != null)
        {
            foreach (GroupNode groupNode in groupNodes)
            {
                ListItem li = new ListItem();
                li.Text = PageGroupMultiEditor.GroupNodeName(groupNode);
                li.Attributes.Add("class", PageGroupMultiEditor.ListItemCssClass(groupNode));
                this.GroupsList.Items.Add(li);
            }

            if (this.txtGroupsCountLabel != null)
            {
                this.txtGroupsCountLabel.Text = string.Format(
                    Resources.IPAMWebContent.IPAMWEBCODE_AK1_4, this.GroupsList.Items.Count);
            }
        }

        #endregion // Groups List

        #region Subnet Fields

        if (this.chbParent != null)
        {
            this.chbParent.Enabled = !hasIPv6Subnets;
        }
        SetJSCheckBoxControl(this.chbComments, this.txtComments);
        SetJSCheckBoxControl(this.chbVLAN, this.txtVLAN);
        SetJSCheckBoxControl(this.chbLocation, this.txtLocation);

        #endregion // Subnet Fields

        CustomPropertyEdit.RetrieveCustomProperties(new GroupNode(), groups.Select(g => g.GroupId).ToList());

        #region Scan Settings

        bool isIcmpEnabled, isSnmpEnabled, isNeighborEnabled;
        this.Editor.RetrieveScanSettings(
            out isIcmpEnabled, out isSnmpEnabled, out isNeighborEnabled);

        // set controls visibility
        if (this.IcmpScanInfo != null)
        {
            this.IcmpScanInfo.Visible = !isIcmpEnabled;
        }

        if (this.NeighborScanInfo != null)
        {
            this.NeighborScanInfo.Visible = !isNeighborEnabled;
        }

        if (this.TransientPeriod != null)
        {
            this.TransientPeriod.Duration = null;
        }

        if (cbRetainUserData != null)
        {
            cbRetainUserData.Checked = false;
        }

        // set controls default states
        IpamClientProxy proxy = SwisConnector.OpenCacheConnection();

        Setting scanenable = proxy.AppProxy.Setting.GetGlobal(
            SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANENABLE);
        if ((scanenable != null) && (this.cbDisableAutoScanning != null))
        {
            bool? enable;
            if (scanenable.TryGetValue(out enable) && enable.HasValue)
            {
                this.cbDisableAutoScanning.Checked = !enable.Value;
            }
        }

        Setting scaninterval = proxy.AppProxy.Setting.GetGlobal(
            SettingCategory.Global, SettingName.SETTING_DEFAULT_SCANINTERVAL);
        if ((scaninterval != null) && (this.ScanInterval != null))
        {
            TimeSpan? span;
            if (scaninterval.TryGetValue(out span) && span.HasValue)
            {
                this.ScanInterval.Value = span.Value;
            }
        }

        Setting neighborScanEnable = proxy.AppProxy.Setting.GetGlobal(
            SettingCategory.Global, SettingName.SETTING_DEFAULT_NEIGHBORSCANENABLE);
        if ((neighborScanEnable != null) && (this.NeighborScan != null))
        {
            bool? neighborEnable;
            if (neighborScanEnable.TryGetValue(out neighborEnable) && neighborEnable.HasValue)
            {
                this.NeighborScan.IsDisabled = !neighborEnable.Value;
            }
        }

        Setting neighborScanInterval = proxy.AppProxy.Setting.GetGlobal(
            SettingCategory.Global, SettingName.SETTING_DEFAULT_NEIGHBORSCANINTERVAL);
        if ((neighborScanInterval != null) && (this.NeighborScan != null))
        {
            TimeSpan? neighborTimeSpan;
            if (neighborScanInterval.TryGetValue(out neighborTimeSpan) && neighborTimeSpan.HasValue)
            {
                this.NeighborScan.ScanTimeInterval = neighborTimeSpan.Value;
            }
        }

        #endregion // Scan Settings
    }

    protected void MsgSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickSave(object sender, EventArgs e)
    {
        Save();
    }
    protected void ClickCancel(object sender, EventArgs e)
    {
        Response.Redirect("~/Orion/IPAM/Subnets.aspx");
    }

    private void Save()
    {
        Dictionary<string, object> changedProperties = CreatePropertyChanges();
        Dictionary<string, string> changedCustomProperties = CustomPropertyEdit.GetMultiEditChanges();
        this.Editor.Store(true, changedProperties, changedCustomProperties);
    }

    private Dictionary<string, object> CreatePropertyChanges()
    {
        Dictionary<string, object> changes = new Dictionary<string, object>();

        if ((this.hfParent != null) &&
            (this.chbParent != null) && this.chbParent.Checked)
        {
            int parentId = -1;
            if (int.TryParse(this.hfParent.Value, out parentId))
            {
                changes.Add("ParentId", parentId);
            }
        }

        if ((this.txtComments != null) &&
            (this.chbComments != null) && this.chbComments.Checked)
        {
            changes.Add("Comments", this.txtComments.Text);
        }

        if ((this.txtVLAN != null) &&
            (this.chbVLAN != null) && this.chbVLAN.Checked)
        {
            changes.Add("VLAN", this.txtVLAN.Text);
        }

        if ((this.txtLocation != null) &&
            (this.chbLocation != null) && this.chbLocation.Checked)
        {
            changes.Add("Location", this.txtLocation.Text);
        }

        if ((this.DnsZoneSelector != null) &&
            (this.chbDnsZone != null) && this.chbDnsZone.Checked)
        {
            changes.Add("DnsZoneId", DnsZoneSelector.GetSelectedDnsZoneId());
        }

        if ((this.cbDisableAutoScanning != null) &&
            (this.ScanInterval != null) &&
            (this.chbAutoScanning != null) && this.chbAutoScanning.Checked)
        {
            changes.Add("DisableAutoScanning", this.cbDisableAutoScanning.Checked);
        }

        if ((this.cbRetainUserData != null) &&
            (this.chbAutoScanning != null) && this.chbAutoScanning.Checked)
        {
            changes.Add("RetainUserData", this.cbRetainUserData.Checked);
        }

        if ((this.ScanInterval != null) &&
            (this.chbAutoScanning != null) && this.chbAutoScanning.Checked)
        {
            int minutes = Convert.ToInt32(Math.Round(this.ScanInterval.Value.TotalMinutes));
            changes.Add("ScanInterval", minutes);
        }

        if ((this.NeighborScan != null) &&
            (this.chbNeighborScanning != null) && this.chbNeighborScanning.Checked)
        {
            changes.Add("DisableNeighborScanning", this.NeighborScan.IsDisabled);
            changes.Add("NeighborScanAddress", this.NeighborScan.IPAddress);
            changes.Add("NeighborScanInterval", this.NeighborScan.ScanInterval);
        }

        if ((this.TransientPeriod != null) &&
            (this.chbTransientPeriod != null) && this.chbTransientPeriod.Checked)
        {
            changes.Add("TransientPeriod", this.TransientPeriod.Value);
        }

        if ((this.PollingEngine != null) &&
            (this.chbPollingEngine != null) && this.chbPollingEngine.Checked)
        {
            int? newEngineId = PollingEngine?.GetSelectedPollingEngine();
            if (newEngineId.HasValue)
            {
                changes.Add("EngineId", newEngineId.Value);
            }            
        }
        return changes;
    }

    private void SetJSCheckBoxControl(Control checkBox, Control control)
    {
        if ((checkBox == null) || (control == null))
        {
            return;
        }

        this.JS.AddScript(
            string.Format("$SW.MultiEdit.CheckBoxInit('{0}', '{1}');{2}",
            checkBox.ClientID, control.ClientID, Environment.NewLine)
        );
    }
    #endregion // Methods
}
