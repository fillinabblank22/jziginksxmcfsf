using System;
using System.Collections.Generic;
using System.Web.UI;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class SubnetScanStatus : CommonPageServices
	{
    	private PageSubnetScanStatus manager;

        public SubnetScanStatus()
        {
        	manager = new PageSubnetScanStatus(this);
        }

        public GridBox Grid
        {
            get { return grid; }
        }
    	
		public bool ScanEnable
    	{
			get { return manager.ScanEnable; }
    	}
	}
}
