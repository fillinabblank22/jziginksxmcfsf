using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.IPAM.Web.Common;

namespace SolarWinds.IPAM.WebSite
{
    public partial class ExportStructureOnlyPickColumns : CommonPageServices
    {
        PageExportStructureOnlyPickColumns mgr;
        public bool IsExportOnlyStructure { get; set; }

        public ExportStructureOnlyPickColumns() 
        {
            mgr = new PageExportStructureOnlyPickColumns(this); 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (lblHeader != null)
            {
                lblHeader.Text = string.Format(lblHeader.Text, mgr.NumberOfObjects);
            }

            IsExportOnlyStructure = mgr.IsExportOnlyStructure;
        }
    }
}
