using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.IPAM.Web.Layout;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.IPAM.Web.Common.Utility;

namespace SolarWinds.IPAM.WebSite
{
    public partial class Search : CommonPageServices
    {
        #region Properties

        private PageSearchManager mgr;

        public GridBox Grid
        {
            get { return this.SearchGrid; }
        }

        public string EncodedSearchColumns
        {
            get
            {
                return JsonHelper.Encode(mgr.ColumnsString);
            }
        }

        public string EncodedSearchKey
        {
            get
            {
                return JsonHelper.Encode(this.HtmlEncodedSearchKey);
            }
        }

        public string HtmlEncodedSearchKey
        {
            get
            {
                return WebSecurityHelper.SanitizeHtmlV2(HttpUtility.HtmlEncode(Request.Params[PageSearchManager.QUERY_TEXT_PARAM] ?? ""));
            }
        }

        protected bool TimePeriodEnabled
        {
            get
            {
                if (mgr == null)
                {
                    return false;
                }
                return mgr.TimePeriodEnabled;
            }
        }

        protected string TimePeriodData
        {
            get
            {
                if ((mgr == null) || (mgr.TimePeriod == null))
                {
                    return string.Empty;
                }
                return System.Web.HttpUtility.UrlEncode(mgr.TimePeriod.Serialize());
            }
        }

        protected string BeginDate
        {
            get
            {
                if ((mgr == null) || (mgr.TimePeriod == null))
                {
                    return string.Empty;
                }
                return mgr.TimePeriod.BeginDate.ToUniversalTime().ToString();
            }
        }

        protected string EndDate
        {
            get
            {
                if ((mgr == null) || (mgr.TimePeriod == null))
                {
                    return string.Empty;
                }
                return mgr.TimePeriod.EndDate.ToUniversalTime().ToString();
            }
        }

        #endregion //Properties

        #region Constructors

        public Search()
        {
            this.mgr = new PageSearchManager(this);
        }

        #endregion // Constructors

        protected void Page_Load(object sender, EventArgs e)
        {
            // read time period control's value (if needed)
            ReadTimePeriodPostBack();

            // set UI controls with proper time period value
            SetTimePeriodControls();
            
            // update where calue to load proper history data
            TimePeriodValue timeperiod = mgr.TimePeriodEnabled ? mgr.TimePeriod : null;
            mgr.UpdateSubselectClause(timeperiod);

            // other controls
            if (searchtext != null)
                searchtext.Text = String.Format(IPAMWebContent.IPAMWEBCODE_TM0_1, "<span class=\"sw-search-q\">" + this.HtmlEncodedSearchKey + "</span>");

            if (fieldlist != null)
                fieldlist.Text = mgr.DecoratedFieldList;

            searchbox1.Value = this.HtmlEncodedSearchKey;

            // set flag, whether entry point is from search resource or search box
            if (this.SearchState1 != null)
            {
                this.SearchState1.StoreColumns = mgr.StoreColumns;
            }
            
            if (SearchGrid != null && GridPreloader1 != null)
            {
                SearchGrid.Store.SetPageSize(256);
                GridPreloader1.SpecificFields = mgr.GetSpecificFieldsWithAlias();
                GridPreloader1.SubselectClause = mgr.GetSubselectClause(null);
                GridPreloader1.SpecificAlias = mgr.GetSpecificAlias();
                GridPreloader1.PostProcessData += mgr.PostProcessData;
            }
        }

        /// <summary>
        /// retrieve correct time period (depends on post-back)
        /// </summary>
        private void ReadTimePeriodPostBack()
        {
            // do only during postback
            if (this.IsPostBack == false)
            {
                return;
            }

            // check, whether the time period selector is checked
            if (this.rbHistoryYes != null)
            {
                mgr.TimePeriodEnabled = this.rbHistoryYes.Checked;
            }

            // set new value, whether the read value does not fail
            if (this.TimePeriodSelector != null)
            {
                TimePeriodValue value = this.TimePeriodSelector.ReadValue();
                if (value != null)
                {
                    mgr.TimePeriod = value;
                }
            }
        }

        /// <summary>
        /// Set correct time period into controls
        /// </summary>
        private void SetTimePeriodControls()
        {
            this.rbHistoryNo.Checked = !mgr.TimePeriodEnabled;
            this.rbHistoryYes.Checked = mgr.TimePeriodEnabled;

            this.TimePeriodText.Text = mgr.TimePeriod.ToDisplayString();
            this.TimePeriodSelector.SetValue(mgr.TimePeriod);
        }
    }
}
