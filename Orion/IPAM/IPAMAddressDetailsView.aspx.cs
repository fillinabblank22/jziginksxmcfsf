﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.NetObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Web.Master;
using SolarWinds.Orion.Web.UI;

public partial class Orion_IPAM_IPAMAddressDetailsView : OrionView, IProvidePrincipal
{
    private const string NavigateUrlFormat = "{0}/{1}";

    #region Properties

    protected IPNode IPAddress { get; set; }
    protected TimePeriodValue TimePeriod { get; set; }

    public override string ViewType
    {
        get { return "IPAM Address Details View"; }
    }

    #endregion // Properties

    #region Events

    protected override void OnPreLoad(EventArgs e)
    {
        // handle given NetObject
        IpamIPNode netObject = this.NetObject as IpamIPNode;
        if ((netObject != null) && (netObject.IPAddress != null))
        {
            this.IPAddress = netObject.IPAddress;
        }

        // [oh] init access check contol with subnetId for custom user delegation.
        if (AccessCheck != null && this.IPAddress != null && this.IPAddress.IpNodeId != IpamIPNode.PreviewNetObjectId)
            AccessCheck.GroupIds.Add(this.IPAddress.SubnetId);

        this.addressResHost.InitTimePeriod();
        this.TimePeriod = this.addressResHost.TimePeriod;

        // retrieve correct time period (depends on post-back)
        if (this.IsPostBack == true)
        {
            TimePeriodValue value = this.TimePeriodSelector.ReadValue();
            if (value != null)
            {
                // set new value, whether the read value does not fail
                this.TimePeriod = value;

                // this will also set correct time period to resource container,
                // and also sets it's internal IsModified flag..
                this.addressResHost.TimePeriod = this.TimePeriod;
            }
        }

        // set page controls
        this.Title = this.ViewInfo.ViewTitle;
        this.ipAddressLink.IPNode = this.IPAddress;
        this.ipAddressLinkURL.NavigateUrl = IpamSiteMapRenderer.GetNavigateAddressUrl(this.IPAddress);
        this.TimePeriodValue.Text = this.TimePeriod.ToDisplayString();
        this.TimePeriodSelector.SetValue(this.TimePeriod);

        // set help link
        ((Orion_View)this.Master).HelpFragment = "OrionIPAMAddressDetails";

        // set resource container
        this.resContainer.DataSource = this.ViewInfo;
        this.resContainer.DataBind();

        base.OnPreLoad(e);
    }

    protected void OnInitIPNodeSiteMapPath(object sender, EventArgs e)
    {
        if (false == CommonWebHelper.IsBreadcrumbsDisabled)
        {
            var renderer = new IpamSiteMapRenderer();

            KeyValuePair<string, string>[] data = new KeyValuePair<string, string>[2];
            // ip address net object
            data[0] = new KeyValuePair<string, string>(
                IpamSiteMapRenderer.NetObjectKey,
                Request.Form[IpamSiteMapRenderer.NetObjectKey] ?? string.Empty);

            // subnet data
            GroupNode subnet = this.addressResHost.Subnet;
            string subnetName = (subnet == null) ? "subnet" :
                string.Format("{0} / {1}",subnet.Address, subnet.CIDR);
            data[1] = new KeyValuePair<string, string>(
                IpamSiteMapRenderer.SubnetKey, subnetName);

            renderer.SetUpData(data);

            this.IPNodeSiteMapPath.SetUpRenderer(renderer);
        }
    }

    public IPrincipal GetPrincipal()
    {
        return AuthorizationHelper.GetPrincipal();
    }

    #endregion // Events
}
