using System;
using SolarWinds.IPAM.Web.Common;
using SolarWinds.IPAM.BusinessObjects;
using SolarWinds.IPAM.Web.Common.Utility;
using SolarWinds.IPAM.Client;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Resources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.HighAvailability.Contract;
using SolarWinds.IPAM.BusinessObjects.Credentials;
using SolarWinds.IPAM.Common;
using SolarWinds.Orion.Web.Extensions;

namespace SolarWinds.IPAM.WebSite
{
    public partial class DhcpServerEdit : CommonPageServices
    {
        #region Constants
        private const int MaxFailoverDeconfigureJobScheckAttempts = 5;
        private const int MaxSleepTime = 1000;
        private const string ScanServerListKey = "ScanServerList";
        private const string ShowScanMessageKey = "ShowScanMessage";
        #endregion

        private PageServerEditor editor;
        public DhcpServer DhcpServer => editor.Server;

        protected override void OnLoad(EventArgs e)
        {
            ShowNotifications();

            base.OnLoad(e);
            editor = new PageServerEditor(this);
            editor.Initialize();

            // set Dhcp server's node is for 'Test' credentials button
            CredSelector.FnSupplyServerAddress = string.Format(
                "function(fnContinue){{ fnContinue({0}); }}",
                editor.Server.NodeId);

            if (!this.IsPostBack)
            {
                InitControls();
            }
        }

        private void ShowNotifications()
        {
            if (Session[ShowScanMessageKey] != null)
            {
                Session[ShowScanMessageKey] = null;
                string function = $"ScanInform('{IPAMWebContent.IPAMWEBDATA_VB1_867}')";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "ScanInformMessage", function, true);
            }
        }

        protected void MsgSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            this.Save();
        }

        protected void ClickSave(object sender, EventArgs e)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer) return;
            this.Save();
        }

        void InitControls()
        {
            DhcpServer server = DhcpServer;
            IpamEngine engine = this.editor.Engine;

            if (server == null)
                return;

            CustomPropertyRepeater.RetrieveCustomProperties(DhcpServer, new List<int>(){ server.GroupId });

            //IP Address in DhcpServerDetails is in sync with Nodes table
            txtIpAddress.Text = server.FoundAddress;

            // Polling engine information.
            txtPollingEngine.Text = engine.ServerName;
            engineStatusImage.ImageUrl = this.editor.GetEngineStatusImage(engine.KeepAliveUtc);

            // set credential (existing, or new one)
            CredSelector.TrySetCredentials(server.ServerType, server.CredentialId);

            DhcpServerScanSettings.AutoAddNewScopes = server.AddNewScopes;

            if (server.ScanInterval.HasValue)
            {
                DhcpServerScanSettings.ScanInterval = server.ScanInterval.Value;
            }

            NewDhcpScopeSettings.EnableSubnetScanning = server.ScanNewScopesByDefault && server.NewScopesScanInterval > 0;
            if (server.NewScopesScanInterval > 0)
            {
                NewDhcpScopeSettings.ScanInterval = server.NewScopesScanInterval;
            }

            txtServerType.Text = GetDhcpServerVendor(server.ServerType);
            txtComment.Text = WebSecurityHelper.SanitizeHtml(server.Comments);
            txtLocation.Text = WebSecurityHelper.SanitizeHtml(server.Location);
            txtVLAN.Text = WebSecurityHelper.SanitizeHtml(server.VLAN);

            // statistical info
            if (server.StatStartTime != null)
            {
                server.StatStartTime = server.StatStartTime.Value.ConvertToDisplayDate();
            }

            txtStartField.Text = string.Format("{0}", server.StatStartTime);
            txtDiscovers.Text = string.Format("{0}", server.StatDiscovers);
            txtOffers.Text = string.Format("{0}", server.StatOffers);
            txtRequests.Text = string.Format("{0}", server.StatRequests);
            txtAcks.Text = string.Format("{0}", server.StatAcks);
            txtNacks.Text = string.Format("{0}", server.StatNaks);
            txtDeclines.Text = string.Format("{0}", server.StatDeclines);
            txtReleases.Text = string.Format("{0}", server.StatReleases);
            failoverNotScannedYetText.Visible = false;

            if (editor.FailoverCompatabilityCheck(server.NodeId) && server.Status == (byte)IpamStatus.Up)
            {
                failoverpanel.Visible = true;
                if (server.LastDiscovery == null)
                {
                    editFailover.Visible = false;
                    failoverNotScannedYetText.Visible = true;
                }
                else
                {
                    editFailover.CurrentDhcpServerGroupId = server.GroupId;
                    editFailover.FailoverData = editor.DhcpFailoverData;
                }
            }

            DhcpServerScanSettings.ParentId = DhcpServer.ParentId;
            if (DhcpServer.ClusterId != null)
            {
                DhcpServerScanSettings.ClusterId = DhcpServer.ClusterId.Value;
            }
        }

        protected void Save()
        {
            this.Validate();
            if (!this.IsValid)
                return;

            List<SwisOpCtx> serverOps = new List<SwisOpCtx>();
            List<SwisOpCtx> failoverOps = new List<SwisOpCtx>();

            DhcpServer server = DhcpServer;
            List<DhcpFailoverData> failoverData = editFailover.GetChanges();
            UpdateServerProperties(server);

            if (server.IsDirty)
            {
                serverOps.Add(new SwisOpCtx("Update DHCP Server",
                    delegate (IpamClientProxy proxy, object param)
                    {
                        return proxy.AppProxy.DhcpServer.Update(server);
                    })
                );
            }
            SwisConnector.RunSwisOps(serverOps.ToArray());

            //Processing failover part 
            if (failoverData.Any())
            {
                List<int> serverList = ProcessFailover(failoverData, failoverOps, server);

                //Run failover operations
                SwisConnector.RunSwisOps(failoverOps.ToArray(), true);

                Session[ScanServerListKey] = serverList;
                Session[ShowScanMessageKey] = true;
                Response.Redirect(Request.Url.AbsoluteUri); //To show message from backend and start scanning we should reload page 
            }
            else
            {
                CloseEditForm();
            }
        }

        private void CloseEditForm()
        {
            if (this.HideChrome)
            {
                string page = "~/Orion/IPAM/DialogWindowDone.aspx";

                this.Server.Transfer(page);
            }
            else
                this.Response.Redirect("~/Orion/IPAM/Dhcp.Management.aspx");
        }

        private List<int> ProcessFailover(List<DhcpFailoverData> failoverData, List<SwisOpCtx> ops, DhcpServer server)
        {
            List<DhcpFailoverData> originalList = editor.DhcpFailoverData;
            List<DhcpFailoverData> deleted = failoverData.Where(row => row.FailoverStatus == FailoverStatus.Deleted && row.FailoverInitialStatus != FailoverStatus.Added).ToList();
            List<DhcpFailoverData> added = failoverData.Where(row => row.FailoverStatus == FailoverStatus.Added).ToList();
            List<DhcpFailoverData> scopesChanged = failoverData.Where(row => row.FailoverStatus == FailoverStatus.ScopeRemoved || row.FailoverStatus == FailoverStatus.ScopeAdded).ToList();
            List<int> serversToScan = new List<int>();

            //Handle removed failovers
            if (deleted.Any())
            {
                ops.AddRange(deleted.Select(dhcpFailoverData => new SwisOpCtx("DHCP Failover delete", delegate (IpamClientProxy proxy, object param)
                {
                    if (dhcpFailoverData != null)
                    {
                        proxy.AppProxy.DhcpServer.DeleteFailover(Guid.NewGuid(), server.GroupId, dhcpFailoverData);
                        AddServersToScanList(serversToScan, dhcpFailoverData);
                    }
                    return null;
                })));
            }

            //Handle changed scpoes in failovers
            if (scopesChanged.Any())
            {
                //Find and handle changed scopes
                List<DhcpFailoverData> addedScopes = new List<DhcpFailoverData>();
                List<DhcpFailoverData> removedScopes = new List<DhcpFailoverData>();

                foreach (DhcpFailoverData dhcpFailoverData in scopesChanged)
                {
                    DhcpFailoverData origin =
                        originalList.FirstOrDefault(
                            row => row.RelationshipName == dhcpFailoverData.RelationshipName);

                    if (origin != null)
                    {
                        Dictionary<int, string> removedDictionary = new Dictionary<int, string>();
                        foreach (KeyValuePair<int, string> scope in origin.Scopes)
                        {
                            if (!dhcpFailoverData.Scopes.ContainsKey(scope.Key))
                            {
                                removedDictionary.Add(scope.Key, scope.Value);
                            }
                        }
                        if (removedDictionary.Any())
                        {
                            DhcpFailoverData removedScopesCopy = dhcpFailoverData.Clone();
                            removedScopesCopy.Scopes = removedDictionary;
                            removedScopes.Add(removedScopesCopy);
                        }

                        Dictionary<int, string> addDictionary = new Dictionary<int, string>();
                        foreach (KeyValuePair<int, string> scope in dhcpFailoverData.Scopes)
                        {
                            if (!origin.Scopes.ContainsKey(scope.Key))
                            {
                                addDictionary.Add(scope.Key, scope.Value);
                            }
                        }

                        if (addDictionary.Any())
                        {
                            DhcpFailoverData addedScopesCopy = dhcpFailoverData.Clone();
                            addedScopesCopy.Scopes = addDictionary;
                            addedScopes.Add(addedScopesCopy);
                        }
                        AddServersToScanList(serversToScan, dhcpFailoverData);
                    }
                }

                bool haveRemovedScopes = removedScopes.Any();
                bool haveAddedScopes = addedScopes.Any();

                if (haveRemovedScopes || haveAddedScopes)
                {
                    ops.Add(new SwisOpCtx("DHCP Failover scope remove/add operations",
                        delegate (IpamClientProxy proxy, object param)
                        {
                            Guid taskId = Guid.NewGuid();
                            int tryCount = 0;

                            if (haveRemovedScopes)
                            {
                                proxy.AppProxy.DhcpServer.DeconfigureFailoverForScopes(taskId, server.GroupId, removedScopes);

                                //We must be sure that deconfigure operation finished to perform another operations
                                while (tryCount < MaxFailoverDeconfigureJobScheckAttempts)
                                {
                                    UIJob job = proxy.AppProxy.UIJob.Get(taskId);

                                    if (job.CompletionState == UIJobCompletionState.Finished)
                                    {
                                        break;
                                    }

                                    if (job.CompletionState == UIJobCompletionState.Failed)
                                    {
                                        break;
                                    }

                                    //Wait sone time for check job status again and increase try count
                                    Thread.Sleep(MaxSleepTime);
                                    tryCount++;
                                }
                            }

                            if (haveAddedScopes)
                            {
                                proxy.AppProxy.DhcpServer.AddScopeToFailover(Guid.NewGuid(), server.GroupId, addedScopes);
                            }
                            return null;
                        }));
                }
            }

            //Hanle added failovers
            if (added.Any())
            {
                foreach (DhcpFailoverData dhcpFailoverData in added)
                {
                    ops.Add(new SwisOpCtx("DHCP Failover create",
                        delegate (IpamClientProxy proxy, object param)
                        {
                            proxy.AppProxy.DhcpServer.CreateFailover(Guid.NewGuid(), server.GroupId, dhcpFailoverData);
                            return null;
                        }));
                    AddServersToScanList(serversToScan, dhcpFailoverData);
                }
            }

            return serversToScan;
        }

        private void AddServersToScanList(List<int> serversToScan, DhcpFailoverData dhcpFailoverData)
        {
            if (!serversToScan.Contains(dhcpFailoverData.PrimaryServerId))
            {
                serversToScan.Add(dhcpFailoverData.PrimaryServerId);
            }
            if (!serversToScan.Contains(dhcpFailoverData.SecondaryServerId))
            {
                serversToScan.Add(dhcpFailoverData.SecondaryServerId);
            }
        }

        private void UpdateServerProperties(DhcpServer server)
        {
            // DhcpServer attributes
            server.Comments = this.txtComment.Text;
            server.Location = this.txtLocation.Text;
            server.VLAN = this.txtVLAN.Text;

            // server type by selected credentials type
            server.ServerType = CredSelector.DhcpServerType;

            // Credentials
            server.CredentialId = StoreCredentials();

            // ScanSettings
            server.AddNewScopes = this.DhcpServerScanSettings.AutoAddNewScopes;
            server.ScanInterval = this.DhcpServerScanSettings.ScanInterval;

            // ScopeSettings
            server.ScanNewScopesByDefault = this.NewDhcpScopeSettings.EnableSubnetScanning;
            server.NewScopesScanInterval = this.NewDhcpScopeSettings.ScanInterval;

            // custom properties
            CustomPropertyRepeater.GetChanges(server);
        }

        private Nullable<int> StoreCredentials()
        {
            Nullable<int> credentialId = CredSelector.CredId;
            if (credentialId == null)
            {
                ICredentials credentials = CredSelector.GetCredentials();
                SwisOpCtx createNewCredentialSwisOpCtx = new SwisOpCtx(
                    "Create New Credential",
                    delegate (IpamClientProxy proxy, object param)
                    {
                        credentialId = proxy.AppProxy.Credentials.Create(credentials);
                        return null;
                    }
                );
                SwisConnector.RunSwisOps(new SwisOpCtx[] { createNewCredentialSwisOpCtx });
            }

            return credentialId;
        }

        private string GetDhcpServerVendor(DhcpServerType dhcpServerType)
        {
            // Get the vendor name by server type.
            switch (dhcpServerType)
            {
                case DhcpServerType.Windows:
                    return "Windows";

                case DhcpServerType.CISCO:
                    return "Cisco";

                case DhcpServerType.ASA:
                    return "ASA";

                case DhcpServerType.ISC:
                    return "ISC";

                case DhcpServerType.Infoblox:
                    return "Infoblox";
                    
                default:
                    return string.Empty;
            }
        }
    }
}
