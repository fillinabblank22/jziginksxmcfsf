<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" 
    EnableSessionState="ReadOnly" CodeFile="ReportWait.aspx.cs" Inherits="Orion_ReportWait" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headPlaceholder" runat="server">
    <style type="text/css">
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="waitmodal" style="z-index: 999; text-align: center; padding: 30px 0 30px 0;"><div style="border: 3px solid #aaa; -moz-border-radius: 3px; border-radius: 3px; padding: 30px; display: inline-block; max-width: 80%; background-color: #fff; text-align: center;">
                                                                 
        <div style="margin-bottom: 10px;"><img src="/Orion/images/AJAX-Loader.gif" width="16" height="16"/></div>
        
        <div><%= DefaultSanitizer.SanitizeHtml(string.Format(HttpUtility.HtmlEncode(Resources.CoreWebContent.WEBCODE_PCC_55), "<b>" + HttpUtility.HtmlEncode(this.ReportName) + "</b>")) %></div>
        <%--FB253125 - hiding remaining time text for now--%>
        <%--<div class="sw-text-helpful" id="remainingtime"><%= HttpUtility.HtmlEncode(Resources.CoreWebContent.WEBCODE_PCC_56) %></div>--%>
        
        <div style="margin-top: 24px;"><orion:LocalizableButtonLink runat="server" NavigateUrl="/Orion/Reports/ViewReports.aspx" LocalizedText="Cancel" /></div>

        <div id="errbucket" style="display: none;">
            <div class="sw-suggestion sw-suggestion-fail"><span class="sw-suggestion-icon"></span><%= HttpUtility.HtmlEncode(Resources.CoreWebContent.WEBCODE_PCC_57) %></div>
        </div>

    </div></div>
    
    <script type="text/javascript">
    // <![CDATA[

        (function () {

            jQuery.fn.center = function () {
                this.css("position", "absolute");
                this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                $(window).scrollTop()) + "px");
                this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
                return this;
            };

            var pg = SW.Core.namespace("SW.pg");

            pg.StartReportUrl = '<%= HttpUtility.JavaScriptStringEncode(this.AjaxStartUrl ?? string.Empty) %>';
            pg.LoadReportUrl = '<%= HttpUtility.JavaScriptStringEncode(this.AjaxDestinationUrl) %>';
            pg.RefreshReportUrl = '<%= HttpUtility.JavaScriptStringEncode(this.AjaxRefreshUrl) %>';

            pg.ParseJson = function (txt) {
                try {
                    return jQuery.parseJSON(txt);
                } catch (e) {
                    return null;
                }
            };

            pg.HandleError = function(txt) {
                if (/loginsig\(09470EE361E54F3DB730000176BA16B2\)/.test(txt))
                    window.location = "/Orion/Login.aspx?sessionTimeout=yes";
                $('#errbucket').show();
                $('#waitmodal').center();
            };

            pg.Refresh = function () {
                $.ajax({
                    url: pg.RefreshReportUrl,
                    dataType: 'text',
                    success: function (txt) {
                        var v = pg.ParseJson(txt);
                        if (!v) return pg.HandleError(txt);

                        if (v.ready)
                            window.location = pg.LoadReportUrl;
                        else {
                            //$('#remainingtime').text(v.remaining);
                            $('#waitmodal').center();
                            window.setTimeout(SW.pg.Refresh, 2000);
                        }
                    },
                    error: function (xhr, s, err) {
                        if (s == 'timeout') // normal.
                            return pg.Refresh();
                        pg.HandleError(err);
                    }
                });
            };

            pg.Start = function () {
                if (!pg.StartReportUrl) {
                    pg.Refresh();
                    return;
                }

                $.ajax({
                    url: pg.StartReportUrl,
                    dataType: 'json',
                    timeout: 5000, // 5 seconds. if we get a reply back we'll load it up.
                    success: function (v) {
                        if (v.ready) window.location = pg.LoadReportUrl;
                        else pg.Refresh();
                    },
                    error: function (xhr, s, err) {
                        if (s == 'timeout') // normal.
                            return pg.Refresh();
                        pg.HandleError(err);
                    }
                });
            };

        })();

        $(function () {
            var $overlay = $('<div class="ui-overlay"><div class="ui-widget-overlay"></div></div>').hide().appendTo('body');
            $overlay.fadeIn();

            $(window).resize(function () {
                $overlay.width($(document).width());
                $overlay.height($(document).height());
                $('#waitmodal').center();
            });

            $('#waitmodal').center();
        });

        $(SW.pg.Start);
    // ]]>
    </script>
</asp:Content>
