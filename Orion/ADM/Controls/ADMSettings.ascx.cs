﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.ADM.Web.Controllers;
using SolarWinds.Orion.Core.Common.Models;
using SolarWinds.Orion.Web;
using SolarWinds.ADM.Web.ViewModels;

public partial class Orion_ADM_Controls_ADMSettings : UserControl, INodePropertyPlugin
{
    private AdmSettingsService settingsService;

    protected void Page_Load(object sender, EventArgs e)
    {
        AdmSettingsContent.Visible = settingsService.Model.AdmSettingsVisible;
        if (!AdmSettingsContent.Visible || IsPostBack)
        {
            return;
        }

        var baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                         Request.ApplicationPath.TrimEnd('/') + "/";

        AdmSettingsAgentMissingMessage.Visible = settingsService.Model.AgentMissing;
        AdmSettingsAdmEnabledCheckbox.Checked = settingsService.Model.AdmEnabled;
        AdmSettingsAgentMissingMessageLink.HRef = baseUrl + settingsService.Model.AgentMissingMessageLinkHref;
    }

    public void Initialize(IList<Node> nodes, NodePropertyPluginExecutionMode mode, Dictionary<string, object> pluginState)
    {
        settingsService = new AdmSettingsService();
        settingsService.InitAdmSettingsViewModel(nodes.Select(node => node.Id).ToList());
    }

    public bool Validate()
    {
        return true;
    }

    public bool Update()
    {
        return settingsService.UpdateAdmEnabled(AdmSettingsAdmEnabledCheckbox.Checked);
    }
}