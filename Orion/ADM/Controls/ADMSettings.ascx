﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ADMSettings.ascx.cs" Inherits="Orion_ADM_Controls_ADMSettings" %>
<div id="AdmSettingsContent" runat="server" class="contentBlock">
    <table>
        <tbody>
            <tr>
                <td class="contentBlockModuleHeader">
                    <asp:Literal runat="server" Text="<%$Resources: AdmWebContent,AdmSettingsTitleText %>"></asp:Literal>                    
                </td>
                <td>
                    <span class="checkbox"><asp:CheckBox runat="server" ID="AdmSettingsAdmEnabledCheckbox" Text="<%$Resources: AdmWebContent,AdmSettingsAdmEnabledCheckboxLabel %>" /></span>
                </td>
            </tr>
            <tr runat="server" id="AdmSettingsAgentMissingMessage">
                <td></td>
                <td>
                    <div class="sw-suggestion sw-suggestion-warn">
                        <span class="sw-suggestion-icon"></span>
                        <asp:Literal runat="server" Text="<%$Resources: AdmWebContent,AdmSettingsAgentMissingMessageText %>"></asp:Literal>
                        <a runat="server" target="_blank" id="AdmSettingsAgentMissingMessageLink" class="RenewalsLink">
                            <asp:Literal runat="server" Text="<%$Resources: AdmWebContent,AdmSettingsAgentMissingMessageLinkText%>"></asp:Literal>
                        </a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>