<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewGroupSubNavTab.ascx.cs" Inherits="ViewGroupSubNavTab" %>
<%@ Register Src="~/Orion/ViewGroupSubNavTabFlyout.ascx" TagPrefix="orion" TagName="ViewGroupSubNavTabFlyout" %>

            <div class='<%# DefaultSanitizer.SanitizeHtml(this.TabHoverCssClass) %>' ng-click="navToSubview('<%# DefaultSanitizer.SanitizeHtml(IsDemoMode && IsAddTab ? "" : GetViewGroupLink( View, ParentView )) %>', '<%# DefaultSanitizer.SanitizeHtml(IsAddTab) %>' == 'True');" >
                <table id="tblSubNavLayout" >
                    <tr>
                       <td class="SubNav-TabLeft" >&nbsp;&nbsp;</td>
                       <td>
                           <div class='SubNav-TabCenter'>
                               <div class='SubNav-TabTitle' title="<%# HttpUtility.HtmlEncode(this.TabTitle) %>">
                                   <img id="tabImage" src='<%# DefaultSanitizer.SanitizeHtml(this.TabIconInfo.WebImagePath) %>' runat="server" visible='<%# DefaultSanitizer.SanitizeHtml(!string.IsNullOrEmpty(this.View.ViewIcon)) %>' />
                                   <span class='SubNav-TabTitleText' > <%# DefaultSanitizer.SanitizeHtml(this.TabTitle) %> </span>
                                    <asp:PlaceHolder runat="server" ID="pnlTabEditControls" Visible='<%# DefaultSanitizer.SanitizeHtml(IsEdit && !IsAddTab) %>'>
                                        <span style="float:right; position: absolute; right:0px; margin-top: 4px; margin-right: 2px;">
                                            <asp:ImageButton ID="btnMoveUp1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveUp.png" OnClick="btnMoveUp_Click"
			                                                AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_56 %>" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_56 %>" />
                                            <asp:ImageButton ID="btnMoveDown1" runat="server" ImageUrl="~/orion/images/SubViewImages/MoveDown.png" OnClick="btnMoveDown_Click"
			                                                AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_57 %>" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_57 %>" />
                                                        <% if (View.ViewGroup != 0 && ViewManager.GetFirstViewInGroup(View.ViewGroup).ViewID != View.ViewID){ %>
                                            <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="~/orion/images/SubViewImages/delete_16x16.png" OnClick="btnDelete_Click"
			                                                AlternateText="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_58 %>" ToolTip="<%$ HtmlEncodedResources: CoreWebContent, WEBDATA_VB1_58 %>" />
                                                        <%}%>
                                        </span>
                                        <script type="text/javascript">
                                           //Unbind to remove asp.net doPostback
                                           $('#<%=btnDelete1.ClientID %>').unbind('click').click( function () {
                                               var callback = function(){<%= Page.ClientScript.GetPostBackEventReference(btnDelete1, null) %>};
                                               var tabTitle = '<%= this.TabTitle %>';
                                               OpenDialogConfirmDeleteTab( tabTitle, callback);
                                               return false;
                                        });
                                       </script>
                                   </asp:PlaceHolder>   
                                </div>
                            </div>
                            
                            <%--Check fb169611 before reenabling this--%>
                            <%--<orion:ViewGroupSubNavTabFlyout runat="server" id="LeftSubNav" View='<%# DefaultSanitizer.SanitizeHtml(this.View) %>' IsActive='<%# DefaultSanitizer.SanitizeHtml(this.IsActive) %>' ></orion:ViewGroupSubNavTabFlyout>--%>
                            <span class="SubNav-TabRight"></span>
                       </td>
                    </tr>
                </table>   
           </div> 
    
          
