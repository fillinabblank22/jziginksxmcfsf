﻿<%@ Page Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_1%>" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="SearchResults.aspx.cs" Inherits="Orion_TrafficAnalysis_SearchResults_SearchResults" %>
<%@ Register TagPrefix="netflow" TagName="Search" Src="~/Orion/TrafficAnalysis/Search/SearchNetFlowTraffic.ascx" %>
<%@ Register Src="~/Orion/Controls/HelpHint.ascx" TagPrefix="netflow" TagName="HelpHint" %>
<%@ Register TagPrefix="netflow" TagName="InfoHint" Src="~/Orion/TrafficAnalysis/Controls/InfoHint.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <orion:Include ID="Include1" Module="TrafficAnalysis" runat="server" File="Search.css" />
    <orion:Include ID="Include2" Module="TrafficAnalysis" runat="server" File="NetFlow.css" />

    <div style="float: right; margin-right: 5px; margin-top: 15px" id="NetFlowSearchDiv">
        <netflow:Search ID="netFlowSearch" runat="server"/>
    </div>

    <h1 class="ntaHeader"><%= Resources.NTAWebContent.NTAWEBDATA_VB1_1%></h1>

    <div id="searchHintWrapper" style="margin-left: 15px">

        <div id="searchHintItem" align="left" style="margin-top: 10px">
            <%= GetSearchLabel() %>
        </div>

        <div ID="resultsLimitedDiv" runat="server" visible="false" style="margin-top: 10px">
            <span class="searchHint">
            <img src="../../images/Icon.Lightbulb.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_2%>" />
            <asp:Label runat="server" ID="lblResultsLimited" />
            </span>
        </div>

        <div align="left" style="margin-top:10px;">
            <asp:Label runat="server" ID="lblPersistantNotSet" Visible="true" ForeColor="#D50000" Font-Bold="true" />
        </div>

        <div align="left">
            <asp:Label runat="server" ID="lblNoDataLabel" Visible="false" />
        </div>

        <div align="left" ID="validationErrorDiv" runat="server" visible="false">
            <br />
            <asp:Label Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_3%>" runat="server" ForeColor="#D50000" Font-Bold="false" />
            <asp:Label runat="server" ID="validationErrorLabel" ForeColor="#D50000" Font-Bold="true" />
        </div>
        <br />
        <netflow:InfoHint runat="server" ID="WildcardSearchHintsDiv" Visible="False">
            <HelpContent>
                <div class="infoTitle"><%= GetHintLabel() %></div>
                <%= Resources.NTAWebContent.NTAWEBDATA_JT1_1%><br />
                <ul style="margin: 0px; padding-left: 20px; color:#767676">
                    <li>SNMP</li>
                    <li>SNMP*Trap</li>
                </ul>
            </HelpContent>
        </netflow:InfoHint>

        <netflow:InfoHint runat="server" ID="portSearchHintsDiv" Visible="False">
            <HelpContent>
                <div class="infoTitle"><%= GetHintLabel() %></div>
                <%= Resources.NTAWebContent.NTAWEBDATA_JT1_2%><br />
                <ul style="margin: 0px; padding-left: 20px; color:#767676">
                    <li>80</li>
                    <li>80, 2517, 1718-1720</li>
                </ul>
            </HelpContent>
        </netflow:InfoHint>
        
        <netflow:InfoHint runat="server" ID="PrefixPostfixSearchHintsDiv" Visible="False">
            <HelpContent>
                <div class="infoTitle"><%= GetHintLabel() %></div>
                <%= Resources.NTAWebContent.NTAWEBDATA_AK2_1%><br />
                <ul style="margin: 0px; padding-left: 20px; color:#767676">
                    <li>2001:*</li>
                    <li>*:f6e0</li>
                </ul>
            </HelpContent>
        </netflow:InfoHint>

        <netflow:InfoHint runat="server" ID="IPv4SearchHintsDiv" Visible="False">
            <HelpContent>
                <div class="infoTitle"><%= GetHintLabel() %></div>
                <%= Resources.NTAWebContent.NTAWEBDATA_JT1_3%><br />
                <ul style="margin: 0px; padding-left: 20px; color:#767676">
                    <li>10.210.5.1</li>
                    <li>10.210.*.1</li>
                    <li>10.210.*</li>
                    <li>10.210.5.1 - 10.210.5.99</li>
                    <li>10.210.5.0/24</li>
                </ul>
            </HelpContent>
        </netflow:InfoHint>
        
        <netflow:InfoHint runat="server" ID="IPv6SearchHintsDiv" Visible="False">
            <HelpContent>
                <div class="infoTitle"><%= GetHintLabel() %></div>
                <%= Resources.NTAWebContent.NTAWEBDATA_JT1_3%><br />
                <ul style="margin: 0px; padding-left: 20px; color:#767676">
                    <li>f000:ca29:ae6:d82b:0:f5da:0:ef01</li>
                    <li>f000::f5da:*:ef01</li>
                    <li>f000:ca29:ae6:*</li>
                    <li>f000::f5da:0:1 - f000::f5da:0:ef01</li>
                </ul>
            </HelpContent>
        </netflow:InfoHint>

    </div>

    <asp:Panel ID="pagerPanel" runat="server" Visible="false">
    <table id="pagerTable" style="width: 100%; border: 0;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: right; width: 25%; padding-right: 10px">
                <b><%= string.Format(Resources.NTAWebContent.NTAWEBDATA_VB1_7, (this.PageNumber + 1).ToString())%></b>
            </td>
            <td>
                <asp:LinkButton ID="firstButton" runat="server" ForeColor="Blue" OnClick="FirstBtnClick">&lt;&lt;</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="prevButton" runat="server" ForeColor="Blue" OnClick="PrevBtnClick">&lt;</asp:LinkButton>&nbsp;
                <asp:Repeater ID="rptPages" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnPage" CommandName="Page" ForeColor="Blue" CommandArgument="<%# Container.DataItem %>"
                            runat="server"><%# Container.DataItem %> </asp:LinkButton>&nbsp;
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:LinkButton ID="nextButton" runat="server" ForeColor="Blue" OnClick="NextBtnClick">&gt;</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lastButton" runat="server" ForeColor="Blue" OnClick="LastBtnClick">&gt;&gt;</asp:LinkButton>
            </td>
        </tr>
    </table>
    </asp:Panel>

    <div id="searchResultWrapper" style="margin-left: 30px; margin-top: 10px;">
    <asp:Repeater ID="rptSearchItems" runat="server">
        <ItemTemplate>
            <div ID="searchResultItem" runat="server">
            <orion:collapsepanel ID="Collapsepanel1" runat="server" datasource='<%# Container.DataItem %>' collapsed="<%# collapseMainExpander %>">
                <TitleTemplate>
                    <%# GetIconHtml() %>
                    <a href='<%# GetSummaryItemLink() %>' >
                        <b><%# HighlightSearchString(SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(GetItemDisplayName())) %></b>
                    </a>
                    <br />
                </TitleTemplate>
                <BlockTemplate>
                    <asp:Repeater id="rptNodes" runat="server" DataSource='<%# Eval("SubItems") %>'> 
                        <ItemTemplate>
                            <orion:CollapsePanel ID="CollapsePanel2" runat="server" DataSource='<%# Container.DataItem %>' Collapsed="<%# collapseNodeExpander %>">
                                <TitleTemplate>
                                    <a href='<%# GetDetailItemLink("N:" + Eval("NodeID")) %>'>
                                        <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("NodeCaption").ToString()) %>
                                    </a>
                                    <br />
                                </TitleTemplate>
                                <BlockTemplate>
                                    <asp:Repeater id="rptInterfaces" runat="server" DataSource='<%# Eval("SubItems") %>'> 
                                        <ItemTemplate>
                                            <a href='<%# GetDetailItemLink("I:" + Eval("InterfaceID")) %>' >
                                                <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("InterfaceCaption").ToString()) %>
                                            </a>
                                            <br />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    </BlockTemplate>
                            </orion:CollapsePanel>
                        </ItemTemplate>
                    </asp:Repeater>
                </BlockTemplate>
            </orion:collapsepanel>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    </div>

</asp:Content>
