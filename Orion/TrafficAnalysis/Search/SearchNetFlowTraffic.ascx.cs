﻿using System;
using System.Drawing;
using System.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Search;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Utils;
using SolarWinds.Orion.Web.Helpers;
using UIHelper = SolarWinds.Netflow.Web.UIHelper;

public partial class Orion_TrafficAnalysis_SearchResults_SearchNetFlowTraffic : UserControl
{
    public string SearchString
    {
        get { return searchString.Text; }
        set
        {
            searchString.Text = WebSecurityHelper.SanitizeHtmlV2(value);
            searchString.ForeColor = Color.Black;
        }
    }

    public string SelectedSearchDefinitionValue
    {
        get { return searchField.SelectedValue; }
        set { searchField.SelectedValue = value; }
    }

    public TimePeriodUiFilter TimePeriodFilter
    {
        get { return searchPeriodMenu.PeriodFilter; }
        set { searchPeriodMenu.PeriodFilter = value; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            searchField.DataSource = SearchOption.GetAllOptions();
            searchField.DataTextField = "Text";
            searchField.DataValueField = "Value";
            searchField.DataBind();

            if (!string.IsNullOrEmpty(GlobalSettingsDALCached.DefaultSearchTimePeriod))
            {
                this.searchPeriodMenu.PeriodFilter =  TimePeriodUiFilter.Parse(GlobalSettingsDALCached.DefaultSearchTimePeriod);
            }
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        UIHelper.RegisterEnterKey(searchString, btnSearch);
        UIHelper.RegisterEnterKey(searchField, btnSearch);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!IsPostBack)
        {
            if (searchString.Text == string.Empty)
            {
                searchString.Text = Resources.NTAWebContent.NTAWEBCODE_VB1_5;
                searchString.ForeColor = Color.Gray;

                string script = string.Format(@"$(document).ready(function () {{ 
        $(""#{0}"").click(function () {{
            $(""#{0}"").removeAttr('presetWatermark');
            $(this).val('').css('color', 'black');

            $(this).unbind('click');
        }});

        $(""#{0}"").attr('presetWatermark', true);
}});", this.searchString.ClientID);

                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClearSearchTextBoxOnClick", script, true);
            }
        }
    }

    private string EncodeSearchString(string searchStr, SearchOption searchDefinition)
    {
        if (!searchDefinition.SurroundWithWildcard)
        {
            // trim non-wildcards values (numeric ones)
            searchStr = searchStr.Trim();
        }

        searchStr = searchStr.Replace("*", "%").Replace("'", string.Empty);
        
        return Server.UrlEncode(searchStr);
    }

    private string ConvertAbsoluteTimeToUtc(string timePeriodName)
    {
        timePeriodName = timePeriodName.Replace(" to ", "~");

        if (timePeriodName.Contains("~"))
        {
            string[] startEndTime = timePeriodName.Split('~');
            timePeriodName = DateTime.Parse(startEndTime[0]).ToUtcSortableString() + "~" + DateTime.Parse(startEndTime[1]).ToUtcSortableString();
        }

        return timePeriodName;
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        string searchStr = searchString.Text;
        
        if (string.IsNullOrEmpty(searchStr))
            return;

        SearchOption searchOption = SearchOption.GetOptionByValue(searchField.SelectedValue);
        searchStr = EncodeSearchString(searchStr, searchOption);

        string searchPage = GetSearchUrl(searchStr, searchOption, this.searchPeriodMenu.PeriodFilter.PeriodName);
        
        Response.Redirect(searchPage);
    }

    public string GetSearchUrl(string searchStr, SearchOption searchOption, string timePeriod)
    {
        if (string.IsNullOrEmpty(searchStr))
            throw new ArgumentException("searchString must be specified");

        const string SearchUrlTemplate = "/Orion/TrafficAnalysis/Search/SearchResults.aspx?SearchStr={0}&SearchBy={1}&T={2}";

        return string.Format(SearchUrlTemplate, searchStr, searchOption.Value, timePeriod);
    }
}