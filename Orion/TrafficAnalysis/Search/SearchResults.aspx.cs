﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Search;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.Platform;

public partial class Orion_TrafficAnalysis_SearchResults_SearchResults : Page
{
    protected bool collapseMainExpander = true;
    protected bool collapseNodeExpander = true;

    private string searchString;
    private string searchStringRaw;
    private string timePeriodNameUTC;
    private string timePeriodNameLocal;
    private SearchOption searchOption;
    private TimePeriodUiFilter TimePeriodFilter;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        rptPages.ItemCommand += rptPages_ItemCommand;

        searchStringRaw = Server.HtmlDecode(Request.Params["SearchStr"]);
        string searchOptionValue = Request.Params["SearchBy"];
        timePeriodNameUTC = Request.Params["T"];

        // convert UTC time to Local (for TimePeriod Control and caption)
        this.TimePeriodFilter = TimePeriodUiFilter.Parse(timePeriodNameUTC);
        this.timePeriodNameLocal = this.TimePeriodFilter.PeriodNameTitle;

        searchOption = SearchOption.GetOptionByValue(searchOptionValue);
        searchString = searchStringRaw.Replace('%', '*');
        if (searchOption.SurroundWithWildcard)
        {
            if (!searchStringRaw.StartsWith("%"))
            {
                searchStringRaw = "%" + searchStringRaw;
            }
            if (!searchStringRaw.EndsWith("%"))
            {
                searchStringRaw = searchStringRaw + "%";
            }
        }

        OrionInclude.ModuleFile("TrafficAnalysis", NetflowAdvancedApplication.IconCssFileName);
    }

    private void rptPages_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
        LoadData();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!IsPostBack)
        {
            this.netFlowSearch.SearchString = searchString;
            this.netFlowSearch.SelectedSearchDefinitionValue = searchOption.Value;
            this.netFlowSearch.TimePeriodFilter = this.TimePeriodFilter;

            LoadData();
        }
    }

    private void LoadData()
    {
        var dal = HttpContext.Current.Resolve<ISearchDAL>();
        //Why just first one gets validated???
        SearchValidationResult validationResult = dal.TryValidateSearch(searchOption.SearchDefinitions[0], searchStringRaw);

        if (!validationResult.IsValid)
        {
            validationErrorDiv.Visible = true;
            validationErrorLabel.Text = validationResult.ErrorMessage;
            DisplaySearchHints(validationResult.ErrorType);

            return;
        }

        if ((searchOption.Value.Equals("Hostname", StringComparison.InvariantCultureIgnoreCase) ||
            searchOption.Value.Equals("Domain", StringComparison.InvariantCultureIgnoreCase)) &&
            GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled == false)
        {
            this.lblPersistantNotSet.Text = string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_1, this.searchOption.Value);

            return;
        }

        lblResultsLimited.Text = string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_2, ModuleConfig.MaxSearchResults);

        PagedDataSource pgItems = new PagedDataSource();


        TimePeriodUiFilter tp = TimePeriodUiFilter.Parse(timePeriodNameUTC);
        TimePeriodUiDbInfo dbInfo = tp.DbInfo;
        TimePeriodDbFilter dbFilter = new TimePeriodDbFilter(dbInfo.StartTime.ToUniversalTime(), dbInfo.EndTime.ToUniversalTime(), dbInfo.Intervals[0].SampleMinutes, true, false);

        var results = new List<SearchResultItem>();
        foreach (var searchDefinition in searchOption.SearchDefinitions)
        {
            results.AddRange(dal.Search(searchDefinition, searchStringRaw, dbFilter.FilterID));
        }
        pgItems.DataSource = results;

        resultsLimitedDiv.Visible = pgItems.DataSourceCount >= ModuleConfig.MaxSearchResults;

        pgItems.AllowPaging = true;
        pgItems.PageSize = ModuleConfig.MaxSearchResultsPerPage;

        if (PageNumber >= pgItems.PageCount - 1)
            PageNumber = pgItems.PageCount - 1;

        pgItems.CurrentPageIndex = PageNumber;
        if (pgItems.PageCount > 1)
        {
            pagerPanel.Visible = true;
            ArrayList pages = new ArrayList();
            for (int i = 1; i < pgItems.PageCount + 1; i++)
                pages.Add(i.ToString());
            rptPages.DataSource = pages;
            rptPages.DataBind();
        }
        else
            pagerPanel.Visible = false;

        if (pgItems.PageCount < 2)
        {
            nextButton.Visible = false;
            prevButton.Visible = false;
            firstButton.Visible = false;
            lastButton.Visible = false;
        }

        ExpandResultsOnOneItem(pgItems);

        rptSearchItems.DataSource = pgItems;
        rptSearchItems.DataBind();

        if (((IList)pgItems.DataSource).Count <= 0)
        {
            string message = string.Format("<br><font color='#D50000' size=2>{0}</font>", Resources.NTAWebContent.NTAWEBCODE_VB1_3);

            lblNoDataLabel.Text = string.Format(message, WebSecurityHelper.SanitizeHtmlV2(Server.HtmlEncode(searchString)), searchOption.Text);
            lblNoDataLabel.Visible = true;
        }
    }

    private void ExpandResultsOnOneItem(PagedDataSource pgItems)
    {
        if (pgItems.Count != 1)
        {
            return;
        }
        collapseMainExpander = false;

        var resultsEnumerator = pgItems.DataSource.GetEnumerator();
        resultsEnumerator.MoveNext();
        var result = (SearchResultItem)resultsEnumerator.Current;
        var hasOnlyOneNode = result?.SubItems.Count == 1;
        if (hasOnlyOneNode)
        {
            collapseNodeExpander = false;
        }
    }

    protected void DisplaySearchHints(SearchValidationResult.ErrorTypes errorCode)
    {
        switch (errorCode)
        {
            case SearchValidationResult.ErrorTypes.PostfixOrPrefixError:
                PrefixPostfixSearchHintsDiv.Visible = true;
                break;
            case SearchValidationResult.ErrorTypes.PortError:
                portSearchHintsDiv.Visible = true;
                break;
            case SearchValidationResult.ErrorTypes.IPv4Error:
                IPv4SearchHintsDiv.Visible = true;
                break;
            case SearchValidationResult.ErrorTypes.IPv6Error:
                IPv6SearchHintsDiv.Visible = true;
                break;
            default:
                WildcardSearchHintsDiv.Visible = true;
                break;
        }
    }

    protected string GetItemDisplayName()
    {
        var dataItem = (SearchResultItem)this.GetDataItem();
        return dataItem.SearchDefinition.GetDisplayName(dataItem);
    }

    protected string GetSummaryItemLink()
    {
        var dataItem = (SearchResultItem)this.GetDataItem();
        return dataItem.SearchDefinition.GetSummaryItemLink(dataItem, timePeriodNameUTC);
    }

    protected string GetDetailItemLink(string netFlowSource)
    {
        var dataItem = (SearchResultItem)this.GetDataItem();
        return dataItem.SearchDefinition.GetDetailItemLink(dataItem, netFlowSource, timePeriodNameUTC);
    }

    protected string GetIconHtml()
    {
        var dataItem = (SearchResultItem)this.GetDataItem();
        return dataItem.SearchDefinition.GetIconHtml(dataItem);
    }

    protected string GetSearchLabel()
    {
        return string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_4, WebSecurityHelper.SanitizeHtmlV2(Server.HtmlEncode(searchString)), searchOption.Text, timePeriodNameLocal);
    }

    protected string GetHintLabel()
    {
        return string.Format(Resources.NTAWebContent.NTAWEBCODE_AK2_1, searchOption.Text);
    }


    protected string HighlightSearchString(string text)
    {
        int startIndex = 0;
        int searchStringIndex;
        string result = string.Empty;
        string highlightString = searchString.Trim('*');

        while ((searchStringIndex = text.IndexOf(highlightString, startIndex, StringComparison.InvariantCultureIgnoreCase)) >= 0
               && startIndex < text.Length
               && searchStringIndex + highlightString.Length <= text.Length)
        {
            result += text.Substring(startIndex, searchStringIndex - startIndex) + "<span class=\"searchStringHighlight\">" +
                      text.Substring(searchStringIndex, highlightString.Length) + "</span>";

            startIndex = searchStringIndex + highlightString.Length;
        }

        result += text.Substring(startIndex);
        return result;
    }

    public int PageNumber
    {
        get
        {
            if (ViewState["PageNumber"] != null)
                return Convert.ToInt32(ViewState["PageNumber"]);

            return 0;
        }
        set
        {
            ViewState["PageNumber"] = value;
        }
    }

    protected void FirstBtnClick(object sender, EventArgs e)
    {
        PageNumber = 0;
        LoadData();
    }

    protected void LastBtnClick(object sender, EventArgs e)
    {
        PageNumber += 9999;
        LoadData();
    }

    protected void NextBtnClick(object sender, EventArgs e)
    {
        PageNumber++;
        LoadData();
    }

    protected void PrevBtnClick(object sender, EventArgs e)
    {
        if (PageNumber > 0)
            PageNumber--;

        LoadData();
    }

}
