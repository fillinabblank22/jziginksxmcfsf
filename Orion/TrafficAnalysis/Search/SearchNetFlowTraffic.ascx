﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchNetFlowTraffic.ascx.cs" Inherits="Orion_TrafficAnalysis_SearchResults_SearchNetFlowTraffic" %>
<%@ Register TagPrefix="netflow" TagName="PeriodMenu" Src="~/Orion/TrafficAnalysis/TimePeriod/PeriodMenu.ascx" %>

<script type="text/javascript">

    function PerformSearch() {
        var searchString = $("#<%=searchString.ClientID %>").val();

        if (IsValidString(searchString)) {
            $("#<%=btnSearch.ClientID %>").hide();
            $("#<%=imgLoading.ClientID %>").show();

            return true;
        } else {
            return false;
        }
    }
    
    function IsValidString(searchString) {
        return searchString && !$("#<%=searchString.ClientID %>").attr('presetWatermark') && searchString.replace(/\'/g, "") != "";
    }

</script>


<div id="SearchNetFlowTraffic" >
    <div class="vert-block-with-padding">
        <span id="Search1Label" class="menuFilterLabel"><%=  Resources.NTAWebContent.NTAWEBCODE_Search_Label %></span> 
        <br />
        <asp:TextBox runat="server" ID="searchString" Width="140px" Rows="1" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_8%>"/>
        <asp:Label runat="server" ID="lblBy" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_9%>"></asp:Label>
        <asp:ListBox runat="server" ID="searchField" Rows="1" SelectionMode="Single"/>
    </div>
    <div class="vert-block-with-padding">
        <netflow:PeriodMenu ID="searchPeriodMenu" runat="server" TimePeriod="Last 15 Minutes" NoLeftMargin="true" AlignRight="true" NoBold="true" DontPostbackWhenSubmit="true" />
        <asp:ImageButton runat="server" style="height:21px; width:21px;" ID="btnSearch" ImageUrl="/Orion/TrafficAnalysis/images/SmallButton.Search2.gif" OnClientClick="return PerformSearch();"  OnClick="btnSearch_Click" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_74%>" />
        <asp:ImageButton runat="server" id="imgLoading" style="display:none; height:16px; width:16px; padding-top:2px;" src="/Orion/images/loading_gen_small.gif" />
    </div>
</div>


