<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowConversationDetails.aspx.cs" Inherits="Orion_Netflow_ConversationDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="netflow" TagName="ConversationDescriptor" Src="~/Orion/TrafficAnalysis/Controls/ConversationDescriptor.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
        <asp:Label runat="server">                                
            <netflow:ConversationDescriptor ID="ConversationDescriptor" runat="server" NotInUpdatePanel="true" /> 
        </asp:Label>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:ConversationResourceHost runat="server" ID="convResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:ConversationResourceHost>
</asp:Content>