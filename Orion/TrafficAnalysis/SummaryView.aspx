<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" CodeFile="SummaryView.aspx.cs" Inherits="Orion_Netflow_Summary" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_125%>" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="NetflowContent">
    <orion:ResourceHostControl runat="server" ID="sumResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </orion:ResourceHostControl>
</asp:Content>