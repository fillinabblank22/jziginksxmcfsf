using System;
using System.Web.UI;
using System.Web.Services;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_Netflow_ApplicationDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow Application"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.appResHost.NetflowApplication = (NetflowApplication)this.NetObject;

        this.Page.Form.Attributes["autocomplete"] = "off";
        base.OnInit(e);


        this.Page.Title = this.ViewInfo.ViewTitle + " - " + AppName();
    }

    /// <summary>
    /// Application name and port if it is single port application
    /// </summary>
    /// <returns></returns>
    public string AppName()
    {
        return (this.NetObject as NetflowApplication).AppNameAndPort;
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
