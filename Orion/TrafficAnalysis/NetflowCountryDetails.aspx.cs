using System;
using System.Web.UI;
using System.Web.Services;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_Netflow_CountryDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow Country"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.countryResHost.NetflowCountry = this.NetObject as NetflowCountry;

        this.Page.Form.Attributes["autocomplete"] = "off";

        base.OnInit(e);
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);        
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if ( SolarWinds.Netflow.Web.OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
