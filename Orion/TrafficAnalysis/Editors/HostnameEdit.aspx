<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" CodeFile="HostnameEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Editors_HostnameEdit" %>

<asp:Content ID="Content1"  ContentPlaceHolderID="MainContent" runat="server">
	<div class="EditPage">
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table Class="text" border="0" cellpadding="4" cellspacing="0">
				<tr>
					<td colspan="10" class="PageHeader"><%= String.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_34, ipAddress)%></td>
				</tr>
				<tr>
					<td colspan="2">
					    <asp:TextBox runat="server" ID="txtHostname" Columns="30" />					    
                        <asp:RequiredFieldValidator 
					        ID="valRequired" 
					        runat="server" 
					        ControlToValidate="txtHostname" 
					        EnableClientScript="false"
					        Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_45%>"
					        />
                        <asp:CustomValidator 
                            ID="lengthValidator"
                            runat="server"
                            ControlToValidate="txtHostname"
                            OnServerValidate="LengthValidation"
                            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_VB0_46%>"
                            />
                        <br/>
                        <asp:CustomValidator 
                            ID="cantBeIpAddressValidator"
                            runat="server"
                            ControlToValidate="txtHostname"
                            OnServerValidate="FormatValidation"
                            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_EditHostName_CannotLookLikeIpAddress%>"
                            />                        
					</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>			
					<td colspan="2">
					<orion:LocalizableButton ID="Submit" runat="server" LocalizedText="Submit" DisplayType="Primary" OnClick="SubmitClick" />
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		
	</div>
</asp:Content>