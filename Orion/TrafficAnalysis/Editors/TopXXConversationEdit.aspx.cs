using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Internationalization.Exceptions;


public partial class Orion_TrafficAnalysis_Resources_NetFlowConversation_TopXXConversationEdit : EditPageBase
{
    private const int MAX_MESSAGES = 100;
    private const string ROW_COUNT = "25"; // default
    private string resourceID;
    
    private ResourceInfo _resource;

    #region Properties

    public new string Title
    {
        get { return this._resource.Title; }
        set 
        {
            if (!String.IsNullOrEmpty(value))
            {
                this._resource.Title = value;
            }
            else
            {
                var resourceControl = (BaseResourceControl)LoadControl(_resource.File);
                _resource.Title = resourceControl.Title;
            }
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(this._resource);
        }
    }

    public string MaxMessages
    {
        get 
        {
            string maxMessages = this._resource.Properties["MaxMessages"];
            return string.IsNullOrEmpty(maxMessages) ? ROW_COUNT : maxMessages;
        }
        set { this._resource.Properties["MaxMessages"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceID = Request.Params["ResourceID"];

        if (string.IsNullOrEmpty(resourceID))
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_115);
        }

        this._resource = ResourceManager.GetResourceByID(Convert.ToInt32(this.resourceID));

        errorMessage.Visible = false;

        if (IsPostBack)
        {
            string maxMessages = Request.Params["Property-MaxMessages"];

            if (!ValidateMaxMessages(maxMessages))
            {
                return;
            }

            MaxMessages = maxMessages;
            DoRedirect();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    private bool ValidateMaxMessages(string maxMessages)
    {
        int count;
        if (int.TryParse(maxMessages, out count))
        {
            if (count > 0 && count <= MAX_MESSAGES)
            {
                errorMessage.Visible = false;
                return true;
            }
        }
        errorMessage.Visible = true;
        return false;
    }
}
