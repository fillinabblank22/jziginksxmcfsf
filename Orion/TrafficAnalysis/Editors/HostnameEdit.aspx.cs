using System;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.DAL;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;

public partial class Orion_TrafficAnalysis_Editors_HostnameEdit : EditPageBase
{
    protected string ipAddress;

    protected void Page_Load(object sender, EventArgs e)
    {
        string addr = this.Request.QueryString["Address"];
        System.Net.IPAddress ipAddr = null;
        if (!System.Net.IPAddress.TryParse(addr, out ipAddr))
        {
            throw new InvalidOperationException("Input IP Address is improperly formatted.");
        }

        this.ipAddress = ipAddr.ToString();
        if (!IsPostBack)
        {
            this.txtHostname.Text = Request.QueryString["Hostname"];
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        this.Page.Validate();
        if (this.Page.IsValid)
        {
            HostnameEditDAL.UpdateHostname(this.txtHostname.Text, this.Request.QueryString["Address"]);
            DoRedirect();
        }
    }

    protected void LengthValidation(object source, ServerValidateEventArgs arguments)
    {
        arguments.IsValid = arguments.Value.Length <= 200;
    }

    protected void FormatValidation(object source, ServerValidateEventArgs arguments)
    {
        string value = arguments.Value;
        EndpointFilter.Endpoint endpoint = EndpointFilter.GetEndpoint(value, FiltersNameProvider.Instance);

        arguments.IsValid = (endpoint.EndpointType == EndpointFilter.EndpointType.HostName) || this.ipAddress.Equals(endpoint.GetIpAddress(), StringComparison.OrdinalIgnoreCase);
    }
}
