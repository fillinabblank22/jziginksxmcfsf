﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;

public partial class Orion_TrafficAnalysis_RemoveResource : Page
{
    protected Dictionary<int, int> _checkBoxes = new Dictionary<int, int>();

    /// <summary>
    /// Used for comparing in resources. Should has exact name of the resource(e.g. "What's New in Orion NTA")
    /// </summary>
    public string ResourceName { get; set; }

    /// <summary>
    /// Used in GUI (e.g. "What's New")
    /// </summary>
    public string ResourceNameShort { get; set; }

    public string ResourceFile { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (string.IsNullOrEmpty(Request.QueryString["ResourceName"]) == false)
        {
            ResourceName = Request.QueryString["ResourceName"].Replace("\"", "").Trim();
        }

        if (string.IsNullOrEmpty(Request.QueryString["ResourceNameShort"]) == false)
        {
            ResourceNameShort = Request.QueryString["ResourceNameShort"].Replace("\"","").Trim();
        }

        if (string.IsNullOrEmpty(Request.QueryString["ResourceFile"]) == false)
        {
            ResourceFile = Request.QueryString["ResourceFile"].Trim();
        }

        ViewInfoCollection views = ViewManager.GetAllViews();

        int countOfCheckBoxes = 0;
        ResourceInfoCollection resources;

        foreach (ViewInfo info in views)
        {
            resources = ViewManager.GetResourcesForView(info);

            foreach (ResourceInfo resInfo in resources)
            {
                if (0 == string.Compare(resInfo.File, ResourceFile, StringComparison.OrdinalIgnoreCase))
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = "CheckBox" + countOfCheckBoxes.ToString();
                    cb.Checked = true;
                    LiteralControl lc = new LiteralControl(info.ViewTitle + "<br />");
                    this.CheckBoxDiv.Controls.Add(cb);
                    this.CheckBoxDiv.Controls.Add(lc);
                    _checkBoxes[countOfCheckBoxes] = resInfo.ID;
                    countOfCheckBoxes++;
                }
            }
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        CheckBox tempCheckBox;

        int position = 0;
        foreach (Control checkies in this.CheckBoxDiv.Controls)
        {
            if (checkies.GetType().Name == "CheckBox")
            {
                tempCheckBox = (CheckBox)checkies;
                if (tempCheckBox.Checked == true)
                {
                    ResourceManager.DeleteById(_checkBoxes[position]);
                }
                position++;
            }
        }

        Response.Redirect("/Orion/TrafficAnalysis/SummaryView.aspx");
    }
}
