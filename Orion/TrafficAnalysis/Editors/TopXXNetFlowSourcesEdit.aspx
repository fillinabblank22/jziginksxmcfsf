<%@ Page Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" AutoEventWireup="true" CodeFile="TopXXNetFlowSourcesEdit.aspx.cs" Inherits="NetFlow_TopXXReports_TopXXNetFlowSourcesEdit" %>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta" TagName="NtaHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="HelpButton9" runat="server" HelpUrlFragment="OrionNetFlowPHTopNetFlowSource" Title="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_39%>"  />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
   <style type="text/css">
    .nfsBreadcrumb { margin-left: 10px !Important; }
   </style>
  <div class="sw-res-editor">
	<h1> <%= Title %> </h1>
    
    <div>
        
        <div style="display:none">
            <b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_35%></b><br/>
            <input type="text" name="Property-Title" size="30" value="<%= DisplayTitle %>" title="<%= Resources.NTAWebContent.NTAWEBDATA_TM0_35%>"/>
	    </div>
	
	
	    <div>
	        <b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_38%></b><br>
	        <input name="Property-MaxMessages" value="<%=MaxMessages%>" title="<%= Resources.NTAWebContent.NTAWEBDATA_TM0_36%>"/>
	        <asp:Label id="errorMessage" Font-Size="10pt" style="color:red" visible="false" runat="server"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_37%></asp:Label>
    	
	    </div>
	
	</div>  <%--Resource wrapper DIV END--%>
	<div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary" />
	</div>

    <input type=hidden id=viewPathHolder runat=server />
    <input type=hidden id=resourceIdHolder runat=server />
 </div>
</asp:Content>
