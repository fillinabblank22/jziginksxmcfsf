<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master"
    CodeFile="AllNetflowSourcesEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_Edit_NetFlow_Sources_Settings" %>

<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta"
    TagName="NtaHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="HelpButton9" runat="server" HelpUrlFragment="OrionNetFlowPHSummaryActiveSources"
        Title="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_2%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        .nfsBreadcrumb { margin-left: 10px !Important; }
    </style>
    <div class="sw-res-editor">
        <h1> <%= Resources.NTAWebContent.NTAWEBDATA_AK0_1%></h1>
        <div>
            <div>
                <b><%= Resources.NTAWebContent.NTAWEBDATA_AK0_3%></b><br/>
                <input type="text" name="Property-Title" size="30" value="<%=Title%>" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_3%>"/>
            </div>
        </div>
        <div class="sw-btn-bar">
            <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary"/>
        </div>
    </div>
</asp:Content>
