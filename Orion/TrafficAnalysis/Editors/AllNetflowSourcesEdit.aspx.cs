using System;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Internationalization.Exceptions;

public partial class Orion_TrafficAnalysis_Admin_Edit_NetFlow_Sources_Settings : EditPageBase
{
    private string resourceId;
    private ResourceInfo resource;
    
    public new string Title
    {
        get { return resource.Title; }
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                resource.Title = value;
            }
            else
            {
                var resourceControl = (BaseResourceControl)LoadControl(resource.File);
                resource.Title = resourceControl.Title;
            }
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(resource);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceId = Request.Params["ResourceID"];

        if (string.IsNullOrEmpty(resourceId))
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_115);
        }
        resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceId));

        if (!IsPostBack)
        {
            return;
        }

        Title = Request.Params["Property-Title"];

        DoRedirect();
    }
}
