﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" 
AutoEventWireup="true" CodeFile="RemoveResource.aspx.cs" Inherits="Orion_TrafficAnalysis_RemoveResource" %>

<asp:Content ID="ContentPlaceHolder1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="PageHeader" id="etxxHeader" style="width: auto;">
        <%= String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_4, ResourceNameShort) %>
    </div>

    <div class="ResourceWrapper" id="etxxRW">
        <div class="etxxLH">
            <div runat="server" id="CheckBoxDiv">
            </div>
            <br />
            <%= String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_5,
                                ResourceNameShort,
                                String.Format("<a href=\"/Orion/Admin/ListViews.aspx\" title=\"{0}\" style=\"color: #86601c; text-decoration: underline;\">", 
                                                Resources.NTAWebContent.NTAWEBCODE_TM0_6),
                                "</a>" )%>
            <br />
        </div>
    </div>
    <div class="sw-btn-bar">
        <orion:LocalizableButton CssClass="etxxBtn" runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="SubmitClick" DisplayType="Primary" />
    </div>
</asp:Content>

