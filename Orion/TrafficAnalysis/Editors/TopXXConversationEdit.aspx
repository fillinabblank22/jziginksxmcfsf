<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" CodeFile="TopXXConversationEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowConversation_TopXXConversationEdit" %>

<asp:Content ID="Content1"  ContentPlaceHolderID="MainContent" runat=server>
	<div class="EditPage">
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				<table Class="text" border="0" cellpadding="4" cellspacing="0">
				<tr>
					<td colspan="10" class="PageHeader"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_42%> <%=Title%></td>
				</tr>
				<tr>
					<td colspan=2>
						<b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_43%></b><br>
						<input name="Property-MaxMessages" size='5' value="<%=MaxMessages%>">
						<asp:Label id="errorMessage" Font-Size="10pt" style="color:red" visible="false" runat="server"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_44%></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>			
					<td colspan=2>
                        <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="primary" ID="submitBtn" />
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
</asp:Content>