using System;

using SolarWinds.Orion.Web;

using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Internationalization.Exceptions;

public partial class NetFlow_TopXXReports_TopXXNetFlowSourcesEdit : EditPageBase
{
    private string resourceID;
    private ResourceInfo _resource;
    private const int MAX_MESSAGES = 100;
    private const string ROW_COUNT = "10"; // default

    #region Properties

    public string DisplayTitle
    {
        get { return this._resource.Title; }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                this._resource.Title = value;
            }
            else
            {
                var resourceControl = (BaseResourceControl)LoadControl(_resource.File);
                _resource.Title = resourceControl.Title;
            }
            SolarWinds.Orion.Web.DAL.ResourcesDAL.Update(this._resource);
        }
    }

    public string MaxMessages
    {
        get 
        {
            string max = this._resource.Properties["MaxMessages"];
            if (max != null) return max;
            return ROW_COUNT;
        }
        set { this._resource.Properties["MaxMessages"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        resourceID = Request.Params["ResourceID"];

        if (string.IsNullOrEmpty(resourceID))
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_115);
        }
        this._resource = ResourceManager.GetResourceByID(Convert.ToInt32(resourceID));

        errorMessage.Visible = false;

        if (IsPostBack)
        {
            string maxMessages = Request.Params["Property-MaxMessages"];

            if (ValidateMaxMessages(maxMessages))
            {
                string title = Request.Params["Property-Title"];

                DisplayTitle = title;
                MaxMessages = maxMessages;
                DoRedirect();
            }
        }

        Page.Title = String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_15, DisplayTitle);
    }

    private bool ValidateMaxMessages(string maxMessages)
    {
        int count;
        if (int.TryParse(maxMessages, out count))
        {
            if (count > 0 && count <= MAX_MESSAGES)
            {
                errorMessage.Visible = false;
                return true;
            }
        }
        errorMessage.Visible = true;
        return false;
    }
}
