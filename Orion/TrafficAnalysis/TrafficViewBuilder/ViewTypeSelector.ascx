﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewTypeSelector.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeSelector" %>
<%@ Register TagPrefix="tvb" TagName="ViewTypeSummary" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/ViewTypeSummary.ascx" %>
<%@ Register TagPrefix="tvb" TagName="ViewTypeDetail" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/ViewTypeDetail.ascx" %>

<div id="summaryDiv">
    <asp:RadioButton Text="&nbsp;<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_110%>" ID="summaryRb" GroupName="viewTypeRbg" runat="server" />
    <div id="summaryContentDiv" class="RadioButtonDivContent" style="padding-top: 5px" runat="server">
        <tvb:ViewTypeSummary ID="viewTypeSummary" runat="server" />
    </div>
</div>

<div id="detailDiv" style="padding-top: 5px">
    <asp:RadioButton Text="&nbsp;<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_109%>" ID="detailRb" GroupName="viewTypeRbg" runat="server" />
    <input id="hiddenGlobalFilterID" type="hidden" value="" runat="server" />
    <input id="detailAsyncStatus" type="hidden" value="" runat="server" />

    <div id="detailContentDiv" class="RadioButtonDivContent" style="padding-top: 5px" runat="server">
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="loadDetailUiContentButton" runat="server" OnClick="loadDetailUiContentButton_Click" Style="display: none;" />
                <asp:Button ID="updateAlertBuilderButton" runat="server" OnClick="UpdateAlertBuilderButton_Click" Style="display: none;" />
                <div id="detailContentLoadingDiv" visible="true" runat="server">
                    <asp:Image ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server" />&nbsp;&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_97%>
                </div>
                <div id="detailContentLoadedDiv" runat="server" visible="false">
                    <tvb:ViewTypeDetail ID="viewTypeDetail" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $rbSummary = $('#<%=summaryRb.ClientID%>')
        $rbDetail = $('#<%=detailRb.ClientID%>')

        $rbSummary.click(function () { showHideRadioButtonsContent(500); });
        $rbDetail.click(function () { showHideRadioButtonsContent(500); });

        showHideRadioButtonsContent(0);

        var firstTimeExecuted = false;
        function showHideRadioButtonsContent(fadeTime) {

            if ($rbSummary.attr('checked')) {
                $('#<%=summaryContentDiv.ClientID%>').show(fadeTime);
                $('#<%=detailContentDiv.ClientID%>').hide(fadeTime);
            }
            else {
                $('#<%=summaryContentDiv.ClientID%>').hide(fadeTime);
                $('#<%=detailContentDiv.ClientID%>').show(fadeTime);

                var detailLoadStatus = document.getElementById("<%=detailAsyncStatus.ClientID%>");
                if (detailLoadStatus.value == '') {
                    detailLoadStatus.value = 'Loaded';
                    $('#<%=loadDetailUiContentButton.ClientID %>').click();
                }
            }

            showHideGlobalFiltersAccordingToDetail();
            if (firstTimeExecuted) {
                $('#<%=updateAlertBuilderButton.ClientID %>').click();
            }
            firstTimeExecuted = true;
        }
    });

    function showHideGlobalFiltersAccordingToDetail() {

        var hiddenGlobalFilter = document.getElementById('<%=this.hiddenGlobalFilterID.ClientID %>');
            var shownDetailFilter = document.getElementById('<%=this.DetailShownFilterHiddenFieldClientID %>');

        if (hiddenGlobalFilter.value != '') {
            document.getElementById(hiddenGlobalFilter.value).style.display = 'block';
            hiddenGlobalFilter.value = '';
        }

        if (shownDetailFilter != null && shownDetailFilter.value != '' && $rbDetail.attr('checked')) {
            var elementToHideID = 'CategoryDiv_' + shownDetailFilter.value;
            var elementToHide = document.getElementById(elementToHideID);
            if (elementToHide != null) {
                hiddenGlobalFilter.value = elementToHideID;
                elementToHide.style.display = 'none';
            }
        }
    }
</script>
