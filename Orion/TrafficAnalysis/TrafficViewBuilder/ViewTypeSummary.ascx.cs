﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using System.Collections.Specialized;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Globalization;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeSummary : UserControl
{
    private const string defaultViewName = ViewKeys.Summary.NtaSummary;

    /// <summary>
    /// Key: ViewKey, Value: summary view short title 
    /// </summary>
    private static Dictionary<string, string> viewShortTitles = null;

    /// <summary>
    /// This dictionary contains ViewKey (non-internationalized) to view short title mapping
    /// </summary>
    private static Dictionary<string, string> ViewShortTitles
    {
        get
        {
            if (viewShortTitles == null)
            {
                viewShortTitles = new Dictionary<string, string>();

                using (LocaleThreadState.EnsurePrimaryLocale())
                {
                    viewShortTitles.Add(ViewKeys.Summary.NtaSummary, Resources.NTAWebContent.ViewShortTitle_nta_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Applications, Resources.NTAWebContent.ViewShortTitle_applications_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Conversations, Resources.NTAWebContent.ViewShortTitle_conversations_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Countries, Resources.NTAWebContent.ViewShortTitle_countries_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Endpoints, Resources.NTAWebContent.ViewShortTitle_endpoints_summary);
                    viewShortTitles.Add(ViewKeys.Summary.IpGroups, Resources.NTAWebContent.ViewShortTitle_ip_groups_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Protocols, Resources.NTAWebContent.ViewShortTitle_protocols_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Receivers, Resources.NTAWebContent.ViewShortTitle_receivers_summary);
                    viewShortTitles.Add(ViewKeys.Summary.TypesOfService, Resources.NTAWebContent.ViewShortTitle_tos_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Transmitters, Resources.NTAWebContent.ViewShortTitle_transmitters_summary);
                    viewShortTitles.Add(ViewKeys.Summary.Bgp, Resources.NTAWebContent.ViewShortTitle_bgp_summary);
                }
            }

            return viewShortTitles;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
        {
            //Load list of summary views
            ViewInfoCollection summaryViews = ViewManager.GetViewsByType(ViewTypeNames.NetFlowSummary);
            foreach (var view in summaryViews)
            {
                string displayText = null;

                if (view.IsSystem && ViewShortTitles.ContainsKey(view.ViewKey))
                {
                    displayText = ViewShortTitles[view.ViewKey];
                }
                else
                {
                    displayText = view.ViewTitle;
                }

                // use ViewID as value with exception of NTA summary view, where it's ViewKey is used as value
                // this is because we don't need ViewID for summary view (it's referenced directly), but we need 
                // to be able to preselect it easily.
                bool isNtaSummary = view.IsSystem && view.ViewKey.Equals(ViewKeys.Summary.NtaSummary, StringComparison.InvariantCultureIgnoreCase);
                string val = isNtaSummary ? defaultViewName : view.ViewID.ToString(CultureInfo.InvariantCulture);

                this.summaryDropDown.Items.Add(new ListItem(displayText, val));
            }
        }
    }
    
    /// <summary>
    /// ViewID or ViewKey for NTA summary
    /// </summary>
    private string SelectedView
    {
        get
        {
            return this.summaryDropDown.SelectedValue;
        }
        set
        {
            for (int i = 0; i < this.summaryDropDown.Items.Count; i++)
            {
                if (this.summaryDropDown.Items[i].Value.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    this.summaryDropDown.SelectedIndex = i;
                    break;
                }
            }
        }
    }


    public void LoadFromInfo(ViewTypeLoadInfo loadInfo)
    {
        string viewId = loadInfo.ViewId;
        if (string.IsNullOrEmpty(viewId))
        {
            viewId = defaultViewName;
        }
        this.SelectedView = viewId;
    }

    public IFilterCollection GetFilters()
    {
        var filters = FiltersHelper.CreateUniversalFilterCollection();

        return filters;
    }

    public string GetUrlAndFilters(out UniversalFilterCollection filters)
    {
        filters = FiltersHelper.CreateUniversalFilterCollection();

        string viewNameParam = this.SelectedView != defaultViewName ? "?viewid=" + SelectedView + "&" : "?";
        return String.Format("/Orion/TrafficAnalysis/SummaryView.aspx{0}NetObject=NSF:", viewNameParam);

    }
}