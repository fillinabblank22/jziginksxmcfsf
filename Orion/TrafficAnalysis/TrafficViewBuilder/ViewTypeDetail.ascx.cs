﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Data;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Orion.Core.Common.i18n;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Netflow.Common.Filters;
using System.IO;
using System.Globalization;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeDetail : UserControl
{
    protected const string TruncatedCaptionColumnName = "TruncatedCaption";
    private struct FilterEditorDefinition
    {
        public string ControlPath; //Path to FilterEdit ascx control
        public string DetailViewType; //View type for which is this control intended
        public int MaxFiltersCount; //Max count of filters which can be specified. 0 means default which is defined in FilterTracker.
    }

    /// <summary>List of relations between DETAIL VIEW TYPE and appropriate FILTER EDITOR PATH</summary>    
    private static readonly List<FilterEditorDefinition> filterEditorDefinitions = new List<FilterEditorDefinition>()
    {
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ApplicationsEditor.ascx", 
                                     DetailViewType="NetFlow Application", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AdvancedApplicationsEditor.ascx", 
                                     DetailViewType="NetFlow Advanced Application", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AutonomousSystemsEditor.ascx", 
                                     DetailViewType="NetFlow Autonomous Systems", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AutonomousSystemConversationsEditor.ascx", 
                                     DetailViewType="NetFlow Autonomous System Conversations", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/CBQoSClassMapEditor.ascx", 
                                     DetailViewType="CBQoS", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ConversationsEditor.ascx", 
                                     DetailViewType="NetFlow Conversation", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/CountriesEditor.ascx", 
                                     DetailViewType="NetFlow Country", MaxFiltersCount=1},
        
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/DomainsEditor.ascx", 
                                     DetailViewType="NetFlow Domain", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/EndpointsEditor.ascx", 
                                     DetailViewType="NetFlow Endpoint", MaxFiltersCount=1},
        
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/IPGroupsEditor.ascx", 
                                     DetailViewType="NetFlow IP Address Group", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/IPAddressGroupsConversationEditor.ascx", 
                                     DetailViewType="NetFlow Groups Conversation", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ProtocolsEditor.ascx", 
                                     DetailViewType="NetFlow Protocol", MaxFiltersCount=1},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/TosEditor.ascx", 
                                     DetailViewType="NetFlow Type of Service", MaxFiltersCount=1}
    };

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private static Dictionary<string, string> viewTypeShortNames = null;

    /// <summary>
    /// Dictionary contains ViewType to i18ned view type short title mapping
    /// </summary>
    private static Dictionary<string, string> ViewTypeShortNames
    {
        get
        {
            if (viewTypeShortNames == null)
            {
                viewTypeShortNames = new Dictionary<string, string>();

                using (LocaleThreadState.EnsurePrimaryLocale())
                {
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowNodeDetails, Resources.NTAWebContent.ViewTypeShortName_node_details);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowInterfaceDetails, Resources.NTAWebContent.ViewTypeShortName_interface_details);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowApplication, Resources.NTAWebContent.ViewTypeShortName_application);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowAdvancedApplication, Resources.NTAWebContent.ViewTypeShortName_advanced_application);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowConversation, Resources.NTAWebContent.ViewTypeShortName_conversation);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowCountry, Resources.NTAWebContent.ViewTypeShortName_country);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowDomain, Resources.NTAWebContent.ViewTypeShortName_domain);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowEndpoint, Resources.NTAWebContent.ViewTypeShortName_endpoint);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowIpGroup, Resources.NTAWebContent.ViewTypeShortName_ip_group);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowProtocol, Resources.NTAWebContent.ViewTypeShortName_protocol);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowTypeOfService, Resources.NTAWebContent.ViewTypeShortName_type_of_service);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowAutonomousSystems, Resources.NTAWebContent.ViewTypeShortName_autonomous_systems);
                    viewTypeShortNames.Add(ViewTypeNames.NetFlowAutonomousSystemConversations, Resources.NTAWebContent.ViewTypeShortName_autonomous_system_conversations);
                    viewTypeShortNames.Add(ViewTypeNames.CBQoS, Resources.NTAWebContent.ViewTypeShortName_cbqos);
                }
            }

            return viewTypeShortNames;
        }
    }

    private NetflowView view;

    /// <summary>Holds list of instances of filter editors - key is viewtype</summary>
    private Dictionary<string, TvbFilterEditorWithTrackerBase> filterEditors = new Dictionary<string, TvbFilterEditorWithTrackerBase>();


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnInit(EventArgs e)
    {
        view = this.Page as NetflowView;

        base.OnInit(e);
        this.BuildFilterEditors();

        this.nodeDdl.Attributes.Add("onChange", String.Format("javascript: $('#{0}').hide();$('#{1}').show()", interfaceDdl.ClientID, interfaceLoadingDiv.ClientID));        
    }

    /// <summary>
    /// Builds filter editors on UI according to filterEditorDefinitions list.
    /// </summary>
    /// <param name="filters">Current filters which will be shown in editors as current values</param>
    private void BuildFilterEditors()
    {
        foreach (FilterEditorDefinition def in filterEditorDefinitions)
        {
            TvbFilterEditorWithTrackerBase editor = (TvbFilterEditorWithTrackerBase)Page.LoadControl(def.ControlPath);
            editor.ID = "Detail_" + Path.GetFileNameWithoutExtension(def.ControlPath);
            editor.EnableExcludeOption = false;
            if (def.MaxFiltersCount > 0)
                editor.MaxFiltersCount = def.MaxFiltersCount;
            this.filterEditDiv.Controls.Add(editor);

            this.filterEditors.Add(def.DetailViewType, editor);
        }
    }

    protected void loadFilterUiContentButton_Click(object sender, EventArgs e)
    {
        TvbFilterEditorWithTrackerBase editor = GetVisibleFilterEditor();

        if (editor != null)
        {
            if (editor is TvbFilterDetailBaseWithTrackerBase)
                (editor as TvbFilterDetailBaseWithTrackerBase).LoadUiContent(this.SelectedNodeID, this.SelectedInterfaceID);
            else
                editor.LoadUiContent();            
        }
    }

    protected void viewTypeDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        string viewType = viewTypeDdl.SelectedValue;

        //Load view names
        ViewInfoCollection detailViews = ViewManager.GetViewsByType(viewType);
        viewNameDdl.Items.Clear();
        foreach (ViewInfo view in detailViews)
        {
            viewNameDdl.Items.Add(new ListItem(view.ViewTitle, view.ViewID.ToString(CultureInfo.InvariantCulture)));
        }
        viewNameDiv.Visible = viewNameDdl.Items.Count > 1;

        //Show / hide filter editors
        ShowFilterEditorsBySelectedViewType();

        //Show / hide interfaces
        if (viewType == ViewTypeNames.NetFlowNodeDetails)
        {
            this.interfaceDiv.Visible = false;
        }
        else
        {
            this.interfaceDiv.Visible = true;
            AddRemoveAllInterfacesOptionInInterfacesList(false);
        }
    }

    protected void nodeDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.interfaceDdl.Items.Clear();

        this.interfaceDdl.DataSource = DataTableHelper.AddColumnWithTruncatedStringFor(TrafficViewBuilderDAL.GetInterfaces(SelectedNodeID, true), "Caption", TruncatedCaptionColumnName);
        this.interfaceDdl.DataBind();
        AddRemoveAllInterfacesOptionInInterfacesList(true);

        TvbFilterDetailBaseWithTrackerBase detailEditor = GetVisibleFilterEditor() as TvbFilterDetailBaseWithTrackerBase;

        if (detailEditor != null)
        {
            this.LoadFilterUiContent(detailEditor);
        }

        view.UpdateFiltersInAlertBuilder();
    }

    protected void interfaceDdl_SelectedIndexChanged(object sender, EventArgs e)
    {
        TvbFilterDetailBaseWithTrackerBase detailEditor = GetVisibleFilterEditor() as TvbFilterDetailBaseWithTrackerBase;
        if (detailEditor != null && detailEditor.RequireInterfaceId)
            this.LoadFilterUiContent(detailEditor);

        view.UpdateFiltersInAlertBuilder();
    }

    protected TvbFilterEditorWithTrackerBase GetVisibleFilterEditor()
    {
        foreach (var editor in this.filterEditors.Values)
        {
            if (editor.Visible)
            {
                return editor;
            }
        }
        return null;
    }

    private void AddRemoveAllInterfacesOptionInInterfacesList(bool justLoaded)
    {

        ListItem item = justLoaded ? null : interfaceDdl.Items.FindByValue("0");

        TvbFilterDetailBaseWithTrackerBase detailFilterEditor = GetVisibleFilterEditor() as TvbFilterDetailBaseWithTrackerBase;

        bool detailFilterRequireInterfaceId = detailFilterEditor != null ? detailFilterEditor.RequireInterfaceId : false;

        if (item == null && SelectedViewType != ViewTypeNames.NetFlowInterfaceDetails && !detailFilterRequireInterfaceId)
        {
            this.interfaceDdl.Items.Insert(0, new ListItem(Resources.NTAWebContent.NTAWEBCODE_TM0_102, "0"));
        }

        if (item != null && (SelectedViewType == ViewTypeNames.NetFlowInterfaceDetails || detailFilterRequireInterfaceId))
        {
            this.interfaceDdl.Items.Remove(item);
        }
    }

    private void ShowFilterEditorsBySelectedViewType()
    {
        this.filterEditDiv.Visible = false;
        this.shownFilterEditPath.Value = "";
        string script = "showHideGlobalFiltersAccordingToDetail();";

        foreach (var item in this.filterEditors)
        {
            if (item.Key == SelectedViewType)
            {
                this.filterEditDiv.Visible = true;
                item.Value.Visible = true;
                this.filterEditTitle.Text = String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_103, item.Value.Title);
                this.shownFilterEditPath.Value = item.Value.AppRelativeVirtualPath;
                this.LoadFilterUiContent(item.Value);
            }
            else
            {
                item.Value.Visible = false;
            }
        }
        ScriptManager.RegisterStartupScript(viewTypeDdl, this.GetType(), "ShowHideGlobalFiltersAccordingToDetail", script, true);
    }

    private void LoadFilterUiContent(TvbFilterEditorWithTrackerBase editor)
    {
        if (editor == null)
            return;

        bool filterContentLoaded;
        if (editor is TvbFilterDetailBaseWithTrackerBase)
        {
            if ((editor as TvbFilterDetailBaseWithTrackerBase).RequireInterfaceId && this.SelectedInterfaceID == -1)
                filterContentLoaded = true;
            else
                filterContentLoaded = (editor as TvbFilterDetailBaseWithTrackerBase).UiContentLoadedForNodeAndInterface(this.SelectedNodeID, this.SelectedInterfaceID);
        }
        else
            filterContentLoaded = editor.UiContentLoaded;

        if (!filterContentLoaded)
        {
            //Load UI content of the filter edit asynchronously
            string script = "$('#" + this.loadFilterUiContentButton.ClientID + "').click();";
            ScriptManager.RegisterStartupScript(viewTypeDdl, this.GetType(), "ViewTypeDetailLoadFilterContent", script, true);
        }

    }

    public string ShownFilterHiddenFieldClientID { get { return this.shownFilterEditPath.ClientID; } }

    private string SelectedViewType
    {
        get { return viewTypeDdl.SelectedValue; }
        set { viewTypeDdl.SelectedValue = value; }
    }

    private string SelectedViewId
    {
        get { return viewNameDdl.SelectedValue; }
        set { viewNameDdl.SelectedValue = value; }
    }

    private int SelectedNodeID
    {
        get { return nodeDdl.SelectedItem != null ? Convert.ToInt32(nodeDdl.SelectedValue) : -1; }
        set { nodeDdl.SelectedValue = value.ToString(); }
    }

    private int SelectedInterfaceID
    {
        get { return interfaceDdl.SelectedItem != null ? Convert.ToInt32(interfaceDdl.SelectedItem.Value) : -1; }
        set { interfaceDdl.SelectedValue = value.ToString(); }
    }

    public void LoadUiContent()
    {
        string loadViewType = (string)this.ViewState["LoadViewType"];
        string loadViewId = (string) this.ViewState["LoadViewId"];
        int nodeID = ViewState["LoadNodeID"] != null ? (int)ViewState["LoadNodeID"] : -1;
        int interfaceID = ViewState["LoadInterfaceID"] != null ? (int)ViewState["LoadInterfaceID"] : -1;


        //Load list of detail views
        this.viewTypeDdl.Items.Add(new ListItem(ViewTypeShortNames[ViewTypeNames.NetFlowNodeDetails], ViewTypeNames.NetFlowNodeDetails));
        this.viewTypeDdl.Items.Add(new ListItem(ViewTypeShortNames[ViewTypeNames.NetFlowInterfaceDetails], ViewTypeNames.NetFlowInterfaceDetails));

        foreach (KeyValuePair<string, string> viewTypeShortName in ViewTypeShortNames)
        {
            if (filterEditorDefinitions.Exists(f => f.DetailViewType == viewTypeShortName.Key))
            {
                this.viewTypeDdl.Items.Add(new ListItem(viewTypeShortName.Value, viewTypeShortName.Key));
            }
        }

        //Load nodes
        this.nodeDdl.DataSource = DataTableHelper.AddColumnWithTruncatedStringFor(TrafficViewBuilderDAL.GetNodes(true), "Caption", TruncatedCaptionColumnName);
        this.nodeDdl.DataBind();

        if (loadViewType != null)
        {
            //View Type
            ListItem listItem = viewTypeDdl.Items.FindByValue(loadViewType);
            if (listItem == null)
            {
                string msg = String.Format("Detail View type: '{0}' isn't supported by Traffic View Builder", loadViewType); //[Localization] This is exception which can appear only during development when the control is used in wrong way
                log.Error(msg);
                throw new InvalidOperationException(msg);
            }
            listItem.Selected = true;
            viewTypeDdl_SelectedIndexChanged(viewTypeDdl, null);

            //View Name
            if (!String.IsNullOrEmpty(loadViewId))
            {
                listItem = viewNameDdl.Items.FindByValue(loadViewId);
                if (listItem == null)
                    throw new InvalidOperationException(String.Format("Detail View ID: '{0}' isn't supported by Traffic View Builder", loadViewId)); //[Localization] This is exception which can appear only during development when the control is used in wrong way
                listItem.Selected = true;
            }

            //Node & Interface
            this.SelectedNodeID = nodeID;
            nodeDdl_SelectedIndexChanged(nodeDdl, null);

            if (interfaceID > -1)
                this.SelectedInterfaceID = interfaceID;

            //ViewDefinitionFilter (other than Node or Interface)
            ShowFilterEditorsBySelectedViewType();

        }
        else
        {
            viewTypeDdl_SelectedIndexChanged(viewTypeDdl, null);
            nodeDdl_SelectedIndexChanged(nodeDdl, null);
        }


    }

    public void LoadFromInfo(ViewTypeLoadInfo loadInfo)
    {

        //Load filters content - filters are already created (in OnInit), now just load content from filters collection
        foreach (var item in filterEditors)
        {
            item.Value.LoadFromFilters(loadInfo.Filters);
        }

        if (loadInfo.ViewType == null) //We are on not on detail view, just load filters
            return;

        //Content will be loaded in LoadUiContent durin async potback
        this.ViewState["LoadViewType"] = loadInfo.ViewType;
        this.ViewState["LoadViewId"] = loadInfo.ViewId;

        //Get NodeID and InterfaceID
        int nodeID = -1;
        int interfaceID = -1;
        if (loadInfo.ViewType == ViewTypeNames.NetFlowNodeDetails)
        {
            nodeID = (int)((NodeFilter)loadInfo.Filters.ViewDefinitionFilter).NodeID;
        }
        else
            if (loadInfo.ViewType == ViewTypeNames.NetFlowInterfaceDetails)
            {
                interfaceID = (int)((InterfaceFilter)loadInfo.Filters.ViewDefinitionFilter).InterfaceID;
                nodeID = (int)((InterfaceFilter)loadInfo.Filters.ViewDefinitionFilter).NodeID;
            }
            else
            {
                NodeFilter nodeFilter = loadInfo.Filters.NetflowSource as NodeFilter;
                InterfaceFilter interfaceFilter = loadInfo.Filters.NetflowSource as InterfaceFilter;
                if (nodeFilter != null)
                    nodeID = (int)nodeFilter.NodeID;
                else
                    if (interfaceFilter != null)
                    {
                        nodeID = (int)interfaceFilter.NodeID;
                        interfaceID = (int)interfaceFilter.InterfaceID;
                    }


            }
        if (nodeID == -1)
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_110); 

        this.ViewState["LoadNodeID"] = nodeID;
        this.ViewState["LoadInterfaceID"] = interfaceID;



    }

    public IFilterCollection GetFilters(bool forAlerting = false)
    {
        // The value is unknown.
        if (SelectedNodeID == -1)
        {
            return null;
        }

        NodeFilter nodeFilter = new NodeFilter(this.SelectedNodeID.ToString(), FiltersNameProvider.Instance);
        InterfaceFilter interfaceFilter = this.SelectedInterfaceID > 0 ? new InterfaceFilter(this.SelectedInterfaceID.ToString(), FiltersNameProvider.Instance) : null;
        string filterID = "";

        // Get current filter ID with ViewDefinition and NetflowSource component
        if (this.SelectedViewType == ViewTypeNames.NetFlowNodeDetails)
        {
            filterID = nodeFilter.FilterID;
        }
        else
        {
            if (this.SelectedViewType == ViewTypeNames.NetFlowInterfaceDetails)
            {
                filterID = interfaceFilter.FilterID;
            }
            else
            {
                
                TvbFilterEditorWithTrackerBase editor = this.filterEditors.Values.First(f => f.Visible);
                if (editor == null)
                    throw new InvalidOperationException("There has to be one filter editor visible for current view type."); // This is exception which can appear only during development when the control is used in wrong way

                INetflowFilter filter = editor.GetCurrentFilter();
                if (filter == null && !forAlerting)
                    throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_111);

                if (filter != null)
                    filterID = filter.FilterID + ";";
                
                filterID += (interfaceFilter != null ? interfaceFilter.FilterID : nodeFilter.FilterID);
            }
        }

        //Build filters collection and url
        var filters = FiltersHelper.CreateFilterCollection(filterID);

        return filters;
    }


    public string GetUrlAndFilters(out IFilterCollection filters)
    {
        filters = GetFilters();

        if (filters != null)
        { 
            var cbqosFilter = filters.Contains(typeof(CBQoSClassMapFilter));
            string pagePath = ViewManager.GetViewsByType(this.SelectedViewType).First().PagePath;

            string viewNameParam = this.viewNameDdl.Items.Count > 1 ? "?viewid=" + SelectedViewId + "&" : "?";
            string url = String.Format("{0}{1}NetObject=", pagePath, viewNameParam);
            if (cbqosFilter)
                url += "C";
            else
                url += "N";

            return url;
        }

        return null;
    }

}