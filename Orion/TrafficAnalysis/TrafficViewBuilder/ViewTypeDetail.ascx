﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewTypeDetail.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeDetail" %>
<%@ Register TagPrefix="tvb" TagName="CategoryContainer" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/CategoryContainer.ascx" %>


<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <input id="shownFilterEditPath" type="hidden" value="" runat="server" />
        <asp:Button ID="loadFilterUiContentButton" runat="server" OnClick="loadFilterUiContentButton_Click" Style="display: none;" />
        
        <div id="viewTypeDiv" runat="server" class="ViewTypeSubCategory">
            <%= Resources.NTAWebContent.NTAWEBDATA_TM0_101%><br />
            <asp:DropDownList runat="server" ID="viewTypeDdl" AutoPostBack="true" OnSelectedIndexChanged="viewTypeDdl_SelectedIndexChanged" Width="100%"
                              onChange="javascript: $('#viewTypeContentLoaded').hide();$('#viewTypeContentLoading').show()"  />
        </div>
        <div id="viewTypeContentLoaded">
            <div id="viewNameDiv" runat="server" class="ViewTypeSubCategory">
                <%= Resources.NTAWebContent.NTAWEBDATA_TM0_102%><br />
                <asp:DropDownList runat="server" ID="viewNameDdl" Width="100%" />
            </div>

            <div id="nodeDiv" runat="server" class="ViewTypeSubCategory">
                <%= Resources.NTAWebContent.NTAWEBDATA_TM0_103%><br />
                <asp:DropDownList runat="server" ID="nodeDdl" AutoPostBack="true" DataTextField="<%# TruncatedCaptionColumnName %>" DataValueField="ID" AppendDataBoundItems="true" OnSelectedIndexChanged="nodeDdl_SelectedIndexChanged" Width="100%"/>
                
                <asp:CompareValidator runat="server" ID="valSelectNode" ControlToValidate="nodeDdl" EnableClientScript="false" Type="String" Operator="NotEqual" ValueToCompare="-1" Text="Please select a router." Display="Dynamic" />
            </div>
        
            <div id="interfaceDiv" runat="server" class="ViewTypeSubCategory">
                <%= Resources.NTAWebContent.NTAWEBDATA_TM0_104%></b><br />
                <asp:DropDownList runat="server" ID="interfaceDdl" AutoPostBack="true" OnSelectedIndexChanged="interfaceDdl_SelectedIndexChanged" DataTextField="<%# TruncatedCaptionColumnName %>" DataValueField="ID" AppendDataBoundItems="true" Width="100%" />
                <div id="interfaceLoadingDiv" runat="server" style="display:none">
                    <asp:Image ID="interfaceLoadingImg" ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server" />&nbsp;&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_97%>
                </div>
            </div>

            <div id="filterEditDiv" runat="server" class="ViewTypeSubCategory">
                <div style="margin-bottom:5px">
                    <asp:Label ID="filterEditTitle" runat="server" Text="" />
                </div>            
            </div>
        </div>
        <div id="viewTypeContentLoading" style="display:none">
            <asp:Image ID="loadingViewTypeContentImg" ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server" />&nbsp;&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_97%>
        </div>
    </ContentTemplate>

</asp:UpdatePanel>

<style type="text/css">
    .ViewTypeSubCategory
    {
        margin-bottom: 10px;
    }
</style>
