﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewTypeSummary.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeSummary" %>
    <div id="viewTitleDiv" runat="server">
        <%= Resources.NTAWebContent.NTAWEBDATA_TM0_105%><br />
        <asp:DropDownList ID="summaryDropDown" runat="server" Width="100%"/>
    </div>