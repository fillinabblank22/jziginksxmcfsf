﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_FlowDirectionEditor : TvbFilterEditorBase
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
    }

    public override void LoadFromFilters(IFilterCollection filters)
    {
        this.flowDirectionSelector.FlowDirection = filters.FlowDirection;
    }

    public override void SaveToFilters(IFilterCollection filters)
    {
        if (filters.NetflowSource is NodeFilter &&
            this.flowDirectionSelector.FlowDirection.Contains(FlowDirectionFilter.FlowDirectionType.Egress) && this.flowDirectionSelector.FlowDirection.Contains(FlowDirectionFilter.FlowDirectionType.Ingress))
            filters.FlowDirection = FlowDirectionFilter.FlowDirectionType.Ingress;
        else
            filters.FlowDirection = this.flowDirectionSelector.FlowDirection;
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to do, this control doesn't have any special data (like list of nodes)
    }

    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_95; } }


}