﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutonomousSystemConversationsEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_AutonomousSystemConversationsEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>
<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <asp:TextBox runat="server" ID="filter1Text" Width="128px" />&nbsp;-&nbsp;<asp:TextBox runat="server" ID="filter2Text" Width="128px"/>
    </FilterEditTemplate>
</tvb:FilterTracker>
