﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Internationalization.Exceptions;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_AutonomousSystemConversationsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_89; } }

    public override Type GetFilterType()
    {
        return typeof(AutonomousSystemConversationFilter);
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to do
    }

    public override string GetValueForKey(string key)
    {
        string[] ass = key.Split('-');

        string val1 = TrafficViewBuilderDAL.GetAutonomousSystemName(Convert.ToInt32(ass[0]));
        string val2 = TrafficViewBuilderDAL.GetAutonomousSystemName(Convert.ToInt32(ass[1]));

        if (val1 == null)
            val1 = String.Format("{0} (Custom AS)", ass[0]);

        if (val2 == null)
            val2 = String.Format("{0} (Custom AS)", ass[1]);

        return val1 + " - " + val2;

    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        if (this.filter1Text.Text == String.Empty && this.filter2Text.Text == String.Empty)
            return new KeyValuePair<string, string>("", "");

        uint dummy;
        if (this.filter1Text.Text ==String.Empty || !uint.TryParse(this.filter1Text.Text, out dummy))
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_102);



        if (this.filter2Text.Text == String.Empty || !uint.TryParse(this.filter2Text.Text, out dummy))
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_103);



        return new KeyValuePair<string, string>(this.filter1Text.Text + "-" + this.filter2Text.Text, GetValueForKey(this.filter1Text.Text + "-" + this.filter2Text.Text));
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        if (keyVal.Key == String.Empty)
        {
            this.filter1Text.Text = String.Empty;
            this.filter2Text.Text = String.Empty;
        }
        else
        {
            string[] ass = keyVal.Key.Split('-');
            this.filter1Text.Text = ass[0];
            this.filter2Text.Text = ass[1];
        }
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filter1Text);
        textBoxes.Add(this.filter2Text);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }

}