﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationsEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_ApplicationsEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>
<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <table>
            <tr>
                <td style="width: 140px">
                    <%# Resources.NTAWebContent.NTAWEBDATA_TM0_99%>
                </td>
                <td style="width:300px">
                    <asp:DropDownList runat="server" ID="filterDdl" DataTextField="FullNameTruncated" DataValueField="ID" Width="100%" AppendDataBoundItems="True">
                        <asp:ListItem Text="" Value="" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 140px">
                    <%# Resources.NTAWebContent.NTAWEBDATA_TM0_100%>
                </td>
                <td style="width:300px">
                    <asp:TextBox runat="server" ID="filterText" Width="100%" />
                </td>
            </tr>
        </table>
    </FilterEditTemplate>
</tvb:FilterTracker>
