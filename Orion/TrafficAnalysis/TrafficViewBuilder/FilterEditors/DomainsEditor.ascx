﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DomainsEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_DomainsEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>

<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <asp:TextBox runat="server" ID="domainTb" Width="100%"  />
    </FilterEditTemplate>
</tvb:FilterTracker>
