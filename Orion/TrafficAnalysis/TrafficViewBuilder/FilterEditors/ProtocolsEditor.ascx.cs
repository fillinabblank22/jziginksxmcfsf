﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_ProtocolsEditor : TvbFilterEditorWithTrackerBase
{
    protected const string TruncatedCaptionColumnName = "TruncatedCaption";
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_97; } }

    public override Type GetFilterType()
    {
        return typeof(ProtocolFilter);
    }

    protected override void InternalLoadUiContent()
    {
        filterDdl.DataSource = DataTableHelper.AddColumnWithTruncatedStringFor(TrafficViewBuilderDAL.GetProtocols(), "Caption", TruncatedCaptionColumnName);
        filterDdl.DataBind();
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }


    public override string GetValueForKey(string key)
    {
        ListItem item = filterDdl.Items.FindByValue(key);

        if (item != null)
        {
            return item.Text;
        }
        else
            return key;
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        return new KeyValuePair<string, string>(this.filterDdl.SelectedItem.Value, this.filterDdl.SelectedItem.Text);
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filterDdl);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        this.filterDdl.SelectedValue = keyVal.Key;
    }
}