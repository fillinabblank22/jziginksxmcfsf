﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressGroupsConversationEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_GroupsConversationEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>

<tvb:FilterTracker ID="filterTracker1" runat="server">
    <FilterEditTemplate>
        <asp:DropDownList runat="server" ID="filter0" DataTextField="CaptionTruncated" DataValueField="ID" Width="128px" AppendDataBoundItems="True">
            <asp:ListItem Text="" Value="" />
        </asp:DropDownList>
        &nbsp;-&nbsp;
         <asp:DropDownList runat="server" ID="filter1" DataTextField="CaptionTruncated" DataValueField="ID" Width="128px" AppendDataBoundItems="True">
            <asp:ListItem Text="" Value="" />
        </asp:DropDownList>
    </FilterEditTemplate>
</tvb:FilterTracker>

