﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Netflow.Web;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_FilterTracker : UserControl, ITvbFilterTracker
{
    private NetflowView view;

    public Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_FilterTracker()
    {
        this.MaxFiltersCount = 21; //Default value
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (FilterEditTemplate != null)
        {
            FilterEditTemplate.InstantiateIn(this.filterEditDiv);
        }
        else
        {
            throw new InvalidOperationException("FilterEditTemplate has to be specified."); //[Localization] This is exception which can appear only during development when the control is used in wrong way
        }

        view = this.Page as NetflowView;

        this.addButton.OnClientClick = String.Format("$('#{0}').hide();$('#{1}').show();", this.addButton.ClientID, this.addButtonLoading.ClientID);
        this.addButtonLoading.Attributes.Add("Style", "Display:none");
    }

    public void FilterEditorInitialized(ITvbFilterEditorWithTracker filterEditor)
    {
        if (filterEditor == null)
            throw new ArgumentNullException("filterEditor", "FilterEditor has to be specified."); //[Localization] This is exception which can appear only during development when the control is used in wrong way

        this.FilterEditor = filterEditor;

        //Register Enter key as ADD button for editor's textboxes
        List<WebControl> textBoxes = new List<WebControl>();
        this.FilterEditor.GetTextBoxesToRegisterEnterKey(textBoxes);
        foreach (WebControl textEdit in textBoxes)
        {
            UIHelper.RegisterEnterKey(textEdit, this.addButton);
        }
    }

    /// <summary>Filter editor control template which will be shown inside this control</summary>
    [PersistenceModeAttribute(PersistenceMode.InnerProperty)]
    [BrowsableAttribute(false)]
    [TemplateInstanceAttribute(TemplateInstance.Single)]
    public ITemplate FilterEditTemplate { get; set; }

    /// <summary>Filter Editor control associated with this tracker - interface provides function required in this class</summary>
    public ITvbFilterEditorWithTracker FilterEditor { get; set; }


    /// <summary>Load UI content for current filter (UI representation of list of filters)</summary>
    public void LoadUiContent()
    {
        if (this.Filter != null)
        {
            this.FiltersUI.Clear();
            foreach (string key in this.Filter.FilterIDs)
            {
                string value = this.FilterEditor.GetValueForKey(key);
                if (!String.IsNullOrEmpty(value))
                    this.FiltersUI.Add(key, value);
                else
                {   //Filter editor doesn't know this filter ID, we have to remove it from filters
                    this.RemoveFilterID(key);
                    this.FiltersUI.Remove(key);
                }
            }

            if (this.FiltersUI.Count == this.MaxFiltersCount)
            {
                KeyValuePair<string, string> keyVal = this.FiltersUI.Last();

                this.RemoveFilterID(keyVal.Key);
                this.FiltersUI.Remove(keyVal.Key);

                this.FilterEditor.SetCurrentKeyAndValue(keyVal);
            }
            this.rptItems.DataBind();
        }
        else
        {
            this.ShowHideFilterEditorAccordingToFiltersCount();
        }

        SetLoadingVisible(false);
    }

    public void SetLoadingVisible(bool isLoadingImageVisible)
    {
        this.contentLoadingDiv.Visible = isLoadingImageVisible;
        this.contentLoadedDiv.Visible = !isLoadingImageVisible;
    }

    /// <summary>Get appropriate filter from the collection and save it to the viewstate</summary>
    public void LoadFromFilters(IFilterCollection filters)
    {
        Type filterType = FilterEditor.GetFilterType();
        string filterID = String.Empty;

        //Try to find appropriate filter in filter collection
        foreach (var filter in filters)
        {
            if (filter.GetType() == filterType)
            {
                filterID = filter.FilterID;
                break;
            }
        }

        //If there is filter, create instance of filter object
        if (filterID != String.Empty)
        {
            INetflowFilter filter = this.AddFilterID(filterID, false);
            this.ExcludeFilter = filter != null ? filter.IsExclusive : false;
        }
        else
        {
            this.Filter = null;
        }
    }

    /// <summary>Add current filter to filter collection</summary>    
    public void SaveToFilters(IFilterCollection filters)
    {
        INetflowFilter filter = GetCurrentFilter();
        if (filter != null)
        {
            filters.Add(filter);
        }
    }

    /// <summary>Returns instance of current filter with proper IsExclusive flag</summary>
    public INetflowFilter GetCurrentFilter()
    {
        try
        {
            KeyValuePair<string, string> currKeyVal = FilterEditor.GetCurrentKeyAndValue();
            if (!String.IsNullOrEmpty(currKeyVal.Key))
            {
                this.AddItem(currKeyVal.Key, currKeyVal.Value, true, false);
            }

            if (this.Filter == null)
            {
                return null;
            }
            else
            {
                INetflowFilter filter = this.Filter;

                if (this.ExcludeFilter && this.EnableExcludeOption)
                {
                    string filterID = filter.FilterID;
                    filterID = AddExcludeTagToFilterID(filterID);
                    filter = (INetflowFilter)Activator.CreateInstance(FilterEditor.GetFilterType(), new object[] { filterID, FiltersNameProvider.Instance });
                }

                return filter;
            }
        }
        catch (Exception ex)
        {            
            string messageFormat = "{0} - {1}";

            string message = string.Format(messageFormat, this.FilterEditor.Title, ex is IHasLocalizedMessage ? (ex as IHasLocalizedMessage).LocalizedMessage : ex.Message);

            throw new LocalizedExceptionBase(() => message, ex);
        }
    }

    /// <summary>Determine if "Exclude" checkbox will be visible or not</summary>
    public bool EnableExcludeOption
    {
        get { return this.excludeTd.Visible || !this.contentLoadedDiv.Visible; }
        set { this.excludeTd.Visible = value; }
    }


    /// <summary>Max count of filters, when this number will be reached, Add option will be hidden.</summary>
    public int MaxFiltersCount { get; set; }

    /// <summary>Instance of current filter. Is created from FilterID which is stored in the ViewState</summary>
    private INetflowFilter Filter { get; set; }

    /// <summary>List of filters for UI. This property is datasource for list of Fitlers on UI. Property is saved in ViewState.</summary>
    protected Dictionary<string, string> FiltersUI { get; private set; }

    private string temporaryAddedKey;

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);
        object vs;

        vs = this.ViewState["FilterID"];
        if (vs != null && ((string)vs) != String.Empty)
            this.AddFilterID((string)vs, false);
        else
            this.Filter = null;

        vs = this.ViewState["FiltersUI"];
        if (vs != null)
            this.FiltersUI = (Dictionary<string, string>)vs;
        else
            this.FiltersUI = new Dictionary<string, string>();

        vs = this.ViewState["TemporaryAddedKey"];
        if (vs != null && ((string)vs) != string.Empty)
            this.temporaryAddedKey = (string)vs;
        else
            this.temporaryAddedKey = null;
    }

    protected override object SaveViewState()
    {
        this.ViewState["FilterID"] = this.Filter != null ? this.Filter.FilterID : String.Empty;
        this.ViewState["FiltersUI"] = this.FiltersUI;
        this.ViewState["TemporaryAddedKey"] = this.temporaryAddedKey != null ? this.temporaryAddedKey : String.Empty;
        return base.SaveViewState();
    }

    private void AddItem(string key, string value, bool temporary, bool addFilterUi)
    {
        if (this.temporaryAddedKey != null)
        {
            this.RemoveFilterID(temporaryAddedKey);
            this.FiltersUI.Remove(temporaryAddedKey);
            this.temporaryAddedKey = null;
        }

        this.AddFilterID(key, true);
        if (addFilterUi)
        {
            this.FiltersUI.Add(key, value);
        }

        this.rptItems.DataBind();
        if (!temporary)
            this.FilterEditor.SetCurrentKeyAndValue(new KeyValuePair<string, string>("", ""));
        else
            this.temporaryAddedKey = key;

    }

    private void ShowHideFilterEditorAccordingToFiltersCount()
    {

        if (this.MaxFiltersCount == 1)
        {
            this.addButton.Visible = false;
            this.filtersLimitReached.Visible = false;
        }
        else
        {
            if (this.Filter != null)
            {
                this.addButton.Enabled = this.Filter.FilterIDs.Length < this.MaxFiltersCount - 1;
            }
            else
            {
                this.addButton.Enabled = true;
            }
        }
        if (this.addButton.Enabled)
        {
            this.addButton.Attributes["Style"] = "cursor:pointer";
            this.filtersLimitReached.Visible = false;
        }
        else
        {
            this.addButton.Attributes["Style"] = "cursor:default";
            this.filtersLimitReached.Visible = true;
        }
    }

    private bool ExcludeFilter
    {
        get { return this.excludeDdl.SelectedValue == "Exclude"; }
        set { this.excludeDdl.SelectedValue = value ? "Exclude" : "Include"; }
    }

    /// <summary>
    /// Add filterID to current instance of the filter and save it to the ViewState.
    /// If instance doesn't exist will be created from provided filterID
    /// </summary>
    /// <param name="filterID">FilterID(s) which will be added to instance of the filter</param>
    private INetflowFilter AddFilterID(string filterID, bool encodeFilterId)
    {
        INetflowFilter result;
        if (this.Filter == null)
        {
            Type filterType = FilterEditor.GetFilterType();
            if (encodeFilterId)
                filterID = FilterUtils.EncodeFilterItem(filterID);
            result = (INetflowFilter)Activator.CreateInstance(filterType, new object[] { filterID, FiltersNameProvider.Instance });
            if (result.IsExclusive)
                this.Filter = (INetflowFilter)Activator.CreateInstance(filterType, new object[] { RemoveExcludeTagFromFilterID(result.FilterID), FiltersNameProvider.Instance });
            else
                this.Filter = result;

            //Remove filters which exceed maximum allowed count of filters for this edit control
            for (int i = this.Filter.FilterIDs.Length - 1; i >= this.MaxFiltersCount; i--)
            {
                this.Filter.Exclude(new string[] { this.Filter.FilterIDs[i] });
            }
        }
        else
        {
            this.Filter.Include(new string[] { filterID });
            result = this.Filter;
        }
        this.UpdateSubTitle();
        this.ShowHideFilterEditorAccordingToFiltersCount();
        return result;
    }

    /// <summary>
    /// Remove filterID from current instance of the filter and save it to the ViewState
    /// If instance doesn't exist, exception is thrown
    /// </summary>
    /// <param name="filterID">FilterID which will be removed from the filter instance</param>
    private void RemoveFilterID(string filterID)
    {
        if (this.Filter == null)
            throw new InvalidOperationException("Filter is null"); // This is exception which can appear only during development when the control is used in wrong way

        this.Filter.Exclude(new string[] { filterID });
        if (this.Filter.FilterIDs.Length == 0)
        {
            this.Filter = null;
        }
        this.UpdateSubTitle();
        this.ShowHideFilterEditorAccordingToFiltersCount();
    }

    /// <summary>Updates subtitle (subtitle in the header) according to current filter status</summary>
    private void UpdateSubTitle()
    {
        if (this.Filter != null)
        {
            int filtersCount = this.Filter.FilterIDs.Length;
            this.FilterEditor.ChangeSubTitle(String.Format(filtersCount == 1 ? Resources.NTAWebContent.NTAWEBCODE_TM0_107 : Resources.NTAWebContent.NTAWEBCODE_TM0_108, filtersCount));
        }
        else
        {
            this.FilterEditor.ChangeSubTitle("");
        }
    }

    private string RemoveExcludeTagFromFilterID(string filterID)
    {
        return filterID.Replace("~", "");
    }

    private string AddExcludeTagToFilterID(string filterID)
    {
        if (String.IsNullOrEmpty(filterID))
            return filterID;
        int idx = filterID.IndexOf(':');
        return filterID.Insert(idx + 1, "~");
    }

    protected void DeleteButtonClick(object sender, EventArgs e)
    {
        ImageButton btn = sender as ImageButton;
        if (btn.CommandName.Equals("Delete", StringComparison.InvariantCultureIgnoreCase))
        {
            string key = btn.CommandArgument;
            this.RemoveFilterID(key);
            this.FiltersUI.Remove(key);
            this.rptItems.DataBind();
        }

        view.UpdateFiltersInAlertBuilder();
    }

    protected void AddButtonClick(object sender, EventArgs e)
    {
        try
        {
            KeyValuePair<string, string> keyVal = FilterEditor.GetCurrentKeyAndValue();
            if (keyVal.Key != String.Empty)
            {
                this.AddItem(keyVal.Key, keyVal.Value, false, true);
            }
            else
            {
                FilterEditor.ShowAsyncErrorMessage(Resources.NTAWebContent.NTAWEBCODE_TM0_105);
            }
            
            view.UpdateFiltersInAlertBuilder();
            
        }
        catch (Exception ex)
        {
            string msg = ex is IHasLocalizedMessage? (ex as IHasLocalizedMessage).LocalizedMessage : ex.Message;
            FilterEditor.ShowAsyncErrorMessage(msg);
        }
    }

    

    protected void IncludeExcludeDropDownChanged(object sender, EventArgs e)
    {
        view.UpdateFiltersInAlertBuilder();
    }

    protected void rptItems_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ImageButton btn = (ImageButton)e.Item.FindControl("deleteButton");
            Image img = (Image)e.Item.FindControl("deleteButtonLoading");

            btn.OnClientClick = String.Format("$('#{0}').hide();$('#{1}').show();", btn.ClientID, img.ClientID);
            img.Attributes.Add("Style", "Display:none");
        }
    }

    protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ImageButton btn = (ImageButton)e.Item.FindControl("deleteButton");
            Image img = (Image)e.Item.FindControl("deleteButtonLoading");

            btn.OnClientClick = String.Format("$('#{0}').hide();$('#{1}').show();", btn.ClientID, img.ClientID);
            img.Attributes.Add("Style", "Display:none");
        }
    }

}