﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FlowDirectionEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_FlowDirectionEditor" %>
<%@ Register TagPrefix="nta" TagName="FlowDirectionSelector" Src="~/Orion/TrafficAnalysis/Utils/FlowDirectionSelector.ascx" %>

<nta:FlowDirectionSelector runat="server" ID="flowDirectionSelector" ValidationEnabled="true" />