﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CountriesEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_CountriesEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>

<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <asp:DropDownList runat="server" ID="filterDdl" DataTextField="Caption" DataValueField="ID" Width="100%" AppendDataBoundItems="True">
            <asp:ListItem Text="" Value="" />
        </asp:DropDownList>
    </FilterEditTemplate>
</tvb:FilterTracker>
