﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TimePeriodEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_TimePeriodEditor" %>
<%@ Register TagPrefix="nta" TagName="PeriodSelector" Src="~/Orion/TrafficAnalysis/TimePeriod/PeriodSelector.ascx" %>

<nta:PeriodSelector runat="server" ID="timePeriodSelector" ValidationEnabled="true" />