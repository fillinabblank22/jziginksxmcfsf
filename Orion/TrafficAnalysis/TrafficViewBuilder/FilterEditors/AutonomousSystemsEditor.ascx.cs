﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Internationalization.Exceptions;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_AutonomousSystemsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_90; } }

    public override Type GetFilterType()
    {
        return typeof(AutonomousSystemFilter);
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to do
    }

    public override string GetValueForKey(string key)
    {
        string val = TrafficViewBuilderDAL.GetAutonomousSystemName(Convert.ToInt32(key));
        if (!String.IsNullOrEmpty(val))
            return val;
        else
            return String.Format("{0} (Custom AS)", key);
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        KeyValuePair<string, string> keyVal;
        if (this.filterText.Text != String.Empty)
        {
            string autonomousSystemId = this.filterText.Text;
            uint dummy;
            if (!uint.TryParse(autonomousSystemId, out dummy))
                throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_105);
            keyVal = new KeyValuePair<string, string>(this.filterText.Text, GetValueForKey(this.filterText.Text));
        }
        else
        {
            keyVal = new KeyValuePair<string, string>("", "");
        }

        return keyVal;
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        this.filterText.Text = keyVal.Key;
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filterText);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }

}