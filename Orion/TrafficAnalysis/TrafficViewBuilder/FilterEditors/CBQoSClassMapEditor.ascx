﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSClassMapEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_CBQoSClassMapEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>

<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <asp:DropDownList runat="server" ID="filterDdl" DataTextField="Caption" DataValueField="ID" Width="100%" AppendDataBoundItems="True"/>
    </FilterEditTemplate>
</tvb:FilterTracker>
