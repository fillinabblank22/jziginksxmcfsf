﻿using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_GroupsConversationEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBDATA_IPAddressGroupsConversation_Filter; } }

    public override Type GetFilterType()
    {
        return typeof(IPAddressGroupsConversationFilter);
    }

    public override string GetValueForKey(string key)
    {
        string[] ipSorts = key.Split('-');
        ListItem item0 = filter0.Items.FindByValue(ipSorts[0]);
        ListItem item1 = filter1.Items.FindByValue(ipSorts[1]);

        if (item0 != null && item1 != null)
        {
            return $"{PlaceSpaceAfterCommas(item0.Text)}-{PlaceSpaceAfterCommas(item1.Text)}";
        }
        else
            return key;
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker1;
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        if (filter0.SelectedItem.Value == string.Empty && filter1.SelectedItem.Value == string.Empty)
            return new KeyValuePair<string, string>("", "");

        if (filter0.SelectedItem.Value == string.Empty)
        {
            ////<value>Please select ip groups for first .</value>
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBDATA_IPAddressGroupsConversationWarningMessage_First);
        }

        if (filter1.SelectedItem.Value == string.Empty)
        {
            ////<value>Please select ip groups for second .</value>
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBDATA_IPAddressGroupsConversationWarningMessage_Second);
        }

        var key = $"{filter0.SelectedItem.Value}-{filter1.SelectedItem.Value}";
        var value = $"{PlaceSpaceAfterCommas(filter0.SelectedItem.Text)} - {PlaceSpaceAfterCommas(filter1.SelectedItem.Text)}";

        return new KeyValuePair<string, string>(key, value);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        if (keyVal.Value == String.Empty)
        {
            this.filter0.SelectedValue = "";
            this.filter1.SelectedValue = "";
        }
        else
        {
            string[] endpoints = keyVal.Key.Split('-');
            this.filter0.SelectedValue = endpoints[0].Trim();
            this.filter1.SelectedValue = endpoints[1].Trim();
        }
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filter0);
        textBoxes.Add(this.filter1);
    }

    protected override void InternalLoadUiContent()
    {
        filter0.DataSource = TrafficViewBuilderDAL.GetIPGroups().Values.OrderBy(ipGroup => ipGroup.Caption);
        filter0.DataBind();

        filter1.DataSource = TrafficViewBuilderDAL.GetIPGroups().Values.OrderBy(ipGroup => ipGroup.Caption);
        filter1.DataBind();
    }
}