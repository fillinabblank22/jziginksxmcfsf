﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Data;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_CBQoSClassMapEditor : TvbFilterDetailBaseWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_91; } }

    public override Type GetFilterType()
    {
        return typeof(CBQoSClassMapFilter);
    }

    public override bool RequireInterfaceId { get { return true; } }

    protected override void InternalLoadUiContent(int nodeId, int InterfaceId)
    {
        filterDdl.Items.Clear();
        filterDdl.Items.Add(new ListItem("", ""));
        foreach(DataRow row in TrafficViewBuilderDAL.GetCBQoSClassMap(InterfaceId).Rows)
        {
            DateTime endDate = (DateTime)row["EndDate"];
            filterDdl.Items.Add(new ListItem(string.Format("{0} ({1} - {2})", row["Path"], ((DateTime)row["StartDate"]), endDate.Year == 9999 ? "Now" : endDate.ToString()), 
                row["PolicyID"].ToString()));
        }
    }

    public override string GetValueForKey(string key)
    {
        ListItem item = filterDdl.Items.FindByValue(key);

        if (item != null)
        {
            return item.Text;
        }
        else
            return String.Empty;  //Key doesn't exist, so we don't want to show this key in the filter list at all
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        if (this.filterDdl.SelectedItem == null)
        {
            return new KeyValuePair<string, string>();
        }

        return new KeyValuePair<string, string>(this.filterDdl.SelectedItem.Value, this.filterDdl.SelectedItem.Text);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        this.filterDdl.SelectedValue = keyVal.Key;
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filterDdl);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }
}