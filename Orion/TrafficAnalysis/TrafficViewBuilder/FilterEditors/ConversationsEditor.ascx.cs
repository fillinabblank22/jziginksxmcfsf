﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Net;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Common;
using SolarWinds.Orion.Web.Platform;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_ConversationsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_92; } }

    public override Type GetFilterType()
    {
        return typeof(ConversationFilter);
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to load
    }

    public override string GetValueForKey(string key)
    {
        string[] ipSorts = key.Split('-');
        return FilterUtils.CreateIPAddressFromFilter(ipSorts[0]).ToString() + " - " + FilterUtils.CreateIPAddressFromFilter(ipSorts[1]).ToString();
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        string endpoint1 = this.endpoint1Tb.Text.Trim();
        string endpoint2 = this.endpoint2Tb.Text.Trim();

        if (endpoint1 == String.Empty && endpoint2 == String.Empty)
            return new KeyValuePair<string, string>("", "");

        var hostsDal = HttpContext.Current.Resolve<IHostsDal>();

        NetflowIPAddress addr1;
        if (!NetflowIPAddress.TryParse(endpoint1, out addr1))
        {
            IPAddress hostAddr = null;
            if (endpoint1 == String.Empty || !hostsDal.TryGetIPAddressWithWildcard(endpoint1, out hostAddr))
                throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_106);

            addr1 = new NetflowIPAddress(hostAddr);
        }

        NetflowIPAddress addr2;
        if (!NetflowIPAddress.TryParse(endpoint2, out addr2))
        {
            IPAddress hostAddr = null;
            if (endpoint2 == String.Empty || !hostsDal.TryGetIPAddressWithWildcard(endpoint2, out hostAddr))
                throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_107);

            addr2 = new NetflowIPAddress(hostAddr);
        }

        if (addr1.Version != addr2.Version)
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_151);


        string key = ConversationsDAL.CreateConversationID(addr1.ToString(), addr2.ToString());


        return new KeyValuePair<string, string>(key, endpoint1 + " - " + endpoint2);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        if (keyVal.Value == String.Empty)
        {
            this.endpoint1Tb.Text = "";
            this.endpoint2Tb.Text = "";
        }
        else
        {
            string[] endpoints = keyVal.Value.Split('-');
            this.endpoint1Tb.Text = endpoints[0].Trim();
            this.endpoint2Tb.Text = endpoints[1].Trim();
        }
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.endpoint1Tb);
        textBoxes.Add(this.endpoint2Tb);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }


}