﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Text.RegularExpressions;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.Applications.DAL;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_ApplicationsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_88; } }

    public override Type GetFilterType()
    {
        return typeof(ApplicationFilter);
    }

    protected override void InternalLoadUiContent()
    {
        filterDdl.DataSource = TrafficViewBuilderDAL.GetApplications();
        filterDdl.DataBind();
    }

    public override string GetValueForKey(string key)
    {
        ListItem item = filterDdl.Items.FindByValue(key);

        if (item != null)
        {
            return item.Text;
        }
        else
            return String.Format("Custom port({0})", key);
    }

    private static readonly Regex filterValidator = new Regex(ApplicationFilter.RegexPattern);

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        KeyValuePair<string, string> keyVal;
        if (this.filterDdl.SelectedItem.Value != String.Empty && this.filterText.Text != String.Empty)
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_100);
        }

        if (this.filterDdl.SelectedItem.Value != String.Empty)
        {
            keyVal = new KeyValuePair<string, string>(this.filterDdl.SelectedItem.Value, this.filterDdl.SelectedItem.Text);
        }
        else
        {
            if (this.filterText.Text != String.Empty)
            {
                string filter = this.filterText.Text;
                var match = filterValidator.Match(filter);
                if (!match.Success)
                    throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_OSC_101);
                var protocol = ApplicationDALCriteria.FilterApplicationProtocol.Any;
                if (match.Groups["type"].Success)
                {
                    if (match.Groups["type"].Value == "T")
                    {
                        protocol = ApplicationDALCriteria.FilterApplicationProtocol.OnlyTcp;
                    }
                    else if(match.Groups["type"].Value == "U")
                    {
                        protocol = ApplicationDALCriteria.FilterApplicationProtocol.OnlyUdp;
                    }
                }
                var key = TrafficViewBuilderDAL.SearchApplications(
                    int.Parse(match.Groups["port"].Value),
                    protocol)
                    .Where(a => !a.IPGroups.Any())
                    .Select(a => a.ID.ToString())
                    .FirstOrDefault() ?? filter;
                
                keyVal = new KeyValuePair<string, string>(key, GetValueForKey(key));
            }
            else
            {
                keyVal = new KeyValuePair<string, string>("", "");
            }
        }

        return keyVal;
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        if (keyVal.Key == String.Empty)
        {
            this.filterDdl.SelectedValue = String.Empty;
            this.filterText.Text = String.Empty;
        }
        else
        {
            ListItem item = this.filterDdl.Items.FindByValue(keyVal.Key);
            if (item != null)
            {
                filterDdl.SelectedValue = keyVal.Key;
                this.filterText.Text = "";
            }
            else
            {
                this.filterDdl.SelectedValue = "";
                this.filterText.Text = keyVal.Key;
            }
        }
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.filterDdl);
        textBoxes.Add(this.filterText);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }

    protected void Page_Load(Object obj, EventArgs e)
    {
        if (!IsPostBack)
            filterTracker.DataBind();
    }

}