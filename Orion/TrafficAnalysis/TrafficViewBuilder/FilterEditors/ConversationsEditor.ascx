﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversationsEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_ConversationsEditor" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>

<tvb:FilterTracker ID="filterTracker" runat="server">
    <FilterEditTemplate>
        <asp:TextBox runat="server" ID="endpoint1Tb" Width="128px" />&nbsp;-&nbsp;<asp:TextBox runat="server" ID="endpoint2Tb" Width="128px"/>
    </FilterEditTemplate>
</tvb:FilterTracker>
