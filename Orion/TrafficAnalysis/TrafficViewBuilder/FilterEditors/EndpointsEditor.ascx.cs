﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Net;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_EndpointsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_99; } }

    public override Type GetFilterType()
    {
        return typeof(EndpointFilter);
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to load
    }

    private EndpointFilter.Endpoint GetValidatedEndpoint(string key)
    {
        var endpoint = EndpointFilter.GetEndpoint(key, FiltersNameProvider.Instance);

        if (endpoint.EndpointType != EndpointFilter.EndpointType.HostName)
        {
            return endpoint;
        }

        if (!GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled)
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_DNSPersistOffEndpoint);
        }

        if (!TrafficViewBuilderDAL.CheckHostname(endpoint.FilterName))
        {
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "alert", string.Format("alert('{0}')", string.Format(Resources.NTAWebContent.NTAWEBCODE_HostnameNotInDBEndpointFilterError, HttpUtility.JavaScriptStringEncode(endpoint.FilterName))), true);
        }

        return endpoint;
    }

    public override string GetValueForKey(string key)
    {
        EndpointFilter.Endpoint endpoint = EndpointFilter.GetEndpoint(key, FiltersNameProvider.Instance);

        return endpoint.FilterName;
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        string endpointStr = this.endpointTb.Text.Trim();
        if (endpointStr == String.Empty)
            return new KeyValuePair<string, string>("", "");

        //Try to validate entered endpoint
        var endpoint = GetValidatedEndpoint(WebSecurityHelper.SanitizeHtml(endpointStr));

        return new KeyValuePair<string, string>(endpoint.FilterId, endpoint.FilterName);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        this.endpointTb.Text = keyVal.Value;
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.endpointTb);
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }


}