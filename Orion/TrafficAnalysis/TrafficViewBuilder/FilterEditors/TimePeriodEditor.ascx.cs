﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Netflow.Web.Reporting.Filters;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_TimePeriodEditor : TvbFilterEditorBase
{
    public override void LoadFromFilters(IFilterCollection filters)
    {
        if (!IsPostBack)
        {
            this.timePeriodSelector.PeriodFilter = filters.PeriodFilterUi();
        }

        bool inlineRendering = false;
        bool.TryParse(this.Page.Request.QueryString["InlineRendering"], out inlineRendering);

        if (!inlineRendering)
        {
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetTimePeriod", string.Format("$(document).ready(function() {{ SetSelectedPeriodName_{0}('{1}'); }});",
                    this.timePeriodSelector.ClientID, filters.PeriodFilterUi().PeriodName), true);
        }
        else
        {
            this.Visible = false;
        }
    }

    public override void SaveToFilters(IFilterCollection filters)
    {
        filters.PeriodFilter = this.timePeriodSelector.PeriodFilter;
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to do, this control doesn't have any special data (like list of nodes)
    }

    public override string Title { get { return Resources.NTAWebContent.NTAWEBDATA_TM0_68; } }

    /// <summary> Returns timePeriodSelector's ClientID which TVB submit button use for validation</summary>
    public override string ValidationJS
    {
        get
        {
            return string.Format("timePeriodValidate_{0}()", timePeriodSelector.ClientID);
        }
    }

}