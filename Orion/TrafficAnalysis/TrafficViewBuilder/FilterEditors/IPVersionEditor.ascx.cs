﻿using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_IPVersionEditor : TvbFilterEditorBase
{
    public override void LoadFromFilters(IFilterCollection filters)
    {
        IPVersionSelector.IPVersion = filters.IPVersion;
    }

    public override void SaveToFilters(IFilterCollection filters)
    {
        filters.IPVersion = IPVersionSelector.IPVersion;
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to do, this control doesn't have any special data (like list of nodes)
    }

    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_FlowNavigator_IP_Version; } }


}