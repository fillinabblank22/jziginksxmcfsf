﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Common;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_DomainsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TM0_94; } }

    public override Type GetFilterType()
    {
        return typeof(DomainFilter);
    }

    protected override void InternalLoadUiContent()
    {
        //Nothing to load
    }

    public override string GetValueForKey(string key)
    {        
        return key;
    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        string key = domainTb.Text.Trim();
        if (key == String.Empty)
            return new KeyValuePair<string, string>(string.Empty, string.Empty);

        if (!GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled)
        {
            throw new LocalizedExceptionBase(() => Resources.NTAWebContent.NTAWEBCODE_DNSPersistOff);
        }

        if (!TrafficViewBuilderDAL.CheckDomain(key))
        {
            throw new LocalizedExceptionBase(() => string.Format(Resources.NTAWebContent.NTAWEBCODE_OSC_108, key));
        }

        return new KeyValuePair<string, string>(key, key);
    }
    
    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {
        textBoxes.Add(this.domainTb);
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        this.domainTb.Text = keyVal.Key;
    }
}