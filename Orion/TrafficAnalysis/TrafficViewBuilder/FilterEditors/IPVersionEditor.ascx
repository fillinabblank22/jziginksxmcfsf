﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPVersionEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_IPVersionEditor" %>
<%@ Register TagPrefix="nta" TagName="IPVersionSelector" Src="~/Orion/TrafficAnalysis/Utils/IPVersionSelector.ascx" %>

<nta:IPVersionSelector runat="server" ID="IPVersionSelector" ValidationEnabled="true" />