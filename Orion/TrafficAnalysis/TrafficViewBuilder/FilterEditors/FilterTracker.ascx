﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterTracker.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_FilterTracker" %>

<div id="contentLoadingDiv" visible="true" runat="server">
    <asp:Image ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server" />&nbsp;&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_97%>
</div>

<div id="contentLoadedDiv"  runat="server" visible="false">   
    <table>
    <tr>
    <td id="excludeTd" runat="server" visible="true">
        <asp:DropDownList ID="excludeDdl" runat="server" AutoPostBack="true" OnSelectedIndexChanged="IncludeExcludeDropDownChanged">
            <asp:ListItem Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_106%>" Value="Include"/>
            <asp:ListItem Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_107%>" Value="Exclude"/>
        </asp:DropDownList>
    </td>
    </tr>
        <asp:Repeater runat="server" ID="rptItems" DataSource='<%# this.FiltersUI %>' OnItemDataBound="rptItems_ItemDataBound">
            <ItemTemplate>
                <tr>
                    <td style="width: 95%; word-break: break-all;">
                        <%# Eval("Value") %>
                    </td>
                    <td>
                        <asp:ImageButton runat="server" ID="deleteButton" CommandName="Delete" CommandArgument='<%# Eval("Key") %>' ImageUrl="~/Orion/TrafficAnalysis/images/Button.DeleteFilter.png" OnClick="DeleteButtonClick" />
                        <asp:Image ID="deleteButtonLoading" ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server"/>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
        <table>
            <tbody>
                <tr>
                    <td valign="top" style="width:300px">
                        <div id="filterEditDiv" runat="server" style="margin-right: 5px;vertical-align:top">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

    <table>
        <tbody>
            <tr>
                <td style="width:300px">
                    <orion:LocalizableButton runat="server" ID="addButton" LocalizedText="CustomText" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_108%>" 
                                             OnClick="AddButtonClick" DisplayType="Small" />
                    <asp:Image ID="addButtonLoading" ImageUrl="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" runat="server"/>                    
                    &nbsp<asp:Label runat="server" ID="filtersLimitReached" Text="<%$ Resources: NTAWebContent, FlowNavigator_FilterLimitReached%>"/>
                    
                </td>
            </tr>
        </tbody>
    </table>    
</div>