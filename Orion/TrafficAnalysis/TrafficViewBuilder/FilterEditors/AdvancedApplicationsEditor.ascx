<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedApplicationsEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_AdvancedApplicationsEditor" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="tvb" TagName="FilterTracker" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FilterTracker.ascx" %>
<%@ Register TagPrefix="orion" TagName="IncludeExtJs" Src="~/Orion/Controls/IncludeExtJs.ascx" %>

<orion:IncludeExtJs runat="server" Version="4.0" />
<orion:Include runat="server" Framework="Ext" FrameworkVersion="4.0" />

<tvb:FilterTracker ID="filterTracker" runat="server">
	<FilterEditTemplate>
		<table>
			<tr>
				<td style="width: 140px">
					<%# NTAWebContent.NTAWEBDATA_TM0_99%>
				</td>
				<td style="width:300px">
				    <asp:Panel runat="server" ID="appListContainer"></asp:Panel>
					<asp:HiddenField runat="server" ID="appListKeyHiddenField"/>
				</td>
			</tr>
		</table>
	</FilterEditTemplate>
</tvb:FilterTracker>

<asp:Panel runat="server" ID="pnlInitScript">
<script type="text/javascript">
    function GenerateAdvancedApplicationsFilterComboBox(appListKeyHiddenFieldClientID, appListContainerClientID) {

        var appListKeyHiddenField = $('#' + appListKeyHiddenFieldClientID);

        if (!appListKeyHiddenField.length)
            return;

        Ext4.ux.AspNetHttpProxy = Ext4.extend(Ext4.data.HttpProxy, {
            doRequest: function (operation, callback, scope) {
                var request = this.buildRequest(operation, callback, scope);
                Ext4.apply(request, {
                    headers: this.headers,
                    method: this.getMethod(request),
                    scope: this,
                    callback: this.createRequestCallback(request, operation, callback, scope),
                    jsonData: JSON.stringify({ query: request.params.query }),
                    timeout: this.timeout,
                });
                Ext4.Ajax.request(request);
                return request;
            }
        });

        // Model
        Ext4.define("AdvancedApplication", {
            extend: "Ext.data.Model",
            fields: [
                { type: 'integer', name: 'id' },
                { type: 'string', name: 'name' },
                { type: 'string', name: 'iconClass' }
            ]
        });

        var store = Ext4.create('Ext4.data.Store', {
            model: 'AdvancedApplication',
            proxy: new Ext4.ux.AspNetHttpProxy({
                url: '/Orion/TrafficAnalysis/WebServices/TrafficViewBuilderService.asmx/GetAdvancedApplications',
                actionMethods: 'POST',
                noCache: false,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                reader: { type: 'json', root: 'd.Items', totalProperty: 'd.ResultCount', successProperty: 'd.Success', idProperty: 'id' }
            }),
            remoteSort: true,
            remoteFilter: true,
            autoLoad: false
        });

        // ComboBox with a custom item template
        Ext4.create('Ext4.form.field.ComboBox', {
            displayField: 'name',
            valueField: 'id',
            grow: true,
            store: store,
            renderTo: appListContainerClientID,
            width: 160,
            fieldStyle:'width: 100px;',
            queryMode: 'remote',
            emptyText: '<%=NTAWebContent.NTAWEB_JS_FlowNavigator_AdvancedApplicationFilter_EmptyText %>',
            typeAhead: true,
            minChars: 2,
            triggerAction: 'all',
            listConfig: {
                getInnerTpl: function () {
                    var tpl = '<div>' +
                        '<div class="{iconClass}"></div>&nbsp;&nbsp;' +
                        '{name}</div>';
                    return tpl;
                }
            },
            listeners: {
                select: function(combo, rec, opt) {
                    if (rec && rec[0]) {
                        var v = rec[0].get(combo.valueField);
                        appListKeyHiddenField.val(v);
                    }
                },
                render: function (combo, opt) {
                    var value = appListKeyHiddenField.val();
                    if (value) {
                        // Value was set already, let's set the selected item
                        setAdvancedApplicationsFilterComboBoxSelectedItem(combo, value);
                    }
                }
            }
        });

        // Load single Advanced Application item by Id from web service, load it to store and set it as a combobox's selected item
        function setAdvancedApplicationsFilterComboBoxSelectedItem(combo, value) {
            $.ajax({
                url: '/Orion/TrafficAnalysis/WebServices/TrafficViewBuilderService.asmx/GetAdvancedApplication',
                type: 'POST',
                data: JSON.stringify({ advancedApplicationId: value }),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    var singleItem;
                    if (data.d.Success && (singleItem = data.d.Items[0])) {
                        var advancedApplication = Ext4.ModelManager.create(singleItem, 'AdvancedApplication', value);
                        combo.store.loadData(advancedApplication);
                        combo.setRawValue(singleItem[combo.displayField]);
                    } else {
                        window.console && console.log('Failed to load advanced applications combobox selected item with value=' + value);
                    }
                }
            });
        }
    }
</script>
</asp:Panel>
