﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.Text.RegularExpressions;
using System.Web.UI;
using Resources;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.Applications.DAL;
using SolarWinds.Netflow.Contracts.Applications;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_FilterEditors_AdvancedApplicationsEditor : TvbFilterEditorWithTrackerBase
{
    public override string Title { get { return Resources.NTAWebContent.NTAWEBCODE_TrafficViewBuilder_CategoryTitle_AdvancedApplications; } }

    public override Type GetFilterType()
    {
        return typeof(AdvancedApplicationFilter);
    }

    protected override void InternalLoadUiContent()
    {
        pnlInitScript.Visible = true;
    }

    public override string GetValueForKey(string key)
    {
        int advancedApplicationID;
        if (!int.TryParse(key, out advancedApplicationID))
            return null;

        AdvancedApplication advancedApplication = TrafficViewBuilderDAL.GetAdvancedApplication(advancedApplicationID);

        return advancedApplication != null ? advancedApplication.Name : null;
    }

    public override KeyValuePair<string, string> GetCurrentKeyAndValue()
    {
        KeyValuePair<string, string> keyVal;

        if (!string.IsNullOrEmpty(appListKeyHiddenField.Value))
        {
            keyVal = new KeyValuePair<string, string>(
                appListKeyHiddenField.Value,
               GetValueForKey(appListKeyHiddenField.Value));
        }
        else
        {
            keyVal = new KeyValuePair<string, string>("", "");            
        }

        return keyVal;
    }

    public override void SetCurrentKeyAndValue(KeyValuePair<string, string> keyVal)
    {
        appListKeyHiddenField.Value = keyVal.Key;
    }

    public override void GetTextBoxesToRegisterEnterKey(List<WebControl> textBoxes)
    {

    }

    protected override ITvbFilterTracker GetFilterTracker()
    {
        return this.filterTracker;
    }

    protected void Page_Load(Object obj, EventArgs e)
    {
        if (!IsPostBack)
            filterTracker.DataBind();

        string key = string.Format("GenerateAdvancedApplicationsFilterComboBox_{0}", this.ClientID);
        string script = string.Format("GenerateAdvancedApplicationsFilterComboBox('{0}', '{1}');", appListKeyHiddenField.ClientID, appListContainer.ClientID);

        ScriptManager.RegisterStartupScript(this, this.GetType(), key, script, true);
    }
}