using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.IO;
using System.Text.RegularExpressions;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.UI.AlertBuilder;
using System.Threading;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_TrafficViewBuilder : UserControl, IFiltersProvider
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private struct FilterEditorDefinition
    {
        public string ControlPath;
        public bool Expanded;
        public bool LoadAsync;
    }

    /// <summary>Definition of FILTER EDITORS which will be places inside the Traffic View Builder</summary>
    private static readonly List<FilterEditorDefinition> filterEditorDefinitions = new List<FilterEditorDefinition>()
    {
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/TimePeriodEditor.ascx", 
                                     Expanded=false, LoadAsync=false},
        
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/FlowDirectionEditor.ascx", 
                                     Expanded=false, LoadAsync=false},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/IPVersionEditor.ascx",
                                    Expanded=false, LoadAsync=false},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ApplicationsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AdvancedApplicationsEditor.ascx",
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AutonomousSystemsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/AutonomousSystemConversationsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ConversationsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/CountriesEditor.ascx", 
                                     Expanded=false, LoadAsync=true},
        
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/DomainsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/EndpointsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/IPGroupsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/IPAddressGroupsConversationEditor.ascx", 
                                     Expanded=false, LoadAsync=true},

        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/ProtocolsEditor.ascx", 
                                     Expanded=false, LoadAsync=true},
                                      
        new FilterEditorDefinition(){ControlPath="~/Orion/TrafficAnalysis/TrafficViewBuilder/FilterEditors/TosEditor.ascx", 
                                     Expanded=false, LoadAsync=true} 
    };
    
    /// <summary>Holds list of instances of filter editors</summary>
    private List<TvbFilterEditorBase> filterEditors = new List<TvbFilterEditorBase>();
    
    private NetflowView view;
    
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        this.view = this.Page as NetflowView;
        if (view == null)
            throw new InvalidOperationException("Traffic View Builder can be placed only on NetFlow View."); // This is exception which can appear only during development when the control is used in wrong way
        
        OrionInclude.ExternalFile("/Orion/TrafficAnalysis/styles/TrafficViewBuilder.css", SolarWinds.Orion.Web.UI.Localizer.PathFinder.ResourceType.CascadingStyleSheet);
        OrionInclude.ModuleFile("TrafficAnalysis", NetflowAdvancedApplication.IconCssFileName);

        if (!this.IsPostBack)
        {
            //Loads content of view type selector component from current url and netobject
            ViewTypeLoadInfo viewTypeloadInfo = new ViewTypeLoadInfo() {                 
                ViewType = view.ViewType,
                ViewId = view.ViewInfo.ViewID.ToString(CultureInfo.InvariantCulture),
                Filters = view.NetflowObject.Filters
            };  

            this.viewTypeSelector.LoadFromInfo(viewTypeloadInfo);
        }

        this.BuildFilterEditors(view.NetflowObject.Filters);

        string validators = ("(true)");
        foreach (TvbFilterEditorBase filterEditor in this.filterEditors)
        {
            if (!String.IsNullOrEmpty(filterEditor.ValidationJS))
                validators += " && (" + filterEditor.ValidationJS + ")";
        }

        btnSubmit.OnClientClick = 
        string.Format(@"
if (true==({0}))
{{
    $(""#submitDiv"").hide(0); $(""#submittingDiv"").show(0);
    return true;
}}
else return false;
", validators);

        btnSave.Visible = Profile.AllowCustomize;

        btnSave.OnClientClick =
            string.Format(@"
if (true==({0}))
{{
    var tmp = onSaveClick();
    if (tmp)
    {{
        $(""#submitDiv"").hide(0); $(""#submittingDiv"").show(0);
        return true;
    }}
    return false;
}}
else return false;
", validators);
            


    }

    /// <summary>
    /// Builds filter editors on UI according to filterEditorDefinitions list.
    /// </summary>
    /// <param name="filters">Current filters which will be shown in editors as current values</param>
    private void BuildFilterEditors(IFilterCollection filters)
    {
        foreach (FilterEditorDefinition def in filterEditorDefinitions)
        {
            TvbFilterEditorBase editor = (TvbFilterEditorBase)Page.LoadControl(def.ControlPath);
            editor.ID = "General_"+Path.GetFileNameWithoutExtension(def.ControlPath);

            this.AddFilterEditorToUI(editor, def.Expanded, def.LoadAsync);

            if (!this.IsPostBack)
            {
                editor.LoadFromFilters(filters); //Load values from filter collection
            }

            this.filterEditors.Add(editor);
        }
    }
   
    private void AddFilterEditorToUI(TvbFilterEditorBase editor, bool contentIsExpanded, bool loadAsync)
    {
        //Create category Header (title with +/- button)
        Orion_TrafficAnalysis_TrafficViewBuilder_CategoryContainer categoryContainer = (Orion_TrafficAnalysis_TrafficViewBuilder_CategoryContainer)Page.LoadControl("~/Orion/TrafficAnalysis/TrafficViewBuilder/CategoryContainer.ascx");
        categoryContainer.Title = editor.Title;
        categoryContainer.ContentExpandedByDefault = contentIsExpanded;
        categoryContainer.ID = editor.ID + "Category";
        categoryContainer.LoadUiContent += new EventHandler(delegate(object sender, EventArgs e) { editor.LoadUiContent(); });        
        categoryContainer.LoadUiContentAsync = loadAsync;
        categoryContainer.ContentTemplateContainer.Controls.Add(editor);
        editor.SubTitleChanged += new EventHandler<TitleChangedEventArgs>(delegate(object sender, TitleChangedEventArgs e) { categoryContainer.SubTitle = e.SubTitle; });

        //Create hiaerchy of controls
        this.filterEditorsDiv.Controls.Add(categoryContainer);
        categoryContainer.CategoryDivIdSuffix = editor.AppRelativeVirtualPath; //It's used for hidding / showing global filters according to current detail fitler
    }

    protected void SaveClick(object sender, EventArgs e)
    {
        if (!Profile.AllowCustomize)
            return;

        string url;
        try
        {
            url = BuildUrl();
            string name = HttpUtility.HtmlEncode(txtSaveName.Value);

            log.DebugFormat("Creating TrafficBuilder Link for user {0} ({2}): {1}", Profile.UserName, url, txtSaveName.Value);
            TrafficViewBuilderDAL.SaveUserLink(Profile.UserName, name, url);

            //Response.Redirect(url);
            ScriptManager.RegisterStartupScript(this.btnSave, this.btnSave.GetType(), "TrafficViewBuilderOK",
                "alert(' " + String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_104, txtSaveName.Value.Replace("'", "\\'")) + "'); window.location.href='" + url + "';", true);
        }
        catch (Exception ex)
        {
            string msg = ex is IHasLocalizedMessage ? (ex as IHasLocalizedMessage).LocalizedMessage : ex.Message;
            msg = HttpUtility.JavaScriptStringEncode(msg);
            log.Warn("Failed to save a user link", ex);
            ScriptManager.RegisterStartupScript(this.btnSave, this.btnSave.GetType(), "TrafficViewBuilderSubmitErrorMessage", "alert('" + msg + "');", true);
        }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string url;
        try
        {
            url = BuildUrl();
            Response.Redirect(url);
        }
        catch (ThreadAbortException)
        {
            // According MSDN - Response.Redirect always abort current thread
        }
        catch (Exception ex)
        {
            string msg = ex is IHasLocalizedMessage ? (ex as IHasLocalizedMessage).LocalizedMessage : ex.Message;
            msg = HttpUtility.JavaScriptStringEncode(msg);
            log.Warn("Failed to submit changes", ex);
            ScriptManager.RegisterStartupScript(this.btnSubmit, this.btnSubmit.GetType(), "TrafficViewBuilderSubmitErrorMessage", "alert('" + msg + "');", true);
        }
    }

    public IFilterCollection GetFilters()
    {
        return GetFilters(false);
    }

    public IFilterCollection GetAlertFilters()
    {
        return GetFilters(true);
    }

    private IFilterCollection GetFilters(bool forAlerting)
    {
        IFilterCollection filters = null;
        try
        {
            filters = forAlerting ? this.viewTypeSelector.GetAlertFilters() : this.viewTypeSelector.GetFilters();
        }
        catch (Exception ex)
        {
            log.Warn("Failed to get filters", ex);
        }

        if (filters != null)
        {
            foreach (TvbFilterEditorBase editor in this.filterEditors)
            {
                bool add = true;

                //Don't add filter which is already defined as view definition filter for detail view
                if (filters.ViewDefinitionFilter != null && editor is TvbFilterEditorWithTrackerBase)
                {
                    if ((editor as TvbFilterEditorWithTrackerBase).GetFilterType()
                        == filters.ViewDefinitionFilter.GetType())
                        add = false;
                }

                if (add)
                {
                    try
                    {
                        editor.SaveToFilters(filters);
                    }
                    catch (Exception ex)
                    {
                        if (filters.ViewDefinitionFilter != null && editor is TvbFilterEditorWithTrackerBase)
                        {
                            log.Debug(string.Format("Failed to get and save filter {0}", (editor as TvbFilterEditorWithTrackerBase).GetFilterType()), ex);
                        }
                        else
                        {
                            log.Debug("Failed to get and save filter", ex);
                        }
                    }
                }
            }
        }

        return filters;
    }

    private string BuildUrl()
    {
        string url;
        IFilterCollection filters;
        url = this.viewTypeSelector.GetUrlAndFilters(out filters);

        foreach (TvbFilterEditorBase editor in this.filterEditors)
        {
            bool add = true;

            //Don't add filter which is already defined as view definition filter for detail view
            if (filters.ViewDefinitionFilter != null && editor is TvbFilterEditorWithTrackerBase)
            {
                if ((editor as TvbFilterEditorWithTrackerBase).GetFilterType() == filters.ViewDefinitionFilter.GetType())
                    add = false;
            }

            if (add)
                editor.SaveToFilters(filters);
        }
        url = url + filters.FilterID;

        return url;
    }

}