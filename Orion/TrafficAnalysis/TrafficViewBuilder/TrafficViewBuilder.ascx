﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrafficViewBuilder.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_TrafficViewBuilder" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="tvb" TagName="ViewTypeSelector" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/ViewTypeSelector.ascx" %>
<%@ Register TagPrefix="nta" TagName="FlowDirectionSelector" Src="~/Orion/TrafficAnalysis/Utils/FlowDirectionSelector.ascx" %>
<%@ Register TagPrefix="tvb" TagName="CategoryContainer" Src="~/Orion/TrafficAnalysis/TrafficViewBuilder/CategoryContainer.ascx" %>
<%@ Register TagPrefix="orion" TagName="HelpButton" Src="~/Orion/Controls/HelpButton.ascx" %>
<script type="text/javascript">
    function TVB_Collapse() {
        $('#tvbExpandedTd').hide(500);
        $('#tvbCollapsedTd').show(500);
        // when autorefresher is active and TVB is hidden then autorefresher is enabled
        if (typeof orionPageRefreshTimeout != 'undefined' && typeof orionPageRefreshMilSecs != 'undefined')
            orionPageRefreshTimeout = setTimeout('window.location.reload(true)', orionPageRefreshMilSecs);
    }
</script>

<div id="tvbCollapsedTd" valign="top" class="TrafficViewBuilderPanelCollapsed" style="cursor: pointer" onmousedown="
                 $('#tvbExpandedTd').show(500);
                 $('#tvbCollapsedTd').hide(500);
                 // when autorefresher is active and TVB is displayed then autorefresher is disabled
                 if (typeof orionPageRefreshTimeout != 'undefined') 
                    clearTimeout(orionPageRefreshTimeout);                 
                 ">
    <img src="/Orion/TrafficAnalysis/images/Button.TrafficViewBuilder.Expand.png" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_27 %>" id="expandButton" />
    <img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("TrafficAnalysis", "TrafficViewbuilder.CollapsedHeader.Title.gif") %>" style="padding-left: 2px" />
</div>
<div id="tvbExpandedTd" class="TrafficViewBuilderPanelExpanded" style="display: none">
    <div class="TrafficViewBuilderPanelTitle">
        <table width="100%">
            <tr>
                <td style="width: 99%; cursor: pointer" onmousedown="TVB_Collapse()">
                    <%= Resources.NTAWebContent.NTAWEBDATA_OSC9%>
                </td>
                <td align="right" style="width: 1%">
                    <div style="padding-top: 1px; padding-right: 5px;">
                        <orion:helpbutton id="TrafficViewBuilderHelpButton" helpurlfragment="OrionNetFlowPHTrafficViewBuilder" runat="server" />
                    </div>
                </td>
                <td align="right" style="width: 1%; cursor: pointer" onmousedown="TVB_Collapse()">
                    <img src="/Orion/TrafficAnalysis/images/Button.TrafficViewBuilder.Collapse.png" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_28 %>"
                        id="Img1" />
                </td>
            </tr>
        </table>
    </div>

    <tvb:categorycontainer title="<%$ Resources: NTAWebContent, NTAWEBCODE_TM0_100%>" contentexpandedbydefault="true" id="viewTypeCategory" loaduicontentasync="false" runat="server">
                <ContentTemplate>
                    <tvb:ViewTypeSelector runat="server" ID="viewTypeSelector" ValidationEnabled="true" />
                </ContentTemplate>
            </tvb:categorycontainer>

    <div id="filterEditorsDiv" runat="server">
    </div>

    <asp:updatepanel id="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="sw-btn-bar" id="submitDiv" style="padding-left: 10px;">
                        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" 
                                                 OnClick="SubmitClick" DisplayType="Primary" />

                        <orion:LocalizableButton runat="server" ID="btnSave" LocalizedText="CustomText"  
                                                 Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_98%>" OnClick="SaveClick" DisplayType="Secondary" />
                        <asp:HiddenField runat="server" ID="txtSaveName" />
                    </div>
                    <div class="TvbSubmit" id="submittingDiv" style="display: none">
                        <img src="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" alt="" />&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_96%>
                    </div>
                    <script type="text/javascript">
                        function onSaveClick() {
                            var name = prompt("<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_TM0_3)%>");
                            if (name != null && name != "") {
                                name = name.replace(/^\s+|\s+$/g, '');
                                if (name != "") {
                                    document.getElementById("<%=txtSaveName.ClientID %>").value = name;
                                    return true;
                                }
                            }
                            alert("<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_TM0_4)%>");
                            return false;
                        }
                    </script>
                </ContentTemplate>
            </asp:updatepanel>
</div>
