﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoryContainer.ascx.cs" Inherits="Orion_TrafficAnalysis_TrafficViewBuilder_CategoryContainer" %>
<div class="TvbCategory" id="CategoryDiv_<%=this.CategoryDivIdSuffix %>">

    <asp:Button ID="loadUiContentButton" runat="server" OnClick="loadUiContentButton_Click" Style="display: none;" />
    
    
    <div class="TvbCategoryHeader">
        <div id="toggleButton" class="TvbCategoryCollapsed" runat="server">
            <asp:Label ID="titleLabel" runat="server" Style="margin-left: 20px" />
            <asp:Label ID="subTitleLabel" runat="server" Style="margin-left: 5px;font-weight: normal" />
        </div>
    </div>

    <asp:UpdatePanel runat="server" ID="updatePanel" UpdateMode="Conditional">
        <ContentTemplate>           
            <input id="asyncStatus" type="hidden" value="" runat="server" />
            <input id="subTitleHf" type="hidden" value="" runat="server" />
            <div id="contentDiv" runat="server" class="TvbCategoryContent">
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="loadUiContentButton" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

</div>

<script type="text/javascript">
    $(document).ready(function () {
       
        $('#<%=this.toggleButton.ClientID %>').click(function () {
            <%=this.contentDiv.ClientID %>_toggle(500); 
        
        });

        if ('<%=this.ContentExpandedByDefault %>' == 'True')
        {
            <%=this.contentDiv.ClientID %>_toggle(0);
        }
        else
        {
            $('#<%=this.contentDiv.ClientID %>').hide(0);
        }
    });
    
    //We need to write value to subTitle from HF after each async postback, because div header can't be in updatepanel.
    Sys.Application.add_load(function() {
        $('#<%=this.subTitleLabel.ClientID %>').html(document.getElementById("<%=subTitleHf.ClientID%>").value);
    });
    
    function <%=this.contentDiv.ClientID %>_toggle(fadeTime) {

        if ($('#<%=this.toggleButton.ClientID %>').attr("class") == "TvbCategoryExpanded") {

            $('#<%=this.contentDiv.ClientID %>').hide(fadeTime);
            $('#<%=this.toggleButton.ClientID %>').attr("class", "TvbCategoryCollapsed");
        }
        else {
            var dataLoadStatus = document.getElementById("<%=asyncStatus.ClientID%>");
            
            if (dataLoadStatus.value == 'NotLoaded')
            {
                $('#<%=this.subTitleLabel.ClientID %>').hide(500);
                dataLoadStatus.value='Load';
                $('#<%=loadUiContentButton.ClientID %>').click();
            }

            $('#<%=this.contentDiv.ClientID %>').show(fadeTime);
            $('#<%=this.toggleButton.ClientID %>').attr("class", "TvbCategoryExpanded");
        }
    }

</script>
