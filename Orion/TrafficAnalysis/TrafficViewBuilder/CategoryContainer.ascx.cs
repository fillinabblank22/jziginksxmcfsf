﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_CategoryContainer : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (!this.ContentExpandedByDefault)
            {
                this.contentDiv.Attributes["style"] = "display:none";
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.titleLabel.Text = this.Title;

        if (ContentTemplate != null)
        {
            ContentTemplate.InstantiateIn(this.contentDiv);
        }

        if (!this.IsPostBack)
        {
            if (this.LoadUiContentAsync)
            {
                this.asyncStatus.Value = "NotLoaded";
            }
            else
            {
                OnLoadUiContent();
            }
        }
    }

    protected void loadUiContentButton_Click(Object sender, EventArgs e)
    {
        this.contentDiv.Attributes["style"] = "display:inherit";
        this.toggleButton.Attributes["class"] = "TvbCategoryExpanded";
        this.OnLoadUiContent();
    }

    protected virtual void OnLoadUiContent()
    {
        this.contentDiv.Attributes["style"] = "display:block";

        this.asyncStatus.Value = "Loaded";
        if (this.LoadUiContent != null)
            this.LoadUiContent(this, new EventArgs());

    }
    
    /// <summary>Subtitle which is shown on the right side of the Title</summary>   
    public string SubTitle
    {
        get
        {
            return this.subTitleHf.Value;
        }
        set
        {
            this.subTitleHf.Value = value;
        }
    }
    
    /// <summary>Content of the category container</summary>
    [PersistenceModeAttribute(PersistenceMode.InnerProperty)]
    [BrowsableAttribute(false)]
    [TemplateInstanceAttribute(TemplateInstance.Single)]
    public ITemplate ContentTemplate { get; set; }
    
    /// <summary>Container for category content. It's mainly used for adding controls in code behind.</summary>
    [BrowsableAttribute(false)]
    public Control ContentTemplateContainer { get { return this.contentDiv; } }

    public string CategoryDivIdSuffix { get; set; }

    /// <summary>
    /// Determine if control will load content asynchronously after user expand the header.
    /// If is set to false, content will be loaded synchronously when control is created (in OnInit)
    /// </summary>
    public bool LoadUiContentAsync { get; set; }

    /// <summary>Title which will be in the header</summary>
    public string Title { get; set; }

    /// <summary>
    /// Determine if the content is expanded after load or not
    /// By default isn't expanded, is collapsed
    /// </summary>
    public bool ContentExpandedByDefault { get; set; }
    
    /// <summary>Fire event when user expand the conent for first time and LoadUiContentAsync is true.</summary>
    public event EventHandler LoadUiContent;




}