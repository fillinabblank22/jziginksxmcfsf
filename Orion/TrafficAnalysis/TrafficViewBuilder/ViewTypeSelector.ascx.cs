﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;
using System.Collections.Specialized;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;

public enum TvbViewType
{
    Summary,
    Detail
}

public partial class Orion_TrafficAnalysis_TrafficViewBuilder_ViewTypeSelector : UserControl
{
    private NetflowView view;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (ViewType == TvbViewType.Summary)
            {
                this.detailContentDiv.Attributes["style"] = "display:none";
            }
            if (ViewType == TvbViewType.Detail)
            {
                this.summaryContentDiv.Attributes["style"] = "display:none";
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        view = this.Page as NetflowView;
        base.OnInit(e);
    }

    private TvbViewType ViewType
    {
        get
        {
            if (this.summaryRb.Checked)
                return TvbViewType.Summary;
            else
                return TvbViewType.Detail;
        }
        set
        {
            if (value == TvbViewType.Summary)
                this.summaryRb.Checked = true;

            if (value == TvbViewType.Detail)
                this.detailRb.Checked = true;
        }
    }


    protected void loadDetailUiContentButton_Click(object sender, EventArgs e)
    {
        this.detailContentLoadingDiv.Visible = false;
        this.detailContentLoadedDiv.Visible = true;
        this.viewTypeDetail.LoadUiContent();
    }

    protected string DetailShownFilterHiddenFieldClientID { get { return this.viewTypeDetail.ShownFilterHiddenFieldClientID; } }

    public void LoadFromInfo(ViewTypeLoadInfo loadInfo)
    {
        if (loadInfo.Filters is UniversalFilterCollection)
        {
            this.ViewType = TvbViewType.Summary;
            this.viewTypeSummary.LoadFromInfo(loadInfo);
            this.viewTypeDetail.LoadFromInfo(new ViewTypeLoadInfo() { Filters = loadInfo.Filters }); //Just load filters even for detail content
        }
        else
            if (loadInfo.Filters is FilterCollection)
            {
                this.ViewType = TvbViewType.Detail;
                this.viewTypeDetail.LoadFromInfo(loadInfo);
            }
            else
                throw new NotImplementedException(String.Format("Filter collection: {0} isn't supported.", loadInfo.Filters.GetType()));  // This is exception which can appear only during development when the control is used in wrong way
    }

    public IFilterCollection GetAlertFilters()
    {
        return GetFilters(true);
    }

    public IFilterCollection GetFilters()
    {
        return GetFilters(false);
    }

    private IFilterCollection GetFilters(bool forAlerting)
    {
        if (this.ViewType == TvbViewType.Summary)
        {
            return this.viewTypeSummary.GetFilters();
        }
        else
        if (this.ViewType == TvbViewType.Detail)
        {
            return this.viewTypeDetail.GetFilters(forAlerting);
        }
        else
            throw new NotImplementedException(String.Format("View type: {0} isn't supported.", this.ViewType)); // This is exception which can appear only during development when the control is used in wrong way

    }

    public string GetUrlAndFilters(out IFilterCollection filters)
    {
        if (this.ViewType == TvbViewType.Summary)
        {
            UniversalFilterCollection fc;
            string url = this.viewTypeSummary.GetUrlAndFilters(out fc);
            filters = fc;
            return url;

        }
        else
            if (this.ViewType == TvbViewType.Detail)
            {
                IFilterCollection fc;
                string url = this.viewTypeDetail.GetUrlAndFilters(out fc);
                filters = fc;
                return url;
            }
            else
                throw new NotImplementedException(String.Format("View type: {0} isn't supported.", this.ViewType)); // This is exception which can appear only during development when the control is used in wrong way

    }

    protected void UpdateAlertBuilderButton_Click(object sender, EventArgs e)
    {
        view.UpdateFiltersInAlertBuilder();
    }
}