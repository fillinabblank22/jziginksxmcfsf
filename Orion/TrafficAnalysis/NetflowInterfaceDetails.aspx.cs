using System;
using System.Web.UI;
using System.Web.Services;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Netflow_InterfaceDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlowInterfaceDetails"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.ifResHost.NetflowInterface = this.NetObject as NetflowInterface;
        
        this.nodeLink.NodeID = this.NetflowObject.Node.NetObjectID;
        this.nodeLink.Content.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(this.NetflowObject.Node.Name)));

        this.interfaceLink.InterfaceID = this.NetflowObject.Filters.NetflowSource.FilterID;
        this.interfaceLink.Content.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(this.NetObject.Name)));

        this.Page.Form.Attributes["autocomplete"] = "off";

        base.OnInit(e);
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
