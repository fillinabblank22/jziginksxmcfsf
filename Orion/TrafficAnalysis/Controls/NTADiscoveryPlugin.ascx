﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NTADiscoveryPlugin.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_NTADiscoveryPlugin" %>
<%@ Register Src="~/Orion/Controls/HelpLink.ascx" TagPrefix="orion" TagName="HelpLink" %>
<%@ Register TagPrefix="orion" TagName="ManagedNetObjectsInfo" Src="~/Orion/Admin/ManagedNetObjectsInfo.ascx" %>

<div class="coreDiscoveryIcon">
<asp:Image ID="Image1" runat="server" ImageUrl="/Orion/TrafficAnalysis/images/sources.svg"/>
</div>

<div class="coreDiscoveryPluginBody">
    <h2>
        <%= Resources.NTAWebContent.NTAWEBDATA_VB1_14%></h2>
    <div>
       <%= Resources.NTAWebContent.NTAWEBDATA_VB1_15%></div>
    <span class="LinkArrow">&#0187;</span>

        <orion:HelpLink ID="NTADiscoveryHelpLink" runat="server" HelpUrlFragment="OrionNetFlowPHSummaryActiveSources"
        HelpDescription="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_16%>" CssClass="helpLink" />
    
    <br />
    <br />

    <asp:PlaceHolder ID="netflowSources" runat="server" /> 
    <orion:ManagedNetObjectsInfo ID="ManagedNetObjectsInfo1" runat="server" EntityName="<%$ Resources:NTAWebContent,NTAWEBCODE_VB1_8%>" NumberOfElements="<%# GetTotalCountMonitoredNTASources %>" />
    

   <orion:LocalizableButtonLink ID="LocalizableButtonLink1" NavigateUrl="<%# HelpUrl %>" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_17%>" runat="server" />
</div>
