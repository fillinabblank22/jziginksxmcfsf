<%@ Import Namespace="SolarWinds.Orion.Web.Helpers"%>
<%@ Import Namespace="SolarWinds.Netflow.Common"%>
<%@ Control Language="C#" ClassName="NtaHelpButton" %>

<script runat="server">
	
    private string _helpUrlFragment;

    public string HelpUrlFragment
    {
        get { return _helpUrlFragment; }
        set
        {
            _helpUrlFragment = value;
            this.Visible = !string.IsNullOrEmpty(_helpUrlFragment);
        }
    }

	public static string GetHelpUrl(string fragment)
	{
	    return HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, fragment);
	}
	
    protected string HelpURL
    {
        get
        {
			return GetHelpUrl(this.HelpUrlFragment);
        }
    }

	private string _imageName = "Help";

	public string ImageName
	{
		get { return _imageName; }
		set { _imageName = value; }
	}

    private string _title = Resources.NTAWebContent.NTAWEBDATA_AK0_6;

    public string Title
    {
        get { return _title; }
        set { _title = value; }
    }
    
</script>
<style type="text/css">
.NtaHelpButton
{
	background-position:left center;
	background-repeat:no-repeat;
	padding:2px 0px 2px 20px;
	background-image:url('/Orion/images/Icon.Help.gif'); 
	font-weight:normal;
}

.NtaHelpButtonDiv { float:right;  margin-bottom: -20px; }

a.NtaHelpButton:hover { text-decoration:underline; }
</style>

<div class="NtaHelpButtonDiv">
<a class="NtaHelpButton" href="<%= this.HelpURL %>" target="_blank" rel="noopener noreferrer" title="<%= this.Title %>">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_6%>
</a>
</div>
