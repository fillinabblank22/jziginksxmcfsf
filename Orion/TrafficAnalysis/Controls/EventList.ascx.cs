﻿using System.Web.UI;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Controls_EventList : UserControl, IEventList
{
    public object DataSource
    {
        get
        {
            return eventsRepeater.DataSource;
        }
        set
        {
            eventsRepeater.DataSource = value;
        }
    }
}