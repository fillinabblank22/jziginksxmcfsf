﻿<%@ Control Language="C#" CodeFile="DnsInfo.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_DnsInfo" %>

<span runat="server" id="ResolvedHostname"></span>
<img runat="server" id="DnsImage" visible="false" alt="<%$ Resources: NTAWebContent,NTAWEBDATA_MM0_58 %>" title="<%$ Resources: NTAWebContent,NTAWEBDATA_MM0_58 %>" style="width:10px;height:10px"/>

