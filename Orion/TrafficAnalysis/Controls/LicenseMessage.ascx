﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseMessage.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_LicenseMessage" %>

<div runat="server" id="NetFlowNotificationBarHolder" 
style="background-image: url('/Orion/TrafficAnalysis/images/error_banner_background.gif'); 
    height:27px; background-repeat:repeat-x; color:White; font-weight:bold; padding:4px; 
    white-space:nowrap; ">
    &nbsp;<%=NotificationBarText %>
</div>