﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowNotificator.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_NetflowNotificator" %>

<div id="netflow-notificator">
    <asp:Repeater ID="NetflowNotificatorRepeater" runat="server" OnInit="NetflowNotificatorRepeater_Init">
    <HeaderTemplate>
        <ul style="margin: 0; padding: 0;">
    </HeaderTemplate>
    <ItemTemplate>
            <li id="<%# (bool) Eval("IsDismissable") ? Eval("Code") : string.Empty%>" class="<%#Eval("Type")%> notification-body" style="width: 100%; overflow: hidden;">
                <div class="status" style="width: <%#ToPercent(Eval("Progress"), 3)%>%;"></div>
                <div class="percent" style="display: <%#ToDisplay(Eval("Progress"))%>">
                    <%#ToPercent(Eval("Progress"), 2)%>%
                </div>
                <div class="message">
                    &nbsp;<%#Eval("Message")%>
                </div>
                <div class="dismiss" runat="server" Visible='<%# (bool) Eval("IsDismissable")  %>'>
                    <a class="notification-close-button">
                        <img src="/Orion/TrafficAnalysis/images/exclamation.gif"/>
                    </a>
                </div>
            </li>
    </ItemTemplate>
    <FooterTemplate>
		</ul>
    </FooterTemplate>
    </asp:Repeater>
</div>

<script>
	$(document).ready(function() {
		$('.notification-close-button').click(function (e) {
			e.preventDefault();

			var self = $(this);
			var notificationBody = self.closest('li.notification-body');
			var notificationId = notificationBody.attr('id');

			// Hide notification.
			notificationBody.hide();

			// Dismiss this notification to prevent showing it in future.
			var data = JSON.stringify({
				notificationId: notificationId
			});

			$.ajax({
				type: "POST",
				data: data,
				url: "/Orion/TrafficAnalysis/WebServices/NetflowNotificatorService.asmx/DismissNotification",
				contentType: "application/json; charset=utf-8",
				dataType: "json"
			});
		});
	});
</script>