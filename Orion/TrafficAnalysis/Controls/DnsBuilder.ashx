﻿<%@ WebHandler Language="C#" Class="DnsBuilder" %>

using System;
using System.Web;
using SolarWinds.Netflow.Web.Reporting.DNS;
using SolarWinds.Orion.Web.Helpers;

/// <summary>
/// Build Dns Query
/// </summary>
public class DnsBuilder : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        HttpResponse response = context.Response;

        response.ContentType = "text/plain";

        response.Cache.SetCacheability(HttpCacheability.Public);
        response.Cache.SetValidUntilExpires(true);
        response.Cache.SetExpires(DateTime.Now.AddHours(1));

        string hostname = WebSecurityHelper.SanitizeHtml(context.Request.QueryString["hostname"]);
        string ip = WebSecurityHelper.SanitizeHtml(context.Request.QueryString["ip"]);

        var dnsResult = OnDemandDns.Instance.Resolve(hostname, ip).ToString();
        response.Write(dnsResult);
    }

    public bool IsReusable => false;
}

