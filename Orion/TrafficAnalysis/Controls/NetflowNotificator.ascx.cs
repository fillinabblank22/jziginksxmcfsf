﻿using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Netflow.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using SolarWinds.Netflow.Contracts;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Core.Common.DALs;
using SolarWinds.Orion.Web.DAL;

public partial class Orion_TrafficAnalysis_Controls_NetflowNotificator : System.Web.UI.UserControl
{
    private static IDALProvider dalProvider = DALProvider.GetInstance();
    private const string SettingsKeyDismissedNofications = "TrafficAnalysis_DismissedNofications";

    protected void Page_Load(object sender, EventArgs e) =>
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(this.Page, "/Orion/TrafficAnalysis/styles/Netflow.css");

    private List<string> GetDismissedNofications() =>
        JsonConvert.DeserializeObject<List<string>>(WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, SettingsKeyDismissedNofications, "[]")) ?? new List<string>();

    protected void NetflowNotificatorRepeater_Init(object sender, EventArgs e)
    {
        if (NetflowNotificatorRepeater.DataSource == null)
        {
            var dismissedNofications = GetDismissedNofications();
            var notifications = dalProvider.GetDAL<INetflowNotificationsDAL>()
                .GetNetflowNotifications().Where(x => !dismissedNofications.Contains(x.Code.ToString()));

            NetflowNotificatorRepeater.DataSource = LocalizeSpecialNotifications(notifications).ToArray();
        }

		NetflowNotificatorRepeater.DataBind();
    }

    protected static string ToPercent(object value, int decimals)
    {
        var percent = Convert.ToSingle(value) * 100;
        var format = string.Format("{{0:F{0}}}", decimals);
        return string.Format(CultureInfo.InvariantCulture.NumberFormat, format, percent);
    }

    protected static string ToDisplay(object value)
    {
        var percent = Convert.ToSingle(value);
        var isvisible = percent > 0.0f && percent < 1.0f;
        return isvisible ? "inherit" : "none";
    }

    protected static IEnumerable<NetflowNotificationItem> LocalizeSpecialNotifications(IEnumerable<NetflowNotificationItem> notifications)
    {
        if (notifications == null)
            throw new ArgumentNullException("notifications");

        foreach (var notification in notifications)
        {
            var code = notification.Code.ToString().ToUpper();
            notification.Progress = notification.Progress < 0 ? 0 : notification.Progress;
            switch (code)
            {
                case NetflowNotificationCodes.SQLExpress:
                    notification.Message = String.Format(Resources.NTAWebContent.NTAWEBDATA_NOTIFICATION_SQLEXPRESS);
                    break;

                case NetflowNotificationCodes.SqlExpressDbSizeLimit:
                    // Limit warning value has been stored in the AdditionalData property.
                    string limitWarningInPercents = notification.AdditionalData;

                    notification.Message = String.Format(Resources.NTAWebContent.NTAWEBDATA_NOTIFICATION_SQLEXPRESS_DATABASE_SIZE, limitWarningInPercents);

		            break;

                default:
                    break;
            }

            yield return notification;
        }
    }
}