﻿using System;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Common;

public partial class Orion_TrafficAnalysis_Controls_NTADiscoveryPlugin : System.Web.UI.UserControl
{
    private static Log _log;
    private static Log _logger { get { return _log ?? (_log = new Log()); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataBind(); 
    }

    protected int GetTotalCountMonitoredNTASources
    {
        get
        {
            return new AllNetflowSourcesDAL().GetFlowNodesCount();
        }
    }

    protected string HelpUrl
    {
        get { return HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, "OrionNetFlowPHSetupFirstNetFlow"); }
    }
}