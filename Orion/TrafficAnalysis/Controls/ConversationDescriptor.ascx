﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversationDescriptor.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_ConversationDescriptor" %>
<%@ Register TagPrefix="netflow" TagName="DnsInfo" Src="~/Orion/TrafficAnalysis/Controls/DnsInfo.ascx" %>

<netflow:DnsInfo ID="dnsInfoFirst" runat="server" />
<netflow:DnsInfo ID="dnsInfoSecond" runat="server" />