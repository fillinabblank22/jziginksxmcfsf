﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;

public partial class Orion_TrafficAnalysis_Controls_ConversationDescriptor : System.Web.UI.UserControl
{
    public string HostName1
    {
        get { return dnsInfoFirst.HostName; }
        set { dnsInfoFirst.HostName = value; }
    }
    public string HostName2
    {
        get { return dnsInfoSecond.HostName; }
        set { dnsInfoSecond.HostName = value; }
    }

    public string IPAddress1
    {
        get { return dnsInfoFirst.AttachedIP; }
        set { dnsInfoFirst.AttachedIP = value; }
    }
    
    public string IPAddress2
    {
        get { return dnsInfoSecond.AttachedIP; }
        set { dnsInfoSecond.AttachedIP = value; }
    }

    public bool NotInUpdatePanel { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.dnsInfoFirst.NotInUpdatePanel = NotInUpdatePanel;
        this.dnsInfoSecond.NotInUpdatePanel = NotInUpdatePanel;
    }

    protected override void Render(HtmlTextWriter writer)
    {
        StringBuilder sb = new StringBuilder();

        using(StringWriter tw = new StringWriter(sb))
        using (HtmlTextWriter hw = new HtmlTextWriter(tw))
        {
            this.dnsInfoFirst.RenderControl(hw);
            string dnsInfoFirstRendered = sb.ToString();

            sb.Clear();

            this.dnsInfoSecond.RenderControl(hw);
            string dnsInfoSecondRendered = sb.ToString();

            writer.Write("<span>" + Resources.NTAWebContent.NTAWEBCODE_VB1_48 + "</span>", dnsInfoFirstRendered, dnsInfoSecondRendered);
        }
    }

}