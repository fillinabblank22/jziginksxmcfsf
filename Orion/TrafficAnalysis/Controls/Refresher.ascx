<%@ Control Language="C#" ClassName="Refresher" EnableViewState="false" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>
<%@ Import Namespace="SolarWinds.Netflow.Common" %>

<script runat="server">
    protected override void OnPreRender(EventArgs e)
    {
        if (GlobalSettingsDALCached.AutoRefreshEnabled)
        {
            string reloadScript = string.Format(@"
var orionPageRefreshTimeout=setTimeout('window.location.reload(true)', {0});
var orionPageRefreshMilSecs = {0};", GlobalSettingsDALCached.AutoRefreshSeconds * 1000);

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "timeout", reloadScript, true);
            base.OnPreRender(e);
        }
    }
</script>