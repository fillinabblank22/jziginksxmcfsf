﻿using System;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DAL;

public partial class Orion_TrafficAnalysis_Controls_LicenseMessage : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bool upgradeRunning = UpgradeDAL.IsUpgradeRunning;
            this.Visible = !OrionConfiguration.IsDemoServer && (!LicenseHelper.IsLicensed || LicenseHelper.IsEval || upgradeRunning);
            
            // show or hide Error Notification bar
            NetFlowNotificationBarHolder.Visible = ShowLicenseBar || upgradeRunning;
        }
    }

    private bool ShowLicenseBar
    {
        get { return LicenseHelper.CommunicationFailed; }
    }

    /// <summary>
    /// Used in Error Notification Bar
    /// </summary>
    protected string NotificationBarText
    {
        get
        {
            string frm;

            if (false == ShowLicenseBar && UpgradeDAL.IsUpgradeRunning)
            {
                frm = string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_39, "<a href='{0}' target='_blank' rel='noopener noreferrer' title='{1}'><u style='color:white'>", "</u></a>");
                return string.Format(frm, HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, "OrionNetFlowPHUpgradeInformation"), Resources.NTAWebContent.NTAWEBCODE_AK0_40);
            }

            frm = string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_41, "<a href='{0}' target='_blank' rel='noopener noreferrer' title='{1}'><u style='color:white'>", "</u></a>");
            return string.Format(frm, HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, "OrionNetFlowPHLicensingError"), Resources.NTAWebContent.NTAWEBCODE_AK0_40);
        }
    }
}
