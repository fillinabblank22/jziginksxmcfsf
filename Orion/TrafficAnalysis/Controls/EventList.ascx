﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventList.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_EventList" %>

<orion:Include File="/Styles/LegacyEvents.css" runat="server" />

<asp:Repeater ID="eventsRepeater" runat="server">
	<HeaderTemplate>
		<table width="100%" cellspacing="0" cellpadding="0" class="Events">
	</HeaderTemplate>
			
	<ItemTemplate>
		<tr>
			<td width="20" style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">&nbsp;</td>
			<td style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;" width="125"><%#Eval("EventTime", "{0:g}") %></td>
			<td style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;" width="19">
				<img alt="EventImg" src='/Orion/TrafficAnalysis/images/Events/Event-<%#Eval("EventType")%>.gif' border="0">
			</td>
			<td style="background-color:#<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;"><%#Eval("Message")%></td>
		</tr>
	</ItemTemplate>
			
	<FooterTemplate>
		</table>
	</FooterTemplate>
</asp:Repeater>