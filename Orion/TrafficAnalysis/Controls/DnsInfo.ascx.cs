﻿using System;
using System.ComponentModel;
using System.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.DNS;

/// <summary>
/// 
/// </summary>
public partial class Orion_TrafficAnalysis_Utils_DnsInfo : UserControl
{    
    private bool async;

    public Orion_TrafficAnalysis_Utils_DnsInfo()
    {        
        Async = true;
        DnsResult = null;
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string HostName { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string AttachedIP { get; set; }   

    [PersistenceMode(PersistenceMode.Attribute)]
    public IDnsResult DnsResult { get; private set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool NeedsResolve { get; private set; }

    /// <summary>
    /// If set to true different js code will be generated for when this control is loaded asynchronously through web service (not update panel)
    /// </summary>
    public bool NotInUpdatePanel { get; set; }    

    [PersistenceMode(PersistenceMode.Attribute)]
    [DefaultValue(true)]
    private bool Async
    {
        get
        {
            bool result;
            if (bool.TryParse(Page.Request.QueryString["InlineRendering"], out result) && result)
                return false;

            return async;
        }
        set
        {
            async = value;
        }
    }

    public void TryGetFromCache()
    {
        if (DnsResult == null)
        {
            bool needsResolve;
            DnsResult = OnDemandDns.Instance.GetCached(HostName, AttachedIP, out needsResolve);
            NeedsResolve = needsResolve;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        TryGetFromCache();

        ResolvedHostname.InnerHtml = DnsResult.ToString();
        DnsImage.Src = Page.ResolveUrl("/Orion/TrafficAnalysis/images/animated_loading_16x16.gif");

        if (NeedsResolve)
        {
            NeedsResolve = false;

            if (Async)                
            {                
                DnsImage.Visible = true;
                string callback;

                if (NotInUpdatePanel)
                {
                    // used when this control is loaded by webservice (core charts legend) -> cant use ClientID because they can be the same
                    callback = string.Format(
                        "this.onload = null; $.ajax({{ url: \"/Orion/TrafficAnalysis/Controls/DnsBuilder.ashx\", data: {{hostname:\"{0}\",ip:\"{1}\", cache:\"{3}\" }}, context: this, success: function(data){{ $(this).siblings('#{2}').html(data); $(this).hide(); if (typeof SetTitle == 'function') SetTitle('{0}', data); }} }});",
                        HostName,
                        AttachedIP,
                        ResolvedHostname.ClientID,
                        GlobalSettingsDALCached.DnsOnDemandClientCacheEnabled ? 0 : DateTime.Now.Ticks);
                }
                else
                {
                    // used when in update panel (old charts) -> jQuery.ajax({context:this}) is not resolved correctly in this case and would cause errors
                    callback = string.Format(
                        "this.onload = null; $.get(\"/Orion/TrafficAnalysis/Controls/DnsBuilder.ashx\",{{hostname:\"{0}\",ip:\"{1}\", cache:\"{4}\" }}, function(data){{ $('#{2}').html(data); $('#{3}').hide(); if (typeof SetTitle == 'function') SetTitle('{0}', data); }});",
                        HostName,
                        AttachedIP,
                        ResolvedHostname.ClientID,
                        DnsImage.ClientID,
                        GlobalSettingsDALCached.DnsOnDemandClientCacheEnabled ? 0 : DateTime.Now.Ticks);
                }

                DnsImage.Attributes.Add("onload", callback);
            }
            else
            {
                ResolvedHostname.InnerHtml = OnDemandDns.Instance.Resolve(HostName, AttachedIP).ToString();
            }
        }

        base.OnPreRender(e);
    }
}
