﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditPieChart.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_EditResourceControls_EditPieChart" %>

<%@ Register TagPrefix="nta" TagName="TopResourceEditor" Src="~/Orion/TrafficAnalysis/Controls/EditResourceControls/EditTopResource.ascx" %>

<div class="sw-res-editor">
    <nta:TopResourceEditor ID="topResourceEditor" runat="server" />
</div>