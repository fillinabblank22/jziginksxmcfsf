﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.UI;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Netflow.Web.UI;
using System.Linq;
using System.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Netflow.Web.UI.TopResources;


public partial class Orion_TrafficAnalysis_Controls_EditResourceControls_EditTopResource : UserControl, IChartEditorSettings
{
    private TopResourceProperties resourceProperties;
    private TopResourceParameters resourceParameters;

    private static readonly Dictionary<string, string> chartTypeNameMapping = new Dictionary<string, string>()
    {
        { ChartType.Area.ToString(), "NTA_AreaChart" },
        { ChartType.Pie2D.ToString(), "NTA_PieChart" }
    };

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected bool IsDemo
    {
        get
        {
            return OrionConfiguration.IsDemoServer;
        }
    }

    private bool IsNode { get; set; }

    private string PeriodToSelect
    {
        get
        {
            return HttpUtility.HtmlEncode((string)ViewState["PeriodToSelect"]);
        }
        set
        {
            ViewState["PeriodToSelect"] = value;
        }
    }

    protected bool AllowEditItemCount
    {
        get
        {
            bool result = false;
            bool.TryParse(Request.Params["AllowEditItemCount"], out result);

            return result;
        }
    }

    private bool IsResourceOnSummaryView { get; set; }
    private ICustomPropertyProvidingResource PropertyProvidingResource { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.maxMessagesRv.MaximumValue = "100";
        this.maxMessagesRv.MinimumValue = "1";

        string errorMessage = string.Format(Resources.NTAWebContent.NTAWEBDATA_MM0_30, maxMessagesRv.MinimumValue, maxMessagesRv.MaximumValue);

        this.maxMessagesRv.ErrorMessage = errorMessage;
        this.maxMessagesRfv.ErrorMessage = errorMessage;

        this.periodSelector.ValidationGroup = string.Empty;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (PeriodToSelect != null)
            this.periodSelector.PeriodFilter = TimePeriodUiFilter.Parse(PeriodToSelect);
    }

    private void InitTimePeriodSelector()
    {
        this.usePeriodFromViewCB.Checked = this.resourceParameters.UsePeriodFromView;

        string viewFilter = Request.Params["ViewTimePeriodName"];
        if (string.IsNullOrEmpty(viewFilter))
            throw new Exception("Missing ViewTimePeriodName in query string.");

        if (this.usePeriodFromViewCB.Checked)
            PeriodToSelect = viewFilter;
        else
            PeriodToSelect = this.resourceParameters.ResourceTimePeriod;
        
        this.periodSelector.PeriodFilter = TimePeriodUiFilter.Parse(PeriodToSelect);

        // The radio buttons are not set to reflect the PeriodFilter setter does not set them.
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "SetTimePeriod", string.Format("$(document).ready(function() {{ SetSelectedPeriodName_{0}('{1}'); }});",
            this.periodSelector.ClientID, HttpUtility.JavaScriptStringEncode(PeriodToSelect)), true);
    }

    private void InitChartStyles()
    {
        var chartStyles = new List<Tuple<int, string>>();

        chartStyles.Add(new Tuple<int, string>((int)ChartStyle.Chart, GetLocalizedProperty(typeof(ChartStyle).Name, ChartStyle.Chart.ToString())));
        chartStyles.Add(new Tuple<int, string>((int)ChartStyle.NoChart, GetLocalizedProperty(typeof(ChartStyle).Name, ChartStyle.NoChart.ToString())));

        this.chartStylesDdl.DataSource = chartStyles;
        this.chartStylesDdl.DataValueField = "Item1";
        this.chartStylesDdl.DataTextField = "Item2";
        this.chartStylesDdl.SelectedValue = ((int)this.resourceParameters.ChartStyle).ToString();
        this.chartStylesDdl.DataBind();
    }

    private void InitFlowDirection()
    {
        var flowDirections = new List<Tuple<int, string>>();

        flowDirections.Add(new Tuple<int, string>(
            (int)ChartFD.ChartFlowDirection.NotSet,
            ChartFD.GetText(ChartFD.ChartFlowDirection.NotSet)));

        if (!this.IsNode)
            flowDirections.Add(new Tuple<int, string>(
                (int)ChartFD.ChartFlowDirection.Both,
                ChartFD.GetText(ChartFD.ChartFlowDirection.Both)));

        flowDirections.Add(new Tuple<int, string>(
            (int)ChartFD.ChartFlowDirection.Ingress,
            ChartFD.GetText(ChartFD.ChartFlowDirection.Ingress)));

        flowDirections.Add(new Tuple<int, string>(
            (int)ChartFD.ChartFlowDirection.Egress,
            ChartFD.GetText(ChartFD.ChartFlowDirection.Egress)));

        this.flowDirectionDdl.DataSource = flowDirections;
        this.flowDirectionDdl.DataValueField = "Item1";
        this.flowDirectionDdl.DataTextField = "Item2";

        var selectedFlow = this.resourceParameters.ChartFlowDirection;
        if (selectedFlow == ChartFD.ChartFlowDirection.Both && this.IsNode)
            selectedFlow = ChartFD.ChartFlowDirection.NotSet;

        this.flowDirectionDdl.SelectedValue = ((int)selectedFlow).ToString();
        this.flowDirectionDdl.DataBind();
    }

    private void InitChatType(ResourceInfo resourceInfo)
    {
        IsResourceOnSummaryView = resourceInfo.View.ViewType.IndexOf("Summary", StringComparison.InvariantCultureIgnoreCase) != -1;

        // determine clicked value from POST
        this.useChartStyleFromViewSelectedByUser.Value = WebSecurityHelper.HtmlEncode(Request.Form[this.useChartStyleFromViewSelectedByUser.UniqueID]);

        bool useChartStyleFromView = false;
        if (bool.TryParse(this.useChartStyleFromViewSelectedByUser.Value, out useChartStyleFromView) && useChartStyleFromView) // if value was set (clicked) on the UI by the user set it here
        {
            if (string.Equals(Request.Form[this.useChartStyleFromViewCb.UniqueID], "on")) // checkbox value is returned as "on"/"off" in POST              
                this.useChartStyleFromViewCb.Checked = true;
            else
                this.useChartStyleFromViewCb.Checked = false;
        }
        else
        {
            // set checked value from DB
            if (resourceProperties.UseDefaultChartType.HasValue)
                this.useChartStyleFromViewCb.Checked = resourceProperties.UseDefaultChartType.Value;
            else
                this.useChartStyleFromViewCb.Checked = true;
        }
    }

    /// <summary>
    /// Returns appropriate default chart style (depending on which view is resource placed (summary/detail view)) mapped to ChartName
    /// </summary>
    /// <returns></returns>
    protected string GetDefaultChartTypeName()
    {
        string chartType = (IsResourceOnSummaryView ? GlobalSettingsDALCached.ChartStyleOnSummaryView
            : GlobalSettingsDALCached.ChartStyleOnDetailView);

        return chartTypeNameMapping[chartType];
    }

    protected string GetNtaPieChartTypeName()
    {
        return chartTypeNameMapping[ChartType.Pie2D.ToString()];
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        Control resourceControl = LoadControl(resourceInfo.File);
        TopResourceCoreChartBase topResourceCoreChartControl = null;

        // this is required if our control passed as EmbeddedObjectResource
        // this happens for Custom Object Resource
        // in such case created Descriptor describes the EmbeddedObjectResource while parameters/properties point to the Custom Object Resource
        if (resourceControl is BaseResourceControl && !(resourceControl is TopResourceCoreChartBase) &&
            !string.IsNullOrEmpty(resourceInfo.Properties["EmbeddedObjectResource"]))
            resourceControl = LoadControl(resourceInfo.Properties["EmbeddedObjectResource"]);

        if (resourceControl is TopResourceCoreChartBase)
            topResourceCoreChartControl = resourceControl as TopResourceCoreChartBase;
        else
        {
            string message = string.Format("The passed resource [ID:{0}] nor its EmbeddedObjectResource are of type TopResourceCoreChartBase and cannot be edited using this control.", resourceInfo.ID);

            log.Error(message);
            throw new InvalidOperationException(message);
        }

        topResourceCoreChartControl.Resource = resourceInfo;
        topResourceCoreChartControl.InitDescriptor(false);

        this.resourceProperties = new TopResourceProperties(resourceInfo.Properties);
        this.resourceParameters = topResourceCoreChartControl.Descriptor.Parameters;

        string nodeFilter = Request.Params["IsNode"];
        if (string.IsNullOrEmpty(nodeFilter))
            throw new Exception("Missing IsNode in query string.");
        IsNode = bool.Parse(nodeFilter);
        this.PropertyProvidingResource = topResourceCoreChartControl as ICustomPropertyProvidingResource;
        InitTimePeriodSelector();
        InitChartStyles();
        InitFlowDirection();
        InitChatType(resourceInfo);
        InitCustomConfigurationOptions();
        InitPieChartLabelOptions();
        if (AllowEditItemCount)
        {
            this.tbMaxMessages.Text = this.resourceParameters.ItemCount.ToString();
            this.itemCountDiv.Visible = true;
        }

        // setup correct help link fragment - we obtain it from TopSummaryResourceBase which inherits it from BaseResourceControl
        if (!string.IsNullOrEmpty(Request.Params["HelpLinkFragment"]))
            this.btnHelp.HelpUrlFragment = Request.Params["HelpLinkFragment"];
    }

    private void InitPieChartLabelOptions()
    {
        pieChartDisplayLabelCb.Checked = resourceParameters.PieChartLabelOptions.HasFlag(PieChartLabelOptions.Label);

        pieChartDisplayIconCb.Checked = resourceParameters.PieChartLabelOptions.HasFlag(PieChartLabelOptions.Icon);

        pieChartDisplayPercentageCb.Checked = resourceParameters.PieChartLabelOptions.HasFlag(PieChartLabelOptions.Percentage);
    }

    protected void InitCustomConfigurationOptions()
    {
        if (this.PropertyProvidingResource != null)
        {
            var control = this.PropertyProvidingResource;
            this.ddlOptionsRepeater.Visible = control.DropDownListProperties != null && control.DropDownListProperties.Count() > 0;
            this.ddlOptionsRepeater.ItemDataBound += this.ddlOptionsRepeater_ItemDataBound;
            this.ddlOptionsRepeater.DataSource = control.DropDownListProperties;
            this.ddlOptionsRepeater.DataBind();
        }
    }

    protected void ddlOptionsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        var dataItem = (DropDownListProperty)e.Item.DataItem;
        var propertyLabel = e.Item.FindControl("propertyLabel") as Label;
        propertyLabel.Text = dataItem.DisplayName;
        var propertyKey = e.Item.FindControl("propertyKey") as HtmlInputHidden;
        propertyKey.Value = dataItem.DbKey;
        var propertyValue = e.Item.FindControl("propertyValue") as DropDownList;
        foreach (var option in dataItem.ValuesWithDisplayNames)
        {
            var value = option.Key;
            var displayName = option.Value;
            propertyValue.Items.Add(new ListItem(displayName, value));
        }
        propertyValue.Attributes["automation"] = dataItem.DbKey;
        propertyValue.SelectedValue = this.resourceProperties[dataItem.DbKey, this.PropertyProvidingResource.GetDefaultValue(dataItem.DbKey)];
    }

    private PieChartLabelOptions GetPieChartLabelOptions()
    {
        var labelOptions = PieChartLabelOptions.None;

        if (pieChartDisplayLabelCb.Checked)
        {
            labelOptions |= PieChartLabelOptions.Label;
        }
        if (pieChartDisplayIconCb.Checked)
        {
            labelOptions |= PieChartLabelOptions.Icon;
        }
        if (pieChartDisplayPercentageCb.Checked)
        {
            labelOptions |= PieChartLabelOptions.Percentage;
        }

        return labelOptions;
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        var resourceProperties = new TopResourceProperties(properties);
        bool check = this.useChartStyleFromViewCb.Checked;
        resourceProperties.UseDefaultChartType = this.useChartStyleFromViewCb.Checked;
        if (resourceProperties.UseDefaultChartType.HasValue && resourceProperties.UseDefaultChartType.Value)
        {
            resourceProperties.ChartType = null;
        }
        else
        {
            resourceProperties.UseDefaultChartType = false;
        }
        resourceProperties.ChartStyle = (ChartStyle)int.Parse(this.chartStylesDdl.SelectedValue);
        resourceProperties.ChartFlowDirection = (ChartFD.ChartFlowDirection)int.Parse(this.flowDirectionDdl.SelectedValue);
        if (!string.IsNullOrEmpty(this.tbMaxMessages.Text))
        {
            resourceProperties.ItemCount = int.Parse(this.tbMaxMessages.Text);
        }
        resourceProperties.TimePeriodName = this.periodSelector.PeriodFilter.PeriodName;
        resourceProperties.UsePeriodFromView = this.usePeriodFromViewCB.Checked;
        foreach (RepeaterItem option in this.ddlOptionsRepeater.Items)
        {
            var propertyKey = option.FindControl("propertyKey") as HtmlInputHidden;
            var propertyValue = option.FindControl("propertyValue") as DropDownList;
            var selectedValue = propertyValue.SelectedValue;
            resourceProperties[propertyKey.Value] = selectedValue;
        }
        resourceProperties.PieChartLabelOptions = GetPieChartLabelOptions();
    }
}
