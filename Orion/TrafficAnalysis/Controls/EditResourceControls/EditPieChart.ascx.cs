﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;

public partial class Orion_TrafficAnalysis_Controls_EditResourceControls_EditPieChart : UserControl, IChartEditorSettings
{
    #region IChartEditorSettings Members

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        this.topResourceEditor.Initialize(settings, resourceInfo, netObjectId);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        this.topResourceEditor.SaveProperties(properties);

        var resourceProperties = new TopResourceProperties(properties);

        if (!resourceProperties.UseDefaultChartType.HasValue || !resourceProperties.UseDefaultChartType.Value)
            resourceProperties.ChartType = ChartType.Pie2D;
    }

    #endregion
}