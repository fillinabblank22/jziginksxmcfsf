﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditAreaChart.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_EditResourceControls_EditAreaChart" %>

<%@ Register TagPrefix="nta" TagName="TopResourceEditor" Src="~/Orion/TrafficAnalysis/Controls/EditResourceControls/EditTopResource.ascx" %>
<%@ Register TagPrefix="nta" TagName="ImageSelector" Src="~/Orion/TrafficAnalysis/Utils/ImageSelector.ascx" %>

<orion:Include ID="IncludeJQuery" runat="server" Framework="jQuery" FrameworkVersion="1.7.1" />
<orion:Include ID="IncludeTrafficAnalysis" runat="server" File="/TrafficAnalysis/styles/TrafficAnalysis.css" />

<div class="sw-res-editor">
    <nta:TopResourceEditor ID="topResourceEditor" runat="server" />

    <div class="sw-res-editor-block">
        <b><asp:Label ID="areaTypeLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_26 %>" AssociatedControlID="AreaTypesCtrl" /></b>
        <nta:ImageSelector id="AreaTypesCtrl" runat="server" />
    </div>

    <div class="sw-res-editor-block">
        <b><asp:Label ID="dataUnitsLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_74 %>" AssociatedControlID="dataUnitsDdl" /></b>
        <br />
        <asp:DropDownList ID="dataUnitsDdl" runat="server" ToolTip="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_75 %>" />
    </div>
</div>