﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditTopResource.ascx.cs" Inherits="Orion_TrafficAnalysis_Controls_EditResourceControls_EditTopResource" %>

<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Register TagPrefix="nta" TagName="PeriodSelector" Src="~/Orion/TrafficAnalysis/TimePeriod/PeriodSelector.ascx" %>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta" TagName="NtaHelpButton" %>


<nta:NtaHelpButton ID="btnHelp" runat="server" HelpUrlFragment="OrionNetFlowPHSummaryView" Title="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_78%>" />

<orion:Include ID="IncludeNetflow" runat="server" Module="TrafficAnalysis" File="Netflow.css" />
<orion:Include ID="IncludeTrafficAnalysis" runat="server" Module="TrafficAnalysis" File="TrafficAnalysis.css" />
<orion:Include ID="IncludeDemoScripts" runat="server" Module="TrafficAnalysis" File="DemoScripts.js" />

<input type="hidden" id="demoHidden" value="<%= IsDemo %>" />

<script type="text/javascript">

    $(document).ready(function () {

        $_chb = $("[id$='usePeriodFromViewCB']");
        $_div = $("[id$='periodSelectorDIV']");
        $_divDU = $("[id$='DivDataUnits']");        // div DATA UNITS
        // hidden field to distinguish default value of chart type
        $_hfDefaultChartStyle = $("[id$='hfDefaultChartStyle']");
        $_chartStyleDdl = $("[id$='chartStylesDdl']");

        $_chb.click(function () { showHideTimePeriod(500); });
        $_chartStyleDdl.change(function () { showHidePieChartLabelOptions(500); });

        showHideTimePeriod(0); // for onload
        showHidePieChartLabelOptions(0); // for onload

        if (IsDemo()) {
            var submitButton = $("a[id$=_btnSubmit]");

            submitButton.attr("href", "javascript:void(0)");
            submitButton.click(function () {
                DisplayWarning('<asp:Literal ID="demoWarningMessageLt" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_59 %>" />');
                return false;
            });
        }
    });

    function showHideTimePeriod(fadeTime) {

        if ($_chb.attr('checked')) {
            $_div.hide(fadeTime);
        }
        else {
            $_div.show(fadeTime);
        }
    }

    function showHidePieChartLabelOptions(fadeTime) {

        var chartSelector = $("select[id$=ListOfCharts]"); // core's 'Data'
        var selectedChart = chartSelector.val(); // selected in core's 'Data' drop down list
        var pieChartLabelOptionsDiv = $("#pieChartLabelOptionsDiv");

        if (selectedChart == "<%= GetNtaPieChartTypeName() %>" && $_chartStyleDdl.val() == "<%= (int)ChartStyle.Chart %>") {
            pieChartLabelOptionsDiv.show(fadeTime);
        }
        else {
            pieChartLabelOptionsDiv.hide(fadeTime);
        }
    }

    $(document).ready(function () {

        var useChartStyleFromViewCb = $("input[id$=useChartStyleFromViewCb]");
        var useChartStyleFromViewSelectedByUser = $("input[id$=useChartStyleFromViewSelectedByUser]");
        var defaultChartTypeName = "<%= this.GetDefaultChartTypeName() %>";
        var chartSelector = $("select[id$=ListOfCharts]"); // core's 'Data'
        var selectedChart = chartSelector.val(); // selected in core's 'Data' drop down list

        if (useChartStyleFromViewSelectedByUser.val() == "false") {
            useChartStyleFromViewCb.attr("checked", false);
        }

        if (useChartStyleFromViewSelectedByUser.val() != "false") {
            if (useChartStyleFromViewCb.attr("checked") && defaultChartTypeName != selectedChart) {
                setTimeout(function() {
                    chartSelector.val(defaultChartTypeName);
                    chartSelector.trigger("change", true);
                }, 0);
            }
        }

        useChartStyleFromViewCb.bind("click", function () {
            useChartStyleFromViewSelectedByUser.val("true");

            if (this.checked && defaultChartTypeName != selectedChart) {
                chartSelector.val(defaultChartTypeName);
                chartSelector.trigger("change", true);
            }
        });

        chartSelector.bind("change", function (event, raisedFromCode) {
            if (!raisedFromCode) {
                useChartStyleFromViewSelectedByUser.val("false");
            }
        });
    });

</script>

<div class="sw-res-editor-block">
    <asp:HiddenField runat="server" ID="useChartStyleFromViewSelectedByUser" Value="" />
    <asp:CheckBox runat="server" ID="useChartStyleFromViewCb" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_55 %>" />
</div>

<div ID="itemCountDiv" runat="server" visible="false" class="sw-res-editor-block">
    <b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_43%></b>
    <br />
    <asp:TextBox ID="tbMaxMessages" runat="server"/>
    <asp:RequiredFieldValidator ID="maxMessagesRfv" runat="server" Display="Dynamic" ControlToValidate="tbMaxMessages" />
    <asp:RangeValidator ID="maxMessagesRv" runat="server" Display="Dynamic" ControlToValidate="tbMaxMessages" Type="Integer" />
</div>

<div class="sw-res-editor-block">
    <b><asp:Literal ID="usePeriodFromViewLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_68 %>" /></b>
    <br />
    <asp:CheckBox runat="server" ID="usePeriodFromViewCB" GroupName="PeriodType" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_69 %>" />
    <br />
    <div id="periodSelectorDIV" runat="server" style="display: none;" >
        <nta:PeriodSelector runat="server" ID="periodSelector" ValidationEnabled="true" ForceInitialization="true" />
    </div>
</div>

<div class="sw-res-editor-block"> 
    <b><asp:Label ID="chartStylesLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_70 %>" AssociatedControlID="chartStylesDdl" /></b>
    <br />
    <asp:DropDownList ID="chartStylesDdl" runat="server" />
</div>

<div ID="pieChartLabelOptionsDiv" class="sw-res-editor-block">
    <b><asp:Label ID="pieChartLabelOptionsLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_PieChartLabelOptions %>" /></b>
    <br />
    <asp:CheckBox ID="pieChartDisplayIconCb" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_PieChartLabelOption_DisplayIcon %>" runat="server"/>
    <br />
    <asp:CheckBox ID="pieChartDisplayLabelCb" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_PieChartLabelOption_DisplayLabel %>" runat="server"/>
    <br />
    <asp:CheckBox ID="pieChartDisplayPercentageCb" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_PieChartLabelOption_DisplayPercentage %>" runat="server"/>
    <br />
</div>

<div class="sw-res-editor-block">
    <b><asp:Label ID="flowDirectionLbl" runat="server" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_25 %>" AssociatedControlID="flowDirectionDdl" /></b>
    <br />
    <asp:DropDownList ID="flowDirectionDdl" runat="server" />
</div>

<asp:Repeater ID="ddlOptionsRepeater" runat="server">
    <ItemTemplate>
        <div class="sw-res-editor-block">
            <b><asp:Label ID="propertyLabel" AssociatedControlID="propertyValue" runat="server" /></b>
            <br />
            <input type="hidden" id="propertyKey" runat="server" />
            <asp:DropDownList ID="propertyValue" runat="server" />
        </div>
    </ItemTemplate>
</asp:Repeater>
