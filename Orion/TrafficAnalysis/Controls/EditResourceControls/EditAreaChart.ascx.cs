﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Common;
using SolarWinds.Orion.Web.UI;

public partial class Orion_TrafficAnalysis_Controls_EditResourceControls_EditAreaChart : UserControl, IChartEditorSettings
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    const string urlPrefix = @"/Orion/TrafficAnalysis/images/CoreChartThumbs/";
    const string filePrefix = "Chart";
    const string fileExtension = ".png";

    private TopResourceProperties resourceProperties;
    private TopResourceParameters resourceParameters;
    private TopResourceDescriptor resourceDescriptor;

    private void InitDataUnits()
    {
        var defaultText = ChartDU.DefaultText;
        var dataUnits = new List<Tuple<int, string>>();

        dataUnits.Add(new Tuple<int, string>(
            (int)ChartDU.ChartDataUnit.BitRate,
            string.Format(AddDefault(ChartDU._BITRATE), ChartDU.GetDataUnitsText(ChartDU.ChartDataUnit.BitRate, defaultText))));

        if (IsInterface && !resourceDescriptor.HidePercentOfInterfaceSpeedOption)
            dataUnits.Add(new Tuple<int, string>(
                (int)ChartDU.ChartDataUnit.InterfaceUtilization,
                string.Format(AddDefault(ChartDU._INTERFACE_UTILIZATION), ChartDU.GetDataUnitsText(ChartDU.ChartDataUnit.InterfaceUtilization, defaultText))));

        if (!resourceDescriptor.HidePercentOfTotalTrafficOption)
            dataUnits.Add(new Tuple<int, string>(
                (int)ChartDU.ChartDataUnit.PercentOfTotalBytes,
                string.Format(AddDefault(ChartDU._PERCENT_OF_TOTALBYTES), ChartDU.GetDataUnitsText(ChartDU.ChartDataUnit.PercentOfTotalBytes, defaultText))));

        dataUnits.Add(new Tuple<int, string>(
            (int)ChartDU.ChartDataUnit.TransferredBytes,
            string.Format(AddDefault(ChartDU._TRANSFERRED_BYTES), ChartDU.GetDataUnitsText(ChartDU.ChartDataUnit.TransferredBytes, defaultText))));

        if (!resourceDescriptor.HidePercentOfClassUtilizationOption)
        {
            dataUnits.Add(new Tuple<int, string>(
                (int)ChartDU.ChartDataUnit.PercentOfClassUtilization,
                string.Format(AddDefault(ChartDU._PERCENT_OF_CLASS_UTILIZATION), ChartDU.GetDataUnitsText(ChartDU.ChartDataUnit.PercentOfClassUtilization, defaultText))));

            this.dataUnitsDdl.Attributes.Add("onchange", "if($(this).val() == " + (int)ChartDU.ChartDataUnit.PercentOfClassUtilization + ") $('img[src=\"" + GetAreaChartTypeSrc(AreaChartType.LineChart) + "\"]').parent().click();");
        }

        this.dataUnitsDdl.DataSource = dataUnits;
        this.dataUnitsDdl.DataValueField = "Item1";
        this.dataUnitsDdl.DataTextField = "Item2";

        var selectedUnit = this.resourceParameters.ChartDataUnit;
        if (selectedUnit == ChartDU.ChartDataUnit.InterfaceUtilization && !this.IsInterface)
            selectedUnit = ChartDU.ChartDataUnit.BitRate;

        this.dataUnitsDdl.SelectedValue = ((int)selectedUnit).ToString();
        this.dataUnitsDdl.DataBind();
    }

    private static string GetAreaChartTypeSrc(AreaChartType t)
    {
        return String.Format("{0}{1}{2}{3}", urlPrefix, filePrefix, t, fileExtension);
    }

    private void InitAreaChartType()
    {
        ListItemCollection areaTypesCol = new ListItemCollection();

        foreach (AreaChartType t in Enum.GetValues(typeof(AreaChartType)))
        {
            areaTypesCol.Add(new ListItem(GetLocalizedProperty("AreaChartType", Enum.GetName(typeof(AreaChartType), t)), GetAreaChartTypeSrc(t)));
        }

        AreaTypesCtrl.DataList = areaTypesCol;

        AreaTypesCtrl.SelectedIndex = (int)this.resourceParameters.AreaChartType;
    }


    private bool IsInterface
    {
        get;
        set;
    }

    /// <summary>
    /// If type is the same as global value returns "{type} (default)"
    /// otherwise returns unchanged string passed in {type} parameter
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    private string AddDefault(string type)
    {
        if (type == GlobalSettingsDALCached.ChartGlobalDataUnits)
        {
            return Resources.NTAWebContent.NTAWEBCODE_TM0_73;
        }

        return "{0}";
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    public void Initialize(ChartSettings settings, ResourceInfo resourceInfo, string netObjectId)
    {
        Control resourceControl = LoadControl(resourceInfo.File);
        TopResourceCoreChartBase topResourceCoreChartControl = null;

        // this is required if our control passed as EmbeddedObjectResource
        // this happens for Custom Object Resource
        // in such case created Descriptor describes the EmbeddedObjectResource while parameters/properties point to the Custom Object Resource
        if (resourceControl is BaseResourceControl && !(resourceControl is TopResourceCoreChartBase) &&
            !string.IsNullOrEmpty(resourceInfo.Properties["EmbeddedObjectResource"]))
            resourceControl = LoadControl(resourceInfo.Properties["EmbeddedObjectResource"]);

        if (resourceControl is TopResourceCoreChartBase)
            topResourceCoreChartControl = resourceControl as TopResourceCoreChartBase;
        else
        {
            string message = string.Format("The passed resource [ID:{0}] nor its EmbeddedObjectResource are of type TopResourceCoreChartBase and cannot be edited using this control.", resourceInfo.ID);

            log.Error(message);
            throw new InvalidOperationException(message);
        }

        topResourceCoreChartControl.Resource = resourceInfo;
        topResourceCoreChartControl.InitDescriptor(false);

        this.resourceParameters = topResourceCoreChartControl.Descriptor.Parameters;
        this.resourceDescriptor = topResourceCoreChartControl.Descriptor;

        string interfaceFilter = Request.Params["IsInterface"];
        if (string.IsNullOrEmpty(interfaceFilter))
            throw new Exception("Missing IsInterface in query string.");
        IsInterface = bool.Parse(interfaceFilter);

        InitDataUnits();
        InitAreaChartType();

        this.topResourceEditor.Initialize(settings, resourceInfo, netObjectId);
    }

    public void SaveProperties(Dictionary<string, object> properties)
    {
        this.topResourceEditor.SaveProperties(properties);

        var resourceProperties = new TopResourceProperties(properties);

        resourceProperties.ChartDataUnit = (ChartDU.ChartDataUnit)int.Parse(this.dataUnitsDdl.SelectedValue);
        resourceProperties.AreaChartType = (AreaChartType)this.AreaTypesCtrl.SelectedIndex;

        if (!resourceProperties.UseDefaultChartType.HasValue || !resourceProperties.UseDefaultChartType.Value)
            resourceProperties.ChartType = ChartType.Area;
    }
}