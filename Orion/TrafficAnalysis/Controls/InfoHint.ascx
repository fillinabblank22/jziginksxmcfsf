﻿<%@ Control Language="C#" ClassName="InfoHint" %>
<script runat="server">
    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate HelpContent
    {
        get;
        set;
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        
        if (null != this.HelpContent)
        {
            this.HelpContent.InstantiateIn(this.phContents);
        }
    }
</script>
<table class="sw-suggestion-info infoHint">
    <tr>
        <td style="width: 20px; padding-right: 3px !important; vertical-align: top;">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20" xml:space="preserve">
                <circle fill="#FFFFFF" stroke="#297994" stroke-miterlimit="10" cx="10" cy="10" r="9.5"></circle>
                <path fill="#297994" d="M9,5h2v2H9V5z M9,15h2V9H9V15z"></path>
            </svg>
        </td>
        <td style="vertical-align: middle;">
            <asp:PlaceHolder runat="server" ID="phContents" />
        </td>
    </tr>
</table>
