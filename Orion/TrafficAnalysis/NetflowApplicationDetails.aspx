<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowApplicationDetails.aspx.cs" Inherits="Orion_Netflow_ApplicationDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
      <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AppName()) %>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:ApplicationResourceHost runat="server" ID="appResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:ApplicationResourceHost>
</asp:Content>