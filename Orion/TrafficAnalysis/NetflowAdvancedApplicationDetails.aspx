<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowAdvancedApplicationDetails.aspx.cs" Inherits="Orion_Netflow_AdvancedApplicationDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
      <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AdvancedApplicationName()) %>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:AdvancedApplicationResourceHost runat="server" ID="advancedApplicationResourceHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:AdvancedApplicationResourceHost>
</asp:Content>