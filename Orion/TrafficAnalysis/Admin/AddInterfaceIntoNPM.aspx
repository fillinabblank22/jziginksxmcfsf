﻿<%@ Page Title="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_150%>" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="AddInterfaceIntoNPM.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_AddInterfaceIntoNPM" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Import Namespace="SolarWinds.Netflow.Common" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="nta" TagName="Breadcrumb" Src="~/Orion/TrafficAnalysis/Admin/NetflowSettingsBreadcrumb.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <orion:Include Module="TrafficAnalysis" runat="server" File="TrafficAnalysis.css" />
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionNetFlowPHSettingsUnmanageableSources" />   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <orion:Include Module="TrafficAnalysis" runat="server" File="TrafficAnalysis.css" />
    <orion:Include Module="TrafficAnalysis" runat="server" File="NetFlow.css" />
    <orion:Include Module="TrafficAnalysis" runat="server" File="DemoScripts.js" />
    <orion:Include runat="server" Framework="jQuery" FrameworkVersion="1.7.1" />
   
    <style type="text/css">
    
    .ntasHR { height:1px; color:#DFDED7; background-color:gray; }
    .ntasButton { margin-top:7px; }
    .ntasDIV0 {
        background-color: white;
        border: 1px solid #BBBBBB;
        box-shadow: 2px 2px 3px #DDDDDD;
        margin-bottom: 11px;
        overflow-x: hidden;
        padding: 10px;
        overflow:hidden; /* fix "Charting and Graphing Settings" scrollbar in Chrome */
        margin-top: 10px;
        margin-left: 7px;
        max-width: 70%;
    }
    .ntasDIV1 { background-color: #E4F1F8; padding:7px; line-height:20px; border:0px solid red;  }
    
    .outer {
        position:relative;
        height:100px;
    }
    .inner {
        position:absolute;
        right:0;
        bottom:0;
    }
    
    </style>

    <div class="PageHeader" id="etxxHeader" style='width:auto !Important;'>
        <%=Page.Title%>
    </div>
    <div class="ntasLeft">
        <asp:Label id="subTitle" runat="server" />
        <ul>
            <li id="addInterfaceTitleLi" runat="server"/>
        </ul>
    </div>
    <div class="ntasDIV0">
        <span class="PageHeader">
            <%= Resources.NTAWebContent.NTAWEBDATA_MM0_4 %>
        </span>

        <div class="ntasDIV1" id="generalDiv">
            <table style="margin-left:20%" id="generalTable">
                <tr>
                    <td style="white-space:nowrap">
                        <%= Resources.NTAWebContent.NTAWEBDATA_MM0_6 %>
                    </td>
                    <td>
                        <asp:Label ID="nodeNameLbl" Text="" runat="server" />
                    </td>
                </tr>

                <tr>
                    <td style="white-space:nowrap">
                        <%= Resources.NTAWebContent.NTAWEBDATA_MM0_7 %>
                    </td>
                    <td id="interfaceNameEditTd" runat="server">
                        <asp:TextBox ID="interfaceNameTb" Text="" runat="server" style="width:240px" />
                        &nbsp;
                        <asp:RequiredFieldValidator ID="interfaceNameRequiredValidator" runat="server" ErrorMessage="<%$ Resources:NTAWebContent, NTAWEBDATA_MM0_15 %>" ControlToValidate="interfaceNameTb" Display="Dynamic" ValidationGroup="addInterfaceGroup" />
                    </td>
                    <td id="interfaceNameReadOnlyTd" runat="server">
                        <asp:Literal runat="server" ID="interfaceNameLt"></asp:Literal>
                    </td>
                </tr>

                <tr>
                    <td style="white-space:nowrap">
                        <%= Resources.NTAWebContent.NTAWEBDATA_MM0_8 %>
                    </td>
                    <td>
                        <asp:Literal ID="interfaceIndexLt" Text="" runat="server" />
                    </td>
                </tr>
                
                <tr id="interfaceTypeTr" runat="server">
                    <td style="white-space:nowrap">
                        <%= Resources.NTAWebContent.NTAWEBDATA_OS1_200%>
                    </td>
                    <td>
                        <asp:Literal ID="interfaceTypeLt" runat="server" />
                    </td>
                </tr>
            </table>
        </div>

        <span class="PageHeader" id="speedCategory" runat="server">
            <%= Resources.NTAWebContent.NTAWEBDATA_MM0_5 %>
        </span>

        <div class="ntasDIV1" id="speedDiv" runat="server">
            <table style="margin-left:20%" id="speedTable">
                <tr>
                    <td>
                        <%= Resources.NTAWebContent.NTAWEBDATA_MM0_9 %>
                    </td>
                    <td id="interfaceSpeedEditTd" runat="server">
                        <asp:TextBox ID="interfaceSpeedTb" Text="" runat="server" style="max-height:60px" />
                        &nbsp;
                        <asp:DropDownList ID="interfaceSpeedUnitsDdl" runat="server" />
                        &nbsp;
                        <asp:RequiredFieldValidator ID="interfaceSpeedRequiredValidator" runat="server" ErrorMessage="<%$ Resources:NTAWebContent, NTAWEBDATA_MM0_15 %>" ControlToValidate="interfaceSpeedTb" Display="Dynamic" ValidationGroup="addInterfaceGroup" />
                        <asp:RangeValidator ID="interfaceSpeedPosIntegerValidator" runat="server" ErrorMessage="<%$ Resources:NTAWebContent, NTAWEBDATA_MM0_16 %>" ControlToValidate="interfaceSpeedTb" Type="Double" Display="Dynamic" EnableClientScript="false" ValidationGroup="addInterfaceGroup" />
                    </td>
                    <td id="interfaceSpeedReadonlyTd" runat="server">
                        <asp:Label runat="server" ID="interfaceSpeedLt"></asp:Label>
                    </td>
                </tr>
            </table>
            <div style="margin-left:20%; color: gray" runat="server" id="interfaceSpeedHint">
                <img src="../../images/Icon.Lightbulb.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_2%>" />
                <%= Resources.NTAWebContent.NTAWEBCODE_MK0_4%>
                <a href="<%= HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, "OrionNTAPHUnmanageableInterfaceSpeed") %>" target="_blank" rel="noopener noreferrer" style="color: #336699">&raquo; <%= Resources.NTAWebContent.NTAWEBCODE_MK0_5 %></a>
            </div>
        </div>

        <div style="margin-top:14px; margin-left:35%;">
            <span style="margin-right:20px">
                <orion:LocalizableButton runat="server" LocalizedText="Submit" DisplayType="Primary" OnClick="AddInterface_Click" ValidationGroup="addInterfaceGroup" />
            </span>
            <span>
                <orion:LocalizableButton runat="server" LocalizedText="Cancel" DisplayType="Secondary" OnClick="ReturnToRefferer_Click" />
            </span>
        </div>
    </div>

    <div id="messageBoxModal" style="display:none">
        <div class="outer">
            <asp:Localize ID="messageBoxModalText" runat="server" />
            <div class="inner">
                <orion:LocalizableButton ID="messageBoxModalRedirectButton" runat="server" LocalizedText="OK" DisplayType="Primary" OnClick="ReturnToRefferer_Click"/>
                <orion:LocalizableButton ID="messageBoxModalStayButton" runat="server" LocalizedText="OK" DisplayType="Primary" OnClientClick="$('#messageBoxModal').dialog('close'); return false;"/>
            </div>
        </div>
    </div>

</asp:Content>
