using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Netflow.Contracts.Applications;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.Applications.DAL;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using IpGroupCollisionException = SolarWinds.Netflow.Web.IPGroups.IpGroupCollisionException;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Admin_ManageApps : Page
{
    private static readonly Log Log = new Log();
    protected string monitoredPorts;
    private readonly ApplicationDALCriteria appCriteria = new ApplicationDALCriteria();
    public static readonly string ANY_DESTINATION = "ANY_DESTINATION";
    
    protected enum AppViewType
    {
        All,
        MultiPort,
        SinglePort,
        TCP,
        UDP,
        Disabled,
        Enabled
    }

    #region Properties
    protected AppViewType ViewType { get; private set; }

    protected string SortField
    {
        get
        {
            string sortField = "Port";
            if (!string.IsNullOrEmpty(this.Request.QueryString["SortField"]))
            {
                sortField = this.Request.QueryString["SortField"];
            }
            return sortField;
        }
    }

    protected bool SortDesc
    {
        get
        {
            string sortDirVar = this.Request.QueryString["SortDir"];
            bool sortDesc = false;
            if (!string.IsNullOrEmpty(sortDirVar))
            {
                if (sortDirVar.Trim().Equals("DESC", StringComparison.InvariantCultureIgnoreCase))
                    sortDesc = true;
            }

            return sortDesc;
        }
    }

    protected int PageSize
    {
        // we might make this configurable later
        get
        {
            return 50;
        }
    }

    protected int TotalRecords
    {
        get
        {
            return (null == ViewState["TotalRecords"]) ? 0 : (int)ViewState["TotalRecords"];
        }
        set
        {
            ViewState["TotalRecords"] = value;
        }
    }

    protected int PageNumber
    {
        get
        {
            return this.pagerBottom.PageNumber;
        }
        set
        {
            this.pagerBottom.PageNumber = value;
        }
    }

    /// <summary>
    /// Used for the delete dialog
    /// </summary>
    private string CurrentEditedApp
    {
        get { return (null == ViewState["CurrentEditedApp"]) ? string.Empty : (string)ViewState["CurrentEditedApp"]; }
        set { ViewState["CurrentEditedApp"] = value; }
    }

    protected string WebId
    {
        get
        {
            string webId = Request.QueryString["WebId"];
            if (!string.IsNullOrEmpty(webId))
            {
                return HttpUtility.HtmlEncode(webId);
            }

            return null;
        }
    }

    private bool IsSubmitCollision
    {
        get
        {
            return !String.IsNullOrEmpty(Request.QueryString["SubmitCollision"]);
        }
    }

    #endregion

    private void LoadCriteria()
    {
        switch (this.ViewType)
        {
            case AppViewType.All:
                break;
            case AppViewType.MultiPort:
                this.appCriteria.FilterPort = ApplicationDALCriteria.FilterApplicationPort.MultiPort;
                break;
            case AppViewType.SinglePort:
                this.appCriteria.FilterPort = ApplicationDALCriteria.FilterApplicationPort.SinglePort;
                break;
            case AppViewType.TCP:
                this.appCriteria.FilterProtocol = ApplicationDALCriteria.FilterApplicationProtocol.OnlyTcp;
                break;
            case AppViewType.UDP:
                this.appCriteria.FilterProtocol = ApplicationDALCriteria.FilterApplicationProtocol.OnlyUdp;
                break;
            case AppViewType.Disabled:
                this.appCriteria.FilterEnabled = ApplicationDALCriteria.FilterApplicationEnabled.Disabled;
                break;
            case AppViewType.Enabled:
                this.appCriteria.FilterEnabled = ApplicationDALCriteria.FilterApplicationEnabled.Enabled;
                break;
        }

        if (this.SortField.Equals("Name", StringComparison.InvariantCultureIgnoreCase))
        {
            this.appCriteria.SortColumn = ApplicationDALCriteria.AppSortColumn.Name;
            this.appCriteria.SortType = this.SortDesc ? ApplicationDALCriteria.SortStyle.Descending : ApplicationDALCriteria.SortStyle.Ascending;
        }
        else if (this.SortField.Equals("Port", StringComparison.InvariantCultureIgnoreCase))
        {
            this.appCriteria.SortColumn = ApplicationDALCriteria.AppSortColumn.Port;
            this.appCriteria.SortType = this.SortDesc ? ApplicationDALCriteria.SortStyle.Descending : ApplicationDALCriteria.SortStyle.Ascending;
        }
        else if (this.SortField.Equals("Protocol", StringComparison.InvariantCultureIgnoreCase))
        {
            this.appCriteria.SortColumn = ApplicationDALCriteria.AppSortColumn.Protocol;
            this.appCriteria.SortType = this.SortDesc ? ApplicationDALCriteria.SortStyle.Descending : ApplicationDALCriteria.SortStyle.Ascending;
        }
        else if (this.SortField.Equals("GroupNameSrc", StringComparison.InvariantCultureIgnoreCase))
        {
            this.appCriteria.SortColumn = ApplicationDALCriteria.AppSortColumn.GroupNameSrc;
            this.appCriteria.SortType = this.SortDesc ? ApplicationDALCriteria.SortStyle.Descending : ApplicationDALCriteria.SortStyle.Ascending;
        }
        else if (this.SortField.Equals("GroupNameDst", StringComparison.InvariantCultureIgnoreCase))
        {
            this.appCriteria.SortColumn = ApplicationDALCriteria.AppSortColumn.GroupNameDst;
            this.appCriteria.SortType = this.SortDesc ? ApplicationDALCriteria.SortStyle.Descending : ApplicationDALCriteria.SortStyle.Ascending;
        }
        if (!string.IsNullOrEmpty(this.Request.QueryString["SearchString"]))
        {
            this.appCriteria.SearchString = this.Request.QueryString["SearchString"].Trim();
        }
    }

    private void InitSelectedSummary()
    {
        int total;
        List<Application> apps = GetApplications(new ApplicationDALCriteria(), 0, 0, out total);
        int spAppCount, mpAppCount, mpMonitoredPorts;

        ApplicationsHelper.GetAppCounts(apps, out spAppCount, out mpAppCount, out mpMonitoredPorts);

        this.lblSelectedSummary.Text = string.Format(mpAppCount == 1 ?
            NTAWebContent.NTAWEBCODE_VB0_73 : NTAWebContent.NTAWEBCODE_VB0_74,
            spAppCount,
            mpMonitoredPorts,
            mpAppCount
            );

        monitoredPorts = (spAppCount + mpMonitoredPorts).ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (string.IsNullOrEmpty(WebId))
        {
            InitializeSessionStorage();
            return;
        }

        appEditor.WebId = WebId;

        // add progress bars onclientclicks method:
        imgButtMonitorAll.OnClientClick = string.Format("show_progress_bar('{0}')", dlgMonitorAll.ClientID);
        imgButtUnMonitorAll.OnClientClick = string.Format("show_progress_bar('{0}')", dlgUnmonitorAll.ClientID);
        imgButtRestore.OnClientClick = string.Format("show_progress_bar('{0}')", dlgRestoreToDefault.ClientID);
        imgButtDelete.OnClientClick = string.Format("show_progress_bar('{0}')", dlgConfirmDelete.ClientID);

        this.Form.Attributes["autocomplete"] = "off";
        try
        {
            this.ViewType = (AppViewType)Enum.Parse(typeof(AppViewType), this.Request.QueryString["View"]);
        }
        catch
        {
            this.ViewType = AppViewType.All;
        }

        this.LoadCriteria();

        this.BindViewMenu();
        this.InitSelectedSummary();
        this.appGrid.Criteria = this.appCriteria;
        try
        {
            string pgNum = this.Request.QueryString["Page"];
            if (!string.IsNullOrEmpty(pgNum))
            {
                this.pagerBottom.PageNumber = Convert.ToInt32(pgNum);
            }
        }
        catch
        {
            // do nothing; default to 1
        }

        this.lnkAddApp.Style[HtmlTextWriterStyle.MarginRight] = "10px";

        string searchText = this.Request.QueryString.Get("SearchString");

        if (!string.IsNullOrEmpty(searchText))
        {
            this.txtSearch.Text = searchText;
        }
        else
        {
            // HACK for IE8,IE9 lack of placeholder attribute support
            this.txtSearch.CssClass += " x-form-empty-field";
        }

        if (!this.IsPostBack)
        {
            UpdateGrid();
        }
    }

    private void InitializeSessionStorage()
    {
        using (SessionStorage storage = new SessionStorage(this, null))
        {
            var col = new NameValueCollection(Request.QueryString);
            col["WebId"] = storage.WebId;
            DoRedirect(col);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        divUnsubmittedChanges.Visible = AreUnsubmittedChanges();
        pendingUpdatesWarning.Visible = divUnsubmittedChanges.Visible;

        if (IsSubmitCollision)
        {
            updPanelSubmitCollision.Update();
            dlgSubmitCollision.Show();
        }

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    /// <summary>
    /// 0, 50, 100 ...
    /// </summary>
    public int RecordIndex
    {
        get
        {
            return (this.pagerBottom.PageNumber - 1) * PageSize;
        }
    }

    public override void DataBind()
    {
        int totalRecords;
        List<Application> apps = GetApplications(appCriteria, PageSize, RecordIndex, out totalRecords);

        this.TotalRecords = totalRecords;

        this.pagerBottom.MaxPageNumber = (this.TotalRecords / this.PageSize) + 1;

        lblPortsText.Text = TotalRecords == 0 ?
            NTAWebContent.NTAWEBCODE_VB0_75 :
            string.Format(NTAWebContent.NTAWEBCODE_VB0_76,
                (PageNumber - 1) * PageSize + 1,
                Math.Min(PageNumber * PageSize, TotalRecords),
                TotalRecords);

        this.appGrid.DataSource = apps;

        base.DataBind();

    }

    private List<Application> GetApplications(ApplicationDALCriteria criteria, int pageSize, int recordIndex, out int totalApps)
    {
        Dictionary<int, Application> allApps;

        using (SessionStorage storage = new SessionStorage(this, WebId))
        {
            allApps = storage.Applications;
        }

        totalApps = ApplicationsHelper.FilterApplications(allApps, criteria, 0, 0).Count;

        return ApplicationsHelper.FilterApplications(allApps, criteria, pageSize, recordIndex);
    }

    #region Monitor All
    public void ShowMonitorAllDialog(object sender, EventArgs e)
    {
        this.updPanelMonitorAll.Update();
        this.dlgMonitorAll.Show();

        UpdateGrid();
    }

    public void MonitorAllCancelHandler(object sender, EventArgs e)
    {
        this.dlgMonitorAll.Hide();
    }

    public void MonitorAllConfirmHandler(object sender, EventArgs e)
    {
        this.dlgMonitorAll.Hide();
        this.ModalProgress.Hide();

        using (var storage = new SessionStorage(this, WebId))
        {
            storage.EnableAll(true);
            storage.CommitSessionStorage();
        }
        UpdateGrid();
    }

    #endregion

    #region Restore To Default
    public void ShowRestoreToDefaultDialog(object sender, EventArgs e)
    {
        this.updPanelRestore.Update();
        this.dlgRestoreToDefault.Show();

        UpdateGrid();
    }

    public void RestoreToDefaultCancelHandler(object sender, EventArgs e)
    {
        this.dlgRestoreToDefault.Hide();
    }

    public void RestoreToDefaultConfirmHandler(object sender, EventArgs e)
    {
        this.dlgRestoreToDefault.Hide();
        this.ModalProgress.Hide();

        using (var storage = new SessionStorage(this, WebId))
        {
            storage.MonitorRecommended();
            storage.CommitSessionStorage();
        }
        UpdateGrid();

    }

    #endregion

    #region Unmonitor All
    public void ShowUnmonitorAllDialog(object sender, EventArgs e)
    {
        this.updPanelUnmonitorAll.Update();
        this.dlgUnmonitorAll.Show();

        UpdateGrid();
    }

    public void UnmonitorAllCancelHandler(object sender, EventArgs e)
    {
        this.dlgUnmonitorAll.Hide();
    }

    public void UnmonitorAllConfirmHandler(object sender, EventArgs e)
    {
        this.dlgUnmonitorAll.Hide();
        this.ModalProgress.Hide();

        using (var storage = new SessionStorage(this, WebId))
        {
            storage.EnableAll(false);
            storage.CommitSessionStorage();
        }
        UpdateGrid();
    }

    #endregion

    public void SearchClickHandler(object sender, EventArgs e)
    {
        if (this.txtSearch.Text == null || this.txtSearch.Text.Equals(NTAWebContent.NTAWEBCODE_VB0_80, StringComparison.InvariantCultureIgnoreCase))
        {
            this.txtSearch.Text = "";
        }

        NameValueCollection col = new NameValueCollection(this.Request.QueryString);
        col["SearchString"] = HttpUtility.UrlEncode(this.txtSearch.Text);
        col["Page"] = "1";

        this.DoRedirect(col);
    }

    /// <summary>
    /// Returns true if there are any application changes not yet submitted to apply.
    /// The value is cached to not query BL each time.
    /// </summary>
    /// <returns></returns>
    protected bool AreUnsubmittedChanges()
    {
        using (var storage = new SessionStorage(this, WebId))
        {
            return storage.IsModified;
        }
    }

    private void RedirectToNetflowSettings()
    {
        Response.Redirect("~/Orion/TrafficAnalysis/Admin/NetFlowSettings.aspx");
    }

    protected void CancelApplicationChangesClickHandler(object sender, EventArgs e)
    {
        using (var storage = new SessionStorage(this, WebId))
        {
            storage.ClearSessionStorage();
        }

        RedirectToNetflowSettings();
    }

    #region View Menu
    protected void BindViewMenu()
    {
        this.rptViews.DataSource = Enum.GetValues(typeof(AppViewType));
        this.rptViews.DataBind();
    }

    protected string GetViewName(object viewType)
    {
        AppViewType type = (AppViewType)viewType;

        switch (type)
        {
            case AppViewType.All:
                return NTAWebContent.NTAWEBCODE_VB0_81;
            case AppViewType.MultiPort:
                return NTAWebContent.NTAWEBCODE_VB0_82;
            case AppViewType.SinglePort:
                return NTAWebContent.NTAWEBCODE_VB0_83;
            case AppViewType.TCP:
                return NTAWebContent.NTAWEBCODE_VB0_84;
            case AppViewType.UDP:
                return NTAWebContent.NTAWEBCODE_VB0_85;
            case AppViewType.Disabled:
                return NTAWebContent.NTAWEBCODE_VB0_86;
            case AppViewType.Enabled:
                return NTAWebContent.NTAWEBCODE_VB0_87;
        }

        return NTAWebContent.NTAWEBCODE_VB0_81;
    }

    protected string GetViewClass(object viewType)
    {
        if ((AppViewType)viewType == this.ViewType)
        {
            return "selected";
        }

        return string.Empty;
    }

    protected string GetViewHref(object viewType)
    {
        return string.Format("/Orion/TrafficAnalysis/Admin/ManageApps.aspx?View={0}&WebId={1}", viewType, WebId);
    }
    #endregion

    #region Application Editor Handlers

    protected void ShowAddApplicationDialog(object sender, EventArgs e)
    {
        this.appEditor.Clear();
        this.appEditor.EditMode = false;
        //this.dlgAddEdit.Width = 450;

        //update update panel
        updatePanelEdit.Update();

        this.dlgAddEdit.Show();
    }

    protected void AddEditApplicationCancel(object sender, EventArgs e)
    {
        this.dlgAddEdit.Hide();
        // When IP Group is created, js side doesn't affect it so we need full reload of page        
        this.Response.Redirect(this.Request.Url.ToString());
        //UpdateGrid();
    }

    /// <summary>
    /// Gets application from the dialog (AppEditor)
    /// </summary>
    /// <returns></returns>
    private Application GetApplication()
    {
        // get Data:
        string description = appEditor.Description;
        string port = appEditor.Ports;
        string protocol = appEditor.Protocol.ToString();
        string sourceIP = appEditor.SourceIPAddress;
        string destIP = appEditor.DestinationIPAddress;

        int src = 0;
        if (sourceIP != ANY_DESTINATION)
        {
            src = Convert.ToInt32(sourceIP);
        }

        int dest = 0;
        if (destIP != ANY_DESTINATION)
        {
            dest = Convert.ToInt32(destIP);
        }

        Log.DebugFormat("Description: " + description + ", Port: " + port + ", Protocol: " + protocol + ", Source ID for IP Group DAL: " + src.ToString() + ", Destination IP for IP Group DAL: " + dest.ToString());

        // prepare ports
        IList<int> portsCollection = BuildPortsFromRanges(port);
        Application app;

        // add new
        if (!appEditor.EditMode)
        {
            // Add new mode
            app = new Application();
        }
        else
        {
            // Edit mode
            string appIDstr = appEditor.ApplicationID;
            int appID = 0;
            if (!string.IsNullOrEmpty(appIDstr))
            {
                try { appID = Convert.ToInt32(appIDstr); }
                catch { }
            }

            app = new Application(appID);
        }

        app.Name = WebSecurityHelper.HtmlEncode(description);
        app.Protocols = NetFlowCommon.ResolveProtocols(appEditor.Protocol);

        foreach (int p in portsCollection)
        {
            app.Ports.Add(new ApplicationPort(p, ApplicationProperties.ConditionDirection.Both));
        }

        if (src != 0)
        {
            app.IPGroups.Add(new ApplicationGroup(src, ApplicationProperties.ConditionDirection.Source));
        }
        if (dest != 0)
        {
            app.IPGroups.Add(new ApplicationGroup(dest, ApplicationProperties.ConditionDirection.Destination));
        }

        bool enabled;
        Boolean.TryParse(appEditor.ApplicationEnabled, out enabled);
        app.Enabled = enabled;

        return app;
    }

    protected void EditorValidateHandler(object sender, EventArgs e)
    {
        Application app = this.GetApplication();
        Log.DebugFormat("Trying to add application {0} with {1} ports", app.Name, app.Ports.Count);

        appEditor.ApplicationCollision = "";

        try
        {
            using (var storage = new SessionStorage(this, WebId))
            {
                storage.UpdateApplication(app);
                storage.CommitSessionStorage();
            }
        }
        catch (IpGroupCollisionException ex)
        {
            Log.WarnFormat("Updating of application failed, there was an collision: {0}", ex.Message);
            appEditor.IpGroupCollision = ex.Message;
        }
        catch (ApplicationCollisionException ex)
        {
            // show collision dialog
            Log.WarnFormat("Updating of application failed, there was an collision: {0}", ex.Message);
            appEditor.ApplicationCollision = ex.Message;

        }
        catch (Exception ex)
        {
            Log.ErrorFormat("Updating of application failed with exception: {0}", ex.ToString());
        }
    }

    protected void EditorResolveHandler(object sender, EventArgs e)
    {
        Application app = GetApplication();

        appEditor.ApplicationCollision = "";
        try
        {
            using (var storage = new SessionStorage(this, WebId))
            {
                storage.UpdateApplication(app);
                storage.CommitSessionStorage();
            }
        }
        catch (ApplicationCollisionException ex)
        {
            using (var storage = new SessionStorage(this, WebId))
            {
                storage.ResolveCollision(ex);
                storage.UpdateApplication(app);
                storage.CommitSessionStorage();
            }
            // When IP Group is created, js side doesn't affect it so we need full reload of page
            this.Response.Redirect(this.Request.Url.ToString());
            //UpdateGrid();
        }
    }

    protected void EditorSubmitHandler(object sender, EventArgs e)
    {
        this.dlgAddEdit.Hide();
        // When IP Group is created, js side doesn't affect it so we need full reload of page
        this.Response.Redirect(this.Request.Url.ToString());
        //UpdateGrid(); 
    }

    private void UpdateGrid()
    {
        this.DataBind();
        this.upMain.Update();
    }

    #region Helpers

    private IList<int> BuildPortsFromRanges(string ranges)
    {
        string[] parts = ranges.Split(',');
        IList<int> ports = new List<int>();
        int rangeInt;
        foreach (string range in parts)
        {
            if (range.Contains("-"))
            {
                string[] portRange = range.Split('-');
                int start = Convert.ToInt32(portRange[0]);
                int end = Convert.ToInt32(portRange[1]);
                for (int i = start; i <= end; i++)
                {
                    if (ports.Contains(i) == false)
                    {
                        ports.Add(i);
                    }
                }
            }
            else
            {
                rangeInt = Convert.ToInt32(range);
                if (ports.Contains(rangeInt) == false)
                {
                    ports.Add(rangeInt);
                }
            }
        }

        return ports;
    }

    private int GetAppID(CommandEventArgs e)
    {
        string appStr = e.CommandArgument.ToString();

        int appID = -1;

        try { appID = Convert.ToInt32(appStr); }
        catch { }

        return appID;
    }

    #endregion Helpers

    #endregion

    #region Data Grid Handlers
    protected void DataGridSortHandler(object source, DataGridSortCommandEventArgs e)
    {
        NameValueCollection newQueryString = new NameValueCollection(this.Request.QueryString);
        if (this.SortField.Equals(e.SortExpression))
        {
            // change sort order, keep same sort field
            if (this.SortDesc)
                newQueryString["SortDir"] = "ASC";
            else
                newQueryString["SortDir"] = "DESC";
        }
        else
        {
            // sort ascending, change sort field
            newQueryString["SortDir"] = "ASC";
            newQueryString["SortField"] = e.SortExpression;
        }

        newQueryString["SortField"] = e.SortExpression;
        this.DoRedirect(newQueryString);
    }

    protected void DataGridEditHandler(object source, DataGridCommandEventArgs e)
    {
        string appNum = e.CommandArgument.ToString();
        int appId = Convert.ToInt32(appNum.Split('.')[0]);

        Application app;
        using (var storage = new SessionStorage(this, WebId))
        {
            app = storage.GetApplication(appId);
        }

        this.appEditor.ApplicationID = appId.ToString();
        this.appEditor.EditMode = true;
        this.appEditor.Description = app.Name;
        this.appEditor.Ports = NetFlowCommon.GetPortRangesString(app.Ports);
        this.appEditor.Protocol = NetFlowCommon.GetProtocolNumber(app.Protocols);

        this.appEditor.ApplicationEnabled = app.Enabled.ToString();

        // first IP group
        if (app.IPGroups.Count > 0)
        {
            if (app.IPGroups[0].Direction == ApplicationProperties.ConditionDirection.Source)
            {
                this.appEditor.SourceIPAddress = app.IPGroups[0].GroupID.ToString();
            }
            else if (app.IPGroups[0].Direction == ApplicationProperties.ConditionDirection.Destination)
            {
                this.appEditor.DestinationIPAddress = app.IPGroups[0].GroupID.ToString();
            }
            // else throw exception?
        }

        if (app.IPGroups.Count > 1)
        {
            if (app.IPGroups[1].Direction == ApplicationProperties.ConditionDirection.Source)
            {
                this.appEditor.SourceIPAddress = app.IPGroups[1].GroupID.ToString();
            }
            else if (app.IPGroups[1].Direction == ApplicationProperties.ConditionDirection.Destination)
            {
                this.appEditor.DestinationIPAddress = app.IPGroups[1].GroupID.ToString();
            }
            // else throw exception?
        }

        this.dlgAddEdit.Show();

        // update content of ajax modal popup extender
        this.updatePanelEdit.Update();
    }

    protected void DataGridDeleteHandler(object source, DataGridCommandEventArgs e)
    {
        DeleteApp(e, false);

        UpdateGrid();
    }

    protected void DataGridDeleteRangeHandler(object source, CommandEventArgs e)
    {
        DeleteApp(e, true);

        UpdateGrid();
    }

    private void DeleteApp(CommandEventArgs e, bool isRange)
    {
        string[] strArray = e.CommandArgument.ToString().Split(new string[] { "~~" }, StringSplitOptions.None);

        string appNameStr = strArray[0];
        int appId;

        if (Int32.TryParse(strArray[0], out appId))
        {
            Application app;
            using (var storage = new SessionStorage(this, WebId))
            {
                app = storage.GetApplication(appId);
            }
            appNameStr = app.Name;
        }

        //this.CurrentEditedApp = appID;
        if (isRange == true)
        {
            lblDeleteHeader.Text = NTAWebContent.NTAWEBCODE_VB0_88;
            lblDeleteConfirm.Text = string.Format(NTAWebContent.NTAWEBCODE_VB0_89, strArray[1], appNameStr);
        }
        else
        {
            lblDeleteHeader.Text = NTAWebContent.NTAWEBCODE_VB0_90;
            lblDeleteConfirm.Text = string.Format(NTAWebContent.NTAWEBCODE_VB0_91, appNameStr);
        }

        // update label with text
        updatePanelDeleteConfirm.Update();

        // store appID to the dialog
        this.CurrentEditedApp = e.CommandArgument.ToString();

        // show ajax modal dialog 
        this.dlgConfirmDelete.Show();
    }

    private void AppEnableDisable(CommandEventArgs e, bool pEnable)
    {
        using (var storage = new SessionStorage(this, WebId))
        {
            storage.Enable(GetAppID(e), pEnable);
            storage.CommitSessionStorage();
        }
        
        UpdateGrid();
    }

    protected void DataGridEnableHandler(object source, CommandEventArgs e)
    {
        AppEnableDisable(e, true);
    }

    protected void DataGridDisableHandler(object source, CommandEventArgs e)
    {
        AppEnableDisable(e, false);
    }

    #endregion

    #region Delete Confirmation Dialog Handlers
    protected void DeleteConfirmCancelHandler(object source, EventArgs e)
    {
        this.lblDeleteConfirm.Text = string.Empty;
        //this.CurrentEditedApp = "0";
        this.dlgConfirmDelete.Hide();
        // we don't need refresh page when using ajax popup modal extender

    }

    protected void DeleteConfirmHandler(object source, EventArgs e)
    {
        // hide modal window
        this.dlgConfirmDelete.Hide();
        this.ModalProgress.Hide();

        string[] strArray = this.CurrentEditedApp.Split(new string[] { "~~" }, StringSplitOptions.None);

        int appId;
        if (!Int32.TryParse(strArray[0], out appId))
        {
            appId = Convert.ToInt32(strArray[0]);
        }

        if (strArray.Length > 1)
        {
            // we have multiport
            IList<int> ports = this.BuildPortsFromRanges(strArray[1]);
            Application app;
            using (var storage = new SessionStorage(this, WebId))
            {
                app = storage.GetApplication(appId);
            }

            List<ApplicationPort> tempList = new List<ApplicationPort>();
            foreach (int port in ports)
            {
                foreach (ApplicationPort appPort in app.Ports)
                {
                    if (appPort.Port == port)
                    {
                        tempList.Add(appPort);
                    }
                }
            }

            foreach (ApplicationPort appPort in tempList)
            {
                app.Ports.Remove(appPort);
            }

            try
            {
                using (var storage = new SessionStorage(this, WebId))
                {
                    storage.UpdateApplication(app);
                    storage.CommitSessionStorage();
                }
            }
            catch (Exception ex)
            {
                Log.Debug(String.Format("Exception while removing multi-port application id: {0}", appId), ex);
            }
        }
        else
        {
            using (var storage = new SessionStorage(this, WebId))
            {
                storage.DeleteApplication(appId);
                storage.CommitSessionStorage();
            }
        }

        UpdateGrid();
    }
    #endregion

    protected void PageChangeHandler(object source, EventArgs e)
    {
        NameValueCollection newQueryString = new NameValueCollection(this.Request.QueryString);
        newQueryString["Page"] = this.pagerBottom.PageNumber.ToString();
        this.DoRedirect(newQueryString);
    }

    private void DoRedirect(NameValueCollection newQueryString)
    {
        string newQuery = string.Join("&", newQueryString.Cast<string>().Select(key => string.Concat(key, "=", newQueryString[key])));
        Response.Redirect(this.Request.Url.GetLeftPart(UriPartial.Path) + "?" + newQuery);
    }

    protected void SubmitCollisionOkHandler(object sender, EventArgs e)
    {
        using (var storage = new SessionStorage(this, WebId))
        {
            storage.ClearSessionStorage();
        }

        DoRedirect(new NameValueCollection());
    }
}
