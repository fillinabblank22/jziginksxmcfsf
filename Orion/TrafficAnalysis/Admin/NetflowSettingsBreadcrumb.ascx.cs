﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Netflow.Web;

public partial class Orion_TrafficAnalysis_Admin_NetflowSettingsBreadcrumb : System.Web.UI.UserControl
{

    protected override void OnInit(EventArgs e)
    {
        this.lnkSecond.NavigateUrl = UIConsts.URL_NETFLOW_ADMIN_PAGE;
        base.OnInit(e);

        ViewState["pathToRedirect"] = Request.QueryString.ToString() == string.Empty ? UIConsts.URL_NETFLOW_ADMIN_PAGE : Request.QueryString["ViewPath"];

        object obj = ViewState["pathToRedirect"];

        if (obj == null)
        {
            return;
        }

        string str = obj.ToString();

        if (str.Contains("SummaryView.aspx") == true)
        {
            this.lnkFirst.Text = Resources.NTAWebContent.NTAWEBCODE_AK0_11;
            this.lnkFirst.NavigateUrl = "/Orion/TrafficAnalysis/SummaryView.aspx";
            this.lnkFirst.ToolTip = lnkFirst.Text;
            this.lnkSecond.Visible = false;
            this.lblSecond.Visible = false;
        }

        
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (HideSecond == true)
        {
            this.lnkSecond.Visible = false;
            this.lblSecond.Visible = false;
        }
    }

    public bool HideSecond { get; set; }

}
