<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppPager.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_AppPager" %>

<table class="app-pager">
    <tr>
        <td class="pager-button">
            <asp:ImageButton runat="server" 
                 ID="btnMoveFirst" 
                 AlternateText="<%$  Resources:NTAWebContent,NTAWEBDATA_VB0_85%>" 
                 ImageUrl="~/Orion/TrafficAnalysis/images/Button.First.gif"
                 OnClick="MoveFirstClickHandler"
                 CssClass="PagerButton"
                 />
        </td>
        <td class="pager-button">
            <asp:ImageButton runat="server" 
                 ID="btnMovePrev" 
                 AlternateText="<%$  Resources:NTAWebContent,NTAWEBDATA_VB0_86%>"  
                 ImageUrl="~/Orion/TrafficAnalysis/images/Button.Previous.gif" 
                 OnClick="MovePrevClickHandler"
                 CssClass="PagerButton"
                 />    
        </td>        
        <td>
            <span class="vert-separator"></span>
        </td>
        <td class="pager-text">
            <asp:Literal runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AppPager_Page%>" />
        </td>
        <td>
            <asp:TextBox runat="server" 
                        ID="txtPageNum" 
                        Text="1" 
                        Columns="2"
                        MaxLength="4"
                        OnTextChanged="TextChangeHandler"
                        AutoPostBack="true"/>
        </td>
        <td class="pager-text">
            <asp:Literal runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AppPager_OfPage%>" />&nbsp;
            <asp:Label runat="server" ID="totalPages" Text="1"/>
        </td>
        <td>
            <span class="vert-separator"></span>
        </td>
        <td class="pager-button">
            <asp:ImageButton runat="server" 
                 ID="btnMoveNext" 
                 AlternateText="<%$  Resources:NTAWebContent,NTAWEBDATA_VB0_87%>" 
                 ImageUrl="~/Orion/TrafficAnalysis/images/Button.Next.gif" 
                 OnClick="MoveNextClickHandler"
                 CssClass="PagerButton"
                 />
        </td>
        <td class="pager-button">
            <asp:ImageButton runat="server" 
                 ID="btnMoveLast" 
                 AlternateText="<%$  Resources:NTAWebContent,NTAWEBDATA_VB0_88%>" 
                 ImageUrl="~/Orion/TrafficAnalysis/images/Button.Last.gif" 
                 OnClick="MoveLastClickHandler"
                 CssClass="PagerButton"
                 />            
        </td>
    </tr>
</table>