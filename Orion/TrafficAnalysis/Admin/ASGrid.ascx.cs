using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Web.DALInterfaces;

public partial class Orion_TrafficAnalysis_Admin_ASGrid : UserControl
{
    public event DataGridSortCommandEventHandler SortCommand;
    public event DataGridCommandEventHandler EditCommand;
    public event DataGridCommandEventHandler DeleteCommand;
    public event CommandEventHandler DeleteRangeCommand;

    public event DataGridCommandEventHandler DisableCommand;
    public event DataGridCommandEventHandler EnableCommand;

    //private ApplicationsDAL _appDAL = new ApplicationsDAL();

    public AutonomousSystemsDALCriteria Criteria { get; set; }    // new one


    public object DataSource
    {
        get
        {
            return this.dgAS.DataSource;
        }
        set
        {
            DataTable dt = value as DataTable;
            this.dgAS.DataSource = dt;
        }
    }

   

    protected override void OnInit(EventArgs e)
    {
        this.Page.Header.Controls.Add(this.ParseControl(GRID_CSS));
        
        base.OnInit(e);
    }

    #region Sort Indicator Display Routines
    protected string GetIDSortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.ID);
    }
    protected string GetNameSortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.Name);
    }
    protected string GetCountrySortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.Country);
    }
    protected string GetOrganizationSortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.Organization);
    }
    protected string GetRegistrationDateSortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.RegistrationDate);
    }
    protected string GetLastUpdatedDateSortImg()
    {
        return this.GetSortIcon(AutonomousSystemSortColumn.LastUpdated);
    }


    private string GetSortIcon(AutonomousSystemSortColumn which)
    {
        string sortDir = this.Request.QueryString["SortDir"];
        string sortField = this.Request.QueryString["SortField"];

        if (false == Enum.GetNames(typeof(AutonomousSystemSortColumn)).Contains<string>(sortField))
        {
            return string.Empty;
        }

        if (which != (AutonomousSystemSortColumn)Enum.Parse(typeof(AutonomousSystemSortColumn), sortField))
        {
            return string.Empty;
        }


        AutonomousSystemSortStyle style = AutonomousSystemSortStyle.None;
        if (sortDir == "DESC")
        {
            style = AutonomousSystemSortStyle.Descending;
        }
        else if (sortDir == "ASC")
        {
            style = AutonomousSystemSortStyle.Ascending;
        }

        switch (style)
        {
            case AutonomousSystemSortStyle.None:
                return string.Empty;
            case AutonomousSystemSortStyle.Ascending:
                return String.Format("<img src='/Orion/TrafficAnalysis/images/Icon.SortAsc.gif' alt='{0}' />", Resources.NTAWebContent.NTAWEBCODE_VB0_61);
            case AutonomousSystemSortStyle.Descending:
                return String.Format("<img src='/Orion/TrafficAnalysis/images/Icon.SortDesc.gif' alt='{0}' />",Resources.NTAWebContent.NTAWEBCODE_VB0_62);
            default:
                return string.Empty;
        }
    } 
    #endregion

    protected string GetProtocolName(object objTCP, object objUDP)
    {
        bool tcp = Convert.ToBoolean(objTCP);
        bool udp = Convert.ToBoolean(objUDP);

        if (tcp && udp)
            return "All";

        if (tcp)
            return "TCP";
        if (udp)
            return "UDP";

        throw new InvalidOperationException("At least one of TCP and UDP must be true.");//TODO:[localization] {exception message temporary ignored}
    }

    protected void DataGridOnItemCommandHandler(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Disable"))
        {
            DataGridDisableHandler(source, e);
        }
        else if (e.CommandName.Equals("Enable"))
        {
            DataGridEnableHandler(source, e);
        }
    }

    protected void DataGridSortHandler(object source, DataGridSortCommandEventArgs e)
    {
        if (null != this.SortCommand)
            this.SortCommand(source, e);
    }

    protected void DataGridEditHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.EditCommand)
            this.EditCommand(source, e);
    }

    protected void DataGridDeleteHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.DeleteCommand)
            this.DeleteCommand(source, e);
    }

    protected void DeleteRangeHandler(object sender, ImageClickEventArgs e)
    {
        ImageButton button = sender as ImageButton;

        CommandEventArgs args = new CommandEventArgs(button.CommandName, button.CommandArgument);
        if (null != this.DeleteRangeCommand)
            this.DeleteRangeCommand(sender, args);
    }

    protected void DataGridDisableHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.DisableCommand)
            this.DisableCommand(source, e);
    }

    protected void DataGridEnableHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.EnableCommand)
            this.EnableCommand(source, e);
    }

    protected string GetDeleteRangeArgument(object appID, object appRange)
    {
        return string.Format("{0}~~{1}", appID, appRange);
    }


    protected void OnRowDataBound(object sender, DataGridItemEventArgs e)
    {
        
    }

    private void DoScriptRegister(string key, string script)
    {
        if (null == ScriptManager.GetCurrent(this.Page))
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), key, script, true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), key, script, true);
        }
    }

    protected string GetDescriptionSpanClass(object isMulti, object portRange)
    {
        if (portRange is string && !string.IsNullOrEmpty((string)portRange))
        {
            return "ChildRow"; 
        }
      
        if (isMulti != null && Boolean.Parse(isMulti.ToString()) == true)
        {
            return "ParentRow";
        }
        
        return "NormalRow";
 
    }

    protected string GetCollapseButton(bool isMultiPort, int appID, object portRange)
    {
        if (isMultiPort == true && portRange is string && ((string)portRange) == string.Empty)
        {
            return string.Format("<img src='/Orion/images/Button.Expand.gif' class='CollapseButton' alt='' id='expandButton_{0}' style='cursor: pointer;' onclick='Collapse_Click(this); return false;' />", appID);
        }
        
        return string.Empty;
    }

    protected string GetOkIcon(string enabled)
    {
        if (Convert.ToBoolean((enabled.ToLower())))
            return string.Format("<img src='/Orion/TrafficAnalysis/images/icon_OK.gif' alt=''></img>");

        return string.Empty;
    }
    
    private const string GRID_CSS = @"
<style type='text/css'>
    .DataGrid
    {
        font-size: 11px;
        border-collapse: separate;
        margin: 0 0 0 0;
        padding: 0 0 0 0;
        clear: right;
    }
    
    .DataGridHeader
    {
        margin-top: 0px;
    }
    
    .DataGridHeader td
    {
        margin-top: 0px;
        border-right: 1px solid white;
    }
    
    .DataGridHeader td.RightHeader
    {
        border-right: 0px solid white;
    }

    .DataGrid td img.CollapseButton
    {
        float: left;
        margin-top: 2px;
        vertical-align: middle;
    }

    .DataGrid td .ParentRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        padding-left: 4px;
    }

    .DataGrid td .ChildRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        padding-left: 30px;
    }

    .DataGrid td .NormalRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        margin-left: 15px;
        display:block
    }
</style>
";
}
