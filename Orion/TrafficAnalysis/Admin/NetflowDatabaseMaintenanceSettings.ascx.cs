using Resources;
using SolarWinds.Logging;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.InformationService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common.FlowStorage;
using SolarWinds.Orion.Common;

public partial class Orion_TrafficAnalysis_Admin_DatabaseMaintenanceSettings : System.Web.UI.UserControl, INetFlowSettings
{
    private static readonly Log log = new Log();

    private const string FlowsPerSecondForLast5Minutes = "FlowsPerSecondForLast5Minutes";

    private const string FlowsPerSecondForLast24Hours = "FlowsPerSecondForLast24Hours";

    private const string FlowsPerSecondForLast3Days = "FlowsPerSecondForLast3Days";

    private const string DemoFlowStorageDatabaseName = "Demo/FlowStorageDB";
    

    private INetFlowSettingsHost _host;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        this.DataBind();

        this.SetUI();
    }

    public string DatePickerRegionalSettings
    {
        get
        {
            return new NetFlowCommon().GetDatePickerRegionalSettings();
        }
    }


    public bool DBMaintenanceEnabled
    {
        get
        {
            return GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceEnabled;
        }
    }

    public string ExecutionTime
    {
        get
        {
            return DateTimeValidations.ConvertTimeInvariantToCulture(CultureInfo.CurrentCulture, GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceExecutionTime);
        }
    }

    public string CompressedPeriod
    {
        get { return GlobalSettingsDALCached.RetainCompressedDataInDays.ToString(); }
    }

    public int FlowCorrelationCleanUpPeriod
    {
        get
        {
            return GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceFlowCorrelationCleanUpPeriod;
        }
    }

    public int DBMaintenancePeriod
    {
        get
        {
            return GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenancePeriod;
        }
    }

    void INetFlowSettings.SetUI()
    {
        this.SetUI();
    }

    void SetUI()
    {
        chbEnableDatabaseMaintenance.Checked = DBMaintenanceEnabled;

        // 4.
        tbCompPeriod.Text = CompressedPeriod;

        // 5.
        ddlFlowCorrelationCleanUpFrequency.SelectedIndex = FlowCorrelationCleanUpPeriod;

        // 6.
        ddlDatabaseMaintenanceFrequency.SelectedIndex = DBMaintenancePeriod;

        if (OrionConfiguration.IsDemoServer)
        {
            FlowStorageLocation.Text = DemoFlowStorageDatabaseName;
        }
        else
        {
            FlowStorageLocation.Text = GetFlowStorageLocation();
        }

        FlowsPerSecondForLast5MinutesL.Text = FlowsPerSecondForSomePeriod(DateTime.Now.AddMinutes(-5), FlowsPerSecondForLast5Minutes);

        FlowsPerSecondForLast24HoursL.Text = FlowsPerSecondForSomePeriod(DateTime.Now.AddHours(-24), FlowsPerSecondForLast24Hours);

        FlowsPerSecondForLast3DaysL.Text = FlowsPerSecondForSomePeriod(DateTime.Now.AddDays(-3), FlowsPerSecondForLast3Days);
    }

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        this._host = host;
    }


    void INetFlowSettings.StoreSettings()
    {
        GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceEnabled = this.chbEnableDatabaseMaintenance.Checked;
        if (this.chbEnableDatabaseMaintenance.Checked)
        {
            int MaxHistory = 3650;
            int MinHistory = 1;
            int cperiod = Convert.ToInt32(tbCompPeriod.Text);

            if ((cperiod >= MinHistory) && (cperiod <= MaxHistory))
            {
                GlobalSettingsDALCached.RetainCompressedDataInDays = cperiod;
            }

            // Convert to invariant culture
            GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceExecutionTime = DateTimeValidations.ConvertTimeCultureToInvariant(CultureInfo.CurrentCulture, txtboxExecutionTime.Text);

            GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenanceFlowCorrelationCleanUpPeriod = int.Parse(ddlFlowCorrelationCleanUpFrequency.SelectedValue);

            GlobalSettingsDALCached.InstanceInterface.DatabaseMaintenancePeriod = int.Parse(ddlDatabaseMaintenanceFrequency.SelectedValue);
        }
    }

    public string Icon
    {
        get { return "/Orion/TrafficAnalysis/images/NTASettings/database-settings.svg"; }
    }

    public string Caption
    {
        get { return NTAWebContent.NTAWEBCODE_AK0_58; }
    }

    public string Redirect
    {
        get { return "Database"; }
    }

    public string Help
    {
        get { return "OrionNetFlowPHSettingsDatabaseMaintenance"; }
    }

    internal string TriggerSaveScript
    {
        get
        {
            return Page.ClientScript.GetPostBackEventReference(_host.SaveButton, "");
        }
    }

    private string GetFlowStorageLocation()
    {
        try
        {
            var result = BusinessLayerHelper.GetFlowsDatabaseInfo();

            return string.Format(NTAWebContent.NTAWEBDATA_FlowStorageDatabaseLocation,
                                 result.ServerName,
                                 result.DatabaseName);
        }
        catch (Exception e)
        {
            log.WarnFormat("Exception was throw while connecting to service. Exception: {0}", e);
            return NTAWebContent.NTAWEBDATA_FlowStorage_LocationNotRetrieved;
        }
    }

    private string FlowsPerSecondForSomePeriod(DateTime timePeriod, string flowsForLastPeriod)
    {
        string query = @"SELECT SUM(StatisticsValue) AS StatisticsValue FROM Orion.Netflow.NetFlowEnginesStatistics NFEStat
        INNER JOIN Orion.Engines Eng ON NFEStat.EngineID = Eng.EngineID
        WHERE Eng.KeepAlive = @TimePeriod OR Eng.KeepAlive > @TimePeriod AND NFEStat.StatisticsName = @FlowsForLastPeriod";

        string flowsValue = String.Empty;

        using (var swis = InformationServiceProxy.CreateV3())
        {
            DataTable value = swis.Query(query, new Dictionary<string, object> { { "TimePeriod", timePeriod }, { "FlowsForLastPeriod", flowsForLastPeriod } });

            if (value != null)
                flowsValue = System.Convert.ToString(value.Rows[0].ItemArray[0]);
        }

        return !String.IsNullOrEmpty(flowsValue) ? flowsValue : "0";
    }

    /// <summary>
    /// Validates the input time value is valid in current user's culture.
    /// </summary>
    protected void TimeCultureValidate(object source, ServerValidateEventArgs args)
    {
        string any;
        args.IsValid = args.Value != null
            && args.Value.Length != 0
            && DateTimeValidations.TryConvertTimeCultureToInvariant(CultureInfo.CurrentCulture, args.Value, out any);
    }

    /// <summary>
    /// Renders all given culture supported time formats as examples.
    /// </summary>
    /// <param name="c">Culture whose formats to render.</param>
    private static string RenderTimeFormatExamples(CultureInfo c)
    {
        // Get all short time string patterns
        // Note: We assume at least one always exists
        string[] fmts = c.DateTimeFormat.GetAllDateTimePatterns('t');

        DateTime example = new DateTime(1, 1, 1, 1, 30, 0, DateTimeKind.Local);

        // In some cultures the resulting example values can differ by trailing whitespace only
        // so these almost-duplicates need to be removed.
        var examples = fmts.Select(fmt => string.Concat("'", example.ToString(fmt), "'")).Distinct().OrderBy(e => e);

        return string.Join(", ", examples);
    }

    /// <summary>
    /// Gets error message for invalid time value with possible formats shown.
    /// </summary>
    protected static string TimeErrorFormats
    {
        get
        {
            return string.Format(NTAWebContent.NTAWEBDATA_AK0_137, RenderTimeFormatExamples(CultureInfo.InvariantCulture));
        }
    }

    /// <summary>
    /// Gets example time formats.
    /// </summary>
    protected static string TimeExampleFormats
    {
        get
        {
            return string.Format(NTAWebContent.NTAWEBDATA_AK0_121, RenderTimeFormatExamples(CultureInfo.InvariantCulture));
        }
    }

    protected static long NetFlowDemoStorageInfo_OccupiedDiskSpace
    {
        get
        {
            long disk;
            return long.TryParse(ConfigurationManager.AppSettings["NetFlowDemoStorageInfo_OccupiedDiskSpace"], out disk) ? disk : 0;
        }
    }

    protected static int NetFlowDemoStorageInfo_CountOfdays
    {
        get
        {
            int days;
            return int.TryParse(ConfigurationManager.AppSettings["NetFlowDemoStorageInfo_CountOfdays"], out days) ? days: 0;
        }
    }
}
