<%@ Page Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master"
    CodeFile="TransportProtocolsEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_TransportProtocolsEdit"
    Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_62%>" MaintainScrollPositionOnPostback="true" %>
    <%@ Register TagPrefix="nta" TagName="NtaHelpButton" Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx"%>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
            <nta:NtaHelpButton ID="HelpButton5" runat="server" HelpUrlFragment="OrionNetFlowPHSettingsMonitoredProtocols" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_63%>" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    
    <div class="PageHeader" id="etxxHeader" style="width: auto !important;">
        <%=Page.Title%>
    </div>
    <br />
    <div class="Header2"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_64%></div>
    
    <div class="mnsLeft">
            <%= Resources.NTAWebContent.NTAWEBDATA_VB0_65%>
    </div>
    <br />
    <div class="ResourceWrapper" id="etxxRW" style="width: 600px;">
        <div class="mnsLeft" style="height: 27px"><b>
            <asp:Label ID="checkedProtocols" runat="server" Text="0" />
            <%= Resources.NTAWebContent.NTAWEBDATA_VB0_66%> </b>
        </div>    
        <div class="etxxLH">
            
            <asp:DataList ID="TransportProtocolsList" runat="server" DataKeyField="ProtocolID">
                <HeaderTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" id="edpTable">
                        <thead>
                            <tr>
                                <th width="5%" style="border-right:none;">
                                    &nbsp;
                                </th>
                                <th style="text-align: left; border-left: none; border-right: none;">
                                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_67%>
                                </th>
                                <th width="65%" style="text-align: left; border-left:none;">
                                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_68%>
                                </th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="5%" class="etptd1">
                            <asp:CheckBox ToolTip='<%#: Eval("ProtocolName")%>' runat="server" OnCheckedChanged="CheckProtocol" AutoPostBack="True"
                                Checked='<%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem , "Enabled").ToString())%>' />
                        </td>
                        <td align="left" width="30%" class="etptd2">
                            <%#: Eval("ProtocolName")%>
                        </td>
                        <td align="left" width="65%" class="etptd3">
                            <%#: Eval("ProtocolDesc")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="GridRowA">
                        <td width="5%" class="etptd1">
                            <asp:CheckBox ToolTip='<%#: Eval("ProtocolName")%>' runat="server" OnCheckedChanged="CheckProtocol" AutoPostBack="True"
                                Checked='<%#Convert.ToBoolean(DataBinder.Eval(Container.DataItem , "Enabled").ToString())%>' />
                        </td>
                        <td align="left" width="30%" class="etptd2">
                            <%#: Eval("ProtocolName")%>
                        </td>
                        <td align="left" width="65%" class="etptd3">
                            <%#: Eval("ProtocolDesc")%>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:DataList>
        </div> <%--etxxLH DIV END--%>
    </div> <%--Resource wrapper DIV END--%>
    
    <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" DisplayType="Primary" OnClick="btnSubmit_Click" style="margin-left:7px;" />
        
</asp:Content>
