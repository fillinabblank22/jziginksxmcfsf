<%@ Page Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" AutoEventWireup="true" CodeFile="ManageApps.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_ManageApps" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_107%>" %>

<%@ Register TagPrefix="nta" TagName="AppPager" Src="~/Orion/TrafficAnalysis/Admin/AppPager.ascx" %>
<%@ Register TagPrefix="nta" TagName="AppGrid" Src="~/Orion/TrafficAnalysis/Admin/AppGrid.ascx" %>
<%@ Register TagPrefix="nta" TagName="AppEditor" Src="~/Orion/TrafficAnalysis/Utils/AppEditor.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Import Namespace="SolarWinds.Orion.Common" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<%@ Register TagPrefix="nta" TagName="NtaHelpButton" Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="NtaHelpButton1" runat="server" HelpUrlFragment="OrionNetFlowPHSettingsApplicationServicePorts" Title="<%$Resources:NTAWebContent,NTAWEBCODE_VB0_97%>" />
</asp:Content>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <orion:Include runat="server" Framework="Ext" FrameworkVersion="3.4" />
    <orion:Include Module="TrafficAnalysis" runat="server" File="NetFlow.css" />
    <style type="text/css">
        .pendingUpdate {
            font-weight: normal;
            white-space: normal;
            display: block;
            margin-top: 10px;
        }

        .innerStatusMessage {
            font-size: 11px;
            width: 600px;
        }

        .DataGrid td img {
            vertical-align: middle;
        }

        .x-form-empty-field {
            font-style: italic !important;
            color: #808080
        }
    </style>

    <script type="text/javascript">
        function show_progress_bar(modal_window_to_close) {
            // hide modal window
            $find(modal_window_to_close).hide();
            // show progress bar modal window
            var ModalProgress = "<%= ModalProgress.ClientID %>";
            $find(ModalProgress).show();
        }

        var hideBottomWarningIfNeeded = function () {
            var elementsInViewport = function (topEl, bottomEl) {
                var elementTopPosition = function (el) {
                    var top = el.offsetTop;

                    while (el.offsetParent) {
                        el = el.offsetParent;
                        top += el.offsetTop;
                    }

                    return top;
                }

                var topPosition = elementTopPosition(topEl);
                var bottomPosition = elementTopPosition(bottomEl);

                return (bottomPosition - topPosition) <= window.innerHeight;
            }

            var topWarningEl = document.getElementById('<%= divUnsubmittedChanges.ClientID %>');
            var bottomWarningEl = document.getElementById('<%= pendingUpdatesWarning.ClientID %>');
            var bottomWarning = $('#<%= pendingUpdatesWarning.ClientID %>');

            //we need to show bottom warning, otherwise element position will be always 0
            bottomWarning.show();

            if (topWarningEl && bottomWarningEl && elementsInViewport(topWarningEl, bottomWarningEl))
                bottomWarning.hide();
        }

        //This method is fired when window has been resized 
        window.addEventListener('resize', function () {
            hideBottomWarningIfNeeded();
        });

        //This method is fired on page load and all subsequent partial page updates
        function pageLoad() {
            $("[id$='txtSearch']").keyup(function (e) {
                if (e.keyCode === 13) {
                    __doPostBack('<%=btnSearch.UniqueID%>', '');
                }
            });
            hideBottomWarningIfNeeded();
        }

        function ApplyApplicationsChanges() {
            var jQueryTimeout = 60 * 1000;
            var checkFrequency = 2 * 1000;

            <% if (OrionConfiguration.IsDemoServer)
               { %>
            DisplayWarning(' <%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEBCODE_VB0_92) %>');
            return;
            <% } %>

            var progressBar = Ext.MessageBox.show({
                title: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitChangesTitle%>',
                msg: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitChangesText%>',
                width: 400,
                wait: true,
                closable: false
            });

            var data = JSON.stringify({
                webId: <%= HttpUtility.JavaScriptStringEncode(WebId, true) %>
            });

            var showErrorMessage = function (text) {
                Ext.MessageBox.show({
                    title: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTitle%>',
                    msg: text,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }

            var showTryAgainErrorMessage = function (text) {
                Ext.MessageBox.show({
                    title: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTitle%>',
                    msg: text,
                    buttons: { yes: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTryAgainText%>', no: '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedCancelText%>' },
                    fn: function (btn) {
                        Ext.MessageBox.hide();

                        if (btn === "yes")
                            ApplyApplicationsChanges();
                    },
                    icon: Ext.MessageBox.ERROR
                });
            }

            var failHandler = function (xhr, status) {
                progressBar.hide();
                if (status === "timeout")
                    showErrorMessage('<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextWithError%>' + '<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextTimeout%>');
                else
                    showTryAgainErrorMessage('<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextWithoutError%>');
            };

            var checkSubmitProgress = function () {
                $.ajax({
                    type: "POST",
                    url: "/Orion/TrafficAnalysis/WebServices/NetFlowAppsService.asmx/GetSubmitApplicationChangesProgress",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    timeout: jQueryTimeout
                }).done(function (response) {
                    if (response && response.d) {
                        //still processing
                        if (!response.d.Finished) {
                            setTimeout(checkSubmitProgress, checkFrequency);
                            return;
                        }

                        //Submit succeess
                        if (response.d.Success)
                            window.location = "/Orion/TrafficAnalysis/Admin/NetFlowSettings.aspx";
                        //There was an submit collision
                        else if (response.d.Collision)
                            window.location = window.location + "&SubmitCollision=true";
                        //Unexpected error
                        else {
                            progressBar.hide();
                            showErrorMessage('<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextWithError%>' + response.d.Error);
                        }
                    } else {
                        progressBar.hide();
                        showTryAgainErrorMessage('<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextWithoutError%>');
                    }
                }).fail(failHandler);
            }

            $.ajax({
                type: "POST",
                url: "/Orion/TrafficAnalysis/WebServices/NetFlowAppsService.asmx/SubmitApplicationChanges",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                timeout: jQueryTimeout
            }).done(function (response) {
                if (response && response.d && !response.d.Success) {
                    progressBar.hide();
                    showTryAgainErrorMessage('<%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitFailedTextWithoutError%>');
                }
            }).fail(failHandler);

            //Submit started
            setTimeout(checkSubmitProgress, checkFrequency);
        }

        var src;
        $(function() {
            $('#<%= txtSearch.ClientID %>').focus(function() {
                if (!this.clicked) {
                    this.clicked = true;
                    $(this)
                        .val('')
                        .removeClass('x-form-empty-field'); // hack for missing support of placeholder attribute in IE 8, 9
                }
            });
        });
    </script>

    <div style="white-space: nowrap">
        <div class="PageHeader" id="etxxHeader" style="width: 998px;">
            <%= Resources.NTAWebContent.NTAWEBDATA_AK0_33%>
        </div>

        <div id="appTable" style="display: inline-block; border: 0px solid green; max-width: 22%; white-space: normal;">
            <div id="viewMenu">
                <h1><%= Resources.NTAWebContent.NTAWEBDATA_VB0_112%></h1>
                <ul>
                    <asp:Repeater runat="server" ID="rptViews">
                        <itemtemplate>
                            <li class='<%# this.GetViewClass(Container.DataItem) %>'>
                                <a href='<%# this.GetViewHref(Container.DataItem) %>'>
                                    <%# this.GetViewName(Container.DataItem) %>
                                </a>
                            </li>
                        </itemtemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>

        <div style="display: inline-block; border: 0px solid red; margin-left: 5px; max-width: 78%;">
            <asp:UpdatePanel runat="server" ID="upMain" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="progPanel" runat="server">
                        <ProgressTemplate>
                            <%-- Working....--%>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <div id="portsSummary">
                        <img src="../images/icon_large_info.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_121%>" style="margin-bottom: -3px; margin-right:5px; float:left;"/>
                        <b><%# String.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_96, monitoredPorts) %></b>
                        <br />
                        <asp:Label runat="server" ID="lblSelectedSummary" />
                    </div>
                    
                    <div id="divUnsubmittedChanges" runat="server">
                        <div class="sw-suggestion pendingUpdate">
                            <div class="innerStatusMessage">
                                <span class="sw-suggestion-icon"></span>
                                <span><%=Resources.NTAWebContent.NTAWEBDATA_HistoricalUpdates_AppsPortsSettings_InfoText %></span>
                                <br />
                                <span class="ntasBlueLink">
                                    <a href="<%=KnowledgebaseHelper.GetMindTouchKBUrl(109086) %>" target="blank" rel="noopener noreferrer"><%= Resources.NTAWebContent.NTAWEBDATA_LearnMore_HelpLinkText %></a>
                                </span>
                            </div>
                        </div>

                        <div class="sw-btn-bar">   
                            <orion:LocalizableButton runat="server" ID="pnlBtnSubmitApplicationChanges" CssClass="etxxBtn" LocalizedText="Submit" DisplayType="Primary" OnClientClick="ApplyApplicationsChanges(); return false;" />
                            <orion:LocalizableButton runat="server" ID="pnlBtnCancelApplicationChanges" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelApplicationChangesClickHandler" />
                        </div>
                    </div>
                        
                    <div class="mainGridArea" >
                        <div class="toolbar">
                            <asp:LinkButton ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_108%>" runat="server" ID="lnkAddApp" OnClick="ShowAddApplicationDialog"> <img src="/Orion/TrafficAnalysis/images/icon_add.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_38%>" style="margin-bottom: -3px; margin-left:3px;" /> <%= Resources.NTAWebContent.NTAWEBDATA_AK0_38%> </asp:LinkButton>
                            <asp:LinkButton ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_109%>" runat="server" ID="lnkMonitorAll" OnClick="ShowMonitorAllDialog"> <img src="../images/icon_monitor_all.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_113%>" style="margin-bottom: -3px" /> <%= Resources.NTAWebContent.NTAWEBDATA_VB0_113%> </asp:LinkButton>
                            <asp:LinkButton ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_110%>" runat="server" ID="lnkUnmonitorAll" OnClick="ShowUnmonitorAllDialog"> <img src="../images/icon_unmonitor_all.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_114%>" style="margin-bottom: -3px" /> <%= Resources.NTAWebContent.NTAWEBDATA_VB0_114%> </asp:LinkButton>
                            <asp:LinkButton ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_111%>" runat="server" ID="lnkDefaultAll" OnClick="ShowRestoreToDefaultDialog" > <img src="../images/icon_restore_to_default.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_115%>" style="margin-bottom: -3px" /> <%= Resources.NTAWebContent.NTAWEBDATA_VB0_115%> </asp:LinkButton>
                        </div>
                        <div id="maHeader">
                            <div class="maDiv1">
                                &nbsp;&nbsp;<asp:Label ID="lblPortsText" runat="server" />
                            </div>
                            <div class="maDiv2 x-small-editor" style="padding-right:5px;">
                                <div class="x-form-field-wrap x-form-field-trigger-wrap" style="width: 200px;">
                                    <asp:TextBox runat="server" style="padding-right: 0; margin-right: 0; width: 175px; vertical-align: middle" ID="txtSearch" CssClass="x-form-text x-form-field" Columns="30" Placeholder="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_80%>" Text="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_80%>" onkeydown="return (event.keyCode!=13);" /><!--
                                 --><span class="x-form-twin-triggers"><!--
                                    --><asp:ImageButton runat="server" ImageUrl="/Orion/images/search_button.gif" CssClass="x-form-trigger x-form-search-trigger" ID="btnSearch" OnClick="SearchClickHandler" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mainGridArea" style="margin-top:0px; padding-top:0px;">
                        <nta:AppGrid runat="server" 
                            ID="appGrid" 
                            OnSortCommand="DataGridSortHandler" 
                            OnEditCommand="DataGridEditHandler" 
                            OnDeleteCommand="DataGridDeleteHandler" 
                            OnDeleteRangeCommand="DataGridDeleteRangeHandler"
                            OnEnableCommand="DataGridEnableHandler"
                            OnDisableCommand="DataGridDisableHandler"
                            />
                    </div>
                    <div class="mainGridArea" style="margin-top:0; padding: 3px 5px">
                        <nta:AppPager runat="server" ID="pagerBottom" OnPageChange="PageChangeHandler" /> 
                    </div>
                    
                    <div id="pendingUpdatesWarning" runat="server">
                        <div class="sw-suggestion pendingUpdate">
                            <div class="innerStatusMessage">
                                <span class="sw-suggestion-icon"></span>
                                <span><%=Resources.NTAWebContent.NTAWEBDATA_HistoricalUpdates_AppsPortsSettings_InfoText %></span>
                                <br />
                                <span class="ntasBlueLink">
                                    <a href="<%=KnowledgebaseHelper.GetMindTouchKBUrl(109086) %>" target="blank" rel="noopener noreferrer"><%= Resources.NTAWebContent.NTAWEBDATA_LearnMore_HelpLinkText %></a>
                                </span>
                            </div>
                        </div>
                        <div class="sw-btn-bar">   
                            <orion:LocalizableButton runat="server" ID="btnSubmitApplicationChanges" CssClass="etxxBtn" LocalizedText="Submit" DisplayType="Primary" OnClientClick="ApplyApplicationsChanges(); return false;" />
                            <orion:LocalizableButton runat="server" ID="btnCancelApplicationChanges" LocalizedText="Cancel" DisplayType="Secondary" OnClick="CancelApplicationChangesClickHandler" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <asp:HiddenField ID="HiddenButtonEdit" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgAddEdit" runat="server"
        BehaviorID="addEditPopup"
        TargetControlID="HiddenButtonEdit"
        PopupControlID="PanelEdit"
        DropShadow="false"
        PopupDragHandleControlID="PanelEdit"
        BackgroundCssClass="modalBackground"
        RepositionMode="None" />
    <asp:Panel ID="PanelEdit" runat="server" CssClass="modalPopup" Style="display: none" Width="733px">
        <asp:UpdatePanel ID="updatePanelEdit" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <nta:AppEditor runat="server" 
                    ID="appEditor"
                    OnCancel="AddEditApplicationCancel" 
                    OnValidate="EditorValidateHandler" 
                    OnEditSuccess="EditorSubmitHandler"
                    OnResolveConflicts="EditorResolveHandler"
                    />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:HiddenField ID="HiddenButtonDelete" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgConfirmDelete" runat="server"
        TargetControlID="HiddenButtonDelete"
        PopupControlID="PanelDeleteConfirm"
        PopupDragHandleControlID="PanelDeleteConfirm"
        DropShadow="false"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelDeleteConfirm" runat="server" CssClass="modalPopup" Style="display: none" Width="383px">
        <asp:UpdatePanel ID="updatePanelDeleteConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h1 class="modalHeader">
                    <asp:Label ID="lblDeleteHeader"  runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_90%>"></asp:Label>    
                </h1>
                <div class="modalContent">
                    <asp:Label ID="lblDeleteConfirm" style="line-height:20px;" runat="server" CssClass="modalPopupHeader"></asp:Label>    
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                    <orion:LocalizableButton ID="imgButtDelete" runat="server" OnClick="DeleteConfirmHandler" LocalizedText="CustomText" Text="<%$Resources:NTAWebContent,NTAWEBCODE_VB0_107%>" DisplayType="Primary" />
                    <orion:LocalizableButton ID="imgButtDeleteCancel" runat="server" OnClick="DeleteConfirmCancelHandler" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:HiddenField ID="HiddenButtonMonitorAll" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgMonitorAll" runat="server"
        TargetControlID="HiddenButtonMonitorAll"
        PopupControlID="PanelMonitorAll"
        DropShadow="false"
        PopupDragHandleControlID="PanelMonitorAll"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelMonitorAll" runat="server" CssClass="modalPopup" Style="display: none" Width="433px">
        <asp:UpdatePanel ID="updPanelMonitorAll" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <h1 class="modalHeader"> 
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_116%>
                </h1>
                <div class="modalContent">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_117%>
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="imgButtMonitorAll" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_122%>" DisplayType="Primary"  OnClick="MonitorAllConfirmHandler" />
                    <orion:LocalizableButton runat="server" ID="imgButtMonitorAllCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="MonitorAllCancelHandler" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:HiddenField ID="HiddenButtonDisableMonitor" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgUnmonitorAll" runat="server"
        TargetControlID="HiddenButtonDisableMonitor"
        PopupControlID="PanelUnmonitorMonitorAll"
        DropShadow="false"
        PopupDragHandleControlID="PanelUnmonitorMonitorAll"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelUnmonitorMonitorAll" runat="server" CssClass="modalPopup" Style="display: none" Width="433px">
        <asp:UpdatePanel ID="updPanelUnmonitorAll" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <h1 class="modalHeader">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_118%>
                </h1>
                <div class="modalContent">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_119%>
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="imgButtUnMonitorAll" LocalizedText="CustomText"  Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_123%>" DisplayType="Primary" OnClick="UnmonitorAllConfirmHandler" />
                    <orion:LocalizableButton runat="server" ID="imgButtUnMonitorAllCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="UnmonitorAllCancelHandler" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:HiddenField ID="HiddenButtonRestore" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgRestoreToDefault" runat="server"
        TargetControlID="HiddenButtonRestore"
        PopupControlID="PanelRestore"
        DropShadow="false"
        PopupDragHandleControlID="PanelRestore"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelRestore" runat="server" CssClass="modalPopup" Style="display: none" Width="433px">
        <asp:UpdatePanel ID="updPanelRestore" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <h1 class="modalHeader">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_115%>
                </h1>
                <div class="modalContent">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_120%> 
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="imgButtRestore" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_115%>" LocalizedText="CustomText" DisplayType="Primary" OnClick="RestoreToDefaultConfirmHandler" />
                    <orion:LocalizableButton runat="server" ID="imgButtRestoreCancel" LocalizedText="Cancel" DisplayType="Secondary" OnClick="RestoreToDefaultCancelHandler" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="maupdateProgress" style="display: none">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
            <ProgressTemplate> 
                <div style="text-align: center; border:none;"> 
                    <div style="width:75px; float:left; border:none; ">
                        <img src="/Orion/TrafficAnalysis/images/animated_loading_graphic.gif" style="margin-top:20px;" alt="Processing" /> 
                    </div>
                    <div style="margin-top:40px; width:175px; float:right; border:none;">
                        <b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_75%></b>
                    </div>
                </div> 
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" 
        TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" 
        PopupControlID="panelUpdateProgress" />

    <asp:HiddenField ID="HiddenSubmitCollision" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgSubmitCollision" runat="server"
        TargetControlID="HiddenSubmitCollision"
        PopupControlID="PanelSubmitCollision"
        DropShadow="false"
        PopupDragHandleControlID="PanelSubmitCollision"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelSubmitCollision" runat="server" CssClass="modalPopup" Style="display: none" Width="433px">
        <asp:UpdatePanel ID="updPanelSubmitCollision" runat="server" UpdateMode="Conditional">
            <ContentTemplate>  
                <h1 class="modalHeader">
                    <%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitCollisionHeader %>
                </h1>
                <div class="modalContent">
                    <%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitCollisionText %>
                    <br /><br />
                    <%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitCollisionText2 %>
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                    <orion:LocalizableButton runat="server" ID="imgButtCollisionOk" LocalizedText="Ok" DisplayType="Primary" OnClick="SubmitCollisionOkHandler" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
