<%@ Control Language="C#" ClassName="TransportProtocols" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>

<script runat="server">
    public int GetCount()
    {
        return TransportProtocolsDAL.SelectedCount();
    }
</script>
<table border="0" cellPadding="2" cellSpacing="0" width="500">
    <tr>
        <td class="Text"> <b> <%=GetCount() %></b>
          <%= Resources.NTAWebContent.NTAWEBDATA_VB0_61%>
         </td>
    </tr>
</table>