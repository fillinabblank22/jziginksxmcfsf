<%@ Page Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" AutoEventWireup="true" CodeFile="ManageAS.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_ManageAS" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_69%>" %>

<%@ Register TagPrefix="nta" TagName="AppPager" Src="~/Orion/TrafficAnalysis/Admin/AppPager.ascx" %>
<%@ Register TagPrefix="nta" TagName="ASGrid" Src="~/Orion/TrafficAnalysis/Admin/ASGrid.ascx" %>
<%@ Register TagPrefix="nta" TagName="ASEditor" Src="~/Orion/TrafficAnalysis/Utils/ASEditor.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="nta" TagName="NtaHelpButton" Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="NtaHelpButton1" runat="server" HelpUrlFragment="OrionNetFlowPHNodeDetailsManageAS" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_70%>" />
</asp:Content>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" Runat="Server">

    <link rel="stylesheet" type="text/css" href="/Orion/TrafficAnalysis/styles/NetFlow.css" />

    <script type="text/javascript">
        function show_progress_bar(modal_window_to_close) {

            // hide modal window
            $find(modal_window_to_close).hide();

            // show progress bar modal window
            var ModalProgress = "<%= ModalProgress.ClientID %>";
            $find(ModalProgress).show();
        }

        $(document).ready(function () {
            $("[id$='txtSearch']").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("[id$='btnSearch']").click();
                }
            });
        });

    </script>

    <div class="PageHeader" id="etxxHeader" style="width: auto !important;">
        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_69%>
    </div>
    <div runat="server" id="dynamicDiv" style="display: inline-block; border: 0px solid red; margin-left: 5px;">
        <asp:UpdatePanel runat="server" ID="upMain" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdateProgress ID="progPanel2" runat="server">
                    <ProgressTemplate>
                        <%-- Working....--%>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                              
                <div class="mainGridArea">
                    <div class="toolbar" >
                        <asp:LinkButton ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_71%>" runat="server" ID="lnkAddApp" OnClick="ShowAddApplicationDialog"> <img src="/Orion/TrafficAnalysis/images/icon_add.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_72%>" /> <%= Resources.NTAWebContent.NTAWEBDATA_VB0_72%> </asp:LinkButton>
                    </div>
                    <div id="maHeader">
                        <div class="maDiv1">
                            &nbsp;&nbsp;<asp:Label ID="lblPortsText" runat="server" />
                        </div>
                        <div class="maDiv2" style="padding-right:5px;">
                            <nta:AppPager runat="server" ID="pagerTop" OnPageChange="PageChangeHandler" />
                            <asp:TextBox runat="server" ID="txtSearch" Columns="30" ForeColor="#787878" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_73%>" onkeydown="return (event.keyCode!=13);" />
                            <orion:LocalizableButton runat="server" ID="btnSearch" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_74%>" DisplayType="Small" OnClick="SearchClickHandler" />
                        </div>
                    </div>
                </div>
                <div class="mainGridArea" style="margin-top:0px; padding-top:0px;">
                    <nta:ASGrid runat="server" 
                                ID="asGrid" 
                                OnSortCommand="DataGridSortHandler" 
                                OnEditCommand="DataGridEditHandler" 
                                OnDeleteCommand="DataGridDeleteHandler" 
                                    />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:HiddenField ID="HiddenButtonEdit" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgAddEdit" runat="server"
        BehaviorID="addEditPopup"
        TargetControlID="HiddenButtonEdit"
        PopupControlID="PanelEdit"
        DropShadow="false"
        PopupDragHandleControlID="PanelEdit"
        BackgroundCssClass="modalBackground"
        RepositionMode="None" />
    <asp:Panel ID="PanelEdit" runat="server" CssClass="modalPopup" Style="display: none" Width="733px">
        <asp:UpdatePanel ID="updatePanelEdit" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <nta:ASEditor runat="server" 
                    ID="asEditor"
                    OnCancel="AddEditApplicationCancel" 
                    OnValidate="EditorValidateHandler" 
                    OnEditSuccess="EditorSubmitHandler"
                    OnResolveConflicts="EditorResolveHandler"
                    />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:HiddenField ID="HiddenButtonDelete" runat="server" />
    <ajaxToolkit:ModalPopupExtender ID="dlgConfirmDelete" runat="server"
        TargetControlID="HiddenButtonDelete"
        PopupControlID="PanelDeleteConfirm"
        PopupDragHandleControlID="PanelDeleteConfirm"
        DropShadow="false"
        BackgroundCssClass="modalBackground"
        RepositionMode="RepositionOnWindowResizeAndScroll" />
    <asp:Panel ID="PanelDeleteConfirm" runat="server" CssClass="modalPopup" Style="display: none" Width="500px">
        <asp:UpdatePanel ID="updatePanelDeleteConfirm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modalHeader">
                    <asp:Label ID="lblDeleteHeader"  runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_53%>"></asp:Label>
                </div>
                <div class="modalContent">
                    <asp:Label ID="lblDeleteConfirm" style="line-height:20px;" runat="server" CssClass="modalPopupHeader"></asp:Label>
                </div>
                <div class="modalButtonPanel sw-btn-bar">
                   <orion:LocalizableButton ID="imgButtDelete" runat="server" OnClick="DeleteConfirmHandler" LocalizedText="Delete" DisplayType="Primary" />
                   <orion:LocalizableButton ID="imgButtDeleteCancel" runat="server" OnClick="DeleteConfirmCancelHandler" LocalizedText="Cancel" DisplayType="Secondary" />
                </div>
           </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="maupdateProgress" style="display: none">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
            <ProgressTemplate> 
                <div style="text-align: center; border:none;"> 
                    <div style="width:75px; float:left; border:none;">
                        <img src="/Orion/TrafficAnalysis/images/animated_loading_graphic.gif" style="margin-top:20px;" alt="Processing" /> 
                    </div>
                    <div style="margin-top:40px; width:175px;float:right; border:none;">
                        <b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_75%></b>
                    </div>
                </div> 
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" 
        TargetControlID="panelUpdateProgress"
        PopupControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" />
</asp:Content>
