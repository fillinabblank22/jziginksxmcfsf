using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Netflow.Contracts.Applications;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Web.Applications.DAL;

public partial class Orion_TrafficAnalysis_Admin_AppGrid : UserControl
{
    public event DataGridSortCommandEventHandler SortCommand;
    public event DataGridCommandEventHandler EditCommand;
    public event DataGridCommandEventHandler DeleteCommand;
    public event CommandEventHandler DeleteRangeCommand;

    public event DataGridCommandEventHandler DisableCommand;
    public event DataGridCommandEventHandler EnableCommand;

    public ApplicationDALCriteria Criteria { get; set; }    // new one


    public object DataSource
    {
        get
        {
            return this.dgApplications.DataSource;
        }
        set
        {
            List<Application> apps = value as List<Application>;
            if (apps == null)
                throw new InvalidOperationException("List of applications was not defined");

            DataTable t = new DataTable();
            t.Columns.Add("AppID", typeof(int));
            t.Columns.Add("Enabled", typeof(int));
            t.Columns.Add("TCP", typeof (bool));
            t.Columns.Add("UDP", typeof(bool));
            t.Columns.Add("Name", typeof (string));
            t.Columns.Add("MultiPort", typeof (bool));
            t.Columns.Add("Port", typeof (string));
            t.Columns.Add("PortRange", typeof(string));
            t.Columns.Add("SrcIPGroupName", typeof(string));
            t.Columns.Add("DstIPGroupName", typeof(string));
            t.Columns.Add("CssClass", typeof(string));
            int counter = 0;
            foreach (Application app in apps)
            {
                DataRow row = t.NewRow();
                row["AppID"] = app.ID;
                row["Enabled"] = app.Enabled;
                row["TCP"] = app.Protocols.HasFlag(ApplicationProperties.ApplicationProtocol.TCP);
                row["UDP"] = app.Protocols.HasFlag(ApplicationProperties.ApplicationProtocol.UDP);
                row["Name"] = app.Name;
                row["MultiPort"] = app.IsMultiPort;
                row["Port"] = app.IsMultiPort ? NTAWebContent.NTAWEBDATA_ManageApps_MultiPort : app.Ports.First().Port.ToString();
                row["PortRange"] = string.Empty;
                var cssClass = (counter++ % 2 == 0) ? "TopResourceEvenTableRow" : "TopResourceOddTableRow";
                row["CssClass"] = cssClass + " TopResourceRow";
                ApplicationGroup srcGroup = app.IPGroups.Find(g => g.Direction == ApplicationProperties.ConditionDirection.Source);
                ApplicationGroup dstGroup = app.IPGroups.Find(g => g.Direction == ApplicationProperties.ConditionDirection.Destination);

                row["SrcIPGroupName"] = ApplicationsHelper.GetApplicationGroupName(srcGroup.GroupID);
                row["DstIPGroupName"] = ApplicationsHelper.GetApplicationGroupName(dstGroup.GroupID);

                t.Rows.Add(row.ItemArray);

                if (app.IsMultiPort)
                {
                    foreach (string port in NetFlowCommon.GetRanges(app.Ports))
                    {
                        row["PortRange"] = port;
                        row["Name"] = port.Contains("-") ? NTAWebContent.NTAWEBCODE_VB0_98 : NTAWebContent.NTAWEBCODE_VB0_99;
                        row["Port"] = port;
                        t.Rows.Add(row.ItemArray);
                    }
                }
            }

            this.dgApplications.DataSource = t;
        }
    }

   

    protected override void OnInit(EventArgs e)
    {
        this.Page.Header.Controls.Add(this.ParseControl(GRID_CSS));
        
        base.OnInit(e);
    }

    #region Sort Indicator Display Routines
    protected string GetDescriptionSortImg()
    {
        return this.GetSortIcon(ApplicationDALCriteria.AppSortColumn.Name);
    }

    protected string GetPortSortImg()
    {
        return this.GetSortIcon(ApplicationDALCriteria.AppSortColumn.Port);
    }

    protected string GetProtocolSortImg()
    {
        return this.GetSortIcon(ApplicationDALCriteria.AppSortColumn.Protocol);
    }
    protected string GetSourceSortImg()
    {
        return this.GetSortIcon(ApplicationDALCriteria.AppSortColumn.GroupNameSrc);
    }
    protected string GetDestinationSortImg()
    {
        return this.GetSortIcon(ApplicationDALCriteria.AppSortColumn.GroupNameDst);
    }
    

    private string GetSortIcon(ApplicationDALCriteria.AppSortColumn which)
    {
        string sortDir = this.Request.QueryString["SortDir"];
        string sortField = this.Request.QueryString["SortField"];

        if (false == Enum.GetNames(typeof(ApplicationDALCriteria.AppSortColumn)).Contains<string>(sortField))
        {
            return string.Empty;
        }

        if (which != (ApplicationDALCriteria.AppSortColumn)Enum.Parse(typeof(ApplicationDALCriteria.AppSortColumn), sortField))
        {
            return string.Empty;
        }


        ApplicationDALCriteria.SortStyle style = ApplicationDALCriteria.SortStyle.None;
        if (sortDir == "DESC")
        {
            style = ApplicationDALCriteria.SortStyle.Descending;
        }
        else if (sortDir == "ASC")
        {
            style = ApplicationDALCriteria.SortStyle.Ascending;
        }

        switch (style)
        {
            case ApplicationDALCriteria.SortStyle.None:
                return string.Empty;
            case ApplicationDALCriteria.SortStyle.Ascending:
                return String.Format("<img src='/Orion/TrafficAnalysis/images/Icon.SortAsc.gif' alt='{0}' />",Resources.NTAWebContent.NTAWEBCODE_VB0_61);
            case ApplicationDALCriteria.SortStyle.Descending:
                return String.Format("<img src='/Orion/TrafficAnalysis/images/Icon.SortDesc.gif' alt='{0}' />",Resources.NTAWebContent.NTAWEBCODE_VB0_62);
            default:
                return string.Empty;
        }
    } 
    #endregion

    protected string GetProtocolName(object objTCP, object objUDP)
    {
        bool tcp = Convert.ToBoolean(objTCP);
        bool udp = Convert.ToBoolean(objUDP);

        if (tcp && udp)
            return NTAWebContent.NTAWEBDATA_VB0_94;

        if (tcp)
            return NTAWebContent.NTAWEBDATA_VB0_95;
        if (udp)
            return NTAWebContent.NTAWEBDATA_VB0_96;

        throw new InvalidOperationException("At least one of TCP and UDP must be true.");//TODO:[localization] {exception messages are temporary ignored}
    }

    protected void DataGridOnItemCommandHandler(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.Equals("Disable"))
        {
            DataGridDisableHandler(source, e);
        }
        else if (e.CommandName.Equals("Enable"))
        {
            DataGridEnableHandler(source, e);
        }
    }

    protected void DataGridSortHandler(object source, DataGridSortCommandEventArgs e)
    {
        if (null != this.SortCommand)
            this.SortCommand(source, e);
    }

    protected void DataGridEditHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.EditCommand)
            this.EditCommand(source, e);
    }

    protected void DataGridDeleteHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.DeleteCommand)
            this.DeleteCommand(source, e);
    }

    protected void DeleteRangeHandler(object sender, EventArgs e)
    {
        var button = sender as LocalizableButton;

        CommandEventArgs args = new CommandEventArgs(button.CommandName, button.CommandArgument);
        if (null != this.DeleteRangeCommand)
            this.DeleteRangeCommand(sender, args);
    }

    protected void test(object source, DataGridCommandEventArgs e)
    {
        
    }

    protected void DataGridDisableHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.DisableCommand)
            this.DisableCommand(source, e);
    }

    protected void DataGridEnableHandler(object source, DataGridCommandEventArgs e)
    {
        if (null != this.EnableCommand)
            this.EnableCommand(source, e);
    }

    protected string GetDeleteRangeArgument(object appID, object appRange)
    {
        return string.Format("{0}~~{1}", appID, appRange);
    }

    protected void OnRowDataBound(object sender, DataGridItemEventArgs e)
    {
        var dgi = e.Item;
        if (dgi.ItemType == ListItemType.Item || dgi.ItemType == ListItemType.AlternatingItem)
        {
            dgi.CssClass = (string)DataBinder.Eval(e.Item.DataItem, "[CssClass]");
        }
        int appID = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "[AppID]"));
        if (appID == 0) 
        { 
            return; 
        }

        string portRange = DataBinder.Eval(e.Item.DataItem, "[PortRange]").ToString();
        if (portRange != null && portRange != string.Empty)
        {
            string btnClientID = string.Format("expandButton_{0}", appID);
            this.DoScriptRegister(string.Format("{0}_{1}", appID, portRange), string.Format("RegisterCollapseRow('{0}', '{1}');\n", btnClientID, e.Item.ClientID));
        }
    }

    private void DoScriptRegister(string key, string script)
    {
        if (null == ScriptManager.GetCurrent(this.Page))
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), key, script, true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), key, script, true);
        }
    }

    protected string GetDescriptionSpanClass(object isMulti, object portRange)
    {
        if (portRange is string && !string.IsNullOrEmpty((string)portRange))
        {
            return "ChildRow"; 
        }
      
        if (isMulti != null && Boolean.Parse(isMulti.ToString()) == true)
        {
            return "ParentRow";
        }
        
        return "NormalRow";
 
    }

    protected string GetCollapseButton(bool isMultiPort, int appID, object portRange)
    {
        if (isMultiPort == true && portRange is string && ((string)portRange) == string.Empty)
        {
            return string.Format("<img src='/Orion/images/Button.Expand.gif' class='CollapseButton' alt='' id='expandButton_{0}' style='cursor: pointer;' onclick='Collapse_Click(this); return false;' />", appID);
        }
        
        return string.Empty;
    }

    protected string GetOkIcon(string enabled)
    {
        if (Convert.ToBoolean((enabled.ToLower())))
            return string.Format("<img src='/Orion/TrafficAnalysis/images/icon_OK.gif' alt=''></img>");

        return string.Empty;
    }
    
    private const string GRID_CSS = @"
<style type='text/css'>
    .DataGrid
    {
        font-size: 11px;
        border-collapse: separate;
        margin: 0 0 0 0;
        padding: 0 0 0 0;
        clear: right;
    }
    
    .DataGridHeader
    {
        margin-top: 0px;
    }
    
    .DataGridHeader td
    {
        margin-top: 0px;
        border-right: 1px solid white;
    }
    
    .DataGridHeader td.RightHeader
    {
        border-right: 0px solid white;
    }

    .DataGrid td img.CollapseButton
    {
        float: left;
        margin-top: 2px;
        vertical-align: middle;
    }

    .DataGrid td .ParentRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        padding-left: 4px;
    }

    .DataGrid td .ChildRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        padding-left: 30px;
    }

    .DataGrid td .NormalRow
    {
        padding: 0 0 0 0;
        margin: 0 0 0 0;
        margin-left: 15px;
        display:block
    }
</style>
";
}
