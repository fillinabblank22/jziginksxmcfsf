<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowDatabaseMaintenanceSettings.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_DatabaseMaintenanceSettings" %>
<%@ Register TagPrefix="charting" Namespace="SolarWinds.Orion.Web.Charting.v2" Assembly="OrionWeb" %>

<charting:IncludeCharts runat="server" />
<orion:Include runat="server" File="../js/jquery/timePicker.css" />


<b><asp:Label runat="server" ID="OrionDatabaseSettings" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_OrionDatabaseSettingsTitle%>" Font-Size="11" /></b><br>

<%--Enable database maitanece--%>
<div class="settingsOption">
    <asp:CheckBox class="checkBox" ID="chbEnableDatabaseMaintenance" runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_139%>" Checked='<%#DBMaintenanceEnabled%>' onclick="Check_Clicked(this);" />
</div>
<asp:Panel runat="server" ID="pnlDatabaseMaintenance" Width="100%" >

<%--Maintenance is executed at ....--%>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_120%>
    <asp:TextBox runat="server" ID="txtboxExecutionTime" Text='<%#ExecutionTime%>' Width="70px"></asp:TextBox>
    <span class="settingDescription">
        <%= TimeExampleFormats %>
    </span>
    

    <asp:RequiredFieldValidator CssClass="ntasValidator" ID="execTimeRequiredFieldValidator1_nta_database_validator"
        runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_140%>" ControlToValidate="txtboxExecutionTime">
                    <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
    </asp:RequiredFieldValidator>


    <asp:CustomValidator CssClass="ntasValidator" ID="execTimeRegularExpressionValidator2_nta_database_validator"
        ControlToValidate="txtboxExecutionTime" OnServerValidate="TimeCultureValidate"
        ValidateEmptyText="true"
        Display="Dynamic" runat="server">
            <a href="#" title="<%= TimeErrorFormats %>" alt="<%= TimeErrorFormats %>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
    </asp:CustomValidator>
</div>
<div class="settingsOption">

    <%--Compress database and log files--%>
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_129%>
    <asp:DropDownList ID="ddlDatabaseMaintenanceFrequency" runat="server">
        <asp:ListItem Value="0" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_130%>"/>
        <asp:ListItem Value="1" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_131%>"/>
        <asp:ListItem Value="2" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_128%>"/>
    </asp:DropDownList>
</div>

<hr>

<img src="/Orion/TrafficAnalysis/images/NTASettings/collector.svg" style="float:left; margin-left: -114px">
<b><asp:Label runat="server" ID="Label11" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_FlowStorageDatabaseSettingsTitle%>" Font-Size="11" /></b>
<br>

<table class="DatabaseInfo" style="width:auto" border="0">
    <colgroup span="3">
        <col width="30%">
        <col width="15%">
        <col width="55%">
    </colgroup>
   <tbody>   
<tr>
    <%--flow storage location --%>
  <td><%= Resources.NTAWebContent.NTAWEBDATA_FlowStorage_Name %></td>
  <td colspan="2"><asp:Label runat="server" ID="FlowStorageLocation"></asp:Label></td>		
</tr>
<tr>
    <%--Retention period--%>
  <td><%= Resources.NTAWebContent.NTAWEBDATA_AK0_123%></td>
  <td colspan="2">
      <asp:TextBox runat="server" ID="tbCompPeriod" Rows="1" Width="46" Style="_width: 44px;" />
      <%= Resources.NTAWebContent.NTAWEBDATA_AK0_124%>
      <span class="settingDescription">
        <%= Resources.NTAWebContent.NTAWEBDATA_DataRetentionTime_Hint%>
      </span>

      <asp:RequiredFieldValidator CssClass="ntasValidator" ID="RequiredFieldValidator1_nta_database_validator"
        runat="server" ControlToValidate="tbCompPeriod" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_83%>"
        Display="Dynamic">
    <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
    </asp:RequiredFieldValidator>
    <asp:RangeValidator CssClass="ntasValidator" ID="RangeValidator1_nta_database_validator" runat="server"
        ControlToValidate="tbCompPeriod" Type="integer" MaximumValue="3650" MinimumValue="3"
        ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_138%>" Display="Dynamic">
<a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_138%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_138%>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
    </asp:RangeValidator>
  </td>		
</tr>
<tr>
    <%--Delete expride data--%>
  <td><%= Resources.NTAWebContent.NTAWEBDATA_AK0_125%></td>
  <td colspan="2"><asp:DropDownList ID="ddlFlowCorrelationCleanUpFrequency" runat="server">
                <asp:ListItem Value="0" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_126%>"/>
                <asp:ListItem Value="1" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_127%>"/>
                <asp:ListItem Value="2" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_128%>"/>
            </asp:DropDownList></td>		
</tr>
<tr>
  <td><%= Resources.NTAWebContent.NTAWEBDATA_AverageFlowsPerSecond%></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast5MinutesLabel" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_FlowsPerSecondForLast5Minutes%>" /></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast5MinutesL" type="text" name="flows per second for last 5 minutes" readonly="true" /></td>		
</tr>
<tr>
  <td></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast24HoursLabel" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_FlowsPerSecondForLast24Hours%>" /></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast24HoursL" type="text" name="flows per second for last 24 hours" readonly="true" /></td>		
</tr>
<tr>
  <td></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast3DaysLabel" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_FlowsPerSecondForLast3Days%>" /></td>
  <td><asp:Label runat="server" ID="FlowsPerSecondForLast3DaysL" type="text" name="flows per second for last 3 days" readonly="true" /></td>		
</tr>
</tbody>
</table>

    
</asp:Panel>

<hr>

<orion:Include runat="server" File="jquery.js" />
<orion:Include runat="server" File="jquery/jquery.timePicker.js" />



<script type="text/javascript">
   
    function Check_Clicked(ChkBox) {
        var $_panel = $("[id$='pnlDatabaseMaintenance']");
        var checked = ChkBox.checked;
        EnableDisableValidators(checked);
        
        ShowHidePanel($_panel, checked);
        return true;
    }

    // When user set wrong value and then hides panel (uncheck Database Maintenance)
    // next beahivour could be confusing
    function EnableDisableValidators(checked)
    {
        ValidatorEnable($("[id$='ntaDatabase_RangeValidator1_nta_database_validator']")[0], checked)
        ValidatorEnable($("[id$='ntaDatabase_execTimeRegularExpressionValidator2_nta_database_validator']")[0], checked)
    }

    function ShowHidePanel(panelID, show)
    {
        if (panelID != null && $(panelID).length != 0)
        {
            if (show.toString().indexOf('true') > -1)
                $(panelID).show('normal');
            else 
                $(panelID).hide('normal');
        }
    }

    $(document).ready(function() {
        // show/hide panels regarding checked checkboxes
        ShowHidePanel($("[id$='pnlDatabaseMaintenance']", 'true'), $("[id$='chbEnableDatabaseMaintenance']").attr('checked'));
        
        Ext4.Msg.buttonText.yes = "<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_AK0_6)%>";
        Ext4.Msg.buttonText.no = "<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_AK0_5)%>";

        // TimePeriod
        var regionalSettings = <%=DatePickerRegionalSettings %>;

        $("#<%=txtboxExecutionTime.ClientID %>").timePicker({
            separator: regionalSettings.timeSeparator,
            show24Hours: regionalSettings.show24Hours,
            step:15
        });
        // IE null error
        $("#<%=txtboxExecutionTime.ClientID %>").attr("onchange","");
    });

    function fnConfirm(btn) {
        if (btn == 'no') {
            $("[id$='chbEnableEndpoint']").attr("checked", false);
        }
    }
</script>
