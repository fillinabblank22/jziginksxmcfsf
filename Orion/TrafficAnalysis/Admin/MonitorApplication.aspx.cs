﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.UI;
using System.Globalization;
using System.Threading;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Admin_MonitorApplication : System.Web.UI.Page
{
    private static SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    /// <summary>
    /// The Uri to return to after interface was added successfuly or not
    /// </summary>
    private Uri Referrer
    {
        get
        {
            if (ViewState["previousPageReferrer"] == null)
                return new Uri(ResolveUrl("~/Orion/TrafficAnalysis/SummaryView.aspx"), UriKind.Relative);

            return new Uri((string)ViewState["previousPageReferrer"], UriKind.RelativeOrAbsolute);
        }
        set
        {
            if (value != null)
                ViewState["previousPageReferrer"] = value.ToString();
        }
    }

    private object AppID { get; set; }
    private object AppName { get; set; }
    private object ProtocolName { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (Request.UrlReferrer == null && Request.QueryString["ReturnUrl"] == null)
            {
                Func<CultureInfo, string> getMessage = (culture) =>
                {
                    return Resources.NTAWebContent.ResourceManager.GetString("NTAWEBDATA_MM0_56", culture);
                };

                _log.Error(getMessage(CultureInfo.InvariantCulture));
                throw new InvalidOperationException(getMessage(Thread.CurrentThread.CurrentUICulture));
            }

            // store the page the user came from
            if (Request.UrlReferrer != null)
            {
                Referrer = Request.UrlReferrer;
            } else
            {
                Referrer = new Uri( Request.QueryString["ReturnUrl"]);
            }


            Func<CultureInfo, string, string> missingParamMessage = (culture, paramName) =>
            {
                return string.Format(Resources.NTAWebContent.ResourceManager.GetString("NTAWEBDATA_MM0_57", culture), paramName);
            };

            AppID = Request.QueryString["pAppID"];
            if (AppID == null)
                throw new Exception(missingParamMessage(Thread.CurrentThread.CurrentUICulture, "pAppID"));

            AppName = Request.QueryString["pAppName"];
            if (AppName == null)
                throw new Exception(missingParamMessage(Thread.CurrentThread.CurrentUICulture, "pAppName"));

            ProtocolName = Request.QueryString["pProtocolName"];
            if (ProtocolName == null)
                throw new Exception(missingParamMessage(Thread.CurrentThread.CurrentUICulture, "pProtocolName"));
        }
    }

    protected string GetMonitorAppScript()
    {
        return ApplicationHelper.MonitorPort(AppID, AppName, ProtocolName);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        unmonitoredStuff.Visible = true;
        ScriptManager.GetCurrent(this.Page).Scripts.Add(new ScriptReference("/Orion/TrafficAnalysis/scripts/MonitorPort.js"));
        ScriptManager.GetCurrent(this.Page).Scripts.Add(new ScriptReference("/Orion/TrafficAnalysis/scripts/json2.js"));
        ScriptManager.GetCurrent(this.Page).Scripts.Add(new ScriptReference("/Orion/TrafficAnalysis/scripts/DemoScripts.js"));
    }

    protected void appEditor_Cancel(object sender, EventArgs e)
    {      
        this.Response.Redirect(Referrer.ToString());
    }

    protected void appEditor_Submit(object sender, EventArgs e)
    {       
        this.Response.Redirect(Referrer.ToString());
    }
}