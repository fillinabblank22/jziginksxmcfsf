<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowChartingSettings.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_NetflowChartingSettings" %>
<%@ Register TagPrefix="nta" TagName="RelativePeriod" Src="../TimePeriod/RelativePeriod.ascx" %>
<%@ Register src="../Utils/NumberUpDownControl.ascx" tagname="NumberUpDownControl" tagprefix="uc1" %>

<div class="settingsOption">
    <asp:CheckBox class="checkBox" ID="ProgressiveChartingCheckBox" runat="server" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_107%>" />
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_108%>
    <asp:DropDownList ID="ddlPercentage" runat="server"></asp:DropDownList>    
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_109%> 
    <asp:DropDownList ID="ddlDataUnits" runat="server">
    </asp:DropDownList>
</div>
<hr />
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_110%>
    <nta:RelativePeriod runat="server" id="relativePeriodDetail" style="font-weight: normal; margin-left: -10px;" />
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_111%>
    <nta:RelativePeriod runat="server" id="relativePeriodSummary" style="font-weight: normal; margin-left: -10px;" />
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_112%>
    <nta:RelativePeriod runat="server" id="relativePeriodSearch" style="font-weight: normal; margin-left: -10px;" />
</div>
<hr />
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_113%>
    <asp:DropDownList ID="ddlChartStyleDetail" runat="server" OnSelectedIndexChanged="Index_Changed"></asp:DropDownList>
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_114%>
    <asp:DropDownList ID="ddlChartStyleSummary" runat="server" OnSelectedIndexChanged="Index_Changed"></asp:DropDownList>
</div>
<hr />
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_115%>
    <asp:DropDownList ID="ddlFlowDirectionSummary" runat="server"></asp:DropDownList>
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_116%>
    <asp:DropDownList ID="ddlFlowDirectionNodeDetail" runat="server"></asp:DropDownList>
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_117%>
    <asp:DropDownList ID="ddlFlowDirectionDetail" runat="server"></asp:DropDownList>
</div>
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_OSC_11%>
    <asp:DropDownList ID="ddlFlowDirectionCbqos" runat="server" OnSelectedIndexChanged="Index_Changed"></asp:DropDownList>
</div>
<hr />
<div class="settingsOption">
    <asp:CheckBox class="checkBox" runat="server" ID="chAutoRefreshEnabled" style="float: left; margin-top: 5px;" />
    
    <uc1:NumberUpDownControl ID="numAutoRefreshMinutes" MinimumValue="1" MaximumValue="300" runat="server" TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_RV0_1%>" />
</div>


<style>

</style>
 
