using System;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Orion.Core.Common.i18n.Registrar;


public partial class Orion_TrafficAnalysis_Admin_NetflowChartingSettings : System.Web.UI.UserControl, INetFlowSettings
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            SetUI();
        }
    }

    public void SetUI()
    {
        // 1. Progressive Charting
        this.ProgressiveChartingCheckBox.Checked = GlobalSettingsDALCached.ProgressiveCharting;

        this.ddlPercentage.Items.Clear();
        this.ddlPercentage.Items.Add(new ListItem(Resources.NTAWebContent.NTAWEBCODE_AK0_55, "absolute"));
        this.ddlPercentage.Items.Add(new ListItem(Resources.NTAWebContent.NTAWEBCODE_AK0_56, "relative"));

        this.ddlPercentage.SelectedIndex = (GlobalSettingsDALCached.PercentagesType == PercentageType.Absolute) ? 0 : 1;
        
        // 2. Data Units
        this.ddlDataUnits.Items.Clear();
        this.ddlDataUnits.Items.Add(new ListItem(ChartDU.DefaultText.Bitrate, ChartDU._BITRATE));
        this.ddlDataUnits.Items.Add(new ListItem(ChartDU.DefaultText.InterfaceUtilization, ChartDU._INTERFACE_UTILIZATION));
        this.ddlDataUnits.Items.Add(new ListItem(ChartDU.DefaultText.PercentOfTotalBytes, ChartDU._PERCENT_OF_TOTALBYTES));
        this.ddlDataUnits.Items.Add(new ListItem(ChartDU.DefaultText.TransferredBytes, ChartDU._TRANSFERRED_BYTES));

        // 3. Chart style fore resources placed on detail views
        this.ddlChartStyleDetail.Items.Add(new ListItem(NetFlowCommon.GetChartTypeText(ChartType.Area), ChartType.Area.ToString()));
        this.ddlChartStyleDetail.Items.Add(new ListItem(NetFlowCommon.GetChartTypeText(ChartType.Pie2D), ChartType.Pie2D.ToString()));

        // 4. Chart style fore resources placed on summary views
        this.ddlChartStyleSummary.Items.Add(new ListItem(NetFlowCommon.GetChartTypeText(ChartType.Area), ChartType.Area.ToString()));
        this.ddlChartStyleSummary.Items.Add(new ListItem(NetFlowCommon.GetChartTypeText(ChartType.Pie2D), ChartType.Pie2D.ToString()));

        this.ddlFlowDirectionSummary.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, true), "Both"));
        this.ddlFlowDirectionSummary.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, false), "Ingress"));
        this.ddlFlowDirectionSummary.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(false, true), "Egress"));

        this.ddlFlowDirectionNodeDetail.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, false), "Ingress"));
        this.ddlFlowDirectionNodeDetail.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(false, true), "Egress"));

        this.ddlFlowDirectionDetail.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, true), "Both"));
        this.ddlFlowDirectionDetail.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, false), "Ingress"));
        this.ddlFlowDirectionDetail.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(false, true), "Egress"));

        this.ddlFlowDirectionCbqos.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, true), "Both"));
        this.ddlFlowDirectionCbqos.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(true, false), "Ingress"));
        this.ddlFlowDirectionCbqos.Items.Add(new ListItem(FlowDirectionFilter.GetFlowDirectionTitle(false, true), "Egress"));


        // set actual global value from database
        string val = GlobalSettingsDALCached.ChartGlobalDataUnits;
        ddlDataUnits.SelectedValue = val;

        // 5. Default resource time for DETAIL 
        relativePeriodDetail.PeriodFilter = TimePeriodUiFilter.Parse(GlobalSettingsDALCached.DefaultDetailTimePeriod);

        // 6. Default resource time for SUMMARY
        relativePeriodSummary.PeriodFilter = TimePeriodUiFilter.Parse(GlobalSettingsDALCached.InstanceInterface.DefaultSummaryTimePeriod);

        // default time period for search
        relativePeriodSearch.PeriodFilter = TimePeriodUiFilter.Parse(GlobalSettingsDALCached.DefaultSearchTimePeriod);

        // 7. set actual global value from database
        val = GlobalSettingsDALCached.ChartStyleOnDetailView;
        ddlChartStyleDetail.SelectedValue = val;

        val = GlobalSettingsDALCached.ChartStyleOnSummaryView;
        ddlChartStyleSummary.SelectedValue = val;

        ddlFlowDirectionSummary.SelectedValue = GlobalSettingsDALCached.DefaultSummaryFlowDirection;
        ddlFlowDirectionNodeDetail.SelectedValue = GlobalSettingsDALCached.DefaultNodeDetailFlowDirection;
        ddlFlowDirectionDetail.SelectedValue = GlobalSettingsDALCached.DefaultDetailFlowDirection;
        ddlFlowDirectionCbqos.SelectedValue = GlobalSettingsDALCached.DefaultCbqosFlowDirection;

        chAutoRefreshEnabled.Checked = GlobalSettingsDALCached.AutoRefreshEnabled;
        numAutoRefreshMinutes.Value = (int)TimeSpan.FromSeconds(GlobalSettingsDALCached.AutoRefreshSeconds).TotalMinutes;
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    public void StoreSettings()
    {
        GlobalSettingsDALCached.ProgressiveCharting = this.ProgressiveChartingCheckBox.Checked;
        // save stack chart options setting
        GlobalSettingsDALCached.ChartGlobalDataUnits = this.ddlDataUnits.SelectedValue;
        GlobalSettingsDALCached.PercentagesType = (ddlPercentage.SelectedIndex == 0) ? PercentageType.Absolute : PercentageType.Relative;
        GlobalSettingsDALCached.DefaultDetailTimePeriod = relativePeriodDetail.PeriodFilter.PeriodName;
        GlobalSettingsDALCached.InstanceInterface.DefaultSummaryTimePeriod = relativePeriodSummary.PeriodFilter.PeriodName;
        GlobalSettingsDALCached.DefaultSearchTimePeriod = relativePeriodSearch.PeriodFilter.PeriodName;

        GlobalSettingsDALCached.ChartStyleOnDetailView = ddlChartStyleDetail.SelectedValue;
        GlobalSettingsDALCached.ChartStyleOnSummaryView = ddlChartStyleSummary.SelectedValue;

        GlobalSettingsDALCached.DefaultSummaryFlowDirection = ddlFlowDirectionSummary.SelectedValue;
        GlobalSettingsDALCached.DefaultNodeDetailFlowDirection = ddlFlowDirectionNodeDetail.SelectedValue;
        GlobalSettingsDALCached.DefaultDetailFlowDirection = ddlFlowDirectionDetail.SelectedValue;
        GlobalSettingsDALCached.DefaultCbqosFlowDirection = ddlFlowDirectionCbqos.SelectedValue;
        
        GlobalSettingsDALCached.AutoRefreshEnabled = chAutoRefreshEnabled.Checked;
        GlobalSettingsDALCached.AutoRefreshSeconds = numAutoRefreshMinutes.Value * 60;
    }

    /// <summary>
    /// If index is changed then we remove value from session
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void Index_Changed(Object sender, EventArgs e)
    {
        ResetChartTypesFromSession();
    }

    /// <summary>
    /// Reset all occurences of NTA_ChartSessionSettins ... ChartType
    /// in the case that Chart Style value was changed.
    /// </summary>
    private void ResetChartTypesFromSession()
    {
        List<string> sessionList = new List<string>();
        IEnumerator ie = Session.GetEnumerator();

        var sessionKeys = Session.Keys.Cast<string>();
        sessionKeys.Where(s => s.Contains("NTA_ChartSessionSettings#ChartType")).ToList()
            .ForEach(s => { Session.Remove(s); });
    }

    public string Icon
    {
        get
        {
            return "/Orion/TrafficAnalysis/images/NTASettings/chart-pie.svg";
        }
    }

    public string Caption
    {
        get
        {
            return Resources.NTAWebContent.NTAWEBCODE_AK0_51;
        }
    }

    public string Redirect
    {
        get
        {
            return "Chart";
        }
    }

    public string Help
    {
        get { return "OrionNetFlowPHSettingsChartGraph"; }
    }

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        // No host services are consumed
    }
}
