<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ASGrid.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_ASGrid" %>

<asp:DataGrid runat="server" 
              ID="dgAS" 
              AllowPaging="false"
              AllowSorting="true" 
              PageSize="50" 
              BorderStyle="Solid" 
              AutoGenerateColumns="false"
              ShowFooter="false"
              GridLines="None"
              CellSpacing="1"
              CellPadding="5"
              CssClass="DataGrid NeedsZebraStripes"
              BorderColor="#dfdfde"
              OnItemCommand="DataGridOnItemCommandHandler"
              OnSortCommand="DataGridSortHandler"
              OnItemDataBound="OnRowDataBound"
              OnEditCommand="DataGridEditHandler"
              OnDeleteCommand="DataGridDeleteHandler"
              >
    <HeaderStyle Font-Italic="true" CssClass="DataGridHeader" />
    <Columns>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_10%>" SortExpression="Name" >
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton ID="lbName" runat="server" CommandName="Sort" CommandArgument="Name" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_10%>" />
                 <%# this.GetNameSortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                    <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("Name").ToString()) %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_11%>" SortExpression="ID" ItemStyle-Width="40" >
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton ID="lbID" runat="server" CommandName="Sort" CommandArgument="ID" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_11%>" />
                 <%# this.GetIDSortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("ID") %>
            </ItemTemplate>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_18%>" SortExpression="Country">
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton ID="lbCountry" runat="server" CommandName="Sort" CommandArgument="Country" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_18%>" />
                 <%# this.GetCountrySortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("Country").ToString()) %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_12%>" SortExpression="Organization">
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton ID="lbOrganization" runat="server" CommandName="Sort" CommandArgument="Organization" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_12%>" />
                 <%# this.GetOrganizationSortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("Organization").ToString()) %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_13%>" SortExpression="RegistrationDate" >
            <ItemStyle HorizontalAlign="Center" />
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton runat="server" ID="lbRegistrationDate" CommandName="Sort" CommandArgument="RegistrationDate" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_13%>" />
                 <%# this.GetRegistrationDateSortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                 <%# Eval("RegistrationDate") %>
            </ItemTemplate>
        </asp:TemplateColumn>
        
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_14%>" SortExpression="LastUpdated" >
            <ItemStyle HorizontalAlign="Center" />
            <HeaderTemplate>
            <div style="white-space:nowrap;">
                <asp:LinkButton ID="lbSourceSort" runat="server" CommandName="Sort" CommandArgument="LastUpdated" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_14%>" />
                <%# this.GetLastUpdatedDateSortImg()%>
            </div>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("LastUpdated")%>
            </ItemTemplate>
        </asp:TemplateColumn>
       
           
       
        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_84%>">
            <HeaderStyle CssClass="RightHeader" />
            <ItemTemplate>
                <div style="white-space:nowrap;">
               <orion:LocalizableButton ID="btnDelete" 
                                runat="server" 
                                CommandName="<%# DataGrid.DeleteCommandName %>" 
				LocalizedText="Delete"
				DisplayType="Small"
                                CommandArgument='<%# Eval("ID") %>'
                                />
                <orion:LocalizableButton 
                                ID="btnEdit" 
                                runat="server" 
                                CommandName="<%# DataGrid.EditCommandName %>" 
                                CommandArgument='<%# Eval("ID") %>'
				LocalizedText="Edit"
				DisplayType="Small"
                                />
                </div>
            </ItemTemplate>
        </asp:TemplateColumn>
    </Columns>
</asp:DataGrid>

