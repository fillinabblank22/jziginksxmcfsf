using System;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Orion.Web;

public partial class Orion_TrafficAnalysis_Admin_ManageAS : Page
{
    private static readonly SolarWinds.Logging.Log myLog = new SolarWinds.Logging.Log();

    private AutonomousSystemsDALCriteria _asCriteria = new AutonomousSystemsDALCriteria();
    public static readonly string ANY_DESTINATION = "ANY_DESTINATION";
    public static readonly string CREATE_NEW = "CREATE_NEW";
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    private IAutonomousSystemsDAL _asDAL = new AutonomousSystemsDAL();

    private const string SEARCH_JS = @"
function SearchTextInitializer(textbox)
{
    if(!textbox.clicked)
    {
        textbox.value = '';
        textbox.style.color = 'black';
    }
    else
    {
        textbox.clicked = true;
    }
}
";

    #region Properties
   

    protected string SortField
    {
        get 
        {
            string sortField = "ID";
            if (!string.IsNullOrEmpty(this.Request.QueryString["SortField"]))
            {
                sortField = this.Request.QueryString["SortField"];
            }
            return sortField;
        }
    }

    protected bool SortDesc
    {
        get
        {
            string sortDirVar = this.Request.QueryString["SortDir"];
            bool sortDesc = false;
            if (!string.IsNullOrEmpty(sortDirVar))
            {
                if (sortDirVar.Trim().Equals("DESC", StringComparison.InvariantCultureIgnoreCase))
                    sortDesc = true;
            }

            return sortDesc;
        }
    }

    protected int PageSize
    {
        // we might make this configurable later
        get
        {
            return 50;
        }
    }

    protected int TotalRecords
    {
        get
        {
            return (null == ViewState["TotalRecords"]) ? 0 : (int)ViewState["TotalRecords"];
        }
        set
        {
            ViewState["TotalRecords"] = value;
        }
    }

    protected int PageNumber
    {
        get
        {
            return this.pagerTop.PageNumber;
        }
        set
        {
            this.pagerTop.PageNumber = value;
        }
    }

    /// <summary>
    /// Used for delete dialog
    /// </summary>
    private string CurrentEditedApp
    {
        get { return (null == ViewState["CurrentEditedApp"]) ? string.Empty : (string)ViewState["CurrentEditedApp"]; }
        set { ViewState["CurrentEditedApp"] = value; }
    }

    #endregion
  
    protected override void OnInit(EventArgs e)
    {

        base.OnInit(e);
     
        // add progress bars onclientclicks method:
        imgButtDelete.OnClientClick = string.Format("show_progress_bar('{0}')", dlgConfirmDelete.ClientID);

        this.Form.Attributes["autocomplete"] = "off";
       
        this.asGrid.Criteria = this._asCriteria;
        try
        {
            string pgNum = this.Request.QueryString["Page"];
            if (!string.IsNullOrEmpty(pgNum))
            {
                this.pagerTop.PageNumber = Convert.ToInt32(pgNum);
            }
        }
        catch
        {
            // do nothing; default to 1
        }

        this.lnkAddApp.Style[HtmlTextWriterStyle.MarginRight] = "10px";
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "search_js", SEARCH_JS, true);
        this.txtSearch.Attributes["onfocus"] = "SearchTextInitializer(this);";
        this.txtSearch.Style[HtmlTextWriterStyle.FontSize] = "11px";
        this.txtSearch.Style[HtmlTextWriterStyle.Height] = "13px";
        this.txtSearch.Style[HtmlTextWriterStyle.VerticalAlign] = "middle";
        this.btnSearch.Style[HtmlTextWriterStyle.VerticalAlign] = "middle";

        string searchText = this.Request.QueryString.Get("SearchString");

        this.LoadCriteria();

        if (!String.IsNullOrEmpty(searchText))
        {
            this.txtSearch.Text = searchText;
            this.txtSearch.Style[HtmlTextWriterStyle.Color] = "black";
        }

        if (!this.IsPostBack)
        {
            UpdateGrid();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    private void LoadCriteria()
    {
        if (this.SortField.Equals("ID", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.ID;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        else if (this.SortField.Equals("Name", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.Name;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        else if (this.SortField.Equals("Country", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.Country;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        else if (this.SortField.Equals("Organization", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.Organization;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        else if (this.SortField.Equals("RegistrationDate", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.RegistrationDate;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        else if (this.SortField.Equals("LastUpdated", StringComparison.InvariantCultureIgnoreCase))
        {
            this._asCriteria.SortColumn = AutonomousSystemSortColumn.LastUpdated;
            this._asCriteria.SortType = this.SortDesc ? AutonomousSystemSortStyle.Descending : AutonomousSystemSortStyle.Ascending;
        }
        if (!string.IsNullOrEmpty(this.Request.QueryString["SearchString"]))
        {
            this._asCriteria.SearchString = this.Request.QueryString["SearchString"].Trim();
        }
    }


    /// <summary>
    /// 0, 50, 100 ...
    /// </summary>
    public int RecordIndex
    {
        get
        {
            return (this.pagerTop.PageNumber - 1) * PageSize;
        }
    }

    public override void DataBind()
    {
        DataTable dt = new DataTable();
        int totalRecords = _asDAL.GetRawData(_asCriteria, PageSize, RecordIndex, out dt);
        this.TotalRecords = totalRecords;
        
        this.pagerTop.MaxPageNumber = (this.TotalRecords / this.PageSize) + 1;
        
        lblPortsText.Text = totalRecords == 0 ? 
            Resources.NTAWebContent.NTAWEBCODE_VB0_50 :
            string.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_51,
                (PageNumber - 1) * PageSize + 1,
                Math.Min(PageNumber * PageSize, TotalRecords),
                TotalRecords);

        this.asGrid.DataSource = dt;

        base.DataBind();
        
    }

    

    protected void ShowAddApplicationDialog(object sender, EventArgs e)
    {
        
        this.asEditor.EditMode = false;
        
        this.dlgAddEdit.Show();

        //update update panel
        updatePanelEdit.Update();

        this.asEditor.AsID = string.Empty;
        //this.asEditor.Clear();
    }

    protected void AddEditApplicationCancel(object sender, EventArgs e)
    {
        this.dlgAddEdit.Hide();

        this.asEditor.Clear();
        //this.Response.Redirect(this.Request.Url.ToString());
        UpdateGrid();
    }

    /// <summary>
    /// Gets Autonomous System from the dialog (ASEditor)
    /// </summary>
    /// <returns></returns>
    private AutonomousSystemItem GetAutonomousSystem()
    {
        // get Data:
        AutonomousSystemItem autSyst = new AutonomousSystemItem();
        autSyst.ID = Convert.ToInt32(asEditor.AsID);
        autSyst.Name = asEditor.Name;
        autSyst.CountryCode = asEditor.CountryCode;
        autSyst.Organization = asEditor.Organization;

        DateTime regDate = DateTime.MinValue;
        DateTime.TryParse(asEditor.RegistrationDate, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out regDate);
        autSyst.RegistrationDate = regDate;

        DateTime lastUpd = DateTime.MinValue;
        DateTime.TryParse(asEditor.LastUpdated, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out lastUpd);
        autSyst.LastUpdated = lastUpd;

        return autSyst;
    }

    protected void EditorValidateHandler(object sender, EventArgs e)
    {
        // validate input
        // here to check if AS already exists in database and if yes a dialog or message will appear
        // otherwise save
        asEditor.AsIDCollision = string.Empty;
        if (!asEditor.EditMode)
        {

            AutonomousSystemItem autSyst;
            _asDAL.GetAutonomousSystem(Convert.ToInt32(asEditor.AsID), out autSyst);

            if (autSyst == null)
            {
                EditorSubmitHandler(sender, e);
            }
            else
            {
                asEditor.AsIDCollision = autSyst.ID.ToString();
            }
        }
    }

    protected void EditorResolveHandler(object sender, EventArgs e)
    {
        
    }
    
    protected void EditorSubmitHandler(object sender, EventArgs e)
    {
        if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_VB0_52, true))
        {
            return;
        }
        
        AutonomousSystemItem autSyst = GetAutonomousSystem();

        try
        {
            _asDAL.Update(autSyst);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Exception in EditoSubmitHandler, Update: {0} ", ex);
        } 
        
        this.dlgAddEdit.Hide();
        UpdateGrid();
        this.Response.Redirect(this.Request.Url.ToString());
        //this.asEditor.Clear();
    }

    private void UpdateGrid()
    {
        this.DataBind();
        this.upMain.Update();
    }

    #region Data Grid Handlers
    protected void DataGridSortHandler(object source, DataGridSortCommandEventArgs e)
    {
        NameValueCollection newQueryString = new NameValueCollection(this.Request.QueryString);
        if (this.SortField.Equals(e.SortExpression))
        {
            // change sort order, keep same sort field
            if (this.SortDesc)
                newQueryString["SortDir"] = "ASC";
            else
                newQueryString["SortDir"] = "DESC";
        }
        else
        {
            // sort ascending, change sort field
            newQueryString["SortDir"] = "ASC";
            newQueryString["SortField"] = e.SortExpression;
        }

        newQueryString["SortField"] = e.SortExpression;
        this.DoRedirect(newQueryString);
    }

    protected void DataGridEditHandler(object source, DataGridCommandEventArgs e)
    {
        string asNum = e.CommandArgument.ToString();
        int asID = Convert.ToInt32(asNum.Split('.')[0]);

        AutonomousSystemItem autSyst = new AutonomousSystemItem();
        _asDAL.GetAutonomousSystem(asID, out autSyst);

        this.asEditor.AsID = asID.ToString();
        this.asEditor.Name = autSyst.Name;
        this.asEditor.CountryCode = autSyst.CountryCode;
        this.asEditor.Organization = autSyst.Organization;

        if (autSyst.RegistrationDate == DateTime.MinValue)
        {
            this.asEditor.RegistrationDate = string.Empty;
        }
        else
        {
            this.asEditor.RegistrationDate = autSyst.RegistrationDate.ToString("d",  CultureInfo.CreateSpecificCulture("en-US"));
        }
        
        if (autSyst.LastUpdated == DateTime.MinValue)
        {
            this.asEditor.LastUpdated = string.Empty;
        }
        else
        {
            this.asEditor.LastUpdated = autSyst.LastUpdated.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
        }
        this.asEditor.EditMode = true;

        this.dlgAddEdit.Show();

        // update content of ajax modal popup extender
        this.updatePanelEdit.Update();
    }

    protected void DataGridDeleteHandler(object source, DataGridCommandEventArgs e)
    {
        DeleteAS(e, false);

        //this.Response.Redirect(this.Request.Url.ToString());
        UpdateGrid();
    }

    private void DeleteAS(CommandEventArgs e, bool isRange)
    {
        lblDeleteHeader.Text = Resources.NTAWebContent.NTAWEBCODE_VB0_53;
        lblDeleteConfirm.Text = string.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_54, e.CommandArgument.ToString());

        // update label with text
        updatePanelDeleteConfirm.Update();
        
        // store appID to the dialog
        this.CurrentEditedApp = e.CommandArgument.ToString();
        
        // show ajax modal dialog 
        this.dlgConfirmDelete.Show();
    }
   
    #endregion

    #region Delete Confirmation Dialog Handlers
    protected void DeleteConfirmCancelHandler(object source, EventArgs e)
    {
        this.lblDeleteConfirm.Text = string.Empty;
        //this.CurrentEditedApp = "0";
        this.dlgConfirmDelete.Hide();
        // we don't need refresh page when using ajax popup modal extender
        //this.Response.Redirect(this.Request.Url.ToString());
    }

    protected void DeleteConfirmHandler(object source, EventArgs e)
    {
        // hide modal window
        this.dlgConfirmDelete.Hide();
        this.ModalProgress.Hide();

        if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_VB0_55, true))
        {
            return;
        }
        
        int asID;
        if (Int32.TryParse(this.CurrentEditedApp, out asID))
        {
            _asDAL.Delete(asID);    
        }

        UpdateGrid();
        //this.Response.Redirect(this.Request.Url.ToString());
    } 
    #endregion

    protected void PageChangeHandler(object source, EventArgs e)
    {
        NameValueCollection newQueryString = new NameValueCollection(this.Request.QueryString);
        newQueryString["Page"] = this.pagerTop.PageNumber.ToString();
        this.DoRedirect(newQueryString);
    }

    private void DoRedirect(NameValueCollection newQueryString)
    {
        string newQuery = string.Empty;
        foreach (string key in newQueryString)
        {
            newQuery = string.Format("{0}&{1}={2}", newQuery, key, newQueryString[key]);
        }
        // trim the leading ampersand
        newQuery = newQuery.Substring(1);

        Response.Redirect(this.Request.Url.GetLeftPart(UriPartial.Path) + "?" + newQuery);
    }

    public void SearchClickHandler(object sender, EventArgs e)
    {
        if (this.txtSearch.Text == null || this.txtSearch.Text.Equals(Resources.NTAWebContent.NTAWEBCODE_VB0_56, StringComparison.InvariantCultureIgnoreCase))
        {
            this.txtSearch.Text = "";
        }

        NameValueCollection col = new NameValueCollection(this.Request.QueryString);
        col["SearchString"] = this.txtSearch.Text;
        col["Page"] = "1";

        this.DoRedirect(col);
    }
}
