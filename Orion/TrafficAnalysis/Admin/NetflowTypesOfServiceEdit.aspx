<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master"
    CodeFile="NetflowTypesOfServiceEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_NetflowTypesOfServiceEdit"
    Title="<%$ Resources: NTAWebContent, NTAWEBDATA_VB0_55%>" %>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta" TagName="NtaHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="HelpButton6" runat="server" HelpUrlFragment="OrionNetFlowPHSettingsNetFlowTypesService" Title="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_56%>" />
    <style type="text/css">
        .sw-btn {margin:3px;}
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="PageHeader" id="etxxHeader" style="width: auto !important;">
        <%=Page.Title%>
    </div>
    
    <div class="ResourceWrapper" id="etxxRW" style="width: 702px;">
        <div class="etxxLH">
            <asp:DataGrid runat="server" ID="dgToS" AllowPaging="false" AllowSorting="false"
                Width="100%" CellPadding="0" CellSpacing="0" AutoGenerateColumns="false" BorderWidth="0"
                GridLines="none" OnEditCommand="EditHandler" OnCancelCommand="CancelHandler"
                OnUpdateCommand="UpdateHandler">
                <ItemStyle BackColor="White" CssClass="ntsItems" />
                <AlternatingItemStyle BackColor="#F6F6F6" />
                <HeaderStyle Font-Bold="true" CssClass="ntsHeader" />
                <Columns>
                    <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_57%>" ItemStyle-CssClass="ntsItemsLeft"
                        HeaderStyle-CssClass="ntsHeaderLeft">
                        <ItemTemplate>
                            <%# Eval("ToSName", "{0}").Trim() %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox runat="server" ID="txtToSName" Columns="10" MaxLength="16" Text='<%# Eval("ToSName", "{0}").Trim() %>' />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtToSName" EnableClientScript="false"
                                ValidationGroup="EditToS" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_60%>" />
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="DSCP" HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_58%>" ReadOnly="true"
                        HeaderStyle-CssClass="ntsHeaderCenter"
                        ItemStyle-CssClass="ntsItemsCenter"  />
                    <asp:TemplateColumn ItemStyle-HorizontalAlign="right" ItemStyle-CssClass="ntsItemsRight"
                        HeaderStyle-HorizontalAlign="right" HeaderStyle-CssClass="ntsHeaderRight">
                        <ItemTemplate>
                            <orion:LocalizableButton runat="server" Style="line-height: 13px; margin: 2px 2px 2px 0" CommandName="Edit" LocalizedText="Edit" DisplayType="Small"/>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <orion:LocalizableButton runat="server" Style="line-height: 13px; margin: 2px 2px 2px 0" CommandName="Update" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_59%>" DisplayType="Small"/>
                            <orion:LocalizableButton runat="server" Style="line-height: 13px; margin: 2px 2px 2px 0" CommandName="Cancel" LocalizedText="Cancel" DisplayType="Small"/>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        
        </div>  <%--etxxLH DIV END--%>
    </div>  <%--Resource wrapper DIV END--%>
    
    <div class="sw-btn-bar">
    <orion:LocalizableButton runat="server" ID="btnSubmit" DisplayType="Primary" LocalizedText="Submit"
        OnClick="btnSubmit_Click" ToolTip="Submit" AlternateText="Submit" style="margin-left:7px;" />
        </div>
</asp:Content>
