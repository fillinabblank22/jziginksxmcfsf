﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowCBQoSPolling.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_NetflowCBQoSPolling" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>
<%@ Import Namespace="System.Web" %>

<div class="settingsOption">
    <asp:CheckBox class="checkBox" Checked="true" runat="server" ID="chkbEnableCBQoSPolling" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_86%>" />
    <div class="checkBoxDescription" id="spanHiding" runat="server">
        <%= string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_84,
            "<span class='ntasBlueLink' style='font-size: 11px;'><a title='"+ HttpUtility.HtmlEncode(CBQoSSourcesTitle)+"' href='"+UIConsts.URL_CBQOS_SOURCES_MANAGEMENT+"'>"+HttpUtility.HtmlEncode(CBQoSSourcesTitle)+"</a></span>")%>
    </div>
</div>

<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_87%>
    <asp:TextBox ID="tbPollingInterval" runat="server" Text='300' Width="35px" MaxLength="9" />
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_88%>
       
    <asp:RequiredFieldValidator CssClass="ntasValidator" ID="customMinutesValidator" ControlToValidate="tbPollingInterval"
        runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_89%>">
        <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" class="ntasValIcon" onclick="return false;">&nbsp;</a>
        </asp:RequiredFieldValidator>
            
    <asp:RangeValidator CssClass="ntasValidator" ID="RangeValidator1" runat="server"
        ControlToValidate="tbPollingInterval" Type="integer" MaximumValue="900" MinimumValue="60"
        ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_90%>" Display="Dynamic">
	        <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_90%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_90%>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
    </asp:RangeValidator>
</div>

<br />

<div class="settingDescription" style="line-height: 18px">
    <div style="float:left; width: 17px; height:16px;">
        <img width="16" height="16" alt="Info" title="Info" src="../images/NTASettings/cbqosInfo.gif" /> 
    </div>
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_91%>
<br />
</div>

<script type="text/javascript">

    // handling checkbox with hiding text&link
    $_cb = $("[id$='chkbEnableCBQoSPolling']");
    $_span = $("[id$='spanHiding']");

    function checkCB() {
        if ($_cb.attr('checked')) {
            $_span.css("visibility", "visible");
        } else {
        $_span.css("visibility", "hidden");
        }
    }

    $_cb.click(function() {
        checkCB();
    });
    
    checkCB();
</script>