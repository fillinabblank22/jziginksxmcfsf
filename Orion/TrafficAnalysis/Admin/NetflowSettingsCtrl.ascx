﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowSettingsCtrl.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_NetflowSettingsCtrl" %>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta"
    TagName="NtaHelpButton" %>
<a name="<%=AnchorName %>"></a>
<div class="ResourceWrapper">
    <nta:NtaHelpButton ID="helpBtn" runat="server" />
    <div class="PageHeader" runat="server" id="caption">
    </div>    
    <div class="ntasDIV1">
        <img runat="server" class="ntasIMG1" id="icon" width="84" height="84" />
        <div class="ntasDIV2">
            <asp:PlaceHolder runat="server" ID="PlaceHolder1" />
            <div class="vspace"></div>
            <orion:LocalizableButton ID="imgBtn" runat="server" DisplayType="Primary" LocalizedText="Save" OnClick="SubmitClick"/>
			<div id="imageSaved" runat="server" class="sw-suggestion sw-suggestion-pass" style="display:inline"><span class="sw-suggestion-icon"></span><%= Resources.NTAWebContent.NTAWEBDATA_AK0_80%></div>
        </div>
    </div>
    <asp:PlaceHolder runat="server" ID="PlaceHolder2" />
</div>
<asp:HiddenField ID="hfSaved" runat="server"/>

<style type="text/css">
    .nfsBreadcrumb, #etxxHeader, .PageHeader { margin-left: 15px; }
    #etxxHeader { padding: 0; }
    .PageHeader { line-height: 45px; }
    .ResourceWrapper
    {
        overflow: hidden; /* fix "Charting and Graphing Settings" scrollbar in Chrome */
        margin: 0 15px 15px 15px;        
        width: 1024px;
        padding: 0;         
        box-shadow: 0 0 5px 0 var(--nui-shadow-color,rgba(17,17,17,.3));
        line-height: 18px;   
    }    
    .NtaHelpButton 
    {         
        margin-right: 15px;
        line-height: 45px;
    }    
    .ntasButton { margin-top:7px; }
    .ntasDIV1 
    { 
        background-color: var(--body-bg,#fafafa); 
        padding: 20px 20px 20px 30px; 
        border-top: 1px solid #d9d9d9;
    }
    .ntasDIV2, .ntasDIV4 { margin-left:114px; }
    .ntasDIV3 
    { 
        background-color: white; 
        padding: 20px 20px 20px 30px; 
        line-height: 20px;        
        border-top: 1px solid #d9d9d9;
    }
    .ntasDIV4 { min-height: 84px; }
    .ntasIMG1 
    { 
        display: inline;
        float: left;
    }    
    .ntasDIV2 .PeriodSelector 
    {
        font-weight:normal;
        margin-left:0;
    }
    .ntasValidator { font-size: small; font-weight:bold; margin-left:3px; clear:right; }
    .ntasValIcon 
    {
        background-image: url('/Orion/TrafficAnalysis/images/exclamation.gif'); 
        background-position:left center;
        background-repeat:no-repeat;
        padding:5px 0px 2px 20px;
        cursor: pointer; 
        width:16px;
        height:16px;
    }
    .vspace { margin-top: 10px; }

    .settingsOption, .numberUpDown, .ntasBlueLink, select, input, .ntasDIV2, .ntasDIV4 { font-size: 13px; }

    .settingsOption, .numberUpDown
    {
        font-weight: 400;
        color: #111;
        padding: 5px 0 5px 0;
    }
    .checkBox, .checkBoxDescription 
    { 
        padding-left: 23px;
        display: block; 
    }
    .checkBox input 
    {
        float:left; 
        margin: 3px 10px 3px -23px;
    }
    .checkBox label 
    {
        padding: 0;
        vertical-align: top;
    }
    .checkBoxDescription, .settingDescription 
    {
        line-height: 14px;
        font-size: 11px; 
        font-weight: 400;
        color: #888;
    }
    .numberUpDown .textBefore, .numberUpDown .textAfter { font-weight: 400 !important; }
    .numberUpDown { min-height: 18px; }
    .ntasBlueLink { padding-top: 13px; }

    .ntasBlueLink a, .ntasBlueLink a:hover, .ntasBlueLink a:visited,  .ntasBlueLink a:link, 
    .ntasBlueLink a:active { color: #0079AA; text-decoration:none; }
    .ntasBlueLink a:hover { text-decoration:underline; }

    hr 
    {
        margin: 9px 0 9px 0;
        border: none;
    }

    select, input 
    {
        font-family: 'Open Sans', Arial, Helvetica, Sans-Serif;
        font-weight: 400;
        color: #111;
        vertical-align: baseline;
    }
</style>


<script type="text/javascript">
    $(document).ready(function() {

        var $_hf = $("input[id='<%=this.hfSaved.ClientID %>']");
        var $_savedIcon = $("#<%=this.imageSaved.ClientID %>");

        if ($_hf.val() == "1") {
            $_hf.val("0"); // reset hidden field
            $_savedIcon.fadeOut(2000);
        }


        $("a[id$='imgBtn']").click(function() {
            if (typeof ValidateJS == 'function') {
                ValidateJS(this);
            }
        });

    });
</script>
