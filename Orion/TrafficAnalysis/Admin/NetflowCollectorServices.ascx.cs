using System;
using System.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;

public partial class Orion_TrafficAnalysis_Admin_NetflowCollectorServices : UserControl
{
    private static IDALProvider _dalProvider = DALProvider.GetInstance();

    protected override void OnInit(EventArgs e)
    {
        collectorRepeater.DataSource = _dalProvider.GetDAL<INetflowCollectorServicesDAL>().GetCollectorServers();
        collectorRepeater.DataBind();
        base.OnInit(e);
    }

}
