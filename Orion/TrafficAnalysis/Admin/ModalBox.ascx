<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModalBox.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_ModalBox" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<style type="text/css">
.UpdateProgressPanel
{
       z-index: 1000;
       width: 200px;
       text-align: center;
       vertical-align: middle;
       position: fixed;
       bottom: 50%;
       left: 45%;
       padding: 10px;
}

/*background-color:Silver;*/
/*border: solid 2px White;*/

#overlay 
{
    z-index: 990;
    background-color: Silver;
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    min-height: 100%;
    min-width: 100%;
    filter: alpha(opacity=20);
    opacity: 0.2;
    -moz-opacity: 0.2;
 }

</style>

<asp:UpdatePanel runat="server" ID="upModalBox" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div runat="server" id="divContainer" style="display: none;">
            <div runat="server" id="divMask">
                &nbsp;
            </div>
            <div style="position: absolute; top: 20%; left: 0px; z-index: 100; width: 100%;">
                <div runat="server" id="divDialog" style="margin-left: auto; margin-right: auto;
                    width: 350px; height: auto; background-color: White;">
                    <asp:PlaceHolder runat="server" ID="phContents" />
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
    <div id="overlay"></div>
                <div class="UpdateProgressPanel">
                   <img src="/Orion/images/loading_gen_small.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_36%>" />
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_124%>
                </div>
    </ProgressTemplate>
</asp:UpdateProgress>
