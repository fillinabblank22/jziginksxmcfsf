﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Orion.Web.InformationService;
using SolarWinds.Netflow.Utils;
using System.Net;
using SolarWinds.Logging;
using System.Globalization;
using System.Threading;
using System.Data;
using SolarWinds.Netflow.Common.Swis;
using System.Web.Services;
using SolarWinds.Interfaces.Common.Models.Discovery;

public partial class Orion_TrafficAnalysis_Admin_AddInterfaceIntoNPM : EditPageBase
{
    private static readonly Log log = new Log();
    private static IList<Tuple<int, string>> speedUnits = new List<Tuple<int, string>>
    {
        new Tuple<int, string>((int)DataUnitMagnitude.NoMagnitude, Resources.NTAWebContent.NTAWEBDATA_MM0_19),
        new Tuple<int, string>((int)DataUnitMagnitude.Kilo, Resources.NTAWebContent.NTAWEBDATA_MM0_20),
        new Tuple<int, string>((int)DataUnitMagnitude.Mega, Resources.NTAWebContent.NTAWEBDATA_MM0_21),
        new Tuple<int, string>((int)DataUnitMagnitude.Giga, Resources.NTAWebContent.NTAWEBDATA_MM0_22),
    };

    /// <summary>
    /// ID of node passed in query string parameter NodeID
    /// </summary>
    private int NodeId { get; set; }

    private static KeyValueCache<int, DiscoveredLiteInterface[]> discoveredInterfacesOnNodeCache = new KeyValueCache<int, DiscoveredLiteInterface[]>(TimeSpan.FromMinutes(2));
    
    /// <summary>
    /// Caption of node with ID passed in query string parameter NodeID.
    /// If this caption is unavailable node's ID is returned
    /// </summary>
    private string NodeCaption
    {
        get
        {
            return nodeNameLbl.Text;
        }
        set
        {
            nodeNameLbl.Text = value;
            addInterfaceTitleLi.InnerText = string.Format(Resources.NTAWebContent.NTAWEBDATA_MM0_3, NodeCaption);
        }
    }

    private string InterfaceCaption
    {
        get
        {
            return IsManageable ? interfaceNameLt.Text : interfaceNameTb.Text;
        }
        set
        {
            if (IsManageable)
                interfaceNameLt.Text = WebSecurityHelper.HtmlEncode(value);
            else
                interfaceNameTb.Text = value;
        }
    }

    private int InterfaceIndex
    {
        get
        {
            return Convert.ToInt32(interfaceIndexLt.Text);
        }
        set
        {
            interfaceIndexLt.Text = value.ToString();
        }
    }

    private string InterfaceType
    {
        get
        {
            return interfaceTypeLt.Text;
        }
        set
        {
            interfaceTypeLt.Text = WebSecurityHelper.HtmlEncode(value);
        }
    }

    private double InterfaceSpeed
    {
        get
        {
            return (double)Math.Floor((double.Parse(this.interfaceSpeedTb.Text) * Math.Pow(10, 3d * int.Parse(this.interfaceSpeedUnitsDdl.SelectedValue))));
        }
        set
        {

            this.interfaceSpeedTb.Text = value.ToString();
            BitsPerSecond bps = new BitsPerSecond(value);
            this.interfaceSpeedLt.Text = bps.ToString();
        }
    }


    /// <summary>
    /// Returns bool value based on query string parameter Manageable
    /// </summary>
    private bool IsManageable { get; set; }

    /// <summary>
    /// The Uri to return to after interface was added successfuly or not
    /// </summary>
    private Uri Referrer
    {
        get
        {
            if (ViewState["previousPageReferrer"] == null)
                return new Uri(ResolveUrl("~/Orion/TrafficAnalysis/SummaryView.aspx"), UriKind.Relative);

            return new Uri((string)ViewState["previousPageReferrer"], UriKind.RelativeOrAbsolute);
        }
        set
        {
            if (value != null)
                ViewState["previousPageReferrer"] = value.ToString();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        //Get NodeID from query string
        int nodeID;
        if (!int.TryParse(this.Request.QueryString["NodeID"], out nodeID))
        {
            Func<CultureInfo, string> getMessage = (culture) => { return Resources.NTAWebContent.ResourceManager.GetString("NTAWEBDATA_MM0_13", culture); };
            log.Error(getMessage(CultureInfo.InvariantCulture));
            throw new ArgumentException(getMessage(Thread.CurrentThread.CurrentUICulture));
        }
        NodeId = nodeID;

        //Get Interface index from query string
        int interfaceIndex;
        if (!int.TryParse(this.Request.QueryString["InterfaceIndex"], out interfaceIndex))
        {
            Func<CultureInfo, string> getMessage = (culture) => { return Resources.NTAWebContent.ResourceManager.GetString("NTAWEBDATA_MM0_14", culture); };
            log.Error(getMessage(CultureInfo.InvariantCulture));
            throw new ArgumentException(getMessage(Thread.CurrentThread.CurrentUICulture));
        }
        InterfaceIndex = interfaceIndex;

        //Get manageable parameter from query string
        IsManageable = (this.Request.QueryString["Manageable"] != null && this.Request.QueryString["Manageable"] == "1");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        subTitle.Text = IsManageable ? Resources.NTAWebContent.NTAWEBDATA_MM0_50 : Resources.NTAWebContent.NTAWEBDATA_MM0_2;
        
        //Check if user has admin rights
        if (!LicenseHelper.IsAllowed(Profile.AllowAdmin))
        {
            ShowModalMessage(Resources.NTAWebContent.NTAWEBCODE_TM0_87, true);
        }

        if (!this.IsPostBack)
        {
            // store the page the user came from
            Referrer = Request.UrlReferrer;

            this.interfaceSpeedUnitsDdl.DataSource = speedUnits;
            this.interfaceSpeedUnitsDdl.DataValueField = "Item1";
            this.interfaceSpeedUnitsDdl.DataTextField = "Item2";
            this.interfaceSpeedUnitsDdl.DataBind();
            this.interfaceSpeedUnitsDdl.Items.FindByValue(((int)DataUnitMagnitude.Mega).ToString()).Selected = true;

            this.interfaceSpeedPosIntegerValidator.MinimumValue = "0";
                        
            //Check if node / interface exists and fill UI
            if (CheckIfNodeExistsAndFillUI())
                CheckIfInterfaceDoesntExistAndFillValuesOnUI();
            
        }

        //Set visibility of edits / fields based on Manageable / unmanageable attribute
        this.interfaceNameReadOnlyTd.Visible = IsManageable;
        this.interfaceNameEditTd.Visible = !IsManageable;
        this.interfaceTypeTr.Visible = IsManageable;
        this.interfaceSpeedEditTd.Visible = !IsManageable;
        this.interfaceSpeedReadonlyTd.Visible = IsManageable;
        this.speedDiv.Visible = !IsManageable;
        this.speedCategory.Visible = !IsManageable;
        this.interfaceSpeedHint.Visible = !IsManageable;


        // sets the validator maximum bps thact the DB can handle (ie. 32bit value), the actual value is lower based on selected unit/rate magnitude
        //999 999 999 999 999 (999 999 Gbps)
        this.interfaceSpeedPosIntegerValidator.MaximumValue = (999999999999999 / Math.Pow(10, 3d * int.Parse(this.interfaceSpeedUnitsDdl.SelectedValue))).ToString();
        this.interfaceSpeedPosIntegerValidator.MinimumValue = "1";
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    /// <summary>
    /// Determines if node exists and if not, registers JS displaying error message
    /// </summary>
    /// <returns>true if node exists</returns>
    private bool CheckIfNodeExistsAndFillUI()
    {
        using (var swql = InformationServiceProxy.CreateV3())
        {
            var table = swql.Query("select Caption from Orion.Nodes where NodeID=@nodeId",
                                   new Dictionary<string, object>()
                                   {
                                       { "nodeId", NodeId }
                                   });

            if (table.Rows.Count > 0)
            {
                this.NodeCaption = table.Rows[0]["Caption"].ToString();
                return true;
            }
            else
            {
                ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_MM0_23, true);
                return false;
            }
        }
    }

    /// <summary>
    /// Determines if interface exists and if it does, registers JS displaying error message
    /// </summary>
    /// <returns>true if interface doesn't exist</returns>
    private bool CheckIfInterfaceDoesntExistAndFillValuesOnUI()
    {
        using (var swql = InformationServiceProxy.CreateV3())
        {
            // subtype 0 is normal interface, subtype 2 is unmanageble interface
            var table = swql.Query("select InterfaceIndex, InterfaceCaption, InterfaceSpeed, InterfaceTypeDescription from Orion.NPM.Interfaces where NodeID=@nodeId and InterfaceIndex=@ifIndex and InterfaceSubType in (0, 2)",
                                   new Dictionary<string, object>()
                                   {
                                       { "nodeId", NodeId },
                                       { "ifIndex", InterfaceIndex }
                                   });


            if (table.Rows.Count > 0)
            {  //Interface already exists in DB, just show message and redirect user back to refferer page
                DataRow row = table.Rows[0];

                this.InterfaceCaption = row["InterfaceCaption"].ToString();
                this.InterfaceSpeed = Convert.ToDouble(row["InterfaceSpeed"]);
                this.InterfaceType = row["InterfaceTypeDescription"].ToString();

                //Show message                
                ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_MM0_24, true);
                

                return false;
            }
            else
            {   //Interface doesn't exist, we can continue
                if (IsManageable)
                {                                    
                    DiscoveredLiteInterface discoveredInterface = DiscoverInterfaceOnNode(NodeId, InterfaceIndex);
                    if (discoveredInterface != null)
                    {
                        this.InterfaceCaption = discoveredInterface.Caption;
                        this.InterfaceSpeed = 0; //Will be replaced by real value by NPM during NPM's polling
                        this.InterfaceType = discoveredInterface.ifType == 0 ? this.InterfaceType.ToString() : SolarWinds.Orion.Core.Common.DALs.DiscoveryDatabaseDAL.GetInterfaceTypeDescription(discoveredInterface.ifType);
                    }
                    else
                    {
                        ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_OS0_500, true);
                    }
                }
                else
                {
                    this.InterfaceCaption = string.Format(Resources.NTAWebContent.NTAWEBDATA_MM0_12, InterfaceIndex);
                }

                return true;
            }
        }
    }

    private DiscoveredLiteInterface DiscoverInterfaceOnNode(int nodeId, int interfaceIndex)
    {
        DiscoveredLiteInterface[] interfaces;
        if (!discoveredInterfacesOnNodeCache.TryGetValue(nodeId, out interfaces))
        {
            using (var swql = InformationServiceProxy.CreateV3())
            {
                interfaces = NpmInterfaces.DiscoverInterfacesOnNode(swql.Invoke, nodeId);
                discoveredInterfacesOnNodeCache.StoreValue(nodeId, interfaces);
            }
        }
        
        foreach (DiscoveredLiteInterface interf in interfaces)
        {
            if (interf.ifIndex == interfaceIndex)
                return interf;
        }

        return null;
    }


    /// <summary>
    /// OnClick event handler - adds interface to DB through SWIS
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void AddInterface_Click(object sender, EventArgs e)
    {
        if (!IsValid) // page is not valid so
            return; // and dont add the interface

        log.InfoFormat("Adding interface into NPM. NodeID: {0}, InterfaceIndex: {1}, InterfaceName: {2}, InterfaceSpeed: {3}",
            NodeId, InterfaceIndex, InterfaceCaption, InterfaceSpeed);


        var interfaceToBeAdded = new DiscoveredLiteInterface();
        interfaceToBeAdded.ifIndex = InterfaceIndex;
        interfaceToBeAdded.Caption = InterfaceCaption;
        interfaceToBeAdded.ifSpeed = InterfaceSpeed;
        interfaceToBeAdded.Manageable = IsManageable;

        LiteDiscoveryResult ldr = null;
        using (var swql = InformationServiceProxy.CreateV3())
        {
            AddPollers pollers = IsManageable ? AddPollers.AddDefaultPollers : AddPollers.AddNoPollers;
            try
            {
                if (CheckIfNodeExistsAndFillUI())
                    ldr = NpmInterfaces.AddInterfacesToNpm(swql.Invoke, NodeId, new DiscoveredLiteInterface[] { interfaceToBeAdded }, pollers);
                else
                {
                    log.ErrorFormat("Failed to add interface [index:{0}] on node [ID:{1}]. Node does not exist.", InterfaceIndex, NodeId);
                    return;
                }
            }
            catch (System.ServiceModel.FaultException<SolarWinds.InformationService.Contract2.InfoServiceFaultContract> ex)
            {
                throw new InvalidOperationException("Error while adding interface into NPM: " + ex.Detail.Message);
            }
        }

        if (ldr.Result == LiteDiscoveryResultCode.Succeed)
        {
            //Referrer = new Uri(ResolveUrl(string.Format("~/Orion/TrafficAnalysis/NetflowInterfaceDetails.aspx?NetObject=NI:{0}", ldr.DiscoveredInterfaces[0].InterfaceID)), UriKind.Relative);
            log.InfoFormat("Interface has been succesfully added.");
            ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_MM0_10, true);            
        }
        else if (ldr.Result == LiteDiscoveryResultCode.InvalidNode)
        {
            log.ErrorFormat("Failed to add interface [index:{0}] on node [ID:{1}]. SWIS method AddInterfacesOnNode returned LiteDiscoveryResultCode.InvalidNode", InterfaceIndex, NodeId);
            ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_MM0_17, false);
        }
        else
        {
            log.ErrorFormat("Failed to add interface [index:{0}] on node [ID:{1}].", InterfaceIndex, NodeId);
            ShowModalMessage(Resources.NTAWebContent.NTAWEBDATA_MM0_18, false);
        }
    }

    /// <summary>
    /// OnClick event handler - returns the user to the page he came from
    /// or if that is unavailable/invalid to NTA summary view
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ReturnToRefferer_Click(object sender, EventArgs e)
    {
        Response.Redirect(Referrer.ToString());
    }
 
    
    private void ShowModalMessage(string message, bool returnToReferrer)
    {
        this.messageBoxModalText.Text = message;
        if (returnToReferrer)
        {
            messageBoxModalRedirectButton.Visible = true;
            messageBoxModalStayButton.Visible = false;
        }
        else
        {
            messageBoxModalRedirectButton.Visible = false;
            messageBoxModalStayButton.Visible = true;
        }

        string dialogJs = @"<script type='text/javascript'>
                                            $(document).ready(function () {
                                                $('#messageBoxModal').dialog({ draggable: false, modal: true, resizable: false, closeOnEscape: false });
                                                $('[aria-labelledby$=messageBoxModal]').find('.ui-dialog-titlebar').hide();
                                            });
                                            </script>";

        ClientScript.RegisterClientScriptBlock(this.GetType(), "modalDialog", dialogJs);
    }
}
