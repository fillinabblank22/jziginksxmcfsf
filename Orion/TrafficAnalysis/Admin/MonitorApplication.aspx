﻿<%@ Page Title="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_69%>" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="MonitorApplication.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_MonitorApplication" %>
<%@ Register TagPrefix="nta" TagName="AppEditor" Src="~/Orion/TrafficAnalysis/Utils/AppEditor.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="stylesContent" ContentPlaceHolderID="ResourcesStyleSheetPlaceHolder" runat="server" >
    <orion:Include ID="IncludeNetFlowCss" runat="server" Module="TrafficAnalysis" File="NetFlow.css" />
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {
            <%= this.GetMonitorAppScript() %>         
        });
    
    </script>

    <div id="unmonitoredStuff" runat="server" visible="false">
        <asp:HiddenField ID="HiddenButtonEdit" runat="server" />
        <asp:Panel ID="PanelEdit" runat="server" Width="733px">
            <nta:AppEditor 
                runat="server" ID="appEditor" 
                Description="Description" EditMode="true"
                SinglePortMode="false" FromResource="true" FromResourceAndOnDedicatedPage="true"
                OnCancel="appEditor_Cancel" OnEditSuccess="appEditor_Submit" />
        </asp:Panel>
    </div>
</asp:Content>