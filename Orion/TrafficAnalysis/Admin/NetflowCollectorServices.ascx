<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowCollectorServices.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_NetflowCollectorServices" %>

<table border="0" cellPadding="2" cellSpacing="0" width="500" style="clear:both;" class="NeedsZebraStripes">
<asp:Repeater Runat="server" ID="collectorRepeater" EnableViewState="false">
  <HeaderTemplate>
    <thead>
       <tr>
          <td class="ReportHeader" width="20">&nbsp;</td>
          <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_23%></td>
          <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_24%></td>
          <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_25%></td>
       </tr>
    </thead>
  </HeaderTemplate>
 
  <ItemTemplate>
     <tr>
		<td class="Property" width="20">
		    <%#Convert.ToInt32(Eval("StatusTime")) < 15 ? "<img name='up' src='/NetPerfMon/images/small-up.gif'> " : " <img name='down' src='/NetPerfMon/images/small-down.gif'>"%>
		</td>
		<td class="Property"><%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("ServerName").ToString()) %>&nbsp;</td>
		<td class="Property"><%#Convert.ToInt32(Eval("StatusTime")) < 15 ? Resources.NTAWebContent.NTAWEBCODE_TM0_11 : Resources.NTAWebContent.NTAWEBCODE_TM0_12 %></td>
		<td class="Property"><%#Eval("NetflowPort")%>&nbsp;</td>
	</tr>
    
  </ItemTemplate>
</asp:Repeater>

</table> 