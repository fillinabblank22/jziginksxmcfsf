using System;
using System.Web.UI;

public partial class Orion_TrafficAnalysis_Admin_ModalBox : UserControl
{
    public override string ClientID
    {
        get
        {
            return this.divContainer.ClientID;
        }
    }

    private ITemplate _dialogContents;

    [PersistenceMode(PersistenceMode.InnerProperty)]
    [TemplateInstance(TemplateInstance.Single)]
    public ITemplate DialogContents
    {
        get {return _dialogContents;}
        set {_dialogContents = value;}
    }
    
    public bool ProgressVisible
    {
        get
        {
            return UpdateProgress1.Visible;
        }
        set
        {
            UpdateProgress1.Visible = value;
        }
    }


    public int Width
    {
        get 
        {
            string widthStr = this.divDialog.Style[HtmlTextWriterStyle.Width];
            if (string.IsNullOrEmpty(widthStr))
            {
                return 350;
            }
            
            //else
            return Convert.ToInt32(widthStr.Substring(0, widthStr.Length - 2));
        }
        set 
        {
            this.divDialog.Style[HtmlTextWriterStyle.Width] = value.ToString() + "px";
        }
    }
	

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.divMask.Attributes["style"] = MASK_STYLE;

        if (null != this.DialogContents)
        {
            this.DialogContents.InstantiateIn(this.phContents);
        }
    }

    private void CreateContents()
    {

    }

    public void Hide()
    {
        this.divContainer.Attributes["style"] = "display: none;";
        this.upModalBox.Update();
    }

    public void Show()
    {        
        this.divContainer.Attributes["style"] = "";
        this.upModalBox.Update();
    }

    private const string MASK_STYLE = @"
position: absolute; 
left: 0px;
top: 0px;
width: 100%;
height: 100%;
background-color: Black;
opacity: 0.5;
-moz-opacity: 0.5;
filter: alpha(opacity=50);
z-index: 50;
";
}
