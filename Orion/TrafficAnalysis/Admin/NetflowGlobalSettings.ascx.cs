using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using NetFlowCollectorServicesControl = Orion_TrafficAnalysis_Admin_NetflowCollectorServices;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

public partial class Orion_TrafficAnalysis_Admin_NetflowGlobalSettings : UserControl, INetFlowSettings
{
    protected override void OnInit(System.EventArgs e)
    {
        base.OnInit(e);
        if (!IsPostBack)
        {
            SetUI();
        }

        try
        {
            var control = LoadControl("~/Orion/TrafficAnalysis/Admin/NetflowCollectorServices.ascx") as NetFlowCollectorServicesControl;
            ntaCollectorPlaceHolder.Controls.Add(control);
        }catch(Exception exception)
        {
            ExceptionWarning.DataSource = GetErrorInspectedDetails(exception);          
            ExceptionWarning.Visible = true;
            manageCollectorsLink.Visible = false;
        }
    }

    public string UnmonitoredPortStats
    {
        get
        {
            // Should be a repeater
            int overall = 0;
            int unmonitored = 0;
            DateTime lastUpdate = DateTime.MinValue;
            IList<UnmonitoredPortStruct> stats = StatsDAL.GetUnmonitoredPortStats();

            if (stats == null || stats.Count == 0)
                return Resources.NTAWebContent.NTAWEBCODE_AK0_44;

            foreach (UnmonitoredPortStruct stat in stats)
            {
                overall += stat.MonitoredPDUCount + stat.UnmonitoredPDUCount;
                unmonitored += stat.UnmonitoredPDUCount;
                lastUpdate = DateTime.Compare(lastUpdate, stat.LastUpdate) > 0 ? lastUpdate : stat.LastUpdate;
            }

            return string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_43, unmonitored, overall, lastUpdate);
        }
    }

    public bool ShowSecondPart { get; set; }

    public void SetUI()
    {
        this.DivFirst.Visible = true;
        this.DivSecond.Visible = false;

        if (ShowSecondPart == true)
        {
            this.DivFirst.Visible = false;
            this.DivSecond.Visible = true;
        }

        // Enable automatic addition of NetFlow sources
        chkbAddSources.Checked = GlobalSettingsDALCached.InstanceInterface.AutoAddSources;

        // Allow monitoring of flows from unmonitored ports
        chkbRetainUnmonitoredData.Checked = GlobalSettingsDALCached.RetainUnmonitoredPortsData;
        lblRetainDescription.Text = UnmonitoredPortStats;

        // Allow monitoring of flows from unmanaged interfaces
        chkbAllowUnmIntfMonitor.Checked = GlobalSettingsDALCached.AllowMonitorUnmanagedInterfaces;

        // Match nodes also by IP addresses from the table NodeIPAddress, otherwise only by IP Address from Nodes table is flow matched.
        chkbMatchNodeByMoreIpAddresses.Checked = GlobalSettingsDALCached.MatchNodeByMoreIpAddresses;

        // Show Notification bar for UTN events.
        chkbShowNotificationBarForUTN.Checked = GlobalSettingsDALCached.ShowNotificationBarForUTN;

        // Enable processing of IPv6 flow data.
        chkbEnableProcessingOfIPv6Flows.Checked = GlobalSettingsDALCached.InstanceInterface.IPv6FlowProcessingEnabled;

        // Process flow data from Meraki MX older than MX 15.14
        chkbMerakiMxInterfaceMapping.Checked = GlobalSettingsDALCached.InstanceInterface.MerakiMxInterfaceMapping;
    }

    public void StoreSettings()
    {
        GlobalSettingsDALCached.InstanceInterface.AutoAddSources = chkbAddSources.Checked;
        GlobalSettingsDALCached.RetainUnmonitoredPortsData = chkbRetainUnmonitoredData.Checked;
        GlobalSettingsDALCached.AllowMonitorUnmanagedInterfaces = chkbAllowUnmIntfMonitor.Checked;
        GlobalSettingsDALCached.MatchNodeByMoreIpAddresses = chkbMatchNodeByMoreIpAddresses.Checked;
        GlobalSettingsDALCached.ShowNotificationBarForUTN = chkbShowNotificationBarForUTN.Checked;
        GlobalSettingsDALCached.InstanceInterface.IPv6FlowProcessingEnabled = chkbEnableProcessingOfIPv6Flows.Checked;
        GlobalSettingsDALCached.InstanceInterface.MerakiMxInterfaceMapping = chkbMerakiMxInterfaceMapping.Checked;
    }

    private ErrorInspectorDetails GetErrorInspectedDetails(Exception ex)
    {
        var errorDetails = (ErrorInspector.Inspect(Context, ex, ErrorInspector.CoreUnhandledWebsiteInspector()) ??
                                ErrorInspector.Inspect(Context, ex.InnerException, ErrorInspector.CoreUnhandledWebsiteInspector())) ??
                                new ErrorInspectorDetails { Error = ex };
        OrionErrorPageBase.CacheError(Context, errorDetails);
        return errorDetails;
    }

    #region INetFlowSettings Members


    public string Icon
    {
        get { return "/Orion/TrafficAnalysis/images/NTASettings/protocol.svg"; }
    }

    public string Caption
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK0_45; }
    }

    public string Redirect
    {
        get { return "Global"; }
    }

    #endregion

    #region INetFlowSettings Members


    public string Help
    {
        get { return "OrionNetFlowPHSettingsGlobal"; }
    }

    #endregion

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        // No host services are consumed
    }
}
