<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowDNSSettings.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_NetflowDNSSettings" %>
<%@ Register Src="../Utils/NumberUpDownControl.ascx" TagName="NumberUpDownControl"
    TagPrefix="uc1" %>

<div class="settingsOption">
    <asp:CheckBox class="checkBox" runat="server" ID="chkbDnsResolverUseNetbiosFunction" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_95%>" />
</div>
<div class="settingsOption">
    <asp:CheckBox class="checkBox" runat="server" ID="chkbDnsOnDemandEnabled" Text="<%$ Resources:NTAWebContent,DnsOnDemandEnabledLabel%>" />
</div>
<div class="settingsOption">
    <asp:CheckBox class="checkBox" runat="server" ID="chkbDnsPersistEnabled" Text="<%$ Resources:NTAWebContent,DnsPersistentEnabledLabel%>" />
    <div class="checkBoxDescription">
        <%= Resources.NTAWebContent.DnsPersistentEnabledExplanation %>
    </div>
</div>
<div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_94%>" href="<%=LearnMoreLink %>" target="_blank" rel="noopener noreferrer"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_94%></a></div>
<hr />
<uc1:NumberUpDownControl ID="numUpDnCacheExpirationDays" MinimumValue="1" MaximumValue="30"
  TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_96%>"  runat="server" />

<uc1:NumberUpDownControl ID="numUpDnCacheExpirationDaysNotResolved" MinimumValue="1" MaximumValue="30" 
    TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_97%>" runat="server" ValidatorText="Invalid" />

<uc1:NumberUpDownControl ID="numUpDnRowsToFetchForNameResolutionCount" MinimumValue="1" MaximumValue="1000"
    TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_98%>" TextBoxWidth="4em" runat="server" />

<uc1:NumberUpDownControl ID="numUpDnNameResolverDelay" MinimumValue="1" MaximumValue="60000"
    TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_99%>" TextBoxWidth="4em" runat="server" />
<hr />
<div class="settingsOption">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_92%>
</div>
<div class="settingsOption">
    <asp:RadioButton ID="rbMinutesToBailout" GroupName="MyGroup2" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_100%>" runat="server" onclick="EnableCustomMinuteTxtBox()" />
    
    <asp:TextBox ID="tbCustomMinutes" runat="server" Text='<%#MinutesBeforeBailout%>' Width="40px" MaxLength="9" />

    <asp:RequiredFieldValidator CssClass="ntasValidator" ID="customMinutesValidator" ControlToValidate="tbCustomMinutes"
                                runat="server" Display="Dynamic" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_102%>">
        <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" class="ntasValIcon" onclick="return false;"></a>
    </asp:RequiredFieldValidator>
        
    <asp:RegularExpressionValidator CssClass="ntasValidator" ID="RegularExpressionValidator1" ControlToValidate="tbCustomMinutes"
                                    ValidationExpression="^\b\d+\b$" ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_102%>"
                                    Display="Dynamic" runat="server"><a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_103%>" alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_103%>" onclick="return false;" class="ntasValIcon">&nbsp;</a></asp:RegularExpressionValidator>  <br />

    <div class="checkBoxDescription">
        <%# string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_101, CurrentExpiredNumber)%>
    </div>

    </div>
<div class="settingsOption">
    <asp:RadioButton ID="rbNeverBailout" GroupName="MyGroup2" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_104%>" runat="server" onclick="EnableCustomMinuteTxtBox()" /><br />
    <div class="checkBoxDescription">
        <%# string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_104D, CurrentExpiredNumber)%>
    </div>
</div>

<script type="text/javascript">
 function EnableCustomMinuteTxtBox() {
        txtBox = document.getElementById('<%=tbCustomMinutes.ClientID %>');
        rb = document.getElementById('<%=rbMinutesToBailout.ClientID %>');

        if (txtBox != null && rb != null) {

            if (rb.checked) {
                txtBox.disabled = false;
                txtBox.value = "15";
            }
            else {
                txtBox.value = "0";
                txtBox.disabled = true;
            }
        }

        return true;
    }
</script>
