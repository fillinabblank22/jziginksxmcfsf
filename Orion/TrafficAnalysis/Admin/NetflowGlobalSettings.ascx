<%@ Import Namespace=SolarWinds.Netflow.DAL %>
<%@ Register TagPrefix="netflow" TagName="NetflowCollectorServices" Src="~/Orion/TrafficAnalysis/Admin/NetflowCollectorServices.ascx" %>
<%@ Import Namespace="SolarWinds.Orion.Web" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowGlobalSettings.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_NetflowGlobalSettings" %>
              
        <div id="DivFirst" runat="server" visible="false">
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbAddSources" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_53%>" />
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbRetainUnmonitoredData" onclick="RetainUnmonitoredDataConfirmation(this)" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_54%>" />
                <div class="checkBoxDescription"><asp:Label runat="server" ID="lblRetainDescription" /></div>
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbAllowUnmIntfMonitor" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_57%>" />
                <div class="checkBoxDescription"><%=Resources.NTAWebContent.NTAWEBDATA_AK0_57D %></div>
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbMatchNodeByMoreIpAddresses" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_7%>" />
                <div class="checkBoxDescription"><%=Resources.NTAWebContent.NTAWEBDATA_OSC_7D %></div>
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbShowNotificationBarForUTN" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_71%>" />
                <div class="checkBoxDescription"><%=Resources.NTAWebContent.NTAWEBDATA_OSC_71D %></div>
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbEnableProcessingOfIPv6Flows" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_GlobalSettings_EnableProcessingOfIPv6Flows%>" />
                <div class="checkBoxDescription"><%=Resources.NTAWebContent.NTAWEBDATA_GlobalSettings_EnableProcessingOfIPv6FlowsDescription %></div>
            </div>
            <div class="settingsOption">
                <asp:CheckBox class="checkBox" runat="server" ID="chkbMerakiMxInterfaceMapping" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_GlobalSettings_MerakiMxInterfaceMapping%>" />
                <div class="checkBoxDescription"><%=Resources.NTAWebContent.NTAWEBDATA_GlobalSettings_MerakiMxInterfaceMappingDescription %></div>
            </div>
            <div class="ntasBlueLink">&#187;&nbsp;<a title="<%=Resources.NTAWebContent.NTAWEBDATA_OSC_4 %>" href="/Orion/TrafficAnalysis/UnknownTrafficEvents.aspx"><%=Resources.NTAWebContent.NTAWEBDATA_OSC_5 %></a>
            </div>
        </div>
        
        <div id="DivSecond" runat="server" visible="false">
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/application-server.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_58%>" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_58%>" />
            <div class="ntasDIV4">
                <div class="ntasBlueLinkBig"><a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_58%>" href="/Orion/TrafficAnalysis/Admin/ManageApps.aspx">
                    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_58%></a></div>
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_60%>" href="/Orion/TrafficAnalysis/Admin/ManageApps.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_59%></a> </div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/autonomous-system.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_61%>" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_61%>" />
            <div class="ntasDIV4">
                <div class="ntasBlueLinkBig"><a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_61%>" href="/Orion/TrafficAnalysis/Admin/ManageAS.aspx">
                    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_61%></a></div>
                    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_62%><br />
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_63%>" href="/Orion/TrafficAnalysis/Admin/ManageAS.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_63%></a> </div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/IPs.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_64%>" />
            <div class="ntasDIV4">
                    <div class="ntasBlueLinkBig">
                        <a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_64%>" href="<%= UIConsts.URL_IPGROUPS_MANAGEMENT %>"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_64%></a></div>
                    <%= string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_65, string.Format("<span class='ntasBlueLink'><a title='{0}' href='/Orion/TrafficAnalysis/Admin/ManageApps.aspx'>", Resources.NTAWebContent.NTAWEBDATA_AK0_60), "</a></span>")%>
                <br />
                <div class="ntasBlueLink">
                    &#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_66%>" href="<%= UIConsts.URL_IPGROUPS_MANAGEMENT %>"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_66%></a>
                </div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/protocol.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_61%>" />
            <div class="ntasDIV4">
                    <div class="ntasBlueLinkBig"><a title="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_61%>" href="/Orion/TrafficAnalysis/Admin/TransportProtocolsEdit.aspx">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_61%></a></div>
                    
                <%= string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_68, TransportProtocolsDAL.SelectedCount())%>
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_69%>" href="/Orion/TrafficAnalysis/Admin/TransportProtocolsEdit.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_70%></a> </div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/sources.svg"
                alt="<%= Resources.NTAWebContent.NTA_Settings_ManageFlowSources%>" />
            <div class="ntasDIV4">
                    <div class="ntasBlueLinkBig">
                        <a title="<%= Resources.NTAWebContent.NTA_Settings_ManageFlowSources%>" href="/apps/nta-flowsources/management"><%= Resources.NTAWebContent.NTA_Settings_ManageFlowSources%></a></div>
                    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_71%><br />
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_72%>" href="<%= UIConsts.URL_NETFLOW_SOURCES_MANAGEMENT %>"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_72%></a></div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/cbqos_polling.svg"
                 alt="<%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesTitle%>" />
            <div class="ntasDIV4">
                <div class="ntasBlueLinkBig">
                    <a title="<%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesTitle%>" href="/apps/nta-cbqossources/management"><%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesTitle%></a></div>
                <%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesDescription%><br />
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesLink%>" href="<%= UIConsts.URL_CBQOS_SOURCES_MANAGEMENT %>"><%= Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesLink%></a></div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/collector.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBCODE_TM0_9%>" />
            <div class="ntasDIV4">
                     <div class="ntasBlueLinkBig"><a title="<%= Resources.NTAWebContent.NTAWEBCODE_TM0_9%>" href="/Orion/TrafficAnalysis/Admin/NetflowCollectorServicesEdit.aspx">
                    <%= Resources.NTAWebContent.NTAWEBCODE_TM0_9%></a></div>
                    
                    
                <%= Resources.NTAWebContent.NTAWEBDATA_AK0_75%><br />
                 <asp:PlaceHolder ID="ntaCollectorPlaceHolder" runat="server"></asp:PlaceHolder>
				 <orion:ExceptionWarning ID="ExceptionWarning" Text="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_109 %>" runat="server" Visible="false" />
                <div class="ntasBlueLink" ID = "manageCollectorsLink" runat="server">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_76%>" href="/Orion/TrafficAnalysis/Admin/NetflowCollectorServicesEdit.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_74%></a> </div>
            </div>
        </div>
        <div class="ntasDIV3">
            <img class="ntasIMG1" src="/Orion/TrafficAnalysis/images/NTASettings/label.svg"
                alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_77%>" />
            <div class="ntasDIV4">
                    <div class="ntasBlueLinkBig"><a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_77%>" href="/Orion/TrafficAnalysis/Admin/NetflowTypesOfServiceEdit.aspx">
                    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_77%></a></div>
                <div class="ntasBlueLink">&#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_79%>" href="/Orion/TrafficAnalysis/Admin/NetflowTypesOfServiceEdit.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_78%></a>
                </div>
            </div>
            
            
            </div>
            </div>

<script type="text/javascript">
    function RetainUnmonitoredDataConfirmation(chkBox) {
        if (IsDemo()) 
        {
            chkBox.checked = !chkBox.checked;
            DisplayWarning('<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_AK0_7)%>');
            return;
        }
    
        if (chkBox.checked)
            return;

        var agreeVal = confirm("<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_AK0_19)%>");
        if (!agreeVal)
            chkBox.checked = true;

        return;
    }
     
</script>
