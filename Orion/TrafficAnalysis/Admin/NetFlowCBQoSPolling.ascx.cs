﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;

using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.CBQoS;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.Utility;

using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Admin_NetflowCBQoSPolling : UserControl, INetFlowSettings
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            SetUI();
        }
    }

    public void SetUI()
    {
        this.chkbEnableCBQoSPolling.Checked = CBQoSGlobalSettingsDALCached.CBQoSEnabled;
        this.tbPollingInterval.Text = CBQoSGlobalSettingsDALCached.CBQoSPollingInterval.ToString();
    }

    public void StoreSettings()
    {

        CBQoSGlobalSettingsDALCached.CBQoSEnabled = this.chkbEnableCBQoSPolling.Checked;
        CBQoSGlobalSettingsDALCached.CBQoSPollingInterval = Convert.ToInt32(this.tbPollingInterval.Text);
    }

    public string Icon
    {
        get { return "/Orion/TrafficAnalysis/images/NTASettings/cbqos_polling.svg"; }
    }

    public string Caption
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK0_48; }
    }

    public string Redirect
    {
        get { return "CBQoS"; }
    }

    public string Help
    {
        get { return "OrionNetFlowPHCBQoSSettings"; }
    }

    public readonly string CBQoSSourcesTitle = Resources.NTAWebContent.NTA_Settings_ManageCBQoSSourcesTitle;

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        // No host services are consumed
    }
}
