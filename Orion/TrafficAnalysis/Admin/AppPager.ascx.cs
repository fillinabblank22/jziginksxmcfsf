using System;
using System.Web.UI;

public partial class Orion_TrafficAnalysis_Admin_AppPager : UserControl
{
    public event EventHandler PageChange;

    public int PageNumber
    {
        get 
        {
            try
            {
                return Convert.ToInt32(this.txtPageNum.Text);
            }
            catch
            {
                return 1;
            }
        }
        set 
        {
            if (value > 0)
                this.txtPageNum.Text = value.ToString();
            else
                throw new ArgumentOutOfRangeException("value", "value must be greater than zero."); //[Localization] This is exception which can appears only during development when the control is used in wrong way
        }
    }
    public int MaxPageNumber
    {
        get { return (int?) ViewState["MaxPageNumber"] ?? 1; }
        set
        {
            ViewState["MaxPageNumber"] = value;
            this.totalPages.Text = value.ToString();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.Page.Header.Controls.Add(this.ParseControl(PAGER_CSS));
        this.txtPageNum.Attributes["onblur"] = this.GetDoPostBackScript();
        this.txtPageNum.Attributes["onkeyup"] = "AppPager_HandleKeyDown(event);";
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "KeyUpHandler", string.Format(PAGER_JS_TEMPLATE, this.GetDoPostBackScript()), true);
        base.OnInit(e);
    }

    protected void MoveFirstClickHandler(object sender, ImageClickEventArgs e)
    {
        this.PageNumber = 1;
        this.RaiseEvent(this.PageChange);
    }
    protected void MoveNextClickHandler(object sender, ImageClickEventArgs e)
    {
        if(this.PageNumber < this.MaxPageNumber)
            this.PageNumber++;
        this.RaiseEvent(this.PageChange);
    }
    protected void MovePrevClickHandler(object sender, ImageClickEventArgs e)
    {
        if (this.PageNumber > 1)
            this.PageNumber--;
        this.RaiseEvent(this.PageChange);
    }
    protected void MoveLastClickHandler(object sender, ImageClickEventArgs e)
    {
        if (this.PageNumber != this.MaxPageNumber)
            this.PageNumber = this.MaxPageNumber;
        this.RaiseEvent(this.PageChange);
    }
    protected void TextChangeHandler(object sender, EventArgs e)
    {
        int dummy;
        if(Int32.TryParse(this.txtPageNum.Text, out dummy))
            this.RaiseEvent(this.PageChange);
    }


    private void RaiseEvent(EventHandler handler)
    {
        if (null != handler)
            handler(this, new EventArgs());
    }

    private string GetDoPostBackScript()
    {
        return string.Format("__doPostBack('{0}', '');", this.UniqueID);
    }

    private const string PAGER_CSS = @"
<style type='text/css'>
.PagerText
{
    text-align: center;
    vertical-align: top;
    height: 13px;
    font-size: 11px;
    padding-top: 0px;
    width: 2.5em;
}
</style>
";

    private const string PAGER_JS_TEMPLATE = @"
function AppPager_HandleKeyDown(evt)
{{
    var keyCode;
    if(evt.which)
    {{
        keyCode = evt.which;
    }}
    else
    {{
        keyCode = evt.keyCode;
    }}

    if(keyCode == 13)
    {{
        {0}
    }}
}}
";
}
