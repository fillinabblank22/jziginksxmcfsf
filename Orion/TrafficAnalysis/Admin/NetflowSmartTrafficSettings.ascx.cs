﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;

using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.Utility;

using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Admin_NetflowSmartTrafficSettings : UserControl, INetFlowSettings
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            SetUI();
        }
    }

    public void SetUI()
    {
        this.numKeepPercent.Value = GlobalSettingsDALCached.InstanceInterface.SmartFilteringKeepTopPercentage;
    }

    public void StoreSettings()
    {

        GlobalSettingsDALCached.InstanceInterface.SmartFilteringKeepTopPercentage = this.numKeepPercent.Value;
    }

    public string Icon
    {
        get { return "/Orion/TrafficAnalysis/images/NTASettings/talk.svg"; }
    }

    public string Caption
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK0_46; }
    }

    public string Redirect
    {
        get { return "Smart"; }
    }

    protected string LearnMoreLink
    {
         get
        {
            return HelpHelper.GetHelpUrl(this.Help);
        }
    }

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        // No host services are consumed
    }


    public string Help
    {
        get { return "OrionNetFlowPHSettingsSmartTrafficOptimization"; }
    }
}
