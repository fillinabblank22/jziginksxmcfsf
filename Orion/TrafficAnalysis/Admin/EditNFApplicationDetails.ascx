<%@ Control Language="C#" ClassName="EditNFApplicationDetails" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl" %>
<%@ Register TagPrefix="orion" TagName="SelectView" Src="~/Orion/Controls/SelectViewForViewType.ascx" %>

<script runat="server">
    public override string PropertyValue
    {
        get
        {
            return this.ViewSelector.PropertyValue;
        }
        set
        {
            this.ViewSelector.PropertyValue = value;
        }
    }
</script>

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="NetFlow Application" />