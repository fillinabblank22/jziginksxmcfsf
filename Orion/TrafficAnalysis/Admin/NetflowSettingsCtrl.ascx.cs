﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Admin_NetflowSettingsCtrl : UserControl, INetFlowSettingsHost
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (PlaceHolder1 == null)
        {
            return;
        }

        INetFlowSettings settings = PlaceHolder1.Controls[0] as INetFlowSettings;
        if (settings == null)
            return;

        this.icon.Src = settings.Icon;
        this.icon.Alt = settings.Caption;
        this.helpBtn.HelpUrlFragment = settings.Help;
        this.imgBtn.Visible = !HideSaveButton;

        this.caption.InnerText= settings.Caption;

        settings.Init(this);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!Page.ClientScript.IsStartupScriptRegistered(typeof(Page), "PageScrollPositionScript"))
        {
            string posXS = Request.Form["__SCROLLPOSITIONX"];
            int posX = 0;
            if (posXS != null)
                int.TryParse(posXS, out posX);
            string posYS = Request.Form["__SCROLLPOSITIONY"];
            int posY = 0;
            if (posYS != null)
                int.TryParse(posYS, out posY);

            Page.ClientScript.RegisterHiddenField("__SCROLLPOSITIONX", posX.ToString(CultureInfo.InvariantCulture));
            Page.ClientScript.RegisterHiddenField("__SCROLLPOSITIONY", posY.ToString(CultureInfo.InvariantCulture));
            Page.ClientScript.RegisterStartupScript(typeof(Page), "PageScrollPositionScript", "\r\ntheForm.oldSubmit = theForm.submit;\r\ntheForm.submit = WebForm_SaveScrollPositionSubmit;\r\n\r\ntheForm.oldOnSubmit = theForm.onsubmit;\r\ntheForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;\r\n" + (this.IsPostBack ? "\r\ntheForm.oldOnLoad = window.onload;\r\nwindow.onload = WebForm_RestoreScrollPosition;\r\n" : string.Empty), true);
        }

        base.Render(writer);
    }

    public bool HideSaveButton { get; set; }

    /// <summary>
    /// Gets the save button.
    /// </summary>
    public Control SaveButton
    {
        get
        {
            return imgBtn;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // if url will contains #anchor and saved property then show saved image
        if (this.hfSaved.Value == "1")
        {
            this.imageSaved.Visible = true;
            // javascript remove it
            //this.hfSaved.Value = "0";
        }
        else
        {
            this.imageSaved.Visible = false;
        }
    }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder Content
    {
        get { return PlaceHolder1; }
    }

    public string AnchorName { get; set; }

    [PersistenceMode(PersistenceMode.InnerProperty)]
    public PlaceHolder ContentBelowButton
    {
        get { return PlaceHolder2; }
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        if (PlaceHolder1 == null)
        {
            return;
        }

        INetFlowSettings settingsControll = PlaceHolder1.Controls[0] as INetFlowSettings;
        if (settingsControll == null)
            return;

        if (!Page.IsValid)
            return;

        if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this.Page, Resources.NTAWebContent.NTAWEBCODE_AK0_42, false))
        {
            settingsControll.SetUI();
        }
        else
        {
            settingsControll.StoreSettings();
            this.hfSaved.Value = "1";
        }

    }
}
