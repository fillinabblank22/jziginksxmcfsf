﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowSmartTrafficSettings.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Admin_NetflowSmartTrafficSettings" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>
<%@ Register Src="../Utils/NumberUpDownControl.ascx" TagName="NumberUpDownControl" TagPrefix="uc1" %>

<uc1:NumberUpDownControl runat="server" ID="numKeepPercent" MinimumValue="0" MaximumValue="100" TextBeforeAndAfter="<%$ Resources:NTAWebContent,NTAWEBDATA_RV0_2%>" />
<div class="checkBoxDescription" style="padding-left: 0;">
    <%= Resources.NTAWebContent.NTAWEBDATA_AK0_82%>
</div>
<div class="ntasBlueLink">
    &#187;&nbsp;<a title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_81%>" href="<%=LearnMoreLink %>" target="_blank" rel="noopener noreferrer"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_81%></a>
</div>