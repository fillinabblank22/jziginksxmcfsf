using System;
using SolarWinds.Logging;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;

public partial class Orion_Netflow_Settings : EditPageBase
{
    private static readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        if (!LicenseHelper.IsAllowed(Profile.AllowAdmin))
        {
            Server.Transfer(string.Format("~/Orion/Error.aspx?Message={0}", Server.UrlEncode(Resources.NTAWebContent.NTAWEBCODE_AK0_38)));
        }

        base.OnInit(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }
}
