<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppGrid.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_AppGrid" %>

<style type="text/css"> 
.dgApplications-nowrap { 
  white-space: nowrap; 
} 
</style>

<asp:DataGrid runat="server" 
              ID="dgApplications" 
              AllowPaging="false"
              AllowSorting="true" 
              PageSize="50" 
              BorderStyle="Solid" 
              Width="100%"
              AutoGenerateColumns="false"
              ShowFooter="false"
              GridLines="None"
              CellSpacing="1"
              CellPadding="5"
              CssClass="DataGrid"
              BorderColor="#dfdfde"
              OnItemCommand="DataGridOnItemCommandHandler"
              OnSortCommand="DataGridSortHandler"
              OnItemDataBound="OnRowDataBound"
              OnEditCommand="DataGridEditHandler"
              OnDeleteCommand="DataGridDeleteHandler" OnUpdateCommand="test"
              >
    <HeaderStyle Font-Italic="true" CssClass="DataGridHeader" />
    <Columns>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_36%>" SortExpression="Name">
            <ItemTemplate>
                <%# this.GetCollapseButton(Boolean.Parse(Eval("MultiPort").ToString()), Convert.ToInt32(Eval("AppID")), Eval("PortRange"))%>
               <span class='<%# this.GetDescriptionSpanClass(Eval("MultiPort"), Eval("PortRange")) %>' style="white-space:normal">
                    <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("Name").ToString()) %>
                </span>
            </ItemTemplate>
            <HeaderTemplate>
                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Name" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_36%>" />
                 <%# this.GetDescriptionSortImg()%>
            </HeaderTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_102%>">
            <ItemTemplate>
                <asp:Image
                    ID="imgCheck"
                    runat="server" 
                    ImageUrl='<%# Convert.ToInt32(Eval("Enabled")) == 1 ? "/Orion/TrafficAnalysis/images/icon_check.gif" : "/Orion/TrafficAnalysis/images/ignore.gif"%>' 
                    ToolTip='<%# Convert.ToInt32(Eval("Enabled")) == 1 ? Resources.NTAWebContent.NTAWEBCODE_VB0_94 : Resources.NTAWebContent.NTAWEBCODE_VB0_95%>'
                    />
                <asp:Label 
                    runat="server" 
                    Text='<%# Convert.ToInt32(Eval("Enabled")) == 1 ? Resources.NTAWebContent.NTAWEBCODE_VB0_94 : Resources.NTAWebContent.NTAWEBCODE_VB0_95%>'
                    ToolTip='<%# Convert.ToInt32(Eval("Enabled")) == 1 ? Resources.NTAWebContent.NTAWEBCODE_VB0_94 : Resources.NTAWebContent.NTAWEBCODE_VB0_95%>'
                    />
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_19%>" SortExpression="Port">
            <HeaderTemplate>
                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Port" Text="<%$ Resources:NTAWebContent,NTAWEBCODE_VB0_19%>" />
                 <%# this.GetPortSortImg()%>
            </HeaderTemplate>
            <ItemTemplate>
                <%# Eval("Port") %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_5%>" SortExpression="Protocol">
            <ItemStyle HorizontalAlign="Center" />
            <HeaderTemplate>
                <asp:LinkButton runat="server" CommandName="Sort" CommandArgument="Protocol" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_5%>" />
                 <%# this.GetProtocolSortImg()%>
            </HeaderTemplate>
            <ItemTemplate>
                <%# this.GetProtocolName(Eval("TCP"), Eval("UDP")) %>
            </ItemTemplate>
        </asp:TemplateColumn>
        
        <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_103%>" SortExpression="Source">
            <ItemStyle HorizontalAlign="Center" />
            <HeaderTemplate>
                <asp:LinkButton ID="lnkBtnSourceSort" runat="server" CommandName="Sort" CommandArgument="GroupNameSrc" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_103%>" />
                <%# this.GetSourceSortImg()%>
            </HeaderTemplate>
            <ItemTemplate>
                <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("SrcIPGroupName").ToString()) %>
            </ItemTemplate>
        </asp:TemplateColumn>
        
           <asp:TemplateColumn HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_104%>" SortExpression="Destination">
            <ItemStyle HorizontalAlign="Center" />
            <HeaderTemplate>
                <asp:LinkButton ID="lnkBtnDestinationSort" runat="server" CommandName="Sort" CommandArgument="GroupNameDst" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_104%>" />
                <%# this.GetDestinationSortImg()%>
            </HeaderTemplate>
            <ItemTemplate>
                <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("DstIPGroupName").ToString()) %>
            </ItemTemplate>
        </asp:TemplateColumn>
       
        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="dgApplications-nowrap" HeaderText="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_84%>">
            <HeaderStyle CssClass="RightHeader" />
            <ItemTemplate>
                <orion:LocalizableButton 
                                ID="btnDisable" 
                                runat="server" 
                                CommandName="Disable" 
                                LocalizedText="CustomText" 
				                DisplayType="Small"
				                Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_105%>"
                                CommandArgument='<%# Eval("AppID") %>'
                                Visible='<%# Convert.ToInt32(Eval("Enabled")) == 1 && !string.IsNullOrEmpty(Eval("Port").ToString()) %>'
                                />
                <orion:LocalizableButton 
                                ID="btnEnable" 
                                runat="server" 
                                CommandName="Enable" 
				                DisplayType="Small"
				                LocalizedText="CustomText"
				                Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_106%>"
                                CommandArgument='<%# Eval("AppID") %>'
                                Visible='<%# (Convert.ToInt32(Eval("Enabled")) == 0 && !string.IsNullOrEmpty(Eval("Port").ToString()))  %>'
                                />
                <orion:LocalizableButton ID="btnDelete" 
                                runat="server" 
                                CommandName="<%# DataGrid.DeleteCommandName %>" 
				                LocalizedText="Delete"
				                DisplayType="Small"
                                CommandArgument='<%# Eval("AppID") %>'
                                Visible='<%# string.IsNullOrEmpty(Eval("PortRange").ToString()) %>' 
                                />
                <orion:LocalizableButton ID="btnDeleteRange" 
                                runat="server" 
                                CommandName="DeleteRange" 
                                LocalizedText="Delete"
                                DisplayType="Small"
                                CommandArgument='<%# GetDeleteRangeArgument(Eval("AppID"), Eval("PortRange")) %>'
                                OnClick="DeleteRangeHandler"
                                Visible='<%# !string.IsNullOrEmpty(Eval("PortRange").ToString()) %>'
                                />
                <orion:LocalizableButton 
                                ID="btnEdit" 
                                runat="server" 
                                CommandName="<%# DataGrid.EditCommandName %>" 
                                CommandArgument='<%# Eval("AppID") %>'
                                DisplayType="Small"
                                LocalizedText="Edit"
                                Visible='<%# !string.IsNullOrEmpty(Eval("Port").ToString()) %>'    
                                />
            </ItemTemplate>
        </asp:TemplateColumn>
    </Columns>
</asp:DataGrid>

<script language="javascript" type="text/javascript">

// JScript File

function Collapse_Click(button)
{
    //alert("collapse_click " + button);
    if(button.src.indexOf("/Orion/images/Button.Expand.gif") > 0)
    {
        if(button.childRows)
        {
            // do expands
            for(var i = 0; i < button.childRows.length; i++)
            {
                button.childRows[i].style["display"] = "";
            }
        }
        button.src = "/Orion/images/Button.Collapse.gif";
    }
    else
    {
        if(button.childRows)
        {   
            // do collapses
            for(var i = 0; i < button.childRows.length; i++)
            {
                button.childRows[i].style["display"] = "none";
            }
        }
        button.src = "/Orion/images/Button.Expand.gif";
    }
    
    return false;
}

function RegisterCollapseRow(buttonID, rowID)
{
    //alert('regcollrow ' + buttonID +", row: "+ rowID);
    var button = document.getElementById(buttonID);
    var row = document.getElementById(rowID);

    if (button == null) { return; }
    if(!button.childRows)
    {
        button.childRows = new Array();
    }
    
    button.childRows.push(row);
    row.style["display"] = "none";
}

</script>
