<%@ Control Language="C#" ClassName="EditNetflowView" Inherits="SolarWinds.Orion.Web.UI.ProfilePropEditUserControl" %>
<%@ Register Src="~/Orion/Controls/SelectViewForViewType.ascx" TagPrefix="orion" TagName="SelectView" %>

<script runat="server">

  public override string PropertyValue
  { 
    get 
    { 
       return ViewSelector.PropertyValue; 
    }
    set 
    { 
       ViewSelector.PropertyValue = value; 
    }
  }

</script>

<!-- //TODO: To avoid creating of ~10 identical controls pass view type here -->

<orion:SelectView runat="server" ID="ViewSelector" AllowViewsByDeviceType="false" ViewType="Summary" />
