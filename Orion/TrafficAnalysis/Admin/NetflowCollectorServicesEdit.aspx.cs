using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Orion.Web;

public partial class Orion_TrafficAnalysis_Admin_NetflowCollectorServicesEdit : EditPageBase
{
    private const int MAX_PORT = 65535;
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();
    #region Properties

    private static IDALProvider _dalProvider = DALProvider.GetInstance();

    private static INetflowCollectorServicesDAL _netflowColectorServicesDAL = new NetflowCollectorServicesDAL();

    public string SubmitClientID 
    {
        get 
        {
            string id = this.btnSubmit.ClientID;
            return id;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadData();
        this.collectorRepeater.ItemCommand += new RepeaterCommandEventHandler(collectorRepeater_ItemCommand);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    void collectorRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {

            if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_TM0_28, false))
            {
                return;
            }

            string serverName = e.CommandArgument.ToString();
            try
            {
                _dalProvider.GetDAL<INetflowCollectorServicesDAL>().DeleteCollectorServer(serverName);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("Exception caught during NetFlow Collector Service delete: {0}", ex.ToString());
            }
			LoadData();
        }
    }

    private void LoadData()
    {
        try
        {
            collectorRepeater.DataSource = _dalProvider.GetDAL<INetflowCollectorServicesDAL>().GetCollectorServers();
            collectorRepeater.DataBind();
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Caught exception when retrieving NetFlow Collector Services list:\n\t{0}", ex.ToString());
            this.collectorRepeater.Visible = false;
            this.Message.Text = string.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_27, ex.GetType().ToString());
            this.Message.Visible = true;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e) 
    {
        string server = string.Empty;
        string port = string.Empty;
        bool isOK = true;

        if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_TM0_29, false))
        {
            return;
        }

        foreach(RepeaterItem item in collectorRepeater.Items)
        {
            server = ((HiddenField)item.FindControl("hidNetFlowServer")).Value;
            port = ((TextBox)item.FindControl("txtNetFlowPort")).Text.Trim(", ".ToCharArray());
            try
            {
                if (ValidatePort(port))
                {
                    _dalProvider.GetDAL<INetflowCollectorServicesDAL>().SetCollectorServerPort(server, port);
                }
                else
                {
                    AlertError(Resources.NTAWebContent.NTAWEBCODE_TM0_26);
                    return;
                }
            }
            catch(Exception ex) 
            {
                _log.ErrorFormat("Caught exception when updating NetFlow Collector Services list:\n\t{0}", ex.ToString());
                AlertError(ex.Message);
                isOK = false;
            }
        }
        if (isOK) 
        {
            DoRedirect();
        }
    }

    protected enum EngineServerType
    {
        Primary,
        Additional
    }

    private bool ValidatePort(string port) 
    {
        if (string.IsNullOrEmpty(port))
            return false;

        string[] tokens = port.Split(',');

        int tmpint = 0;
        foreach (string item in tokens)
            if (!int.TryParse(item, out tmpint))
                return false;
            else if (tmpint < 0 || tmpint > 65535)
                return false;

        return true;
    }

    private void AlertError(string message)
    {
        String scriptString = "";
        scriptString = "<script language=JavaScript> alert('{0}')";
        scriptString += "<";
        scriptString += "/";
        scriptString += "script>";
        scriptString = string.Format(scriptString, message);
        if (!this.Page.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", scriptString);
    }
}
