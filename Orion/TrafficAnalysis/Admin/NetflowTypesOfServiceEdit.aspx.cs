using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;

public partial class Orion_TrafficAnalysis_Admin_NetflowTypesOfServiceEdit : EditPageBase
{
    private static readonly SolarWinds.Logging.Log myLog = new SolarWinds.Logging.Log();

    protected bool NewTypeMode
    {
        get { return (null == ViewState["NewTypeMode"]) ? false : (bool)ViewState["NewTypeMode"]; }
        set 
        {
            ViewState["NewTypeMode"] = value;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.IsPostBack)
            this.DataBind();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    public override void DataBind()
    {
        this.dgToS.DataSource = NetflowTypesOfServiceDAL.GetData();
        this.dgToS.DataBind();

        base.DataBind();
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        DoRedirect();
    }


    #region DataGrid events
    protected void EditHandler(object sender, DataGridCommandEventArgs e)
    {
        if (-1 == this.dgToS.EditItemIndex && (!this.NewTypeMode))
        {
            this.dgToS.EditItemIndex = e.Item.ItemIndex;
            this.DataBind();
        }
    }

    protected void CancelHandler(object sender, DataGridCommandEventArgs e)
    {
        this.dgToS.EditItemIndex = -1;
        this.DataBind();
    }

    protected void UpdateHandler(object sender, DataGridCommandEventArgs e)
    {
        this.Page.Validate("EditToS");
        if (!this.Page.IsValid)
            return;

        if (SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_VB0_47, false))
        {
            return;
        }

        string newName = (e.Item.Cells[0].FindControl("txtTosName") as TextBox).Text;
        string codePoint = e.Item.Cells[1].Text;
        NetflowTypesOfServiceDAL.UpdateToSName(newName, codePoint);
        this.dgToS.EditItemIndex = -1;
        this.DataBind();
    }
    #endregion
}
