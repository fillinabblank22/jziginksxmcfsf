using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Core.Common.i18n.Registrar;
using SolarWinds.Orion.Web.UI.Localizer;
using SolarWinds.Orion.Web.Helpers;


public partial class Orion_TrafficAnalysis_Admin_NetflowDNSSettings : System.Web.UI.UserControl, INetFlowSettings
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);         
        DataBind(); 
        SetUI();
    }
    
    public int MinutesBeforeBailout => GlobalSettingsDALCached.DatabaseMaintenanceMinutesBeforeBailout;
    internal static int CurrentExpiredNumber => FlowCorrelationPostDNSDAL.GetExpiredItemsCount();

    public void SetUI()
    {
        chkbDnsResolverUseNetbiosFunction.Checked = GlobalSettingsDALCached.DnsResolverUseNetbiosFunction;
        chkbDnsOnDemandEnabled.Checked = GlobalSettingsDALCached.InstanceInterface.DnsOnDemandEnabled;
        chkbDnsPersistEnabled.Checked = GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled;

        numUpDnCacheExpirationDays.Value = GlobalSettingsDALCached.CacheExpirationDays;

        numUpDnCacheExpirationDaysNotResolved.Value = GlobalSettingsDALCached.CacheExpirationDaysNotResolved;

        if (MinutesBeforeBailout == 0)
        {
            tbCustomMinutes.Enabled = false;
            rbNeverBailout.Checked = true;
        }
        else
        {
            tbCustomMinutes.Enabled = true;
            tbCustomMinutes.Text = MinutesBeforeBailout.ToString();
            rbMinutesToBailout.Checked = true;
        }

        numUpDnNameResolverDelay.Value = GlobalSettingsDALCached.NameResolverNameResolverDelay;
        numUpDnRowsToFetchForNameResolutionCount.Value = GlobalSettingsDALCached.NameResolverRowsToFetchForNameResolutionCount;       
    }

    void INetFlowSettings.StoreSettings()
    {
        GlobalSettingsDALCached.DnsResolverUseNetbiosFunction = chkbDnsResolverUseNetbiosFunction.Checked;
        GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled = chkbDnsPersistEnabled.Checked;
        GlobalSettingsDALCached.InstanceInterface.DnsOnDemandEnabled = chkbDnsOnDemandEnabled.Checked;
        
        int cacheExpirationDays = numUpDnCacheExpirationDays.Value;
        int cacheExpirationDaysNotResolved = numUpDnCacheExpirationDaysNotResolved.Value;

        GlobalSettingsDALCached.CacheExpirationDays = cacheExpirationDays;
        GlobalSettingsDALCached.CacheExpirationDaysNotResolved = cacheExpirationDaysNotResolved;

        GlobalSettingsDALCached.NameResolverNameResolverDelay = numUpDnNameResolverDelay.Value;
        GlobalSettingsDALCached.NameResolverRowsToFetchForNameResolutionCount = numUpDnRowsToFetchForNameResolutionCount.Value;
        GlobalSettingsDALCached.DatabaseMaintenanceMinutesBeforeBailout = rbNeverBailout.Checked ? 0 : int.Parse(tbCustomMinutes.Text);
    }

    protected string GetLocalizedProperty(string prefix, string property)
    {
        ResourceManagerRegistrar manager = ResourceManagerRegistrar.Instance;
        string key = manager.CleanResxKey(prefix, property);
        return manager.SearchAll(key, manager.GetAllResourceManagerIds());
    }

    public string Icon => "/Orion/TrafficAnalysis/images/NTASettings/network.svg";
    public string Caption => Resources.NTAWebContent.NTAWEBCODE_AK0_49;
    public string Redirect => "DNS";
    public string Help => "OrionNetFlowPHSettingsDNSNetBIOSResolution";
    protected string LearnMoreLink => HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, Help);

    void INetFlowSettings.Init(INetFlowSettingsHost host)
    {
        // No host services are consumed
    }
}
