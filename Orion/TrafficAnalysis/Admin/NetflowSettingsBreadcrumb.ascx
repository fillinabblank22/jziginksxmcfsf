﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowSettingsBreadcrumb.ascx.cs" Inherits="Orion_TrafficAnalysis_Admin_NetflowSettingsBreadcrumb" %>

<div class="nfsBreadcrumb" style="margin-top: 20px; padding: 0;">
    <asp:HyperLink runat="server" ID="lnkFirst" NavigateUrl="/Orion/Admin" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_7%>" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_8%>" ></asp:HyperLink>
    <asp:Label ID="lblFirst" Text="&nbsp;>&nbsp;" runat="server" > </asp:Label>
    <asp:HyperLink runat="server" ID="lnkSecond" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_9%>" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_9%>"></asp:HyperLink>
    <asp:Label ID="lblSecond" Text="&nbsp;>&nbsp;" runat="server"></asp:Label>
</div>