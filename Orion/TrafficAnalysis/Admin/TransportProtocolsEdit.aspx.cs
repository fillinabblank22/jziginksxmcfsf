using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;

public partial class Orion_TrafficAnalysis_Admin_TransportProtocolsEdit : EditPageBase
{
    private static readonly SolarWinds.Logging.Log myLog = new SolarWinds.Logging.Log();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                List<ItemProtocol> ProtocolsList = TransportProtocolsDAL.GetData();
                TransportProtocolsList.DataSource = ProtocolsList;
                TransportProtocolsList.DataBind();
                this.checkedProtocols.Text = this.GetCount().ToString();
            }
        } 
        catch (Exception ex)
        {
            myLog.DebugFormat("Caught exception when retrieving Transport Protocols list:\n\t{0}", ex);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        var file = OrionInclude.CoreFile("OrionMaster.js", OrionInclude.Section.Bottom);
        file.AddJsInit("SW.Core.Header.SyncTo('.PageHeader');");
    }

    public int GetCount()
    {
        return TransportProtocolsDAL.SelectedCount();
    }

    protected void CheckProtocol(object sender, EventArgs e)
    {
        CheckBox cb = (CheckBox)sender;

        if(SolarWinds.Netflow.Web.Utility.DemoHelper.CheckIfDemoAndWarnUser(this, Resources.NTAWebContent.NTAWEBCODE_VB0_49, false))
        {
            cb.Checked = !cb.Checked;
            return;
        }

        
        DataListItem row = (DataListItem)cb.NamingContainer;
        byte id = (byte)TransportProtocolsList.DataKeys[(int)row.ItemIndex];

        if (cb.Checked)
        {
            TransportProtocolsDAL.Enable(id);
        }
        else
        {
            TransportProtocolsDAL.Disable(id);
        }

        this.checkedProtocols.Text = this.GetCount().ToString();
    }

    public void btnSubmit_Click(object sender, EventArgs e)
    {
        DoRedirect();
    }
}
