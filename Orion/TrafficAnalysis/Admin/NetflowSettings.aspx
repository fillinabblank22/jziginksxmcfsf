<%@ Page Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master"
    AutoEventWireup="true" CodeFile="NetflowSettings.aspx.cs" Inherits="Orion_Netflow_Settings"
    Title="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_51%>" MaintainScrollPositionOnPostback="true" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>
<%@ Import Namespace="SolarWinds.Common" %>

<%@ Register TagPrefix="nta" TagName="Breadcrumb" Src="~/Orion/TrafficAnalysis/Admin/NetflowSettingsBreadcrumb.ascx" %>
<%@ Register TagPrefix="netflow" TagName="NTALicenseMessage" Src="~/Orion/TrafficAnalysis/Controls/LicenseMessage.ascx" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions" %>
<%@ Register TagPrefix="netflow" Assembly="NetflowWeb" Namespace="SolarWinds.Netflow.Web.Utility" %>

<%@ Register Src="~/Orion/TrafficAnalysis/Admin/NetflowSettingsCtrl.ascx" TagPrefix="nta" TagName="NetflowSettingsCtrl" %>

<%@ Register TagPrefix="netflow" TagName="GlobalSettings" Src="~/Orion/TrafficAnalysis/Admin/NetflowGlobalSettings.ascx" %>
<%@ Register TagPrefix="netflow" TagName="SmartTraffic" Src="~/Orion/TrafficAnalysis/Admin/NetflowSmartTrafficSettings.ascx" %>
<%@ Register TagPrefix="netflow" TagName="CBQoSPolling" Src="~/Orion/TrafficAnalysis/Admin/NetflowCBQoSPolling.ascx" %>
<%@ Register TagPrefix="netflow" TagName="DNSSettings" Src="~/Orion/TrafficAnalysis/Admin/NetflowDNSSettings.ascx" %>
<%@ Register TagPrefix="netflow" TagName="DatabaseMaintenanceSettings" Src="~/Orion/TrafficAnalysis/Admin/NetflowDatabaseMaintenanceSettings.ascx" %>
<%@ Register TagPrefix="netflow" TagName="ChartingSettings" Src="~/Orion/TrafficAnalysis/Admin/NetflowChartingSettings.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
    label { padding: 3px; }
    *label { padding: 0px; }
    body 
    {
        background-color: var(--body-bg,#eaeaea);
    }

    </style>

    <orion:Include Module="TrafficAnalysis" runat="server" File="TrafficAnalysis.css" />
    <orion:Include Module="TrafficAnalysis" runat="server" File="NetFlow.css" />
    <orion:Include Module="TrafficAnalysis" runat="server" File="DemoScripts.js" />
    <orion:Include ID="IncludeExt" runat="server" Framework="Ext" FrameworkVersion="4.0" />

    <nta:Breadcrumb runat="server" ID="breadcrumb" HideSecond="true" />
    <div class="PageHeader" id="etxxHeader" style='width:auto !Important;'>
        <%= Resources.NTAWebContent.NTAWEBDATA_AK0_51%></div>

    <netflow:NTALicenseMessage ID="NTALicenseMessage1" runat="server" />
    
    <input type="hidden" id="demoHidden"
        value="<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>"/>
    
    <nta:NetflowSettingsCtrl runat="server" ID="global" AnchorName="Global" >
        <Content>
            <netflow:GlobalSettings ID="ntaGlobal1" runat="server" ShowSecondPart="false" />
        </Content>
        <ContentBelowButton>
            <netflow:GlobalSettings ID="ntaGlobal2" runat="server" ShowSecondPart="true" />
        </ContentBelowButton>
    </nta:NetflowSettingsCtrl>
    
    <nta:NetflowSettingsCtrl runat="server" ID="smart" AnchorName="Smart" >
        <Content>
            <netflow:SmartTraffic ID="ntaSmart" runat="server" />
        </Content>
    </nta:NetflowSettingsCtrl>

    <nta:NetflowSettingsCtrl runat="server" ID="cbqos" AnchorName="CBQoS">
        <Content>
            <netflow:CBQoSPolling ID="ntaCBQoSPolling" runat="server" />
        </Content>
    </nta:NetflowSettingsCtrl>

    <nta:NetflowSettingsCtrl runat="server" ID="dns" AnchorName="DNS">
        <Content>
            <netflow:DNSSettings ID="ntaDNS" runat="server" />
        </Content>
    </nta:NetflowSettingsCtrl>

    <nta:NetflowSettingsCtrl runat="server" ID="database" AnchorName="Database">
        <Content>
            <netflow:DatabaseMaintenanceSettings ID="ntaDatabase" runat="server" />
        </Content>
    </nta:NetflowSettingsCtrl>

    <nta:NetflowSettingsCtrl runat="server" ID="chart" AnchorName="Chart">
        <Content>
            <netflow:ChartingSettings ID="ntaCharting" runat="server" />
        </Content>
    </nta:NetflowSettingsCtrl>
   
</asp:Content>