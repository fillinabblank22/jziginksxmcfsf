<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" CodeFile="NetflowCollectorServicesEdit.aspx.cs" Inherits="Orion_TrafficAnalysis_Admin_NetflowCollectorServicesEdit" %>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta" TagName="NtaHelpButton" %>
<%@ Import Namespace="SolarWinds.Netflow.DAL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
<nta:NtaHelpButton ID="HelpButton9" runat="server" HelpUrlFragment="OrionNetFlowPHSettingsCollectorServices" Title="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_29%>"  />
</asp:Content>

<asp:Content ID="collectorContent"  ContentPlaceHolderID="MainContent" runat="server">
    <orion:Include Module="TrafficAnalysis" runat="server" File="NetFlow.css" />
	
	    <script type="text/javascript" language="javascript">
	         function KeyDownHandler(ev) {
	            var evt = document.all ? window.event : ev;
	            if (evt.keyCode == 13)
	            {
                    evt.returnValue = false;
                    evt.cancel = true;
                    document.getElementById('<%=SubmitClientID%>').click();
                }
             }
	    </script>
	    
	    <div class="PageHeader" id="etxxHeader" style="width: auto;">
            <%= Resources.NTAWebContent.NTAWEBDATA_TM0_81%>
	    </div>
	    
	    <div class="SettingsWrapper" id="etxxRW" style="width: 600px;">
	        <div class="etxxLH">
	        
	        
	            <div><b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_27%></b></div><br/>
	            <div><%= Resources.NTAWebContent.NTAWEBDATA_TM0_28%></div><br/>
    	        <div style="width:550px; text-align:left;">
    	            <table border="0" cellpadding="2" cellspacing="0" width="600">
	        <asp:Repeater Runat="server" ID="collectorRepeater" EnableViewState="false">
	          <HeaderTemplate>
	            <tr>
		            <td class="ReportHeader" width="20">&nbsp;</td>
		            <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_23%></td>
		            <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_24%></td>
		            <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_25%></td>
                    <td class="ReportHeader"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_118%></td>
		            <td class="ReportHeader" width="50">&nbsp;</td>
	            </tr>
	          </HeaderTemplate>
    	     
	          <ItemTemplate>
                 <tr>
				    <td class="Property" width="20">
				        <%#Convert.ToInt32(Eval("StatusTime")) < 15 ? "<img name='up' src='/NetPerfMon/images/small-up.gif'>" : "<img name='down' src='/NetPerfMon/images/small-down.gif'>"%>
				    </td>
				    <td class="Property"><%#Eval("ServerName")%>&nbsp;</td>
				    <td class="Property"><%#Convert.ToInt32(Eval("StatusTime")) < 15 ? Resources.NTAWebContent.NTAWEBCODE_TM0_11 : Resources.NTAWebContent.NTAWEBCODE_TM0_12 %></td>
				    <td class="Property">
				        <asp:TextBox runat="server" Rows="1" ID="txtNetFlowPort" Text='<%#Eval("NetflowPort")%>' onkeydown="KeyDownHandler(event)"></asp:TextBox>
				        <asp:HiddenField runat="server" ID="hidNetFlowServer" Value='<%#Eval("ServerName")%>' />
			        </td>
                    <td class="Property"><%#Convert.ToString(Eval("ServerType")) == EngineServerType.Additional.ToString() ? Resources.NTAWebContent.NTAWEBDATA_TM0_119 : Resources.NTAWebContent.NTAWEBDATA_TM0_120 %></td>
			        <td class="Property">
                        <orion:LocalizableButton runat="server" ID="btnDelete" LocalizedText="Delete" CommandName="Delete" CommandArgument='<%#Eval("ServerName")%>' 
                            DisplayType="Resource" Visible='<%#Convert.ToString(Eval("ServerType")) == EngineServerType.Additional.ToString()%>' />
			        </td>
			    </tr>
              </ItemTemplate>
            </asp:Repeater>
                </table> 
               </div>
            <asp:Label runat="server" ID="Message" ForeColor="Red" Visible="false"></asp:Label>
    	    
	        </div>  <%--etxxLH DIV END--%>
	    </div>  <%--Resource wrapper DIV END--%>
	    
        <div class="sw-btn-bar">
            <orion:LocalizableButton CssClass="etxxBtn" runat="server" ID="btnSubmit" LocalizedText="Submit" 
                                     OnClick="btnSubmit_Click" DisplayType="Primary" />
	    </div>
</asp:Content>