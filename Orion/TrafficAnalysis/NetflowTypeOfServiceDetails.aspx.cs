using System;
using System.Web.UI;
using System.Web.Services;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_Netflow_TypeOfServiceDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow Type of Service"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.tosResHost.NetFlowTypeOfService = this.NetObject as NetflowTypeOfService;

        this.Page.Form.Attributes["autocomplete"] = "off";

        base.OnInit(e);
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
