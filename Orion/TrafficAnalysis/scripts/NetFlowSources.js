﻿SW.NTA = SW.NTA || {};
SW.NTA.NetflowSources = SW.NTA.NetflowSources || {};

SW.NTA.NetflowSources.AllNetflowSourcesNodeTree = {

    Succeeded: function (result, context) {
        var contentsDiv = $(context);
        $(contentsDiv).get(0).innerHTML = result;
        contentsDiv.slideDown('fast');
    },
    SucceededVerified: function (result, context) {
        if (!(result.lastIndexOf('<table cellspacing="0" style="font-size:11px;">', 0) === 0)) {
            contentsDiv.innerHTML = "Unauthorized!";
            window.location.href = '/Orion/Login.aspx?ReturnUrl=' + encodeURIComponent(window.location.href);
            return false;
        }
        var contentsDiv = $(context);
        $(contentsDiv).get(0).innerHTML = result;
        $("table>tbody>tr>td", contentsDiv).css("border-top", "1pt solid #D5D5D5");
    },
    Failed: function (error, context) {
        var contentsDiv = context;
        contentsDiv.innerHTML = "Server error";
    },

    AutoClickLinks: function (contentsDiv) {
        // auto-click the "get 100 more Nodes" links
        $("[id*='-startFrom-'] a", contentsDiv).click();

        // expand any nodes that are marked to be expanded	    
        $("[expand]", contentsDiv).removeAttr("expand").click();
    },

    Click: function (rootId, resourceId, keys, contentsDiv) {
        AllNetflowSourcesNodeTree.GetTreeSection(resourceId, rootId, keys,
            SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.SucceededVerified,
            SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.Failed
            , contentsDiv);
    },


    LoadAllNetflowSources: function (treeDiv, resourceId) {

        treeDiv.html("<div><img src = '/Orion/images/loading_gen_small.gif' alt = 'Loading...'></div>");
        AllNetflowSourcesNodeTree.GetTreeSection(resourceId, "NodeTree_r" + resourceId, [],
            SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.Succeeded,
            SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.Failed,
            treeDiv.get(0));
    },

    LoadInterfaceCount: function (treeDiv, resourceWrapperId) {
        var subTitle = document.getElementById(resourceWrapperId + "_headerBar").getElementsByClassName('sw-widget__subtitle')[0];
        subTitle.innerHTML = "<div><img src = '/Orion/images/loading_gen_small.gif' alt = 'Loading...'></div>";

        treeDiv.html("<div><img src = '/Orion/images/loading_gen_small.gif' alt = 'Loading...'></div>");
        AllNetflowSourcesNodeTree.GetNodesCount(
            function (result, context) {
                subTitle.innerHTML = result;
            },
            function (result, context) {
                subTitle.innerHTML = "Server error";
            },
            treeDiv.get(0));
    },
    LoadRoot: function (treeDiv, resourceId, resourceWrapperId) {
        SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.LoadAllNetflowSources(treeDiv, resourceId);
        SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.LoadInterfaceCount(treeDiv, resourceWrapperId);
    },

};

function NetFlowSources_Click(rootId, resourceId, keys) {
    var toggleImg = $get(rootId + '-toggle');
    var contentsDiv = $get(rootId + '-contents');

    if (contentsDiv.style.display == "") {
        contentsDiv.style.display = "none";
        toggleImg.src = "/Orion/images/Button.Expand.gif";
    }
    else {
        contentsDiv.style.display = "";
        toggleImg.src = "/Orion/images/Button.Collapse.gif";

        if (!contentsDiv.contentLoaded) {
            contentsDiv.innerHTML = "<img src='/Orion/images/loading_gen_small.gif' alt='Loading...' />";
            SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.Click(rootId, resourceId, keys, contentsDiv);
            contentsDiv.contentLoaded = true;
        }
    }

    return false;
}