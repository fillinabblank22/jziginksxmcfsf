﻿function ProgressDialog(title) {
    var self = this;
    self.CancelButtonId = 'ProgressDialogCancelButton';
    self.CancelButtonIdSelector = '#' + self.CancelButtonId;
    self.CloseButtonSelector = '.ui-dialog-titlebar-close';
    self.title = title;
    self.cancelCallback = undefined;

    self.show = function (cancelCallback) {
        self.cancelCallback = cancelCallback;
        var encodedTitle = $("<div/>").text(self.title).html();

        $("#progressDialog").show().dialog({
            width: '550px',
            height: 'auto',
            modal: true,
            overlay: { "background-color": "black", opacity: 0.4 },
            title: encodedTitle
        });

        var cancelTitle = $(self.CloseButtonSelector),
            cancelButton = $(self.CancelButtonIdSelector);
        cancelTitle.unbind('click').click(cancelCallback || function () { });
        cancelButton.unbind('click').click(cancelCallback || function () { }).attr('href', '#');
    };

    self.setProgressBar = function(value, maxValue) {
        if (value > maxValue) {
            value = maxValue;
        }
        if (value == 0) {
            $("#progressBar").empty();
        } else {
            if ($("#progressBar img").length == 0) {
                $("#progressBar").append(createBarImage());
            }
            $("#progressBar img").width((300 / maxValue) * value);
        }
    };
    
    var createBarImage = function() {
        return $("<img />").attr("src", "/Orion/TrafficAnalysis/images/progBar_on.gif").height(20);
    };

    self.setStatusText = function(text) {
        $("#statusText").text(text);
    };

    self.close = function () {
        $("#progressDialog").dialog("close");
    };
    
    self.enableCancellation = function (enabled) {
        var cancelTitle = $(self.CloseButtonSelector),
            cancelButton = $(self.CancelButtonIdSelector);
        if (enabled) {
            cancelTitle.show();
            cancelButton.removeClass('sw-btn-disabled').unbind('click').click(self.cancelCallback || function () { }).attr('href', '#');
        } else {
            cancelTitle.hide();
            cancelButton.addClass('sw-btn-disabled').unbind('click');
        }
    };
}