/*
Solve issues of java script controls used by UpdatePanel which uses jQuery at dynamic contents but
does not have proper implementation of .dispose as explained in: 
http://weblogs.asp.net/leftslipper/archive/2006/11/13/HOWTO_3A00_-Write-controls-compatible-with-UpdatePanel-without-linking-to-the-ASP.NET-AJAX-DLL.aspx

Created by:     Roman Konecny
Based on:       http://malnotna.wordpress.com/2009/01/21/solutions-to-the-jquerydata-updatepanel-memory-leak-problem/
jQuery version: 1.3.2
*/
$(function() {
    if (Sys && Sys.WebForms && !Sys.WebForms.PageRequestManager.jQueryDestroyTreeOverriden) {
        Sys.WebForms.PageRequestManager.jQueryDestroyTreeOverriden = true;
        var oldFn = Sys.WebForms.PageRequestManager.prototype._destroyTree;
        var depth = 0;
        Sys.WebForms.PageRequestManager.prototype._destroyTree = function(element) {
            depth++;
            oldFn.apply(this, [element]);
            if (--depth == 0) {
                $(element).empty();
                $.cleanEmptyCache();
            }
        }
    }
});

/*
jQuery enhancement
Created by: Roman Konecny
*/
(function($) {
    $.cleanEmptyCache = function() {
        // clean jQuery cache = just remove all items whose contents is empty
        for (var id in $.cache) {
            var empty = true;
            for (var cnt in $.cache[id]) {
                empty = false;
                break;
            }
            if (empty)
                delete $.cache[id];
        }
    };
})(jQuery);
