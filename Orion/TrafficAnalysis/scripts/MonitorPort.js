﻿var _appString;
var _monitoredDivID;
var _monitorButtonID; // used for disable click again (after port is already monitored)
var _appNameID;       // used for change application name after dialog is closed
var _protocol;
var _result;
var PASSED = "PASSED";

var _appID;
var _appName;
var _ipGroupSrc;
var _ipGroupDst;
var _protocolName;
var _protocolNameDetected;

// new stuff
var $_tbPort;
var $_ddlProtocol;
var $_tbDescription;
var _ports;

function OpenModalDialog(pAppID, pAppName, pPorts, pProtocolName, pIPGroupSrc, pIPGroupDst) {

    $_tbDescription = $("input[id$='txtDescription']");
    $_tbPort = $("input[id$='txtPorts']");
    $_ddlProtocol = $("[id$='ddlProtocol']");

    // remove ".U" or ".T" if it exist after the port number
    _appID = RemoveTU(pAppID);
    _ports = RemoveTU(pPorts);
    _appName = pAppName;
    _ports = pPorts;
    _protocolName = pProtocolName;
    _protocolNameDetected = pProtocolName;
    _ipGroupSrc = pIPGroupSrc;
    _ipGroupDst = pIPGroupDst;
    var oldAppID = pAppID;

    SetDialog();

    _appString = oldAppID + "~~" + _appName + "~~" + _protocol;
    _monitorButtonID = "_monitorButton" + oldAppID;
    _monitoredDivID = "_monitoredDiv" + oldAppID;
    _appNameID = "appName" + oldAppID;

    ModalPopupDisplay(true);
}

function SetDialog() {
    $_tbDescription.val(_appName);    //bind description
    $_tbPort.val(_ports);             //bind port

    // bind IP Groups
    if (_ipGroupSrc > 0) {
        _ddlSource.val(_ipGroupSrc);
    }
    if (_ipGroupDst > 0) {
        _ddlDestination.val(_ipGroupDst);
    }
    $_tbPort.attr("disabled", true);

    if (_protocolName != "") {
        _protocol = "0";
        if (_protocolName == "TCP") {
            _protocol = "6";
        }
        if (_protocolName == "UDP") {
            _protocol = "17";
        }
    }

    // set protocol (drop down list)
    $_ddlProtocol.val(_protocol);
}


// remove ".U" or ".T" if it exist after the port number
function RemoveTU(str) {
    if (str.indexOf(".U") > -1) {
        str = str.substring(0, str.indexOf("U") - 1);
    }
    if (str.indexOf(".T") > -1) {
        str = str.substring(0, str.indexOf("T") - 1);
    }
    return str;
}


function Update() {

    if ($_ddlProtocol.length <= 0) {
        return;
    }

    var protocolDet = "0";
    
    if (_protocolNameDetected != "") {
        protocolDet = "0";
        if (_protocolNameDetected == "TCP") {
            protocolDet = "6";
        }
        if (_protocolNameDetected == "UDP") {
            protocolDet = "17";
        }
    }
    
    var protocol = $_ddlProtocol.val();

    // port ID ~~ name ~~ protocol ~~ sourceGroup ~~ destinationGroup ~~ oldProtocol (Detected by traffic)
    _appString = _appID + "~~" + $_tbDescription.val() + "~~" + protocol + "~~" + $(_ddlSource).val() + "~~" + $(_ddlDestination).val() + "~~" + protocolDet;
    
}

function GetProtocol(protocol) {
    if (protocol == "6") {
        return "(TCP)";
    }
    if (protocol == "17") {
        return "(UDP)";
    }
    return "(TCP,UDP)";
}

function GetProtocol2(protocol) {
    if (protocol == "6") {
        return "TCP";
    }
    if (protocol == "17") {
        return "UDP";
    }
    return "TCP,UDP";
}

function MakeBoth(pProt) {
    if (pProt == "TCP,UDP") {
        return "Both";
    }
    return pProt;
}

function ModalPopupDisplay(pShow) {
    var modalPopupBehavior = $find('MonitorPortPopupBehID');

    if (!modalPopupBehavior)
        return;
    
    if (pShow) {
        modalPopupBehavior.show();
    }
    else {
        modalPopupBehavior.hide();
    }
}


function ResetControl() {
    $(_labelSourceIP).hide();
    $(_tbDescription).val("");
    $(_tbPorts).val("").attr("disabled", "true");
    $(_ddlSource).val("ANY_DESTINATION");
    $(_ddlDestination).val("ANY_DESTINATION");
    $(_ddlProtocol).val("0");
    $(_labelDestinationIP).hide();
}

//callback success
function ProcessCallBackSuccess(result, context) {
    _result = result;
    if (Finish())
    { ResetControl(); }
    return;
}

function fnCollisionDlg(btn) {
    if (btn == 'no') {
        SetDialog();
        ModalPopupDisplay(true);
    } else {
        // resolve conflict

        // call again Invoke query for resolve conficts
        // probably add groups protocols etc...
        _appString = "ResolveCollisions~~" + _appID + "~~" + _ipGroupSrc + "~~" + _ipGroupDst + "~~" + _appName;
        //alert(_appName);

        GetIPGroupDetail(_appString, "");
        
        ChangeTextAndButton();
    }
}


function Finish() {
    ModalPopupDisplay(false);

    if (_result == "DEMO") 
    {
        DisplayWarning("Adding and changing applications");
        return false;
    }
    
    if (_result != "PASSED") 
    {
        if (_result.indexOf("COLLISION~~") > -1) 
        {
            // COLLISION~~json~~srcIPGroup~~dstIPGroup~~protocol
            var splitResult = _result.split("~~");

            // foreach it COLLISION~~json [1]
            // set values for case of cancel button
            JSONObject = JSON.parse(splitResult[1]);
            _ipGroupSrc = splitResult[2];
            _ipGroupDst = splitResult[3];
            _appName = splitResult[5];
            _protocol = splitResult[4];
            _protocolName = GetProtocol2(_protocol);

            var message = String.format("@{R=NTA.Strings;K=NTAWEBJS_VB1_1;E=js}", "<b>", "</b>", "<br>", _appName, _ports, MakeBoth(_protocolName));

            message += "@{R=NTA.Strings;K=NTAWEBJS_VB1_2;E=js}";

            var count = 0;
            $.each(JSONObject, function (row, value) {
                message += String.format("@{R=NTA.Strings;K=NTAWEBJS_VB1_3;E=js}", "<b>", "</b>", "<br>", value.appNamem, value.port, MakeBoth(value.protocol));
                count++;
                if (count > 5) {
                    message += "@{R=NTA.Strings;K=NTAWEBJS_VB1_4;E=js}";
                    return false;
                }
            });

            message += String.format("@{R=NTA.Strings;K=NTAWEBJS_VB1_5;E=js}", "<b>", _ports, "\"", _appName, "</b>");

            var dlg = Ext4.Msg.show({
                title: "@{R=NTA.Strings;K=NTAWEBJS_VB1_6;E=js}",
                msg: message,
                buttons: Ext4.MessageBox.YESNO,
                fn: fnCollisionDlg, 
                cls: 'myCls'
            });

            // make button more visible
            $(".myCls").find('.x-btn-wrap').addClass('msgBtn');
            
            return false;
        }
        return false;
    }
    ChangeTextAndButton();
    DoPostBackIfNeeded();
    return true;
}

function DoPostBackIfNeeded() {
    if (typeof(getPostBackButton) == "function")
        __doPostBack(getPostBackButton(), "OnClick");
}

function jq(myid) {
    return '#' + myid.replace(/(:|\.)/g, '\\$1');
}


function ChangeTextAndButton() {
    // disable click again if port is already monitored here not works for jQuery
    var monitorButton = $('a[id$="' + _monitorButtonID + '"]');
    var monitoredDiv = $('div[id$="' + _monitoredDivID + '"]');
    if (monitorButton != null) {
        monitorButton.click("");
        monitorButton.css('display', 'none');
        monitoredDiv.css('display', 'inline');
    }
}

function ReceiveIPGroupDetails(ipGroupDetails, context) {

    _labelSourceIP = $("[id$='labelSourceIP']");
    _labelDestinationIP = $("[id$='labelDestinationIP']");

    // show values in label or div
    if (_whichLabel == 0) {
        $(_labelSourceIP).text(ipGroupDetails);
    } else if (_whichLabel == 1) {
        $(_labelDestinationIP).text(ipGroupDetails);
    } else { alert('ReceiveIPGroupDetails error: Which label'); }

    _result = "PASSED~~" + ipGroupDetails;

}

function InvokeQuery(fnc) {
    Update();
    fnc(_appString);
}

function ProcessCallBackError(arg, context) {
    window.alert("pcb error - monitor port: " + arg);
}

// addon from addon1.js
function IPControlCanceled() {

    ResetIPGroupControl();

    _divIPControl.hide("slow");

    EnableDisableImputs(false);
    ParentControlFinished2();
}

function ParentControlFinished2() {
    $(_tbPorts).attr("disabled", "true");
}