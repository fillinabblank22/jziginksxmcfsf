﻿SW.NTA = SW.NTA || {};
SW.NTA.Renderers = SW.NTA.Renderers || {};
SW.NTA.Interfaces = SW.NTA.Interfaces || {};

(function (nta) {

    var statusImage = {
        path: '/Orion/images/StatusIcons/',
        up: 'Small-Up.gif',
        down: 'Small-Down.gif',
        external: 'Small-External.gif',
        unmanaged: 'Small-Unmanaged.gif',
        unknown: 'Small-unknown.gif'
    };

    nta.Interface = function (value, md, record) {
        var status = record.data['type'],
            imgSrc = statusImage.path + (statusImage[status] || statusImage.unknown);
        return '<img src="' + imgSrc + '" class="StatusIcon"><span>' + value + '</span>';
    };

})(SW.NTA.Renderers);

(function (ext, nta) {

    var strings = {
        // localized strings
        columns: [
            '',
            '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_InterfaceColumn;E=js}',
            '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_IngressColumn;E=js}',
            '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_EgressColumn;E=js}'
        ],
        columnsIngressText: '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_IngressCell;E=js}',
        columnsEgressText: '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_EgressCell;E=js}',

        // data storage settings
        storeIdProperty: 'id',
        storeFields: ['id', 'name', 'type', 'ingress', 'egress'],
        storeSorters: [{ property: 'name', direction: 'asc' }],
        storeFilters: [{ property: 'name', value: '' }],

        searchField: 'name',
        pagingDisplayMsg: '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_PagingDisplayMsg;E=js}',
        pagingEmptyMsg: '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_PagingEmptyMsg;E=js}',

        dataUpdaterErrorMsg: '@{R=NTA.Strings;K=NTAWEB_JS_Interfaces_UpdateErrorMsg;E=js}'
    };
    
    nta.Grid = function (o) {
        var opt = o,
            el = o.element,
            serviceUrl = o.serviceUrl,
            dataProvider = (o.dataProvider || 'GetInterfaces'),
            dataUpdater = (o.dataUpdater || 'UpdateInterfaces'),
            pageSize = o.pageSize || 20,
            webId = o.webId;

        var callDataUpdater = function (interfacesFn) {
            return function (column, index, checked) {
                SW.Core.Services.callWebService(serviceUrl, dataUpdater,
                    {
                        interfaces: interfacesFn.apply(this, [index]),
                        direction: '' + this.dataIndex,
                        state: (checked === true),
                        webId: webId
                    },
                    function (results) { },
                    function (results) { alert(strings['dataUpdaterErrorMsg']); }
                );
            };
        };

        var updateSingle = callDataUpdater(function (index) {
            var record = this.store.getAt(index);
            return [record.get(record.idProperty)];
        });
        
        var updateAll= callDataUpdater(function () {
            var interfaces = [];
            this.store.each(function (record) {
                interfaces.push(record.get(record.idProperty));
            }, this);
            return interfaces;
        });

        var store = new ext.create('Ext.data.Store', {
            pageSize: pageSize,
            fields: strings['storeFields'],
            proxy: {
                type: 'ajax',
                url: serviceUrl + '/' + dataProvider,
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                actionMethods: { read: 'GET' },
                reader: { type: 'json', root: 'd.Items', totalProperty: 'd.ResultCount', successProperty: 'd.Success', idProperty: strings['storeIdProperty'] },
                writer: { type: 'json' },
                extraParams: { webId: ext.JSON.encode(webId) }
            },
            remoteSort: true,
            autoLoad: true,
            sorters: strings['storeSorters'],
            filters: strings['storeFilters'],
            listeners: {}
        });

        var pagingToolbar = new ext.create('Ext.toolbar.Paging', {
            store: store,
            dock: 'bottom',
            displayInfo: true,
            displayMsg: strings['pagingDisplayMsg'],
            emptyMsg: strings['pagingEmptyMsg'],
            plugins: [{ ptype: "pagesize", uxPageSize: pageSize }]
        });

        var topToolbar = new ext.create('Ext.toolbar.Toolbar', {
            id: 'topToolbar',
            border: 0,
            items: ['->', ' ', {
                xtype: 'searchfield', width: 180, searchFn: function (query) {
                    // clear all filters and set search query
                    store.remoteFilter = false;
                    store.clearFilter();
                    store.remoteFilter = true;
                    store.filters.add({ property: strings['searchField'], value: query });
                    // move bottom toolbar to 1st page (which will trigger reload)
                    if (pagingToolbar) pagingToolbar.moveFirst();
                }
            }]
        });

        var container = $("#" + el);
        var grid = new ext.create('Ext.grid.Panel', {
            store: store,
            renderTo: el,
            height: opt.height || container.height(),
            width: opt.width || container.width(),
            tbar: topToolbar,
            bbar: pagingToolbar,
            viewConfig: {
                markDirty: false
            },
            columns: [
                {
                    text: strings['columns'][1],
                    dataIndex: strings['storeFields'][1],
                    flex: 1,
                    sortable: true,
                    hideable: false,
                    renderer: SW.NTA.Renderers.Interface
                },
                {
                    xtype: "checkcolumn",
                    text: strings['columns'][2],
                    dataIndex: strings['storeFields'][3],
                    columnCellText: strings['columnsIngressText'],
                    width: 150,
                    columnHeaderCheckbox: true,
                    store: store,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    listeners: {
                        checkchange: updateSingle,
                        checkallchange: updateAll
                    }
                },
                {
                    xtype: "checkcolumn",
                    text: strings['columns'][3],
                    dataIndex: strings['storeFields'][4],
                    columnCellText: strings['columnsEgressText'],
                    width: 150,
                    columnHeaderCheckbox: true,
                    store: store,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    listeners: {
                        checkchange: updateSingle,
                        checkallchange: updateAll
                    }
                }
            ]
        });
    };

})(Ext4, SW.NTA.Interfaces);