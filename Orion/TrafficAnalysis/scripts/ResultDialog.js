﻿SW.NTA = SW.NTA || {};
SW.NTA.ConfigureSource = SW.NTA.ConfigureSource || {};
(function (nta) {
    nta.ResultDialog = function(title, okClickCallback) {
        var self = this;
        self.CloseButtonSelector = '.ui-dialog-titlebar-close';
        self.BackoutButtonId = 'ResultDialogBackoutButton';
        self.BackoutButtonIdSelector = '#' + self.BackoutButtonId;
        self.OkButtonId = 'ResultDialogOkButton';
        self.OkButtonIdSelector = '#' + self.OkButtonId;
        self.DialogId = "resultDialog";
        self.DialogIdSelector = "#" + self.DialogId;
        self.StatusTextId = "resultStatusText";
        self.StatusTextIdSelector = "#" + self.StatusTextId;
        self.WarningTextId = "resultWarningScrollBlock";
        self.WarningTextIdSelector = "#" + self.WarningTextId;
        self.title = title;
        self.okClickCallback = (okClickCallback || function () { });

        self.show = function() {
            var encodedTitle = $("<div/>").text(self.title).html();

            $(self.DialogIdSelector).show().dialog({
                width: '550px',
                height: 'auto',
                modal: true,
                overlay: { "background-color": "black", opacity: 0.4 },
                title: encodedTitle
            });

            var closeButton = $(self.CloseButtonSelector);
            closeButton.unbind('click').click(self.okClickHandler);

            var okButton = $(self.OkButtonIdSelector);
            okButton.unbind('click').click(self.okClickHandler).attr('href', '#');
        };

        self.okClickHandler = function () {
            return self.okClickCallback.apply(self, arguments);
        }

        self.setStatusText = function (text, raw) {
            var el = $(self.StatusTextIdSelector);
            return (raw === true) ? el.html(text) : el.text(text);
        };

        self.setWarningHtml = function(html) {
            $(self.WarningTextIdSelector).html(html);
        };

        self.close = function() {
            $(self.DialogIdSelector).dialog("close");
        };

        self.displayBackoutButton = function(visible) {
            var backoutButton = $(self.BackoutButtonIdSelector);

            if (visible) {
                backoutButton.show();
            } else {
                backoutButton.hide();
            }
        }
    }
})(SW.NTA.ConfigureSource);