
var func_onload;
var func_onresize;
func_onload = window.onload;
func_onresize = window.onresize;

window.onload = function() {
    if (func_onload != null) func_onload();
       
    setMaskHeight();
    
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
}

window.onresize = function() {
    if (func_onresize != null) func_onresize();
       
    setMaskHeight();
}

function PageLoaded(sender, args)
{
    setMaskHeight();   
}

function setMaskHeight()
{
	if (document.getElementById)
	{
	    var divMaskID = $get('divMaskID').value;
		var divMask = $get(divMaskID);
		var contentHeight = $get('content').offsetHeight;
		var footerHeight = $get('footer').offsetHeight;
		
		divMask.style.height = contentHeight + footerHeight + 'px';
	}
}