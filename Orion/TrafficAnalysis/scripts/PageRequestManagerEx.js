﻿// FB368383 .. The solution for Asp.net Ajax UpdatePanel Simultaneous Update
// Source page: http://geekswithblogs.net/rashid/archive/2007/08/08/Asp.net-Ajax-UpdatePanel-Simultaneous-Update---A-Remedy.aspx

var PageRequestManagerEx =
{
    isInitialized: false,
    init: function () {
        if (!PageRequestManagerEx.isInitialized) {
            var callQueue = new Array();
            var currentExecutingElement = null;

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_initializeRequest(initializeRequest);
            prm.add_endRequest(endRequest);

            PageRequestManagerEx.isInitialized = true;
        }

        function initializeRequest(sender, args) {
            if (prm.get_isInAsyncPostBack()) {
                //if we are here that means there already a call pending.

                //Get the element which cause the postback
                var postBackElement = args.get_postBackElement();

                //We need to check this otherwise it will abort the request which we made from the end request
                if (currentExecutingElement != postBackElement) {
                    // Grab the event argument value
                    var evArg = $get("__EVENTARGUMENT").value;

                    //Does not match which means it is another control which request the update,
                    // so cancel it temporary and add it in the call queue
                    args.set_cancel(true);
                    Array.enqueue(callQueue, new Array(postBackElement, evArg));
                }

                //Reset it as we are done with our matching
                currentExecutingElement = null;
            }
        }

        function endRequest(sender, args) {
            //Check if we have a pending call
            if (callQueue.length > 0) {
                //Get the first item from the call queue and setting it as current executing item
                currentExecutingElement = Array.dequeue(callQueue);

                var el = currentExecutingElement[0];
                var evArg = currentExecutingElement[1];

                // check, whether the invocation element is on the page
                if($('#'+el.id).length > 0)
                    //Now Post the from which will also fire the initializeRequest
                    prm._doPostBack(el.name, evArg);
            }
        }

    }
}
