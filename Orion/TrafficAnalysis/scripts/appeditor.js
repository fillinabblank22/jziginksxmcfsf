// when window is loaded register all jQuery scripts here
Sys.Application.add_load(Init);

var _ddlSource;
var _labelSourceIP;
var _tbDescription;
var _tbPorts;
var _ddlDestination;
var _labelDestinationIP;
var _ddlProtocol;
var _hfIPGroupSource;
var _hfIPGroupDestination;

var _whichLabel = 0;

function Init() {
    _ddlSource = $("[id$='ddlSourceIP']");
    _labelSourceIP = $("[id$='labelSourceIP']");
    _tbDescription = $("[id$='txtDescription']");
    _tbPorts = $("[id$='txtPorts']");
    _ddlDestination = $("[id$='ddlDestinationIP']");
    _labelDestinationIP = $("[id$='labelDestinationIP']");
    _ddlProtocol = $("[id$='ddlProtocol']");

    // we have to store DDL selected value in the hidden field
    // because a new values added by JavaScript are not evaluated on the server side
    _hfIPGroupSource = $("[id$='hfIPGroupSource']");
    _hfIPGroupDestination = $("[id$='hfIPGroupDestination']");

    _hfIPGroupSource.val("");
    _hfIPGroupDestination.val("");

    _labelSourceIP.hide();
    _labelDestinationIP.hide();

    _ddlSource.change(function() { DdlSourceChanged(_ddlSource.val()) });
    _ddlDestination.change(function() { DdlDestinationChanged(_ddlDestination.val()) });

    RevieweDDL();

    if (_tbDescription.val() != null) {
        // adjust text boxes width
        if (_tbDescription.val().length > 30) {
            _tbDescription.width("462px");
        }
        else {
            _tbDescription.width("231px");
        }

        if (_tbPorts.val().length > 30) {
            _tbDescription.width("462px");
            _tbPorts.width("462px");
        } else {
            _tbPorts.width("231px");
        }

    }
}

function RevieweDDL() {
    // after a while load selected values if any
    if (_ddlSource.val() != null && _ddlSource.val() != "ANY_DESTINATION") {

        window.setTimeout(function() {
            DdlSourceChanged(_ddlSource.val());

            // after a while load selected values if any
            if (_ddlDestination.val() != null && _ddlDestination.val() != "ANY_DESTINATION") {
                window.setTimeout(function() {
                    DdlDestinationChanged(_ddlDestination.val());
                }, 300);
            }
        }, 300);
    } else {
        // after a while load selected values if any
        if (_ddlDestination.val() != null && _ddlDestination.val() != "ANY_DESTINATION") {
            window.setTimeout(function() {
                DdlDestinationChanged(_ddlDestination.val());
            }, 300);
        }

    }
}

function DdlSourceChanged(selectedValue) {

    if (selectedValue == "ANY_DESTINATION") {
        _labelSourceIP.hide("normal");
    } else {
        _labelSourceIP.show("normal");

        // show info about the group
        // call async post back to obtain values from server and show them
        _whichLabel = 0;
    }
    
    $(_hfIPGroupSource).val($(_ddlSource).val());
    $(_hfIPGroupDestination).val($(_ddlDestination).val());
    
    GetIPGroupDetail("GetIPGroup=" + selectedValue);
}

function DdlDestinationChanged(selectedValue) 
{
    if (selectedValue == "ANY_DESTINATION") {
        _labelDestinationIP.hide("normal");
    } else {
        _labelDestinationIP.show("normal");

        // show info about the group
        // call async post back to obtain values from server and show them
        _whichLabel = 1;
    
        GetIPGroupDetail("GetIPGroup=" + selectedValue);
    }

    $(_hfIPGroupSource).val($(_ddlSource).val());
    $(_hfIPGroupDestination).val($(_ddlDestination).val());
    
}

        (function($) { 
          $.fn.disable = function(options){ 
          // plugin defaults 
          var defaults = { 
              disabled: true 
          }; 
          // Extend our default options with those provided. 
          var opts = $.extend({}, defaults, options);
              return this.each(function() {
                  if (typeof this.disabled != 'undefined' && opts.disabled != false) {
                      this.disabled = true;
                  } else {
                      this.disabled = false;
                  }
              });
          } 
  
          //Prototype Methods: extending it with check, uncheck, disableToggle 
            $.fn.extend({
                disableToggle: function() {
                    return this.each(function() {
                        this.disabled = this.disabled == true ? false : true;
                    });
                }
            });
        })(jQuery);

      function ReceiveIPGroupDetails(ipGroupDetails, context) {

         // show values in label or div
          if (_whichLabel == 0) {
              $(_labelSourceIP).text(ipGroupDetails);
          } else if (_whichLabel == 1) {
              $(_labelDestinationIP).text(ipGroupDetails);
          } else { alert('ReceiveIPGroupDetails error: Which label'); }
      }

      function ValidateInputs() {

          // if there is a port/app conflict, validation is invoked twice
          // and on second time textboxes are empty
          if (_tbDescription.val() == null) {
              return true;
          }
        
        if (_tbDescription.val()=="")
        {
            _tbDescription.css("background", "LightPink");
            alert("@{R=NTA.Strings;K=NTAWEBJS_AK0_7;E=js}");
            _tbDescription.click(function() { $(this).css("background", "white") });
            return false;
        }
        if (_tbPorts.val() == "") {
            _tbPorts.css("background", "LightPink");
            alert("@{R=NTA.Strings;K=NTAWEBJS_VB0_3;E=js}");
            _tbPorts.click(function() { $(this).css("background", "white") });
            return false;
        }

        var getPortNumber = function (port) {
            port = port.trim();
            if (!/^[\d]+$/g.test(port)) { // checks if port is a positive integer
                return NaN;
            }
            port = parseFloat(port);
            if (port > 65535) {
                return NaN;
            }
            return port;
        }

        var validatePortsField = function (str) {
            var arr = str.split(',');
            for (var i in arr) {
                if (arr[i].indexOf('-') === -1) {
                    var port = getPortNumber(arr[i]);
                    if (isNaN(port)) {
                        return false;
                    }
                }
                else {
                    // port range
                    var ports = arr[i].split('-');
                    if (ports.length !== 2) {
                        return false;
                    }
                    var port1 = getPortNumber(ports[0]);
                    var port2 = getPortNumber(ports[1]);
                    if (isNaN(port1) || isNaN(port2) || port1 >= port2) {
                        return false;
                    }
                }
            }
            return true;
        }
        // regex port and port ranges
        if (!validatePortsField($(_tbPorts).val()))
        {
            //alert(_tbDescription.val());
            _tbPorts.css("background", "LightPink");
            alert("@{R=NTA.Strings;K=NTAWEBJS_VB0_4;E=js}");
            _tbPorts.click(function() { $(this).css("background", "white") });
            return false;
        }

        return true;
      }
