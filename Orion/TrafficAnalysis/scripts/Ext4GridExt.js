﻿(function (ext) {

    //
    // localized strings
    //
    var strings = {
        emptySearchText: '@{R=NTA.Strings;K=NTAWEB_JS_Ext4Grid_Search;E=js}',
        ingressColumnCellText: '@{R=NTA.Strings;K=NTAWEB_JS_Ext4Grid_Ingress;E=js}',
        egressColumnCellText: '@{R=NTA.Strings;K=NTAWEB_JS_Ext4Grid_Egress;E=js}',

        pageSizeBeforeText: '@{R=NTA.Strings;K=NTAWEB_JS_Ext4Grid_PageSizeText;E=js}',
        pageSizeAfterText: ''
    };

    // 
    // ExtJs 4.0.7 'headerclick' event fix
    // 
    ext.override(ext.grid.header.Container, {
        onHeaderClick: function (header, e, t) {
            header.fireEvent('headerclick', this, header, e, t);
            this.fireEvent('headerclick', this, header, e, t);
        }
    });

    // 
    // ExtJs checkbox grid column
    // 
    ext.define('Ext.ux.CheckColumn', {
        extend: 'Ext.grid.column.Column',
        alias: 'widget.checkcolumn',

        columnHeaderCheckbox: false,
        columnCellText: '&#160;',

        disableColumn: false,
        disableFunction: null,
        disabledColumnDataIndex: null,
        cls: ext.baseCSSPrefix + 'column-header-checkbox',
        checkerOnCls: ext.baseCSSPrefix + 'grid-hd-checker-on',
        checkerNullCls: ext.baseCSSPrefix + 'grid-hd-checker-null',
        checkerDisableCls: ext.baseCSSPrefix + 'grid-hd-checker-disabled',

        constructor: function (config) {
            var me = this;
            me.callParent(arguments);
            me.addEvents('beforecheckchange', 'checkchange', 'beforecheckallchange', 'checkallchange');
            if (config.columnHeaderCheckbox) {
                me.on('headerclick', function () {
                    this.updateAllRecords();
                }, me);

                me.on('render', function (comp) {
                    var grid = comp.up('grid');
                    this.mon(grid, 'reconfigure', function () {
                        if (this.isVisible()) { this.bindStore(); }
                    }, this);

                    if (this.isVisible()) { this.bindStore(); }
                    this.on('show', function () { this.bindStore(); });
                    this.on('hide', function () { this.unbindStore(); });
                }, me);
            }
        },

        bindStore: function () {
            var me = this, store = me.up('grid').getStore();

            me.store = store;
            me.mon(store, 'datachanged', function () { this.onStoreDataUpdate(); }, me);
            me.mon(store, 'update', function (store, rec) { this.onStoreDataUpdate(rec); }, me);
            me.onStoreDataUpdate();
        },

        unbindStore: function () {
            var me = this;
            me.mun(me.store, 'datachanged');
            me.mun(me.store, 'update');
        },

        onStoreDataUpdate: function () {
            var allChecked, image;

            if (!this.store.updatingAll) {
                allChecked = this.getStoreIsAllChecked();
                if (allChecked !== this.allChecked) {
                    this.onToggleHeader(allChecked);
                }
            }
        },

        onToggleHeader: function (isChecked) {
            this.allChecked = isChecked;

            if (!this.el) return;
            if (isChecked === true) this.el.addCls(this.checkerOnCls);
            else this.el.removeCls(this.checkerOnCls);

            if (isChecked == null) this.el.addCls(this.checkerNullCls);
            else this.el.removeCls(this.checkerNullCls);
        },

        getStoreIsAllChecked: function () {
            var me = this, count = 0,
                scnt = me.store.count();
            me.store.each(function (record) {
                count += !record.get(this.dataIndex) ? -1 : 1;
            }, me);
            return (count == scnt) ? true : (count == -scnt ? false : null);
        },

        updateAllRecords: function () {
            var me = this,
                allChecked = !me.allChecked;

            if (me.fireEvent('beforecheckallchange', me, allChecked) !== false) {
                me.store.updatingAll = true;
                me.store.each(function (record) {
                    if (record.get(this.dataIndex) !== allChecked)
                        record.set(this.dataIndex, allChecked);
                }, me);
                me.store.updatingAll = false;
                me.onStoreDataUpdate();
                me.fireEvent('checkallchange', me, null, allChecked);
            }
        },

        processEvent: function (type, view, cell, recordIndex, cellIndex, e) {
            if (type == 'mousedown' || (type == 'keydown' && (e.getKey() == e.ENTER || e.getKey() == e.SPACE))) {
                var record = view.panel.store.getAt(recordIndex),
                    dataIndex = this.dataIndex,
                    checked = !record.get(dataIndex),
                    column = view.panel.columns[cellIndex];
                if (!(column.disableColumn || record.get(column.disabledColumnDataIndex) || (column.disableFunction && column.disableFunction(checked, record)))) {
                    if (this.fireEvent('beforecheckchange', this, recordIndex, checked, record)) {
                        record.set(dataIndex, checked);
                        this.fireEvent('checkchange', this, recordIndex, checked);
                    }
                }
                // cancel selection.
                return false;
            } else {
                return this.callParent(arguments);
            }
        },

        renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
            var disabled = "",
                column = view.panel.columns[colIndex];
            if (column.disableColumn || column.disabledColumnDataIndex || (column.disableFunction && column.disableFunction(value, record)))
                disabled = "-disabled";
            var cssPrefix = ext.baseCSSPrefix,
                cls = [cssPrefix + 'grid-checkbox' + disabled],
                cellText = column.columnCellText || '';

            if (value) {
                cls.push(cssPrefix + 'grid-checkbox-checked' + disabled);
            }
            return '<div class="' + cls.join(' ') + '">' + cellText + '</div>';
        }
    });

    // 
    // ExtJs searchfield 'twintrigger' toolbox control
    // 
    ext.define('Ext.ux.form.SearchField', {
        extend: 'Ext.form.field.Trigger',
        alias: 'widget.searchfield',
        trigger1Cls: ext.baseCSSPrefix + 'form-clear-trigger',
        trigger2Cls: ext.baseCSSPrefix + 'form-search-trigger',

        validationEvent: false,
        validateOnBlur: false,
        hasSearch: false,
        searchFn: function (query) { },
        emptyText: strings['emptySearchText'],

        onTrigger1Click: function () {
            if (!this.hasSearch) return;

            this.setValue('');
            this.searchFn('');
            this.hasSearch = false;

            this.toggleClearBtn(false);
            this.doComponentLayout();
        },
        onTrigger2Click: function () {
            var query = this.getValue();

            if (query.length < 1)
                return this.onTrigger1Click();

            this.searchFn(query);
            this.hasSearch = true;

            this.toggleClearBtn(true);
            this.doComponentLayout();
        },
        initComponent: function () {
            this.callParent(arguments);
            this.on('specialkey', function (f, e) {
                if (e.getKey() == e.ENTER) { this.onTrigger2Click(); }
            }, this);
            this.on('focus', function (f, e) {
                if (this.getRawValue() == '') { this.setValue(''); }
            }, this);
        },
        afterRender: function () {
            this.callParent();
            this.toggleClearBtn(false);
        },
        toggleClearBtn: function (show) {
            this.triggerEl.item(0).setDisplayed(show ? 'block' : 'none');
        }
    });


    // 
    // ExtJs 4 pageSize plugin for Ext.toolbar.Paging
    //
    ext.define('Ext.ux.grid.PageSize', {
        extend : 'Ext.form.field.ComboBox',
        alias : 'plugin.pagesize',
        beforeText : strings['pageSizeBeforeText'],
        afterText : strings['pageSizeAfterText'],
        mode : 'local',
        displayField: 'text',
        valueField : 'value',
        allowBlank : false,
        triggerAction: 'all',
        width : 50,
        maskRe : /[0-9]/,

        // initialize the paging combo after the pagebar is rendered
        init: function(paging) {
            if(this.xzPageSize) {
                paging.store.pageSize = this.uxPageSize;
                this.setValue(this.uxPageSize);
            }
            paging.on('afterrender', this.onInitView, this);
        },
        // create a local store for availabe range of pages
        store: new ext.data.SimpleStore({
            fields: ['text', 'value'],
            data: [
                ['10', 10], 
                ['20', 20], 
                ['50', 50],
                ['100', 100],
                ['200', 200]
            ]
        }),
        // assing the select and specialkey events for the combobox after the pagebar is rendered.
        onInitView: function(paging) {
            var r = paging.items.map.refresh.itemId,
                i = paging.items.indexOfKey(r)+1;
            this.setValue(paging.store.pageSize);
            paging.add(i, '-', this.beforeText, this, this.afterText);
            this.on('select', this.onPageSizeChanged, paging);
            this.on('specialkey', function(combo, e) {
                if (e.getKey() == e.ENTER) {
                    this.onPageSizeChanged.call(paging, this);
                }
            });
        },
        // refresh the page when the value is changed
        onPageSizeChanged: function(combo) {
            this.store.pageSize = parseInt(combo.getRawValue(), 10);
            this.moveFirst();
        }
    });
    })(Ext4);