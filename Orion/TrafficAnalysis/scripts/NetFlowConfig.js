var configureNetFlowEnabled = function(grid) {
    var nodeIds = grid.GetSelectedNodeIDs();

    return nodeIds.length === 1;
}

var configureNetFlowCallback = function (grid) {
    var nodeId = grid.GetSelectedNodeIDs()[0];
    window.location.href = "/ui/nta/netflowDevices/startNetflowEnabler/nodeId/" + nodeId;
}