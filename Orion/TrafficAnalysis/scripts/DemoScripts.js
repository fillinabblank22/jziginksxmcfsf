﻿
function IsDemo() 
{
    if (document.getElementById("demoHidden") == null) 
    {
        return false;
    }

    //return (document.getElementById("demoHidden").value.toLowerCase() == "true");
    return ($('#demoHidden')[0].value.toLowerCase() == "true");
}

function DisplayWarning(warningText) 
{
    message = String.format("@{R=NTA.Strings;K=NTAWEBJS_AK0_21;E=js}", "\r\n\t",  warningText);

    try 
    {
        demoAdminBlockedMessage(message);
    }
    catch (ex) 
    {
        alert(message);
    }
}