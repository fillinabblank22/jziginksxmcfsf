﻿SW.NTA = SW.NTA || {};
SW.NTA.ConfigureSource = SW.NTA.ConfigureSource || {};
(function (nta) {
    nta.ProgressDialogBehaviour = function (strings, config) {

        var timeoutHandle;
        var checkProgressTimeout = 500;
        var configurationStatus = config.configurationStatus;

        var progressDialog;
        var resultDialog;

        function isEmpty(value) {
            return !value || $.trim(value) === '';
        };

        var onProgressReceived = function(result) {
            if (isEmpty(result)) {
                scheduleNextProgressChecking();
                return;
            }

            var data = result && result.d ? result.d : result,
                    info = (typeof (data) !== "string") ? data : $.parseJSON(data);

            switch (info.status) {
                case configurationStatus.InProgress:
                    progressDialog.enableCancellation(true);
                    progressDialog.setProgressBar(info.progress * 100, 100);
                    var label = String.format(strings.configurationLabelFormat, config.nodeCaption());
                    progressDialog.setStatusText(label);
                    scheduleNextProgressChecking();
                    break;

                case configurationStatus.Finished:
                    progressDialog.enableCancellation(false);
                    clearTimeout(timeoutHandle);
                    progressDialog.setProgressBar(100, 100);
                    progressDialog.close();

                    var msg = strings['configurationComplete'];
                    if (config.backout) msg = strings['backoutComplete'];
                    if (!!progressDialog.isConfigurationCanceled) msg = strings['configurationCompleteWithCancel'];

                    displayResultDialog(msg, false, false, true);
                    break;

                case configurationStatus.Cancelled:
                    progressDialog.setProgressBar(100, 100);
                    progressDialog.close();

                    var msg = strings['cancellingSuccessful'];
                    displayResultDialog(msg, false);
                    break;

                case configurationStatus.Error:
                    progressDialog.setProgressBar(100, 100);
                    progressDialog.close();

                    displayResultDialog(info.errorMessage, false);
                    break;

                case configurationStatus.FinishedWithWarning:
                    progressDialog.setProgressBar(100, 100);
                    progressDialog.close();
                    displayResultDialog(info.errorMessage, info.warningHtml, true);
                    break;

                default:
                    scheduleNextProgressChecking();
            }
        };

        var scheduleNextProgressChecking = function() {
            timeoutHandle = setTimeout(getConfigurationProgress, checkProgressTimeout);
        };

        var getConfigurationProgress = function() {
            PageMethods.ReportNetflowSnmpConfigurationProgress(
                config.jobId,
                config.webId,
                onProgressReceived,
                function(error) {
                    alert(strings.errorDuringConfiguration);
                    progressDialog.close();
                });
        };

        var cancelConfiguration = function() {
            clearTimeout(timeoutHandle);
            if (confirm(strings.cancelConfigurationConfirmation)) {
                progressDialog.isConfigurationCanceled = true;
                progressDialog.enableCancellation(false);
                progressDialog.setStatusText(strings.cancellingConfiguration);
                PageMethods.CancelConfiguration(
                    config.jobId,
                    function() { // cancel initiated
                        getConfigurationProgress();
                    },
                    function (error) {
                        alert(String.format(strings.errorDuringCancellingConfiguration, error.get_message()));
                        progressDialog.close();
                    });
            } else {
                getConfigurationProgress();
            }
        };

        var acceptConfiguration = function() {
            this.close();
            __doPostBack(config.showConfigurationResultInvokerId, '');
        };

        var displayResultDialog = function(message, warningHtml, showBackout, raw) {
            setTimeout(function() {
                resultDialog.show();
                resultDialog.setStatusText(message, raw);
                resultDialog.setWarningHtml(warningHtml);
                if (showBackout) $('#resultWarningBottomText').show();
                else $('#resultWarningBottomText').hide();
                resultDialog.displayBackoutButton(showBackout);
            }, 1);
        };

        $(function () {
            config.jobId = $(config.jobIdEl).val();
            if (isEmpty(config.jobId)) return;

            config.webId = $(config.webIdEl).val();
            config.backout = $(config.backoutIdEl).val() != '';

            // begin receiving progress
            progressDialog = new ProgressDialog(strings.progressDialogTitle);
            progressDialog.setStatusText(strings.startingConfiguration);
            progressDialog.show(cancelConfiguration);
            progressDialog.isConfigurationCanceled = false;
            progressDialog.enableCancellation(false);

            resultDialog = new SW.NTA.ConfigureSource.ResultDialog(strings.progressDialogTitle, acceptConfiguration);

            scheduleNextProgressChecking();
        });
    };
})(SW.NTA.ConfigureSource);
