﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Utils;
using SolarWinds.Netflow.Web.Reporting.Filters;

public partial class Orion_TrafficAnalysis_Utils_AbsolutePeriod : UserControl
{
    #region properties
    public string ValidationGroup { get; set; }

    public string CIDabsStartDate { get { return this.txtStartDate.ClientID; } }

    public string CIDabsStartTime { get { return this.txtStartTime.ClientID; } }

    public string CIDabsEndDate { get { return this.txtEndDate.ClientID; } }

    public string CIDabsEndTime { get { return this.txtEndTime.ClientID; } }

    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            return TimePeriodUiFilter.Parse(string.Concat(DateTime.Parse(this.DateTimeStart).ToUtcSortableString(), "~", DateTime.Parse(this.DateTimeEnd).ToUtcSortableString()));
        }
        set
        {
            SelectTimePeriod(value);
        }
    }

    #endregion

    public string DateTimeStart
    {
        get { return this.txtStartDate.Text + " " + this.txtStartTime.Text; }
    }

    public string DateTimeEnd
    {
        get { return this.txtEndDate.Text + " " + this.txtEndTime.Text; }
    }

    public string TimeSeparator
    {
        get { return new NetFlowCommon().TimeSeparator; }
    }

    public string DateFormat
    {
        get { return new NetFlowCommon().DateFormat; }
    }

    public bool TimeFormatIs24
    {
        get { return new NetFlowCommon().TimeFormatIs24; }
    }

    public string DatePickerRegionalSettings
    {
        get
        {
            return new NetFlowCommon().GetDatePickerRegionalSettings();
        }
    }    

    private string SCRIPT(string pStartDate, string pEndDate, string pStartTime, string pEndTime)
    {
        string ret = @" 

            $(document).ready(function() {
          
                var regionalSettings =  " + DatePickerRegionalSettings + @";
                $.dpStart = $('#" + pStartDate + @"');
                $.dpEnd = $('#" + pEndDate + @"');
                
                jQuery.each([$.dpStart, $.dpEnd], function() {

                this.datepicker($.extend({
                    showOn: 'both',
                    duration: 'fast',
                    buttonImage: '/Orion/js/extjs/resources/images/default/shared/calendar.gif',
                    buttonImageOnly: true,
                    closeAtTop: false, 
                    mandatory: true,
                    showButtonPanel: false
                    }, regionalSettings));

                });

                $.tpStart = $('#" + pStartTime + @"');
                $.tpEnd = $('#" + pEndTime + @"');

                jQuery.each([$.tpStart, $.tpEnd], function() {

                this.timePicker({   
                    separator:regionalSettings.timeSeparator, 
                    show24Hours:regionalSettings.show24Hours,
                    step:15,
                    endTime:new Date(0, 0, 0, 23, 45, 0)
                    });
                this.attr('onchange','');

                });
            
            //
            if ($(""input[id$='radAbsolute']"").attr('checked'))
            {
                $.dpStart.datepicker('enable');
                $.dpEnd.datepicker('enable');
            }
            else
            {
               $.dpStart.datepicker('disable');
               $.dpEnd.datepicker('disable');
            }


            });
            ";

        return ret;
    }


    private void RegisterScript()
    {
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(this.Page, "~/Orion/js/jquery/timePicker.css");

        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "datePickerDefaults" + this.ClientID, SCRIPT(txtStartDate.ClientID, txtEndDate.ClientID, txtStartTime.ClientID, txtEndTime.ClientID), true);
    }

    protected string CustomValidateFunctionName
    {
        get { return "validateAbsoluteTimePeriod_" + ClientID; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // for Search Endpoints resource - javascript can't be registered if control is disabled
        if (this.Visible == false)
        {
            return;
        }

        ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
            this.Controls.AddAt(0, sm);
        }

        RegisterScript();

        if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), CustomValidateFunctionName))
        {
            string validateScript =
                string.Format(
                    @"function {0}(source, clientside_arguments) {{
	var validationResult = false;
    $.ajax({{ type: ""POST"", url: ""/Orion/TrafficAnalysis/TimePeriod/TimePeriodValidator.asmx/ValidateTime"",
		data: ""{{'startDateString':'"" + $(""#{1}"").val() +
        ""', 'startTimeString':'"" + $(""#{2}"").val() +
        ""', 'endDateString':'"" + $(""#{3}"").val() +
        ""', 'endTimeString':'"" + $(""#{4}"").val() + ""'}}"",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (result) {{ 
            validationResult = result.d.IsValid;
            if (validationResult == true) {{
                $('#{5}').attr('periodName', result.d.PeriodName);
                $('#{5}').attr('periodTitle', result.d.PeriodTitle);
            }}
        }},
        error: function(error) {{ validationResult = false; }}
        }});
	
	clientside_arguments.IsValid = validationResult;
}}",
                    CustomValidateFunctionName, this.txtStartDate.ClientID, this.txtStartTime.ClientID, this.txtEndDate.ClientID, this.txtEndTime.ClientID, this.ClientID);

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), CustomValidateFunctionName, validateScript, true);
        }

        valPeriodCustom.ClientValidationFunction = CustomValidateFunctionName;
        SetupValidator(valPeriodCustom);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ScriptManager.GetCurrent(this.Page).Services.Add(new ServiceReference("~/Orion/TrafficAnalysis/TimePeriod/TimePeriodValidator.asmx"));
    }

    private void SetupValidator(BaseValidator validator)
    {
        if (!string.IsNullOrEmpty(this.ValidationGroup))
        {
            validator.ValidationGroup = this.ValidationGroup;
        }
    }

    protected void valPeriodCustom_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SolarWinds.Netflow.Web.Reporting.TimePeriodValidator.ValidateAbsoluteTimePeriod(this.txtStartDate.Text, this.txtStartTime.Text,
                                                                      this.txtEndDate.Text, this.txtEndTime.Text).IsValid;
    }

    private void SelectTimePeriod(TimePeriodUiFilter periodFilter)
    {
        this.txtStartDate.Text = periodFilter.StartTime.ToShortDateString();
        this.txtStartTime.Text = periodFilter.StartTime.ToShortTimeString();

        this.txtEndDate.Text = periodFilter.EndTime.ToShortDateString();
        this.txtEndTime.Text = periodFilter.EndTime.ToShortTimeString();
    }

    protected ScriptManager GetScriptManager()
    {
        ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
        }


        ScriptManager ret = ScriptManager.GetCurrent(Page);

        if (ret == null)
        {
            throw new HttpException("A ScriptManager control must exist on the current page.");
        }

        return ret;
    }

    protected override void OnPreRender(EventArgs e)
    {
        ScriptManager sm1 = ScriptManager.GetCurrent(this.Page);

        if (sm1 == null)
        {
            sm1 = new ScriptManager();
            this.Controls.AddAt(0, sm1);
        }

        base.OnPreRender(e);

        ScriptManager sm2 = ScriptManager.GetCurrent(this.Page);

        if (sm2 == null)
        {
            sm2 = new ScriptManager();
            this.Controls.AddAt(0, sm2);
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        //important !!! neccessary to render div tags 
        //if not present client side api will not be able work with the control

        writer.AddAttribute("id", this.ClientID);
        writer.RenderBeginTag(HtmlTextWriterTag.Div);

        base.Render(writer);

        writer.RenderEndTag();
    }

    const string clientEnableTemplate =
            @"
var absStartDate = document.getElementById('{0}');
var absStartTime = document.getElementById('{1}');
var absEndDate = document.getElementById('{2}');
var absEndTime = document.getElementById('{3}');

absStartDate.disabled = {4};
absStartTime.disabled = {4};
absEndDate.disabled = {4};
absEndTime.disabled = {4};
//$('.ui-datepicker-trigger').css('visibility', 'visible');
$(absStartDate).datepicker('{5}');
  $(absEndDate).datepicker('{5}');
ValidatorEnable(document.getElementById('{6}'), {7});
";

    public string GetClientEnableScript()
    {
        return string.Format(clientEnableTemplate, CIDabsStartDate, CIDabsStartTime, CIDabsEndDate, CIDabsEndTime, "false", "enable", this.valPeriodCustom.ClientID, "true");
    }

    public string GetClientDisableScript()
    {
        return string.Format(clientEnableTemplate, CIDabsStartDate, CIDabsStartTime, CIDabsEndDate, CIDabsEndTime, "true", "disable", this.valPeriodCustom.ClientID, "false");
    }

    public string GetClientStoreState()
    {
        string storeStateScript = @"$('#{0}').prop('lastValue', $('#{0}').val());";
        
        return string.Concat(
            string.Format(storeStateScript, this.txtStartDate.ClientID),
            string.Format(storeStateScript, this.txtStartTime.ClientID),
            string.Format(storeStateScript, this.txtEndDate.ClientID),
            string.Format(storeStateScript, this.txtEndTime.ClientID)
            );
    }

    public string GetClientRestoreState()
    {
        string restoreStateScript = @"if ($('#{0}').prop('lastValue')) {{ $('#{0}').val($('#{0}').prop('lastValue')); }}";
        
        return string.Concat(
            string.Format(restoreStateScript, this.txtStartDate.ClientID),
            string.Format(restoreStateScript, this.txtStartTime.ClientID),
            string.Format(restoreStateScript, this.txtEndDate.ClientID),
            string.Format(restoreStateScript, this.txtEndTime.ClientID)
            );
    }
}
