using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Utils_PeriodSelector : UserControl
{
    [PersistenceMode(PersistenceMode.Attribute)]
    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            if (this.radNamed.Checked)
            {
                return TimePeriodUiFilter.Parse(this.lbxNamedPeriods.SelectedValue);
            }
            else if (this.radRelative.Checked)
            {
                return this.relativePeriod.PeriodFilter;
            }
            else if (this.radAbsolute.Checked)
            {
                return this.absolutePeriod.PeriodFilter;
            }
            else
            {
                return (this.Page is NetflowSummaryView) ?
                    TimePeriodUiFilter.Parse(GlobalSettingsDALCached.InstanceInterface.DefaultSummaryTimePeriod) :
                    TimePeriodUiFilter.Parse(GlobalSettingsDALCached.DefaultDetailTimePeriod);
            }
        }
        set
        {
            foreach (ListItem item in this.lbxNamedPeriods.Items)
            {
                if (value.PeriodName.Equals(item.Value, StringComparison.InvariantCultureIgnoreCase))
                {
                    item.Selected = true;
                    break;
                }
            }

            this.relativePeriod.PeriodFilter = value;
            this.absolutePeriod.PeriodFilter = value;

            this.periodNameHiddenField.Value = value.PeriodName;
        }
    }

    /// <summary>
    /// If set to true it will force initialization on postback as if there was no postback
    /// </summary>
    public bool ForceInitialization
    {
        get;
        set;
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool ValidationEnabled { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ValidationGroup = "timePeriodValGroup_" + ClientID;
        this.relativePeriod.ForceInitialization = ForceInitialization;

        this.radNamed.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radNamed.Attributes.Add("onclick", this.radNamed.Attributes["onselect"]);
        this.radRelative.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radRelative.Attributes.Add("onclick", this.radRelative.Attributes["onselect"]);
        this.radAbsolute.Attributes.Add("onselect", this.ActuatorScriptName + "(); return true;");
        this.radAbsolute.Attributes.Add("onclick", this.radAbsolute.Attributes["onselect"]);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        ScriptManager sm = ScriptManager.GetCurrent(this.Page);

        if (sm == null)
        {
            sm = new ScriptManager();
            this.Controls.AddAt(0, sm);
        }


        if (!this.IsPostBack || ForceInitialization)
        {
            string[] namedPeriodNames = new string[]
                    {
                        "Last 15 Minutes",
                        "Last 30 Minutes",
                        "Last Hour",
                        "Last 2 Hours",
                        "Last 24 Hours",
                        "Today"
                    };

            foreach (string namedPeriodName in namedPeriodNames)
            {
                TimePeriodUiFilter periodFilter = TimePeriodUiFilter.Parse(namedPeriodName);

                this.lbxNamedPeriods.Items.Add(new ListItem(TimePeriodI18nHelper.GetLocalizedRelativeTimePeriodTitle(periodFilter.RelativeTimePeriodInfo), periodFilter.PeriodName));
            }
        }

        absolutePeriod.ValidationGroup = this.ValidationGroup;
        relativePeriod.ValidationGroup = this.ValidationGroup;
    }

    public string ValidationGroup
    {
        get;
        set;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Setup Client Scripts for handling controls
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), this.ActuatorScriptName, this.BuildActuatorScript(), true);
        string script = String.Format("Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded({0}) \n", this.ActuatorScriptName);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ActuatorScriptName + "_Startup", script, true);
    }

    private const string RADIO_CHANGE_TEMPLATE = @"

function {0}()
{{
    var namedDropDown = document.getElementById('{1}');
    var radNamed = document.getElementById('{6}');
    var radRelative = document.getElementById('{7}');
    var radAbsolute = document.getElementById('{8}');
    
    if(radNamed.checked == true)
    {{
        namedDropDown.disabled = false;
    }}
    else
    {{
        namedDropDown.disabled = true;
    }}

    if(radRelative.checked == true)
    {{
        {2}
    }}
    else
    {{
        {3}
    }}

    if(radAbsolute.checked == true)
    {{
        {4}
    }}
    else
    {{
        {5}
    }}
}}
";

    private string BuildActuatorScript()
    {
        return string.Format(
            RADIO_CHANGE_TEMPLATE,
            this.ActuatorScriptName, // 0
            this.lbxNamedPeriods.ClientID, //1
            this.relativePeriod.GetClientEnableScript(),//2
            this.relativePeriod.GetClientDisableScript(),//3
            this.absolutePeriod.GetClientEnableScript(),//4
            this.absolutePeriod.GetClientDisableScript(),//5
            radNamed.ClientID,//6
            radRelative.ClientID,//7
            radAbsolute.ClientID//8
            );
    }

    private string ActuatorScriptName
    {
        get { return this.ClientID + "_Actuate"; }
    }

    protected string StoreStateScript
    {
        get { return absolutePeriod.GetClientStoreState(); }
    }

    protected string RestoreStateScript
    {
        get { return absolutePeriod.GetClientRestoreState(); }
    }

    public string PeriodNameHiddenFieldValue
    {
        get { return this.periodNameHiddenField.Value; }
    }

    public string PeriodNameHiddenFieldCliendId
    {
        get { return this.periodNameHiddenField.ClientID; }
    }
}
