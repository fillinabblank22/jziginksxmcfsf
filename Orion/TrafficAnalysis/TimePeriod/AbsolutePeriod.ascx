﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AbsolutePeriod.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_AbsolutePeriod" %>

<script type="text/javascript" src="/Orion/js/jquery/ui.datepicker.js"></script>
<script type="text/javascript" src="/Orion/js/jquery/jquery.timePicker.js"></script>

<table>
    <tr>
        <td>
            <span id="twoLinesFromDiv" runat="server"><%= Resources.NTAWebContent.NTAWEBCODE_VB0_44%></span>
        </td>
        <td>
            <asp:TextBox ID="txtStartDate" runat="server"  Width="75px" CssClass="PeriodSelector" />  
            <asp:TextBox ID="txtStartTime" runat="server"  Width="60px" />
        </td>
    </tr>
    <tr>
        <td>
            <span id="twoLinesToDiv" runat="server"><%= Resources.NTAWebContent.NTAWEBCODE_VB0_46%></span>
        </td>
        <td>
            <asp:TextBox ID="txtEndDate" runat="server" Width="75px" />
            <asp:TextBox ID="txtEndTime" runat="server" Width="60px" />
        </td>
    </tr>
</table>

<asp:CustomValidator ID="valPeriodCustom" runat="server"
    OnServerValidate="valPeriodCustom_ServerValidate"
    Display="Dynamic"
    ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_MK0_1%>"
    Enabled="true">
    <a onclick="return false;" id="valIcon" title="<%$ Resources:NTAWebContent,NTAWEBDATA_MK0_1%>" runat="server" href="#" class="ntasValIcon">&nbsp;</a></asp:CustomValidator>

