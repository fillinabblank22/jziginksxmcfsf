using System;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.UI.Localizer;

public partial class Orion_TrafficAnalysis_Utils_PeriodMenu : System.Web.UI.UserControl
{
    private NetflowView netflowView;
    private TimePeriodUiFilter periodFilter;

    public TimePeriodUiFilter PeriodFilter
    {
        get { return periodFilter; }
        set
        {
            this.periodFilter = value;
            this.periodSelector.PeriodFilter = this.periodFilter;
        }
    }

    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    public bool DontPostbackWhenSubmit { get; set; }
    
    public bool NoLeftMargin { get; set; }

    public bool NoBold { get; set; }

    public string PeriodTitleID { get { return this.PeriodTitle.ClientID; } }

    /// <summary>
    /// Normally is PeriodMenu aligned to the left edge of Period Title (dropdown button).
    /// If this property is set then PeriodMenu is aligned to the right edge of the dropdown button.
    /// </summary>
    public bool AlignRight { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (NoLeftMargin)
        {
            PeriodTitle.Attributes.CssStyle.Add("font-size", "small");
        }

        if (NoBold)
        {
            PeriodTitle.Attributes.CssStyle.Add("font-weight", "normal");
        }

        // prepare javascript controls        
        PeriodDropDown.Attributes.Add("onclick", string.Format("ShowMenu_{0}('{1}','{2}', 'true', '{3}');  return false;",
            periodSelector.ClientID, PeriodMenu.ClientID, PeriodTitle.ClientID, AlignRight.ToString().ToLower()));

        PeriodDropDown.HRef = this.Page.Request.Url.PathAndQuery + "#";
        btnCancel.OnClientClick = string.Format("HideMenu_{0}('{1}'); return false;", periodSelector.ClientID, PeriodMenu.ClientID);

        // Include script for TimePeriod menu
        string script = this.GetTimePeriodMenuJS(this.periodSelector.ClientID);
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
          string.Format("TimePeriodPeriodMenu{0}", this.ClientID), script, true);

        if (DontPostbackWhenSubmit) //It's in Search by Endpoint, not on view
        {
            btnSubmit.OnClientClick = string.Format("if (timePeriodValidate_{0}()) {{ ChangePeriodLabel_{0}('{1}','{2}'); }} return false;",
                periodSelector.ClientID, PeriodMenu.ClientID, PeriodTitle.ClientID);

            this.PeriodFilter = TimePeriodUiFilter.Parse("Last 5 Minutes"); //Some default value which should be replaced later by owner. This value can't be null after OnInit
        }
        else //It's on view, not in Search by Endpoint
        {
            btnSubmit.OnClientClick = string.Format("return timePeriodValidate_{0}();", this.periodSelector.ClientID);

            this.netflowView = (this.Page as NetflowView);
            if (netflowView == null) //If we are not on netflowView we have to hide this control (this control is in master page)
            {
                this.Visible = false;
                return;
            }
        }

        this.Visible = true;

        ControlHelper.AddStylesheet(this.Page, "/Orion/TrafficAnalysis/styles/Netflow.css");

        if (!this.IsPostBack)
        {
            if (PeriodFilter == null)
                PeriodFilter = netflowView.NetflowObject.Filters.PeriodFilterUi();

            this.periodSelector.PeriodFilter = this.PeriodFilter;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (DontPostbackWhenSubmit && IsPostBack && !string.IsNullOrEmpty(this.periodSelector.PeriodNameHiddenFieldValue))
        {
            this.PeriodFilter = TimePeriodUiFilter.Parse(this.periodSelector.PeriodNameHiddenFieldValue);
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        IFilterCollection filters = this.netflowView.NetflowObject.Filters;

        try
        {
            filters.PeriodFilter = this.periodSelector.PeriodFilter;
        }
        catch (Exception ex)
        {
            // do nothing; we still want to do the redirect            
            log.Error("Error while setting PeriodFilter: " + ex.Message);
        }

        Response.Redirect(this.netflowView.GetURLForFilterID(filters.FilterID));
    }

    /// <summary>
    /// JavaScript for TimePeriod menu.
    /// </summary>
    private string GetTimePeriodMenuJS(string clientId)
    {
        return string.Format(@"
    var activeMenu = """";

    function HideMenu_{0}(periodMenuId) {{
        $(""#"" + periodMenuId).hide(); 
        activeMenu = """";
    }}

    // for flow direction selector
    function HideMenu(periodMenuId)
    {{
        HideMenu_{0}(periodMenuId);
    }}

    // for flow direction selector
    function ShowMenu(periodMenuId, periodTitle, pSetPopup)
    {{
        ShowMenu_{0}(periodMenuId, periodTitle, pSetPopup, 'false');
    }}

    // PeriodDropDown's onClick
    function ShowMenu_{0}(periodMenuId, periodTitle, pSetPopup, pAlignRight) {{

        var $_periodMenu = $(""#"" + periodMenuId);
        var $_periodTitle = $(""#"" + periodTitle);
       
        if (activeMenu != """") {{
            HideMenu_{0}(activeMenu);
        }}

        activeMenu = periodMenuId;

        if (pAlignRight === 'true') {{
            $_periodMenu.css(""left"", $_periodTitle.position().left + $_periodTitle.width() - $_periodMenu.width());
        }}
        else {{
            $_periodMenu.css(""left"", $_periodTitle.position().left);
        }}

        $_periodMenu.css(""top"", $_periodTitle.position().top+25);
        $_periodMenu.show();

        if (pSetPopup) {{
            SetSelectedPeriodName_{0}($(""#{1}"").val());
        }}

        return false;
    }}

    function ChangePeriodLabel_{0}(periodMenuId, periodTitle) {{
        var textStr = GetSelectedPeriodName_{0}();
        var $_div = $(""#"" + periodTitle);
        var $_div1 = $(""#divSE1"");
        var $_div2 = $(""#divSE2"");

        if (textStr.length > 16) {{
            //  make css smaller
            $_div.css(""font-size"", ""11px"");
            $_div1.css(""height"", ""40px"");
            $_div2.css(""height"", ""40px"");
        }}
        else {{
            // make css bigger
            $_div.css(""font-size"", ""small"");
            $_div1.css(""height"", ""auto"");
            $_div2.css(""height"", ""auto"");
        }}

        $_div.html(textStr);
        HideMenu_{0}(periodMenuId)

        return false;
    }}", clientId, this.periodSelector.PeriodNameHiddenFieldCliendId);

    }
}
