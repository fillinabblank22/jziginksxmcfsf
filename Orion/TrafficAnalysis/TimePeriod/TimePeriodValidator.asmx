﻿<%@ WebService Language="C#" Class="TimePeriodValidator" %>

using System;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Netflow.Web.Reporting;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TimePeriodValidator  : WebService
{
    private static Log log = new Log();

    [WebMethod]
    public TimePeriodValidationResult ValidateTime(string startDateString, string startTimeString, string endDateString, string endTimeString)
    {
        return SolarWinds.Netflow.Web.Reporting.TimePeriodValidator.ValidateAbsoluteTimePeriod(startDateString, startTimeString,
                                                                                        endDateString, endTimeString);
    }
}