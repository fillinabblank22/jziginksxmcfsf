<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodSelector.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_PeriodSelector" %>
<%@ Register TagPrefix="nta" TagName="RelativePeriod" Src="~/Orion/TrafficAnalysis/TimePeriod/RelativePeriod.ascx" %>
<%@ Register TagPrefix="nta" TagName="AbsolutePeriod" Src="~/Orion/TrafficAnalysis/TimePeriod/AbsolutePeriod.ascx" %>




<script type="text/javascript" language="javascript">

var $_radName_<%=this.ClientID %>;              // radio button Named Type Period
var $_radRelative_<%=this.ClientID %>;          // radio button Relative Time Period
var $_radAbsolute_<%=this.ClientID %>;          // radio button Absolute Time Period

var $_tbRelative_<%=this.ClientID %>;           // text box Relative Time Period
var $_tbAbsoluteStartDate_<%=this.ClientID %>;  // text box Absolute Start Date field
var $_tbAbsoluteEndDate_<%=this.ClientID %>;    // text box Absolute End Date field
var $_tbAbsoluteStartTime_<%=this.ClientID %>;  // text box Absoulte Start Time field
var $_tbAbsoluteEndTime_<%=this.ClientID %>;    // text box Absoulte End Time field

$(document).ready(function() {
    Setup_<%=this.ClientID %>();
});


function Setup_<%=this.ClientID %>()
{
    $_radName_<%=this.ClientID %> = $("#" + '<%=this.radNamed.ClientID %>');
    $_radRelative_<%=this.ClientID %> = $("#" + '<%=this.radRelative.ClientID %>');
    $_radAbsolute_<%=this.ClientID %> = $("#" + '<%=this.radAbsolute.ClientID %>');

    $_tbRelative_<%=this.ClientID %> = $("#" + '<%=this.relativePeriod.TxtQuantityClientID%>');
    $_tbAbsoluteStartDate_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsStartDate %>');
    $_tbAbsoluteEndDate_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsEndDate %>');
    $_tbAbsoluteStartTime_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsStartTime %>');
    $_tbAbsoluteEndTime_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsEndTime %>');
}

function timePeriodValidate_<%=this.ClientID %>() {
    if (Page_ClientValidate('<%=this.ValidationGroup %>')) {
        <%= StoreStateScript %>

        var periodName;
        var periodTitle;
        if ($_radName_<%=this.ClientID %>.attr("checked")) {
            periodName = $("#" + '<%=this.lbxNamedPeriods.ClientID %>').val();
            periodTitle = $("#" + '<%=this.lbxNamedPeriods.ClientID %> option:selected').text();
        } else if ($_radRelative_<%=this.ClientID %>.attr("checked")) {
            periodName = "Last " + $("#" + '<%=this.relativePeriod.TxtQuantityClientID%>').val() + " " + $("#" + '<%=this.relativePeriod.LbxTimeTypeClientID %>').val();
            periodTitle = "<%= Resources.NTAWebContent.NTAWEBDATA_VB0_51%> " + $("#" + '<%=this.relativePeriod.TxtQuantityClientID%>').val() + " " + $("#" + '<%=this.relativePeriod.LbxTimeTypeClientID %> option:selected').text();
        } else if ($_radAbsolute_<%=this.ClientID %>.attr("checked")) {
            periodName = $("#" + '<%=this.absolutePeriod.ClientID %>').attr('periodName');
            periodTitle = $("#" + '<%=this.absolutePeriod.ClientID %>').attr('periodTitle');
        }

        $("#" + '<%= this.periodNameHiddenField.ClientID %>').val(periodName);
        $("#" + '<%= this.periodNameHiddenField.ClientID %>').attr('periodTitle', periodTitle);

        return true;
    }

    return false;
}

// used by javascript in NetFlowMenus.js for Search by Endpoint
function GetSelectedPeriodName_<%=this.ClientID%>() {
    return $("#" + '<%= this.periodNameHiddenField.ClientID %>').attr('periodTitle');
}

function SetSelectedPeriodName_<%=this.ClientID %>(pPeriod) {
    $_dateStart_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsStartDate %>');
    $_dateEnd_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsEndDate %>');
    $_timeStart_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsStartTime %>');
    $_timeEnd_<%=this.ClientID %> = $("#" + '<%=this.absolutePeriod.CIDabsEndTime %>');

    $_dateStart_<%=this.ClientID %>.datepicker('disable');
    $_dateEnd_<%=this.ClientID %>.datepicker('disable');
    
    if (!pPeriod) {
        $_radName_<%=this.ClientID %> = $("#" + '<%=this.radNamed.ClientID %>');
        $_name_<%=this.ClientID %> = $("#" + '<%=this.lbxNamedPeriods.ClientID %>');
        $_name_<%=this.ClientID %>.val("Last Hour");
        return;
    }

    // named
    $_radName_<%=this.ClientID %> = $("#" + '<%=this.radNamed.ClientID %>');
    $_name_<%=this.ClientID %> = $("#" + '<%=this.lbxNamedPeriods.ClientID %>');
    if (pPeriod == "Last Hour" || pPeriod == "Last 2 Hours" || pPeriod == "Last 24 Hours" || pPeriod == "Today" || pPeriod == "Last 30 Minutes" || pPeriod=="Last 15 Minutes") {
 
        $_radName_<%=this.ClientID %>.attr("checked", "true");
        $_radName_<%=this.ClientID %>.click();
        $_name_<%=this.ClientID %>.val(pPeriod);
  
        return;
    }
    
    // relative
    var arr = pPeriod.split(" ");
    if (arr[0] == "Last") {
        $("#" + '<%=this.relativePeriod.TxtQuantityClientID%>').val(arr[1]);
        $_radRelative_<%=this.ClientID %> = $("#" + '<%=this.radRelative.ClientID %>');
        $_radRelative_<%=this.ClientID %>.attr("checked", "true");
        $_radRelative_<%=this.ClientID %>.click();

        $("#" + '<%=this.relativePeriod.LbxTimeTypeClientID %>').val(arr[2]);
        return;
    }

   
    // absolute
    
    <%= RestoreStateScript %>
    
    $_radAbsolute_<%=this.ClientID %> = $("#" + '<%=this.radAbsolute.ClientID %>');
    $_radAbsolute_<%=this.ClientID %>.attr("checked", "true");
    $_radAbsolute_<%=this.ClientID %>.click();

    $_dateStart_<%=this.ClientID %>.datepicker('enable');
    $_dateEnd_<%=this.ClientID %>.datepicker('enable');
        
    // if nothing - Last Hour
    $_radName_<%=this.ClientID %> = $("#" + '<%=this.radNamed.ClientID %>');
    $_name_<%=this.ClientID %> = $("#" + '<%=this.lbxNamedPeriods.ClientID %>');
    $_name_<%=this.ClientID %>.val("Last Hour");
}
</script>

<div class="PeriodSelector">
    <asp:RadioButton runat="server" ID="radNamed" GroupName="PeriodType" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_47%>" />
    <div class="RadioButtonDivContent">
        <asp:ListBox runat="server" ID="lbxNamedPeriods" Rows="1"/>
    </div>
    
    <asp:RadioButton runat="server" ID="radRelative" GroupName="PeriodType" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_48%>" />
    <div class="RadioButtonDivContent">
        <nta:RelativePeriod runat="server" ID="relativePeriod"/>
    </div>

    <asp:RadioButton runat="server" ID="radAbsolute" GroupName="PeriodType" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_49%>" />
    <div class="RadioButtonDivContent">
        <nta:AbsolutePeriod ID="absolutePeriod" runat="server"/>
    </div>

    <asp:HiddenField runat="server" ID="periodNameHiddenField" />
</div>
