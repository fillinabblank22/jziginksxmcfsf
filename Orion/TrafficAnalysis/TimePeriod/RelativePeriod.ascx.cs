using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.Filters;

public partial class Orion_TrafficAnalysis_Utils_RelativePeriod : UserControl
{
    protected const int MIN_VALUE = 1;
    protected const int MAX_VALUE = 1000000000;

    public string TxtQuantityClientID
    {
        get { return this.txtQuantity.ClientID; }
    }

    public string LbxTimeTypeClientID
    {
        get { return this.lbxTimeType.ClientID; }
    }

    public TimePeriodUiFilter PeriodFilter
    {
        get
        {
            if (!ValidateQuantityCustomValue(this.txtQuantity.Text))
            {
                throw new FormatException();
            }

            return TimePeriodUiFilter.Parse(string.Concat("Last ", this.txtQuantity.Text, " ", this.lbxTimeType.SelectedValue));
        }
        set
        {
            SelectTimePeriod(value);
        }
    }

    public string ValidationGroup { get; set; }

    /// <summary>
    /// Used for forcing call to FillTimeType() regardless of whether this is postback or not
    /// </summary>
    public bool ForceInitialization
    {
        get;
        set;
    }

    private const string CLIENT_DISABLE_TEMPLATE = @"
document.getElementById('{2}').disabled = {0};
document.getElementById('{3}').disabled = {0};
ValidatorEnable(document.getElementById('{4}'), {1});
ValidatorEnable(document.getElementById('{5}'), {1});";

    public string GetClientDisableScript()
    {
        return string.Format(
            CLIENT_DISABLE_TEMPLATE, 
            "true",
            "false",
            this.txtQuantity.ClientID, 
            this.lbxTimeType.ClientID, 
            this.valQuantityRequired.ClientID,
            this.valQuantityCustom.ClientID);
    }

    public string GetClientEnableScript()
    {
        return string.Format(
            CLIENT_DISABLE_TEMPLATE,
            "false",
            "true",
            this.txtQuantity.ClientID,
            this.lbxTimeType.ClientID,
            this.valQuantityRequired.ClientID,
            this.valQuantityCustom.ClientID);

    }

    protected void FillTimeType()
    {
        lbxTimeType.Items.Clear();

        RelativeTimePeriodUnit[] relativeTimePeriodUnits = new[]
                                                               {
                                                                   RelativeTimePeriodUnit.Minutes,
                                                                   RelativeTimePeriodUnit.Hours,
                                                                   RelativeTimePeriodUnit.Days,
                                                                   RelativeTimePeriodUnit.Months
                                                               };

        foreach (RelativeTimePeriodUnit relativeTimePeriodUnit in relativeTimePeriodUnits)
        {
            lbxTimeType.Items.Add(new ListItem(TimePeriodI18nHelper.GetLocalizedRelativeTimePeriodUnit(relativeTimePeriodUnit), relativeTimePeriodUnit.ToString()));
        }
    }

    protected void SelectTimePeriod(TimePeriodUiFilter periodFilter)
    {
        if (periodFilter == null)
            return;

        // if this is not relative time period, preselect some default values and exit
        if (!periodFilter.IsRelative ||
            (periodFilter.RelativeTimePeriodInfo.Type == RelativeTimePeriodType.LastHour) ||
            (periodFilter.RelativeTimePeriodInfo.Type == RelativeTimePeriodType.Today))
        {
            txtQuantity.Text = "2";
            lbxTimeType.SelectedValue = RelativeTimePeriodUnit.Hours.ToString();
            return;
        }

        txtQuantity.Text = periodFilter.RelativeTimePeriodInfo.Value.ToString();
        lbxTimeType.SelectedValue = periodFilter.RelativeTimePeriodInfo.Unit.ToString();
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
            FillTimeType();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (ForceInitialization)
            FillTimeType();

        valIcon.Title = Resources.NTAWebContent.NTAWEBCODE_VB0_40;

        //if there is more than one range control, we register only once client validation function
        if (!Page.ClientScript.IsClientScriptBlockRegistered("ClientValidateRelativePeriodRange"))
        {
            const string scriptTemplate = @"
            function ClientValidateRelativePeriodRange(source, clientside_arguments)
            {{
              if (isNaN(clientside_arguments.Value))
              {{
                 clientside_arguments.IsValid = false;
              }}
              else if (parseInt(clientside_arguments.Value) < {0} || parseInt(clientside_arguments.Value) > {1})
              {{
                  clientside_arguments.IsValid = false;
              }}
              else
              {{
                  clientside_arguments.IsValid = true;
              }}
            }}";
            string script = string.Format(scriptTemplate, MIN_VALUE.ToString(), MAX_VALUE.ToString());

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClientValidateRelativePeriodRange", script, true);
        }

        if (!String.IsNullOrEmpty(ValidationGroup))
        {
            this.valQuantityRequired.ValidationGroup = this.ValidationGroup;
            this.valQuantityCustom.ValidationGroup = this.ValidationGroup;
        }
    }
    
    protected void valQuantityCustom_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = ValidateQuantityCustomValue(args.Value);
    }

    protected bool ValidateQuantityCustomValue(string value)
    {
        int n;
        if (!Int32.TryParse(value, out n))
        {
            return false;
        }
        else
        {
            return (n >= MIN_VALUE && n <= MAX_VALUE);
        }
    }
}
