<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PeriodMenu.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_PeriodMenu" %>
<%@ Register TagPrefix="nta" TagName="PeriodSelector" Src="~/Orion/TrafficAnalysis/TimePeriod/PeriodSelector.ascx" %>


<div style="display: inline; margin-left: 10px;">
    <div class="vert-block">
        <span id="PeriodLabel" class="menuFilterLabel"><%=  Resources.NTAWebContent.NTAWEBDATA_TM0_68 %></span> 
        <br />
        <span id="PeriodTitle" runat="server" class="menuSelectedFilter"><%= this.PeriodFilter != null ? this.PeriodFilter.PeriodNameTitle : "" %></span>
    </div>
    <a runat="server" id="PeriodDropDown" class="PeriodIcon" href="#">&nbsp;</a>
</div>

<div id="PeriodMenu" runat="server" style="text-align: left; display: none; width: 260px;" class="PeriodMenu">

    <nta:PeriodSelector runat="server" ID="periodSelector" />

    <div style="float: right">
        <orion:LocalizableButton runat="server" ID="btnSubmit" DisplayType="Primary" LocalizedText="Submit" OnClick="Submit_Click" CssClass="PeriodSubmitButton" />
        <orion:LocalizableButton runat="server" ID="btnCancel" DisplayType="Secondary" LocalizedText="Cancel" CssClass="PeriodCancelButton" />
    </div>

</div>



