<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RelativePeriod.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_RelativePeriod" %>

<asp:Label runat="server"  Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_51%> " CssClass="PeriodSelector" ></asp:Label>
<asp:TextBox runat="server" Columns="5" ID="txtQuantity" MaxLength="9" />
<asp:ListBox runat="server" ID="lbxTimeType" Rows="1" SelectionMode="single" />

<asp:RequiredFieldValidator runat="server" ID="valQuantityRequired" ControlToValidate="txtQuantity" 
ErrorMessage="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_45%>" Display="Dynamic">
    <a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_45%>" onclick="return false;" class="ntasValIcon">&nbsp;</a>
</asp:RequiredFieldValidator>

<asp:CustomValidator runat="server" ID="valQuantityCustom" 
    ClientValidationFunction="ClientValidateRelativePeriodRange" 
    onservervalidate="valQuantityCustom_ServerValidate"
    ControlToValidate="txtQuantity" 
    Display="Dynamic">
    <a onclick="return false;" id="valIcon" runat="server" href="#" class="ntasValIcon">&nbsp;</a>
</asp:CustomValidator>
<br />