<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowInterfaceDetails.aspx.cs" Inherits="Orion_Netflow_InterfaceDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx"  %>
<%@ Register TagPrefix="npm" TagName="InterfaceLink" Src="~/Orion/Interfaces/Controls/InterfaceLink.ascx"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <npm:NodeLink runat="server" ID="nodeLink" 
                  TargetURLFormat="/Orion/TrafficAnalysis/NetflowNodeDetails.aspx?NetObject=N{0}" />
    -
    <npm:InterfaceLink runat="server" ID="interfaceLink" 
                       TargetURLFormat="/Orion/TrafficAnalysis/NetflowInterfaceDetails.aspx?NetObject=N{0}" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:InterfaceResourceHost runat="server" ID="ifResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:InterfaceResourceHost>
</asp:Content>