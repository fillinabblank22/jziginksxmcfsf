﻿<%@ Page Title="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_3%>" Language="C#" MasterPageFile="~/Orion/OrionMasterPage.master" AutoEventWireup="true" CodeFile="UnknownTrafficEvents.aspx.cs" Inherits="Orion_TrafficAnalysis_UnknownTrafficEvents" %>
<%@ Register Src="~/Orion/Controls/IconHelpButton.ascx" TagPrefix="orion" TagName="IconHelpButton" %>
<%@ Register TagPrefix="nta" TagName="Breadcrumb" Src="~/Orion/TrafficAnalysis/Admin/NetflowSettingsBreadcrumb.ascx" %>
<%@ Register TagPrefix="nta" TagName="EventList" Src="~/Orion/TrafficAnalysis/Controls/EventList.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" Runat="Server">
    <orion:IconHelpButton ID="IconHelpButton1" runat="server" HelpUrlFragment="OrionNTAPHUnresolvedTrafficEvents" />   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <nta:Breadcrumb runat="server" ID="breadcrumb" />  
    <div class="PageHeader" id="etxxHeader" style="width: auto !important;">
    <%=Page.Title%>
    </div>
        <table id="addnodecontenta" cellspacing="0" cellpadding="0"  style="width:98%;margin-top:10px">
        <tr>      
            <td style="padding-left: 10px;padding-right:10px;">
                <div>
                    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />
                    
                    <div id="buttons">
                         <orion:LocalizableButton ID="btnResetNotifications" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_6%>" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_10%>" OnClick="btnResetNotificationsClick"/>
                         <span style="margin-left:10px;"/>
                         <orion:LocalizableButton ID="btnRefreshPage" runat="server" DisplayType="Secondary" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_OSC_8%>" OnClick="btnRefreshPageClick"/>

                    </div>
                    <div id="unrosolvedEventsContent" class="ResourceWrapper">
		                <nta:EventList runat="server" ID="eventList"/>
                    </div>                    
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
