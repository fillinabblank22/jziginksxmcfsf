using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Core.Common;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using ReportType = SolarWinds.Netflow.Web.Utility.ReportType;
using SolarWinds.Netflow.Common;
using SolarWinds.Logging;

public partial class Orion_TrafficAnalysis_NetflowView : MasterPage
{
    private static readonly Log log = new Log();

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        CheckConditions();

        NetflowDetailView view = this.Page as NetflowDetailView;
        
        ControlHelper.AddStylesheet(this.Page, "/Orion/TrafficAnalysis/styles/NetFlow.css");

        // Use appropriate stylesheet instead of legace one
        CSSUtils.RemoveStylesheet("/Events.css", this.Page);
        ControlHelper.AddStylesheet(this.Page, "/Orion/Styles/Events.css");

        if (this.Request.QueryString["ReportType"] == null)
        {
            //Disable caching for IE
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            //Disable caching for FireFox
            HttpContext.Current.Response.Cache.SetNoStore();
        }

        // when Printable or InlineRendering parameter is used we don't want to show TVB (Alerts, Reports,...)
        bool result = false;
        if ((bool.TryParse(Page.Request.QueryString["InlineRendering"], out result) && result) ||
            (bool.TryParse(Page.Request.QueryString["Printable"], out result) && result))
        {
            TrafficViewBuilderTd.Visible = false;
        }

    }

    protected override void OnPreRender(EventArgs e)
    {
        // if null, then the surrounding ContentPlaceHolder has been populated by a child page
        if (null != this.phDefaultTitle)
            this.phDefaultTitle.Controls.Add(new LiteralControl(this.Page.Title));
        // refresher

        base.OnPreRender(e);
    }

    protected void IntegrationIconsOnInit(object sender, EventArgs e)
    {
        NetflowDetailView view = this.Page as NetflowDetailView;

        if (view == null)
            return;

        if (view.ViewType == ReportType.Node || view.ViewType == ReportType.Interface)
        {
            IntegrationIcons1.ObjectToManage.InternalNetworkObject = view.NetflowObject.Node;
            IntegrationIcons1.Visible = true;
        }

        if (view.ViewType == ReportType.Endpoint)
        {
            IntegrationIcons1.ObjectToManage.InternalNetworkObject = (NetflowEndpoint)view.NetflowObject;
            IntegrationIcons1.Visible = true;
        }
    }

    void CheckConditions()
    {
        string errorMessage = string.Empty;

        // DemoServerEdit 
        if (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
        {
            LicenseHelper.TryLoadLicenseInfo();
            if (!LicenseHelper.IsLicensed)
            {
                errorMessage = LicenseHelper.LicenseMessage;
            }
        }


        if (!string.IsNullOrEmpty(errorMessage))
        {
            if (!Request.FilePath.EndsWith("SummaryView.aspx", StringComparison.InvariantCultureIgnoreCase))
            {
                OrionErrorPageBase.TransferToErrorPage(Context, new ErrorInspectorDetails { Error = new Exception(errorMessage), Title = NTAWebContent.NTAWEBCODE_VB1_6, HtmlHelpfulHints = NTAWebContent.NTAWEBCODE_VB1_7 });
            }
        }
    }

    /// <summary>
    /// Returns current Date and Time
    /// The format of the string depends on the region 
    /// and browser language settings 
    /// </summary>
    protected string CurrentDateTime
    {
        get
        {
            return DateTime.Now.ToString("F");
        }
    }

}