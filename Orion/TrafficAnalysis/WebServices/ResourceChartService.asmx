﻿<%@ WebService Language="C#" Class="ResourceChartService" %>

using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Charting.v2;
using SolarWinds.Netflow.Web.AsyncResultsCoreCharts;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Utils;
using System.Threading.Tasks;
using SolarWinds.InformationService.Contract2;
using SolarWinds.Orion.Web.UI;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ResourceChartService : ChartDataWebService
{
    private AsyncResourceResult resourceResult;
    private Guid asyncResultKey;
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();
    private const int DelayForMoreData = 1000;


    [WebMethod]
    public void ClearPage(Guid pageId)
    {
        log.DebugFormat("Cancelling page {0}", pageId);

        // doing that in thread so we don't block page navigation
        Task.Factory.StartNew((state) => ((AsyncResultManagerCoreChart)state).RemoveByPageId(pageId), AsyncResultManagerCoreChart.Instance);
    }

    [WebMethod(EnableSession = true)]
    public PieChartDataResults PieChartData(ChartDataRequest request)
    {
        log.Debug("Begin Pie chart data request handler.");

        List<PieChartDataSeries> dataSeries = new List<PieChartDataSeries>();
        PieChartDataResults results = new PieChartDataResults(dataSeries);
        results.DelayForMoreDataInMs = DelayForMoreData;
        TopResourceDescriptor resourceDescriptor = null;

        try
        {
            if (!Init(request))
                return results;

            DataTable topTable = this.resourceResult.AsyncResult.GetTopData();
            this.resourceResult.ChartServiceFlags.NoData = CheckForNoDataOrErrorInAsyncQuery(topTable, this.resourceResult.AsyncResult);
            resourceDescriptor = resourceResult.ResourceDescriptor;

            if (!string.IsNullOrEmpty(resourceDescriptor.CustomNoDataUrl))
                results.NoDataUrl = resourceDescriptor.CustomNoDataUrl;

            if (!this.resourceResult.ChartServiceFlags.NoData && topTable != null)
            {
                log.DebugFormat("Postprocessing top table");

                if (resourceDescriptor.ResourceType != ResourceType.CBQoS)
                {
                    topTable = TopTablePostProcessor.PostProcessTopTable(topTable);
                }
                else
                {
                    topTable = TopTablePostProcessor.PostProcessCbqosTopTable(topTable);
                }

                log.DebugFormat("Top table is ready, building results object. RowCount={0}.", topTable.Rows.Count);

                ChartDataResultBuilder chartDataResultBuilder = new ChartDataResultBuilder();

                dataSeries.AddRange(chartDataResultBuilder.BuildPieChartDataSeries(topTable, resourceResult.ResourceDescriptor));
            }

            results.MoreDataAvailable = (topTable == null && !resourceResult.ChartServiceFlags.NoData);

            resourceResult.ChartServiceFlags.ChartInitialized = !results.MoreDataAvailable;
            resourceResult.ChartServiceFlags.LastDataSent = !results.MoreDataAvailable;

            log.Debug("Returning top data.");

            return results;
        }
        catch (Exception exception)
        {
            results.NoDataHtml = HandleResourceError(exception);

            log.Error("Exception occured during pie chart data request. Returning No Data to the client.", exception);
            this.resourceResult.ChartServiceFlags.NoData = true;
            results.MoreDataAvailable = false;

            return results;
        }
        finally
        {
            if (resourceResult != null)
            {
                if (this.resourceResult.ChartServiceFlags.NoData)
                    dataSeries.AddRange(new List<PieChartDataSeries>()); //Add empty series to indicate we don't have data

                this.RemoveAsyncResourceResultFromManagerIfPossible();
            }

            log.DebugFormat("End Pie chart data request handler. RequestID={0}, IsCompleted={1}, WaitBeforeNextRequest={2} ms, ReturnedSeries={3}", this.asyncResultKey, !results.MoreDataAvailable, results.DelayForMoreDataInMs, dataSeries.Count);
        }

    }

    [WebMethod(EnableSession = true)]
    public ChartDataResults AreaChartData(ChartDataRequest request)
    {
        log.Debug("Begin Area chart data request handler.");

        List<DataSeries> dataSeries = new List<DataSeries>();
        ChartDataResults results = new ChartDataResults(dataSeries);
        results.DelayForMoreDataInMs = DelayForMoreData;

        try
        {
            if (!Init(request))
                return results;

            bool isCompleted;
            ChartDataResultBuilder chartDataResultBuilder = new ChartDataResultBuilder();
            ChartOptions options = new ChartOptions();
            results.ChartOptionsOverride = options;

            if (!string.IsNullOrEmpty(this.resourceResult.ResourceDescriptor.CustomNoDataUrl))
                results.NoDataUrl = this.resourceResult.ResourceDescriptor.CustomNoDataUrl;

            if (this.resourceResult.ResourceDescriptor.Parameters.DisableProgressiveLoading && !this.resourceResult.AsyncResult.IsCompleted)
            {
                // when parameters force to disable progressive loading, we need to wait until all data (top and detail) before
                // any data is sent to client. This is to workaround bug in Highcharts with bar charts

                log.VerboseFormat("Forced non-progressive loading and result is not completed. Waiting until data is ready.");

                isCompleted = false;
            }
            else if (!resourceResult.ChartServiceFlags.ChartInitialized)
            {
                DataTable topTable = this.resourceResult.AsyncResult.GetTopData();
                this.resourceResult.ChartServiceFlags.NoData = CheckForNoDataOrErrorInAsyncQuery(topTable, this.resourceResult.AsyncResult);
                isCompleted = this.resourceResult.ChartServiceFlags.NoData;

                if (!this.resourceResult.ChartServiceFlags.NoData)
                {
                    if (topTable != null)
                    {
                        DataTable detailMatrix = resourceResult.AsyncResult.DetailTableMatrix;
                        DataTable detailTable = this.resourceResult.AsyncResult.GetAndEraseDetailData(out isCompleted);
                        TopResourceDescriptor resourceDescriptor = resourceResult.ResourceDescriptor;

                        if (this.resourceResult.ResourceDescriptor.Parameters.ChartStyle == ChartStyle.NoChart)
                        {
                            // No chart style so just complete the request, add empty serie just to trigger legend request
                            DataSeries emptyDataSeries = new DataSeries();
                            emptyDataSeries.AddPoint(DataPoint.CreatePoint(DateTime.Now, 0));
                            dataSeries.Add(emptyDataSeries);
                        }
                        else
                        {
                            string navigatorSeriesKey = null;

                            log.DebugFormat("Postprocessing top table");

                            if (resourceDescriptor.ResourceType != ResourceType.CBQoS)
                            {
                                topTable = TopTablePostProcessor.PostProcessTopTable(topTable);
                            }
                            else
                            {
                                topTable = TopTablePostProcessor.PostProcessCbqosTopTable(topTable);
                            }

                            dataSeries.AddRange(chartDataResultBuilder.BuildFirstDetailDataSeries(topTable, detailTable, detailMatrix,
                                    resourceResult.ResourceDescriptor, GlobalSettingsDALCached.CoreChartsDataSeriesLimit, "Default", "Navigator", isCompleted, out navigatorSeriesKey));

                            resourceResult.NavigatorSeriesKey = navigatorSeriesKey;
                        }

                        options.SetAreaChartType("Default", this.resourceResult.ResourceDescriptor.Parameters.AreaChartType, "default", dataSeries.Count);
                        options.SetAreaChartType("Navigator", this.resourceResult.ResourceDescriptor.Parameters.AreaChartType, "default", dataSeries.Count);
                        options.SetYAxisUnit(0, ChartDU.CombineUnitAndType(this.resourceResult.ResourceDescriptor.Unit,
                            this.resourceResult.ResourceDescriptor.Parameters.ChartDataUnit));

                        resourceResult.ChartServiceFlags.ChartInitialized = true;
                    }
                    else
                    {
                        isCompleted = false;
                    }
                }
            }
            else
            {
                DataTable detailTable = this.resourceResult.AsyncResult.GetAndEraseDetailData(out isCompleted);

                if (detailTable != null)
                {
                    dataSeries.AddRange(chartDataResultBuilder.BuildDetailDataSeries(
                        detailTable, resourceResult.ResourceDescriptor, "Default", resourceResult.NavigatorSeriesKey, "Navigator"));
                }
            }

            results.MoreDataAvailable = !isCompleted;
            resourceResult.ChartServiceFlags.LastDataSent = !results.MoreDataAvailable;

            log.Debug("Returning detail data.");

            return results;
        }
        catch (Exception exception)
        {
            log.Error("Exception occured during area chart data request. Returning No Data to the client.", exception);

            results.NoDataHtml = HandleResourceError(exception);

            this.resourceResult.ChartServiceFlags.NoData = true;

            results.MoreDataAvailable = false;

            return results;
        }
        finally
        {
            if (this.resourceResult != null)
            {
                if (this.resourceResult.ChartServiceFlags.NoData == true)
                    dataSeries.AddRange(new Dictionary<string, DataSeries>().Values);

                this.RemoveAsyncResourceResultFromManagerIfPossible();
            }

            log.DebugFormat("End area chart data request handler. RequestID={0}, IsCompleted={1}, WaitBeforeNextRequest={2} ms, ReturnedSeries={3}", this.asyncResultKey, !results.MoreDataAvailable, results.DelayForMoreDataInMs, dataSeries.Count);

        }
    }

    private static string HandleResourceError(Exception exception)
    {
        string errorMessage = null;

        if (exception is TimeoutException ||
            (exception as System.ServiceModel.FaultException<InfoServiceFaultContract>)?.Detail?.Message.IndexOf("TIMEOUT", StringComparison.OrdinalIgnoreCase) >= 0)
        {
            errorMessage = Resources.NTAWebContent.NTAWEBDATA_NoDataTimeout;
        }

        if (errorMessage == null)
            throw exception;

        return string.Format("<div class='sw-suggestion sw-suggestion-fail'>{0}</div>", System.Web.HttpUtility.HtmlEncode(errorMessage));
    }


    [WebMethod(EnableSession = true)]
    public string TopResourceLegend(ChartDataRequest request)
    {
        log.Debug("Begin Legend data request handler.");

        try
        {
            // get top table data
            if (!Init(request))
                return string.Empty;

            return TopResourceCoreChartBase.TopResourceLegendFromAsyncResult(this.resourceResult, Server);
        }
        catch (Exception ex)
        {
            log.Error("Legend loading error", ex);
            resourceResult.ChartServiceFlags.LegendLoaded = true;
            throw;
        }
        finally
        {
            this.RemoveAsyncResourceResultFromManagerIfPossible();
        }
    }

    [WebMethod(EnableSession = true)]
    public string TopResourceLegendWarningMessage(string filterID, int[] resourceUpdateTypes)
    {
        log.Debug("Begin Legend warning message request handler.");

        try
        {
            var filters = FiltersHelper.CreateUniversalFilterCollection(filterID);
            return TopResourceCoreChartBase.TopResourceLegendWarningMessage(resourceUpdateTypes.Select(m => (NetflowUpdateQueryType)m).ToArray(), filters) ?? "";
        }
        catch (Exception ex)
        {
            log.Error("Legend loading error", ex);
            throw;
        }
    }

    private bool CheckForNoDataOrErrorInAsyncQuery(DataTable topTable, IAsyncNTAResultCoreCharts asyncResult)
    {
        if (asyncResult.IsCancelled)
        {
            log.Error(String.Format("Error while quering data in Async object: '{0}'. Returning No Data to the client.", asyncResult.CancelReason));
            return true;
        }

        if (topTable == null) // Still no table -> waiting for data or error (cancelled flag)
            return false;

        if (topTable.Rows.Count > 0) //Received some data, that's fine
            return false;

        //No TOP data
        log.DebugFormat("No TOP data. Returning No Data to the client.");

        return true;
    }

    private bool Init(ChartDataRequest request)
    {
        log.Debug("Init begins");
        TopResourceClientParameters resourceParams = null;

        Guid asyncPageId = Guid.Empty;

        foreach (string objectId in request.NetObjectIds)
        {
            if (objectId.StartsWith("AsyncResultKey:"))
            {
                string resultKeyStr = objectId.Replace("AsyncResultKey:", "");
                asyncResultKey = Guid.Parse(resultKeyStr);
                log.Debug("Request key: " + resultKeyStr);
            }

            if (objectId.StartsWith("AsyncPageId:"))
            {
                string resultKeyStr = objectId.Replace("AsyncPageId:", "");
                asyncPageId = Guid.Parse(resultKeyStr);
                log.Debug("AsyncPageId : " + resultKeyStr);
            }

            if (objectId.StartsWith("TopResourceClientParameters:"))
            {
                string resourceParamsStr = objectId.Replace("TopResourceClientParameters:", "");
                resourceParams = TopResourceClientParameters.FromJson<TopResourceClientParameters>(resourceParamsStr);
                log.Debug("TopResourceParameters deserialized");
            }
        }

        if (!AsyncResultManagerCoreChart.Instance.TryGetResult(asyncResultKey, out resourceResult))
        {
            log.InfoFormat("Can not find async result for ID: {0}", asyncResultKey);

            if (AsyncResultManagerCoreChart.Instance.IsPageCanceled(asyncPageId))
            {
                log.InfoFormat("Page {0} was already canceled", asyncPageId);
                return false;
            }

            if (resourceParams == null)
            {
                string message = String.Format("Unable to find async result {0} and resource does not have parameters defined.", asyncResultKey);
                log.Error(message);
                throw new InvalidOperationException(message);
            }

            ApplicationProvider applicationProvider;
            if (request.AllSettings.ContainsKey("applicationProvider") &&
                Enum.TryParse(request.AllSettings["applicationProvider"] as string, out applicationProvider))
                resourceParams.ApplicationProvider = applicationProvider;

            DateTime? relativeDateTime = null;
            object relativeTimestamp;
            if (request.AllSettings.TryGetValue("relativeDateTime", out relativeTimestamp) && relativeTimestamp is int)
            {
                relativeDateTime = DateTimeHelper.FromUnixTimestamp((int)relativeTimestamp);
            }
            resourceParams.RelativeDateTime = relativeDateTime;

            log.DebugFormat("Getting resource info for resource ID: {0}", resourceParams.ResourceID);
            ResourceInfo resourceInfo = ResourceManager.GetResourceByID(resourceParams.ResourceID);
            if (resourceInfo == null)
            {
                string message = String.Format("Unable to get resource info for resource ID: {0}", resourceParams.ResourceID);
                log.Error(message);
                throw new InvalidOperationException(message);
            }

            Context.Items[typeof(ViewInfo).Name] = resourceInfo.View;

            log.DebugFormat("Getting resource descriptor for resource ID: {0}", resourceParams.ResourceID);
            TopResourceDescriptor descriptor = GetResourceDescriptor(resourceParams, resourceInfo);

            if (descriptor == null)
            {
                string message = String.Format("Unable to get resource descriptor for resource ID: {0}", resourceParams.ResourceID);
                log.Error(message);
                throw new InvalidOperationException(message);
            }

            log.DebugFormat("Getting new data for resource ID: {0} and async result key: {1}", resourceParams.ResourceID, asyncResultKey);
            resourceResult = TopResourceCoreChartBase.RequestData(asyncResultKey, descriptor, asyncPageId);
        }

        log.Debug("Init finished");

        if (this.resourceResult.AsyncResult?.IsFailed == true)
        {
            throw this.resourceResult.AsyncResult.FailedError;
        }

        return true;
    }

    private TopResourceDescriptor GetResourceDescriptor(TopResourceClientParameters parameters, ResourceInfo resource)
    {
        var userControl = new UserControl();

        var control = userControl.LoadControl(resource.File);

        // this is required if our control passed as EmbeddedObjectResource
        // this happens for Custom Object Resource
        // in such case created Descriptor describes the EmbeddedObjectResource while parameters/properties point to the Custom Object Resource
        if (control is BaseResourceControl &&
            !(control is TopResourceCoreChartBase) &&
            !string.IsNullOrEmpty(resource.Properties["EmbeddedObjectResource"]))
        {
            control = userControl.LoadControl(resource.Properties["EmbeddedObjectResource"]);
        }

        var chart = control as TopResourceCoreChartBase;

        if (chart == null)
        {
            return null;
        }

        chart.InitDescriptor(parameters);
        return chart.Descriptor;
    }

    /// <summary>
    /// If all conditions are satisfied, this method removes AsyncResourceResult from the manager
    /// This method should be called after each async call from webservice
    /// </summary>
    private void RemoveAsyncResourceResultFromManagerIfPossible()
    {
        if ((resourceResult.ChartServiceFlags.LegendLoaded && resourceResult.ChartServiceFlags.LastDataSent) || (resourceResult.ChartServiceFlags.NoData))
        {
            AsyncResourceResult.PageLoadStat pageLoadStat = this.resourceResult.ParentPageLoadStat;
            //By removing from the result manager, AsyncResult and SWIS proxy inside will be safely disposed
            AsyncResultManagerCoreChart.Instance.Remove(this.asyncResultKey);

            //Log page load time if there are no more pending async result for page on which is this resource
            if (!AsyncResultManagerCoreChart.Instance.AnyResult(result => result.ParentPageLoadStat.UniquePageId == pageLoadStat.UniquePageId))
            {
                DateTime now = DateTime.Now;
                double allCompleteTime = (now - pageLoadStat.LoadStartTime).TotalMilliseconds;

                NetFlowPageLoadStatsDAL.LogPageLoadStatisticAsync(now, pageLoadStat.PageUrl, pageLoadStat.PageTitle, NetFlowPageLoadStatsDAL.StatType.CoreCharts, allCompleteTime);
            }
        }
    }


}
