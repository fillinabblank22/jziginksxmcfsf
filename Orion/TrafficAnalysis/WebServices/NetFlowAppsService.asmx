﻿<%@ WebService Language="C#" Class="NetFlowAppsService" %>

using System;
using System.Web;
using System.Threading;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using SolarWinds.Logging;
using SolarWinds.Orion.Common;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.Utility;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NetFlowAppsService  : WebService
{
    private static readonly Log Log = new Log();

    [WebMethod(EnableSession = true)]
    public object SubmitApplicationChanges(string webId)
    {
        if (string.IsNullOrEmpty(webId) || OrionConfiguration.IsDemoServer)
        {
            return new {Success = false};
        }

        Log.InfoFormat("Received request for application changes submit for webId: {0}", webId);

        DateTime lastApplicationUpdate;
        DateTime lastIpGroupUpdate;
        List<SessionOperation> journal;
        using (var storage = new SessionStorage(HttpContext.Current, webId))
        {
            journal = storage.GetSubmitJournal(out lastApplicationUpdate, out lastIpGroupUpdate);
        }

        SubmitThread thread = new SubmitThread(webId, journal, lastApplicationUpdate, lastIpGroupUpdate);
        HttpContext.Current.Cache.Insert(GetSubmitThreadKey(webId), thread, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10));

        Log.DebugFormat("Starting application changes submit thread for webId: {0}", webId);
        thread.Start();

        return new {Success = true};
    }

    [WebMethod]
    public object GetSubmitApplicationChangesProgress(string webId)
    {
        SubmitThread thread = (SubmitThread)HttpContext.Current.Cache.Get(GetSubmitThreadKey(webId));

        if (thread == null)
            return new {Finished = false};

        thread.SyncLock.EnterReadLock();
        try
        {
            return new {thread.Finished, thread.Success, thread.Collision, thread.Error};
        }
        finally
        {
            thread.SyncLock.ExitReadLock();
        }
    }

    /// <summary>
    /// Helper class, which encapsulates the background logic required to submit applications.
    /// </summary>
    private class SubmitThread
    {
        public bool Finished { get; private set; }
        public bool Success { get; private set; }
        public bool Collision { get; private set; }
        public string Error { get; private set; }
        private string WebId { get; set; }

        private List<SessionOperation> Journal { get; set; }
        private DateTime LastApplicationUpdate { get; set; }
        private DateTime LastIpGroupUpdate { get; set; }
        internal readonly ReaderWriterLockSlim SyncLock = new ReaderWriterLockSlim();
        
        public SubmitThread(string webId, List<SessionOperation> journal, DateTime lastAppUpdate, DateTime lastGroupUpdate)
        {
            WebId = webId;
            Journal = journal;
            LastApplicationUpdate = lastAppUpdate;
            LastIpGroupUpdate = lastGroupUpdate;
            Finished = false;
        }

        public void Start()
        {
            ThreadPool.QueueUserWorkItem(Submit);
        }

        private void Submit(object state)
        {
            Log.InfoFormat("Submitting application changes for webId: {0}", WebId);

            try
            {
                SessionStorage.SubmitApplicationChanges(Journal, LastApplicationUpdate, LastIpGroupUpdate);

                //Sends notification to BL, so it refreshes cache of applications, used for processing of flows.
                //Also updates LastApplicationUpdate global state.
                BusinessLayerHelper.ApplyApplicationsChanges();
            }
            catch (SubmitCollisionException)
            {
                Log.WarnFormat("Detected submit collision for webId: {0}, notifying user.", WebId);

                SetResult(true, false, true);
                return;
            }
            catch (Exception e)
            {
                Log.ErrorFormat("Exception during submit of application changes for webId {0}, {1}", WebId, e);

                SetResult(true, false, false, e.Message);
                return;
            }

            Log.InfoFormat("Successfully submitted application changes for webId: {0}", WebId);
            SetResult(true, true, false);
        }

        private void SetResult(bool finished, bool success, bool collision, string error = "")
        {
            SyncLock.EnterWriteLock();
            try
            {
                Finished = finished;
                Success = success;
                Collision = collision;
                Error = error;
            }
            finally
            {
                SyncLock.ExitWriteLock();
            }
        }
    }

    private static string GetSubmitThreadKey(string webId)
    {
        return String.Format("ManageAppsSubmitResult|{0}", webId);
    }
}