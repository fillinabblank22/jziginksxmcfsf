﻿<%@ WebService Language="C#" Class="ResourceService" %>

using System;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DataConverters;
using SolarWinds.Netflow.Web.Reporting.DNS;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ResourceService : WebService
{
    private static Log log = new Log();
    
    private string ResolveHostname(string hostname, string ipAddress)
    {
        bool needsResolve;
        hostname = OnDemandDns.Instance.GetCached(hostname, ipAddress, out needsResolve).ToString();

        try
        {
            if (needsResolve)
            {
                var ajaxRequest = string.Format("var $this=$(this);$.ajax({{ url: \"/Orion/TrafficAnalysis/Controls/DnsBuilder.ashx\", data: {{hostname:\"{0}\",ip:\"{1}\", cache:\"{2}\" }}, success: function(data){{ $this.parent().html(data); }} }});",
                    hostname,
                    ipAddress,
                    GlobalSettingsDALCached.DnsOnDemandClientCacheEnabled ? 0 : DateTime.Now.Ticks);
                hostname = string.Format("<img alt='{0}' title='{0}' src='{1}' style='width:10px;height:10px' onload='{2}'/>",
                    Resources.NTAWebContent.NTAWEBDATA_MM0_58,
                    System.Web.VirtualPathUtility.ToAbsolute("/Orion/TrafficAnalysis/images/animated_loading_16x16.gif"),
                    "this.onload = null;" + ajaxRequest);
            }
        }
        catch (Exception ex)
        {
            log.Debug(string.Format("Error while resolving hostname \"{0}\" from dns cache", hostname), ex);
        }
        return hostname;
    }

    [WebMethod]
    public object[] TopConversations(string endpointIpAddress, string filterId, int maxMessages)
    {
        log.Debug("Begin TopConversations request handler.");
        try
        {
            var filters = FiltersHelper.CreateUniversalFilterCollection(filterId);
            var conversations = new ConversationsDAL().GetReport(filters, maxMessages, true, true, false, false, false).TopConversations;

            var data = ConversationEndpointsConverter.ConvertTopConversationsDataTable(endpointIpAddress, filterId, conversations, this.ResolveHostname);

            return data;
        }
        catch (Exception ex)
        {
            log.Error("TopConversations loading error", ex);
            throw;
        }
    }
}