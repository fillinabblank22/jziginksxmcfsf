<%@ WebService Language="C#" Class="AllNetflowSourcesNodeTree" %>

using System;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Runtime.Serialization.Json;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.DisplayTypes;
using SolarWinds.Logging;
using CustomPropertyMgr = SolarWinds.Orion.Core.Common.CustomPropertyMgr;
using Limitation = SolarWinds.Orion.Web.Limitation;
using Node = SolarWinds.Orion.NPM.Web.Node;
using RegistrySettings = SolarWinds.Orion.Core.Common.RegistrySettings;
using System.Web;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Netflow.Web.Reporting.Models;
using SolarWinds.Netflow.Web.Utility;

/// <summary>
/// Summary description for NodeTree
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AllNetflowSourcesNodeTree : System.Web.Services.WebService
{
    private static readonly SolarWinds.Logging.Log _log = new SolarWinds.Logging.Log();

    public IAllNetFlowSourcesDAL AllNetflowSourcesDAL { get { return _allNetFlowSourcesDAL; } set { _allNetFlowSourcesDAL = value; } }

    private static IAllNetFlowSourcesDAL _allNetFlowSourcesDAL = new AllNetflowSourcesDAL();

    [WebMethod(EnableSession = true)]
    public string GetTreeSection(int resourceId, string rootId, object[] keys)
    {
        _log.DebugFormat("GetTreeSection({1}, {2}, {3})", resourceId, rootId, FormatArray(keys));

        try
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            BuildTreeLevel(htmlWriter, resourceId, rootId, keys.Length + 1, keys);

            return stringWriter.ToString();
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Exception in GetTreeSection({0}, {1}, {2}) . {3}", resourceId, rootId, FormatArray(keys), ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public string GetNodesCount()
    {
        _log.Debug("GetNodesCount()");

        try
        {
           var t= DALProvider.GetInstance().GetDAL<IAllNetFlowSourcesDAL>().GetNodesCount();
                return t;
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Exception in GetNodesCount() . {0}", ex);
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    private void BuildTreeLevel(HtmlTextWriter writer, int resourceId, string rootId, int treeLevel, object[] keys)
    {
        // get view ID from resource ID
        var currentResource = SolarWinds.Orion.Web.ResourceManager.GetResourceByID(resourceId);
        int viewId = currentResource != null && currentResource.View != null ? currentResource.View.ViewID : 0;

        if (treeLevel == 2)
        {
            //Load Interfaces
            string nodeID = GetNodeID(keys);

            // please ensure that html starts with '<table cellspacing="0" style="font-size:11px;">'. We need this in NetFlowSources.js 
            // when session timeout and login page is returned instead of this html!

            writer.AddStyleAttribute(HtmlTextWriterStyle.FontSize, "11px");

            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.AddStyleAttribute("table-layout", "fixed");


            BuildInterfaceList(writer, nodeID, viewId);

            writer.RenderEndTag();
        }
        else
        {
            //Load Nodes
            var nodes = AllNetflowSourcesDAL.GetNodes(viewId).ToList();

            if (!nodes.Any())
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "font-size:12px;");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, "line-height: 17px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Write(NoSourcesMessage());
                writer.RenderEndTag();//td
                return;
            }

            int index = 0;

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "0px");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0px");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "2px");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "500px");

            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.AddStyleAttribute("table-layout", "fixed");


            Header(writer);

            foreach (var node in nodes)
            {
                string childId = string.Format("{0}-{1}", rootId, index);

                string key = node.Node.NodeID.ToString();

                RenderNode(writer, resourceId, childId, ArrayAppend(keys, key), node, index);

                index++;
            }

            writer.RenderEndTag(); // table
        }
    }

    private void RenderProperty(HtmlTextWriter writer, string message, string textAlign, string width, string textPadding, string textPaddingValue, bool pNoWrap)
    {
        if (String.IsNullOrEmpty(message))
            message = "&nbsp;";

        if (!String.IsNullOrEmpty(width))
            writer.AddAttribute(HtmlTextWriterAttribute.Width, width);

        if (!String.IsNullOrEmpty(textAlign))
        {
            writer.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, textAlign);
            writer.AddAttribute(HtmlTextWriterAttribute.Align, textAlign);  // IE6
        }

        if (!String.IsNullOrEmpty(textPadding) && !String.IsNullOrEmpty(textPaddingValue))
            switch (textPadding)
            {
                case "left":
                    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, textPaddingValue);
                    break;
                case "right":
                    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingRight, textPaddingValue);
                    writer.AddAttribute(HtmlTextWriterAttribute.Align, "right");  // IE6
                    break;
                case "top":
                    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingTop, textPaddingValue);
                    break;
                case "bottom":
                    writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingBottom, textPaddingValue);
                    break;
                default:
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Padding, textPaddingValue);
                    break;
            }

        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        if (true == pNoWrap)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Nowrap, string.Empty);   //FF+ other browsers nowrap
            writer.Write("<nobr>" + message + "</nobr>");                           //IE6 nowrap (<nobr>)
        }
        else
        {
            writer.Write(message);
        }
        writer.RenderEndTag();//td
    }

    private void RenderIcon(HtmlTextWriter writer, string iconPath, string alt)
    {
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "4px");

        writer.BeginRender();

        writer.AddAttribute(HtmlTextWriterAttribute.Class, "StatusIcon");

        writer.AddAttribute(HtmlTextWriterAttribute.Alt, alt);

        writer.AddAttribute(HtmlTextWriterAttribute.Src, iconPath);

        writer.RenderBeginTag(HtmlTextWriterTag.Img);
        writer.RenderEndTag();

        writer.EndRender();
    }

    private void Header(HtmlTextWriter writer)
    {
        writer.RenderBeginTag(HtmlTextWriterTag.Thead);

        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        //writer.RenderBeginTag(HtmlTextWriterTag.Col)

        RenderProperty(writer, Resources.NTAWebContent.NTAWEB_FlowSourcesResource_SourceTitle, "left", "235px", "left", null, false);
        RenderProperty(writer, Resources.NTAWebContent.NTAWEBCODE_AK0_3, "right", "60px", "right", null, false);
        RenderProperty(writer, Resources.NTAWebContent.NTAWEBCODE_AK0_4, "right", "60px", "right", null, false);
        RenderProperty(writer, Resources.NTAWebContent.NTAWEBCODE_AK0_5, "right", "80px", "right", null, false);
        RenderProperty(writer, Resources.NTAWebContent.NTAWEBCODE_AK0_6, "right", "80px", "right", null, false);

        writer.RenderEndTag();//Tr

        writer.RenderEndTag();//thead
    }



    private void RenderNode(HtmlTextWriter writer, int resourceId, string rootId, object[] keys, NetflowSourceNode netflowSourceNode, int nodeIndex)
    {
        bool nodeIsExpandable = netflowSourceNode.HasInterfaces;

        string toggleId = rootId + "-toggle";

        writer.BeginRender();

        if (!netflowSourceNode.NodeHadTraffic)
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "font-style:italic;");

        writer.AddAttribute("automation", "nodeRow");
        var backgroundColor = nodeIndex % 2 == 0 ? "#FFFFFF" : "#F6F6F6";
        writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, backgroundColor);
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "3");

        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        if (nodeIsExpandable)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick,
                string.Format("NetFlowSources_Click('{0}', {1}, {2}); return false;", rootId, resourceId, FormatArray(keys)));

            writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");

            writer.RenderBeginTag(HtmlTextWriterTag.A); // expand button

            writer.AddAttribute(HtmlTextWriterAttribute.Src, "/Orion/images/Button.Expand.gif");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, toggleId);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "Toggle");

            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag(); // img
            writer.RenderEndTag(); // expand button
        }
        else
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "netflow-source-nodesource");
        }

        // Link has to be always rendered to display the NPM node pop up
        writer.AddAttribute(HtmlTextWriterAttribute.Href, NodeLink(netflowSourceNode, true));

        if (!netflowSourceNode.NodeHadTraffic)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, string.Format(SolarWinds.Netflow.Web.UIHelper.NoDataOnNodeMessageBox, netflowSourceNode.Node.Name));
        }

        // Solarwinds Engineers Toolset context menu integration (TT #2432)
        // + Toolset integration menu is not opened on NetFlow web page for the device 
        // from which traffic was not received. (TT #3151)
        writer.AddAttribute("IP", netflowSourceNode.Node.IPAddressString);
        writer.AddAttribute("NodeHostName", netflowSourceNode.Node.Hostname);
        writer.AddAttribute("Community", netflowSourceNode.Node.CommunityString);

        writer.RenderBeginTag(HtmlTextWriterTag.A); // Node link
        RenderNodeStatus(writer, netflowSourceNode.Node.Status);
        RenderNodeIcon(writer, netflowSourceNode.Node.VendorIcon);
        writer.Write(netflowSourceNode.Node.Name);
        writer.RenderEndTag(); // Node link

        writer.RenderEndTag(); // Td

        RenderProperty(writer, ConvertTime(netflowSourceNode.NetFlowLastTime), "right", null, "right", "2px", true);
        RenderProperty(writer, ConvertTime(netflowSourceNode.CbQoSLastTime), "right", null, "right", "2px", true);

        writer.RenderEndTag(); // Tr

        if (nodeIsExpandable)
        {
            RenderInterfacesContent(writer, rootId, backgroundColor);
        }

        writer.EndRender();
    }

    private void RenderNodeStatus(HtmlTextWriter writer, CompoundStatus status)
    {
        string alt;
        if (status.ChildStatus.Value == OBJECT_STATUS.Up)
            alt = string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_7, status.ParentStatus);
        else
            alt = string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_8,
                status.ParentStatus, status.ChildStatus);

        RenderIcon(writer, status.ToString("smallimgpath", null), alt);
    }

    private void RenderNodeIcon(HtmlTextWriter writer, string icon)
    {
        const string vendorIconPath = "/NetPerfMon/images/Vendors/{0}";

        string imagePath = string.Format(vendorIconPath, icon.Trim());
        if (!File.Exists(HttpContext.Current.Server.MapPath(imagePath)))
        {
            imagePath = string.Format(vendorIconPath, "Unknown.gif");
        }
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingRight, "2px");

        RenderIcon(writer, imagePath, String.Empty);
    }

    private void RenderInterfacesContent(HtmlTextWriter writer, string rootId, string backgroundColor)
    {
        string contentsId = rootId + "-contents";

        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "hidden");
        writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, backgroundColor);
        writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "6");

        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        writer.AddAttribute(HtmlTextWriterAttribute.Id, contentsId);
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
        writer.RenderBeginTag(HtmlTextWriterTag.Div);
        writer.RenderEndTag(); // div

        writer.RenderEndTag(); // Td

        writer.RenderEndTag(); // Tr
    }

    private void BuildInterfaceList(HtmlTextWriter writer, string nodeID, int viewID)
    {
        List<Interface> interfaces = AllNetflowSourcesDAL.GetRouterInterfacesForView(nodeID, viewID);

        for (int interfacesIndex = 0; interfacesIndex < interfaces.Count; ++interfacesIndex)
        {
            RenderInterface(writer, interfaces[interfacesIndex]);
        }
    }


    private void RenderInterface(HtmlTextWriter writer, Interface curInterface)
    {
        writer.BeginRender();

        DateTime netFlowLastTime; DateTime cbqosLastTime;

        //Last Time Received
        bool interfaceHadTraffic = AllNetflowSourcesDAL.GetLastReceivedTime(curInterface.InterfaceID, out netFlowLastTime, out cbqosLastTime);
        bool intefaceIsManageable = AllNetflowSourcesDAL.IsInterfaceManageable(curInterface.InterfaceID);

        if (!interfaceHadTraffic)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "font-style:italic; cursor:pointer");
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, string.Format(SolarWinds.Netflow.Web.UIHelper.NoDataOnInterfaceMessageBox, curInterface.InterfaceName, curInterface.Node.Name));
        }

        writer.AddAttribute("automation", "interfaceRow");
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

        writer.AddAttribute(HtmlTextWriterAttribute.Width, "235px");

        writer.RenderBeginTag(HtmlTextWriterTag.Td);

        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingLeft, "22px");
        writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "inline-block");

        writer.AddAttribute(HtmlTextWriterAttribute.Href, InterfaceLink(curInterface.InterfaceID, true));

        // Solarwinds Engineers Toolset context menu integration (TT #2432)
        // + Toolset integration menu is not opened on NetFlow web page for the device 
        // from which traffic was not received. (TT #3151)
        if (curInterface.Parent is Node)
        {
            Node node = curInterface.Parent as Node;

            writer.AddAttribute("IP", node.IPAddressString);
            writer.AddAttribute("NodeHostName", node.Hostname);
            writer.AddAttribute("Community", node.CommunityString);
            writer.AddAttribute("IFIndex", curInterface.InterfaceIndex.ToString());
            writer.AddAttribute("IFName", curInterface.InterfaceName);
        }

        writer.RenderBeginTag(HtmlTextWriterTag.A);
        RenderInterfaceStatus(writer, curInterface.OperationalStatus);
        RenderInterfaceIcon(writer, curInterface);
        writer.WriteEncodedText(curInterface.Caption);
        writer.RenderEndTag();  //a href

        writer.RenderEndTag();//td

        // render interface content
        {
            string align = "right";

            //In-Out Bps            
            RenderProperty(writer, intefaceIsManageable ? curInterface.InBps.ToString() : Resources.NTAWebContent.NTAWEBCODE_AK0_24, align, "60px", align, null, true);
            RenderProperty(writer, intefaceIsManageable ? curInterface.OutBps.ToString() : Resources.NTAWebContent.NTAWEBCODE_AK0_24, align, "60px", align, null, true);

            RenderProperty(writer, ConvertTime(netFlowLastTime), align, "80px", align, null, true);
            RenderProperty(writer, ConvertTime(cbqosLastTime), align, "80px", align, null, true);
        }

        writer.RenderEndTag();//tr

        writer.EndRender();
    }

    private void RenderInterfaceStatus(HtmlTextWriter writer, Status status)
    {
        string alt = status.ToString();

        RenderIcon(writer, status.ToString("smallimgpath", null), alt);
    }

    private void RenderInterfaceIcon(HtmlTextWriter writer, Interface curInterface)
    {
        writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingRight, "2px");
        RenderIcon(writer, string.Format("/NetPerfMon/images/Interfaces/{0}.gif", curInterface.InterfaceType), String.Empty);
    }

    private static string FormatArray(object[] values)
    {
        DataContractJsonSerializer serializer = new DataContractJsonSerializer(values.GetType());
        using (MemoryStream ms = new MemoryStream())
        {
            serializer.WriteObject(ms, values);
            return Encoding.Default.GetString(ms.ToArray());
        }
    }

    private object[] ArrayAppend(object[] array, object toAppend)
    {
        object[] newArray = new object[array.Length + 1];
        array.CopyTo(newArray, 0);
        newArray[array.Length] = toAppend;
        return newArray;
    }

    private string GetNodeID(object[] keys)
    {
        List<string> clauses = new List<string>();

        for (int level = 0; level < Math.Min(keys.Length, 3); ++level)
        {
            string property = "NodeID";
            if (string.IsNullOrEmpty(property) || property.Equals("none", StringComparison.OrdinalIgnoreCase))
                break;

            Type cpType = CustomPropertyMgr.GetTypeForProp("Nodes", property);
            object value = CustomPropertyMgr.ChangeType(keys[level], cpType);

            return value.ToString();
        }

        return string.Join(" AND ", clauses.ToArray());
    }

    private static string NodeLink(NetflowSourceNode netflowSourceNode, bool hasTraffic)
    {
        string link = hasTraffic
            ? netflowSourceNode.IsWLC
                ? LinkBuilder.GetNpmNodeDetailsLink(netflowSourceNode.Node.NodeID)
                : LinkBuilder.GetNetflowNodeDetailsLink(netflowSourceNode.Node.NodeID)
            : string.Empty;

        return link;
    }

    private static string InterfaceLink(int InterfaceID, bool hasTraffic)
    {
        string link = hasTraffic
                  ? LinkBuilder.GetNetflowInterfaceDetailsLink(InterfaceID)
                  : string.Empty;

        return link;
    }

    public string ConvertTime(DateTime date)
    {
        if (date == DateTime.MinValue)
        {
            return Resources.NTAWebContent.NTAWEBCODE_AK0_9;
        }
        else
        {
            string strDate = date.ToString("d");
            string strTime = date.ToString("t");

            // replace long year from date to short year (yyyy to yy)
            string yyyy = date.ToString("yyyy");
            string yy = date.ToString("yy");

            strDate = strDate.Replace(yyyy, yy);

            return strDate + " " + strTime;
        }
    }

    private string NoSourcesMessage()
    {
        string noSources = string.Format(Resources.NTAWebContent.NTAWEBCODE_AK2_4,
                                         SolarWinds.Netflow.Web.UIConsts.URL_ORION_ADMIN_DISCOVERY,
                                         SolarWinds.Netflow.Web.UIConsts.URL_ORION_ADMIN_NODES_ADD,
                                         "/apps/nta-flowsources/management",
                                         "/apps/nta-cbqossources/management");

        return noSources;
    }

}