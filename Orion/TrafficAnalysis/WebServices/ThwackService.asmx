﻿<%@ WebService Language="C#" Class="ThwackService" %>

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Orion.Web;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ThwackService : WebService
{
    private const string ThwackUrl = @"http://thwackfeeds.solarwinds.com/products/community/NTA.aspx";
    private static readonly SolarWinds.Logging.Log Log = new SolarWinds.Logging.Log();

    [WebMethod]
    public string GetRecentNetFlowPosts(int count)
    {
        var posts = GetPostItems(ThwackUrl);
        return posts?.ItemList != null ? FormatRecentPostsTable(posts.ItemList, count) : GetErrorMessage();
    }

    private static PostItemList GetPostItems(string url)
    {
        try
        {
            return PostsManagerGeneric<PostItemList>.GetPostItemsList(url);
        }
        catch (Exception ex)
        {
            Log.Error("Unable to get post items", ex);
        }
        return null;
    }

    private static string FormatRecentPostsTable(List<PostItem> posts, int count)
    {
        return RenderTable(writer =>
        {
            if (posts.Any())
            {
                RenderPostListTableBody(posts, count, writer);
            }
            else
            {
                RenderEmptyListInfo(writer);
            }
        });
    }

    private static string RenderTable(Action<HtmlTextWriter> renderTableBody)
    {
        var content = new StringWriter();
        var writer = new HtmlTextWriter(content);

        writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
        writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
        writer.RenderBeginTag(HtmlTextWriterTag.Table);

        renderTableBody(writer);

        writer.RenderEndTag();

        return content.ToString();
    }

    private static void RenderEmptyListInfo(HtmlTextWriter writer)
    {
        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");

        writer.RenderBeginTag(HtmlTextWriterTag.Td);
        writer.RenderBeginTag(HtmlTextWriterTag.Ul);
        writer.RenderBeginTag(HtmlTextWriterTag.Li);

        writer.WriteEncodedText(Resources.NTAWebContent.NTAWEBCODE_TM0_17);

        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.RenderEndTag();
        writer.RenderEndTag();
    }

    private static void RenderPostListTableBody(IReadOnlyList<PostItem> posts, int count, HtmlTextWriter writer)
    {
        var realCount = Math.Min(count, posts.Count);
        for (var i = 0; i < realCount; i++)
        {
            if (i % 2 != 0)
            {
                writer.AddAttribute("class", "ZebraStripe");
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.AddAttribute(HtmlTextWriterAttribute.Href, posts[i].Link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.WriteEncodedText(posts[i].Title);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
    }

    private static string GetErrorMessage()
    {
        return RenderTable(writer =>
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "0px");
            writer.AddStyleAttribute(HtmlTextWriterStyle.Color, "red");

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.WriteEncodedText(Resources.NTAWebContent.NTAWEBCODE_TM0_18);

            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
            writer.RenderEndTag();
        });
    }
}