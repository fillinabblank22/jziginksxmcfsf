﻿<%@ WebService Language="C#" Class="DnsResolverService" %>

using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using SolarWinds.Logging;
using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Utils;
using SolarWinds.Orion.Common;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class DnsResolverService  : WebService
{
    private static readonly Log Log = new Log();

    private static object dnsResolveInProgressSyncObject = new object();
    private static HashSet<string> dnsResolveInProgress = new HashSet<string>();

    [WebMethod]
    public bool LookupHostname(string ipAddress)
    {
        string ipAddressTrimmed = ipAddress?.Trim();
        if (string.IsNullOrEmpty(ipAddressTrimmed))
        {
            return false;
        }

        if (OrionConfiguration.IsDemoServer)
        {
            return false;
        }

        var username = HttpContext.Current.Profile.UserName;
        if (!CanStartResolving(username))
        {
            Log.WarnFormat("User {0} requested DNS resolving before the previous request ended.", username);
            return false;
        }

        try
        {
            IPHostEntry ipHostEntry = DNSResolveHelper.GetIPv4HostEntry(ipAddressTrimmed, GlobalSettingsDALCached.DnsResolverUseNetbiosFunction);
            if (ipHostEntry != null)
            {
                string hostName = ipHostEntry.HostName?.Trim();
                if (!string.Equals(hostName, ipAddressTrimmed, StringComparison.OrdinalIgnoreCase))
                {
                    HostnameEditDAL.UpdateHostname(hostName, ipAddressTrimmed, GlobalSettingsDALCached.CacheExpirationDays);
                
                    FinishedResolving(username);
                    return true;
                }
            }
        }
        finally
        {
            FinishedResolving(username);
        }

        return false;
    }

    private static bool CanStartResolving(string username)
    {
        lock (dnsResolveInProgressSyncObject)
        {
            if (dnsResolveInProgress.Contains(username))
            {
                return false;
            }

            dnsResolveInProgress.Add(username);
        }

        return true;
    }

    private static void FinishedResolving(string username)
    {
        lock (dnsResolveInProgressSyncObject)
        {
            dnsResolveInProgress.Remove(username);
        }
    }
}