﻿<%@ WebService Language="C#" Class="NetflowNotificatorService" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using SolarWinds.Logging;
using SolarWinds.Orion.Web.DAL;
using SolarWinds.Netflow.Contracts;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class NetflowNotificatorService : WebService
{
	private const string SettingsKeyDismissedNotifications = "TrafficAnalysis_DismissedNofications";

	private static readonly Log _log = new Log();

	[WebMethod]
	public void DismissNotification(string notificationId)
	{
		try
		{
            if (!NetflowNotificationCodes.Codes.Contains(notificationId, StringComparer.OrdinalIgnoreCase))
            {
                throw new ArgumentOutOfRangeException(nameof(notificationId), notificationId, "Unknown NetFlow Notification ID");
            }

			// Get currently stored dismissed notifications.
			List<string> dismissedNofications =
				JsonConvert.DeserializeObject<List<string>>(
					WebUserSettingsDAL.Get(HttpContext.Current.Profile.UserName, SettingsKeyDismissedNotifications, "[]"))
				?? new List<string>();

			// Check if notification has already been dismissed.
			bool hasBeenDismissed = dismissedNofications.Any(s => s.Equals(notificationId, StringComparison.OrdinalIgnoreCase));

			if (!hasBeenDismissed)
			{
				// Add the notification to dismessed list and store it.
				dismissedNofications.Add(notificationId);

				WebUserSettingsDAL.Set(HttpContext.Current.Profile.UserName,
				                       SettingsKeyDismissedNotifications,
				                       JsonConvert.SerializeObject(dismissedNofications));
			}
		}
		catch (Exception ex)
		{
			_log.Warn("An error occurred when trying to store the notification in the Dismissed list.", ex);

			throw;
		}
	}
}