﻿<%@ WebService Language="C#" Class="TrafficViewBuilderService" %>

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using SolarWinds.Logging;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Contracts.Applications;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class TrafficViewBuilderService : WebService
{
    private static Log log = new Log();

    public class Result
    {
        public bool Success = true;
        public List<object> Items;
        public int ResultCount;
    }

    [WebMethod]
    public Result GetAdvancedApplications(string query)
    {
        var advancedApplications = TrafficViewBuilderDAL.GetAllAdvancedApplications(query);

        return GetAdvancedApplications(advancedApplications, new Dictionary<string, object>() { { "query", query } });
    }

    [WebMethod]
    public Result GetAdvancedApplication(int advancedApplicationId)
    {
        var advancedApplication = TrafficViewBuilderDAL.GetAdvancedApplication(advancedApplicationId);

        return GetAdvancedApplications(new[] { advancedApplication }, new Dictionary<string, object>() { { "advancedApplicationId", advancedApplicationId } });
    }

    /// <summary>
    /// Create web service result from array of advanced applications
    /// </summary>
    /// <param name="advancedApplications">Array of advanced applications</param>
    /// <param name="requestParams">Request params for logging purposes</param>
    /// <returns>Web service result object</returns>
    private Result GetAdvancedApplications(IEnumerable<AdvancedApplication> advancedApplications, Dictionary<string, object> requestParams)
    {
        var result = new Result();
        result.Success = true;
        result.Items = new List<object>();

        try
        {
            foreach (var advancedApplication in advancedApplications)
            {
                result.Items.Add(new
                {
                    id = advancedApplication.ID,
                    name = advancedApplication.Name,
                    iconClass = TrafficViewBuilderDAL.GetAdvancedApplicationIconCssClassName(advancedApplication),
                });
            }

            result.ResultCount = result.Items.Count;
            return result;
        }
        catch (Exception exception)
        {
            var errorMessage =
                new StringBuilder(
                    "Exception occured during advanced applications data request. Returning no records to the client.");

            foreach (var requestParam in requestParams)
            {
                errorMessage
                    .AppendLine()
                    .AppendFormat("{0}={1}", requestParam.Key, requestParam.Value);
            }
            log.Error(errorMessage, exception);
            result.Success = false;
            return result;
        }
        finally
        {
            log.DebugFormat("End TrafficViewBuilder advanced applications data request handler. ResultSuccess={0}, ResultCount={1}", result.Success, result.ResultCount);
        }
    }
}
