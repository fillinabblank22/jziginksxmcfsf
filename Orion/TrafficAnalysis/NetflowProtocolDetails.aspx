<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowProtocolDetails.aspx.cs" Inherits="Orion_Netflow_ProtocolDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
        <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.NetObject.Name) %>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:ProtocolResourceHost runat="server" ID="protResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:ProtocolResourceHost>
</asp:Content>