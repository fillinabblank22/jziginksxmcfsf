﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.Filters;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web.UI.TrafficViewBuilder;
using System.IO;
using System.Linq.Dynamic;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Logging;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Orion.Web.Alerts;
using System.Text;
using Resources;
using SolarWinds.Netflow.Web.UI.AlertBuilder;
using SolarWinds.Netflow.Web.DALInterfaces;

public partial class Orion_TrafficAnalysis_AlertBuilder_AlertNetFlowSourceArea : UserControl, IAlertNetFlowSource
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private IAlertBuilderDAL _alertBuilderDAL;
    protected IAlertBuilderDAL alertBuilderDAL
    {
        get
        {
            if (_alertBuilderDAL == null)
            {
                _alertBuilderDAL = new AlertBuilderDAL();
            }

            return _alertBuilderDAL;
        }
    }

    public string NetflowSourceString
    {
        get
        {
            if (string.IsNullOrEmpty(nodeString))
            {
                return "";
            }

            if (string.IsNullOrEmpty(interfaceString))
            {
                return $"{nodeString}"; 
            }

            return $"{nodeString} - {interfaceString}";
        }
    }

    protected long nodeID;
    protected long interfaceID;

    protected string nodeString;
    protected string interfaceString;

    public bool ShowNetFlowSource(INetflowSourceFilter filter)
    {
        string netFlowSourceString = NTAWebContent.NTAWEBDATA_AlertBuilder_NoNetflowSourceSelected;

        bool loadUI = false;

        var netflowSource = filter;

        bool isNodeFilter = netflowSource is NodeFilter;
        bool isInterfaceFilter = netflowSource is InterfaceFilter;

        if (isNodeFilter || isInterfaceFilter)
        {
            if (nodeID != netflowSource.NodeID)
            {
                nodeID = netflowSource.NodeID;
                nodeString = alertBuilderDAL.GetNodeName(netflowSource.NodeID);
            }

            if (isInterfaceFilter)
            {
                var iID = ((INetflowInterfaceFilter) netflowSource).InterfaceID;
                if (interfaceID != iID)
                {
                    interfaceID = iID;
                    interfaceString = alertBuilderDAL.GetInterfaceName(iID);
                }
            }
            else
            {
                interfaceID = 0;
                interfaceString = null;
            }
            
            var netflowSourceDescription = isInterfaceFilter
                ? string.Format(NTAWebContent.NTAWEBDATA_AlertBuilder_FieldNetflowSourceInterfaceDescription,
                                nodeString,
                                interfaceString)
                : string.Format(NTAWebContent.NTAWEBDATA_AlertBuilder_FieldNetflowSourceNodeDescription,
                                nodeString);

            netFlowSourceString = string.Format(NTAWebContent.NTAWEBDATA_AlertBuilder_FieldNetflowSourceInfoText,
                                                netflowSourceDescription);

            loadUI = true;
        }
        else
        {
            log.Debug("No interface or node filter found.");

            interfaceID = 0;
            interfaceString = null;
            nodeID = 0;
            nodeString = null;
        }

        SelectedNetflowSource.Text = netFlowSourceString;
        return loadUI;
    }
}