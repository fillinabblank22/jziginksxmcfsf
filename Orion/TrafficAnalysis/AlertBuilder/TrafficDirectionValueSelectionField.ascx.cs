﻿using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.AlertBuilder;
using System;

public partial class Orion_TrafficAnalysis_AlertBuilder_TrafficDirectionValueSelectionField : UserControl, ITopXAppsTrafficOptions
{
    public FlowDirectionFilter.FlowDirectionType Direction
    {
        get {
            int i;
            if (int.TryParse(alertDirectionDropDown.SelectedValue, out i))
            {
                return (FlowDirectionFilter.FlowDirectionType)i;
            }
            return FlowDirectionFilter.FlowDirectionType.Both;
        }
    }

	public short Value
	{
		get { return short.Parse(alertTopXApplicationsValue.Text); }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (!IsPostBack)
		{
			TopValueEmptyValidator.DataBind(); // required for <%# ... %> tags
			TopValueFieldLimitationValidator.DataBind(); // required for <%# ... %> tags
		}
	}
}