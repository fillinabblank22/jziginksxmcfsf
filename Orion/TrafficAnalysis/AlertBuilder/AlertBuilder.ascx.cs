using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using System.IO;
using System.Web;
using SolarWinds.Orion.Web.Alerts;
using Resources;
using SolarWinds.Internationalization.Exceptions;
using SolarWinds.Netflow.Web.Reporting.AlertAdmin.AlertProviders;
using SolarWinds.Netflow.Web.UI.AlertBuilder;
using SolarWinds.Netflow.Web.Reporting.AlertAdmin;
using SolarWinds.Netflow.Web.UI.AlertBuilder.Validators;
using SolarWinds.Orion.Web.Platform;

public interface IControlDefinition
{
    string VirtualPath { get; }
    string ID { get; }
    string Div { get; }
    void Init(Page page, Control alertBuilder);
}

public class ControlDefinition<T> : IControlDefinition where T : IAlertBuilderControl
{
    public string VirtualPath { get; private set; }
    private T control; 
    public string ID { get { return "General_" + Path.GetFileNameWithoutExtension(VirtualPath); } }
    public string Div { get; private set; }

    public ControlDefinition(string vPath, string div)
    {
        VirtualPath = vPath;
        Div = div;
    }

    public void Init(Page page, Control alertBuilder)
    {
        if (control == null)
        {
            var div = alertBuilder.FindControl(Div);
            var c = page.LoadControl(VirtualPath);
            c.ID = ID; // Not storing always the same ID causes the control to loose its state during postbacks.
            control = (T) (object) c;
            div.Controls.Add(c);
        }
    }

    public T GetControl()
    {
        return control;
    }
}


public partial class Orion_TrafficAnalysis_AlertBuilder_AlertBuilder : UserControl, IFiltersUpdater
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    private readonly INetflowAlertSettings alertSettings;
    private readonly List<AlertTypeUIDefinition> alertTypes;

    protected NetflowView view;

    protected IFilterCollection filters;

    public void UpdateFilters()
    {
        LoadFilters();

        LoadUI();

        // this is from external source, it probably should be enough to update here
        // all other events come directly from the same update panel so it should not be needed anywhere else
        AlertUpdatePanel.Update();
    }

    private readonly List<IControlDefinition> controls = new List<IControlDefinition>()
    {
        new ControlDefinition<IAlertNetFlowSource>("~/Orion/TrafficAnalysis/AlertBuilder/AlertNetFlowSourceArea.ascx", "AlertNetflowSourceArea" ),
        new ControlDefinition<ITriggerConditionFilters>("~/Orion/TrafficAnalysis/AlertBuilder/TriggerConditionFilters.ascx", "TriggerConditionFiltersDiv" ),
        new ControlDefinition<ILastXMinutesValue>("~/Orion/TrafficAnalysis/AlertBuilder/LastXMinutesField.ascx", "LastXMinutesFieldDiv" ),
        new ControlDefinition<IDirectionalThresholdValues>("~/Orion/TrafficAnalysis/AlertBuilder/DirectionalTrafficThresholdField.ascx", "TriggerConditionFieldDiv" ),
		new ControlDefinition<ITopXAppsTrafficOptions>("~/Orion/TrafficAnalysis/AlertBuilder/TrafficDirectionValueSelectionField.ascx", "TrafficDirectionValueSelectionDiv" ),
	};

    public Orion_TrafficAnalysis_AlertBuilder_AlertBuilder()
    {
        this.alertSettings = HttpContext.Current.Resolve<INetflowAlertSettings>();
        this.alertTypes = LoadAlertTypes();
    }

    private void InitControls()
    {
        foreach (var control in controls)
        {
            control.Init(Page, this);
        }
    }

    private T GetControl<T>() where T : IAlertBuilderControl
    {
        var cDef = (ControlDefinition<T>)controls.FirstOrDefault(x => x is ControlDefinition<T>);
        if (cDef != null)
        {
            return cDef.GetControl();
        }
        else return default(T);
    }

    private List<AlertTypeUIDefinition> LoadAlertTypes()
    {
        return new List<AlertTypeUIDefinition>
        {
            new AlertTypeUIDefinition
            {
                // Application Threshold
                ID = 1,
                AlertTypeString = NTAWebContent.NTAWEBDATA_AlertBuilder_ApplicationThresholdAlertTypeDescription,
                AlertNameString = NTAWebContent.NTAWEBDATA_AlertBuilder_ApplicationThresholdAlertShortName,
                ApplicableFilters = new[] { typeof(ApplicationFilter), typeof(AdvancedApplicationFilter) },
                OptionalFilters = new [] { typeof(IPAddressGroupFilter), typeof(EndpointFilter) },
                FilterValidator = new ApplicationThresholdAlertValidator(),
                AlertDefinitionProvider = new ApplicationThresholdAlertDefinitionProvider(alertSettings),
                TriggerConditionFieldType = TriggerConditionFieldType.DirectionalThreshold,
            },
            new AlertTypeUIDefinition
            {
                ID = 2,
                AlertTypeString = NTAWebContent.NTAWEBDATA_AlertBuilder_NotInTopApplicationsAlertTypeDescription,
                AlertNameString = NTAWebContent.NTAWEBDATA_AlertBuilder_NotInTopApplicationsAlertShortName,
                ApplicableFilters = new[] { typeof(ApplicationFilter), typeof(AdvancedApplicationFilter) },
                OptionalFilters = new [] { typeof(IPAddressGroupFilter), typeof(EndpointFilter) },
                FilterValidator = new ApplicationInTopXAppsAlertFilterValidator(),
                AlertDefinitionProvider = new ApplicationInTopXAppsAlertDefinitionProvider(false, alertSettings),
                TriggerConditionFieldType = TriggerConditionFieldType.ApplicationSelector
            },
            new AlertTypeUIDefinition
            {
                ID = 3,
                AlertTypeString = NTAWebContent.NTAWEBDATA_AlertBuilder_NoFlowDataAlertTypeDescription,
                AlertNameString = NTAWebContent.NTAWEBDATA_AlertBuilder_NoFlowDataAlertShortName,
                AlertDefinitionProvider = new NoFlowDataAlertDefinitionProvider(alertSettings),
                FilterValidator = new DefaultAlertFilterValidator(),
                TriggerConditionFieldType = TriggerConditionFieldType.None
            },
            new AlertTypeUIDefinition
            {
                ID = 4,
                AlertTypeString = NTAWebContent
                    .NTAWEBDATA_AlertBuilder_UnexpectedlyInTopApplicationsAlertTypeDescription,
                AlertNameString = NTAWebContent.NTAWEBDATA_AlertBuilder_UnexpectedlyInTopApplicationsAlertShortName,
                ApplicableFilters = new[] { typeof(ApplicationFilter), typeof(AdvancedApplicationFilter) },
                OptionalFilters = new [] { typeof(IPAddressGroupFilter), typeof(EndpointFilter) },
                FilterValidator = new ApplicationInTopXAppsAlertFilterValidator(),
                AlertDefinitionProvider = new ApplicationInTopXAppsAlertDefinitionProvider(true, alertSettings),
                TriggerConditionFieldType = TriggerConditionFieldType.ApplicationSelector
            },
        }.OrderBy(order => order.AlertTypeString).ToList();
    }

    protected int SelectedAlertType
    {
        get
        {
            int i;
            if (int.TryParse(alertTypeDropdown.SelectedValue, out i))
            {
                return i;
            }
            return 1;
        }
    }

    protected AlertTypeUIDefinition SelectedAlertTypeDefinition { get { return alertTypes.FirstOrDefault(a => a.ID == SelectedAlertType); } }

    protected void AlertTypeChanged_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadFilters();

        LoadUI();
    }

    private void GenerateAlertName(string alertNameString)
    {
        var netFlowSource = GetControl<IAlertNetFlowSource>();

        var generatedName = string.Format(NTAWebContent.NTAWEBDATA_AlertBuilder_DefaultAlertNameTemplate,
                                          alertNameString,
                                          netFlowSource.NetflowSourceString);

        if (string.IsNullOrEmpty(alertNameTextBox.Text) 
            || string.IsNullOrEmpty(alertNameGenerated.Text)
            || alertNameTextBox.Text.Equals(alertNameGenerated.Text))
        {
            alertNameTextBox.Text = generatedName;
        }

        alertNameGenerated.Text = generatedName;
    }

    private void ShowUIForAlertType()
    {
        AlertButtonArea.Visible = true;

        var uiDef = SelectedAlertTypeDefinition;

        if (uiDef != null)
        {
            GenerateAlertName(uiDef.AlertNameString);

            AlertCommonSettings.Visible = true;
            AlertTriggerConditionArea.Visible = true;
            AlertWizardRedirectArea.Visible = true;

            var lastXMinutes = GetControl<ILastXMinutesValue>();
            lastXMinutes.SetLimitation(uiDef.AlertDefinitionProvider.MinimumMinuteInterval, uiDef.AlertDefinitionProvider.MaximumMinuteInterval);

            TriggerConditionFieldDiv.Visible = false;
			TrafficDirectionValueSelectionDiv.Visible = false;
			TriggerConditionFiltersDiv.Visible = true;

			if (uiDef.TriggerConditionFieldType == TriggerConditionFieldType.DirectionalThreshold)
			{
				TriggerConditionFieldDiv.Visible = true;
			}
			else if (uiDef.TriggerConditionFieldType == TriggerConditionFieldType.None)
			{
				TriggerConditionFiltersDiv.Visible = false;
			}
			else if (uiDef.TriggerConditionFieldType == TriggerConditionFieldType.ApplicationSelector)
			{
				TrafficDirectionValueSelectionDiv.Visible = true;
			}
			               
            var triggerConditionFilters = GetControl<ITriggerConditionFilters>();
            triggerConditionFilters.UpdateFilterCollection(filters);
            triggerConditionFilters.ChangeAlertType(uiDef);

            if (triggerConditionFilters.ApplicableFiltersPresent)
            {
                btnSubmit.Enabled = true;
            }
            else
            {
                btnSubmit.Enabled = false;
            }
        }
    }

    private void HideUI()
    {
        AlertCommonSettings.Visible = false;
        AlertTriggerConditionArea.Visible = false;
        AlertWizardRedirectArea.Visible = false;
        AlertButtonArea.Visible = false;
        btnSubmit.Enabled = false;
    }

    private void LoadFilters()
    {
        var loadedFromUrl = false;
        filters = view.ReadAlertFiltersFromTrafficViewBuilder();

        // Filters are not prepared to be read from flow navigator, taking current value from URL.
        if (filters == null)
        {
            loadedFromUrl = true;
            filters = view.NetflowObject.Filters;
        }

        if (log.IsDebugEnabled)
        {
            log.DebugFormat("Loading filters from {1}: {0}", filters.FilterID, loadedFromUrl ? "URL" : "view");
        }
        
        var triggerConditionFilters = GetControl<ITriggerConditionFilters>();
        triggerConditionFilters.UpdateFilterCollection(filters);
    }

    private void LoadUI()
    {
        if (!Profile.AllowAlertManagement)
        {
            Visible = false;
        }
        else if (filters != null)
        {            
            var c = GetControl<IAlertNetFlowSource>();
            bool loadAlertContent = c.ShowNetFlowSource(filters.NetflowSource);

            if (loadAlertContent)
            {
                ShowUIForAlertType();
            }
            else
            {
                HideUI();
            }
        }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.view = this.Page as NetflowView;
        if (view == null)
        {
            throw new InvalidOperationException("Alert Builder can be placed only on NetFlow View."); // This is exception which can appear only during development when the control is used in wrong way
        }

        SolarWinds.Orion.Web.OrionInclude.ExternalFile("/Orion/TrafficAnalysis/styles/AlertBuilder.css", SolarWinds.Orion.Web.UI.Localizer.PathFinder.ResourceType.CascadingStyleSheet);

        if (!IsPostBack)
        {
            alertTypeDropdown.DataSource = alertTypes;
            alertTypeDropdown.DataBind();
            alertTypeDropdown.SelectedValue = alertTypes.First(x => x.ID == 1).ID.ToString();

            var severityNameMapping = new[]
            {
                NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityValueInformational,
                NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityValueWarning,
                NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityValueCritical,
                NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityValueSerious,
                NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityValueNotice,
            };
            BindEnumToDropbox(typeof(AddAlertWorkflowDefinition.AddAlertSeverity), severityNameMapping, alertSeverityDropdown, new []{ 4, 0, 1, 3, 2});

            AlertNameRequiredValidator.DataBind(); // required for <%# ... %> tags
        }

        InitControls();
        LoadFilters();
        LoadUI();

        btnSubmit.OnClientClick = "return CreateAlertClickHandler();";
    }

    private void BindEnumToDropbox(Type enumName, string[] nameMapping, DropDownList dropDown, int[] bindOrder)
    {
        Array values = Enum.GetValues(enumName);

        for (int i = 0; i <= bindOrder.Length - 1; i++)
        {
            dropDown.Items.Add(new ListItem(nameMapping[bindOrder[i]], Convert.ToInt32(values.GetValue(bindOrder[i])).ToString()));
        }
    }

    private AddAlertWorkflowDefinition BuildAlertDefinition()
    {
        // To be sure we have current settings from Flow Navigator.
        LoadFilters();

        var provider = SelectedAlertTypeDefinition.AlertDefinitionProvider;

        var directionalThresholdValues = GetControl<IDirectionalThresholdValues>();
		var topAppsOptions = GetControl<ITopXAppsTrafficOptions>();
		var lastXMinutes = GetControl<ILastXMinutesValue>();        

        var parameters = new AlertDefinitionParameters()
        {
            Filters = filters,
            TrafficDirection = SelectedAlertTypeDefinition.TriggerConditionFieldType == TriggerConditionFieldType.DirectionalThreshold ? directionalThresholdValues.Direction : topAppsOptions.Direction,
            MinutesInterval = lastXMinutes.Value,
            BpsThreshold = Convert.ToInt64(directionalThresholdValues.ThresholdValue),
            ThresholdComparisonOperator = directionalThresholdValues.ConditionOperator,
			TopXApplications = topAppsOptions.Value
		};

        if (log.IsDebugEnabled)
        {
            log.DebugFormat("Read alert definition parameters: {0}", parameters.GetDebugString());
        }

        var alert = provider.GetAlertDefinition(parameters);

        var aDef = new AddAlertWorkflowDefinition
        {
            Name = alertNameTextBox.Text,
            Description = $"NTA: {SelectedAlertTypeDefinition.AlertTypeString}",
            Frequency = alertSettings.AlertEvaluationFrequence,
            Severity = (AddAlertWorkflowDefinition.AddAlertSeverity) int.Parse(alertSeverityDropdown.SelectedValue),
            AlertMessage = alert.Message,
            ObjectType = alert.ObjectType,
            TriggerConditionType = AddAlertWorkflowDefinition.AddAlertTriggerConditionType.CustomSwql,
            TriggerCondition = alert.SwqlQuery,
            InitialWizardStep = AddAlertWorkflow.AlertStepType.Summary,
            DirectSubmit = !openAlertEditorCheckBox.Checked // If the button is checked we need revert the value to False to redirect to Alert editor
        };

        if (log.IsDebugEnabled)
        {
            log.Debug($@"Created AddAlertWorkflowDefinition:
{nameof(aDef.Name)}: {aDef.Name}
{nameof(aDef.Description)}: {aDef.Description}
{nameof(aDef.Frequency)}: {aDef.Frequency}
{nameof(aDef.Severity)}: {aDef.Severity}
{nameof(aDef.AlertMessage)}: {aDef.AlertMessage}
{nameof(aDef.ObjectType)}: {aDef.ObjectType}
{nameof(aDef.TriggerConditionType)}: {aDef.TriggerConditionType}
{nameof(aDef.TriggerCondition)}: {aDef.TriggerCondition}
{nameof(aDef.InitialWizardStep)}: {aDef.InitialWizardStep}
{nameof(aDef.DirectSubmit)}: {aDef.DirectSubmit}");
        }

        return aDef;
    }

    protected void SubmitClick(object sender, EventArgs e)
    {
        string url;
        try
        {
            log.Debug("Submit button clicked. Creating an alert.");

            var workflowDefinition = BuildAlertDefinition();
            
            AddAlertWorkflow workflow = AddAlertWorkflow.CreateFrom(workflowDefinition);
            url = workflow.GetCurrentStepRedirectUrl();
            
            log.DebugFormat("Wizard session populated. Creating an alert by redirecting to URL: {0}", url);
            Response.Redirect(url);
        }
        catch (Exception ex)
        {
            string msg = ex is IHasLocalizedMessage ? (ex as IHasLocalizedMessage).LocalizedMessage : ex.Message;
            ScriptManager.RegisterStartupScript(this.btnSubmit, this.btnSubmit.GetType(), "AlertBuilderSubmitErrorMessage", "alert('" + msg + "');", true);
        }
    }
}
