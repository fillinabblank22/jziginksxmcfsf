﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TriggerConditionFilters.ascx.cs" Inherits="Orion_TrafficAnalysis_AlertBuilder_TriggerConditionFilters" %>

<div ID="ConditionTriggerFilters">
    <p><%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_TriggerConditionFiltersMainLabel%></p>
    <div ID="AlertApplicableFilters" style="padding: 0 10px">
        <!--<asp:Label ID="FilterCollectionLabel" runat="server"></asp:Label>-->
        <asp:Label ID="SelectedFiltersLabel" runat="server" style="word-break: break-all;"></asp:Label>
    </div>
</div>
         
