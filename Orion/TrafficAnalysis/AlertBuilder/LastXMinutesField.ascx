<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LastXMinutesField.ascx.cs" Inherits="Orion_TrafficAnalysis_AlertBuilder_LastXMinutesField" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI.AlertBuilder" %>
<%@ Import Namespace="Resources" %>


<p>
    <%= NTAWebContent.NTAWEBDATA_AlertBuilder_LastXMinutesLabelFirstHalf %>
    <asp:TextBox ID="AlertLastXMinutesValue" width="20px" runat="server" Text="5"></asp:TextBox>
    <%= NTAWebContent.NTAWEBDATA_AlertBuilder_LastXMinutesLabelSecondHalf %>
    <span class="validators">
        <asp:RequiredFieldValidator runat="server" ID="AlertLastXMinutesValueRequiredValidator"
                                    ControlToValidate="AlertLastXMinutesValue" 
                                    ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                    ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_CannotBeEmpty%>"
                                    Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_CannotBeEmpty%>">&nbsp;</a>
        </asp:RequiredFieldValidator>
        <asp:RangeValidator runat="server" ID="AlertLastXMinutesValueValidator"
                                        ControlToValidate="AlertLastXMinutesValue"
                                        ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                        ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_LastXMinutesInvalid%>"
                                        MinimumValue = "5" MaximumValue="15" Type="Integer"
                                        Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_LastXMinutesInvalid%>">&nbsp;</a>
        </asp:RangeValidator>
    </span>
</p>

