﻿using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI.AlertBuilder;
using System;

public partial class Orion_TrafficAnalysis_AlertBuilder_DirectionalTrafficThresholdField : UserControl, IDirectionalThresholdValues
{
	public FlowDirectionFilter.FlowDirectionType Direction
	{
		get
		{
			int i;
			if (int.TryParse(alertDirectionDropDown.SelectedValue, out i))
			{
				return (FlowDirectionFilter.FlowDirectionType)i;
			}
			return FlowDirectionFilter.FlowDirectionType.Both;
		}
	}

	public string ConditionOperator
    {
        get { return alertOperatorDropDown.SelectedItem.Text; }
    }

    public double ThresholdValue
    {
        get {
            double d;
            if (double.TryParse(alertThresholdValue.Text, out d))
            {
                return d * SelectedThresholdUnit;
            }
            return 0f;
        }
    }

    public long SelectedThresholdUnit
    {
        get
        {
            long l;
            if (long.TryParse(bpsUnitDropDown.SelectedValue, out l))
            {
                return l;
            }
            return 1;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            ThresholdValueRequiredValidator.DataBind(); // required for <%# ... %> tags
            ThresholdValueValidator.DataBind(); // required for <%# ... %> tags
        }
    }
}