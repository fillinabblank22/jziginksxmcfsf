﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common.Filters;
using System.Linq.Dynamic;
using System.Text;
using Resources;
using SolarWinds.Netflow.Web.UI.AlertBuilder;

public partial class Orion_TrafficAnalysis_AlertBuilder_TriggerConditionFilters : TriggerConditionFiltersBase
{
    protected string Filters;

    protected override void UpdateFiltersString()
    {
        Filters = TranslateFiltersToString();
        SelectedFiltersLabel.Text = string.IsNullOrEmpty(Filters) ? string.Empty : Filters;
    }

    private string TranslateFiltersToString()
    {
        ApplicableFiltersPresent = false;

        if (NoFiltersRequired)
        {
            ApplicableFiltersPresent = true;
            return NTAWebContent.NTAWEBDATA_AlertBuilder_TriggerConditionFiltersNoFiltersRequired;
        }

        var applicableFilters = GetAcceptedFilters();
        if (applicableFilters == null)
        {
            return GetEmptyFiltersString();
        }

        var validationResult = FilterValidator.Validate(applicableFilters);
        if (!validationResult.IsValid)
        {
            return validationResult.FailureDisplayText;
        }
        ApplicableFiltersPresent = true;

        var sb = new StringBuilder();
        sb.Append(GetStringForFilters(applicableFilters));
        if (sb.Length == 0)
        {
            return GetEmptyFiltersString();
        }

        var optionalFilters = GetOptionalFilters();
        sb.Append(GetStringForFilters(optionalFilters));

        return sb.ToString();
    }

    private string GetStringForFilters(IEnumerable<INetflowFilter> filters)
    {
        var sb = new StringBuilder();
        foreach (var filter in filters)
        {
            var filterStr = BuildStringForFilter(filter);

            if (filterStr != null)
            {
                sb.Append(filterStr);
            }
        }

        return sb.ToString();
    }

    private string GetEmptyFiltersString()
    {
        var list = ApplicableFilterTypes.ToList().ConvertAll(x => "<b>" + GetFilterTypeString(x) + "</b>");
        var strlist = string.Join(NTAWebContent.NTAWEBDATA_AlertBuilder_TriggerConditionFiltersFilterOrSeparator, list);
        var emptyString = string.Format(NTAWebContent.NTAWEBDATA_AlertBuilder_TriggerConditionFiltersNoApplicableIncludeFilters, strlist);

        return emptyString;
    }

    protected string BuildStringForFilter(INetflowFilter filter)
    {
        var f = filter as NetflowFilterWithNameBase;

        if (f == null)
        {
            return null;
        }

        if (filter is ApplicationFilter && filter.IsExclusive)
        {
            return null;
        }

        IEnumerable<string> filterItems = f.FilterNames;
        // This is here only because Application Threshold does not support port application filters.
        if (filter is ApplicationFilter)
        {
            var af = filter as ApplicationFilter;
            filterItems = af.DeclaredOnlyApplicationsNames;
        }

        if (!filterItems.Any())
        {
            return null;
        }
        
        var delimiter = NTAWebContent.NTAWEBDATA_AlertBuilder_TriggerConditionFiltersFilterAndSeparator;

        return
            $"{f.FilterTitleSimplified} <b>{string.Join("</b>" + delimiter + "<b>", filterItems)}</b><br/>";

    }

    private string GetFilterTypeString(Type filterType)
    {

        if (filterType == typeof(ApplicationFilter))
        {
            return NTAWebContent.NTAWEBCODE_TM0_88;
        }
        if (filterType == typeof(AdvancedApplicationFilter))
        {
            return NTAWebContent.NTAWEBCODE_TrafficViewBuilder_CategoryTitle_AdvancedApplications;
        }

        return "";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            UpdateFiltersString();
        }
    }
}
