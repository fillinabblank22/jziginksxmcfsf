﻿using System;
using System.Web.UI;
using SolarWinds.Netflow.Web.UI.AlertBuilder;

public partial class Orion_TrafficAnalysis_AlertBuilder_LastXMinutesField : UserControl, ILastXMinutesValue
{
    public int Value
    {
        get { return int.Parse(AlertLastXMinutesValue.Text); }
    }

    public void SetLimitation(int minimumValue, int maximumValue)
    {
        AlertLastXMinutesValueValidator.MinimumValue = minimumValue.ToString();
        AlertLastXMinutesValueValidator.MaximumValue = maximumValue.ToString();

        if (Value < minimumValue || Value > maximumValue)
        {
            AlertLastXMinutesValue.Text = minimumValue.ToString();
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!IsPostBack)
        {
            AlertLastXMinutesValueRequiredValidator.DataBind(); // required for <%# ... %> tags
            AlertLastXMinutesValueValidator.DataBind(); // required for <%# ... %> tags
        }
    }
}