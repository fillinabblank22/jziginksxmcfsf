﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertBuilder.ascx.cs" Inherits="Orion_TrafficAnalysis_AlertBuilder_AlertBuilder" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI.AlertBuilder" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI" %>
<%@ Register TagPrefix="orion" TagName="HelpButton" Src="~/Orion/Controls/HelpButton.ascx" %>
<%@ Register TagPrefix="orion" Namespace="SolarWinds.Orion.Web.UI" Assembly="OrionWeb" %>

<orion:Include Module="TrafficAnalysis" runat="server" File="DemoScripts.js" />

<script type="text/javascript">
    $(document).on('change', '#<%=alertTypeDropdown.ClientID%>', function (e) {
        $(e.target).prop('disabled', true);
        $('#alertTypeElementsToHide').hide(500);
        $('#alertTypeContentLoader').show(0);

        // NTA-7236 calling validation over non-existing group to reset validators that otherwise cause page irresponsivness due to blocked postbacks
        Page_ClientValidate('ResetAllPageValidations');
    });

    function AB_Collapse() {
        $('#abExpandedTd').hide(500);
        $('#abCollapsedTd').show(500);
        // when autorefresher is active and TVB is hidden then autorefresher is enabled
        if (typeof orionPageRefreshTimeout != 'undefined' && typeof orionPageRefreshMilSecs != 'undefined')
            orionPageRefreshTimeout = setTimeout('window.location.reload(true)', orionPageRefreshMilSecs);
    }

    function CreateAlertClickHandler() {
        if (Page_ClientValidate('<%= AlertBuilderControlProperties.ValidationGroupName %>')) {
            if (IsDemo() && !$('#<%= openAlertEditorCheckBox.ClientID %>').is(':checked')) {
                DisplayWarning('<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_DemoWarningMessageText) %>');
                return false;
            } else {
                $("#abSubmitDiv").hide(0);
                $("#absubmittingDiv").show(0);
                return true;
            }
        }
        return false;
    }
</script>

<div id="abCollapsedTd" valign="top" class="AlertBuilderPanelCollapsed" style="cursor: pointer" onmousedown="
                 $('#abExpandedTd').show(500);
                 $('#abCollapsedTd').hide(500);
                 // when autorefresher is active and AB is displayed then autorefresher is disabled
                 if (typeof orionPageRefreshTimeout != 'undefined') 
                    clearTimeout(orionPageRefreshTimeout);                 
                 ">
    <img src="/Orion/TrafficAnalysis/images/Button.TrafficViewBuilder.Expand.png" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_27 %>" id="expandButton" />    
    <img src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("TrafficAnalysis", "AlertBuilder.CollapsedHeader.Title.gif") %>" style="padding-left: 2px" />
</div> 
<div id="abExpandedTd" class="AlertBuilderPanelExpanded" style="display: none">
    <div class="AlertBuilderPanelTitle">
        <table width="100%">
            <tr>
                <td style="width: 99%; cursor: pointer" onmousedown="AB_Collapse()">
		            <%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_Title%>
                </td>
                <td align="right" style="width: 1%">
                    <div style="padding-top: 1px; padding-right: 5px;">			
                        <orion:helpbutton id="AlertBuilderHelpButton" helpurlfragment="OrionNetFlowAG_AlertsPredefinedNetflowSpecific" runat="server" />
                    </div>
                </td>
                <td align="right" style="width: 1%; cursor: pointer" onmousedown="AB_Collapse()">
                    <img src="/Orion/TrafficAnalysis/images/Button.TrafficViewBuilder.Collapse.png" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_28 %>"
                        id="Img1" />
                </td>
            </tr>
        </table>
    </div>
    
    <div ID="AlertBuilderContent">

        <asp:UpdatePanel ID="AlertUpdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div ID="AlertNetflowSourceArea" class="AbArea" runat="server"></div>
                
                <div ID="AlertCommonSettings" runat="server">
                    <div ID="AlertNameArea" class="AbArea">
                        <h4><%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_FieldAlertNameLabel%></h4>
                        <asp:TextBox id="alertNameGenerated" runat="server" visible="false" />
                        <asp:TextBox id="alertNameTextBox" runat="server" Width="100%" /> 
                        <span class="validators">
                            <asp:RequiredFieldValidator runat="server" ID="AlertNameRequiredValidator"
                                                        ControlToValidate="alertNameTextBox" 
                                                        ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                                        ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_CannotBeEmpty%>"
                                                        Display="Dynamic">
                                <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_CannotBeEmpty%>">&nbsp;</a>
                            </asp:RequiredFieldValidator>
                        </span>
                    </div>
        
                    <div ID="SeverityArea" class="AbArea">
                        <h4><%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_FieldSeverityLabel%></h4>
                        <asp:DropDownList runat="server" 
                                          ID="alertSeverityDropdown" 
                                          DataTextField="AlertTypeString" 
                                          DataValueField="ID" 
                                          Width="100%" 
                                          AppendDataBoundItems="True">
                        </asp:DropDownList> 
                    </div>
                </div>

                <div ID="AlertTriggerConditionArea" runat="server" class="AbArea">
                    <h4><%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_FieldTriggerConditionLabel%></h4>
                    <div ID="AlertTypeArea" class="AbArea">
                        <asp:DropDownList runat="server" 
                                          ID="alertTypeDropdown" 
                                          DataTextField="AlertTypeString" 
                                          DataValueField="ID" 
                                          Width="100%" 
                                          AppendDataBoundItems="True" 
                                          AutoPostBack="true"
                                          OnSelectedIndexChanged="AlertTypeChanged_SelectedIndexChanged">
                        </asp:DropDownList>
                        <div class="AdSubmit" id="alertTypeContentLoader" style="display: none">
                            <img src="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" alt="" />&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_96%>
                        </div>
                    </div>
                    <div id="alertTypeElementsToHide">
                        <div ID="TrafficDirectionValueSelectionDiv" runat="server" class="AbArea"></div>
                        <div ID="TriggerConditionFieldDiv" runat="server" class="AbArea"></div>
                        <div ID="LastXMinutesFieldDiv" runat="server" class="AbArea"></div>
                        <div ID="TriggerConditionFiltersDiv" runat="server" class="AbArea"></div>
                    </div>
                </div>
                
                <div ID="AlertWizardRedirectArea" class="AbArea" runat="server">
                    <div ID="AlertWizardRedirectInfo">
                        <div class="icon">
                            <i class="xui-icon xui-icon-severity_info  filled " is-dynamic="true" icon="severity_info">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" x="0px" y="0px" viewBox="0 0 20 20" width="20" height="20" enable-background="new 0 0 20 20" xml:space="preserve">
                                    <circle fill="#FFFFFF" stroke="#297994" stroke-miterlimit="10" cx="10" cy="10" r="9.5"></circle>
                                    <path fill="#297994" d="M9,5h2v2H9V5z M9,15h2V9H9V15z"></path>
                                </svg>
                            </i>
                        </div>
                        <div class="text">
                            <p><%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_OpenAlertEditorDescription %></p> 
                            <asp:CheckBox id="openAlertEditorCheckBox" text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_OpenAlertEditorHoverText %>" runat="server" /> 
                        </div>
                    </div>
                </div>
                <div ID="AlertButtonArea" runat="server">
                    <div class="sw-btn-bar" id="abSubmitDiv">
                        <orion:LocalizableButton Text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_SubmitButtonText %>" runat="server" ID="btnSubmit" LocalizedText="CustomText" 
                                                 OnClick="SubmitClick" DisplayType="Primary"/>   
                    </div>
                    <!-- This is needed for the "Working" effect when submitting -->
                    <div class="AdSubmit" id="absubmittingDiv" style="display: none">
                        <img src="/Orion/TrafficAnalysis/images/animated_loading_16x16.gif" alt="" />&nbsp;<%= Resources.NTAWebContent.NTAWEBDATA_TM0_96%>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <input type="hidden" id="demoHidden"
           value="<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>"/>    
</div>
