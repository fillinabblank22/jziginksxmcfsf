﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DirectionalTrafficThresholdField.ascx.cs" Inherits="Orion_TrafficAnalysis_AlertBuilder_DirectionalTrafficThresholdField" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI.AlertBuilder" %>

<div>

	<asp:DropDownList runat="server" 
                      ID="alertDirectionDropDown" 
                      Width="40%" >
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_DirectionalTrafficThresholdIngressTraffic %>"
            Value="1"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_DirectionalTrafficThresholdEgressTraffic %>"
            Value="2"
        />
    </asp:DropDownList> 

    <asp:DropDownList runat="server" 
                      ID="alertOperatorDropDown" 
                      Width="15%" >
        <asp:ListItem
            Text=">"
        />
        <asp:ListItem
            Text="<="
        />
    </asp:DropDownList> 
                
    <asp:TextBox id="alertThresholdValue" runat="server" Width="22%"/>

    <asp:DropDownList runat="server" 
                      ID="bpsUnitDropDown"
                      Width="18%" Style="float:right">
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_19%>"
            Value="1"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_20%>"
            Value="1000"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_21%>"
            Value="1000000"
            Selected="True"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_MM0_22%>"
            Value="1000000000"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_PS0_1%>"
            Value="1000000000000"
        />
        
    </asp:DropDownList>

    <span class="validators">
        <asp:RequiredFieldValidator runat="server" ID="ThresholdValueRequiredValidator"
                                    ControlToValidate="alertThresholdValue" 
                                    ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                    ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_CannotBeEmpty%>"
                                    Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_CannotBeEmpty%>">&nbsp;</a>
        </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="ThresholdValueValidator"
                                        ControlToValidate="alertThresholdValue"
                                        ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                        ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_BpsThresholdInvalid%>"
                                        ValidationExpression="(1000(\.[0]+)?|[0-9]{0,3}(\.[0-9]+)?)"
                                        Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_BpsThresholdInvalid%>">&nbsp;</a>
        </asp:RegularExpressionValidator>
    </span>
</div>
         
