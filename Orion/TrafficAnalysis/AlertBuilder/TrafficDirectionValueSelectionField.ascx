﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrafficDirectionValueSelectionField.ascx.cs" Inherits="Orion_TrafficAnalysis_AlertBuilder_TrafficDirectionValueSelectionField" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI.AlertBuilder" %>
<%@ Import Namespace="Resources" %>

<div>
    <asp:DropDownList runat="server" 
                      ID="alertDirectionDropDown" 
                      Width="35%" >
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_DirectionalTrafficThresholdIngressTraffic %>"
            Value="1"
        />
        <asp:ListItem
            Text="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_DirectionalTrafficThresholdEgressTraffic %>"
            Value="2"
        />
    </asp:DropDownList> 

	<%= NTAWebContent.NTAWEBDATA_AlertBuilder_NotInTopXApps %>
	<asp:TextBox id="alertTopXApplicationsValue" runat="server" Width="20px" Text="5"/>
	<%= NTAWebContent.ViewShortTitle_applications_summary %>

	<span class="validators">
        <asp:RequiredFieldValidator runat="server" ID="TopValueEmptyValidator"
                                    ControlToValidate="alertTopXApplicationsValue" 
                                    ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                    ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_CannotBeEmpty%>"
                                    Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_CannotBeEmpty%>">&nbsp;</a>
        </asp:RequiredFieldValidator>
        <asp:RangeValidator runat="server" ID="TopValueFieldLimitationValidator"
                                        ControlToValidate="alertTopXApplicationsValue"
                                        ValidationGroup="<%# AlertBuilderControlProperties.ValidationGroupName %>"
                                        ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_AlertBuilder_TopXApplicationsInvalid%>"
                                        MinimumValue = "1" MaximumValue="100" Type="Integer"
                                        Display="Dynamic">
            <a href="#" onclick="return false;" class="ntasValIcon" title="<%= Resources.NTAWebContent.NTAWEBDATA_AlertBuilder_TopXApplicationsInvalid%>">&nbsp;</a>
        </asp:RangeValidator>
    </span>
</div>
         
