﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.CoreCharts;
using System.Data;

public partial class Orion_TrafficAnalysis_CoreCharts_TopEndpointsLegend : ChartLegendBase
{
    public override void InitializeChartLegend(DataTable topTable, bool appendPercents, bool appendUtilizations, string warningMessage)
    {
        this.legendContentLc.InitializeChartLegend(this.LegendDescriptor, topTable, appendPercents, appendUtilizations, warningMessage);
    }
}