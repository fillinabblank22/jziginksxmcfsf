﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopEndpointsLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopEndpointsLegend" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<form id="topEndpointsLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContentLc" runat="server" >
            <CaptionItemTemplate>
                <a href='<%# LinkBuilder.GetLinkTarget(Eval(LegendDescriptor.KeyName), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>&From=<%# Eval("HostName") %>'>
                    <%# this.legendContentLc.GetItemIconHtml(Eval(LegendDescriptor.KeyName))%>
                    <netflow:DnsInfo runat="server" HostName=<%# Eval("HostName") %> NotInUpdatePanel="true" />
                </a>
            </CaptionItemTemplate>
        </nta:LegendContent>
    </div>
</form>