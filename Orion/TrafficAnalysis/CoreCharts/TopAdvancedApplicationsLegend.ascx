﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopAdvancedApplicationsLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopAdvancedApplicationsLegend" EnableViewState="false" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI" %>

<form id="topApplicationsLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContentLc" runat="server" >
            <CaptionItemTemplate>
                <%# this.legendContentLc.GetItemIconWithLinkHtml(Eval(LegendDescriptor.KeyName))%>                
                <%# this.legendContentLc.GetTemplateCaption()%>
            </CaptionItemTemplate>
        </nta:LegendContent>       
    </div>
</form>