﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopResourceCommonLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopResourceCommonLegend" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>

<form id="topResourceCommonLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContentLc" runat="server" >
            <CaptionItemTemplate>
                <%# this.legendContentLc.GetItemIconWithLinkHtml(Eval(LegendDescriptor.KeyName))%>                
                <%# this.legendContentLc.GetTemplateCaption()%>
            </CaptionItemTemplate>
        </nta:LegendContent>
    </div>
</form>