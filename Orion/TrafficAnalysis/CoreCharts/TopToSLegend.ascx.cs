﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;

public partial class Orion_TrafficAnalysis_CoreCharts_TopToSLegend : ChartLegendBase
{
    private DataTable topTable;

    public override void InitializeChartLegend(DataTable topTable, bool appendPercents, bool appendUtilizations, string warningMessage)
    {
        this.topTable = topTable;
        this.legendContentLc.InitializeChartLegend(this.LegendDescriptor, topTable, appendPercents, appendUtilizations, warningMessage);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        bool isTosSupported = TypesOfServiceDAL.IsTosSupported(this.topTable);

        this.legendContentLc.Visible = isTosSupported;
        this.customNoDataMessageWrapper.Visible = !isTosSupported;
    }
}