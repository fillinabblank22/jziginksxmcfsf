﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Orion.Web.Charting.v2;

public partial class Orion_TrafficAnalysis_CoreCharts_TopResourceLegendInitializer : UserControl, IChartLegendControl
{
    #region IChartLegendControl Members

    public string LegendInitializer
    {
        get 
        {
            return "TopResourceLegendInitializer__" + legendContainer.ClientID;
        }
    }

    #endregion
}