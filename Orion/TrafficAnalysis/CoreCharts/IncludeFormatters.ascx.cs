﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SolarWinds.Orion.Web;

public partial class Orion_TrafficAnalysis_CoreCharts_IncludeFormatters : System.Web.UI.UserControl
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        OrionInclude.CoreFile("TrafficAnalysis/CoreCharts/DataFormatters.Lang.js");
        OrionInclude.CoreFile("TrafficAnalysis/CoreCharts/DataFormatters.js");
    }
}