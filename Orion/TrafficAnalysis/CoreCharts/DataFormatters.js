﻿(function () {
    var lang = Highcharts.getOptions().lang_NTA;

    function formatString(format, arg0) {
        var a = format.indexOf("{");
        var b = format.indexOf("}", a + 1);
        return a >= 0 && b >= 0 ? format.substring(0, a) + arg0 + format.substring(b + 1) : format;
    }

    function formatUnit(value, baseValue, sizes, digits) {
        if (value === 0 || value == undefined)
            return formatString(sizes[0], value);

        var i = parseInt(Math.floor(Math.log(Math.abs(value)) / Math.log(baseValue)));
        i = Math.max(i, 0);
        i = Math.min(i, sizes.length - 1);

        return formatString(sizes[i], (value / Math.pow(baseValue, i)).toFixed(digits));
    }

    SW.Core.Charts.dataFormatters.NTA_bps = function (value, axis) {
        return formatUnit(value, axis.baseConversionValue || 1024, lang.bpsUnitSizes, 1);
    };

    SW.Core.Charts.dataFormatters.NTA_packets = function (value, axis) {
        return formatUnit(value, axis.baseConversionValue || 1000, lang.packetUnitSizes, 1);
    };

    SW.Core.Charts.dataFormatters.NTA_pps = function (value, axis) {
        return formatUnit(value, axis.baseConversionValue || 1000, lang.ppsUnitSizes, 1);
    };

    SW.Core.Charts.dataFormatters.NTA_visitors = function (value, axis) {
        return formatUnit(value, axis.baseConversionValue || 1000, lang.noUnitSizes, 1);
    };

    SW.Core.Charts.dataFormatters.NTA_vps = function (value, axis) {
        return formatUnit(value, axis.baseConversionValue || 1000, lang.vpsUnitSizes, 1);
    };
})();