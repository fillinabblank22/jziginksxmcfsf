﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TopResourceLegendTopTablePart.aspx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopResourceLegendTopTablePart" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Register TagPrefix="netflow" TagName="NodeLink" Src="~/Orion/TrafficAnalysis/Utils/NodeLink.ascx" %>
<%@ Register TagPrefix="netflow" TagName="InterfaceLink" Src="~/Orion/TrafficAnalysis/Utils/InterfaceLink.ascx" %>

<asp:Repeater runat="server" OnPreRender="rptNodes_OnPreRender" ID="rptNodes" DataSource='<%# this.GetNodeReportData() %>'>
    <ItemTemplate>
        <tr valign="middle" automation="nodeRow" class="TopResourceNodeOrInterfaceRow">
            <td>&nbsp;</td>
            <td>
                <img src="/Orion/images/Button.Expand.gif" alt="" runat="server" id="ifExpandButton" style="cursor: pointer;" ondatabinding="InterfaceCollapseLink_DataBind" />
            </td>
            <td colspan="3">
                <netflow:NodeLink runat="server" ID="nodeLink" NodeID='<%# Eval("[NodeID]") %>' TargetURLFormat='<%# TopUnifiedReportHelper.BuildLink(Container.DataItem, true, this.HideLinks, this.TopKey, this.Prefix, this.Filters, this.PeriodName) %>' />
            </td>
            <td>&nbsp;</td>
            <td style="<%#ColumnsVisibility.CustomColumn%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NoUnits, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
                    
            <td style="<%#ColumnsVisibility.IngressBytes%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NodeIngress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
            <td style="<%#ColumnsVisibility.EgressBytes%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NodeEgress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
        
            <td style="<%#ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsIngess, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
            <td style="<%#ColumnsVisibility.EgressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsEgress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
            <td>&nbsp;</td>
        </tr>

        <asp:Repeater runat="server" ID="rptInterfaces" DataSource='<%# this.GetInterfaceReportData(Eval("[NodeID]")) %>'>
            <ItemTemplate>
                <tr automation="interfaceRow" valign="middle" runat="server" id="ifRow" ondatabinding="OnInterfaceDataBound" style="display:none" class="TopResourceNodeOrInterfaceRow">
                    <td colspan="3">&nbsp;</td>
                    <td>
                        &nbsp; &nbsp; <netflow:InterfaceLink runat="server" ID="interfaceLink" InterfaceID='<%# Eval("[InterfaceID]") %>' InterfaceIndex='<%# Eval("[InterfaceIndex]") %>' TargetURLFormat='<%# TopUnifiedReportHelper.BuildLink(Container.DataItem, false, this.HideLinks, this.TopKey, this.Prefix, this.Filters, this.PeriodName) %>' />
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                                        
                    <td style="<%#ColumnsVisibility.CustomColumn%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NoUnits, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>

                    <td style="<%#ColumnsVisibility.IngressBytes%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.InterfaceIngress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
                    <td style="<%#ColumnsVisibility.EgressBytes%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.InterfaceEgress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
                                        
                    <td style="<%#ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsIngess, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
                    <td style="<%#ColumnsVisibility.EgressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkNodeInterface(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsEgress, this.HideLinks, this.TopKey, this.Prefix, this.PeriodName)%></td>
                    <td>&nbsp;</td>
                </tr>                                    
            </ItemTemplate>
        </asp:Repeater>       
    </ItemTemplate>
</asp:Repeater>