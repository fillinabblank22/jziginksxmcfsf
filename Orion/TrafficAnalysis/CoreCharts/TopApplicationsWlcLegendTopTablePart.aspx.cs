﻿using System.Data;
using System.Web.UI;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web;
using SolarWinds.Wireless.Web;

public partial class Orion_TrafficAnalysis_CoreCharts_TopApplicationsWlcLegendTopTablePart : LegendTopTablePartBase
{
    protected override Control ItemsControl
    {
        get { return rptSecondLevel; }
    }

    protected bool IsClientLevel
    {
        get { return Prefix == "AA"; }
    }

    protected string GetItemCaption(IWlcReportRow item)
    {
        return string.Format("{0} <span>{1}</span>", GetItemIconHtml(item), item.Name);
    }

    protected string GetItemIconHtml(IWlcReportRow item)
    {
        if (IsClientLevel)
        {
            return WirelessResourceBase.GetIconForDeviceType(PollerDeviceType.WirelessController);
        }
        return NetflowAdvancedApplication.GetIconHtml(item.AppId);
    }

    protected DataRow[] GetTopTable()
    {
        return TopTable.Select("", "TotalBytes DESC");
    }
}