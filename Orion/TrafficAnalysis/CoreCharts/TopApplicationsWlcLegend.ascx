﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopApplicationsWlcLegend.ascx.cs" Inherits="Web.Orion.TrafficAnalysis.CoreCharts.Orion_TrafficAnalysis_CoreCharts_TopApplicationsWlcLegend" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Import Namespace="Resources" %>

<form id="topResourceCommonLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContentLc" runat="server" CaptionItemColspan="3">
            <CaptionItemTemplate>
                <%# this.legendContentLc.GetItemIconHtml(Eval(LegendDescriptor.KeyName))%>
                <%# this.GetItemCaption() %>
            </CaptionItemTemplate>
            <CustomColumnsHeaders>
                <td style="min-width: 118px"><%= NTAWebContent.NTAWEBCODE_VB1_ClientMAC %></td>
                <td style="min-width: 80px"><%= NTAWebContent.NTAWEBCODE_VB1_AccessPoint %></td>
            </CustomColumnsHeaders>
            <CustomColumns>
                <td><%# GetItem(Container).ClientMacFormatted %></td>
                <td><%# GetItem(Container).AccessPointName %></td>
            </CustomColumns>
            <CustomColumnsFooters>
                <td colspan="2"/>
            </CustomColumnsFooters>
        </nta:LegendContent>
    </div>
</form>