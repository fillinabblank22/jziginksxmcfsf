﻿function LegendItemCollapseTop_Click(button, url) {

    if (button.childInfo) {

        var resource = document.getElementById(button.childInfo[0]);
        var content = document.getElementById(button.childInfo[1]);

        if (button.src.indexOf("/Orion/images/Button.Expand.gif") > 0) {
            // do expands
            var disp = content.style["display"];
            content.style["display"] = "";

            if (!button.contentLoaded) {
                //Show "Loading circle"
                content = ReplaceTopResourceLegendTBodyContent(content, "<tr><td colspan='15'>&nbsp;<img src='/Orion/images/loading_gen_small.gif' alt='...' /><td></tr>");

                //Get HTML from server
                $.ajax({
                    url: resource.childInfo[10] +
                    '?TopKey=' + button.childInfo[2] +
                    '&NetObjectID=' + resource.childInfo[0] +
                    '&DALTypeName=' + resource.childInfo[1] +
                    '&ResourceID=' + resource.childInfo[2] +
                    '&OriginalTimePeriodName=' + resource.childInfo[3] +
                    '&TopCount=' + resource.childInfo[4] +
                    '&Prefix=' + resource.childInfo[5] +
                    '&HideLinks=' + resource.childInfo[6] +
                    '&CustomColumnDataName=' + resource.childInfo[7] +
                    '&Tx=' + resource.childInfo[8] +
                    '&Rx=' + resource.childInfo[9]
                    ,
                    type: 'GET',
                    dataType: 'html',
                    success: function (data) {
                        // when we get some html that doesn't start with expected piece we show refresh warning
                        if (!/^\s*<tr/.test(data)) {
                            content = ReplaceTopResourceLegendTBodyContent(content, "<tr><td colspan='15'><b style='color:gray'>&nbsp;&nbsp;@{R=NTA.Strings;K=NTAWEBJS_TM0_1;E=js}</b></td></tr>");
                            return false;
                        }
                        ReplaceTopResourceLegendTBodyContent(content, data);
                    },
                    error: function () {
                        content = ReplaceTopResourceLegendTBodyContent(content, "<tr><td colspan='15'>@{R=NTA.Strings;K=NTAWEBJS_TM0_2;E=js}<td></tr>");
                    }
                });
                button.contentLoaded = true;
            }
            button.src = "/Orion/images/Button.Collapse.gif";
        }
        else {
            // do collapses
            content.style["display"] = "none";
            button.src = "/Orion/images/Button.Expand.gif";
        }

    }
    else {
        // if there are no childRows, hide the button
        button.style["display"] = "none";
    }

    return false;
}

function ReplaceTopResourceLegendTBodyContent(tb, newRows) {
    return $(tb).html(newRows).show().get(0);
}


function RegisterTopResourceLegendCollapseRow(buttonID, resourceID, rowID, topKey) {
    var button = document.getElementById(buttonID);

    if (!button.childInfo) {
        button.childInfo = new Array();
    }

    button.childInfo.push(resourceID);
    button.childInfo.push(rowID);
    button.childInfo.push(topKey);
}

function RegisterTopResourceLegendCollapseInfo(resourceInfoHfID, netObjectID, DALTypeName, resourceID, originalTimePeriodName, topCount, prefix, hideLinks, customColumnDataName, tx, rx, expandUrl) {

    var resource = document.getElementById(resourceInfoHfID);
    if (!resource.childInfo) {
        resource.childInfo = new Array();
    }

    resource.childInfo.push(netObjectID);
    resource.childInfo.push(DALTypeName);
    resource.childInfo.push(resourceID);
    resource.childInfo.push(originalTimePeriodName);
    resource.childInfo.push(topCount);
    resource.childInfo.push(prefix);
    resource.childInfo.push(hideLinks);
    resource.childInfo.push(customColumnDataName);
    resource.childInfo.push(tx);
    resource.childInfo.push(rx);
    resource.childInfo.push(expandUrl);
}