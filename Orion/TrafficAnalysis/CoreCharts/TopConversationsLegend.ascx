﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopConversationsLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopConversationsLegend" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Register TagPrefix="netflow" TagName="ConversationDescriptor" Src="~/Orion/TrafficAnalysis/Controls/ConversationDescriptor.ascx" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>

<form id="topTransmmitersLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContentLc" runat="server" >
            <CaptionItemTemplate>
                <a href='<%# LinkBuilder.GetLinkTarget(Eval(LegendDescriptor.KeyName), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>&From=<%# Eval("IPAddress1") %>,<%# Eval("IPAddress2") %>'>
                    <%# this.legendContentLc.GetItemIconHtml(Eval(LegendDescriptor.KeyName))%> 
                    <netflow:ConversationDescriptor ID="conversationCD" runat="server" NotInUpdatePanel="true"
                         HostName1='<%# (string)Eval("Hostname1") %>' HostName2='<%# (string)Eval("Hostname2") %>'
                         IPAddress1='<%# (string)Eval("IPAddress1") %>' IPAddress2='<%# (string)Eval("IPAddress2") %>' /> 
                </a>
            </CaptionItemTemplate>
        </nta:LegendContent>
    </div>
</form>