﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopApplicationsLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopApplicationsLegend" EnableViewState="false" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.UI" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<form id="topApplicationsLegendForm" runat="server">
    <div id="legendWrapper">

        <nta:LegendContent ID="legendContentLc" runat="server">
            <CaptionItemTemplate>
                <a id='appName<%# Eval(LegendDescriptor.KeyName) %>' href='<%# LinkBuilder.GetLinkTarget(Eval(LegendDescriptor.KeyName), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>'>
                    <%# this.legendContentLc.GetItemIconHtml(ApplicationHelper.ExtractPortNumber(Eval(LegendDescriptor.KeyName)))%> 
                    <span><%# this.ApplicationHelperInstance.BuildAppName(Eval("AppID"), Eval("ApplicationName"), Eval("ProtocolName"))%></span>
                </a>
                <br />
                <% if (ApplicationHelperInstance.IsUnmonitored)
                   { %>
                    <%# ApplicationHelper.MonitorPort(Eval(LegendDescriptor.KeyName), Eval("ApplicationName"), Eval("ProtocolName"), this.ClientID, ResolveUrl("~/Orion/TrafficAnalysis/Admin/MonitorApplication.aspx"))%>                
                <% } %>
            </CaptionItemTemplate>
        </nta:LegendContent>       
        <input type="hidden" id="demoHidden" value="<%= SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer %>" />
    </div>
</form>