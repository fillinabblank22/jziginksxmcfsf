﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LegendContent.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_LegendContent" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<asp:HiddenField runat="server" ID="resourceInfoHf"/>

<netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>

<asp:Repeater runat="server" OnPreRender="rptTopTable_OnPreRender" ID="rptTopTable" Visible="false" DataSource="<%# TopTable %>" OnItemCreated="rptTopTable_OnItemCreated">
    <HeaderTemplate>
        <table cellpadding="2" cellspacing="0" class="ntaTable" style="margin-top: 10px">
            <thead id="resourceHead" runat="server">
                <tr>
                    <td colspan="3" />
                    <td><%# this.LegendDescriptor.ItemDisplayName%></td>
                    <td />
                    <td />
                    <asp:PlaceHolder runat="server" ID="customColumnsHeaders" />
                    <td style="<%= ColumnsVisibility.CustomColumn%>">  <%= this.LegendDescriptor.CustomColumnCaption%></td>
                    <td style="min-width: 80px; <%= ColumnsVisibility.IngressBytes%>">  <%= NTAWebContent.NTAWEBDATA_TM0_63%></td>
                    <td style="min-width: 80px; <%= ColumnsVisibility.EgressBytes%>">   <%= NTAWebContent.NTAWEBDATA_TM0_64%></td>
                    <td style="<%= ColumnsVisibility.IngressPackets%>"><%= NTAWebContent.NTAWEBDATA_TM0_65%></td>
                    <td style="<%= ColumnsVisibility.EgressPackets%>"> <%= NTAWebContent.NTAWEBDATA_TM0_66%></td>
                    <%= RenderTopTableHeader(rptTopTable.DataSource) %>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
            <tbody id="rootRow" class="<%# GetRowCssClass(Container.ItemIndex) %>" runat="server">
                <tr valign="middle" automation="rootRow">
                    <td style="width:16px;">
                        <img src="/Orion/images/Button.Expand.gif" alt="" runat="server" id="nodeExpandButton" style="cursor: pointer;" ondatabinding="NodeCollapseLink_DataBind" />
                    </td>
                    <td id="legendSymbolContainer" style="width:16px;<%= NoChartStyle %>"></td>
                    <td id="legendCheckboxContainer" style="<%= ChartTypeStyle %><%= NoChartStyle %>"></td>
                    <td class="chartLegendItemCaption" colspan="3" runat="server" id="templateCaptionContainer" ondatabinding="CaptionItemTemplate_OnItemDataBound" />
                    <asp:PlaceHolder runat="server" ID="customColumns" />
                    <td style="<%# ColumnsVisibility.CustomColumn%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.NoUnits, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.IngressBytes%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.SummaryIngress, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.EgressBytes%>">   <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.SummaryEgress, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.PacketsIngess, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.EgressPackets%>"> <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.PacketsEgress, this.LegendDescriptor.HideLinks)%></td>
                    <%# RenderTopTableData(rptTopTable.DataSource, Container.DataItem) %>
                </tr>
            </tbody>
            
            <tbody runat="server" style="display: none;" id="nodeRows" ondatabinding="OnNodeDataBound" class="<%# GetRowCssClass(Container.ItemIndex) %>">
            </tbody>
    
    </ItemTemplate>
    
    <FooterTemplate>
        <tbody ID="otherTrafficRow" class="<%# GetRowCssClass(TopTable.Rows.Count) %>" visible="<%#this.OtherTrafficRowVisible %>" runat="server">
            <tr valign="middle" automation="remainingTrafficRow">
                <td id="expandButtonPlaceholder" style="width:16px;"/>
                <td id="legendSymbolContainer" style="width:16px;"/>
                <td id="legendCheckboxContainer" style="<%= NoChartStyle %>"/>                
                <td class="chartLegendItemCaption" colspan="3">
                    <img class="ntaIcon" src="/Orion/TrafficAnalysis/images/Charts.RemainingTrafficRow.png" alt="" id="remainingTrafficImg"/>
                    <span><%= Resources.NTAWebContent.NTAWEBDATA_LegendContent_OtherTraffic%></span>
                </td>
                <asp:PlaceHolder runat="server" ID="customColumnsFooters" />
                <td style="<%# ColumnsVisibility.CustomColumn%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.NoUnits, true)%></td>
                <td style="<%# ColumnsVisibility.IngressBytes%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.SummaryIngress, true)%></td>
                <td style="<%# ColumnsVisibility.EgressBytes%>">   <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.SummaryEgress, true)%></td>
                <td style="<%# ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.PacketsIngess, true)%></td>
                <td style="<%# ColumnsVisibility.EgressPackets%>"> <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.PacketsEgress, true)%></td>
                <%# RenderTopTableData(rptTopTable.DataSource, this.otherTrafficDataRow)%>

            </tr>
        </tbody>
        
        </table>
    </FooterTemplate>
</asp:Repeater>

<asp:Repeater runat="server" ID="rptTopTableWebByMail" Visible="false" DataSource="<%# TopTable %>">
    <HeaderTemplate>
        <table cellpadding="2" cellspacing="0" class="ntaTable">
            <thead id="resourceHead" runat="server">
                <tr>
                    <td colspan="3"><%# this.LegendDescriptor.ItemDisplayName%></td>
                    <asp:PlaceHolder runat="server" ID="customColumnsHeaders" />
                    <td style="<%= ColumnsVisibility.CustomColumn%>">  <%= this.LegendDescriptor.CustomColumnCaption%></td>
                    <td style="<%= ColumnsVisibility.IngressBytes%>">  <%= NTAWebContent.NTAWEBDATA_TM0_63%></td>
                    <td style="<%= ColumnsVisibility.EgressBytes%>">   <%= NTAWebContent.NTAWEBDATA_TM0_64%></td>
                    <td style="<%= ColumnsVisibility.IngressPackets%>"><%= NTAWebContent.NTAWEBDATA_TM0_65%></td>
                    <td style="<%= ColumnsVisibility.EgressPackets%>"> <%= NTAWebContent.NTAWEBDATA_TM0_66%></td>
                    <%= RenderTopTableHeader(rptTopTableWebByMail.DataSource) %>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
            <tbody id="rootRow" class="<%# GetRowCssClass(Container.ItemIndex) %>" runat="server">
                <tr valign="middle" automation="rootRow">
                    <td class="chartLegendItemCaption" colspan="3" runat="server" id="templateCaptionContainer" ondatabinding="CaptionItemTemplate_OnItemDataBound" />
                    <asp:PlaceHolder runat="server" ID="customColumns" />        
                    <td style="<%# ColumnsVisibility.CustomColumn%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.NoUnits, this.LegendDescriptor.HideLinks)%></td>           
                    <td style="<%# ColumnsVisibility.IngressBytes%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.SummaryIngress, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.EgressBytes%>">   <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.SummaryEgress, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.PacketsIngess, this.LegendDescriptor.HideLinks)%></td>
                    <td style="<%# ColumnsVisibility.EgressPackets%>"> <%# TopUnifiedReportHelper.GenerateLinkTop(Container.DataItem, TopUnifiedReportHelper.LinkTypeTop.PacketsEgress, this.LegendDescriptor.HideLinks)%></td>
                    <%# RenderTopTableData(rptTopTableWebByMail.DataSource, Container.DataItem) %>
                </tr>
            </tbody>
    </ItemTemplate>
    <FooterTemplate>
        <tbody ID="otherTrafficRow" class="<%# GetRowCssClass(TopTable.Rows.Count) %>" visible="<%#this.OtherTrafficRowVisible %>" runat="server">
            <tr valign="middle" automation="remainingTrafficRow">
                <td colspan="3" class="chartLegendItemCaption">
                    <img class="ntaIcon" src="/Orion/TrafficAnalysis/images/Charts.RemainingTrafficRow.png" alt="" id="remainingTrafficImg"/>
                    <span><%= Resources.NTAWebContent.NTAWEBDATA_LegendContent_OtherTraffic%></span>
                </td>
                <asp:PlaceHolder runat="server" ID="customColumnsFooters" />
                <td style="<%# ColumnsVisibility.CustomColumn%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.NoUnits, true)%></td>
                <td style="<%# ColumnsVisibility.IngressBytes%>">  <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.SummaryIngress, true)%></td>
                <td style="<%# ColumnsVisibility.EgressBytes%>">   <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.SummaryEgress, true)%></td>
                <td style="<%# ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.PacketsIngess, true)%></td>
                <td style="<%# ColumnsVisibility.EgressPackets%>"> <%# TopUnifiedReportHelper.GenerateLinkTop(this.otherTrafficDataRow, TopUnifiedReportHelper.LinkTypeTop.PacketsEgress, true)%></td>
                <%# RenderTopTableData(rptTopTableWebByMail.DataSource, this.otherTrafficDataRow)%>
            </tr>
        </tbody>
        
        </table>
    </FooterTemplate>
</asp:Repeater>
