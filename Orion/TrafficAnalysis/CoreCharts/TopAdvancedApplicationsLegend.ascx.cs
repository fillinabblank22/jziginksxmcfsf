﻿using System;
using System.Data;
using SolarWinds.Netflow.Web.CoreCharts;
using Resources;
using SolarWinds.Orion.Web.Helpers;
using UIHelper = SolarWinds.Netflow.Web.UIHelper;

public partial class Orion_TrafficAnalysis_CoreCharts_TopAdvancedApplicationsLegend : ChartLegendBase
{
    public override void InitializeChartLegend(DataTable topTable, bool appendPercents, bool appendUtilizations, string warningMessage)
    {
        this.legendContentLc.InitializeChartLegend(this.LegendDescriptor, topTable, appendPercents, appendUtilizations, warningMessage);
    }

    protected string GetItemIconWithVendorInTitle(object iconPath, object vendorName)
    {
        if (iconPath == null || vendorName == null || String.IsNullOrEmpty(iconPath.ToString()))
        {
            return String.Empty;
        }

        var title = String.Format(NTAWebContent.NTAWEBCODE_TM0_110, WebSecurityHelper.HtmlEncode(vendorName.ToString()));
        var iconKeyEncoded = WebSecurityHelper.HtmlEncode(iconPath.ToString());
        return String.Format("<span title='{0}'>{1}</span>", title, UIHelper.GetIconImgHtml(iconKeyEncoded));
    }
}