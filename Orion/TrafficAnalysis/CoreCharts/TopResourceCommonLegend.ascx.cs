﻿using System;
using System.Data;
using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.CoreCharts;

public partial class Orion_TrafficAnalysis_CoreCharts_TopResourceCommonLegend : ChartLegendBase
{
    public override void InitializeChartLegend(DataTable topTable, bool appendPercents, bool appendUtilizations, string warningMessage)
    {
        this.legendContentLc.InitializeChartLegend(this.LegendDescriptor, topTable, appendPercents, appendUtilizations, warningMessage);
    }
}