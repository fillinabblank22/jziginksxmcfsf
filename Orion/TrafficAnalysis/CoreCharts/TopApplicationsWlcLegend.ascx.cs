﻿using System.Data;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Netflow.Web.Reporting;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web.Orion.TrafficAnalysis.CoreCharts
{
    public partial class Orion_TrafficAnalysis_CoreCharts_TopApplicationsWlcLegend : ChartLegendBase
    {
        public override void InitializeChartLegend(DataTable topTable, bool appendPercents, bool appendUtilizations, string warningMessage)
        {
            var isClientLevel = LegendDescriptor.Prefix != "AA";

            if (!isClientLevel)
            {
                // hide ClientMAC and access point columns and extend application caption column to take their space
                legendContentLc.CustomColumns = null;
                legendContentLc.CaptionItemColspan = 5;
            }

            this.legendContentLc.InitializeChartLegend(this.LegendDescriptor, topTable, appendPercents, appendUtilizations, warningMessage);
        }

        public string GetItemCaption()
        {
            return "<span>" + LegendDescriptor.GetItemCaption(this.Page.GetDataItem()) + "</span>";
        }

        public static IWlcReportRow GetItem(RepeaterItem container)
        {
            var dataItem = container.DataItem;
            var row = dataItem as DataRow ?? ((DataRowView)dataItem).Row;
            return row as IWlcReportRow;
        }
    }
}