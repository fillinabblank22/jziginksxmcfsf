﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TopApplicationsWlcLegendTopTablePart.aspx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopApplicationsWlcLegendTopTablePart" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<asp:Repeater runat="server" ID="rptSecondLevel" DataSource='<%# GetTopTable() %>' ItemType="SolarWinds.Netflow.Web.Reporting.IWlcReportRow">
    <ItemTemplate>
        <tr valign="middle" automation="secondLevelRow" class="TopWlcResourceSecondLevelRow">
            <td colspan="3"/>
            <td colspan="<%# IsClientLevel ? 3 : 5 %>" class="chartLegendItemCaption"><%# GetItemCaption(Item) %></td>
            <asp:PlaceHolder ID="ClientColumns" runat="server" Visible="<%# IsClientLevel %>">
                <td><%# Item.ClientMacFormatted %></td>
                <td><%# Item.AccessPointName %></td>
            </asp:PlaceHolder>
            <td style="<%# ColumnsVisibility.IngressBytes%>"><%# TopUnifiedReportHelper.GetFormattedFieldValue(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NodeIngress) %></td>
            <td style="<%# ColumnsVisibility.EgressBytes%>"><%# TopUnifiedReportHelper.GetFormattedFieldValue(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.NodeEgress) %></td>
            <td style="<%# ColumnsVisibility.IngressPackets%>"><%# TopUnifiedReportHelper.GetFormattedFieldValue(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsIngess) %></td>
            <td style="<%# ColumnsVisibility.EgressPackets%>"><%# TopUnifiedReportHelper.GetFormattedFieldValue(Container.DataItem, TopUnifiedReportHelper.LinkTypeNodeInterface.PacketsEgress) %></td>
            <td />
        </tr>
    </ItemTemplate>
</asp:Repeater>