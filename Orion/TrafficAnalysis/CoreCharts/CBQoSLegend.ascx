﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_CBQoSLegend" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<%@ Register TagPrefix="netflow" TagName="CBQoSLegendPolicies" Src="~/Orion/TrafficAnalysis/Utils/CBQoSLegendPolicies.ascx" %>

<form id="CBQoSLegendForm" runat="server">
    <div id="legendWrapper">
        
        <div id="cbqosNoDataMessageWrapper" runat="server">
            <span id="cbqosNoDataMessage"><%= LegendDescriptor.CustomMessage %></span>
        </div>

        <table cellspacing="0" cellpadding="3" class="ntaTable" style="border-collapse: collapse;">
            <thead>
                <tr>
                    <td colspan="4" rowspan="2" align="center" valign="bottom"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_19%></td>
                    <td colspan="4" align="center"><%= IsClassUtilization ? Resources.NTAWebContent.NTAWEBDATA_CBQoSLegend_ClassUtilization : Resources.NTAWebContent.NTAWEBDATA_AK0_20%></td>    
                </tr>
                <tr>
                    <td align="center"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_21%></td>
                    <td>&nbsp;</td>
                    <td align="center"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_22%></td>
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <netflow:CBQoSLegendPolicies runat="server" ID="cbqosLegendPolicies" />
        </table>
    </div>
</form>
