﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web.CoreCharts;

public partial class Orion_TrafficAnalysis_CoreCharts_TopResourceLegendTopTablePart : LegendTopTablePartBase
{
    private readonly Guid guid = Guid.NewGuid();

    private class RenderedRowInfo
    {
        public HtmlImage CurrentButton = null;
        public HtmlImage PrevButton = null;
        public StringBuilder Script = new StringBuilder();
    }
    private RenderedRowInfo renderedRowInfo = new RenderedRowInfo();

    protected override Control ItemsControl
    {
        get { return rptNodes; }
    }

    protected object GetNodeReportData()
    {
        return TopTable.Select("NodeID IS NOT NULL AND InterfaceID IS NULL", "TotalBytes DESC");
    }

    protected object GetInterfaceReportData(object nodeID)
    {
        return TopTable.Select(string.Format("InterfaceID IS NOT NULL AND NodeID={0}", nodeID), "TotalBytes DESC");
    }

    protected void InterfaceCollapseLink_DataBind(object sender, EventArgs e)
    {
        HtmlImage myButton = sender as HtmlImage;
        this.renderedRowInfo.CurrentButton = myButton;
        if (this.renderedRowInfo.PrevButton == null)
            this.renderedRowInfo.PrevButton = myButton;
    }

    protected void OnInterfaceDataBound(object sender, EventArgs e)
    {
        Control row = sender as Control;

        if (renderedRowInfo.CurrentButton != renderedRowInfo.PrevButton)
        {
            AddScriptToExpandButton(renderedRowInfo);
            renderedRowInfo.Script.Length = 0;
            renderedRowInfo.PrevButton = renderedRowInfo.CurrentButton;
        }

        row.ID = "ifRow_" + this.guid;
        renderedRowInfo.CurrentButton.ID = "btn_" + this.guid;

        //Show hide current interface line
        renderedRowInfo.Script.AppendLine("$('#" + row.ClientID + "')" + ".css('display', displayValue);");
    }

    private void AddScriptToExpandButton(RenderedRowInfo rowInfo)
    {
        if (rowInfo.Script.Length == 0)
            return;

        string fullScript = @"
var displayValue; 
var button = this;
    
if(button.src.indexOf('/Orion/images/Button.Expand.gif') > 0)
{       
    displayValue = '';        
    button.src = '/Orion/images/Button.Collapse.gif';
}
else
{
    displayValue = 'none';        
    button.src = '/Orion/images/Button.Expand.gif';
}
" + rowInfo.Script.ToString();

        rowInfo.PrevButton.Attributes.Add("onclick", fullScript + " return false;");
    }

    protected void rptNodes_OnPreRender(object sender, EventArgs e)
    {
        AddScriptToExpandButton(renderedRowInfo);
    }
}