﻿(function () {
    Highcharts.setOptions({
        lang_NTA: {
            noUnitSizes: ['@{R=NTA.Strings;K=CFGDATA_VT0_1;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_2;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_3;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_4;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_5;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_6;E=js}'],
            bpsUnitSizes: ['@{R=NTA.Strings;K=CFGDATA_TM0_133;E=js}', '@{R=NTA.Strings;K=CFGDATA_TM0_134;E=js}', '@{R=NTA.Strings;K=CFGDATA_TM0_135;E=js}', '@{R=NTA.Strings;K=CFGDATA_TM0_136;E=js}', '@{R=NTA.Strings;K=CFGDATA_TM0_137;E=js}', '@{R=NTA.Strings;K=CFGDATA_TM0_138;E=js}'],
            packetUnitSizes: ['@{R=NTA.Strings;K=CFGDATA_VT0_13;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_14;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_15;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_16;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_17;E=js}', '@{R=NTA.Strings;K=CFGDATA_VT0_18;E=js}'],
            ppsUnitSizes: ['{0} @{R=NTA.Strings;K=LIBCODE_VB0_9;E=js}'],
            vpsUnitSizes: ['@{R=NTA.Strings;K=ChartType_VisitorsUnits;E=js}']
        }
    });
})();