﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopToSLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopToSLegend" %>
<%@ Import Namespace="Resources" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>

<form id="topToSLegendForm" runat="server">
    <div id="legendWrapper">
        <div id="customNoDataMessageWrapper" runat="server" Visible="False">
            <div id="customNoDataMessage" style="margin-left:10px; font-size:11px; font-weight:bold;">
                <%= NTAWebContent.NTAWEBDATA_MK0_2 %>
            </div>
        </div>
        <nta:LegendContent ID="legendContentLc" runat="server">
            <CaptionItemTemplate>
                <%# this.legendContentLc.GetItemIconWithLinkHtml(Eval(LegendDescriptor.KeyName))%>                
                <%# this.legendContentLc.GetTemplateCaption()%>
            </CaptionItemTemplate>
        </nta:LegendContent>
    </div>
</form>
