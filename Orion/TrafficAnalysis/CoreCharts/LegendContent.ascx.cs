﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.Utility;
using System.Web.UI.WebControls;
using System.ComponentModel;

public partial class Orion_TrafficAnalysis_CoreCharts_LegendContent : ChartContentLegendBase
{
    private static SolarWinds.Logging.Log log = new SolarWinds.Logging.Log();

    protected System.Web.UI.WebControls.Repeater TopTableControl { get; set; }

    protected DataRow otherTrafficDataRow;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        registerCollapseRowsTop = new List<RegisterCollapseRowsTop>();
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.TopTableControl = !LegendDescriptor.WebByMail ? this.rptTopTable : this.rptTopTableWebByMail;

        this.resourceInfoHf.ID = string.Format("resourceInfoHf_{0}", LegendDescriptor.Parameters.ResourceID);

        ColumnsVisibility = new TopUnifiedReportColumnsVisibility(TopTable, LegendDescriptor.CustomColumnDataName);

        DataTable otherTrafficDataTable = (this.TopTable as IReportTable).OtherTrafficTable;
        otherTrafficDataRow = otherTrafficDataTable != null ? otherTrafficDataTable.Rows[0] : null;

        this.rptTopTable.Visible = false;
        this.rptTopTableWebByMail.Visible = false;

        this.TopTableControl.Visible = true;
        this.TopTableControl.DataBind();

        warningMessage.ResourceId = LegendDescriptor.Parameters.ResourceID;
        warningMessage.WarningMessage = WarningMessage;
        warningMessage.UpdateTypes = LegendDescriptor.UpdateTypes;
        warningMessage.FilterID = LegendDescriptor.Parameters.Filters.FilterIdWithAbsoluteTimePeriodFilter();
    }

    /// <summary>
    /// Additional columns to be put in header between item caption and traffic columns
    /// </summary>
    [TemplateContainer(typeof(RepeaterItem))]
    [TemplateInstance(TemplateInstance.Single)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate CustomColumnsHeaders { get; set; }

    /// <summary>
    /// Additional columns to be put in data rows between item caption and traffic columns
    /// </summary>
    [TemplateContainer(typeof(RepeaterItem))]
    [TemplateInstance(TemplateInstance.Multiple)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate CustomColumns { get; set; }

    /// <summary>
    /// Additional columns to be put in footer between item caption and traffic columns
    /// </summary>
    [TemplateContainer(typeof(RepeaterItem))]
    [TemplateInstance(TemplateInstance.Single)]
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate CustomColumnsFooters { get; set; }

    private int captionItemColspan = 3;
    /// <summary>
    /// Use this to change colspan of item caption (doesn't affect header and footer)
    /// </summary>
    [DefaultValue(3)]
    public int CaptionItemColspan
    {
        get { return captionItemColspan; }
        set { captionItemColspan = value; }
    }

    /// <summary>
    /// Use this to override just item caption (eg. icon and text) and keep other default columns
    /// </summary>
    [PersistenceMode(PersistenceMode.InnerProperty)]
    public ITemplate CaptionItemTemplate { get; set; }

    protected void CaptionItemTemplate_OnItemDataBound(object sender, EventArgs e)
    {
        var cell = (HtmlTableCell)sender;

        if (null != this.CaptionItemTemplate)
        {
            CaptionItemTemplate.InstantiateIn(cell);
        }

        cell.ColSpan = CaptionItemColspan;
    }

    protected string GetRowCssClass(int dataIndex)
    {
        return dataIndex%2 == 0 ? "TopResourceEvenTableRow" : "TopResourceOddTableRow";
    }

    protected void OnNodeDataBound(object sender, EventArgs e)
    {
        if (this.TopTableControl != this.rptTopTable)
            return;

        Control c = sender as Control;

        c.ID = string.Format("nodeRows_{0}", LegendDescriptor.Parameters.ResourceID);
        Control button = c.NamingContainer.FindControl(string.Format("nodeExpandButton_{0}", LegendDescriptor.Parameters.ResourceID));

        RegisterCollapseRowsTop collapseRows = new RegisterCollapseRowsTop();

        collapseRows.ButtonClientID = button.ClientID;
        collapseRows.RowClientID = c.ClientID;
        collapseRows.TopKey = Convert.ToString(Eval(LegendDescriptor.KeyName));

        registerCollapseRowsTop.Add(collapseRows);
    }

    protected void NodeCollapseLink_DataBind(object sender, EventArgs e)
    {
        if (this.TopTableControl != this.rptTopTable)
            return;

        HtmlImage myButton = sender as HtmlImage;

        myButton.ID = string.Format("nodeExpandButton_{0}", LegendDescriptor.Parameters.ResourceID);

        if (!string.IsNullOrEmpty(LegendDescriptor.ChartLegendExpandedLevelFilePath))
        {
            myButton.Attributes.Add("onclick", "LegendItemCollapseTop_Click(this); return false;");
        }
    }

    protected void rptTopTable_OnPreRender(object sender, EventArgs e)
    {
        if (this.TopTableControl != this.rptTopTable)
            return;

        StringBuilder script = new StringBuilder();

        string hfResourceInfoClientID = resourceInfoHf.ClientID;
        if (registerCollapseRowsTop.Count > 0)
        {
            //Register values for each row
            foreach (RegisterCollapseRowsTop row in registerCollapseRowsTop)
            {
                script.AppendFormat("RegisterTopResourceLegendCollapseRow('{0}', '{1}', '{2}', '{3}');",
                    row.ButtonClientID, hfResourceInfoClientID, row.RowClientID, System.Web.HttpUtility.UrlEncode(row.TopKey));
            }

            resourceInfoHf.Value += script.ToString();

            //Register common  values for all lines (resource info)
            script.Length = 0;

            script.AppendFormat("RegisterTopResourceLegendCollapseInfo('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}');",
                hfResourceInfoClientID,
                LegendDescriptor.Parameters.Filters.FilterCollectionToNetObjectId(),
                LegendDescriptor.DALTypeName,
                LegendDescriptor.Parameters.ResourceID,
                LegendDescriptor.Parameters.Filters.PeriodFilterUi().PeriodName,
                LegendDescriptor.Parameters.ItemCount,
                LegendDescriptor.Prefix,
                LegendDescriptor.HideLinks,
                LegendDescriptor.CustomColumnDataName,
                LegendDescriptor.Tx,
                LegendDescriptor.Rx,
                ResolveUrl(LegendDescriptor.ChartLegendExpandedLevelFilePath));

            resourceInfoHf.Value += script.ToString();
        }
    }

    protected bool OtherTrafficRowVisible
    {
        get
        {
            return this.otherTrafficDataRow != null;
        }
    }

    /// <summary>
    /// For pie chart we don't want to set minimum width for checkbox, since there's no checkboxes and it only creates empty space
    /// </summary>
    protected string ChartTypeStyle
    {
        get
        {
            return this.LegendDescriptor.Parameters.ChartType == ChartType.Area ? "width:16px;" : string.Empty;
        }
    }

    protected string NoChartStyle
    {
        get
        {
            return this.LegendDescriptor.Parameters.ChartStyle == ChartStyle.NoChart ? "visibility: hidden;" : "";
        }
    }

    protected void rptTopTable_OnItemCreated(object sender, RepeaterItemEventArgs e)
    {
        switch (e.Item.ItemType)
        {
            case ListItemType.Header:
                if (CustomColumnsHeaders != null)
                {
                    var container = e.Item.FindControl("customColumnsHeaders");
                    CustomColumnsHeaders.InstantiateIn(container);
                }
                break;

            case ListItemType.Footer:
                if (CustomColumnsFooters != null)
                {
                    var container = e.Item.FindControl("customColumnsFooters");
                    CustomColumnsFooters.InstantiateIn(container);
                }
                break;

            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                if (CustomColumns != null)
                {
                    var container = e.Item.FindControl("customColumns");
                    CustomColumns.InstantiateIn(container);
                }
                break;
        }
    }
}