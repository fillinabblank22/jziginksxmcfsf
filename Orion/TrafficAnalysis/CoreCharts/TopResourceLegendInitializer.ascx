﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopResourceLegendInitializer.ascx.cs" Inherits="Orion_TrafficAnalysis_CoreCharts_TopResourceLegendInitializer" %>


<script type="text/javascript">

    var chartSeries__<%= legendContainer.ClientID %>;
    var dataUrlParameters__<%= legendContainer.ClientID %> = {};

    SW.Core.Charts.Legend.TopResourceLegendInitializer__<%= legendContainer.ClientID %> = function (chart, dataUrlParameters) {

        chartSeries__<%= legendContainer.ClientID %> = chart.series;
        dataUrlParameters__<%= legendContainer.ClientID %> = JSON.stringify(dataUrlParameters);

        SW.Core.Charts.callWebService(
            "/Orion/TrafficAnalysis/WebServices/ResourceChartService.asmx/TopResourceLegend",
            dataUrlParameters,
            successCallback__<%= legendContainer.ClientID %>,
            errorCallback__<%= legendContainer.ClientID %>);
    };

    var successCallback__<%= legendContainer.ClientID %> = function (result) {

        var container = $("#<%= legendContainer.ClientID %>");
        var legend = $(result).find("div#legendWrapper"); // remove form element and other "asp postback clutter" from result

        if (legend.find("#cbqosNoDataMessage").length || legend.find("#customNoDataMessage").length) { // no data message id defined - cbqos or other resource wants to dislpay legend only
            container.closest("div[id$=_wrapper_resContent]").html(legend.html()); // remove empty (zero filled) chart and replace it with legend
        } else {
            container.html(legend.html());
            eval(container.find("input[id*=resourceInfoHf]").val()); // register expand/collapse button scripts

            // add update warning message functionality
            // InaccurateDataWarning.aspx contains script tag. Adding it manually from response.
            // append method evals javascript at appropriate time
            container.append($(result).filter("script")); 

            // we check if received html blob contains identifiers with color index. If it does we will respect them later in for loops, 
            // otherwise default behavior (based on order) is used
            var hasColorIndexes = container.find("[id^=legendSymbolContainer_]:eq(0)").size() > 0;

            if (chartSeries__<%= legendContainer.ClientID %>[0].type == 'pie') {
                // for pie chart iterate over points in first serie
                var pieChartData = chartSeries__<%= legendContainer.ClientID %>[0].data;

                for (var i = 0; i < pieChartData.length; ++i) {

                    if (hasColorIndexes) {
                        var symbContainer = container.find("#legendSymbolContainer_" + pieChartData[i].key);

                        if (symbContainer.size() > 0) {
                            SW.Core.Charts.Legend.addLegendSymbol(pieChartData[i], symbContainer);
                        }
                    } else {
                        SW.Core.Charts.Legend.addLegendSymbol(pieChartData[i], container.find("#legendSymbolContainer:eq(" + i + ")"));
                    }
                }
            } else {
                for (var index = 0; index < chartSeries__<%= legendContainer.ClientID %>.length - 1; index++) {
                    if (hasColorIndexes) {
                        var classId = (chartSeries__<%= legendContainer.ClientID %>[index].options.uniqueId+'').replace("Default", "").replace("Navigator", "");
                        var symbolContainer = container.find("#legendSymbolContainer_" + classId);
                        if (symbolContainer.size() > 0) {
                            SW.Core.Charts.Legend.addCheckbox(chartSeries__<%= legendContainer.ClientID %>[index], container.find("#legendCheckboxContainer_" + classId));
                            SW.Core.Charts.Legend.addLegendSymbol(chartSeries__<%= legendContainer.ClientID %>[index], symbolContainer);
                        }
                    } else {
                        SW.Core.Charts.Legend.addCheckbox(chartSeries__<%= legendContainer.ClientID %>[index], container.find("#legendCheckboxContainer:eq(" + index + ")"));
                        SW.Core.Charts.Legend.addLegendSymbol(chartSeries__<%= legendContainer.ClientID %>[index], container.find("#legendSymbolContainer:eq(" + index + ")"));
                    }
                }
            }
        }
        // remove loading signalization class
        $('#<%= legendContainer.ClientID %>.highcharts-loading').removeClass('highcharts-loading');
    }

    var errorCallback__<%= legendContainer.ClientID %> = function (result) {
        // remove loading signalization class
        $('#<%= legendContainer.ClientID %>.highcharts-loading').removeClass('highcharts-loading');

        var container = $("#<%= legendContainer.ClientID %>");

        container.html("<%= Resources.NTAWebContent.NTAWEBDATA_MM0_51 %>");
    }
</script>

<orion:Include ID="liNetflowCss" runat="server" Module="TrafficAnalysis" File="NetFlow.css" />
<orion:Include ID="liTrafficAnalysis" runat="server" Module="TrafficAnalysis" File="TrafficAnalysis.css" />

<script type="text/javascript" src="/Orion/TrafficAnalysis/CoreCharts/TopResourceLegend.js"></script>
<asp:Panel ID="legendContainer" runat="server" CssClass=".ResourceWrapper highcharts-loading"></asp:Panel>
