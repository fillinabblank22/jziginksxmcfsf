﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolarWinds.Netflow.Web.CoreCharts;
using System.Data;
using SolarWinds.Netflow.Web.DAL.CBQoS;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_TrafficAnalysis_CoreCharts_CBQoSLegend : ChartContentLegendBase, IChartLegend
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        IReportTable topTable = TopTable as IReportTable;
        if (topTable != null)
            cbqosNoDataMessageWrapper.Visible = topTable.TotalBytes == 0;

        cbqosLegendPolicies.Policies = CBQoSTopDAL.GetCbqosConfigurationTop(TopTable as ReportingDS.TopCBQoSDataTable);
        cbqosLegendPolicies.NoChartStyle = NoChartStyle;
        cbqosLegendPolicies.LegendDescriptor = LegendDescriptor;
        cbqosLegendPolicies.IsRoot = true;
        cbqosLegendPolicies.DataBind();
    }

    public bool IsClassUtilization
    {
        get { return LegendDescriptor.Parameters.ChartDataUnit == ChartDU.ChartDataUnit.PercentOfClassUtilization; }
    }

    protected string NoChartStyle
    {
        get
        {
            return LegendDescriptor.Parameters.ChartStyle == ChartStyle.NoChart ? "display: none;" : "display: inline-block;";
        }
    }
}