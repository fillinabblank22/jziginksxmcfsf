﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversationTotalBytesLegend.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_ConversationTotalBytesLegend" %>
<%@ Register TagPrefix="nta" TagName="LegendContent" Src="~/Orion/TrafficAnalysis/CoreCharts/LegendContent.ascx" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<form id="topResourceCommonLegendForm" runat="server">
    <div id="legendWrapper">
        <nta:LegendContent ID="legendContent" runat="server">
            <CaptionItemTemplate>
                <a href='<%# LinkBuilder.GetLinkTarget(Eval(LegendDescriptor.KeyName), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>&From=<%# Eval("SourceIP") %>'>
                    <%# this.legendContent.GetItemIconHtml(null)%>
                    <%= Resources.NTAWebContent.NTAWEBCODE_TM0_109 %>
                    <netflow:DnsInfo id="dnsInfo" runat="server" HostName=<%# Eval("SourceHostname") %> AttachedIP=<%# Eval("SourceIP") %> NotInUpdatePanel="true" />
                </a>
            </CaptionItemTemplate>
        </nta:LegendContent>
    </div>
</form>