<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowEndpointDetails.aspx.cs" Inherits="Orion_Netflow_EndpointDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="netflow" TagName="DnsInfo" Src="~/Orion/TrafficAnalysis/Controls/DnsInfo.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
        <netflow:DnsInfo ID="DnsInfo" runat="server"/>
    </a>

    <script type="text/javascript">
        function SetTitle(host, data) {
            if (document.title.indexOf(host) > 0) {
                document.title = "NetFlow Endpoint - " + SolarWinds.Orion.Web.Helpers.WebSecurityHelper.JavaScriptStringEncode(data);
            }
            $("input[id$='hfTitle']").val(document.title);
        }

        $(document).ready(function() {
            $("input[id$='hfTitle']").val(document.title);
        });  
    
    </script>

    <asp:HiddenField runat="server" ID="hfTitle" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:EndpointResourceHost runat="server" ID="endpointResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:EndpointResourceHost>
</asp:Content>

