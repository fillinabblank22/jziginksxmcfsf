using System;
using System.Web.Services;
using System.Web.UI;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_Netflow_IPAddressGroupDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow IP Address Group"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.groupResHost.NetflowIPAddressGroup = this.NetObject as NetflowIPAddressGroup;
        
        this.Page.Form.Attributes["autocomplete"] = "off";
        
        base.OnInit(e);
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
