﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Common;

public partial class Orion_TrafficAnalysis_UnknownTrafficEvents : EventListViewBase
{
    private static readonly IEnumerable<NetFlowEventType> EventsTypeToShow = new[]
    {
        NetFlowEventType.UnknownNetFlowSource,
        NetFlowEventType.UnmonitoredFlowSource,
        NetFlowEventType.UnmonitoredFlowSourceAdded,
        NetFlowEventType.UnmonitoredNetFlowInterfaceAdded,
        NetFlowEventType.UnknownNetFlowInterface,
        NetFlowEventType.UnmonitoredNetFlowInterface,
        NetFlowEventType.InvalidTemplate,
        NetFlowEventType.NoTemplateReceived,
        NetFlowEventType.UnsupportedFlow,
        NetFlowEventType.IcmpNetFlowNode,
        NetFlowEventType.NotPrimaryNpmNodeIpAddress,
        NetFlowEventType.UnknownTrafficNotificationsReset
    };

    protected override IEventList EventList
    {
        get { return eventList; }
    }

    protected override DataTable GetDataSource()
    {
        return DalProvider.GetDAL<IEventsDAL>().GetEvents(EventsTypeToShow, false, NetFlowEventType.UnknownTrafficNotificationsReset);
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (OrionConfiguration.IsDemoServer)
            this.btnResetNotifications.OnClientClick = UIHelper.GetJSAlertForDisabledFunctionalityOnDemo(this.btnResetNotifications.Text);
    }

    protected void btnResetNotificationsClick(object sender, EventArgs e)
    {
        string accountId = HttpContext.Current.Profile.UserName;
        BusinessLayerHelper.ResetUnknownTrafficNotifications(accountId);
        this.LoadData();
    }

    protected void btnRefreshPageClick(object sender, EventArgs e)
    {
        this.LoadData();
    }
}