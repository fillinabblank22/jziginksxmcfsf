using System;
using System.Web.Services;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting;
using System.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_Netflow_Summary : NetflowSummaryView
{
    public override string ViewType
	{
		get { return "Netflow Summary"; }
	}

    protected override void OnLoad(EventArgs e)
    {        
        ScriptManager scriptManager = ScriptManager.GetCurrent(this);

        if (GlobalSettingsDALCached.WebUseAsync || !(scriptManager.IsInAsyncPostBack || GlobalSettingsDALCached.WebUseAsync))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }

        base.OnLoad(e);
    }
}
