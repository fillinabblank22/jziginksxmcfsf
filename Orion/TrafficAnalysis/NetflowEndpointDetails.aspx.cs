using System;
using System.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting.DNS;

public partial class Orion_Netflow_EndpointDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow Endpoint"; }
	}

    private string _ipAddress;
    private string _hostName;

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;

        this.endpointResHost.NetflowEndpoint = (NetflowEndpoint)this.NetObject;
        
        this.Page.Form.Attributes["autocomplete"] = "off";

        base.OnInit(e);

        if (!Page.IsPostBack)
        {
            DnsInfo.HostName = this.endpointResHost.NetflowEndpoint.Hostname;
            DnsInfo.AttachedIP = this.endpointResHost.NetflowEndpoint.IPAddress;
        }
    }

    
    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }

        var hostname = OnDemandDns.Instance.GetCached(endpointResHost.NetflowEndpoint.Hostname, endpointResHost.NetflowEndpoint.IPAddress).ToString();
        Page.Title = $"{ViewInfo.ViewTitle} - {hostname}";
        
        if (hfTitle.Value != null && hfTitle.Value != string.Empty)
        {
            this.Page.Title = hfTitle.Value;
        }
    }
}
