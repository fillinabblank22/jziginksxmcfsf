<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowNodeDetails.aspx.cs" Inherits="Orion_Netflow_NodeDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <npm:NodeLink runat="server" ID="nodeLink" TargetURLFormat="/Orion/TrafficAnalysis/NetflowNodeDetails.aspx?NetObject=N{0}" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:NetflowNodeResourceHost runat="server" ID="nnNodeResHost" style="display:inline" >
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:NetflowNodeResourceHost>
</asp:Content>