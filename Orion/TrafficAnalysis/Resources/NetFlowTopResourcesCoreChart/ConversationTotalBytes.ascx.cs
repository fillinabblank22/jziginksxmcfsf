using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_ConversationTotalBytes : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHConversationTotalTransferredBytesV2"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(INetflowConversationProvider) };
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_37; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
           reportingDAL: new ConversationTotalBytesDAL(),
           keyName: "SourceIP",
           prefix: "E",
           itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_49,
           summaryViewKey: "Netflow Conversations Summary",
           additionalUpdateTypes: null,
           getItemCaption: dataItem => DataBinder.Eval(dataItem, "SourceHostName").ToString(),
           getItemIconHtml: key => string.Empty)
           {
               DefaultAreaType = AreaChartType.BarChart,
               ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/ConversationTotalBytesLegend.ascx"
           };
    }
}
