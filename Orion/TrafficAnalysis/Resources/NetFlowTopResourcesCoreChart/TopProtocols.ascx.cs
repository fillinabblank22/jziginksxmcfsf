using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopProtocols : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopProtocolsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_55; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new ProtocolsDAL(),
            keyName: "Protocol",
            prefix: "P",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_56,
            summaryViewKey: "Netflow Protocols Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "ProtocolName").ToString(),
            getItemIconHtml: key => NetflowProtocol.GetIconHtml(Convert.ToInt32(key)));
    }
}