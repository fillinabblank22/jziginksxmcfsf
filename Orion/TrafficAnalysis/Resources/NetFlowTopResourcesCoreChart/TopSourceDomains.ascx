﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopSourceDomains.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopSourceDomains" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="netflow" TagName="TopDomainCoreChartContainer" Src="~/Orion/TrafficAnalysis/Utils/TopDomainCoreChartContainer.ascx" %>

<orion:ResourceWrapper runat="server" ID="wrapper">
    <Content>
        <netflow:TopDomainCoreChartContainer runat="server" ID="topCoreChartContainer" OwnerResource="<%# this %>" />
     </Content>
</orion:ResourceWrapper>
