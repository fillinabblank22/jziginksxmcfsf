using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.Helpers;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopAS : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopASV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_38; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        var descriptor =  new TopResourceDescriptor(
            reportingDAL: new AutonomousSystemDAL(),
            keyName: "AsID",
            prefix: "S",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_36,
            summaryViewKey: "NetFlow BGP Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => WebSecurityHelper.SanitizeHtmlV2(DataBinder.Eval(dataItem, "AsName").ToString()),
            getItemIconHtml: key => NetflowAS.GetIconHtml(Convert.ToInt32(key)));

        descriptor.UpdateDataPointName = s => WebSecurityHelper.SanitizeHtmlV2(s);

        return descriptor;
    }
}
