using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.UI.TopResources;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web;
using System.Data;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Wireless.Web.NetObjects;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Wireless.Web;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopApplicationsWlc : TopResourceCoreChartBase, ICustomPropertyProvidingResource
{
    private static readonly Log log = new Log();

    private static readonly string FirstLevelPropertyName = ConfigurationPropertyHelper.GetPropertyKey<TopApplicationsWlcFirstLevel>();

    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { yield return typeof (SolarWinds.Orion.NPM.Web.INodeProvider); }
    }

    protected override string DefaultTitle
    {
        get { return NTAWebContent.TopXXApplicationsWLCEndpointCentric; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHTopWLCApplications";
        }
    }

    protected override TopResourceParameters GetResourceParameters(TopResourceDescriptor descriptor, TopResourceProperties properties, bool fillNetObjectAndFilters)
    {
        return new Parameters(this, descriptor, properties, fillNetObjectAndFilters);
    }


    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: null,
            keyName: "ID",
            prefix: "AA",
            itemDisplayName: string.Format("{0}<br />/ {1}", NTAWebContent.NTAWEBCODE_VB1_21, NTAWebContent.NTAWEBCODE_VB1_Client),
            summaryViewKey: "Netflow Applications WLC Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => GetItem(x).Name,
            getItemIconHtml: key => NetflowAdvancedApplication.GetIconHtml(Convert.ToInt32(key)))
            {
                HideLinks = true,
                CustomNoDataUrl = KnowledgebaseHelper.GetMindTouchKBUrl(UIConsts.KB_ID_NODATA_WLC),
                ShowPercentageInTooltip = true,
                IconCssFileName = NetflowAdvancedApplication.IconCssFileName,
                ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopApplicationsWlcLegend.ascx",
                ChartLegendExpandedLevelFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopApplicationsWlcLegendTopTablePart.aspx",
                DefaultPieChartLabelOptions = PieChartLabelOptions.Icon | PieChartLabelOptions.Label | PieChartLabelOptions.Percentage
            };
    }

    private static IWlcReportRow GetItem(object dataItem)
    {
        var row = dataItem as DataRow ?? ((DataRowView)dataItem).Row;
        return row as IWlcReportRow;
    }

    public override bool DataSupported
    {
        get { return true; }
    }

    #region ICustomPropertyProvidingResource
    public IEnumerable<DropDownListProperty> DropDownListProperties
    {
        get
        {
            var options = new List<DropDownListProperty>();
            ConfigurationPropertyHelper.AddDropDownListProperty<TopApplicationsWlcFirstLevel>(options);
            return options;
        }
    }

    public string GetDefaultValue(string key)
    {
        if (key == FirstLevelPropertyName)
        {
            return TopApplicationsWlcFirstLevel.Application.ToString();
        }
        else
        {
            return null;
        }
    }
    #endregion

    private static TopApplicationsWlcFirstLevel GetFirstLevel(int resourceId)
    {
        var resourceInfo = ResourceManager.GetResourceByID(resourceId);
        if (resourceInfo != null)
        {
            TopApplicationsWlcFirstLevel firstLevelFromProperty;
            
            if (Enum.TryParse(resourceInfo.Properties[FirstLevelPropertyName], out firstLevelFromProperty))
                return firstLevelFromProperty;
        }

        return TopApplicationsWlcFirstLevel.Application;
    }

    protected override void UpdateDescriptor()
    {
        UpdateDescriptor(Resource.ID);
    }

    protected override void UpdateDescriptor(TopResourceClientParameters parameters)
    {
        UpdateDescriptor(parameters.ResourceID);
    }

    private void UpdateDescriptor(int resourceId)
    {
        var firstLevel = GetFirstLevel(resourceId);
        string firstLevelName, secondLevelName;

        switch (firstLevel)
        {
            default:
                firstLevelName = NTAWebContent.NTAWEBCODE_VB1_21;
                secondLevelName = NTAWebContent.NTAWEBCODE_VB1_Client;
                Descriptor.DAL = new AdvancedApplicationsWlcDAL();
                break;
            case TopApplicationsWlcFirstLevel.Client:
                firstLevelName = NTAWebContent.NTAWEBCODE_VB1_Client;
                secondLevelName = NTAWebContent.NTAWEBCODE_VB1_21;
                Descriptor.DAL = new AdvancedApplicationsWlcByClientDAL();
                Descriptor.Prefix = "E";
                Descriptor.GetItemIconHtml = ip => WirelessResourceBase.GetIconForDeviceType(PollerDeviceType.WirelessController);
                break;

        }

        Descriptor.ItemDisplayName = string.Format("{0}<br />/&nbsp;{1}", firstLevelName, secondLevelName);
    }

    class Parameters : TopResourceParameters
    {
        public Parameters(TopResourceCoreChartBase resourceControl, TopResourceDescriptor descriptor, TopResourceProperties properties, bool fillNetObjectAndFilters)
            : base(resourceControl, descriptor, properties, fillNetObjectAndFilters)
        { }

        protected override NetflowObject GetBaseNetObject(BaseResourceControl resourceControl, bool usePeriodFromView, SolarWinds.Netflow.Web.Utility.ChartFD.ChartFlowDirection chartFlowDirection, TopResourceProperties properties)
        {
            var netObjectProvider = resourceControl.GetInterfaceInstance<IThinAccessPointProvider>();

            if (netObjectProvider != null)
            {
                var accessPoint = netObjectProvider.ThinAccessPoint;
                return (NetflowObject) NetObjectFactory.Create(string.Format("NAP:{0};N:{1}", accessPoint.ID, accessPoint.NodeID));
            }

            return base.GetBaseNetObject(resourceControl, usePeriodFromView, chartFlowDirection, properties);
        }

        protected override bool GetIsAllowed(NetflowObject netObject)
        {
            return new AllNetflowSourcesDAL().HasNodeHadNetWlcTraffic(NetObject.Node?.NodeID, ViewId);
        }
    }
}