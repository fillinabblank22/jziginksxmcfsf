﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopTransmitters.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopTransmitters" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="netflow" TagName="TopCoreChartContainer" Src="~/Orion/TrafficAnalysis/Utils/TopCoreChartContainer.ascx" %>

<orion:ResourceWrapper runat="server" ID="wrapper">
    <Content>
        <netflow:TopCoreChartContainer runat="server" ID="topCoreChartContainer" OwnerResource="<%# this %>" />
     </Content>
</orion:ResourceWrapper>
