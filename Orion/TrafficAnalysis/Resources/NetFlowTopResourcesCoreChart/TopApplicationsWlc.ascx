﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopApplicationsWlc.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopApplicationsWlc" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="netflow" TagName="TopCoreChartContainer" Src="~/Orion/TrafficAnalysis/Utils/TopCoreChartContainer.ascx" %>

<orion:ResourceWrapper runat="server" ID="wrapper" CssClass="wlcResource">
    <Content>
        <netflow:TopCoreChartContainer runat="server" ID="topCoreChartContainer" OwnerResource="<%# this %>" />
     </Content>
</orion:ResourceWrapper>