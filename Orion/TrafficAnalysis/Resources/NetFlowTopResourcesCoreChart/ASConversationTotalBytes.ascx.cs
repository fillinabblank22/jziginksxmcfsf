using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_ASConversationTotalBytes : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHASConversationTotalTransferredBytesV2"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INetflowASConvProvider) };
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_35; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
           reportingDAL: new ASConversationTotalBytesDAL(),
           keyName: "AsID",
           prefix: "S",
           itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_36,
           summaryViewKey: "Netflow Autonomous System Conversations Summary",
           additionalUpdateTypes: null,
           getItemCaption: dataItem => string.Format(Resources.NTAWebContent.NTAWEBDATA_MM0_60, DataBinder.Eval(dataItem, "AsName")),
           getItemIconHtml: key => NetflowAS.GetIconHtml(0))
           {
               DefaultAreaType = AreaChartType.BarChart
           };
    }
}
