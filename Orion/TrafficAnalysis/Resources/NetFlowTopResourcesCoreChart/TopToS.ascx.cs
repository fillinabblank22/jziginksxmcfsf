using System;
using System.Web.UI;
using Resources;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.TypesOfService;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopToS : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopTypesServiceV2"; }
    }

    protected override string DefaultTitle
    {
        get { return NTAWebContent.NTAWEBCODE_TM0_60; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new TypesOfServiceDAL(),
            keyName: "TosID",
            prefix: "TS",
            itemDisplayName: NTAWebContent.NTAWEBCODE_TM0_61,
            summaryViewKey: "NetFlow Types of Service Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "TosName").ToString(),
            getItemIconHtml: key => NetflowTypeOfService.GetIconHtml(TypesOfServiceCache.Instance.GetTypeOfServiceName(Convert.ToInt32(key))))
        {
            ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopToSLegend.ascx"
        };
    }
}