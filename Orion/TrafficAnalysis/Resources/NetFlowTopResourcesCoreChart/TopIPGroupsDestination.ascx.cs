using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopIPGroupsDestination : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopDestIPGroupsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_53; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new IPGroupsDAL(),
            keyName: "GroupID",
            prefix: "G",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_50,
            summaryViewKey: "NetFlow IP Address Groups Summary",
            additionalUpdateTypes: new[] { NetflowUpdateQueryType.IPGroups },
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "GroupName").ToString(),
            getItemIconHtml: key => NetflowIPAddressGroup.GetIconHtml(Convert.ToInt32(key)))
            {
                Tx = false,
                Rx = true
            };
    }
}