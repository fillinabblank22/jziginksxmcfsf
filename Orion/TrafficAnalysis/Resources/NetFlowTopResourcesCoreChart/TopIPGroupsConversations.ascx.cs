using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopIPGroupsConversations : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopIPAddressGroupsConversationsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_52; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new IPGroupsConversationsDAL(),
            keyName: "ConversationID",
            prefix: "G",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_44,
            summaryViewKey: "NetFlow IP Address Groups Summary",
            additionalUpdateTypes: new[] { NetflowUpdateQueryType.IPGroups },
            getItemCaption: dataItem => string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_48, DataBinder.Eval(dataItem, "SourceIPGroupName"), DataBinder.Eval(dataItem, "DestinationIPGroupName")),
            getItemIconHtml: key => NetflowIPAddressGroup.GetIconHtml(0))
            {
                HideLinks = true,
                ModifyKeyForItemLink = key =>
                {
                    string keyStr = key.ToString();
                    keyStr = keyStr.Replace("-", ",");
                    return keyStr;
                },
                GetRootLink = (description, key) => description.ToString()
            };
    }
}