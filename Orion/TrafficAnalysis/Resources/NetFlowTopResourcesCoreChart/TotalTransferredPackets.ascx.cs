using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TotalTransferredPackets : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTotalTransferredPacketsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_65; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        ChartDU.ChartDataUnitText dataUnit = ChartDU.DefaultText;
        dataUnit.Bitrate = Resources.NTAWebContent.NTAWEBCODE_TM0_79;
        dataUnit.TransferredBytes = Resources.NTAWebContent.NTAWEBCODE_TM0_78;

        return new TopResourceDescriptor(
            reportingDAL: new TransferredPacketsDAL(),
            keyName: "DirectionID",
            prefix: "",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_64,
            summaryViewKey: "Netflow Endpoints Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "DirectionName").ToString(),
            getItemIconHtml: UIHelper.GetIconHtmlForTotalTransferredResource)
            {
                DefaultAreaType = AreaChartType.BarChart,
                Unit = DataType.Packets,
                HidePercentOfInterfaceSpeedOption = true,
                HidePercentOfTotalTrafficOption = true,
                GetRootLink = (description, key) => description.ToString(),
                ChartDataUnitTexts = dataUnit,
            };
    }
}