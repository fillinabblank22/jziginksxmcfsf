using System.Web.UI;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopEndpoints : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer => this.topCoreChartContainer;

    public override string HelpLinkFragment => "OrionNetFlowPHTopEndpointsV2";

    protected override string DefaultTitle => Resources.NTAWebContent.NTAWEBCODE_TM0_48;

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new HostsDAL(),
            keyName: "HostName",
            prefix: "E",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_49,
            summaryViewKey: "Netflow EndPoints Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "HostName").ToString(),
            getItemIconHtml: key => NetflowEndpoint.GetIconHtml(key as string))
            {
                ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopEndpointsLegend.ascx"
            };
    }
}