using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopSourceCountries : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopSourcesCountriesV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_58; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new CountriesDAL(),
            keyName: "CountryCode",
            prefix: "L1",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_42,
            summaryViewKey: "Netflow Countries Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "CountryName").ToString(),
            getItemIconHtml: key => NetflowCountry.GetIconHtml(key as string))
            {
                Tx = true,
                Rx = false
            };
    }
}