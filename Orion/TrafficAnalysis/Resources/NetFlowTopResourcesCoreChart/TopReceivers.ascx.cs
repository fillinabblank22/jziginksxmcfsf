using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopReceivers : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopReceiversV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_57; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        return new TopResourceDescriptor(
            reportingDAL: new HostsDAL(),
            keyName: "HostName",
            prefix: "E",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_49,
            summaryViewKey: "Netflow Receivers Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "HostName").ToString(),
            getItemIconHtml: key => NetflowEndpoint.GetIconHtml(key as string))
            {
                Tx = false,
                Rx = true,
                ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopEndpointsLegend.ascx"
            };
    }
}