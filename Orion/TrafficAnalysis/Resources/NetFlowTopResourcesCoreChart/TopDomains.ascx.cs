using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTopResourcesCoreChart_TopDomains : TopResourceCoreChartBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopDomainsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_46; }
    }

    public override string SubTitle
    {
        get
        {
            if (!GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled)
            {
                return Resources.NTAWebContent.NTAWEBCODE_TM0_80;
            }

            return base.SubTitle;
        }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        var descriptor = new TopResourceDescriptor(
            reportingDAL: new DomainsDAL(),
            keyName: "DomainName",
            prefix: "D",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_TM0_47,
            summaryViewKey: "Netflow Domains Summary",
            additionalUpdateTypes: null,
            getItemCaption: dataItem => DataBinder.Eval(dataItem, "DomainName").ToString(),
            getItemIconHtml: key => NetflowDomain.GetIconHtml(key as string));

        descriptor.GetRootLink = (description, key) =>
        {
            if (descriptor.Parameters.ViewType == ViewType.Summary)
                return description.ToString();
            
            string linkTarget = LinkBuilder.GetLinkTarget(descriptor.ModifyKeyForItemLink(key), descriptor.SummaryViewKey, descriptor.Prefix, descriptor.Parameters.Filters, descriptor.ResourceType, descriptor.Parameters.ViewType);

            return string.IsNullOrEmpty(linkTarget) ? description.ToString() : string.Format("<a href='{0}'>{1}</a>", linkTarget, description);
        };

        return descriptor;
    }
}