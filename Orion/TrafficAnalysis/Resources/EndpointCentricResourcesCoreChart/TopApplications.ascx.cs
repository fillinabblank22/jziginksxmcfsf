using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Resources;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Utils;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopApplications : TopResourceCoreEndpointCentricBase
{
    protected const string ApplicationProviderPropertyName = "ApplicationProvider";

    protected DropDownList drApplicationProviderMenu;

    protected ApplicationProvider? defaultApplicationProvider;
    protected ApplicationProvider DefaultApplicationProvider
    {
        get
        {
            if (!defaultApplicationProvider.HasValue)
                defaultApplicationProvider = GetDefaultApplicationProvider();

            return defaultApplicationProvider.Value;
        }
    }

    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopApplicationsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return NTAWebContent.NTAWEBCODE_VB1_20; }
    }

    protected ApplicationProvider GetDefaultApplicationProvider()
    {
        ResourceInfo resourceInfo = ResourceManager.GetResourceByID(Resource.ID);
        if (resourceInfo != null)
        {
            ApplicationProvider providerFromProperty;
            if (Enum.TryParse(resourceInfo.Properties[ApplicationProviderPropertyName], out providerFromProperty))
            {
                return providerFromProperty;
            }

            if (HasAarData())
            {
                return ApplicationProvider.NBAR2;
            }
        }

        return ApplicationProvider.Netflow;
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        var result = new TopResourceDescriptor(
            reportingDAL: new ApplicationsDAL(),
            keyName: "AppID",
            prefix: "A",
            itemDisplayName: NTAWebContent.NTAWEBCODE_VB1_21,
            summaryViewKey: "Netflow Applications Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "ApplicationName").ToString(),
            iconCssFileName: NetflowAdvancedApplication.IconCssFileName)
        {
            DefaultPieChartLabelOptions =
                PieChartLabelOptions.Icon | PieChartLabelOptions.Label | PieChartLabelOptions.Percentage
        };

        result.GetItemIconHtml = key => NetflowApplication.GetIconHtml(key.ToString());
        result.ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopApplicationsLegend.ascx";

        return result;
    }

    protected override void UpdateDescriptor()
    {
        base.UpdateDescriptor();

        UpdateDescriptorByProvider(DefaultApplicationProvider);
    }

    protected override void UpdateDescriptor(TopResourceClientParameters parameters)
    {
        base.UpdateDescriptor(parameters);

        UpdateDescriptorByProvider(parameters.ApplicationProvider);
    }

    protected void UpdateDescriptorByProvider(ApplicationProvider? applicationProvider)
    {
        if (applicationProvider == ApplicationProvider.NBAR2)
        {
            Descriptor.Prefix = "AA";
            Descriptor.GetItemIconHtml = key => NetflowAdvancedApplication.GetIconHtml(Convert.ToInt32(key));
            Descriptor.DAL = new AdvancedApplicationsDAL();
            Descriptor.CustomNoDataUrl = KnowledgebaseHelper.GetKBUrl(UIConsts.KB_ID_NODATA_NBAR2);
            Descriptor.ShowPercentageInTooltip = true;
        }
        else if (applicationProvider == ApplicationProvider.Netflow)
        {
            Descriptor.Prefix = "A";
            Descriptor.UpdateTypes = Descriptor.UpdateTypes.Union(new[] { NetflowUpdateQueryType.Applications }).Distinct().ToArray();
            Descriptor.GetItemIconHtml = key => NetflowApplication.GetIconHtml(key.ToString());
            Descriptor.DAL = new ApplicationsDAL();
            Descriptor.ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopApplicationsLegend.ascx";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        Control resourceWrapper = GetResourceWrapper(this);
        if (resourceWrapper != null && resourceWrapper.TemplateControl is IResourceWrapper)
        {
            IResourceWrapper templateControl = (IResourceWrapper)resourceWrapper.TemplateControl;

            HtmlGenericControl div = new HtmlGenericControl("div")
            {
                ID = "applicationProviderMenu"
            };
            div.Attributes.Add("class", "EditResourceButton applicationProviderMenu");

            drApplicationProviderMenu = new DropDownList { ID = "drApplicationProviderMenu" };

            foreach (ApplicationProvider applicationProvider in Enum.GetValues(typeof(ApplicationProvider)))
            {
                ListItem item = new ListItem(GetApplicationProviderTitle(applicationProvider), applicationProvider.ToString())
                {
                    Selected = (applicationProvider == DefaultApplicationProvider)
                };

                drApplicationProviderMenu.Items.Add(item);
            }

            div.Controls.Add(drApplicationProviderMenu);

            templateControl.HeaderButtons.Controls.Add(div);
        }
    }

    protected bool HasAarData()
    {
        int? nodeID = null;
        int? interfaceID = null;
        DateTime? startTime = null;
        DateTime? endTime = null;

        INetflowSourceFilter sourceFilter = Descriptor.Parameters.Filters != null ? Descriptor.Parameters.Filters.NetflowSource : null;
        if (sourceFilter != null)
        {
            nodeID = (int?)sourceFilter.NodeID;

            INetflowInterfaceFilter interfaceFilter = sourceFilter as INetflowInterfaceFilter;
            if (interfaceFilter != null)
                interfaceID = (int?)interfaceFilter.InterfaceID;
        }

        ITimePeriodFilter timeFilter = Descriptor.Parameters.Filters?.PeriodFilter;
        if (timeFilter != null)
        {
            startTime = timeFilter.StartTime;
            endTime = timeFilter.EndTime;
        }

        IAllNetFlowSourcesDAL allNetFlowSourcesDAL = new AllNetflowSourcesDAL();
        var result = allNetFlowSourcesDAL.HasNbar2Data(nodeID, interfaceID, startTime, endTime);
        return result;
    }

    protected string GetApplicationProviderTitle(ApplicationProvider applicationProvider)
    {
        switch (applicationProvider)
        {
            case ApplicationProvider.Netflow:
                return Resources.NTAWebContent.NTAWEBDATA_TopXXApplications_ApplicationProvider_Netflow;
            case ApplicationProvider.NBAR2:
                return Resources.NTAWebContent.NTAWEBDATA_TopXXApplications_ApplicationProvider_NBAR2;
        }
        return null;
    }

    protected Control GetResourceWrapper(Control control)
    {
        if (control.TemplateControl is IResourceWrapper)
            return control;

        foreach (Control childControl in control.Controls)
        {
            Control result = GetResourceWrapper(childControl);

            if (result != null)
                return result;
        }

        return null;
    }

    protected override void SetChartInitializerScript()
    {
        string applicationProviderChangeFunctionName = string.Format("resource{0}_onApplicationProviderChange", Resource.ID);
        string refreshViewFunctionName = string.Format("resource{0}_refreshView", Resource.ID);
        var relativeDateTimeFieldName = string.Format("resource{0}_relativeDateTime", Resource.ID);

        drApplicationProviderMenu.Attributes["onchange"] = string.Format("{0}();", applicationProviderChangeFunctionName);

        string script = string.Format(@"
            var {10} = {11};

            // Chart initializer script for resource {0}

            function {3}() {{
                var details = {1};
                
                var applicationProvider = $('#{6}').val();
                details.applicationProvider = applicationProvider;
                details.relativeDateTime = {10};

                var options = {2};

                $('#{9}').html('');
                SW.Core.Charts.initializeStandardChart(details, options); 
            }}

            $(function () {{ 
                function updateTimePeriodAndRefresh() {{
                    {10} = Math.floor(new Date / 1e3);
                    {3}();
                }}

                if((typeof(SW.Core.View) != 'undefined') && (typeof(SW.Core.View.AddOnRefresh) != 'undefined')) {{            
                    SW.Core.View.AddOnRefresh(updateTimePeriodAndRefresh, '{4}');
                }}
                {3}();
            }});

            function {5}() {{                
                var applicationProvider = $('#{6}').val();" + (!SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer ? @"

                SW.Core.Services.callController('/api/Resources/AddOrUpdateResourceProperty',
                {{
                    resourceId: {7},
                    propertyName: '{8}',
                    propertyValue: applicationProvider
                }}, $.noop, $.noop);" : "") + @"

                {3}();
            }}
            ",
            Resource.Name,
            DisplayDetails,
            ChartOptions,
            refreshViewFunctionName,
            ChartPlaceHolder.ClientID,
            applicationProviderChangeFunctionName,
            drApplicationProviderMenu.ClientID,
            Resource.JavaScriptFriendlyID,
            ApplicationProviderPropertyName,
            ChartLegendControl.LegendInitializer.Replace("TopResourceLegendInitializer__", string.Empty),
            relativeDateTimeFieldName,
            DateTime.Now.ToUnixTimestamp()
            );

        ScriptInitializer.InnerHtml = script;
    }
}