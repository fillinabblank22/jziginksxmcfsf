using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TotalTransferredBytes : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTotalTransferredBytesV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_42; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        return new TopResourceDescriptor(
            reportingDAL: new TransferredBytesDAL(),
            keyName: "DirectionID",
            prefix: "",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_43,
            summaryViewKey: "Netflow Endpoints Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "DirectionName").ToString(),
            getItemIconHtml: UIHelper.GetIconHtmlForTotalTransferredResource)
            {
                DefaultAreaType = AreaChartType.BarChart,
                GetRootLink = (description, key) => description.ToString()
            };
    }
}