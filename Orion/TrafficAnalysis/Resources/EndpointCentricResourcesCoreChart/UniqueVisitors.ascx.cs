using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_UniqueVisitors : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHUniqueVisitorsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_46; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        ChartDU.ChartDataUnitText dataUnit = ChartDU.DefaultText;
        dataUnit.Bitrate = Resources.NTAWebContent.NTAWEBCODE_TM0_76;
        dataUnit.TransferredBytes = Resources.NTAWebContent.NTAWEBCODE_TM0_77;

        return new TopResourceDescriptor(
            reportingDAL: new UniqueVisitorsDAL(),
            keyName: "Caption",
            prefix: "V",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_47,
            summaryViewKey: "Netflow Endpoints Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "Caption").ToString(),
            getItemIconHtml: x => string.Empty)
            {
                DefaultAreaType = AreaChartType.BarChart,
                Unit = DataType.Visitors,
                AllowPercentColumn = false,
                GetRootLink = (description, key) => description.ToString(),
                HideLinks = true,
                CustomColumnCaption = Resources.NTAWebContent.NTAWEBCODE_TM0_70,
                CustomColumnDataName = "Caption",
                HidePercentOfTotalTrafficOption = true,
                HidePercentOfInterfaceSpeedOption = true,
                ChartDataUnitTexts = dataUnit
            };
    }
}