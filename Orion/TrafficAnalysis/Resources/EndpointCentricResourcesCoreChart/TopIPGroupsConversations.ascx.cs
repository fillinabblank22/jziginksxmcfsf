using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopIPGroupsConversations : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopIPAddressGroupsConversationsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_33; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        return new TopResourceDescriptor(
            reportingDAL: new IPGroupsConversationsDAL(),
            keyName: "ConversationID",
            prefix: "G",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_32,
            summaryViewKey: "NetFlow IP Address Groups Summary",
            additionalUpdateTypes: new[] { NetflowUpdateQueryType.IPGroups },
            getItemCaption: x => string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_48, DataBinder.Eval(x, "SourceIPGroupName"), DataBinder.Eval(x, "DestinationIPGroupName")),
            getItemIconHtml: x => NetflowIPAddressGroup.GetIconHtml(0))
            {
                HideLinks = true,
                ModifyKeyForItemLink = key =>
                {
                    string keyStr = key.ToString();
                    keyStr = keyStr.Replace("-", ",");
                    return keyStr;
                },
                GetRootLink = (description, key) => description.ToString()
            };
    }
}