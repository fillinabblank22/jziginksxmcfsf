using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopReceivers : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopReceiversV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_36; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        return new TopResourceDescriptor(
            reportingDAL: new HostsDAL(),
            keyName: "HostName",
            prefix: "E",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_37,
            summaryViewKey: "Netflow Receivers Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "HostName").ToString(),
            getItemIconHtml: x => NetflowEndpoint.GetIconHtml(x as string))
            {
                Tx = false,
                Rx = true,
                ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopEndpointsLegend.ascx"
            };
    }
}