﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopDomains.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopDomains" %>
<%@ Register TagPrefix="orion" TagName="ResourceWrapper" Src="~/Orion/ResourceWrapper.ascx" %>
<%@ Register TagPrefix="netflow" TagName="TopDomainCoreChartContainer" Src="~/Orion/TrafficAnalysis/Utils/TopDomainCoreChartContainer.ascx" %>

<orion:ResourceWrapper runat="server" ID="wrapper">
    <Content>
        <netflow:TopDomainCoreChartContainer runat="server" ID="topCoreChartContainer" OwnerResource="<%# this %>" />
     </Content>
</orion:ResourceWrapper>