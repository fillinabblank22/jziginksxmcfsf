using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopConversations : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }
    
    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopConversationsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_23; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        return new TopResourceDescriptor(
            reportingDAL: new ConversationsDAL(),
            keyName: "ConversationID",
            prefix: "C",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_22,
            summaryViewKey: "NetFlow Conversations Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => string.Empty,
            getItemIconHtml: x => NetflowConversation.GetIconHtml())
            {
                ChartLegendFilePath = "~/Orion/TrafficAnalysis/CoreCharts/TopConversationsLegend.ascx"
            };
    }
}