using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_EndpointCentricResourcesCoreChart_TopDomains : TopResourceCoreEndpointCentricBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTopDomainsV2"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB1_27; }
    }

    protected override TopResourceDescriptor GetDescriptorEndpointCentric()
    {
        TopResourceDescriptor descriptor = new TopResourceDescriptor(
            reportingDAL: new DomainsDAL(),
            keyName: "DomainName",
            prefix: "D",
            itemDisplayName: Resources.NTAWebContent.NTAWEBCODE_VB1_26,
            summaryViewKey: "Netflow Domains Summary",
            additionalUpdateTypes: null,
            getItemCaption: x => DataBinder.Eval(x, "DomainName").ToString(),
            getItemIconHtml: x => NetflowDomain.GetIconHtml(x as string));

        descriptor.GetRootLink = (description, key) =>
        {
            if (descriptor.Parameters.ViewType == ViewType.Summary)
                return description.ToString();
            
            string linkTarget = LinkBuilder.GetLinkTarget(descriptor.ModifyKeyForItemLink(key), descriptor.SummaryViewKey, descriptor.Prefix, descriptor.Parameters.Filters, descriptor.ResourceType, descriptor.Parameters.ViewType);

            return string.IsNullOrEmpty(linkTarget) ? description.ToString() : string.Format("<a href='{0}'>{1}</a>", linkTarget, description);
        };

        return descriptor;
    }
}