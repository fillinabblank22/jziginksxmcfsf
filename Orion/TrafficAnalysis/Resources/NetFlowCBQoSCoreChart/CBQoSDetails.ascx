﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowCBQoSCoreChart_CBQoSDetails" %>
<%@ Register TagPrefix="netflow" TagName="cbQoSInfo" Src="~/Orion/TrafficAnalysis/Utils/CBQoSInformation.ascx" %>


<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    
        <netflow:cbQoSInfo ID="CbQoSInfo1" runat="server" 
            NodeID="<%# NetObject.Node.NodeID %>" 
            InterfaceID="<%# NetObject.Interface.InterfaceID %>"
            PolicyID="<%# PolicyID %>"
            InterfaceSpeed="<%# InterfaceSpeed %>"
            />
    </Content>
</orion:resourceWrapper>
