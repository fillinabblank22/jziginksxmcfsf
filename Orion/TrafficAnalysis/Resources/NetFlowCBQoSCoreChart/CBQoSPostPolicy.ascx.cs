using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowCBQoSCoreChart_CBQoSPostPolicy : TopResourceCoreCBQoSBase
{
    protected override ITopCoreChartContainer ChartContainer
    {
        get { return this.topCoreChartContainer; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK0_22; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHCBQoSTopPostPolicyClassMapV2"; }
    }

    public override string StatsID
    {
        get { return CBQoSSupport.PostPolicyStatsName; }
    }

    protected override TopResourceDescriptor GetDescriptor()
    {
        var descriptor = base.GetDescriptor();
        descriptor.CustomNoDataUrl = KnowledgebaseHelper.GetKBUrl(UIConsts.KB_ID_NODATA_CBQOS);
        return descriptor;
    }
}