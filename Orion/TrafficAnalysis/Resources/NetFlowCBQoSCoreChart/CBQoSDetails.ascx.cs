using System;
using System.Data;
using System.Text;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.NPM.Web;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Charts)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowCBQoSCoreChart_CBQoSDetails
    : BaseResourceControl
{
    protected override void OnInit(EventArgs e)
    {
        object obj = this.GetInterfaceInstance<INetflowInterfaceProvider>();

        if (obj == null)
        {
            obj = this.GetInterfaceInstance<INetflowCBQoSProvider>();
        }
        if (obj == null)
        {
            obj = this.GetInterfaceInstance<IInterfaceProvider>();
        }

        if (obj is INetflowCBQoSProvider)
        {
            NetObject = (obj as INetflowCBQoSProvider).NetflowCBQoS;
        }
        else if (obj is INetflowInterfaceProvider)
        {
            NetObject = (obj as INetflowInterfaceProvider).NetflowInterface;
        }
        else if (obj is IInterfaceProvider)
        {
            string netObjectID = this.Request.QueryString["NetObject"];

            if ((netObjectID != null) && netObjectID.StartsWith("CCM", StringComparison.InvariantCultureIgnoreCase))
            {
                NetObject = (NetflowObject)NetObjectFactory.Create(netObjectID);
            }
            else
            {
                NetObject = (NetflowObject)NetObjectFactory.Create(string.Format("N{0}", (obj as IInterfaceProvider).Interface.NetObjectID));
            }
        }
        else
        {
            throw new ArgumentException("Unknown provider");//[Localization] This is exception which can appears only during development when the control is used in wrong way
        }

        if (NetObject == null)
            throw new ArgumentException("Invalid cast to provider");//[Localization] This is exception which can appears only during development when the control is used in wrong way

        NetObjectReportParameters reportParams = new NetObjectReportParameters(NetObject.Interface, NetObject.Filters.FlowDirection);
        InterfaceSpeed = TopXXReportHelper.GetInterfaceBandwidth(reportParams);

        if (!IsPostBack)
        {
            this.DataBind();
        }

        base.OnInit(e);
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(IInterfaceProvider) };
        }
    }

    #region Properties

    public NetflowObject NetObject { get; private set; }

    public int PolicyID
    {
        get
        {
            if (NetObject is NetflowCBQoS)
            {
                return (NetObject as NetflowCBQoS).PolicyID;
            }
            else
            {
                return 0;
            }
        }
    }

    public double InterfaceSpeed { get; private set; }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHCBQoSDetails";
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK0_12; }
    }

    public string Icon
    {
        get
        {
            if (this.NetObject != null)
            {
                return this.NetObject.IconPath;
            }
            return string.Empty;
        }
    }

    #endregion Properties
}
