<%@ Page Language="C#" MasterPageFile="~/Orion/TrafficAnalysis/Admin/NetflowResourceEdit.master" AutoEventWireup="true" CodeFile="EditThwack.aspx.cs" Inherits="EditThwack" Title="Untitled Page"%>
<%@ Register Src="~/Orion/TrafficAnalysis/Controls/NtaHelpButton.ascx" TagPrefix="nta" TagName="NtaHelpButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TopRightPageLinks" runat="server">
    <nta:NtaHelpButton ID="HelpButton9" runat="server" HelpUrlFragment="OrionNetFlowPHThwackRecentNetFlowPosts" 
                       Title="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_43%>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  <style type="text/css">
    .nfsBreadcrumb { margin-left: 10px !Important; }
  </style>
  <div class="sw-res-editor">
    <h1> <%= Title %> </h1>

    <div>
        <div>
            <b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_42%></b>
             <br />
            <asp:TextBox ID="PostsCount" runat="server" />
            <asp:RangeValidator ID="PostsCountRangeValidator" runat="server" 
                    ErrorMessage="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_44%>" Display="Dynamic" ControlToValidate="PostsCount" 
                    Type="Integer" MinimumValue="1" SetFocusOnError="True" MaximumValue="1000"></asp:RangeValidator>
        
        </div>
    </div>  <%--Resource wrapper DIV END--%>
    <div class="sw-btn-bar">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="SubmitClick" 
                                 DisplayType="Primary" />
    </div>    
  </div>
</asp:Content>
