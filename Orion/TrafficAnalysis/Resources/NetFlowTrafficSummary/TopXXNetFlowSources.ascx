<%@ Import namespace="SolarWinds.Orion.NPM.Web"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXNetFlowSources.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_TopXXNetFlowSources" %>

<orion:Include ID="IncludeExt" runat="server" Framework="Ext" FrameworkVersion="4.0" />

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        
        <script type="text/javascript">

            Ext4.override(Ext4.ZIndexManager, {
                _showModalMask: function (comp) {
                    var zIndex = comp.el.getStyle('zIndex') - 4,
                    maskTarget = comp.floatParent ? comp.floatParent.getTargetEl() : Ext4.get(comp.getEl().dom.parentNode),
                    parentBox = maskTarget.getBox();

                    if (Ext4.isSandboxed) {
                        if (comp.isXType('loadmask')) {
                            zIndex = zIndex + 3;
                        }

                        if (
                            maskTarget.hasCls(Ext4.baseCSSPrefix + 'reset') &&
                            maskTarget.is('div')) {

                            maskTargetParentTemp = Ext4.get(maskTarget.dom.parentNode)
                            if (maskTargetParentTemp.is('body')) {
                                maskTarget = maskTargetParentTemp;
                                parentBox = maskTarget.getBox();
                            }
                        }
                    }

                    if (!this.mask) {
                        this.mask = Ext4.getBody().createChild({
                            cls: Ext4.baseCSSPrefix + 'mask'
                        });
                        this.mask.setVisibilityMode(Ext4.Element.DISPLAY);
                        this.mask.on('click', this._onMaskClick, this);
                    }

                    if (maskTarget.dom === document.body) {
                        parentBox.height = document.documentElement.scrollHeight;
                    }
                    maskTarget.addCls(Ext4.baseCSSPrefix + 'body-masked');
                    this.mask.setBox(parentBox);
                    this.mask.setStyle('zIndex', zIndex);
                    this.mask.show();
                }
            });

        </script>

        <table border="0" cellpadding="2" cellspacing="0" width="500" class="NeedsZebraStripes">
	        <asp:Repeater ID="rptSources" runat="server" EnableViewState="false">
              <HeaderTemplate>
                <thead>
                    <tr>
                        <td><%= Resources.NTAWebContent.NTAWEBDATA_TM0_30%></td>
	                    <td><%= Resources.NTAWebContent.NTAWEBDATA_TM0_31%></td>
	                    <td><%= Resources.NTAWebContent.NTAWEBDATA_TM0_32%></td>
	                    <td><%= Resources.NTAWebContent.NTAWEBDATA_TM0_33%></td>
	                </tr>
                </thead>
              </HeaderTemplate>
              <ItemTemplate>
                <tr>
                <td style='<%# this.NodeHasHadTraffic(Eval("NodeID")) ? "font-style:normal" : "font-style:italic"%>'>
                    <img src='<%# "/Orion/images/StatusIcons/Small-" + Eval("GroupStatus") %>' alt='' />
                   <%# WriteNode(Eval("NodeID"), Eval("NodeName")) %>
                </td>    
                <td style='<%# this.InterfaceHasHadTraffic(Eval("InterfaceID")) ? "font-style:normal" : "font-style:italic"%>'>
                    <img src='<%# "/Orion/images/StatusIcons/Small-" + Eval("StatusLED") %>' alt='' />
                   <%#WriteInterface(Eval("InterfaceID"), Eval("Caption"), Eval("NodeName"))%>
                </td>    
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="border-style:none; width:45px">
                                <asp:Label runat="server" Text='<%# Eval("InPercentUtil")%>' OnLoad="PercentLabelOnLoad"/>
                            </td>
                            <td style="border-style:none; " align="left">
                                <asp:Image runat="server" OnLoad="PercentImageOnLoad" Width='<%# (Convert.ToDouble(Eval("InPercentUtil")) > Int16.MaxValue ? Int16.MaxValue : Convert.ToDouble(Eval("InPercentUtil"))) %>' Height="12"/>
                            </td>
                        </tr>                    
                    </table>
                </td>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="border-style:none; width:45px">
                                <asp:Label runat="server" Text='<%# Eval("OutPercentUtil")%>' OnLoad="PercentLabelOnLoad"/>
                            </td>
                            <td style="border-style:none; " align="left">
                                <asp:Image runat="server" OnLoad="PercentImageOnLoad" Width='<%# (Convert.ToDouble(Eval("OutPercentUtil")) > Int16.MaxValue ? Int16.MaxValue : Convert.ToDouble(Eval("OutPercentUtil"))) %>' Height="12"/>
                            </td>
                        </tr>                    
                    </table>
                </td>
                </tr>
               </ItemTemplate>
            </asp:Repeater>
            <asp:Label runat="server" 
                       ID="infoMsg" 
                       Visible="false" 
                       Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_34%>" 
                       style="margin-left:10px">
            </asp:Label>
        </table> 
     </Content>
</orion:resourceWrapper>