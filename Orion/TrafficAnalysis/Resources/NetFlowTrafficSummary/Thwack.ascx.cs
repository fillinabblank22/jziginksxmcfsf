using System;
using System.Web.UI;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Orion.Web.Helpers;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Reports)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_Thwack : BaseResourceControl
{
    private const int DefaultNumberOfPosts = 15;

    protected void Page_Load(object sender, EventArgs e)
    {
        AddStylesheet(@"/Orion/styles/Thwack.css");

        HelpButton.NavigateUrl = HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, HelpLinkFragment);
        HelpButton.Text = Resources.CoreWebContent.ResourcesAll_Help;
        linkEdit.NavigateUrl = EditURL;
    }

    public string ResourceID => Resource.ID.ToString();

    [PersistenceMode(PersistenceMode.Attribute)]
    public int Count
    {
        get
        {
            int count;
            return int.TryParse(Resource.Properties["NumberOfPosts"], out count) ? count : DefaultNumberOfPosts;
        }
    }

    protected override string DefaultTitle => Resources.NTAWebContent.NTAWEBCODE_TM0_16;

    public override string SubTitle => string.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_20, Count);

    public override string EditURL => SolarWinds.Netflow.Web.UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Resources/NetFlowTrafficSummary/EditThwack.aspx", Resource.ID);

    public override string HelpLinkFragment => "OrionNetFlowPHThwackRecentNetFlowPosts";

    public override ResourceLoadingMode ResourceLoadingMode => ResourceLoadingMode.Ajax;
}
