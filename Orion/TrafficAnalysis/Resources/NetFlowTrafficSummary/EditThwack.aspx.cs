using System;

using SolarWinds.Orion.Web;
public partial class EditThwack : System.Web.UI.Page
{
	private int _resourceID;
	private ResourceInfo _resource;
	private string _netObjectID;

    protected ResourceInfo Resource
	{
		get { return _resource; }
	}

	protected override void OnInit(EventArgs e)
	{
		if (!string.IsNullOrEmpty(Request.QueryString["ResourceID"]))
		{
			_resourceID = Convert.ToInt32(Request.QueryString["ResourceID"]);
			_resource = ResourceManager.GetResourceByID(_resourceID);
            Page.Title = string.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_15, _resource.Title);
		}

		if (!string.IsNullOrEmpty(Request.QueryString["NetObject"]))
		{
			_netObjectID = Request.QueryString["NetObject"];
		}
		else
		{
			_netObjectID = string.Empty;
		}

		if (!string.IsNullOrEmpty(Resource.Properties["NumberOfPosts"]) && !IsPostBack)
		{
			PostsCount.Text = Resource.Properties["NumberOfPosts"];
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void SubmitClick(object sender, EventArgs e)
	{
         // Server validation part, note it still could be an empty string
        if (PostsCountRangeValidator.IsValid)
        {
            int count;
            if (Int32.TryParse(PostsCount.Text, out count) || string.IsNullOrEmpty(PostsCount.Text))
            {
                Resource.Properties.Clear();
                if (!string.IsNullOrEmpty(PostsCount.Text))
                {
                    Resource.Properties.Add("NumberOfPosts", count.ToString());
                }

                string url = string.Format("/Orion/View.aspx?ViewID={0}", Resource.View.ViewID);
                if (!string.IsNullOrEmpty(_netObjectID))
                {
                    url = string.Format("{0}&NetObject={1}", url, _netObjectID);
                }
                Response.Redirect(url);
            }
        }
	}
}
