<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Thwack.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_Thwack" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy" runat="server">
    <Services>
        <asp:ServiceReference path="~/Orion/TrafficAnalysis/WebServices/ThwackService.asmx"  />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourceWrapper runat="server" ID="resourceWrapper" ShowHeaderBar="false">
    <Content>
        <style type="text/css">
            #manageButtons .HelpButton {
                float: none !Important;
            }
            #manageButtons .EditResourceButton {
                float: none !Important;
            }
        </style>
        <div>
            <table cellpadding="0" cellspacing="0" border="0" class="ThwackHeaderBackground">
                <tr>
                    <td style="border: none; height: 36px; width: 10px;">&nbsp;</td>
                    <td style="border: 0; border-bottom: 0;">
                        <table cellpadding="0px" cellspacing="0px" border="0" class="ThwackHeader">
                            <tr>
                                <td class="ThwackHeaderLogo" style="border: none;">
                                    <img class="ThwackHeaderImage" src="<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("Orion", "ThwackImages/ThwackLogo_b.png") %>" alt="<%=Resources.InterfacesWebContent.NPMWEBDATA_AK0_46%>" />
                                </td>
                                <td class="ThwackHeaderText" style="border: none;">
                                    <span><%=Resources.NTAWebContent.NTAWEBDATA_RecentNetFlowPosts%></span>
                                </td>
                                <td id="manageButtons" style="border: none;" align="right">
                                    <asp:PlaceHolder ID="phEditButton" runat="server">
                                        <orion:LocalizableButtonLink ID="linkEdit" runat="server" LocalizedText="Edit" DisplayType="Secondary" CssClass="EditResourceButton" />
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="phHelpButton" runat="server">
                                        <orion:LocalizableButtonLink ID="HelpButton" runat="server" LocalizedText="CustomText" DisplayType="Secondary" CssClass="HelpButton" />
                                    </asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="border: none; height: 34px; width: 15px;">&nbsp;</td>
                </tr>
            </table>

            <div runat="server" id="ThwackRecentPostsList" class="ThwackContent" />

            <table cellpadding="0px" cellspacing="0px" class="ThwackSubmenu">
                <tr>
                    <td style="border: none; text-align: right; padding-right: 10px;">
                        <a href="http://thwack.com/forums/10.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_40%></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="http://thwack.com/user/CreateUser.aspx"><%= Resources.NTAWebContent.NTAWEBDATA_TM0_41%></a>
                    </td>
                </tr>
            </table>
        </div>
        
        <script type="text/javascript">
//<![CDATA[
            Sys.Application.add_init(function(){
                ThwackService.GetRecentNetFlowPosts(<%= this.Count %>, 
                    function(result) {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = result;
                    },
                    function() {
                        var thwackRecentPostsList = $get('<%=this.ThwackRecentPostsList.ClientID %>');
                        thwackRecentPostsList.innerHTML = '<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEBCODE_TM0_17)%>';
                    }); });
//]]>
        </script>
    </Content>
</orion:resourceWrapper>
