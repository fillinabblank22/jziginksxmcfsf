using System;
using System.Data;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.TopXXLists)]
public partial class Orion_TrafficAnalysis_Resources_TopXXNetFlowSources : BaseResourceControl
{
    private Log _log = new Log("TopXXnetFlowSources");

    bool NetFlow => true;
    bool CBQoS => true;

    public int ViewId { get; internal set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable data = new TopNetflowSourcesUtizationDAL().GetTopSources(this.ItemCount);
        if (data.Rows.Count > 0)
        {
            rptSources.DataSource = data;
            rptSources.DataBind();
        }
        else
        {
            rptSources.Visible = false;
            infoMsg.Visible = true;
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // for NodePopup on detached resource
        ControlHelper.AddStylesheet(this.Page, "/Orion/TrafficAnalysis/styles/NetFlow.css");

        var currentResource = ResourceManager.GetResourceByID(Resource.ID);
        ViewId = currentResource != null && currentResource.View != null ? currentResource.View.ViewID : 0;
    }

    public string WriteInterface(object interfaceID, object caption, object nodeName)
    {
        string str = string.Format(@"<a href='/Orion/TrafficAnalysis/NetflowInterfaceDetails.aspx?NetObject=NI:{0}'", interfaceID);

        // add onclick when no data
        if (false == this.InterfaceHasHadTraffic(interfaceID))
        {
            string s = string.Format(UIHelper.NoDataOnInterfaceMessageBox, nodeName, caption);
            str += string.Format(@" onclick='{0}' ", s);
        }

        str += ">" + caption.ToString()+ "</a>";
        return str;
    }

    public string WriteNode(object nodeID, object nodeName)
    {
        string str = string.Format("<a href='/Orion/TrafficAnalysis/NetflowNodeDetails.aspx?NetObject=NN:{0}'", nodeID);
        if (false == this.NodeHasHadTraffic(nodeID))
        {
            string s = string.Format(UIHelper.NoDataOnNodeMessageBox, nodeName);
            str += string.Format(@" onclick='{0}' ", s);
        }

        str += ">" + nodeName.ToString() + "</a>";
        return str;
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_13; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHTopNetFlowSource";
        }
    }

    public override string EditURL
    {
        get
        {
            return UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Editors/TopXXNetFlowSourcesEdit.aspx", Resource.ID);
        }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            return String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_14, this.ItemCount);
        }
    }

    protected int ItemCount
    {
        get
        {
            return this.GetIntProperty("MaxMessages", 10);
        }
    }

    protected bool NodeHasHadTraffic(object nodeID)
    {
        if (nodeID != null)
        {
            try
            {
                int id = (int)nodeID;
                return new AllNetflowSourcesDAL().HasNodeHadNetFlowTraffic(id, ViewId);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return false;
            }
        }

        return false;
    }

    protected bool InterfaceHasHadTraffic(object interfaceID)
    {
        if (interfaceID != null)
        {
            try
            {
                int id = (int)interfaceID;
                return new AllNetflowSourcesDAL().HasInterfaceHadNetFlowTraffic(id, ViewId);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return false;
            }
        }
        return false;
    }

    protected void PercentImageOnLoad(object sender, EventArgs e)
    {
        UIHelper.PercentImageOnLoad(sender);
    }

    protected void PercentLabelOnLoad(object sender, EventArgs e)
    {
        UIHelper.PercentLabelOnLoad(sender);
    }
}
