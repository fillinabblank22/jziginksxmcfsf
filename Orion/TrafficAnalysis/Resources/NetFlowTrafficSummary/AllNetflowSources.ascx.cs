using System;

using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_TrafficAnalysis_Resources_AllNetflowSources : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_AK2_5; }
    }

    public override string EditURL
    {
        get
        {
            return UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Editors/AllNetflowSourcesEdit.aspx", Resource.ID);
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHSummaryActiveSources";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        this.ResourceWrapper1.ManageButtonTarget = UIConsts.URL_NETFLOW_SOURCES_MANAGEMENT;
        this.ResourceWrapper1.ShowManageButton = SolarWinds.Netflow.Web.Utility.LicenseHelper.IsAllowed(Profile.AllowAdmin);
        this.ResourceWrapper1.ManageButtonText = Resources.NTAWebContent.NTAWEBCODE_ManageFlowSources;
        // Note: This code was at least partially broken (FB 93866). Removal scheduled in FB 95347.
        // it is BAD idea to use something derived from BaseResourceControl inside of another BaseResourceControl.                                                 // the Resource property isn't set, and it is confusing.

        base.OnInit(e);
    }

}


