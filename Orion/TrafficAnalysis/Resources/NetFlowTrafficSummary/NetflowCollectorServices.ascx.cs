using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;

using SolarWinds.Netflow.DAL;
using SolarWinds.Netflow.Web;

using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.InventoryLists)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_NetflowCollectorServices : BaseResourceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Resource.Title = Resources.NTAWebContent.NTAWEBCODE_TM0_9;
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_9; }
    }

    public override string SubTitle
    {
        get
        {
            return string.Empty;
        }
    }

    public override string EditURL
    {
        get
        {
            return UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Admin/NetflowCollectorServicesEdit.aspx");
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHSummaryCollectorServices";
        }
    }
}
