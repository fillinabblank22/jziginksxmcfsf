using System;
using System.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Help)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_WhatsNew : BaseResourceControl
{
   
    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_1; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHNewInNTA"; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            return String.Format(Resources.NTAWebContent.NTAWEBCODE_TM0_2, this.Title, GetMajorNtaVersion());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Resource.Title = DefaultTitle;

        btnLearnMore.Text = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_13, GetMajorNtaVersion());
        btnRemoveResource.NavigateUrl = String.Format("/Orion/TrafficAnalysis/Editors/RemoveResource.aspx?ResourceName={0}&ResourceNameShort={1}&ResourceFile={2}",
                                                      HttpUtility.UrlEncode(DefaultTitle), HttpUtility.UrlEncode(ShortName), HttpUtility.UrlEncode(Resource.File));
    }

    public string ShortName 
    { 
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_3; } 
    }

    private string GetMajorNtaVersion()
    {
        return "2020.2";
    }

    // we do not use it right now, but in future maybe, so I didn't delete this code and this upgrade info file
    //public override string DetachURL
    //{
    //    get
    //    {            
    //        return "/Orion/TrafficAnalysis/Utils/DetailedUpgradeInfo.aspx";
    //    }
    //}        
}
