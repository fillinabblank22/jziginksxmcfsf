<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NetflowCollectorServices.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_NetflowCollectorServices" %>

<%@ Register TagPrefix="netflow" TagName="NetflowCollectorServices" Src="~/Orion/TrafficAnalysis/Admin/NetflowCollectorServices.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	    
	   <netflow:NetflowCollectorServices runat="server" />
        
     </Content>
</orion:resourceWrapper>
<br>