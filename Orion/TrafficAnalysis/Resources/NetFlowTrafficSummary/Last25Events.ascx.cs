using System;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.Events)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_Last25Events : BaseResourceControl
{
    private static IDALProvider _dalProvider = DALProvider.GetInstance();    

    protected void eventsRepeater_Init(object sender, EventArgs e)
    {
        if (eventsRepeater.DataSource == null)
        {
            bool includeAlerts = NetflowWebSettings.Instance.Last25EventsIncludeNetFlowAlerts;
            eventsRepeater.DataSource = _dalProvider.GetDAL<IEventsDAL>().GetEvents(rowLimit: 25, includeNetFlowAlerts: includeAlerts);
        }
        eventsRepeater.DataBind();    
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SolarWinds.Orion.Web.ControlHelper.AddStylesheet(this.Page, "/Orion/TrafficAnalysis/styles/Netflow.css"); 
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_TM0_21; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHSummaryLast25Events";
        }
    }

    public override ResourceLoadingMode ResourceLoadingMode
    {
        get
        {
            return ResourceLoadingMode.RenderControl;
        }
    }
}
