<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllNetflowSources.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_AllNetflowSources" %>

<asp:ScriptManagerProxy id="NTATreeScriptManager" runat="server">
    <services>
        <asp:ServiceReference path="/Orion/TrafficAnalysis/WebServices/AllNetflowSourcesNodeTree.asmx" />
    </services>
</asp:ScriptManagerProxy>

<orion:Include runat="server" File="../TrafficAnalysis/scripts/NetFlowSources.js" />

<orion:resourceWrapper runat="server"  ID="ResourceWrapper1" ShowManageButton="true" ShowEditButton="true" ManageButtonImage="/orion/trafficanalysis/images/button.resource.mngsrcs.gif" ManageButtonTarget="/orion/trafficanalysis/admin/AllNetflowSourcesEdit.aspx">
    <Content>
        <!-- All Netflow sources list control is shared with Admin part -->
        <script type="text/javascript">
            //<![CDATA[
            $(function () {
                var refresh = function () {
                    SW.NTA.NetflowSources.AllNetflowSourcesNodeTree.LoadRoot($('#<%=this.nodetree.ClientID%>'), '<%=this.Resource.ID %>','<%=ResourceWrapper1.ClientID %>');
                };          
                refresh();
             });
            //]]>
        </script>
        <div id="nodetree" runat="server" style="color:Black;font-size:11pt;">
        </div>
    </Content>
</orion:resourceWrapper>