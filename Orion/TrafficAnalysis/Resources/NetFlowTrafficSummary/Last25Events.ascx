<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Last25Events.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_Last25Events" %>

<orion:resourceWrapper ID="Wrapper" runat="server">
    <Content>
        <div id="EventsGridDiv">
            <asp:Repeater ID="eventsRepeater" OnInit="eventsRepeater_Init" runat="server">
                <HeaderTemplate>
                    <table width="100%" cellspacing="0" cellpadding="3" class="Events"> <!-- NeedsZebraStripes -->
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="width: 100px;"><%#Eval("EventTime", "{0:g}") %></td>
                        <td>
                            <div class="event-icon" style="background-color: #<%# Convert.ToInt32(Eval("BackColor")).ToString("X") %>;">
                                <img src='/Orion/TrafficAnalysis/images/Events/Event-<%#Eval("EventType")%>.gif' border="0" alt="Event Type">
                            </div>
                        </td>
                        <td>
                            <%#Eval("Message") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </Content>
</orion:resourceWrapper>
