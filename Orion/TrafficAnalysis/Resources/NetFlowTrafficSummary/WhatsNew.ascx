<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WhatsNew.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowTrafficSummary_WhatsNew" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="SolarWinds.Netflow.Common" %>
<%@ Import Namespace="SolarWinds.Netflow.Web" %>
<%@ Import Namespace="SolarWinds.Orion.Web.Helpers" %>
<link href="/Orion/TrafficAnalysis/styles/UpgradeContent.css" rel="stylesheet" type="text/css" />
<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <style type="text/css">
        .ULExtJSfix { margin-left:15px; padding-left:0px; }
        .bullet { padding-left: 64px;}
        .bullet a { color: #336699; }
        .bullet1 { background: url('<%= SolarWinds.Orion.Web.UI.Localizer.PathResolver.GetVirtualPath("TrafficAnalysis", "Icon_New.png") %>') no-repeat transparent; }
        </style>
        <div class="WhatsNewContainer1" style="line-height:17px;">
            <div class="bullet bullet1">
                <b><p><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_1_0%></p></b>
                <ul>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_1_1%></li>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_1_2%></li>
                </ul>
            </div>
            <div class="bullet">
                <b><p><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_2_0%></p></b>
                <ul>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_2_1%></li>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_2_2%></li>
                </ul>
            </div>
            <div class="bullet">
                <b><p><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_3_0%></p></b>
                <ul>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_3_1%></li>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_3_2%></li>
                </ul>
            </div>
            <div class="bullet">
                <b><p><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_4_0%></p></b>
                <ul>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_4_1%></li>
                    <li><%= Resources.NTAWebContent.NTAWEBDATA_WhatsNewResource_v20202_4_2%></li>
                </ul>
            </div>
        </div>
        <div class="sw-btn-bar-wizard">
            <orion:LocalizableButtonLink target="_blank" NavigateUrl="https://www.solarwinds.com/documentation/kbloader.aspx?kb=NTA_2020-2_release_notes" 
                runat="server" ID="btnLearnMore" LocalizedText="CustomText" Text="" DisplayType="Primary" />
            <orion:LocalizableButtonLink runat="server" ID="btnRemoveResource" LocalizedText="CustomText" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_TM0_14%>" DisplayType="Secondary" />
        </div>
    </Content>
</orion:resourceWrapper>
