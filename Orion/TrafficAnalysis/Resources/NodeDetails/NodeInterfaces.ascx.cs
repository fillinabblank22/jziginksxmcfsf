using System;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Web.DALInterfaces;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class NetflowNodeInterfaces : BaseResourceControl
{
    protected override string DefaultTitle
    {
        get 
        { 
            return Resources.NTAWebContent.NTAWEBCODE_AK0_28; 
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHNodeDetailsInterfaces";
        }
    }

    public override System.Collections.Generic.IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INetflowNodeProvider) };
        }
    }

    public static IAllNetFlowSourcesDAL AllNetflowSourcesDAL
    {
        get { return _allNetFlowSourcesDAL; }
        set { _allNetFlowSourcesDAL = value; }
    }

        private static IAllNetFlowSourcesDAL _allNetFlowSourcesDAL = new AllNetflowSourcesDAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string nodeID = this.GetInterfaceInstance<INodeProvider>().Node.NodeID.ToString();
            interfacesRepeater.DataSource = AllNetflowSourcesDAL.GetRouterInterfaces(nodeID);
            interfacesRepeater.DataBind();
        }
        catch (Exception ex)
        {
            new SolarWinds.Logging.Log().Error(ex);
        }
    }
  
}
