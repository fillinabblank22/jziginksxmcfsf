<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeInterfaces.ascx.cs" Inherits="NetflowNodeInterfaces" %>

<%@ Register TagPrefix="orion" TagName="SmallStatusIcon" Src="~/Orion/Controls/SmallStatusIcon.ascx"  %>

<orion:resourceWrapper runat="server" ID="Wrapper">
	<Content>
	
        <asp:Repeater Runat="server" ID="interfacesRepeater" EnableViewState="false">
          <HeaderTemplate>
          <table border="0" cellpadding="2" cellspacing="0" class="NeedsZebraStripes">
            <thead>
                <tr>
                    <td><%= Resources.NTAWebContent.NTAWEBDATA_AK0_25%></td>
	                <td><%= Resources.NTAWebContent.NTAWEBDATA_TM0_31%></td>
	                <td>&nbsp;</td>
	                <td><%= Resources.NTAWebContent.NTAWEBDATA_AK0_26%></td>
	                <td><%= Resources.NTAWebContent.NTAWEBDATA_AK0_27%></td>
	                <td>&nbsp;</td>
                </tr>
            </thead>
          </HeaderTemplate>
          <ItemTemplate>
            <tr>
			    <td valign="middle"> <orion:SmallStatusIcon runat="server" StatusValue='<%#Eval("Status")%>' /></td>
                <td>
                    <img src='<%# string.Format("/NetPerfMon/images/Interfaces/{0}.gif", Eval("[InterfaceType]")) %>' alt="" />
                    <%# SolarWinds.Netflow.Web.UIHelper.BuildInterfaceLink(
                        DataBinder.Eval(Container.DataItem, "InterfaceID"), 
                        SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(DataBinder.Eval(Container.DataItem, "Caption").ToString()))
                    %>
                </td>
                <td>&nbsp;</td>
                <td><%#Eval("InBps")%>&nbsp;</td>
			    <td><%#Eval("OutBps")%>&nbsp;</td>
            </tr>
           </ItemTemplate>
           
           <FooterTemplate>
           </table> 
           </FooterTemplate>
        </asp:Repeater>
        
     </Content>
</orion:resourceWrapper>
	    