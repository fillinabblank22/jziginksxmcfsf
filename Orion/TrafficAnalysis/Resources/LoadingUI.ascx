<%@ Control Language="C#" ClassName="LoadingUI" %>
<%@ Implements Interface="SolarWinds.Netflow.Web.UI.ILoadingUI" %>

<script runat="server">
    public event EventHandler Cancel;
    public event EventHandler CancelAll;

    public void SetCancelled()
    {
        this.phCancelMessage.Visible = true;
        this.divLoadingUI.Visible = false;
    }
    
    protected void CancelClick(object sender, EventArgs e)
    {
        if (null != this.Cancel)
            this.Cancel(this, new EventArgs());
    }

    protected void CancelAllClick(object sender, EventArgs e)
    {
        if (null != this.CancelAll)
            this.CancelAll(this, new EventArgs());
    }

    public void AddStyle(string key, string value)
    {
        divLoadingUI.Style.Add(key, value);
    }

    public void EnableProgressbar(bool enable, float percent)
    {
        divProgressUI.Visible = enable;
        divProgressUIpart.Style.Add("width", percent.ToString() + "%");                
    }

    public int ReportWidth { get; set; }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        header.Style.Add(HtmlTextWriterStyle.Padding, "2px 0px 0px 0px");
        header.Style.Add(HtmlTextWriterStyle.FontSize, "18px");
        header.Style.Add(HtmlTextWriterStyle.Margin, "0px 0px 0px " + (this.ReportWidth / 2 - 70).ToString() + "px");
        header.Style.Add(HtmlTextWriterStyle.Color, "#717171"); 
        header.Style.Add(HtmlTextWriterStyle.Width, "150px"); 
        header.Style.Add(HtmlTextWriterStyle.VerticalAlign, "middle");

        divLoadingUI.Style.Add(HtmlTextWriterStyle.Position, "relative");
        divLoadingUI.Style.Add(HtmlTextWriterStyle.BackgroundColor, "transparent");
        divLoadingUI.Style.Add(HtmlTextWriterStyle.Height, "70px");

        divProgressUI.Style.Add(HtmlTextWriterStyle.Width, "60px");
        divProgressUI.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
        divProgressUI.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");
        divProgressUI.Style.Add(HtmlTextWriterStyle.BorderColor, "black");
        divProgressUI.Style.Add(HtmlTextWriterStyle.TextAlign, "left");
        divProgressUI.Style.Add(HtmlTextWriterStyle.Padding, "0px");
        divProgressUI.Style.Add(HtmlTextWriterStyle.Margin, "1px 0px 0px 30px");

        divProgressUIpart.Style.Add(HtmlTextWriterStyle.FontSize, "0px");
        divProgressUIpart.Style.Add(HtmlTextWriterStyle.Height, "5px");
        divProgressUIpart.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#717171");
    }
    
</script>

<div id="divLoadingUI" runat="server" class="DefaultShading">
    <div id="header" runat="server">
        <img src="/Orion/images/loading_gen_small.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_36%>" />
        <span style="white-space:nowrap;"><b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_36%></b></span>
        <div runat="server" id="divProgressUI" visible="false">
            <div runat="server" id="divProgressUIpart"></div>                    
        </div>    
        
        <div style="padding: 2px 0px 0px 0px;white-space:nowrap;">
            <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Small" OnClick="CancelClick" />
            <orion:LocalizableButton runat="server" ID="btnCancelAll" LocalizedText="CustomText" Text="<%$ Resources: NTAWebContent, NTAWEBDATA_VB0_39%>" DisplayType="Small" OnClick="CancelAllClick" />
        </div>    
    </div>        
</div>    

<asp:PlaceHolder runat="server" ID="phCancelMessage" Visible="false">
    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_38%>
    <br />
    <br />
    <span class="DefaultShading" style="font-size: x-small;">
        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_37%>
    </span>
    <br />
</asp:PlaceHolder>