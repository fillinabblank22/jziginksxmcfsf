﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EndpointDetails.ascx.cs"
    Inherits="Orion_TrafficAnalysis_Resources_NetFlowEndpoint_EndpointDetails" %>
<%@ Register TagPrefix="netflow" TagName="IntegrationIcons" Src="~/Orion/TrafficAnalysis/Utils/IntegrationIcons.ascx" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>
<%@ Register TagPrefix="netflow" TagName="DnsInfo" Src="~/Orion/TrafficAnalysis/Controls/DnsInfo.ascx" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy" runat="server">
    <Services>
        <asp:ServiceReference path="~/Orion/TrafficAnalysis/WebServices/DnsResolverService.asmx"  />
    </Services>
</asp:ScriptManagerProxy>

<orion:resourcewrapper runat="server" id="Wrapper">
	<Content>
    <script type="text/javascript">

        var canCall = true; 
    
        function lookupBtnClick() {
            if (canCall) {
                canCall = false;
                var lookupProgress = document.getElementById('lookupProgress');
                if (lookupProgress != null)
                    lookupProgress.style.display = 'block';

                DnsResolverService.LookupHostname('<%=IP%>', callBackFunction);
            }
        }

    function callBackFunction(result) {
        canCall = true; 
        var lookupProgress = document.getElementById('lookupProgress');
        if (lookupProgress != null)
            lookupProgress.style.display = 'none';
            
     if (!result)
        {
            alert('<%= ControlHelper.EncodeJsString(Resources.NTAWebContent.NTAWEB_JS_CODE_VB0_1)%>');
            return false;
        }
        else
            window.location.reload(false);
    }
    </script>
    <style type="text/css">
        .IntegIconDIV1 { margin-top: -27px; width: 100%; text-align:right; margin-bottom:20px; _margin-top:-30px; }
        .IntegIconDIV2 { margin-right:85px; }
    </style>
    
        <%if (Profile.ToolsetIntegration){%>    
        <div class="IntegIconDIV1">
            <div class="IntegIconDIV2">
            <netflow:IntegrationIcons runat="server" ID="integrationIcons" OnLoad="IntegrationIconsOnLoad" Visible="false"/>
            </div>
        </div>
        <%}%>
        
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <asp:Repeater runat="server" ID="ipAddressesRepeater" DataSource="<%# this.GetIpAddressesWithLinkAndIcon() %>" OnItemDataBound="ipAddressRepeater_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_25%></th>
                        <td><asp:Literal runat="server" ID="ipAddressIcon" /></td>
                        <td colspan="2">
                            <a id="IPAddress" runat="server"></a>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <th class="sw-btn-bar">
                    <%= Resources.NTAWebContent.NTAWEBDATA_VB0_24%>&nbsp;
                    <% if(Profile.AllowAdmin) {%>
                        <orion:LocalizableButtonLink ID="editBtn" LocalizedText="Edit" DisplayType="Small" NavigateUrl='<%# String.Format("{0}&Address={1}&Hostname={2}",EditHostnameLink,IP,System.Web.HttpUtility.UrlEncode(DNS))%>' runat="server"/>
                    <%}%>
                    <orion:LocalizableButton ID="lookupBtn" OnClientClick="lookupBtnClick();return false;" runat="server" DisplayType="Small" LocalizedText="CustomText" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_41%>" />
                </th>
                <td>&nbsp;</td>
                <td>
                    <netflow:DnsInfo runat="server" ID="DnsInfo" />                    
                </td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_22%></th>
                <td><%= IPAddressGroupIconHtml%></td>
                <td><a href="<%= IPAddressGroupLink %>"><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(IPAddressGroupName)%></a>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_26%></th>
                <td><%= DomainIconHtml%></td>
                <td><a href="<%= this.DomainLink %>"><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Domain)%></a>&nbsp;</td>
            </tr>
            <asp:Repeater runat="server" ID="countriesRepeater" DataSource="<%# this.GetCountries() %>" OnItemDataBound="countriesRepeater_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_18%></th>
                        <td><asp:Literal runat="server" ID="countryIcon" /></td>
                        <td><a runat="server" ID="countryLink"></a>&nbsp;</td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_27%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficTxBytes%>&nbsp; <%=Period%></td>
            </tr>
            <tr>
				<th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_28%></th>
				<td>&nbsp;</td>
				<td><%=TotalTrafficRxBytes%>&nbsp; <%=Period%></td>
            </tr>
            <tr runat="server" id="WirelessControllerRow">
	            <th><%= Resources.NTAWebContent.WirelessController%></th>
	            <td>&nbsp;</td>
	            <td>
	                <a href="<%=WirelessControllerLink%>">
	                    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(WirelessControllerName)%>
	                </a>
	            </td>
	        </tr>
            <tr runat="server" id="WirelessAccessPointRow">
	            <th><%= Resources.NTAWebContent.WirelessAccessPoint%></th>
	            <td>&nbsp;</td>
	            <td>
	                <a href="<%=WirelessAccessPointLink%>">
	                    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(WirelessAccessPointName)%>
	                </a>
	            </td>
	        </tr>
        </table>
        <div id="lookupProgress" style="display:none; font-family:Arial; font-size: 8pt;">
	        <img src="/Orion/images/loading_gen_small.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_30%>" />
	        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_29%>
	    </div>

        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourcewrapper>
