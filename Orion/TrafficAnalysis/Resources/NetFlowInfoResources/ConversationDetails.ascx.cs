using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using SolarWinds.Data.Utility;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowConversation_ConversationDetails : InfoResourceBase
{
    protected NetflowConversation netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_23; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHConversationTrafficHistory"; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowConversationProvider) }; }
    }

    private readonly string[] Colors = {
            "#FF0000", //0
            "#009900", //1
            "#0000FF", //2
            "#990000", //3
            "#009999", //4
            "#ff9966", //5
            "#c800c8", //6 
            "#ff6666", //7
            "#33c833", //8
            "#9999ff", //9
            "#999900", //10
            "#c866c8", //11
            "#666666", //12
            "#996633", //13
            "#666600"  //14
        };

    private bool isDest;    // used for determine Source / Dest string
    
    /// <summary>
    /// used for determine Source / Dest string
    /// </summary>
    private string Direction
    {
        get
        {
            return isDest ? "Dest" : "Source";
        }
    }

   
    /// <summary>
    /// Render application link. 
    /// </summary>
    /// <param name="dataItem"></param>
    /// <param name="isDestination">used for determine Source / Dest string</param>
    /// <returns></returns>
    public string RenderContent(object dataItem, bool isDestination)
    {
        this.isDest = isDestination;

        string format = @"<a style='color: {0};' href='{1}'>{2}</a>";
        string str = string.Empty;

        int protID;
        if (Int32.TryParse(DataBinder.Eval(dataItem, "[ProtocolNumber]").ToString(), out protID)
            && (protID != 6 && protID != 17))
        {
            return Resources.NTAWebContent.NTAWEBCODE_VB0_27; // and colour is black as default
        }

        // {0} = Application Color, {1} = Application Link, {2} = Application Name
        str = string.Format(format, this.GetApplicationColor(DataBinder.Eval(dataItem, string.Format("[{0}Port]", Direction))), // {0} - Application Color
            this.GetApplicationLink(DataBinder.Eval(dataItem, string.Format("[{0}AppID]", Direction)), // {1} - Application Link
            DataBinder.Eval(dataItem, string.Format("[{0}Port]", Direction))),

            // {2} - Application Name
            this.BuildAppName(DataBinder.Eval(dataItem, string.Format("[{0}Port]", Direction)),
            DataBinder.Eval(dataItem, string.Format("[{0}PortName]", Direction)),
            DataBinder.Eval(dataItem, string.Format("[{0}SinglePort]", Direction))));
        
        return str;
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        netflowObj = GetInterfaceInstance<INetflowConversationProvider>().NetflowConversation;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();
        
        rptDetails.DataSource = netflowObj.GetConversationDetails();
        rptDetails.DataBind();
    }    

    protected string GetEndpointLink(object hostname)
    {
        return LinkBuilder.GetLink(ViewType.NetflowEndpoint, "E", hostname, netflowObj.Filters);
    }

    protected string GetProtocolLink(object protocolNumber)
    {
        return LinkBuilder.GetLink(ViewType.Protocol, "P", protocolNumber, netflowObj.Filters);
    }

    protected string GetApplicationLink(object appID, object portNumber)
    {
        object obj = appID;
        
        if (appID is DBNull || (appID is string && appID as string == string.Empty))
        {
            obj = portNumber;
        }

        return LinkBuilder.GetLink(ViewType.Protocol, "A", obj, netflowObj.Filters);
    }

    protected string GetProtocolColor(object protocolNumber)
    {
        return this.Colors[Convert.ToInt64(protocolNumber).GetHashCode() % this.Colors.Length];
    }

    protected string GetApplicationColor(object portNumber)
    {
        return this.Colors[(Convert.ToInt64(portNumber).GetHashCode() + 1) % this.Colors.Length];
    }

    /// <summary>
    /// Application Name
    /// </summary>
    /// <param name="port"></param>
    /// <param name="portName"></param>
    /// <param name="singlePort"></param>
    /// <returns></returns>
    protected string BuildAppName(object port, object portName, object singlePort)
    {
        int portInt = Convert.ToInt32(port);
        
        // random high
        if (portInt == -2)
        {
            return Resources.NTAWebContent.NTAWEBCODE_VB0_24;
        }

        // port 0
        if (portInt == 0)
        {
            return Resources.NTAWebContent.NTAWEBCODE_Port_0;
        }

        int single = Convert.ToInt32(singlePort);

        return string.Format("{0} ({1})", portName, single == -3 ? Resources.NTAWebContent.NTAWEBCODE_VB0_26 : singlePort);        
    }
}
