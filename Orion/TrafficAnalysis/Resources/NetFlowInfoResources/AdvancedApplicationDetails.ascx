<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdvancedApplicationDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowApplication_AdvancedApplicationDetails" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
    
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_AdvancedApplicationDetails%></th>
                <td><%=IconHtml%></td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AdvancedApplication)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_AdvancedApplicationCategory%></th>
                <td>&nbsp;</td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AdvancedApplicationCategory)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_AdvancedApplicationSubCategory%></th>
                <td>&nbsp;</td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AdvancedApplicationSubCategory)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_AdvancedApplicationGroup%></th>
                <td>&nbsp;</td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(AdvancedApplicationGroup)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_7%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficBytes%>&nbsp;<%=Period%></td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_9%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficPackets%> &nbsp;<%=Period%></td>
            </tr>
        </table>
        
        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>