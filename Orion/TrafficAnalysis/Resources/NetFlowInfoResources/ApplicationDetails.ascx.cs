using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Contracts.Applications;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowApplication_ApplicationDetails : InfoResourceBase
{
    private NetflowApplication netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_15; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    protected new string Application
    {
        get
        {
            return netflowObj.Name;
        }
    }

    public string Protocol
    {
        get
        {
            return netflowObj.Protocol;
        }
    }

    public string IPGroupRows
    {
        get
        {
            if (netflowObj.HasGroups == true)
            {
                var outputStr = new StringBuilder();

                outputStr.Append("<tr>");
                outputStr.AppendFormat("<th>{0}</th>",Resources.NTAWebContent.NTAWEBCODE_VB0_16);
                outputStr.Append("<td>&nbsp;</td>");
                outputStr.AppendFormat("<td>{0}</td>", IPAddresses(ApplicationProperties.ConditionDirection.Source));
                outputStr.Append("</tr>");

                outputStr.Append("<tr>");
                outputStr.AppendFormat("<th>{0}</th>",Resources.NTAWebContent.NTAWEBCODE_VB0_17);
                outputStr.Append("<td>&nbsp;</td>");
                outputStr.AppendFormat("<td>{0}</td>", IPAddresses(ApplicationProperties.ConditionDirection.Destination));
                outputStr.Append("</tr>");

                return outputStr.ToString();
            }

            return string.Empty;
        }
    }

    private string IPAddresses(ApplicationProperties.ConditionDirection pDirection)
    {
        return this.netflowObj.IPAddresses(pDirection);
    }


    /// <summary>
    /// Returns HTML code for Port and Port number
    /// In the case that port is unmonitored/other/-1 no row is displayed
    /// </summary>
    public string PortsRow
    {
        get
        {
            //string outputStr=string.Empty;
            var outputStr = new StringBuilder();

            if (netflowObj.IsUnmonitored == true)
            {
                return outputStr.ToString();
            }

            outputStr.Append("<tr>");
            outputStr.AppendFormat("<th>{0}</th>", PortPropertyName);
            outputStr.Append("<td>&nbsp;</td>");
            outputStr.AppendFormat("<td>{0}&nbsp;{1}</td>",Ports,Protocol);
            outputStr.Append("</tr>");
            return outputStr.ToString();
        }
    }

    protected string PortPropertyName
    {
        get
        {
            return (netflowObj.IsMultiPort == false) ? Resources.NTAWebContent.NTAWEBCODE_VB0_19 : Resources.NTAWebContent.NTAWEBCODE_VB0_20;
        }
    }

    protected string Ports
    {
        get
        {
            return netflowObj.PortRanges;
        }
    }

    protected string TotalTrafficBytes
    {
        get { return netflowObj.GetTotalBytes(true, true); }
    }

    protected string TotalTrafficPackets
    {
        get { return netflowObj.GetTotalPackets(true, true); }
    }

    protected string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new Type[] { typeof(INetflowApplicationProvider) };
        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        this.netflowObj = this.GetInterfaceInstance<INetflowApplicationProvider>().NetflowApplication;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        base.OnInit(e);
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHApplicationDetails";
        }
    }

    protected override NetflowUpdateQueryType[] AdditionalUpdateTypes
    {
        get { return new[] {NetflowUpdateQueryType.Applications}; }
    }
}
