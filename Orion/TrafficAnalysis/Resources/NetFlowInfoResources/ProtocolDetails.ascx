<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProtocolDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowProtocol_ProtocolDetails" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_5%></th>
                <td><%=IconHtml%></td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(ProtocolName)%>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_6%></th>
                <td>&nbsp;</td>
                <td><%= String.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_14,ProtocolNumber)%>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_7%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficBytes%>&nbsp; <%=Period%></td>
            </tr>
        </table>

        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>
