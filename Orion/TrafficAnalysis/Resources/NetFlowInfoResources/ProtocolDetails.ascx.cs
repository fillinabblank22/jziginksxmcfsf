using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web;
using System.Collections.Generic;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowProtocol_ProtocolDetails : InfoResourceBase
{
    private NetflowProtocol netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_13; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    public string ProtocolName
    {
        get { return netflowObj.Name; }
    }

    public string ProtocolNumber
    {
        get { return netflowObj.ProtocolNumber.ToString(); }
    }

    public string TotalTrafficBytes
    {
        get { return netflowObj.TotalTrafficBytes; }
    }
    public string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHProtocolDetails";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowProtocolProvider) }; }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowObj = this.GetInterfaceInstance<INetflowProtocolProvider>().NetflowProtocol;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        base.OnInit(e);
    }
}
