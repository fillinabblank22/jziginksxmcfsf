using System;
using System.Collections.Generic;
using Resources;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowApplication_AdvancedApplicationDetails : InfoResourceBase
{
    private NetflowAdvancedApplication netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return NTAWebContent.NTAWEBCODE_AdvancedApplicationDetails_ResourceTitle; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    protected string AdvancedApplication
    {
        get { return netflowObj.Name; }
    }

    protected string AdvancedApplicationCategory
    {
        get { return netflowObj.Category; }
    }

    protected string AdvancedApplicationSubCategory
    {
        get { return netflowObj.SubCategory; }
    }

    protected string AdvancedApplicationGroup
    {
        get { return netflowObj.ApplicationGroup; }
    }

    protected string TotalTrafficBytes
    {
        get { return netflowObj.GetTotalBytes(true, true); }
    }

    protected string TotalTrafficPackets
    {
        get { return netflowObj.GetTotalPackets(true, true); }
    }

    protected string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get
        {
            return new[] { typeof(INetflowAdvancedApplicationProvider) };
        }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowObj = this.GetInterfaceInstance<INetflowAdvancedApplicationProvider>().NetflowAdvancedApplication;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        OrionInclude.ModuleFile("TrafficAnalysis", NetflowAdvancedApplication.IconCssFileName);

        base.OnInit(e);
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHAdvancedApplicationDetails";
        }
    }
}
