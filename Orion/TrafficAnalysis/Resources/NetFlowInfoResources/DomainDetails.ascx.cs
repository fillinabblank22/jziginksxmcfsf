using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web;
using System.Collections.Generic;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowDomain_DomainDetails : InfoResourceBase
{
    private NetflowDomain netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_29; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    public string Domain
    {
        get { return netflowObj.Domain; }
    }

    public string TotalTrafficBytes
    {
        get { return netflowObj.GetTotalBytes(true, true); }
    }

    public string TotalTrafficPackets
    {
        get { return netflowObj.GetTotalPackets(true, true); }
    }

    public string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHDomainDetails";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowDomainProvider) }; }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowObj = this.GetInterfaceInstance<INetflowDomainProvider>().NetflowDomain;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        base.OnInit(e);
    }
}
