<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPAddressGroupDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowIPAddressGroup_IPAddressGroupDetails" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_22%></th>
                <td><%=IconHtml%></td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(IPAddressGroupName)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_23%></th>
                <td>&nbsp;</td>
                <td>
                <asp:Repeater runat="server" ID="rptRanges">
                    <ItemTemplate>
                    <%# Container.DataItem.ToString() %> <br />
                    </ItemTemplate>
                </asp:Repeater>                
                </td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_7%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficBytes%>&nbsp; <%=Period %></td>
            </tr>

        </table>
        
        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>