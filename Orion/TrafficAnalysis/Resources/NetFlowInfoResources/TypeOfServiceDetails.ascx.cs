using System;
using SolarWinds.Netflow.Web;
using System.Collections.Generic;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowTypesOfService_TypesOfServiceDetails : InfoResourceBase
{
    private NetflowTypeOfService netflowObj;

    #region Properties

    public override string HelpLinkFragment
    {
        get { return "OrionNetFlowPHTypesServiceDetails"; }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_30; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    public string ToSName
    {
        get { return netflowObj.Name; }
    }

    public string TotalTrafficBytes
    {
        get { return netflowObj.TotalTrafficBytes; }
    }

    public string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowTypeOfServiceProvider) }; }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowObj = this.GetInterfaceInstance<INetflowTypeOfServiceProvider>().NetFlowTypeOfService;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        base.OnInit(e);
    }
}
