<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopXXConversationEndpoints.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowEndpoint_TopXXConversations" EnableViewState="false" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>  
        <table class="NeedsZebraStripes" id="dataTable_<%=this.ClientID %>" border="0" cellpadding="3" cellspacing="0" width="100%" style="display: none;">
		    <thead>
                <tr>
                    <td>&nbsp;</td>
                    <td><%= Resources.NTAWebContent.NTAWEBDATA_VB0_31%></td>
                    <td>&nbsp;</td>
                    <td colspan="3"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_32%></td>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
        </table>       
        <img id="loaderImage" runat="server" alt="" src="/Orion/images/AJAX-Loader.gif" style="margin:0 auto; display: block;"/>

        <script type="text/javascript">
            $(document).ready(function () {
                var renderDataTable  = function(params, result) {
                    var html = "";
                    $.each(result, function (i, row) {
                        var endpoint = ''.concat(
                            '<td>' +
                                '<a href="' + row.EndpointLink + '">', row.IpAddressIconHtml, '</a>' +
                            '</td>' +
                            '<td>' +
                                '<a href="' + row.EndpointLink + '">' + row.Hostname +
                            '</a>' +
                            '</td>');

                        var conversation =
                            '<td>' +
                                '<a href="' + row.ConversationLink + '">' +
                                    '<img src="/Orion/TrafficAnalysis/images/Button.Conversation.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_33%>" />' +
                                '</a>' +
                            '</td>' +
                            '<td>' +
                                '<a href="' + row.ConversationLink + '">' + row.Bytes + '</a>' +
                            '</td>' +
                            '<td>' +
                                '<a href="' + row.ConversationLink + '">' +
                                    '<img src="/Orion/TrafficAnalysis/images/Bar.Green.gif" alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB0_33%>" width="' + row.BytesBarWidth + '" height="12" />' +
                                '</a>' +
                            '</td>' +
                            '<td>' +
                                '<a href="' + row.ConversationLink + '">' + row.BytesPercent + "%" + '</a>' +
                            '</td>';

                        html = html.concat('<tr>', endpoint, conversation, '</tr>');
                    });

                    if (html !== "") {
                        $("#" + params.dataTableId + " tbody").append(html);
                    }
                    $("#" + params.loaderId).hide();
                    $("#" + params.dataTableId).show();
                }

                var renderError = function(loaderId, error) {
                    $("#" + loaderId).hide().after("<div>Server error: " + error.status + "-" + error.statusText + "</div>");
                }

                var refresh = function() {
                    var params = {
                        loaderId: "<%= loaderImage.ClientID %>",
                        // server control doesn't have thead, so using client control table with unique id
                        dataTableId: "dataTable_<%=this.ClientID %>"
                    };
                    <% if (this.InlineRendering) { %>
                    renderDataTable(params, <%= this.SerializedData %>);
                    <% } else { %>
                    $("#" + params.loaderId).show();
                    $("#" + params.dataTableId).hide();
                    $.ajax({
                        type: "POST",
                        url: "/Orion/TrafficAnalysis/WebServices/ResourceService.asmx/TopConversations",
                        data: "{ endpointIpAddress: \"<%= this.Endpoint.IPAddress %>\", filterId: \"<%= this.Endpoint.FilterID %>\", maxMessages: \"<%= this.MaxMessages %>\" }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(result) {
                            renderDataTable(params, result.d);
                        },
                        error: function(error) {
                            renderError("<%= loaderImage.ClientID %>", error);
                        }
                    });
                    <% } %>
                };

                if(SW.Core && SW.Core.View && SW.Core.View.AddOnRefresh) {
                    SW.Core.View.AddOnRefresh(refresh, '<%=this.ClientID %>');
                }
                refresh();
            });
        </script>
    </Content>
</orion:resourceWrapper>