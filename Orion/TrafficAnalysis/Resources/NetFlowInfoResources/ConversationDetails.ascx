<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConversationDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowConversation_ConversationDetails" %>
<%@ Import Namespace="SolarWinds.Orion.Web.DisplayTypes" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>
<%@ Register TagPrefix="netflow" TagName="DnsInfo" Src="~/Orion/TrafficAnalysis/Controls/DnsInfo.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        <asp:Repeater runat="server" ID="rptDetails">
            <HeaderTemplate>
                <table cellpadding="3" cellspacing="0" id="detailTable" class="NeedsZebraStripes">
                    <thead>
                        <tr>
                            <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_15%></th>
                            <td>&nbsp;</td>
                            <th>
                                <a href='<%# this.GetEndpointLink(this.netflowObj.Hostname1) %>&From=<%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.netflowObj.Hostname1) %>'>
                                    <netflow:DnsInfo runat="server" HostName=<%# this.netflowObj.Hostname1 %> />
                                </a> 
                            </th>
                            <td><img src="/Orion/TrafficAnalysis/images/Direction.Both.gif" alt="" /></td>
                            <th>
                                <a href='<%# this.GetEndpointLink(this.netflowObj.Hostname2) %>&From=<%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.netflowObj.Hostname2) %>'>
                                    <netflow:DnsInfo runat="server" HostName=<%# this.netflowObj.Hostname2 %> />
                                </a> 
                            </th>
                            <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_16%></th>
                            <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_17%></th>
                        </tr>    
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# ((DateTime)Eval("[DateTime]")).ToLocalTime() %></td>
                    <td>
                        <a  style='color: <%# this.GetProtocolColor(Eval("[ProtocolNumber]")) %>;'
                            href='<%# this.GetProtocolLink(Eval("[ProtocolNumber]")) %>'>
                            <%# SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Eval("[ProtocolName]").ToString()) %>
                        </a>
                    </td>
                    
                    <asp:PlaceHolder runat="server" ID="SourceIsEndpoint1" Visible='<%# Eval("[SourceIP]").Equals(this.netflowObj.IPAddress1) %>'>
                        <td>
                            <%#RenderContent(Container.DataItem, false)%>
                        </td>
                        <td><img src="/Orion/TrafficAnalysis/images/Direction.Right.gif" alt="" /></td>
                        <td>
                           <%#RenderContent(Container.DataItem, true)%>
                        </td>
                    </asp:PlaceHolder>
                    
                    <asp:PlaceHolder runat="server" ID="DestIsEndpoint1" Visible='<%# Eval("[DestinationIP]").Equals(this.netflowObj.IPAddress1) %>'>
                        <td>
                            <%#RenderContent(Container.DataItem, true)%>
                        </td>
                        <td><img src="/Orion/TrafficAnalysis/images/Direction.Left.gif" alt="" /></td>
                        <td>
                            <%#RenderContent(Container.DataItem, false)%>
                        </td>
                    </asp:PlaceHolder>
                    
                    <td><%# new Bytes(Eval("[TotalBytes]")).ToString() %></td>
                    <td><%# new NTAPackets(Eval("[TotalPackets]")).ToString() %></td>
                </tr>
            </ItemTemplate>
            
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        
        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>