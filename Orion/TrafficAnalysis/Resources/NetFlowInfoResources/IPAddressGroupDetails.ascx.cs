using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SolarWinds.Netflow.Web;
using System.Collections.Generic;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowIPAddressGroup_IPAddressGroupDetails : InfoResourceBase
{
    private NetflowIPAddressGroup netflowObj;

    #region Properties

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_32; }
    }

    protected override NetflowObject NetflowObject
    {
        get { return netflowObj; }
    }

    public string IPAddressGroupName
    {
        get { return netflowObj.GroupName; }
    }
    
    public string TotalTrafficBytes
    {
        get { return netflowObj.GetTotalBytes(true, true); }
    }

    public string Period
    {
        get { return netflowObj.PeriodNameTitle; }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHIPAddressGroupDetails";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowIPAddressGroupProvider) }; }
    }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowObj = this.GetInterfaceInstance<INetflowIPAddressGroupProvider>().NetflowIPAddressGroup;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowObj.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowObj.Filters.FilterIdWithAbsoluteTimePeriodFilter();

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        Resource.Title = DefaultTitle;

        if (!IsPostBack)
        {
            rptRanges.DataSource = netflowObj.GroupRanges;
            rptRanges.DataBind();
        }

        base.OnLoad(e);
    }

    protected override NetflowUpdateQueryType[] AdditionalUpdateTypes
    {
        get { return new[] {NetflowUpdateQueryType.IPGroups}; }
    }
}
