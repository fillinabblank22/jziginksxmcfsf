<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ASDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowProtocol_ASDetails" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_10%></th>
                <td><%=IconHtml%></td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(ASName)%>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_11%></th>
                <td>&nbsp;</td>
                <td><%=ASNumber%>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_12%></th>
                <td>&nbsp;</td>
                <td><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(ASOrganization)%>&nbsp;</td>
            </tr>

            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_13%></th>
                <td>&nbsp;</td>
                <td><%=ASRegistrationDate%>&nbsp;</td>
            </tr>

            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_14%></th>
                <td>&nbsp;</td>
                <td><%=ASLastUpdated%>&nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_7%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficBytes%>&nbsp; <%=Period%></td>
            </tr>
             <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_9%></th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficPackets%>&nbsp; <%=Period%></td>
            </tr>
        </table>

        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>
