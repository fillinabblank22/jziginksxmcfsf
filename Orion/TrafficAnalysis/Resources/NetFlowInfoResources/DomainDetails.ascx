<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DomainDetails.ascx.cs" Inherits="Orion_TrafficAnalysis_Resources_NetFlowDomain_DomainDetails" %>
<%@ Register TagPrefix="netflow" TagName="InaccurateDataWarning" Src="~/Orion/TrafficAnalysis/Utils/InaccurateDataWarning.ascx" %>

<orion:resourceWrapper runat="server" ID="Wrapper">
    <Content>
        
        <table border="0" cellpadding="2" cellspacing="0" width="100%" id="detailTable" class="NeedsZebraStripes">
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_20%></th>
                <td class="Property"><%=IconHtml%></td>
                <td class="Property"><%=SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(Domain)%> &nbsp;</td>
            </tr>
            <tr>
                <th><%= Resources.NTAWebContent.NTAWEBDATA_VB0_7%> </th>
                <td>&nbsp;</td>
                <td><%=TotalTrafficBytes%>&nbsp; <%=Period%></td>
            </tr>
        </table>
        
        <netflow:InaccurateDataWarning runat="server" ID="warningMessage"/>
    </Content>
</orion:resourceWrapper>