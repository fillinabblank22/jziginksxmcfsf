using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DataConverters;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Reporting.DNS;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;
using SolarWinds.Orion.Web.UI;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowEndpoint_TopXXConversations : BaseResourceControl
{
    private static readonly Log log = new Log();
    private static readonly JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

    private NetflowEndpoint endpoint = null;

    protected NetflowEndpoint Endpoint
    {
        get
        {
            if (null == this.endpoint)
            {
                endpoint = this.GetInterfaceInstance<INetflowEndpointProvider>().NetflowEndpoint; 
            }
            
            return endpoint;
        }
    }

    protected int MaxMessages
    {
        get
        {
            return this.GetIntProperty("MaxMessages", 5);
        }
    }

    protected bool InlineRendering
    {
        get;
        private set;
    }

    protected string SerializedData { get; private set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        bool inlineRendering;
        bool.TryParse(Request.QueryString.Get("InlineRendering"), out inlineRendering);
        InlineRendering = inlineRendering;

        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(Endpoint.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = Endpoint.Filters.FilterIdWithAbsoluteTimePeriodFilter();
    }

    private static string ResolveHostname(string hostname, string ipAddress)
    {
        return OnDemandDns.Instance.Resolve(hostname, ipAddress).ToString();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (InlineRendering)
        {
            var filters = this.Endpoint.Filters;

            var conversations = new ConversationsDAL().GetReport(filters, this.MaxMessages, true, true, false, false, false).TopConversations;

            var data = ConversationEndpointsConverter.ConvertTopConversationsDataTable(this.Endpoint.IPAddress, this.Endpoint.FilterID, conversations, ResolveHostname);

            SerializedData = javaScriptSerializer.Serialize(data);
        }
    }

    protected override string DefaultTitle
    {
        get { return Resources.NTAWebContent.NTAWEBCODE_VB0_33; }
    }

    public sealed override string DisplayTitle
    {
        get
        {
            return String.Format(Resources.NTAWebContent.NTAWEBCODE_PN0_01, this.MaxMessages);
        }
    }

    public override string HelpLinkFragment
    {
        get
        {
            return "OrionNetFlowPHEndpointTopConversations";
        }
    }

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowEndpointProvider) }; }
    }

    public override string EditURL
    {
        get
        {
            return UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Editors/TopXXConversationEdit.aspx", Resource.ID);
        }
    }

    /// <summary>
    /// All current info resources are affected by Netflow Sources update.
    /// </summary>
    protected NetflowUpdateQueryType[] UpdateTypes
    {
        get { return new[] { NetflowUpdateQueryType.Sources }.Union(AdditionalUpdateTypes ?? Enumerable.Empty<NetflowUpdateQueryType>()).Distinct().ToArray(); }
    }

    /// <summary>
    /// Used for including any additional update types, which affect data in info resource.
    /// </summary>
    protected virtual NetflowUpdateQueryType[] AdditionalUpdateTypes { get { return null; } }

    /// <summary>
    /// Returns warning message, based on the filters and update types.
    /// </summary>
    /// <param name="filters">Resource filters.</param>
    /// <returns>Null if no warning, otherwise text of warning.</returns>
    public string GetWarningMessage(IFilterCollection filters)
    {
        return TopResourceCoreChartBase.TopResourceLegendWarningMessage(UpdateTypes, filters);
    }
}
