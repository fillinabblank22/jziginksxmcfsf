using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Netflow.Web.Reporting.Models;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Orion.Common;
using SolarWinds.Orion.Web.ResourcesMetadata;
using SolarWinds.Orion.Web.ResourcesMetadata.Enums;

[ResourceMetadata(StandardMetadataPropertyName.IsCompatibleWithReporting, "false")]
[ResourceMetadata(StandardMetadataPropertyName.Type, CoreMetadataTypeValues.CustomResources)]
public partial class Orion_TrafficAnalysis_Resources_NetFlowEndpoint_EndpointDetails : InfoResourceBase
{
    private NetflowEndpoint netflowEndpoint;
    private WirelessController wirelessController;
    private WirelessAccessPoint wirelessAccessPoint;

    #region Properties

    protected override NetflowObject NetflowObject => netflowEndpoint;

    public string IP => netflowEndpoint.IPAddress;

    public string Key => netflowEndpoint.NetObjectID;

    public string DNS => string.IsNullOrEmpty(netflowEndpoint.Hostname) ? netflowEndpoint.IPAddress : netflowEndpoint.Hostname;

    public string IPAddressGroupIconHtml => NetflowIPAddressGroup.GetIconHtml(this.IPAddressGroupID);

    public long IPAddressGroupID => netflowEndpoint.IPAddressGroupID;

    public string IPAddressGroupName => netflowEndpoint.IPAddressGroupName;

    public string IPAddressGroupLink => LinkBuilder.GetLink(ViewType.IPAddressGroup, "G", this.IPAddressGroupID > 0 ? this.IPAddressGroupID.ToString() : string.Empty, NetflowObject.Filters);

    public string DomainIconHtml => NetflowDomain.GetIconHtml(this.Domain);

    public string Domain => netflowEndpoint.Domain;

    public string DomainLink => LinkBuilder.GetLink(ViewType.Domain, "D", this.Domain ?? string.Empty, NetflowObject.Filters);

    protected IEnumerable<Tuple<string, string>> GetCountries()
    {
        return netflowEndpoint.GetCountries();
    } 

    public string GetCountryIconHtml(string countryCode)
    {
        return NetflowCountry.GetIconHtml(countryCode);
    }

    public string GetCountryLink(string countryCode)
    {
        return LinkBuilder.GetLink(ViewType.Country, "L1", countryCode, NetflowObject.Filters);
    }

    public string TotalTrafficTxBytes => netflowEndpoint.GetTotalBytes(true, false);

    public string TotalTrafficRxBytes => netflowEndpoint.GetTotalBytes(false, true);

    public string WirelessControllerLink => LinkBuilder.GetNpmNodeDetailsLink(this.wirelessController.NodeId);

    public string WirelessControllerName => this.wirelessController.DisplayName;

    public string WirelessAccessPointLink => LinkBuilder.GetNpmThinAccessPointDetailsLink(this.wirelessAccessPoint.Id);

    public string WirelessAccessPointName => this.wirelessAccessPoint.DisplayName;

    public string Period => netflowEndpoint.PeriodNameTitle;

    public string EditHostnameLink => UIHelper.FormatEditUrl("/Orion/TrafficAnalysis/Editors/HostnameEdit.aspx", this.Resource.ID);

    public IEnumerable<Tuple<string, string, string>> GetIpAddressesWithLinkAndIcon()
    {
        return netflowEndpoint.GetIpAddressWithLinkAndIcon();
    }

    #region BaseResourceControl overrides
    protected override string DefaultTitle => Resources.NTAWebContent.NTAWEBCODE_VB0_31;

    public override string HelpLinkFragment => "OrionNetFlowPHEndpointDetails";

    public override IEnumerable<Type> RequiredInterfaces
    {
        get { return new Type[] { typeof(INetflowEndpointProvider) }; }
    }
    #endregion

    #endregion

    protected override void OnInit(EventArgs e)
    {
        netflowEndpoint = this.GetInterfaceInstance<INetflowEndpointProvider>().NetflowEndpoint;
        warningMessage.ResourceId = Resource.ID;
        warningMessage.WarningMessage = GetWarningMessage(netflowEndpoint.Filters);
        warningMessage.UpdateTypes = this.UpdateTypes;
        warningMessage.FilterID = netflowEndpoint.Filters.FilterIdWithAbsoluteTimePeriodFilter();
        wirelessController = netflowEndpoint.WirelessController;
        wirelessAccessPoint = netflowEndpoint.WirelessAccessPoint;

        if (!Page.IsPostBack)
        {
            DnsInfo.HostName = netflowEndpoint.Hostname;
            DnsInfo.AttachedIP = netflowEndpoint.IPAddress;

            NetflowIPAddress ipAddress;
            var containsEndpointIpv4Address = NetflowIPAddress.TryParse(netflowEndpoint.IPAddress, out ipAddress)
                    && ipAddress.Version == IPVersion.IPv4;

            editBtn.Visible = lookupBtn.Visible = containsEndpointIpv4Address;
        }

        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {

        WirelessControllerRow.Visible = wirelessController != null;
        WirelessAccessPointRow.Visible = wirelessAccessPoint != null;

        DataBind();

        if (OrionConfiguration.IsDemoServer)
        {
            lookupBtn.OnClientClick = UIHelper.GetJSAlertForDisabledFunctionalityOnDemo(this.lookupBtn.Text);
        }

        base.OnLoad(e);
    }

    protected void ipAddressRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal icon = (Literal) e.Item.FindControl("ipAddressIcon");
            HtmlAnchor link = (HtmlAnchor) e.Item.FindControl("IPAddress");
            Tuple<string, string, string> ipInfo = e.Item.DataItem as Tuple<string, string, string>;

            link.InnerText = ipInfo.Item1;
            link.HRef = @"/Orion/TrafficAnalysis/NetflowEndpointDetails.aspx?NetObject=" + ipInfo.Item2;
            icon.Text = ipInfo.Item3;

            if (Profile.ToolsetIntegration)
            {
                link.Attributes.Add("IP", ipInfo.Item1);
            }
        }
    }

    protected void countriesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Literal icon = (Literal)e.Item.FindControl("countryIcon");
            HtmlAnchor link = (HtmlAnchor)e.Item.FindControl("countryLink");
            Tuple<string, string> countryInfo = e.Item.DataItem as Tuple<string, string>;

            link.InnerText = countryInfo.Item2;
            link.HRef = GetCountryLink(countryInfo.Item1);
            icon.Text =  GetCountryIconHtml(countryInfo.Item1);
        }
    }

    protected void IntegrationIconsOnLoad(object sender, EventArgs e)
    {
        this.integrationIcons.ObjectToManage.InternalNetworkObject = netflowEndpoint;

        // If resource is detached as stand alone a toolset integration icons
        // bar should be displayed
        if (UIHelper.IsDetached(Request.Url.AbsoluteUri))
        {
            this.integrationIcons.Visible = true;
        }
    }
}