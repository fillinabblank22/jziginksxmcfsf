<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowIPAddressGroupDetails.aspx.cs" Inherits="Orion_Netflow_IPAddressGroupDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
        <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.NetObject.Name) %>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:IPGroupResourceHost runat="server" ID="groupResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:IPGroupResourceHost>
</asp:Content>