using System;
using System.Web.UI;
using System.Web.Services;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Netflow_NodeDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlowNodeDetails"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        base.OnInit(e);

        this.nnNodeResHost.NetflowNode = (NetflowNode)this.NetObject;

        this.nodeLink.NodeID = this.NetflowObject.Node.NetObjectID;
        this.nodeLink.Content.Controls.Add(this.ParseControl(WebSecurityHelper.HtmlEncode(this.NetObject.Name)));

        this.Page.Form.Attributes["autocomplete"] = "off";
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
