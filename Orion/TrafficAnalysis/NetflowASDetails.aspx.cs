using System;
using System.Web.UI;
using System.Web.Services;

using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_Netflow_ASDetails : NetflowDetailView
{
	public override string ViewType
	{
        get { return "NetFlow Autonomous Systems"; }
	}

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.asResHost.NetflowAS = this.NetObject as NetflowAS;

        this.Page.Form.Attributes["autocomplete"] = "off";
        
        base.OnInit(e);
    }

    /// <summary>
    /// Returns ASName and ASNumber in brackets or only ASNumber
    /// example: 16002 == 16002 => "16002"
    /// example: OurLab != 16002 => "OurLab (16002)"
    /// </summary>
    public string Caption
    {
        get
        {
            // example 16002 == 16002 => "16002"
            if (0 == string.Compare(this.NetObject.Name, (this.NetObject as NetflowAS).ASNumber.ToString()
                , StringComparison.InvariantCultureIgnoreCase))
            {
                return this.NetObject.Name;
            }
            else // example OurLab != 16002 => "OurLab (16002)"
            {
                return string.Format("{0} ({1})", this.NetObject.Name, (this.NetObject as NetflowAS).ASNumber);
            }
        }
    }
    
    
    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
