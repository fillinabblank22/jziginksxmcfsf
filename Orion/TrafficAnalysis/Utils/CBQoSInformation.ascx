﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSInformation.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_CBQoSInformations" %>
<%@ Register TagPrefix="netflow" Assembly="NetflowWeb" Namespace="SolarWinds.Netflow.Web.UI" %>
<%@ Register TagPrefix="netflow" TagName="CBQoSClasses" Src="~/Orion/TrafficAnalysis/Utils/CBQoSInformationClasses.ascx" %>

<%@ Import Namespace="SolarWinds.Netflow.Web.DAL.CBQoS" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<orion:Include runat="server" Module="TrafficAnalysis" File="NetFlow.css" />

<div id="PolicyDetails_Caption" style='width:100%; height:25px;'>
     <%= string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_15, 
                        string.Format("<img alt='{0}' src='/Orion/TrafficAnalysis/images/CBQoS/CBQoS_policy.gif' style='margin-bottom:-4px;' />", 
                                    Resources.NTAWebContent.NTAWEBCODE_AK0_16),
                        string.Format("<img alt='{0}' src='/Orion/TrafficAnalysis/images/CBQoS/CBQoS_class.gif' style='margin-bottom:-4px;' />", 
                                    Resources.NTAWebContent.NTAWEBCODE_AK0_20))%>
</div>

<asp:Panel runat="server" ID="dataEmpty">
    <div style='width:100%; height:25px; margin-top:10px; font-weight: bold;'>
        <%= Resources.NTAWebContent.NTAWEBDATA_AK0_11%>
    </div>
</asp:Panel>

<asp:Repeater runat="server" ID="policies">
    <ItemTemplate>
        <div id="PolicyRoot_<%# Eval("Direction") %>" style="width: 100%; height: 17px; padding: 3px">        
            <img src="<%= GetPolicyIcon() %>" alt='' title='<%# string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_17, Eval("Name")) %>' style='margin-bottom:-3px' /> <b> <%#  Eval("Name") %> </b> 
            <img src="<%# GetPolicyIconDirection((CBQoSPolicyDirection)Eval("Direction")) %>" alt='' title='<%# string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_18, Eval("Direction")) %>' style='margin-bottom:-3px;' />
            <span style='font-size:smaller'>
                <%# GetPolicyDirectionSentence((CBQoSPolicyDirection)Eval("Direction"))%>
            </span>
        </div>

        <table border="0" cellpadding="3" cellspacing="0" frame="void" rules="none" class="ntaTable">
			<thead>
				<tr>
					<td/>
					<td><%= Resources.NTAWebContent.CBQoSInformation_Policies_Header_Limit%>&nbsp;(<%= GetPolicyRateUnits() %>)</td>
					<td colspan="2"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_10%></td>
					<td colspan="2"><%= Resources.NTAWebContent.NTAWEBDATA_AK0_12%></td>
				</tr>
			</thead>
            <netflow:CBQoSClasses runat="server" ID="cbqosClasses" 
                CBQoSClasses='<%# Eval("Classes") %>'
                InterfaceID='<%# InterfaceID %>'
                InterfaceSpeed='<%# InterfaceSpeed %>'
                Level='0'
                CssClass=""
            />
        </table>
    </ItemTemplate>
    <SeparatorTemplate>
        <hr style='color:LightGray;' noshade='noshade' />
    </SeparatorTemplate>
</asp:Repeater>
