﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OnDemandWarning.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_OnDemandWarning" %>

<orion:Include runat="server" ID="OnDemandWarningStyleSheet" Module="TrafficAnalysis" File="DnsOnDemandWarning.css" />

<div class="odwMainDiv">
<img style="float:left; alt="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_23 %>" title="<%= Resources.NTAWebContent.NTAWEBDATA_VB1_23 %>" src="/Orion/TrafficAnalysis/images/warning_blue.gif" />

<div class="odwSecondDiv">
<b><%= Resources.NTAWebContent.NTAWEBDATA_VB1_18 %></b><br class="odwBottomSpaceSmall" />
<%# string.Format(Resources.NTAWebContent.NTAWEBDATA_VB1_19, SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(ResourceName)) %>
<br /><br />
<b><%= Resources.NTAWebContent.NTAWEBDATA_VB1_20 %></b>
<br /><br />
<%= Resources.NTAWebContent.NTAWEBDATA_VB1_21 %><br /><br class="odwBottomSpace" />
<a href="<%=LearnMoreLink %>" target="_blank" rel="noopener noreferrer" style="color: #336699; ">&raquo; <u><%= Resources.NTAWebContent.NTAWEBDATA_VB1_22 %></u></a>   
</div>
<div class="sw-btn-bar-wizard">
    <orion:LocalizableButton ID="btnRemoveResource" runat="server" DisplayType="Secondary" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_24 %>" OnClick="ButtonRemoveResourceClick" ToolTip="<%$ Resources:NTAWebContent,NTAWEBDATA_VB1_25 %>"/>
</div>
</div>

<asp:HiddenField ID="hfResourceID" runat="server" />
<asp:HiddenField ID="hfResourceName" runat="server" />
