﻿<%@ WebHandler Language="C#" Class="ChartImageGenerator" %>

using System;
using System.Data;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;


/// <summary>
/// 
/// </summary>
public class ChartImageGenerator : IHttpHandler
{
    
    private readonly Dictionary<string, string> _lighterDic = new Dictionary<string, string>()
    {
        {"008000", "69B469"},
        {"076CBA", "6DA9D6"},
        
        {"40E0D0", "8FEDED"},
        {"6699FF", "A5C3FF"},
        {"6CB624", "A9D47E"},
        
        {"800080", "B469B4"},
        {"808080", "B4B4B4"},
        {"B0E0E6", "D1EDF0"},
        
        {"B22222", "D27D7D"},
        {"E6BE02", "F0D96A"},
        
        {"FC7A0A", "FDB16F"},
        {"FF6347", "FFA393"},
        
        {"FFA500", "FFCA69"},
        {"FFFF00", "FFFF69"},
        
    };       

    public void ProcessRequest(HttpContext context)
    {
        HttpResponse response = context.Response;
        
        // client cache is enabled
        response.Cache.SetCacheability(HttpCacheability.Public);        
        response.Cache.SetExpires(DateTime.Now.AddMonths(1));


        string colorname = context.Request.QueryString["color"].ToUpper();
        bool lighter = false;
        bool.TryParse(context.Request.QueryString["lighter"], out lighter);

        if (lighter)
            if (!_lighterDic.TryGetValue(colorname, out colorname))
                colorname = context.Request.QueryString["color"];

        Color gencolor = Color.Empty;

        try
        {
            gencolor = System.Drawing.ColorTranslator.FromHtml("#" + colorname);
        }
        catch 
        {
        };       

        
        
        Bitmap bmp = new Bitmap(1, 1);
        ImageFormat bmptype = ImageFormat.Jpeg;

        bmp.SetPixel(0, 0, gencolor);        

        Stream outputStream = new MemoryStream();
        response.ContentType = "image/" + bmptype.ToString().ToLower();

        bmp.Save(outputStream, bmptype);
        byte[] buffer = ((MemoryStream)outputStream).ToArray();
        response.OutputStream.Write(buffer, 0, buffer.Length);

        response.Write("\r\n");
        response.Flush();
        response.End();
    }       
    
    public bool IsReusable
    {
        get{ return false; }
    }    
}

