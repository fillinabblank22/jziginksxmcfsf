<%@ Control Language="C#" ClassName="IntegrationIcons" %>
<%@ Import Namespace="SolarWinds.Orion.NPM.Web" %>

<script runat="server">
    public class ToolsetManagedObject
    {
        public enum NetworkObjectType
        {
            Node,
            Endpoint,
            Unknown
        }

        public NetworkObjectType ManagedObjectType
        {
            get
            {
                if (internalNetworkObject is Node) return NetworkObjectType.Node;
                if (internalNetworkObject is SolarWinds.Netflow.Web.NetflowEndpoint) return NetworkObjectType.Endpoint;
                return NetworkObjectType.Unknown;
            }
        }

        object internalNetworkObject;
        public object InternalNetworkObject
        {
            set { internalNetworkObject = value; }
        }

        public string Name
        {
            get
            {
                switch (ManagedObjectType)
                {
                    case NetworkObjectType.Node:
                        return (internalNetworkObject as Node).Name;
                    case NetworkObjectType.Endpoint:
                        return (internalNetworkObject as SolarWinds.Netflow.Web.NetflowEndpoint).Name;
                    case NetworkObjectType.Unknown:
                    default:
                        return "";
                }
            }
        }

        public string IPAddress
        {
            get
            {
                switch (ManagedObjectType)
                {
                    case NetworkObjectType.Node:
                        return (internalNetworkObject as Node).IPAddress.ToString();
                    case NetworkObjectType.Endpoint:
                        return (internalNetworkObject as SolarWinds.Netflow.Web.NetflowEndpoint).IPAddress;
                    case NetworkObjectType.Unknown:
                    default:
                        return "";
                }
            }
        }

        public string Hostname
        {
            get
            {
                switch (ManagedObjectType)
                {
                    case NetworkObjectType.Node:
                        return (internalNetworkObject as Node).Hostname;
                    case NetworkObjectType.Endpoint:
                        return (internalNetworkObject as SolarWinds.Netflow.Web.NetflowEndpoint).Hostname;
                    case NetworkObjectType.Unknown:
                    default:
                        return "";

                }
            }
        }

        public string CommunityString
        {
            get
            {
                switch (ManagedObjectType)
                {
                    case NetworkObjectType.Node:
                        return (internalNetworkObject as Node).CommunityString;
                    case NetworkObjectType.Endpoint:
                    case NetworkObjectType.Unknown:
                    default:
                        return "";

                }
            }
        }
    }

    private ToolsetManagedObject _objectToManage;
    public ToolsetManagedObject ObjectToManage
    {
        get
        {
            if (_objectToManage == null)
                _objectToManage = new ToolsetManagedObject();
            
            return _objectToManage;
        }
        set
        {
            _objectToManage = value;
        }
    }

    protected void AddNodeParams(HtmlControl control)
    {
        if (Profile.ToolsetIntegration && ObjectToManage.ManagedObjectType == ToolsetManagedObject.NetworkObjectType.Node)
        {
            control.Attributes.Add("IP", ObjectToManage.IPAddress.ToString());
            control.Attributes.Add("NodeHostname", ObjectToManage.Hostname);

            if (SolarWinds.Orion.Core.Common.RegistrySettings.AllowSecureDataOnWeb())
            {
                control.Attributes.Add("Community", ObjectToManage.CommunityString);
            }
            else
            {
                string attrValue = string.Format("GUID{{{0}}}", SolarWinds.Orion.Web.DAL.WebCommunityStringsDAL.GetCommunityStringGuid(ObjectToManage.CommunityString));
                control.Attributes.Add("Community", attrValue);
            }
        }
    }

    protected override void OnInit(EventArgs e)
    {
        if (!this.Profile.ToolsetIntegration)
            this.Visible = false;
        
        base.OnInit(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        AddNodeParams(this.pingTool);
        AddNodeParams(this.traceRouteTool);
    }
</script>

<script type="text/javascript">
    //<![CDATA[
    $(function() {
        var srcReplace = function(img, find, replace) {
            img.src = img.src.replace(find, replace);
        };
        $('img', '#<%=this.iconwrapper.ClientID %>').hover(function() {
            srcReplace(this, '.tb.', '.mo.');
        }, function() {
            srcReplace(this, '.mo.', '.tb.');
        });
    });
    //]]>
</script>

<span runat="server" id="iconwrapper"><a href='<%= string.Format("http://{0}", this.ObjectToManage.IPAddress) %>'>
    <img src="/Orion/images/ToolsetIntegration/Small.Goto.tb.gif" alt="Web Browse to <%= this.ObjectToManage.Name %>"
        title="Web Browse to <%= this.ObjectToManage.Name %>" /></a><a href='<%= string.Format("ssh://{0}", this.ObjectToManage.IPAddress) %>'><img
            src="/Orion/images/ToolsetIntegration/Small.SSH.tb.gif" alt="Open a Secure Shell (SSH) session to <%= this.ObjectToManage.Name %>"
            title="Open a Secure Shell (SSH) session to <%= this.ObjectToManage.Name %>" /></a><a
                href='<%= string.Format("telnet://{0}", this.ObjectToManage.IPAddress) %>'><img src="/Orion/images/ToolsetIntegration/Small.Telnet.tb.gif"
                    alt="Open a Telnet session to <%= this.ObjectToManage.Name %>" title="Open a Telnet session to <%= this.ObjectToManage.Name %>" /></a><a
                        href="" onclick="return false;"><img runat="server" id="pingTool" src="/Orion/images/ToolsetIntegration/Small.Ping.tb.gif"
                            alt="Launch SolarWinds PING tool" title="Launch SolarWinds PING tool" onmouseout="status=''; return false;"
                            onmouseover="status='Launch SolarWinds PING tool'; return true;" onclick="DoAction('SWTOOL:PING', '');" /></a><a
                                href="" onclick="return false;"><img runat="server" id="traceRouteTool" src="/Orion/images/ToolsetIntegration/Small.TraceRoute.tb.gif"
                                    alt="Launch SolarWinds TraceRoute tool" title="Launch SolarWinds TraceRoute tool"
                                    onmouseout="status=''; return false;" onmouseover="status='Launch SolarWinds TraceRoute tool'; return true;"
                                    onclick="DoAction('SWTOOL:TraceRoute', '');" /></a><a href='<%= string.Format("/Orion/RemoteDesktop.aspx?Server={0}", this.ObjectToManage.IPAddress) %>'
                                        target="_blank" rel="noopener noreferrer"><img src="/Orion/images/ToolsetIntegration/Small.RDP.tb.gif" alt="Open a Remote Desktop session to <%= this.ObjectToManage.Name %>"
                                            title="Open a Remote Desktop session to <%= this.ObjectToManage.Name %>" /></a>
</span>