﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.CoreCharts;
using SolarWinds.Netflow.Web.DAL.CBQoS;
using SolarWinds.Netflow.Web.UI.CBQoS;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Utils_CBQoSLegendPolicies : UserControl, ICBQoSLegendPolicies
{
    public IEnumerable<CBQoSPolicyTop> Policies { get; set; }
    public int Level { get; set; }
    public string NoChartStyle { get; set; }
    public bool IsRoot { get; set; }
    public IChartLegendDescriptor LegendDescriptor { get; set; }
    public string CssClass { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        RepeaterPolicies.DataSource = Policies;
        RepeaterPolicies.DataBind();
    }
    
    protected string GetRowIdent(bool indentForNestedClass)
    {
        const int baseIndent = 16;
        const int margin = 5;

        int ident = margin + (indentForNestedClass ? baseIndent : 0) + (baseIndent * (Level * 2));

        return ident.ToString(CultureInfo.InvariantCulture) + "px";
    }

    protected void RepeaterClasses_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CBQoSClassTop cbqosClass = (CBQoSClassTop)e.Item.DataItem;
            Control nestedPoliciesContainer = e.Item.FindControl("nestedPoliciesContainer");
            LoadNestedPoliciesControl(nestedPoliciesContainer.Controls, cbqosClass.NestedPolicies, GetRowCssClass(e.Item.ItemIndex));
        }
    }

    private void LoadNestedPoliciesControl(ControlCollection container, IEnumerable<CBQoSPolicyTop> policies, string cssClass)
    {
        Control control = Page.LoadControl("~/Orion/TrafficAnalysis/Utils/CBQoSLegendPolicies.ascx");

        ICBQoSLegendPolicies policiesControl = (ICBQoSLegendPolicies) control;
        policiesControl.Policies = policies;
        policiesControl.NoChartStyle = NoChartStyle;
        policiesControl.LegendDescriptor = LegendDescriptor;
        policiesControl.Level = Level + 1;
        policiesControl.IsRoot = false;
        policiesControl.CssClass = cssClass;
        container.Add(control);
    }

    protected string GetBorderStyle()
    {
        return Level > 0 ? "border-top: 1pt solid #D5D5D5;" : "";
    }

    protected string GetRowCssClass(int dataIndex)
    {
        return string.IsNullOrEmpty(this.CssClass) ? (dataIndex % 2 == 0 ? "TopResourceEvenTableRow" : "TopResourceOddTableRow") : this.CssClass;
    }

    protected string GetPolicyIcon()
    {
        return IconPathHelper.GetCBQoSIconPolicyPath();
    }

    protected string GetClassIcon()
    {
        return IconPathHelper.GetIconClassMap();
    }

    protected string GetPolicyIconDirection(CBQoSPolicyDirection dir)
    {
        return IconPathHelper.GetCBQoSIconPath(dir == CBQoSPolicyDirection.Input ?
            IconPathHelper.ICON_POLICY_DIRECTION_INPUT :
            IconPathHelper.ICON_POLICY_DIRECTION_OUTPUT);
    }

    protected string FormatUtilization(double utilization)
    {
        string retStr = (utilization / 100).ToString("P", CultureInfo.InvariantCulture);
        return retStr.Replace(" ", "&nbsp");
    }

    protected InlineBar.ThresholdLevel GetThresholdLevel(double utilization)
    {
        return utilization > 100 ? InlineBar.ThresholdLevel.Error : InlineBar.ThresholdLevel.Normal;
    }
}