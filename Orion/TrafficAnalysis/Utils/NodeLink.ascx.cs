using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Resources;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Utils_NodeLink : System.Web.UI.UserControl
{
    private int _nodeID;

    [PersistenceMode(PersistenceMode.Attribute)]
    public int NodeID
    {
        get { return _nodeID; }
        set 
        {            
            _nodeID = value;
            string netObjectID = string.Concat("N:", value);

            try
            {
                this.MyNode = (Node)NetObjectFactory.Create(netObjectID);
                this.NodeLink.NodeID = netObjectID;
            }
            catch(IndexOutOfRangeException)
            {
                lblName.Text = string.Format(NTAWebContent.NTAWEBCODE_AK0_60, value);
            }
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetURLFormat
    {
        get { return NodeLink.TargetURLFormat; }
        set { NodeLink.TargetURLFormat = (MyNode==null?string.Empty:value); }
    }

    private Node _myNode;

    protected Node MyNode
    {
        get { return _myNode; }
        set 
        {
            _myNode = value;
            this.nodeStatus.StatusValue = this.MyNode.Status;
            this.imgVendor.Src = this.VendorIconPath(this.MyNode["SysObjectID"].ToString());
            this.lblName.Text = WebSecurityHelper.SanitizeHtmlV2(this.MyNode.Name);
        }
    }

    protected string VendorIconPath(string sysObjectID)
    {
        const string imagePath = "/NetPerfMon/images/Vendors/";
        const string sysObjectPrefix = "1.3.6.1.4.1.";

        if (string.IsNullOrEmpty(sysObjectID) || (!sysObjectID.ToString().StartsWith(sysObjectPrefix)))
        {
            return string.Format("{0}{1}", imagePath, "Unknown.gif");
        }

        string vendorNum = sysObjectID.Substring(sysObjectPrefix.Length);
	  	  	 	       
        // exception is thrown if vendorNum doesn't ends with dot (".")
        if (vendorNum.IndexOf('.') > 0)
        {
            vendorNum = vendorNum.Substring(0, vendorNum.IndexOf('.'));
        }
          	 	       
        return string.Format("{0}{1}.gif", imagePath, vendorNum);
    }
}
