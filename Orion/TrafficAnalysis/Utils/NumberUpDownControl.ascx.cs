﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

[ValidationPropertyAttribute("ValidationText")]  
public partial class Orion_TrafficAnalysis_Utils_NumberUpDownControl : System.Web.UI.UserControl
{
    public Unit TextBoxWidth
    {
        get { return txbValue.Width; }
        set { txbValue.Width = value; }
    }

    public int Value 
    {
        get { return Convert.ToInt32(txbValue.Text); }
        set { txbValue.Text = value.ToString(); } 
    }

    public string ValidationText
    {
        get { return txbValue.Text; }
        set { txbValue.Text = value; }
    }

    public int MinimumValue
    {
        get { return vldRangValue.MinimumValue != "" ? Convert.ToInt32(vldRangValue.MinimumValue) : 0; }
        set
        {
            vldRangValue.MinimumValue = value.ToString();
            numExtValue.Minimum = value;
            SetVldRangValueErrorMessage();
        }
    }

    public string ValidatorText
    {
        get { return vldRangValue.ErrorMessage; }
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                vldRangValue.ErrorMessage = value;
            }
        }
    }

    /// <summary>
    /// Text which is permanently displayed before control and after control before validators (e.g. minutes)
    /// </summary>
    public string TextBeforeAndAfter
    {
        get { return this.lblTextBefore.Text + "{0}" + this.lblTextAfter.Text; }
        set
        {
            if (value == null || value.Length == 0)
            {
                this.lblTextBefore.Text = "";
                this.lblTextAfter.Text = "";
            }
            else
            {
                string a;
                try
                {
                    a = string.Format(value, "\0");
                }
                catch (FormatException)
                {
                    throw new ArgumentOutOfRangeException("TextBeforeAndAfter");
                }

                int b = a.IndexOf('\0');
                if (b < 0)
                {
                    this.lblTextBefore.Text = a;
                    this.lblTextAfter.Text = "";
                }
                else
                {
                    this.lblTextBefore.Text = a.Substring(0, b);
                    this.lblTextAfter.Text = a.Substring(b + 1);
                }
            }
        }
    }

    public string TextAfterCss 
    {
        get { return lblTextAfter.CssClass; } 
        
        set 
        {
            if (string.IsNullOrEmpty(value))
            {
                lblTextAfter.CssClass = "textAfter";
            }
            else
            {
                lblTextAfter.CssClass = value;
            }
        }
    }

    public string TextBeforeCss
    {
        get { return lblTextBefore.CssClass; }

        set
        {
            if (string.IsNullOrEmpty(value))
            {
                lblTextBefore.CssClass = "textBefore";
            }
            else
            {
                lblTextBefore.CssClass = value;
            }
        }
    }

    public int MaximumValue
    {
        get { return vldRangValue.MaximumValue != "" ? Convert.ToInt32(vldRangValue.MaximumValue) : 0; }
        set
        {
            vldRangValue.MaximumValue = value.ToString();
            numExtValue.Maximum = value;
            SetVldRangValueErrorMessage();
        }
    }

    private void SetVldRangValueErrorMessage()
    {
            vldRangValue.ErrorMessage = String.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_47, MinimumValue, MaximumValue);
            valIcn.Title = vldRangValue.ErrorMessage;
    }

    private double _step = 1;

    public double Step
    {
        get
        {
            return _step;
        }
        set
        {
            this._step = value;
            this.numExtValue.Step = _step;
        }
    }

  
}
