﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSLegendPolicies.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_CBQoSLegendPolicies" %>
<%@ Register TagPrefix="netflow" Assembly="NetflowWeb" Namespace="SolarWinds.Netflow.Web.UI" %>

<%@ Import Namespace="SolarWinds.Netflow.Web" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.DAL.CBQoS" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<asp:Repeater ID="RepeaterPolicies" runat="server">
    <ItemTemplate>
        <tr id="Policy_<%# Eval("PolicyMapId")%>_Level_<%# Level %>" class="<%# GetRowCssClass(Container.ItemIndex) %>" style="<%# GetBorderStyle() %>">
            <td style='padding-left:<%# GetRowIdent(false) %>;'>
                <img src="<%# GetPolicyIcon()%>" title="<%# string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_17, Eval("Name"))%>"/>
                <% if (IsRoot) {%>
                    <img src="<%# IconPathHelper.GetIconCMDirection(Eval("Direction")) %>" alt="<%# IconPathHelper.GetDirectionTitle(Eval("Direction"), Eval("Name")) %>" title="<%# IconPathHelper.GetDirectionTitle(Eval("Direction"), Eval("Name")) %>"  />
                <% } %>
                <%#Eval("Name") %>
            </td>
            <td colspan="7" />
        </tr>
        <asp:Repeater ID="RepeaterClasses" runat="server" DataSource='<%# Eval("Classes") %>' OnItemDataBound="RepeaterClasses_ItemDataBound">
            <ItemTemplate> 
                <tr id="Class_<%# Eval("PolicyId")%>_Level_<%# Level %>" class="<%# GetRowCssClass(Container.ItemIndex) %>" style="border-top: 1pt solid #D5D5D5;" class="<%# GetRowCssClass(((RepeaterItem)Container.Parent.Parent).ItemIndex) %>">
                    <td colspan="4" style='padding-left:<%# GetRowIdent(true) %>;'>
                        <a href='<%# LinkBuilder.GetLinkTarget(Eval("ClassId"), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>' title='<%# string.Format(Resources.NTAWebContent.NTAWEBDATA_AK0_23, Eval("Name")) %>'>
                            <img src="<%# IconPathHelper.GetIconClassMap() %>" style="margin-bottom:5px;" /> 
                        </a>
                        
                        <span id='legendSymbolContainer_<%# Eval("LimitedClassId") %>' style="<%= NoChartStyle %>"></span>
                        
                        <% if (LegendDescriptor.Parameters.ChartType == ChartType.Area) { %>
                            <span id='legendCheckboxContainer_<%# Eval("LimitedClassId") %>' style="<%= NoChartStyle %>"></span>
                        <% } %>

                        <a href='<%# LinkBuilder.GetLinkTarget(Eval("ClassId"), LegendDescriptor.SummaryViewKey, LegendDescriptor.Prefix, LegendDescriptor.Parameters.Filters, LegendDescriptor.ResourceType, LegendDescriptor.Parameters.ViewType) %>'>
                            <%# Eval("Name") %>
                        </a>
                    </td>                
                    <netflow:InlineBar ID="InlineBar1" Width="100" runat="server" Percentage='<%# (double)Eval("Utilization") %>' ThresholdLevelValue='<%# GetThresholdLevel((double)Eval("Utilization")) %>'>
                    </netflow:InlineBar>
                    <td>
                        <%# FormatUtilization((double)Eval("Utilization")) %>
                    </td>
                    <netflow:InlineBar ID="InlineBar2" runat="server" Width="100" Percentage='<%# (double)Eval("LastPolledUtilization") %>' ThresholdLevelValue='<%# GetThresholdLevel((double)Eval("LastPolledUtilization")) %>'>
                    </netflow:InlineBar>
                    <td>
                        <%# FormatUtilization((double)Eval("LastPolledUtilization")) %>
                    </td>
                </tr>
                <div id="nestedPoliciesContainer" runat="server">
                        
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:Repeater>