﻿using System;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.Filters;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.UI;
using SolarWinds.Netflow.Web.UI;
using System.Web.UI;

public partial class Orion_TrafficAnalysis_Utils_FlowDirectionSelector : UserControl
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        BothSelectorDiv.Visible = !this.HideBothSelector;
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        this.rbBoth.Text = FlowDirectionFilter.GetFlowDirectionTitle(true, true);
        this.rbBoth.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_111, this.rbBoth.Text);
        
        this.rbIngress.Text = FlowDirectionFilter.GetFlowDirectionTitle(true, false);
        this.rbIngress.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_111, this.rbIngress.Text);
        
        this.rbEgress.Text = FlowDirectionFilter.GetFlowDirectionTitle(false, true);
        this.rbEgress.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_111, this.rbEgress.Text);
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public bool HideBothSelector { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public FlowDirectionFilter.FlowDirectionType FlowDirection
    {
        get
        {
            if (this.rbBoth.Checked)
            {
                return FlowDirectionFilter.FlowDirectionType.Egress | FlowDirectionFilter.FlowDirectionType.Ingress;
            }
            else if (this.rbEgress.Checked)
            {
                return FlowDirectionFilter.FlowDirectionType.Egress;
            }
            else if (this.rbIngress.Checked)
            {
                return FlowDirectionFilter.FlowDirectionType.Ingress;
            }

            throw new InvalidOperationException("Unsupported combination in FlowDirection selector.");
        }
        set
        {
            this.rbBoth.Checked = value.Contains(FlowDirectionFilter.FlowDirectionType.Egress) && value.Contains(FlowDirectionFilter.FlowDirectionType.Ingress);
            if (rbBoth.Checked)
                return;

            this.rbIngress.Checked = value.Contains(FlowDirectionFilter.FlowDirectionType.Ingress);
            this.rbEgress.Checked = value.Contains(FlowDirectionFilter.FlowDirectionType.Egress);
        }

    }

    public string CurrentDirectionText
    {
        get
        {
            if (this.rbBoth.Checked)
            {
                return this.rbBoth.Text;
            }
            else if (this.rbIngress.Checked)
            {
                return this.rbIngress.Text;
            }
            else if (this.rbEgress.Checked)
            {
                return this.rbEgress.Text;
            }
            return this.rbBoth.Text;
        }
    }
}
