﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.CBQoS;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL.CBQoS;
using SolarWinds.Netflow.Web.UI.CBQoS;
using SolarWinds.Orion.Web.DisplayTypes;

public partial class Orion_TrafficAnalysis_Utils_CBQoSInformationClasses : System.Web.UI.UserControl, ICBQoSInformationClasses
{
    protected override void OnInit(EventArgs e)
    {
        RateLayout = CBQoSGlobalSettingsDALCached.CBQoSPolicyDetailsRateLayout;
    }

    protected string GetIconClassMap()
    {
        return IconPathHelper.GetIconClassMap();
    }

    protected string GetPolicyIcon()
    {
        return IconPathHelper.GetCBQoSIconPolicyPath();
    }

    /// <summary>
    /// Make link to CBQoS View page
    /// </summary>
    /// <param name="pClassMapID"></param>
    /// <returns></returns>
    public string MakeLink(object pClassMapID)
    {
        // check pClassMapID
        if (pClassMapID == null)
        {
            return HttpUtility.HtmlEncode(this.Request.Url.AbsoluteUri);
        }

        // if time period is defined
        int timeIndex = Request.Url.Query.LastIndexOf(";T:");
        string timePeriod = string.Empty;

        if (timeIndex > -1)
        {
            timePeriod = Request.Url.Query.Substring(timeIndex);
        }

        string retStr = string.Format("{0}://{1}:{2}/Orion/TrafficAnalysis/NetflowCBQoSDetails.aspx?NetObject=CCM:{3};I:{4}{5}", Request.Url.Scheme, Request.Url.Host,
            Request.Url.Port, pClassMapID, this.InterfaceID, timePeriod);


        return HttpUtility.HtmlEncode(retStr);
    }

    public int Level { get; set; }
    
    public IEnumerable<CBQoSClassDetail> CBQoSClasses { get; set; }

    public int InterfaceID { get; set; }

    public double InterfaceSpeed { get; set; }

    protected CbqosPolicyRateLayout RateLayout { get; set; }

    protected void RepeaterNestedPolicies_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CBQoSPolicyDetail cbqosPolicy = (CBQoSPolicyDetail)e.Item.DataItem;
            Control nestedPoliciesContainer = e.Item.FindControl("nestedClassesContainer");
            LoadNestedClassesControl(nestedPoliciesContainer.Controls, cbqosPolicy.Classes, GetRowCssClass(((RepeaterItem)e.Item.Parent.Parent).ItemIndex));
        }
    }

    protected string GetRowIdent(bool identForNestedPolicy)
    {
        int baseIdent = 15;
        int ident = 4 + (baseIdent * (this.Level*2));
            if (identForNestedPolicy)
                ident += baseIdent;
        return ident.ToString()+"px";
    }

    public string CssClass { get; set; }

    protected string GetBorderStyle()
    {
        return Level > 0 ? "border-top: 1pt solid #D5D5D5;" : "";
    }

    protected string GetRowCssClass(int dataIndex)
    {
        return string.IsNullOrEmpty(this.CssClass) ? (dataIndex % 2 == 0 ? "TopResourceEvenTableRow" : "TopResourceOddTableRow") : this.CssClass;
    }

    private void LoadNestedClassesControl(ControlCollection container, IEnumerable<CBQoSClassDetail> cbqosClasses, string cssClass)
    {
        Control control  = Page.LoadControl("~/Orion/TrafficAnalysis/Utils/CBQoSInformationClasses.ascx");

        ICBQoSInformationClasses classesControl = (ICBQoSInformationClasses) control;
        classesControl.InterfaceID = this.InterfaceID;
        classesControl.InterfaceSpeed = InterfaceSpeed;
        classesControl.CBQoSClasses = cbqosClasses;
        classesControl.Level = this.Level + 1;
        classesControl.CssClass = cssClass;
        
        container.Add(control);
        control.DataBind();
    }

    protected string GetDisplayRate(CBQoSClassDetail classDetail)
    {
        if (classDetail == null || classDetail.RateType == CBQoSPolicyRateType.None)
            return null;

        if (RateLayout == CbqosPolicyRateLayout.Default)
        {
            switch (classDetail.RateType)
            {
                case CBQoSPolicyRateType.Bps:
                    return new BitsPerSecond(classDetail.Rate).ToString();
                case CBQoSPolicyRateType.Percentage:
                    return (classDetail.Rate / 100).ToString("P0");
            }
        }

        if (RateLayout == CbqosPolicyRateLayout.Percentage)
        {
            double rate = 0;
            if (classDetail.EffectiveRate > 0 && InterfaceSpeed > 0)
                rate = classDetail.EffectiveRate / InterfaceSpeed;

            return rate.ToString("P0");
        }

        if (RateLayout == CbqosPolicyRateLayout.Bps)
        {
            return new BitsPerSecond(classDetail.EffectiveRate).ToString();
        }

        throw new ArgumentOutOfRangeException("Incorrect value of Cbqos Policy Rate");
    }

}