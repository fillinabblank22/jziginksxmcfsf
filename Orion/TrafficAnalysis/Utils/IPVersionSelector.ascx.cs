﻿using System;
using SolarWinds.Netflow.Common.Filters;
using System.Web.UI;

public partial class Orion_TrafficAnalysis_Utils_IPVersionSelector : UserControl
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        
        this.rbBoth.Text = IPVersionFilter.GetTitle(IPVersionFilter.Type.Both);
        this.rbBoth.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_112, this.rbBoth.Text);
        
        this.rbIPv4.Text = IPVersionFilter.GetTitle(IPVersionFilter.Type.IPv4);
        this.rbIPv4.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_112, this.rbIPv4.Text);
        
        this.rbIPv6.Text = IPVersionFilter.GetTitle(IPVersionFilter.Type.IPv6);
        this.rbIPv6.ToolTip = String.Format(Resources.NTAWebContent.NTAWEBDATA_TM0_112, this.rbIPv6.Text);
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public IPVersionFilter.Type IPVersion
    {
        get
        {
            if (this.rbBoth.Checked)
            {
                return IPVersionFilter.Type.Both;
            }
            else if (rbIPv4.Checked)
            {
                return IPVersionFilter.Type.IPv4;
            }
            else if (rbIPv6.Checked)
            {
                return IPVersionFilter.Type.IPv6;
            }

            throw new InvalidOperationException("Unsupported combination in FlowDirection selector.");
        }
        set
        {
            this.rbBoth.Checked = value.Contains(IPVersionFilter.Type.IPv4) && value.Contains(IPVersionFilter.Type.IPv6);
            if (rbBoth.Checked)
            {
                return;
            }

            rbIPv4.Checked = value.Contains(IPVersionFilter.Type.IPv4);
            rbIPv6.Checked = value.Contains(IPVersionFilter.Type.IPv6);
        }
    }

    public string CurrentIPVersionText
    {
        get
        {
            if (rbIPv4.Checked)
            {
                return rbIPv4.Text;
            }

            if (rbIPv6.Checked)
            {
                return rbIPv6.Text;
            }

            return this.rbBoth.Text;
        }
    }
}
