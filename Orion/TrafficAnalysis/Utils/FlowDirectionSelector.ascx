﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FlowDirectionSelector.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_FlowDirectionSelector" %>

<style type="text/css">
.FlowDirectionSelectorItem
{
    padding-left:5px;
    padding-bottom:5px;
}
</style>

<div id="flowDirectionSelectorDiv" style="margin-bottom:5px">
    <div class="FlowDirectionSelectorItem" runat="server" id="BothSelectorDiv" visible="true">
        <asp:RadioButton ID="rbBoth" GroupName="rbGroup" runat="server"/>
    </div>    
    <div class="FlowDirectionSelectorItem">
        <asp:RadioButton ID="rbIngress" GroupName="rbGroup" runat="server" />
    </div>
    <div class="FlowDirectionSelectorItem">
        <asp:RadioButton ID="rbEgress" GroupName="rbGroup" runat="server" />
    </div>
</div>