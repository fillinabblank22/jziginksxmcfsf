using System;
using System.Web.UI;
using Resources;
using SolarWinds.Netflow.Contracts;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Orion.NPM.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Utils_InterfaceLink : System.Web.UI.UserControl
{
    private object _interfaceIndex;

    [PersistenceMode(PersistenceMode.Attribute)]
    public object InterfaceIndex
    {
        get
        {
            return _interfaceIndex;
        }
        set
        {
            _interfaceIndex = value;
            Redraw();
        }
    }

    private int _interfaceID;

    [PersistenceMode(PersistenceMode.Attribute)]
    public int InterfaceID
    {
        get { return _interfaceID; }
        set
        {
            _interfaceID = value;
            Redraw();
        }
    }

    [PersistenceMode(PersistenceMode.Attribute)]
    public string TargetURLFormat
    {
        get
        {
            return InterfaceLink.TargetURLFormat;
        }
        set
        {
            InterfaceLink.TargetURLFormat = value; 
            Redraw();
        }
    }

    protected void Redraw()
    {
        if (FlowInterfaceId.IsValid(InterfaceID))
        {
            string netObjectID = string.Concat("I:", InterfaceID);
            try
            {
                var npmInterface = (Interface)NetObjectFactory.Create(netObjectID);

                ifStatus.StatusValue = npmInterface.Status;
                imgIfType.Src = string.Format("/NetPerfMon/images/Interfaces/{0}.gif", npmInterface["InterfaceType"]);
                lblName.Text = WebSecurityHelper.SanitizeHtmlV2(npmInterface.Caption);

                InterfaceLink.InterfaceID = netObjectID;
                return;
            }
            catch (IndexOutOfRangeException)
            {
            }
        }

        lblName.Text = InterfaceIndex == DBNull.Value
            ? string.Format(NTAWebContent.NTAWEBCODE_AK0_59, InterfaceID)
            : string.Format(NTAWebContent.NTAWEBDATA_UnknownInterface, InterfaceIndex);

        InterfaceLink.TargetURLFormat = string.Empty;
    }

}