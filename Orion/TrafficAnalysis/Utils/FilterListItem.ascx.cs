using System;
using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using System.Text;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Utils_FilterListItem : UserControl
{  
    [PersistenceMode(PersistenceMode.Attribute)]    
    public NetflowFilterWithNameBase Filter { get; set; }

    protected string GetFilterNames()
    {
        StringBuilder result = new StringBuilder();
        string conjuction = string.Empty;
        string optConjuction = Filter.IsExclusive ? Resources.NTAWebContent.NTAWEBCODE_VB1_9 : Resources.NTAWebContent.NTAWEBCODE_VB1_10;
        foreach (string filterName in this.Filter.FilterNames)
        {            
            result.Append(conjuction).Append("<b>").Append(WebSecurityHelper.HtmlEncode(filterName)).Append("</b>");
            conjuction = string.Format(" {0} ", optConjuction);
        }
        return result.ToString();
    }
}
