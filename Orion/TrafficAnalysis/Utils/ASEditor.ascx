<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ASEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_ASEditor" %>
<%@ Register Src="~/Orion/TrafficAnalysis/TimePeriod/AbsolutePeriod.ascx" TagName="AboslutePeriod" TagPrefix="nta" %>

<script type="text/javascript" src="/Orion/TrafficAnalysis/scripts/json2.js"></script>
<script type="text/javascript">
    function ValidateInputs() {
        return true;
    }
</script>
<style type="text/css">
    #tbodyEditor td {
        border-style: none;
    }

    #ntaProperty td {
        border-style: none;
    }

    .addAsTD {
        white-space: nowrap;
    }
</style>
<div>
    <h1 class="modalHeader">
        <asp:Label runat="server" ID="lblTitle" />
    </h1>
    <div class="modalContent">
        <table style="vertical-align: top; width: 730px;" id="ntaProperty">
            <tbody runat="server" id="tbodyEditor">
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_76%>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ValidationGroup="AutonomousSystemValidationGroup" ID="tbID" Width="100px" />
                        <span class="validators">
                            <asp:RequiredFieldValidator     ControlToValidate="tbID" runat="server" ValidationGroup="AutonomousSystemValidationGroup" Display="Dynamic"
                                                            ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_ASEditor_CannotBeEmpty%>">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ControlToValidate="tbID" runat="server" ValidationGroup="AutonomousSystemValidationGroup" Display="Dynamic"
                                                            ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_ASEditor_OnlyNumbersError%>"
                                                            ValidationExpression="^\d+$">
                            </asp:RegularExpressionValidator>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_77%>
                    </td>
                    <td>
                        <asp:TextBox ID="tbName" runat="server" Width="300px" />
                    </td>
                </tr>
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_78%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCountryCode" DataTextField="Caption" DataValueField="ID"
                            AppendDataBoundItems="True" Width="305px">
                            <asp:ListItem Text="" Value="" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_79%>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbOrganization" Width="300px" />
                    </td>
                </tr>
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_80%>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbRegistrationDate" Width="200px" />
                        <asp:Label ID="Label1" runat="server" ForeColor="Gray" Font-Size="11px"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_81%>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="addAsTD">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_82%>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="tbLastUpdated" Width="200px" />
                        <asp:Label ID="lbl2" runat="server" ForeColor="Gray" Font-Size="11px"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_81%>
                        </asp:Label>
                    </td>
                </tr>
            </tbody>
            <tr>
                <td />
                <td>
                    <asp:Label runat="server" ID="lblASConflictHeader" Visible="false">
                        <b><%= String.Format(Resources.NTAWebContent.NTAWEBCODE_VB0_63,SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.AsID)) %><br /><br /> </b>
                    </asp:Label>
                    <asp:Label runat="server" ID="lblASConflictBody" Visible="false">
                    </asp:Label>
                    <asp:Label runat="server" ID="lblASConflictFooter" Font-Bold="true" Visible="false">
                        <br /><%= Resources.NTAWebContent.NTAWEBDATA_VB0_83%><br />
                    </asp:Label>
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div class="modalButtonPanel sw-btn-bar">
        <orion:LocalizableButton OnClientClick="return ValidateInputs(); " runat="server" ID="btnAddApp" ValidationGroup="AutonomousSystemValidationGroup"
            LocalizedText="Save" DisplayType="Primary" OnClick="OnAddClick" />
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" DisplayType="Secondary"
            OnClick="OnCancelClick" />
    </div>
</div>
