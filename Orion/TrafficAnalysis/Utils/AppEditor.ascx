<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AppEditor.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_AppEditor" %>

<script type="text/javascript" src="/Orion/TrafficAnalysis/scripts/json2.js"></script>

<style type="text/css">
    #tbodyEditor td {
        border-style: none;
    }

    #ntaProperty td {
        border-style: none;
    }

    .addAppTD {
        white-space: nowrap;
    }
</style>

<div>
    <h1 class="modalHeader">
        <asp:Label runat="server" ID="lblTitle" />
    </h1>
    <div class="modalContent">
        <table style="vertical-align: top; width: 730px;" id="ntaProperty">
            <tbody runat="server" id="tbodyEditor">
                <tr>
                    <td class="addAppTD"><%=  Resources.NTAWebContent.NTAWEBDATA_AK0_43%></td>
                    <td>
                        <asp:TextBox ID="txtDescription" runat="server" /><br />
                        <asp:Label runat="server" ID="lblDescription" Font-Size="12px">
                            <%= Resources.NTAWebContent.NTAWEBDATA_VB0_89%>
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="addAppTD"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_92%></td>
                    <td style="width: 70%">
                        <asp:TextBox runat="server" ID="txtPorts" /><br />
                        <asp:Label runat="server" ID="lblMultiPort" Font-Size="12px">
                            <%= Resources.NTAWebContent.NTAWEBDATA_VB0_90%>
                        </asp:Label>
                    </td>
                </tr>
                <tr id="trHideSource" runat="server">
                    <td class="addAppTD"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_91%></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlSourceIP" Width="300px"></asp:DropDownList>
                    </td>
                </tr>
                <tr id="trHideLabelSource" runat="server">
                    <td></td>
                    <td>
                        <label id="labelSourceIP" title="<%= Resources.NTAWebContent.NTAWEBCODE_VB0_16%>" class="addAppLabel"></label>
                    </td>
                </tr>
                <tr id="trHideDestination" runat="server">
                    <td class="addAppTD"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_93%></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDestinationIP" Width="300px"></asp:DropDownList>
                    </td>
                </tr>
                <tr id="trHideLabelDestination" runat="server">
                    <td></td>
                    <td>
                        <label id="labelDestinationIP" title="<%=Resources.NTAWebContent.NTAWEBCODE_VB0_17%>" class="addAppLabel"></label>
                    </td>
                </tr>
                <tr>
                    <td class="addAppTD"><%= Resources.NTAWebContent.NTAWEBDATA_VB0_99%></td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlProtocol">
                            <asp:ListItem Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_94%>" Value="0" />
                            <asp:ListItem Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_95%>" Value="6" />
                            <asp:ListItem Text="<%$ Resources:NTAWebContent,NTAWEBDATA_VB0_96%>" Value="17" />
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
            <tr>
                <td />
                <td>
                    <asp:Label runat="server" ID="lblPortConflictHeader" Visible="false">
                    <b><%= Resources.NTAWebContent.NTAWEBDATA_VB0_97%></b>
                    </asp:Label>
                    <asp:Label runat="server" ID="lblGroupDeleted" Visible="false">
                    <b><%= Resources.NTAWebContent.NTAWEBDATA_ManageApps_SubmitAppIpGroupDeletedConflict%></b>
                    </asp:Label>
                    <asp:Repeater runat="server" ID="rptPortConflicts">
                        <HeaderTemplate>
                            <div style="height: 100px; overflow: auto; font-size: 11px; margin: 5px; margin-left: 15px;">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="font-size: 10pt;">
                                <%#  String.Format(Convert.ToBoolean(Eval("Multi")) ? Resources.NTAWebContent.NTAWEBDATA_VB0_101 : Resources.NTAWebContent.NTAWEBDATA_VB0_100, Eval("Port"), SolarWinds.Netflow.Web.Utility.NetFlowCommon.GetProtocolStr(Convert.ToBoolean(Eval("TCP")), Convert.ToBoolean(Eval("UDP")), true), Eval("Description"))%>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate></div></FooterTemplate>
                    </asp:Repeater>
                    <asp:Label runat="server" ID="lblPortConflictFooter" Font-Bold="true" Visible="false">
                        <%= Resources.NTAWebContent.NTAWEBDATA_VB0_98%>
                      <br /></asp:Label><br />
                </td>
            </tr>
        </table>
    </div>
    <div class="modalButtonPanel sw-btn-bar">
        <orion:LocalizableButton OnClientClick="return ValidateInputs(); " runat="server" ID="btnAddApp" LocalizedText="CustomText" DisplayType="Primary" Text="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_38%>" OnClick="OnAddClick" />
        <orion:LocalizableButton runat="server" ID="btnCancel" DisplayType="Secondary" LocalizedText="Cancel" OnClick="OnCancelClick" />
        <orion:LocalizableButton runat="server" ID="btnOk" DisplayType="Primary" LocalizedText="OK" OnClick="OnCancelClick" Visible="false" />
    </div>

    <asp:HiddenField ID="hfApplicationID" runat="server" />
    <asp:HiddenField ID="hfApplicationEnabled" runat="server" />

    <asp:HiddenField ID="hfIPGroupSource" runat="server" />
    <asp:HiddenField ID="hfIPGroupDestination" runat="server" />

</div>
