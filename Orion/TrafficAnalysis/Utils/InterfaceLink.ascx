<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterfaceLink.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_InterfaceLink" %>
<%@ Register TagPrefix="i" TagName="InterfaceLink" Src="~/Orion/Interfaces/Controls/InterfaceLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallStatus" Src="~/Orion/Controls/SmallStatusIcon.ascx" %>

<i:InterfaceLink runat="server" ID="InterfaceLink" TargetURLFormat="/Orion/TrafficAnalysis/NetflowInterfaceDetails.aspx?NetObject=N{0}">
    <Content>
        <orion:SmallStatus runat="server" ID="ifStatus" />
        <img runat="server" id="imgIfType" alt="" src="" /> &nbsp;
        <asp:Literal runat="server" ID="lblName" />
    </Content>
</i:InterfaceLink>
