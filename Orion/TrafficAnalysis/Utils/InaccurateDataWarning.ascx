﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InaccurateDataWarning.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_InaccurateDataWarning" %>
<%@ Import Namespace="Resources" %>

<orion:Include Module="TrafficAnalysis" runat="server" File="TrafficAnalysis.css" />

<asp:Panel CssClass="sw-suggestion sw-suggestion-info warning-inaccurate-data" runat="server" ID="warningBlock" Visible="false">
    <span class="sw-suggestion-icon"></span>
    <span class="warning-inaccurate-data-title">
        <%= NTAWebContent.NTAWEBDATA_LegendContent_WarningMessage_Title %><img src="/Orion/TrafficAnalysis/images/AnimatedDots.gif" style="margin-bottom: -2px"/>
    </span>
    <br />
    <asp:Label runat="server" CssClass="warning-inaccurate-data-message" ID="warningMessage"></asp:Label>
</asp:Panel>
