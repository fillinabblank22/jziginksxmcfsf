<%@ Page Language="C#" AutoEventWireup="false" %>

<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        string ip = Request.QueryString["IPAddress"];
        SolarWinds.Orion.Core.Common.Models.Node node = NodeWorkflowHelper.CreateNode();
        node.Name = ip;
        node.NodeSubType = SolarWinds.Orion.Core.Common.Models.NodeSubType.SNMP;
        node.ObjectSubType = "SNMP";
        node.SNMPVersion = SolarWinds.Orion.Core.Common.Models.SNMPVersion.SNMP2c;
        node.ReadOnlyCredentials.CommunityString = "public";
        NodeWorkflowHelper.Node = node;

        Response.Redirect(string.Format(SolarWinds.Netflow.Web.UIConsts.
            URL_ORION_ADMIN_NODES_ADD_IP_ADDRESS_FORMAT, ip));
    }
</script>