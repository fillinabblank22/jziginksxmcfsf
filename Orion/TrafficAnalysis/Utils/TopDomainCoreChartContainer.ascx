﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopDomainCoreChartContainer.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_TopDomainCoreChartContainer" %>
<%@ Register TagPrefix="netflow" TagName="TopCoreChartContainer" Src="~/Orion/TrafficAnalysis/Utils/TopCoreChartContainer.ascx" %>
<%@ Register TagPrefix="netflow" TagName="OnDemandWarning" Src="~/Orion/TrafficAnalysis/Utils/OnDemandWarning.ascx" %>

<netflow:TopCoreChartContainer runat="server" ID="topCoreChartContainer" OwnerResource="<%# this.OwnerResource %>" />
<netflow:OnDemandWarning ID="onDemandWarning" runat="server" Visible="false" />