﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NumberUpDownControl.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_NumberUpDownControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div class="numberUpDown">
    
    <asp:Label ID="lblTextBefore" runat="server" CssClass="textBefore"></asp:Label>
    
    <asp:TextBox runat="server" id="txbValue" CssClass="value"></asp:TextBox>
    <div class="upDownBtns">
        <span><asp:Button ID="btnValueUp" runat="server" Text="▲" CssClass="btnUp" /></span>
        <span><asp:Button ID="btnValueDown" runat="server" Text="▼" CssClass="btnDown"/></span> 
    </div>
    <cc1:NumericUpDownExtender 
        ID="numExtValue" runat="server"
        TargetControlID="txbValue"
        TargetButtonUpID="btnValueUp"
        TargetButtonDownID="btnValueDown"
        width="30" >
    </cc1:NumericUpDownExtender>
    
    <asp:Label ID="lblTextAfter" runat="server" CssClass="textAfter"></asp:Label>
    
    
    <asp:RequiredFieldValidator ID="vldReqValue" runat="server" 
      CssClass="ntasValidator"
      ControlToValidate="txbValue"
      ErrorMessage="<%$ Resources:NTAWebContent,NTAWEBDATA_AK0_83%>" Display="Dynamic"
       ><a href="#" title="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" 
       alt="<%= Resources.NTAWebContent.NTAWEBDATA_AK0_83%>" class="ntasValIcon" onclick="return false;">&nbsp;</a>
       </asp:RequiredFieldValidator>
    <asp:RangeValidator ID="vldRangValue" runat="server" 
      CssClass="ntasValidator"
      ControlToValidate="txbValue"
      Type="integer" Display="Dynamic"
      ><a runat="server" id="valIcn" href="#" title="" alt="" class="ntasValIcon" onclick="return false;">&nbsp;</a></asp:RangeValidator>
</div>