﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPVersionSelector.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_IPVersionSelector" %>

<style type="text/css">
.IPVersionSelectorItem
{
    padding-left:5px;
    padding-bottom:5px;
}
</style>

<div id="IPVersionSelectorDiv" style="margin-bottom:5px">
    <div class="IPVersionSelectorItem" runat="server" id="BothSelectorDiv" visible="true">
        <asp:RadioButton ID="rbBoth" GroupName="rbGroup" runat="server"/>
    </div>    
    <div class="IPVersionSelectorItem">
        <asp:RadioButton ID="rbIPv4" GroupName="rbGroup" runat="server" />
    </div>
    <div class="IPVersionSelectorItem">
        <asp:RadioButton ID="rbIPv6" GroupName="rbGroup" runat="server" />
    </div>
</div>