﻿using System;
using System.Web.UI;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Web.UI;

public partial class Orion_TrafficAnalysis_Utils_TopDomainCoreChartContainer : UserControl, ITopCoreChartContainer
{
    public TopResourceCoreChartBase OwnerResource { get; set; }

    public Control ChartContainer
    {
        get { return this.topCoreChartContainer.ChartContainer; }
    }

    public Control MessageContainer
    {
        get { return this.topCoreChartContainer.MessageContainer; }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!GlobalSettingsDALCached.InstanceInterface.DnsPersistEnabled)
        {
            this.topCoreChartContainer.Visible = false;
            this.onDemandWarning.Visible = true;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.onDemandWarning.ResourceID = this.OwnerResource.Resource.ID.ToString();
        this.onDemandWarning.ResourceName = this.OwnerResource.Resource.Name;
    }
}