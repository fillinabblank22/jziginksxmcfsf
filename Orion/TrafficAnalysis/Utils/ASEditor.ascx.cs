using System;
using System.Globalization;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Netflow.Web.DAL;
using SolarWinds.Netflow.Web.DALInterfaces;

public partial class Orion_TrafficAnalysis_Utils_ASEditor : UserControl
{
    #region Events

    public event EventHandler Cancel;
    public event EventHandler Validate;
    public event EventHandler ResolveConflicts;
    public event EventHandler EditSuccess;

    #endregion Events

    #region Fields

    private string TITLE_ADD_NEW = Resources.NTAWebContent.NTAWEBCODE_VB0_57;
    private string TITLE_CONFLICT = Resources.NTAWebContent.NTAWEBCODE_VB0_58;
    private string TITLE_EDIT = Resources.NTAWebContent.NTAWEBCODE_VB0_59;
    private static Log _log = new Log();

    #endregion Fields

    #region Properties

    private bool ResolveMode
    {
        get { return (null == ViewState["ResolveMode"]) ? false : (bool)ViewState["ResolveMode"]; }
        set
        {
            ViewState["ResolveMode"] = value;
            this.SetupState();
        }
    }

    public string DescrCID
    {
        get
        {
            return this.tbName.ClientID;
        }
    }

    public string AsID
    {
        get
        {
            return tbID.Text;
        }
        set
        {
            this.tbID.Text = value;
        }
    }

    public string Name
    {
        get { return this.tbName.Text; }
        set { this.tbName.Text = value; }
    }

    public string CountryCode
    {
        get { return this.ddlCountryCode.SelectedItem.Value; }
        set { this.ddlCountryCode.Text = value; }

    }

    public string Organization
    {
        get { return this.tbOrganization.Text; }
        set { this.tbOrganization.Text = value; }
    }

    public string RegistrationDate
    {
        get { return this.tbRegistrationDate.Text; }
        set { this.tbRegistrationDate.Text = value; }
    }

    public string LastUpdated
    {
        get { return this.tbLastUpdated.Text; }
        set { this.tbLastUpdated.Text = value; }
    }

    public bool EditMode
    {
        get { return (null == ViewState["EditMode"]) ? false : (bool)ViewState["EditMode"]; }
        set
        {
            ViewState["EditMode"] = value;
            this.SetupState();
        }
    }

    public string AsIDCollision
    {
        get { return (null == ViewState["AsIDCollision"]) ? null : (string)ViewState["AsIDCollision"]; }
        set { ViewState["AsIDCollision"] = value; }
    }

    #endregion Properties

    public void Clear()
    {
        this.ID = string.Empty;
        this.Name = string.Empty;
        this.Organization = string.Empty;
        this.CountryCode = string.Empty;
        this.LastUpdated = string.Empty;
        this.RegistrationDate = string.Empty;

        this.AsIDCollision = null;
        this.ResolveMode = false;
    }

    protected void OnCancelClick(object sender, EventArgs e)
    {
        if (this.ResolveMode == true)
        {
            this.ResolveMode = false;
            SetupState();
            return;
        }

        if (null != this.Cancel)
            this.Cancel(this, e);
    }

    protected void OnAddClick(object sender, EventArgs e)
    {
        if (this.ResolveMode)
        {
            this.RaiseEvent(this.ResolveConflicts);
            this.ResolveMode = false;
            this.RaiseEvent(this.EditSuccess);
        }
        else
        {
            this.RaiseEvent(Validate);
            if (!String.IsNullOrEmpty(AsIDCollision))
            {
                this.ResolveMode = true;
            }
            else
            {
                this.RaiseEvent(EditSuccess);
            }
        }
    }

    private void RaiseEvent(EventHandler handler)
    {
        if (null != handler)
            handler(this, new EventArgs());
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        SetupState();

        // bind country drop down list
        ddlCountryCode.DataSource = TrafficViewBuilderDAL.GetCountries();
        ddlCountryCode.DataBind();

    }

    private string FillConflictDialog()
    {
        string format = Resources.NTAWebContent.NTAWEBCODE_VB0_60;
        string str = string.Empty;
        try
        {
            AutonomousSystemsDAL asDAL = new AutonomousSystemsDAL();
            AutonomousSystemItem autSyst;
            asDAL.GetAutonomousSystem(Convert.ToInt32(AsID), out autSyst);
            if (autSyst == null)
                return str;

            string regDate = string.Empty;
            string lastUpd = string.Empty;

            if (autSyst.RegistrationDate > DateTime.MinValue)
                regDate = autSyst.RegistrationDate.ToString("d",  CultureInfo.CreateSpecificCulture("en-US"));
            
            if (autSyst.LastUpdated > DateTime.MinValue)
                autSyst.LastUpdated.ToString("d", CultureInfo.CreateSpecificCulture("en-US"));

            str = string.Format(format, autSyst.Name, autSyst.CountryCode, autSyst.Organization, 
                regDate, lastUpd);
        }
        catch (Exception ex)
        {
            _log.ErrorFormat("Exception in fill conflict dialog: {0}", ex);
        }

        return str;
        
    }

    private void SetupState()
    {
        //this.btnAddApp.ImageUrl = string.Format("~/Orion/TrafficAnalysis/images/{0}", this.EditMode ? "Button.Save.gif" : "Button.Save.gif");

        if (this.ResolveMode)
        {
            this.lblTitle.Text = TITLE_CONFLICT;
            this.lblASConflictFooter.Visible = true; 
            this.lblASConflictHeader.Visible = true;
            this.lblASConflictBody.Visible = true;

            this.lblASConflictBody.Text = FillConflictDialog();

            this.tbodyEditor.Visible = false;
        }
        else
        {
            this.lblASConflictFooter.Visible = false;
            this.lblASConflictHeader.Visible = false;
            this.lblASConflictBody.Visible = false;
            
            this.tbodyEditor.Visible = true;
            if (this.EditMode)
            {
                this.lblTitle.Text = TITLE_EDIT;
                this.tbID.Enabled = false;
            }
            else
            {
                this.lblTitle.Text = TITLE_ADD_NEW;
                this.tbID.Enabled = true;
            }
        }
    }
}
