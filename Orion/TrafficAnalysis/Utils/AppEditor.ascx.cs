using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Logging;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Utility;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Contracts.Applications;
using SolarWinds.Netflow.Web.Applications;
using SolarWinds.Netflow.Web.IPGroups.DAL;
using SolarWinds.Netflow.Web.Applications.DAL;
using SolarWinds.Orion.Web.Helpers;


public partial class Orion_TrafficAnalysis_Utils_AppEditor : System.Web.UI.UserControl, System.Web.UI.ICallbackEventHandler
{
    public class AppArgs : EventArgs
    {
        private readonly string _portNumbers;
        private readonly string _desc;
        private readonly int _protocol;

        public string PortNumbers
        {
            get { return _portNumbers; }
        }
        public int Protocol
        {
            get { return _protocol; }
        }
        public string Description
        {
            get { return _desc; }
        }

        public AppArgs(string portNums, string descr, int protocolNum)
        {
            _portNumbers = portNums;
            _desc = descr;
            _protocol = protocolNum;
        }
    }

    #region Events

    public event EventHandler Cancel;
    public event EventHandler Validate;
    public event EventHandler ResolveConflicts;
    public event EventHandler EditSuccess;

    #endregion Events

    #region Fields

    private string TITLE_ADD_NEW = Resources.NTAWebContent.NTAWEBCODE_VB0_65;
    private string TITLE_CONFLICT = Resources.NTAWebContent.NTAWEBCODE_VB0_66;
    private string TITLE_EDIT_SINGLE_TEMPLATE = Resources.NTAWebContent.NTAWEBCODE_VB0_67;
    private string TITLE_EDIT_MULTI = Resources.NTAWebContent.NTAWEBCODE_VB0_68;
    private string TITLE_MONITOR_APP = Resources.NTAWebContent.NTAWEBCODE_VB0_69;

    private const string PASSED = "PASSED";
    private bool _FromResource = false;
    private string _result = string.Empty;
    private IPAddressGroupDAL _ipDAL;
    private ApplicationsDAL _appDAL;
    private static Log _log = new Log();

    #endregion Fields

    #region Properties

    #region CID for Javascript

    public string ProtocolCID
    {
        get
        {
            return this.ddlProtocol.ClientID;
        }
    }

    public string PortsCID
    {
        get
        {
            return this.txtPorts.ClientID;
        }
    }


    public string ApplicationEnabled
    {
        get
        {
            return this.hfApplicationEnabled.Value;
        }
        set
        {
            this.hfApplicationEnabled.Value = value;
        }
    }



    public string ApplicationID
    {
        get
        {
            return this.hfApplicationID.Value;
        }
        set
        {
            this.hfApplicationID.Value = value;
        }
    }

    public string WebId { get; set; }

    public string DescrCID
    {

        get
        {
            return this.txtDescription.ClientID;
        }
    }

    #endregion CID for Javascript

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // bind IP AddressGroups to DropDown lists
        BindIPAddressGroupsDDL();
    }

    public string Ports
    {
        get
        {
            return this.txtPorts.Text;
        }
        set
        {
            this.txtPorts.Text = value;
            this.SetupState();
        }
    }

    public string Description
    {
        get { return this.txtDescription.Text; }
        set { this.txtDescription.Text = value; }
    }

    private string sourceIpAddressViewState
    {
        get { return (string) ViewState["SourceIpAddress"]; }
        set { ViewState["SourceIpAddress"] = value; }
    }

    public string SourceIPAddress
    {
        get
        {
            // get from hidden field
            // we have to store DDL selected value in the hidden field
            // because a new values added by JavaScript are not evaluated on the server side
            if (this.hfIPGroupSource.Value == null || this.hfIPGroupSource.Value == string.Empty)
            {
                return this.ddlSourceIP.SelectedValue;
            }

            return hfIPGroupSource.Value;
        }
        set { sourceIpAddressViewState = value; }
    }

    private string destinationIpAddressViewState
    {
        get { return (string)ViewState["DestinationIpAddress"]; }
        set { ViewState["DestinationIpAddress"] = value; }
    }

    public string DestinationIPAddress
    {
        get
        {
            // get from hidden field
            // we have to store DDL selected value in the hidden field
            // because a new values added by JavaScript are not evaluated on the server side
            if (this.hfIPGroupDestination.Value == null || this.hfIPGroupDestination.Value == string.Empty)
            {
                return this.ddlDestinationIP.SelectedValue;
            }

            return hfIPGroupDestination.Value;
        }
        set { destinationIpAddressViewState = value; }
    }

    public int Protocol
    {
        get
        {
            return Convert.ToInt32(this.ddlProtocol.SelectedValue);
        }
        set
        {
            switch (value)
            {
                case 6:
                    this.ddlProtocol.SelectedIndex = 1;
                    break;
                case 17:
                    this.ddlProtocol.SelectedIndex = 2;
                    break;
                default:
                    this.ddlProtocol.SelectedIndex = 0;
                    break;
            }
        }
    }
    public string IpGroupCollision
    {
        get { return (null == ViewState["IpGroupCollision"]) ? null : (string)ViewState["IpGroupCollision"]; }
        set { ViewState["IpGroupCollision"] = value; }
    }

    public string ApplicationCollision
    {
        get { return (null == ViewState["ApplicationCollision"]) ? null : (string)ViewState["ApplicationCollision"]; }
        set { ViewState["ApplicationCollision"] = value; }
    }

    private bool ResolveMode
    {
        get { return (null == ViewState["ResolveMode"]) ? false : (bool)ViewState["ResolveMode"]; }
        set
        {
            ViewState["ResolveMode"] = value;
            this.SetupState();
        }
    }

    private bool ResolveGroupMode
    {
        get { return (null == ViewState["ResolveGroupMode"]) ? false : (bool)ViewState["ResolveGroupMode"]; }
        set
        {
            ViewState["ResolveGroupMode"] = value;
            this.SetupState();
        }
    }

    public bool FromResourceAndOnDedicatedPage { get; set; }

    public bool FromResource
    {
        set
        {
            this._FromResource = value;
        }
    }

    public bool EditMode
    {
        get { return (null == ViewState["EditMode"]) ? false : (bool)ViewState["EditMode"]; }
        set
        {
            ViewState["EditMode"] = value;
            this.SetupState();
        }
    }

    public bool SinglePortMode
    {
        get { return (null == ViewState["SinglePortMode"]) ? false : (bool)ViewState["SinglePortMode"]; }
        set
        {
            ViewState["SinglePortMode"] = value;
            this.SetupState();
        }
    }

    public string CallBack
    {
        get
        {
            return "function(appString){" + Page.ClientScript.GetCallbackEventReference(this, "appString", "ProcessCallBackSuccess", string.Empty, "ProcessCallBackError", true) + " }";
        }
    }

    public string CallBackGetIPGroups
    {
        get { return "function(param){" + Page.ClientScript.GetCallbackEventReference(this, "param", "ProcessCallBackGetIPGroupsSuccess", string.Empty, "ProcessCallBackGetIPGroupsError", true) + " }"; }
    }


    #endregion Properties

    public void Clear()
    {
        this.Ports = string.Empty;
        this.Description = string.Empty;
        this.Protocol = 0;
        this.ApplicationCollision = null;
        this.IpGroupCollision = null;
        this.ResolveMode = false;
        this.ddlSourceIP.SelectedValue = "ANY_DESTINATION";
        this.ddlDestinationIP.SelectedValue = "ANY_DESTINATION";

        this.hfApplicationID.Value = string.Empty;
        this.hfApplicationEnabled.Value = Boolean.TrueString;
    }

    protected void OnCancelClick(object sender, EventArgs e)
    {
        if (this.ResolveMode == true)
        {
            this.ResolveMode = false;
            SetupState();
            return;
        }
        else if (this.ResolveGroupMode == true)
        {
            this.ResolveGroupMode = false;
            this.IpGroupCollision = "";
            SetupState();
            return;
        }

        if (null != this.Cancel)
            this.Cancel(this, e);
    }

    protected void OnAddClick(object sender, EventArgs e)
    {
        sourceIpAddressViewState = SourceIPAddress;
        destinationIpAddressViewState = DestinationIPAddress;

        if (this.ResolveMode)
        {
            this.RaiseEvent(this.ResolveConflicts);
            this.ResolveMode = false;
            this.RaiseEvent(this.EditSuccess);
        }
        else
        {
            // here to call Application. Update and get exception
            // if exception exists this.ResolveMode = true
            this.RaiseEvent(Validate);
            if (null != ApplicationCollision && string.Empty != ApplicationCollision)
            {
                this.ResolveMode = true;
            }
            else if (IpGroupCollision != null && string.Empty != IpGroupCollision) 
            {
                this.ResolveGroupMode = true;
            }
            else
            {
                this.RaiseEvent(EditSuccess);
            }
        }
    }
    
    private void RaiseEvent(EventHandler handler)
    {
        if (null != handler)
            handler(this, new EventArgs());
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        ScriptManager.GetCurrent(this.Page).Scripts.Add(new ScriptReference("/Orion/TrafficAnalysis/scripts/appeditor.js"));

        // If it is used on Unmonitored Traffic - Monitor Application button
        if (this._FromResource == true)
        {
            if (SolarWinds.Orion.Common.OrionConfiguration.IsDemoServer)
            {
                Response.Write(String.Format("<script language=''javascript''>alert('{0}')</script>", Resources.NTAWebContent.NTAWEBDATA_DemoMSGBox_FunctionalityDisabled));
                Server.Transfer(Request.UrlReferrer.AbsolutePath);
                return;
            }

            btnAddApp.OnClientClick = string.Format("InvokeQuery({0});return false;", CallBack);

            if (!FromResourceAndOnDedicatedPage)
                btnCancel.OnClientClick = "ResetControl(); ModalPopupDisplay(false); return false;";
            else
            {
                string function = String.Format("function getPostBackButton() {{ return '{0}'; }}", btnAddApp.UniqueID);
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "GetPostBackButton", function, true);
            }
            btnAddApp.Text = Resources.NTAWebContent.NTAWEBDATA_AK0_38;
            this.lblTitle.Text = TITLE_MONITOR_APP;

            txtPorts.Enabled = true;
        }

        string callBack = this.Page.ClientScript.GetCallbackEventReference(this, "arg", "ReceiveIPGroupDetails", string.Empty);
        string ipcallback = "function GetIPGroupDetail(arg, context) {" + callBack + ";}";
        this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GetIPGroupDetail", ipcallback, true);

        this._ipDAL = new IPAddressGroupDAL();
        this._appDAL = new ApplicationsDAL();
    }

    private void BindIPAddressGroupsDDL()
    {
        FillDropDownList(this.ddlSourceIP);
        FillDropDownList(this.ddlDestinationIP);

        SetDropDownValue(this.ddlSourceIP, sourceIpAddressViewState);
        SetDropDownValue(this.ddlDestinationIP, destinationIpAddressViewState);
    }

    private static void SetDropDownValue(DropDownList ddl, string address)
    {
        try
        {
            ddl.SelectedValue = WebSecurityHelper.SanitizeHtmlV2(HttpUtility.HtmlEncode(address));
        }
        catch
        {
            ddl.SelectedValue = "ANY_DESTINATION";
        }
    }

    private void FillDropDownList(DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem(Resources.NTAWebContent.NTAWEBCODE_VB0_70, "ANY_DESTINATION"));
        foreach (KeyValuePair<int, IPAddressGroup> ipgroup in _ipDAL.EnumerateGroups(false))
        {
            ListItem li = new ListItem(ipgroup.Value.Name, ipgroup.Value.ID.ToString());
            ddl.Items.Add(li);
        }
    }

    private void FillRepeater()
    {
        Dictionary<int, Application> applications;
        using (var storage = new SessionStorage(Page, WebId))
        {
            applications = storage.Applications;
        }

        this.rptPortConflicts.DataSource = ApplicationsHelper.CreateConflictDataTable(ApplicationCollision, applications);
        this.rptPortConflicts.DataMember = "PortConflicts";
        try
        {
            this.rptPortConflicts.DataBind();
        }
        catch (Exception ex)
        {
            _log.Error("Error binding port conflict repeater", ex);
        }

    }

    private void SetupState()
    {
        this.txtPorts.Attributes.Remove("disabled");

        this.btnAddApp.Visible = !ResolveGroupMode;
        this.btnOk.Visible = ResolveGroupMode;
        this.btnCancel.Visible = !ResolveGroupMode;
        this.lblGroupDeleted.Visible = ResolveGroupMode;
        this.tbodyEditor.Visible = !ResolveGroupMode && !ResolveMode;

        if (this.ResolveMode)
        {
            btnAddApp.Text = Resources.NTAWebContent.NTAWEBDATA_AppEditor_ResolveConflict;

            this.lblTitle.Text = TITLE_CONFLICT;
            this.lblPortConflictFooter.Visible = true;
            this.lblPortConflictHeader.Visible = true;
            this.rptPortConflicts.Visible = true;

            // here to bind info about conflict
            FillRepeater();
        }
        else
        {
            btnAddApp.Text = this.EditMode ? Resources.NTAWebContent.NTAWEBCODE_VB0_108 : Resources.NTAWebContent.NTAWEBDATA_AK0_38;

            this.lblPortConflictFooter.Visible = false;
            this.lblPortConflictHeader.Visible = false;
            this.rptPortConflicts.Visible = false;
            if (this.EditMode)
            {
                if (this.SinglePortMode)
                {
                    this.lblTitle.Text = string.Format(TITLE_EDIT_SINGLE_TEMPLATE, WebSecurityHelper.HtmlEncode(txtPorts.Text));
                    this.txtPorts.Attributes["disabled"] = "true";
                    this.lblMultiPort.Visible = false;
                }
                else
                {
                    this.lblTitle.Text = TITLE_EDIT_MULTI;
                }
            }
            else
            {
                this.lblTitle.Text = TITLE_ADD_NEW;
            }
        }
    }

    #region ICallbackEventHandler Members

    public string GetCallbackResult()
    {
        return _result;
    }

    private Application App(int port, bool tcp, bool udp, string srcIPgroup, string destIPgroup, int pProtocolOld)
    {
        Application app = new Application(port);

        bool appExist = _appDAL.GetApplication(port, tcp, udp, out app);

        if (true == appExist)
        {
            // just update application
            app.Enabled = true;
            UpdateGroups(ref app, srcIPgroup, destIPgroup);
            app.Protocols = NetFlowCommon.ResolveProtocols(this.Protocol);
            app.Name = this.Description;
        }
        else
        {
            // create new application
            app = new Application();
            app.Enabled = true;
            app.Ports.Add(new ApplicationPort(port, ApplicationProperties.ConditionDirection.Both));
            app.Protocols = NetFlowCommon.ResolveProtocols(pProtocolOld);
            app.Name = this.Description;
            UpdateGroups(ref app, srcIPgroup, destIPgroup);
        }

        return app;
    }

    private void UpdateGroups(ref Application app, string srcIPgroup, string destIPgroup)
    {
        int src = 0;
        int dst = 0;

        Int32.TryParse(srcIPgroup, out src);
        Int32.TryParse(destIPgroup, out dst);

        if (src > 0)
        {
            app.IPGroups.Add(new ApplicationGroup(src, ApplicationProperties.ConditionDirection.Source));
        }
        else if (src == 0)
        {
            ApplicationGroup srcGroup = app.IPGroups.FirstOrDefault(g => g.Direction == ApplicationProperties.ConditionDirection.Source);
            app.IPGroups.Remove(srcGroup);
        }

        if (dst > 0)
        {
            app.IPGroups.Add(new ApplicationGroup(dst, ApplicationProperties.ConditionDirection.Destination));
        }
        else if (dst == 0)
        {
            ApplicationGroup dstGroup = app.IPGroups.FirstOrDefault(g => g.Direction == ApplicationProperties.ConditionDirection.Destination);
            app.IPGroups.Remove(dstGroup);
        }
    }





    private void MonitorApplication(string pSourceIPGroup, string pDestinationIPGroup, int pDetectedProtocol)
    {

        int port = Convert.ToInt32(this.Ports);
        bool res = true;

        bool tcp; bool udp;
        NetFlowCommon.GetProtocols(pDetectedProtocol, out tcp, out udp);
        Application app = App(port, tcp, udp, pSourceIPGroup, pDestinationIPGroup, pDetectedProtocol);

        // update
        app.Protocols = NetFlowCommon.ResolveProtocols(this.Protocol);
        app.Name = this.Description;

        try
        {
            _appDAL.UpdateApplication(app);
            ApplyApplicationsChanges();
        }
        catch (ApplicationCollisionException ex)
        {
            // collision 
            _log.Info("Application collision", ex);

            // convert ex.Items to JSON
            MemoryStream stream1 = new MemoryStream();
            List<ApplicationsUtils.JsonCollisionData> jsonColData = new List<ApplicationsUtils.JsonCollisionData>();
            foreach (ApplicationCollision col in ex.Items)
            {
                jsonColData.Add(new ApplicationsUtils.JsonCollisionData(col));
            }

            // optimize JsonCollisionList
            jsonColData = ApplicationsUtils.MakeRanges(jsonColData);

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<ApplicationsUtils.JsonCollisionData>));
            ser.WriteObject(stream1, jsonColData);

            stream1.Position = 0; string jsonString = string.Empty;
            StreamReader sr = new StreamReader(stream1);
            jsonString = sr.ReadToEnd();
            stream1.Close();
            sr.Close();

            // COLLISION~~json~~ipGroupSrc~~ipGroupDst~~protocol(int)~~appName
            this._result = "COLLISION~~" + jsonString + "~~" + pSourceIPGroup + "~~" + pDestinationIPGroup + "~~" + this.Protocol + "~~" + this.Description;
            res = false;
        }

        if (true == res)
        {
            this._result = PASSED;
        }
    }

    private void ApplyApplicationsChanges()
    {
        try
        {
            BusinessLayerHelper.ApplyApplicationsChanges();
        }
        catch { }
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        // parse ID, Name and Protocol and call event to store to DB
        if (eventArgument == null || eventArgument == string.Empty)
        {
            _result = "Unknown event argument!";
            return;
        }

        //appID
        if (eventArgument.Contains("ResolveCollisions"))
        {
            if (DemoHelper.CheckIfDemoAndWarnUser(this.Page, Resources.NTAWebContent.NTAWEBCODE_VB0_72, true))
            {
                _result = "DEMO";
                return;
            }

            //we want Application info
            //eventArgument = eventArgument.Remove(0, 18);

            // ResolveCollisions~~appID~~srcIPGroup~~dstIPGroup~~appName
            string[] result2 = eventArgument.Split(new string[] { "~~" }, StringSplitOptions.None);

            // ResolveCollisions~~appID [1]
            int appID = Convert.ToInt32(result2[1]);

            bool tcp; bool udp;
            NetFlowCommon.GetProtocols(Protocol, out tcp, out udp);
            Application app = App(appID, tcp, udp, result2[2], result2[3], Protocol);
            app.Name = result2[4];  // in case of rename

            try
            {
                _appDAL.UpdateApplication(app);
            }
            catch (ApplicationCollisionException ex)
            {
                // probably collision
                _appDAL.ResolveCollision(ex);
            }

            try
            {
                _appDAL.UpdateApplication(app);
            }
            catch (ApplicationCollisionException ex)
            {
                _log.Error("Resolving of  collisions failed", ex);
            }

            // add a applications
            _result = "PASSED";

            return;
        }

        if (eventArgument.Contains("GetIPGroup"))
        {
            //we want IP Group info
            eventArgument = eventArgument.Remove(0, 11);
            int groupID;

            if (false == Int32.TryParse(eventArgument, out groupID))
            {
                return;
            }

            //_result 
            foreach (Range range in _ipDAL.GetGroupByID(groupID).Ranges)
            {
                _result += range.ToString() + ", ";
            }

            _result = _result.Remove(_result.LastIndexOf(", "));

            return;
        }

        // port<.prot2><.prot3>~~description~~Protocol~~srcIP~~dstIP~~detectedProtocol<~~SUBMIT>
        string[] result = eventArgument.Split(new string[] { "~~" }, StringSplitOptions.None);

        if (result.Length == 0)
        {
            _result = "Cannot parse event argument";
            return;
        }

        // case with app and dot (105072.) instead of (105072) ...
        this.Ports = result[0].Split('.')[0];
        this.Description = WebSecurityHelper.SanitizeHtml(result[1]);
        string prot = result[2];
        this.SourceIPAddress = result[3];
        this.DestinationIPAddress = result[4];
        if (NetFlowCommon.GetProtocolNumber(prot) > -1)
        {
            this.Protocol = NetFlowCommon.GetProtocolNumber(prot);
        }

        if (this.Ports == null || this.Ports == string.Empty)
        {
            this._result = "Invalid port!";
            return;
        }

        int oldProt = 0;
        Int32.TryParse(result[5], out oldProt);

        if (DemoHelper.CheckIfDemoAndWarnUser(this.Page, Resources.NTAWebContent.NTAWEBCODE_VB0_72, true))
        {
            _result = "DEMO";
            return;
        }

        MonitorApplication(result[3], result[4], oldProt);
    }



    #endregion

}
