using System;
using SolarWinds.Netflow.Web;
using SolarWinds.Orion.Web;
using SolarWinds.Netflow.Common;
using SolarWinds.Logging;

public partial class Orion_TrafficAnalysis_Utils_OnDemandWarning : System.Web.UI.UserControl
{
    private static Log _log = new Log("OnDemandWarning");

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

    public string ResourceID { get { return this.hfResourceID.Value; } set { this.hfResourceID.Value = value; } }

    public string LearnMoreLink => SolarWinds.Orion.Web.Helpers.HelpHelper.GetHelpUrl(ProductInfo.ShortModuleName, "OrionNetFlowPHSettingsDNSNetBIOSResolution");

    public string ResourceName
    {
        get
        {
            if (!string.IsNullOrEmpty(hfResourceName.Value))
            {
                return hfResourceName.Value;
            }

            return Resources.NTAWebContent.NTAWEBCODE_TM0_46;
        }
        set
        {
            this.hfResourceName.Value = value;
        }
    }

    protected void ButtonRemoveResourceClick(object sender, EventArgs e)
    {
        _log.DebugFormat("On Demand DNS warning page: Remove resource button clicked. Delete resource no.: '{0}'. ", this.ResourceID); 

        // remove this resource from this view
        ResourceManager.DeleteById(Convert.ToInt32(this.ResourceID));

        // on detached resource this redirect should be handled
        if (UIHelper.IsDetached(Request.Url.PathAndQuery))
        {
            Response.Redirect("/Orion/TrafficAnalysis/SummaryView.aspx");
        }
        else
        {
            Response.Redirect(this.Request.Url.PathAndQuery);
        }
    }
}
