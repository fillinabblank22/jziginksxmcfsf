﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Common;

public partial class Orion_TrafficAnalysis_Utils_TopCoreChartContainer : UserControl, ITopCoreChartContainer
{
    public TopResourceCoreChartBase OwnerResource { get; set; }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.chartDebugInfoDiv.Visible = GlobalSettingsDALCached.CoreChartsShowDebugInfo;
    }

    public Control ChartContainer
    {
        get { return this.chartPlaceHolder; }
    }

    public Control MessageContainer 
    {
        get { return this.messagePlaceHolder; }
    }
}