using System;
using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web.Reporting.DAL;
using SolarWinds.Netflow.Web.UI;
using System.Collections.Generic;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_TrafficAnalysis_Utils_FilterListUnified : System.Web.UI.UserControl
{
    private IFilterCollection filters { get { return this.view.NetflowObject.Filters; } }
    private NetflowView view;

    protected struct FilterUiItem
    {
        public NetflowFilterWithNameBase Filter;        
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.view = this.Page as NetflowView;
        if (this.view == null)
            throw new InvalidOperationException("Filter list can be placed only on NetFlowView pages.");//[Localization] This is exception which can appears only during development when the control is used in wrong way

        this.HandleSource();
        filtersRpt.DataBind();
    }

    protected IEnumerable<FilterUiItem> GetFiltersToShow()
    {
        List<FilterUiItem> result = new List<FilterUiItem>();

        foreach (INetflowFilter filter in this.filters)
        {
            if (filter is NetflowFilterWithNameBase && filter.GetType() != filters.ViewDefinitionFilter.GetType())
            {
                result.Add(new FilterUiItem() { Filter = filter as NetflowFilterWithNameBase });
            }
        }
        
        return result;
    }

    private void HandleSource()
    {
        if (this.filters is UniversalFilterCollection)
        {
            traversingThroughDiv.Visible = false;
            return;
        }
        // we checked this earlier in OnInit
        if (view.ViewInfo.ViewType.Equals("NetflowNodeDetails", StringComparison.InvariantCultureIgnoreCase) ||
            view.ViewInfo.ViewType.Equals("NetflowInterfaceDetails", StringComparison.InvariantCultureIgnoreCase))
        {
            this.traversingThroughDiv.Visible = false;
            return;
        }

        this.traversingThroughDiv.Visible = true;

        if (filters.NetflowSource is InterfaceFilter)
        {
            this.phInterfaceSource.Visible = true;
            this.interfaceLink.InterfaceID = filters.NetflowSource.FilterID;
            this.interfaceLink.Content.Controls.Add(
                new LiteralControl(WebSecurityHelper.HtmlEncode(LookupFunctionsDAL.LookupInterfaceName(((InterfaceFilter)this.filters.NetflowSource).InterfaceID))));
            this.interfaceLink.TargetURLFormat = "/Orion/TrafficAnalysis/NetflowInterfaceDetails.aspx?NetObject=N{0}";
        }
        else
        {
            this.phInterfaceSource.Visible = false;
        }

        this.nodeLink.NodeID = string.Format("N:{0}", filters.NetflowSource.NodeID);
        nodeLink.TargetURLFormat = "/Orion/TrafficAnalysis/NetflowNodeDetails.aspx?NetObject=N{0}";
        nodeLink.Content.Controls.Add(new LiteralControl(WebSecurityHelper.HtmlEncode(LookupFunctionsDAL.LookupNodeName(this.filters.NetflowSource.NodeID))));
    }
}
