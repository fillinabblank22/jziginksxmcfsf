﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using Resources;
using SolarWinds.Netflow.Common;
using SolarWinds.Netflow.Common.CBQoS;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.DAL.CBQoS;
using SolarWinds.Netflow.Web.Reporting.ReportingDAL;

public partial class Orion_TrafficAnalysis_Utils_CBQoSInformations : System.Web.UI.UserControl
{
    // policy direction (input, output)
    readonly string POLICY_DIRECTION_OUTPUT_SENTENCE = Resources.NTAWebContent.NTAWEBCODE_AK0_13;
    readonly string POLICY_DIRECTION_INPUT_SENTENCE = Resources.NTAWebContent.NTAWEBCODE_AK0_14;

    // no active policy
    CBQoSPolicyDetail[] _policies;

    protected bool Empty
    {
        get
        {
            EnsureData();
            return _policies.Length == 0;
        }
    }

    protected string GetPolicyIcon()
    {
        return IconPathHelper.GetCBQoSIconPolicyPath();
    }
    
    protected string GetPolicyIconDirection(CBQoSPolicyDirection dir)
    {
        return IconPathHelper.GetCBQoSIconPath(dir == CBQoSPolicyDirection.Input ? 
            IconPathHelper.ICON_POLICY_DIRECTION_INPUT : 
            IconPathHelper.ICON_POLICY_DIRECTION_OUTPUT);
    }

    protected string GetPolicyDirectionSentence(CBQoSPolicyDirection dir)
    {
        return (dir == CBQoSPolicyDirection.Input ?
            POLICY_DIRECTION_INPUT_SENTENCE :
            POLICY_DIRECTION_OUTPUT_SENTENCE);
    }
    
    [PersistenceMode(PersistenceMode.Attribute)]
    public int InterfaceID { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int NodeID { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public int PolicyID { get; set; }

    [PersistenceMode(PersistenceMode.Attribute)]
    public double InterfaceSpeed { get; set; }

    protected override void OnLoad(EventArgs e)
    {
        // obtain data
        EnsureData();

        // hide if CBQoS is not supported
        if (!CBQoSDAL.IsCBQoSEnabled(NodeID, InterfaceID))
        {
            if (this.Parent != null && this.Parent.Parent != null)
            {
                Parent.Parent.Visible = false;
                return;
            }
        }
        base.OnLoad(e);

        dataEmpty.Visible = Empty;
        policies.DataSource = _policies;
        policies.DataBind();
    }

    /// <summary>
    /// Reads data from database and set variables
    /// </summary>
    private void EnsureData()
    {
        if (this._policies == null)
        {
            this._policies = new CBQoSDetailDAL().GetCBQoSConfigurationDetails(NodeID, InterfaceID, PolicyID)
                .OrderBy(x => x.Direction) // first input
                .ToArray();
        }
    }

    protected string GetPolicyRateUnits()
    {
        switch (CBQoSGlobalSettingsDALCached.CBQoSPolicyDetailsRateLayout)
        {
            case CbqosPolicyRateLayout.Default:
                return NTAWebContent.CBQoSInformation_Policies_Header_Units_Both;
            case CbqosPolicyRateLayout.Percentage:
                return NTAWebContent.CBQoSInformation_Policies_Header_Units_Percent;
            case CbqosPolicyRateLayout.Bps:
                return NTAWebContent.CBQoSInformation_Policies_Header_Units_Bps;
            default:
                throw new ArgumentOutOfRangeException("Incorrect value of Policy Rate Layout");
        }
    }
}
