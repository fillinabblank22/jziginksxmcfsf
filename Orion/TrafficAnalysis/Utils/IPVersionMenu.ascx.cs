﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Common.Filters;

public partial class Orion_TrafficAnalysis_Utils_IPVersionMenu : System.Web.UI.UserControl
{
    private NetflowView netflowView;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        netflowView = (this.Page as NetflowView);

#warning When we will use NetflowView.Master only for NTA Views (and not for TVB), we should throw exception here
        if (netflowView == null) //If we are not on netflowView we have to hide this control (this control is in master page)
        {
            this.Visible = false;
            return;
        }
        
        this.ipVersionSelector.IPVersion = this.netflowView.NetflowObject.Filters.IPVersion;
    }

    public void Submit_Click(object sender, EventArgs e)
    {
        this.netflowView.NetflowObject.Filters.IPVersion = this.ipVersionSelector.IPVersion;

        Response.Redirect(this.netflowView.GetURLForFilterID(this.netflowView.NetflowObject.Filters.FilterID));
    }
}