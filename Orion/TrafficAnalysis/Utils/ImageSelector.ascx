﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageSelector.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_ImageSelector" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<orion:Include ID="IncludeJQuery" runat="server" Framework="jQuery" FrameworkVersion="1.7.1" />
<script type="text/javascript">
    
    // image is selected after page is loaded
    $(document).ready(function () {
        
        $("[id$='_panel']").click(function () {
            
            // deselect all images (make border img-selector-unselected)
            $("[id$='_panel']").removeClass("img-selector-selected");

            // select current image and make it img-selector-selected
            $(this).addClass("img-selector-selected");

            // find index of the picture
            var index = $("[id$='_panel']").index(this);

            // store selected value to hidden field for server processing after submit
            $("[id$='_hidden']").val(index);
        });

        // when page is loaded we need to select image which should be selected
        $("[id$='_panel']:eq(<%=SelectedIndexJS %>)").click();
    }); 
</script>

<asp:HiddenField runat="server" ID="hidden" />

<asp:Repeater ID="repeater" runat="server" >
    <HeaderTemplate>
        <table border="0" cellspacing="0" cellpadding="5" >
        <tr>
            
    </HeaderTemplate>

    <ItemTemplate>
        <td align="center" class="img-selector" valign="top">
            <asp:Panel ID="panel" runat="server" CssClass="img-selector-item" HorizontalAlign="Center">
                <asp:Image ID="imgItem" runat="server"  ToolTip='<%# DataBinder.Eval(Container.DataItem, "Text") %>' ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Value") %>'  /><br />
            </asp:Panel>
            <asp:Label runat="server" ID="lbl" Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>' />
        </td>
    </ItemTemplate>

    <FooterTemplate>
        <td class="img-selector"></td>
        </tr>
        </table>
    </FooterTemplate>

</asp:Repeater>