﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FlowDirectionMenu.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_FlowDirectionMenu" %>
<%@ Register TagPrefix="nta" TagName="FlowDirectionSelector" Src="~/Orion/TrafficAnalysis/Utils/FlowDirectionSelector.ascx" %>


<div style="margin-left:5px; display:inline;">
    <div class="vert-block">
        <span id="FlowDirectionLabel" class="menuFilterLabel"><%=  Resources.NTAWebContent.NTAWEBCODE_TM0_95 %></span> 
        <br />
        <span id="FlowDirectionTitle" class="menuSelectedFilter"><%=this.flowDirectionSelector.CurrentDirectionText %></span>
    </div>
    <a id="FlowDirectionDropDown" title="<%=  Resources.NTAWebContent.NTAWEBDATA_VB1_26 %>" href="#" onclick="ShowMenu('FlowDirectionMenu','FlowDirectionTitle'); return false;" class="PeriodIcon">&nbsp;</a>
</div>


<div id="FlowDirectionMenu" style="display: none; width:200px; padding:5px; line-height:17px;" >
    <b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_117%></b><br /><br />
    <nta:FlowDirectionSelector runat="server" ID="flowDirectionSelector" />
    
    <div style="display: inline; float: right">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="Submit_Click" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClientClick="HideMenu('FlowDirectionMenu'); return false;" DisplayType="Secondary" />
    </div>
    
</div>
