﻿using System;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using SolarWinds.Logging;
using SolarWinds.Netflow.Common;

public partial class Orion_TrafficAnalysis_Utils_InaccurateDataWarning : UserControl
{
    private static readonly Log log = new Log();
    private static readonly JavaScriptSerializer serializer = new JavaScriptSerializer();

    public string WarningMessage { get; set; }

    public int ResourceId { get; set; }

    public string FilterID
    {
        get { return this.warningBlock.Attributes["data-filter-Id"]; }
        set { this.warningBlock.Attributes["data-filter-Id"] = value; }
    }

    public NetflowUpdateQueryType[] UpdateTypes
    {
        get
        {
            return serializer.Deserialize<NetflowUpdateQueryType[]>(this.warningBlock.Attributes["data-update-types"]);
        }
        set
        {
            this.warningBlock.Attributes["data-update-types"] = serializer.Serialize(value.Select(v => (int)v));
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        log.VerboseFormat("Displaying {0}accurate data for resource ID: {1}. ",
            string.IsNullOrEmpty(WarningMessage) ? "" : "in", ResourceId);

        if (!string.IsNullOrWhiteSpace(this.WarningMessage))
        {
            this.warningBlock.Visible = true;
            this.warningMessage.Text = this.WarningMessage;
        }
    }
}