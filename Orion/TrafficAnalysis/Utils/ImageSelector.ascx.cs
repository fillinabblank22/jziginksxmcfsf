﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SolarWinds.Netflow.Web.Reporting;
using SolarWinds.Netflow.Web.Utility;

public partial class Orion_TrafficAnalysis_Utils_ImageSelector : System.Web.UI.UserControl
{
    
    #region Fields

    private ListItemCollection m_DataList;  // obtained collection 
    private int m_Index;                    // index of selected item

    #endregion Fields

    #region Properties
    
    /// <summary>
    /// Index of selected item
    /// </summary>
    public int SelectedIndex {
        get
        {
            string s = hidden.Value;
            try
            {
                return Convert.ToInt32(s);    
            }
            catch (Exception)
            {
                return 0;
            }
        }
        set
        {
            repeater.DataSource = DataList;
            repeater.DataBind();
            this.m_Index = value;
        }
    }

    /// <summary>
    /// Selected Index for JavaScript/JQuery
    /// </summary>
    public string SelectedIndexJS
    {
        get
        {
            return m_Index.ToString();
        }
    }

    /// <summary>
    /// Collection of Items
    /// </summary>
    public ListItemCollection DataList
    {
        get
        {
            if (m_DataList == null)
            {
                m_DataList = new ListItemCollection();
            }
            return this.m_DataList;
        }
        set
        {
            this.m_DataList = value;
        }
    }

    #endregion Properties
}
