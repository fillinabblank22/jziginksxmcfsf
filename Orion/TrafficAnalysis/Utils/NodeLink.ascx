<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NodeLink.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_NodeLink" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>
<%@ Register TagPrefix="orion" TagName="SmallNodeStatus" Src="~/Orion/Controls/SmallNodeStatus.ascx" %>

<npm:NodeLink runat="server" ID="NodeLink" TargetURLFormat="/Orion/TrafficAnalysis/NetflowNodeDetails.aspx?NetObject=N{0}">
    <Content>
        <orion:SmallNodeStatus runat="server" ID="nodeStatus" />
        <img runat="server" id="imgVendor" alt="" src="" /> &nbsp;
        <asp:Literal runat="server" ID="lblName" />
    </Content>
</npm:NodeLink>
