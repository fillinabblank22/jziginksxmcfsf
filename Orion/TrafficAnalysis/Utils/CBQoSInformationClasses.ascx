﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CBQoSInformationClasses.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_CBQoSInformationClasses" %>
<%@ Register TagPrefix="netflow" Assembly="NetflowWeb" Namespace="SolarWinds.Netflow.Web.UI" %>

<%@ Import Namespace="SolarWinds.Netflow.Web.DAL.CBQoS" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>

<orion:Include ID="Include1" runat="server" Module="TrafficAnalysis" File="NetFlow.css" />


<asp:Repeater ID="RepeaterClasses" runat="server" DataSource='<%# CBQoSClasses %>'>
    <ItemTemplate>                                
        <tr id="Class_<%# Eval("PolicyId")%>_Level_<%# Level %>" class="<%# GetRowCssClass(Container.ItemIndex) %>" style="<%# GetBorderStyle() %>">
            <td style='width: 105px; border-style:none;padding-left:<%# GetRowIdent(false) %>;'>
                <a title='<%# string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_19, Eval("Name"))%>' style="background-image:url('<%# GetIconClassMap()%>')" class="ClassMapIcon" href='<%# MakeLink(HttpUtility.UrlEncode(Eval("PolicyID").ToString())) %>'>
                    <%#Eval("Name") %>
                </a>
            </td>
            <td style='border-style:none; white-space: nowrap;'>
                <%# GetDisplayRate((CBQoSClassDetail) Container.DataItem) %>
            </td>
            <td style='width:75px; text-align:left; border-style:none;'>
                <%# Eval("LastHourRate") %>
            </td>

            <netflow:InlineBar ID="InlineBar4" runat='server' Percentage='<%# (double)Eval("LastHourRatePerc") %>' HideBorder="true" TooltipFormat='<%# Resources.NTAWebContent.NTAWEBDATA_CBQoSLegendPolicies_InlineBar_TooltipFormat %>'></netflow:InlineBar>
            
            <td style='width:75px; text-align:left; border-style:none;'>
                <%# Eval("LastDayRate") %>
            </td>
            <netflow:InlineBar ID="InlineBar5" runat='server' Percentage='<%# (double)Eval("LastDayRatePerc") %>' HideBorder="true" TooltipFormat='<%# Resources.NTAWebContent.NTAWEBDATA_CBQoSLegendPolicies_InlineBar_TooltipFormat %>'></netflow:InlineBar>
        </tr>

        <asp:Repeater ID="RepeaterNestedPolicies" runat="server" DataSource='<%# Eval("NestedPolicies") %>' OnItemDataBound="RepeaterNestedPolicies_ItemDataBound">
            <ItemTemplate>                        
                <tr id="Policy_<%# Eval("PolicyMapId")%>_Level_<%# Level %>" style="border-top: 1pt solid #D5D5D5;" class="<%# GetRowCssClass(((RepeaterItem)Container.Parent.Parent).ItemIndex) %>">
                    <td style='width: 105px; border-style:none;padding-left:<%# GetRowIdent(true) %>;' colspan='6'>
                        <label title='<%# string.Format(Resources.NTAWebContent.NTAWEBCODE_AK0_17, Eval("Name"))%>' style="background-image:url('<%# GetPolicyIcon()%>')" class="ClassMapIcon"><%#Eval("Name") %></label>
                    </td>
                </tr>
                <div id="nestedClassesContainer" runat="server">
                        
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </ItemTemplate>
</asp:Repeater>