﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IPVersionMenu.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_IPVersionMenu" %>
<%@ Register TagPrefix="nta" TagName="IPVersionSelector" Src="~/Orion/TrafficAnalysis/Utils/IPVersionSelector.ascx" %>

<div style="margin-left:5px; display:inline;">
    <div class="vert-block">
        <span id="IPVersionLabel" class="menuFilterLabel"><%=  Resources.NTAWebContent.NTAWEBCODE_FlowNavigator_IP_Version %></span> 
        <br />
        <span id="IPVersionTitle" class="menuSelectedFilter"><%=this.ipVersionSelector.CurrentIPVersionText %></span>
    </div>
    <a id="IPVersionDropDown" title="IP Version Selector" href="#" onclick="ShowMenu('IPVersionMenu','IPVersionTitle'); return false;" class="PeriodIcon">&nbsp;</a>
</div>


<div id="IPVersionMenu" style="display: none; width:200px; padding:5px; line-height:17px;" >
    <b><%= Resources.NTAWebContent.NTAWEBDATA_TM0_121%></b><br /><br />
    <nta:IPVersionSelector runat="server" ID="ipVersionSelector" />
    
    <div style="display: inline; float: right">
        <orion:LocalizableButton runat="server" ID="btnSubmit" LocalizedText="Submit" OnClick="Submit_Click" DisplayType="Primary" />
        <orion:LocalizableButton runat="server" ID="btnCancel" LocalizedText="Cancel" OnClientClick="HideMenu('IPVersionMenu'); return false;" DisplayType="Secondary" />
    </div>
    
</div>
