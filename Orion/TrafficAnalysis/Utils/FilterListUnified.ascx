<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterListUnified.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_FilterListUnified" %>
<%@ Register TagPrefix="netflow" TagName="FilterListItem" Src="~/Orion/TrafficAnalysis/Utils/FilterListItem.ascx" %>
<%@ Register TagPrefix="npm" TagName="InterfaceLink" Src="~/Orion/Interfaces/Controls/InterfaceLink.ascx" %>
<%@ Register TagPrefix="npm" TagName="NodeLink" Src="~/Orion/NetPerfMon/Controls/NodeLink.ascx" %>

<div style="font-size:8pt">
    <asp:Repeater runat="server" ID="filtersRpt" DataSource="<%# this.GetFiltersToShow() %>">
        <ItemTemplate>
            <span style="white-space:nowrap"><netflow:FilterListItem runat="server" ID="filterItem" Filter="<%# ((FilterUiItem)Container.DataItem).Filter %>"/></span>
        </ItemTemplate>
        <SeparatorTemplate>
            <span style="white-space:nowrap">&nbsp;|&nbsp;</span>
        </SeparatorTemplate>
    </asp:Repeater>
    
    <div runat="server" id="traversingThroughDiv" style="font-size:8pt">
        <%= Resources.NTAWebContent.NTAWEBDATA_AK0_24%><u><b><npm:NodeLink runat="server" id="nodeLink"/></b></u>
        <asp:PlaceHolder runat="server" ID="phInterfaceSource">
            - <u><b><npm:InterfaceLink runat="server" id="interfaceLink" /></b></u>
        </asp:PlaceHolder>    
    </div>
</div>