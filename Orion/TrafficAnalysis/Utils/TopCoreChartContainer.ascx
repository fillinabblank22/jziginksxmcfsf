﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopCoreChartContainer.ascx.cs" Inherits="Orion_TrafficAnalysis_Utils_TopCoreChartContainer" %>
<%@ Import Namespace="SolarWinds.Netflow.Web.Utility" %>
<%@ Register TagPrefix="nta" TagName="IncludeFormatters" Src="~/Orion/TrafficAnalysis/CoreCharts/IncludeFormatters.ascx" %>

<asp:PlaceHolder runat="server" ID="chartPlaceHolder"></asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="messagePlaceHolder"></asp:PlaceHolder>
<nta:IncludeFormatters runat="server"></nta:IncludeFormatters>

<div id="chartDebugInfoDiv" runat="server">
<b>Debug information:</b><br />
Top Items: <%= this.OwnerResource.Descriptor.Parameters.ItemCount %> <br />
ChartType: <%= this.OwnerResource.Descriptor.Parameters.ChartType.ToString()%>
(<%= this.OwnerResource.Descriptor.Parameters.ChartType == ChartType.Area ? this.OwnerResource.Descriptor.Parameters.AreaChartType.ToString() : ""%>) <br/>
Use time period from view: <%= this.OwnerResource.Descriptor.Parameters.UsePeriodFromView%> <br />
Time period: <%= this.OwnerResource.Descriptor.Parameters.UsePeriodFromView ? "-" : this.OwnerResource.Descriptor.Parameters.ResourceTimePeriod%> <br />
Resource Style: <%= this.OwnerResource.Descriptor.Parameters.ChartStyle.ToString()%> <br/>
Flow Direction: <%= this.OwnerResource.Descriptor.Parameters.ChartFlowDirection.ToString()%>
</div>