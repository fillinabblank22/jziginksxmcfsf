using System;
using System.Web.UI;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.Reporting.DNS;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.Web.Helpers;

public partial class Orion_Netflow_ConversationDetails : NetflowDetailView
{
    public override string ViewType
    {
        get { return "NetFlow Conversation"; }
    }

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;
        this.convResHost.NetflowConversation = (NetflowConversation) this.NetObject;

        if (!Page.IsPostBack)
        {
            ConversationDescriptor.HostName1 = convResHost.NetflowConversation.Hostname1;
            ConversationDescriptor.IPAddress1 = convResHost.NetflowConversation.IPAddress1;
            ConversationDescriptor.HostName2 = convResHost.NetflowConversation.Hostname2;
            ConversationDescriptor.IPAddress2 = convResHost.NetflowConversation.IPAddress2;
        }

        base.OnInit(e);
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }

        var host1 = OnDemandDns.Instance.GetCached(convResHost.NetflowConversation.Hostname1, convResHost.NetflowConversation.IPAddress1).ToString();
        var host2 = OnDemandDns.Instance.GetCached(convResHost.NetflowConversation.Hostname2, convResHost.NetflowConversation.IPAddress2).ToString();
        var conversationName = WebSecurityHelper.HtmlEncode(string.Format(Resources.NTAWebContent.NTAWEBCODE_VB1_48, host1, host2));
        Page.Title = $"{ViewInfo.ViewTitle} - {conversationName}";
    }
}
