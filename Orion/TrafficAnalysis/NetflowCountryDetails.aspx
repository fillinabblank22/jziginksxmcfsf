<%@ Page Language="C#" MasterPageFile="NetflowView.master" AutoEventWireup="true" CodeFile="NetflowCountryDetails.aspx.cs" Inherits="Orion_Netflow_CountryDetails" Title="Untitled Page" %>
<%@ Register Namespace="SolarWinds.Netflow.Web.UI" Assembly="NetflowWeb" TagPrefix="netflow" %>
<%@ Register TagPrefix="orion" TagName="ResourceContainer" Src="~/Orion/ResourceContainer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.ViewInfo.ViewTitle) %> - 
    <a href="<%= this.UnfilteredViewLink %>">
        <%= SolarWinds.Orion.Web.Helpers.WebSecurityHelper.HtmlEncode(this.NetObject.Name) %>
    </a>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="NetflowContent">
    <netflow:CountryResourceHost runat="server" ID="countryResHost">
        <orion:ResourceContainer runat="server" ID="resourceContainer" />
    </netflow:CountryResourceHost>
</asp:Content>