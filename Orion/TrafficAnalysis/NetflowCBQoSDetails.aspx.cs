﻿using System;
using System.Web.UI;
using SolarWinds.Netflow.Common.Filters;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Orion.NPM.Web;

public partial class Orion_Netflow_NetflowCBQoSDetails : NetflowDetailView, IInterfaceProvider
{
    public override string ViewType
    {
        get { return "CBQoS"; }
    }

    protected override void OnInit(EventArgs e)
    {
        _start = DateTime.Now;

        this.cbQoSResHost.NetflowCBQoS = this.NetObject as NetflowCBQoS;
        
        this.nodeLink.NodeID = this.NetflowObject.Node.NetObjectID;
        this.nodeLink.Content.Controls.Add(new LiteralControl(this.NetflowObject.Node.Name));

        string tempInterfaceLink;
        if (NetflowObject.Filters.NetflowSource is InterfaceFilter)
            tempInterfaceLink = NetflowObject.NetflowSourceID;
        else
        {
            tempInterfaceLink = this.NetflowObject.NetObjectID.Substring(
                this.NetflowObject.NetObjectID.IndexOf(";") + 1);
            if (tempInterfaceLink.Contains(";T"))
            {
                tempInterfaceLink = tempInterfaceLink.Remove(tempInterfaceLink.IndexOf(";T"));
            }
        }
        
        this.interfaceLink.InterfaceID = tempInterfaceLink;
        if (this.NetflowObject.Interface != null)
        {
            this.interfaceLink.Content.Controls.Add(new LiteralControl(this.NetflowObject.Interface.Caption));
        }

        this.Page.Form.Attributes["autocomplete"] = "off";

        base.OnInit(e);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    public Interface Interface
    {
        get { return this.NetflowObject.Interface; }
    }

    public Node Node
    {
        get { return this.NetflowObject.Node; }
    }
}
