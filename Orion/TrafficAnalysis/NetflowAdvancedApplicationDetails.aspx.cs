using System;
using System.Web.UI;
using System.Web.Services;
using SolarWinds.Netflow.Web;
using SolarWinds.Netflow.Config;
using SolarWinds.Netflow.Web.UI;
using SolarWinds.Netflow.Web.Reporting;

public partial class Orion_Netflow_AdvancedApplicationDetails : NetflowDetailView
{
	public override string ViewType
	{
		get { return "NetFlow Advanced Application"; }
	}

    protected override void OnInit(EventArgs e)
    {
        this.advancedApplicationResourceHost.NetflowAdvancedApplication = (NetflowAdvancedApplication)this.NetObject;

        this.Page.Form.Attributes["autocomplete"] = "off";
        base.OnInit(e);

        this.Page.Title = this.ViewInfo.ViewTitle + " - " + AdvancedApplicationName();
    }

    /// <summary>
    /// Application name and port if it is single port application
    /// </summary>
    /// <returns></returns>
    public string AdvancedApplicationName()
    {
        return (this.NetObject as NetflowAdvancedApplication).Name;
    }

    protected void AsyncPostError(object sender, AsyncPostBackErrorEventArgs e)
    {
        myLog.Debug("Caught async exception.", e.Exception);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (OrionWebHelper.UseAsyncBinding(Page))
        {
            this.resourceContainer.DataSource = this.ViewInfo;
            this.resourceContainer.DataBind();
        }
    }
}
