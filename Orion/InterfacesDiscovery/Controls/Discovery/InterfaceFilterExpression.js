﻿function initInterfaceFilter(controlID) {

    var RegexNum = 0;               // # of regex operator selected, this is needed for hiding ninja tooltip
    
    // converts expressions to JSON
    function GetExpressions() {
        var expressions = new Array();
    
        $.each($("#AdvancedOptions:not(.hidden) .expression"), function (i, o) {
            expressions.push({
                __type: 'InterfaceFiltering+Expression',
                Prop: $(".property", o).val(),
                Op: $(".operator:not(.hidden)", o).val(),
                Val: escape($(".value", o).val())
            });
        });
    
        return JSON.stringify(expressions);
    }
    
    function Load(initialState) {
        var expr = JSON.parse(initialState || '[]');
        if(!expr || expr.length == 0) {
            // prefill first expression (must be done here to know about VLANs
            var filledTemplate = _.template($('#ExpressionTemplate').html(), '{}');
            $("#SelectedKeywords").html(filledTemplate);
            $("#SelectedKeywords .expressionCell.add").removeClass('hidden');
        }
        else {
            for(var i in expr) {
                AddExpression();
                var exprhtml = $("#SelectedKeywords .expression:last");
                var prop = $(".property", exprhtml);
                prop.val(expr[i].Prop);
                ExpressionPropertyChange.call(prop);
                $(".operator:not(.hidden)", exprhtml).val(expr[i].Op);
                $(".value", exprhtml).val(unescape(expr[i].Val));
            }
            ShowAdvancedOptions();
        }
    }
    
    function ShowAdvancedOptions() {
        $("#AdvancedOptions").removeClass("hidden");
        $("#ShowAdvancedOptions").addClass("hidden");
    }
    
    function AddExpression() {
        var template = $('#ExpressionTemplate').html();
        var filledTemplate = _.template(template, '{}');
    
        // add nex expression at the end
        if ($("#SelectedKeywords .expression:last").length == 0) {
            $("#SelectedKeywords").html(filledTemplate);
        }
        else {
            $("#SelectedKeywords .expression:last").after(filledTemplate);
        }
    
        $("#SelectedKeywords .expressionCell.add:not(.hidden)").addClass('hidden');
        $("#SelectedKeywords .expression:last .expressionCell.add").removeClass('hidden');
    }
    
    function RemoveExpression() {
        if ($("#SelectedKeywords .expression").length == 1) {
            // there is only one expression, do not delete it, rather hide advanced options
    
            $("#AdvancedOptions").addClass("hidden");
            $("#ShowAdvancedOptions").removeClass("hidden");
        }
        else {
            // hide ninja of removing last regex row
            if ($(".operator", $(this).parent()).hasClass('reg')) {
                RegexNum--;
                if (RegexNum == 0)
                    $("#ExpressionNinja").addClass('hidden');
            }
            $(this).parent().remove();
            // make sure last expression has add button visible
            $("#SelectedKeywords .expression:last .expressionCell.add").removeClass('hidden');
        }
    }
    
    function ExpressionPropertyChange() {
        var row = $(this).parent().parent();
    
        if ($(this).val() == 'vlan') {
            $(".operator:not(.vlan)", row).addClass('hidden');
            $(".operator.vlan", row).removeClass('hidden');
        } else {
            $(".operator:not(.vlan)", row).removeClass('hidden');
            $(".operator.vlan", row).addClass('hidden');
        }
    }
    
    function ExpressionOperatorChange() {
        if ($(this).val() == 'regex' || $(this).val() == '!regex') {
            if (!$(this).hasClass('reg')) {
                $(this).addClass('reg');
                RegexNum++;
            }
            $("#ExpressionNinja").removeClass('hidden');
        } else {
            if ($(this).hasClass('reg')) {
                $(this).removeClass('reg');
                RegexNum--;
                if (RegexNum == 0)
                    $("#ExpressionNinja").addClass('hidden');
            }
        }
    }
    
    // called when page is loaded

    $(document).on("click", "#ShowAdvancedOptions", ShowAdvancedOptions);
    $(document).on("change", ".expression .property", ExpressionPropertyChange);
    $(document).on("change", ".expression .operator", ExpressionOperatorChange);
    $(document).on("click", "#SelectedKeywords .expressionCell.add", AddExpression);
    $(document).on("click", "#SelectedKeywords .expressionCell.remove", RemoveExpression);

    var control = $('#' + controlID);

    initialState = $('.SerializedExpression', control).val();
    Load(initialState);

    control.data('store_expression_callback', function () {
        $('.SerializedExpression', control).val(GetExpressions());
    });

    control.data('get_expression_callback', function () {
        return GetExpressions();
    });

    // add ninja tooltip row
    var filledTemplate = _.template($('#ExpressionNinjaTemplate').html(), '{}');
    $("#SelectedKeywords .expression:last").after(filledTemplate);
}