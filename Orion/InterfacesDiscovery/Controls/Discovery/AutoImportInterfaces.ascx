﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoImportInterfaces.ascx.cs" Inherits="Orion_InterfacesDiscovery_Controls_Discovery_AutoImportInterfaces" %>
<%@ Register Src="~/Orion/InterfacesDiscovery/Controls/Discovery/InterfaceFilterExpression.ascx" TagPrefix="orion" TagName="InterfaceFilterExpression" %>

<style type="text/css">
    .interfaces-title { padding: 5px 0 5px 0; }
    table.interfaces-table { width: 100%; }
    table.interfaces-table th {
         text-transform: uppercase; 
         width: 30%;
         text-align: left;
    }
    ul.interface-checkboxes {
        list-style: none;
        padding: 0;
        margin: 0;
        max-height: 320px;
    }
    ul.interface-checkboxes li {
        width: initial !important;
        padding: 2px 0;
    }
    ul.interface-checkboxes li img {
        padding-right: 5px;
        position: relative;
        top: 2px;
    }
    #ExpressionNinja { padding-left: 380px; }
    #AdvancedOptions .ButtonCollapse { 
        background: url("/Orion/Images/Button.Collapse.Arrow.gif") no-repeat scroll 0 0 transparent;
        color: #000000; 
        display: inline-block !important;
        height: 16px;
        width: 16px; 
    }

</style>

<div class="interfaces-title"><%= Resources.InterfacesDiscoveryWebContent.AutoImportWizardInterfacesTitle %></div>

<table class="interfaces-table">
    <tr>
        <th class="status"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_36)%></th>
        <th class="port"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_37)%></th>
        <th class="hardware"><%=(Resources.InterfacesDiscoveryWebContent.INTERFACESFILTERING_MP0_39)%></th>
    </tr>
    <tr>
        <td><asp:CheckBoxList runat="server" ID="InterfaceStatusCheckboxes" RepeatLayout="UnorderedList" CssClass="interface-checkboxes"/></td>
        <td><asp:CheckBoxList runat="server" ID="InterfacePortModeCheckboxes" RepeatLayout="UnorderedList" CssClass="interface-checkboxes"/></td>
        <td><asp:CheckBoxList runat="server" ID="InterfaceHardwareCheckboxes" RepeatLayout="UnorderedList" CssClass="interface-checkboxes"/></td>
    </tr>
</table>

<orion:InterfaceFilterExpression ID="InterfaceFilterExpression1" runat="server" />
